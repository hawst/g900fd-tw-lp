.class final Laah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private NG:Z

.field NH:Z

.field NI:Z

.field private NJ:Ljava/util/HashMap;

.field private synthetic Nf:Lzi;

.field private bf:Z

.field private cZ:I

.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lzi;Landroid/content/Context;ZI)V
    .locals 1

    .prologue
    .line 1514
    iput-object p1, p0, Laah;->Nf:Lzi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1515
    iput-object p2, p0, Laah;->mContext:Landroid/content/Context;

    .line 1516
    iput-boolean p3, p0, Laah;->NG:Z

    .line 1517
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laah;->NJ:Ljava/util/HashMap;

    .line 1518
    iput p4, p0, Laah;->cZ:I

    .line 1519
    return-void
.end method

.method private a(JLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    .line 2476
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2477
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2478
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 2479
    if-nez v0, :cond_0

    .line 2480
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2487
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2488
    new-instance v0, Laam;

    invoke-direct {v0, p0}, Laam;-><init>(Laah;)V

    invoke-static {p3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2494
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 2495
    iget-wide v4, v0, Lwq;->JA:J

    const-wide/16 v6, -0x64

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 2496
    iget-wide v4, v0, Lwq;->Bd:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_2

    .line 2497
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2498
    iget-wide v4, v0, Lwq;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2500
    :cond_2
    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2502
    :cond_3
    iget-wide v4, v0, Lwq;->JA:J

    const-wide/16 v6, -0x65

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    .line 2503
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2504
    iget-wide v4, v0, Lwq;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2506
    :cond_4
    iget-wide v4, v0, Lwq;->JA:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2507
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2508
    iget-wide v4, v0, Lwq;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2510
    :cond_5
    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2514
    :cond_6
    return-void
.end method

.method private static a(JLjava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 10

    .prologue
    .line 2540
    invoke-virtual {p3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2541
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 2542
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvy;

    .line 2543
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 2544
    iget-wide v6, v0, Lwq;->JA:J

    const-wide/16 v8, -0x64

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    iget-wide v6, v0, Lwq;->Bd:J

    cmp-long v0, v6, p0

    if-nez v0, :cond_1

    .line 2546
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2548
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2551
    :cond_2
    return-void
.end method

.method private a(Laaf;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2595
    if-eqz p5, :cond_0

    move v6, v7

    .line 2598
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v4, v8

    .line 2599
    :goto_1
    if-ge v4, v9, :cond_3

    .line 2601
    add-int/lit8 v0, v4, 0x6

    if-gt v0, v9, :cond_1

    const/4 v5, 0x6

    .line 2602
    :goto_2
    new-instance v0, Laap;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Laap;-><init>(Laah;Laaf;Ljava/util/ArrayList;II)V

    .line 2612
    if-eqz v6, :cond_2

    .line 2613
    monitor-enter p5

    .line 2614
    :try_start_0
    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2615
    monitor-exit p5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2599
    :goto_3
    add-int/lit8 v4, v4, 0x6

    goto :goto_1

    :cond_0
    move v6, v8

    .line 2595
    goto :goto_0

    .line 2601
    :cond_1
    sub-int v5, v9, v4

    goto :goto_2

    .line 2615
    :catchall_0
    move-exception v0

    monitor-exit p5

    throw v0

    .line 2617
    :cond_2
    iget-object v1, p0, Laah;->Nf:Lzi;

    invoke-static {v1, v0, v7}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    goto :goto_3

    .line 2622
    :cond_3
    invoke-virtual {p4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2623
    new-instance v0, Laaq;

    invoke-direct {v0, p0, p1, p4}, Laaq;-><init>(Laah;Laaf;Ljava/util/HashMap;)V

    .line 2631
    if-eqz v6, :cond_5

    .line 2632
    monitor-enter p5

    .line 2633
    :try_start_1
    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2634
    monitor-exit p5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2641
    :cond_4
    :goto_4
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2642
    :goto_5
    if-ge v8, v1, :cond_7

    .line 2643
    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyy;

    .line 2644
    new-instance v2, Laar;

    invoke-direct {v2, p0, p1, v0}, Laar;-><init>(Laah;Laaf;Lyy;)V

    .line 2652
    if-eqz v6, :cond_6

    .line 2653
    invoke-virtual {p5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2642
    :goto_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 2634
    :catchall_1
    move-exception v0

    monitor-exit p5

    throw v0

    .line 2636
    :cond_5
    iget-object v1, p0, Laah;->Nf:Lzi;

    invoke-static {v1, v0, v7}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    goto :goto_4

    .line 2655
    :cond_6
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0, v2, v7}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    goto :goto_6

    .line 2658
    :cond_7
    return-void
.end method

.method private a(Ljava/util/HashMap;Lwq;Ljava/util/concurrent/atomic/AtomicBoolean;)Z
    .locals 10

    .prologue
    .line 1763
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 1764
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v1

    .line 1765
    iget v0, v1, Ltu;->Dh:F

    float-to-int v2, v0

    .line 1766
    iget v0, v1, Ltu;->Dg:F

    float-to-int v3, v0

    .line 1768
    iget-wide v4, p2, Lwq;->Bd:J

    .line 1769
    iget-wide v6, p2, Lwq;->JA:J

    const-wide/16 v8, -0x65

    cmp-long v0, v6, v8

    if-nez v0, :cond_5

    .line 1771
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    iget-wide v2, p2, Lwq;->Bd:J

    long-to-int v2, v2

    invoke-interface {v0, v2}, Laaf;->bi(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1773
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1774
    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error loading shortcut into hotseat "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " into position ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p2, Lwq;->Bd:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lwq;->Bb:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lwq;->Bc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") occupied by all apps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    const/4 v0, 0x0

    .line 1849
    :goto_0
    return v0

    .line 1780
    :cond_1
    const-wide/16 v2, -0x65

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lwq;

    .line 1783
    iget-wide v2, p2, Lwq;->Bd:J

    long-to-float v2, v2

    iget v3, v1, Ltu;->Di:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 1784
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading shortcut "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " into hotseat position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p2, Lwq;->Bd:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", position out of bounds: (0 to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, v1, Ltu;->Di:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1788
    const/4 v0, 0x0

    goto :goto_0

    .line 1791
    :cond_2
    if-eqz v0, :cond_4

    .line 1792
    iget-wide v2, p2, Lwq;->Bd:J

    long-to-int v1, v2

    aget-object v1, v0, v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-eqz v1, :cond_3

    .line 1793
    const-string v1, "Launcher.Model"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Error loading shortcut into hotseat "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " into position ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p2, Lwq;->Bd:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p2, Lwq;->Bb:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p2, Lwq;->Bc:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") occupied by "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v0, -0x65

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lwq;

    iget-wide v4, p2, Lwq;->Bd:J

    long-to-int v3, v4

    aget-object v0, v0, v3

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1798
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1800
    :cond_3
    iget-wide v2, p2, Lwq;->Bd:J

    long-to-int v1, v2

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 1801
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1804
    :cond_4
    iget v0, v1, Ltu;->Di:F

    float-to-int v0, v0

    const/4 v1, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lwq;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lwq;

    .line 1805
    iget-wide v2, p2, Lwq;->Bd:J

    long-to-int v1, v2

    aget-object v1, v0, v1

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 1806
    const-wide/16 v2, -0x65

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1807
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1809
    :cond_5
    iget-wide v0, p2, Lwq;->JA:J

    const-wide/16 v6, -0x64

    cmp-long v0, v0, v6

    if-eqz v0, :cond_6

    .line 1811
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1814
    :cond_6
    iget-wide v0, p2, Lwq;->Bd:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1815
    add-int/lit8 v0, v2, 0x1

    add-int/lit8 v1, v3, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lwq;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lwq;

    .line 1816
    iget-wide v6, p2, Lwq;->Bd:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1819
    :cond_7
    iget-wide v0, p2, Lwq;->Bd:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lwq;

    .line 1820
    iget-wide v6, p2, Lwq;->JA:J

    const-wide/16 v8, -0x64

    cmp-long v1, v6, v8

    if-nez v1, :cond_8

    iget v1, p2, Lwq;->Bb:I

    if-ltz v1, :cond_9

    :cond_8
    iget v1, p2, Lwq;->Bc:I

    if-ltz v1, :cond_9

    iget v1, p2, Lwq;->Bb:I

    iget v6, p2, Lwq;->AY:I

    add-int/2addr v1, v6

    if-gt v1, v2, :cond_9

    iget v1, p2, Lwq;->Bc:I

    iget v6, p2, Lwq;->AZ:I

    add-int/2addr v1, v6

    if-le v1, v3, :cond_a

    .line 1823
    :cond_9
    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Error loading shortcut "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " into cell ("

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "-"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p2, Lwq;->Bd:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p2, Lwq;->Bb:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p2, Lwq;->Bc:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") out of screen bounds ( "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1827
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1831
    :cond_a
    iget v1, p2, Lwq;->Bb:I

    move v2, v1

    :goto_1
    iget v1, p2, Lwq;->Bb:I

    iget v3, p2, Lwq;->AY:I

    add-int/2addr v1, v3

    if-ge v2, v1, :cond_d

    .line 1832
    iget v1, p2, Lwq;->Bc:I

    :goto_2
    iget v3, p2, Lwq;->Bc:I

    iget v6, p2, Lwq;->AZ:I

    add-int/2addr v3, v6

    if-ge v1, v3, :cond_c

    .line 1833
    aget-object v3, v0, v2

    aget-object v3, v3, v1

    if-eqz v3, :cond_b

    .line 1834
    const-string v3, "Launcher.Model"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error loading shortcut "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " into cell ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p2, Lwq;->Bd:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") occupied by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v0, v0, v2

    aget-object v0, v0, v1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1839
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1832
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1831
    :cond_c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1843
    :cond_d
    iget v1, p2, Lwq;->Bb:I

    :goto_3
    iget v2, p2, Lwq;->Bb:I

    iget v3, p2, Lwq;->AY:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_f

    .line 1844
    iget v2, p2, Lwq;->Bc:I

    :goto_4
    iget v3, p2, Lwq;->Bc:I

    iget v4, p2, Lwq;->AZ:I

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_e

    .line 1845
    aget-object v3, v0, v1

    aput-object p2, v3, v2

    .line 1844
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1843
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1849
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private static b(JLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 2522
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyy;

    .line 2523
    if-eqz v0, :cond_0

    .line 2524
    iget-wide v2, v0, Lyy;->JA:J

    const-wide/16 v4, -0x64

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    iget-wide v2, v0, Lyy;->Bd:J

    cmp-long v2, v2, p0

    if-nez v2, :cond_1

    .line 2526
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2528
    :cond_1
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2531
    :cond_2
    return-void
.end method

.method private g(IZ)V
    .locals 28

    .prologue
    .line 2664
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    .line 2669
    move-object/from16 v0, p0

    iget-object v4, v0, Laah;->Nf:Lzi;

    invoke-static {v4}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Laaf;

    .line 2670
    if-nez v19, :cond_0

    .line 2672
    const-string v4, "Launcher.Model"

    const-string v5, "LoaderTask running with no launcher"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2786
    :goto_0
    return-void

    .line 2677
    :cond_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2678
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2680
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 2681
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 2682
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 2683
    sget-object v5, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v5

    .line 2684
    :try_start_0
    sget-object v4, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2685
    sget-object v4, Lzi;->MY:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2686
    sget-object v4, Lzi;->MZ:Ljava/util/HashMap;

    invoke-virtual {v15, v4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2687
    sget-object v4, Lzi;->MW:Ljava/util/HashMap;

    invoke-virtual {v14, v4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2688
    sget-object v4, Lzi;->Nb:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2689
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691
    const/16 v4, -0x3e9

    move/from16 v0, p1

    if-eq v0, v4, :cond_3

    const/4 v4, 0x1

    move/from16 v24, v4

    .line 2693
    :goto_1
    if-eqz v24, :cond_4

    .line 2695
    :goto_2
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, p1

    if-lt v0, v4, :cond_1

    .line 2697
    const/16 p1, -0x3e9

    .line 2700
    :cond_1
    if-gez p1, :cond_5

    const-wide/16 v6, -0x1

    .line 2705
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Laah;->Nf:Lzi;

    invoke-virtual {v4}, Lzi;->iu()V

    .line 2708
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2709
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2710
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 2712
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2714
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 2715
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v5, p0

    .line 2717
    invoke-direct/range {v5 .. v10}, Laah;->a(JLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 2719
    move-object/from16 v0, v21

    invoke-static {v6, v7, v12, v0, v11}, Laah;->b(JLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-wide v12, v6

    .line 2721
    invoke-static/range {v12 .. v17}, Laah;->a(JLjava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 2723
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Laah;->q(Ljava/util/ArrayList;)V

    .line 2724
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Laah;->q(Ljava/util/ArrayList;)V

    .line 2727
    new-instance v4, Laas;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v4, v0, v1}, Laas;-><init>(Laah;Laaf;)V

    .line 2735
    move-object/from16 v0, p0

    iget-object v5, v0, Laah;->Nf:Lzi;

    const/4 v6, 0x1

    invoke-static {v5, v4, v6}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    .line 2737
    new-instance v4, Laao;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v4, v0, v1, v2}, Laao;-><init>(Laah;Laaf;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Laah;->Nf:Lzi;

    const/4 v6, 0x1

    invoke-static {v5, v4, v6}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    .line 2740
    const/16 v23, 0x0

    move-object/from16 v18, p0

    move-object/from16 v20, v9

    move-object/from16 v22, v16

    invoke-direct/range {v18 .. v23}, Laah;->a(Laaf;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .line 2742
    if-eqz v24, :cond_2

    .line 2743
    new-instance v4, Laat;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p1

    invoke-direct {v4, v0, v1, v2}, Laat;-><init>(Laah;Laaf;I)V

    .line 2751
    move-object/from16 v0, p0

    iget-object v5, v0, Laah;->Nf:Lzi;

    const/4 v6, 0x1

    invoke-static {v5, v4, v6}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    .line 2756
    :cond_2
    sget-object v5, Lzi;->MS:Ljava/util/ArrayList;

    monitor-enter v5

    .line 2757
    :try_start_1
    sget-object v4, Lzi;->MS:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2758
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2759
    if-eqz v24, :cond_6

    sget-object v13, Lzi;->MS:Ljava/util/ArrayList;

    :goto_4
    move-object/from16 v8, p0

    move-object/from16 v9, v19

    move-object/from16 v12, v17

    invoke-direct/range {v8 .. v13}, Laah;->a(Laaf;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    .line 2763
    new-instance v17, Laaj;

    move-object/from16 v18, p0

    move/from16 v20, p2

    move-wide/from16 v21, v26

    invoke-direct/range {v17 .. v22}, Laaj;-><init>(Laah;Laaf;ZJ)V

    .line 2779
    if-eqz v24, :cond_7

    .line 2780
    sget-object v5, Lzi;->MS:Ljava/util/ArrayList;

    monitor-enter v5

    .line 2781
    :try_start_2
    sget-object v4, Lzi;->MS:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2782
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 2689
    :catchall_1
    move-exception v4

    monitor-exit v5

    throw v4

    .line 2691
    :cond_3
    const/4 v4, 0x0

    move/from16 v24, v4

    goto/16 :goto_1

    .line 2693
    :cond_4
    invoke-interface/range {v19 .. v19}, Laaf;->hP()I

    move-result p1

    goto/16 :goto_2

    .line 2700
    :cond_5
    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto/16 :goto_3

    .line 2758
    :catchall_2
    move-exception v4

    monitor-exit v5

    throw v4

    .line 2759
    :cond_6
    const/4 v13, 0x0

    goto :goto_4

    .line 2784
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Laah;->Nf:Lzi;

    const/4 v5, 0x1

    move-object/from16 v0, v17

    invoke-static {v4, v0, v5}, Lzi;->a(Lzi;Ljava/lang/Runnable;I)V

    goto/16 :goto_0
.end method

.method private iF()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1531
    iput-boolean v0, p0, Laah;->NH:Z

    .line 1538
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->c(Lzi;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1540
    invoke-direct {p0}, Laah;->iJ()Z

    .line 1541
    monitor-enter p0

    .line 1542
    :try_start_0
    iget-boolean v0, p0, Laah;->bf:Z

    if-eqz v0, :cond_0

    .line 1543
    monitor-exit p0

    .line 1551
    :goto_0
    return v2

    .line 1545
    :cond_0
    iget-object v0, p0, Laah;->Nf:Lzi;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lzi;->b(Lzi;Z)Z

    .line 1546
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1550
    :cond_1
    const/4 v0, -0x1

    invoke-direct {p0, v0, v2}, Laah;->g(IZ)V

    goto :goto_0

    .line 1546
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private iH()V
    .locals 8

    .prologue
    .line 1739
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->j(Lzi;)Lyu;

    sget-object v1, Lyu;->Mi:Landroid/content/Context;

    .line 1743
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1744
    sget-object v3, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v3

    .line 1745
    :try_start_0
    iget-object v0, p0, Laah;->Nf:Lzi;

    iget-object v0, v0, Lzi;->MU:Lrp;

    iget-object v0, v0, Lrp;->xk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrr;

    .line 1746
    iget-object v5, p0, Laah;->Nf:Lzi;

    iget-object v6, v0, Lrr;->xr:Landroid/content/ComponentName;

    iget-object v7, v0, Lrr;->Jl:Lahz;

    invoke-static {v5, v6, v7}, Lzi;->a(Lzi;Landroid/content/ComponentName;Lahz;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1747
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1749
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1751
    const-string v5, "Launcher.Model"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Missing Application on load: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1754
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1755
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1756
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-virtual {v0, v1, v2}, Lzi;->b(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 1758
    :cond_2
    return-void
.end method

.method private static iI()V
    .locals 2

    .prologue
    .line 1854
    sget-object v1, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v1

    .line 1855
    :try_start_0
    sget-object v0, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1856
    sget-object v0, Lzi;->MY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1857
    sget-object v0, Lzi;->MZ:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1858
    sget-object v0, Lzi;->MW:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1859
    sget-object v0, Lzi;->Na:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1860
    sget-object v0, Lzi;->Nb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1861
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private iJ()Z
    .locals 53

    .prologue
    .line 1867
    const-string v4, "Launcher.Model"

    const-string v5, "11683562 - loadWorkspace()"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1869
    move-object/from16 v0, p0

    iget-object v15, v0, Laah;->mContext:Landroid/content/Context;

    .line 1872
    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 1873
    invoke-virtual {v15}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v22

    .line 1874
    invoke-static {v15}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v23

    .line 1875
    invoke-virtual/range {v22 .. v22}, Landroid/content/pm/PackageManager;->isSafeMode()Z

    move-result v24

    .line 1876
    invoke-static {v15}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v25

    .line 1877
    const/4 v5, 0x0

    new-instance v6, Landroid/content/IntentFilter;

    const-string v7, "com.android.launcher3.SYSTEM_READY"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    move/from16 v21, v5

    .line 1880
    :goto_0
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v5

    .line 1881
    iget-object v5, v5, Lyu;->Mk:Lur;

    invoke-virtual {v5}, Lur;->gl()Ltu;

    move-result-object v5

    .line 1882
    iget v6, v5, Ltu;->Dh:F

    .line 1883
    iget v5, v5, Ltu;->Dg:F

    .line 1885
    move-object/from16 v0, p0

    iget v5, v0, Laah;->cZ:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    .line 1886
    const-string v5, "Launcher.Model"

    const-string v6, "loadWorkspace: resetting launcher database"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1887
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher3/LauncherProvider;->iP()V

    .line 1890
    :cond_0
    move-object/from16 v0, p0

    iget v5, v0, Laah;->cZ:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_4

    .line 1892
    const-string v5, "Launcher.Model"

    const-string v6, "loadWorkspace: migrating from launcher2"

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1893
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher3/LauncherProvider;->iO()V

    .line 1901
    :goto_1
    const-string v5, "Launcher.Model"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "11683562 -   loadedOldDb: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1906
    sget-object v26, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v26

    .line 1907
    :try_start_0
    invoke-static {}, Laah;->iI()V

    .line 1908
    move-object/from16 v0, p0

    iget-object v5, v0, Laah;->mContext:Landroid/content/Context;

    invoke-static {v5}, Laht;->C(Landroid/content/Context;)Laht;

    move-result-object v5

    invoke-virtual {v5}, Laht;->lK()Ljava/util/HashSet;

    move-result-object v27

    .line 1911
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 1912
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 1913
    sget-object v5, Labn;->OT:Landroid/net/Uri;

    .line 1915
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1920
    new-instance v30, Ljava/util/HashMap;

    invoke-direct/range {v30 .. v30}, Ljava/util/HashMap;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1923
    :try_start_1
    const-string v6, "_id"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v31

    .line 1924
    const-string v6, "intent"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v32

    .line 1926
    const-string v6, "title"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 1928
    const-string v6, "iconType"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 1930
    const-string v6, "icon"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 1931
    const-string v6, "iconPackage"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    .line 1933
    const-string v6, "iconResource"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    .line 1935
    const-string v6, "container"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v33

    .line 1937
    const-string v6, "itemType"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v34

    .line 1939
    const-string v6, "appWidgetId"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v35

    .line 1941
    const-string v6, "appWidgetProvider"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v36

    .line 1943
    const-string v6, "screen"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v37

    .line 1945
    const-string v6, "cellX"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v38

    .line 1947
    const-string v6, "cellY"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v39

    .line 1949
    const-string v6, "spanX"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v40

    .line 1951
    const-string v6, "spanY"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v41

    .line 1953
    const-string v6, "restored"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v42

    .line 1955
    const-string v6, "profileId"

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v43

    .line 1969
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v6, v0, Laah;->bf:Z

    if-nez v6, :cond_28

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_28

    .line 1970
    new-instance v44, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v6, 0x0

    move-object/from16 v0, v44

    invoke-direct {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1972
    :try_start_2
    move/from16 v0, v34

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 1973
    move/from16 v0, v42

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_5

    const/4 v9, 0x1

    .line 1974
    :goto_3
    const/4 v6, 0x0

    .line 1976
    packed-switch v19, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 1979
    :pswitch_1
    move/from16 v0, v31

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v46

    .line 1980
    move/from16 v0, v32

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 1981
    move/from16 v0, v43

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-long v0, v7

    move-wide/from16 v48, v0

    .line 1982
    move-object/from16 v0, p0

    iget-object v7, v0, Laah;->Nf:Lzi;

    invoke-static {v7}, Lzi;->k(Lzi;)Laia;

    move-result-object v7

    move-wide/from16 v0, v48

    invoke-virtual {v7, v0, v1}, Laia;->m(J)Lahz;

    move-result-object v8

    .line 1983
    move/from16 v0, v42

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1984
    if-nez v8, :cond_6

    .line 1986
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2322
    :catch_0
    move-exception v6

    .line 2323
    :try_start_3
    const-string v7, "Launcher.Model"

    const-string v8, "Desktop items loading interrupted"

    const/4 v9, 0x1

    invoke-static {v7, v8, v6, v9}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 2327
    :catchall_0
    move-exception v4

    if-eqz v10, :cond_2

    .line 2328
    :try_start_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2465
    :catchall_1
    move-exception v4

    monitor-exit v26

    throw v4

    .line 1877
    :cond_3
    const/4 v5, 0x0

    move/from16 v21, v5

    goto/16 :goto_0

    .line 1896
    :cond_4
    const-string v5, "Launcher.Model"

    const-string v6, "loadWorkspace: loading default favorites"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1897
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/launcher3/LauncherProvider;->iN()V

    goto/16 :goto_1

    .line 1973
    :cond_5
    const/4 v9, 0x0

    goto :goto_3

    .line 1990
    :cond_6
    const/4 v13, 0x0

    :try_start_5
    move-object/from16 v0, v20

    invoke-static {v0, v13}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v13

    .line 1991
    invoke-virtual {v13}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v45

    .line 1992
    if-eqz v45, :cond_12

    invoke-virtual/range {v45 .. v45}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_12

    .line 1993
    invoke-virtual/range {v45 .. v45}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v25

    invoke-virtual {v0, v14, v8}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v50

    .line 1995
    if-eqz v50, :cond_8

    move-object/from16 v0, v25

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v8}, Lahn;->e(Landroid/content/ComponentName;Lahz;)Z

    move-result v14

    if-eqz v14, :cond_8

    const/4 v14, 0x1

    .line 1998
    :goto_4
    if-eqz v14, :cond_9

    .line 1999
    if-eqz v9, :cond_11

    .line 2001
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2002
    const/4 v9, 0x0

    :cond_7
    :goto_5
    move v14, v6

    move v6, v7

    move-object v7, v13

    .line 2089
    :goto_6
    if-eqz v9, :cond_14

    .line 2090
    :try_start_6
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v9

    invoke-virtual {v8, v9}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 2091
    const-string v8, "Launcher.Model"

    const-string v9, "constructing info for partially restored package"

    const/4 v13, 0x1

    invoke-static {v8, v9, v13}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2094
    move-object/from16 v0, p0

    iget-object v8, v0, Laah;->Nf:Lzi;

    invoke-virtual {v8, v10, v12, v7, v6}, Lzi;->a(Landroid/database/Cursor;ILandroid/content/Intent;I)Ladh;

    move-result-object v6

    .line 2095
    move-object/from16 v0, p0

    iget-object v8, v0, Laah;->Nf:Lzi;

    invoke-static {v8, v10, v15, v7}, Lzi;->a(Lzi;Landroid/database/Cursor;Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v7

    move-object v8, v6

    .line 2123
    :goto_7
    if-eqz v8, :cond_19

    .line 2124
    move-wide/from16 v0, v46

    iput-wide v0, v8, Ladh;->id:J

    .line 2125
    iput-object v7, v8, Ladh;->intent:Landroid/content/Intent;

    .line 2126
    move/from16 v0, v33

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 2127
    int-to-long v0, v9

    move-wide/from16 v50, v0

    move-wide/from16 v0, v50

    iput-wide v0, v8, Ladh;->JA:J

    .line 2128
    move/from16 v0, v37

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v50, v0

    move-wide/from16 v0, v50

    iput-wide v0, v8, Ladh;->Bd:J

    .line 2129
    move/from16 v0, v38

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iput v6, v8, Ladh;->Bb:I

    .line 2130
    move/from16 v0, v39

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iput v6, v8, Ladh;->Bc:I

    .line 2131
    const/4 v6, 0x1

    iput v6, v8, Ladh;->AY:I

    .line 2132
    const/4 v6, 0x1

    iput v6, v8, Ladh;->AZ:I

    .line 2133
    iget-object v6, v8, Ladh;->intent:Landroid/content/Intent;

    const-string v13, "profile"

    move-wide/from16 v0, v48

    invoke-virtual {v6, v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2134
    if-eqz v24, :cond_17

    invoke-static {v15, v7}, Ladp;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v6

    if-nez v6, :cond_17

    const/4 v6, 0x1

    :goto_8
    iput-boolean v6, v8, Ladh;->SV:Z

    .line 2138
    const/4 v6, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2139
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v8, v2}, Laah;->a(Ljava/util/HashMap;Lwq;Ljava/util/concurrent/atomic/AtomicBoolean;)Z

    move-result v6

    if-nez v6, :cond_18

    .line 2140
    invoke-virtual/range {v44 .. v44}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2141
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    .line 1995
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_4

    .line 2004
    :cond_9
    if-eqz v50, :cond_c

    .line 2005
    const/4 v13, 0x0

    .line 2006
    and-int/lit8 v9, v7, 0x2

    if-eqz v9, :cond_a

    .line 2009
    :try_start_7
    invoke-virtual/range {v45 .. v45}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    .line 2011
    if-eqz v13, :cond_a

    .line 2012
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 2013
    const-string v14, "intent"

    const/16 v50, 0x0

    move/from16 v0, v50

    invoke-virtual {v13, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    invoke-virtual {v9, v14, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2015
    const-string v14, "_id= ?"

    .line 2016
    const/16 v50, 0x1

    move/from16 v0, v50

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v50, v0

    const/16 v51, 0x0

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v52

    aput-object v52, v50, v51

    .line 2017
    move-object/from16 v0, v50

    invoke-virtual {v4, v5, v9, v14, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2021
    :cond_a
    if-nez v13, :cond_b

    .line 2024
    const-string v6, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid component removed: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2026
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/net/URISyntaxException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 2084
    :catch_1
    move-exception v6

    :try_start_8
    const-string v6, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid uri: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 2030
    :cond_b
    :try_start_9
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2031
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 2033
    :cond_c
    if-eqz v9, :cond_e

    .line 2036
    const-string v14, "Launcher.Model"

    new-instance v50, Ljava/lang/StringBuilder;

    const-string v51, "package not yet restored: "

    invoke-direct/range {v50 .. v51}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v50

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    const/16 v51, 0x1

    move-object/from16 v0, v50

    move/from16 v1, v51

    invoke-static {v14, v0, v1}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2039
    and-int/lit8 v14, v7, 0x8

    if-nez v14, :cond_11

    .line 2041
    invoke-virtual/range {v45 .. v45}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 2043
    or-int/lit8 v7, v7, 0x8

    .line 2044
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 2045
    const-string v45, "restored"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    move-object/from16 v0, v45

    move-object/from16 v1, v50

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2047
    const-string v45, "_id= ?"

    .line 2048
    const/16 v50, 0x1

    move/from16 v0, v50

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v50, v0

    const/16 v51, 0x0

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v52

    aput-object v52, v50, v51

    .line 2049
    move-object/from16 v0, v45

    move-object/from16 v1, v50

    invoke-virtual {v4, v5, v14, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_5

    .line 2052
    :cond_d
    const-string v6, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unrestored package removed: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2054
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2057
    :cond_e
    if-eqz v21, :cond_f

    .line 2060
    const-string v6, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid package removed: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2062
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2067
    :cond_f
    const-string v6, "Launcher.Model"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v50, "Invalid package: "

    move-object/from16 v0, v50

    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v50, " (check again later)"

    move-object/from16 v0, v50

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/16 v50, 0x1

    move/from16 v0, v50

    invoke-static {v6, v14, v0}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2069
    sget-object v6, Lzi;->Nc:Ljava/util/HashMap;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashSet;

    .line 2070
    if-nez v6, :cond_10

    .line 2071
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2072
    sget-object v14, Lzi;->Nc:Ljava/util/HashMap;

    invoke-virtual {v14, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2074
    :cond_10
    invoke-virtual/range {v45 .. v45}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2075
    const/4 v6, 0x1

    :cond_11
    move v14, v6

    move v6, v7

    move-object v7, v13

    .line 2078
    goto/16 :goto_6

    :cond_12
    if-nez v45, :cond_7

    .line 2080
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/net/URISyntaxException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2081
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 2098
    :cond_13
    :try_start_a
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2101
    :cond_14
    if-nez v19, :cond_15

    .line 2103
    move-object/from16 v0, p0

    iget-object v6, v0, Laah;->Nf:Lzi;

    move-object/from16 v0, p0

    iget-object v13, v0, Laah;->NJ:Ljava/util/HashMap;

    move-object v9, v15

    invoke-virtual/range {v6 .. v14}, Lzi;->a(Landroid/content/Intent;Lahz;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;Z)Ladh;

    move-result-object v6

    move-object v8, v6

    goto/16 :goto_7

    .line 2106
    :cond_15
    move-object/from16 v0, p0

    iget-object v13, v0, Laah;->Nf:Lzi;

    move-object v14, v10

    move/from16 v19, v11

    move/from16 v20, v12

    invoke-static/range {v13 .. v20}, Lzi;->a(Lzi;Landroid/database/Cursor;Landroid/content/Context;IIIII)Ladh;

    move-result-object v6

    .line 2113
    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_16

    invoke-virtual {v7}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v8

    if-eqz v8, :cond_16

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android.intent.action.MAIN"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    invoke-virtual {v7}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v8

    const-string v9, "android.intent.category.LAUNCHER"

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 2117
    const/high16 v8, 0x10200000

    invoke-virtual {v7, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_16
    move-object v8, v6

    goto/16 :goto_7

    .line 2134
    :cond_17
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 2146
    :cond_18
    packed-switch v9, :pswitch_data_1

    .line 2153
    sget-object v6, Lzi;->MZ:Ljava/util/HashMap;

    int-to-long v0, v9

    move-wide/from16 v44, v0

    move-wide/from16 v0, v44

    invoke-static {v6, v0, v1}, Lzi;->b(Ljava/util/HashMap;J)Lvy;

    move-result-object v6

    .line 2155
    invoke-virtual {v6, v8}, Lvy;->j(Ladh;)V

    .line 2158
    :goto_9
    sget-object v6, Lzi;->MW:Ljava/util/HashMap;

    iget-wide v0, v8, Ladh;->id:J

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2162
    move-object/from16 v0, p0

    iget-object v6, v0, Laah;->Nf:Lzi;

    sget-object v7, Lzi;->Na:Ljava/util/HashMap;

    invoke-virtual {v6, v7, v8, v10, v11}, Lzi;->a(Ljava/util/HashMap;Ladh;Landroid/database/Cursor;I)Z

    goto/16 :goto_2

    .line 2149
    :pswitch_2
    sget-object v6, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2164
    :cond_19
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Unexpected null ShortcutInfo"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2169
    :pswitch_3
    move/from16 v0, v31

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 2170
    sget-object v8, Lzi;->MZ:Ljava/util/HashMap;

    invoke-static {v8, v6, v7}, Lzi;->b(Ljava/util/HashMap;J)Lvy;

    move-result-object v8

    .line 2172
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v8, Lvy;->title:Ljava/lang/CharSequence;

    .line 2173
    iput-wide v6, v8, Lvy;->id:J

    .line 2174
    move/from16 v0, v33

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 2175
    int-to-long v0, v13

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    iput-wide v0, v8, Lvy;->JA:J

    .line 2176
    move/from16 v0, v37

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    int-to-long v0, v14

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    iput-wide v0, v8, Lvy;->Bd:J

    .line 2177
    move/from16 v0, v38

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    iput v14, v8, Lvy;->Bb:I

    .line 2178
    move/from16 v0, v39

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    iput v14, v8, Lvy;->Bc:I

    .line 2179
    const/4 v14, 0x1

    iput v14, v8, Lvy;->AY:I

    .line 2180
    const/4 v14, 0x1

    iput v14, v8, Lvy;->AZ:I

    .line 2183
    const/4 v14, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v14}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2184
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v8, v2}, Laah;->a(Ljava/util/HashMap;Lwq;Ljava/util/concurrent/atomic/AtomicBoolean;)Z

    move-result v14

    if-nez v14, :cond_1a

    .line 2186
    invoke-virtual/range {v44 .. v44}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2187
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2192
    :cond_1a
    packed-switch v13, :pswitch_data_2

    .line 2199
    :goto_a
    if-eqz v9, :cond_1b

    .line 2201
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2204
    :cond_1b
    sget-object v6, Lzi;->MW:Ljava/util/HashMap;

    iget-wide v0, v8, Lvy;->id:J

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2205
    sget-object v6, Lzi;->MZ:Ljava/util/HashMap;

    iget-wide v0, v8, Lvy;->id:J

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 2195
    :pswitch_4
    sget-object v13, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 2210
    :pswitch_5
    move/from16 v0, v35

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 2211
    move/from16 v0, v36

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 2212
    move/from16 v0, v31

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v46

    .line 2213
    invoke-static/range {v19 .. v19}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v20

    .line 2216
    move/from16 v0, v42

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 2217
    and-int/lit8 v6, v8, 0x1

    if-nez v6, :cond_1c

    const/4 v6, 0x1

    move v13, v6

    .line 2220
    :goto_b
    and-int/lit8 v6, v8, 0x2

    if-nez v6, :cond_1d

    const/4 v6, 0x1

    move v9, v6

    .line 2223
    :goto_c
    if-eqz v13, :cond_1e

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v6

    move-object v7, v6

    .line 2227
    :goto_d
    invoke-static {v7}, Lzi;->a(Landroid/appwidget/AppWidgetProviderInfo;)Z

    move-result v6

    .line 2228
    if-nez v24, :cond_1f

    if-eqz v9, :cond_1f

    if-nez v6, :cond_1f

    .line 2229
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Deleting widget that isn\'t installed anymore: id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v46

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " appWidgetId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2231
    const-string v7, "Launcher.Model"

    invoke-static {v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2232
    const-string v7, "Launcher.Model"

    const/4 v8, 0x0

    invoke-static {v7, v6, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2233
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2217
    :cond_1c
    const/4 v6, 0x0

    move v13, v6

    goto :goto_b

    .line 2220
    :cond_1d
    const/4 v6, 0x0

    move v9, v6

    goto :goto_c

    .line 2223
    :cond_1e
    move-object/from16 v0, v20

    invoke-static {v15, v0}, Lzi;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v6

    move-object v7, v6

    goto :goto_d

    .line 2235
    :cond_1f
    if-eqz v6, :cond_22

    .line 2236
    new-instance v6, Lyy;

    iget-object v0, v7, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v6, v14, v0}, Lyy;-><init>(ILandroid/content/ComponentName;)V

    .line 2238
    invoke-static {v15, v7}, Lcom/android/launcher3/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v7

    .line 2240
    const/4 v14, 0x0

    aget v14, v7, v14

    iput v14, v6, Lyy;->JB:I

    .line 2241
    const/4 v14, 0x1

    aget v7, v7, v14

    iput v7, v6, Lyy;->JC:I

    .line 2244
    if-nez v9, :cond_32

    .line 2249
    if-eqz v13, :cond_21

    .line 2250
    const/4 v7, 0x0

    .line 2256
    :goto_e
    iput v7, v6, Lyy;->Mq:I

    .line 2279
    :cond_20
    :goto_f
    move-wide/from16 v0, v46

    iput-wide v0, v6, Lyy;->id:J

    .line 2280
    move/from16 v0, v37

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-long v0, v7

    move-wide/from16 v48, v0

    move-wide/from16 v0, v48

    iput-wide v0, v6, Lyy;->Bd:J

    .line 2281
    move/from16 v0, v38

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v6, Lyy;->Bb:I

    .line 2282
    move/from16 v0, v39

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v6, Lyy;->Bc:I

    .line 2283
    move/from16 v0, v40

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v6, Lyy;->AY:I

    .line 2284
    move/from16 v0, v41

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v6, Lyy;->AZ:I

    .line 2286
    move/from16 v0, v33

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 2287
    const/16 v9, -0x64

    if-eq v7, v9, :cond_24

    const/16 v9, -0x65

    if-eq v7, v9, :cond_24

    .line 2289
    const-string v6, "Launcher.Model"

    const-string v7, "Widget found where container != CONTAINER_DESKTOP nor CONTAINER_HOTSEAT - ignoring!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2252
    :cond_21
    and-int/lit8 v7, v8, -0x3

    goto :goto_e

    .line 2258
    :cond_22
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Widget restore pending id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v46

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " appWidgetId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " status ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2261
    new-instance v6, Lyy;

    move-object/from16 v0, v20

    invoke-direct {v6, v14, v0}, Lyy;-><init>(ILandroid/content/ComponentName;)V

    .line 2263
    iput v8, v6, Lyy;->Mq:I

    .line 2265
    and-int/lit8 v7, v8, 0x8

    if-nez v7, :cond_20

    .line 2267
    invoke-virtual/range {v20 .. v20}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_23

    .line 2269
    iget v7, v6, Lyy;->Mq:I

    or-int/lit8 v7, v7, 0x8

    iput v7, v6, Lyy;->Mq:I

    goto/16 :goto_f

    .line 2272
    :cond_23
    const-string v6, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unrestored widget removed: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2274
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2294
    :cond_24
    move/from16 v0, v33

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-long v0, v7

    move-wide/from16 v48, v0

    move-wide/from16 v0, v48

    iput-wide v0, v6, Lyy;->JA:J

    .line 2296
    const/4 v7, 0x0

    move-object/from16 v0, v44

    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2297
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v6, v2}, Laah;->a(Ljava/util/HashMap;Lwq;Ljava/util/concurrent/atomic/AtomicBoolean;)Z

    move-result v7

    if-nez v7, :cond_25

    .line 2299
    invoke-virtual/range {v44 .. v44}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2300
    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2305
    :cond_25
    iget-object v7, v6, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v7

    .line 2306
    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_26

    iget v9, v6, Lyy;->Mq:I

    if-eq v9, v8, :cond_27

    .line 2308
    :cond_26
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2309
    const-string v9, "appWidgetProvider"

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2311
    const-string v7, "restored"

    iget v9, v6, Lyy;->Mq:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2313
    const-string v7, "_id= ?"

    .line 2314
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v9, v13

    .line 2315
    invoke-virtual {v4, v5, v8, v7, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2317
    :cond_27
    sget-object v7, Lzi;->MW:Ljava/util/HashMap;

    iget-wide v8, v6, Lyy;->id:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2318
    sget-object v7, Lzi;->MY:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_2

    .line 2327
    :cond_28
    if-eqz v10, :cond_29

    .line 2328
    :try_start_b
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 2333
    :cond_29
    move-object/from16 v0, p0

    iget-boolean v6, v0, Laah;->bf:Z

    if-eqz v6, :cond_2a

    .line 2334
    invoke-static {}, Laah;->iI()V

    .line 2335
    const/4 v4, 0x0

    monitor-exit v26

    .line 2466
    :goto_10
    return v4

    .line 2338
    :cond_2a
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_2b

    .line 2339
    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v7

    .line 2342
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_11
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-wide v10

    .line 2348
    const/4 v6, 0x0

    :try_start_c
    invoke-static {v10, v11, v6}, Labn;->a(JZ)Landroid/net/Uri;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v12, 0x0

    invoke-virtual {v7, v6, v9, v12}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_11

    .line 2351
    :catch_2
    move-exception v6

    :try_start_d
    const-string v6, "Launcher.Model"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v12, "Could not remove id = "

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 2356
    :cond_2b
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_2c

    .line 2357
    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-result-object v4

    .line 2361
    :try_start_e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2362
    const-string v6, "_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2363
    const-string v6, " IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2364
    const-string v6, ", "

    move-object/from16 v0, v29

    invoke-static {v6, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2365
    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2366
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 2367
    const-string v7, "restored"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2368
    sget-object v7, Labn;->OT:Landroid/net/Uri;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v6, v5, v8}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 2375
    :cond_2c
    :goto_12
    if-nez v21, :cond_2d

    :try_start_f
    sget-object v4, Lzi;->Nc:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2d

    .line 2376
    new-instance v4, Laae;

    move-object/from16 v0, p0

    iget-object v5, v0, Laah;->Nf:Lzi;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Laae;-><init>(Lzi;B)V

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "com.android.launcher3.SYSTEM_READY"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-static {}, Lzi;->iC()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v15, v4, v5, v6, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 2381
    :cond_2d
    move-object/from16 v0, p0

    iget-object v4, v0, Laah;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lzi;->u(Landroid/content/Context;)Ljava/util/TreeMap;

    move-result-object v5

    .line 2412
    invoke-virtual {v5}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_13
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 2413
    sget-object v7, Lzi;->Nb:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 2371
    :catch_3
    move-exception v4

    const-string v4, "Launcher.Model"

    const-string v5, "Could not update restored rows"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12

    .line 2416
    :cond_2e
    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "11683562 -   sBgWorkspaceScreens: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ", "

    sget-object v7, Lzi;->Nb:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2420
    new-instance v5, Ljava/util/ArrayList;

    sget-object v4, Lzi;->Nb:Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2421
    sget-object v4, Lzi;->MW:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2f
    :goto_14
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lwq;

    .line 2422
    iget-wide v8, v4, Lwq;->Bd:J

    .line 2423
    iget-wide v10, v4, Lwq;->JA:J

    const-wide/16 v12, -0x64

    cmp-long v4, v10, v12

    if-nez v4, :cond_2f

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 2425
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_14

    .line 2430
    :cond_30
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_31

    .line 2432
    const-string v4, "Launcher.Model"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "11683562 -   unusedScreens (to be removed): "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ", "

    invoke-static {v7, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v4, v6, v7}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2435
    sget-object v4, Lzi;->Nb:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 2436
    move-object/from16 v0, p0

    iget-object v4, v0, Laah;->Nf:Lzi;

    sget-object v5, Lzi;->Nb:Ljava/util/ArrayList;

    invoke-virtual {v4, v15, v5}, Lzi;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 2465
    :cond_31
    monitor-exit v26
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 2466
    const/4 v4, 0x0

    goto/16 :goto_10

    :cond_32
    move v7, v8

    goto/16 :goto_e

    .line 1976
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 2146
    :pswitch_data_1
    .packed-switch -0x65
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 2192
    :pswitch_data_2
    .packed-switch -0x65
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private iK()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2806
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    .line 2807
    if-nez v0, :cond_0

    .line 2809
    const-string v0, "Launcher.Model"

    const-string v1, "LoaderTask running with no launcher (onlyBindAllApps)"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2836
    :goto_0
    return-void

    .line 2815
    :cond_0
    iget-object v1, p0, Laah;->Nf:Lzi;

    iget-object v1, v1, Lzi;->MU:Lrp;

    iget-object v1, v1, Lrp;->xk:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 2817
    new-instance v3, Laak;

    invoke-direct {v3, p0, v0, v1}, Laak;-><init>(Laah;Laaf;Ljava/util/ArrayList;)V

    .line 2830
    invoke-static {}, Lzi;->iD()Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    .line 2831
    :goto_1
    if-eqz v0, :cond_2

    .line 2832
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2830
    goto :goto_1

    .line 2834
    :cond_2
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->d(Lzi;)Ltj;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Ltj;->a(Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method private q(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 2556
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 2557
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 2559
    new-instance v1, Laan;

    invoke-direct {v1, p0, v0}, Laan;-><init>(Laah;Ltu;)V

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2573
    return-void
.end method


# virtual methods
.method final bo(I)V
    .locals 3

    .prologue
    .line 1591
    const/16 v0, -0x3e9

    if-ne p1, v0, :cond_0

    .line 1593
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should not call runBindSynchronousPage() without valid page index"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1596
    :cond_0
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->f(Lzi;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->c(Lzi;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1599
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expecting AllApps and Workspace to be loaded"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1601
    :cond_2
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1602
    :try_start_0
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->h(Lzi;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1605
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Error! Background loading is already running"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1607
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1616
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->d(Lzi;)Ltj;

    move-result-object v0

    invoke-virtual {v0}, Ltj;->flush()V

    .line 1620
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laah;->g(IZ)V

    .line 1623
    invoke-direct {p0}, Laah;->iK()V

    .line 1624
    return-void
.end method

.method final c(Laaf;)Laaf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1716
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1717
    :try_start_0
    iget-boolean v0, p0, Laah;->bf:Z

    if-eqz v0, :cond_0

    .line 1718
    monitor-exit v2

    move-object v0, v1

    .line 1734
    :goto_0
    return-object v0

    .line 1721
    :cond_0
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1722
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 1725
    :cond_1
    iget-object v0, p0, Laah;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    .line 1726
    if-eq v0, p1, :cond_2

    .line 1727
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 1729
    :cond_2
    if-nez v0, :cond_3

    .line 1730
    const-string v0, "Launcher.Model"

    const-string v3, "no mCallbacks"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1731
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 1734
    :cond_3
    monitor-exit v2

    goto :goto_0

    .line 1735
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final ev()V
    .locals 3

    .prologue
    .line 2912
    sget-object v1, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v1

    .line 2913
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mContext="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laah;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2914
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mIsLaunching="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Laah;->NG:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2915
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mStopped="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Laah;->bf:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2916
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mLoadAndBindStepFinished="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Laah;->NI:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2917
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "mItems size="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lzi;->MX:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2918
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final iA()Z
    .locals 1

    .prologue
    .line 1526
    iget-boolean v0, p0, Laah;->NH:Z

    return v0
.end method

.method final iE()Z
    .locals 1

    .prologue
    .line 1522
    iget-boolean v0, p0, Laah;->NG:Z

    return v0
.end method

.method public final iG()V
    .locals 1

    .prologue
    .line 1702
    monitor-enter p0

    .line 1703
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Laah;->bf:Z

    .line 1704
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 1705
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 15

    .prologue
    const/16 v2, 0xa

    const/4 v14, 0x0

    const/4 v10, 0x0

    .line 1627
    iget-object v3, p0, Laah;->Nf:Lzi;

    invoke-static {v3}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1630
    :try_start_0
    iget-object v4, p0, Laah;->Nf:Lzi;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lzi;->c(Lzi;Z)Z

    .line 1631
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1638
    iget-object v3, p0, Laah;->Nf:Lzi;

    invoke-static {v3}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1641
    :try_start_1
    iget-boolean v4, p0, Laah;->NG:Z

    if-eqz v4, :cond_0

    move v2, v10

    :cond_0
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1643
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1645
    invoke-direct {p0}, Laah;->iF()Z

    .line 1647
    iget-boolean v2, p0, Laah;->bf:Z

    if-nez v2, :cond_4

    .line 1648
    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1654
    :try_start_2
    iget-boolean v2, p0, Laah;->NG:Z

    if-eqz v2, :cond_1

    .line 1656
    const/16 v2, 0xa

    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1658
    :cond_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1659
    monitor-enter p0

    :try_start_3
    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->d(Lzi;)Ltj;

    move-result-object v2

    new-instance v3, Laai;

    invoke-direct {v3, p0}, Laai;-><init>(Laah;)V

    new-instance v4, Ltk;

    invoke-direct {v4, v2, v3}, Ltk;-><init>(Ltj;Ljava/lang/Runnable;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v3}, Ltj;->a(Ljava/lang/Runnable;I)V

    :goto_0
    iget-boolean v2, p0, Laah;->bf:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Laah;->NI:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->e(Lzi;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    move-result v2

    if-nez v2, :cond_2

    const-wide/16 v2, 0x3e8

    :try_start_4
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    .line 1631
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 1643
    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2

    .line 1658
    :catchall_2
    move-exception v2

    monitor-exit v3

    throw v2

    .line 1659
    :cond_2
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 1663
    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->f(Lzi;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Laaf;

    if-nez v8, :cond_5

    const-string v2, "Launcher.Model"

    const-string v3, "LoaderTask running with no launcher (loadAllApps)"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    monitor-enter p0

    :try_start_6
    iget-boolean v2, p0, Laah;->bf:Z

    if-eqz v2, :cond_8

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 1666
    :goto_2
    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1667
    const/4 v2, 0x0

    :try_start_7
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1668
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 1673
    :cond_4
    sget-object v4, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v4

    .line 1674
    :try_start_8
    sget-object v2, Lzi;->Na:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1675
    iget-object v6, p0, Laah;->Nf:Lzi;

    iget-object v7, p0, Laah;->mContext:Landroid/content/Context;

    move-object v0, v3

    check-cast v0, Ladh;

    move-object v2, v0

    sget-object v8, Lzi;->Na:Ljava/util/HashMap;

    invoke-virtual {v8, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-virtual {v6, v7, v2, v3}, Lzi;->a(Landroid/content/Context;Ladh;[B)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_3

    .line 1678
    :catchall_3
    move-exception v2

    monitor-exit v4

    throw v2

    .line 1659
    :catchall_4
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1663
    :cond_5
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->k(Lzi;)Laia;

    move-result-object v2

    invoke-virtual {v2}, Laia;->getUserProfiles()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Laah;->Nf:Lzi;

    iget-object v3, v3, Lzi;->MU:Lrp;

    iget-object v4, v3, Lrp;->xk:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iget-object v4, v3, Lrp;->xl:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iget-object v4, v3, Lrp;->O:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    iget-object v3, v3, Lrp;->xm:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lahz;

    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->l(Lzi;)Lahn;

    move-result-object v2

    invoke-virtual {v2, v14, v5}, Lahn;->i(Ljava/lang/String;Lahz;)Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_3

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Laaz;

    iget-object v3, p0, Laah;->NJ:Ljava/util/HashMap;

    invoke-direct {v2, v3}, Laaz;-><init>(Ljava/util/HashMap;)V

    invoke-static {v12, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v9, v10

    :goto_4
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_6

    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lahk;

    iget-object v2, p0, Laah;->Nf:Lzi;

    iget-object v13, v2, Lzi;->MU:Lrp;

    new-instance v2, Lrr;

    iget-object v3, p0, Laah;->mContext:Landroid/content/Context;

    iget-object v6, p0, Laah;->Nf:Lzi;

    invoke-static {v6}, Lzi;->b(Lzi;)Lwi;

    move-result-object v6

    iget-object v7, p0, Laah;->NJ:Ljava/util/HashMap;

    invoke-direct/range {v2 .. v7}, Lrr;-><init>(Landroid/content/Context;Lahk;Lahz;Lwi;Ljava/util/HashMap;)V

    invoke-virtual {v13, v2}, Lrp;->a(Lrr;)V

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_4

    :cond_7
    iget-object v2, p0, Laah;->Nf:Lzi;

    iget-object v2, v2, Lzi;->MU:Lrp;

    iget-object v2, v2, Lrp;->xl:Ljava/util/ArrayList;

    iget-object v3, p0, Laah;->Nf:Lzi;

    iget-object v3, v3, Lzi;->MU:Lrp;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v3, Lrp;->xl:Ljava/util/ArrayList;

    iget-object v3, p0, Laah;->Nf:Lzi;

    invoke-static {v3}, Lzi;->d(Lzi;)Ltj;

    move-result-object v3

    new-instance v4, Laal;

    invoke-direct {v4, p0, v8, v2}, Laal;-><init>(Laah;Laaf;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4, v10}, Ltj;->a(Ljava/lang/Runnable;I)V

    goto/16 :goto_1

    :cond_8
    :try_start_9
    iget-object v2, p0, Laah;->Nf:Lzi;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lzi;->d(Lzi;Z)Z

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    goto/16 :goto_2

    :catchall_5
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_9
    invoke-direct {p0}, Laah;->iK()V

    goto/16 :goto_2

    .line 1668
    :catchall_6
    move-exception v2

    monitor-exit v3

    throw v2

    .line 1677
    :cond_a
    :try_start_a
    sget-object v2, Lzi;->Na:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 1678
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 1680
    invoke-static {}, Lyu;->im()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1684
    invoke-direct {p0}, Laah;->iH()V

    .line 1690
    :cond_b
    iput-object v14, p0, Laah;->mContext:Landroid/content/Context;

    .line 1692
    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->g(Lzi;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1694
    :try_start_b
    iget-object v2, p0, Laah;->Nf:Lzi;

    invoke-static {v2}, Lzi;->i(Lzi;)Laah;

    move-result-object v2

    if-ne v2, p0, :cond_c

    .line 1695
    iget-object v2, p0, Laah;->Nf:Lzi;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lzi;->a(Lzi;Laah;)Laah;

    .line 1697
    :cond_c
    iget-object v2, p0, Laah;->Nf:Lzi;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lzi;->c(Lzi;Z)Z

    .line 1698
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_7

    return-void

    :catchall_7
    move-exception v2

    monitor-exit v3

    throw v2
.end method
