.class public final Lizv;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dVq:[Lizv;


# instance fields
.field private aez:I

.field private afz:I

.field private dPa:J

.field private dRq:J

.field private dVA:I

.field private dVB:I

.field private dVC:I

.field private dVD:I

.field private dVE:J

.field private dVF:[Lizj;

.field public dVG:Ljcu;

.field public dVH:Ljbp;

.field public dVI:Ljde;

.field private dVJ:[B

.field private dVK:[B

.field private dVL:Z

.field private dVM:I

.field private dVN:J

.field public dVr:Liwk;

.field public dVs:Lixx;

.field private dVt:I

.field private dVu:J

.field private dVv:Z

.field private dVw:I

.field private dVx:I

.field private dVy:I

.field private dVz:I

.field public entry:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Lizv;->aez:I

    iput-object v2, p0, Lizv;->dVr:Liwk;

    iput-object v2, p0, Lizv;->entry:Lizj;

    iput-object v2, p0, Lizv;->dVs:Lixx;

    iput-wide v4, p0, Lizv;->dRq:J

    iput v1, p0, Lizv;->dVt:I

    iput-wide v4, p0, Lizv;->dVu:J

    const/4 v0, 0x1

    iput v0, p0, Lizv;->afz:I

    iput-wide v4, p0, Lizv;->dPa:J

    iput-boolean v1, p0, Lizv;->dVv:Z

    iput v1, p0, Lizv;->dVw:I

    iput v1, p0, Lizv;->dVx:I

    iput v1, p0, Lizv;->dVy:I

    iput v1, p0, Lizv;->dVz:I

    iput v1, p0, Lizv;->dVA:I

    iput v1, p0, Lizv;->dVB:I

    iput v1, p0, Lizv;->dVC:I

    iput v1, p0, Lizv;->dVD:I

    iput-wide v4, p0, Lizv;->dVE:J

    invoke-static {}, Lizj;->bcP()[Lizj;

    move-result-object v0

    iput-object v0, p0, Lizv;->dVF:[Lizj;

    iput-object v2, p0, Lizv;->dVG:Ljcu;

    iput-object v2, p0, Lizv;->dVH:Ljbp;

    iput-object v2, p0, Lizv;->dVI:Ljde;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lizv;->dVJ:[B

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lizv;->dVK:[B

    iput-boolean v1, p0, Lizv;->dVL:Z

    iput v1, p0, Lizv;->dVM:I

    iput-wide v4, p0, Lizv;->dVN:J

    iput-object v2, p0, Lizv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizv;->eCz:I

    return-void
.end method

.method public static bdB()[Lizv;
    .locals 2

    sget-object v0, Lizv;->dVq:[Lizv;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lizv;->dVq:[Lizv;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lizv;

    sput-object v0, Lizv;->dVq:[Lizv;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lizv;->dVq:[Lizv;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lizv;->dVr:Liwk;

    if-nez v0, :cond_1

    new-instance v0, Liwk;

    invoke-direct {v0}, Liwk;-><init>()V

    iput-object v0, p0, Lizv;->dVr:Liwk;

    :cond_1
    iget-object v0, p0, Lizv;->dVr:Liwk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lizv;->entry:Lizj;

    if-nez v0, :cond_2

    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    iput-object v0, p0, Lizv;->entry:Lizj;

    :cond_2
    iget-object v0, p0, Lizv;->entry:Lizj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizv;->dRq:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Lizv;->afz:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lizv;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizv;->dVF:[Lizj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lizj;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lizv;->dVF:[Lizj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lizv;->dVF:[Lizj;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lizv;->dVF:[Lizj;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lizv;->dVG:Ljcu;

    if-nez v0, :cond_6

    new-instance v0, Ljcu;

    invoke-direct {v0}, Ljcu;-><init>()V

    iput-object v0, p0, Lizv;->dVG:Ljcu;

    :cond_6
    iget-object v0, p0, Lizv;->dVG:Ljcu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lizv;->dVH:Ljbp;

    if-nez v0, :cond_7

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Lizv;->dVH:Ljbp;

    :cond_7
    iget-object v0, p0, Lizv;->dVH:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lizv;->dVJ:[B

    iget v0, p0, Lizv;->aez:I

    const v2, 0x8000

    or-int/2addr v0, v2

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lizv;->dVs:Lixx;

    if-nez v0, :cond_8

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Lizv;->dVs:Lixx;

    :cond_8
    iget-object v0, p0, Lizv;->dVs:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizv;->dVu:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVt:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lizv;->dVI:Ljde;

    if-nez v0, :cond_9

    new-instance v0, Ljde;

    invoke-direct {v0}, Ljde;-><init>()V

    iput-object v0, p0, Lizv;->dVI:Ljde;

    :cond_9
    iget-object v0, p0, Lizv;->dVI:Ljde;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizv;->dPa:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lizv;->dVv:Z

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVw:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lizv;->dVK:[B

    iget v0, p0, Lizv;->aez:I

    const/high16 v2, 0x10000

    or-int/2addr v0, v2

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVx:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVy:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVz:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lizv;->dVL:Z

    iget v0, p0, Lizv;->aez:I

    const/high16 v2, 0x20000

    or-int/2addr v0, v2

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVA:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVB:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVC:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVD:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizv;->dVE:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizv;->dVM:I

    iget v0, p0, Lizv;->aez:I

    const/high16 v2, 0x40000

    or-int/2addr v0, v2

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizv;->dVN:J

    iget v0, p0, Lizv;->aez:I

    const/high16 v2, 0x80000

    or-int/2addr v0, v2

    iput v0, p0, Lizv;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
        0x78 -> :sswitch_d
        0x80 -> :sswitch_e
        0x88 -> :sswitch_f
        0x92 -> :sswitch_10
        0x98 -> :sswitch_11
        0xa0 -> :sswitch_12
        0xa8 -> :sswitch_13
        0xb0 -> :sswitch_14
        0xb8 -> :sswitch_15
        0xc0 -> :sswitch_16
        0xc8 -> :sswitch_17
        0xd0 -> :sswitch_18
        0xd8 -> :sswitch_19
        0xe0 -> :sswitch_1a
        0xe8 -> :sswitch_1b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget-object v0, p0, Lizv;->dVr:Liwk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lizv;->dVr:Liwk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_0
    iget-object v0, p0, Lizv;->entry:Lizj;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lizv;->entry:Lizj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v2, p0, Lizv;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_2
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lizv;->afz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_3
    iget-object v0, p0, Lizv;->dVF:[Lizj;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lizv;->dVF:[Lizj;

    array-length v0, v0

    if-lez v0, :cond_5

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lizv;->dVF:[Lizj;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lizv;->dVF:[Lizj;

    aget-object v1, v1, v0

    if-eqz v1, :cond_4

    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lizv;->dVG:Ljcu;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lizv;->dVG:Ljcu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_6
    iget-object v0, p0, Lizv;->dVH:Ljbp;

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lizv;->dVH:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_7
    iget v0, p0, Lizv;->aez:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lizv;->dVJ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    :cond_8
    iget-object v0, p0, Lizv;->dVs:Lixx;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lizv;->dVs:Lixx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_9
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-wide v2, p0, Lizv;->dVu:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_a
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget v1, p0, Lizv;->dVt:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_b
    iget-object v0, p0, Lizv;->dVI:Ljde;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lizv;->dVI:Ljde;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_c
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_d

    const/16 v0, 0xf

    iget-wide v2, p0, Lizv;->dPa:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_d
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_e

    const/16 v0, 0x10

    iget-boolean v1, p0, Lizv;->dVv:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_e
    iget v0, p0, Lizv;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_f

    const/16 v0, 0x11

    iget v1, p0, Lizv;->dVw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_f
    iget v0, p0, Lizv;->aez:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_10

    const/16 v0, 0x12

    iget-object v1, p0, Lizv;->dVK:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    :cond_10
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_11

    const/16 v0, 0x13

    iget v1, p0, Lizv;->dVx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_11
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_12

    const/16 v0, 0x14

    iget v1, p0, Lizv;->dVy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_12
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_13

    const/16 v0, 0x15

    iget v1, p0, Lizv;->dVz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_13
    iget v0, p0, Lizv;->aez:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_14

    const/16 v0, 0x16

    iget-boolean v1, p0, Lizv;->dVL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_14
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_15

    const/16 v0, 0x17

    iget v1, p0, Lizv;->dVA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_15
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_16

    const/16 v0, 0x18

    iget v1, p0, Lizv;->dVB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_16
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_17

    const/16 v0, 0x19

    iget v1, p0, Lizv;->dVC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_17
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_18

    const/16 v0, 0x1a

    iget v1, p0, Lizv;->dVD:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_18
    iget v0, p0, Lizv;->aez:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_19

    const/16 v0, 0x1b

    iget-wide v2, p0, Lizv;->dVE:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_19
    iget v0, p0, Lizv;->aez:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_1a

    const/16 v0, 0x1c

    iget v1, p0, Lizv;->dVM:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1a
    iget v0, p0, Lizv;->aez:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_1b

    const/16 v0, 0x1d

    iget-wide v2, p0, Lizv;->dVN:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_1b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bcB()J
    .locals 2

    iget-wide v0, p0, Lizv;->dRq:J

    return-wide v0
.end method

.method public final bdC()I
    .locals 1

    iget v0, p0, Lizv;->dVt:I

    return v0
.end method

.method public final bdD()J
    .locals 2

    iget-wide v0, p0, Lizv;->dVu:J

    return-wide v0
.end method

.method public final bdE()Z
    .locals 1

    iget-boolean v0, p0, Lizv;->dVL:Z

    return v0
.end method

.method public final bdF()Z
    .locals 2

    iget v0, p0, Lizv;->aez:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cw(J)Lizv;
    .locals 1

    iput-wide p1, p0, Lizv;->dRq:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final cx(J)Lizv;
    .locals 1

    iput-wide p1, p0, Lizv;->dVu:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final cy(J)Lizv;
    .locals 1

    iput-wide p1, p0, Lizv;->dVE:J

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final hy(Z)Lizv;
    .locals 1

    iput-boolean p1, p0, Lizv;->dVv:Z

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final hz(Z)Lizv;
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lizv;->dVL:Z

    iget v0, p0, Lizv;->aez:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 5

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget-object v1, p0, Lizv;->dVr:Liwk;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lizv;->dVr:Liwk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lizv;->entry:Lizj;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lizv;->entry:Lizj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Lizv;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Lizv;->afz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lizv;->dVF:[Lizj;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lizv;->dVF:[Lizj;

    array-length v1, v1

    if-lez v1, :cond_6

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lizv;->dVF:[Lizj;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lizv;->dVF:[Lizj;

    aget-object v2, v2, v0

    if-eqz v2, :cond_4

    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    :cond_6
    iget-object v1, p0, Lizv;->dVG:Ljcu;

    if-eqz v1, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lizv;->dVG:Ljcu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lizv;->dVH:Ljbp;

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    iget-object v2, p0, Lizv;->dVH:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lizv;->aez:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-eqz v1, :cond_9

    const/16 v1, 0x9

    iget-object v2, p0, Lizv;->dVJ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lizv;->dVs:Lixx;

    if-eqz v1, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lizv;->dVs:Lixx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_b

    const/16 v1, 0xb

    iget-wide v2, p0, Lizv;->dVu:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_c

    const/16 v1, 0xc

    iget v2, p0, Lizv;->dVt:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lizv;->dVI:Ljde;

    if-eqz v1, :cond_d

    const/16 v1, 0xd

    iget-object v2, p0, Lizv;->dVI:Ljde;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    iget-wide v2, p0, Lizv;->dPa:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    iget-boolean v2, p0, Lizv;->dVv:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Lizv;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_10

    const/16 v1, 0x11

    iget v2, p0, Lizv;->dVw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget v1, p0, Lizv;->aez:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    if-eqz v1, :cond_11

    const/16 v1, 0x12

    iget-object v2, p0, Lizv;->dVK:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_12

    const/16 v1, 0x13

    iget v2, p0, Lizv;->dVx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_13

    const/16 v1, 0x14

    iget v2, p0, Lizv;->dVy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_14

    const/16 v1, 0x15

    iget v2, p0, Lizv;->dVz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget v1, p0, Lizv;->aez:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-eqz v1, :cond_15

    const/16 v1, 0x16

    iget-boolean v2, p0, Lizv;->dVL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_15
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_16

    const/16 v1, 0x17

    iget v2, p0, Lizv;->dVA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_17

    const/16 v1, 0x18

    iget v2, p0, Lizv;->dVB:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_18

    const/16 v1, 0x19

    iget v2, p0, Lizv;->dVC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_19

    const/16 v1, 0x1a

    iget v2, p0, Lizv;->dVD:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    iget v1, p0, Lizv;->aez:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_1a

    const/16 v1, 0x1b

    iget-wide v2, p0, Lizv;->dVE:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1a
    iget v1, p0, Lizv;->aez:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1b

    const/16 v1, 0x1c

    iget v2, p0, Lizv;->dVM:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1b
    iget v1, p0, Lizv;->aez:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1c

    const/16 v1, 0x1d

    iget-wide v2, p0, Lizv;->dVN:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1c
    return v0
.end method

.method public final nX(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVt:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final nY(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVw:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final nZ(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVx:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final oa(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVy:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final ob(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVz:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final oc(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVA:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method

.method public final od(I)Lizv;
    .locals 1

    iput p1, p0, Lizv;->dVD:I

    iget v0, p0, Lizv;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lizv;->aez:I

    return-object p0
.end method
