.class final Laex;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private synthetic UE:Laew;


# direct methods
.method constructor <init>(Laew;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Laex;->UE:Laew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 59
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 82
    :goto_0
    return-void

    .line 61
    :pswitch_0
    iget-object v0, p0, Laex;->UE:Laew;

    iget-object v0, v0, Laew;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v0, p0, Laex;->UE:Laew;

    iget-object v0, v0, Laew;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 67
    :pswitch_2
    iget-object v0, p0, Laex;->UE:Laew;

    invoke-static {v0}, Laew;->a(Laew;)Lcom/android/launcher3/MemoryTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/MemoryTracker;->iU()[I

    move-result-object v2

    .line 69
    iget-object v0, p0, Laex;->UE:Laew;

    invoke-virtual {v0}, Laew;->getChildCount()I

    move-result v3

    .line 70
    array-length v0, v2

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Laex;->UE:Laew;

    invoke-virtual {v0}, Laew;->kD()V

    .line 79
    :cond_0
    :goto_1
    iget-object v0, p0, Laex;->UE:Laew;

    iget-object v0, v0, Laew;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_0

    .line 72
    iget-object v0, p0, Laex;->UE:Laew;

    invoke-virtual {v0, v1}, Laew;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Laez;

    .line 73
    invoke-virtual {v0}, Laez;->getPid()I

    move-result v4

    invoke-static {v2, v4}, Laew;->a([II)I

    move-result v4

    if-gez v4, :cond_2

    .line 74
    iget-object v0, p0, Laex;->UE:Laew;

    invoke-virtual {v0}, Laew;->kD()V

    goto :goto_1

    .line 77
    :cond_2
    invoke-virtual {v0}, Laez;->update()V

    .line 71
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
