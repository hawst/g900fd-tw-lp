.class public abstract Lddj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private bbr:Z

.field private final cs:I

.field public final mEventBus:Ldda;


# direct methods
.method public constructor <init>(Ldda;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p2, p0, Lddj;->cs:I

    .line 47
    iput-object p1, p0, Lddj;->mEventBus:Ldda;

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Landroid/os/Bundle;I)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method protected a(Lcom/google/android/shared/search/Query;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public abstract a(Lddb;)V
.end method

.method public a(Letj;)V
    .locals 3

    .prologue
    .line 175
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 176
    return-void
.end method

.method public final aaJ()Z
    .locals 2

    .prologue
    .line 66
    iget-boolean v0, p0, Lddj;->bbr:Z

    .line 67
    const/4 v1, 0x0

    iput-boolean v1, p0, Lddj;->bbr:Z

    .line 68
    return v0
.end method

.method protected b(Landroid/os/Bundle;I)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 142
    return-void
.end method

.method public final e(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/io/PrintWriter;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 181
    new-instance v1, Letj;

    invoke-direct {v1, p1}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 182
    invoke-virtual {v1, p0}, Letj;->b(Leti;)V

    .line 183
    invoke-virtual {v1, v0}, Letj;->c(Ljava/io/PrintWriter;)V

    .line 184
    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    .line 185
    return-void
.end method

.method protected final getId()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lddj;->cs:I

    return v0
.end method

.method public final notifyChanged()V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddj;->bbr:Z

    .line 57
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    iget v1, p0, Lddj;->cs:I

    invoke-virtual {v0, v1}, Ldda;->fG(I)V

    .line 58
    return-void
.end method

.method protected s(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method
