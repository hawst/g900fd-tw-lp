.class public final Ljiu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiX:Ljcn;

.field private enX:Ljava/lang/String;

.field private enY:Ljava/lang/String;

.field private enZ:Ljava/lang/String;

.field private eoa:J

.field private eob:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljiu;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljiu;->enX:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljiu;->enY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljiu;->enZ:Ljava/lang/String;

    iput-object v2, p0, Ljiu;->aiX:Ljcn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljiu;->eoa:J

    const-string v0, ""

    iput-object v0, p0, Ljiu;->eob:Ljava/lang/String;

    iput-object v2, p0, Ljiu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljiu;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljiu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljiu;->enX:Ljava/lang/String;

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljiu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljiu;->enY:Ljava/lang/String;

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljiu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljiu;->enZ:Ljava/lang/String;

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljiu;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljiu;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljiu;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Ljiu;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljiu;->eoa:J

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljiu;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljiu;->eob:Ljava/lang/String;

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljiu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljiu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljiu;->enX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljiu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljiu;->enY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljiu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljiu;->enZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Ljiu;->aiX:Ljcn;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljiu;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget v0, p0, Ljiu;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v2, p0, Ljiu;->eoa:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_4
    iget v0, p0, Ljiu;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljiu;->eob:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljiu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljiu;->enX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljiu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljiu;->enY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljiu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljiu;->enZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljiu;->aiX:Ljcn;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljiu;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Ljiu;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Ljiu;->eoa:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljiu;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljiu;->eob:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    return v0
.end method

.method public final wc(Ljava/lang/String;)Ljiu;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljiu;->enY:Ljava/lang/String;

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljiu;->aez:I

    return-object p0
.end method

.method public final wd(Ljava/lang/String;)Ljiu;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljiu;->enZ:Ljava/lang/String;

    iget v0, p0, Ljiu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljiu;->aez:I

    return-object p0
.end method
