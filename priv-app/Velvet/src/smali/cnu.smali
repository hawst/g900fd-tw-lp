.class public final Lcnu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcob;


# instance fields
.field final aTW:Ljava/util/concurrent/Executor;

.field private final atG:Leqo;

.field private final bdg:Landroid/content/BroadcastReceiver;

.field final bdh:Landroid/location/LocationManager;

.field final bdi:Landroid/database/ContentObserver;

.field bdj:Z

.field bdk:Z

.field bdl:I

.field bdm:Z

.field final bdn:Ljava/lang/Object;

.field private bdo:Ljava/lang/Boolean;

.field final bdp:Ljava/lang/Runnable;

.field final bdq:Ljava/lang/Runnable;

.field private final bdr:Ljava/util/Set;

.field final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leqo;Ljava/util/concurrent/Executor;)V
    .locals 6

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcnu;->bdn:Ljava/lang/Object;

    .line 81
    new-instance v0, Lcoa;

    invoke-direct {v0, p0}, Lcoa;-><init>(Lcnu;)V

    iput-object v0, p0, Lcnu;->bdp:Ljava/lang/Runnable;

    .line 82
    new-instance v0, Lcnz;

    invoke-direct {v0, p0}, Lcnz;-><init>(Lcnu;)V

    iput-object v0, p0, Lcnu;->bdq:Ljava/lang/Runnable;

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcnu;->bdr:Ljava/util/Set;

    .line 88
    iput-object p1, p0, Lcnu;->mContext:Landroid/content/Context;

    .line 89
    iput-object p2, p0, Lcnu;->atG:Leqo;

    .line 90
    iput-object p3, p0, Lcnu;->aTW:Ljava/util/concurrent/Executor;

    .line 91
    iget-object v0, p0, Lcnu;->aTW:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcnu;->bdq:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 94
    iget-object v0, p0, Lcnu;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcnu;->bdh:Landroid/location/LocationManager;

    .line 95
    new-instance v0, Lcnv;

    invoke-direct {v0, p0}, Lcnv;-><init>(Lcnu;)V

    iput-object v0, p0, Lcnu;->bdg:Landroid/content/BroadcastReceiver;

    .line 101
    iget-object v0, p0, Lcnu;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcnu;->bdg:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.location.PROVIDERS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 105
    new-instance v0, Lcnw;

    iget-object v1, p0, Lcnu;->atG:Leqo;

    invoke-interface {v1}, Leqo;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcnw;-><init>(Lcnu;Landroid/os/Handler;)V

    iput-object v0, p0, Lcnu;->bdi:Landroid/database/ContentObserver;

    .line 117
    iget-object v0, p0, Lcnu;->aTW:Ljava/util/concurrent/Executor;

    new-instance v1, Lcnx;

    const-string v2, "register"

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/4 v5, 0x2

    aput v5, v3, v4

    invoke-direct {v1, p0, v2, v3, p1}, Lcnx;-><init>(Lcnu;Ljava/lang/String;[ILandroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 124
    return-void
.end method

.method private QT()Z
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, Lcnu;->bdn:Ljava/lang/Object;

    .line 252
    :goto_0
    iget-boolean v0, p0, Lcnu;->bdk:Z

    if-nez v0, :cond_0

    .line 254
    :try_start_0
    iget-object v0, p0, Lcnu;->bdn:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 255
    :catch_0
    move-exception v0

    .line 256
    const-string v1, "QSB.LocationOptIn"

    const-string v2, "Unexpected InterrupedException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 259
    :cond_0
    iget-boolean v0, p0, Lcnu;->bdj:Z

    return v0
.end method

.method private QU()I
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcnu;->bdn:Ljava/lang/Object;

    .line 306
    :goto_0
    iget-boolean v0, p0, Lcnu;->bdm:Z

    if-nez v0, :cond_0

    .line 308
    :try_start_0
    iget-object v0, p0, Lcnu;->bdn:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    const-string v1, "QSB.LocationOptIn"

    const-string v2, "Unexpected InterrupedException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 313
    :cond_0
    iget v0, p0, Lcnu;->bdl:I

    return v0
.end method

.method private static ee(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 365
    if-ne p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final QL()Z
    .locals 2

    .prologue
    .line 128
    iget-object v1, p0, Lcnu;->bdn:Ljava/lang/Object;

    monitor-enter v1

    .line 129
    :try_start_0
    invoke-direct {p0}, Lcnu;->QT()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcnu;->QU()I

    move-result v0

    invoke-static {v0}, Lcnu;->ee(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final QM()Z
    .locals 2

    .prologue
    .line 135
    iget-object v1, p0, Lcnu;->bdn:Ljava/lang/Object;

    monitor-enter v1

    .line 136
    :try_start_0
    invoke-direct {p0}, Lcnu;->QT()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final QN()Z
    .locals 2

    .prologue
    .line 142
    iget-object v1, p0, Lcnu;->bdn:Ljava/lang/Object;

    monitor-enter v1

    .line 143
    :try_start_0
    invoke-direct {p0}, Lcnu;->QU()I

    move-result v0

    invoke-static {v0}, Lcnu;->ee(I)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final QO()Z
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lcnu;->QP()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcnu;->QL()Z

    move-result v0

    goto :goto_0
.end method

.method public final QP()Z
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcnu;->bdo:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcnu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_APPS_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcnu;->bdo:Ljava/lang/Boolean;

    .line 167
    :cond_0
    iget-object v0, p0, Lcnu;->bdo:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 165
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final QQ()Z
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcnu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.location.settings.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final QR()I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 183
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v1, v2, :cond_0

    .line 191
    :goto_0
    return v0

    .line 187
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcnu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_mode"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v1

    .line 190
    const-string v2, "QSB.LocationOptIn"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final QS()V
    .locals 4

    .prologue
    .line 197
    invoke-virtual {p0}, Lcnu;->QP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v1, p0, Lcnu;->bdn:Ljava/lang/Object;

    monitor-enter v1

    .line 201
    :try_start_0
    invoke-direct {p0}, Lcnu;->QT()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcnu;->QU()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 202
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 201
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 204
    :cond_3
    monitor-exit v1

    .line 207
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gsf.action.SET_USE_LOCATION_FOR_SERVICES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 210
    :try_start_1
    iget-object v1, p0, Lcnu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    const-string v1, "QSB.LocationOptIn"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t start location opt-in: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcoc;)V
    .locals 2

    .prologue
    .line 237
    iget-object v1, p0, Lcnu;->bdn:Ljava/lang/Object;

    monitor-enter v1

    .line 238
    :try_start_0
    iget-object v0, p0, Lcnu;->bdr:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 239
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcoc;)V
    .locals 2

    .prologue
    .line 244
    iget-object v1, p0, Lcnu;->bdn:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_0
    iget-object v0, p0, Lcnu;->bdr:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 246
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final b(ZI)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 340
    iget-object v0, p0, Lcnu;->bdn:Ljava/lang/Object;

    .line 342
    iget-boolean v0, p0, Lcnu;->bdj:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcnu;->bdl:I

    invoke-static {v0}, Lcnu;->ee(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 343
    :goto_0
    iput-boolean p1, p0, Lcnu;->bdj:Z

    .line 344
    iput p2, p0, Lcnu;->bdl:I

    .line 345
    iget-boolean v3, p0, Lcnu;->bdj:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcnu;->bdl:I

    invoke-static {v3}, Lcnu;->ee(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 346
    :goto_1
    if-eq v0, v1, :cond_0

    .line 348
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcnu;->bdr:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lcnu;->atG:Leqo;

    new-instance v3, Lcny;

    const-string v4, "Notify observers"

    invoke-direct {v3, p0, v4, v0, v1}, Lcny;-><init>(Lcnu;Ljava/lang/String;Ljava/util/Set;Z)V

    invoke-interface {v2, v3}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 350
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 342
    goto :goto_0

    :cond_2
    move v1, v2

    .line 345
    goto :goto_1
.end method

.method public final hv(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gsf.GOOGLE_APPS_LOCATION_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 220
    if-eqz p1, :cond_0

    .line 221
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    :cond_0
    return-object v0
.end method
