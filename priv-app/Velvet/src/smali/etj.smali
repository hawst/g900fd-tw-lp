.class public Letj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final ciA:Lifg;

.field private static final ciB:Lifw;

.field private static final ciC:Ljava/text/SimpleDateFormat;


# instance fields
.field private final ciD:Ljava/util/List;

.field private final ciE:I

.field private final ciF:Ljava/util/Map;

.field private final ciG:Ljava/lang/ref/WeakReference;

.field private final ciH:Letj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final ciI:Ljava/util/Map;

.field private final ciJ:Lipn;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    new-instance v0, Letk;

    invoke-direct {v0}, Letk;-><init>()V

    sput-object v0, Letj;->ciA:Lifg;

    .line 74
    new-instance v0, Letl;

    invoke-direct {v0}, Letl;-><init>()V

    sput-object v0, Letj;->ciB:Lifw;

    .line 81
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MMM-dd HH:mm:ss.SSZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Letj;->ciC:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 106
    const/4 v2, 0x0

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v4

    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {}, Lior;->aYd()Ljava/util/IdentityHashMap;

    move-result-object v7

    invoke-static {}, Liiz;->aWT()Liiz;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Letj;-><init>(Landroid/content/res/Resources;ILjava/util/List;Ljava/util/Map;Letj;Ljava/lang/ref/WeakReference;Ljava/util/Map;Lipn;)V

    .line 110
    return-void
.end method

.method private constructor <init>(Landroid/content/res/Resources;ILjava/util/List;Ljava/util/Map;Letj;Ljava/lang/ref/WeakReference;Ljava/util/Map;Lipn;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Letj;->mResources:Landroid/content/res/Resources;

    .line 118
    iput p2, p0, Letj;->ciE:I

    .line 119
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Letj;->ciD:Ljava/util/List;

    .line 120
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Letj;->ciF:Ljava/util/Map;

    .line 121
    iput-object p5, p0, Letj;->ciH:Letj;

    .line 122
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Letj;->ciG:Ljava/lang/ref/WeakReference;

    .line 123
    invoke-static {p7}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Letj;->ciI:Ljava/util/Map;

    .line 124
    invoke-static {p8}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipn;

    iput-object v0, p0, Letj;->ciJ:Lipn;

    .line 125
    return-void
.end method

.method private a(Leti;)Letj;
    .locals 9

    .prologue
    .line 136
    new-instance v0, Letj;

    iget-object v1, p0, Letj;->mResources:Landroid/content/res/Resources;

    iget v2, p0, Letj;->ciE:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Letj;->ciD:Ljava/util/List;

    iget-object v4, p0, Letj;->ciF:Ljava/util/Map;

    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget-object v7, p0, Letj;->ciI:Ljava/util/Map;

    iget-object v8, p0, Letj;->ciJ:Lipn;

    move-object v5, p0

    invoke-direct/range {v0 .. v8}, Letj;-><init>(Landroid/content/res/Resources;ILjava/util/List;Ljava/util/Map;Letj;Ljava/lang/ref/WeakReference;Ljava/util/Map;Lipn;)V

    return-object v0
.end method

.method private a(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 336
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Letj;->ciD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 338
    const-string v0, ""

    .line 339
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v0

    move v3, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Letm;

    .line 340
    iget v1, v0, Letm;->ciE:I

    const-string v5, "  "

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v5, v1}, Ligh;->P(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 341
    invoke-interface {p2, v0}, Lifg;->as(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 342
    iget-boolean v8, v0, Letm;->ciM:Z

    if-eqz v8, :cond_1

    iget v8, v0, Letm;->ciE:I

    if-ne v8, v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    const/16 v9, 0x61

    if-gt v8, v9, :cond_1

    .line 347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v5

    :goto_1
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    :cond_0
    const-string v0, " | "

    goto :goto_1

    .line 350
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 351
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    :cond_2
    iget v2, v0, Letm;->ciE:I

    .line 354
    iget-boolean v0, v0, Letm;->ciM:Z

    if-eqz v0, :cond_3

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v3, v2

    move-object v2, v0

    goto :goto_0

    .line 359
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    const-string v0, ""

    move v3, v2

    move-object v2, v0

    .line 363
    goto/16 :goto_0

    .line 364
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 365
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    :cond_5
    return-object v6
.end method

.method static synthetic a(Letj;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Letj;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 7

    .prologue
    .line 381
    iget-object v6, p0, Letj;->ciD:Ljava/util/List;

    new-instance v0, Letm;

    iget v1, p0, Letj;->ciE:I

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Letm;-><init>(ILjava/lang/String;Ljava/lang/String;ZZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    return-void
.end method

.method private avM()Ljava/lang/Iterable;
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Letj;->ciD:Ljava/util/List;

    sget-object v1, Lifi;->dBe:Lifi;

    invoke-direct {p0, v0, v1}, Letj;->a(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic avQ()Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Letj;->ciC:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method private c(Leti;)Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Letj;->ciG:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Letj;->ciH:Letj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Letj;->ciH:Letj;

    invoke-direct {v0, p1}, Letj;->c(Leti;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Letj;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Letj;->ciF:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Letj;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Letj;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method private s(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 228
    if-nez p1, :cond_0

    const-string p1, "null"

    .line 229
    :cond_0
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Letj;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 230
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 270
    invoke-direct {p0}, Letj;->avM()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 271
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 273
    :cond_0
    return-void
.end method

.method public final avJ()Letj;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Letj;->a(Leti;)Letj;

    move-result-object v0

    return-object v0
.end method

.method public final avK()Ljava/util/Map;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Letj;->ciF:Ljava/util/Map;

    invoke-static {v0}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    return-object v0
.end method

.method public final avL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    const/16 v0, 0xa

    invoke-static {v0}, Lifj;->e(C)Lifj;

    move-result-object v0

    invoke-direct {p0}, Letj;->avM()Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final avN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 303
    const/16 v0, 0xa

    invoke-static {v0}, Lifj;->e(C)Lifj;

    move-result-object v0

    invoke-virtual {p0}, Letj;->avO()Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final avO()Ljava/lang/Iterable;
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Letj;->ciD:Ljava/util/List;

    sget-object v1, Letj;->ciA:Lifg;

    invoke-direct {p0, v0, v1}, Letj;->a(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final avP()Ljava/lang/Iterable;
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Letj;->ciD:Ljava/util/List;

    sget-object v1, Letj;->ciB:Lifw;

    invoke-static {v0, v1}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    sget-object v1, Letj;->ciA:Lifg;

    invoke-direct {p0, v0, v1}, Letj;->a(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public final b(Leti;)V
    .locals 7
    .param p1    # Leti;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    if-nez p1, :cond_0

    .line 183
    :goto_0
    return-void

    .line 157
    :cond_0
    invoke-direct {p0, p1}, Letj;->c(Leti;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    const-string v0, "[cycle detected]"

    invoke-direct {p0, v0, v4}, Letj;->s(Ljava/lang/String;Z)V

    goto :goto_0

    .line 161
    :cond_1
    instance-of v0, p1, Letz;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 162
    check-cast v0, Letz;

    .line 163
    iget-object v1, p0, Letj;->ciI:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 164
    invoke-interface {v0}, Letz;->getLabel()Ljava/lang/String;

    move-result-object v2

    .line 165
    if-eqz v1, :cond_2

    .line 166
    const-string v0, "[%s #%s - see above for complete dump]"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v4

    aput-object v1, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Letj;->s(Ljava/lang/String;Z)V

    goto :goto_0

    .line 170
    :cond_2
    iget-object v1, p0, Letj;->ciJ:Lipn;

    invoke-interface {v1, v2}, Lipn;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v1, p0, Letj;->ciJ:Lipn;

    invoke-interface {v1, v2}, Lipn;->bj(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 172
    iget-object v3, p0, Letj;->ciI:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string v0, "[%s #%s]"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v4

    aput-object v1, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Letj;->s(Ljava/lang/String;Z)V

    .line 175
    :cond_3
    invoke-direct {p0, p1}, Letj;->a(Leti;)Letj;

    move-result-object v0

    .line 177
    :try_start_0
    invoke-interface {p1, v0}, Leti;->a(Letj;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v1

    .line 181
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Letj;->s(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final c(Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 260
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Letj;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public final d(Ljava/lang/Iterable;)V
    .locals 3

    .prologue
    .line 203
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Letj;->s(Ljava/lang/String;Z)V

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/Iterable;)V
    .locals 3

    .prologue
    .line 213
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 214
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Letj;->s(Ljava/lang/String;Z)V

    goto :goto_0

    .line 216
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/Iterable;)V
    .locals 2

    .prologue
    .line 222
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leti;

    .line 223
    invoke-virtual {p0, v0}, Letj;->b(Leti;)V

    goto :goto_0

    .line 225
    :cond_0
    return-void
.end method

.method public final in(I)Letn;
    .locals 3

    .prologue
    .line 253
    new-instance v0, Letn;

    iget-object v1, p0, Letj;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Letn;-><init>(Letj;Ljava/lang/String;B)V

    return-object v0
.end method

.method public final lt(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 146
    iget-object v6, p0, Letj;->ciD:Ljava/util/List;

    new-instance v0, Letm;

    iget v1, p0, Letj;->ciE:I

    add-int/lit8 v1, v1, -0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    move v5, v4

    invoke-direct/range {v0 .. v5}, Letm;-><init>(ILjava/lang/String;Ljava/lang/String;ZZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    return-void
.end method

.method public final lu(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 189
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Letj;->s(Ljava/lang/String;Z)V

    .line 190
    return-void
.end method

.method public final lv(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Letj;->s(Ljava/lang/String;Z)V

    .line 197
    return-void
.end method

.method public final lw(Ljava/lang/String;)Letn;
    .locals 2

    .prologue
    .line 245
    new-instance v0, Letn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Letn;-><init>(Letj;Ljava/lang/String;B)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    invoke-virtual {p0}, Letj;->avN()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
