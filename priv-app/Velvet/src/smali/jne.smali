.class public final Ljne;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private euK:I

.field private euL:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 70
    iput v0, p0, Ljne;->aez:I

    iput v0, p0, Ljne;->euK:I

    iput-boolean v0, p0, Ljne;->euL:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljne;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljne;->eCz:I

    .line 71
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljne;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljne;->euK:I

    iget v0, p0, Ljne;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljne;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljne;->euL:Z

    iget v0, p0, Ljne;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljne;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 85
    iget v0, p0, Ljne;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x1

    iget v1, p0, Ljne;->euK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 88
    :cond_0
    iget v0, p0, Ljne;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 89
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljne;->euL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 91
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 92
    return-void
.end method

.method public final bqA()Z
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Ljne;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bqz()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ljne;->euK:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 96
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 97
    iget v1, p0, Ljne;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 98
    const/4 v1, 0x1

    iget v2, p0, Ljne;->euK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_0
    iget v1, p0, Ljne;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 102
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljne;->euL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 105
    :cond_1
    return v0
.end method

.method public final qP(I)Ljne;
    .locals 1

    .prologue
    .line 37
    iput p1, p0, Ljne;->euK:I

    .line 38
    iget v0, p0, Ljne;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljne;->aez:I

    .line 39
    return-object p0
.end method
