.class public final Ldki;
.super Ljava/io/InputStream;
.source "PG"

# interfaces
.implements Ldkc;


# instance fields
.field private final bAH:Ljava/util/LinkedList;

.field private final bAI:Ljava/util/Deque;

.field private bAJ:I

.field private bAK:I

.field private bAL:Ljava/io/InputStream;

.field private final bAM:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final bAN:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final bAO:Ljava/util/concurrent/LinkedBlockingDeque;

.field private final bbV:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final bbd:Lenw;

.field private final mListener:Ldkj;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldkj;)V
    .locals 2
    .param p1    # Ldkj;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 226
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 126
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    .line 127
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldki;->bAI:Ljava/util/Deque;

    .line 128
    iput v1, p0, Ldki;->bAJ:I

    .line 129
    iput v1, p0, Ldki;->bAK:I

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Ldki;->bAL:Ljava/io/InputStream;

    .line 131
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Ldki;->bAM:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 132
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Ldki;->bAN:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 138
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Ldki;->bbd:Lenw;

    .line 143
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 150
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Ldki;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 227
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkj;

    iput-object v0, p0, Ldki;->mListener:Ldkj;

    .line 228
    return-void
.end method

.method private static a(Ljava/util/Queue;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 646
    invoke-interface {p0}, Ljava/util/Queue;->size()I

    move-result v1

    .line 647
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 649
    :try_start_0
    invoke-interface {p0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkb;

    .line 650
    invoke-virtual {v0}, Ldkb;->toShortString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 655
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [ConcurrentModificationException]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 658
    :goto_1
    return-object v0

    .line 657
    :cond_0
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Ldkb;)V
    .locals 6
    .param p1    # Ldkb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 496
    .line 497
    if-eqz p1, :cond_0

    move v0, v3

    .line 498
    :goto_0
    if-eqz p1, :cond_1

    .line 499
    iget v1, p0, Ldki;->bAJ:I

    invoke-virtual {p1}, Ldkb;->acz()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, p0, Ldki;->bAJ:I

    .line 500
    iget-object v1, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 501
    instance-of v1, p1, Ldkl;

    if-eqz v1, :cond_4

    move v1, v2

    .line 504
    :goto_1
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkb;

    move-object p1, v0

    move v0, v1

    goto :goto_0

    .line 497
    :cond_0
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkb;

    move-object p1, v0

    move v0, v3

    goto :goto_0

    .line 507
    :cond_1
    if-eqz v0, :cond_2

    .line 508
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    sget-object v1, Ldkl;->bAQ:Ldkl;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->addFirst(Ljava/lang/Object;)V

    .line 513
    :cond_2
    iget-object v0, p0, Ldki;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 542
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Unknown state"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 518
    :pswitch_0
    invoke-direct {p0}, Ldki;->acH()V

    .line 519
    iget-object v0, p0, Ldki;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    .line 521
    iget-object v0, p0, Ldki;->bAI:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 522
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    iget-object v1, p0, Ldki;->bAI:Ljava/util/Deque;

    invoke-virtual {v0, v3, v1}, Ljava/util/LinkedList;->addAll(ILjava/util/Collection;)Z

    .line 523
    iget-object v0, p0, Ldki;->bAI:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 524
    iget v0, p0, Ldki;->bAJ:I

    iget v1, p0, Ldki;->bAK:I

    add-int/2addr v0, v1

    iput v0, p0, Ldki;->bAJ:I

    .line 525
    iput v3, p0, Ldki;->bAK:I

    .line 527
    :cond_3
    iput-object v5, p0, Ldki;->bAL:Ljava/io/InputStream;

    .line 528
    iget-object v0, p0, Ldki;->bAN:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 540
    :goto_2
    :pswitch_1
    return-void

    .line 531
    :pswitch_2
    invoke-direct {p0}, Ldki;->acH()V

    .line 533
    iget-object v0, p0, Ldki;->bAI:Ljava/util/Deque;

    iget-object v1, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-interface {v0, v1}, Ljava/util/Deque;->addAll(Ljava/util/Collection;)Z

    .line 534
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 535
    iget v0, p0, Ldki;->bAK:I

    iget v1, p0, Ldki;->bAJ:I

    add-int/2addr v0, v1

    iput v0, p0, Ldki;->bAK:I

    .line 536
    iput v3, p0, Ldki;->bAJ:I

    .line 537
    iput-object v5, p0, Ldki;->bAL:Ljava/io/InputStream;

    .line 539
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    sget-object v1, Ldkl;->bAQ:Ldkl;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    move v1, v0

    goto :goto_1

    .line 513
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private acH()V
    .locals 3

    .prologue
    .line 547
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 548
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 549
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkb;

    .line 550
    instance-of v2, v0, Ldkl;

    if-nez v2, :cond_1

    instance-of v0, v0, Ldkm;

    if-eqz v0, :cond_0

    .line 551
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 554
    :cond_2
    return-void
.end method

.method private acI()Ljava/io/InputStream;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 590
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Ldki;->a(Ldkb;)V

    .line 594
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->takeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkb;

    .line 596
    invoke-direct {p0, v0}, Ldki;->a(Ldkb;)V

    .line 599
    :cond_1
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkb;

    .line 600
    iget-object v2, p0, Ldki;->bAN:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 602
    instance-of v2, v0, Ldkl;

    if-eqz v2, :cond_2

    .line 604
    iget-object v2, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    move-object v0, v1

    .line 631
    :goto_1
    return-object v0

    .line 606
    :cond_2
    instance-of v2, v0, Ldkk;

    if-eqz v2, :cond_3

    .line 608
    iget-object v2, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    iget-object v2, p0, Ldki;->bAH:Ljava/util/LinkedList;

    sget-object v3, Ldkl;->bAQ:Ldkl;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Ldki;->bAI:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->clear()V

    const/4 v2, 0x0

    iput v2, p0, Ldki;->bAJ:I

    const/4 v2, 0x0

    iput v2, p0, Ldki;->bAK:I

    const/4 v2, 0x0

    iput-object v2, p0, Ldki;->bAL:Ljava/io/InputStream;

    .line 609
    check-cast v0, Ldkk;

    iget-object v0, v0, Ldkk;->bAP:Ljava/io/IOException;

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move-object v0, v1

    .line 631
    goto :goto_1

    .line 610
    :cond_3
    :try_start_1
    instance-of v2, v0, Ldkd;

    if-eqz v2, :cond_4

    .line 611
    iget-object v2, p0, Ldki;->bAI:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 615
    iget v2, p0, Ldki;->bAK:I

    invoke-virtual {v0}, Ldkb;->acz()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Ldki;->bAK:I

    .line 616
    iget v2, p0, Ldki;->bAJ:I

    invoke-virtual {v0}, Ldkb;->acz()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Ldki;->bAJ:I

    .line 617
    check-cast v0, Ldkd;

    .line 618
    invoke-virtual {v0}, Ldkd;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_1

    .line 619
    :cond_4
    instance-of v0, v0, Ldkm;

    if-nez v0, :cond_0

    .line 622
    const-string v0, "EagerBufferedIS"

    const-string v2, "Unknown chunk in stream"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private f([BII)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 415
    iget-object v1, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    .line 417
    invoke-direct {p0, v6}, Ldki;->a(Ldkb;)V

    .line 420
    if-gtz p3, :cond_1

    .line 459
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 427
    :goto_1
    if-ge v1, p3, :cond_5

    .line 429
    iget-object v2, p0, Ldki;->bAL:Ljava/io/InputStream;

    if-nez v2, :cond_2

    .line 430
    invoke-direct {p0}, Ldki;->acI()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Ldki;->bAL:Ljava/io/InputStream;

    .line 431
    iget-object v2, p0, Ldki;->bAL:Ljava/io/InputStream;

    if-eqz v2, :cond_5

    .line 433
    :cond_2
    if-nez p1, :cond_3

    .line 440
    iget-object v2, p0, Ldki;->bAL:Ljava/io/InputStream;

    sub-int v3, p3, v1

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 445
    :goto_2
    if-gtz v2, :cond_4

    .line 447
    iput-object v6, p0, Ldki;->bAL:Ljava/io/InputStream;

    goto :goto_1

    .line 443
    :cond_3
    iget-object v2, p0, Ldki;->bAL:Ljava/io/InputStream;

    add-int v3, p2, v1

    sub-int v4, p3, v1

    invoke-virtual {v2, p1, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    goto :goto_2

    .line 449
    :cond_4
    add-int/2addr v1, v2

    .line 451
    goto :goto_1

    .line 454
    :cond_5
    if-nez v1, :cond_6

    .line 455
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 459
    goto :goto_0
.end method

.method private setState(I)V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->reset()V

    .line 284
    iget-object v0, p0, Ldki;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 289
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    sget-object v1, Ldkm;->bAR:Ldkm;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 290
    return-void
.end method


# virtual methods
.method public final PO()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    sget-object v1, Ldkl;->bAQ:Ldkl;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 247
    iget-object v0, p0, Ldki;->mListener:Ldkj;

    invoke-interface {v0}, Ldkj;->PO()V

    .line 248
    return-void
.end method

.method public final available()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Ldki;->bAJ:I

    return v0
.end method

.method public final c(Ldkd;)V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Ldki;->bAM:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 241
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 242
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ldki;->setState(I)V

    .line 279
    return-void
.end method

.method public final d(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 252
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/io/IOException;

    .line 253
    :goto_0
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    new-instance v1, Ldkk;

    invoke-direct {v1, p1}, Ldkk;-><init>(Ljava/io/IOException;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 254
    iget-object v0, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    sget-object v1, Ldkl;->bAQ:Ldkl;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 255
    iget-object v0, p0, Ldki;->mListener:Ldkj;

    invoke-interface {v0}, Ldkj;->PP()V

    .line 256
    return-void

    .line 252
    :cond_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public final mark(I)V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 318
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "mark() not supported by EagerBufferedInputStream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 328
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 339
    iget-object v0, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 342
    invoke-direct {p0, v1}, Ldki;->a(Ldkb;)V

    .line 345
    :goto_0
    iget-object v0, p0, Ldki;->bAL:Ljava/io/InputStream;

    if-nez v0, :cond_1

    .line 346
    invoke-direct {p0}, Ldki;->acI()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Ldki;->bAL:Ljava/io/InputStream;

    .line 347
    iget-object v0, p0, Ldki;->bAL:Ljava/io/InputStream;

    if-nez v0, :cond_1

    .line 349
    const/4 v0, -0x1

    .line 356
    :cond_0
    return v0

    .line 353
    :cond_1
    iget-object v0, p0, Ldki;->bAL:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 354
    if-gez v0, :cond_0

    .line 359
    iput-object v1, p0, Ldki;->bAL:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 372
    if-nez p1, :cond_0

    .line 373
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Read into null buffer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Ldki;->f([BII)I

    move-result v0

    return v0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldki;->setState(I)V

    .line 269
    return-void
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 384
    iget-object v2, p0, Ldki;->bbd:Lenw;

    invoke-virtual {v2}, Lenw;->auS()Lenw;

    .line 387
    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    .line 394
    :goto_0
    return-wide v0

    .line 390
    :cond_0
    long-to-int v0, p1

    .line 391
    if-gez v0, :cond_1

    .line 392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteCount too large: int overflow"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 394
    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Ldki;->f([BII)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 673
    iget-object v0, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ldkl;

    .line 675
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 676
    const-string v2, "EagerBufferedInputStream{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 677
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "source "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const-string v0, "complete"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "state: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Ldki;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "unknown"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "new chunks "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldki;->bAO:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-static {v2}, Ldki;->a(Ljava/util/Queue;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unread chunks "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldki;->bAH:Ljava/util/LinkedList;

    invoke-static {v2}, Ldki;->a(Ljava/util/Queue;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "read chunks "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldki;->bAI:Ljava/util/Deque;

    invoke-static {v2}, Ldki;->a(Ljava/util/Queue;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    const-string v0, "(D=data,E=exception,X=sentinel,-=signal), "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Ldki;->bAJ:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " unread bytes, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Ldki;->bAK:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " read bytes"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 685
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 677
    :cond_0
    const-string v0, "incomplete"

    goto/16 :goto_0

    .line 678
    :pswitch_0
    const-string v0, "normal"

    goto/16 :goto_1

    :pswitch_1
    const-string v0, "reset"

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "closed"

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
