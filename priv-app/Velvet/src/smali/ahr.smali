.class public final Lahr;
.super Lahn;
.source "PG"


# instance fields
.field private Yw:Landroid/content/pm/LauncherApps;

.field private Yx:Ljava/util/Map;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lahn;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lahr;->Yx:Ljava/util/Map;

    .line 44
    const-string v0, "launcherapps"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/LauncherApps;

    iput-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Laho;)V
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lahs;

    invoke-direct {v0, p1}, Lahs;-><init>(Laho;)V

    .line 82
    iget-object v1, p0, Lahr;->Yx:Ljava/util/Map;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-object v2, p0, Lahr;->Yx:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    iget-object v1, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {v1, v0}, Landroid/content/pm/LauncherApps;->registerCallback(Landroid/content/pm/LauncherApps$Callback;)V

    .line 86
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/content/ComponentName;Lahz;Landroid/graphics/Rect;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p3, p4}, Landroid/content/pm/LauncherApps;->startMainActivity(Landroid/content/ComponentName;Landroid/os/UserHandle;Landroid/graphics/Rect;Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method public final c(Landroid/content/Intent;Lahz;)Lahk;
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/LauncherApps;->resolveActivity(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/pm/LauncherActivityInfo;

    move-result-object v1

    .line 64
    if-eqz v1, :cond_0

    .line 65
    new-instance v0, Lahm;

    invoke-direct {v0, v1}, Lahm;-><init>(Landroid/content/pm/LauncherActivityInfo;)V

    .line 67
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/content/ComponentName;Lahz;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    iget-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/pm/LauncherApps;->startAppDetailsActivity(Landroid/content/ComponentName;Landroid/os/UserHandle;Landroid/graphics/Rect;Landroid/os/Bundle;)V

    .line 78
    return-void
.end method

.method public final e(Landroid/content/ComponentName;Lahz;)Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/LauncherApps;->isActivityEnabled(Landroid/content/ComponentName;Landroid/os/UserHandle;)Z

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/String;Lahz;)Ljava/util/List;
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/LauncherApps;->getActivityList(Ljava/lang/String;Landroid/os/UserHandle;)Ljava/util/List;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 52
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 59
    :goto_0
    return-object v0

    .line 54
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 56
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/LauncherActivityInfo;

    .line 57
    new-instance v3, Lahm;

    invoke-direct {v3, v0}, Lahm;-><init>(Landroid/content/pm/LauncherActivityInfo;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 59
    goto :goto_0
.end method

.method public final j(Ljava/lang/String;Lahz;)Z
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lahr;->Yw:Landroid/content/pm/LauncherApps;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/LauncherApps;->isPackageEnabled(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v0

    return v0
.end method
