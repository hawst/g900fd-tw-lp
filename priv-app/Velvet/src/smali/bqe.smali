.class public final Lbqe;
.super Lbqg;


# instance fields
.field private final aEh:Lbhh;

.field private final aHi:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Lbhh;Ljava/lang/Class;)V
    .locals 0

    invoke-direct {p0}, Lbqg;-><init>()V

    iput-object p1, p0, Lbqe;->aEh:Lbhh;

    iput-object p2, p0, Lbqe;->aHi:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;)V
    .locals 2

    iget-object v0, p0, Lbqe;->aEh:Lbhh;

    iget-object v1, p0, Lbqe;->aHi:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbhh;->al(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;)V
    .locals 2

    iget-object v0, p0, Lbqe;->aEh:Lbhh;

    iget-object v1, p0, Lbqe;->aHi:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbhh;->al(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Response;)V
    .locals 2

    iget-object v0, p0, Lbqe;->aEh:Lbhh;

    iget-object v1, p0, Lbqe;->aHi:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbhh;->al(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/SetExperimentIdsCall$Response;)V
    .locals 2

    iget-object v0, p0, Lbqe;->aEh:Lbhh;

    iget-object v1, p0, Lbqe;->aHi:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbhh;->al(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/search/global/SetSignedInAccountCall$Response;)V
    .locals 2

    iget-object v0, p0, Lbqe;->aEh:Lbhh;

    iget-object v1, p0, Lbqe;->aHi:Ljava/lang/Class;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lbhh;->al(Ljava/lang/Object;)V

    return-void
.end method
