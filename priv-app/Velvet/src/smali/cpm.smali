.class public Lcpm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final beW:Ljava/lang/String;

.field private final beX:Ljava/util/List;

.field private beY:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 978
    iput-object p1, p0, Lcpm;->beW:Ljava/lang/String;

    .line 979
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcpm;->beX:Ljava/util/List;

    .line 980
    const/4 v0, 0x0

    iput v0, p0, Lcpm;->beY:I

    .line 981
    return-void
.end method


# virtual methods
.method public final Rl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 999
    iget v0, p0, Lcpm;->beY:I

    if-nez v0, :cond_0

    .line 1000
    const-string v0, ""

    .line 1006
    :goto_0
    return-object v0

    .line 1002
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcpm;->beX:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1003
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    iget v0, p0, Lcpm;->beY:I

    if-ge v1, v0, :cond_1

    .line 1004
    iget-object v0, p0, Lcpm;->beW:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v0, p0, Lcpm;->beX:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1003
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1006
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ax(Ljava/lang/Object;)Lcpm;
    .locals 2

    .prologue
    .line 984
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcpm;->hz(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 985
    iget-object v0, p0, Lcpm;->beX:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 986
    iget-object v0, p0, Lcpm;->beX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcpm;->beY:I

    .line 991
    :goto_0
    return-object p0

    .line 989
    :cond_0
    iget-object v0, p0, Lcpm;->beX:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected hz(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 995
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
