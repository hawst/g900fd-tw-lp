.class final Lint;
.super Limr;
.source "PG"


# static fields
.field private static final serialVersionUID:J = 0x3L


# direct methods
.method constructor <init>(Linz;Linz;Lifc;Lifc;JJIILiml;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 1

    .prologue
    .line 4042
    invoke-direct/range {p0 .. p12}, Limr;-><init>(Linz;Linz;Lifc;Lifc;JJIILiml;Ljava/util/concurrent/ConcurrentMap;)V

    .line 4044
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4052
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 4053
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v3

    new-instance v4, Limd;

    invoke-direct {v4}, Limd;-><init>()V

    iget v0, v4, Limd;->dEo:I

    if-ne v0, v10, :cond_8

    move v0, v1

    :goto_0
    const-string v5, "initial capacity was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v4, Limd;->dEo:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-ltz v3, :cond_9

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    iput v3, v4, Limd;->dEo:I

    iget-object v3, p0, Limr;->dEr:Linz;

    iget-object v0, v4, Limd;->dEr:Linz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    const-string v5, "Key strength was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v4, Limd;->dEr:Linz;

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linz;

    iput-object v0, v4, Limd;->dEr:Linz;

    sget-object v0, Linz;->dFI:Linz;

    if-eq v3, v0, :cond_0

    iput-boolean v1, v4, Limd;->dEn:Z

    :cond_0
    iget-object v3, p0, Limr;->dEs:Linz;

    iget-object v0, v4, Limd;->dEs:Linz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    const-string v5, "Value strength was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v4, Limd;->dEs:Linz;

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linz;

    iput-object v0, v4, Limd;->dEs:Linz;

    sget-object v0, Linz;->dFI:Linz;

    if-eq v3, v0, :cond_1

    iput-boolean v1, v4, Limd;->dEn:Z

    :cond_1
    iget-object v3, p0, Limr;->dEw:Lifc;

    iget-object v0, v4, Limd;->dEw:Lifc;

    if-nez v0, :cond_c

    move v0, v1

    :goto_4
    const-string v5, "key equivalence was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v4, Limd;->dEw:Lifc;

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifc;

    iput-object v0, v4, Limd;->dEw:Lifc;

    iput-boolean v1, v4, Limd;->dEn:Z

    iget-object v3, p0, Limr;->dEx:Lifc;

    iget-object v0, v4, Limd;->dEx:Lifc;

    if-nez v0, :cond_d

    move v0, v1

    :goto_5
    const-string v5, "value equivalence was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, v4, Limd;->dEx:Lifc;

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifc;

    iput-object v0, v4, Limd;->dEx:Lifc;

    iput-boolean v1, v4, Limd;->dEn:Z

    iget v3, p0, Limr;->dEp:I

    iget v0, v4, Limd;->dEp:I

    if-ne v0, v10, :cond_e

    move v0, v1

    :goto_6
    const-string v5, "concurrency level was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v4, Limd;->dEp:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-lez v3, :cond_f

    move v0, v1

    :goto_7
    invoke-static {v0}, Lifv;->gX(Z)V

    iput v3, v4, Limd;->dEp:I

    iget-object v3, p0, Limr;->dCT:Liml;

    iget-object v0, v4, Limd;->dCT:Liml;

    if-nez v0, :cond_10

    move v0, v1

    :goto_8
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liml;

    iput-object v0, v4, Liiw;->dCT:Liml;

    iput-boolean v1, v4, Limd;->dEn:Z

    iget-wide v6, p0, Limr;->dEt:J

    cmp-long v0, v6, v12

    if-lez v0, :cond_3

    iget-wide v6, p0, Limr;->dEt:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Limd;->e(JLjava/util/concurrent/TimeUnit;)V

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    iput-wide v8, v4, Limd;->dEt:J

    cmp-long v0, v6, v12

    if-nez v0, :cond_2

    iget-object v0, v4, Limd;->dEv:Limf;

    if-nez v0, :cond_2

    sget-object v0, Limf;->dEC:Limf;

    iput-object v0, v4, Limd;->dEv:Limf;

    :cond_2
    iput-boolean v1, v4, Limd;->dEn:Z

    :cond_3
    iget-wide v6, p0, Limr;->dEu:J

    cmp-long v0, v6, v12

    if-lez v0, :cond_5

    iget-wide v6, p0, Limr;->dEu:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7, v0}, Limd;->e(JLjava/util/concurrent/TimeUnit;)V

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    iput-wide v8, v4, Limd;->dEu:J

    cmp-long v0, v6, v12

    if-nez v0, :cond_4

    iget-object v0, v4, Limd;->dEv:Limf;

    if-nez v0, :cond_4

    sget-object v0, Limf;->dEC:Limf;

    iput-object v0, v4, Limd;->dEv:Limf;

    :cond_4
    iput-boolean v1, v4, Limd;->dEn:Z

    :cond_5
    iget v0, p0, Limr;->dEq:I

    if-eq v0, v10, :cond_7

    iget v3, p0, Limr;->dEq:I

    iget v0, v4, Limd;->dEq:I

    if-ne v0, v10, :cond_11

    move v0, v1

    :goto_9
    const-string v5, "maximum size was already set to %s"

    new-array v6, v1, [Ljava/lang/Object;

    iget v7, v4, Limd;->dEq:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v0, v5, v6}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    if-ltz v3, :cond_6

    move v2, v1

    :cond_6
    const-string v0, "maximum size must not be negative"

    invoke-static {v2, v0}, Lifv;->c(ZLjava/lang/Object;)V

    iput v3, v4, Limd;->dEq:I

    iput-boolean v1, v4, Limd;->dEn:Z

    iget v0, v4, Limd;->dEq:I

    if-nez v0, :cond_7

    sget-object v0, Limf;->dED:Limf;

    iput-object v0, v4, Limd;->dEv:Limf;

    .line 4054
    :cond_7
    invoke-virtual {v4}, Limd;->aXt()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lint;->dEO:Ljava/util/concurrent/ConcurrentMap;

    .line 4055
    :goto_a
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    :cond_8
    move v0, v2

    .line 4053
    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto/16 :goto_2

    :cond_b
    move v0, v2

    goto/16 :goto_3

    :cond_c
    move v0, v2

    goto/16 :goto_4

    :cond_d
    move v0, v2

    goto/16 :goto_5

    :cond_e
    move v0, v2

    goto/16 :goto_6

    :cond_f
    move v0, v2

    goto/16 :goto_7

    :cond_10
    move v0, v2

    goto/16 :goto_8

    :cond_11
    move v0, v2

    goto :goto_9

    .line 4056
    :cond_12
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 4059
    iget-object v0, p0, Lint;->dEO:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3

    .prologue
    .line 4047
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 4048
    iget-object v0, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 4049
    return-void
.end method
