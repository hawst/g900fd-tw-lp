.class public abstract Lgof;
.super Lq;
.source "PG"


# instance fields
.field private cRg:Ljava/lang/String;

.field private cRh:Lgoh;

.field private cRi:Lgoj;

.field private cRj:Lgoc;

.field private cRk:Landroid/content/res/Resources$Theme;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lq;-><init>()V

    return-void
.end method

.method private static N(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 52
    invoke-static {p0}, Lgof;->O(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 55
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.velour.INNER_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    goto :goto_0
.end method

.method private static O(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    if-nez p0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_0

    .line 66
    const-string v2, "dynact"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Landroid/content/Intent;Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "dynact"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 112
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 113
    if-eqz v1, :cond_0

    .line 114
    const-string v2, "data"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 116
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 117
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 118
    const-string v0, "com.google.android.velour.INNER_INTENT"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 119
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p0, p3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2, v0}, Lgof;->a(Ljava/lang/String;Landroid/content/Intent;Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/app/Activity;)Lgoh;
    .locals 1

    .prologue
    .line 40
    check-cast p0, Lgof;

    .line 41
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    return-object v0
.end method


# virtual methods
.method protected abstract aIr()Lgod;
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lgoc;

    invoke-direct {v0, p1}, Lgoc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgof;->cRj:Lgoc;

    .line 214
    iget-object v0, p0, Lgof;->cRj:Lgoc;

    invoke-super {p0, v0}, Lq;->attachBaseContext(Landroid/content/Context;)V

    .line 215
    return-void
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 317
    invoke-super {p0}, Lq;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lgof;->N(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgof;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    return-object v0
.end method

.method public getTheme()Landroid/content/res/Resources$Theme;
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lgof;->cRk:Landroid/content/res/Resources$Theme;

    if-nez v0, :cond_1

    .line 220
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    if-nez v0, :cond_0

    .line 221
    const-string v0, "Velour.DynamicActivityLoader"

    const-string v1, "getTheme call before hosted activity created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-super {p0}, Lq;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 232
    :goto_0
    return-object v0

    .line 224
    :cond_0
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->getStyle()I

    move-result v0

    .line 225
    if-nez v0, :cond_2

    .line 226
    invoke-super {p0}, Lq;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iput-object v0, p0, Lgof;->cRk:Landroid/content/res/Resources$Theme;

    .line 232
    :cond_1
    :goto_1
    iget-object v0, p0, Lgof;->cRk:Landroid/content/res/Resources$Theme;

    goto :goto_0

    .line 228
    :cond_2
    invoke-virtual {p0}, Lgof;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iput-object v0, p0, Lgof;->cRk:Landroid/content/res/Resources$Theme;

    .line 229
    iget-object v0, p0, Lgof;->cRk:Landroid/content/res/Resources$Theme;

    iget-object v1, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v1}, Lgoh;->getStyle()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->cN()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    invoke-super {p0}, Lq;->onBackPressed()V

    .line 345
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 138
    .line 139
    if-eqz p1, :cond_0

    .line 140
    const-string v0, "com.google.android.velour.ACTIVITY_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgof;->cRg:Ljava/lang/String;

    .line 141
    const-string v0, "com.google.android.velour.INNER_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 147
    :goto_0
    iget-object v0, p0, Lgof;->cRg:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No activity name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_0
    invoke-super {p0}, Lq;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lgof;->O(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lgof;->cRg:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 151
    :cond_2
    invoke-virtual {p0}, Lgof;->aIr()Lgod;

    move-result-object v0

    .line 153
    :try_start_0
    iget-object v2, p0, Lgof;->cRg:Ljava/lang/String;

    invoke-interface {v0, v2}, Lgod;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Lgot; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    invoke-interface {v0}, Lgod;->aIp()Lgoh;

    move-result-object v2

    iput-object v2, p0, Lgof;->cRh:Lgoh;

    .line 160
    invoke-interface {v0}, Lgod;->aIq()Lgoj;

    move-result-object v0

    iput-object v0, p0, Lgof;->cRi:Lgoj;

    .line 164
    iget-object v0, p0, Lgof;->cRj:Lgoc;

    iget-object v2, p0, Lgof;->cRi:Lgoj;

    invoke-virtual {v2}, Lgoj;->aIs()Lgoi;

    move-result-object v2

    iget-object v2, v2, Lgoi;->cRo:Ljava/lang/ClassLoader;

    iput-object v2, v0, Lgoc;->cRe:Ljava/lang/ClassLoader;

    .line 165
    if-eqz v1, :cond_3

    .line 166
    iget-object v0, p0, Lgof;->cRj:Lgoc;

    invoke-virtual {v0}, Lgoc;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 172
    :cond_3
    invoke-super {p0, p1}, Lq;->onCreate(Landroid/os/Bundle;)V

    .line 174
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0, v1}, Lgoh;->onCreate(Landroid/os/Bundle;)V

    .line 175
    return-void

    .line 154
    :catch_0
    move-exception v0

    .line 156
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to load jar file"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0, p1}, Lgoh;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->onDestroy()V

    .line 270
    iget-object v0, p0, Lgof;->cRi:Lgoj;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lgof;->cRi:Lgoj;

    invoke-virtual {v0}, Lgoj;->destroy()V

    .line 273
    :cond_0
    invoke-super {p0}, Lq;->onDestroy()V

    .line 274
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 288
    invoke-super {p0}, Lq;->onLowMemory()V

    .line 289
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    .line 290
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1}, Lq;->onNewIntent(Landroid/content/Intent;)V

    .line 295
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-static {p1}, Lgof;->N(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgoh;->onNewIntent(Landroid/content/Intent;)V

    .line 296
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0, p1}, Lgoh;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 257
    invoke-super {p0}, Lq;->onPause()V

    .line 258
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->onPause()V

    .line 259
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0, p1}, Lgoh;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRestart()V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Lq;->onRestart()V

    .line 246
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    .line 247
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 251
    invoke-super {p0}, Lq;->onResume()V

    .line 252
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->onResume()V

    .line 253
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 278
    invoke-super {p0, p1}, Lq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 279
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 280
    iget-object v1, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v1, v0}, Lgoh;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 281
    const-string v1, "com.google.android.velour.INNER_STATE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 282
    const-string v0, "com.google.android.velour.ACTIVITY_NAME"

    iget-object v1, p0, Lgof;->cRg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0}, Lq;->onStart()V

    .line 238
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->onStart()V

    .line 239
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 263
    invoke-super {p0}, Lq;->onStop()V

    .line 264
    iget-object v0, p0, Lgof;->cRh:Lgoh;

    invoke-virtual {v0}, Lgoh;->onStop()V

    .line 265
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 322
    invoke-virtual {p0}, Lgof;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v0, "com.google.android.velour.INNER_INTENT"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-super {p0, v1}, Lq;->setIntent(Landroid/content/Intent;)V

    .line 323
    return-void
.end method
