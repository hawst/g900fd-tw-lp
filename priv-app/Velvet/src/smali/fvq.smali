.class public final Lfvq;
.super Lfuz;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 36
    iput-object p4, p0, Lfvq;->mPhotoWithAttributionDecorator:Lgbd;

    .line 37
    return-void
.end method

.method private a(Landroid/content/Context;Ljba;)Lanh;
    .locals 10

    .prologue
    const v9, 0x7f0d016b

    .line 56
    new-instance v1, Lanw;

    invoke-direct {v1}, Lanw;-><init>()V

    .line 58
    invoke-virtual {p2}, Ljba;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p2}, Ljba;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanw;->aZ(Ljava/lang/String;)Lanw;

    .line 61
    :cond_0
    iget-object v0, p2, Ljba;->dXD:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 62
    const-string v0, " \u00b7 "

    iget-object v2, p2, Ljba;->dXD:[Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanw;->ba(Ljava/lang/String;)Lanw;

    .line 65
    :cond_1
    invoke-virtual {p2}, Ljba;->pX()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    invoke-virtual {p2}, Ljba;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanw;->bb(Ljava/lang/String;)Lanw;

    .line 68
    :cond_2
    iget-object v0, p2, Ljba;->aiS:Ljcn;

    if-eqz v0, :cond_3

    .line 69
    iget-object v0, p0, Lfvq;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v2, p2, Ljba;->aiS:Ljcn;

    const v3, 0x7f0d01f4

    const v4, 0x7f0d01f5

    invoke-virtual {v0, p1, v2, v3, v4}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanw;->bc(Ljava/lang/String;)Lanw;

    .line 74
    :cond_3
    invoke-virtual {p2}, Ljba;->pZ()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 75
    invoke-virtual {p2}, Ljba;->pY()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanw;->bd(Ljava/lang/String;)Lanw;

    .line 77
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 78
    iget-object v3, p2, Ljba;->dXF:[Lixt;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_9

    aget-object v5, v3, v0

    .line 79
    new-instance v6, Lanx;

    invoke-direct {v6}, Lanx;-><init>()V

    .line 80
    invoke-virtual {v5}, Lixt;->oK()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 81
    invoke-virtual {v5}, Lixt;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lanx;->be(Ljava/lang/String;)Lanx;

    .line 83
    :cond_5
    invoke-virtual {v5}, Lixt;->qc()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 84
    invoke-virtual {v5}, Lixt;->qb()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lanx;->bf(Ljava/lang/String;)Lanx;

    .line 86
    :cond_6
    iget-object v7, v5, Lixt;->aiS:Ljcn;

    if-eqz v7, :cond_7

    .line 87
    iget-object v7, p0, Lfvq;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v8, v5, Lixt;->aiS:Ljcn;

    invoke-virtual {v7, p1, v8, v9, v9}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lanx;->bg(Ljava/lang/String;)Lanx;

    .line 92
    :cond_7
    iget-object v7, v5, Lixt;->ahD:Lixx;

    if-eqz v7, :cond_8

    .line 93
    new-instance v7, Lfsa;

    const/16 v8, 0x88

    invoke-direct {v7, v8}, Lfsa;-><init>(I)V

    iget-object v5, v5, Lixt;->ahD:Lixx;

    invoke-virtual {v7, v5}, Lfsa;->b(Lixx;)Lani;

    move-result-object v5

    iput-object v5, v6, Lanx;->aiQ:Lani;

    .line 98
    :cond_8
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_9
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lanx;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanx;

    iput-object v0, v1, Lanw;->aiM:[Lanx;

    .line 103
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 104
    const/16 v2, 0x35

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 105
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 106
    iput-object v1, v0, Lanh;->ahp:Lanw;

    .line 108
    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTn:Ljba;

    .line 43
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 44
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 45
    invoke-direct {p0, p1, v0}, Lfvq;->a(Landroid/content/Context;Ljba;)Lanh;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v3, v0, Ljba;->ahD:Lixx;

    if-eqz v3, :cond_0

    .line 47
    new-instance v3, Laoj;

    invoke-direct {v3}, Laoj;-><init>()V

    const v4, 0x7f0a04c0

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lanh;->cm(I)Lanh;

    iput-object v3, v4, Lanh;->agz:Laoj;

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iput-object v3, v4, Lanh;->ahu:Lizj;

    new-instance v3, Lfsa;

    iget-object v5, v0, Ljba;->ahD:Lixx;

    invoke-virtual {v5}, Lixx;->oY()I

    move-result v5

    invoke-direct {v3, v5}, Lfsa;-><init>(I)V

    iget-object v0, v0, Ljba;->ahD:Lixx;

    invoke-virtual {v3, v0}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v4, Lanh;->ahs:Lani;

    iget-object v0, v4, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahC:Lanb;

    const v3, 0x7f0200ed

    invoke-virtual {v0, v3}, Lanb;->ch(I)Lanb;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 50
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 51
    return-object v1
.end method
