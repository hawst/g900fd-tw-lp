.class public final Liyv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private dRj:F

.field private dRk:Ljava/lang/String;

.field private dRl:Ljava/lang/String;

.field private dRm:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38846
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 38847
    const/4 v0, 0x0

    iput v0, p0, Liyv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liyv;->afh:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Liyv;->dRj:F

    const-string v0, ""

    iput-object v0, p0, Liyv;->dRk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liyv;->dRl:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Liyv;->dRm:I

    const/4 v0, 0x0

    iput-object v0, p0, Liyv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liyv;->eCz:I

    .line 38848
    return-void
.end method


# virtual methods
.method public final T(F)Liyv;
    .locals 1

    .prologue
    .line 38770
    const v0, 0x3f456042    # 0.771f

    iput v0, p0, Liyv;->dRj:F

    .line 38771
    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liyv;->aez:I

    .line 38772
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 38723
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liyv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyv;->afh:Ljava/lang/String;

    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liyv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Liyv;->dRj:F

    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liyv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyv;->dRk:Ljava/lang/String;

    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liyv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyv;->dRl:Ljava/lang/String;

    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Liyv;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liyv;->dRm:I

    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Liyv;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 38865
    iget v0, p0, Liyv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 38866
    const/4 v0, 0x1

    iget-object v1, p0, Liyv;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38868
    :cond_0
    iget v0, p0, Liyv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 38869
    const/4 v0, 0x2

    iget v1, p0, Liyv;->dRj:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 38871
    :cond_1
    iget v0, p0, Liyv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 38872
    const/4 v0, 0x3

    iget-object v1, p0, Liyv;->dRk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38874
    :cond_2
    iget v0, p0, Liyv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 38875
    const/4 v0, 0x4

    iget-object v1, p0, Liyv;->dRl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38877
    :cond_3
    iget v0, p0, Liyv;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 38878
    const/4 v0, 0x5

    iget v1, p0, Liyv;->dRm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 38880
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 38881
    return-void
.end method

.method public final bcu()F
    .locals 1

    .prologue
    .line 38767
    iget v0, p0, Liyv;->dRj:F

    return v0
.end method

.method public final bcv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38786
    iget-object v0, p0, Liyv;->dRk:Ljava/lang/String;

    return-object v0
.end method

.method public final bcw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38808
    iget-object v0, p0, Liyv;->dRl:Ljava/lang/String;

    return-object v0
.end method

.method public final bcx()I
    .locals 1

    .prologue
    .line 38830
    iget v0, p0, Liyv;->dRm:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38745
    iget-object v0, p0, Liyv;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 38885
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 38886
    iget v1, p0, Liyv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 38887
    const/4 v1, 0x1

    iget-object v2, p0, Liyv;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38890
    :cond_0
    iget v1, p0, Liyv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 38891
    const/4 v1, 0x2

    iget v2, p0, Liyv;->dRj:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 38894
    :cond_1
    iget v1, p0, Liyv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 38895
    const/4 v1, 0x3

    iget-object v2, p0, Liyv;->dRk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38898
    :cond_2
    iget v1, p0, Liyv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 38899
    const/4 v1, 0x4

    iget-object v2, p0, Liyv;->dRl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38902
    :cond_3
    iget v1, p0, Liyv;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 38903
    const/4 v1, 0x5

    iget v2, p0, Liyv;->dRm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 38906
    :cond_4
    return v0
.end method

.method public final nP(I)Liyv;
    .locals 1

    .prologue
    .line 38833
    const/4 v0, 0x1

    iput v0, p0, Liyv;->dRm:I

    .line 38834
    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Liyv;->aez:I

    .line 38835
    return-object p0
.end method

.method public final rx(Ljava/lang/String;)Liyv;
    .locals 1

    .prologue
    .line 38789
    if-nez p1, :cond_0

    .line 38790
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38792
    :cond_0
    iput-object p1, p0, Liyv;->dRk:Ljava/lang/String;

    .line 38793
    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liyv;->aez:I

    .line 38794
    return-object p0
.end method

.method public final ry(Ljava/lang/String;)Liyv;
    .locals 1

    .prologue
    .line 38811
    if-nez p1, :cond_0

    .line 38812
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38814
    :cond_0
    iput-object p1, p0, Liyv;->dRl:Ljava/lang/String;

    .line 38815
    iget v0, p0, Liyv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Liyv;->aez:I

    .line 38816
    return-object p0
.end method
