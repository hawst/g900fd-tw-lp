.class public final Lbpb;
.super Ljava/lang/Object;

# interfaces
.implements Lbgp;
.implements Lbgq;


# instance fields
.field private final aGR:Lbus;

.field aGS:Lbpd;

.field aGT:Z


# direct methods
.method public constructor <init>(Lbus;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbpb;->aGR:Lbus;

    const/4 v0, 0x0

    iput-object v0, p0, Lbpb;->aGS:Lbpd;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbpb;->aGT:Z

    return-void
.end method


# virtual methods
.method public final a(Lbgm;)V
    .locals 2

    iget-object v0, p0, Lbpb;->aGS:Lbpd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbpd;->bL(Z)V

    iget-boolean v0, p0, Lbpb;->aGT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbpb;->aGR:Lbus;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbgm;->yj()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbpb;->aGR:Lbus;

    invoke-virtual {p1}, Lbgm;->yl()Landroid/app/PendingIntent;

    invoke-interface {v0}, Lbus;->AV()V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbpb;->aGT:Z

    return-void

    :cond_1
    iget-object v0, p0, Lbpb;->aGR:Lbus;

    invoke-interface {v0}, Lbus;->AW()V

    goto :goto_0
.end method

.method public final onDisconnected()V
    .locals 2

    iget-object v0, p0, Lbpb;->aGS:Lbpd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbpd;->bL(Z)V

    return-void
.end method

.method public final wQ()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbpb;->aGS:Lbpd;

    invoke-virtual {v0, v1}, Lbpd;->bL(Z)V

    iget-boolean v0, p0, Lbpb;->aGT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbpb;->aGR:Lbus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbpb;->aGR:Lbus;

    invoke-interface {v0}, Lbus;->AU()V

    :cond_0
    iput-boolean v1, p0, Lbpb;->aGT:Z

    return-void
.end method
