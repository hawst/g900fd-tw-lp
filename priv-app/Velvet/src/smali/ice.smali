.class public final Lice;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bhl:Ljava/util/concurrent/Executor;

.field final dK:Ljava/lang/Object;

.field private final dxM:Ljava/util/concurrent/Executor;

.field private final dxN:Lidc;

.field final dxO:Ljava/util/HashMap;

.field public dxP:I

.field dxQ:Landroid/speech/tts/TextToSpeech;

.field private dxR:Ljava/util/Locale;

.field private dxS:I

.field dxT:Z

.field dxU:Z

.field dxV:Z

.field public final dxW:Landroid/speech/tts/UtteranceProgressListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field final mAudioRouter:Lhhu;

.field private final mContext:Landroid/content/Context;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lhhu;Lhym;Lidc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lice;->dK:Ljava/lang/Object;

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lice;->dxS:I

    .line 125
    new-instance v0, Licf;

    invoke-direct {v0, p0}, Licf;-><init>(Lice;)V

    iput-object v0, p0, Lice;->dxW:Landroid/speech/tts/UtteranceProgressListener;

    .line 189
    iput-object p1, p0, Lice;->mContext:Landroid/content/Context;

    .line 190
    iput-object p2, p0, Lice;->bhl:Ljava/util/concurrent/Executor;

    .line 191
    iput-object p3, p0, Lice;->dxM:Ljava/util/concurrent/Executor;

    .line 192
    iput-object p4, p0, Lice;->mAudioRouter:Lhhu;

    .line 193
    iput-object p5, p0, Lice;->mVoiceSettings:Lhym;

    .line 194
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lice;->dxO:Ljava/util/HashMap;

    .line 195
    iput-object p6, p0, Lice;->dxN:Lidc;

    .line 196
    iput-boolean v1, p0, Lice;->dxT:Z

    .line 197
    iput-boolean v1, p0, Lice;->dxU:Z

    .line 198
    return-void
.end method


# virtual methods
.method public final a(ILesk;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lesk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 210
    iget-object v0, p0, Lice;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lice;->a(Ljava/lang/String;Lesk;Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method final a(Ljava/lang/String;ILesk;ILjava/lang/String;)V
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 322
    iget-object v0, p0, Lice;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTG()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p5, v0}, Lepb;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lice;->dxR:Ljava/util/Locale;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lice;->dxR:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "LocalTtsManager"

    const-string v2, "Bad TextToSpeech instance detected. Re-creating."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    :cond_0
    iget-object v1, p0, Lice;->dxN:Lidc;

    invoke-virtual {v1, v0}, Lidc;->g(Ljava/util/Locale;)Landroid/speech/tts/TextToSpeech;

    move-result-object v1

    iput-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    iput-object v0, p0, Lice;->dxR:Ljava/util/Locale;

    iget-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1, v0}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v1, v0}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    :cond_1
    iget-object v0, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lice;->dxW:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 325
    :cond_2
    iget-object v1, p0, Lice;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 326
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "utterance:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lice;->dxP:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lice;->dxP:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 327
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    iget-object v7, p0, Lice;->dK:Ljava/lang/Object;

    monitor-enter v7

    .line 334
    :try_start_1
    iget-object v8, p0, Lice;->dxO:Ljava/util/HashMap;

    new-instance v0, Lick;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lick;-><init>(Ljava/lang/String;ILesk;ILjava/lang/String;)V

    invoke-virtual {v8, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 338
    iget-object v0, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lice;->dxV:Z

    if-eqz v0, :cond_5

    .line 340
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lice;->dxV:Z

    .line 341
    invoke-virtual {p0, v6}, Lice;->onUtteranceCompleted(Ljava/lang/String;)V

    .line 362
    :cond_4
    :goto_0
    return-void

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 336
    :catchall_1
    move-exception v0

    monitor-exit v7

    throw v0

    .line 345
    :cond_5
    iget-object v0, p0, Lice;->mAudioRouter:Lhhu;

    invoke-interface {v0}, Lhhu;->aPo()V

    .line 346
    sget-object v0, Lcgg;->aVD:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lice;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_6

    iget-object v0, p0, Lice;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lice;->mAudioManager:Landroid/media/AudioManager;

    :cond_6
    iget-object v0, p0, Lice;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v1, p0, Lice;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/16 v2, 0x12

    const/16 v3, 0x32

    div-int/2addr v3, v1

    div-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_7

    iget-object v2, p0, Lice;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x6

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_7
    iget v1, p0, Lice;->dxS:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_8

    iput v0, p0, Lice;->dxS:I

    .line 351
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lice;->dxT:Z

    .line 355
    iget-object v0, p0, Lice;->dxQ:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x0

    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "utteranceId"

    invoke-virtual {v2, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "streamType"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    if-eq p4, v3, :cond_9

    const/4 v3, 0x2

    if-ne p4, v3, :cond_b

    :cond_9
    const-string v3, "networkTts"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_a
    :goto_1
    invoke-virtual {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    move-result v0

    .line 358
    if-eqz v0, :cond_4

    .line 360
    invoke-virtual {p0, v6}, Lice;->onUtteranceCompleted(Ljava/lang/String;)V

    goto :goto_0

    .line 355
    :cond_b
    const/4 v3, 0x3

    if-ne p4, v3, :cond_a

    const-string v3, "embeddedTts"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lesk;ILjava/lang/String;)V
    .locals 6
    .param p2    # Lesk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 237
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lice;->a(Ljava/lang/String;Ljava/lang/Integer;Lesk;ILjava/lang/String;)V

    .line 238
    return-void
.end method

.method public final a(Ljava/lang/String;Lesk;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lesk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 223
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lice;->a(Ljava/lang/String;Lesk;ILjava/lang/String;)V

    .line 224
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/Integer;Lesk;ILjava/lang/String;)V
    .locals 10
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lesk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 266
    iput-boolean v1, p0, Lice;->dxV:Z

    .line 267
    iget-object v9, p0, Lice;->dxM:Ljava/util/concurrent/Executor;

    new-instance v0, Licg;

    const-string v2, "Enqueue utterance"

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    move-object v6, p3

    move v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Licg;-><init>(Lice;Ljava/lang/String;[ILjava/lang/Integer;Ljava/lang/String;Lesk;ILjava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 276
    return-void
.end method

.method public final aUL()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 286
    iget-object v1, p0, Lice;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 287
    :try_start_0
    iget-object v0, p0, Lice;->dxO:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 288
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    invoke-virtual {p0}, Lice;->stop()V

    .line 291
    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final aUM()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 435
    sget-object v0, Lcgg;->aVD:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    iget v0, p0, Lice;->dxS:I

    if-eq v0, v4, :cond_0

    .line 439
    iget-object v0, p0, Lice;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x6

    iget v2, p0, Lice;->dxS:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 440
    iput v4, p0, Lice;->dxS:I

    goto :goto_0
.end method

.method final onUtteranceCompleted(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 366
    iget-object v0, p0, Lice;->mAudioRouter:Lhhu;

    invoke-interface {v0}, Lhhu;->aPp()V

    .line 368
    iget-object v0, p0, Lice;->dxM:Ljava/util/concurrent/Executor;

    new-instance v1, Lici;

    const-string v2, "Restore Bluetooth SCO volume"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lici;-><init>(Lice;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 377
    iget-object v1, p0, Lice;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 378
    :try_start_0
    iget-object v0, p0, Lice;->dxO:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lick;

    .line 379
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    if-eqz v0, :cond_0

    iget-object v1, v0, Lick;->dyh:Lesk;

    if-eqz v1, :cond_0

    .line 382
    iget-object v1, p0, Lice;->bhl:Ljava/util/concurrent/Executor;

    new-instance v2, Licj;

    const-string v3, "Remove callback"

    invoke-direct {v2, p0, v3, p1, v0}, Licj;-><init>(Lice;Ljava/lang/String;Ljava/lang/String;Lick;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 395
    :cond_0
    return-void

    .line 379
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final stop()V
    .locals 4

    .prologue
    .line 298
    iget-boolean v0, p0, Lice;->dxT:Z

    if-eqz v0, :cond_0

    .line 300
    const/4 v0, 0x1

    iput-boolean v0, p0, Lice;->dxU:Z

    .line 315
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lice;->dxM:Ljava/util/concurrent/Executor;

    new-instance v1, Lich;

    const-string v2, "Stop"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lich;-><init>(Lice;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
