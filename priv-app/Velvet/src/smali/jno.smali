.class public final Ljno;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eqc:J

.field public erP:Ljkh;

.field public euo:Ljkg;

.field private evm:I

.field private evn:Z

.field private evo:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 110
    iput v2, p0, Ljno;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljno;->eqc:J

    iput v2, p0, Ljno;->evm:I

    iput-object v3, p0, Ljno;->euo:Ljkg;

    iput-boolean v2, p0, Ljno;->evn:Z

    iput-object v3, p0, Ljno;->erP:Ljkh;

    iput-boolean v2, p0, Ljno;->evo:Z

    iput-object v3, p0, Ljno;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljno;->eCz:I

    .line 111
    return-void
.end method


# virtual methods
.method public final Pu()J
    .locals 2

    .prologue
    .line 30
    iget-wide v0, p0, Ljno;->eqc:J

    return-wide v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljno;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljno;->eqc:J

    iget v0, p0, Ljno;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljno;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljno;->evm:I

    iget v0, p0, Ljno;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljno;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljno;->euo:Ljkg;

    if-nez v0, :cond_1

    new-instance v0, Ljkg;

    invoke-direct {v0}, Ljkg;-><init>()V

    iput-object v0, p0, Ljno;->euo:Ljkg;

    :cond_1
    iget-object v0, p0, Ljno;->euo:Ljkg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljno;->evn:Z

    iget v0, p0, Ljno;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljno;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljno;->erP:Ljkh;

    if-nez v0, :cond_2

    new-instance v0, Ljkh;

    invoke-direct {v0}, Ljkh;-><init>()V

    iput-object v0, p0, Ljno;->erP:Ljkh;

    :cond_2
    iget-object v0, p0, Ljno;->erP:Ljkh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljno;->evo:Z

    iget v0, p0, Ljno;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljno;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 129
    iget v0, p0, Ljno;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 130
    const/4 v0, 0x1

    iget-wide v2, p0, Ljno;->eqc:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 132
    :cond_0
    iget v0, p0, Ljno;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 133
    const/4 v0, 0x2

    iget v1, p0, Ljno;->evm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 135
    :cond_1
    iget-object v0, p0, Ljno;->euo:Ljkg;

    if-eqz v0, :cond_2

    .line 136
    const/4 v0, 0x3

    iget-object v1, p0, Ljno;->euo:Ljkg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 138
    :cond_2
    iget v0, p0, Ljno;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 139
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljno;->evn:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 141
    :cond_3
    iget-object v0, p0, Ljno;->erP:Ljkh;

    if-eqz v0, :cond_4

    .line 142
    const/4 v0, 0x5

    iget-object v1, p0, Ljno;->erP:Ljkh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 144
    :cond_4
    iget v0, p0, Ljno;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 145
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljno;->evo:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 147
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 148
    return-void
.end method

.method public final bqN()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Ljno;->evm:I

    return v0
.end method

.method public final bqO()Z
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Ljno;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bqP()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Ljno;->evn:Z

    return v0
.end method

.method public final bqQ()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Ljno;->evo:Z

    return v0
.end method

.method public final dA(J)Ljno;
    .locals 1

    .prologue
    .line 33
    iput-wide p1, p0, Ljno;->eqc:J

    .line 34
    iget v0, p0, Ljno;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljno;->aez:I

    .line 35
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 152
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 153
    iget v1, p0, Ljno;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 154
    const/4 v1, 0x1

    iget-wide v2, p0, Ljno;->eqc:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_0
    iget v1, p0, Ljno;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 158
    const/4 v1, 0x2

    iget v2, p0, Ljno;->evm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_1
    iget-object v1, p0, Ljno;->euo:Ljkg;

    if-eqz v1, :cond_2

    .line 162
    const/4 v1, 0x3

    iget-object v2, p0, Ljno;->euo:Ljkg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_2
    iget v1, p0, Ljno;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 166
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljno;->evn:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 169
    :cond_3
    iget-object v1, p0, Ljno;->erP:Ljkh;

    if-eqz v1, :cond_4

    .line 170
    const/4 v1, 0x5

    iget-object v2, p0, Ljno;->erP:Ljkh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_4
    iget v1, p0, Ljno;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 174
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljno;->evo:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 177
    :cond_5
    return v0
.end method

.method public final qT(I)Ljno;
    .locals 1

    .prologue
    .line 52
    iput p1, p0, Ljno;->evm:I

    .line 53
    iget v0, p0, Ljno;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljno;->aez:I

    .line 54
    return-object p0
.end method
