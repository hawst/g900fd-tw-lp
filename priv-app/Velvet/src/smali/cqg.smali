.class public final Lcqg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Landroid/location/Location;II)Ljava/lang/String;
    .locals 6
    .param p0    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide v4, 0x416312d000000000L    # 1.0E7

    .line 60
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    .line 62
    :cond_0
    new-instance v0, Lkfa;

    invoke-direct {v0}, Lkfa;-><init>()V

    .line 63
    new-instance v1, Lkex;

    invoke-direct {v1}, Lkex;-><init>()V

    .line 65
    invoke-virtual {v0, p1}, Lkfa;->uc(I)Lkfa;

    .line 66
    invoke-virtual {v0, p2}, Lkfa;->ud(I)Lkfa;

    .line 68
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lkex;->ua(I)Lkex;

    .line 69
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lkex;->ub(I)Lkex;

    .line 70
    iput-object v1, v0, Lkfa;->eVA:Lkex;

    .line 71
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    .line 72
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lkfa;->dW(J)Lkfa;

    .line 74
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Lkfa;->ad(F)Lkfa;

    .line 78
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "w "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 42
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    .line 44
    :cond_0
    const/16 v0, 0xc

    invoke-static {p0, v2, v0}, Lcqg;->a(Landroid/location/Location;II)Ljava/lang/String;

    move-result-object v0

    .line 47
    const/4 v1, 0x4

    invoke-static {p1, v1, v2}, Lcqg;->a(Landroid/location/Location;II)Ljava/lang/String;

    move-result-object v1

    .line 50
    const-string v2, " "

    invoke-static {v2}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v2

    invoke-virtual {v2}, Lifj;->aVV()Lifj;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v1, v3}, Lifj;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
