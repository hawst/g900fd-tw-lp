.class public abstract Laht;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final Yl:Ljava/lang/Object;

.field private static Yz:Laht;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Laht;->Yl:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static C(Landroid/content/Context;)Laht;
    .locals 2

    .prologue
    .line 35
    sget-object v1, Laht;->Yl:Ljava/lang/Object;

    monitor-enter v1

    .line 36
    :try_start_0
    sget-object v0, Laht;->Yz:Laht;

    if-nez v0, :cond_0

    .line 37
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    new-instance v0, Lahx;

    invoke-direct {v0, p0}, Lahx;-><init>(Landroid/content/Context;)V

    sput-object v0, Laht;->Yz:Laht;

    .line 43
    :cond_0
    :goto_0
    sget-object v0, Laht;->Yz:Laht;

    monitor-exit v1

    return-object v0

    .line 40
    :cond_1
    new-instance v0, Lahu;

    invoke-direct {v0, p0}, Lahu;-><init>(Landroid/content/Context;)V

    sput-object v0, Laht;->Yz:Laht;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract lK()Ljava/util/HashSet;
.end method

.method public abstract lL()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract onStop()V
.end method
