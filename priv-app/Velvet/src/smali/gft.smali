.class public final Lgft;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bAP:Ljava/io/IOException;

.field private final cHI:I

.field cHS:Z

.field private final cIU:I

.field private cIV:I

.field private cIW:I

.field private final cIX:[I

.field private cIY:Z

.field public final cIn:Ljava/io/InputStream;

.field final mDelegate:Ljava/io/InputStream;

.field private final rV:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;IIII)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-ge p3, p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 109
    iput-object p1, p0, Lgft;->mDelegate:Ljava/io/InputStream;

    .line 110
    mul-int v0, p4, p2

    new-array v0, v0, [B

    iput-object v0, p0, Lgft;->rV:[B

    .line 111
    mul-int v0, p3, p2

    iput v0, p0, Lgft;->cIU:I

    .line 112
    iput v1, p0, Lgft;->cIV:I

    .line 113
    iput v1, p0, Lgft;->cIW:I

    .line 114
    iput-boolean v1, p0, Lgft;->cHS:Z

    .line 115
    iput p2, p0, Lgft;->cHI:I

    .line 116
    new-array v0, p5, [I

    iput-object v0, p0, Lgft;->cIX:[I

    .line 117
    iget-object v0, p0, Lgft;->cIX:[I

    const v2, 0x7fffffff

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 119
    new-instance v0, Lgfu;

    invoke-direct {v0, p0}, Lgfu;-><init>(Lgft;)V

    iput-object v0, p0, Lgft;->cIn:Ljava/io/InputStream;

    .line 120
    iget-object v0, p0, Lgft;->cIX:[I

    aput v1, v0, v1

    .line 121
    return-void

    :cond_0
    move v0, v1

    .line 108
    goto :goto_0
.end method

.method private b(I[BII)V
    .locals 4

    .prologue
    .line 328
    iget-object v0, p0, Lgft;->rV:[B

    .line 329
    array-length v1, v0

    .line 330
    add-int v2, p1, p4

    .line 331
    if-gt v2, v1, :cond_0

    .line 332
    invoke-static {v0, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 343
    :goto_0
    return-void

    .line 333
    :cond_0
    array-length v2, v0

    if-lt p1, v2, :cond_1

    .line 335
    sub-int v1, p1, v1

    invoke-static {v0, v1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 338
    :cond_1
    sub-int/2addr v1, p1

    .line 339
    sub-int v2, p4, v1

    .line 340
    invoke-static {v0, p1, p2, p3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 341
    const/4 v3, 0x0

    add-int/2addr v1, p3

    invoke-static {v0, v3, p2, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private kk(I)I
    .locals 3

    .prologue
    .line 348
    iget-object v0, p0, Lgft;->rV:[B

    array-length v0, v0

    .line 349
    if-ge p1, v0, :cond_0

    .line 350
    :goto_0
    sub-int/2addr v0, p1

    iget v1, p0, Lgft;->cHI:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 353
    :try_start_0
    iget-object v0, p0, Lgft;->mDelegate:Ljava/io/InputStream;

    iget-object v1, p0, Lgft;->rV:[B

    iget v2, p0, Lgft;->cHI:I

    invoke-static {v0, v1, p1, v2}, Leoo;->a(Ljava/io/InputStream;[BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 349
    :cond_0
    sub-int/2addr p1, v0

    goto :goto_0

    .line 350
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 354
    :catch_0
    move-exception v0

    .line 356
    monitor-enter p0

    .line 357
    :try_start_1
    iput-object v0, p0, Lgft;->bAP:Ljava/io/IOException;

    .line 358
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 359
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    throw v0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final a(I[BII)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 258
    move v0, v1

    move v2, v1

    .line 269
    :goto_0
    monitor-enter p0

    move v3, v0

    .line 272
    :goto_1
    :try_start_0
    iget-object v0, p0, Lgft;->bAP:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lgft;->bAP:Ljava/io/IOException;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 277
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgft;->cIX:[I

    aget v0, v0, p1

    .line 278
    const v4, 0x7fffffff

    if-ne v0, v4, :cond_1

    .line 280
    monitor-exit p0

    .line 304
    :goto_2
    return v1

    .line 284
    :cond_1
    if-eqz v3, :cond_6

    .line 285
    add-int/2addr v0, v3

    .line 286
    iget-object v3, p0, Lgft;->cIX:[I

    aput v0, v3, p1

    move v4, v0

    move v0, v1

    .line 290
    :goto_3
    if-ne v2, p4, :cond_2

    .line 292
    monitor-exit p0

    move v1, p4

    goto :goto_2

    .line 295
    :cond_2
    iget v3, p0, Lgft;->cIW:I

    .line 296
    if-ne v3, v4, :cond_4

    .line 298
    iget-boolean v3, p0, Lgft;->cHS:Z

    if-eqz v3, :cond_3

    .line 304
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v2

    goto :goto_2

    .line 310
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v3, v0

    .line 315
    goto :goto_1

    .line 313
    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 314
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Interrupted waiting for buffers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 319
    sub-int v0, v3, v4

    .line 320
    sub-int v3, p4, v2

    .line 321
    if-ge v0, v3, :cond_5

    .line 322
    :goto_4
    add-int v3, p3, v2

    invoke-direct {p0, v4, p2, v3, v0}, Lgft;->b(I[BII)V

    .line 323
    add-int/2addr v2, v0

    .line 324
    goto :goto_0

    :cond_5
    move v0, v3

    .line 321
    goto :goto_4

    :cond_6
    move v4, v0

    move v0, v3

    goto :goto_3
.end method

.method public final declared-synchronized aFH()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgft;->cIY:Z

    if-eqz v0, :cond_0

    .line 137
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No splits possible, buffers rewound."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    :cond_0
    const/4 v0, 0x1

    .line 141
    :goto_0
    :try_start_1
    iget-object v1, p0, Lgft;->cIX:[I

    array-length v1, v1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lgft;->cIX:[I

    aget v1, v1, v0

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_1

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_1
    iget-object v1, p0, Lgft;->cIX:[I

    array-length v1, v1

    if-ne v0, v1, :cond_2

    .line 145
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No splits possible, too many siblings."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_2
    new-instance v1, Lgfv;

    invoke-direct {v1, p0, v0}, Lgfv;-><init>(Lgft;I)V

    .line 149
    iget-object v2, p0, Lgft;->cIX:[I

    const/4 v3, 0x0

    aput v3, v2, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    monitor-exit p0

    return-object v1
.end method

.method final k([BII)I
    .locals 10

    .prologue
    .line 159
    iget-object v0, p0, Lgft;->rV:[B

    array-length v5, v0

    .line 160
    const/4 v2, 0x0

    .line 161
    const/4 v1, 0x0

    .line 163
    const/4 v0, -0x1

    .line 174
    :goto_0
    monitor-enter p0

    .line 175
    :try_start_0
    iget-object v3, p0, Lgft;->bAP:Ljava/io/IOException;

    if-eqz v3, :cond_0

    .line 177
    iget-object v0, p0, Lgft;->bAP:Ljava/io/IOException;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180
    :cond_0
    :try_start_1
    iget-object v3, p0, Lgft;->cIX:[I

    const/4 v4, 0x0

    aget v4, v3, v4

    .line 181
    const v3, 0x7fffffff

    if-ne v4, v3, :cond_2

    .line 183
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 188
    sub-int/2addr v2, v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    :goto_1
    return v2

    .line 190
    :cond_1
    monitor-exit p0

    goto :goto_1

    .line 194
    :cond_2
    :try_start_2
    iget v3, p0, Lgft;->cIW:I

    .line 195
    const/4 v6, -0x1

    if-eq v0, v6, :cond_12

    .line 196
    add-int/2addr v3, v0

    .line 197
    iput v3, p0, Lgft;->cIW:I

    .line 198
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 199
    iget v6, p0, Lgft;->cHI:I

    if-ge v0, v6, :cond_3

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgft;->cHS:Z

    .line 208
    monitor-exit p0

    goto :goto_1

    .line 210
    :cond_3
    const/4 v0, -0x1

    move v9, v3

    move v3, v0

    move v0, v9

    .line 214
    :goto_2
    if-eqz v1, :cond_11

    .line 215
    add-int/2addr v1, v4

    .line 216
    iget-object v4, p0, Lgft;->cIX:[I

    const/4 v6, 0x0

    aput v1, v4, v6

    .line 217
    :goto_3
    if-ne v2, p3, :cond_4

    .line 222
    monitor-exit p0

    move v2, p3

    goto :goto_1

    .line 225
    :cond_4
    if-ne v0, v1, :cond_10

    .line 226
    iget-boolean v4, p0, Lgft;->cHS:Z

    if-eqz v4, :cond_5

    .line 229
    monitor-exit p0

    goto :goto_1

    .line 232
    :cond_5
    iget v4, p0, Lgft;->cHI:I

    add-int/2addr v4, v0

    iget v6, p0, Lgft;->cIV:I

    sub-int/2addr v4, v6

    if-le v4, v5, :cond_10

    .line 233
    iget-object v0, p0, Lgft;->cIX:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v1, p0, Lgft;->cIU:I

    if-lt v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, Lifv;->gX(Z)V

    iget-object v0, p0, Lgft;->cIX:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v1, p0, Lgft;->cIU:I

    sub-int v6, v0, v1

    const v1, 0x7fffffff

    iget-object v7, p0, Lgft;->cIX:[I

    array-length v8, v7

    const/4 v0, 0x0

    move v4, v0

    :goto_5
    if-ge v4, v8, :cond_7

    aget v0, v7, v4

    if-ge v0, v1, :cond_f

    :goto_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_5

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    iget v0, p0, Lgft;->cIW:I

    if-gt v1, v0, :cond_9

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lgft;->rV:[B

    array-length v4, v1

    iget v1, p0, Lgft;->cIW:I

    iget v6, p0, Lgft;->cHI:I

    add-int/2addr v1, v6

    sub-int/2addr v1, v0

    if-gt v1, v4, :cond_c

    const/4 v1, 0x1

    iput-boolean v1, p0, Lgft;->cIY:Z

    if-lt v0, v4, :cond_b

    const/4 v1, 0x0

    :goto_8
    iget-object v6, p0, Lgft;->cIX:[I

    array-length v6, v6

    if-eq v1, v6, :cond_a

    iget-object v6, p0, Lgft;->cIX:[I

    aget v6, v6, v1

    const v7, 0x7fffffff

    if-eq v6, v7, :cond_8

    iget-object v6, p0, Lgft;->cIX:[I

    aget v7, v6, v1

    sub-int/2addr v7, v4

    aput v7, v6, v1

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_9
    const/4 v0, 0x0

    goto :goto_7

    :cond_a
    sub-int/2addr v0, v4

    iget v1, p0, Lgft;->cIW:I

    sub-int/2addr v1, v4

    iput v1, p0, Lgft;->cIW:I

    :cond_b
    iput v0, p0, Lgft;->cIV:I

    .line 234
    iget-object v0, p0, Lgft;->cIX:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    move v4, v0

    .line 238
    :goto_9
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 240
    if-ne v0, v4, :cond_e

    .line 241
    invoke-direct {p0, v0}, Lgft;->kk(I)I

    move-result v1

    .line 242
    add-int/2addr v0, v1

    move v9, v0

    move v0, v1

    move v1, v9

    .line 244
    :goto_a
    sub-int/2addr v1, v4

    .line 245
    sub-int v3, p3, v2

    .line 246
    if-ge v1, v3, :cond_d

    .line 247
    :goto_b
    add-int v3, p2, v2

    invoke-direct {p0, v4, p1, v3, v1}, Lgft;->b(I[BII)V

    .line 248
    add-int/2addr v2, v1

    .line 249
    goto/16 :goto_0

    .line 233
    :cond_c
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Buffer overflow, no available space."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgft;->bAP:Ljava/io/IOException;

    iget-object v0, p0, Lgft;->bAP:Ljava/io/IOException;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_d
    move v1, v3

    .line 246
    goto :goto_b

    :cond_e
    move v1, v0

    move v0, v3

    goto :goto_a

    :cond_f
    move v0, v1

    goto :goto_6

    :cond_10
    move v4, v1

    goto :goto_9

    :cond_11
    move v1, v4

    goto/16 :goto_3

    :cond_12
    move v9, v3

    move v3, v0

    move v0, v9

    goto/16 :goto_2
.end method

.method final declared-synchronized remove(I)V
    .locals 2

    .prologue
    .line 378
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgft;->cIX:[I

    const v1, 0x7fffffff

    aput v1, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    monitor-exit p0

    return-void

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
