.class public final Lfns;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final atm:Lfjv;

.field private final cuk:Lern;

.field cxb:Lfnl;

.field private final cxc:Lekg;

.field private final cxd:Landroid/view/View$OnLayoutChangeListener;

.field private final hO:Z

.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemp;Lfjv;Lern;Z)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    new-instance v0, Lfnt;

    invoke-direct {v0, p0}, Lfnt;-><init>(Lfns;)V

    iput-object v0, p0, Lfns;->cxc:Lekg;

    .line 347
    new-instance v0, Lfnu;

    invoke-direct {v0, p0}, Lfnu;-><init>(Lfns;)V

    iput-object v0, p0, Lfns;->cxd:Landroid/view/View$OnLayoutChangeListener;

    .line 55
    iput-object p1, p0, Lfns;->mAppContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lfns;->mClock:Lemp;

    .line 57
    iput-object p3, p0, Lfns;->atm:Lfjv;

    .line 58
    iput-object p4, p0, Lfns;->cuk:Lern;

    .line 59
    iput-boolean p5, p0, Lfns;->hO:Z

    .line 60
    return-void
.end method

.method private a(Landroid/view/View;Lizj;ZJZ)Lizv;
    .locals 8

    .prologue
    .line 250
    const v0, 0x7f110001

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 251
    if-eqz v0, :cond_3

    .line 252
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v2, p4, v0

    .line 253
    const-wide/16 v0, 0x1f4

    cmp-long v0, v2, v0

    if-ltz v0, :cond_3

    .line 254
    iget-boolean v0, p0, Lfns;->hO:Z

    if-eqz v0, :cond_2

    iget-object v1, p2, Lizj;->dUo:[Liwk;

    array-length v4, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v1, v0

    invoke-virtual {v5}, Liwk;->getType()I

    move-result v6

    const/16 v7, 0x15

    if-ne v6, v7, :cond_1

    new-instance v0, Lgam;

    invoke-direct {v0, p2, v5, p4, p5}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, v0, Lgam;->cEo:I

    invoke-virtual {v0, p3}, Lgam;->fB(Z)Lgam;

    move-result-object v0

    iput-wide v2, v0, Lgam;->cEn:J

    invoke-virtual {v0}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 257
    :goto_1
    if-eqz p6, :cond_0

    .line 259
    const v1, 0x7f110001

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 264
    :cond_0
    :goto_2
    return-object v0

    .line 254
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 264
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Lekf;IILandroid/view/View;J)Z
    .locals 7

    .prologue
    const v6, 0x7f110001

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 164
    invoke-interface {p1, p4}, Lekf;->aH(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_1

    invoke-virtual {p4}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    sub-int v4, v5, v4

    invoke-virtual {p4}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    if-ge v4, v5, :cond_0

    if-gt v0, p2, :cond_1

    if-lt v3, p3, :cond_1

    :cond_0
    move v0, v1

    .line 166
    :goto_0
    invoke-virtual {p4, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p4, v6, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 164
    goto :goto_0

    :cond_2
    move v1, v2

    .line 166
    goto :goto_1
.end method

.method private static bp(Landroid/view/View;)Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 292
    const v0, 0x7f110004

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static bq(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 296
    const v0, 0x7f11000a

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfnl;)V
    .locals 1

    .prologue
    .line 86
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnl;

    iput-object v0, p0, Lfns;->cxb:Lfnl;

    .line 87
    return-void
.end method

.method public final aBn()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wj()Lekf;

    move-result-object v0

    iget-object v1, p0, Lfns;->cxc:Lekg;

    invoke-interface {v0, v1}, Lekf;->a(Lekg;)V

    .line 67
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lfns;->cxd:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 69
    return-void
.end method

.method public final aBo()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wj()Lekf;

    move-result-object v0

    iget-object v1, p0, Lfns;->cxc:Lekg;

    invoke-interface {v0, v1}, Lekf;->b(Lekg;)V

    .line 76
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v0

    iget-object v1, p0, Lfns;->cxd:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 78
    return-void
.end method

.method public final aBp()V
    .locals 11

    .prologue
    .line 94
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wk()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Lfns;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    .line 98
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wj()Lekf;

    move-result-object v2

    .line 99
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v8

    .line 102
    invoke-interface {v2}, Lekf;->getScrollY()I

    move-result v3

    .line 103
    invoke-interface {v2}, Lekf;->ate()I

    move-result v0

    add-int v4, v3, v0

    .line 105
    invoke-virtual {v8}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v9

    .line 106
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v9, :cond_2

    .line 110
    invoke-virtual {v8, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 111
    invoke-static {v5}, Lfns;->bq(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v1

    if-lez v1, :cond_1

    move-object v1, p0

    .line 115
    invoke-direct/range {v1 .. v7}, Lfns;->a(Lekf;IILandroid/view/View;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v1, v5}, Lfnl;->ak(Landroid/view/View;)V

    .line 122
    :cond_0
    invoke-static {v5}, Lfns;->bp(Landroid/view/View;)Ljava/util/List;

    move-result-object v1

    .line 123
    if-eqz v1, :cond_1

    .line 124
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    move-object v1, p0

    .line 125
    invoke-direct/range {v1 .. v7}, Lfns;->a(Lekf;IILandroid/view/View;J)Z

    goto :goto_1

    .line 106
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_2
    return-void
.end method

.method public final aBq()V
    .locals 11

    .prologue
    .line 196
    iget-object v0, p0, Lfns;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 198
    iget-object v0, p0, Lfns;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 200
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v1, v0, :cond_3

    const/4 v3, 0x1

    .line 202
    :goto_0
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 203
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v9

    .line 204
    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v9, :cond_5

    .line 205
    iget-object v0, p0, Lfns;->cxb:Lfnl;

    invoke-interface {v0}, Lfnl;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 206
    invoke-static {v1}, Lfns;->bq(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 210
    const v0, 0x7f110003

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizj;

    .line 211
    if-eqz v2, :cond_0

    .line 212
    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lfns;->a(Landroid/view/View;Lizj;ZJZ)Lizv;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_0

    .line 215
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_0
    const v0, 0x7f110002

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizj;

    .line 221
    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lfns;->a(Landroid/view/View;Lizj;ZJZ)Lizv;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_1

    .line 224
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_1
    invoke-static {v1}, Lfns;->bp(Landroid/view/View;)Ljava/util/List;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_4

    .line 230
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 231
    const v0, 0x7f110002

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizj;

    .line 232
    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lfns;->a(Landroid/view/View;Lizj;ZJZ)Lizv;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_2

    .line 235
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 200
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    .line 204
    :cond_4
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 242
    :cond_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 243
    iget-object v0, p0, Lfns;->cuk:Lern;

    new-instance v1, Lfnv;

    iget-object v2, p0, Lfns;->atm:Lfjv;

    invoke-direct {v1, v8, v2}, Lfnv;-><init>(Ljava/util/List;Lfjv;)V

    invoke-interface {v0, v1}, Lern;->a(Leri;)V

    .line 245
    :cond_6
    return-void
.end method
