.class public final Ldio;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private byT:Ldii;

.field public final mAsyncServices:Lema;

.field public final mContext:Landroid/content/Context;

.field public final mCoreServices:Lcfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfo;Lema;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldio;->mContext:Landroid/content/Context;

    .line 32
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfo;

    iput-object v0, p0, Ldio;->mCoreServices:Lcfo;

    .line 33
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lema;

    iput-object v0, p0, Ldio;->mAsyncServices:Lema;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ldhu;Ldgc;Lcks;)Ldii;
    .locals 7

    .prologue
    .line 63
    iget-object v0, p0, Ldio;->byT:Ldii;

    if-nez v0, :cond_1

    .line 64
    new-instance v3, Ldjo;

    iget-object v0, p0, Ldio;->mContext:Landroid/content/Context;

    iget-object v1, p0, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    iget-object v2, p0, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-direct {v3, v0, v1, v2}, Ldjo;-><init>(Landroid/content/Context;Lcjs;Lchk;)V

    .line 66
    new-instance v0, Ldjk;

    iget-object v1, p0, Ldio;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DD()Lcjs;

    move-result-object v2

    iget-object v4, p0, Ldio;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    iget-object v5, p0, Ldio;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->aus()Leqo;

    move-result-object v5

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Ldjk;-><init>(Landroid/content/Context;Lcjs;Ldjo;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ldgc;)V

    .line 74
    iget-object v1, p0, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-interface {v1}, Lcke;->NO()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    new-instance v1, Ldip;

    invoke-direct {v1, p0, v0, p3}, Ldip;-><init>(Ldio;Ldjk;Lcks;)V

    invoke-virtual {v0, v1}, Ldjk;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 89
    :cond_0
    invoke-virtual {v0}, Ldjk;->acg()V

    .line 90
    new-instance v1, Ldii;

    invoke-direct {v1, v0, p1}, Ldii;-><init>(Ldjk;Ldhu;)V

    iput-object v1, p0, Ldio;->byT:Ldii;

    .line 92
    :cond_1
    iget-object v0, p0, Ldio;->byT:Ldii;

    return-object v0
.end method
