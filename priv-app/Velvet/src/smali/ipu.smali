.class abstract Lipu;
.super Ljava/util/AbstractSet;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 846
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method abstract aWB()Lipn;
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 870
    invoke-virtual {p0}, Lipu;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0}, Lipn;->clear()V

    .line 871
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 850
    instance-of v1, p1, Lipo;

    if-eqz v1, :cond_0

    .line 852
    check-cast p1, Lipo;

    .line 853
    invoke-interface {p1}, Lipo;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 860
    :cond_0
    :goto_0
    return v0

    .line 856
    :cond_1
    invoke-virtual {p0}, Lipu;->aWB()Lipn;

    move-result-object v1

    invoke-interface {p1}, Lipo;->aWi()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lipn;->bj(Ljava/lang/Object;)I

    move-result v1

    .line 857
    invoke-interface {p1}, Lipo;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 865
    invoke-virtual {p0, p1}, Lipu;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lipu;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0}, Lipn;->aWA()Ljava/util/Set;

    move-result-object v0

    check-cast p1, Lipo;

    invoke-interface {p1}, Lipo;->aWi()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
