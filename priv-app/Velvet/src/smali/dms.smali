.class public final Ldms;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "PG"


# static fields
.field private static final bCA:[[B

.field private static bCD:Landroid/net/SSLSessionCache;


# instance fields
.field private final bCB:Z

.field private bCC:Ljavax/net/ssl/SSLSocketFactory;

.field private bCE:Z

.field private final bCd:Ldla;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [[B

    const/4 v1, 0x0

    const/4 v2, 0x6

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x8

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Ldms;->bCA:[[B

    return-void

    nop

    :array_0
    .array-data 1
        0x73t
        0x70t
        0x64t
        0x79t
        0x2ft
        0x33t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x68t
        0x74t
        0x74t
        0x70t
        0x2ft
        0x31t
        0x2et
        0x31t
    .end array-data
.end method

.method public constructor <init>(Ldla;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    .line 79
    iput-object p1, p0, Ldms;->bCd:Ldla;

    .line 80
    iput-object p2, p0, Ldms;->mContext:Landroid/content/Context;

    .line 81
    iput-boolean p3, p0, Ldms;->bCB:Z

    .line 82
    return-void
.end method

.method private declared-synchronized a(Ljava/net/Socket;)Ljava/net/Socket;
    .locals 3

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Ldms;->bCE:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 165
    :cond_0
    :goto_0
    monitor-exit p0

    return-object p1

    .line 149
    :cond_1
    :try_start_1
    instance-of v1, p1, Ljavax/net/ssl/SSLSocket;

    if-eqz v1, :cond_0

    .line 150
    move-object v0, p1

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    move-object v1, v0

    .line 155
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 157
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    invoke-interface {v1}, Ljavax/net/ssl/SSLSession;->getSessionContext()Ljavax/net/ssl/SSLSessionContext;

    move-result-object v1

    .line 158
    const/16 v2, 0x19

    invoke-interface {v1, v2}, Ljavax/net/ssl/SSLSessionContext;->setSessionCacheSize(I)V

    .line 159
    const/4 v1, 0x1

    iput-boolean v1, p0, Ldms;->bCE:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized adA()V
    .locals 3

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Ldms;->bCd:Ldla;

    invoke-interface {v0}, Ldla;->Ex()Z

    move-result v2

    .line 171
    iget-boolean v0, p0, Ldms;->bCB:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldms;->bCd:Ldla;

    invoke-interface {v0}, Ldla;->Ey()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 178
    :goto_0
    if-eqz v2, :cond_2

    iget-object v0, p0, Ldms;->mContext:Landroid/content/Context;

    invoke-static {v0}, Ldms;->an(Landroid/content/Context;)Landroid/net/SSLSessionCache;

    move-result-object v0

    .line 179
    :goto_1
    iget-object v2, p0, Ldms;->bCd:Ldla;

    invoke-interface {v2}, Ldla;->Dg()I

    move-result v2

    invoke-static {v2, v0}, Landroid/net/SSLCertificateSocketFactory;->getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    .line 181
    if-eqz v1, :cond_0

    .line 182
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    check-cast v0, Landroid/net/SSLCertificateSocketFactory;

    sget-object v1, Ldms;->bCA:[[B

    invoke-virtual {v0, v1}, Landroid/net/SSLCertificateSocketFactory;->setNpnProtocols([[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :cond_0
    monitor-exit p0

    return-void

    .line 171
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 178
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized an(Landroid/content/Context;)Landroid/net/SSLSessionCache;
    .locals 2

    .prologue
    .line 136
    const-class v1, Ldms;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldms;->bCD:Landroid/net/SSLSessionCache;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Landroid/net/SSLSessionCache;

    invoke-direct {v0, p0}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V

    sput-object v0, Ldms;->bCD:Landroid/net/SSLSessionCache;

    .line 141
    :cond_0
    sget-object v0, Ldms;->bCD:Landroid/net/SSLSessionCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ldms;->adA()V

    .line 108
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    invoke-direct {p0, v0}, Ldms;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public final createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ldms;->adA()V

    .line 116
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    invoke-direct {p0, v0}, Ldms;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public final createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ldms;->adA()V

    .line 124
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public final createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Ldms;->adA()V

    .line 132
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public final createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ldms;->adA()V

    .line 101
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    invoke-direct {p0, v0}, Ldms;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ldms;->adA()V

    .line 87
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getDefaultCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ldms;->adA()V

    .line 93
    iget-object v0, p0, Ldms;->bCC:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
