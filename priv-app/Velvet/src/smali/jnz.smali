.class public final Ljnz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private evP:I

.field private evQ:I

.field private evR:Ljava/lang/String;

.field private evS:Ljava/lang/String;

.field private evT:Ljava/lang/String;

.field private evU:Z

.field private evV:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 407
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 408
    iput v1, p0, Ljnz;->aez:I

    iput v1, p0, Ljnz;->evP:I

    iput v1, p0, Ljnz;->evQ:I

    const-string v0, ""

    iput-object v0, p0, Ljnz;->evR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnz;->evS:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnz;->evT:Ljava/lang/String;

    iput-boolean v1, p0, Ljnz;->evU:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljnz;->evV:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljnz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnz;->eCz:I

    .line 409
    return-void
.end method


# virtual methods
.method public final OJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Ljnz;->evR:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 246
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljnz;->evP:I

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljnz;->evQ:I

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnz;->evR:Ljava/lang/String;

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnz;->evS:Ljava/lang/String;

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnz;->evT:Ljava/lang/String;

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljnz;->evU:Z

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljnz;->evV:Z

    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljnz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 428
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 429
    const/4 v0, 0x1

    iget v1, p0, Ljnz;->evP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 431
    :cond_0
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 432
    const/4 v0, 0x2

    iget v1, p0, Ljnz;->evQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 434
    :cond_1
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 435
    const/4 v0, 0x3

    iget-object v1, p0, Ljnz;->evR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 437
    :cond_2
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 438
    const/4 v0, 0x4

    iget-object v1, p0, Ljnz;->evS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 440
    :cond_3
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 441
    const/4 v0, 0x5

    iget-object v1, p0, Ljnz;->evT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 443
    :cond_4
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 444
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljnz;->evU:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 446
    :cond_5
    iget v0, p0, Ljnz;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 447
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljnz;->evV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 449
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 450
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 454
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 455
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 456
    const/4 v1, 0x1

    iget v2, p0, Ljnz;->evP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    :cond_0
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 460
    const/4 v1, 0x2

    iget v2, p0, Ljnz;->evQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 463
    :cond_1
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 464
    const/4 v1, 0x3

    iget-object v2, p0, Ljnz;->evR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_2
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 468
    const/4 v1, 0x4

    iget-object v2, p0, Ljnz;->evS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_3
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 472
    const/4 v1, 0x5

    iget-object v2, p0, Ljnz;->evT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    :cond_4
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 476
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljnz;->evU:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 479
    :cond_5
    iget v1, p0, Ljnz;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 480
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljnz;->evV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 483
    :cond_6
    return v0
.end method

.method public final xw(Ljava/lang/String;)Ljnz;
    .locals 1

    .prologue
    .line 309
    if-nez p1, :cond_0

    .line 310
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 312
    :cond_0
    iput-object p1, p0, Ljnz;->evR:Ljava/lang/String;

    .line 313
    iget v0, p0, Ljnz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnz;->aez:I

    .line 314
    return-object p0
.end method
