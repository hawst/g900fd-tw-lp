.class public final Lbep;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbju;->n(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avW:Z

    invoke-static {p1, v1, v2}, Lbju;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->auB:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avX:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avY:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Lbju;->b(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avZ:Z

    invoke-static {p1, v1, v2}, Lbju;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avr:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avs:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->awa:Z

    invoke-static {p1, v1, v2}, Lbju;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avt:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, Lbju;->o(Landroid/os/Parcel;I)V

    return-void
.end method

.method public static r(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .locals 12

    const/4 v4, 0x0

    const/4 v9, 0x0

    invoke-static {p0}, Lbjs;->x(Landroid/os/Parcel;)I

    move-result v0

    move v8, v9

    move v7, v9

    move v6, v9

    move v5, v9

    move-object v3, v4

    move v2, v9

    move v1, v9

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v10

    if-ge v10, v0, :cond_0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v10

    const v11, 0xffff

    and-int/2addr v11, v10

    sparse-switch v11, :sswitch_data_0

    invoke-static {p0, v10}, Lbjs;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p0, v10}, Lbjs;->c(Landroid/os/Parcel;I)Z

    move-result v2

    goto :goto_0

    :sswitch_1
    invoke-static {p0, v10}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_2
    invoke-static {p0, v10}, Lbjs;->m(Landroid/os/Parcel;I)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_0

    :sswitch_3
    sget-object v4, Lcom/google/android/gms/appdatasearch/Section;->CREATOR:Lbcy;

    invoke-static {p0, v10, v4}, Lbjs;->c(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_0

    :sswitch_4
    invoke-static {p0, v10}, Lbjs;->c(Landroid/os/Parcel;I)Z

    move-result v5

    goto :goto_0

    :sswitch_5
    invoke-static {p0, v10}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v6

    goto :goto_0

    :sswitch_6
    invoke-static {p0, v10}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v7

    goto :goto_0

    :sswitch_7
    invoke-static {p0, v10}, Lbjs;->c(Landroid/os/Parcel;I)Z

    move-result v8

    goto :goto_0

    :sswitch_8
    invoke-static {p0, v10}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v9

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v10

    if-eq v10, v0, :cond_1

    new-instance v1, Lbjt;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overread allowed size end="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lbjt;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v1

    :cond_1
    new-instance v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/QuerySpecification;-><init>(IZLjava/util/List;Ljava/util/List;ZIIZI)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lbep;->r(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/appdatasearch/QuerySpecification;

    return-object v0
.end method
