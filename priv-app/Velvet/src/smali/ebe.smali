.class public final Lebe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final arU:Landroid/animation/TimeInterpolator;


# instance fields
.field private final bTN:Lebi;

.field final bTO:Ldqq;

.field private final bTP:Landroid/animation/ValueAnimator;

.field private final bTQ:Landroid/animation/ValueAnimator;

.field private bso:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lebe;->arU:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Lebi;Lecc;Ldqq;Lebh;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    const/4 v3, 0x2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lebe;->bTN:Lebi;

    .line 41
    iput-object p3, p0, Lebe;->bTO:Ldqq;

    .line 43
    invoke-virtual {p2, p0}, Lecc;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 45
    iget-object v0, p0, Lebe;->bTO:Ldqq;

    const-string v1, "alpha"

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lebe;->bTP:Landroid/animation/ValueAnimator;

    .line 46
    iget-object v0, p0, Lebe;->bTP:Landroid/animation/ValueAnimator;

    sget-object v1, Lebe;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 47
    iget-object v0, p0, Lebe;->bTP:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 49
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    .line 50
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    sget-object v1, Lebe;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 51
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 52
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    new-instance v1, Lebf;

    invoke-direct {v1, p0}, Lebf;-><init>(Lebe;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 58
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    new-instance v1, Lebg;

    invoke-direct {v1, p0, p4}, Lebg;-><init>(Lebe;Lebh;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 66
    return-void

    .line 45
    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data

    .line 49
    :array_1
    .array-data 4
        0x0
        0xff
    .end array-data
.end method


# virtual methods
.method public final aC(II)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 77
    iget-object v0, p0, Lebe;->bTO:Ldqq;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Ldqq;->gi(I)V

    .line 78
    return-void

    .line 76
    nop

    :array_0
    .array-data 4
        0xff
        0xff
    .end array-data
.end method

.method public final amH()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 73
    :cond_0
    return-void
.end method

.method public final amI()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 116
    iget v2, p0, Lebe;->bso:I

    if-nez v2, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 120
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget v0, p0, Lebe;->bso:I

    invoke-static {v0}, Ldtd;->gs(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    iget-object v0, p0, Lebe;->bTN:Lebi;

    invoke-interface {v0, v1}, Lebi;->bW(Z)Z

    :cond_2
    move v0, v1

    .line 126
    goto :goto_0
.end method

.method public final z(IZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 89
    iget v0, p0, Lebe;->bso:I

    if-ne v0, p1, :cond_0

    .line 112
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lebe;->bTP:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 95
    if-nez p1, :cond_3

    move v0, v1

    .line 96
    :goto_1
    if-eqz p2, :cond_4

    .line 97
    iget-object v2, p0, Lebe;->bTP:Landroid/animation/ValueAnimator;

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v0, v3, v1

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 98
    iget-object v0, p0, Lebe;->bTP:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 104
    :goto_2
    if-nez p1, :cond_2

    .line 105
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lebe;->bTQ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 108
    :cond_1
    iget-object v0, p0, Lebe;->bTO:Ldqq;

    invoke-virtual {v0, v1}, Ldqq;->gi(I)V

    .line 111
    :cond_2
    iput p1, p0, Lebe;->bso:I

    goto :goto_0

    .line 95
    :cond_3
    const/16 v0, 0xff

    goto :goto_1

    .line 100
    :cond_4
    iget-object v2, p0, Lebe;->bTO:Ldqq;

    invoke-virtual {v2, v0}, Ldqq;->setAlpha(I)V

    goto :goto_2
.end method
