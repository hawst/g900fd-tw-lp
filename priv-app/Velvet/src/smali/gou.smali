.class public final Lgou;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cRG:Ljava/lang/ClassLoader;

.field cRH:Ljava/lang/ClassLoader;

.field cRI:Ljava/io/File;

.field cRn:Ljava/util/jar/JarFile;

.field final mContext:Landroid/content/Context;

.field final mFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/ClassLoader;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lgou;->mContext:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lgou;->mFile:Ljava/io/File;

    .line 26
    iput-object p3, p0, Lgou;->cRG:Ljava/lang/ClassLoader;

    .line 27
    iput-object p4, p0, Lgou;->cRI:Ljava/io/File;

    .line 28
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/Throwable;)Lgoo;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lgoo;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgou;->mFile:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private getClassLoader()Ljava/lang/ClassLoader;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lgou;->cRH:Ljava/lang/ClassLoader;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not loaded yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    iget-object v0, p0, Lgou;->cRH:Ljava/lang/ClassLoader;

    return-object v0
.end method


# virtual methods
.method public final nq(Ljava/lang/String;)Lgop;
    .locals 4

    .prologue
    .line 58
    :try_start_0
    invoke-direct {p0}, Lgou;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    .line 63
    new-instance v1, Lgop;

    iget-object v2, p0, Lgou;->cRn:Ljava/util/jar/JarFile;

    invoke-direct {p0}, Lgou;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lgop;-><init>(Ljava/util/jar/JarFile;Ljava/lang/ClassLoader;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    invoke-direct {p0, p1, v0}, Lgou;->c(Ljava/lang/String;Ljava/lang/Throwable;)Lgoo;

    move-result-object v0

    throw v0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    invoke-direct {p0, p1, v0}, Lgou;->c(Ljava/lang/String;Ljava/lang/Throwable;)Lgoo;

    move-result-object v0

    throw v0

    .line 68
    :catch_2
    move-exception v0

    .line 69
    invoke-direct {p0, p1, v0}, Lgou;->c(Ljava/lang/String;Ljava/lang/Throwable;)Lgoo;

    move-result-object v0

    throw v0
.end method
