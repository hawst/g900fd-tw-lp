.class public final Lgjw;
.super Lcom/google/speech/recognizer/ResourceManager;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/speech/recognizer/ResourceManager;-><init>()V

    return-void
.end method

.method public static f(Ljava/lang/String;[Ljava/lang/String;)Lgjw;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 17
    new-instance v1, Lgjw;

    invoke-direct {v1}, Lgjw;-><init>()V

    .line 19
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 22
    invoke-static {v2}, Lgjo;->k(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 23
    invoke-super {v1}, Lcom/google/speech/recognizer/ResourceManager;->validate()V

    iget-wide v2, v1, Lcom/google/speech/recognizer/ResourceManager;->nativeObj:J

    invoke-super {v1, v2, v3, p0, p1}, Lcom/google/speech/recognizer/ResourceManager;->nativeInitFromFile(JLjava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 34
    :goto_0
    if-nez v2, :cond_2

    move-object v0, v1

    .line 39
    :goto_1
    return-object v0

    .line 25
    :cond_0
    invoke-static {v2}, Lgjw;->l(Ljava/io/File;)[B

    move-result-object v2

    .line 26
    if-nez v2, :cond_1

    .line 27
    const-string v1, "Greco3ResourceManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error reading g3 config file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 31
    :cond_1
    invoke-super {v1}, Lcom/google/speech/recognizer/ResourceManager;->validate()V

    iget-wide v4, v1, Lcom/google/speech/recognizer/ResourceManager;->nativeObj:J

    invoke-super {v1, v4, v5, v2, p1}, Lcom/google/speech/recognizer/ResourceManager;->nativeInitFromProto(J[B[Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 38
    :cond_2
    const-string v1, "Greco3ResourceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to bring up g3, Status code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static l(Ljava/io/File;)[B
    .locals 1

    .prologue
    .line 44
    :try_start_0
    invoke-static {p0}, Leoo;->e(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 46
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
