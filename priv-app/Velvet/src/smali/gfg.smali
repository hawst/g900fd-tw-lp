.class public final Lgfg;
.super Lgfm;
.source "PG"


# instance fields
.field private cIA:I

.field private final cIB:I

.field private final cIz:I


# direct methods
.method public constructor <init>(IIZLgfn;ZI)V
    .locals 6

    .prologue
    .line 34
    const v2, 0x8000

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lgfm;-><init>(IIZLgfn;Z)V

    .line 35
    iput p6, p0, Lgfg;->cIz:I

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lgfg;->cIA:I

    .line 39
    iget v0, p0, Lgfg;->anB:I

    div-int/lit16 v0, v0, 0x3e8

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgfg;->cIB:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final aFC()Landroid/media/AudioRecord;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 79
    iget v0, p0, Lgfg;->cIz:I

    if-ltz v0, :cond_2

    .line 82
    :try_start_0
    const-class v0, Landroid/media/AudioRecord;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/media/AudioAttributes;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Landroid/media/AudioFormat;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 84
    const-class v1, Landroid/media/AudioAttributes$Builder;

    const-string v2, "setInternalCapturePreset"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 87
    new-instance v2, Landroid/media/AudioFormat$Builder;

    invoke-direct {v2}, Landroid/media/AudioFormat$Builder;-><init>()V

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/media/AudioFormat$Builder;->setChannelMask(I)Landroid/media/AudioFormat$Builder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/media/AudioFormat$Builder;->setEncoding(I)Landroid/media/AudioFormat$Builder;

    move-result-object v2

    iget v3, p0, Lgfg;->anB:I

    invoke-virtual {v2, v3}, Landroid/media/AudioFormat$Builder;->setSampleRate(I)Landroid/media/AudioFormat$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/AudioFormat$Builder;->build()Landroid/media/AudioFormat;

    move-result-object v2

    .line 90
    new-instance v3, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v3}, Landroid/media/AudioAttributes$Builder;-><init>()V

    .line 91
    iget-boolean v4, p0, Lgfg;->cIJ:Z

    if-eqz v4, :cond_0

    .line 93
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v6, 0x7cf

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_0
    invoke-virtual {v3}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v1

    .line 97
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    const/4 v1, 0x2

    iget v2, p0, Lgfg;->cIH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x3

    iget v2, p0, Lgfg;->cIz:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioRecord;

    .line 100
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    if-eq v1, v8, :cond_1

    .line 101
    const-string v1, "DSPMicrophoneInputStream"

    const-string v2, "Failed to initialize AudioRecord"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 102
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    const/4 v0, 0x0

    .line 115
    :cond_1
    :goto_0
    return-object v0

    .line 107
    :catch_0
    move-exception v0

    .line 110
    :goto_1
    const-string v1, "DSPMicrophoneInputStream"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to construct AudioRecord for capture session "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lgfg;->cIz:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 112
    invoke-super {p0}, Lgfm;->aFC()Landroid/media/AudioRecord;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_2
    invoke-super {p0}, Lgfm;->aFC()Landroid/media/AudioRecord;

    move-result-object v0

    goto :goto_0

    .line 107
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method public final kj(I)V
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lgfg;->cIB:I

    mul-int/lit16 v0, v0, 0x1388

    iput v0, p0, Lgfg;->cIA:I

    .line 55
    return-void
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lgfg;->cIA:I

    if-nez v0, :cond_1

    .line 61
    const/4 v0, -0x1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 63
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lgfm;->read([BII)I

    move-result v0

    .line 65
    iget v1, p0, Lgfg;->cIA:I

    if-lez v1, :cond_0

    .line 68
    iget v1, p0, Lgfg;->cIA:I

    sub-int/2addr v1, v0

    iput v1, p0, Lgfg;->cIA:I

    .line 69
    iget v1, p0, Lgfg;->cIA:I

    if-gez v1, :cond_0

    .line 70
    const/4 v1, 0x0

    iput v1, p0, Lgfg;->cIA:I

    goto :goto_0
.end method
