.class final Lgfu;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private final cIZ:Lgft;


# direct methods
.method constructor <init>(Lgft;)V
    .locals 0

    .prologue
    .line 425
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 426
    iput-object p1, p0, Lgfu;->cIZ:Lgft;

    .line 427
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 446
    iget-object v0, p0, Lgfu;->cIZ:Lgft;

    invoke-virtual {v0, v5}, Lgft;->remove(I)V

    .line 447
    iget-object v1, p0, Lgfu;->cIZ:Lgft;

    :try_start_0
    iget-object v0, v1, Lgft;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    monitor-enter v1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v1, Lgft;->cHS:Z

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catch_0
    move-exception v0

    const-string v2, "Tee"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IOException closing audio track: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final read()I
    .locals 2

    .prologue
    .line 431
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Find some other app to be inefficient in."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lgfu;->cIZ:Lgft;

    invoke-virtual {v0, p1, p2, p3}, Lgft;->k([BII)I

    move-result v0

    .line 437
    if-nez v0, :cond_0

    .line 438
    const/4 v0, -0x1

    .line 440
    :cond_0
    return v0
.end method
