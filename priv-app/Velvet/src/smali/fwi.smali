.class public final Lfwi;
.super Lfus;
.source "PG"


# instance fields
.field private final cCJ:Ljeg;

.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Ljeg;Lgbd;Lgbr;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p6}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 53
    iput-object p4, p0, Lfwi;->cCJ:Ljeg;

    .line 54
    iput-object p5, p0, Lfwi;->mPhotoWithAttributionDecorator:Lgbd;

    .line 55
    return-void
.end method

.method private f(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 374
    iget-object v0, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    .line 375
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p2

    sub-long/2addr v0, v2

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lesi;->b(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 13
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v5, 0x8

    const v12, 0x7f0d015b

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-virtual {p2}, Lizj;->getType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 70
    const-string v0, "QpResearchTopicEntyAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skip unsupported entry type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lizj;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    :goto_0
    return-object v3

    .line 62
    :sswitch_0
    invoke-virtual {p2}, Lizj;->getType()I

    move-result v2

    const/16 v4, 0x80

    if-ne v2, v4, :cond_1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p2, Lizj;->dTx:Ljef;

    if-nez v0, :cond_2

    const-string v0, "QpResearchTopicEntyAdapter"

    const-string v1, "got an empty browseModeAuthorStoryEntry"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    invoke-virtual {v3, v5}, Lanh;->cm(I)Lanh;

    iput-object p2, v3, Lanh;->ahu:Lizj;

    new-instance v0, Lanj;

    invoke-direct {v0}, Lanj;-><init>()V

    iput-object v0, v3, Lanh;->agF:Lanj;

    iget-object v2, p2, Lizj;->dTx:Ljef;

    invoke-virtual {v2}, Ljef;->pb()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Ljef;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lanj;->ao(Ljava/lang/String;)Lanj;

    :cond_3
    invoke-virtual {p0, p1, v2}, Lfwi;->c(Landroid/content/Context;Ljef;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lanj;->ap(Ljava/lang/String;)Lanj;

    :cond_4
    invoke-virtual {v2}, Ljef;->pf()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Ljef;->oo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lanj;->ar(Ljava/lang/String;)Lanj;

    :cond_5
    iget-object v4, v2, Ljef;->aiX:Ljcn;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lfwi;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v5, v2, Ljef;->aiX:Ljcn;

    invoke-virtual {v4, p1, v5, v12, v12}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v4

    iput-object v4, v0, Lanj;->ahM:Laoi;

    :cond_6
    invoke-virtual {v2}, Ljef;->qG()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xd6

    new-array v4, v1, [I

    invoke-static {p2, v0, v4}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    const/16 v5, 0xd5

    new-array v1, v1, [I

    invoke-static {v4, v5, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    new-instance v0, Lfsa;

    const/16 v4, 0xd6

    invoke-direct {v0, v4}, Lfsa;-><init>(I)V

    iget-object v4, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v5, p0, Lfwi;->cCJ:Ljeg;

    iget-object v5, v5, Ljeg;->ajE:Ljei;

    invoke-virtual {v0, v4, v1, v5}, Lfsa;->a(Lizq;Liwk;Ljei;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    iget-object v0, v3, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahH:Laom;

    invoke-virtual {v2}, Ljef;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laom;->bZ(Ljava/lang/String;)Laom;

    iget-object v0, v3, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahH:Laom;

    invoke-virtual {v2}, Ljef;->bcs()Z

    move-result v1

    invoke-virtual {v0, v1}, Laom;->bd(Z)Laom;

    goto/16 :goto_0

    .line 64
    :sswitch_1
    invoke-virtual {p2}, Lizj;->getType()I

    move-result v2

    const/16 v4, 0x53

    if-ne v2, v4, :cond_7

    move v2, v0

    :goto_2
    invoke-static {v2}, Lifv;->gY(Z)V

    iget-object v2, p2, Lizj;->dTC:Lixm;

    if-nez v2, :cond_8

    const-string v0, "QpResearchTopicEntyAdapter"

    const-string v1, "got an empty browseModeVideoEntry"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    move v2, v1

    goto :goto_2

    :cond_8
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/16 v2, 0x16

    invoke-virtual {v3, v2}, Lanh;->cm(I)Lanh;

    iput-object p2, v3, Lanh;->ahu:Lizj;

    new-instance v2, Laps;

    invoke-direct {v2}, Laps;-><init>()V

    iput-object v2, v3, Lanh;->agS:Laps;

    iget-object v4, p2, Lizj;->dTC:Lixm;

    invoke-virtual {v4}, Lixm;->pb()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {v4}, Lixm;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Laps;->dD(Ljava/lang/String;)Laps;

    :cond_9
    invoke-virtual {v4}, Lixm;->bai()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {v4}, Lixm;->tn()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Laps;->dE(Ljava/lang/String;)Laps;

    :cond_a
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Lixm;->bao()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v4}, Lixm;->ban()J

    move-result-wide v6

    invoke-direct {p0, p1, v6, v7}, Lfwi;->f(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-virtual {v4}, Lixm;->bak()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f10003f

    invoke-virtual {v4}, Lixm;->baj()I

    move-result v8

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v4}, Lixm;->baj()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v0, v1

    invoke-virtual {v6, v7, v8, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, " \u00b7 "

    invoke-static {v0, v5}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laps;->dF(Ljava/lang/String;)Laps;

    :cond_d
    iget-object v0, v4, Lixm;->aiX:Ljcn;

    if-eqz v0, :cond_f

    invoke-virtual {v4}, Lixm;->bam()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v4}, Lixm;->bal()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laps;->dH(Ljava/lang/String;)Laps;

    :cond_e
    iget-object v0, p0, Lfwi;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v1, v4, Lixm;->aiX:Ljcn;

    const v5, 0x7f0d0178

    const v6, 0x7f0d0179

    invoke-virtual {v0, p1, v1, v5, v6}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laps;->dG(Ljava/lang/String;)Laps;

    :cond_f
    iget-object v0, v4, Lixm;->dNL:Lixx;

    if-eqz v0, :cond_0

    new-instance v0, Lfsa;

    const/16 v1, 0x9d

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    iget-object v1, v4, Lixm;->dNL:Lixx;

    invoke-virtual {v0, v1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    goto/16 :goto_0

    .line 66
    :sswitch_2
    invoke-virtual {p2}, Lizj;->getType()I

    move-result v2

    const/16 v4, 0x51

    if-ne v2, v4, :cond_10

    :goto_3
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p2, Lizj;->dTA:Lixl;

    if-nez v0, :cond_11

    const-string v0, "QpResearchTopicEntyAdapter"

    const-string v1, "got an empty browseModeEntityEntry"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_10
    move v0, v1

    goto :goto_3

    :cond_11
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    invoke-virtual {v3, v5}, Lanh;->cm(I)Lanh;

    iput-object p2, v3, Lanh;->ahu:Lizj;

    new-instance v0, Lanj;

    invoke-direct {v0}, Lanj;-><init>()V

    iput-object v0, v3, Lanh;->agF:Lanj;

    iget-object v2, p2, Lizj;->dTA:Lixl;

    invoke-virtual {v2}, Lixl;->pb()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-virtual {v2}, Lixl;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lanj;->ao(Ljava/lang/String;)Lanj;

    :cond_12
    iget-object v4, v2, Lixl;->dNH:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_13

    const-string v4, " \u00b7 "

    iget-object v5, v2, Lixl;->dNH:[Ljava/lang/String;

    invoke-static {v4, v5}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lanj;->aq(Ljava/lang/String;)Lanj;

    :cond_13
    iget-object v4, v2, Lixl;->dNG:Ljdq;

    if-eqz v4, :cond_14

    iget-object v4, v2, Lixl;->dNG:Ljdq;

    invoke-static {p1, v4, v1}, Lgbh;->a(Landroid/content/Context;Ljdq;I)Laok;

    move-result-object v1

    iput-object v1, v0, Lanj;->ahN:Laok;

    :cond_14
    iget-object v1, v2, Lixl;->aiX:Ljcn;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lfwi;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v4, v2, Lixl;->aiX:Ljcn;

    invoke-virtual {v1, p1, v4, v12, v12}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v1

    iput-object v1, v0, Lanj;->ahM:Laoi;

    :cond_15
    iget-object v0, v2, Lixl;->ahD:Lixx;

    if-eqz v0, :cond_0

    new-instance v0, Lfsa;

    const/16 v1, 0x9b

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    iget-object v1, v2, Lixl;->ahD:Lixx;

    invoke-virtual {v0, v1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    goto/16 :goto_0

    .line 68
    :sswitch_3
    invoke-virtual {p2}, Lizj;->getType()I

    move-result v2

    const/16 v4, 0x64

    if-ne v2, v4, :cond_16

    move v2, v0

    :goto_4
    invoke-static {v2}, Lifv;->gY(Z)V

    iget-object v2, p2, Lizj;->dTZ:Ljef;

    if-nez v2, :cond_17

    const-string v0, "QpResearchTopicEntyAdapter"

    const-string v1, "got an empty tvRelatedWebSiteEntry"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_16
    move v2, v1

    goto :goto_4

    :cond_17
    new-instance v6, Lanh;

    invoke-direct {v6}, Lanh;-><init>()V

    invoke-virtual {v6, v5}, Lanh;->cm(I)Lanh;

    iput-object p2, v6, Lanh;->ahu:Lizj;

    new-instance v7, Lanj;

    invoke-direct {v7}, Lanj;-><init>()V

    iput-object v7, v6, Lanh;->agF:Lanj;

    iget-object v8, p2, Lizj;->dTZ:Ljef;

    invoke-virtual {v8}, Ljef;->pb()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-virtual {v8}, Ljef;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lanj;->ao(Ljava/lang/String;)Lanj;

    :cond_18
    invoke-virtual {v8}, Ljef;->pX()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-virtual {v8}, Ljef;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lanj;->aq(Ljava/lang/String;)Lanj;

    :cond_19
    invoke-virtual {v8}, Ljef;->bip()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-virtual {v8}, Ljef;->bio()Ljava/lang/String;

    move-result-object v2

    :goto_5
    invoke-virtual {v8}, Ljef;->bir()Z

    move-result v4

    if-eqz v4, :cond_1e

    const v4, 0x7f0a048a

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljef;->biq()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v1

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_6
    invoke-virtual {v8}, Ljef;->bao()Z

    move-result v5

    if-eqz v5, :cond_1d

    invoke-virtual {v8}, Ljef;->ban()J

    move-result-wide v10

    invoke-direct {p0, p1, v10, v11}, Lfwi;->f(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    :goto_7
    const-string v9, " \u00b7 "

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/CharSequence;

    aput-object v2, v10, v1

    aput-object v4, v10, v0

    const/4 v0, 0x2

    aput-object v5, v10, v0

    invoke-static {v9, v10}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1a

    invoke-virtual {v7, v0}, Lanj;->ap(Ljava/lang/String;)Lanj;

    :cond_1a
    invoke-virtual {v8}, Ljef;->qG()Z

    move-result v0

    if-eqz v0, :cond_1b

    new-instance v0, Lfsa;

    const/16 v1, 0xbb

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    invoke-virtual {v8}, Ljef;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v6, Lanh;->ahs:Lani;

    :cond_1b
    iget-object v0, v8, Ljef;->aiX:Ljcn;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lfwi;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v1, v8, Ljef;->aiX:Ljcn;

    invoke-virtual {v0, p1, v1, v12, v12}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v0

    iput-object v0, v7, Lanj;->ahM:Laoi;

    :cond_1c
    move-object v3, v6

    goto/16 :goto_0

    :cond_1d
    move-object v5, v3

    goto :goto_7

    :cond_1e
    move-object v4, v3

    goto :goto_6

    :cond_1f
    move-object v2, v3

    goto :goto_5

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x51 -> :sswitch_2
        0x53 -> :sswitch_1
        0x64 -> :sswitch_3
        0x80 -> :sswitch_0
    .end sparse-switch
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 5

    .prologue
    const/16 v4, 0xd5

    .line 343
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-static {v0, v4, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 345
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Liwk;->aZp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Liwk;->afA:Ljbj;

    if-nez v0, :cond_1

    .line 348
    :cond_0
    const/4 v0, 0x0

    .line 364
    :goto_0
    return-object v0

    .line 352
    :cond_1
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    .line 353
    invoke-virtual {v1}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 355
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 356
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    .line 357
    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 358
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 360
    new-instance v2, Lfsa;

    invoke-direct {v2, v4}, Lfsa;-><init>(I)V

    const v3, 0x7f0200ed

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v4, p0, Lfwi;->cCJ:Ljeg;

    iget-object v4, v4, Ljeg;->ajE:Ljei;

    invoke-virtual {v2, v3, v1, v4}, Lfsa;->a(Lizq;Liwk;Ljei;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    goto :goto_0
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 317
    iget-object v1, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v1}, Ljeg;->biz()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v1}, Ljeg;->bix()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v1}, Ljeg;->pf()Z

    move-result v1

    if-nez v1, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v1}, Ljeg;->biz()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v0}, Ljeg;->biy()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aX(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v0}, Ljeg;->bix()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v0}, Ljeg;->biw()Ljava/lang/String;

    move-result-object v0

    .line 335
    :goto_0
    return-object v0

    .line 332
    :cond_0
    iget-object v0, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v0}, Ljeg;->pf()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lfwi;->cCJ:Ljeg;

    invoke-virtual {v0}, Ljeg;->oo()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 335
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/util/List;)Lanh;
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Landroid/content/Context;Ljef;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 397
    invoke-virtual {p2}, Ljef;->bir()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 398
    const v1, 0x7f0a048a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljef;->biq()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_0
    invoke-virtual {p2}, Ljef;->bip()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 401
    invoke-virtual {p2}, Ljef;->bio()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 403
    :cond_1
    invoke-virtual {p2}, Ljef;->bao()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 404
    invoke-virtual {p2}, Ljef;->ban()J

    move-result-wide v2

    invoke-direct {p0, p1, v2, v3}, Lfwi;->f(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    :cond_2
    const-string v1, " \u00b7 "

    invoke-static {v1, v0}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
