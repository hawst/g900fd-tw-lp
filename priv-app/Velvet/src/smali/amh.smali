.class public final Lamh;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile aeG:[Lamh;


# instance fields
.field private aeH:Ljava/lang/String;

.field private aeI:D

.field private aeJ:D

.field private aeK:F

.field private aeL:J

.field private aeM:Ljava/lang/String;

.field private aeN:Ljava/lang/String;

.field private aez:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 169
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput v0, p0, Lamh;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lamh;->aeH:Ljava/lang/String;

    iput-wide v2, p0, Lamh;->aeI:D

    iput-wide v2, p0, Lamh;->aeJ:D

    const/4 v0, 0x0

    iput v0, p0, Lamh;->aeK:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamh;->aeL:J

    const-string v0, ""

    iput-object v0, p0, Lamh;->aeM:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lamh;->aeN:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lamh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamh;->eCz:I

    .line 171
    return-void
.end method

.method public static mQ()[Lamh;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lamh;->aeG:[Lamh;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lamh;->aeG:[Lamh;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lamh;

    sput-object v0, Lamh;->aeG:[Lamh;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lamh;->aeG:[Lamh;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Q(Ljava/lang/String;)Lamh;
    .locals 1

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Lamh;->aeH:Ljava/lang/String;

    .line 37
    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamh;->aez:I

    .line 38
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamh;->aeH:Ljava/lang/String;

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lamh;->aeI:D

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lamh;->aeJ:D

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lamh;->aeK:F

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamh;->aeL:J

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamh;->aeM:Ljava/lang/String;

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamh;->aeN:Ljava/lang/String;

    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lamh;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 190
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x1

    iget-object v1, p0, Lamh;->aeH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 193
    :cond_0
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 194
    const/4 v0, 0x2

    iget-wide v2, p0, Lamh;->aeI:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 196
    :cond_1
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 197
    const/4 v0, 0x3

    iget-wide v2, p0, Lamh;->aeJ:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 199
    :cond_2
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 200
    const/4 v0, 0x4

    iget v1, p0, Lamh;->aeK:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 202
    :cond_3
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 203
    const/4 v0, 0x5

    iget-wide v2, p0, Lamh;->aeL:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 205
    :cond_4
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 206
    const/4 v0, 0x6

    iget-object v1, p0, Lamh;->aeM:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 208
    :cond_5
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 209
    const/4 v0, 0x7

    iget-object v1, p0, Lamh;->aeN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 211
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 212
    return-void
.end method

.method public final b(D)Lamh;
    .locals 1

    .prologue
    .line 55
    iput-wide p1, p0, Lamh;->aeI:D

    .line 56
    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamh;->aez:I

    .line 57
    return-object p0
.end method

.method public final c(D)Lamh;
    .locals 1

    .prologue
    .line 74
    iput-wide p1, p0, Lamh;->aeJ:D

    .line 75
    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamh;->aez:I

    .line 76
    return-object p0
.end method

.method public final getProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lamh;->aeH:Ljava/lang/String;

    return-object v0
.end method

.method public final getTimestampMillis()J
    .locals 2

    .prologue
    .line 109
    iget-wide v0, p0, Lamh;->aeL:J

    return-wide v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 216
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 217
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 218
    const/4 v1, 0x1

    iget-object v2, p0, Lamh;->aeH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_0
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 222
    const/4 v1, 0x2

    iget-wide v2, p0, Lamh;->aeI:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 225
    :cond_1
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 226
    const/4 v1, 0x3

    iget-wide v2, p0, Lamh;->aeJ:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 229
    :cond_2
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 230
    const/4 v1, 0x4

    iget v2, p0, Lamh;->aeK:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 233
    :cond_3
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 234
    const/4 v1, 0x5

    iget-wide v2, p0, Lamh;->aeL:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_4
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 238
    const/4 v1, 0x6

    iget-object v2, p0, Lamh;->aeM:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_5
    iget v1, p0, Lamh;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 242
    const/4 v1, 0x7

    iget-object v2, p0, Lamh;->aeN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_6
    return v0
.end method

.method public final mR()D
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lamh;->aeI:D

    return-wide v0
.end method

.method public final mS()D
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lamh;->aeJ:D

    return-wide v0
.end method

.method public final mT()F
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lamh;->aeK:F

    return v0
.end method

.method public final mU()Z
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lamh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p(J)Lamh;
    .locals 1

    .prologue
    .line 112
    iput-wide p1, p0, Lamh;->aeL:J

    .line 113
    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamh;->aez:I

    .line 114
    return-object p0
.end method

.method public final w(F)Lamh;
    .locals 1

    .prologue
    .line 93
    iput p1, p0, Lamh;->aeK:F

    .line 94
    iget v0, p0, Lamh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamh;->aez:I

    .line 95
    return-object p0
.end method
