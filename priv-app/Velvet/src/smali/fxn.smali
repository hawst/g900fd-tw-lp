.class public final Lfxn;
.super Lfuz;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgan;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 31
    iput-object p4, p0, Lfxn;->mFifeImageUrlUtil:Lgan;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 7

    .prologue
    const v6, 0x7f0d0169

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 37
    new-instance v1, Lapo;

    invoke-direct {v1}, Lapo;-><init>()V

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iget-object v3, v2, Lizj;->dTs:Ljig;

    invoke-virtual {v3}, Ljig;->pb()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljig;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lapo;->dq(Ljava/lang/String;)Lapo;

    :cond_0
    invoke-virtual {v3}, Ljig;->bnb()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Ljig;->bfA()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lapo;->dr(Ljava/lang/String;)Lapo;

    :cond_1
    iget-object v4, v3, Ljig;->aiS:Ljcn;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lfxn;->mFifeImageUrlUtil:Lgan;

    iget-object v5, v3, Ljig;->aiS:Ljcn;

    invoke-virtual {v4, p1, v5, v6, v6}, Lgan;->a(Landroid/content/Context;Ljcn;II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lapo;->ds(Ljava/lang/String;)Lapo;

    :cond_2
    invoke-virtual {v3}, Ljig;->pM()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Ljig;->pL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lapo;->dt(Ljava/lang/String;)Lapo;

    :cond_3
    invoke-virtual {v3}, Ljig;->td()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Ljig;->tc()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lapo;->du(Ljava/lang/String;)Lapo;

    :cond_4
    iget-object v4, v3, Ljig;->enn:[Ljbm;

    if-eqz v4, :cond_5

    iget-object v4, v3, Ljig;->enn:[Ljbm;

    iput-object v4, v1, Lapo;->aly:[Ljbm;

    :cond_5
    invoke-virtual {v3}, Ljig;->bnd()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v3}, Ljig;->bnc()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lapo;->dv(Ljava/lang/String;)Lapo;

    :cond_6
    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    const/16 v5, 0x24

    invoke-virtual {v4, v5}, Lanh;->cm(I)Lanh;

    iput-object v1, v4, Lanh;->aha:Lapo;

    iput-object v2, v4, Lanh;->ahu:Lizj;

    iget-object v1, v3, Ljig;->ahD:Lixx;

    if-eqz v1, :cond_7

    new-instance v1, Lfsa;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    iget-object v2, v3, Ljig;->ahD:Lixx;

    invoke-virtual {v1, v2}, Lfsa;->b(Lixx;)Lani;

    move-result-object v1

    iput-object v1, v4, Lanh;->ahs:Lani;

    :cond_7
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 40
    iget-object v2, v1, Lang;->ags:[Lanh;

    invoke-static {v2, v0}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 41
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 42
    return-object v1
.end method
