.class public final Ldqf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leax;


# instance fields
.field final aPu:Lenz;

.field private bFk:Lfmq;

.field private final bFl:Lesk;

.field private final bFm:Lesk;

.field final mNowRemoteClient:Lfml;


# direct methods
.method public constructor <init>(Lfml;Lenz;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ldqg;

    const-string v1, "RecordPromoClicked"

    invoke-direct {v0, p0, v1}, Ldqg;-><init>(Ldqf;Ljava/lang/String;)V

    iput-object v0, p0, Ldqf;->bFl:Lesk;

    .line 41
    new-instance v0, Ldqh;

    const-string v1, "RecordPromoDismissed"

    invoke-direct {v0, p0, v1}, Ldqh;-><init>(Ldqf;Ljava/lang/String;)V

    iput-object v0, p0, Ldqf;->bFm:Lesk;

    .line 51
    iput-object p1, p0, Ldqf;->mNowRemoteClient:Lfml;

    .line 52
    iput-object p2, p0, Ldqf;->aPu:Lenz;

    .line 53
    return-void
.end method

.method private c(Lesk;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Ldqf;->bFk:Lfmq;

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Ldqf;->mNowRemoteClient:Lfml;

    const-string v1, "GoogleNowPromoControllerImpl"

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Ldqf;->bFk:Lfmq;

    .line 73
    iget-object v0, p0, Ldqf;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 75
    :cond_0
    iget-object v0, p0, Ldqf;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    invoke-interface {p1}, Lesk;->run()V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Ldqf;->mNowRemoteClient:Lfml;

    invoke-virtual {v0, p1}, Lfml;->d(Lesk;)V

    goto :goto_0
.end method


# virtual methods
.method public final aeE()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Ldqf;->aPu:Lenz;

    const-string v2, "GSAPrefs.now_promo_dismissed"

    invoke-virtual {v1, v2, v0}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldqf;->aPu:Lenz;

    const-string v2, "GEL.GSAPrefs.now_enabled"

    invoke-virtual {v1, v2, v0}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldqf;->aPu:Lenz;

    const-string v2, "GEL.GSAPrefs.can_optin_to_now"

    invoke-virtual {v1, v2, v0}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final aeF()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Ldqf;->bFl:Lesk;

    invoke-direct {p0, v0}, Ldqf;->c(Lesk;)V

    .line 63
    return-void
.end method

.method public final aeG()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldqf;->bFm:Lesk;

    invoke-direct {p0, v0}, Ldqf;->c(Lesk;)V

    .line 67
    return-void
.end method

.method final aeH()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Ldqf;->mNowRemoteClient:Lfml;

    iget-object v1, p0, Ldqf;->bFl:Lesk;

    invoke-virtual {v0, v1}, Lfml;->e(Lesk;)V

    .line 137
    iget-object v0, p0, Ldqf;->mNowRemoteClient:Lfml;

    iget-object v1, p0, Ldqf;->bFm:Lesk;

    invoke-virtual {v0, v1}, Lfml;->e(Lesk;)V

    .line 138
    iget-object v0, p0, Ldqf;->bFk:Lfmq;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Ldqf;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Ldqf;->bFk:Lfmq;

    .line 142
    :cond_0
    return-void
.end method
