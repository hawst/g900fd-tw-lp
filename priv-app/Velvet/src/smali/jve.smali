.class public final Ljve;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFY:[Ljve;


# instance fields
.field private aez:I

.field private dzl:F

.field private eFZ:Ljava/lang/String;

.field private eGa:Ljava/lang/String;

.field private eGb:Z

.field private eGc:F

.field private eGd:F

.field private eGe:F

.field private eGf:Ljuz;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3239
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3240
    iput v2, p0, Ljve;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljve;->eFZ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljve;->eGa:Ljava/lang/String;

    iput-boolean v2, p0, Ljve;->eGb:Z

    iput v1, p0, Ljve;->dzl:F

    iput v1, p0, Ljve;->eGc:F

    iput v1, p0, Ljve;->eGd:F

    iput v1, p0, Ljve;->eGe:F

    iput-object v3, p0, Ljve;->eGf:Ljuz;

    iput-object v3, p0, Ljve;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljve;->eCz:I

    .line 3241
    return-void
.end method

.method public static buU()[Ljve;
    .locals 2

    .prologue
    .line 3084
    sget-object v0, Ljve;->eFY:[Ljve;

    if-nez v0, :cond_1

    .line 3085
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3087
    :try_start_0
    sget-object v0, Ljve;->eFY:[Ljve;

    if-nez v0, :cond_0

    .line 3088
    const/4 v0, 0x0

    new-array v0, v0, [Ljve;

    sput-object v0, Ljve;->eFY:[Ljve;

    .line 3090
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3092
    :cond_1
    sget-object v0, Ljve;->eFY:[Ljve;

    return-object v0

    .line 3090
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljve;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljve;->eFZ:Ljava/lang/String;

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljve;->eGa:Ljava/lang/String;

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljve;->dzl:F

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljve;->eGc:F

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljve;->eGd:F

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljve;->eGe:F

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljve;->eGb:Z

    iget v0, p0, Ljve;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljve;->aez:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljve;->eGf:Ljuz;

    if-nez v0, :cond_1

    new-instance v0, Ljuz;

    invoke-direct {v0}, Ljuz;-><init>()V

    iput-object v0, p0, Ljve;->eGf:Ljuz;

    :cond_1
    iget-object v0, p0, Ljve;->eGf:Ljuz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3261
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3262
    const/4 v0, 0x1

    iget-object v1, p0, Ljve;->eFZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3264
    :cond_0
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3265
    const/4 v0, 0x2

    iget-object v1, p0, Ljve;->eGa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3267
    :cond_1
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 3268
    const/4 v0, 0x3

    iget v1, p0, Ljve;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 3270
    :cond_2
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 3271
    const/4 v0, 0x4

    iget v1, p0, Ljve;->eGc:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 3273
    :cond_3
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 3274
    const/4 v0, 0x5

    iget v1, p0, Ljve;->eGd:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 3276
    :cond_4
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 3277
    const/4 v0, 0x6

    iget v1, p0, Ljve;->eGe:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 3279
    :cond_5
    iget v0, p0, Ljve;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 3280
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljve;->eGb:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 3282
    :cond_6
    iget-object v0, p0, Ljve;->eGf:Ljuz;

    if-eqz v0, :cond_7

    .line 3283
    const/16 v0, 0x8

    iget-object v1, p0, Ljve;->eGf:Ljuz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3285
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3286
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3290
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3291
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3292
    const/4 v1, 0x1

    iget-object v2, p0, Ljve;->eFZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3295
    :cond_0
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3296
    const/4 v1, 0x2

    iget-object v2, p0, Ljve;->eGa:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3299
    :cond_1
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 3300
    const/4 v1, 0x3

    iget v2, p0, Ljve;->dzl:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3303
    :cond_2
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 3304
    const/4 v1, 0x4

    iget v2, p0, Ljve;->eGc:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3307
    :cond_3
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 3308
    const/4 v1, 0x5

    iget v2, p0, Ljve;->eGd:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3311
    :cond_4
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 3312
    const/4 v1, 0x6

    iget v2, p0, Ljve;->eGe:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3315
    :cond_5
    iget v1, p0, Ljve;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 3316
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljve;->eGb:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3319
    :cond_6
    iget-object v1, p0, Ljve;->eGf:Ljuz;

    if-eqz v1, :cond_7

    .line 3320
    const/16 v1, 0x8

    iget-object v2, p0, Ljve;->eGf:Ljuz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3323
    :cond_7
    return v0
.end method
