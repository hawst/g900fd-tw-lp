.class public final Ljoe;
.super Ljsl;
.source "PG"


# static fields
.field public static final ewe:Ljsm;


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private ewf:Ljava/lang/String;

.field public ewg:[Ljof;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/16 v0, 0xb

    const-class v1, Ljoe;

    const v2, 0x1fd66cc2

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljoe;->ewe:Ljsm;

    .line 391
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 445
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 446
    const/4 v0, 0x0

    iput v0, p0, Ljoe;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljoe;->ajB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljoe;->ewf:Ljava/lang/String;

    invoke-static {}, Ljof;->bre()[Ljof;

    move-result-object v0

    iput-object v0, p0, Ljoe;->ewg:[Ljof;

    const/4 v0, 0x0

    iput-object v0, p0, Ljoe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljoe;->eCz:I

    .line 447
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljoe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoe;->ajB:Ljava/lang/String;

    iget v0, p0, Ljoe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoe;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoe;->ewf:Ljava/lang/String;

    iget v0, p0, Ljoe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoe;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoe;->ewg:[Ljof;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljof;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljoe;->ewg:[Ljof;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljof;

    invoke-direct {v3}, Ljof;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljoe;->ewg:[Ljof;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljof;

    invoke-direct {v3}, Ljof;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoe;->ewg:[Ljof;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 462
    iget v0, p0, Ljoe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 463
    const/4 v0, 0x1

    iget-object v1, p0, Ljoe;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 465
    :cond_0
    iget v0, p0, Ljoe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 466
    const/4 v0, 0x5

    iget-object v1, p0, Ljoe;->ewf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 468
    :cond_1
    iget-object v0, p0, Ljoe;->ewg:[Ljof;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljoe;->ewg:[Ljof;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 469
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljoe;->ewg:[Ljof;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 470
    iget-object v1, p0, Ljoe;->ewg:[Ljof;

    aget-object v1, v1, v0

    .line 471
    if-eqz v1, :cond_2

    .line 472
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 469
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 476
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 477
    return-void
.end method

.method public final bhT()Z
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Ljoe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Ljoe;->ewf:Ljava/lang/String;

    return-object v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Ljoe;->ajB:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 481
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 482
    iget v1, p0, Ljoe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 483
    const/4 v1, 0x1

    iget-object v2, p0, Ljoe;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 486
    :cond_0
    iget v1, p0, Ljoe;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 487
    const/4 v1, 0x5

    iget-object v2, p0, Ljoe;->ewf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    :cond_1
    iget-object v1, p0, Ljoe;->ewg:[Ljof;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljoe;->ewg:[Ljof;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 491
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljoe;->ewg:[Ljof;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 492
    iget-object v2, p0, Ljoe;->ewg:[Ljof;

    aget-object v2, v2, v0

    .line 493
    if-eqz v2, :cond_2

    .line 494
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 491
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 499
    :cond_4
    return v0
.end method

.method public final xx(Ljava/lang/String;)Ljoe;
    .locals 1

    .prologue
    .line 404
    if-nez p1, :cond_0

    .line 405
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 407
    :cond_0
    iput-object p1, p0, Ljoe;->ajB:Ljava/lang/String;

    .line 408
    iget v0, p0, Ljoe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoe;->aez:I

    .line 409
    return-object p0
.end method

.method public final xy(Ljava/lang/String;)Ljoe;
    .locals 1

    .prologue
    .line 426
    if-nez p1, :cond_0

    .line 427
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 429
    :cond_0
    iput-object p1, p0, Ljoe;->ewf:Ljava/lang/String;

    .line 430
    iget v0, p0, Ljoe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoe;->aez:I

    .line 431
    return-object p0
.end method
