.class public final Ljha;
.super Ljsl;
.source "PG"


# instance fields
.field public elt:Ljhc;

.field public elu:[Ljhb;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput-object v1, p0, Ljha;->elt:Ljhc;

    invoke-static {}, Ljhb;->blw()[Ljhb;

    move-result-object v0

    iput-object v0, p0, Ljha;->elu:[Ljhb;

    iput-object v1, p0, Ljha;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljha;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljha;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljha;->elt:Ljhc;

    if-nez v0, :cond_1

    new-instance v0, Ljhc;

    invoke-direct {v0}, Ljhc;-><init>()V

    iput-object v0, p0, Ljha;->elt:Ljhc;

    :cond_1
    iget-object v0, p0, Ljha;->elt:Ljhc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljha;->elu:[Ljhb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhb;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljha;->elu:[Ljhb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljhb;

    invoke-direct {v3}, Ljhb;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljha;->elu:[Ljhb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljhb;

    invoke-direct {v3}, Ljhb;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljha;->elu:[Ljhb;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget-object v0, p0, Ljha;->elt:Ljhc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljha;->elt:Ljhc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_0
    iget-object v0, p0, Ljha;->elu:[Ljhb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljha;->elu:[Ljhb;

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljha;->elu:[Ljhb;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ljha;->elu:[Ljhb;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 5

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget-object v1, p0, Ljha;->elt:Ljhc;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljha;->elt:Ljhc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljha;->elu:[Ljhb;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljha;->elu:[Ljhb;

    array-length v1, v1

    if-lez v1, :cond_3

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljha;->elu:[Ljhb;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ljha;->elu:[Ljhb;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    :cond_3
    return v0
.end method
