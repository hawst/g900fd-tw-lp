.class public abstract Lisa;
.super Liry;
.source "PG"


# instance fields
.field private final dGO:Ljava/nio/ByteBuffer;

.field private final dGP:I

.field private final dGQ:I


# direct methods
.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p1}, Lisa;-><init>(II)V

    .line 88
    return-void
.end method

.method private constructor <init>(II)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Liry;-><init>()V

    .line 101
    rem-int v0, p2, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 104
    add-int/lit8 v0, p2, 0x7

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    .line 107
    iput p2, p0, Lisa;->dGP:I

    .line 108
    iput p1, p0, Lisa;->dGQ:I

    .line 109
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aYo()V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 235
    invoke-direct {p0}, Lisa;->aYp()V

    .line 237
    :cond_0
    return-void
.end method

.method private aYp()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 241
    :goto_0
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v1, p0, Lisa;->dGQ:I

    if-lt v0, v1, :cond_0

    .line 244
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Lisa;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 246
    :cond_0
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 247
    return-void
.end method


# virtual methods
.method public final Q(Ljava/lang/CharSequence;)Lisg;
    .locals 3

    .prologue
    .line 172
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 173
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iget-object v2, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lisa;->aYo()V

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_0
    return-object p0
.end method

.method protected abstract a(Ljava/nio/ByteBuffer;)V
.end method

.method public final aYm()Lisb;
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0}, Lisa;->aYp()V

    .line 222
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 223
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_0

    .line 224
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Lisa;->b(Ljava/nio/ByteBuffer;)V

    .line 226
    :cond_0
    invoke-virtual {p0}, Lisa;->aYn()Lisb;

    move-result-object v0

    return-object v0
.end method

.method abstract aYn()Lisb;
.end method

.method public final aa([B)Lisg;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 137
    array-length v1, p1

    invoke-static {p1, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    iget-object v3, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-gt v2, v3, :cond_0

    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lisa;->aYo()V

    :goto_0
    return-object p0

    :cond_0
    iget v2, p0, Lisa;->dGP:I

    iget-object v3, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    sub-int/2addr v2, v3

    :goto_1
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lisa;->aYp()V

    :goto_2
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget v2, p0, Lisa;->dGQ:I

    if-lt v0, v2, :cond_2

    invoke-virtual {p0, v1}, Lisa;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lisa;->dGO:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method protected b(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 126
    iget v0, p0, Lisa;->dGQ:I

    add-int/lit8 v0, v0, 0x7

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 127
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget v1, p0, Lisa;->dGQ:I

    if-ge v0, v1, :cond_0

    .line 128
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 130
    :cond_0
    iget v0, p0, Lisa;->dGQ:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 131
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 132
    invoke-virtual {p0, p1}, Lisa;->a(Ljava/nio/ByteBuffer;)V

    .line 133
    return-void
.end method
