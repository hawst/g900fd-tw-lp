.class public final Ldkv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bAX:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f020300

    sput v0, Ldkv;->bAX:I

    return-void
.end method

.method public static jM(Ljava/lang/String;)Landroid/content/Intent;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 46
    new-instance v6, Landroid/util/JsonReader;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v1}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 49
    :try_start_0
    const-string v1, ""

    .line 51
    invoke-virtual {v6}, Landroid/util/JsonReader;->beginObject()V

    move v3, v2

    move v4, v2

    move v5, v2

    .line 52
    :goto_0
    invoke-virtual {v6}, Landroid/util/JsonReader;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 53
    invoke-virtual {v6}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    .line 54
    const-string v8, "x"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 55
    invoke-virtual {v6}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    goto :goto_0

    .line 56
    :cond_0
    const-string v8, "y"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 57
    invoke-virtual {v6}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    goto :goto_0

    .line 58
    :cond_1
    const-string v8, "w"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 59
    invoke-virtual {v6}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    goto :goto_0

    .line 60
    :cond_2
    const-string v8, "h"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 61
    invoke-virtual {v6}, Landroid/util/JsonReader;->nextInt()I

    move-result v2

    goto :goto_0

    .line 62
    :cond_3
    const-string v8, "url"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 63
    invoke-virtual {v6}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 65
    :cond_4
    invoke-virtual {v6}, Landroid/util/JsonReader;->skipValue()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v1

    .line 97
    :try_start_1
    const-string v2, "GmmIntentUtils"

    const-string v3, "createIntentFromJson: Could not parse json."

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    .line 103
    :goto_1
    return-object v0

    .line 68
    :cond_5
    :try_start_2
    invoke-virtual {v6}, Landroid/util/JsonReader;->endObject()V

    .line 70
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    .line 72
    const/4 v7, 0x1

    invoke-static {v1, v7}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 75
    const-string v7, "com.google.android.apps.maps"

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 76
    const-string v2, "GmmIntentUtils"

    const-string v3, "createIntentFromJson: Unexpected package in intent %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    .line 80
    :cond_6
    if-ltz v5, :cond_7

    if-ltz v4, :cond_7

    if-lez v3, :cond_7

    if-lez v2, :cond_7

    .line 82
    :try_start_3
    const-string v7, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_USE_HERO_TRANSITION"

    const/4 v8, 0x1

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    const-string v7, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_COMPANION_VIEW_RES_ID"

    sget v8, Ldkv;->bAX:I

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    const-string v7, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_HERO_WEBVIEW_RECT"

    new-instance v8, Landroid/graphics/Rect;

    add-int/2addr v3, v5

    add-int/2addr v2, v4

    invoke-direct {v8, v5, v4, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 85
    const-string v2, "fade_in_map_from_transparent"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    :cond_7
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_1

    .line 94
    :cond_8
    :try_start_4
    const-string v1, "GmmIntentUtils"

    const-string v2, "createIntentFromJson: url was empty"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 95
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    .line 99
    :catch_1
    move-exception v1

    .line 100
    :try_start_5
    const-string v2, "GmmIntentUtils"

    const-string v3, "createIntentFromJson: Could not parse intent url."

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 101
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
.end method
