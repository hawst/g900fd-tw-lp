.class public Lgoj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cRq:Ljava/util/Set;

.field private final cRr:Lgoj;

.field private final cRs:Lgoi;


# direct methods
.method public constructor <init>(Lgoj;Lgoi;)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgoj;->cRq:Ljava/util/Set;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lgoj;->cRr:Lgoj;

    .line 41
    iput-object p2, p0, Lgoj;->cRs:Lgoi;

    .line 42
    if-eqz p2, :cond_1

    .line 43
    iget-boolean v0, p2, Lgoi;->mClosed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Adding dependent scope to a closed jar: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lgoi;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p2, Lgoi;->cRp:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_1
    return-void
.end method


# virtual methods
.method public final aIs()Lgoi;
    .locals 1

    .prologue
    .line 88
    :goto_0
    iget-object v0, p0, Lgoj;->cRs:Lgoi;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lgoj;->cRs:Lgoi;

    return-object v0

    .line 91
    :cond_0
    iget-object p0, p0, Lgoj;->cRr:Lgoj;

    goto :goto_0
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lgoj;->cRs:Lgoi;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lgoj;->cRs:Lgoi;

    iget-object v1, v0, Lgoi;->cRp:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, v0, Lgoi;->cRp:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lgoi;->close()V

    .line 105
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lgoj;->cRq:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoj;

    invoke-virtual {v0}, Lgoj;->destroy()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgoj;->cRq:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 106
    iget-object v0, p0, Lgoj;->cRr:Lgoj;

    .line 107
    if-eqz v0, :cond_2

    .line 108
    iget-object v0, v0, Lgoj;->cRq:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 110
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{jar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lgoj;->cRs:Lgoi;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
