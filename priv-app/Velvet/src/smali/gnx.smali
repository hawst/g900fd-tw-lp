.class public final Lgnx;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile cQM:[Lgnx;


# instance fields
.field private aez:I

.field public cQN:[Lgnw;

.field private cQx:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 622
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 623
    const/4 v0, 0x0

    iput v0, p0, Lgnx;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgnx;->cQx:J

    invoke-static {}, Lgnw;->aId()[Lgnw;

    move-result-object v0

    iput-object v0, p0, Lgnx;->cQN:[Lgnw;

    const/4 v0, 0x0

    iput-object v0, p0, Lgnx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lgnx;->eCz:I

    .line 624
    return-void
.end method

.method public static aIi()[Lgnx;
    .locals 2

    .prologue
    .line 587
    sget-object v0, Lgnx;->cQM:[Lgnx;

    if-nez v0, :cond_1

    .line 588
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 590
    :try_start_0
    sget-object v0, Lgnx;->cQM:[Lgnx;

    if-nez v0, :cond_0

    .line 591
    const/4 v0, 0x0

    new-array v0, v0, [Lgnx;

    sput-object v0, Lgnx;->cQM:[Lgnx;

    .line 593
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 595
    :cond_1
    sget-object v0, Lgnx;->cQM:[Lgnx;

    return-object v0

    .line 593
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 581
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lgnx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lgnx;->cQx:J

    iget v0, p0, Lgnx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgnx;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lgnx;->cQN:[Lgnw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lgnx;->cQN:[Lgnw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lgnw;

    invoke-direct {v3}, Lgnw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lgnx;->cQN:[Lgnw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lgnw;

    invoke-direct {v3}, Lgnw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lgnx;->cQN:[Lgnw;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 638
    iget v0, p0, Lgnx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 639
    const/4 v0, 0x1

    iget-wide v2, p0, Lgnx;->cQx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 641
    :cond_0
    iget-object v0, p0, Lgnx;->cQN:[Lgnw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgnx;->cQN:[Lgnw;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 642
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgnx;->cQN:[Lgnw;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 643
    iget-object v1, p0, Lgnx;->cQN:[Lgnw;

    aget-object v1, v1, v0

    .line 644
    if-eqz v1, :cond_1

    .line 645
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 642
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 649
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 650
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 654
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 655
    iget v1, p0, Lgnx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 656
    const/4 v1, 0x1

    iget-wide v2, p0, Lgnx;->cQx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 659
    :cond_0
    iget-object v1, p0, Lgnx;->cQN:[Lgnw;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgnx;->cQN:[Lgnw;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 660
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgnx;->cQN:[Lgnw;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 661
    iget-object v2, p0, Lgnx;->cQN:[Lgnw;

    aget-object v2, v2, v0

    .line 662
    if-eqz v2, :cond_1

    .line 663
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 660
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 668
    :cond_3
    return v0
.end method
