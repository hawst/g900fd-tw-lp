.class final Lcni;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhgm;


# instance fields
.field private final bcP:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcni;->bcP:Ljava/util/List;

    .line 113
    return-void
.end method

.method static synthetic a(Lcni;)Lisy;
    .locals 3

    .prologue
    .line 108
    new-instance v0, Lisy;

    invoke-direct {v0}, Lisy;-><init>()V

    iget-object v1, p0, Lcni;->bcP:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lisx;

    iput-object v1, v0, Lisy;->dHi:[Lisx;

    iget-object v1, p0, Lcni;->bcP:Ljava/util/List;

    iget-object v2, v0, Lisy;->dHi:[Lisx;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public final h(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 118
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 119
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_3

    move v1, v2

    .line 120
    :goto_0
    iget-object v0, p0, Lcni;->bcP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisx;

    invoke-virtual {v0}, Lisx;->aYx()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lisx;->yM()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 121
    :goto_1
    invoke-virtual {v0}, Lisx;->aYy()Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    move v3, v2

    :cond_2
    invoke-virtual {v0, v3}, Lisx;->gZ(Z)Lisx;

    .line 122
    invoke-virtual {v0}, Lisx;->aYz()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lisx;->bT(J)Lisx;

    .line 123
    return-void

    :cond_3
    move v1, v3

    .line 119
    goto :goto_0

    .line 120
    :cond_4
    new-instance v0, Lisx;

    invoke-direct {v0}, Lisx;-><init>()V

    invoke-virtual {v0, v4}, Lisx;->pw(Ljava/lang/String;)Lisx;

    move-result-object v0

    invoke-virtual {v0, v5}, Lisx;->px(Ljava/lang/String;)Lisx;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lisx;->bT(J)Lisx;

    move-result-object v0

    invoke-virtual {v0, v3}, Lisx;->gZ(Z)Lisx;

    move-result-object v0

    iget-object v4, p0, Lcni;->bcP:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
