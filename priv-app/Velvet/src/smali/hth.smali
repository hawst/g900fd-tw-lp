.class final Lhth;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dqz:Lhti;


# direct methods
.method public constructor <init>(Lhti;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lhth;->dqz:Lhti;

    .line 22
    return-void
.end method


# virtual methods
.method public final h(Ljvv;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 25
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v3

    if-ne v3, v1, :cond_5

    iget-object v3, p1, Ljvv;->eIm:Ljvw;

    if-eqz v3, :cond_5

    iget-object v3, p1, Ljvv;->eIm:Ljvw;

    iget-object v5, v3, Ljvw;->eIk:[Ljvs;

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v3, v5, v4

    iget-object v7, v3, Ljvs;->eHS:Ljvy;

    if-eqz v7, :cond_4

    iget-object v3, v3, Ljvs;->eHS:Ljvy;

    iget-object v7, v3, Ljvy;->eIu:[Lkmq;

    array-length v7, v7

    if-nez v7, :cond_1

    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_4

    :goto_2
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 26
    :cond_0
    array-length v4, v0

    move v3, v2

    :goto_3
    if-ge v3, v4, :cond_9

    aget-object v5, v0, v3

    .line 27
    const-string v6, "_cancel"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 28
    iget-object v0, p0, Lhth;->dqz:Lhti;

    invoke-interface {v0}, Lhti;->onCancel()V

    move v0, v1

    .line 45
    :goto_4
    return v0

    .line 25
    :cond_1
    iget-object v3, v3, Ljvy;->eIu:[Lkmq;

    aget-object v3, v3, v2

    iget-object v7, v3, Lkmq;->faK:[Lkmr;

    array-length v7, v7

    if-nez v7, :cond_2

    move-object v3, v0

    goto :goto_1

    :cond_2
    iget-object v7, v3, Lkmq;->faK:[Lkmr;

    aget-object v7, v7, v2

    iget-object v7, v7, Lkmr;->value:Ljava/lang/String;

    if-nez v7, :cond_3

    move-object v3, v0

    goto :goto_1

    :cond_3
    iget-object v3, v3, Lkmq;->faK:[Lkmr;

    aget-object v3, v3, v2

    iget-object v3, v3, Lkmr;->value:Ljava/lang/String;

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_5
    move-object v3, v0

    goto :goto_2

    .line 32
    :cond_6
    const-string v6, "_okay"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 33
    iget-object v0, p0, Lhth;->dqz:Lhti;

    invoke-interface {v0}, Lhti;->aRS()V

    move v0, v1

    .line 34
    goto :goto_4

    .line 37
    :cond_7
    const-string v6, "_select"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 39
    const/16 v6, 0x8

    :try_start_0
    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 41
    iget-object v6, p0, Lhth;->dqz:Lhti;

    invoke-interface {v6, v5}, Lhti;->lv(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :cond_8
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_9
    move v0, v2

    .line 45
    goto :goto_4

    :catch_0
    move-exception v5

    goto :goto_5
.end method
