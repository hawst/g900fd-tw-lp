.class public final Lfwo;
.super Lfuz;
.source "PG"


# static fields
.field private static cCM:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfwo;->cCM:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2d
        0x5a
        0x69
        0x78
    .end array-data
.end method

.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 70
    return-void
.end method

.method private static a(Ljax;II)Lanv;
    .locals 1

    .prologue
    .line 639
    new-instance v0, Lanv;

    invoke-direct {v0}, Lanv;-><init>()V

    invoke-virtual {v0, p1}, Lanv;->cx(I)Lanv;

    move-result-object v0

    invoke-virtual {v0, p2}, Lanv;->cy(I)Lanv;

    move-result-object v0

    .line 642
    iput-object p0, v0, Lanv;->aiG:Ljax;

    .line 643
    return-object v0
.end method

.method public static a(Ljgn;Z)Laoq;
    .locals 2

    .prologue
    .line 587
    new-instance v0, Laoq;

    invoke-direct {v0}, Laoq;-><init>()V

    .line 588
    invoke-virtual {p0}, Ljgn;->bkF()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 589
    invoke-virtual {p0}, Ljgn;->bkE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->ch(Ljava/lang/String;)Laoq;

    .line 591
    :cond_0
    invoke-virtual {p0}, Ljgn;->bkB()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Ljgn;->bkD()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 592
    invoke-virtual {p0}, Ljgn;->bkA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->ci(Ljava/lang/String;)Laoq;

    .line 593
    invoke-virtual {p0}, Ljgn;->bkC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->cj(Ljava/lang/String;)Laoq;

    .line 597
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljgn;->bkG()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 598
    invoke-virtual {p0}, Ljgn;->rf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->ck(Ljava/lang/String;)Laoq;

    .line 600
    :cond_2
    return-object v0

    .line 594
    :cond_3
    invoke-virtual {p0}, Ljgn;->oK()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 595
    invoke-virtual {p0}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoq;->cj(Ljava/lang/String;)Laoq;

    goto :goto_0
.end method

.method private static a(Ljgj;)Ljava/lang/Integer;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 117
    iget-object v1, p0, Ljgj;->ejN:[Ljgn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 118
    invoke-virtual {v3}, Ljgn;->bjS()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljgn;->hasBackgroundColor()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 119
    invoke-virtual {v3}, Ljgn;->getBackgroundColor()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 122
    :goto_1
    return-object v0

    .line 117
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;IILjgm;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const v2, 0x7f0a0279

    const v1, 0x7f0a0278

    const/4 v4, 0x0

    const v0, 0x7f0a025a

    const/4 v3, 0x1

    .line 451
    packed-switch p1, :pswitch_data_0

    .line 526
    const v0, 0x7f0a0259

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 455
    :pswitch_0
    iget-object v0, p3, Ljgm;->ekh:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iget-object v0, p3, Ljgm;->ekh:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 459
    const v0, 0x7f0a026b

    .line 465
    :goto_1
    array-length v2, v1

    if-le p2, v2, :cond_2

    .line 466
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 461
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 463
    const v0, 0x7f0a026c

    goto :goto_1

    .line 468
    :cond_2
    add-int/lit8 v0, p2, -0x1

    aget-object v0, v1, v0

    goto :goto_0

    .line 470
    :pswitch_1
    if-eqz p4, :cond_3

    .line 471
    packed-switch p2, :pswitch_data_1

    .line 477
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 473
    :pswitch_2
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 475
    :pswitch_3
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 482
    :cond_3
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    .line 492
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 484
    :pswitch_5
    const v0, 0x7f0a0271

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 486
    :pswitch_6
    const v0, 0x7f0a0272

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 488
    :pswitch_7
    const v0, 0x7f0a0273

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 490
    :pswitch_8
    const v0, 0x7f0a0274

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 495
    :pswitch_9
    packed-switch p2, :pswitch_data_3

    .line 503
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 497
    :pswitch_a
    const v0, 0x7f0a0275

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 499
    :pswitch_b
    const v0, 0x7f0a0276

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 501
    :pswitch_c
    const v0, 0x7f0a0277

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 507
    :pswitch_d
    packed-switch p2, :pswitch_data_4

    .line 513
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 509
    :pswitch_e
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 511
    :pswitch_f
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 516
    :pswitch_10
    packed-switch p2, :pswitch_data_5

    .line 522
    const v0, 0x7f0a0255

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 518
    :pswitch_11
    const v0, 0x7f0a027a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 520
    :pswitch_12
    const v0, 0x7f0a027b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_9
        :pswitch_d
        :pswitch_d
        :pswitch_10
    .end packed-switch

    .line 471
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 482
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 495
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 507
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 516
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;IJ)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 537
    sget-object v0, Lfwo;->cCM:[I

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 538
    const v0, 0x7f0a0253

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 548
    :goto_0
    return-object v0

    .line 540
    :cond_0
    sget-object v0, Lfwo;->cCM:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    .line 541
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    .line 542
    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 544
    const v1, 0x7f0a0252

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    int-to-long v6, v0

    sub-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 548
    :cond_1
    const v0, 0x7f0a0251

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljgj;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p1}, Ljgj;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 312
    invoke-virtual {p1}, Ljgj;->nk()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lfwo;->h(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 305
    :pswitch_0
    const v0, 0x7f0a0269

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 308
    :pswitch_1
    invoke-virtual {p1}, Ljgj;->nk()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lfwo;->g(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljgn;Ljgn;)Ljax;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 833
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 834
    const/4 v2, 0x0

    .line 848
    :cond_0
    return-object v2

    .line 837
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 838
    invoke-static {v0}, Lfwo;->jI(I)Ljax;

    move-result-object v2

    .line 841
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v7

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 842
    add-int/lit8 v8, v1, 0x1

    invoke-static {v0, p0}, Lfwp;->b(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, p2, p0}, Lfwp;->a(ILjgn;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, p3, p0}, Lfwp;->a(ILjgn;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0xb

    if-ne v0, v6, :cond_2

    const/4 v6, 0x1

    :goto_1
    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move v1, v8

    .line 847
    goto :goto_0

    :cond_2
    move v6, v7

    .line 842
    goto :goto_1
.end method

.method private static a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v0, 0x7f0a021a

    .line 858
    if-nez p4, :cond_0

    .line 859
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p4

    .line 861
    :cond_0
    if-nez p5, :cond_1

    .line 862
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p5

    .line 864
    :cond_1
    iget-object v0, p2, Ljax;->dXu:[Ljhg;

    invoke-static {p3, p6}, Lfwo;->y(Ljava/lang/String;Z)Ljhg;

    move-result-object v1

    aput-object v1, v0, p1

    .line 865
    iget-object v0, p2, Ljax;->dXv:[Ljay;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Ljay;->dXy:[Ljhg;

    invoke-static {p4, p6}, Lfwo;->y(Ljava/lang/String;Z)Ljhg;

    move-result-object v1

    aput-object v1, v0, p1

    .line 866
    iget-object v0, p2, Ljax;->dXv:[Ljay;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Ljay;->dXy:[Ljhg;

    invoke-static {p5, p6}, Lfwo;->y(Ljava/lang/String;Z)Ljhg;

    move-result-object v1

    aput-object v1, v0, p1

    .line 867
    return-void
.end method

.method private aDJ()Ljgn;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    iget-object v0, v0, Ljgj;->ejN:[Ljgn;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 155
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    iget-object v0, v0, Ljgj;->ejN:[Ljgn;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 157
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aDK()Ljgn;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 162
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    iget-object v0, v0, Ljgj;->ejN:[Ljgn;

    array-length v0, v0

    if-le v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    iget-object v0, v0, Ljgj;->ejN:[Ljgn;

    aget-object v0, v0, v1

    .line 165
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljgj;)I
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 557
    const/4 v0, -0x1

    .line 558
    iget-object v5, p0, Ljgj;->ejL:[Ljgm;

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_6

    aget-object v1, v5, v4

    .line 559
    invoke-virtual {v1}, Ljgm;->bkv()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v1}, Ljgm;->bku()I

    move-result v7

    if-gtz v7, :cond_1

    :cond_0
    iget-object v7, v1, Ljgm;->ekh:[Ljava/lang/String;

    array-length v8, v7

    move v1, v2

    :goto_1
    if-ge v1, v8, :cond_4

    aget-object v9, v7, v1

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    move v1, v3

    :goto_2
    if-eqz v1, :cond_5

    :cond_1
    move v1, v3

    :goto_3
    if-eqz v1, :cond_2

    .line 560
    add-int/lit8 v0, v0, 0x1

    .line 558
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 559
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3

    .line 563
    :cond_6
    return v0
.end method

.method public static b(Landroid/content/Context;Ljgj;)Ljava/lang/String;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 361
    invoke-virtual {p1}, Ljgj;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 367
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 363
    :pswitch_0
    invoke-static {p0, p1}, Lfwo;->c(Landroid/content/Context;Ljgj;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 365
    :pswitch_1
    invoke-virtual {p1}, Ljgj;->bew()I

    move-result v2

    invoke-static {p1}, Lfwo;->b(Ljgj;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p1, Ljgj;->ejL:[Ljgm;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Ljgj;->nk()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lfwo;->h(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p1, Ljgj;->ejL:[Ljgm;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljgm;->bky()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3}, Ljgm;->bkx()I

    move-result v0

    :cond_2
    invoke-virtual {p1}, Ljgj;->bkk()Z

    move-result v1

    invoke-static {p0, v2, v0, v3, v1}, Lfwo;->a(Landroid/content/Context;IILjgm;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Ljgm;->bkv()Z

    move-result v4

    if-nez v4, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 v4, 0x4

    if-ne v2, v4, :cond_4

    invoke-virtual {v3}, Ljgm;->bku()I

    move-result v2

    int-to-long v2, v2

    invoke-static {p0, v0, v2, v3}, Lfwo;->a(Landroid/content/Context;IJ)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const v2, 0x7f0a026a

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Ljgm;->bku()I

    move-result v0

    int-to-long v2, v0

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;I)Lanh;
    .locals 3

    .prologue
    .line 944
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 945
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 946
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    .line 948
    new-instance v1, Laoj;

    invoke-direct {v1}, Laoj;-><init>()V

    iput-object v1, v0, Lanh;->agz:Laoj;

    .line 949
    iget-object v1, v0, Lanh;->agz:Laoj;

    invoke-virtual {v1, p1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 951
    new-instance v1, Lfsa;

    invoke-direct {v1, p3}, Lfsa;-><init>(I)V

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 955
    return-object v0
.end method

.method public static c(Landroid/content/Context;Ljgj;)Ljava/lang/String;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const v8, 0x7f0a0248

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 377
    invoke-virtual {p1}, Ljgj;->bew()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    move-object v0, v3

    .line 419
    :goto_0
    return-object v0

    .line 383
    :cond_0
    iget-object v0, p1, Ljgj;->ejL:[Ljgm;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 384
    iget-object v0, p1, Ljgj;->ejL:[Ljgm;

    iget-object v1, p1, Ljgj;->ejL:[Ljgm;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    .line 388
    invoke-virtual {v1}, Ljgm;->bky()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljgm;->bkx()I

    move-result v0

    .line 390
    :goto_1
    invoke-virtual {v1}, Ljgm;->bkw()I

    move-result v1

    move v9, v1

    move v1, v0

    move v0, v9

    .line 393
    :goto_2
    invoke-virtual {p1}, Ljgj;->bew()I

    move-result v4

    if-ne v4, v7, :cond_3

    .line 394
    iget-object v0, p1, Ljgj;->ejL:[Ljgm;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 395
    const/16 v0, 0x9

    if-le v1, v0, :cond_1

    .line 396
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 413
    :cond_1
    :goto_3
    if-eqz v3, :cond_7

    .line 415
    const-string v0, " \u00b7 "

    new-array v1, v7, [Ljava/lang/CharSequence;

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    aput-object v3, v1, v6

    invoke-static {v0, v1}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 388
    :cond_2
    iget-object v0, p1, Ljgj;->ejL:[Ljgm;

    array-length v0, v0

    goto :goto_1

    .line 399
    :cond_3
    if-ne v0, v6, :cond_6

    .line 402
    iget-object v3, p1, Ljgj;->ejL:[Ljgm;

    array-length v4, v3

    move v1, v2

    move v0, v2

    :goto_4
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 403
    invoke-virtual {v5}, Ljgm;->bkw()I

    move-result v5

    if-ne v5, v6, :cond_4

    .line 404
    add-int/lit8 v0, v0, 0x1

    .line 402
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 407
    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f10002c

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v1, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 409
    :cond_6
    if-ne v0, v7, :cond_1

    .line 410
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a024c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 419
    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    move v1, v2

    goto :goto_2
.end method

.method public static d(Landroid/content/Context;Ljgj;)Lanv;
    .locals 18

    .prologue
    .line 649
    move-object/from16 v0, p1

    iget-object v1, v0, Ljgj;->ejN:[Ljgn;

    array-length v1, v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 651
    move-object/from16 v0, p1

    iget-object v1, v0, Ljgj;->ejN:[Ljgn;

    const/4 v2, 0x0

    aget-object v11, v1, v2

    .line 652
    move-object/from16 v0, p1

    iget-object v1, v0, Ljgj;->ejN:[Ljgn;

    const/4 v2, 0x1

    aget-object v12, v1, v2

    .line 654
    move-object/from16 v0, p1

    iget-object v1, v0, Ljgj;->ejL:[Ljgm;

    array-length v9, v1

    .line 655
    const/16 v1, 0x9

    invoke-static {v9, v1}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 656
    invoke-virtual/range {p1 .. p1}, Ljgj;->bew()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v1, v11, Ljgn;->eks:Ljgk;

    if-eqz v1, :cond_3

    iget-object v1, v12, Ljgn;->eks:Ljgk;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    move v10, v1

    .line 658
    :goto_1
    add-int/lit8 v1, v13, 0x1

    .line 659
    if-eqz v10, :cond_4

    .line 661
    add-int/lit8 v1, v1, 0x3

    move v8, v1

    .line 666
    :goto_2
    invoke-static {v8}, Lfwo;->jI(I)Ljax;

    move-result-object v3

    .line 669
    const/4 v2, 0x0

    const v1, 0x7f0a0287

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 675
    sub-int v14, v9, v13

    .line 676
    const/4 v1, 0x0

    move v9, v1

    :goto_3
    if-ge v9, v13, :cond_a

    .line 677
    add-int v1, v14, v9

    .line 678
    move-object/from16 v0, p1

    iget-object v2, v0, Ljgj;->ejL:[Ljgm;

    aget-object v2, v2, v1

    .line 679
    invoke-virtual {v2}, Ljgm;->bky()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Ljgm;->bkx()I

    move-result v1

    .line 680
    :goto_4
    const/4 v5, 0x0

    .line 681
    iget-object v4, v2, Ljgm;->ekh:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_0

    .line 682
    iget-object v4, v2, Ljgm;->ekh:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v5, v4, v5

    .line 684
    :cond_0
    const/4 v6, 0x0

    .line 685
    iget-object v4, v2, Ljgm;->ekh:[Ljava/lang/String;

    array-length v4, v4

    const/4 v7, 0x1

    if-le v4, v7, :cond_1

    .line 686
    iget-object v2, v2, Ljgm;->ekh:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v6, v2, v4

    .line 688
    :cond_1
    add-int/lit8 v2, v9, 0x1

    invoke-virtual/range {p1 .. p1}, Ljgj;->bew()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    :goto_5
    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 676
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_3

    .line 649
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 656
    :cond_3
    const/4 v1, 0x0

    move v10, v1

    goto :goto_1

    .line 664
    :cond_4
    add-int/lit8 v1, v1, 0x1

    move v8, v1

    goto :goto_2

    .line 679
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 688
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Ljgj;->bkk()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x2

    :cond_6
    :goto_6
    if-gt v1, v4, :cond_9

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_7
    const/4 v4, 0x4

    goto :goto_6

    :pswitch_2
    const/4 v4, 0x4

    goto :goto_6

    :pswitch_3
    const/4 v4, 0x3

    goto :goto_6

    :pswitch_4
    const/4 v4, 0x2

    goto :goto_6

    :pswitch_5
    const/4 v4, 0x2

    const/4 v7, 0x1

    if-ne v1, v7, :cond_8

    const v1, 0x7f0a027a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_8
    const/4 v7, 0x2

    if-ne v1, v7, :cond_6

    const v1, 0x7f0a027b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    :cond_9
    sub-int/2addr v1, v4

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f10002c

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v4, v7, v1, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 695
    :cond_a
    if-eqz v10, :cond_b

    .line 697
    iget-object v9, v11, Ljgn;->eks:Ljgk;

    .line 698
    iget-object v10, v12, Ljgn;->eks:Ljgk;

    .line 699
    add-int/lit8 v2, v8, -0x3

    const v1, 0x7f0a0256

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Ljgk;->bkl()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Ljgk;->bkl()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 704
    add-int/lit8 v2, v8, -0x2

    const v1, 0x7f0a0257

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Ljgk;->getHits()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Ljgk;->getHits()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 709
    add-int/lit8 v2, v8, -0x1

    const v1, 0x7f0a0258

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Ljgk;->bkm()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Ljgk;->bkm()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 723
    :goto_7
    const/4 v1, 0x6

    if-le v8, v1, :cond_c

    .line 726
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d015f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 733
    :goto_8
    const/4 v2, 0x0

    invoke-static {v3, v1, v2}, Lfwo;->a(Ljax;II)Lanv;

    move-result-object v1

    return-object v1

    .line 715
    :cond_b
    add-int/lit8 v2, v8, -0x1

    const v1, 0x7f0a0254

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Ljgn;->rf()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Ljgn;->rf()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v7}, Lfwo;->a(Landroid/content/Context;ILjax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_7

    .line 729
    :cond_c
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_8

    .line 688
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static g(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 320
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p1

    .line 321
    const v2, 0x8001a

    invoke-static {p0, v0, v1, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 328
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p1

    .line 329
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v3, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v4, v2}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v4, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    sub-int/2addr v2, v3

    .line 331
    invoke-static {p0, v0, v1, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 332
    packed-switch v2, :pswitch_data_0

    .line 340
    invoke-static {p0, p1, p2}, Lfwo;->g(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 341
    const v2, 0x7f0a0246

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v6

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 334
    :pswitch_0
    const v1, 0x7f0a0244

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 336
    :pswitch_1
    const v1, 0x7f0a0243

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 338
    :pswitch_2
    const v1, 0x7f0a0245

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 332
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static jI(I)Ljax;
    .locals 5

    .prologue
    .line 627
    new-instance v0, Ljax;

    invoke-direct {v0}, Ljax;-><init>()V

    .line 628
    new-array v1, p0, [Ljhg;

    iput-object v1, v0, Ljax;->dXu:[Ljhg;

    .line 629
    new-instance v1, Ljay;

    invoke-direct {v1}, Ljay;-><init>()V

    .line 630
    new-array v2, p0, [Ljhg;

    iput-object v2, v1, Ljay;->dXy:[Ljhg;

    .line 631
    new-instance v2, Ljay;

    invoke-direct {v2}, Ljay;-><init>()V

    .line 632
    new-array v3, p0, [Ljhg;

    iput-object v3, v2, Ljay;->dXy:[Ljhg;

    .line 633
    const/4 v3, 0x2

    new-array v3, v3, [Ljay;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    iput-object v3, v0, Ljax;->dXv:[Ljay;

    .line 634
    return-object v0
.end method

.method private static y(Ljava/lang/String;Z)Ljhg;
    .locals 2

    .prologue
    .line 870
    new-instance v0, Ljhg;

    invoke-direct {v0}, Ljhg;-><init>()V

    invoke-virtual {v0, p0}, Ljhg;->uX(Ljava/lang/String;)Ljhg;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljhg;->ik(Z)Ljhg;

    move-result-object v0

    .line 871
    if-nez p1, :cond_0

    .line 872
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljhg;->pR(I)Ljhg;

    .line 874
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 10

    .prologue
    .line 74
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v4, v0, Lizj;->dSk:Ljgj;

    .line 76
    new-instance v5, Lang;

    invoke-direct {v5}, Lang;-><init>()V

    .line 77
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lang;->aS(Z)Lang;

    .line 79
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 80
    invoke-static {v4}, Lfwo;->a(Ljgj;)Ljava/lang/Integer;

    move-result-object v3

    .line 81
    invoke-virtual {p0, p1, v4, v3}, Lfwo;->a(Landroid/content/Context;Ljgj;Ljava/lang/Integer;)Lanh;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-virtual {v4}, Ljgj;->bew()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 83
    if-nez v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v4, v0}, Lfwo;->a(Landroid/content/Context;Ljgj;Z)Lanh;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    :goto_1
    invoke-virtual {p0, p1, v4}, Lfwo;->e(Landroid/content/Context;Ljgj;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 98
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v5, Lang;->ags:[Lanh;

    .line 100
    return-object v5

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 85
    :cond_2
    if-nez v3, :cond_8

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v4}, Ljgj;->bjY()Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_3
    new-instance v7, Laop;

    invoke-direct {v7}, Laop;-><init>()V

    invoke-static {p1, v4}, Lfwo;->a(Landroid/content/Context;Ljgj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Laop;->ce(Ljava/lang/String;)Laop;

    if-eqz v1, :cond_3

    invoke-static {p1, v4}, Lfwo;->b(Landroid/content/Context;Ljgj;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v7, v2}, Laop;->cf(Ljava/lang/String;)Laop;

    invoke-virtual {v4}, Ljgj;->getStatusCode()I

    move-result v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_a

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f0b00b9

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    :goto_4
    invoke-virtual {v7, v2}, Laop;->cG(I)Laop;

    :cond_3
    invoke-direct {p0}, Lfwo;->aDJ()Ljgn;

    move-result-object v2

    invoke-direct {p0}, Lfwo;->aDK()Ljgn;

    move-result-object v8

    if-eqz v2, :cond_4

    invoke-static {v2, v1}, Lfwo;->a(Ljgn;Z)Laoq;

    move-result-object v9

    iput-object v9, v7, Laop;->ajP:Laoq;

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Ljgn;->bjS()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, v7, Laop;->ajP:Laoq;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Laoq;->be(Z)Laoq;

    const/4 v0, 0x0

    :cond_4
    if-eqz v8, :cond_5

    invoke-static {v8, v1}, Lfwo;->a(Ljgn;Z)Laoq;

    move-result-object v1

    iput-object v1, v7, Laop;->ajQ:Laoq;

    iget-object v1, v7, Laop;->ajQ:Laoq;

    invoke-virtual {v1, v0}, Laoq;->be(Z)Laoq;

    :cond_5
    invoke-virtual {v4}, Ljgj;->bew()I

    move-result v0

    invoke-static {v0}, Lfwq;->jK(I)I

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v7, v1}, Laop;->cH(I)Laop;

    :cond_6
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/4 v2, 0x4

    if-ne v0, v2, :cond_b

    const/16 v0, 0x19

    :goto_5
    invoke-virtual {v1, v0}, Lanh;->cm(I)Lanh;

    iput-object v7, v1, Lanh;->agK:Laop;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-virtual {v4}, Ljgj;->getStatusCode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d

    iget-object v0, v4, Ljgj;->ejN:[Ljgn;

    array-length v0, v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    iget-object v0, v4, Ljgj;->ejL:[Ljgm;

    array-length v0, v0

    if-nez v0, :cond_c

    :cond_7
    const/4 v0, 0x0

    move-object v1, v0

    .line 87
    :goto_6
    if-eqz v1, :cond_0

    .line 89
    if-nez v3, :cond_16

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b00ba

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 92
    :goto_7
    const/high16 v2, 0xc000000

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v3, 0xffffff

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lanh;->co(I)Lanh;

    .line 93
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 85
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_a
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f0b00ba

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_4

    :cond_b
    const/16 v0, 0xd

    goto :goto_5

    .line 86
    :cond_c
    invoke-static {p1, v4}, Lfwo;->d(Landroid/content/Context;Ljgj;)Lanv;

    move-result-object v0

    move-object v1, v0

    :goto_8
    if-nez v1, :cond_15

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_6

    :cond_d
    iget-object v0, v4, Ljgj;->ejN:[Ljgn;

    array-length v0, v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, v4, Ljgj;->ejN:[Ljgn;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget-object v0, v4, Ljgj;->ejN:[Ljgn;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    iget-object v0, v1, Ljgn;->ekt:Ljgo;

    if-eqz v0, :cond_e

    iget-object v0, v2, Ljgn;->ekt:Ljgo;

    if-nez v0, :cond_10

    :cond_e
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_8

    :cond_f
    const/4 v0, 0x0

    goto :goto_9

    :cond_10
    invoke-virtual {v4}, Ljgj;->bkj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_a
    invoke-virtual {v4}, Ljgj;->bkk()Z

    move-result v7

    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v4}, Ljgj;->bew()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_11
    :goto_b
    invoke-static {p1, v8, v1, v2}, Lfwo;->a(Landroid/content/Context;Ljava/util/List;Ljgn;Ljgn;)Ljax;

    move-result-object v0

    if-nez v0, :cond_14

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_8

    :cond_12
    const/4 v0, 0x0

    goto :goto_a

    :pswitch_0
    if-nez v0, :cond_11

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :pswitch_1
    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :pswitch_2
    if-nez v0, :cond_11

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v7, :cond_13

    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v7, :cond_11

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    :pswitch_3
    if-nez v0, :cond_11

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    :pswitch_4
    if-nez v0, :cond_11

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v7, :cond_11

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    :pswitch_5
    if-nez v0, :cond_11

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    :cond_14
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f0d013e

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Lfwo;->a(Ljax;II)Lanv;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_8

    :cond_15
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v2, 0x18

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    iput-object v1, v0, Lanh;->agU:Lanv;

    move-object v1, v0

    goto/16 :goto_6

    :cond_16
    move-object v0, v3

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Ljgj;Ljava/lang/Integer;)Lanh;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 128
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 129
    invoke-virtual {v1, v6}, Lanh;->cm(I)Lanh;

    .line 131
    if-nez p3, :cond_1

    .line 132
    invoke-virtual {v1, v7}, Lanh;->aW(Z)Lanh;

    .line 137
    :goto_0
    new-instance v2, Lany;

    invoke-direct {v2}, Lany;-><init>()V

    .line 138
    invoke-direct {p0}, Lfwo;->aDJ()Ljgn;

    move-result-object v0

    invoke-direct {p0}, Lfwo;->aDK()Ljgn;

    move-result-object v3

    if-nez v0, :cond_2

    if-nez v3, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v0}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 140
    iput-object v2, v1, Lanh;->agw:Lany;

    .line 142
    iget-object v0, p2, Ljgj;->ahD:Lixx;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v1, Lanh;->ahu:Lizj;

    .line 144
    new-instance v0, Lfsa;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0200ed

    invoke-virtual {v0, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    iget-object v2, p2, Ljgj;->ahD:Lixx;

    invoke-virtual {v0, v2}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v1, Lanh;->ahs:Lani;

    .line 149
    :cond_0
    return-object v1

    .line 134
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lanh;->cn(I)Lanh;

    goto :goto_0

    .line 138
    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {v3}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    if-nez v3, :cond_4

    invoke-virtual {v0}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const v4, 0x7f0a0260

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Ljgj;Z)Lanh;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 186
    iget-object v2, p2, Ljgj;->ejY:Ljgl;

    .line 188
    new-instance v3, Lanl;

    invoke-direct {v3}, Lanl;-><init>()V

    .line 189
    invoke-virtual {p2}, Ljgj;->nk()J

    move-result-wide v4

    invoke-static {p1, v4, v5}, Lfwo;->h(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lanl;->aw(Ljava/lang/String;)Lanl;

    .line 190
    invoke-static {p1, p2}, Lfwo;->b(Landroid/content/Context;Ljgj;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 192
    invoke-virtual {v3, v0}, Lanl;->ax(Ljava/lang/String;)Lanl;

    .line 193
    invoke-virtual {p2}, Ljgj;->getStatusCode()I

    move-result v0

    if-ne v0, v10, :cond_6

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b00b9

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v3, v0}, Lanl;->cs(I)Lanl;

    .line 198
    :cond_0
    invoke-virtual {p2}, Ljgj;->tv()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 199
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    iget-object v4, p2, Ljgj;->ejY:Ljgl;

    invoke-virtual {v4}, Ljgl;->bkq()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p2}, Ljgj;->getLocation()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v10

    const-string v4, " - "

    invoke-static {v4, v0}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lanl;->ay(Ljava/lang/String;)Lanl;

    .line 206
    :goto_1
    invoke-direct {p0}, Lfwo;->aDJ()Ljgn;

    move-result-object v0

    .line 207
    new-instance v4, Lanm;

    invoke-direct {v4}, Lanm;-><init>()V

    invoke-virtual {v0}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lanm;->aC(Ljava/lang/String;)Lanm;

    move-result-object v4

    iput-object v4, v3, Lanl;->ahX:Lanm;

    .line 209
    invoke-virtual {v0}, Ljgn;->bkF()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 210
    iget-object v4, v3, Lanl;->ahX:Lanm;

    invoke-virtual {v0}, Ljgn;->bkE()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lanm;->aB(Ljava/lang/String;)Lanm;

    .line 211
    if-eqz p3, :cond_1

    invoke-virtual {v0}, Ljgn;->bjS()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, v3, Lanl;->ahX:Lanm;

    invoke-virtual {v0, v10}, Lanm;->aZ(Z)Lanm;

    move p3, v1

    .line 219
    :cond_1
    invoke-direct {p0}, Lfwo;->aDK()Ljgn;

    move-result-object v0

    .line 220
    new-instance v4, Lanm;

    invoke-direct {v4}, Lanm;-><init>()V

    invoke-virtual {v0}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lanm;->aC(Ljava/lang/String;)Lanm;

    move-result-object v4

    invoke-virtual {v4, p3}, Lanm;->aZ(Z)Lanm;

    move-result-object v4

    iput-object v4, v3, Lanl;->ahY:Lanm;

    .line 223
    invoke-virtual {v0}, Ljgn;->bkF()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 224
    iget-object v4, v3, Lanl;->ahY:Lanm;

    invoke-virtual {v0}, Ljgn;->bkE()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lanm;->aB(Ljava/lang/String;)Lanm;

    .line 227
    :cond_2
    invoke-virtual {v2}, Ljgl;->bkp()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 228
    invoke-virtual {v2}, Ljgl;->bko()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lanl;->az(Ljava/lang/String;)Lanl;

    .line 231
    :cond_3
    invoke-virtual {p2}, Ljgj;->bjY()Z

    move-result v0

    if-nez v0, :cond_9

    .line 232
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 233
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 234
    iget-object v6, p2, Ljgj;->ejL:[Ljgm;

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_8

    aget-object v8, v6, v0

    .line 235
    iget-object v9, v8, Ljgm;->ekh:[Ljava/lang/String;

    array-length v9, v9

    if-lez v9, :cond_4

    .line 236
    iget-object v9, v8, Ljgm;->ekh:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    :cond_4
    iget-object v9, v8, Ljgm;->ekh:[Ljava/lang/String;

    array-length v9, v9

    if-le v9, v10, :cond_5

    .line 239
    iget-object v8, v8, Ljgm;->ekh:[Ljava/lang/String;

    aget-object v8, v8, v10

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 193
    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b00ba

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_0

    .line 203
    :cond_7
    iget-object v0, p2, Ljgj;->ejY:Ljgl;

    invoke-virtual {v0}, Ljgl;->bkq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lanl;->ay(Ljava/lang/String;)Lanl;

    goto/16 :goto_1

    .line 242
    :cond_8
    iget-object v1, v3, Lanl;->ahX:Lanm;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lanm;->aic:[Ljava/lang/String;

    .line 243
    iget-object v1, v3, Lanl;->ahY:Lanm;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lanm;->aic:[Ljava/lang/String;

    .line 245
    invoke-virtual {v2}, Ljgl;->bks()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 246
    invoke-virtual {v2}, Ljgl;->bkr()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lanl;->aA(Ljava/lang/String;)Lanl;

    .line 250
    :cond_9
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 251
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 252
    iput-object v3, v0, Lanh;->ahh:Lanl;

    .line 253
    return-object v0
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0, p1, p2}, Lfwo;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 108
    iget-object v0, v0, Lang;->ags:[Lanh;

    return-object v0
.end method

.method public final e(Landroid/content/Context;Ljgj;)Ljava/util/List;
    .locals 4

    .prologue
    .line 918
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 919
    invoke-virtual {p2}, Ljgj;->aVA()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 920
    const v1, 0x7f0a025d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljgj;->aVz()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x61

    invoke-direct {p0, v1, v2, v3}, Lfwo;->c(Ljava/lang/String;Ljava/lang/String;I)Lanh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 923
    :cond_0
    invoke-virtual {p2}, Ljgj;->bki()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 924
    const v1, 0x7f0a025f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljgj;->bkh()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x63

    invoke-direct {p0, v1, v2, v3}, Lfwo;->c(Ljava/lang/String;Ljava/lang/String;I)Lanh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 927
    :cond_1
    invoke-virtual {p2}, Ljgj;->bkg()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 928
    const v1, 0x7f0a025e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljgj;->bkf()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x60

    invoke-direct {p0, v1, v2, v3}, Lfwo;->c(Ljava/lang/String;Ljava/lang/String;I)Lanh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 931
    :cond_2
    invoke-virtual {p2}, Ljgj;->bkc()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 932
    const v1, 0x7f0a025b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljgj;->bkb()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x62

    invoke-direct {p0, v1, v2, v3}, Lfwo;->c(Ljava/lang/String;Ljava/lang/String;I)Lanh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 935
    :cond_3
    invoke-virtual {p2}, Ljgj;->bke()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 936
    const v1, 0x7f0a025c

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljgj;->bkd()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x5f

    invoke-direct {p0, v1, v2, v3}, Lfwo;->c(Ljava/lang/String;Ljava/lang/String;I)Lanh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 939
    :cond_4
    return-object v0
.end method
