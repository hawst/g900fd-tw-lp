.class public final Leyz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field bbz:Z

.field private final cmp:Leyx;

.field public final cmq:Leyy;

.field private final cmr:Ljava/util/Set;

.field private final cms:Ljava/util/Set;


# direct methods
.method constructor <init>(Leyx;)V
    .locals 1

    .prologue
    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    new-instance v0, Leyy;

    invoke-direct {v0}, Leyy;-><init>()V

    iput-object v0, p0, Leyz;->cmq:Leyy;

    .line 427
    const/4 v0, 0x0

    iput-boolean v0, p0, Leyz;->bbz:Z

    .line 428
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Leyz;->cmr:Ljava/util/Set;

    .line 429
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Leyz;->cms:Ljava/util/Set;

    .line 432
    iput-object p1, p0, Leyz;->cmp:Leyx;

    .line 433
    return-void
.end method

.method private static b(Lamp;)Z
    .locals 1

    .prologue
    .line 601
    invoke-virtual {p0}, Lamp;->nA()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lamp;->nD()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final awM()Leyx;
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Leyz;->cmq:Leyy;

    invoke-virtual {v0}, Leyy;->awM()Leyx;

    move-result-object v0

    return-object v0
.end method

.method public final awN()Z
    .locals 2

    .prologue
    .line 586
    iget-boolean v0, p0, Leyz;->bbz:Z

    if-nez v0, :cond_0

    .line 587
    iget-object v0, p0, Leyz;->cmp:Leyx;

    invoke-virtual {v0}, Leyx;->awK()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    .line 588
    iget-object v1, p0, Leyz;->cmr:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 589
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Leyz;->bbz:Z

    .line 591
    :cond_0
    iget-boolean v0, p0, Leyz;->bbz:Z

    return v0

    .line 589
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lamo;)Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 443
    invoke-static {p1}, Leyx;->a(Lamo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 535
    :cond_0
    :goto_0
    return v3

    .line 444
    :cond_1
    iget-object v0, p0, Leyz;->cmr:Ljava/util/Set;

    invoke-virtual {p1}, Lamo;->nh()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 446
    invoke-static {p1}, Leyx;->b(Lamo;)Ljava/lang/String;

    move-result-object v5

    .line 448
    if-eqz v5, :cond_0

    .line 451
    invoke-static {p1}, Leyx;->c(Lamo;)Ljava/lang/String;

    move-result-object v0

    .line 452
    if-eqz v0, :cond_b

    .line 453
    iget-object v1, p0, Leyz;->cms:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    move v4, v0

    .line 456
    :goto_2
    iget-object v0, p0, Leyz;->cmp:Leyx;

    invoke-virtual {p1}, Lamo;->nh()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Leyx;->bj(J)Lamk;

    move-result-object v6

    .line 458
    if-nez v6, :cond_3

    .line 459
    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    invoke-virtual {v0, v4}, Lamk;->aF(Z)Lamk;

    move-result-object v0

    .line 461
    iput-object p1, v0, Lamk;->aeQ:Lamo;

    .line 462
    new-instance v1, Lamp;

    invoke-direct {v1}, Lamp;-><init>()V

    invoke-virtual {v1, v5}, Lamp;->X(Ljava/lang/String;)Lamp;

    move-result-object v1

    iput-object v1, v0, Lamk;->aeR:Lamp;

    .line 463
    iget-object v1, p0, Leyz;->cmq:Leyy;

    invoke-virtual {v1, v0}, Leyy;->c(Lamk;)Z

    .line 464
    iput-boolean v2, p0, Leyz;->bbz:Z

    :goto_3
    move v3, v2

    .line 535
    goto :goto_0

    :cond_2
    move v0, v3

    .line 453
    goto :goto_1

    .line 471
    :cond_3
    invoke-virtual {v6}, Lamk;->mX()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v6}, Lamk;->mW()Z

    move-result v0

    .line 473
    :goto_4
    iget-object v1, v6, Lamk;->aeR:Lamp;

    invoke-virtual {v1}, Lamp;->nu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-ne v4, v0, :cond_6

    .line 475
    if-ne v4, v0, :cond_5

    .line 476
    iget-object v0, p0, Leyz;->cmq:Leyy;

    invoke-virtual {v0, v6}, Leyy;->c(Lamk;)Z

    goto :goto_3

    :cond_4
    move v0, v3

    .line 471
    goto :goto_4

    .line 478
    :cond_5
    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    invoke-static {v6, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    .line 480
    invoke-virtual {v0, v4}, Lamk;->aF(Z)Lamk;

    .line 482
    iget-object v1, p0, Leyz;->cmq:Leyy;

    invoke-virtual {v1, v0}, Leyy;->c(Lamk;)Z

    .line 483
    iput-boolean v2, p0, Leyz;->bbz:Z

    goto :goto_3

    .line 490
    :cond_6
    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    invoke-static {v6, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    .line 493
    iput-object p1, v0, Lamk;->aeQ:Lamo;

    .line 496
    iget-object v1, v6, Lamk;->aeR:Lamp;

    new-instance v7, Lamp;

    invoke-direct {v7}, Lamp;-><init>()V

    invoke-static {v1, v7}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Lamp;

    .line 498
    invoke-virtual {v1, v5}, Lamp;->X(Ljava/lang/String;)Lamp;

    .line 502
    iget-object v5, v6, Lamk;->aeQ:Lamo;

    .line 503
    invoke-virtual {v5}, Lamo;->no()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lamo;->no()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 504
    invoke-virtual {v1}, Lamp;->nw()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, v1, Lamp;->afr:Lamq;

    invoke-virtual {v5}, Lamq;->nH()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, v1, Lamp;->afr:Lamq;

    invoke-virtual {v5}, Lamq;->nI()Z

    move-result v5

    if-eqz v5, :cond_7

    move v3, v2

    :cond_7
    if-nez v3, :cond_8

    invoke-static {v1}, Leyz;->b(Lamp;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 506
    :cond_8
    const/4 v3, 0x0

    iput-object v3, v1, Lamp;->afr:Lamq;

    .line 507
    invoke-virtual {v1}, Lamp;->ny()Lamp;

    move-result-object v3

    invoke-virtual {v3}, Lamp;->nB()Lamp;

    move-result-object v3

    invoke-virtual {v3}, Lamp;->nE()Lamp;

    move-result-object v3

    invoke-virtual {v3, v2}, Lamp;->aL(Z)Lamp;

    .line 524
    :cond_9
    :goto_5
    iput-object v1, v0, Lamk;->aeR:Lamp;

    .line 527
    invoke-virtual {v0, v4}, Lamk;->aF(Z)Lamk;

    .line 530
    iget-object v1, p0, Leyz;->cmq:Leyy;

    invoke-virtual {v1, v0}, Leyy;->c(Lamk;)Z

    .line 531
    iput-boolean v2, p0, Leyz;->bbz:Z

    goto/16 :goto_3

    .line 514
    :cond_a
    invoke-virtual {v5}, Lamo;->nk()J

    move-result-wide v6

    invoke-virtual {p1}, Lamo;->nk()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-eqz v3, :cond_9

    .line 516
    invoke-static {v1}, Leyz;->b(Lamp;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 517
    invoke-virtual {v1}, Lamp;->nB()Lamp;

    move-result-object v3

    invoke-virtual {v3}, Lamp;->nE()Lamp;

    move-result-object v3

    invoke-virtual {v3, v2}, Lamp;->aL(Z)Lamp;

    goto :goto_5

    :cond_b
    move v4, v3

    goto/16 :goto_2
.end method

.method public final r(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 546
    iget-object v0, p0, Leyz;->cmp:Leyx;

    invoke-virtual {v0}, Leyx;->awH()Ljava/util/Collection;

    move-result-object v0

    .line 547
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 548
    iput-boolean v3, p0, Leyz;->bbz:Z

    .line 564
    :cond_0
    :goto_0
    iget-object v0, p0, Leyz;->cmq:Leyy;

    invoke-virtual {v0, p1}, Leyy;->g(Ljava/lang/Iterable;)Z

    move-result v0

    return v0

    .line 553
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 554
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamm;

    .line 555
    invoke-static {v0}, Leyx;->b(Lamm;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 557
    :cond_2
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamm;

    .line 558
    invoke-static {v0}, Leyx;->b(Lamm;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 559
    iput-boolean v3, p0, Leyz;->bbz:Z

    goto :goto_0
.end method
