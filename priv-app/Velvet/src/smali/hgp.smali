.class public final Lhgp;
.super Ldlv;
.source "PG"


# instance fields
.field private dgC:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

.field dgD:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0x64

    invoke-direct {p0, p1, v0}, Ldlv;-><init>(Landroid/app/Activity;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/velvet/ui/VelvetTopLevelContainer;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lhgp;->dgC:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    .line 25
    return-void
.end method

.method public final varargs b([Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 29
    iget-object v0, p0, Lhgp;->dgC:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    if-eqz v0, :cond_0

    array-length v0, p1

    if-ne v0, v1, :cond_0

    aget-object v0, p1, v3

    const-string v2, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_USE_HERO_TRANSITION"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lhgp;->A(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lhgp;->dgD:Landroid/content/Intent;

    .line 34
    new-array v0, v1, [Landroid/content/Intent;

    iget-object v2, p0, Lhgp;->dgD:Landroid/content/Intent;

    aput-object v2, v0, v3

    invoke-virtual {p0, v0}, Lhgp;->c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avk()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lhgp;->dgD:Landroid/content/Intent;

    const-string v2, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_COMPANION_VIEW_RES_ID"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 38
    iget-object v0, p0, Lhgp;->dgD:Landroid/content/Intent;

    const-string v3, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_HERO_WEBVIEW_RECT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 41
    iget-object v3, p0, Lhgp;->dgD:Landroid/content/Intent;

    const-string v4, "com.google.android.shared.util.SimpleIntentStarter.EXTRA_USE_HERO_TRANSITION"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 44
    iget-object v3, p0, Lhgp;->dgC:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    new-instance v4, Lhgq;

    invoke-direct {v4, p0}, Lhgq;-><init>(Lhgp;)V

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->a(Landroid/graphics/Rect;ILjava/lang/Runnable;)V

    move v0, v1

    .line 56
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Ldlv;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method
