.class public final Lfqg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private final czA:Z

.field final czz:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

.field private final k:I

.field public mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;IZ)V
    .locals 0

    .prologue
    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300
    iput-object p1, p0, Lfqg;->czz:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 301
    iput p2, p0, Lfqg;->k:I

    .line 302
    iput-boolean p3, p0, Lfqg;->czA:Z

    .line 303
    return-void
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 292
    check-cast p1, Lfqg;

    iget-boolean v0, p0, Lfqg;->czA:Z

    iget-boolean v1, p1, Lfqg;->czA:Z

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lfqg;->czA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lfqg;->k:I

    iget v1, p1, Lfqg;->k:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 316
    instance-of v1, p1, Lfqg;

    if-nez v1, :cond_1

    .line 318
    :cond_0
    :goto_0
    return v0

    .line 317
    :cond_1
    check-cast p1, Lfqg;

    .line 318
    iget v1, p0, Lfqg;->k:I

    iget v2, p1, Lfqg;->k:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lfqg;->czA:Z

    iget-boolean v2, p1, Lfqg;->czA:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 324
    iget v0, p0, Lfqg;->k:I

    shl-int/lit8 v1, v0, 0x1

    iget-boolean v0, p0, Lfqg;->czA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
