.class public final Ljse;
.super Ljsl;
.source "PG"


# instance fields
.field public eCb:[Ljsh;

.field public eCc:[Ljsg;

.field public eCd:[Ljsf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 204
    invoke-static {}, Ljsh;->btM()[Ljsh;

    move-result-object v0

    iput-object v0, p0, Ljse;->eCb:[Ljsh;

    invoke-static {}, Ljsg;->btK()[Ljsg;

    move-result-object v0

    iput-object v0, p0, Ljse;->eCc:[Ljsg;

    invoke-static {}, Ljsf;->btJ()[Ljsf;

    move-result-object v0

    iput-object v0, p0, Ljse;->eCd:[Ljsf;

    const/4 v0, 0x0

    iput-object v0, p0, Ljse;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljse;->eCz:I

    .line 205
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 177
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljse;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljse;->eCb:[Ljsh;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsh;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljse;->eCb:[Ljsh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljsh;

    invoke-direct {v3}, Ljsh;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljse;->eCb:[Ljsh;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljsh;

    invoke-direct {v3}, Ljsh;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljse;->eCb:[Ljsh;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljse;->eCc:[Ljsg;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsg;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljse;->eCc:[Ljsg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljsg;

    invoke-direct {v3}, Ljsg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljse;->eCc:[Ljsg;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljsg;

    invoke-direct {v3}, Ljsg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljse;->eCc:[Ljsg;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljse;->eCd:[Ljsf;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsf;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljse;->eCd:[Ljsf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Ljsf;

    invoke-direct {v3}, Ljsf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljse;->eCd:[Ljsf;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Ljsf;

    invoke-direct {v3}, Ljsf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljse;->eCd:[Ljsf;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Ljse;->eCb:[Ljsh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljse;->eCb:[Ljsh;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 220
    :goto_0
    iget-object v2, p0, Ljse;->eCb:[Ljsh;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 221
    iget-object v2, p0, Ljse;->eCb:[Ljsh;

    aget-object v2, v2, v0

    .line 222
    if-eqz v2, :cond_0

    .line 223
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 220
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    iget-object v0, p0, Ljse;->eCc:[Ljsg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljse;->eCc:[Ljsg;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 228
    :goto_1
    iget-object v2, p0, Ljse;->eCc:[Ljsg;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 229
    iget-object v2, p0, Ljse;->eCc:[Ljsg;

    aget-object v2, v2, v0

    .line 230
    if-eqz v2, :cond_2

    .line 231
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 228
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 235
    :cond_3
    iget-object v0, p0, Ljse;->eCd:[Ljsf;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljse;->eCd:[Ljsf;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 236
    :goto_2
    iget-object v0, p0, Ljse;->eCd:[Ljsf;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 237
    iget-object v0, p0, Ljse;->eCd:[Ljsf;

    aget-object v0, v0, v1

    .line 238
    if-eqz v0, :cond_4

    .line 239
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 236
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 243
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 244
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 248
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 249
    iget-object v2, p0, Ljse;->eCb:[Ljsh;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljse;->eCb:[Ljsh;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 250
    :goto_0
    iget-object v3, p0, Ljse;->eCb:[Ljsh;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 251
    iget-object v3, p0, Ljse;->eCb:[Ljsh;

    aget-object v3, v3, v0

    .line 252
    if-eqz v3, :cond_0

    .line 253
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 250
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 258
    :cond_2
    iget-object v2, p0, Ljse;->eCc:[Ljsg;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljse;->eCc:[Ljsg;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 259
    :goto_1
    iget-object v3, p0, Ljse;->eCc:[Ljsg;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 260
    iget-object v3, p0, Ljse;->eCc:[Ljsg;

    aget-object v3, v3, v0

    .line 261
    if-eqz v3, :cond_3

    .line 262
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 259
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 267
    :cond_5
    iget-object v2, p0, Ljse;->eCd:[Ljsf;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljse;->eCd:[Ljsf;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 268
    :goto_2
    iget-object v2, p0, Ljse;->eCd:[Ljsf;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 269
    iget-object v2, p0, Ljse;->eCd:[Ljsf;

    aget-object v2, v2, v1

    .line 270
    if-eqz v2, :cond_6

    .line 271
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 268
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 276
    :cond_7
    return v0
.end method
