.class public final Lhqj;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private bnx:Ldjs;

.field private doc:Lhql;

.field private dod:I

.field private doe:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 111
    return-void
.end method

.method public static a(Lhql;Ljava/lang/String;I)Lhqj;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v1, "TITLE_RES_ID_KEY"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    const-string v1, "LOCATION_STRING_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v1, Lhqj;

    invoke-direct {v1}, Lhqj;-><init>()V

    .line 44
    invoke-virtual {v1, v0}, Lhqj;->setArguments(Landroid/os/Bundle;)V

    .line 45
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Lhqj;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 46
    return-object v1
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    iget-object v1, p0, Lhqj;->doc:Lhql;

    invoke-virtual {v1}, Lhql;->aRv()Lhqm;

    move-result-object v1

    iget-object v1, v1, Lhqm;->doh:Lhqr;

    invoke-virtual {v0, v1}, Ldjs;->a(Landroid/widget/BaseAdapter;)V

    .line 95
    :cond_0
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lhqj;->doc:Lhql;

    invoke-virtual {v0, p1}, Lhql;->onCancel(Landroid/content/DialogInterface;)V

    .line 106
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 51
    if-nez p1, :cond_2

    .line 52
    invoke-virtual {p0}, Lhqj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "TITLE_RES_ID_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhqj;->dod:I

    .line 53
    invoke-virtual {p0}, Lhqj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "LOCATION_STRING_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhqj;->doe:Ljava/lang/String;

    .line 59
    :goto_0
    invoke-virtual {p0}, Lhqj;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 60
    instance-of v1, v0, Lhql;

    if-eqz v1, :cond_0

    .line 61
    check-cast v0, Lhql;

    iput-object v0, p0, Lhqj;->doc:Lhql;

    .line 64
    :cond_0
    new-instance v0, Ldjs;

    invoke-virtual {p0}, Lhqj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f040010

    iget-object v3, p0, Lhqj;->doc:Lhql;

    iget-object v4, p0, Lhqj;->doc:Lhql;

    invoke-virtual {v4}, Lhql;->aRv()Lhqm;

    move-result-object v4

    const v5, 0x7f0a08a1

    invoke-direct/range {v0 .. v5}, Ldjs;-><init>(Landroid/content/Context;ILefk;Ldjw;I)V

    iput-object v0, p0, Lhqj;->bnx:Ldjs;

    .line 70
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    iget v1, p0, Lhqj;->dod:I

    invoke-virtual {v0, v1}, Ldjs;->setTitle(I)V

    .line 71
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    iget-object v1, p0, Lhqj;->doc:Lhql;

    invoke-virtual {v1}, Lhql;->aRv()Lhqm;

    move-result-object v1

    iget-object v1, v1, Lhqm;->doh:Lhqr;

    invoke-virtual {v0, v1}, Ldjs;->a(Landroid/widget/BaseAdapter;)V

    .line 72
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    invoke-virtual {v0}, Ldjs;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 75
    iget-object v0, p0, Lhqj;->doe:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    iget-object v1, p0, Lhqj;->doe:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldjs;->jJ(Ljava/lang/String;)V

    .line 79
    :cond_1
    iget-object v0, p0, Lhqj;->bnx:Ldjs;

    return-object v0

    .line 55
    :cond_2
    const-string v0, "TITLE_RES_ID_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhqj;->dod:I

    .line 56
    const-string v0, "LOCATION_STRING_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhqj;->doe:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 100
    iget-object v0, p0, Lhqj;->doc:Lhql;

    invoke-virtual {v0}, Lhql;->aRw()V

    .line 101
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 85
    const-string v0, "TITLE_RES_ID_KEY"

    iget v1, p0, Lhqj;->dod:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    const-string v0, "LOCATION_STRING_KEY"

    iget-object v1, p0, Lhqj;->bnx:Ldjs;

    invoke-virtual {v1}, Ldjs;->acv()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method
