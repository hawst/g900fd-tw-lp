.class public final Leou;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lerj;
.implements Livq;


# instance fields
.field private final bBY:Ljava/lang/Runnable;

.field private final cgK:Lepl;

.field private final cgL:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lerh;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Leou;->cgL:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 28
    iput-object p1, p0, Leou;->mExecutor:Ljava/util/concurrent/Executor;

    .line 29
    invoke-static {p2}, Lepl;->a(Lerh;)Lepl;

    move-result-object v0

    .line 30
    iput-object v0, p0, Leou;->cgK:Lepl;

    .line 31
    iput-object v0, p0, Leou;->bBY:Ljava/lang/Runnable;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Leou;->auY()V

    .line 74
    iget-object v0, p0, Leou;->cgK:Lepl;

    invoke-virtual {v0, p1, p2}, Lepl;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 75
    return-void
.end method

.method public final auY()V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Leou;->cgL:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Leou;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Leou;->bBY:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 41
    :cond_0
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Leou;->auY()V

    .line 61
    iget-object v0, p0, Leou;->cgK:Lepl;

    invoke-virtual {v0}, Lepl;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Leou;->auY()V

    .line 68
    iget-object v0, p0, Leou;->cgK:Lepl;

    invoke-virtual {v0, p1, p2, p3}, Lepl;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getPermissions()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Leou;->cgK:Lepl;

    invoke-virtual {v0}, Lepl;->getPermissions()I

    move-result v0

    return v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Leou;->cgK:Lepl;

    invoke-virtual {v0}, Lepl;->isDone()Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LazyListenableFuture{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
