.class public abstract Lfac;
.super Landroid/app/Service;
.source "PG"


# static fields
.field static final cmS:Ljava/lang/Integer;

.field static final cmT:Ljava/lang/Integer;


# instance fields
.field private cmP:Landroid/os/Looper;

.field private cmQ:Lfad;

.field public cmR:Z

.field final cmU:Ljava/util/Map;

.field private mClock:Lemp;

.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lfac;->cmS:Ljava/lang/Integer;

    .line 62
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lfac;->cmT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 145
    iput-object p1, p0, Lfac;->mName:Ljava/lang/String;

    .line 146
    invoke-static {}, Lior;->aYb()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lfac;->cmU:Ljava/util/Map;

    .line 147
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Intent;Lfaf;J)I
.end method

.method final is(I)V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lfac;->cmU:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    invoke-virtual {p0, p1}, Lfac;->stopSelf(I)V

    .line 241
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 171
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 172
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DelayedStopIntentService["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfac;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 173
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 175
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lfac;->cmP:Landroid/os/Looper;

    .line 176
    new-instance v0, Lfad;

    iget-object v1, p0, Lfac;->cmP:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lfad;-><init>(Lfac;Landroid/os/Looper;)V

    iput-object v0, p0, Lfac;->cmQ:Lfad;

    .line 177
    new-instance v0, Lere;

    invoke-virtual {p0}, Lfac;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lere;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfac;->mClock:Lemp;

    .line 178
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lfac;->cmP:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 201
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    .line 188
    iget-object v0, p0, Lfac;->cmQ:Lfad;

    invoke-virtual {v0}, Lfad;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 189
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 190
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 191
    const-string v2, "t"

    iget-object v3, p0, Lfac;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 192
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 193
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 194
    iget-object v1, p0, Lfac;->cmQ:Lfad;

    invoke-virtual {v1, v0}, Lfad;->sendMessage(Landroid/os/Message;)Z

    .line 195
    iget-boolean v0, p0, Lfac;->cmR:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
