.class public final Lbyh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Landroid/content/Intent;IIII)V
    .locals 4

    .prologue
    .line 125
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 127
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p0, v0, p1, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 131
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 132
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 142
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 144
    invoke-virtual {v0, p5, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 145
    return-void
.end method

.method public static a(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 7

    .prologue
    const v4, 0x7f020223

    const/16 v5, 0x102

    .line 63
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v6, v0, Lhhq;->mSettings:Lhym;

    .line 64
    if-eqz p1, :cond_4

    .line 65
    new-instance v1, Lbyl;

    invoke-direct {v1}, Lbyl;-><init>()V

    .line 68
    invoke-virtual {v6, p2}, Lhym;->oH(Ljava/lang/String;)V

    .line 70
    const-string v0, "android.intent.action.VOICE_COMMAND"

    invoke-static {p0, v0}, Lhsh;->v(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 73
    if-eqz v0, :cond_3

    .line 75
    invoke-virtual {v6, p2}, Lhym;->oO(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v6, p2}, Lhym;->oK(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 78
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lbyl;->BS()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lhcu;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "entry-point"

    const-string v2, "car-bluetooth"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "bt-address"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v2, 0x7f0a06da

    const v3, 0x7f0a06db

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lbyh;->a(Landroid/content/Context;Landroid/content/Intent;IIII)V

    invoke-virtual {v6, p2}, Lhym;->oM(Ljava/lang/String;)V

    .line 98
    :cond_1
    :goto_1
    return-void

    .line 75
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 86
    :cond_3
    invoke-virtual {v1}, Lbyl;->BS()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lhcu;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "entry-point"

    const-string v2, "multi-assistant"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "bt-address"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v2, 0x7f0a06d8

    const v3, 0x7f0a06d9

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lbyh;->a(Landroid/content/Context;Landroid/content/Intent;IIII)V

    invoke-virtual {v6, p2}, Lhym;->oM(Ljava/lang/String;)V

    goto :goto_1

    .line 92
    :cond_4
    invoke-virtual {v6, p2}, Lhym;->oI(Ljava/lang/String;)V

    .line 95
    invoke-static {p0, v5}, Lbyh;->d(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 117
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 119
    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 120
    return-void
.end method
