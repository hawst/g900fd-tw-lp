.class public final Lfya;
.super Lfro;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private cDe:Lfyd;

.field private cDf:Lfyb;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mRunner:Lerk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lerk;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 44
    iput-object p4, p0, Lfya;->mRunner:Lerk;

    .line 45
    return-void
.end method

.method static synthetic a(Lfya;)Lerk;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfya;->mRunner:Lerk;

    return-object v0
.end method

.method static synthetic a(Lfya;Ljava/util/List;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 32
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f11033f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    const v2, 0x7f110340

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfyc;

    iget-object v2, p0, Lfya;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f04012c

    invoke-virtual {v2, v5, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    const v2, 0x7f1101a9

    iget-object v6, v1, Lfyc;->bLI:Ljava/lang/String;

    invoke-static {v5, v2, v6}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    const v2, 0x7f110080

    iget-object v6, v1, Lfyc;->cDh:Ljava/lang/CharSequence;

    invoke-static {v5, v2, v6}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    const v2, 0x7f1101a8

    iget-object v6, v1, Lfyc;->cBx:Ljava/lang/String;

    invoke-virtual {p0, v5, v2, v6}, Lfya;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    iget-object v2, v1, Lfyc;->cDi:Lani;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lfyc;->cDi:Lani;

    iget-object v2, v2, Lani;->ahC:Lanb;

    invoke-virtual {v2}, Lanb;->oi()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f1102eb

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    iget-object v6, v1, Lfyc;->cDi:Lani;

    iget-object v6, v6, Lani;->ahC:Lanb;

    invoke-virtual {v6}, Lanb;->oh()I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    iget-object v1, v1, Lfyc;->cDi:Lani;

    invoke-virtual {p0, v5, v1}, Lfya;->a(Landroid/view/View;Lani;)V

    iget-object v1, p0, Lfya;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020296

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lfya;)Lfyd;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfya;->cDe:Lfyd;

    return-object v0
.end method

.method private mf(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lfya;->cDf:Lfyb;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lfya;->cDf:Lfyb;

    invoke-virtual {v0}, Lfyb;->cancel()V

    .line 84
    :cond_0
    new-instance v0, Lfyb;

    invoke-direct {v0, p0, p1}, Lfyb;-><init>(Lfya;Ljava/lang/String;)V

    iput-object v0, p0, Lfya;->cDf:Lfyb;

    .line 85
    iget-object v0, p0, Lfya;->cDf:Lfyb;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfyb;->a([Ljava/lang/Object;)Lenp;

    .line 86
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 55
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f11033e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 57
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agZ:Laot;

    .line 58
    invoke-virtual {v1}, Laot;->ry()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-virtual {v1}, Laot;->rx()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 61
    :cond_0
    invoke-virtual {v1}, Laot;->rw()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    iget-object v2, p0, Lfya;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Laot;->getIconResId()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 63
    iget-object v2, p0, Lfya;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 65
    invoke-static {v0, v1, v4, v4, v4}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 69
    :cond_1
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 71
    iget-object v0, p0, Lfro;->cBb:Lfuz;

    instance-of v0, v0, Lfyd;

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, p0, Lfro;->cBb:Lfuz;

    check-cast v0, Lfyd;

    iput-object v0, p0, Lfya;->cDe:Lfyd;

    .line 77
    :goto_0
    const-string v0, ""

    invoke-direct {p0, v0}, Lfya;->mf(Ljava/lang/String;)V

    .line 78
    return-void

    .line 74
    :cond_2
    const-string v0, "SuggestListModulePresenter"

    const-string v1, "CardViewAdapter does not implement SuggestListDelegate"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lfya;->mf(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 49
    iput-object p1, p0, Lfya;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 50
    const v0, 0x7f04012b

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method
