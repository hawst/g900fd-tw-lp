.class public Ldbh;
.super Lddj;
.source "PG"


# instance fields
.field private bqD:Z

.field private bqE:Z

.field private bqF:Z

.field private bqG:Z

.field private bqH:Z


# direct methods
.method public constructor <init>(Ldda;)V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x80

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbh;->bqH:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final Wd()V
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Ldbh;->bqF:Z

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbh;->bqF:Z

    .line 40
    invoke-virtual {p0}, Ldbh;->notifyChanged()V

    .line 42
    :cond_0
    return-void
.end method

.method public final We()V
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Ldbh;->bqF:Z

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbh;->bqF:Z

    .line 49
    invoke-virtual {p0}, Ldbh;->notifyChanged()V

    .line 51
    :cond_0
    return-void
.end method

.method public final Wf()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Ldbh;->bqG:Z

    return v0
.end method

.method public final Wg()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Ldbh;->bqF:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldbh;->bqE:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldbh;->bqD:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Wh()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbh;->bqH:Z

    .line 71
    invoke-virtual {p0}, Ldbh;->notifyChanged()V

    .line 72
    return-void
.end method

.method public final Wi()Z
    .locals 2

    .prologue
    .line 77
    iget-boolean v0, p0, Ldbh;->bqH:Z

    .line 78
    const/4 v1, 0x0

    iput-boolean v1, p0, Ldbh;->bqH:Z

    .line 79
    return v0
.end method

.method public final a(Lddb;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    invoke-virtual {p0}, Ldbh;->Wg()Z

    move-result v3

    .line 107
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yh()Z

    move-result v4

    .line 115
    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XA()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ldbh;->bqD:Z

    .line 116
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XB()Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v2

    :goto_1
    if-nez v0, :cond_8

    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    invoke-virtual {v0}, Ldcw;->isDone()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_2
    iput-boolean v1, p0, Ldbh;->bqE:Z

    .line 118
    :cond_1
    invoke-virtual {p0}, Ldbh;->Wg()Z

    move-result v0

    if-eq v0, v3, :cond_2

    .line 119
    invoke-virtual {p0}, Ldbh;->notifyChanged()V

    .line 121
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 115
    goto :goto_0

    .line 116
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v5

    if-eqz v5, :cond_5

    move v0, v2

    goto :goto_1

    :cond_5
    iget-object v5, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v5}, Ldda;->aao()Ldcu;

    move-result-object v5

    invoke-virtual {v5}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v5

    invoke-virtual {v0, v4}, Lcom/google/android/search/core/state/QueryState;->X(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v5}, Lcom/google/android/search/shared/service/ClientConfig;->anj()Z

    move-result v4

    if-eqz v4, :cond_6

    move v0, v2

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiR()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v5}, Lcom/google/android/search/shared/service/ClientConfig;->ani()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_2
.end method

.method public final da(Z)V
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Ldbh;->bqG:Z

    if-eq p1, v0, :cond_0

    .line 55
    iput-boolean p1, p0, Ldbh;->bqG:Z

    .line 56
    invoke-virtual {p0}, Ldbh;->notifyChanged()V

    .line 58
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "AudioState["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Ldbh;->bqD:Z

    if-eqz v0, :cond_0

    const-string v0, "need_audio_for_query, "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Ldbh;->bqE:Z

    if-eqz v0, :cond_1

    const-string v0, "need_audio_for_tts, "

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Ldbh;->bqF:Z

    if-eqz v0, :cond_2

    const-string v0, "need_audio_for_pending_beeps, "

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Ldbh;->bqG:Z

    if-eqz v0, :cond_3

    const-string v0, "system_spoken_feedback_enabled, "

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_3
.end method
