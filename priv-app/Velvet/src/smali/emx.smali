.class final Lemx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field aSM:Legq;

.field final bBZ:J

.field final cfK:J

.field cfL:J

.field cfM:J

.field final mName:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lemx;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    iget-wide v0, p1, Lemx;->cfK:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 354
    iget-object v0, p1, Lemx;->mName:Ljava/lang/String;

    iput-object v0, p0, Lemx;->mName:Ljava/lang/String;

    .line 355
    iget-object v0, p1, Lemx;->aSM:Legq;

    iput-object v0, p0, Lemx;->aSM:Legq;

    .line 356
    iput-wide v2, p0, Lemx;->cfK:J

    .line 357
    iget-wide v0, p1, Lemx;->bBZ:J

    iput-wide v0, p0, Lemx;->bBZ:J

    .line 358
    return-void

    .line 353
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;ZJLegq;)V
    .locals 3

    .prologue
    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    if-nez p1, :cond_0

    const-string p1, "null"

    :cond_0
    iput-object p1, p0, Lemx;->mName:Ljava/lang/String;

    .line 347
    if-eqz p2, :cond_1

    const-wide/16 v0, -0x1

    :goto_0
    iput-wide v0, p0, Lemx;->cfK:J

    .line 348
    iput-wide p3, p0, Lemx;->bBZ:J

    .line 349
    iput-object p5, p0, Lemx;->aSM:Legq;

    .line 350
    return-void

    .line 347
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method final isCancelled()Z
    .locals 4

    .prologue
    .line 365
    iget-wide v0, p0, Lemx;->cfL:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final isPeriodic()Z
    .locals 4

    .prologue
    .line 361
    iget-wide v0, p0, Lemx;->cfK:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
