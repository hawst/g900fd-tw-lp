.class public final Lcxo;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field private bmt:Lcxp;

.field private bmu:Z

.field mAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 111
    return-void
.end method

.method static b(Landroid/accounts/Account;Z)Lcxo;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcxo;

    invoke-direct {v0}, Lcxo;-><init>()V

    .line 48
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 49
    const-string v2, "turn_off_history_key"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 50
    const-string v2, "account_key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 51
    invoke-virtual {v0, v1}, Lcxo;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v0
.end method


# virtual methods
.method public final TE()V
    .locals 2

    .prologue
    .line 95
    invoke-static {p0}, Lcxn;->a(Landroid/app/Fragment;)Lcxn;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcxn;->TD()V

    .line 98
    invoke-virtual {p0}, Lcxo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "opt_out_progress"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 101
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 106
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcxo;->bmt:Lcxp;

    .line 107
    invoke-virtual {p0}, Lcxo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 109
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcxo;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 60
    const-string v0, "account_key"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcxo;->mAccount:Landroid/accounts/Account;

    .line 61
    const-string v0, "turn_off_history_key"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcxo;->bmu:Z

    .line 65
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcxo;->setRetainInstance(Z)V

    .line 67
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v0

    .line 70
    new-instance v1, Lcxp;

    invoke-virtual {p0}, Lcxo;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lcxo;->bmu:Z

    invoke-direct {v1, p0, v2, v3, v0}, Lcxp;-><init>(Lcxo;Landroid/content/Context;ZLfcx;)V

    iput-object v1, p0, Lcxo;->bmt:Lcxp;

    .line 72
    iget-object v0, p0, Lcxo;->bmt:Lcxp;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcxp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 75
    const/16 v0, 0xa3

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 77
    new-instance v1, Ljix;

    invoke-direct {v1}, Ljix;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljix;->qk(I)Ljix;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Ljix;->qi(I)Ljix;

    move-result-object v1

    iput-object v1, v0, Litu;->dIT:Ljix;

    .line 80
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 81
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcxo;->bmt:Lcxp;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcxo;->bmt:Lcxp;

    invoke-virtual {v0}, Lcxp;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcxo;->bmt:Lcxp;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcxp;->cancel(Z)Z

    .line 90
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcxo;->bmt:Lcxp;

    .line 91
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 92
    return-void
.end method
