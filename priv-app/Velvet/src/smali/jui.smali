.class public final Ljui;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eFe:Ljava/lang/String;

.field private eFf:Ljuk;

.field private eFg:Ljuh;

.field private eFh:F

.field private eFi:Z


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljui;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljui;->eFe:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljui;->eFf:Ljuk;

    if-nez v0, :cond_1

    new-instance v0, Ljuk;

    invoke-direct {v0}, Ljuk;-><init>()V

    iput-object v0, p0, Ljui;->eFf:Ljuk;

    :cond_1
    iget-object v0, p0, Ljui;->eFf:Ljuk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljui;->eFg:Ljuh;

    if-nez v0, :cond_2

    new-instance v0, Ljuh;

    invoke-direct {v0}, Ljuh;-><init>()V

    iput-object v0, p0, Ljui;->eFg:Ljuh;

    :cond_2
    iget-object v0, p0, Ljui;->eFg:Ljuh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljui;->eFh:F

    iget v0, p0, Ljui;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljui;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljui;->eFi:Z

    iget v0, p0, Ljui;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljui;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x1

    iget-object v1, p0, Ljui;->eFe:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 94
    iget-object v0, p0, Ljui;->eFf:Ljuk;

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x2

    iget-object v1, p0, Ljui;->eFf:Ljuk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 97
    :cond_0
    iget-object v0, p0, Ljui;->eFg:Ljuh;

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x3

    iget-object v1, p0, Ljui;->eFg:Ljuh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 100
    :cond_1
    iget v0, p0, Ljui;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 101
    const/4 v0, 0x4

    iget v1, p0, Ljui;->eFh:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 103
    :cond_2
    iget v0, p0, Ljui;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 104
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljui;->eFi:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 106
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 107
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 111
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 112
    const/4 v1, 0x1

    iget-object v2, p0, Ljui;->eFe:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    iget-object v1, p0, Ljui;->eFf:Ljuk;

    if-eqz v1, :cond_0

    .line 115
    const/4 v1, 0x2

    iget-object v2, p0, Ljui;->eFf:Ljuk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_0
    iget-object v1, p0, Ljui;->eFg:Ljuh;

    if-eqz v1, :cond_1

    .line 119
    const/4 v1, 0x3

    iget-object v2, p0, Ljui;->eFg:Ljuh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_1
    iget v1, p0, Ljui;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 123
    const/4 v1, 0x4

    iget v2, p0, Ljui;->eFh:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 126
    :cond_2
    iget v1, p0, Ljui;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 127
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljui;->eFi:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 130
    :cond_3
    return v0
.end method
