.class public final Ljxu;
.super Lbqv;


# instance fields
.field private eLe:J

.field private eLf:I

.field private eLg:I

.field private eLh:Z

.field private eLi:[Ljxv;

.field private eLj:Ljxt;

.field private eLk:[B

.field private eLl:[B

.field private eLm:[B

.field private eLn:Ljxs;

.field private eLo:Ljava/lang/String;

.field private tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lbqv;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljxu;->eLe:J

    const-string v0, ""

    iput-object v0, p0, Ljxu;->tag:Ljava/lang/String;

    iput v2, p0, Ljxu;->eLf:I

    iput v2, p0, Ljxu;->eLg:I

    iput-boolean v2, p0, Ljxu;->eLh:Z

    invoke-static {}, Ljxv;->bvP()[Ljxv;

    move-result-object v0

    iput-object v0, p0, Ljxu;->eLi:[Ljxv;

    iput-object v3, p0, Ljxu;->eLj:Ljxt;

    sget-object v0, Lbrc;->aHv:[B

    iput-object v0, p0, Ljxu;->eLk:[B

    sget-object v0, Lbrc;->aHv:[B

    iput-object v0, p0, Ljxu;->eLl:[B

    sget-object v0, Lbrc;->aHv:[B

    iput-object v0, p0, Ljxu;->eLm:[B

    iput-object v3, p0, Ljxu;->eLn:Ljxs;

    const-string v0, ""

    iput-object v0, p0, Ljxu;->eLo:Ljava/lang/String;

    iput-object v3, p0, Ljxu;->aHk:Lbqw;

    const/4 v0, -0x1

    iput v0, p0, Ljxu;->aHr:I

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljxu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljxu;

    cmp-long v2, v4, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljxu;->tag:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Ljxu;->tag:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljxu;->tag:Ljava/lang/String;

    iget-object v3, p1, Ljxu;->tag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljxu;->eLi:[Ljxv;

    iget-object v3, p1, Ljxu;->eLi:[Ljxv;

    invoke-static {v2, v3}, Lbqy;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljxu;->eLk:[B

    iget-object v3, p1, Ljxu;->eLk:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljxu;->eLl:[B

    iget-object v3, p1, Ljxu;->eLl:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljxu;->eLm:[B

    iget-object v3, p1, Ljxu;->eLm:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljxu;->eLo:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Ljxu;->eLo:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljxu;->eLo:Ljava/lang/String;

    iget-object v3, p1, Ljxu;->eLo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljxu;->tag:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x3fd1

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit16 v0, v0, 0x4d5

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljxu;->eLi:[Ljxv;

    invoke-static {v2}, Lbqy;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljxu;->eLk:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljxu;->eLl:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljxu;->eLm:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljxu;->eLo:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Ljxu;->tag:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ljxu;->eLo:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
