.class final Ldij;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgh;


# instance fields
.field private final byQ:Ljava/util/Map;

.field private final mObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Ldij;->mObservable:Landroid/database/DataSetObservable;

    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldij;->byQ:Ljava/util/Map;

    .line 154
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 185
    const-string v0, "Visible CP sources"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Ldij;->abS()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->f(Ljava/lang/Iterable;)V

    .line 187
    return-void
.end method

.method public final abS()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 178
    iget-object v1, p0, Ldij;->byQ:Ljava/util/Map;

    monitor-enter v1

    .line 179
    :try_start_0
    iget-object v0, p0, Ldij;->byQ:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i(Ljava/util/Collection;)V
    .locals 5

    .prologue
    .line 157
    iget-object v1, p0, Ldij;->byQ:Ljava/util/Map;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-object v0, p0, Ldij;->byQ:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 159
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldiq;

    .line 160
    iget-object v3, p0, Ldij;->byQ:Ljava/util/Map;

    invoke-interface {v0}, Ldiq;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    iget-object v0, p0, Ldij;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 164
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Ldij;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 169
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Ldij;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 174
    return-void
.end method
