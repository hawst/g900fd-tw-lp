.class public final Ldhd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field public final bxH:Ldgm;

.field public final mBgExecutor:Ljava/util/concurrent/Executor;

.field public final mConfig:Lcjs;

.field public final mContext:Landroid/content/Context;

.field public final mGooglePlayServicesHelper:Lcha;

.field public final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcjs;Lchk;Lcha;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Ldhd;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Ldhd;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 56
    iput-object p3, p0, Ldhd;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 57
    iput-object p4, p0, Ldhd;->mConfig:Lcjs;

    .line 58
    iput-object p6, p0, Ldhd;->mGooglePlayServicesHelper:Lcha;

    .line 60
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const v1, 0xf4240

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-virtual {p4}, Lcjs;->MC()I

    move-result v1

    if-ge v0, v1, :cond_1

    move v5, v4

    .line 63
    :goto_0
    new-instance v0, Lbhj;

    invoke-direct {v0, p1}, Lbhj;-><init>(Landroid/content/Context;)V

    sget-object v1, Lbut;->aLm:Lbgx;

    invoke-virtual {v0, v1}, Lbhj;->a(Lbgx;)Lbhj;

    move-result-object v0

    invoke-virtual {v0}, Lbhj;->yx()Lbhi;

    move-result-object v1

    .line 66
    new-instance v0, Ldgm;

    new-instance v2, Lbbt;

    iget-object v6, p0, Ldhd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Lbbt;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v5}, Ldgm;-><init>(Lbhi;Lbbt;ZZZ)V

    iput-object v0, p0, Ldhd;->bxH:Ldgm;

    .line 69
    iget-object v0, p0, Ldhd;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->ML()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Ldhe;

    invoke-direct {v0, p0}, Ldhe;-><init>(Ldhd;)V

    invoke-virtual {p6, v0}, Lcha;->b(Lemy;)V

    .line 79
    new-instance v0, Ldhf;

    invoke-direct {v0, p0}, Ldhf;-><init>(Ldhd;)V

    invoke-virtual {p6, v0}, Lcha;->a(Lchg;)V

    .line 87
    new-instance v0, Ldhg;

    invoke-direct {v0, p0}, Ldhg;-><init>(Ldhd;)V

    invoke-virtual {p5, v0}, Lchk;->a(Lchm;)V

    .line 96
    :cond_0
    return-void

    :cond_1
    move v5, v3

    .line 60
    goto :goto_0
.end method


# virtual methods
.method final a(Ldhu;Lemy;I)V
    .locals 2

    .prologue
    .line 199
    invoke-virtual {p0, p3}, Ldhd;->fQ(I)Z

    move-result v0

    .line 200
    if-eqz v0, :cond_0

    .line 201
    iget-object v1, p0, Ldhd;->bxH:Ldgm;

    invoke-virtual {v1, v0}, Ldgm;->du(Z)V

    .line 202
    iget-object v0, p0, Ldhd;->bxH:Ldgm;

    invoke-virtual {v0, p2}, Ldgm;->d(Lemy;)V

    .line 206
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-interface {p1}, Ldhu;->clear()V

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Ldhd;->bxH:Ldgm;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 211
    return-void
.end method

.method final fQ(I)Z
    .locals 1

    .prologue
    .line 101
    if-nez p1, :cond_0

    iget-object v0, p0, Ldhd;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->ML()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
