.class abstract Limr;
.super Liir;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3L


# instance fields
.field final dCT:Liml;

.field transient dEO:Ljava/util/concurrent/ConcurrentMap;

.field final dEp:I

.field final dEq:I

.field final dEr:Linz;

.field final dEs:Linz;

.field final dEt:J

.field final dEu:J

.field final dEw:Lifc;

.field final dEx:Lifc;


# direct methods
.method constructor <init>(Linz;Linz;Lifc;Lifc;JJIILiml;Ljava/util/concurrent/ConcurrentMap;)V
    .locals 1

    .prologue
    .line 3967
    invoke-direct {p0}, Liir;-><init>()V

    .line 3968
    iput-object p1, p0, Limr;->dEr:Linz;

    .line 3969
    iput-object p2, p0, Limr;->dEs:Linz;

    .line 3970
    iput-object p3, p0, Limr;->dEw:Lifc;

    .line 3971
    iput-object p4, p0, Limr;->dEx:Lifc;

    .line 3972
    iput-wide p5, p0, Limr;->dEt:J

    .line 3973
    iput-wide p7, p0, Limr;->dEu:J

    .line 3974
    iput p9, p0, Limr;->dEq:I

    .line 3975
    iput p10, p0, Limr;->dEp:I

    .line 3976
    iput-object p11, p0, Limr;->dCT:Liml;

    .line 3977
    iput-object p12, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    .line 3978
    return-void
.end method


# virtual methods
.method protected final aWO()Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    .prologue
    .line 3982
    iget-object v0, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method protected final bridge synthetic aWP()Ljava/util/Map;
    .locals 1

    .prologue
    .line 3947
    iget-object v0, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method protected final bridge synthetic aWl()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3947
    iget-object v0, p0, Limr;->dEO:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method
