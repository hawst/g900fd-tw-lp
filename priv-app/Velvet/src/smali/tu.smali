.class public final Ltu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public DA:I

.field public DB:I

.field private DC:I

.field private DD:I

.field public DE:I

.field public DF:I

.field private DG:F

.field public DH:F

.field public DI:I

.field public DJ:I

.field public DK:I

.field public DL:I

.field public DM:I

.field public DN:I

.field public DO:I

.field public DP:I

.field private DQ:I

.field public DR:I

.field public DS:I

.field public DT:I

.field public DU:I

.field DV:I

.field public DW:I

.field public DX:I

.field public DY:I

.field public DZ:I

.field De:F

.field Df:F

.field public Dg:F

.field public Dh:F

.field public Di:F

.field Dj:F

.field private Dk:F

.field private Dl:I

.field private Dm:F

.field public Dn:I

.field public Do:I

.field public Dp:Z

.field public Dq:Z

.field public Dr:Z

.field private Ds:Z

.field public Dt:Z

.field private Du:I

.field public Dv:I

.field private Dw:Landroid/graphics/Rect;

.field public Dx:I

.field public Dy:I

.field public Dz:I

.field public Ea:I

.field public Eb:I

.field private Ec:I

.field public Ed:I

.field private Ee:I

.field public Ef:I

.field public Eg:I

.field private Eh:F

.field Ei:I

.field Ej:I

.field public Ek:Ljava/util/ArrayList;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput v0, p0, Ltu;->Ei:I

    .line 134
    iput v0, p0, Ltu;->Ej:I

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltu;->Ek:Ljava/util/ArrayList;

    .line 159
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;FFIIIILandroid/content/res/Resources;)V
    .locals 8

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const/4 v1, -0x1

    iput v1, p0, Ltu;->Ei:I

    .line 134
    const/4 v1, -0x1

    iput v1, p0, Ltu;->Ej:I

    .line 136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ltu;->Ek:Ljava/util/ArrayList;

    .line 167
    invoke-virtual/range {p9 .. p9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 168
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 170
    const v1, 0x7f0e0009

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Ltu;->Dt:Z

    .line 172
    iput p3, p0, Ltu;->De:F

    .line 173
    iput p4, p0, Ltu;->Df:F

    .line 175
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v4, 0x0

    invoke-static {p1, v1, v4}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Ltu;->Dw:Landroid/graphics/Rect;

    .line 178
    const v1, 0x7f0d0064

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->Dv:I

    .line 179
    iget v1, p0, Ltu;->Dv:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Ltu;->Du:I

    .line 180
    const v1, 0x7f0d0067

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->Ef:I

    .line 182
    const v1, 0x7f0d006a

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->DB:I

    .line 184
    const v1, 0x7f0d0069

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->DQ:I

    .line 186
    const v1, 0x7f0d006b

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->DC:I

    .line 188
    const v1, 0x7f0d006c

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->DD:I

    .line 190
    const v1, 0x7f0d006d

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->DE:I

    .line 192
    const v1, 0x7f0d006e

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->DF:I

    .line 194
    const v1, 0x7f0c0008

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v1, v4

    iput v1, p0, Ltu;->DG:F

    .line 196
    const v1, 0x7f0c0009

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v1, v4

    iput v1, p0, Ltu;->DH:F

    .line 200
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltu;

    .line 201
    new-instance v5, Ltx;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v6}, Ltx;-><init>(Ltu;F)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 203
    :cond_0
    invoke-direct {p0, p3, p4, v3}, Ltu;->a(FFLjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltx;

    iget-object v1, v1, Ltx;->En:Ltu;

    .line 206
    iget v4, v1, Ltu;->Dg:F

    iput v4, p0, Ltu;->Dg:F

    .line 209
    iget v4, v1, Ltu;->Dh:F

    iput v4, p0, Ltu;->Dh:F

    .line 212
    iget v4, v1, Ltu;->Di:F

    iput v4, p0, Ltu;->Di:F

    .line 213
    iget v4, p0, Ltu;->Di:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Ltu;->DY:I

    .line 216
    iget v4, v1, Ltu;->Dn:I

    iput v4, p0, Ltu;->Dn:I

    .line 219
    iget v1, v1, Ltu;->Do:I

    iput v1, p0, Ltu;->Do:I

    .line 222
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 223
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltu;

    .line 224
    new-instance v5, Ltx;

    iget v6, v1, Ltu;->Dj:F

    invoke-direct {v5, v1, v6}, Ltx;-><init>(Ltu;F)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 226
    :cond_1
    invoke-direct {p0, p3, p4, v3}, Ltu;->b(FFLjava/util/ArrayList;)F

    move-result v1

    iput v1, p0, Ltu;->Dj:F

    .line 229
    iget v1, p0, Ltu;->Dj:F

    invoke-static {v1, v2}, Lur;->a(FLandroid/util/DisplayMetrics;)I

    move-result v1

    iput v1, p0, Ltu;->DN:I

    .line 232
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 233
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltu;

    .line 234
    new-instance v5, Ltx;

    iget v6, v1, Ltu;->Dk:F

    invoke-direct {v5, v1, v6}, Ltx;-><init>(Ltu;F)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 236
    :cond_2
    invoke-direct {p0, p3, p4, v3}, Ltu;->b(FFLjava/util/ArrayList;)F

    move-result v1

    iput v1, p0, Ltu;->Dk:F

    .line 237
    const v1, 0x7f0d0068

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Ltu;->Dl:I

    .line 240
    iget v1, p0, Ltu;->Dk:F

    invoke-static {v1, v2}, Lur;->a(FLandroid/util/DisplayMetrics;)I

    .line 243
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 244
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltu;

    .line 245
    new-instance v5, Ltx;

    iget v6, v1, Ltu;->Dm:F

    invoke-direct {v5, v1, v6}, Ltx;-><init>(Ltu;F)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 248
    :cond_3
    invoke-direct {p0, p3, p4, v3}, Ltu;->b(FFLjava/util/ArrayList;)F

    move-result v1

    iput v1, p0, Ltu;->Dm:F

    .line 251
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lacv;->a(Landroid/content/pm/PackageManager;)Lacv;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1, v2}, Lacv;->a(Landroid/util/DisplayMetrics;)Ltu;

    move-result-object v1

    if-eqz v1, :cond_6

    iget v3, v1, Ltu;->Dg:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget v3, v1, Ltu;->Dh:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget v3, v1, Ltu;->Dg:F

    iput v3, p0, Ltu;->Dg:F

    iget v3, v1, Ltu;->Dh:F

    iput v3, p0, Ltu;->Dh:F

    :cond_4
    iget v3, v1, Ltu;->Ei:I

    if-lez v3, :cond_5

    iget v3, v1, Ltu;->Ej:I

    if-lez v3, :cond_5

    iget v3, v1, Ltu;->Ei:I

    iput v3, p0, Ltu;->Ei:I

    iget v3, v1, Ltu;->Ej:I

    iput v3, p0, Ltu;->Ej:I

    :cond_5
    iget v3, v1, Ltu;->Dj:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6

    iget v1, v1, Ltu;->Dj:F

    iput v1, p0, Ltu;->Dj:F

    iget v1, p0, Ltu;->Dj:F

    invoke-static {v1, v2}, Lur;->a(FLandroid/util/DisplayMetrics;)I

    move-result v1

    iput v1, p0, Ltu;->DN:I

    :cond_6
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p9

    move v4, p5

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    .line 254
    invoke-virtual/range {v1 .. v7}, Ltu;->a(Landroid/content/Context;Landroid/content/res/Resources;IIII)V

    .line 255
    invoke-direct {p0, p1}, Ltu;->i(Landroid/content/Context;)V

    .line 256
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-static {}, Lyu;->ij()Lyu;

    iget v2, p0, Ltu;->DW:I

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v1, v3, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Ltu;->Eg:I

    .line 257
    return-void
.end method

.method constructor <init>(Ljava/lang/String;FFFFFFFFII)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput v0, p0, Ltu;->Ei:I

    .line 134
    iput v0, p0, Ltu;->Ej:I

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltu;->Ek:Ljava/util/ArrayList;

    .line 141
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-nez v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    rem-float v0, p8, v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "All Device Profiles must have an odd number of hotseat spaces"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    iput p2, p0, Ltu;->De:F

    .line 147
    iput p3, p0, Ltu;->Df:F

    .line 148
    iput p4, p0, Ltu;->Dg:F

    .line 149
    iput p5, p0, Ltu;->Dh:F

    .line 150
    iput p6, p0, Ltu;->Dj:F

    .line 151
    iput p7, p0, Ltu;->Dk:F

    .line 152
    iput p8, p0, Ltu;->Di:F

    .line 153
    iput p9, p0, Ltu;->Dm:F

    .line 154
    iput p10, p0, Ltu;->Dn:I

    .line 155
    iput p11, p0, Ltu;->Do:I

    .line 156
    return-void
.end method

.method static a(Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 4

    .prologue
    .line 453
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(Landroid/graphics/PointF;Landroid/graphics/PointF;F)F
    .locals 6

    .prologue
    .line 459
    invoke-static {p1, p2}, Ltu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v0

    .line 460
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    .line 461
    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 463
    :goto_0
    return v0

    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    div-double v0, v2, v0

    double-to-float v0, v0

    goto :goto_0
.end method

.method private a(FFLjava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 475
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 479
    new-instance v1, Ltv;

    invoke-direct {v1, p0, v0}, Ltv;-><init>(Ltu;Landroid/graphics/PointF;)V

    invoke-static {p3, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 485
    return-object p3
.end method

.method private a(FILandroid/content/res/Resources;Landroid/util/DisplayMetrics;)V
    .locals 6

    .prologue
    .line 373
    iget v0, p0, Ltu;->Dj:F

    invoke-static {v0, p4}, Lur;->a(FLandroid/util/DisplayMetrics;)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Ltu;->DI:I

    .line 374
    iget v0, p0, Ltu;->Dk:F

    invoke-static {v0, p4}, Lur;->b(FLandroid/util/DisplayMetrics;)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Ltu;->DJ:I

    .line 375
    iput p2, p0, Ltu;->DK:I

    .line 376
    iget v0, p0, Ltu;->Dm:F

    invoke-static {v0, p4}, Lur;->a(FLandroid/util/DisplayMetrics;)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Ltu;->DW:I

    .line 379
    const v0, 0x7f0d0065

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ltu;->Ec:I

    .line 380
    const v0, 0x7f0d0066

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ltu;->Ee:I

    .line 381
    iget v0, p0, Ltu;->Ec:I

    iget v1, p0, Ltu;->Dx:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ltu;->Eb:I

    .line 382
    iget v0, p0, Ltu;->Ee:I

    invoke-virtual {p0}, Ltu;->fz()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->Ed:I

    .line 385
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 386
    iget v1, p0, Ltu;->DJ:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 387
    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 388
    iget v1, p0, Ltu;->DI:I

    iput v1, p0, Ltu;->DL:I

    .line 389
    iget v1, p0, Ltu;->DI:I

    iget v2, p0, Ltu;->DK:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float v0, v2, v0

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->DM:I

    .line 390
    const v0, 0x7f0d0081

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 391
    iget v1, p0, Ltu;->DI:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Ltu;->DI:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Ltu;->Eh:F

    .line 394
    iget v0, p0, Ltu;->DI:I

    iget v1, p0, Ltu;->Dv:I

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->DX:I

    .line 395
    iget v0, p0, Ltu;->DI:I

    .line 396
    iget v0, p0, Ltu;->DI:I

    iput v0, p0, Ltu;->DV:I

    .line 399
    iget v0, p0, Ltu;->DL:I

    iget v1, p0, Ltu;->Dv:I

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->DT:I

    .line 400
    iget v0, p0, Ltu;->DM:I

    iget v1, p0, Ltu;->Dv:I

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->DU:I

    .line 401
    iget v0, p0, Ltu;->Dv:I

    neg-int v0, v0

    iput v0, p0, Ltu;->DR:I

    .line 402
    iget v0, p0, Ltu;->DI:I

    iget v1, p0, Ltu;->DR:I

    neg-int v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->DS:I

    .line 405
    iget-boolean v0, p0, Ltu;->Dp:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Ltu;->ba(I)Landroid/graphics/Rect;

    .line 407
    const v0, 0x7f0d007e

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 409
    iget v0, p0, Ltu;->DN:I

    iput v0, p0, Ltu;->DO:I

    .line 410
    iget v0, p0, Ltu;->DN:I

    add-int/2addr v0, p2

    iget v1, p0, Ltu;->DJ:I

    add-int/2addr v0, v1

    iput v0, p0, Ltu;->DP:I

    .line 411
    const v0, 0x7f0c0005

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 413
    const v0, 0x7f0c0006

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 415
    const v0, 0x7f0c0007

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 417
    iget-boolean v0, p0, Ltu;->Dp:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 418
    :goto_1
    iget-boolean v4, p0, Ltu;->Dp:Z

    if-eqz v4, :cond_2

    .line 420
    :goto_2
    iget v1, p0, Ltu;->Ei:I

    if-lez v1, :cond_5

    iget v1, p0, Ltu;->Ej:I

    if-lez v1, :cond_5

    .line 421
    iget-boolean v0, p0, Ltu;->Dp:Z

    if-eqz v0, :cond_3

    iget v0, p0, Ltu;->Ei:I

    :goto_3
    iput v0, p0, Ltu;->DZ:I

    .line 422
    iget-boolean v0, p0, Ltu;->Dp:Z

    if-eqz v0, :cond_4

    iget v0, p0, Ltu;->Ej:I

    :goto_4
    iput v0, p0, Ltu;->Ea:I

    .line 431
    :goto_5
    return-void

    .line 405
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 417
    goto :goto_1

    :cond_2
    move v2, v1

    .line 418
    goto :goto_2

    .line 421
    :cond_3
    iget v0, p0, Ltu;->Ej:I

    goto :goto_3

    .line 422
    :cond_4
    iget v0, p0, Ltu;->Ei:I

    goto :goto_4

    .line 424
    :cond_5
    iget v1, p0, Ltu;->DA:I

    iget v4, p0, Ltu;->Ef:I

    sub-int/2addr v1, v4

    iget v4, p0, Ltu;->DP:I

    iget v5, p0, Ltu;->DQ:I

    add-int/2addr v4, v5

    div-int/2addr v1, v4

    iput v1, p0, Ltu;->DZ:I

    .line 426
    iget v1, p0, Ltu;->DZ:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ltu;->DZ:I

    .line 427
    iget v0, p0, Ltu;->Dz:I

    iget v1, p0, Ltu;->DO:I

    iget v4, p0, Ltu;->DQ:I

    add-int/2addr v1, v4

    div-int/2addr v0, v1

    iput v0, p0, Ltu;->Ea:I

    .line 429
    iget v0, p0, Ltu;->Ea:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ltu;->Ea:I

    goto :goto_5
.end method

.method private b(FFLjava/util/ArrayList;)F
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/high16 v10, 0x40a00000    # 5.0f

    const/high16 v9, 0x40400000    # 3.0f

    const/4 v2, 0x0

    .line 490
    .line 492
    new-instance v5, Landroid/graphics/PointF;

    invoke-direct {v5, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 496
    invoke-direct {p0, p1, p2, p3}, Ltu;->a(FFLjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    move v1, v2

    move v3, v4

    .line 499
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 500
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltx;

    .line 501
    int-to-float v7, v3

    cmpg-float v7, v7, v9

    if-gez v7, :cond_4

    .line 502
    iget-object v7, v0, Ltx;->Eq:Landroid/graphics/PointF;

    invoke-direct {p0, v5, v7, v10}, Ltu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;F)F

    move-result v7

    .line 503
    const/high16 v8, 0x7f800000    # Float.POSITIVE_INFINITY

    cmpl-float v8, v7, v8

    if-nez v8, :cond_1

    .line 504
    iget v2, v0, Ltx;->value:F

    .line 518
    :cond_0
    return v2

    .line 506
    :cond_1
    add-float v0, v1, v7

    .line 499
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 510
    :cond_2
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 511
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltx;

    .line 512
    int-to-float v3, v4

    cmpg-float v3, v3, v9

    if-gez v3, :cond_3

    .line 513
    iget-object v3, v0, Ltx;->Eq:Landroid/graphics/PointF;

    invoke-direct {p0, v5, v3, v10}, Ltu;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;F)F

    move-result v3

    .line 514
    iget v0, v0, Ltx;->value:F

    mul-float/2addr v0, v3

    div-float/2addr v0, v1

    add-float/2addr v2, v0

    .line 510
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static b(Landroid/view/ViewGroup;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 702
    move v1, v0

    .line 703
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 704
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 705
    add-int/lit8 v1, v1, 0x1

    .line 703
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 708
    :cond_1
    return v1
.end method

.method private i(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 325
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 327
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 328
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 329
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 330
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    .line 338
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 339
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 340
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 341
    invoke-virtual {v0, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 342
    invoke-virtual {v0, v6, v7}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 343
    iget v0, v5, Landroid/graphics/Point;->x:I

    iput v0, p0, Ltu;->Dz:I

    .line 344
    iget v0, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    .line 345
    iget v0, v6, Landroid/graphics/Point;->y:I

    iput v0, p0, Ltu;->DA:I

    .line 353
    :goto_0
    iget v0, p0, Ltu;->Dl:I

    .line 354
    invoke-direct {p0, v1, v0, v2, v3}, Ltu;->a(FILandroid/content/res/Resources;Landroid/util/DisplayMetrics;)V

    .line 355
    iget v4, p0, Ltu;->DM:I

    int-to-float v4, v4

    iget v5, p0, Ltu;->Dg:F

    mul-float/2addr v4, v5

    .line 357
    invoke-virtual {p0}, Ltu;->fA()Landroid/graphics/Rect;

    move-result-object v5

    .line 358
    iget v6, p0, Ltu;->DA:I

    iget v7, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v5, v6, v5

    .line 359
    int-to-float v6, v5

    cmpl-float v6, v4, v6

    if-lez v6, :cond_0

    .line 360
    int-to-float v0, v5

    div-float v1, v0, v4

    .line 361
    const/4 v0, 0x0

    .line 363
    :cond_0
    invoke-direct {p0, v1, v0, v2, v3}, Ltu;->a(FILandroid/content/res/Resources;Landroid/util/DisplayMetrics;)V

    .line 366
    iget-object v0, p0, Ltu;->Ek:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltw;

    .line 367
    invoke-interface {v0, p0}, Ltw;->a(Ltu;)V

    goto :goto_1

    .line 347
    :cond_1
    iget v0, v7, Landroid/graphics/Point;->y:I

    iput v0, p0, Ltu;->DA:I

    goto :goto_0

    .line 369
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/res/Resources;IIII)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 435
    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    .line 436
    iget v0, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ltu;->Dp:Z

    .line 437
    const v0, 0x7f0e0005

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Ltu;->Dq:Z

    .line 438
    const v0, 0x7f0e0006

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Ltu;->Dr:Z

    .line 439
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v0, v4, :cond_2

    .line 440
    invoke-virtual {v3}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Ltu;->Ds:Z

    .line 444
    :goto_2
    iput p3, p0, Ltu;->Dx:I

    .line 445
    iput p4, p0, Ltu;->Dy:I

    .line 446
    iput p5, p0, Ltu;->Dz:I

    .line 447
    iput p6, p0, Ltu;->DA:I

    .line 449
    invoke-direct {p0, p1}, Ltu;->i(Landroid/content/Context;)V

    .line 450
    return-void

    :cond_0
    move v0, v2

    .line 436
    goto :goto_0

    :cond_1
    move v1, v2

    .line 440
    goto :goto_1

    .line 442
    :cond_2
    iput-boolean v2, p0, Ltu;->Ds:Z

    goto :goto_2
.end method

.method public final aZ(I)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 536
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 537
    if-nez p1, :cond_1

    iget-boolean v0, p0, Ltu;->Dt:Z

    if-eqz v0, :cond_1

    .line 539
    iget-boolean v0, p0, Ltu;->Ds:Z

    if-eqz v0, :cond_0

    .line 540
    iget v0, p0, Ltu;->Dz:I

    iget v2, p0, Ltu;->Ed:I

    sub-int/2addr v0, v2

    iget v2, p0, Ltu;->Dv:I

    iget v3, p0, Ltu;->Dz:I

    iget v4, p0, Ltu;->DA:I

    iget v5, p0, Ltu;->Dv:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 567
    :goto_0
    return-object v1

    .line 543
    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Ltu;->Dv:I

    iget v3, p0, Ltu;->Ed:I

    iget v4, p0, Ltu;->DA:I

    iget v5, p0, Ltu;->Dv:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 547
    :cond_1
    iget-boolean v0, p0, Ltu;->Dq:Z

    if-eqz v0, :cond_3

    .line 550
    if-nez p1, :cond_2

    iget v0, p0, Ltu;->Dx:I

    iget v2, p0, Ltu;->Dy:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 555
    :goto_1
    iget v2, p0, Ltu;->Dv:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget v2, p0, Ltu;->Dh:F

    iget v3, p0, Ltu;->DL:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, Ltu;->Dh:F

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    mul-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 557
    iget v2, p0, Ltu;->Dv:I

    add-int/2addr v2, v0

    invoke-virtual {p0}, Ltu;->fz()I

    move-result v3

    iget v4, p0, Ltu;->Dz:I

    iget v5, p0, Ltu;->Dv:I

    add-int/2addr v0, v5

    sub-int v0, v4, v0

    iget v4, p0, Ltu;->Ed:I

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 550
    :cond_2
    iget v0, p0, Ltu;->Dx:I

    iget v2, p0, Ltu;->Dy:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    .line 561
    :cond_3
    iget v0, p0, Ltu;->Du:I

    iget-object v2, p0, Ltu;->Dw:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Ltu;->fz()I

    move-result v2

    iget v3, p0, Ltu;->Dz:I

    iget v4, p0, Ltu;->Du:I

    iget-object v5, p0, Ltu;->Dw:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    sub-int/2addr v3, v4

    iget v4, p0, Ltu;->Ed:I

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public final ba(I)Landroid/graphics/Rect;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 596
    invoke-virtual {p0, p1}, Ltu;->aZ(I)Landroid/graphics/Rect;

    move-result-object v2

    .line 597
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 598
    if-nez p1, :cond_1

    iget-boolean v0, p0, Ltu;->Dt:Z

    if-eqz v0, :cond_1

    .line 601
    iget-boolean v0, p0, Ltu;->Ds:Z

    if-eqz v0, :cond_0

    .line 602
    iget v0, p0, Ltu;->DX:I

    iget v1, p0, Ltu;->Dv:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v4, p0, Ltu;->Dv:I

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 635
    :goto_0
    return-object v3

    .line 605
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Ltu;->Dv:I

    iget v2, p0, Ltu;->DX:I

    iget v4, p0, Ltu;->Dv:I

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 609
    :cond_1
    iget-boolean v0, p0, Ltu;->Dq:Z

    if-eqz v0, :cond_4

    .line 612
    iget v0, p0, Ltu;->Eh:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v8

    add-float v4, v1, v0

    .line 613
    if-nez p1, :cond_2

    iget v0, p0, Ltu;->Dx:I

    iget v1, p0, Ltu;->Dy:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 616
    :goto_1
    if-eqz p1, :cond_3

    iget v0, p0, Ltu;->Dx:I

    iget v5, p0, Ltu;->Dy:I

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 619
    :goto_2
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 620
    iget v5, p0, Ltu;->DX:I

    iget v6, p0, Ltu;->Ef:I

    add-int/2addr v5, v6

    .line 621
    iget v6, p0, Ltu;->Dh:F

    iget v7, p0, Ltu;->DL:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    iget v7, p0, Ltu;->Dh:F

    mul-float/2addr v4, v7

    iget v7, p0, Ltu;->DL:I

    int-to-float v7, v7

    mul-float/2addr v4, v7

    add-float/2addr v4, v6

    float-to-int v4, v4

    sub-int/2addr v1, v4

    invoke-static {v9, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 623
    sub-int/2addr v0, v2

    sub-int/2addr v0, v5

    iget v4, p0, Ltu;->Dg:F

    mul-float/2addr v4, v8

    iget v6, p0, Ltu;->DM:I

    int-to-float v6, v6

    mul-float/2addr v4, v6

    float-to-int v4, v4

    sub-int/2addr v0, v4

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 625
    div-int/lit8 v4, v1, 0x2

    div-int/lit8 v6, v0, 0x2

    add-int/2addr v2, v6

    div-int/lit8 v1, v1, 0x2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    invoke-virtual {v3, v4, v2, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 613
    :cond_2
    iget v0, p0, Ltu;->Dx:I

    iget v1, p0, Ltu;->Dy:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_1

    .line 616
    :cond_3
    iget v0, p0, Ltu;->Dx:I

    iget v5, p0, Ltu;->Dy:I

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    .line 629
    :cond_4
    iget v0, p0, Ltu;->Du:I

    iget-object v1, p0, Ltu;->Dw:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Ltu;->Du:I

    iget-object v4, p0, Ltu;->Dw:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v4

    iget v4, p0, Ltu;->DX:I

    iget v5, p0, Ltu;->Ef:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0
.end method

.method public final fA()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 593
    iget-boolean v0, p0, Ltu;->Dp:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Ltu;->ba(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final fB()Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 652
    iget v0, p0, Ltu;->DG:F

    iget v1, p0, Ltu;->DA:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 653
    iget v1, p0, Ltu;->DD:I

    iget v2, p0, Ltu;->DC:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 655
    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Ltu;->DA:I

    sub-int v0, v2, v0

    iget v2, p0, Ltu;->DA:I

    invoke-direct {v1, v3, v0, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method public final fC()Z
    .locals 1

    .prologue
    .line 694
    iget-boolean v0, p0, Ltu;->Dp:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltu;->Dt:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final fz()I
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Ltu;->Dq:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ltu;->fC()Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    iget v0, p0, Ltu;->Dv:I

    mul-int/lit8 v0, v0, 0x4

    .line 526
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Ltu;->Dv:I

    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method
