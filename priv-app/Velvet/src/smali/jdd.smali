.class public final Ljdd;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHI:Ljava/lang/String;

.field private ebp:Ljava/lang/String;

.field private ebq:Z

.field private ebr:Ljava/lang/String;

.field private ebs:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2377
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2378
    iput v1, p0, Ljdd;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljdd;->ebp:Ljava/lang/String;

    iput-boolean v1, p0, Ljdd;->ebq:Z

    const-string v0, ""

    iput-object v0, p0, Ljdd;->ebr:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljdd;->ebs:I

    const-string v0, ""

    iput-object v0, p0, Ljdd;->dHI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljdd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdd;->eCz:I

    .line 2379
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2246
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdd;->ebp:Ljava/lang/String;

    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdd;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljdd;->ebq:Z

    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdd;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdd;->ebr:Ljava/lang/String;

    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdd;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdd;->ebs:I

    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdd;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdd;->dHI:Ljava/lang/String;

    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdd;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2396
    iget v0, p0, Ljdd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2397
    const/4 v0, 0x1

    iget-object v1, p0, Ljdd;->ebp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2399
    :cond_0
    iget v0, p0, Ljdd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2400
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljdd;->ebq:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2402
    :cond_1
    iget v0, p0, Ljdd;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2403
    const/4 v0, 0x3

    iget-object v1, p0, Ljdd;->ebr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2405
    :cond_2
    iget v0, p0, Ljdd;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2406
    const/4 v0, 0x4

    iget v1, p0, Ljdd;->ebs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2408
    :cond_3
    iget v0, p0, Ljdd;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 2409
    const/4 v0, 0x5

    iget-object v1, p0, Ljdd;->dHI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2411
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2412
    return-void
.end method

.method public final hG(Z)Ljdd;
    .locals 1

    .prologue
    .line 2301
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljdd;->ebq:Z

    .line 2302
    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdd;->aez:I

    .line 2303
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2416
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2417
    iget v1, p0, Ljdd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2418
    const/4 v1, 0x1

    iget-object v2, p0, Ljdd;->ebp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2421
    :cond_0
    iget v1, p0, Ljdd;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2422
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljdd;->ebq:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2425
    :cond_1
    iget v1, p0, Ljdd;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2426
    const/4 v1, 0x3

    iget-object v2, p0, Ljdd;->ebr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2429
    :cond_2
    iget v1, p0, Ljdd;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2430
    const/4 v1, 0x4

    iget v2, p0, Ljdd;->ebs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2433
    :cond_3
    iget v1, p0, Ljdd;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 2434
    const/4 v1, 0x5

    iget-object v2, p0, Ljdd;->dHI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2437
    :cond_4
    return v0
.end method

.method public final tw(Ljava/lang/String;)Ljdd;
    .locals 1

    .prologue
    .line 2279
    if-nez p1, :cond_0

    .line 2280
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2282
    :cond_0
    iput-object p1, p0, Ljdd;->ebp:Ljava/lang/String;

    .line 2283
    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdd;->aez:I

    .line 2284
    return-object p0
.end method

.method public final tx(Ljava/lang/String;)Ljdd;
    .locals 1

    .prologue
    .line 2320
    if-nez p1, :cond_0

    .line 2321
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2323
    :cond_0
    iput-object p1, p0, Ljdd;->ebr:Ljava/lang/String;

    .line 2324
    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdd;->aez:I

    .line 2325
    return-object p0
.end method

.method public final ty(Ljava/lang/String;)Ljdd;
    .locals 1

    .prologue
    .line 2361
    if-nez p1, :cond_0

    .line 2362
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2364
    :cond_0
    iput-object p1, p0, Ljdd;->dHI:Ljava/lang/String;

    .line 2365
    iget v0, p0, Ljdd;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdd;->aez:I

    .line 2366
    return-object p0
.end method
