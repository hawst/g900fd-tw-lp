.class public final Ljyk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eLM:[Ljyk;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private dHr:Ljava/lang/String;

.field private eJm:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 96
    iput v1, p0, Ljyk;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljyk;->bmI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljyk;->dHr:Ljava/lang/String;

    iput v1, p0, Ljyk;->eJm:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljyk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyk;->eCz:I

    .line 97
    return-void
.end method

.method public static bwh()[Ljyk;
    .locals 2

    .prologue
    .line 19
    sget-object v0, Ljyk;->eLM:[Ljyk;

    if-nez v0, :cond_1

    .line 20
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Ljyk;->eLM:[Ljyk;

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Ljyk;

    sput-object v0, Ljyk;->eLM:[Ljyk;

    .line 25
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Ljyk;->eLM:[Ljyk;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyk;->bmI:Ljava/lang/String;

    iget v0, p0, Ljyk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyk;->dHr:Ljava/lang/String;

    iget v0, p0, Ljyk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljyk;->eJm:I

    iget v0, p0, Ljyk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Ljyk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x1

    iget-object v1, p0, Ljyk;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 115
    :cond_0
    iget v0, p0, Ljyk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 116
    const/4 v0, 0x2

    iget-object v1, p0, Ljyk;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 118
    :cond_1
    iget v0, p0, Ljyk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 119
    const/4 v0, 0x3

    iget v1, p0, Ljyk;->eJm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 121
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 122
    return-void
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Ljyk;->bmI:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Ljyk;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 126
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 127
    iget v1, p0, Ljyk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 128
    const/4 v1, 0x1

    iget-object v2, p0, Ljyk;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_0
    iget v1, p0, Ljyk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 132
    const/4 v1, 0x2

    iget-object v2, p0, Ljyk;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_1
    iget v1, p0, Ljyk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 136
    const/4 v1, 0x3

    iget v2, p0, Ljyk;->eJm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_2
    return v0
.end method

.method public final zP(Ljava/lang/String;)Ljyk;
    .locals 1

    .prologue
    .line 38
    if-nez p1, :cond_0

    .line 39
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41
    :cond_0
    iput-object p1, p0, Ljyk;->bmI:Ljava/lang/String;

    .line 42
    iget v0, p0, Ljyk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyk;->aez:I

    .line 43
    return-object p0
.end method

.method public final zQ(Ljava/lang/String;)Ljyk;
    .locals 1

    .prologue
    .line 60
    if-nez p1, :cond_0

    .line 61
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63
    :cond_0
    iput-object p1, p0, Ljyk;->dHr:Ljava/lang/String;

    .line 64
    iget v0, p0, Ljyk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyk;->aez:I

    .line 65
    return-object p0
.end method
