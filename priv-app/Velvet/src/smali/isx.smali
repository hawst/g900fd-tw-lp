.class public final Lisx;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dHd:[Lisx;


# instance fields
.field private aez:I

.field private dHe:Ljava/lang/String;

.field private dHf:Ljava/lang/String;

.field private dHg:Z

.field private dHh:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 220
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 221
    iput v1, p0, Lisx;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lisx;->dHe:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lisx;->dHf:Ljava/lang/String;

    iput-boolean v1, p0, Lisx;->dHg:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lisx;->dHh:J

    const/4 v0, 0x0

    iput-object v0, p0, Lisx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lisx;->eCz:I

    .line 222
    return-void
.end method

.method public static aYw()[Lisx;
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lisx;->dHd:[Lisx;

    if-nez v0, :cond_1

    .line 126
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    sget-object v0, Lisx;->dHd:[Lisx;

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    new-array v0, v0, [Lisx;

    sput-object v0, Lisx;->dHd:[Lisx;

    .line 131
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_1
    sget-object v0, Lisx;->dHd:[Lisx;

    return-object v0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lisx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lisx;->dHe:Ljava/lang/String;

    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lisx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lisx;->dHf:Ljava/lang/String;

    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lisx;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lisx;->dHg:Z

    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lisx;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lisx;->dHh:J

    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lisx;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 238
    iget v0, p0, Lisx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 239
    const/4 v0, 0x1

    iget-object v1, p0, Lisx;->dHe:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 241
    :cond_0
    iget v0, p0, Lisx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 242
    const/4 v0, 0x2

    iget-object v1, p0, Lisx;->dHf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 244
    :cond_1
    iget v0, p0, Lisx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 245
    const/4 v0, 0x3

    iget-boolean v1, p0, Lisx;->dHg:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 247
    :cond_2
    iget v0, p0, Lisx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 248
    const/4 v0, 0x4

    iget-wide v2, p0, Lisx;->dHh:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 250
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 251
    return-void
.end method

.method public final aYx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lisx;->dHe:Ljava/lang/String;

    return-object v0
.end method

.method public final aYy()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lisx;->dHg:Z

    return v0
.end method

.method public final aYz()J
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lisx;->dHh:J

    return-wide v0
.end method

.method public final bT(J)Lisx;
    .locals 1

    .prologue
    .line 207
    iput-wide p1, p0, Lisx;->dHh:J

    .line 208
    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lisx;->aez:I

    .line 209
    return-object p0
.end method

.method public final gZ(Z)Lisx;
    .locals 1

    .prologue
    .line 188
    iput-boolean p1, p0, Lisx;->dHg:Z

    .line 189
    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lisx;->aez:I

    .line 190
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 255
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 256
    iget v1, p0, Lisx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 257
    const/4 v1, 0x1

    iget-object v2, p0, Lisx;->dHe:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_0
    iget v1, p0, Lisx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 261
    const/4 v1, 0x2

    iget-object v2, p0, Lisx;->dHf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_1
    iget v1, p0, Lisx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 265
    const/4 v1, 0x3

    iget-boolean v2, p0, Lisx;->dHg:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 268
    :cond_2
    iget v1, p0, Lisx;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 269
    const/4 v1, 0x4

    iget-wide v2, p0, Lisx;->dHh:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_3
    return v0
.end method

.method public final pw(Ljava/lang/String;)Lisx;
    .locals 1

    .prologue
    .line 144
    if-nez p1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 147
    :cond_0
    iput-object p1, p0, Lisx;->dHe:Ljava/lang/String;

    .line 148
    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lisx;->aez:I

    .line 149
    return-object p0
.end method

.method public final px(Ljava/lang/String;)Lisx;
    .locals 1

    .prologue
    .line 166
    if-nez p1, :cond_0

    .line 167
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 169
    :cond_0
    iput-object p1, p0, Lisx;->dHf:Ljava/lang/String;

    .line 170
    iget v0, p0, Lisx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lisx;->aez:I

    .line 171
    return-object p0
.end method

.method public final yM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lisx;->dHf:Ljava/lang/String;

    return-object v0
.end method
