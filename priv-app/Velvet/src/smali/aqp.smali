.class final Laqp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field private synthetic amN:Laqm;


# direct methods
.method constructor <init>(Laqm;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Laqp;->amN:Laqm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 162
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Laqp;->amN:Laqm;

    iput-object p1, v0, Laqm;->amL:Ljava/lang/Boolean;

    if-nez p1, :cond_0

    const-string v0, "AudioHistorySettingController"

    const-string v1, "Failed to retrieve audio history setting"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iget-object v0, p0, Laqp;->amN:Laqm;

    iget-object v0, v0, Laqm;->amI:Landroid/preference/SwitchPreference;

    const v1, 0x7f0a059e

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    iget-object v0, p0, Laqp;->amN:Laqm;

    iget-object v0, v0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v3}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Laqp;->amN:Laqm;

    const v1, 0x7f0a059f

    invoke-virtual {v0, v1}, Laqm;->cZ(I)V

    :goto_0
    return v4

    :cond_0
    iget-object v0, p0, Laqp;->amN:Laqm;

    iget-object v0, v0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v4}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Laqp;->amN:Laqm;

    iget-object v0, v0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Laqp;->amN:Laqm;

    iget-object v0, v0, Laqm;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhym;->gM(Z)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Laqp;->amN:Laqm;

    invoke-static {v0}, Laqm;->a(Laqm;)V

    :cond_1
    iget-object v0, p0, Laqp;->amN:Laqm;

    iget-object v0, v0, Laqm;->amI:Landroid/preference/SwitchPreference;

    const v1, 0x7f0a0712

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    goto :goto_0
.end method
