.class abstract Lcus;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bjY:I

.field private final bjk:I

.field private bjp:J

.field private final bki:Ljava/util/Map;

.field private final bkj:I

.field private final bkk:I

.field private final bkl:I

.field protected final mCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    const-string v7, "contact_id"

    move-object v0, p1

    move-object v1, p3

    move-object v2, p4

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    .line 177
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    const-string v1, "contact_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcus;->bjk:I

    .line 178
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    const-string v1, "data_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcus;->bkj:I

    .line 179
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    const-string v1, "type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcus;->bkk:I

    .line 180
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    const-string v1, "label"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcus;->bkl:I

    .line 181
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    const-string v1, "score"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcus;->bjY:I

    .line 183
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcus;->bjp:J

    .line 184
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcus;->bki:Ljava/util/Map;

    .line 185
    return-void
.end method

.method private Td()V
    .locals 9

    .prologue
    .line 230
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcus;->bkj:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 231
    iget-wide v2, p0, Lcus;->bjp:J

    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcus;->bkk:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcus;->bkl:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcus;->bjY:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    move-object v1, p0

    invoke-virtual/range {v1 .. v8}, Lcus;->a(JJILjava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcus;->bki:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    return-void
.end method


# virtual methods
.method protected abstract a(JJILjava/lang/String;I)Landroid/content/ContentValues;
.end method

.method public final ag(J)Ljava/util/Map;
    .locals 5

    .prologue
    .line 188
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 189
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 224
    :goto_0
    return-object v0

    .line 192
    :cond_0
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    .line 193
    iget-object v0, p0, Lcus;->bki:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 197
    :cond_1
    :goto_1
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcus;->bjk:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcus;->bjp:J

    .line 202
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_1

    .line 203
    invoke-direct {p0}, Lcus;->Td()V

    goto :goto_1

    .line 207
    :cond_2
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_3

    .line 208
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_3
    :goto_2
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_5

    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 214
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcus;->bjk:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 215
    iget-wide v0, p0, Lcus;->bjp:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_4

    .line 216
    invoke-direct {p0}, Lcus;->Td()V

    goto :goto_2

    .line 218
    :cond_4
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcus;->bki:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 219
    iget-object v1, p0, Lcus;->bki:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 220
    iput-wide v2, p0, Lcus;->bjp:J

    .line 221
    invoke-direct {p0}, Lcus;->Td()V

    goto :goto_0

    .line 224
    :cond_5
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcus;->bki:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcus;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 245
    return-void
.end method
