.class public final Lhqz;
.super Lhqy;
.source "PG"

# interfaces
.implements Landroid/app/ActionBar$TabListener;
.implements Lhj;


# instance fields
.field private final doJ:Landroid/support/v4/view/ViewPager;

.field private final doN:Ljava/util/ArrayList;

.field private final mContext:Landroid/content/Context;

.field private final qi:Landroid/app/ActionBar;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/support/v4/view/ViewPager;)V
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Lhqy;-><init>(Landroid/app/FragmentManager;)V

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhqz;->doN:Ljava/util/ArrayList;

    .line 113
    iput-object p1, p0, Lhqz;->mContext:Landroid/content/Context;

    .line 114
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lhqz;->qi:Landroid/app/ActionBar;

    .line 115
    iput-object p2, p0, Lhqz;->doJ:Landroid/support/v4/view/ViewPager;

    .line 116
    iget-object v0, p0, Lhqz;->doJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lfv;)V

    .line 117
    iget-object v0, p0, Lhqz;->doJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lhj;)V

    .line 118
    return-void
.end method


# virtual methods
.method public final a(IFI)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public final a(Landroid/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lhra;

    invoke-direct {v0, p2, p3}, Lhra;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 122
    invoke-virtual {p1, v0}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    .line 123
    invoke-virtual {p1, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 124
    iget-object v1, p0, Lhqz;->doN:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lhqz;->qi:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 126
    iget-object v0, p0, Lfv;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 127
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lhqz;->doN:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final ls(I)Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lhqz;->doN:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhra;

    .line 137
    iget-object v1, p0, Lhqz;->mContext:Landroid/content/Context;

    iget-object v2, v0, Lhra;->doO:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lhra;->doP:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public final onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 156
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lhqz;->doN:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 157
    iget-object v2, p0, Lhqz;->doN:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v1, :cond_0

    .line 158
    iget-object v2, p0, Lhqz;->doJ:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->r(I)V

    .line 156
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    return-void
.end method

.method public final onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final w(I)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lhqz;->qi:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 147
    return-void
.end method

.method public final x(I)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method
