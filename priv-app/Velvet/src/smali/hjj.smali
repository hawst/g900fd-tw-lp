.class public final Lhjj;
.super Lhjk;
.source "PG"


# static fields
.field private static diE:Ljava/lang/reflect/Method;

.field private static diF:Ljava/lang/reflect/Method;

.field private static diG:Ljava/lang/reflect/Method;

.field private static diH:Ljava/lang/reflect/Method;


# instance fields
.field private diI:Landroid/bluetooth/BluetoothHeadset;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 266
    :try_start_0
    const-class v0, Landroid/bluetooth/BluetoothHeadset;

    const-string v1, "startScoUsingVirtualVoiceCall"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/bluetooth/BluetoothDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lhjj;->diE:Ljava/lang/reflect/Method;

    .line 270
    const-class v0, Landroid/bluetooth/BluetoothHeadset;

    const-string v1, "stopScoUsingVirtualVoiceCall"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/bluetooth/BluetoothDevice;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lhjj;->diF:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :goto_0
    :try_start_1
    const-class v0, Landroid/bluetooth/BluetoothHeadset;

    const-string v1, "connectAudio"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lhjj;->diG:Ljava/lang/reflect/Method;

    .line 284
    const-class v0, Landroid/bluetooth/BluetoothHeadset;

    const-string v1, "disconnectAudio"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lhjj;->diH:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 292
    :goto_1
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 276
    sput-object v5, Lhjj;->diE:Ljava/lang/reflect/Method;

    .line 277
    sput-object v5, Lhjj;->diF:Ljava/lang/reflect/Method;

    .line 278
    const-string v1, "VS.BluetoothShim"

    const-string v2, "Error locating SCO method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 286
    :catch_1
    move-exception v0

    .line 288
    sput-object v5, Lhjj;->diG:Ljava/lang/reflect/Method;

    .line 289
    sput-object v5, Lhjj;->diH:Ljava/lang/reflect/Method;

    .line 290
    const-string v1, "VS.BluetoothShim"

    const-string v2, "Error locating SCO method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method constructor <init>(Landroid/bluetooth/BluetoothHeadset;)V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Lhjk;-><init>()V

    .line 297
    iput-object p1, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    .line 298
    return-void
.end method


# virtual methods
.method public final aPW()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 378
    sget-object v0, Lhjj;->diG:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lhjf;->a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 379
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final aPX()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 390
    sget-object v0, Lhjj;->diH:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lhjf;->a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 391
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final d(Lhji;)Z
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p1, Lhji;->diD:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public final e(Lhji;)Z
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p1, Lhji;->diD:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public final f(Lhji;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 327
    sget-object v0, Lhjj;->diE:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lhji;->diD:Landroid/bluetooth/BluetoothDevice;

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, Lhjf;->a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 329
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final g(Lhji;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 342
    sget-object v0, Lhjj;->diF:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lhji;->diD:Landroid/bluetooth/BluetoothDevice;

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, Lhjf;->a(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 344
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final getConnectedDevices()Ljava/util/List;
    .locals 4

    .prologue
    .line 358
    iget-object v0, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 360
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 362
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 363
    if-eqz v0, :cond_0

    .line 364
    new-instance v3, Lhji;

    invoke-direct {v3, v0}, Lhji;-><init>(Landroid/bluetooth/BluetoothDevice;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 367
    :cond_1
    return-object v1
.end method

.method public final h(Lhji;)I
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lhjj;->diI:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p1, Lhji;->diD:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0
.end method
