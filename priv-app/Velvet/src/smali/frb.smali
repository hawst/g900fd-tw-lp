.class public final Lfrb;
.super Lfra;
.source "PG"


# instance fields
.field private final aTr:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bLI:Ljava/lang/String;

.field private final cAe:Ljava/lang/String;

.field private final cAf:Z

.field private final cAg:[Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cAh:Lizj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mCardContainer:Lfmt;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLfkd;ILjava/lang/String;[Ljava/lang/String;Lizj;Lfmt;)V
    .locals 2
    .param p5    # Lfkd;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # [Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 60
    invoke-interface {p5}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    const/16 v1, 0xaa

    invoke-direct {p0, p10, v0, v1}, Lfra;-><init>(Lfmt;Lizj;I)V

    .line 61
    iput-object p1, p0, Lfrb;->mContext:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lfrb;->cAe:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lfrb;->bLI:Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrb;->cAf:Z

    .line 65
    iput-object p7, p0, Lfrb;->aTr:Ljava/lang/String;

    .line 67
    iput-object p8, p0, Lfrb;->cAg:[Ljava/lang/String;

    .line 68
    iput-object p10, p0, Lfrb;->mCardContainer:Lfmt;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lfrb;->cAh:Lizj;

    .line 70
    return-void
.end method


# virtual methods
.method public final bj(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 76
    iget-object v0, p0, Lfrb;->cAe:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lgau;->O(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 77
    const-string v1, "webview_service"

    iget-object v2, p0, Lfrb;->aTr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "webview_title"

    iget-object v3, p0, Lfrb;->bLI:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "enable_javascript"

    iget-boolean v3, p0, Lfrb;->cAf:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "webview_url_prefixes"

    iget-object v3, p0, Lfrb;->cAg:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    iget-object v1, p0, Lfrb;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 84
    iget-object v0, p0, Lfrb;->cAh:Lizj;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lfrb;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfrb;->cAh:Lizj;

    const/16 v2, 0x14

    invoke-interface {v0, v1, v2, v4, v4}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 87
    :cond_0
    return-void
.end method
