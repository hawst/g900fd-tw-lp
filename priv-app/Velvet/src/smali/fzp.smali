.class public final Lfzp;
.super Lfro;
.source "PG"


# instance fields
.field private cDT:Landroid/widget/TextView;

.field private cDU:Landroid/widget/TextView;

.field private cDV:Landroid/widget/TextView;

.field private cDW:Landroid/widget/TextView;

.field private cDX:Landroid/view/ViewGroup;

.field private cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

.field private kO:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 48
    return-void
.end method

.method static synthetic a(Lfzp;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lfzp;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->wq()Lfnm;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzp;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0146

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lfnm;->aAr()V

    goto :goto_0
.end method

.method private aDU()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lfzp;->cDX:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->aeQ()V

    .line 97
    iget-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lfzp;->cDU:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lfzp;->cDV:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lfzp;->cDW:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lfzp;->cDT:Landroid/widget/TextView;

    const v1, 0x7f0a043d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 104
    iget-object v0, p0, Lfzp;->cDU:Landroid/widget/TextView;

    const v1, 0x7f0a0361

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 105
    iget-object v0, p0, Lfzp;->cDW:Landroid/widget/TextView;

    iget-object v1, p0, Lfzp;->kO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method private aDV()V
    .locals 4

    .prologue
    const v3, 0x7f0a0040

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lfzp;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->wq()Lfnm;

    move-result-object v0

    invoke-virtual {v0}, Lfnm;->Yy()I

    move-result v0

    .line 150
    packed-switch v0, :pswitch_data_0

    .line 161
    const-string v1, "TvRecognitionModulePresenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    return-void

    .line 152
    :pswitch_0
    invoke-direct {p0}, Lfzp;->aDU()V

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v0, p0, Lfzp;->cDX:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDT:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->aeP()V

    goto :goto_0

    .line 158
    :pswitch_2
    iget-object v0, p0, Lfzp;->cDX:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->aeQ()V

    iget-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDU:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDV:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDW:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lfzp;->cDT:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lfzp;->cDU:Landroid/widget/TextView;

    const v1, 0x7f0a0444

    invoke-direct {p0, v1}, Lfzp;->jO(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfzp;->cDV:Landroid/widget/TextView;

    const v1, 0x7f0a0446

    invoke-direct {p0, v1}, Lfzp;->jO(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lfzp;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lfzp;->aDV()V

    return-void
.end method

.method private jO(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 131
    iget-object v0, p0, Lfzp;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d018b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 133
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lfzp;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 135
    new-instance v2, Landroid/text/style/BulletSpan;

    invoke-direct {v2, v0}, Landroid/text/style/BulletSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 136
    return-object v1
.end method


# virtual methods
.method protected final aCZ()V
    .locals 5

    .prologue
    .line 85
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahg:Lapq;

    invoke-virtual {v0}, Lapq;->tj()Ljava/lang/String;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    iget-object v1, p0, Lfzp;->mContext:Landroid/content/Context;

    const v2, 0x7f0a043e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lfzp;->kO:Ljava/lang/String;

    .line 90
    invoke-direct {p0}, Lfzp;->aDV()V

    .line 91
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lfzp;->mContext:Landroid/content/Context;

    const v1, 0x7f0a043f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 52
    const v0, 0x7f040145

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 53
    const v0, 0x7f110082

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfzp;->cDT:Landroid/widget/TextView;

    .line 54
    const v0, 0x7f1101aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfzp;->cDU:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f110374

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfzp;->cDV:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f110375

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfzp;->cDW:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f110376

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;

    iput-object v0, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    .line 59
    const v0, 0x7f110373

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lfzp;->cDX:Landroid/view/ViewGroup;

    .line 61
    iget-object v0, p0, Lfzp;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->wq()Lfnm;

    move-result-object v0

    .line 62
    if-nez v0, :cond_0

    .line 63
    invoke-direct {p0}, Lfzp;->aDU()V

    .line 80
    :goto_0
    return-object v1

    .line 65
    :cond_0
    invoke-virtual {v0}, Lfnm;->aAs()Lequ;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_1

    .line 67
    iget-object v2, p0, Lfzp;->cDY:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-static {v0}, Leef;->a(Lequ;)Ldtb;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a(Ldtb;)V

    .line 70
    :cond_1
    const v0, 0x7f1100f6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lfzq;

    iget-object v3, p0, Lfzp;->mCardContainer:Lfmt;

    iget-object v4, p0, Lfro;->cBc:Lanh;

    iget-object v4, v4, Lanh;->ahu:Lizj;

    const/16 v5, 0x6a

    invoke-direct {v2, p0, v3, v4, v5}, Lfzq;-><init>(Lfzp;Lfmt;Lizj;I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    new-instance v0, Lfzr;

    invoke-direct {v0, p0, v6}, Lfzr;-><init>(Lfzp;B)V

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto :goto_0
.end method
