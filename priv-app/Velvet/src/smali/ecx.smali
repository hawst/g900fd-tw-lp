.class public abstract Lecx;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private final bVj:Landroid/view/View$OnClickListener;

.field public bVk:Lecz;

.field protected final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lecx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lecx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Lecy;

    invoke-direct {v0, p0}, Lecy;-><init>(Lecx;)V

    iput-object v0, p0, Lecx;->bVj:Landroid/view/View$OnClickListener;

    .line 61
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lecx;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 66
    invoke-virtual {p0}, Lecx;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lecx;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 72
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/os/Parcelable;Ljava/lang/Object;Z)Landroid/view/View;
.end method

.method public a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 80
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->ajO()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 82
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alB()Z

    move-result v3

    .line 83
    if-eqz v3, :cond_2

    new-array v0, v1, [Landroid/os/Parcelable;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 84
    :goto_1
    if-nez v3, :cond_3

    move v3, v1

    :goto_2
    invoke-virtual {p0}, Lecx;->removeAllViews()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_4

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p0, v0, p2, v1}, Lecx;->a(Landroid/os/Parcelable;Ljava/lang/Object;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lecx;->addView(Landroid/view/View;)V

    if-eqz v3, :cond_0

    iget-object v4, p0, Lecx;->bVj:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    :cond_1
    move v0, v2

    .line 80
    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v3, v2

    .line 84
    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    .line 85
    :cond_5
    return-void
.end method

.method public a(Lecz;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lecx;->bVk:Lecz;

    .line 76
    return-void
.end method
