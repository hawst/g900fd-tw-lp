.class public final Lcux;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final bkD:Ljava/lang/String;

.field private final bkE:[Ljava/lang/String;

.field private final mConfig:Lcjs;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcjs;Landroid/content/ContentResolver;Landroid/content/SharedPreferences;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    iput-object p1, p0, Lcux;->mConfig:Lcjs;

    .line 391
    iput-object p2, p0, Lcux;->mContentResolver:Landroid/content/ContentResolver;

    .line 392
    iput-object p3, p0, Lcux;->mPreferences:Landroid/content/SharedPreferences;

    .line 393
    iput-object p4, p0, Lcux;->bkD:Ljava/lang/String;

    .line 394
    iput-object p5, p0, Lcux;->bkE:[Ljava/lang/String;

    .line 395
    return-void
.end method


# virtual methods
.method public final Tg()Ljava/lang/Boolean;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 399
    iget-object v0, p0, Lcux;->mConfig:Lcjs;

    invoke-static {v0}, Lcum;->c(Lcjs;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    const-string v0, "UpdateIcingCorporaService"

    const-string v1, "Ignoring contacts task because contacts corpus disabled"

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 401
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 449
    :goto_0
    return-object v0

    .line 404
    :cond_0
    sget-boolean v0, Lcto;->biy:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcux;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->MF()I

    move-result v0

    .line 408
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcux;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "KEY_LAST_CONTACTS_UPDATE"

    const-wide/16 v8, 0x0

    invoke-interface {v3, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 417
    const-string v3, "NONE"

    iget-object v6, p0, Lcux;->bkD:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 418
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 404
    :cond_1
    iget-object v0, p0, Lcux;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->ME()I

    move-result v0

    goto :goto_1

    .line 419
    :cond_2
    const-string v3, "FORCE_ALL"

    iget-object v6, p0, Lcux;->bkD:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "MAYBE"

    iget-object v6, p0, Lcux;->bkD:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    .line 422
    :cond_3
    iget-object v0, p0, Lcux;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjD:Landroid/net/Uri;

    invoke-virtual {v0, v3, v10, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_4

    move v0, v1

    .line 424
    :goto_2
    if-eqz v0, :cond_5

    .line 425
    iget-object v0, p0, Lcux;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "KEY_LAST_CONTACTS_UPDATE"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 431
    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 422
    goto :goto_2

    .line 429
    :cond_5
    const-string v0, "UpdateIcingCorporaService"

    const-string v3, "Full contacts update failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_3

    .line 432
    :cond_6
    const-string v0, "DELTA"

    iget-object v3, p0, Lcux;->bkD:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 433
    iget-object v0, p0, Lcux;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjD:Landroid/net/Uri;

    const-string v4, "delta"

    invoke-virtual {v0, v3, v10, v4, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_8

    move v0, v1

    .line 436
    :goto_4
    if-nez v0, :cond_7

    .line 437
    const-string v0, "UpdateIcingCorporaService"

    const-string v3, "Contacts delta update failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 439
    :cond_7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 433
    goto :goto_4

    .line 440
    :cond_9
    const-string v0, "SPECIFIC"

    iget-object v3, p0, Lcux;->bkD:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 441
    iget-object v0, p0, Lcux;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjD:Landroid/net/Uri;

    const-string v4, "ids"

    iget-object v5, p0, Lcux;->bkE:[Ljava/lang/String;

    invoke-virtual {v0, v3, v10, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_b

    move v0, v1

    .line 444
    :goto_5
    if-nez v0, :cond_a

    .line 445
    const-string v0, "UpdateIcingCorporaService"

    const-string v3, "Contacts delta update failed."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 447
    :cond_a
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 441
    goto :goto_5

    .line 449
    :cond_c
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 381
    invoke-virtual {p0}, Lcux;->Tg()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
