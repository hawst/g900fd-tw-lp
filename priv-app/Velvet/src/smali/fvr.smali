.class public final Lfvr;
.super Lfuz;
.source "PG"


# instance fields
.field private final cCC:Ljava/lang/String;

.field private final cCD:Ljava/lang/String;

.field private final cCE:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lftz;Lemp;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 1
    .param p5    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 33
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-direct {p0, v0, p1, p2}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 34
    iput-object p3, p0, Lfvr;->cCC:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lfvr;->cCD:Ljava/lang/String;

    .line 36
    iput-object p5, p0, Lfvr;->cCE:Landroid/content/Intent;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    move-result-object v1

    .line 45
    new-instance v2, Lanu;

    invoke-direct {v2}, Lanu;-><init>()V

    const v3, 0x7f0a0433

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lanu;->aX(Ljava/lang/String;)Lanu;

    move-result-object v2

    iget-object v3, p0, Lfvr;->cCC:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lanu;->aY(Ljava/lang/String;)Lanu;

    move-result-object v2

    iput-object v2, v1, Lanh;->ahc:Lanu;

    .line 48
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, p0, Lfvr;->cCE:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfvr;->cCD:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    move-result-object v1

    .line 52
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    iget-object v3, p0, Lfvr;->cCD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Laoj;->bS(Ljava/lang/String;)Laoj;

    move-result-object v2

    iput-object v2, v1, Lanh;->agz:Laoj;

    .line 53
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v1, Lanh;->ahu:Lizj;

    .line 54
    new-instance v2, Lfsa;

    const/16 v3, 0x42

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0201bb

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    iget-object v3, p0, Lfvr;->cCE:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lfsa;->L(Landroid/content/Intent;)Lani;

    move-result-object v2

    iput-object v2, v1, Lanh;->ahs:Lani;

    .line 57
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_0
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 61
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lang;->aS(Z)Lang;

    .line 62
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lanh;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 63
    return-object v1
.end method
