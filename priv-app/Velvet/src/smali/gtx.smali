.class public Lgtx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field public static final cWo:[Lgor;

.field private static cWp:Lgtx;

.field private static final cWq:Ljava/util/Map;


# instance fields
.field private final aqs:Ljava/util/concurrent/ScheduledExecutorService;

.field private final cWr:Ljava/util/HashMap;

.field private final cWs:Ljava/io/File;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 49
    const/4 v1, 0x5

    new-array v1, v1, [Lgor;

    sget-object v2, Lhjt;->ctv:Lgor;

    aput-object v2, v1, v0

    const/4 v2, 0x1

    sget-object v3, Lguc;->ctv:Lgor;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lfjl;->ctv:Lgor;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lfjx;->ctv:Lgor;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lfiz;->ctv:Lgor;

    aput-object v3, v1, v2

    sput-object v1, Lgtx;->cWo:[Lgor;

    .line 65
    new-instance v1, Ljava/util/HashMap;

    sget-object v2, Lgtx;->cWo:[Lgor;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Lgtx;->cWq:Ljava/util/Map;

    .line 68
    sget-object v1, Lgtx;->cWo:[Lgor;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 69
    sget-object v4, Lgtx;->cWq:Ljava/util/Map;

    iget-object v5, v3, Lgor;->name:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Two jar classes added with identical name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lgor;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_1
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 4

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lgtx;->mContext:Landroid/content/Context;

    .line 119
    iput-object p2, p0, Lgtx;->aqs:Ljava/util/concurrent/ScheduledExecutorService;

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgtx;->cWr:Ljava/util/HashMap;

    .line 121
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lgtx;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "extradex_jars"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a directory"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can not create directory "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iput-object v0, p0, Lgtx;->cWs:Ljava/io/File;

    .line 122
    return-void
.end method

.method static synthetic a(Lgtx;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgtx;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)Lgtx;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lgtx;

    invoke-direct {v0, p0, p1}, Lgtx;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V

    return-object v0
.end method

.method static synthetic a(Lgtx;Lgop;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p1, Lgop;->ceB:Ljava/lang/Object;

    instance-of v1, v0, Lgtt;

    if-eqz v1, :cond_0

    check-cast v0, Lgtt;

    iget-object v1, p0, Lgtx;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1}, Lgtt;->h(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)Lgtx;
    .locals 3

    .prologue
    .line 130
    const-class v1, Lgtx;

    monitor-enter v1

    .line 131
    :try_start_0
    sget-object v0, Lgtx;->cWp:Lgtx;

    if-eqz v0, :cond_0

    .line 132
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "ExtraDexRegistry already created in this process"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 134
    :cond_0
    :try_start_1
    new-instance v0, Lgtx;

    invoke-direct {v0, p0, p1}, Lgtx;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 135
    sput-object v0, Lgtx;->cWp:Lgtx;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method static synthetic b(Lgtx;)Ljava/io/File;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgtx;->cWs:Ljava/io/File;

    return-object v0
.end method

.method public static c(Livq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 100
    :try_start_0
    invoke-interface {p0}, Livq;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 101
    :catch_0
    move-exception v0

    .line 103
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 104
    :catch_1
    move-exception v0

    .line 105
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 106
    invoke-static {v1}, Lgtx;->f(Ljava/lang/Throwable;)V

    .line 108
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic c(Lgtx;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgtx;->aqs:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method private static f(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 211
    move-object v0, p0

    :goto_0
    if-eqz v0, :cond_1

    .line 212
    instance-of v1, v0, Lgos;

    if-eqz v1, :cond_0

    .line 213
    check-cast v0, Lgos;

    throw v0

    .line 215
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_1
    return-void
.end method

.method public static nC(Ljava/lang/String;)Lgor;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lgtx;->cWq:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgor;

    return-object v0
.end method

.method private nE(Ljava/lang/String;)Lgtz;
    .locals 2

    .prologue
    .line 221
    iget-object v1, p0, Lgtx;->cWr:Ljava/util/HashMap;

    monitor-enter v1

    .line 222
    :try_start_0
    iget-object v0, p0, Lgtx;->cWr:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtz;

    .line 223
    if-nez v0, :cond_0

    .line 224
    invoke-direct {p0, p1}, Lgtx;->nF(Ljava/lang/String;)Lgtz;

    move-result-object v0

    .line 226
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private nF(Ljava/lang/String;)Lgtz;
    .locals 4

    .prologue
    .line 231
    iget-object v1, p0, Lgtx;->cWr:Ljava/util/HashMap;

    monitor-enter v1

    .line 232
    :try_start_0
    iget-object v0, p0, Lgtx;->cWr:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 233
    invoke-static {p1}, Lgtx;->nC(Ljava/lang/String;)Lgor;

    move-result-object v0

    .line 234
    if-nez v0, :cond_1

    .line 235
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JAR class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not known."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 232
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 237
    :cond_1
    :try_start_1
    new-instance v2, Lgtz;

    invoke-direct {v2, p0, v0}, Lgtz;-><init>(Lgtx;Lgor;)V

    .line 238
    iget-object v0, p0, Lgtx;->cWr:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 5

    .prologue
    .line 253
    const-string v0, "ExtraDexRegistry"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 254
    const-string v0, "Entries"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgtx;->cWr:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 255
    iget-object v0, p0, Lgtx;->cWr:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 256
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgtz;

    .line 257
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loaded: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lgtz;->cWu:Leou;

    invoke-virtual {v4}, Leou;->isDone()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; jar: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lgtz;->aKu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 260
    :cond_0
    return-void
.end method

.method public final b(Lgor;)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p1, Lgor;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgtx;->nE(Ljava/lang/String;)Lgtz;

    move-result-object v0

    iget-object v0, v0, Lgtz;->cWu:Leou;

    invoke-virtual {v0}, Leou;->auY()V

    .line 155
    return-void
.end method

.method public final c(Lgor;)Livq;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p1, Lgor;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lgtx;->nD(Ljava/lang/String;)Livq;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lgor;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p1, Lgor;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgtx;->nE(Ljava/lang/String;)Lgtz;

    move-result-object v0

    .line 169
    iget-object v0, v0, Lgtz;->cWu:Leou;

    invoke-static {v0}, Lgtx;->c(Livq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgop;

    iget-object v0, v0, Lgop;->ceB:Ljava/lang/Object;

    return-object v0
.end method

.method public final e(Lgor;)Lgop;
    .locals 3

    .prologue
    .line 244
    iget-object v0, p1, Lgor;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgtx;->nE(Ljava/lang/String;)Lgtz;

    move-result-object v0

    .line 245
    iget-object v1, v0, Lgtz;->cWu:Leou;

    invoke-virtual {v1}, Leou;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JAR "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lgor;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not loaded yet."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    invoke-virtual {v0}, Lgtz;->aKt()Lgop;

    move-result-object v0

    return-object v0
.end method

.method public final nD(Ljava/lang/String;)Livq;
    .locals 2

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lgtx;->nE(Ljava/lang/String;)Lgtz;

    move-result-object v0

    .line 184
    iget-object v0, v0, Lgtz;->cWu:Leou;

    new-instance v1, Lgty;

    invoke-direct {v1, p0}, Lgty;-><init>(Lgtx;)V

    invoke-static {v0, v1}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    return-object v0
.end method
