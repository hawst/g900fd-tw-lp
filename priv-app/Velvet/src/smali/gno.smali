.class public Lgno;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cQq:[I


# instance fields
.field private final bBb:Landroid/net/ConnectivityManager;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lgno;->cQq:[I

    return-void

    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/telephony/TelephonyManager;Landroid/net/ConnectivityManager;)V
    .locals 0
    .param p1    # Landroid/telephony/TelephonyManager;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/ConnectivityManager;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 47
    iput-object p2, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    .line 48
    return-void
.end method

.method private static H(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 326
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 328
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/net/NetworkInfo;)I
    .locals 7

    .prologue
    const/16 v3, 0x9

    const/4 v2, 0x7

    const/4 v1, 0x6

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 154
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    move v0, v4

    .line 230
    :cond_1
    :goto_0
    return v0

    .line 158
    :cond_2
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    .line 159
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v6

    .line 161
    if-eq v5, v0, :cond_1

    .line 165
    if-ne v5, v1, :cond_3

    .line 166
    const/16 v0, 0x13

    goto :goto_0

    .line 169
    :cond_3
    if-ne v5, v2, :cond_4

    .line 170
    const/16 v0, 0x11

    goto :goto_0

    .line 173
    :cond_4
    if-ne v5, v3, :cond_5

    .line 174
    const/16 v0, 0x12

    goto :goto_0

    .line 177
    :cond_5
    if-nez v5, :cond_6

    .line 178
    packed-switch v6, :pswitch_data_0

    move v0, v4

    .line 226
    goto :goto_0

    .line 180
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 183
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 186
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 189
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 192
    goto :goto_0

    :pswitch_5
    move v0, v2

    .line 195
    goto :goto_0

    .line 198
    :pswitch_6
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_7
    move v0, v3

    .line 201
    goto :goto_0

    .line 204
    :pswitch_8
    const/16 v0, 0xa

    goto :goto_0

    .line 207
    :pswitch_9
    const/16 v0, 0xb

    goto :goto_0

    .line 210
    :pswitch_a
    const/16 v0, 0xc

    goto :goto_0

    .line 213
    :pswitch_b
    const/16 v0, 0xd

    goto :goto_0

    .line 216
    :pswitch_c
    const/16 v0, 0xe

    goto :goto_0

    .line 219
    :pswitch_d
    const/16 v0, 0xf

    goto :goto_0

    .line 222
    :pswitch_e
    const/16 v0, 0x10

    goto :goto_0

    :cond_6
    move v0, v4

    .line 230
    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_2
        :pswitch_e
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_6
        :pswitch_d
        :pswitch_3
        :pswitch_a
    .end packed-switch
.end method

.method public static aHY()I
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public aAj()I
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, -0x1

    .line 55
    iget-object v1, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    iget-object v1, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 61
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lgno;->H(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public aAk()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 78
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_0

    .line 79
    iget-object v0, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v0

    .line 86
    :goto_0
    invoke-virtual {p0}, Lgno;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    :goto_1
    return v1

    .line 81
    :cond_0
    iget-object v0, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    if-eq v3, v1, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v3, 0x6

    if-eq v0, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    .line 86
    goto :goto_1
.end method

.method public aAl()[I
    .locals 6
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x3

    .line 95
    iget-object v0, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 96
    sget-object v0, Lgno;->cQq:[I

    .line 104
    :goto_0
    return-object v0

    .line 99
    :cond_0
    iget-object v0, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v3, :cond_2

    .line 101
    :cond_1
    sget-object v0, Lgno;->cQq:[I

    goto :goto_0

    .line 104
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Lgno;->H(Ljava/lang/String;I)I

    move-result v2

    aput v2, v0, v5

    const/4 v2, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lgno;->H(Ljava/lang/String;I)I

    move-result v1

    aput v1, v0, v2

    goto :goto_0
.end method

.method public aAm()[I
    .locals 6
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x3

    .line 116
    iget-object v0, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 117
    sget-object v0, Lgno;->cQq:[I

    .line 125
    :goto_0
    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 121
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v3, :cond_2

    .line 122
    :cond_1
    sget-object v0, Lgno;->cQq:[I

    goto :goto_0

    .line 125
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {v1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4}, Lgno;->H(Ljava/lang/String;I)I

    move-result v2

    aput v2, v0, v5

    const/4 v2, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lgno;->H(Ljava/lang/String;I)I

    move-result v1

    aput v1, v0, v2

    goto :goto_0
.end method

.method public aAn()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, -0x1

    .line 143
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-static {v0}, Lgno;->a(Landroid/net/NetworkInfo;)I

    move-result v0

    goto :goto_0
.end method

.method public aAo()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 296
    const-string v0, ""

    .line 303
    :goto_0
    return-object v0

    .line 299
    :cond_0
    iget-object v0, p0, Lgno;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 300
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 301
    const-string v0, ""

    goto :goto_0

    .line 303
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aHX()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 252
    invoke-virtual {p0}, Lgno;->aAn()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 287
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 258
    :pswitch_0
    const-string v0, "CELL_2G"

    goto :goto_0

    .line 269
    :pswitch_1
    const-string v0, "CELL_3G"

    goto :goto_0

    .line 273
    :pswitch_2
    const-string v0, "CELL_4G"

    goto :goto_0

    .line 276
    :pswitch_3
    const-string v0, "WIFI"

    goto :goto_0

    .line 279
    :pswitch_4
    const-string v0, "ETHERNET"

    goto :goto_0

    .line 282
    :pswitch_5
    const-string v0, "BLUETOOTH"

    goto :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 68
    iget-object v1, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    if-nez v1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    iget-object v1, p0, Lgno;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 73
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
