.class public abstract Ljul;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field protected static eFt:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;


# instance fields
.field private eFr:Ljava/io/File;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field protected eFs:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private eFu:Lcom/google/speech/grammar/pumpkin/Tagger;

.field private eFv:Lcom/google/speech/grammar/pumpkin/UserValidators;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Ljul;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object v0, p0, Ljul;->eFr:Ljava/io/File;

    .line 68
    iput-object v0, p0, Ljul;->eFs:[B

    .line 69
    return-void
.end method


# virtual methods
.method public final buJ()Lcom/google/speech/grammar/pumpkin/Tagger;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Ljul;->eFu:Lcom/google/speech/grammar/pumpkin/Tagger;

    return-object v0
.end method

.method public final buK()Lcom/google/speech/grammar/pumpkin/UserValidators;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ljul;->eFv:Lcom/google/speech/grammar/pumpkin/UserValidators;

    return-object v0
.end method

.method public declared-synchronized ed()V
    .locals 2

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljul;->eFt:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    invoke-direct {v0}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;-><init>()V

    sput-object v0, Ljul;->eFt:Lcom/google/speech/grammar/pumpkin/ActionFrameManager;

    .line 80
    :cond_0
    iget-object v0, p0, Ljul;->eFu:Lcom/google/speech/grammar/pumpkin/Tagger;

    if-nez v0, :cond_1

    .line 91
    new-instance v0, Lcom/google/speech/grammar/pumpkin/Tagger;

    iget-object v1, p0, Ljul;->eFs:[B

    invoke-direct {v0, v1}, Lcom/google/speech/grammar/pumpkin/Tagger;-><init>([B)V

    iput-object v0, p0, Ljul;->eFu:Lcom/google/speech/grammar/pumpkin/Tagger;

    .line 93
    :cond_1
    iget-object v0, p0, Ljul;->eFv:Lcom/google/speech/grammar/pumpkin/UserValidators;

    if-nez v0, :cond_2

    .line 94
    new-instance v0, Lcom/google/speech/grammar/pumpkin/UserValidators;

    iget-object v1, p0, Ljul;->eFs:[B

    invoke-direct {v0, v1}, Lcom/google/speech/grammar/pumpkin/UserValidators;-><init>([B)V

    iput-object v0, p0, Ljul;->eFv:Lcom/google/speech/grammar/pumpkin/UserValidators;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :cond_2
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
