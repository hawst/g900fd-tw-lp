.class final Lckn;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic aZS:Ljava/lang/String;

.field private synthetic aZT:Lemy;

.field private synthetic aZU:Lckm;


# direct methods
.method varargs constructor <init>(Lckm;Ljava/lang/String;[ILjava/lang/String;Lemy;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lckn;->aZU:Lckm;

    iput-object p4, p0, Lckn;->aZS:Ljava/lang/String;

    iput-object p5, p0, Lckn;->aZT:Lemy;

    invoke-direct {p0, p2, p3}, Lepm;-><init>(Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v6, p0, Lckn;->aZU:Lckm;

    iget-object v1, p0, Lckn;->aZS:Ljava/lang/String;

    iget-object v7, p0, Lckn;->aZT:Lemy;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ldlb;

    invoke-direct {v0, v1}, Ldlb;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ldlb;->setUseCaches(Z)V

    :try_start_0
    iget-object v2, v6, Lckm;->mHttpHelper:Ldkx;

    const/16 v3, 0xf

    invoke-virtual {v2, v0, v3}, Ldkx;->d(Ldlb;I)Ldjy;

    move-result-object v0

    iget-object v4, v0, Ldjy;->bAA:[B

    iget-object v0, v0, Ldjy;->mResponseHeaders:Ldyj;

    const-string v5, ""

    if-eqz v0, :cond_0

    const-string v2, "Content-Type"

    invoke-virtual {v0, v2}, Ldyj;->aF(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    :cond_0
    new-instance v0, Lcki;

    iget-object v2, v6, Lckm;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->uptimeMillis()J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, Lcki;-><init>(Ljava/lang/String;J[BLjava/lang/String;)V

    iget-object v1, v6, Lckm;->aZP:Lckj;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lckj;->a(Lcki;Z)V

    if-eqz v7, :cond_1

    invoke-interface {v7, v0}, Lemy;->aj(Ljava/lang/Object;)Z
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :cond_1
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Search.BasePageFetcher"

    const-string v2, "Error getting basePage"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "Search.BasePageFetcher"

    const-string v2, "Error getting BasePage"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
