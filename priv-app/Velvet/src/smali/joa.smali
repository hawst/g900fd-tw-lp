.class public final Ljoa;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private evW:I

.field private evX:I

.field private evY:I

.field private evZ:I

.field private ewa:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x190

    const/4 v0, 0x0

    .line 127
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 128
    iput v0, p0, Ljoa;->aez:I

    iput v1, p0, Ljoa;->evW:I

    iput v1, p0, Ljoa;->evX:I

    iput v0, p0, Ljoa;->evY:I

    const/16 v0, 0x280

    iput v0, p0, Ljoa;->evZ:I

    const/16 v0, 0x140

    iput v0, p0, Ljoa;->ewa:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljoa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljoa;->eCz:I

    .line 129
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljoa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljoa;->evW:I

    iget v0, p0, Ljoa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoa;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljoa;->evX:I

    iget v0, p0, Ljoa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoa;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljoa;->evY:I

    iget v0, p0, Ljoa;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljoa;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljoa;->evZ:I

    iget v0, p0, Ljoa;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljoa;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljoa;->ewa:I

    iget v0, p0, Ljoa;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljoa;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Ljoa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x1

    iget v1, p0, Ljoa;->evW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 149
    :cond_0
    iget v0, p0, Ljoa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x2

    iget v1, p0, Ljoa;->evX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 152
    :cond_1
    iget v0, p0, Ljoa;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 153
    const/4 v0, 0x3

    iget v1, p0, Ljoa;->evY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 155
    :cond_2
    iget v0, p0, Ljoa;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 156
    const/4 v0, 0x4

    iget v1, p0, Ljoa;->evZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 158
    :cond_3
    iget v0, p0, Ljoa;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 159
    const/4 v0, 0x5

    iget v1, p0, Ljoa;->ewa:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 161
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 162
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 166
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 167
    iget v1, p0, Ljoa;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 168
    const/4 v1, 0x1

    iget v2, p0, Ljoa;->evW:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_0
    iget v1, p0, Ljoa;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 172
    const/4 v1, 0x2

    iget v2, p0, Ljoa;->evX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_1
    iget v1, p0, Ljoa;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 176
    const/4 v1, 0x3

    iget v2, p0, Ljoa;->evY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_2
    iget v1, p0, Ljoa;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 180
    const/4 v1, 0x4

    iget v2, p0, Ljoa;->evZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_3
    iget v1, p0, Ljoa;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 184
    const/4 v1, 0x5

    iget v2, p0, Ljoa;->ewa:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_4
    return v0
.end method
