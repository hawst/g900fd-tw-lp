.class public final Lewl;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private ckE:Liwk;

.field private mEntry:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static D(Landroid/content/Intent;)Lewl;
    .locals 4

    .prologue
    .line 53
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 54
    const-string v0, "entry"

    const-class v2, Lizj;

    invoke-static {v1, v0, v2}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 56
    const-string v2, "action"

    const-class v3, Liwk;

    invoke-static {v1, v2, v3}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Liwk;

    .line 58
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 59
    :cond_0
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0, v1}, Lewl;->a(Lizj;Liwk;)Lewl;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lizj;Liwk;)Lewl;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    const-string v1, "entry_key"

    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 40
    const-string v1, "delete_action_key"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 42
    new-instance v1, Lewl;

    invoke-direct {v1}, Lewl;-><init>()V

    .line 43
    invoke-virtual {v1, v0}, Lewl;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v1
.end method


# virtual methods
.method final awq()V
    .locals 5

    .prologue
    .line 93
    invoke-virtual {p0}, Lewl;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 95
    if-nez v0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    const-string v1, "deletePlaceWorkerFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 100
    iget-object v1, p0, Lewl;->mEntry:Lizj;

    iget-object v2, p0, Lewl;->ckE:Liwk;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "entry_key"

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v1, "action_key"

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Lewo;

    invoke-direct {v1}, Lewo;-><init>()V

    invoke-virtual {v1, v3}, Lewo;->setArguments(Landroid/os/Bundle;)V

    .line 102
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "deletePlaceWorkerFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "deletePlaceWorkerFragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 66
    invoke-virtual {p0}, Lewl;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v1

    iput-object v1, p0, Lewl;->mEntry:Lizj;

    .line 68
    const-string v1, "delete_action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->R([B)Liwk;

    move-result-object v0

    iput-object v0, p0, Lewl;->ckE:Liwk;

    .line 70
    new-instance v0, Lexl;

    invoke-virtual {p0}, Lewl;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lewl;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f0a0321

    invoke-direct {v0, v1, v2, v3}, Lexl;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;I)V

    .line 72
    const v1, 0x104000a

    new-instance v2, Lewm;

    invoke-direct {v2, p0}, Lewm;-><init>(Lewl;)V

    invoke-virtual {v0, v1, v2}, Lexl;->a(ILandroid/view/View$OnClickListener;)V

    .line 78
    const/high16 v1, 0x1040000

    new-instance v2, Lewn;

    invoke-direct {v2, p0, v0}, Lewn;-><init>(Lewl;Lexl;)V

    invoke-virtual {v0, v1, v2}, Lexl;->b(ILandroid/view/View$OnClickListener;)V

    .line 86
    invoke-virtual {v0}, Lexl;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 89
    return-object v0
.end method
