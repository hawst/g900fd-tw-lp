.class public final Lfak;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cnj:Lfdg;

.field public final mAlarmHelper:Ldjx;

.field private final mAppContext:Landroid/content/Context;

.field private final mPendingIntentFactory:Lfda;

.field private final mSearchConfig:Lcjs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldjx;Lcjs;Lfdg;Lfda;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lfak;->mAppContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lfak;->mAlarmHelper:Ldjx;

    .line 43
    iput-object p3, p0, Lfak;->mSearchConfig:Lcjs;

    .line 44
    iput-object p4, p0, Lfak;->cnj:Lfdg;

    .line 45
    iput-object p5, p0, Lfak;->mPendingIntentFactory:Lfda;

    .line 46
    return-void
.end method

.method private h(Landroid/content/Context;Z)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 109
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    const-string v0, "com.google.android.apps.sidekick.SCHEDULED_REFRESH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v0, "com.google.android.apps.sidekick.TRACE"

    const/16 v2, 0x31

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 113
    iget-object v2, p0, Lfak;->mPendingIntentFactory:Lfda;

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    const/high16 v0, 0x8000000

    :goto_0
    invoke-interface {v2, v3, v1, v0}, Lfda;->b(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    const/high16 v0, 0x20000000

    goto :goto_0
.end method


# virtual methods
.method public final awP()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lfak;->cnj:Lfdg;

    invoke-virtual {v0}, Lfdg;->ayq()Z

    move-result v0

    .line 78
    if-eqz v0, :cond_0

    iget-object v0, p0, Lfak;->mSearchConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LM()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfak;->mSearchConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LL()I

    move-result v0

    goto :goto_0
.end method

.method public final awQ()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lfak;->mAppContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfak;->h(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lfak;->mAlarmHelper:Ldjx;

    const-string v2, "refresh_alarm"

    invoke-virtual {v1, v0, v2}, Ldjx;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 103
    :cond_0
    return-void
.end method

.method public final fi(Z)V
    .locals 7

    .prologue
    .line 54
    invoke-virtual {p0}, Lfak;->awP()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v2, v0

    .line 61
    iget-object v0, p0, Lfak;->mAppContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfak;->h(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfak;->mAppContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    const-string v1, "com.google.android.apps.sidekick.notifications.INITIALIZE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    iget-object v1, p0, Lfak;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 68
    :cond_0
    iget-object v1, p0, Lfak;->mAlarmHelper:Ldjx;

    iget-object v0, p0, Lfak;->mAppContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {p0, v0, v4}, Lfak;->h(Landroid/content/Context;Z)Landroid/app/PendingIntent;

    move-result-object v4

    const-string v5, "refresh_alarm"

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Ldjx;->a(JLandroid/app/PendingIntent;Ljava/lang/String;Z)V

    .line 70
    return-void
.end method
