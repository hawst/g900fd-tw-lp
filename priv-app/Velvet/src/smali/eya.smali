.class public final Leya;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field private ckI:Z

.field private clC:Leyb;

.field private mEntryProvider:Lfaq;

.field private mSidekickInjector:Lfdb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 123
    return-void
.end method


# virtual methods
.method final fh(Z)V
    .locals 4

    .prologue
    .line 98
    invoke-virtual {p0}, Leya;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 99
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v1

    .line 100
    const-string v2, "renamed_place_key"

    const-class v3, Ljcu;

    invoke-static {v0, v2, v3}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljcu;

    .line 103
    invoke-virtual {p0}, Leya;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v2}, Leyf;->a(Landroid/app/FragmentManager;)V

    .line 106
    if-eqz p1, :cond_0

    .line 109
    iget-object v2, p0, Leya;->mEntryProvider:Lfaq;

    new-instance v3, Lexz;

    invoke-direct {v3, v1, v0}, Lexz;-><init>(Lizj;Ljcu;)V

    invoke-virtual {v2, v3}, Lfaq;->a(Lfbn;)V

    .line 113
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Leya;->clC:Leyb;

    .line 114
    iget-boolean v0, p0, Leya;->ckI:Z

    if-nez v0, :cond_1

    .line 115
    invoke-virtual {p0}, Leya;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "editPlaceWorkerFragment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 118
    :cond_1
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iput-object v0, p0, Leya;->mSidekickInjector:Lfdb;

    .line 61
    iget-object v0, p0, Leya;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    iput-object v0, p0, Leya;->mEntryProvider:Lfaq;

    .line 63
    invoke-virtual {p0}, Leya;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 64
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v2

    .line 65
    const-string v1, "action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->R([B)Liwk;

    move-result-object v3

    .line 66
    const-string v1, "renamed_place_key"

    const-class v4, Ljcu;

    invoke-static {v0, v1, v4}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v4

    check-cast v4, Ljcu;

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leya;->setRetainInstance(Z)V

    .line 72
    invoke-virtual {p0}, Leya;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Leyf;->a(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    .line 75
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v5

    .line 76
    new-instance v0, Leyb;

    iget-object v1, p0, Leya;->mSidekickInjector:Lfdb;

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Leyb;-><init>(Leya;Lizj;Liwk;Ljcu;Lemp;Lfcx;)V

    iput-object v0, p0, Leya;->clC:Leyb;

    .line 78
    iget-object v0, p0, Leya;->clC:Leyb;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leyb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 79
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Leya;->clC:Leyb;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Leya;->clC:Leyb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leyb;->cancel(Z)Z

    .line 86
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Leya;->clC:Leyb;

    .line 87
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 88
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Leya;->ckI:Z

    .line 93
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 94
    return-void
.end method
