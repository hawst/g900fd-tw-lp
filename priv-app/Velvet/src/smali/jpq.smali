.class public final Ljpq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile exS:[Ljpq;


# instance fields
.field private aeK:F

.field private aez:I

.field private akf:Ljava/lang/String;

.field private exT:F

.field private exU:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Ljpq;->aez:I

    iput v1, p0, Ljpq;->exT:F

    iput v1, p0, Ljpq;->exU:F

    const-string v0, ""

    iput-object v0, p0, Ljpq;->akf:Ljava/lang/String;

    iput v1, p0, Ljpq;->aeK:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljpq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpq;->eCz:I

    .line 108
    return-void
.end method

.method public static brW()[Ljpq;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljpq;->exS:[Ljpq;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljpq;->exS:[Ljpq;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljpq;

    sput-object v0, Ljpq;->exS:[Ljpq;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljpq;->exS:[Ljpq;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljpq;->exT:F

    iget v0, p0, Ljpq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljpq;->exU:F

    iget v0, p0, Ljpq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpq;->akf:Ljava/lang/String;

    iget v0, p0, Ljpq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpq;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljpq;->aeK:F

    iget v0, p0, Ljpq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljpq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Ljpq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x1

    iget v1, p0, Ljpq;->exT:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 127
    :cond_0
    iget v0, p0, Ljpq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 128
    const/4 v0, 0x2

    iget v1, p0, Ljpq;->exU:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 130
    :cond_1
    iget v0, p0, Ljpq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 131
    const/4 v0, 0x3

    iget-object v1, p0, Ljpq;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 133
    :cond_2
    iget v0, p0, Ljpq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 134
    const/4 v0, 0x4

    iget v1, p0, Ljpq;->aeK:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 136
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 137
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 141
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 142
    iget v1, p0, Ljpq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 143
    const/4 v1, 0x1

    iget v2, p0, Ljpq;->exT:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 146
    :cond_0
    iget v1, p0, Ljpq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 147
    const/4 v1, 0x2

    iget v2, p0, Ljpq;->exU:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 150
    :cond_1
    iget v1, p0, Ljpq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 151
    const/4 v1, 0x3

    iget-object v2, p0, Ljpq;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_2
    iget v1, p0, Ljpq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 155
    const/4 v1, 0x4

    iget v2, p0, Ljpq;->aeK:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 158
    :cond_3
    return v0
.end method
