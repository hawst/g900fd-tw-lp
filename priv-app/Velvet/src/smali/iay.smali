.class public final Liay;
.super Lejz;
.source "PG"


# instance fields
.field private dwW:Libb;

.field private dwm:I


# direct methods
.method public constructor <init>(Libb;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lejz;-><init>()V

    .line 27
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 28
    iput-object p1, p0, Liay;->dwW:Libb;

    .line 29
    iput p2, p0, Liay;->dwm:I

    .line 30
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onAppear(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;ILandroid/transition/TransitionValues;I)Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 35
    if-nez p4, :cond_0

    .line 39
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    .line 41
    :cond_0
    iget-object v1, p4, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 42
    iget v0, p0, Liay;->dwm:I

    iget-object v2, p0, Liay;->dwW:Libb;

    iget v2, v2, Libb;->factor:I

    mul-int/2addr v2, v0

    .line 43
    const-string v0, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    int-to-float v5, v2

    aput v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    invoke-static {v1, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 45
    new-instance v3, Liaz;

    invoke-direct {v3, p0, v1, v2}, Liaz;-><init>(Liay;Landroid/view/View;I)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public final onDisappear(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;ILandroid/transition/TransitionValues;I)Landroid/animation/Animator;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 71
    if-eqz p4, :cond_0

    iget-object v1, p4, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 73
    :goto_0
    if-nez v1, :cond_2

    .line 85
    :goto_1
    return-object v0

    .line 71
    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, p2, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_0

    .line 76
    :cond_2
    iget v0, p0, Liay;->dwm:I

    iget-object v2, p0, Liay;->dwW:Libb;

    iget v2, v2, Libb;->factor:I

    mul-int/2addr v0, v2

    .line 77
    const-string v2, "translationY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    int-to-float v0, v0

    aput v0, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 79
    new-instance v2, Liba;

    invoke-direct {v2, p0, v1}, Liba;-><init>(Liay;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_1
.end method
