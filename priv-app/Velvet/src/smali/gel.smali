.class public Lgel;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cHs:Ljava/lang/reflect/Constructor;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cHt:I

.field private final mApplicationContext:Landroid/content/Context;

.field private final mSuggestionLogger:Lgma;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgma;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lgel;->cHt:I

    .line 39
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lgel;->mApplicationContext:Landroid/content/Context;

    .line 40
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgma;

    iput-object v0, p0, Lgel;->mSuggestionLogger:Lgma;

    .line 41
    invoke-static {}, Lgel;->aFn()Ljava/lang/reflect/Constructor;

    move-result-object v0

    iput-object v0, p0, Lgel;->cHs:Ljava/lang/reflect/Constructor;

    .line 42
    return-void
.end method

.method private static aFn()Ljava/lang/reflect/Constructor;
    .locals 4

    .prologue
    .line 144
    :try_start_0
    const-class v0, Landroid/text/style/EasyEditSpan;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/app/PendingIntent;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;ILcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannableString;
    .locals 9

    .prologue
    .line 84
    new-instance v7, Landroid/text/SpannableString;

    invoke-virtual {p3}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 86
    if-eqz p1, :cond_0

    .line 87
    invoke-virtual {p3}, Lcom/google/android/shared/speech/Hypothesis;->asX()Lijj;

    move-result-object v0

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/shared/speech/Hypothesis$Span;

    .line 88
    iget-object v0, v6, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 89
    iget-object v0, v6, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    invoke-virtual {v0, v3}, Lijj;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 90
    new-instance v0, Landroid/text/style/SuggestionSpan;

    iget-object v1, p0, Lgel;->mApplicationContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v4, 0x1

    const-class v5, Lcom/google/android/speech/alternates/SuggestionSpanBroadcastReceiver;

    invoke-direct/range {v0 .. v5}, Landroid/text/style/SuggestionSpan;-><init>(Landroid/content/Context;Ljava/util/Locale;[Ljava/lang/String;ILjava/lang/Class;)V

    .line 93
    iget v1, v6, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    iget v2, v6, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    const/16 v3, 0x21

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 96
    iget-object v2, p0, Lgel;->mSuggestionLogger:Lgma;

    invoke-virtual {v0}, Landroid/text/style/SuggestionSpan;->hashCode()I

    move-result v1

    iget v4, v6, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    iget v5, v6, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    move-object v0, v2

    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lgma;->a(ILjava/lang/String;III)V

    goto :goto_0

    .line 103
    :cond_0
    return-object v7
.end method

.method private declared-synchronized b(Landroid/content/Context;Ljava/lang/String;I)Landroid/text/style/EasyEditSpan;
    .locals 5

    .prologue
    .line 114
    monitor-enter p0

    const/4 v1, 0x0

    .line 115
    :try_start_0
    iget-object v0, p0, Lgel;->cHs:Ljava/lang/reflect/Constructor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 118
    :try_start_1
    iget-object v0, p0, Lgel;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, p2, p3}, Lcom/google/android/speech/alternates/SuggestionSpanBroadcastReceiver;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 121
    iget v2, p0, Lgel;->cHt:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lgel;->cHt:I

    const/4 v3, 0x0

    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 123
    iget-object v2, p0, Lgel;->cHs:Ljava/lang/reflect/Constructor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/style/EasyEditSpan;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    :goto_0
    if-nez v0, :cond_0

    .line 137
    :try_start_2
    new-instance v0, Landroid/text/style/EasyEditSpan;

    invoke-direct {v0}, Landroid/text/style/EasyEditSpan;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    :cond_0
    monitor-exit p0

    return-object v0

    .line 132
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;ILcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannableString;
    .locals 5

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lgel;->b(Ljava/lang/String;ILcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannableString;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lgel;->mApplicationContext:Landroid/content/Context;

    invoke-direct {p0, v1, p1, p2}, Lgel;->b(Landroid/content/Context;Ljava/lang/String;I)Landroid/text/style/EasyEditSpan;

    move-result-object v1

    .line 58
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 60
    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannedString;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lgel;->b(Ljava/lang/String;ILcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannableString;

    move-result-object v0

    .line 72
    invoke-static {v0}, Landroid/text/SpannedString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannedString;

    move-result-object v0

    return-object v0
.end method
