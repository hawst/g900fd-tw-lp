.class public final Lagq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic WN:Lcom/android/launcher3/Workspace;

.field private final Xq:Ljava/util/ArrayList;

.field private final Xr:Lyw;

.field private Xs:Z

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Workspace;Ljava/util/ArrayList;Lyw;)V
    .locals 4

    .prologue
    .line 5123
    iput-object p1, p0, Lagq;->WN:Lcom/android/launcher3/Workspace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5124
    iput-object p2, p0, Lagq;->Xq:Ljava/util/ArrayList;

    .line 5125
    iput-object p3, p0, Lagq;->Xr:Lyw;

    .line 5126
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lagq;->mHandler:Landroid/os/Handler;

    .line 5127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lagq;->Xs:Z

    .line 5129
    iget-object v0, p0, Lagq;->Xr:Lyw;

    invoke-virtual {v0, p0}, Lyw;->b(Ljava/lang/Runnable;)V

    .line 5132
    iget-object v0, p0, Lagq;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5133
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 5137
    iget-object v0, p0, Lagq;->Xr:Lyw;

    invoke-virtual {v0, p0}, Lyw;->c(Ljava/lang/Runnable;)V

    .line 5138
    iget-object v0, p0, Lagq;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5140
    iget-boolean v0, p0, Lagq;->Xs:Z

    if-nez v0, :cond_1

    .line 5157
    :cond_0
    return-void

    .line 5144
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lagq;->Xs:Z

    .line 5146
    iget-object v0, p0, Lagq;->Xq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyy;

    .line 5147
    iget-object v1, v0, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    instance-of v1, v1, Lacz;

    if-eqz v1, :cond_2

    .line 5148
    iget-object v1, v0, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    check-cast v1, Lacz;

    .line 5149
    iget-object v2, p0, Lagq;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v2}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Workspace;)Lcom/android/launcher3/Launcher;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Launcher;->a(Lyy;)V

    .line 5151
    invoke-virtual {v1}, Lacz;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout;

    .line 5153
    invoke-virtual {v2, v1}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 5154
    iget-object v1, p0, Lagq;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v1}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Workspace;)Lcom/android/launcher3/Launcher;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher3/Launcher;->b(Lyy;)V

    goto :goto_0
.end method
