.class public final Lhva;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aWG:Landroid/content/IntentFilter;

.field final cOx:Lenw;

.field private final drJ:Landroid/content/BroadcastReceiver;

.field private drK:Z

.field drL:Lhvc;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhva;->aWG:Landroid/content/IntentFilter;

    .line 30
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lhva;->cOx:Lenw;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhva;->drK:Z

    .line 41
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lhva;->mContext:Landroid/content/Context;

    .line 42
    new-instance v0, Lhvb;

    invoke-direct {v0, p0}, Lhvb;-><init>(Lhva;)V

    iput-object v0, p0, Lhva;->drJ:Landroid/content/BroadcastReceiver;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lhvc;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52
    iget-object v0, p0, Lhva;->drL:Lhvc;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 53
    iget-boolean v0, p0, Lhva;->drK:Z

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 55
    iget-object v0, p0, Lhva;->cOx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 57
    iput-object p1, p0, Lhva;->drL:Lhvc;

    .line 59
    iput-boolean v1, p0, Lhva;->drK:Z

    .line 60
    iget-object v0, p0, Lhva;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lhva;->drJ:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lhva;->aWG:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 61
    return-void

    :cond_1
    move v0, v2

    .line 52
    goto :goto_0
.end method

.method public final unregister()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lhva;->cOx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 66
    iget-boolean v0, p0, Lhva;->drK:Z

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhva;->drK:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lhva;->drL:Lhvc;

    .line 69
    iget-object v0, p0, Lhva;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lhva;->drJ:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 71
    :cond_0
    return-void
.end method
