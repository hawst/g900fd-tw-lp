.class public final Lanz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public aiU:Laoi;

.field private aiy:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 3932
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3933
    iput v0, p0, Lanz;->aez:I

    iput-object v1, p0, Lanz;->aiU:Laoi;

    iput-boolean v0, p0, Lanz;->aiy:Z

    iput-object v1, p0, Lanz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lanz;->eCz:I

    .line 3934
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3891
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lanz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lanz;->aiU:Laoi;

    if-nez v0, :cond_1

    new-instance v0, Laoi;

    invoke-direct {v0}, Laoi;-><init>()V

    iput-object v0, p0, Lanz;->aiU:Laoi;

    :cond_1
    iget-object v0, p0, Lanz;->aiU:Laoi;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lanz;->aiy:Z

    iget v0, p0, Lanz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3948
    iget-object v0, p0, Lanz;->aiU:Laoi;

    if-eqz v0, :cond_0

    .line 3949
    const/4 v0, 0x1

    iget-object v1, p0, Lanz;->aiU:Laoi;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3951
    :cond_0
    iget v0, p0, Lanz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 3952
    const/4 v0, 0x2

    iget-boolean v1, p0, Lanz;->aiy:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 3954
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3955
    return-void
.end method

.method public final ba(Z)Lanz;
    .locals 1

    .prologue
    .line 3919
    const/4 v0, 0x1

    iput-boolean v0, p0, Lanz;->aiy:Z

    .line 3920
    iget v0, p0, Lanz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanz;->aez:I

    .line 3921
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3959
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3960
    iget-object v1, p0, Lanz;->aiU:Laoi;

    if-eqz v1, :cond_0

    .line 3961
    const/4 v1, 0x1

    iget-object v2, p0, Lanz;->aiU:Laoi;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3964
    :cond_0
    iget v1, p0, Lanz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 3965
    const/4 v1, 0x2

    iget-boolean v2, p0, Lanz;->aiy:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3968
    :cond_1
    return v0
.end method

.method public final pN()Z
    .locals 1

    .prologue
    .line 3916
    iget-boolean v0, p0, Lanz;->aiy:Z

    return v0
.end method
