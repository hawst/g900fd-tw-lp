.class final Ldge;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final bwU:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Ldge;->bwU:Ljava/util/Map;

    .line 111
    return-void
.end method

.method private i(Ldgb;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Ldge;->bwU:Ljava/util/Map;

    if-nez v0, :cond_0

    move v0, v1

    .line 140
    :goto_0
    return v0

    .line 139
    :cond_0
    iget-object v0, p0, Ldge;->bwU:Ljava/util/Map;

    invoke-interface {p1}, Ldgb;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 140
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 106
    check-cast p1, Ldgb;

    check-cast p2, Ldgb;

    invoke-interface {p1}, Ldgb;->abP()Z

    move-result v0

    invoke-interface {p2}, Ldgb;->abP()Z

    move-result v1

    if-eq v0, v1, :cond_2

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2}, Ldge;->i(Ldgb;)I

    move-result v0

    invoke-direct {p0, p1}, Ldge;->i(Ldgb;)I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
