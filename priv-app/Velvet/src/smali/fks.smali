.class public final Lfks;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cuh:Ljava/util/List;

.field private final cui:Ljava/text/NumberFormat;

.field public final mClock:Lemp;


# direct methods
.method public constructor <init>(Lemp;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lfks;->mClock:Lemp;

    .line 43
    iput-object p2, p0, Lfks;->cuh:Ljava/util/List;

    .line 44
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lfks;->cui:Ljava/text/NumberFormat;

    .line 45
    return-void
.end method

.method public static a(Landroid/content/Context;D)I
    .locals 3

    .prologue
    .line 106
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const v0, 0x7f0b00b9

    .line 107
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 106
    :cond_0
    const v0, 0x7f0b00b7

    goto :goto_0
.end method

.method public static a(FLjava/text/NumberFormat;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 85
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x461c4000    # 10000.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 91
    :goto_0
    float-to-double v0, p0

    invoke-static {p1, v0, v1}, Lemg;->a(Ljava/text/NumberFormat;D)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 88
    :cond_0
    invoke-virtual {p1, v2}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 89
    invoke-virtual {p1, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lemp;J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 170
    invoke-interface {p1}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    .line 171
    invoke-static {p2, p3, v0, v1}, Lesi;->i(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    const-wide/16 v2, 0x0

    sub-long/2addr v0, p2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 173
    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lesi;->b(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x19

    invoke-static {p0, p2, p3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Ljgx;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 74
    invoke-virtual {p0}, Ljgx;->blr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Ljgx;->blq()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1, v2, v2}, Lemg;->a(DII)Ljava/lang/String;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljgx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Ljgx;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljgx;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    invoke-virtual {p0}, Ljgx;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lfks;->e(Ljgx;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Ljgx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0}, Ljgx;->blu()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljgx;->blt()Ljava/lang/String;

    move-result-object v0

    .line 263
    :goto_0
    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 259
    :cond_0
    invoke-virtual {p0}, Ljgx;->getSymbol()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljgx;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 126
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Ljgx;->blm()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 127
    const v2, 0x7f0a0367

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p2}, Ljgx;->bjs()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Ljgx;->getSymbol()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lfks;->mClock:Lemp;

    invoke-static {p1, v5, v0, v1}, Lfks;->a(Landroid/content/Context;Lemp;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljgx;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p1}, Ljgx;->blo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p1}, Ljgx;->bln()F

    move-result v0

    iget-object v1, p0, Lfks;->cui:Ljava/text/NumberFormat;

    invoke-static {v0, v1}, Lfks;->a(FLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljgx;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Ljgx;->rr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p1}, Ljgx;->blp()F

    move-result v0

    iget-object v1, p0, Lfks;->cui:Ljava/text/NumberFormat;

    invoke-static {v0, v1}, Lfks;->a(FLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
