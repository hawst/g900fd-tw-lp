.class public final Liaf;
.super Liah;
.source "PG"


# instance fields
.field private final dwk:Libb;

.field private final dwl:Libb;

.field private final dwm:I


# direct methods
.method public constructor <init>(Libb;Libb;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Liah;-><init>()V

    .line 29
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 30
    iput-object p2, p0, Liaf;->dwl:Libb;

    .line 31
    iput-object p1, p0, Liaf;->dwk:Libb;

    .line 32
    iput p3, p0, Liaf;->dwm:I

    .line 33
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;Landroid/transition/TransitionValues;Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;)Landroid/animation/Animator;
    .locals 15

    .prologue
    .line 39
    move-object/from16 v0, p3

    iget-object v3, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 40
    instance-of v2, v3, Lduc;

    if-nez v2, :cond_0

    .line 41
    const/4 v2, 0x0

    .line 81
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v3

    .line 44
    check-cast v2, Lduc;

    .line 45
    invoke-virtual {v2}, Lduc;->ahl()[Landroid/view/View;

    move-result-object v5

    .line 46
    array-length v4, v5

    new-array v6, v4, [Landroid/animation/Animator;

    move-object v4, v3

    .line 48
    check-cast v4, Lduc;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v7

    move-object/from16 v0, p2

    invoke-virtual {v4, v7, v0}, Lduc;->b(ILjava/lang/CharSequence;)V

    .line 50
    const/4 v4, 0x0

    :goto_1
    array-length v7, v5

    if-ge v4, v7, :cond_1

    .line 51
    aget-object v7, v5, v4

    .line 52
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    .line 54
    iget v9, p0, Liaf;->dwm:I

    iget-object v10, p0, Liaf;->dwk:Libb;

    iget v10, v10, Libb;->factor:I

    mul-int/2addr v9, v10

    .line 55
    const-string v10, "translationY"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    invoke-virtual {v3}, Landroid/view/View;->getTranslationY()F

    move-result v13

    aput v13, v11, v12

    const/4 v12, 0x1

    int-to-float v9, v9

    aput v9, v11, v12

    invoke-static {v7, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 57
    const-string v10, "alpha"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    invoke-virtual {v3}, Landroid/view/View;->getAlpha()F

    move-result v13

    aput v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v7, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 59
    new-instance v11, Liag;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v11, p0, v2, v0, v1}, Liag;-><init>(Liaf;Lduc;Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;)V

    invoke-virtual {v10, v11}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 68
    iget v11, p0, Liaf;->dwm:I

    iget-object v12, p0, Liaf;->dwl:Libb;

    iget v12, v12, Libb;->factor:I

    mul-int/2addr v11, v12

    .line 69
    const-string v12, "translationY"

    const/4 v13, 0x2

    new-array v13, v13, [F

    const/4 v14, 0x0

    int-to-float v11, v11

    aput v11, v13, v14

    const/4 v11, 0x1

    const/4 v14, 0x0

    aput v14, v13, v11

    invoke-static {v7, v12, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 71
    const-string v12, "alpha"

    const/4 v13, 0x2

    new-array v13, v13, [F

    fill-array-data v13, :array_0

    invoke-static {v7, v12, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 74
    const/4 v12, 0x2

    new-array v12, v12, [Landroid/animation/Animator;

    const/4 v13, 0x0

    aput-object v9, v12, v13

    const/4 v13, 0x1

    aput-object v10, v12, v13

    invoke-virtual {v8, v12}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 75
    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/Animator;

    const/4 v12, 0x0

    aput-object v11, v10, v12

    const/4 v12, 0x1

    aput-object v7, v10, v12

    invoke-virtual {v8, v10}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 76
    const/4 v7, 0x2

    new-array v7, v7, [Landroid/animation/Animator;

    const/4 v10, 0x0

    aput-object v9, v7, v10

    const/4 v9, 0x1

    aput-object v11, v7, v9

    invoke-virtual {v8, v7}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 77
    aput-object v8, v6, v4

    .line 50
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 79
    :cond_1
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 80
    invoke-virtual {v2, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto/16 :goto_0

    .line 71
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
