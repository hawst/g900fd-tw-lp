.class public final Lfto;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 7

    .prologue
    const v6, 0x7f1102f0

    const v5, 0x7f11030d

    const/4 v4, 0x0

    .line 34
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 36
    iget-object v2, v0, Lanh;->agw:Lany;

    .line 38
    const v0, 0x7f1101a9

    invoke-virtual {v2}, Lany;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfto;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 39
    const v0, 0x7f110080

    invoke-virtual {v2}, Lany;->ol()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfto;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 40
    invoke-virtual {v2}, Lany;->on()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v6, v0}, Lfto;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 41
    const v0, 0x7f1100b0

    invoke-virtual {v2}, Lany;->oo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfto;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 43
    invoke-virtual {v2}, Lany;->qg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    invoke-virtual {v2}, Lany;->qf()I

    move-result v3

    invoke-virtual {v0, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 49
    :cond_0
    iget-object v0, v2, Lany;->aiS:Ljcn;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, v2, Lany;->aiS:Ljcn;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v5, v0}, Lfto;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 53
    :cond_1
    iget-object v0, v2, Lany;->aiT:Ljcn;

    if-eqz v0, :cond_2

    .line 54
    iget-object v0, v2, Lany;->aiT:Ljcn;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v5, v0}, Lfto;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 55
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;

    .line 57
    iget-object v1, p0, Lfto;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0167

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 58
    iget-object v2, p0, Lfto;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0168

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 60
    invoke-virtual {v0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 61
    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 62
    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 63
    invoke-virtual {v0, v3}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    int-to-float v1, v2

    int-to-float v3, v2

    int-to-float v4, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->c(FFFF)V

    .line 66
    :cond_2
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 29
    const v0, 0x7f04011b

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
