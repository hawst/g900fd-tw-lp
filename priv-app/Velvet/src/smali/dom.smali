.class final Ldom;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic bEm:Ldoj;


# direct methods
.method constructor <init>(Ldoj;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Ldom;->bEm:Ldoj;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.googlequicksearchbox.action.PAUSE_HOTWORD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    const-string v0, "com.google.android.googlequicksearchbox.extra.PAUSE_HOTWORD_REQUESTING_PACKAGE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    iget-object v1, p0, Ldom;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mHotwordState:Ldby;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Ldby;->a(ZLjava/lang/String;)V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.googlequicksearchbox.action.RESUME_HOTWORD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const-string v0, "com.google.android.googlequicksearchbox.extra.PAUSE_HOTWORD_REQUESTING_PACKAGE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 135
    iget-object v1, p0, Ldom;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mHotwordState:Ldby;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ldby;->a(ZLjava/lang/String;)V

    goto :goto_0
.end method
