.class public final Ljfg;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efQ:I

.field private ehO:Z

.field private ehP:Z

.field private ehQ:Z

.field private ehR:[Ljfh;

.field private ehS:[Ljfi;

.field private ehT:[Ljfj;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 12019
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12020
    const/4 v0, 0x0

    iput v0, p0, Ljfg;->aez:I

    iput-object v2, p0, Ljfg;->efN:Ljez;

    iput v1, p0, Ljfg;->efQ:I

    iput-boolean v1, p0, Ljfg;->ehO:Z

    iput-boolean v1, p0, Ljfg;->ehP:Z

    iput-boolean v1, p0, Ljfg;->ehQ:Z

    invoke-static {}, Ljfh;->bjn()[Ljfh;

    move-result-object v0

    iput-object v0, p0, Ljfg;->ehR:[Ljfh;

    invoke-static {}, Ljfi;->bjp()[Ljfi;

    move-result-object v0

    iput-object v0, p0, Ljfg;->ehS:[Ljfi;

    invoke-static {}, Ljfj;->bjq()[Ljfj;

    move-result-object v0

    iput-object v0, p0, Ljfg;->ehT:[Ljfj;

    iput-object v2, p0, Ljfg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfg;->eCz:I

    .line 12021
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11038
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfg;->ehO:Z

    iget v0, p0, Ljfg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfg;->ehP:Z

    iget v0, p0, Ljfg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfg;->ehQ:Z

    iget v0, p0, Ljfg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfg;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljfg;->ehR:[Ljfh;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljfh;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljfg;->ehR:[Ljfh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljfh;

    invoke-direct {v3}, Ljfh;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljfg;->ehR:[Ljfh;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljfh;

    invoke-direct {v3}, Ljfh;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljfg;->ehR:[Ljfh;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljfg;->efN:Ljez;

    if-nez v0, :cond_4

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfg;->efN:Ljez;

    :cond_4
    iget-object v0, p0, Ljfg;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljfg;->efQ:I

    iget v0, p0, Ljfg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfg;->aez:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljfg;->ehS:[Ljfi;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljfi;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljfg;->ehS:[Ljfi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljfi;

    invoke-direct {v3}, Ljfi;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljfg;->ehS:[Ljfi;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljfi;

    invoke-direct {v3}, Ljfi;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljfg;->ehS:[Ljfi;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljfg;->ehT:[Ljfj;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljfj;

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljfg;->ehT:[Ljfj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Ljfj;

    invoke-direct {v3}, Ljfj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ljfg;->ehT:[Ljfj;

    array-length v0, v0

    goto :goto_5

    :cond_a
    new-instance v3, Ljfj;

    invoke-direct {v3}, Ljfj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljfg;->ehT:[Ljfj;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12041
    iget v0, p0, Ljfg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 12042
    const/4 v0, 0x1

    iget-boolean v2, p0, Ljfg;->ehO:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 12044
    :cond_0
    iget v0, p0, Ljfg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 12045
    const/4 v0, 0x2

    iget-boolean v2, p0, Ljfg;->ehP:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 12047
    :cond_1
    iget v0, p0, Ljfg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 12048
    const/4 v0, 0x3

    iget-boolean v2, p0, Ljfg;->ehQ:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 12050
    :cond_2
    iget-object v0, p0, Ljfg;->ehR:[Ljfh;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljfg;->ehR:[Ljfh;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 12051
    :goto_0
    iget-object v2, p0, Ljfg;->ehR:[Ljfh;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 12052
    iget-object v2, p0, Ljfg;->ehR:[Ljfh;

    aget-object v2, v2, v0

    .line 12053
    if-eqz v2, :cond_3

    .line 12054
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 12051
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12058
    :cond_4
    iget-object v0, p0, Ljfg;->efN:Ljez;

    if-eqz v0, :cond_5

    .line 12059
    const/4 v0, 0x5

    iget-object v2, p0, Ljfg;->efN:Ljez;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 12061
    :cond_5
    iget v0, p0, Ljfg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    .line 12062
    const/4 v0, 0x6

    iget v2, p0, Ljfg;->efQ:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 12064
    :cond_6
    iget-object v0, p0, Ljfg;->ehS:[Ljfi;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljfg;->ehS:[Ljfi;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 12065
    :goto_1
    iget-object v2, p0, Ljfg;->ehS:[Ljfi;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 12066
    iget-object v2, p0, Ljfg;->ehS:[Ljfi;

    aget-object v2, v2, v0

    .line 12067
    if-eqz v2, :cond_7

    .line 12068
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 12065
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 12072
    :cond_8
    iget-object v0, p0, Ljfg;->ehT:[Ljfj;

    if-eqz v0, :cond_a

    iget-object v0, p0, Ljfg;->ehT:[Ljfj;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 12073
    :goto_2
    iget-object v0, p0, Ljfg;->ehT:[Ljfj;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 12074
    iget-object v0, p0, Ljfg;->ehT:[Ljfj;

    aget-object v0, v0, v1

    .line 12075
    if-eqz v0, :cond_9

    .line 12076
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 12073
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 12080
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12081
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 12085
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12086
    iget v2, p0, Ljfg;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 12087
    const/4 v2, 0x1

    iget-boolean v3, p0, Ljfg;->ehO:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 12090
    :cond_0
    iget v2, p0, Ljfg;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 12091
    const/4 v2, 0x2

    iget-boolean v3, p0, Ljfg;->ehP:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 12094
    :cond_1
    iget v2, p0, Ljfg;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    .line 12095
    const/4 v2, 0x3

    iget-boolean v3, p0, Ljfg;->ehQ:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 12098
    :cond_2
    iget-object v2, p0, Ljfg;->ehR:[Ljfh;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljfg;->ehR:[Ljfh;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 12099
    :goto_0
    iget-object v3, p0, Ljfg;->ehR:[Ljfh;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 12100
    iget-object v3, p0, Ljfg;->ehR:[Ljfh;

    aget-object v3, v3, v0

    .line 12101
    if-eqz v3, :cond_3

    .line 12102
    const/4 v4, 0x4

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 12099
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 12107
    :cond_5
    iget-object v2, p0, Ljfg;->efN:Ljez;

    if-eqz v2, :cond_6

    .line 12108
    const/4 v2, 0x5

    iget-object v3, p0, Ljfg;->efN:Ljez;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 12111
    :cond_6
    iget v2, p0, Ljfg;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_7

    .line 12112
    const/4 v2, 0x6

    iget v3, p0, Ljfg;->efQ:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 12115
    :cond_7
    iget-object v2, p0, Ljfg;->ehS:[Ljfi;

    if-eqz v2, :cond_a

    iget-object v2, p0, Ljfg;->ehS:[Ljfi;

    array-length v2, v2

    if-lez v2, :cond_a

    move v2, v0

    move v0, v1

    .line 12116
    :goto_1
    iget-object v3, p0, Ljfg;->ehS:[Ljfi;

    array-length v3, v3

    if-ge v0, v3, :cond_9

    .line 12117
    iget-object v3, p0, Ljfg;->ehS:[Ljfi;

    aget-object v3, v3, v0

    .line 12118
    if-eqz v3, :cond_8

    .line 12119
    const/4 v4, 0x7

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 12116
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    move v0, v2

    .line 12124
    :cond_a
    iget-object v2, p0, Ljfg;->ehT:[Ljfj;

    if-eqz v2, :cond_c

    iget-object v2, p0, Ljfg;->ehT:[Ljfj;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 12125
    :goto_2
    iget-object v2, p0, Ljfg;->ehT:[Ljfj;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 12126
    iget-object v2, p0, Ljfg;->ehT:[Ljfj;

    aget-object v2, v2, v1

    .line 12127
    if-eqz v2, :cond_b

    .line 12128
    const/16 v3, 0x8

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 12125
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 12133
    :cond_c
    return v0
.end method
