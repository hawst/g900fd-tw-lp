.class public final Lgbh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Ljdq;I)Laok;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    const/4 v3, 0x0

    .line 89
    invoke-virtual {p1}, Ljdq;->getSource()I

    move-result v0

    if-ne v0, v8, :cond_4

    move v0, v1

    .line 93
    :goto_0
    new-instance v4, Laok;

    invoke-direct {v4}, Laok;-><init>()V

    .line 94
    invoke-virtual {p1}, Ljdq;->bay()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 95
    invoke-virtual {p1}, Ljdq;->bax()I

    move-result v5

    invoke-static {v5}, Lgbe;->jR(I)D

    move-result-wide v6

    .line 97
    invoke-static {v6, v7}, Lgbe;->h(D)I

    move-result v5

    invoke-virtual {v4, v5}, Laok;->cE(I)Laok;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-static {v6, v7}, Lgbe;->g(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Laok;->bU(Ljava/lang/String;)Laok;

    .line 103
    :cond_0
    invoke-virtual {p1}, Ljdq;->getSource()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 112
    :pswitch_0
    invoke-virtual {p1}, Ljdq;->baw()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 113
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f100036

    invoke-virtual {p1}, Ljdq;->bav()I

    move-result v5

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljdq;->bav()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v0, v3, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 120
    :goto_1
    const-string v3, " \u00b7 "

    new-array v5, v8, [Ljava/lang/CharSequence;

    invoke-virtual {p1}, Ljdq;->qo()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v0, v5, v1

    invoke-static {v3, v5}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 122
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 123
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Laok;->bV(Ljava/lang/String;)Laok;

    .line 125
    invoke-virtual {p1}, Ljdq;->bhL()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 126
    :goto_2
    if-eqz v2, :cond_1

    .line 127
    invoke-virtual {v4, v2}, Laok;->cF(I)Laok;

    .line 132
    :cond_1
    invoke-virtual {p1}, Ljdq;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p2, :cond_2

    .line 134
    new-instance v2, Lfsa;

    invoke-direct {v2, p2}, Lfsa;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v0

    iput-object v0, v4, Laok;->ahs:Lani;

    .line 138
    :cond_2
    return-object v4

    .line 105
    :pswitch_1
    const-string v0, ""

    goto :goto_1

    .line 108
    :pswitch_2
    const v0, 0x7f0a0494

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 125
    :pswitch_3
    const v2, 0x7f0201a4

    goto :goto_2

    :pswitch_4
    const v2, 0x7f0201a5

    goto :goto_2

    :cond_3
    move-object v0, v3

    goto :goto_1

    :cond_4
    move v0, v2

    goto/16 :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 125
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
