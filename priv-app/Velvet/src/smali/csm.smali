.class public Lcsm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final aGx:Ljava/lang/String;

.field private aTa:Ljava/util/concurrent/Future;

.field private final anC:I

.field private final anL:I

.field private final bhO:Lggg;

.field private final bhP:Ljava/util/concurrent/ExecutorService;

.field private final bhQ:F

.field private final bhR:[F

.field private final bhS:[F

.field private final bhT:[F

.field private final bhU:Lcom/google/speech/micro/GoogleHotwordData;

.field private final bhV:Lcrz;

.field private bhW:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bhX:Ljava/lang/Runnable;

.field private final mInputStream:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lggg;Ljava/util/concurrent/ExecutorService;ILcrz;Ljava/lang/String;IF[F[F[FLjava/lang/String;)V
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Lcsn;

    const-string v1, "Hotword loop"

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {v0, p0, v1, v2}, Lcsn;-><init>(Lcsm;Ljava/lang/String;[I)V

    iput-object v0, p0, Lcsm;->bhX:Ljava/lang/Runnable;

    .line 85
    iput-object p5, p0, Lcsm;->bhV:Lcrz;

    .line 86
    invoke-interface {p5, p12}, Lcrz;->iG(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcsm;->bhW:[B

    .line 87
    iput-object p1, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    .line 88
    iput-object p2, p0, Lcsm;->bhO:Lggg;

    .line 89
    iput p4, p0, Lcsm;->anL:I

    .line 90
    iput-object p3, p0, Lcsm;->bhP:Ljava/util/concurrent/ExecutorService;

    .line 91
    iput p7, p0, Lcsm;->anC:I

    .line 92
    iput p8, p0, Lcsm;->bhQ:F

    .line 93
    iput-object p12, p0, Lcsm;->aGx:Ljava/lang/String;

    .line 94
    invoke-interface {p5, p6}, Lcrz;->iF(Ljava/lang/String;)[B

    move-result-object v0

    iget v1, p0, Lcsm;->anL:I

    invoke-virtual {p0, v0, v1}, Lcsm;->a([BI)Lcom/google/speech/micro/GoogleHotwordData;

    move-result-object v0

    iput-object v0, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    .line 97
    if-eqz p9, :cond_1

    if-eqz p10, :cond_1

    if-eqz p11, :cond_1

    array-length v0, p9

    if-eqz v0, :cond_1

    array-length v0, p10

    if-eqz v0, :cond_0

    array-length v0, p9

    add-int/lit8 v0, v0, -0x1

    array-length v1, p10

    if-ne v0, v1, :cond_1

    :cond_0
    array-length v0, p11

    if-eqz v0, :cond_2

    array-length v0, p9

    add-int/lit8 v0, v0, -0x1

    array-length v1, p11

    if-eq v0, v1, :cond_2

    .line 105
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    iput-object v0, p0, Lcsm;->bhR:[F

    .line 106
    const/4 v0, 0x0

    new-array v0, v0, [F

    iput-object v0, p0, Lcsm;->bhS:[F

    .line 107
    const/4 v0, 0x0

    new-array v0, v0, [F

    iput-object v0, p0, Lcsm;->bhT:[F

    .line 113
    :goto_0
    return-void

    .line 109
    :cond_2
    iput-object p9, p0, Lcsm;->bhR:[F

    .line 110
    iput-object p10, p0, Lcsm;->bhS:[F

    .line 111
    iput-object p11, p0, Lcsm;->bhT:[F

    goto :goto_0
.end method

.method private static Df()V
    .locals 1

    .prologue
    .line 545
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 546
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 548
    :cond_0
    return-void
.end method

.method private SH()Lcom/google/android/shared/speech/HotwordResult;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/16 v11, 0x59

    const/4 v10, 0x4

    const/4 v3, 0x0

    .line 183
    .line 185
    const/16 v1, 0x58

    :try_start_0
    invoke-static {v1}, Lege;->ht(I)V

    .line 187
    invoke-virtual {p0}, Lcsm;->uf()Lcom/google/speech/micro/GoogleHotwordRecognizer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 188
    :try_start_1
    invoke-static {}, Lcsm;->Df()V

    .line 190
    iget-object v2, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordData;->idealBufferBytes()I

    move-result v7

    .line 194
    mul-int/lit16 v2, v7, 0x3e8

    iget v4, p0, Lcsm;->anL:I

    mul-int/lit8 v4, v4, 0x2

    div-int/2addr v2, v4

    .line 195
    new-instance v8, Lcsa;

    add-int/lit16 v4, v2, 0x7d0

    iget v5, p0, Lcsm;->anL:I

    const/4 v6, 0x2

    invoke-direct {v8, v4, v5, v6, v2}, Lcsa;-><init>(IIII)V

    move v2, v3

    .line 200
    :cond_0
    iget-object v4, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v8, v4}, Lcsa;->g(Ljava/io/InputStream;)Lcsb;

    move-result-object v9

    .line 201
    invoke-static {}, Lcsm;->Df()V

    move-object v4, v0

    move v5, v3

    .line 206
    :goto_0
    if-nez v2, :cond_1

    iget v6, v9, Lcsb;->size:I

    if-ge v5, v6, :cond_1

    .line 207
    iget-object v2, v8, Lcsa;->rV:[B

    iget v4, v9, Lcsb;->offset:I

    add-int/2addr v4, v5

    iget v6, v9, Lcsb;->size:I

    sub-int/2addr v6, v5

    invoke-virtual {v1, v2, v4, v6}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->process([BII)Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;

    move-result-object v2

    .line 210
    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;->hotwordDetected()Z

    move-result v6

    .line 211
    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;->getBytesConsumed()I

    move-result v4

    add-int/2addr v4, v5

    move v5, v4

    move-object v4, v2

    move v2, v6

    goto :goto_0

    .line 213
    :cond_1
    if-eqz v2, :cond_6

    .line 216
    invoke-virtual {v8}, Lcsa;->SB()[B

    move-result-object v0

    array-length v0, v0

    sub-int v2, v7, v5

    sub-int/2addr v0, v2

    .line 218
    iget v2, p0, Lcsm;->anL:I

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit16 v2, v2, 0x7d0

    div-int/lit16 v2, v2, 0x3e8

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 220
    new-array v5, v2, [B

    .line 221
    sub-int/2addr v0, v2

    .line 222
    invoke-virtual {v8}, Lcsa;->SB()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v0, v5, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 224
    iget v0, p0, Lcsm;->anC:I

    const/4 v6, 0x3

    if-eq v0, v6, :cond_3

    .line 226
    invoke-virtual {p0, v1, v4, v5}, Lcsm;->a(Lcom/google/speech/micro/GoogleHotwordRecognizer;Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;[B)Lcom/google/android/shared/speech/HotwordResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 265
    iget-object v2, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 266
    if-eqz v1, :cond_2

    .line 267
    invoke-virtual {v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->close()V

    .line 269
    :cond_2
    iget-object v1, p0, Lcsm;->bhP:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 270
    invoke-static {v11}, Lege;->ht(I)V

    .line 272
    const-string v1, "HotwordRecognitionRnr"

    const-string v2, "Hotword detection finished"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v10, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :goto_1
    return-object v0

    .line 232
    :cond_3
    :try_start_2
    iget v0, p0, Lcsm;->anL:I

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x14

    div-int/lit16 v6, v0, 0x3e8

    .line 234
    mul-int/lit8 v7, v6, 0xa

    .line 235
    new-array v8, v7, [B

    move v0, v3

    .line 237
    :goto_2
    if-ge v0, v7, :cond_4

    .line 238
    iget-object v9, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v9, v8, v0, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v9

    .line 240
    invoke-static {}, Lcsm;->Df()V

    .line 241
    if-ltz v9, :cond_4

    .line 245
    add-int/2addr v0, v9

    .line 248
    goto :goto_2

    .line 249
    :cond_4
    add-int v6, v2, v0

    new-array v6, v6, [B

    .line 251
    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v5, v7, v6, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 253
    const/4 v5, 0x0

    invoke-static {v8, v5, v6, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    invoke-virtual {p0, v1, v4, v6}, Lcsm;->a(Lcom/google/speech/micro/GoogleHotwordRecognizer;Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;[B)Lcom/google/android/shared/speech/HotwordResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 265
    iget-object v2, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 266
    if-eqz v1, :cond_5

    .line 267
    invoke-virtual {v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->close()V

    .line 269
    :cond_5
    iget-object v1, p0, Lcsm;->bhP:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 270
    invoke-static {v11}, Lege;->ht(I)V

    .line 272
    const-string v1, "HotwordRecognitionRnr"

    const-string v2, "Hotword detection finished"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v10, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 258
    :cond_6
    :try_start_3
    iget v4, v9, Lcsb;->size:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-ge v4, v7, :cond_0

    .line 261
    iget-object v2, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 266
    if-eqz v1, :cond_7

    .line 267
    invoke-virtual {v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->close()V

    .line 269
    :cond_7
    iget-object v1, p0, Lcsm;->bhP:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 270
    invoke-static {v11}, Lege;->ht(I)V

    .line 272
    const-string v1, "HotwordRecognitionRnr"

    const-string v2, "Hotword detection finished"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v10, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 265
    :catchall_0
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    :goto_3
    iget-object v2, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 266
    if-eqz v1, :cond_8

    .line 267
    invoke-virtual {v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->close()V

    .line 269
    :cond_8
    iget-object v1, p0, Lcsm;->bhP:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 270
    invoke-static {v11}, Lege;->ht(I)V

    .line 272
    const-string v1, "HotwordRecognitionRnr"

    const-string v2, "Hotword detection finished"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v10, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    throw v0

    .line 265
    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method private a(F[F)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 415
    array-length v1, p2

    if-nez v1, :cond_1

    .line 416
    iget-object v1, p0, Lcsm;->bhR:[F

    aget v0, v1, v0

    .line 427
    :goto_0
    return v0

    .line 420
    :cond_0
    add-int/lit8 v0, v0, 0x1

    :cond_1
    array-length v1, p2

    if-ge v0, v1, :cond_2

    .line 421
    aget v1, p2, v0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    .line 422
    iget-object v1, p0, Lcsm;->bhR:[F

    aget v0, v1, v0

    goto :goto_0

    .line 427
    :cond_2
    iget-object v0, p0, Lcsm;->bhR:[F

    iget-object v1, p0, Lcsm;->bhR:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method private static d([BII)F
    .locals 8

    .prologue
    .line 447
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 461
    :goto_0
    return v0

    .line 449
    :cond_1
    const-wide/16 v0, 0x0

    .line 450
    div-int/lit8 v3, p2, 0x2

    .line 452
    add-int v2, p1, p2

    .line 453
    :goto_1
    add-int/lit8 v4, p1, 0x2

    if-lt v2, v4, :cond_2

    .line 454
    add-int/lit8 v4, v2, -0x1

    aget-byte v4, p0, v4

    shl-int/lit8 v4, v4, 0x8

    add-int/lit8 v5, v2, -0x2

    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v4, v5

    int-to-short v4, v4

    .line 455
    int-to-long v6, v4

    int-to-long v4, v4

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 456
    add-int/lit8 v2, v2, -0x2

    .line 457
    goto :goto_1

    .line 458
    :cond_2
    int-to-long v2, v3

    div-long/2addr v0, v2

    .line 461
    long-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x46fffe00    # 32767.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method final SG()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 124
    const-string v0, "HotwordRecognitionRnr"

    const-string v1, "Starting hotword detection."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 126
    :try_start_0
    invoke-direct {p0}, Lcsm;->SH()Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_1

    .line 128
    iget-object v1, p0, Lcsm;->bhO:Lggg;

    new-instance v2, Leho;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v0}, Leho;-><init>(ILcom/google/android/shared/speech/HotwordResult;)V

    invoke-interface {v1, v2}, Lggg;->a(Lehu;)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget v0, p0, Lcsm;->anC:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 134
    const v0, 0xdd1321

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 135
    iget-object v0, p0, Lcsm;->bhO:Lggg;

    new-instance v1, Leho;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Leho;-><init>(ILcom/google/android/shared/speech/HotwordResult;)V

    invoke-interface {v0, v1}, Lggg;->a(Lehu;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 146
    iget-object v1, p0, Lcsm;->bhO:Lggg;

    new-instance v2, Leic;

    const-string v3, "Error at processing input stream"

    invoke-direct {v2, v3, v0}, Leic;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lggg;->b(Leiq;)V

    goto :goto_0

    .line 137
    :cond_2
    :try_start_1
    iget v0, p0, Lcsm;->anC:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 141
    iget-object v0, p0, Lcsm;->bhO:Lggg;

    new-instance v1, Leho;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Leho;-><init>(ILcom/google/android/shared/speech/HotwordResult;)V

    invoke-interface {v0, v1}, Lggg;->a(Lehu;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 148
    :catch_1
    move-exception v0

    .line 150
    iget-object v1, p0, Lcsm;->bhO:Lggg;

    new-instance v2, Leic;

    const-string v3, "Error at processing input stream"

    invoke-direct {v2, v3, v0}, Leic;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lggg;->b(Leiq;)V

    goto :goto_0

    .line 152
    :catch_2
    move-exception v0

    .line 153
    const-string v1, "HotwordRecognitionRnr"

    const-string v2, "Error creating or using GoogleHotwordRecognizer"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 154
    iget-object v1, p0, Lcsm;->bhO:Lggg;

    new-instance v2, Leic;

    const-string v3, "Error creating GoogleHotwordRecognizer"

    invoke-direct {v2, v3, v0}, Leic;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lggg;->b(Leiq;)V

    goto :goto_0

    .line 156
    :catch_3
    move-exception v0

    .line 157
    const-string v1, "HotwordRecognitionRnr"

    const-string v2, "Error while using GoogleHotwordRecognizer"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 158
    iget-object v1, p0, Lcsm;->bhO:Lggg;

    new-instance v2, Leic;

    const-string v3, "Error while using GoogleHotwordRecognizer"

    invoke-direct {v2, v3, v0}, Leic;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Lggg;->b(Leiq;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/speech/micro/GoogleHotwordRecognizer;Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;[B)Lcom/google/android/shared/speech/HotwordResult;
    .locals 10

    .prologue
    .line 280
    invoke-virtual {p2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;->getHotwordResults()[Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$HotwordResult;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v2, v0, v1

    .line 282
    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$HotwordResult;->getHotwordScore()F

    move-result v0

    .line 283
    iget-object v1, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    invoke-virtual {v1}, Lcom/google/speech/micro/GoogleHotwordData;->isSpeakerVerificationEnabled()Z

    move-result v4

    .line 284
    invoke-virtual {p2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult;->getSpeakerResult()Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$SpeakerResult;

    move-result-object v5

    .line 286
    if-eqz v4, :cond_1

    invoke-virtual {v5}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$SpeakerResult;->getSpeakerDetected()I

    move-result v1

    move v3, v1

    .line 287
    :goto_0
    const/high16 v1, -0x40800000    # -1.0f

    .line 288
    if-eqz v4, :cond_0

    invoke-virtual {v5}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$SpeakerResult;->getSpeakerScores()[F

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_0

    .line 290
    invoke-virtual {v5}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$SpeakerResult;->getSpeakerScores()[F

    move-result-object v1

    const/4 v4, 0x0

    aget v1, v1, v4

    .line 300
    :cond_0
    iget v4, p0, Lcsm;->anC:I

    packed-switch v4, :pswitch_data_0

    .line 383
    iget v1, p0, Lcsm;->anL:I

    invoke-static {v0, p3, v1}, Lcom/google/android/shared/speech/HotwordResult;->a(F[BI)Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    :goto_1
    return-object v0

    .line 286
    :cond_1
    const/4 v1, -0x1

    move v3, v1

    goto :goto_0

    .line 303
    :pswitch_0
    iget-object v1, p0, Lcsm;->bhW:[B

    if-nez v1, :cond_2

    .line 306
    invoke-virtual {p1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->newSpeakerFromProcessedAudio()[B

    move-result-object v1

    iput-object v1, p0, Lcsm;->bhW:[B

    .line 314
    :goto_2
    iget-object v1, p0, Lcsm;->bhV:Lcrz;

    iget-object v2, p0, Lcsm;->bhW:[B

    iget-object v3, p0, Lcsm;->aGx:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcrz;->a([BLjava/lang/String;)V

    .line 315
    iget v1, p0, Lcsm;->anL:I

    invoke-static {v0, p3, v1}, Lcom/google/android/shared/speech/HotwordResult;->b(F[BI)Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    goto :goto_1

    .line 310
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->adaptSpeakerModel(I)Lcom/google/speech/micro/GoogleHotwordRecognizer$AdaptSpeakerModelResult;

    move-result-object v1

    .line 312
    invoke-virtual {v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer$AdaptSpeakerModelResult;->getSpeakerModel()[B

    move-result-object v1

    iput-object v1, p0, Lcsm;->bhW:[B

    goto :goto_2

    .line 319
    :pswitch_1
    iget v1, p0, Lcsm;->anL:I

    invoke-static {v0, p3, v1}, Lcom/google/android/shared/speech/HotwordResult;->c(F[BI)Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    goto :goto_1

    .line 323
    :pswitch_2
    const/4 v4, 0x0

    .line 324
    const/4 v5, 0x0

    .line 325
    if-eqz p3, :cond_3

    .line 326
    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$HotwordResult;->getHotwordStartTime()J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$HotwordResult;->getHotwordEndTime()J

    move-result-wide v6

    sub-long v4, v6, v4

    long-to-int v4, v4

    iget v5, p0, Lcsm;->anL:I

    mul-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x2

    div-int/lit16 v4, v4, 0x3e8

    array-length v5, p3

    sub-int v4, v5, v4

    const/4 v5, 0x0

    invoke-static {p3, v5, v4}, Lcsm;->d([BII)F

    move-result v4

    .line 328
    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$HotwordResult;->getHotwordStartTime()J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$GoogleHotwordResult$HotwordResult;->getHotwordEndTime()J

    move-result-wide v8

    sub-long v6, v8, v6

    long-to-int v2, v6

    iget v5, p0, Lcsm;->anL:I

    mul-int/2addr v2, v5

    mul-int/lit8 v2, v2, 0x2

    div-int/lit16 v2, v2, 0x3e8

    array-length v5, p3

    sub-int/2addr v5, v2

    invoke-static {p3, v5, v2}, Lcsm;->d([BII)F

    move-result v5

    .line 337
    :cond_3
    const/4 v2, 0x0

    .line 338
    invoke-virtual {p0, v5, v4}, Lcsm;->q(FF)F

    move-result v6

    .line 339
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-eqz v7, :cond_6

    .line 342
    cmpl-float v3, v1, v6

    if-ltz v3, :cond_4

    const/4 v2, 0x1

    .line 349
    :cond_4
    :goto_3
    if-eqz v2, :cond_8

    .line 350
    iget v2, p0, Lcsm;->bhQ:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    iget v2, p0, Lcsm;->bhQ:F

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_5

    .line 355
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer;->adaptSpeakerModel(I)Lcom/google/speech/micro/GoogleHotwordRecognizer$AdaptSpeakerModelResult;

    move-result-object v2

    .line 357
    invoke-virtual {v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer$AdaptSpeakerModelResult;->getSpeakerModel()[B

    move-result-object v2

    iput-object v2, p0, Lcsm;->bhW:[B

    .line 365
    iget-object v2, p0, Lcsm;->bhV:Lcrz;

    iget-object v3, p0, Lcsm;->bhW:[B

    iget-object v6, p0, Lcsm;->aGx:Ljava/lang/String;

    invoke-interface {v2, v3, v6}, Lcrz;->a([BLjava/lang/String;)V

    .line 370
    :cond_5
    iget v3, p0, Lcsm;->anL:I

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/shared/speech/HotwordResult;->a(FF[BIFF)Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    goto/16 :goto_1

    .line 346
    :cond_6
    const/4 v2, -0x1

    if-eq v3, v2, :cond_7

    const/4 v2, 0x1

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    .line 374
    :cond_8
    iget v3, p0, Lcsm;->anL:I

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/shared/speech/HotwordResult;->b(FF[BIFF)Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    goto/16 :goto_1

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected a([BI)Lcom/google/speech/micro/GoogleHotwordData;
    .locals 1

    .prologue
    .line 471
    new-instance v0, Lcom/google/speech/micro/GoogleHotwordData;

    invoke-direct {v0, p1, p2}, Lcom/google/speech/micro/GoogleHotwordData;-><init>([BI)V

    return-object v0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 534
    const-string v0, "HotwordRecognitionRnr"

    const-string v1, "Stopping hotword detection."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 536
    iget-object v0, p0, Lcsm;->mInputStream:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 538
    iget-object v0, p0, Lcsm;->aTa:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcsm;->aTa:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 540
    const/4 v0, 0x0

    iput-object v0, p0, Lcsm;->aTa:Ljava/util/concurrent/Future;

    .line 542
    :cond_0
    return-void
.end method

.method public final q(FF)F
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcsm;->bhS:[F

    array-length v0, v0

    if-nez v0, :cond_0

    .line 402
    iget-object v0, p0, Lcsm;->bhT:[F

    invoke-direct {p0, p2, v0}, Lcsm;->a(F[F)F

    move-result v0

    .line 406
    :goto_0
    return v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcsm;->bhT:[F

    array-length v0, v0

    if-nez v0, :cond_1

    .line 404
    iget-object v0, p0, Lcsm;->bhS:[F

    invoke-direct {p0, p1, v0}, Lcsm;->a(F[F)F

    move-result v0

    goto :goto_0

    .line 406
    :cond_1
    iget-object v0, p0, Lcsm;->bhT:[F

    invoke-direct {p0, p2, v0}, Lcsm;->a(F[F)F

    move-result v0

    iget-object v1, p0, Lcsm;->bhS:[F

    invoke-direct {p0, p1, v1}, Lcsm;->a(F[F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lcsm;->aTa:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Duplicate call to start."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 529
    iget-object v0, p0, Lcsm;->bhP:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcsm;->bhX:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcsm;->aTa:Ljava/util/concurrent/Future;

    .line 530
    return-void

    .line 528
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected uf()Lcom/google/speech/micro/GoogleHotwordRecognizer;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x5

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 484
    iget v0, p0, Lcsm;->anC:I

    packed-switch v0, :pswitch_data_0

    .line 523
    :goto_0
    new-instance v0, Lcom/google/speech/micro/GoogleHotwordRecognizer;

    iget-object v1, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    invoke-direct {v0, v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;-><init>(Lcom/google/speech/micro/GoogleHotwordData;)V

    :goto_1
    return-object v0

    .line 491
    :pswitch_0
    iget-object v0, p0, Lcsm;->bhW:[B

    if-nez v0, :cond_0

    .line 492
    new-instance v0, Lcom/google/speech/micro/GoogleHotwordRecognizer;

    iget-object v1, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    invoke-direct {v0, v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;-><init>(Lcom/google/speech/micro/GoogleHotwordData;)V

    goto :goto_1

    .line 494
    :cond_0
    new-instance v0, Lcom/google/speech/micro/GoogleHotwordRecognizer;

    iget-object v1, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    new-array v2, v2, [[B

    iget-object v3, p0, Lcsm;->bhW:[B

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer;-><init>(Lcom/google/speech/micro/GoogleHotwordData;[[B)V

    goto :goto_1

    .line 500
    :pswitch_1
    iget-object v0, p0, Lcsm;->bhW:[B

    if-eqz v0, :cond_2

    .line 501
    iget-object v0, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    iget-object v1, p0, Lcsm;->bhW:[B

    invoke-virtual {v0, v1}, Lcom/google/speech/micro/GoogleHotwordData;->isSpeakerModelCompatible([B)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    new-instance v0, Lcom/google/speech/micro/GoogleHotwordRecognizer;

    iget-object v1, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    new-array v2, v2, [[B

    iget-object v3, p0, Lcsm;->bhW:[B

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/google/speech/micro/GoogleHotwordRecognizer;-><init>(Lcom/google/speech/micro/GoogleHotwordData;[[B)V

    goto :goto_1

    .line 506
    :cond_1
    iput-object v5, p0, Lcsm;->bhW:[B

    .line 507
    iget-object v0, p0, Lcsm;->bhV:Lcrz;

    iget-object v1, p0, Lcsm;->aGx:Ljava/lang/String;

    invoke-interface {v0, v5, v1}, Lcrz;->a([BLjava/lang/String;)V

    .line 510
    const-string v0, "HotwordRecognitionRnr"

    const-string v1, "Non compatible model passed to hotword."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 511
    new-instance v0, Lcom/google/speech/micro/GoogleHotwordRecognizer;

    iget-object v1, p0, Lcsm;->bhU:Lcom/google/speech/micro/GoogleHotwordData;

    invoke-direct {v0, v1}, Lcom/google/speech/micro/GoogleHotwordRecognizer;-><init>(Lcom/google/speech/micro/GoogleHotwordData;)V

    goto :goto_1

    .line 514
    :cond_2
    const-string v0, "HotwordRecognitionRnr"

    const-string v1, "Speaker model should not be null in verification mode!"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 484
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
