.class public final Lddp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aUv:Lddv;

.field final buj:Lepo;

.field final buk:Leqo;

.field bul:Landroid/os/CancellationSignal;

.field final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lepo;Leqo;Lddv;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lddp;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lddp;->buj:Lepo;

    .line 50
    iput-object p3, p0, Lddp;->buk:Leqo;

    .line 51
    iput-object p4, p0, Lddp;->aUv:Lddv;

    .line 52
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lddp;->bul:Landroid/os/CancellationSignal;

    .line 53
    return-void
.end method

.method private an(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 156
    iget-object v0, p0, Lddp;->bul:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    iget-object v0, p0, Lddp;->bul:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 159
    :cond_0
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lddp;->bul:Landroid/os/CancellationSignal;

    .line 162
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 163
    iget-object v0, p0, Lddp;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 165
    if-nez v0, :cond_2

    .line 166
    :try_start_1
    const-string v1, "ContextSuggestionsFetcher"

    const-string v2, "Can\'t find content provider %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 167
    if-eqz v0, :cond_1

    .line 175
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_1
    move-object v0, v7

    :goto_0
    return-object v0

    .line 169
    :cond_2
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_2
    iget-object v6, p0, Lddp;->bul:Landroid/os/CancellationSignal;

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 174
    if-eqz v0, :cond_3

    .line 175
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 171
    :goto_1
    :try_start_3
    const-string v2, "ContextSuggestionsFetcher"

    const-string v3, "Context content provider [%s] call faild."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v0, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 172
    if-eqz v1, :cond_4

    .line 175
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    :cond_4
    move-object v0, v7

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_2
    if-eqz v1, :cond_5

    .line 175
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    :cond_5
    throw v0

    .line 174
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_2

    .line 170
    :catch_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_1
.end method

.method private static k(Landroid/database/Cursor;)Lgny;
    .locals 6

    .prologue
    const/4 v4, 0x5

    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 181
    if-nez p0, :cond_0

    .line 182
    const-string v1, "ContextSuggestionsFetcher"

    const-string v2, "Null cursor"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    :goto_0
    return-object v0

    .line 185
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    const-string v1, "ContextSuggestionsFetcher"

    const-string v2, "Empty cursor"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 189
    :cond_1
    const-string v1, "suggestions"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 190
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 191
    const-string v1, "ContextSuggestionsFetcher"

    const-string v2, "No \'%s\' column in cursor."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "suggestions"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 194
    :cond_2
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 195
    if-nez v1, :cond_3

    .line 196
    const-string v1, "ContextSuggestionsFetcher"

    const-string v2, "Null SsbSuggestions proto bytes."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 200
    :cond_3
    :try_start_0
    invoke-static {v1}, Lgny;->V([B)Lgny;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v1

    .line 202
    const-string v2, "ContextSuggestionsFetcher"

    const-string v3, "Can\'t parse SsbSuggestions proto."

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method final am(Ljava/lang/String;Ljava/lang/String;)Lgny;
    .locals 2

    .prologue
    .line 146
    const/4 v1, 0x0

    .line 148
    :try_start_0
    invoke-direct {p0, p1, p2}, Lddp;->an(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 149
    invoke-static {v1}, Lddp;->k(Landroid/database/Cursor;)Lgny;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 151
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
.end method
