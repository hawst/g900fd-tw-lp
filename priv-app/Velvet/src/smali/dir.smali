.class public Ldir;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final byW:Ldjp;

.field private final mFlags:Lchk;

.field private final mShouldQueryStrategy:Ldfz;


# direct methods
.method public constructor <init>(Lchk;Ldfz;Ldjp;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p3, p0, Ldir;->byW:Ldjp;

    .line 30
    iput-object p2, p0, Ldir;->mShouldQueryStrategy:Ldfz;

    .line 31
    iput-object p1, p0, Ldir;->mFlags:Lchk;

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/shared/search/Query;Ljava/util/List;)Ljava/util/List;
    .locals 6

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 40
    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 44
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 45
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 47
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldiq;

    .line 48
    invoke-interface {v0}, Ldiq;->acj()I

    move-result v4

    if-lt v2, v4, :cond_3

    .line 50
    iget-object v4, p0, Ldir;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v4}, Ldfz;->abB()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Ldir;->mFlags:Lchk;

    invoke-interface {v0}, Ldiq;->ach()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lchk;->go(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 56
    :cond_4
    iget-object v4, p0, Ldir;->byW:Ldjp;

    invoke-virtual {v4, v0, p1}, Ldjp;->a(Ldiq;Lcom/google/android/shared/search/Query;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 59
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 65
    goto :goto_0
.end method

.method public a(Ldef;)V
    .locals 3

    .prologue
    .line 69
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ldef;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Ldir;->byW:Ldjp;

    invoke-interface {p1}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldjp;->ar(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_0
    return-void
.end method
