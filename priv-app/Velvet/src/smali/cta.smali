.class public final Lcta;
.super Lcuo;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 20
    const-string v0, "applications"

    const-string v1, "uri"

    invoke-direct {p0, v0, v1}, Lcuo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final a(Lben;)Lbem;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 25
    const-string v0, "display_name"

    new-instance v1, Lbcg;

    const-string v2, "name"

    invoke-direct {v1, v2}, Lbcg;-><init>(Ljava/lang/String;)V

    iput-boolean v4, v1, Lbcg;->awC:Z

    invoke-virtual {v1}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    move-result-object v0

    const-string v1, "icon_uri"

    new-instance v2, Lbcg;

    const-string v3, "icon_uri"

    invoke-direct {v2, v3}, Lbcg;-><init>(Ljava/lang/String;)V

    iput-boolean v4, v2, Lbcg;->awA:Z

    invoke-virtual {v2}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    move-result-object v0

    const-string v1, "package_name"

    new-instance v2, Lbcg;

    const-string v3, "package_name"

    invoke-direct {v2, v3}, Lbcg;-><init>(Ljava/lang/String;)V

    iput-boolean v4, v2, Lbcg;->awA:Z

    invoke-virtual {v2}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    move-result-object v0

    const-string v1, "class_name"

    new-instance v2, Lbcg;

    const-string v3, "class_name"

    invoke-direct {v2, v3}, Lbcg;-><init>(Ljava/lang/String;)V

    iput-boolean v4, v2, Lbcg;->awA:Z

    invoke-virtual {v2}, Lbcg;->xe()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lben;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lben;

    move-result-object v0

    const-string v1, "score"

    invoke-virtual {v0, v1}, Lben;->eY(Ljava/lang/String;)Lben;

    move-result-object v0

    const-string v1, "created_timestamp_ms"

    invoke-virtual {v0, v1}, Lben;->eZ(Ljava/lang/String;)Lben;

    move-result-object v0

    const-string v1, "text1"

    const v2, 0x7f0a065d

    invoke-virtual {v0, v1, v2}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v1, "intent_action"

    const v2, 0x7f0a065e

    invoke-virtual {v0, v1, v2}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v1, "intent_data"

    const v2, 0x7f0a065f

    invoke-virtual {v0, v1, v2}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const-string v1, "icon"

    const v2, 0x7f0a0660

    invoke-virtual {v0, v1, v2}, Lben;->h(Ljava/lang/String;I)Lben;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lben;->axZ:Z

    const-string v1, "3"

    iput-object v1, v0, Lben;->axW:Ljava/lang/String;

    iput-boolean v4, v0, Lben;->aya:Z

    invoke-virtual {v0}, Lben;->xN()Lbem;

    move-result-object v0

    return-object v0
.end method
