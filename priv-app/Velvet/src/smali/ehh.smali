.class public final Lehh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public amT:J

.field public bKQ:Ljava/lang/CharSequence;

.field public bKR:I

.field public bKS:I

.field public bZA:J

.field public bZB:J

.field public bZn:Ljava/io/Serializable;

.field public bZo:Lijj;

.field public bZp:Ljava/lang/String;

.field public bZq:Lehm;

.field public bZr:Ljava/lang/String;

.field public bZs:I

.field public bZu:J

.field public bZv:Ljava/lang/String;

.field public bZw:Ljava/lang/String;

.field public bZx:Landroid/location/Location;

.field public bZy:Ljava/lang/String;

.field public bZz:Landroid/net/Uri;

.field public bbz:Z

.field public bft:Ljava/util/Map;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public bsH:I

.field public bwM:Ljava/lang/String;

.field public cF:Landroid/os/Bundle;

.field public cZ:I

.field public mQuery:Lcom/google/android/shared/search/Query;

.field public mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private arg()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    const/4 v1, 0x0

    .line 1795
    const v0, 0x280bb000

    invoke-virtual {p0, v0, v3}, Lehh;->aJ(II)Lehh;

    .line 1803
    iput-object v1, p0, Lehh;->bZp:Ljava/lang/String;

    .line 1804
    iput-object v1, p0, Lehh;->bZq:Lehm;

    .line 1805
    iput-object v1, p0, Lehh;->bZr:Ljava/lang/String;

    .line 1806
    iput v2, p0, Lehh;->bKR:I

    .line 1807
    iput v2, p0, Lehh;->bKS:I

    .line 1808
    iput v3, p0, Lehh;->bZs:I

    .line 1809
    iput-object v1, p0, Lehh;->bZx:Landroid/location/Location;

    .line 1810
    iput-object v1, p0, Lehh;->bZy:Ljava/lang/String;

    .line 1811
    iput-object v1, p0, Lehh;->bft:Ljava/util/Map;

    .line 1812
    iput-object v1, p0, Lehh;->bZo:Lijj;

    .line 1813
    iput-object v1, p0, Lehh;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    .line 1814
    return-void
.end method


# virtual methods
.method public final D(Ljava/lang/CharSequence;)Lehh;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1921
    iget-object v0, p0, Lehh;->bKQ:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Leqt;->c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1922
    :goto_0
    iget-boolean v3, p0, Lehh;->bbz:Z

    or-int/2addr v3, v0

    iput-boolean v3, p0, Lehh;->bbz:Z

    .line 1923
    if-eqz v0, :cond_1

    .line 1924
    iget v0, p0, Lehh;->cZ:I

    and-int/lit8 v0, v0, 0xf

    .line 1925
    if-eq v0, v1, :cond_0

    .line 1926
    const/16 v0, 0x800

    invoke-virtual {p0, v0, v2}, Lehh;->aJ(II)Lehh;

    .line 1928
    :cond_0
    invoke-direct {p0}, Lehh;->arg()V

    .line 1930
    :cond_1
    invoke-static {p1}, Leqt;->G(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lehh;->bKQ:Ljava/lang/CharSequence;

    .line 1931
    return-object p0

    :cond_2
    move v0, v2

    .line 1921
    goto :goto_0
.end method

.method public final G(Landroid/os/Bundle;)Lehh;
    .locals 2

    .prologue
    .line 1849
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->cF:Landroid/os/Bundle;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1850
    iput-object p1, p0, Lehh;->cF:Landroid/os/Bundle;

    .line 1851
    return-object p0

    .line 1849
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final H(Landroid/net/Uri;)Lehh;
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2027
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->bZz:Landroid/net/Uri;

    invoke-static {p1, v0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 2028
    iput-object p1, p0, Lehh;->bZz:Landroid/net/Uri;

    .line 2029
    return-object p0

    .line 2027
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aF(J)Lehh;
    .locals 3

    .prologue
    .line 1887
    iget-wide v0, p0, Lehh;->bZu:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 1888
    iput-wide p1, p0, Lehh;->bZu:J

    .line 1889
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1891
    :cond_0
    return-object p0
.end method

.method public final aG(J)Lehh;
    .locals 1

    .prologue
    .line 1901
    iput-wide p1, p0, Lehh;->amT:J

    .line 1902
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1903
    return-object p0
.end method

.method public final aH(J)Lehh;
    .locals 1

    .prologue
    .line 1907
    iput-wide p1, p0, Lehh;->bZB:J

    .line 1908
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1909
    return-object p0
.end method

.method public aI(J)Lehh;
    .locals 5

    .prologue
    .line 2053
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-wide v2, p0, Lehh;->bZA:J

    cmp-long v0, v2, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 2054
    iput-wide p1, p0, Lehh;->bZA:J

    .line 2055
    return-object p0

    .line 2053
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aJ(II)Lehh;
    .locals 3

    .prologue
    .line 1781
    iget v0, p0, Lehh;->cZ:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    or-int v1, v0, p2

    .line 1789
    iget-boolean v2, p0, Lehh;->bbz:Z

    iget v0, p0, Lehh;->cZ:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v2

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1790
    iput v1, p0, Lehh;->cZ:I

    .line 1791
    return-object p0

    .line 1789
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aK(II)Lehh;
    .locals 1

    .prologue
    .line 1977
    iget v0, p0, Lehh;->bKR:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lehh;->bKS:I

    if-eq p2, v0, :cond_1

    .line 1978
    :cond_0
    iput p1, p0, Lehh;->bKR:I

    .line 1979
    iput p2, p0, Lehh;->bKS:I

    .line 1980
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1982
    :cond_1
    return-object p0
.end method

.method public final arh()Lehh;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1867
    iget v0, p0, Lehh;->cZ:I

    and-int/lit8 v0, v0, 0xf

    .line 1868
    if-eqz v0, :cond_0

    .line 1870
    const-string v0, ""

    iput-object v0, p0, Lehh;->bKQ:Ljava/lang/CharSequence;

    .line 1871
    iput v1, p0, Lehh;->bKR:I

    .line 1872
    iput v1, p0, Lehh;->bKS:I

    .line 1874
    :cond_0
    return-object p0
.end method

.method public final ari()Lehh;
    .locals 2

    .prologue
    .line 1878
    sget-object v0, Lcom/google/android/shared/search/Query;->bZm:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->bZu:J

    .line 1879
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lehh;->bZB:J

    .line 1880
    invoke-virtual {p0}, Lehh;->ark()Lehh;

    .line 1881
    invoke-virtual {p0}, Lehh;->arl()Lehh;

    .line 1882
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1883
    return-object p0
.end method

.method public final arj()Lehh;
    .locals 2

    .prologue
    .line 1895
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->amT:J

    .line 1896
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1897
    return-object p0
.end method

.method public final ark()Lehh;
    .locals 2

    .prologue
    .line 1961
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->bZq:Lehm;

    .line 1962
    const/high16 v0, 0x8000000

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lehh;->aJ(II)Lehh;

    .line 1963
    return-object p0
.end method

.method public final arl()Lehh;
    .locals 2

    .prologue
    .line 1967
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->bZr:Ljava/lang/String;

    .line 1968
    const/high16 v0, 0x20000000

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lehh;->aJ(II)Lehh;

    .line 1969
    return-object p0
.end method

.method public final arm()Lcom/google/android/shared/search/Query;
    .locals 32

    .prologue
    .line 2065
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lehh;->bbz:Z

    if-eqz v2, :cond_2

    .line 2066
    move-object/from16 v0, p0

    iget-wide v2, v0, Lehh;->amT:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lehh;->arj()Lehh;

    .line 2067
    :cond_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lehh;->bZB:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-wide v2, v0, Lehh;->amT:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lehh;->bZB:J

    .line 2068
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lehh;->bKQ:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget v3, v0, Lehh;->bKR:I

    invoke-static {v2, v3}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;I)Z

    move-result v2

    const-string v3, "Query has length %s but selection start is %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lehh;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lehh;->bKR:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lehh;->bKQ:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget v3, v0, Lehh;->bKS:I

    invoke-static {v2, v3}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;I)Z

    move-result v2

    const-string v3, "Query has length %s but selection end is %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lehh;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lehh;->bKS:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2069
    new-instance v2, Lcom/google/android/shared/search/Query;

    move-object/from16 v0, p0

    iget v3, v0, Lehh;->cZ:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lehh;->bKQ:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v5, v0, Lehh;->bZo:Lijj;

    move-object/from16 v0, p0

    iget-object v6, v0, Lehh;->bZp:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lehh;->bZq:Lehm;

    move-object/from16 v0, p0

    iget-object v8, v0, Lehh;->bZr:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lehh;->bKR:I

    move-object/from16 v0, p0

    iget v10, v0, Lehh;->bKS:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lehh;->bZw:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v12, v0, Lehh;->bZs:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lehh;->bft:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lehh;->bZu:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->bZv:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->bZx:Landroid/location/Location;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->bZy:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->bZn:Ljava/io/Serializable;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->cF:Landroid/os/Bundle;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->bwM:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lehh;->bZz:Landroid/net/Uri;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lehh;->bZA:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lehh;->amT:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lehh;->bsH:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lehh;->bZB:J

    move-wide/from16 v29, v0

    const/16 v31, 0x0

    invoke-direct/range {v2 .. v31}, Lcom/google/android/shared/search/Query;-><init>(ILjava/lang/CharSequence;Lijj;Ljava/lang/String;Lehm;Ljava/lang/String;IILjava/lang/String;ILjava/util/Map;JLjava/lang/String;Lcom/google/android/shared/search/SearchBoxStats;Landroid/location/Location;Ljava/lang/String;Ljava/io/Serializable;Landroid/os/Bundle;Ljava/lang/String;Landroid/net/Uri;JJIJB)V

    .line 2079
    :goto_0
    return-object v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lehh;->mQuery:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method public final b(Lehm;)Lehh;
    .locals 2

    .prologue
    .line 1955
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->bZq:Lehm;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1956
    iput-object p1, p0, Lehh;->bZq:Lehm;

    .line 1957
    return-object p0

    .line 1955
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;
    .locals 2

    .prologue
    .line 2039
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 2040
    iput-object p1, p0, Lehh;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    .line 2041
    return-object p0

    .line 2039
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hB(I)Lehh;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x400

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1817
    iget v0, p0, Lehh;->cZ:I

    and-int/lit8 v0, v0, 0xf

    .line 1818
    if-eq p1, v0, :cond_3

    .line 1819
    const/16 v0, 0x220f

    invoke-virtual {p0, v0, p1}, Lehh;->aJ(II)Lehh;

    .line 1821
    if-ne p1, v2, :cond_4

    .line 1822
    invoke-virtual {p0, v1, v3}, Lehh;->aJ(II)Lehh;

    .line 1830
    :cond_0
    :goto_0
    if-eq p1, v2, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 1832
    :cond_1
    const-string v0, "web"

    iput-object v0, p0, Lehh;->bZw:Ljava/lang/String;

    .line 1833
    iput-object v4, p0, Lehh;->bft:Ljava/util/Map;

    .line 1835
    :cond_2
    iput-object v4, p0, Lehh;->bZn:Ljava/io/Serializable;

    .line 1837
    :cond_3
    return-object p0

    .line 1823
    :cond_4
    if-nez p1, :cond_0

    .line 1824
    invoke-virtual {p0, v3, v1}, Lehh;->aJ(II)Lehh;

    .line 1827
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p0, v1, v0}, Lehh;->aJ(II)Lehh;

    .line 1828
    const/high16 v0, 0x400000

    invoke-virtual {p0, v1, v0}, Lehh;->aJ(II)Lehh;

    goto :goto_0
.end method

.method public final hC(I)Lehh;
    .locals 1

    .prologue
    .line 1855
    const/16 v0, 0xf0

    invoke-virtual {p0, v0, p1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    return-object v0
.end method

.method public final hD(I)Lehh;
    .locals 1

    .prologue
    .line 1986
    iget v0, p0, Lehh;->bZs:I

    if-eq p1, v0, :cond_0

    .line 1987
    invoke-direct {p0}, Lehh;->arg()V

    .line 1988
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lehh;->hB(I)Lehh;

    .line 1989
    iput p1, p0, Lehh;->bZs:I

    .line 1990
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1992
    :cond_0
    return-object p0
.end method

.method public final hE(I)Lehh;
    .locals 2

    .prologue
    .line 2059
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget v0, p0, Lehh;->bsH:I

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 2060
    iput p1, p0, Lehh;->bsH:I

    .line 2061
    return-object p0

    .line 2059
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kY(Ljava/lang/String;)Lehh;
    .locals 1

    .prologue
    .line 1913
    iget-object v0, p0, Lehh;->bZv:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1914
    iput-object p1, p0, Lehh;->bZv:Ljava/lang/String;

    .line 1915
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1917
    :cond_0
    return-object p0
.end method

.method public final kZ(Ljava/lang/String;)Lehh;
    .locals 2

    .prologue
    .line 1949
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->bZr:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1950
    iput-object p1, p0, Lehh;->bZr:Ljava/lang/String;

    .line 1951
    return-object p0

    .line 1949
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final la(Ljava/lang/String;)Lehh;
    .locals 2

    .prologue
    .line 2021
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->bZy:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 2022
    iput-object p1, p0, Lehh;->bZy:Ljava/lang/String;

    .line 2023
    return-object p0

    .line 2021
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m(Ljava/util/Map;)Lehh;
    .locals 2
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1996
    iget-boolean v1, p0, Lehh;->bbz:Z

    iget-object v0, p0, Lehh;->bft:Ljava/util/Map;

    invoke-interface {p1, v0}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 1997
    iput-object p1, p0, Lehh;->bft:Ljava/util/Map;

    .line 1998
    return-object p0

    .line 1996
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p(Ljava/lang/String;Z)Lehh;
    .locals 1

    .prologue
    .line 2003
    iget-object v0, p0, Lehh;->bZw:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2004
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->bbz:Z

    .line 2005
    invoke-direct {p0}, Lehh;->arg()V

    .line 2006
    if-eqz p2, :cond_0

    .line 2007
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lehh;->hB(I)Lehh;

    .line 2010
    :cond_0
    iput-object p1, p0, Lehh;->bZw:Ljava/lang/String;

    .line 2011
    return-object p0
.end method
