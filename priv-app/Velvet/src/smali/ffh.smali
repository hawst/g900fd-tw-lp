.class public final Lffh;
.super Lfeu;
.source "PG"


# instance fields
.field private final cqe:Ljbz;


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 23
    iget-object v0, p1, Lizj;->dSJ:Ljbz;

    iput-object v0, p0, Lffh;->cqe:Ljbz;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lffh;->cqe:Ljbz;

    iget-object v0, v0, Ljbz;->dZN:Ljbw;

    invoke-virtual {v0}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 64
    iget-object v0, p0, Lffh;->cqe:Ljbz;

    iget-object v0, v0, Ljbz;->dZO:Ljbp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lffh;->cqe:Ljbz;

    iget-object v0, v0, Ljbz;->dZO:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lffz;

    const v1, 0x7f0a049d

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    iget-object v3, v3, Ljbz;->dZN:Ljbw;

    invoke-virtual {v3}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    invoke-virtual {v3}, Ljbz;->bfR()J

    move-result-wide v4

    mul-long/2addr v4, v10

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    iget-object v3, v3, Ljbz;->dZO:Ljbp;

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    invoke-virtual {v3}, Ljbz;->baQ()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lffh;->cqe:Ljbz;

    iget-object v1, v1, Ljbz;->dZO:Ljbp;

    invoke-virtual {v0, v1, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lffz;

    const v1, 0x7f0a049e

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    iget-object v3, v3, Ljbz;->dZN:Ljbw;

    invoke-virtual {v3}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    invoke-virtual {v3}, Ljbz;->bfR()J

    move-result-wide v4

    mul-long/2addr v4, v10

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    invoke-virtual {v3}, Ljbz;->baQ()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lffz;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 35
    const v0, 0x7f0a0349

    new-array v1, v8, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lffh;->cqe:Ljbz;

    invoke-virtual {v3}, Ljbz;->bfR()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {p1, v4, v5, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "event"

    return-object v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f0200f4

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lfgb;->cqC:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lffh;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
