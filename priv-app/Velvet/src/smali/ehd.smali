.class public final Lehd;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/view/View;Lehe;Z)Ljava/util/List;
    .locals 3

    .prologue
    .line 48
    if-eqz p2, :cond_0

    .line 50
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p0

    .line 54
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lehf;->aoX()Ljyc;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lehe;->a(Landroid/content/Context;Ljyc;)Ljyc;

    move-result-object v1

    .line 57
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    invoke-static {p0, v1, v0, p1}, Lehd;->a(Landroid/view/View;Ljyc;Ljava/util/List;Lehe;)V

    .line 61
    return-object v0
.end method

.method private static a(Landroid/view/View;Ljyc;Ljava/util/List;Lehe;)V
    .locals 3

    .prologue
    .line 74
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    :cond_0
    return-void

    .line 79
    :cond_1
    invoke-static {p0}, Lehd;->aG(Landroid/view/View;)I

    move-result v0

    .line 80
    if-lez v0, :cond_2

    .line 81
    invoke-static {}, Lehf;->aoX()Ljyc;

    move-result-object v0

    invoke-interface {p3, p0, v0}, Lehe;->a(Landroid/view/View;Ljyc;)Ljyc;

    move-result-object v0

    .line 83
    iget-object v1, p1, Ljyc;->dKU:[I

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Leqh;->b([II)[I

    move-result-object v1

    iput-object v1, p1, Ljyc;->dKU:[I

    .line 89
    const v1, 0x7f11000f

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 91
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p1, v0

    .line 94
    :cond_2
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 95
    check-cast p0, Landroid/view/ViewGroup;

    .line 96
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 97
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 98
    invoke-static {v1, p1, p2, p3}, Lehd;->a(Landroid/view/View;Ljyc;Ljava/util/List;Lehe;)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static varargs a([Ljyc;)V
    .locals 3

    .prologue
    .line 157
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p0, v0

    .line 158
    invoke-static {v2}, Lehf;->a(Ljyc;)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method

.method public static aG(Landroid/view/View;)I
    .locals 4
    .param p0    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    .line 110
    if-nez p0, :cond_0

    move v0, v2

    .line 116
    :goto_0
    return v0

    .line 113
    :cond_0
    const v0, 0x7f11000e

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_1

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_1
    const-string v3, "ve_tag can only be an Integer."

    invoke-static {v1, v3}, Lifv;->c(ZLjava/lang/Object;)V

    .line 116
    if-eqz v0, :cond_3

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 114
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 116
    goto :goto_0
.end method

.method public static r(Landroid/view/View;I)V
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 127
    if-nez p0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p0, v0}, Lehd;->s(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public static s(Landroid/view/View;I)V
    .locals 2
    .param p0    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 141
    if-nez p0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    if-lez p1, :cond_0

    .line 149
    const v0, 0x7f11000e

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method
