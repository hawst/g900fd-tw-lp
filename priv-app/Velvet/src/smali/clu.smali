.class public final Lclu;
.super Ldlg;
.source "PG"


# instance fields
.field private final bbk:Legl;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final bbl:I

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mExtrasConsumer:Lcoi;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field mGsaConfig:Lchk;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPreloadTaskHandler:Lcpt;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final mSdchManager:Lczz;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mStaticContentCache:Lcpq;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lchk;Lcoi;Lczz;Legl;ILcpt;Lcpq;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcoi;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lczz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Lcpt;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 72
    invoke-direct {p0}, Ldlg;-><init>()V

    .line 73
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lclu;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 74
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lclu;->mGsaConfig:Lchk;

    .line 75
    iput-object p3, p0, Lclu;->mExtrasConsumer:Lcoi;

    .line 76
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczz;

    iput-object v0, p0, Lclu;->mSdchManager:Lczz;

    .line 77
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legl;

    iput-object v0, p0, Lclu;->bbk:Legl;

    .line 78
    iput p6, p0, Lclu;->bbl:I

    .line 79
    iput-object p7, p0, Lclu;->mPreloadTaskHandler:Lcpt;

    .line 80
    iput-object p8, p0, Lclu;->mStaticContentCache:Lcpq;

    .line 81
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/ExecutorService;Lchk;Lczz;Legl;)V
    .locals 9
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lczz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 63
    new-instance v3, Lclv;

    invoke-direct {v3, v0}, Lclv;-><init>(B)V

    const/4 v6, 0x1

    new-instance v7, Lclw;

    invoke-direct {v7, v0}, Lclw;-><init>(B)V

    new-instance v8, Lclx;

    invoke-direct {v8}, Lclx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Lclu;-><init>(Ljava/util/concurrent/ExecutorService;Lchk;Lcoi;Lczz;Legl;ILcpt;Lcpq;)V

    .line 66
    return-void
.end method

.method private a(Lbzh;[BI)Lcma;
    .locals 9
    .param p1    # Lbzh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 86
    new-instance v4, Lcly;

    invoke-direct {v4, p0, p1, p2, p3}, Lcly;-><init>(Lclu;Lbzh;[BI)V

    .line 88
    new-instance v0, Lcma;

    iget-object v2, p0, Lclu;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lclu;->mGsaConfig:Lchk;

    iget-object v5, p0, Lclu;->bbk:Legl;

    iget-object v6, p0, Lclu;->mStaticContentCache:Lcpq;

    iget-object v7, p0, Lclu;->mExtrasConsumer:Lcoi;

    iget-object v8, p0, Lclu;->mPreloadTaskHandler:Lcpt;

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcma;-><init>(Lbzh;Ljava/util/concurrent/ExecutorService;Lchk;Lcly;Legl;Lcpq;Lcoi;Lcpt;)V

    .line 91
    invoke-virtual {v0}, Lcma;->PV()Lclz;

    move-result-object v2

    iget-object v3, v4, Lcly;->bbp:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v1, v4, Lcly;->bbq:Lclz;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lifv;->gY(Z)V

    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lclz;

    iput-object v1, v4, Lcly;->bbq:Lclz;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v4}, Lcly;->PM()V

    .line 92
    return-object v0

    .line 91
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method public final synthetic b(Lbzh;[BI)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lclu;->a(Lbzh;[BI)Lcma;

    move-result-object v0

    return-object v0
.end method
