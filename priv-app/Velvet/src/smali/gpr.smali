.class public final Lgpr;
.super Ljava/util/concurrent/FutureTask;
.source "PG"


# instance fields
.field private final aWp:Ljava/lang/String;

.field private synthetic cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lgpr;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    .line 575
    invoke-direct {p0, p3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 576
    iput-object p2, p0, Lgpr;->aWp:Ljava/lang/String;

    .line 577
    return-void
.end method


# virtual methods
.method public final done()V
    .locals 2

    .prologue
    .line 582
    invoke-super {p0}, Ljava/util/concurrent/FutureTask;->done()V

    .line 583
    iget-object v0, p0, Lgpr;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v1, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 584
    :try_start_0
    iget-object v0, p0, Lgpr;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v0, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 585
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final setException(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 590
    invoke-super {p0, p1}, Ljava/util/concurrent/FutureTask;->setException(Ljava/lang/Throwable;)V

    .line 591
    const-string v0, "Velvet.VelvetBackgroundTasksImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Background task "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgpr;->aWp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 592
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 596
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NotifyOnDoneFutureTask["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lgpr;->aWp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
