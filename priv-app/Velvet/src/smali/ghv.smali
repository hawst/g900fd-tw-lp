.class final Lghv;
.super Lepm;
.source "PG"


# instance fields
.field private cKx:Lght;

.field private mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lght;Landroid/content/ContentResolver;)V
    .locals 2
    .param p1    # Lght;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Landroid/content/ContentResolver;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 87
    const-string v0, "GoogleAccountLookup"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 88
    iput-object p1, p0, Lghv;->cKx:Lght;

    .line 89
    iput-object p2, p0, Lghv;->mContentResolver:Landroid/content/ContentResolver;

    .line 90
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 96
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 97
    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "account_name"

    aput-object v0, v2, v7

    .line 98
    const-string v3, "account_type = ?"

    .line 99
    new-array v4, v6, [Ljava/lang/String;

    const-string v0, "com.google"

    aput-object v0, v4, v7

    .line 100
    const-string v5, "account_name COLLATE LOCALIZED ASC"

    .line 103
    :try_start_0
    iget-object v0, p0, Lghv;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 112
    new-instance v2, Lghu;

    invoke-direct {v2, v1}, Lghu;-><init>(Ljava/util/Set;)V

    invoke-static {v2, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    .line 114
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 118
    :goto_0
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    .line 122
    iget-object v1, p0, Lghv;->cKx:Lght;

    if-ne v0, v6, :cond_0

    move v0, v6

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lght;->b(Ljava/lang/Boolean;)V

    .line 123
    :goto_2
    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    const-string v1, "GoogleAccountLookup"

    const-string v2, "Failed to query content provider"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lghv;->cKx:Lght;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lght;->b(Ljava/lang/Boolean;)V

    goto :goto_2

    .line 115
    :catch_1
    move-exception v0

    .line 116
    const-string v2, "GoogleAccountLookup"

    const-string v3, "Failed to close cursor"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    move v0, v7

    .line 122
    goto :goto_1
.end method
