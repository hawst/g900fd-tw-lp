.class public final Lfuh;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 9

    .prologue
    const v8, 0x7f11031f

    const/4 v7, 0x0

    .line 32
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 33
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 34
    iget-object v2, v0, Lanh;->agW:Laof;

    .line 36
    const v0, 0x7f1101a9

    invoke-virtual {v2}, Laof;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfuh;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 37
    const v0, 0x7f11031d

    invoke-virtual {v2}, Laof;->qB()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfuh;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 38
    const v0, 0x7f1101aa

    invoke-virtual {v2}, Laof;->qF()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfuh;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 41
    invoke-virtual {v2}, Laof;->qE()Ljava/lang/String;

    move-result-object v0

    .line 42
    iget-object v3, p0, Lfuh;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Laof;->qn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v2}, Laof;->qC()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Lgab;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 44
    const-string v4, "  "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/CharSequence;

    aput-object v3, v5, v7

    const/4 v6, 0x1

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 46
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 51
    :cond_0
    invoke-virtual {v2}, Laof;->ok()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    const v0, 0x7f1100ad

    invoke-virtual {v2}, Laof;->oj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lfuh;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 54
    :cond_1
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f04011e

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
