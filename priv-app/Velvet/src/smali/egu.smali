.class public Legu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bLG:I

.field private final bLZ:Ljava/lang/String;

.field private final bOm:I

.field private final bYX:Z

.field private final bYY:Z

.field private final bYZ:I

.field private final bZa:Z

.field private final bZb:I

.field private final bZc:Ljse;

.field private bZd:Z

.field private bZe:Z

.field private final baD:Ljava/lang/String;

.field private final mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;IIIZILjse;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Legu;->baD:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Legu;->mRequestId:Ljava/lang/String;

    .line 38
    iput-boolean p4, p0, Legu;->bYX:Z

    .line 39
    iput-object p5, p0, Legu;->bLZ:Ljava/lang/String;

    .line 40
    iput p6, p0, Legu;->bOm:I

    .line 41
    iput p7, p0, Legu;->bYZ:I

    .line 42
    iput-boolean p2, p0, Legu;->bYY:Z

    .line 43
    iput p8, p0, Legu;->bLG:I

    .line 44
    iput-boolean p9, p0, Legu;->bZa:Z

    .line 45
    iput p10, p0, Legu;->bZb:I

    .line 46
    iput-object p11, p0, Legu;->bZc:Ljse;

    .line 47
    return-void
.end method


# virtual methods
.method public final afW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Legu;->bLZ:Ljava/lang/String;

    return-object v0
.end method

.method public final alk()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Legu;->bOm:I

    return v0
.end method

.method public final aoM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Legu;->baD:Ljava/lang/String;

    return-object v0
.end method

.method public final aoN()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Legu;->bYZ:I

    return v0
.end method

.method public final aoO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Legu;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method public final aoP()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Legu;->bYX:Z

    return v0
.end method

.method public final aoQ()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Legu;->bYY:Z

    return v0
.end method

.method public final aoR()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Legu;->bZa:Z

    return v0
.end method

.method public final aoS()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Legu;->bZb:I

    return v0
.end method

.method public final aoT()Ljse;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Legu;->bZc:Ljse;

    return-object v0
.end method

.method public final aoU()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Legu;->bZd:Z

    return v0
.end method

.method public final aoV()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Legu;->bZe:Z

    return v0
.end method

.method public final oY()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Legu;->bLG:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActionCardVeImpressionData{mRequestId=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Legu;->mRequestId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsNetworkAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Legu;->bYX:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsFollowOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Legu;->bYY:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mQueryString=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Legu;->bLZ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPromptedField="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Legu;->bOm:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCountdownMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Legu;->bYZ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEventId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Legu;->baD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mActionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Legu;->bLG:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsModularAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Legu;->bZa:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mActionState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Legu;->bZb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mActionArguments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Legu;->bZc:Ljse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHasClientEntity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Legu;->bZd:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHasServerEntity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Legu;->bZe:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(ZZ)V
    .locals 0

    .prologue
    .line 135
    iput-boolean p1, p0, Legu;->bZd:Z

    .line 136
    iput-boolean p2, p0, Legu;->bZe:Z

    .line 137
    return-void
.end method
