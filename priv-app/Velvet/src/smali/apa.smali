.class public final Lapa;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private akB:Ljava/lang/String;

.field public akz:[Laoz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7567
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7568
    const/4 v0, 0x0

    iput v0, p0, Lapa;->aez:I

    invoke-static {}, Laoz;->rL()[Laoz;

    move-result-object v0

    iput-object v0, p0, Lapa;->akz:[Laoz;

    const-string v0, ""

    iput-object v0, p0, Lapa;->akB:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lapa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapa;->eCz:I

    .line 7569
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7523
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lapa;->akz:[Laoz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Laoz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lapa;->akz:[Laoz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Laoz;

    invoke-direct {v3}, Laoz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lapa;->akz:[Laoz;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Laoz;

    invoke-direct {v3}, Laoz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lapa;->akz:[Laoz;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapa;->akB:Ljava/lang/String;

    iget v0, p0, Lapa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapa;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 7583
    iget-object v0, p0, Lapa;->akz:[Laoz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lapa;->akz:[Laoz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 7584
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lapa;->akz:[Laoz;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 7585
    iget-object v1, p0, Lapa;->akz:[Laoz;

    aget-object v1, v1, v0

    .line 7586
    if-eqz v1, :cond_0

    .line 7587
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 7584
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7591
    :cond_1
    iget v0, p0, Lapa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 7592
    const/4 v0, 0x2

    iget-object v1, p0, Lapa;->akB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7594
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7595
    return-void
.end method

.method public final cF(Ljava/lang/String;)Lapa;
    .locals 1

    .prologue
    .line 7551
    if-nez p1, :cond_0

    .line 7552
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7554
    :cond_0
    iput-object p1, p0, Lapa;->akB:Ljava/lang/String;

    .line 7555
    iget v0, p0, Lapa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapa;->aez:I

    .line 7556
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 7599
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 7600
    iget-object v0, p0, Lapa;->akz:[Laoz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lapa;->akz:[Laoz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 7601
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lapa;->akz:[Laoz;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 7602
    iget-object v2, p0, Lapa;->akz:[Laoz;

    aget-object v2, v2, v0

    .line 7603
    if-eqz v2, :cond_0

    .line 7604
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 7601
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7609
    :cond_1
    iget v0, p0, Lapa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 7610
    const/4 v0, 0x2

    iget-object v2, p0, Lapa;->akB:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 7613
    :cond_2
    return v1
.end method

.method public final rP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7548
    iget-object v0, p0, Lapa;->akB:Ljava/lang/String;

    return-object v0
.end method
