.class final Lfnj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leml;


# instance fields
.field cwT:Ljava/util/List;

.field final dK:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lfnh;)V
    .locals 1

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfnj;->dK:Ljava/lang/Object;

    .line 155
    const/4 v0, 0x1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfnj;->cwT:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final auB()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic auC()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remote bitmaps are never available now"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e(Lemy;)V
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Lfnj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    :try_start_0
    iget-object v0, p0, Lfnj;->cwT:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f(Lemy;)V
    .locals 2

    .prologue
    .line 177
    iget-object v1, p0, Lfnj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 178
    :try_start_0
    iget-object v0, p0, Lfnj;->cwT:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 179
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isCancelled()Z
    .locals 2

    .prologue
    .line 193
    iget-object v1, p0, Lfnj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 194
    :try_start_0
    iget-object v0, p0, Lfnj;->cwT:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
