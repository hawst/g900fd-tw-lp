.class final Lclr;
.super Lgly;
.source "PG"


# instance fields
.field private bbg:Z

.field final synthetic bbh:Lclq;


# direct methods
.method constructor <init>(Lclq;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lclr;->bbh:Lclq;

    invoke-direct {p0}, Lgly;-><init>()V

    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 186
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, v0, Lclq;->cq:Z

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v1, v0, Lclq;->mListener:Lclt;

    .line 192
    iget-object v2, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, p0, Lclr;->bbg:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lclq;->ct(Z)V

    .line 193
    iget-boolean v0, p0, Lclr;->bbg:Z

    if-nez v0, :cond_0

    .line 194
    new-instance v0, Leis;

    new-instance v2, Leio;

    invoke-direct {v2}, Leio;-><init>()V

    invoke-direct {v0, v2}, Leis;-><init>(Leiq;)V

    invoke-interface {v1, v0}, Lclt;->b(Leis;)V

    .line 197
    :cond_0
    invoke-interface {v1}, Lclt;->CZ()V

    .line 199
    :cond_1
    return-void

    .line 192
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Nr()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, v0, Lclq;->cq:Z

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->mListener:Lclt;

    invoke-interface {v0}, Lclt;->Np()V

    .line 218
    :cond_0
    return-void
.end method

.method public final a(Lieb;)V
    .locals 5

    .prologue
    .line 127
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 129
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, v0, Lclq;->cq:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lclr;->bbg:Z

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p1, Lieb;->dzA:[Lidz;

    invoke-static {v0}, Lcll;->a([Lidz;)Lidz;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_2

    .line 133
    iget-object v1, p0, Lclr;->bbh:Lclq;

    iget-object v1, v1, Lclq;->mVss:Lhhq;

    iget-object v1, v1, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    .line 134
    new-instance v2, Lcls;

    const-string v3, "Insert heard match"

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-direct {v2, p0, v3, v4, p1}, Lcls;-><init>(Lclr;Ljava/lang/String;[ILieb;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 155
    :cond_0
    if-eqz v0, :cond_1

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lclr;->bbg:Z

    .line 159
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Lclr;->bbh:Lclq;

    iget-object v1, v1, Lclq;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->mr(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->mListener:Lclt;

    invoke-interface {v0, p1}, Lclt;->a(Lieb;)V

    .line 164
    :cond_1
    :goto_0
    return-void

    .line 142
    :cond_2
    iget-object v0, p1, Lieb;->dzA:[Lidz;

    invoke-static {v0}, Lcll;->b([Lidz;)Lidz;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    iget-object v1, v0, Lidz;->dzw:Lieh;

    invoke-virtual {v1}, Lieh;->aVF()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    new-instance v0, Lein;

    invoke-direct {v0}, Lein;-><init>()V

    invoke-virtual {p0, v0}, Lclr;->c(Leiq;)V

    goto :goto_0
.end method

.method public final c(Leiq;)V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 171
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, v0, Lclq;->cq:Z

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, v0, Lclq;->bbf:Z

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    invoke-virtual {v0}, Lhik;->aPH()V

    .line 175
    :cond_0
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->mListener:Lclt;

    .line 176
    iget-object v1, p0, Lclr;->bbh:Lclq;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lclq;->ct(Z)V

    .line 177
    new-instance v1, Leis;

    invoke-direct {v1, p1}, Leis;-><init>(Leiq;)V

    invoke-interface {v0, v1}, Lclt;->a(Leis;)V

    .line 179
    :cond_1
    return-void
.end method

.method public final v([B)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 206
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-boolean v0, v0, Lclq;->cq:Z

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lclr;->bbh:Lclq;

    iget-object v0, v0, Lclq;->mListener:Lclt;

    invoke-interface {v0, p1}, Lclt;->p([B)V

    .line 209
    :cond_0
    return-void
.end method
