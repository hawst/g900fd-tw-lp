.class public final Lkdx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static synthetic $assertionsDisabled:Z

.field private static final aRd:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private dLz:Z

.field private eOY:J

.field final eTC:Z

.field private eTW:Lkeh;

.field private final eTX:Lkdm;

.field private final eTY:Lkdc;

.field private final eTZ:Lkde;

.field private final eUa:Ljava/util/Map;

.field private final eUb:Ljava/lang/String;

.field private eUc:I

.field private eUd:I

.field eUe:Lkdt;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 44
    const-class v0, Lkdx;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkdx;->$assertionsDisabled:Z

    .line 58
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string v0, "OkHttp SpdyConnection"

    invoke-static {v0}, Lkbt;->Aw(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, Lkdx;->aRd:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v2

    .line 44
    goto :goto_0
.end method

.method private constructor <init>(Lkeb;)V
    .locals 4

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkdx;->eUa:Ljava/util/Map;

    .line 81
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lkdx;->eOY:J

    .line 91
    iget-object v0, p1, Lkeb;->eTW:Lkeh;

    iput-object v0, p0, Lkdx;->eTW:Lkeh;

    .line 92
    iget-boolean v0, p1, Lkeb;->eTC:Z

    iput-boolean v0, p0, Lkdx;->eTC:Z

    .line 93
    iget-object v0, p1, Lkeb;->eTX:Lkdm;

    iput-object v0, p0, Lkdx;->eTX:Lkdm;

    .line 94
    iget-object v0, p0, Lkdx;->eTW:Lkeh;

    iget-object v1, p1, Lkeb;->in:Ljava/io/InputStream;

    iget-boolean v2, p0, Lkdx;->eTC:Z

    invoke-interface {v0, v1, v2}, Lkeh;->b(Ljava/io/InputStream;Z)Lkdc;

    move-result-object v0

    iput-object v0, p0, Lkdx;->eTY:Lkdc;

    .line 95
    iget-object v0, p0, Lkdx;->eTW:Lkeh;

    iget-object v1, p1, Lkeb;->out:Ljava/io/OutputStream;

    iget-boolean v2, p0, Lkdx;->eTC:Z

    invoke-interface {v0, v1, v2}, Lkeh;->a(Ljava/io/OutputStream;Z)Lkde;

    move-result-object v0

    iput-object v0, p0, Lkdx;->eTZ:Lkde;

    .line 96
    iget-boolean v0, p1, Lkeb;->eTC:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lkdx;->eUd:I

    .line 97
    iget-boolean v0, p1, Lkeb;->eTC:Z

    .line 99
    iget-object v0, p1, Lkeb;->eUb:Ljava/lang/String;

    iput-object v0, p0, Lkdx;->eUb:Ljava/lang/String;

    .line 101
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lkec;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lkec;-><init>(Lkdx;B)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Spdy Reader "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lkdx;->eUb:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 102
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lkeb;B)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lkdx;-><init>(Lkeb;)V

    return-void
.end method

.method static synthetic a(Lkdx;)Lkdc;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkdx;->eTY:Lkdc;

    return-object v0
.end method

.method static synthetic a(Lkdx;I)Lkee;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lkdx;->tS(I)Lkee;

    move-result-object v0

    return-object v0
.end method

.method private a(Lkdb;Lkdb;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 305
    sget-boolean v1, Lkdx;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 308
    :cond_0
    :try_start_0
    iget-object v3, p0, Lkdx;->eTZ:Lkde;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-boolean v1, p0, Lkdx;->dLz:Z

    if-eqz v1, :cond_2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v0

    .line 314
    :goto_0
    monitor-enter p0

    .line 316
    :try_start_4
    iget-object v3, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 317
    iget-object v0, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v3, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Lkee;

    invoke-interface {v0, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkee;

    .line 318
    iget-object v3, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 319
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lkdx;->jL(Z)V

    move-object v3, v0

    .line 321
    :goto_1
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 327
    if-eqz v3, :cond_4

    .line 328
    array-length v4, v3

    move-object v0, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v1, v3, v2

    .line 330
    :try_start_5
    invoke-virtual {v1, p2}, Lkee;->a(Lkdb;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 328
    :cond_1
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 308
    :cond_2
    const/4 v1, 0x1

    :try_start_6
    iput-boolean v1, p0, Lkdx;->dLz:Z

    iget v1, p0, Lkdx;->eUc:I

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v4, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v4, v1, p1}, Lkde;->b(ILkdb;)V

    monitor-exit v3

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v1

    :try_start_8
    monitor-exit v3

    throw v1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    .line 311
    :catch_0
    move-exception v1

    goto :goto_0

    .line 321
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 331
    :catch_1
    move-exception v1

    .line 332
    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_3

    :cond_3
    move-object v1, v0

    .line 337
    :cond_4
    :try_start_9
    iget-object v0, p0, Lkdx;->eTY:Lkdc;

    invoke-interface {v0}, Lkdc;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 349
    :goto_4
    :try_start_a
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v0}, Lkde;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    move-object v0, v1

    .line 354
    :cond_5
    :goto_5
    if-eqz v0, :cond_6

    throw v0

    .line 345
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 346
    goto :goto_4

    .line 350
    :catch_3
    move-exception v0

    .line 351
    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_5

    .line 355
    :cond_6
    return-void

    :cond_7
    move-object v3, v0

    goto :goto_1
.end method

.method static synthetic a(Lkdx;Lkdb;Lkdb;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lkdx;->a(Lkdb;Lkdb;)V

    return-void
.end method

.method static synthetic a(Lkdx;ZIILkds;)V
    .locals 6

    .prologue
    .line 44
    iget-object v1, p0, Lkdx;->eTZ:Lkde;

    monitor-enter v1

    if-eqz p4, :cond_1

    :try_start_0
    iget-wide v2, p4, Lkds;->eTM:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p4, Lkds;->eTM:J

    :cond_1
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v0, p1, p2}, Lkde;->i(ZI)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic a(Lkdx;Z)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkdx;->dLz:Z

    return v0
.end method

.method static synthetic b(Lkdx;I)I
    .locals 0

    .prologue
    .line 44
    iput p1, p0, Lkdx;->eUc:I

    return p1
.end method

.method static synthetic b(Lkdx;ZIILkds;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 44
    const/4 v7, 0x0

    sget-object v8, Lkdx;->aRd:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lkea;

    const-string v2, "OkHttp SPDY Writer %s ping %08x%08x"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v5, p0, Lkdx;->eUb:Ljava/lang/String;

    aput-object v5, v3, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v1

    move-object v1, p0

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v7}, Lkea;-><init>(Lkdx;Ljava/lang/String;[Ljava/lang/Object;ZIILkds;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic b(Lkdx;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lkdx;->dLz:Z

    return v0
.end method

.method static synthetic bzo()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lkdx;->aRd:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic c(Lkdx;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lkdx;->eUc:I

    return v0
.end method

.method static synthetic c(Lkdx;I)Lkds;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lkdx;->tU(I)Lkds;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lkdx;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lkdx;->eUd:I

    return v0
.end method

.method static synthetic e(Lkdx;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkdx;->eUa:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lkdx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkdx;->eUb:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lkdx;)Lkdm;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lkdx;->eTX:Lkdm;

    return-object v0
.end method

.method private declared-synchronized jL(Z)V
    .locals 2

    .prologue
    .line 125
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lkdx;->eOY:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    .line 125
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized tS(I)Lkee;
    .locals 2

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkee;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized tU(I)Lkds;
    .locals 1

    .prologue
    .line 263
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;ZZ)Lkee;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 151
    if-nez p2, :cond_0

    const/4 v3, 0x1

    .line 152
    :cond_0
    iget-object v10, p0, Lkdx;->eTZ:Lkde;

    monitor-enter v10

    .line 160
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 161
    :try_start_1
    iget-boolean v0, p0, Lkdx;->dLz:Z

    if-eqz v0, :cond_1

    .line 162
    new-instance v0, Ljava/io/IOException;

    const-string v1, "shutdown"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 176
    :catchall_1
    move-exception v0

    monitor-exit v10

    throw v0

    .line 164
    :cond_1
    :try_start_3
    iget v1, p0, Lkdx;->eUd:I

    .line 165
    iget v0, p0, Lkdx;->eUd:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkdx;->eUd:I

    .line 166
    new-instance v0, Lkee;

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v7, p0, Lkdx;->eUe:Lkdt;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lkee;-><init>(ILkdx;ZZILjava/util/List;Lkdt;)V

    .line 168
    invoke-virtual {v0}, Lkee;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    iget-object v2, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lkdx;->jL(Z)V

    .line 172
    :cond_2
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 174
    :try_start_4
    iget-object v2, p0, Lkdx;->eTZ:Lkde;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move v5, v1

    move-object v9, p1

    invoke-interface/range {v2 .. v9}, Lkde;->a(ZZIIIILjava/util/List;)V

    .line 176
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 178
    return-object v0
.end method

.method public final a(IZ[BII)V
    .locals 6

    .prologue
    .line 188
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    const/4 v4, 0x0

    move v1, p2

    move v2, p1

    move-object v3, p3

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lkde;->a(ZI[BII)V

    .line 189
    return-void
.end method

.method public final declared-synchronized ayq()Z
    .locals 4

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lkdx;->eOY:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final bD(II)V
    .locals 7

    .prologue
    .line 207
    sget-object v6, Lkdx;->aRd:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lkdz;

    const-string v2, "OkHttp SPDY Writer %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lkdx;->eUb:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lkdz;-><init>(Lkdx;Ljava/lang/String;[Ljava/lang/Object;II)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 215
    return-void
.end method

.method final bE(II)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v0, p1, p2}, Lkde;->bB(II)V

    .line 219
    return-void
.end method

.method public final declared-synchronized bxE()J
    .locals 2

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lkdx;->eOY:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bzn()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v0}, Lkde;->bzf()V

    .line 363
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    new-instance v1, Lkdt;

    invoke-direct {v1}, Lkdt;-><init>()V

    invoke-interface {v0, v1}, Lkde;->a(Lkdt;)V

    .line 364
    return-void
.end method

.method final c(ILkdb;)V
    .locals 7

    .prologue
    .line 192
    sget-object v6, Lkdx;->aRd:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lkdy;

    const-string v2, "OkHttp SPDY Writer %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lkdx;->eUb:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lkdy;-><init>(Lkdx;Ljava/lang/String;[Ljava/lang/Object;ILkdb;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 200
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 301
    sget-object v0, Lkdb;->eSU:Lkdb;

    sget-object v1, Lkdb;->eTf:Lkdb;

    invoke-direct {p0, v0, v1}, Lkdx;->a(Lkdb;Lkdb;)V

    .line 302
    return-void
.end method

.method final d(ILkdb;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v0, p1, p2}, Lkde;->a(ILkdb;)V

    .line 204
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lkdx;->eTZ:Lkde;

    invoke-interface {v0}, Lkde;->flush()V

    .line 273
    return-void
.end method

.method final declared-synchronized tT(I)Lkee;
    .locals 2

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkee;

    .line 118
    if-eqz v0, :cond_0

    iget-object v1, p0, Lkdx;->eUa:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lkdx;->jL(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :cond_0
    monitor-exit p0

    return-object v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
