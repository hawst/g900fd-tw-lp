.class public final Ljif;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public aiX:Ljcn;

.field private alC:Ljava/lang/String;

.field private dMk:Ljava/lang/String;

.field private eni:Ljava/lang/String;

.field private enj:J

.field private enk:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57243
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 57244
    const/4 v0, 0x0

    iput v0, p0, Ljif;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljif;->afh:Ljava/lang/String;

    iput-object v2, p0, Ljif;->aiX:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljif;->eni:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljif;->enj:J

    const-string v0, ""

    iput-object v0, p0, Ljif;->enk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljif;->alC:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljif;->dMk:Ljava/lang/String;

    iput-object v2, p0, Ljif;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljif;->eCz:I

    .line 57245
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 57092
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljif;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljif;->afh:Ljava/lang/String;

    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljif;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljif;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljif;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Ljif;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljif;->eni:Ljava/lang/String;

    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljif;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljif;->enj:J

    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljif;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljif;->dMk:Ljava/lang/String;

    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljif;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljif;->enk:Ljava/lang/String;

    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljif;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljif;->alC:Ljava/lang/String;

    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljif;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 57264
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 57265
    const/4 v0, 0x1

    iget-object v1, p0, Ljif;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 57267
    :cond_0
    iget-object v0, p0, Ljif;->aiX:Ljcn;

    if-eqz v0, :cond_1

    .line 57268
    const/4 v0, 0x2

    iget-object v1, p0, Ljif;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 57270
    :cond_1
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 57271
    const/4 v0, 0x3

    iget-object v1, p0, Ljif;->eni:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 57273
    :cond_2
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 57274
    const/4 v0, 0x4

    iget-wide v2, p0, Ljif;->enj:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 57276
    :cond_3
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 57277
    const/4 v0, 0x5

    iget-object v1, p0, Ljif;->dMk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 57279
    :cond_4
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 57280
    const/4 v0, 0x6

    iget-object v1, p0, Ljif;->enk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 57282
    :cond_5
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 57283
    const/4 v0, 0x7

    iget-object v1, p0, Ljif;->alC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 57285
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 57286
    return-void
.end method

.method public final bmZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57180
    iget-object v0, p0, Ljif;->enk:Ljava/lang/String;

    return-object v0
.end method

.method public final bna()Z
    .locals 1

    .prologue
    .line 57213
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57114
    iget-object v0, p0, Ljif;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 57290
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 57291
    iget v1, p0, Ljif;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 57292
    const/4 v1, 0x1

    iget-object v2, p0, Ljif;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57295
    :cond_0
    iget-object v1, p0, Ljif;->aiX:Ljcn;

    if-eqz v1, :cond_1

    .line 57296
    const/4 v1, 0x2

    iget-object v2, p0, Ljif;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57299
    :cond_1
    iget v1, p0, Ljif;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 57300
    const/4 v1, 0x3

    iget-object v2, p0, Ljif;->eni:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57303
    :cond_2
    iget v1, p0, Ljif;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 57304
    const/4 v1, 0x4

    iget-wide v2, p0, Ljif;->enj:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57307
    :cond_3
    iget v1, p0, Ljif;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 57308
    const/4 v1, 0x5

    iget-object v2, p0, Ljif;->dMk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57311
    :cond_4
    iget v1, p0, Ljif;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 57312
    const/4 v1, 0x6

    iget-object v2, p0, Ljif;->enk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57315
    :cond_5
    iget v1, p0, Ljif;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 57316
    const/4 v1, 0x7

    iget-object v2, p0, Ljif;->alC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57319
    :cond_6
    return v0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 57125
    iget v0, p0, Ljif;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57202
    iget-object v0, p0, Ljif;->alC:Ljava/lang/String;

    return-object v0
.end method

.method public final vH(Ljava/lang/String;)Ljif;
    .locals 1

    .prologue
    .line 57117
    if-nez p1, :cond_0

    .line 57118
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57120
    :cond_0
    iput-object p1, p0, Ljif;->afh:Ljava/lang/String;

    .line 57121
    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljif;->aez:I

    .line 57122
    return-object p0
.end method

.method public final vI(Ljava/lang/String;)Ljif;
    .locals 1

    .prologue
    .line 57183
    if-nez p1, :cond_0

    .line 57184
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57186
    :cond_0
    iput-object p1, p0, Ljif;->enk:Ljava/lang/String;

    .line 57187
    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljif;->aez:I

    .line 57188
    return-object p0
.end method

.method public final vJ(Ljava/lang/String;)Ljif;
    .locals 1

    .prologue
    .line 57205
    if-nez p1, :cond_0

    .line 57206
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57208
    :cond_0
    iput-object p1, p0, Ljif;->alC:Ljava/lang/String;

    .line 57209
    iget v0, p0, Ljif;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljif;->aez:I

    .line 57210
    return-object p0
.end method
