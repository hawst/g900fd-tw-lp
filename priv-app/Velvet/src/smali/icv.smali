.class public final Licv;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcky;Lcom/google/android/speech/embedded/TaggerResult;)I
    .locals 5
    .param p0    # Lcky;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/16 v0, 0xa

    const/4 v1, 0x0

    .line 179
    invoke-virtual {p1}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v2

    .line 180
    const-string v3, "CallContact"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 214
    :cond_0
    :goto_0
    return v0

    .line 182
    :cond_1
    const-string v3, "AmbiguousCommunicationAction"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 186
    const-string v0, "CallNumber"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    const/16 v0, 0x1c

    goto :goto_0

    .line 188
    :cond_2
    const-string v0, "OpenApp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    const/4 v0, 0x3

    goto :goto_0

    .line 190
    :cond_3
    const-string v0, "SendTextToContact"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 191
    const/4 v0, 0x1

    goto :goto_0

    .line 192
    :cond_4
    const-string v0, "Undo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Redo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Cancel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v0, v1

    .line 196
    goto :goto_0

    .line 197
    :cond_6
    const-string v0, "Selection"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "SelectRecipient"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "ConfirmRelationship"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 200
    :cond_7
    if-nez p0, :cond_8

    move v0, v1

    .line 201
    goto :goto_0

    .line 203
    :cond_8
    invoke-virtual {p0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_9

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v2

    if-nez v2, :cond_a

    :cond_9
    move v0, v1

    .line 205
    goto :goto_0

    .line 207
    :cond_a
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agc()I

    move-result v0

    goto :goto_0

    .line 208
    :cond_b
    const-string v0, "AddRelationship"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 209
    const/16 v0, 0x2e

    goto :goto_0

    .line 210
    :cond_c
    const-string v0, "RemoveRelationship"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 211
    const/16 v0, 0x2f

    goto/16 :goto_0

    .line 213
    :cond_d
    const-string v0, "ActionV2Factory"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 214
    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Lghy;Ldzb;Ligi;Lifw;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 14
    .param p0    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lciy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lghy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ligi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p9    # Lifw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 68
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    invoke-static {p1, v0, v1, v2}, Licv;->a(Ldyv;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Ldzb;)Ljoq;

    move-result-object v10

    .line 70
    if-nez v10, :cond_0

    .line 71
    const/4 v3, 0x0

    .line 73
    :goto_0
    return-object v3

    :cond_0
    const/4 v13, 0x1

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    invoke-static/range {v3 .. v13}, Lico;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v3

    goto :goto_0
.end method

.method public static a(Ldyv;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Ldzb;)Ljoq;
    .locals 7
    .param p0    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 108
    const-string v0, "Contact"

    invoke-virtual {p2, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcom/google/android/search/shared/contact/Person;->kD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string v0, "PhoneType"

    invoke-virtual {p2, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 111
    const-string v0, "Number"

    invoke-virtual {p2, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    if-eqz v1, :cond_4

    .line 119
    new-instance v0, Ljoq;

    invoke-direct {v0}, Ljoq;-><init>()V

    .line 120
    invoke-virtual {v0, v1}, Ljoq;->xN(Ljava/lang/String;)Ljoq;

    .line 121
    invoke-virtual {p1}, Lchk;->FW()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 122
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v3

    invoke-virtual {v3}, Lgql;->aJW()Leai;

    move-result-object v3

    .line 124
    invoke-interface {v3, v1}, Leai;->kH(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 125
    invoke-virtual {v0, v6}, Ljoq;->iP(Z)Ljoq;

    .line 126
    invoke-interface {v3, v1}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljoq;->xO(Ljava/lang/String;)Ljoq;

    .line 132
    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Ljoq;->xP(Ljava/lang/String;)Ljoq;

    .line 133
    new-instance v3, Ljop;

    invoke-direct {v3}, Ljop;-><init>()V

    .line 134
    new-array v4, v6, [Ljava/lang/String;

    aput-object v1, v4, v5

    iput-object v4, v3, Ljop;->ewI:[Ljava/lang/String;

    .line 135
    if-eqz p3, :cond_1

    .line 136
    new-array v1, v6, [I

    invoke-static {p3}, Ldzb;->b(Ldzb;)I

    move-result v4

    aput v4, v1, v5

    iput-object v1, v3, Ljop;->dQP:[I

    .line 138
    :cond_1
    if-eqz v2, :cond_2

    .line 139
    invoke-virtual {p0, v2}, Ldyv;->kw(Ljava/lang/String;)Ljor;

    move-result-object v1

    iput-object v1, v3, Ljop;->ewy:Ljor;

    .line 141
    :cond_2
    iput-object v3, v0, Ljoq;->ewM:Ljop;

    .line 151
    :goto_1
    return-object v0

    .line 129
    :cond_3
    invoke-virtual {v0, v5}, Ljoq;->iP(Z)Ljoq;

    goto :goto_0

    .line 143
    :cond_4
    if-eqz v0, :cond_5

    .line 144
    new-instance v1, Ljoj;

    invoke-direct {v1}, Ljoj;-><init>()V

    .line 145
    new-array v2, v6, [Ljon;

    new-instance v3, Ljon;

    invoke-direct {v3}, Ljon;-><init>()V

    invoke-virtual {v3, v0}, Ljon;->xK(Ljava/lang/String;)Ljon;

    move-result-object v0

    aput-object v0, v2, v5

    iput-object v2, v1, Ljoj;->ews:[Ljon;

    .line 147
    new-instance v0, Ljoq;

    invoke-direct {v0}, Ljoq;-><init>()V

    .line 148
    new-array v2, v6, [Ljoj;

    aput-object v1, v2, v5

    iput-object v2, v0, Ljoq;->ewL:[Ljoj;

    goto :goto_1

    .line 151
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;)V
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Leir;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Leir;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    return-void
.end method

.method public static b(Lcom/google/android/speech/embedded/TaggerResult;)Ljks;
    .locals 6
    .param p0    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 81
    new-instance v0, Ljks;

    invoke-direct {v0}, Ljks;-><init>()V

    .line 82
    const-string v1, "Contact"

    invoke-virtual {p0, v1}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    const-string v2, "PhoneType"

    invoke-virtual {p0, v2}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    const-string v3, "Number"

    invoke-virtual {p0, v3}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 86
    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {v0, v1}, Ljks;->wq(Ljava/lang/String;)Ljks;

    .line 89
    :cond_0
    if-eqz v2, :cond_1

    .line 90
    new-instance v1, Ljlc;

    invoke-direct {v1}, Ljlc;-><init>()V

    .line 91
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljlc;->wC(Ljava/lang/String;)Ljlc;

    .line 92
    new-array v2, v5, [Ljlc;

    aput-object v1, v2, v4

    iput-object v2, v0, Ljks;->eqg:[Ljlc;

    .line 94
    :cond_1
    if-eqz v3, :cond_2

    .line 95
    new-instance v1, Ljlc;

    invoke-direct {v1}, Ljlc;-><init>()V

    .line 96
    invoke-virtual {v1, v3}, Ljlc;->wB(Ljava/lang/String;)Ljlc;

    .line 97
    new-array v2, v5, [Ljlc;

    aput-object v1, v2, v4

    iput-object v2, v0, Ljks;->eqg:[Ljlc;

    .line 99
    :cond_2
    return-object v0
.end method

.method public static c(Lcom/google/android/speech/embedded/TaggerResult;)V
    .locals 2
    .param p0    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 156
    const-string v0, "CallNumber"

    invoke-virtual {p0}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "Number"

    invoke-virtual {p0, v0}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    const-string v0, "CallContact"

    invoke-static {v0, p0}, Licv;->a(Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;)V

    goto :goto_0
.end method

.method public static pd(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 221
    const-string v0, "Undo"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Redo"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Selection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SelectRecipient"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ConfirmRelationship"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Cancel"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
