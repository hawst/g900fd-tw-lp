.class final Lelx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;
.implements Livq;


# instance fields
.field private final aWo:Livd;

.field private bmi:Z

.field private final ceT:Landroid/animation/LayoutTransition;

.field private ceU:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ceV:Z

.field private final dK:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/animation/LayoutTransition;Landroid/view/View;)V
    .locals 1
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lelx;->dK:Ljava/lang/Object;

    .line 165
    new-instance v0, Livd;

    invoke-direct {v0}, Livd;-><init>()V

    iput-object v0, p0, Lelx;->aWo:Livd;

    .line 173
    iput-object p1, p0, Lelx;->ceT:Landroid/animation/LayoutTransition;

    .line 174
    iput-object p2, p0, Lelx;->ceU:Landroid/view/View;

    .line 175
    iget-object v0, p0, Lelx;->ceT:Landroid/animation/LayoutTransition;

    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelx;->ceV:Z

    .line 177
    iget-object v0, p0, Lelx;->ceT:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, p0}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 226
    iget-object v1, p0, Lelx;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 227
    :try_start_0
    iget-boolean v0, p0, Lelx;->ceV:Z

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lelx;->aWo:Livd;

    invoke-virtual {v0, p1, p2}, Livd;->b(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 232
    :goto_0
    monitor-exit v1

    return-void

    .line 230
    :cond_0
    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final cancel(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 183
    iget-object v2, p0, Lelx;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 184
    :try_start_0
    iget-boolean v3, p0, Lelx;->ceV:Z

    if-nez v3, :cond_0

    monitor-exit v2

    .line 189
    :goto_0
    return v0

    .line 185
    :cond_0
    iget-object v0, p0, Lelx;->ceT:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, p0}, Landroid/animation/LayoutTransition;->removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lelx;->ceV:Z

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelx;->bmi:Z

    .line 188
    iget-object v0, p0, Lelx;->aWo:Livd;

    invoke-virtual {v0}, Livd;->execute()V

    .line 189
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lelx;->ceU:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lelx;->ceU:Landroid/view/View;

    if-eq p3, v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    :cond_1
    iget-object v1, p0, Lelx;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 246
    :try_start_0
    invoke-virtual {p1, p0}, Landroid/animation/LayoutTransition;->removeTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, Lelx;->ceV:Z

    .line 248
    iget-object v0, p0, Lelx;->aWo:Livd;

    invoke-virtual {v0}, Livd;->execute()V

    .line 249
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    :cond_2
    return-void

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final isCancelled()Z
    .locals 2

    .prologue
    .line 212
    iget-object v1, p0, Lelx;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 213
    :try_start_0
    iget-boolean v0, p0, Lelx;->bmi:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isDone()Z
    .locals 2

    .prologue
    .line 219
    iget-object v1, p0, Lelx;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 220
    :try_start_0
    iget-boolean v0, p0, Lelx;->ceV:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 238
    return-void
.end method
