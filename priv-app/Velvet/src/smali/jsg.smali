.class public final Ljsg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eCf:[Ljsg;


# instance fields
.field private aXR:Ljava/lang/String;

.field private aez:I

.field private agq:Ljava/lang/String;

.field private akf:Ljava/lang/String;

.field private dNT:Ljava/lang/String;

.field private eCg:J

.field private eCh:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 626
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 627
    const/4 v0, 0x0

    iput v0, p0, Ljsg;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljsg;->eCg:J

    const-string v0, ""

    iput-object v0, p0, Ljsg;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljsg;->akf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljsg;->dNT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljsg;->eCh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljsg;->aXR:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljsg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljsg;->eCz:I

    .line 628
    return-void
.end method

.method public static btK()[Ljsg;
    .locals 2

    .prologue
    .line 484
    sget-object v0, Ljsg;->eCf:[Ljsg;

    if-nez v0, :cond_1

    .line 485
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 487
    :try_start_0
    sget-object v0, Ljsg;->eCf:[Ljsg;

    if-nez v0, :cond_0

    .line 488
    const/4 v0, 0x0

    new-array v0, v0, [Ljsg;

    sput-object v0, Ljsg;->eCf:[Ljsg;

    .line 490
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    :cond_1
    sget-object v0, Ljsg;->eCf:[Ljsg;

    return-object v0

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Ar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Ljsg;->dNT:Ljava/lang/String;

    return-object v0
.end method

.method public final KK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Ljsg;->aXR:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 478
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljsg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljsg;->eCg:J

    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsg;->agq:Ljava/lang/String;

    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsg;->akf:Ljava/lang/String;

    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsg;->dNT:Ljava/lang/String;

    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljsg;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsg;->eCh:Ljava/lang/String;

    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljsg;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljsg;->aXR:Ljava/lang/String;

    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljsg;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 646
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 647
    const/4 v0, 0x1

    iget-wide v2, p0, Ljsg;->eCg:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 649
    :cond_0
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 650
    const/4 v0, 0x2

    iget-object v1, p0, Ljsg;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 652
    :cond_1
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 653
    const/4 v0, 0x3

    iget-object v1, p0, Ljsg;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 655
    :cond_2
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 656
    const/4 v0, 0x4

    iget-object v1, p0, Ljsg;->dNT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 658
    :cond_3
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 659
    const/4 v0, 0x5

    iget-object v1, p0, Ljsg;->eCh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 661
    :cond_4
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 662
    const/4 v0, 0x6

    iget-object v1, p0, Ljsg;->aXR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 664
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 665
    return-void
.end method

.method public final bau()Z
    .locals 1

    .prologue
    .line 574
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final btL()J
    .locals 2

    .prologue
    .line 500
    iget-wide v0, p0, Ljsg;->eCg:J

    return-wide v0
.end method

.method public final dH(J)Ljsg;
    .locals 1

    .prologue
    .line 503
    iput-wide p1, p0, Ljsg;->eCg:J

    .line 504
    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsg;->aez:I

    .line 505
    return-object p0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Ljsg;->akf:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Ljsg;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 669
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 670
    iget v1, p0, Ljsg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 671
    const/4 v1, 0x1

    iget-wide v2, p0, Ljsg;->eCg:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    :cond_0
    iget v1, p0, Ljsg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 675
    const/4 v1, 0x2

    iget-object v2, p0, Ljsg;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    :cond_1
    iget v1, p0, Ljsg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 679
    const/4 v1, 0x3

    iget-object v2, p0, Ljsg;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    :cond_2
    iget v1, p0, Ljsg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 683
    const/4 v1, 0x4

    iget-object v2, p0, Ljsg;->dNT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 686
    :cond_3
    iget v1, p0, Ljsg;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 687
    const/4 v1, 0x5

    iget-object v2, p0, Ljsg;->eCh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 690
    :cond_4
    iget v1, p0, Ljsg;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 691
    const/4 v1, 0x6

    iget-object v2, p0, Ljsg;->aXR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 694
    :cond_5
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 552
    iget v0, p0, Ljsg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final yF(Ljava/lang/String;)Ljsg;
    .locals 1

    .prologue
    .line 522
    if-nez p1, :cond_0

    .line 523
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 525
    :cond_0
    iput-object p1, p0, Ljsg;->agq:Ljava/lang/String;

    .line 526
    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsg;->aez:I

    .line 527
    return-object p0
.end method

.method public final yG(Ljava/lang/String;)Ljsg;
    .locals 1

    .prologue
    .line 544
    if-nez p1, :cond_0

    .line 545
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 547
    :cond_0
    iput-object p1, p0, Ljsg;->akf:Ljava/lang/String;

    .line 548
    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsg;->aez:I

    .line 549
    return-object p0
.end method

.method public final yH(Ljava/lang/String;)Ljsg;
    .locals 1

    .prologue
    .line 566
    if-nez p1, :cond_0

    .line 567
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 569
    :cond_0
    iput-object p1, p0, Ljsg;->dNT:Ljava/lang/String;

    .line 570
    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljsg;->aez:I

    .line 571
    return-object p0
.end method

.method public final yI(Ljava/lang/String;)Ljsg;
    .locals 1

    .prologue
    .line 588
    if-nez p1, :cond_0

    .line 589
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 591
    :cond_0
    iput-object p1, p0, Ljsg;->eCh:Ljava/lang/String;

    .line 592
    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljsg;->aez:I

    .line 593
    return-object p0
.end method

.method public final yJ(Ljava/lang/String;)Ljsg;
    .locals 1

    .prologue
    .line 610
    if-nez p1, :cond_0

    .line 611
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 613
    :cond_0
    iput-object p1, p0, Ljsg;->aXR:Ljava/lang/String;

    .line 614
    iget v0, p0, Ljsg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljsg;->aez:I

    .line 615
    return-object p0
.end method
