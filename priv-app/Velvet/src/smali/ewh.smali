.class public final Lewh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bng:Ljava/lang/String;

.field private final ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final cks:Ljava/util/concurrent/CountDownLatch;

.field public volatile ckt:Z

.field private cku:Ljava/lang/Object;

.field private volatile ckv:J

.field private final mAppContext:Landroid/content/Context;

.field private final mPreferencesSupplier:Ligi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ligi;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lewh;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 34
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lewh;->cks:Ljava/util/concurrent/CountDownLatch;

    .line 38
    iput-boolean v2, p0, Lewh;->ckt:Z

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lewh;->cku:Ljava/lang/Object;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lewh;->mAppContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lewh;->mPreferencesSupplier:Ligi;

    .line 50
    iget-object v0, p0, Lewh;->mAppContext:Landroid/content/Context;

    const v1, 0x7f0a0134

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewh;->bng:Ljava/lang/String;

    .line 51
    return-void
.end method

.method private awl()Z
    .locals 2

    .prologue
    .line 121
    :try_start_0
    iget-object v0, p0, Lewh;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 123
    :catch_0
    move-exception v0

    const-string v0, "UserClientIdManager"

    const-string v1, "Initialization latch wait interrupted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 126
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final awk()Ljava/lang/Long;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    invoke-static {}, Lenu;->auQ()V

    .line 65
    iget-object v0, p0, Lewh;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lewh;->cku:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lewh;->mPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iget-object v2, p0, Lewh;->bng:Ljava/lang/String;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lewh;->awm()V

    :goto_0
    iget-object v0, p0, Lewh;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_0
    invoke-direct {p0}, Lewh;->awl()Z

    move-result v0

    if-nez v0, :cond_2

    .line 67
    const/4 v0, 0x0

    .line 82
    :goto_1
    return-object v0

    .line 65
    :cond_1
    :try_start_1
    iget-object v2, p0, Lewh;->bng:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lewh;->ckv:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 70
    :cond_2
    iget-boolean v0, p0, Lewh;->ckt:Z

    if-eqz v0, :cond_4

    .line 71
    iget-object v1, p0, Lewh;->cku:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_2
    iget-boolean v0, p0, Lewh;->ckt:Z

    if-eqz v0, :cond_3

    .line 74
    invoke-virtual {p0}, Lewh;->awm()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lewh;->ckt:Z

    .line 77
    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 80
    :cond_4
    iget-wide v0, p0, Lewh;->ckv:J

    .line 82
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    .line 77
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final awm()V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lewh;->cku:Ljava/lang/Object;

    .line 139
    invoke-static {}, Lenu;->auQ()V

    .line 140
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 141
    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    .line 143
    iget-object v0, p0, Lewh;->mPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 144
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lewh;->bng:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 145
    iput-wide v2, p0, Lewh;->ckv:J

    .line 146
    return-void
.end method
