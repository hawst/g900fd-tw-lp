.class final Lddr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lepn;


# instance fields
.field private synthetic aSa:Lcom/google/android/shared/search/Query;

.field private synthetic anv:Lemy;

.field private synthetic bun:Lddp;

.field private synthetic buo:Ljava/lang/String;


# direct methods
.method constructor <init>(Lddp;Lemy;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lddr;->bun:Lddp;

    iput-object p2, p0, Lddr;->anv:Lemy;

    iput-object p3, p0, Lddr;->aSa:Lcom/google/android/shared/search/Query;

    iput-object p4, p0, Lddr;->buo:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "ContextContentProvider"

    return-object v0
.end method

.method public final run()V
    .locals 18

    .prologue
    .line 91
    move-object/from16 v0, p0

    iget-object v2, v0, Lddr;->bun:Lddp;

    iget-object v5, v2, Lddp;->buk:Leqo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lddr;->anv:Lemy;

    move-object/from16 v0, p0

    iget-object v2, v0, Lddr;->bun:Lddp;

    move-object/from16 v0, p0

    iget-object v7, v0, Lddr;->aSa:Lcom/google/android/shared/search/Query;

    move-object/from16 v0, p0

    iget-object v3, v0, Lddr;->buo:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v8, v2, Lddp;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x0

    invoke-virtual {v8, v4, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v2, "ContextSuggestionsFetcher"

    const-string v4, "Can\'t obtain provider info for %s."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-static {v2, v4, v7}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-static {v5, v6, v2}, Lemz;->a(Ljava/util/concurrent/Executor;Lemy;Ljava/lang/Object;)V

    .line 93
    return-void

    .line 91
    :cond_1
    iget-object v8, v2, Lddp;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    iget-object v9, v4, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    invoke-static {v8, v9}, Lbgt;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v2, "ContextSuggestionsFetcher"

    const-string v4, "Not first party content provider %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-static {v2, v4, v7}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v3, v8}, Lddp;->am(Ljava/lang/String;Ljava/lang/String;)Lgny;

    move-result-object v3

    iget-object v8, v4, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v2, v3, Lgny;->cQR:[Lgnx;

    if-nez v2, :cond_4

    :cond_3
    const-string v2, "ContextSuggestionsFetcher"

    const-string v3, "Content Provider results null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, v3, Lgny;->cQR:[Lgnx;

    array-length v4, v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v9, v3, Lgny;->cQR:[Lgnx;

    array-length v10, v9

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v10, :cond_0

    aget-object v3, v9, v4

    iget-object v11, v3, Lgnx;->cQN:[Lgnw;

    if-eqz v11, :cond_6

    iget-object v11, v3, Lgnx;->cQN:[Lgnw;

    array-length v11, v11

    if-eqz v11, :cond_6

    new-instance v11, Lddx;

    const-string v12, "Context"

    invoke-direct {v11, v12, v7}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    iget-object v12, v3, Lgnx;->cQN:[Lgnw;

    array-length v13, v12

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v13, :cond_5

    aget-object v14, v12, v3

    new-instance v15, Lded;

    invoke-direct {v15}, Lded;-><init>()V

    invoke-virtual {v14}, Lgnw;->aIe()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lded;->bux:Ljava/lang/CharSequence;

    invoke-virtual {v14}, Lgnw;->aIf()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lded;->buy:Ljava/lang/CharSequence;

    invoke-virtual {v14}, Lgnw;->aIg()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lded;->buA:Ljava/lang/String;

    invoke-virtual {v14}, Lgnw;->asE()J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lded;->bjh:J

    invoke-virtual {v14}, Lgnw;->aIh()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v15, Lded;->buD:Ljava/lang/String;

    const/16 v14, 0x56

    invoke-virtual {v15, v14}, Lded;->fI(I)Lded;

    move-result-object v14

    const/4 v15, 0x1

    iput-boolean v15, v14, Lded;->buW:Z

    iput-object v8, v14, Lded;->buv:Ljava/lang/String;

    invoke-virtual {v14}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v14

    invoke-virtual {v11, v14}, Lddx;->d(Lcom/google/android/shared/search/Suggestion;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1
.end method
