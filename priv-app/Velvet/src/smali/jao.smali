.class public final Ljao;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dWX:[Ljao;


# instance fields
.field private aez:I

.field private agv:I

.field private dWY:Ljbu;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v0, p0, Ljao;->aez:I

    iput v0, p0, Ljao;->agv:I

    iput-object v1, p0, Ljao;->dWY:Ljbu;

    iput-object v1, p0, Ljao;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljao;->eCz:I

    return-void
.end method

.method public static bes()[Ljao;
    .locals 2

    sget-object v0, Ljao;->dWX:[Ljao;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljao;->dWX:[Ljao;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljao;

    sput-object v0, Ljao;->dWX:[Ljao;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljao;->dWX:[Ljao;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljao;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljao;->agv:I

    iget v0, p0, Ljao;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljao;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljao;->dWY:Ljbu;

    if-nez v0, :cond_1

    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    iput-object v0, p0, Ljao;->dWY:Ljbu;

    :cond_1
    iget-object v0, p0, Ljao;->dWY:Ljbu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljao;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljao;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget-object v0, p0, Ljao;->dWY:Ljbu;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljao;->dWY:Ljbu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljao;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljao;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljao;->dWY:Ljbu;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljao;->dWY:Ljbu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method
