.class public final Lll;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final kK:Lkt;

.field final kg:Lkw;

.field kl:Lky;

.field final kp:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lkt;)V
    .locals 1

    .prologue
    .line 1262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lll;->kp:Ljava/util/ArrayList;

    .line 1263
    iput-object p1, p0, Lll;->kK:Lkt;

    .line 1264
    iget-object v0, p1, Lkt;->kg:Lkw;

    iput-object v0, p0, Lll;->kg:Lkw;

    .line 1265
    return-void
.end method


# virtual methods
.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Lll;->kg:Lkw;

    iget-object v0, v0, Lkw;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final k(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1321
    iget-object v0, p0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1322
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1323
    iget-object v0, p0, Lll;->kp:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    iget-object v0, v0, Llm;->kM:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1327
    :goto_1
    return v0

    .line 1322
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1327
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1332
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteProviderInfo{ packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lll;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
