.class final Leyo;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic clU:Leyn;


# direct methods
.method constructor <init>(Leyn;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Leyo;->clU:Leyn;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs ko()Ljava/lang/Void;
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 96
    iget-object v5, p0, Leyo;->clU:Leyn;

    new-instance v6, Leyy;

    invoke-direct {v6}, Leyy;-><init>()V

    new-instance v0, Ljava/io/File;

    iget-object v3, v5, Leyn;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "calendar_store"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    const-wide/32 v10, 0x20000

    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    const-string v0, "CalendarDataProviderImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Disk store is too large ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Leyn;->awi()V

    .line 97
    :goto_0
    iget-object v0, p0, Leyo;->clU:Leyn;

    iget-object v0, v0, Leyn;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 99
    return-object v1

    .line 96
    :cond_0
    const-wide/16 v10, 0x1

    cmp-long v0, v8, v10

    if-gez v0, :cond_1

    invoke-virtual {v5}, Leyn;->awi()V

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v0, v5, Leyn;->mAppContext:Landroid/content/Context;

    const-string v3, "calendar_store"

    invoke-virtual {v0, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    long-to-int v3, v8

    :try_start_1
    new-array v7, v3, [B

    long-to-int v3, v8

    move v4, v3

    move v3, v2

    :cond_2
    invoke-virtual {v0, v7, v3, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v8

    if-gtz v8, :cond_3

    :goto_1
    if-gtz v3, :cond_4

    invoke-virtual {v5}, Leyn;->awi()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_3
    sub-int/2addr v4, v8

    add-int/2addr v3, v8

    if-gtz v4, :cond_2

    goto :goto_1

    :cond_4
    :try_start_2
    new-instance v4, Laml;

    invoke-direct {v4}, Laml;-><init>()V

    const/4 v8, 0x0

    invoke-static {v4, v7, v8, v3}, Ljsr;->b(Ljsr;[BII)Ljsr;

    iget-object v3, v4, Laml;->aeV:[Lamk;

    array-length v7, v3

    :goto_2
    if-ge v2, v7, :cond_6

    aget-object v8, v3, v2

    invoke-virtual {v6, v8}, Leyy;->c(Lamk;)Z

    move-result v8

    if-nez v8, :cond_5

    const-string v8, "CalendarDataProviderImpl"

    const-string v9, "Read invalid event data from disk store"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    iget-object v2, v4, Laml;->aeW:[Lamm;

    invoke-static {v2}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v6, v2}, Leyy;->g(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "CalendarDataProviderImpl"

    const-string v3, "Read invalid calendar data from disk store"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {v4}, Laml;->mZ()Z

    move-result v2

    iput-boolean v2, v6, Leyy;->cmm:Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    :goto_3
    invoke-virtual {v6}, Leyy;->awM()Leyx;

    move-result-object v0

    iput-object v0, v5, Leyn;->clT:Leyx;

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_4
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_5
    :try_start_3
    const-string v3, "CalendarDataProviderImpl"

    const-string v4, "Failed reading from disk store"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v5}, Leyn;->awi()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_6
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v2

    move-object v12, v2

    move-object v2, v0

    move-object v0, v12

    goto :goto_5

    :catch_3
    move-exception v2

    goto :goto_4
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Leyo;->ko()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
