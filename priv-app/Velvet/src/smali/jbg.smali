.class public final Ljbg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dYj:[Ljbg;


# instance fields
.field private aez:I

.field private dYk:Ljava/lang/String;

.field private dYl:Ljava/lang/String;

.field private dYm:Ljava/lang/String;

.field private dYn:Ljava/lang/String;

.field private dYo:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37980
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 37981
    iput v1, p0, Ljbg;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbg;->dYk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbg;->dYl:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbg;->dYm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbg;->dYn:Ljava/lang/String;

    iput-boolean v1, p0, Ljbg;->dYo:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljbg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbg;->eCz:I

    .line 37982
    return-void
.end method

.method public static beN()[Ljbg;
    .locals 2

    .prologue
    .line 37860
    sget-object v0, Ljbg;->dYj:[Ljbg;

    if-nez v0, :cond_1

    .line 37861
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 37863
    :try_start_0
    sget-object v0, Ljbg;->dYj:[Ljbg;

    if-nez v0, :cond_0

    .line 37864
    const/4 v0, 0x0

    new-array v0, v0, [Ljbg;

    sput-object v0, Ljbg;->dYj:[Ljbg;

    .line 37866
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37868
    :cond_1
    sget-object v0, Ljbg;->dYj:[Ljbg;

    return-object v0

    .line 37866
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 37854
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbg;->dYk:Ljava/lang/String;

    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbg;->dYl:Ljava/lang/String;

    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbg;->dYm:Ljava/lang/String;

    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbg;->dYn:Ljava/lang/String;

    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbg;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbg;->dYo:Z

    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 37999
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 38000
    const/4 v0, 0x1

    iget-object v1, p0, Ljbg;->dYk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38002
    :cond_0
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 38003
    const/4 v0, 0x2

    iget-object v1, p0, Ljbg;->dYl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38005
    :cond_1
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 38006
    const/4 v0, 0x3

    iget-object v1, p0, Ljbg;->dYm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38008
    :cond_2
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 38009
    const/4 v0, 0x4

    iget-object v1, p0, Ljbg;->dYn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 38011
    :cond_3
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 38012
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljbg;->dYo:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 38014
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 38015
    return-void
.end method

.method public final beO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37898
    iget-object v0, p0, Ljbg;->dYl:Ljava/lang/String;

    return-object v0
.end method

.method public final beP()Z
    .locals 1

    .prologue
    .line 37909
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37920
    iget-object v0, p0, Ljbg;->dYm:Ljava/lang/String;

    return-object v0
.end method

.method public final beR()Z
    .locals 1

    .prologue
    .line 37931
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37942
    iget-object v0, p0, Ljbg;->dYn:Ljava/lang/String;

    return-object v0
.end method

.method public final beT()Z
    .locals 1

    .prologue
    .line 37953
    iget v0, p0, Ljbg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beU()Z
    .locals 1

    .prologue
    .line 37964
    iget-boolean v0, p0, Ljbg;->dYo:Z

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 38019
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 38020
    iget v1, p0, Ljbg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 38021
    const/4 v1, 0x1

    iget-object v2, p0, Ljbg;->dYk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38024
    :cond_0
    iget v1, p0, Ljbg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 38025
    const/4 v1, 0x2

    iget-object v2, p0, Ljbg;->dYl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38028
    :cond_1
    iget v1, p0, Ljbg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 38029
    const/4 v1, 0x3

    iget-object v2, p0, Ljbg;->dYm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38032
    :cond_2
    iget v1, p0, Ljbg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 38033
    const/4 v1, 0x4

    iget-object v2, p0, Ljbg;->dYn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38036
    :cond_3
    iget v1, p0, Ljbg;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 38037
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljbg;->dYo:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 38040
    :cond_4
    return v0
.end method

.method public final sk(Ljava/lang/String;)Ljbg;
    .locals 1

    .prologue
    .line 37901
    if-nez p1, :cond_0

    .line 37902
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37904
    :cond_0
    iput-object p1, p0, Ljbg;->dYl:Ljava/lang/String;

    .line 37905
    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbg;->aez:I

    .line 37906
    return-object p0
.end method

.method public final sl(Ljava/lang/String;)Ljbg;
    .locals 1

    .prologue
    .line 37923
    if-nez p1, :cond_0

    .line 37924
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37926
    :cond_0
    iput-object p1, p0, Ljbg;->dYm:Ljava/lang/String;

    .line 37927
    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbg;->aez:I

    .line 37928
    return-object p0
.end method

.method public final sm(Ljava/lang/String;)Ljbg;
    .locals 1

    .prologue
    .line 37945
    if-nez p1, :cond_0

    .line 37946
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37948
    :cond_0
    iput-object p1, p0, Ljbg;->dYn:Ljava/lang/String;

    .line 37949
    iget v0, p0, Ljbg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbg;->aez:I

    .line 37950
    return-object p0
.end method
