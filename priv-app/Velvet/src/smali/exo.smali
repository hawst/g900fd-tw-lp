.class public Lexo;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final clr:Ljava/util/Collection;

.field private final cls:Ljcu;

.field private final mClock:Lemp;

.field private final mEntry:Lizj;

.field private final mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Lfcx;Lizj;Liwk;Lemp;)V
    .locals 6

    .prologue
    .line 41
    invoke-static {p3}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lexo;-><init>(Lfcx;Lizj;Ljava/util/Collection;Ljcu;Lemp;)V

    .line 42
    return-void
.end method

.method constructor <init>(Lfcx;Lizj;Ljava/util/Collection;Ljcu;Lemp;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 56
    iput-object p1, p0, Lexo;->mNetworkClient:Lfcx;

    .line 57
    iput-object p2, p0, Lexo;->mEntry:Lizj;

    .line 58
    iput-object p3, p0, Lexo;->clr:Ljava/util/Collection;

    .line 59
    iput-object p4, p0, Lexo;->cls:Ljcu;

    .line 60
    iput-object p5, p0, Lexo;->mClock:Lemp;

    .line 61
    return-void
.end method


# virtual methods
.method protected a(Liwk;J)Lizv;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lgam;

    iget-object v1, p0, Lexo;->mEntry:Lizj;

    invoke-direct {v0, v1, p1, p2, p3}, Lgam;-><init>(Lizj;Liwk;J)V

    .line 96
    iget-object v1, p0, Lexo;->cls:Ljcu;

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lexo;->cls:Ljcu;

    iput-object v1, v0, Lgam;->cEq:Ljcu;

    .line 99
    :cond_0
    invoke-virtual {v0}, Lgam;->aDZ()Lizv;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 28
    iget-object v0, p0, Lexo;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    new-instance v1, Liwl;

    invoke-direct {v1}, Liwl;-><init>()V

    iget-object v0, p0, Lexo;->clr:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwk;

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    new-array v5, v5, [Lizv;

    const/4 v6, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lexo;->a(Liwk;J)Lizv;

    move-result-object v0

    aput-object v0, v5, v6

    iput-object v5, v1, Liwl;->afC:[Lizv;

    goto :goto_0

    :cond_1
    const/16 v0, 0x18

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    iput-object v1, v0, Ljed;->eds:Liwl;

    iget-object v1, p0, Lexo;->mNetworkClient:Lfcx;

    invoke-interface {v1, v0}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v1, "RecordActionTask"

    const-string v2, "Error sending request to the server"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-object v0
.end method
