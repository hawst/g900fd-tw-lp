.class public final Lbml;
.super Ljava/lang/Object;


# instance fields
.field aFA:Z

.field aFB:Ljava/util/HashMap;

.field final aFz:Lbmv;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbmv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbml;->aFA:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbml;->aFB:Ljava/util/HashMap;

    iput-object p1, p0, Lbml;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lbml;->aFz:Lbmv;

    return-void
.end method


# virtual methods
.method a(Lbrm;Landroid/os/Looper;)Lbmn;
    .locals 3

    if-nez p2, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    const-string v1, "Can\'t create handler inside thread that has not called Looper.prepare()"

    invoke-static {v0, v1}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lbml;->aFB:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbml;->aFB:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    if-nez v0, :cond_1

    new-instance v0, Lbmn;

    invoke-direct {v0, p1, p2}, Lbmn;-><init>(Lbrm;Landroid/os/Looper;)V

    :cond_1
    iget-object v2, p0, Lbml;->aFB:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final zH()Landroid/location/Location;
    .locals 2

    iget-object v0, p0, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yS()V

    :try_start_0
    iget-object v0, p0, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    iget-object v1, p0, Lbml;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbmi;->fB(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
