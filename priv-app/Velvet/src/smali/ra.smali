.class public final Lra;
.super Lrk;
.source "PG"


# instance fields
.field private wv:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lra;-><init>(Landroid/graphics/Bitmap;Z)V

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/graphics/Bitmap;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, v0}, Lrk;-><init>(Z)V

    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 38
    iput-object p1, p0, Lra;->wv:Landroid/graphics/Bitmap;

    .line 39
    return-void
.end method


# virtual methods
.method protected final d(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method protected final dM()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lra;->wv:Landroid/graphics/Bitmap;

    return-object v0
.end method
