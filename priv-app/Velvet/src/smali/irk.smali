.class public abstract enum Lirk;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum dGE:Lirk;

.field public static final enum dGF:Lirk;

.field public static final enum dGG:Lirk;

.field public static final enum dGH:Lirk;

.field private static enum dGI:Lirk;

.field private static final synthetic dGJ:[Lirk;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    new-instance v0, Lirl;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1, v2}, Lirl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->dGE:Lirk;

    .line 63
    new-instance v0, Lirm;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1, v3}, Lirm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->dGF:Lirk;

    .line 87
    new-instance v0, Lirn;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1, v4}, Lirn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->dGG:Lirk;

    .line 113
    new-instance v0, Liro;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1, v5}, Liro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->dGH:Lirk;

    .line 124
    new-instance v0, Lirp;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1, v6}, Lirp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirk;->dGI:Lirk;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lirk;

    sget-object v1, Lirk;->dGE:Lirk;

    aput-object v1, v0, v2

    sget-object v1, Lirk;->dGF:Lirk;

    aput-object v1, v0, v3

    sget-object v1, Lirk;->dGG:Lirk;

    aput-object v1, v0, v4

    sget-object v1, Lirk;->dGH:Lirk;

    aput-object v1, v0, v5

    sget-object v1, Lirk;->dGI:Lirk;

    aput-object v1, v0, v6

    sput-object v0, Lirk;->dGJ:[Lirk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lirk;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lirk;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lirk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lirk;

    return-object v0
.end method

.method public static values()[Lirk;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lirk;->dGJ:[Lirk;

    invoke-virtual {v0}, [Lirk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lirk;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
.end method
