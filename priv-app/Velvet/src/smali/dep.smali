.class public final Ldep;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bvA:Ljava/util/Map;

.field private final bvy:Landroid/database/DataSetObserver;

.field private final bvz:Ljava/util/List;

.field private cq:Z

.field public mSuggestions:Ldem;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ldeq;

    invoke-direct {v0, p0}, Ldeq;-><init>(Ldep;)V

    iput-object v0, p0, Ldep;->bvy:Landroid/database/DataSetObserver;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldep;->bvz:Ljava/util/List;

    .line 56
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    .line 57
    return-void
.end method

.method private b(Ldex;Lddn;)V
    .locals 2

    .prologue
    .line 183
    iget-boolean v0, p0, Ldep;->cq:Z

    const-string v1, "updateView when not started"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 184
    invoke-virtual {p2}, Lddn;->aaY()Landroid/util/SparseArray;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 189
    invoke-interface {p1, v0}, Ldex;->c(Landroid/util/SparseArray;)V

    .line 191
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lder;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldep;->bvz:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public final a(Ldex;Lddn;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 117
    if-eqz p2, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 118
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_2
    const-string v0, "SuggestionsUi registered twice"

    invoke-static {v1, v0}, Lifv;->d(ZLjava/lang/Object;)V

    .line 120
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    invoke-virtual {p2, v0}, Lddn;->b(Ldem;)V

    .line 123
    iget-boolean v0, p0, Ldep;->cq:Z

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0, p1, p2}, Ldep;->b(Ldex;Lddn;)V

    .line 127
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 116
    goto :goto_0

    :cond_2
    move v0, v2

    .line 117
    goto :goto_1

    :cond_3
    move v1, v2

    .line 118
    goto :goto_2
.end method

.method abo()V
    .locals 3

    .prologue
    .line 169
    iget-boolean v0, p0, Ldep;->cq:Z

    const-string v1, "updateViews when not started"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 170
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 171
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldex;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddn;

    invoke-direct {p0, v1, v0}, Ldep;->b(Ldex;Lddn;)V

    goto :goto_0

    .line 173
    :cond_0
    iget-object v0, p0, Ldep;->bvz:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lder;

    invoke-interface {v0, p0}, Lder;->a(Ldep;)V

    goto :goto_1

    .line 174
    :cond_1
    return-void
.end method

.method public final abp()Z
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    invoke-virtual {v0}, Ldem;->abn()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 210
    invoke-interface {v0}, Ldef;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 211
    const/4 v0, 0x1

    .line 215
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ldem;)V
    .locals 2
    .param p1    # Ldem;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 148
    invoke-static {}, Lenu;->auR()V

    .line 149
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-ne p1, v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_2

    .line 153
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    iget-object v1, p0, Ldep;->bvy:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Ldem;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 156
    :cond_2
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddn;

    .line 157
    invoke-virtual {v0, p1}, Lddn;->b(Ldem;)V

    goto :goto_1

    .line 160
    :cond_3
    iput-object p1, p0, Ldep;->mSuggestions:Ldem;

    .line 162
    iget-boolean v0, p0, Ldep;->cq:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    iget-object v1, p0, Ldep;->bvy:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Ldem;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 164
    invoke-virtual {p0}, Ldep;->abo()V

    goto :goto_0
.end method

.method public final b(Lder;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ldep;->bvz:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 71
    return-void
.end method

.method public final b(Ldex;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    return-void
.end method

.method public final c(Ldex;)V
    .locals 1

    .prologue
    .line 137
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldep;->cq:Z

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddn;

    invoke-direct {p0, p1, v0}, Ldep;->b(Ldex;Lddn;)V

    .line 141
    :cond_0
    return-void
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Ldep;->cq:Z

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldep;->cq:Z

    .line 82
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    iget-object v1, p0, Ldep;->bvy:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Ldem;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 84
    invoke-virtual {p0}, Ldep;->abo()V

    .line 87
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 3

    .prologue
    .line 97
    iget-boolean v0, p0, Ldep;->cq:Z

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldep;->cq:Z

    .line 99
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Ldep;->mSuggestions:Ldem;

    iget-object v1, p0, Ldep;->bvy:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Ldem;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 102
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldep;->mSuggestions:Ldem;

    .line 103
    iget-object v0, p0, Ldep;->bvA:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddn;

    .line 104
    const/4 v2, 0x1

    iput-boolean v2, v0, Lddn;->buh:Z

    goto :goto_0

    .line 107
    :cond_1
    return-void
.end method
