.class public final Lgih;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final cJY:Lchk;

.field public mSearchResult:Lcmq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lchk;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lgih;->cJY:Lchk;

    .line 27
    return-void
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 45
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 47
    iget-object v1, p0, Lgih;->cJY:Lchk;

    invoke-virtual {v1}, Lchk;->Ge()[Ljava/lang/String;

    move-result-object v3

    .line 48
    if-eqz v3, :cond_0

    array-length v1, v3

    if-lez v1, :cond_0

    .line 49
    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 51
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    :cond_0
    iget-object v1, p0, Lgih;->cJY:Lchk;

    invoke-virtual {v1}, Lchk;->FU()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgih;->mSearchResult:Lcmq;

    if-eqz v1, :cond_1

    .line 56
    iget-object v1, p0, Lgih;->mSearchResult:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qt()Lcms;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_1

    iget-object v3, v1, Lcms;->bcx:Ljtg;

    if-eqz v3, :cond_1

    .line 58
    iget-object v1, v1, Lcms;->bcx:Ljtg;

    iget-object v1, v1, Ljtg;->eDo:[Ljava/lang/String;

    .line 59
    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 61
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 65
    :cond_1
    return-object v2
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lgih;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
