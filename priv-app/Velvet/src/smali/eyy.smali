.class public final Leyy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field cmm:Z

.field private final cmn:Ljava/util/Map;

.field private final cmo:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Leyy;->cmn:Ljava/util/Map;

    .line 311
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Leyy;->cmo:Ljava/util/Map;

    return-void
.end method

.method private awL()Ljava/util/Map;
    .locals 6

    .prologue
    .line 365
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 366
    iget-object v0, p0, Leyy;->cmn:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    .line 370
    iget-object v3, v0, Lamk;->aeR:Lamp;

    invoke-virtual {v3}, Lamp;->nu()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 372
    const-string v3, "CalendarMemoryStore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Duplicate calendar entry by hash: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lamk;->aeR:Lamp;

    invoke-virtual {v0}, Lamp;->nu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 376
    :cond_1
    return-object v1
.end method


# virtual methods
.method public final awM()Leyx;
    .locals 8

    .prologue
    .line 409
    const/4 v1, 0x0

    iget-object v0, p0, Leyy;->cmn:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    invoke-virtual {v0}, Lamk;->mY()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v3}, Lamo;->nt()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    iget-object v3, p0, Leyy;->cmo:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lfzy;->a(JLjava/util/Collection;)Lamm;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v4, Lamk;

    invoke-direct {v4}, Lamk;-><init>()V

    invoke-static {v0, v4}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    invoke-virtual {v3}, Lamm;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lamk;->R(Ljava/lang/String;)Lamk;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    iget-object v2, p0, Leyy;->cmn:Ljava/util/Map;

    iget-object v3, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v3}, Lamo;->nh()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 410
    :cond_3
    new-instance v0, Leyx;

    iget-object v1, p0, Leyy;->cmn:Ljava/util/Map;

    invoke-direct {p0}, Leyy;->awL()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Leyy;->cmo:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    iget-boolean v4, p0, Leyy;->cmm:Z

    invoke-direct {v0, v1, v2, v3, v4}, Leyx;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Collection;Z)V

    return-object v0
.end method

.method public final c(Lamk;)Z
    .locals 4

    .prologue
    .line 321
    invoke-static {p1}, Leyx;->b(Lamk;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 329
    :goto_0
    return v0

    .line 323
    :cond_0
    iget-object v0, p0, Leyy;->cmn:Ljava/util/Map;

    iget-object v1, p1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nh()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 325
    const-string v0, "CalendarMemoryStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate calendar entry by provider ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lamk;->aeQ:Lamo;

    invoke-virtual {v2}, Lamo;->nh()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final g(Ljava/lang/Iterable;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 352
    .line 353
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamm;

    .line 354
    invoke-static {v0}, Leyx;->a(Lamm;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_1
    and-int/2addr v0, v1

    move v1, v0

    .line 355
    goto :goto_0

    .line 354
    :cond_0
    iget-object v4, p0, Leyy;->cmo:Ljava/util/Map;

    invoke-virtual {v0}, Lamm;->nb()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string v4, "CalendarMemoryStore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Duplicate calendar info by DB ID: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lamm;->nb()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v0, v2

    goto :goto_1

    .line 356
    :cond_2
    return v1
.end method
