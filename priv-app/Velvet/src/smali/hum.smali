.class public final Lhum;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field bEj:Lhuo;

.field final bbd:Lenw;

.field private bbe:Ljava/lang/String;

.field cq:Z

.field private final mMainThreadExecutor:Ljava/util/concurrent/Executor;

.field private final mSettings:Lhym;

.field private final mVss:Lhhq;


# direct methods
.method public constructor <init>(Lhhq;Lhym;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lhum;->mVss:Lhhq;

    .line 76
    iput-object p2, p0, Lhum;->mSettings:Lhym;

    .line 77
    iput-object p3, p0, Lhum;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    .line 78
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lhum;->bbd:Lenw;

    .line 79
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/shared/speech/HotwordResult;ZZ)V
    .locals 4
    .param p1    # Lcom/google/android/shared/speech/HotwordResult;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 133
    iget-boolean v0, p0, Lhum;->cq:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 137
    iget-object v0, p0, Lhum;->bEj:Lhuo;

    .line 138
    iput-object v3, p0, Lhum;->bEj:Lhuo;

    .line 140
    iget-object v1, p0, Lhum;->mVss:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v1

    iget-object v2, p0, Lhum;->bbe:Ljava/lang/String;

    invoke-interface {v1, v2}, Lgdb;->ms(Ljava/lang/String;)V

    .line 141
    iput-object v3, p0, Lhum;->bbe:Ljava/lang/String;

    .line 142
    const/4 v1, 0x0

    iput-boolean v1, p0, Lhum;->cq:Z

    .line 144
    if-eqz v0, :cond_0

    .line 145
    if-eqz p2, :cond_1

    .line 146
    invoke-interface {v0, p3}, Lhuo;->dd(Z)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    if-eqz p1, :cond_0

    .line 148
    invoke-interface {v0, p1}, Lhuo;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    goto :goto_0
.end method

.method public final a(Lhuo;ZILchk;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    iget-object v0, p0, Lhum;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 96
    iget-object v0, p0, Lhum;->bEj:Lhuo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhum;->bEj:Lhuo;

    if-eq v0, p1, :cond_0

    .line 97
    iput-object p1, p0, Lhum;->bEj:Lhuo;

    .line 98
    const v0, 0x825d7c

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 101
    :cond_0
    iget-boolean v0, p0, Lhum;->cq:Z

    if-nez v0, :cond_1

    .line 102
    iput-object p1, p0, Lhum;->bEj:Lhuo;

    .line 103
    iget-boolean v0, p0, Lhum;->cq:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lhum;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Lhum;->cq:Z

    new-instance v3, Lgmy;

    invoke-direct {v3}, Lgmy;-><init>()V

    iput-boolean v2, v3, Lgmy;->cPF:Z

    iput-boolean v2, v3, Lgmy;->cPG:Z

    iput-boolean v1, v3, Lgmy;->cPH:Z

    iput-boolean v2, v3, Lgmy;->cHB:Z

    const/16 v1, 0x3e80

    iput v1, v3, Lgmy;->cPK:I

    invoke-virtual {v3}, Lgmy;->aHB()Lgmx;

    move-result-object v1

    new-instance v2, Lgnk;

    invoke-direct {v2}, Lgnk;-><init>()V

    iput-object v1, v2, Lgnk;->cPY:Lgmx;

    iput-object v0, v2, Lgnk;->cQb:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Lgnk;->kt(I)Lgnk;

    move-result-object v0

    iput-boolean p2, v0, Lgnk;->cQf:Z

    iput p3, v0, Lgnk;->anC:I

    invoke-virtual {p4}, Lchk;->HN()F

    move-result v1

    iput v1, v0, Lgnk;->bhQ:F

    invoke-virtual {p4}, Lchk;->HK()[F

    move-result-object v1

    iput-object v1, v0, Lgnk;->bhR:[F

    invoke-virtual {p4}, Lchk;->HL()[F

    move-result-object v1

    iput-object v1, v0, Lgnk;->bhS:[F

    invoke-virtual {p4}, Lchk;->HM()[F

    move-result-object v1

    iput-object v1, v0, Lgnk;->bhT:[F

    invoke-virtual {v0}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhum;->bbe:Ljava/lang/String;

    iget-object v1, p0, Lhum;->mVss:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v1

    new-instance v2, Lhun;

    invoke-direct {v2, p0}, Lhun;-><init>(Lhum;)V

    iget-object v3, p0, Lhum;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 105
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 103
    goto :goto_0
.end method

.method public final stop()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lhum;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 113
    iget-boolean v0, p0, Lhum;->cq:Z

    if-eqz v0, :cond_0

    .line 114
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lhum;->a(Lcom/google/android/shared/speech/HotwordResult;ZZ)V

    .line 116
    :cond_0
    return-void
.end method
