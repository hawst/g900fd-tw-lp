.class public final Ljyg;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiD:I

.field private eLI:[Ljava/lang/String;

.field private eLJ:Ljky;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 59
    iput v0, p0, Ljyg;->aez:I

    iput v0, p0, Ljyg;->aiD:I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljyg;->eLI:[Ljava/lang/String;

    iput-object v1, p0, Ljyg;->eLJ:Ljky;

    iput-object v1, p0, Ljyg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyg;->eCz:I

    .line 60
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljyg;->aiD:I

    iget v0, p0, Ljyg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyg;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyg;->eLI:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljyg;->eLI:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljyg;->eLI:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljyg;->eLI:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljyg;->eLJ:Ljky;

    if-nez v0, :cond_4

    new-instance v0, Ljky;

    invoke-direct {v0}, Ljky;-><init>()V

    iput-object v0, p0, Ljyg;->eLJ:Ljky;

    :cond_4
    iget-object v0, p0, Ljyg;->eLJ:Ljky;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 75
    iget v0, p0, Ljyg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 76
    const/4 v0, 0x1

    iget v1, p0, Ljyg;->aiD:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 78
    :cond_0
    iget-object v0, p0, Ljyg;->eLI:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljyg;->eLI:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 79
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljyg;->eLI:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 80
    iget-object v1, p0, Ljyg;->eLI:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 81
    if-eqz v1, :cond_1

    .line 82
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 79
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_2
    iget-object v0, p0, Ljyg;->eLJ:Ljky;

    if-eqz v0, :cond_3

    .line 87
    const/4 v0, 0x3

    iget-object v1, p0, Ljyg;->eLJ:Ljky;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 89
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 90
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 95
    iget v2, p0, Ljyg;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 96
    const/4 v2, 0x1

    iget v3, p0, Ljyg;->aiD:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    :cond_0
    iget-object v2, p0, Ljyg;->eLI:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljyg;->eLI:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 102
    :goto_0
    iget-object v4, p0, Ljyg;->eLI:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 103
    iget-object v4, p0, Ljyg;->eLI:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 104
    if-eqz v4, :cond_1

    .line 105
    add-int/lit8 v3, v3, 0x1

    .line 106
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 102
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_2
    add-int/2addr v0, v2

    .line 111
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 113
    :cond_3
    iget-object v1, p0, Ljyg;->eLJ:Ljky;

    if-eqz v1, :cond_4

    .line 114
    const/4 v1, 0x3

    iget-object v2, p0, Ljyg;->eLJ:Ljky;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_4
    return v0
.end method
