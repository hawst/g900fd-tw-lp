.class public final Litg;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHA:I

.field private dHB:Ljava/lang/String;

.field private dHC:Ljava/lang/String;

.field private dHx:I

.field private dHy:I

.field private dHz:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3498
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3499
    iput v1, p0, Litg;->aez:I

    iput v1, p0, Litg;->dHx:I

    const/4 v0, 0x1

    iput v0, p0, Litg;->dHy:I

    iput v1, p0, Litg;->dHz:I

    iput v1, p0, Litg;->dHA:I

    const-string v0, ""

    iput-object v0, p0, Litg;->dHB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Litg;->dHC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Litg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litg;->eCz:I

    .line 3500
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3355
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litg;->dHx:I

    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litg;->dHy:I

    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litg;->dHz:I

    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litg;->dHA:I

    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Litg;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Litg;->dHB:Ljava/lang/String;

    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Litg;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Litg;->dHC:Ljava/lang/String;

    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Litg;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3518
    iget v0, p0, Litg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3519
    const/4 v0, 0x1

    iget v1, p0, Litg;->dHx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3521
    :cond_0
    iget v0, p0, Litg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3522
    const/4 v0, 0x2

    iget v1, p0, Litg;->dHy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3524
    :cond_1
    iget v0, p0, Litg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3525
    const/4 v0, 0x3

    iget v1, p0, Litg;->dHz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3527
    :cond_2
    iget v0, p0, Litg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 3528
    const/4 v0, 0x4

    iget v1, p0, Litg;->dHA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3530
    :cond_3
    iget v0, p0, Litg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 3531
    const/4 v0, 0x5

    iget-object v1, p0, Litg;->dHB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3533
    :cond_4
    iget v0, p0, Litg;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 3534
    const/4 v0, 0x6

    iget-object v1, p0, Litg;->dHC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3536
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3537
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3541
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3542
    iget v1, p0, Litg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3543
    const/4 v1, 0x1

    iget v2, p0, Litg;->dHx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3546
    :cond_0
    iget v1, p0, Litg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3547
    const/4 v1, 0x2

    iget v2, p0, Litg;->dHy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3550
    :cond_1
    iget v1, p0, Litg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3551
    const/4 v1, 0x3

    iget v2, p0, Litg;->dHz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3554
    :cond_2
    iget v1, p0, Litg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 3555
    const/4 v1, 0x4

    iget v2, p0, Litg;->dHA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3558
    :cond_3
    iget v1, p0, Litg;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 3559
    const/4 v1, 0x5

    iget-object v2, p0, Litg;->dHB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3562
    :cond_4
    iget v1, p0, Litg;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 3563
    const/4 v1, 0x6

    iget-object v2, p0, Litg;->dHC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3566
    :cond_5
    return v0
.end method

.method public final mo(I)Litg;
    .locals 1

    .prologue
    .line 3384
    iput p1, p0, Litg;->dHx:I

    .line 3385
    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litg;->aez:I

    .line 3386
    return-object p0
.end method

.method public final mp(I)Litg;
    .locals 1

    .prologue
    .line 3422
    iput p1, p0, Litg;->dHz:I

    .line 3423
    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litg;->aez:I

    .line 3424
    return-object p0
.end method

.method public final mq(I)Litg;
    .locals 1

    .prologue
    .line 3441
    iput p1, p0, Litg;->dHA:I

    .line 3442
    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Litg;->aez:I

    .line 3443
    return-object p0
.end method

.method public final pF(Ljava/lang/String;)Litg;
    .locals 1

    .prologue
    .line 3460
    if-nez p1, :cond_0

    .line 3461
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3463
    :cond_0
    iput-object p1, p0, Litg;->dHB:Ljava/lang/String;

    .line 3464
    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Litg;->aez:I

    .line 3465
    return-object p0
.end method

.method public final pG(Ljava/lang/String;)Litg;
    .locals 1

    .prologue
    .line 3482
    if-nez p1, :cond_0

    .line 3483
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3485
    :cond_0
    iput-object p1, p0, Litg;->dHC:Ljava/lang/String;

    .line 3486
    iget v0, p0, Litg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Litg;->aez:I

    .line 3487
    return-object p0
.end method
