.class public Lbwu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final aML:I

.field public static final aMM:I

.field public static final aMN:I

.field public static final aMO:I

.field public static final aMP:I

.field private static final aMQ:I

.field private static final aMR:I

.field private static final aMS:I

.field private static final aMT:I

.field private static final aMU:I


# instance fields
.field private final aMV:I

.field private final aMW:I

.field private final aMX:I

.field private final aMY:I

.field private final aMZ:Lesk;

.field private final aNa:I

.field private final aNb:Lesk;

.field private final mActivity:Landroid/app/Activity;

.field private final zj:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const v0, 0x7f11015f

    sput v0, Lbwu;->aML:I

    .line 17
    const v0, 0x7f110158

    sput v0, Lbwu;->aMM:I

    .line 18
    const v0, 0x7f110159

    sput v0, Lbwu;->aMN:I

    .line 19
    const v0, 0x7f11015b

    sput v0, Lbwu;->aMO:I

    .line 21
    const v0, 0x7f110157

    sput v0, Lbwu;->aMP:I

    .line 25
    const v0, 0x7f110154

    sput v0, Lbwu;->aMQ:I

    .line 26
    const v0, 0x7f110155

    sput v0, Lbwu;->aMR:I

    .line 27
    const v0, 0x7f11015d

    sput v0, Lbwu;->aMS:I

    .line 28
    const v0, 0x7f11015e

    sput v0, Lbwu;->aMT:I

    .line 31
    const v0, 0x7f040060

    sput v0, Lbwu;->aMU:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;IIIIILesk;ILesk;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    .line 60
    iput p2, p0, Lbwu;->zj:I

    .line 61
    iput p3, p0, Lbwu;->aMV:I

    .line 62
    iput p4, p0, Lbwu;->aMW:I

    .line 63
    iput v0, p0, Lbwu;->aMX:I

    .line 64
    iput p6, p0, Lbwu;->aMY:I

    .line 65
    iput-object p7, p0, Lbwu;->aMZ:Lesk;

    .line 66
    iput v0, p0, Lbwu;->aNa:I

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lbwu;->aNb:Lesk;

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;IIIILesk;ILesk;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    .line 74
    sget v0, Lbwu;->aMU:I

    iput v0, p0, Lbwu;->zj:I

    .line 75
    iput p2, p0, Lbwu;->aMV:I

    .line 76
    iput p3, p0, Lbwu;->aMW:I

    .line 77
    iput p4, p0, Lbwu;->aMX:I

    .line 78
    iput p5, p0, Lbwu;->aMY:I

    .line 79
    iput-object p6, p0, Lbwu;->aMZ:Lesk;

    .line 80
    iput p7, p0, Lbwu;->aNa:I

    .line 81
    iput-object p8, p0, Lbwu;->aNb:Lesk;

    .line 82
    return-void
.end method

.method private a(ILesk;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 130
    new-instance v1, Lbwv;

    invoke-direct {v1, p0, p2}, Lbwv;-><init>(Lbwu;Lesk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method


# virtual methods
.method public Be()V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    sget v1, Lbwu;->aMQ:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lbwu;->aMV:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 91
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    sget v1, Lbwu;->aMP:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    iget-object v1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lbwu;->aMW:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget v0, p0, Lbwu;->aMX:I

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    sget v1, Lbwu;->aMR:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 97
    iget-object v1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lbwu;->aMX:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    sget v1, Lbwu;->aMS:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 102
    iget-object v1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lbwu;->aMY:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    sget v0, Lbwu;->aMS:I

    iget-object v1, p0, Lbwu;->aMZ:Lesk;

    invoke-direct {p0, v0, v1}, Lbwu;->a(ILesk;)V

    .line 108
    iget v0, p0, Lbwu;->aNa:I

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    sget v1, Lbwu;->aMT:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 110
    iget-object v1, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lbwu;->aNa:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    sget v0, Lbwu;->aMT:I

    iget-object v1, p0, Lbwu;->aNb:Lesk;

    invoke-direct {p0, v0, v1}, Lbwu;->a(ILesk;)V

    .line 115
    :cond_1
    return-void
.end method

.method public final a(ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lbwu;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 122
    return-void
.end method

.method public final getLayoutId()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lbwu;->zj:I

    return v0
.end method
