.class public final Lhvo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final dsl:Lhvr;

.field private final dsm:Ligi;

.field final mSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhym;)V
    .locals 3

    .prologue
    .line 36
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lhvq;

    invoke-direct {v2, v0, v1}, Lhvq;-><init>(Landroid/view/inputmethod/InputMethodManager;Ljava/lang/String;)V

    new-instance v0, Lhvp;

    invoke-direct {v0}, Lhvp;-><init>()V

    invoke-direct {p0, p2, v2, v0}, Lhvo;-><init>(Lhym;Lhvr;Ligi;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lhym;Lhvr;Ligi;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lhvo;->mSettings:Lhym;

    .line 50
    iput-object p2, p0, Lhvo;->dsl:Lhvr;

    .line 51
    iput-object p3, p0, Lhvo;->dsm:Ligi;

    .line 52
    return-void
.end method

.method public static a(Landroid/view/inputmethod/InputMethodManager;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 94
    const/4 v1, 0x0

    .line 95
    invoke-virtual {p0}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 96
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_1
    move-object v1, v0

    .line 99
    goto :goto_0

    .line 100
    :cond_0
    if-nez v1, :cond_1

    .line 101
    const-string v0, "VoiceLanguageSelector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IME not found for package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    :goto_2
    return-object v0

    .line 105
    :cond_1
    invoke-virtual {p0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v3

    .line 106
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 107
    invoke-static {v4}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 108
    :goto_3
    if-ge v2, v4, :cond_3

    .line 113
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodSubtype;

    const-string v5, "bcp47"

    invoke-virtual {v0, v5}, Landroid/view/inputmethod/InputMethodSubtype;->getExtraValueOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_2

    .line 116
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 118
    :cond_2
    const v0, 0x684375

    invoke-static {v0}, Lhwt;->lx(I)V

    goto :goto_4

    :cond_3
    move-object v0, v1

    .line 121
    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final aSR()Ljava/lang/String;
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lhvo;->dsl:Lhvr;

    invoke-interface {v0}, Lhvr;->aSU()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_1

    .line 136
    iget-object v0, p0, Lhvo;->dsl:Lhvr;

    invoke-interface {v0}, Lhvr;->aSS()Ljava/lang/String;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lhvo;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    iget-object v0, p0, Lhvo;->dsm:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lgnq;->a(Ljze;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lhvo;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 160
    :cond_0
    :goto_0
    return-object v0

    .line 148
    :cond_1
    iget-object v0, p0, Lhvo;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v1, p0, Lhvo;->dsl:Lhvr;

    invoke-interface {v1}, Lhvr;->aST()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgnq;->c(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    if-nez v0, :cond_0

    .line 152
    const-string v0, "VoiceLanguageSelector"

    const-string v1, "The subtype of the IME are not aligned with the supported locale"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lhvo;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 158
    const v1, 0x5fb072

    invoke-static {v1}, Lhwt;->lx(I)V

    goto :goto_0
.end method
