.class public final Ljux;
.super Ljsl;
.source "PG"


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private dZn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1964
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1965
    const/4 v0, 0x0

    iput v0, p0, Ljux;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljux;->aeE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljux;->dZn:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljux;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljux;->eCz:I

    .line 1966
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1901
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljux;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljux;->aeE:Ljava/lang/String;

    iget v0, p0, Ljux;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljux;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljux;->dZn:Ljava/lang/String;

    iget v0, p0, Ljux;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljux;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1980
    iget v0, p0, Ljux;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1981
    const/4 v0, 0x1

    iget-object v1, p0, Ljux;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1983
    :cond_0
    iget v0, p0, Ljux;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1984
    const/4 v0, 0x2

    iget-object v1, p0, Ljux;->dZn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1986
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1987
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1991
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1992
    iget v1, p0, Ljux;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1993
    const/4 v1, 0x1

    iget-object v2, p0, Ljux;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1996
    :cond_0
    iget v1, p0, Ljux;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1997
    const/4 v1, 0x2

    iget-object v2, p0, Ljux;->dZn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2000
    :cond_1
    return v0
.end method

.method public final zb(Ljava/lang/String;)Ljux;
    .locals 1

    .prologue
    .line 1926
    if-nez p1, :cond_0

    .line 1927
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1929
    :cond_0
    iput-object p1, p0, Ljux;->aeE:Ljava/lang/String;

    .line 1930
    iget v0, p0, Ljux;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljux;->aez:I

    .line 1931
    return-object p0
.end method

.method public final zc(Ljava/lang/String;)Ljux;
    .locals 1

    .prologue
    .line 1948
    if-nez p1, :cond_0

    .line 1949
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1951
    :cond_0
    iput-object p1, p0, Ljux;->dZn:Ljava/lang/String;

    .line 1952
    iget v0, p0, Ljux;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljux;->aez:I

    .line 1953
    return-object p0
.end method
