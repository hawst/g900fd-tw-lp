.class public final Liam;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# instance fields
.field private synthetic dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwD:Lequ;

    invoke-virtual {v0}, Lequ;->afQ()I

    move-result v0

    .line 84
    iget-object v1, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget v1, v1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwB:I

    if-le v0, v1, :cond_0

    .line 85
    iget-object v1, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v2, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget v2, v2, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwB:I

    add-int/lit8 v2, v2, 0xa

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwB:I

    .line 89
    :goto_0
    iget-object v0, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->invalidate()V

    .line 90
    return-void

    .line 87
    :cond_0
    iget-object v1, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v2, p0, Liam;->dwG:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget v2, v2, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwB:I

    add-int/lit8 v2, v2, -0xa

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwB:I

    goto :goto_0
.end method
