.class public final Lhvi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private atG:Leqo;

.field final dho:Lhvf;

.field private final drV:Lhva;

.field public final drW:Lhve;

.field public final drX:Lhvz;

.field public final drY:Lhuy;

.field public final drZ:Lhup;

.field final dsa:Lhvd;

.field private final dsb:Lhvd;

.field final dsc:Lhvd;

.field private final dsd:Ljava/lang/Runnable;

.field public dse:Z

.field dsf:Z

.field final mSettings:Lhym;

.field final mSoundManager:Lhik;

.field public final mVoiceLanguageSelector:Lhvo;

.field final mVoiceRecognitionHandler:Lhvs;


# direct methods
.method public constructor <init>(Lhvo;Lhva;Leqo;Lhuy;Lhym;Lhik;Lhvf;Lhvz;Lhve;Lhvs;Lhup;Lhvd;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    new-instance v0, Lhvd;

    invoke-direct {v0, v1}, Lhvd;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lhvi;->dsb:Lhvd;

    .line 135
    new-instance v0, Lhvd;

    invoke-direct {v0, v1}, Lhvd;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lhvi;->dsc:Lhvd;

    .line 138
    new-instance v0, Lhvj;

    const-string v1, "Release resources"

    invoke-direct {v0, p0, v1}, Lhvj;-><init>(Lhvi;Ljava/lang/String;)V

    iput-object v0, p0, Lhvi;->dsd:Ljava/lang/Runnable;

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvi;->dsf:Z

    .line 245
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvo;

    iput-object v0, p0, Lhvi;->mVoiceLanguageSelector:Lhvo;

    .line 247
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqo;

    iput-object v0, p0, Lhvi;->atG:Leqo;

    .line 248
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuy;

    iput-object v0, p0, Lhvi;->drY:Lhuy;

    .line 249
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhva;

    iput-object v0, p0, Lhvi;->drV:Lhva;

    .line 250
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhym;

    iput-object v0, p0, Lhvi;->mSettings:Lhym;

    .line 251
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhik;

    iput-object v0, p0, Lhvi;->mSoundManager:Lhik;

    .line 252
    invoke-static {p7}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvf;

    iput-object v0, p0, Lhvi;->dho:Lhvf;

    .line 253
    invoke-static {p8}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvz;

    iput-object v0, p0, Lhvi;->drX:Lhvz;

    .line 254
    invoke-static {p9}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhve;

    iput-object v0, p0, Lhvi;->drW:Lhve;

    .line 255
    iput-object p10, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    .line 256
    iput-object p11, p0, Lhvi;->drZ:Lhup;

    .line 257
    iput-object p12, p0, Lhvi;->dsa:Lhvd;

    .line 258
    return-void
.end method

.method private aSC()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 346
    iget-object v2, p0, Lhvi;->drW:Lhve;

    invoke-interface {v2}, Lhve;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v5

    .line 347
    iget v2, v5, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 349
    and-int/lit16 v2, v2, 0xfff

    .line 351
    const/16 v3, 0x81

    if-ne v2, v3, :cond_1

    move v4, v1

    .line 353
    :goto_0
    const/16 v3, 0xe1

    if-ne v2, v3, :cond_2

    move v3, v1

    .line 355
    :goto_1
    const/16 v6, 0x12

    if-ne v2, v6, :cond_3

    move v2, v1

    .line 358
    :goto_2
    if-nez v4, :cond_0

    if-nez v3, :cond_0

    if-eqz v2, :cond_4

    .line 359
    :cond_0
    const-string v1, "VoiceInputMethodManager"

    const-string v2, "Voice IME is not supported for password input type"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :goto_3
    return v0

    :cond_1
    move v4, v0

    .line 351
    goto :goto_0

    :cond_2
    move v3, v0

    .line 353
    goto :goto_1

    :cond_3
    move v2, v0

    .line 355
    goto :goto_2

    .line 363
    :cond_4
    iget-object v2, v5, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 364
    iget-object v2, v5, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v2, v0

    :goto_4
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 365
    const-string v6, "noMicrophoneKey"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "nm"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 367
    :cond_5
    const-string v1, "VoiceInputMethodManager"

    const-string v2, "Voice IME has been disabled for this field"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 364
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    move v0, v1

    .line 372
    goto :goto_3
.end method

.method private aSD()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 408
    iget-object v0, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    .line 411
    new-array v0, v6, [Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 412
    iget-object v1, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTI()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 413
    iget-object v1, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v0}, Lgnq;->b(Ljze;[Ljava/lang/String;)[Ljzh;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lhvi;->drX:Lhvz;

    invoke-virtual {v1, v2, v0}, Lhvz;->a(Ljava/lang/String;[Ljzh;)V

    .line 417
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    iget-object v1, p0, Lhvi;->mSettings:Lhym;

    iget-object v2, v0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-object v0, v0, Lhvz;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->b(Landroid/content/Context;Lhym;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->op(Ljava/lang/String;)V

    .line 423
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lhvi;->dsa:Lhvd;

    invoke-virtual {v0}, Lhvd;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 420
    iget-object v3, p0, Lhvi;->drX:Lhvz;

    iget-object v4, p0, Lhvi;->mVoiceLanguageSelector:Lhvo;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v4, Lhvo;->dsl:Lhvr;

    invoke-interface {v2}, Lhvr;->aSU()[Ljava/lang/String;

    move-result-object v5

    array-length v2, v5

    if-nez v2, :cond_1

    iget-object v2, v4, Lhvo;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v2

    invoke-static {v2, v0}, Lgnq;->f(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v4

    new-array v2, v6, [Ljzh;

    aput-object v4, v2, v1

    move-object v1, v2

    :goto_1
    invoke-virtual {v3, v0, v1}, Lhvz;->a(Ljava/lang/String;[Ljzh;)V

    goto :goto_0

    :cond_1
    array-length v2, v5

    new-array v2, v2, [Ljzh;

    :goto_2
    array-length v6, v5

    if-ge v1, v6, :cond_2

    iget-object v6, v4, Lhvo;->mSettings:Lhym;

    invoke-virtual {v6}, Lhym;->aER()Ljze;

    move-result-object v6

    aget-object v7, v5, v1

    invoke-static {v6, v7}, Lgnq;->e(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v6

    aput-object v6, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method final aQs()V
    .locals 2

    .prologue
    .line 751
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    const/16 v0, 0x27

    invoke-static {v0}, Lege;->ht(I)V

    .line 752
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    iget-object v0, v0, Lhvz;->dsN:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 753
    iget-boolean v0, p0, Lhvi;->dsf:Z

    if-eqz v0, :cond_0

    .line 754
    invoke-virtual {p0}, Lhvi;->aSM()V

    .line 756
    :cond_0
    return-void
.end method

.method public final aSB()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 261
    const-string v1, "VoiceInputMethodManager"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "#handleStartInputView [active="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lhvi;->dse:Z

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ",keepRecording="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p0, Lhvi;->dsc:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSv()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " ] "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-direct {p0}, Lhvi;->aSC()Z

    move-result v0

    if-nez v0, :cond_2

    .line 265
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "Voice IME cannot be started for this field"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-boolean v0, p0, Lhvi;->dse:Z

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "Switch to unsupported field while VoiceIME active"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    invoke-virtual {p0}, Lhvi;->aSM()V

    .line 343
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 261
    goto :goto_0

    .line 274
    :cond_2
    iget-object v0, p0, Lhvi;->drW:Lhve;

    invoke-interface {v0}, Lhve;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_3

    .line 287
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "Voice IME cannot be started - screen is off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-virtual {p0}, Lhvi;->aSM()V

    goto :goto_1

    .line 292
    :cond_3
    iget-boolean v0, p0, Lhvi;->dse:Z

    if-nez v0, :cond_9

    .line 293
    iget-object v0, p0, Lhvi;->drV:Lhva;

    new-instance v1, Lhvl;

    invoke-direct {v1, p0}, Lhvl;-><init>(Lhvi;)V

    invoke-virtual {v0, v1}, Lhva;->a(Lhvc;)V

    .line 300
    iget-object v10, p0, Lhvi;->drY:Lhuy;

    iget-object v0, p0, Lhvi;->drW:Lhve;

    invoke-interface {v0}, Lhve;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v1, v0, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v8, "voice-ime"

    iget-object v9, v0, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    new-instance v0, Legm;

    const/4 v1, -0x8

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lege;->a(Legm;)V

    :goto_2
    const/16 v0, 0x23

    invoke-static {v0}, Lege;->ht(I)V

    iget-boolean v0, v10, Lhuy;->drH:Z

    if-eqz v0, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, v10, Lhuy;->drI:J

    :goto_3
    iput-boolean v7, v10, Lhuy;->zE:Z

    iput-boolean v2, v10, Lhuy;->drH:Z

    iput-boolean v2, v10, Lhuy;->Kn:Z

    .line 303
    iput-boolean v7, p0, Lhvi;->dse:Z

    .line 304
    iput-boolean v2, p0, Lhvi;->dsf:Z

    .line 306
    iget-object v0, p0, Lhvi;->dsa:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSv()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lhvi;->dsa:Lhvd;

    invoke-virtual {v0}, Lhvd;->getData()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    .line 307
    :cond_4
    iget-object v0, p0, Lhvi;->dsa:Lhvd;

    iget-object v1, p0, Lhvi;->mVoiceLanguageSelector:Lhvo;

    invoke-virtual {v1}, Lhvo;->aSR()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhvd;->aY(Ljava/lang/Object;)V

    .line 310
    :cond_5
    iget-object v0, p0, Lhvi;->dsb:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSv()Z

    move-result v0

    if-nez v0, :cond_8

    .line 315
    iget-object v0, p0, Lhvi;->dsb:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSw()V

    .line 316
    invoke-direct {p0}, Lhvi;->aSD()V

    .line 317
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    invoke-virtual {v0, v2}, Lhvz;->gL(Z)V

    goto/16 :goto_1

    .line 300
    :cond_6
    const-string v0, "voice-ime"

    invoke-static {v0}, Legm;->kS(Ljava/lang/String;)Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    goto :goto_2

    :cond_7
    const/16 v0, 0x24

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_3

    .line 319
    :cond_8
    invoke-virtual {p0}, Lhvi;->aSj()V

    goto/16 :goto_1

    .line 322
    :cond_9
    iget-object v0, p0, Lhvi;->dsc:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSv()Z

    move-result v0

    if-nez v0, :cond_e

    .line 328
    iget-object v0, p0, Lhvi;->dsc:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSw()V

    .line 331
    iget-object v0, p0, Lhvi;->atG:Leqo;

    iget-object v1, p0, Lhvi;->dsd:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 333
    invoke-direct {p0}, Lhvi;->aSD()V

    .line 335
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    iget-object v1, v0, Lhvz;->dsP:Lhwf;

    sget-object v3, Lhwf;->dsV:Lhwf;

    if-ne v1, v3, :cond_a

    invoke-virtual {v0}, Lhvz;->aTa()V

    goto/16 :goto_1

    :cond_a
    iget-object v1, v0, Lhvz;->dsP:Lhwf;

    sget-object v3, Lhwf;->dsT:Lhwf;

    if-ne v1, v3, :cond_b

    invoke-virtual {v0}, Lhvz;->aSZ()V

    goto/16 :goto_1

    :cond_b
    iget-object v1, v0, Lhvz;->dsP:Lhwf;

    sget-object v3, Lhwf;->dsW:Lhwf;

    if-ne v1, v3, :cond_c

    invoke-virtual {v0}, Lhvz;->aTc()V

    goto/16 :goto_1

    :cond_c
    iget-object v1, v0, Lhvz;->dsP:Lhwf;

    sget-object v3, Lhwf;->dsR:Lhwf;

    if-ne v1, v3, :cond_d

    invoke-virtual {v0}, Lhvz;->aSY()V

    goto/16 :goto_1

    :cond_d
    const-string v1, "VoiceInputViewHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Restored into unexpected state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lhvz;->dsP:Lhwf;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v2}, Lhvz;->gL(Z)V

    goto/16 :goto_1

    .line 340
    :cond_e
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#handleStartInputView: unhandled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public final aSE()Lgnj;
    .locals 4

    .prologue
    .line 428
    iget-object v0, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 430
    :goto_0
    iget-object v1, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    iget-object v2, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aTI()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aHP()Z

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lhvs;->a(Ljava/lang/String;Ljava/util/List;Z)Lgnj;

    move-result-object v0

    return-object v0

    .line 428
    :cond_0
    iget-object v0, p0, Lhvi;->dsa:Lhvd;

    invoke-virtual {v0}, Lhvd;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final aSF()V
    .locals 3

    .prologue
    .line 438
    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#handleFinishInput "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    invoke-virtual {p0}, Lhvi;->aSG()V

    .line 441
    return-void
.end method

.method final aSG()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 485
    iget-object v0, p0, Lhvi;->dsc:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSv()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 486
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#releaseResources"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    iget-boolean v0, p0, Lhvi;->dse:Z

    if-eqz v0, :cond_2

    .line 488
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    if-eqz v0, :cond_3

    .line 489
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    iget-boolean v1, v0, Lhuy;->zE:Z

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lhuy;->Kn:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x2a

    invoke-static {v1}, Lege;->ht(I)V

    :cond_0
    const/16 v1, 0x29

    invoke-static {v1}, Lege;->ht(I)V

    invoke-static {}, Legm;->aoz()Legm;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Legm;)V

    :cond_1
    iput-boolean v2, v0, Lhuy;->zE:Z

    .line 495
    :cond_2
    :goto_0
    iput-boolean v2, p0, Lhvi;->dse:Z

    .line 496
    iget-object v0, p0, Lhvi;->drV:Lhva;

    invoke-virtual {v0}, Lhva;->unregister()V

    .line 498
    iget-object v0, p0, Lhvi;->dsa:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSw()V

    .line 503
    iget-object v0, p0, Lhvi;->drW:Lhve;

    invoke-interface {v0}, Lhve;->aSy()V

    .line 505
    invoke-virtual {p0}, Lhvi;->aSH()V

    .line 507
    iget-object v0, p0, Lhvi;->atG:Leqo;

    iget-object v1, p0, Lhvi;->dsd:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 512
    :goto_1
    return-void

    .line 491
    :cond_3
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "onFinishInput - mImeLoggerHelper is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 509
    :cond_4
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#releaseResources - schedule"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    iget-object v0, p0, Lhvi;->atG:Leqo;

    iget-object v1, p0, Lhvi;->dsd:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lhvi;->atG:Leqo;

    iget-object v1, p0, Lhvi;->dsd:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    goto :goto_1
.end method

.method final aSH()V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    if-eqz v0, :cond_1

    .line 520
    iget-object v0, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->aSX()V

    .line 525
    :goto_0
    iget-object v0, p0, Lhvi;->drZ:Lhup;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lhvi;->drZ:Lhup;

    invoke-virtual {v0}, Lhup;->aSl()V

    .line 528
    iget-object v0, p0, Lhvi;->drZ:Lhup;

    invoke-virtual {v0}, Lhup;->reset()V

    .line 530
    :cond_0
    return-void

    .line 522
    :cond_1
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "onFinishInput - mVoiceRecognitionDelegate is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final aSI()V
    .locals 4

    .prologue
    .line 537
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#handleDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    iget-object v0, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->aSX()V

    .line 548
    :cond_0
    iget-object v0, p0, Lhvi;->dsc:Lhvd;

    iget-object v1, v0, Lhvd;->cOx:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lhvd;->drN:J

    invoke-virtual {p0}, Lhvi;->aSG()V

    .line 549
    return-void
.end method

.method public final aSJ()Landroid/view/View;
    .locals 2

    .prologue
    .line 574
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#handleCreateInputView"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    new-instance v1, Lhvm;

    invoke-direct {v1, p0}, Lhvm;-><init>(Lhvi;)V

    invoke-virtual {v0, v1}, Lhvz;->a(Lhwe;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public aSK()V
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    if-eqz v0, :cond_1

    .line 651
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    const/16 v0, 0x2a

    invoke-static {v0}, Lege;->ht(I)V

    .line 654
    :cond_0
    invoke-virtual {p0}, Lhvi;->aSL()V

    .line 655
    invoke-virtual {p0}, Lhvi;->aSM()V

    .line 657
    :cond_1
    return-void
.end method

.method final aSL()V
    .locals 2

    .prologue
    .line 661
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "#stopRecording"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    iget-object v0, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->stopListening()V

    .line 663
    iget-object v0, p0, Lhvi;->drZ:Lhup;

    invoke-virtual {v0}, Lhup;->aSl()V

    .line 664
    return-void
.end method

.method public final aSM()V
    .locals 1

    .prologue
    .line 671
    invoke-static {}, Lenu;->auR()V

    .line 672
    invoke-virtual {p0}, Lhvi;->aSG()V

    .line 673
    iget-object v0, p0, Lhvi;->drW:Lhve;

    invoke-interface {v0}, Lhve;->aSx()V

    .line 674
    return-void
.end method

.method final aSN()V
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Lhvi;->drZ:Lhup;

    invoke-virtual {v0}, Lhup;->aSl()V

    .line 744
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    iget-object v1, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v1}, Lhvs;->aSW()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhvz;->gL(Z)V

    .line 745
    return-void
.end method

.method final aSj()V
    .locals 3

    .prologue
    .line 382
    iget-boolean v0, p0, Lhvi;->dse:Z

    if-nez v0, :cond_0

    .line 396
    :goto_0
    return-void

    .line 388
    :cond_0
    invoke-virtual {p0}, Lhvi;->aSE()Lgnj;

    move-result-object v0

    .line 389
    iget-object v1, p0, Lhvi;->drZ:Lhup;

    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhup;->on(Ljava/lang/String;)V

    .line 391
    invoke-direct {p0}, Lhvi;->aSD()V

    .line 393
    iget-object v1, p0, Lhvi;->drX:Lhvz;

    invoke-virtual {v1}, Lhvz;->aSY()V

    .line 394
    iget-object v1, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v1}, Lhvs;->aSX()V

    .line 395
    iget-object v1, p0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    new-instance v2, Lhvn;

    invoke-direct {v2, p0}, Lhvn;-><init>(Lhvi;)V

    invoke-virtual {v1, v0, v2}, Lhvs;->a(Lgnj;Lhvn;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 444
    const-string v2, "VoiceInputMethodManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#handleConfigurationChanged "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    iget-object v2, p0, Lhvi;->drX:Lhvz;

    iget-object v2, v2, Lhvz;->dsP:Lhwf;

    sget-object v3, Lhwf;->dsU:Lhwf;

    if-ne v2, v3, :cond_3

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 447
    iget-object v2, p0, Lhvi;->dsb:Lhvd;

    invoke-virtual {v2}, Lhvd;->aSw()V

    .line 454
    :cond_0
    iget-object v2, p0, Lhvi;->drX:Lhvz;

    iget-object v2, v2, Lhvz;->dsP:Lhwf;

    sget-object v3, Lhwf;->dsV:Lhwf;

    if-ne v2, v3, :cond_4

    :goto_1
    if-nez v0, :cond_1

    iget-object v0, p0, Lhvi;->drX:Lhvz;

    invoke-virtual {v0}, Lhvz;->aTb()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 455
    :cond_1
    const/16 v0, 0x4b

    invoke-static {v0}, Lege;->ht(I)V

    .line 457
    iget-object v0, p0, Lhvi;->dsc:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSw()V

    .line 459
    :cond_2
    invoke-virtual {p0}, Lhvi;->aSG()V

    .line 460
    return-void

    :cond_3
    move v2, v1

    .line 446
    goto :goto_0

    :cond_4
    move v0, v1

    .line 454
    goto :goto_1
.end method

.method public final gI(Z)V
    .locals 3

    .prologue
    .line 533
    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#handleFinishInputView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    return-void
.end method

.method public final gJ(Z)V
    .locals 3

    .prologue
    .line 564
    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#handleShowWindow["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lhuy;->drH:Z

    .line 568
    :cond_0
    return-void
.end method

.method final l(Leiq;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 717
    iget-object v0, p0, Lhvi;->drZ:Lhup;

    invoke-virtual {v0}, Lhup;->aSk()V

    .line 718
    iget-object v0, p0, Lhvi;->drX:Lhvz;

    invoke-static {p1}, Leno;->e(Leiq;)I

    move-result v1

    sget-object v2, Lhwf;->dsS:Lhwf;

    iput-object v2, v0, Lhvz;->dsP:Lhwf;

    iget-object v2, v0, Lhvz;->dsD:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setKeepScreenOn(Z)V

    const/4 v2, 0x3

    new-array v2, v2, [Landroid/view/View;

    iget-object v3, v0, Lhvz;->dsI:Landroid/widget/ImageView;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, v0, Lhvz;->dsF:Landroid/widget/ImageView;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, v0, Lhvz;->dsM:Landroid/widget/TextView;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lhvz;->c([Landroid/view/View;)V

    iget-object v2, v0, Lhvz;->dsI:Landroid/widget/ImageView;

    const v3, 0x7f020252

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lhvz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v0, Lhvz;->dsM:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 719
    iget-object v0, p0, Lhvi;->drY:Lhuy;

    const/16 v0, 0x26

    invoke-static {v0}, Lege;->ht(I)V

    .line 720
    return-void
.end method
