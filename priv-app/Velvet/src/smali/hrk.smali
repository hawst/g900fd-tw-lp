.class public Lhrk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Leti;


# instance fields
.field private final aOw:Ljava/util/concurrent/Executor;

.field private final dpj:I

.field private final dpk:Lgjq;

.field private final dpl:Ljava/lang/String;

.field private final dpm:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private volatile dpn:Z

.field private volatile dpo:Z

.field private dpp:Ljava/util/Map;

.field private dpq:Landroid/content/BroadcastReceiver;

.field private dpr:Z

.field private final dps:Lesk;

.field private final dpt:Ljava/lang/Runnable;

.field private final dpu:Lesk;

.field private final mContext:Landroid/content/Context;

.field private final mDownloadManager:Landroid/app/DownloadManager;

.field private final mGreco3DataManager:Lgix;

.field private final mSettings:Lhym;


# direct methods
.method protected constructor <init>(Lgix;ILhym;Landroid/app/DownloadManager;Landroid/content/Context;Lgjq;Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-boolean v2, p0, Lhrk;->dpr:Z

    .line 115
    new-instance v0, Lhrl;

    const-string v1, "Finish initialization"

    invoke-direct {v0, p0, v1}, Lhrl;-><init>(Lhrk;Ljava/lang/String;)V

    iput-object v0, p0, Lhrk;->dps:Lesk;

    .line 123
    new-instance v0, Lhrm;

    const-string v1, "Finish init"

    new-array v2, v2, [I

    invoke-direct {v0, p0, v1, v2}, Lhrm;-><init>(Lhrk;Ljava/lang/String;[I)V

    iput-object v0, p0, Lhrk;->dpt:Ljava/lang/Runnable;

    .line 130
    new-instance v0, Lhrn;

    const-string v1, "Dispatch language list changed"

    invoke-direct {v0, p0, v1}, Lhrn;-><init>(Lhrk;Ljava/lang/String;)V

    iput-object v0, p0, Lhrk;->dpu:Lesk;

    .line 142
    iput-object p1, p0, Lhrk;->mGreco3DataManager:Lgix;

    .line 143
    iput p2, p0, Lhrk;->dpj:I

    .line 144
    iput-object p3, p0, Lhrk;->mSettings:Lhym;

    .line 145
    iput-object p4, p0, Lhrk;->mDownloadManager:Landroid/app/DownloadManager;

    .line 146
    iput-object p5, p0, Lhrk;->mContext:Landroid/content/Context;

    .line 147
    iput-object p6, p0, Lhrk;->dpk:Lgjq;

    .line 148
    iput-object p7, p0, Lhrk;->dpl:Ljava/lang/String;

    .line 149
    iput-object p8, p0, Lhrk;->aOw:Ljava/util/concurrent/Executor;

    .line 151
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lhrk;->dpm:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 152
    iget-object v0, p0, Lhrk;->dpk:Lgjq;

    invoke-virtual {v0}, Lgjq;->aGz()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    .line 153
    return-void
.end method

.method public static a(Lgiw;Lhym;Landroid/app/DownloadManager;Landroid/content/Context;Lgjq;Ljava/util/concurrent/Executor;)Lhrk;
    .locals 9

    .prologue
    .line 158
    new-instance v0, Lhrk;

    invoke-virtual {p0}, Lgiw;->aGh()Lgix;

    move-result-object v1

    invoke-virtual {p0}, Lgiw;->aGj()Ligi;

    move-result-object v2

    invoke-interface {v2}, Ligi;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJh()Ljava/lang/String;

    move-result-object v7

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lhrk;-><init>(Lgix;ILhym;Landroid/app/DownloadManager;Landroid/content/Context;Lgjq;Ljava/lang/String;Ljava/util/concurrent/Executor;)V

    .line 162
    invoke-virtual {p4, v0}, Lgjq;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 163
    return-object v0
.end method

.method static synthetic a(Lhrk;Ljzp;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 67
    invoke-static {}, Lenu;->auQ()V

    iget-object v2, p0, Lhrk;->dpp:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "LanguagePackUpdateController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Cannot cancel, no active download ID: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    :try_start_1
    iget-object v0, p0, Lhrk;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v3, 0x1

    new-array v3, v3, [J

    const/4 v6, 0x0

    aput-wide v4, v3, v6

    invoke-virtual {v0, v3}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    :goto_1
    if-eq v0, v7, :cond_1

    :try_start_2
    const-string v1, "LanguagePackUpdateController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "(DownloadManager) Unexpected number of removals: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhrk;->dpk:Lgjq;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgjq;->mU(Ljava/lang/String;)V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v3, "LanguagePackUpdateController"

    const-string v4, "Exception from DownloadManager "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    goto :goto_1
.end method

.method private bK(J)Ljzp;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 581
    .line 582
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 583
    iget-object v1, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_1
    move-object v2, v0

    .line 586
    goto :goto_0

    .line 588
    :cond_0
    if-eqz v2, :cond_1

    .line 589
    iget-object v0, p0, Lhrk;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNd:[Ljzp;

    invoke-static {v2, v0}, Lgkb;->a(Ljava/lang/String;[Ljzp;)Ljzp;

    move-result-object v3

    .line 594
    :goto_2
    return-object v3

    .line 593
    :cond_1
    const-string v0, "LanguagePackUpdateController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Download ID not found in active set :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljzp;Z)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 396
    invoke-static {}, Lenu;->auQ()V

    .line 397
    iget-object v3, p1, Ljzp;->eOd:[I

    array-length v4, v3

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Language pack declares no format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aget v4, v3, v4

    sget-object v5, Lgiw;->cLG:[I

    array-length v6, v5

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_1

    aget v7, v5, v3

    if-eq v4, v7, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incompatible language pack: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_2
    iget-object v3, p0, Lhrk;->dpq:Landroid/content/BroadcastReceiver;

    if-nez v3, :cond_3

    new-instance v3, Lhro;

    invoke-direct {v3, p0}, Lhro;-><init>(Lhrk;)V

    iput-object v3, p0, Lhrk;->dpq:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lhrk;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lhrk;->dpq:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v3}, Lhrk;->ah(Landroid/content/Intent;)V

    .line 400
    :cond_3
    iget-boolean v3, p0, Lhrk;->dpr:Z

    if-eqz v3, :cond_4

    .line 401
    const-string v1, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skipping download (storage low): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :goto_1
    return v0

    .line 405
    :cond_4
    iget-object v6, p0, Lhrk;->dpp:Ljava/util/Map;

    monitor-enter v6

    .line 407
    :try_start_0
    iget-object v3, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 408
    const-string v0, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skipping download (already active): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    monitor-exit v6

    move v0, v1

    goto :goto_1

    .line 414
    :cond_5
    const-wide/16 v4, 0x0

    .line 423
    invoke-virtual {p1}, Ljzp;->bxk()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_6

    .line 426
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lhrk;->c(Ljzp;Z)Landroid/app/DownloadManager$Request;

    move-result-object v3

    .line 427
    iget-object v7, p0, Lhrk;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v7, v3}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    move v1, v2

    .line 438
    :goto_2
    if-eqz v1, :cond_7

    .line 439
    :try_start_2
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    iget-object v0, p0, Lhrk;->dpk:Lgjq;

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v5}, Lgjq;->q(Ljava/lang/String;J)V

    .line 441
    monitor-exit v6

    move v0, v2

    goto :goto_1

    .line 429
    :catch_0
    move-exception v3

    .line 432
    const-string v7, "LanguagePackUpdateController"

    const-string v8, "Exception from DownloadManager"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 445
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 435
    :cond_6
    :try_start_3
    const-string v3, "LanguagePackUpdateController"

    const-string v7, "Language pack has no download URL, abort."

    invoke-static {v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 443
    :cond_7
    monitor-exit v6

    goto :goto_1
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 770
    const-string v0, "LanguagePackUpdateController state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 771
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 772
    const-string v0, "Data manager not initialized."

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 774
    :cond_1
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v1

    .line 775
    const-string v0, "Installed languages"

    invoke-virtual {v1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 776
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->aGp()Ljava/util/Map;

    move-result-object v2

    .line 777
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 778
    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lhrk;->mP(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 779
    invoke-virtual {v0}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const-string v4, "downloaded"

    invoke-virtual {v0, v4, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_1

    .line 782
    :cond_2
    invoke-virtual {v0}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const-string v4, "pre-installed"

    invoke-virtual {v0, v4, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_1

    .line 786
    :cond_3
    iget-object v0, p0, Lhrk;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTA()Ljze;

    move-result-object v0

    .line 787
    if-nez v0, :cond_4

    .line 788
    const-string v0, "All compatible languages: Config not ready"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_0

    .line 790
    :cond_4
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v1

    .line 791
    const-string v3, "All compatible languages"

    invoke-virtual {v1, v3}, Letj;->lt(Ljava/lang/String;)V

    .line 792
    iget-object v0, v0, Ljze;->eNd:[Ljzp;

    sget-object v3, Lgiw;->cLG:[I

    iget v4, p0, Lhrk;->dpj:I

    invoke-static {v2, v0, v3, v4}, Lgkb;->a(Ljava/util/Map;[Ljzp;[II)Ljava/util/Map;

    move-result-object v0

    .line 796
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 797
    invoke-virtual {v0}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Lhrs;)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lhrk;->dpm:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    return-void
.end method

.method public final aGp()Ljava/util/Map;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->aGp()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final aRH()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lhrk;->aOw:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lhrk;->dpt:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 186
    return-void
.end method

.method final aRI()V
    .locals 1

    .prologue
    .line 190
    invoke-virtual {p0}, Lhrk;->aRJ()V

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrk;->dpo:Z

    .line 192
    invoke-virtual {p0}, Lhrk;->aRL()V

    .line 193
    return-void
.end method

.method public final aRJ()V
    .locals 12

    .prologue
    .line 213
    invoke-static {}, Lenu;->auQ()V

    .line 215
    const/4 v2, 0x0

    .line 217
    iget-object v7, p0, Lhrk;->dpp:Ljava/util/Map;

    monitor-enter v7

    .line 218
    :try_start_0
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 220
    array-length v8, v0

    const/4 v1, 0x0

    move v6, v1

    :goto_0
    if-ge v6, v8, :cond_7

    aget-object v9, v0, v6

    .line 222
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    const-string v1, "LanguagePackUpdateController"

    const-string v3, "Cannot find download for empty ID."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v1, v2

    .line 220
    :goto_1
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v1

    goto :goto_0

    .line 227
    :cond_0
    new-instance v3, Landroid/app/DownloadManager$Query;

    invoke-direct {v3}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 228
    const/4 v1, 0x1

    new-array v4, v1, [J

    const/4 v5, 0x0

    iget-object v1, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    aput-wide v10, v4, v5

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    const/4 v1, 0x0

    .line 231
    :try_start_1
    iget-object v4, p0, Lhrk;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v4, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 232
    if-nez v3, :cond_1

    .line 233
    :try_start_2
    const-string v1, "LanguagePackUpdateController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DownloadManager\'s underlying cursor was null. Can\'t clean up "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 245
    :try_start_3
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v1, v2

    goto :goto_1

    .line 237
    :cond_1
    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 238
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_2

    .line 239
    const-string v1, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "DownloadManager query failed for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    const/4 v1, 0x1

    .line 241
    invoke-virtual {p0, v9}, Lhrk;->mU(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 245
    :try_start_5
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    .line 245
    :cond_2
    :try_start_6
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V

    .line 249
    const/4 v4, 0x0

    .line 251
    const-string v1, "-v"

    invoke-virtual {v9, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 252
    if-lez v5, :cond_3

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-ne v5, v1, :cond_4

    .line 253
    :cond_3
    const-string v4, "Malformed language pack ID."

    .line 255
    :cond_4
    const/4 v3, 0x0

    .line 256
    const/4 v1, 0x0

    .line 257
    if-nez v4, :cond_c

    .line 258
    const/4 v1, 0x0

    invoke-virtual {v9, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 259
    add-int/lit8 v5, v5, 0x2

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v5, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v5

    .line 261
    :try_start_7
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v3

    move v5, v3

    move-object v3, v4

    move-object v4, v1

    .line 269
    :goto_2
    if-nez v3, :cond_b

    .line 270
    :try_start_8
    invoke-virtual {p0}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljzp;

    .line 271
    if-nez v1, :cond_5

    .line 272
    const-string v1, "No compatible language pack."

    .line 279
    :goto_3
    if-eqz v1, :cond_a

    .line 280
    const-string v2, "LanguagePackUpdateController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Init\'d with bad active download "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const/4 v1, 0x1

    .line 282
    invoke-virtual {p0, v9}, Lhrk;->mU(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 245
    :catchall_1
    move-exception v0

    :goto_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    .line 263
    :catch_0
    move-exception v4

    const-string v4, "Malformed language pack ID version."

    move v5, v3

    move-object v3, v4

    move-object v4, v1

    goto :goto_2

    .line 273
    :cond_5
    invoke-virtual {v1}, Ljzp;->getVersion()I

    move-result v10

    if-le v10, v5, :cond_6

    .line 274
    const-string v1, "More recent version available."

    goto :goto_3

    .line 275
    :cond_6
    invoke-virtual {v1}, Ljzp;->getVersion()I

    move-result v1

    if-ne v1, v5, :cond_b

    invoke-virtual {p0, v4}, Lhrk;->oj(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 276
    const-string v1, "Already installed."

    goto :goto_3

    .line 286
    :cond_7
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 287
    iget-object v0, p0, Lhrk;->mContext:Landroid/content/Context;

    const-string v1, "download_cache"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_8

    .line 290
    invoke-static {v0}, Lhrb;->t(Ljava/io/File;)Z

    .line 293
    :cond_8
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 295
    if-eqz v2, :cond_9

    .line 296
    const v0, 0x9abde2

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 298
    :cond_9
    return-void

    .line 245
    :catchall_2
    move-exception v0

    move-object v1, v3

    goto :goto_4

    :cond_a
    move v1, v2

    goto/16 :goto_1

    :cond_b
    move-object v1, v3

    goto :goto_3

    :cond_c
    move v5, v3

    move-object v3, v4

    move-object v4, v1

    goto/16 :goto_2
.end method

.method public final aRK()Ljava/util/Map;
    .locals 4

    .prologue
    .line 314
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->aGp()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lhrk;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    iget-object v1, v1, Ljze;->eNd:[Ljzp;

    sget-object v2, Lgiw;->cLG:[I

    iget v3, p0, Lhrk;->dpj:I

    invoke-static {v0, v1, v2, v3}, Lgkb;->a(Ljava/util/Map;[Ljzp;[II)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final aRL()V
    .locals 3

    .prologue
    .line 695
    iget-boolean v0, p0, Lhrk;->dpo:Z

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lhrk;->dpm:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrs;

    .line 697
    invoke-interface {v0}, Lhrs;->aRM()V

    goto :goto_0

    .line 700
    :cond_0
    const-string v0, "LanguagePackUpdateController"

    const-string v1, "dispatchLanguageListChanged(): Not initialized."

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 702
    :cond_1
    return-void
.end method

.method final ah(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 378
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 379
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 380
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrk;->dpr:Z

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhrk;->dpr:Z

    goto :goto_0
.end method

.method public final b(Lhrs;)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lhrk;->dpm:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 209
    return-void
.end method

.method public final b(Ljzp;Z)V
    .locals 3

    .prologue
    .line 449
    iget-object v0, p0, Lhrk;->aOw:Ljava/util/concurrent/Executor;

    new-instance v1, Lhrr;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lhrr;-><init>(Lhrk;Ljzp;ZB)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 450
    return-void
.end method

.method public final bJ(J)Lhrq;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 530
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 531
    new-array v2, v4, [J

    const/4 v3, 0x0

    aput-wide p1, v2, v3

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 535
    :try_start_0
    iget-object v2, p0, Lhrk;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v2, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 536
    if-nez v6, :cond_0

    .line 537
    :try_start_1
    const-string v1, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DownloadManager\'s underlying cursor was null. Can\'t get DownloadInfo for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 540
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    .line 576
    :goto_0
    return-object v0

    .line 542
    :cond_0
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 543
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eq v1, v4, :cond_1

    .line 544
    const-string v1, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Querying download manager failed for ID :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 545
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 548
    :cond_1
    :try_start_3
    invoke-direct {p0, p1, p2}, Lhrk;->bK(J)Ljzp;

    move-result-object v2

    .line 549
    if-eqz v2, :cond_2

    .line 551
    new-instance v0, Lhrq;

    const-string v1, "local_filename"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "status"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lhrq;-><init>(Ljava/lang/String;Ljzp;IJ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 573
    :cond_2
    invoke-static {v6}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method protected c(Ljzp;Z)Landroid/app/DownloadManager$Request;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 599
    invoke-virtual {p0, p1, p2}, Lhrk;->d(Ljzp;Z)Landroid/net/Uri;

    move-result-object v0

    .line 600
    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-direct {v1, v0}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 607
    invoke-virtual {v1, v5}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    .line 608
    invoke-virtual {p0, p2}, Lhrk;->gG(Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    .line 609
    invoke-virtual {v1, v5}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 610
    iget-object v0, p0, Lhrk;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-virtual {p1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 612
    iget-object v2, p0, Lhrk;->mContext:Landroid/content/Context;

    const v3, 0x7f0a08e8

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 614
    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 615
    iget-object v0, p0, Lhrk;->mContext:Landroid/content/Context;

    const v2, 0x7f0a08e9

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 617
    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 625
    :try_start_0
    invoke-static {p1}, Lgkb;->b(Ljzp;)Ljava/lang/String;

    move-result-object v0

    .line 626
    iget-object v2, p0, Lhrk;->mContext:Landroid/content/Context;

    const-string v3, "download_cache"

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 633
    :goto_0
    return-object v1

    .line 627
    :catch_0
    move-exception v0

    .line 628
    const-string v2, "LanguagePackUpdateController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error from #setDestinationInExternalFilesDir :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 629
    :catch_1
    move-exception v0

    .line 630
    const-string v2, "LanguagePackUpdateController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error from #setDestinationInExternalFilesDir :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final cV()V
    .locals 2

    .prologue
    .line 171
    iget-boolean v0, p0, Lhrk;->dpo:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhrk;->dpn:Z

    if-eqz v0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrk;->dpn:Z

    .line 176
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    invoke-virtual {p0}, Lhrk;->aRH()V

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    iget-object v1, p0, Lhrk;->dps:Lesk;

    invoke-virtual {v0, v1}, Lgix;->h(Lesk;)V

    goto :goto_0
.end method

.method public final d(Ljzp;Z)Landroid/net/Uri;
    .locals 4

    .prologue
    const/16 v3, 0x3b

    .line 644
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "av"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhrk;->dpl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "f:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 648
    invoke-virtual {p1}, Ljzp;->getVersion()I

    move-result v0

    .line 649
    const-string v2, "tv:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 651
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->aGp()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 652
    if-nez v0, :cond_1

    const/4 v0, -0x1

    .line 653
    :goto_1
    const-string v2, "pv:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 655
    iget-object v0, p0, Lhrk;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUs()I

    move-result v0

    .line 656
    const-string v2, "s:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 658
    invoke-virtual {p1}, Ljzp;->bxj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "extraforlog"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 644
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 652
    :cond_1
    invoke-virtual {v0}, Ljzp;->getVersion()I

    move-result v0

    goto :goto_1
.end method

.method public final f(Ljzp;)Ljzp;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 333
    invoke-virtual {p0}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 334
    if-nez v0, :cond_2

    .line 335
    const-string v0, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to upgrade "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but no compatible language packs found."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    sget-object v0, Lgiw;->cLG:[I

    iget v2, p0, Lhrk;->dpj:I

    invoke-static {p1, v0, v2}, Lgkb;->a(Ljzp;[II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    const-string v0, "LanguagePackUpdateController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not itself compatible."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_0
    const v0, 0xa8465c

    invoke-static {v0}, Lhwt;->lx(I)V

    move-object v0, v1

    .line 344
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0}, Ljzp;->getVersion()I

    move-result v2

    invoke-virtual {p1}, Ljzp;->getVersion()I

    move-result v3

    if-gt v2, v3, :cond_1

    move-object v0, v1

    goto :goto_0
.end method

.method public final g(Ljzp;)Z
    .locals 2

    .prologue
    .line 348
    iget-object v1, p0, Lhrk;->dpp:Ljava/util/Map;

    monitor-enter v1

    .line 349
    :try_start_0
    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhrk;->ok(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final gG(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 638
    iget-object v1, p0, Lhrk;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUs()I

    move-result v1

    .line 639
    if-nez p1, :cond_0

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Ljzp;)V
    .locals 3

    .prologue
    .line 454
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lhrk;->dpu:Lesk;

    invoke-virtual {v0, p1, v1, v2}, Lgix;->a(Ljzp;Ljava/util/concurrent/Executor;Lesk;)V

    .line 455
    return-void
.end method

.method public final i(Ljzp;)V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lhrk;->aOw:Ljava/util/concurrent/Executor;

    new-instance v1, Lhrp;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lhrp;-><init>(Lhrk;Ljzp;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 507
    return-void
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lhrk;->dpo:Z

    return v0
.end method

.method public final mO(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0, p1}, Lgix;->mO(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final mP(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0, p1}, Lgix;->mP(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final mU(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 517
    iget-object v1, p0, Lhrk;->dpp:Ljava/util/Map;

    monitor-enter v1

    .line 518
    :try_start_0
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    iget-object v0, p0, Lhrk;->dpk:Lgjq;

    invoke-virtual {v0, p1}, Lgjq;->mU(Ljava/lang/String;)V

    .line 520
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final oj(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lhrk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->aGp()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final ok(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lhrk;->dpp:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final ol(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 676
    const/4 v1, 0x0

    .line 677
    invoke-virtual {p0}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 678
    invoke-virtual {v0}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 683
    :cond_1
    if-eqz v1, :cond_2

    .line 684
    iget-object v0, p0, Lhrk;->dpm:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrs;

    .line 685
    invoke-interface {v0, v1}, Lhrs;->j(Ljzp;)V

    goto :goto_0

    .line 688
    :cond_2
    const-string v0, "LanguagePackUpdateController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed download for unknown language pack "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    :cond_3
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 665
    const-string v0, "g3_active_downloads"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    invoke-virtual {p0}, Lhrk;->aRL()V

    .line 672
    :cond_0
    return-void
.end method
