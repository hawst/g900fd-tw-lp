.class public Lcjt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcoc;
.implements Lddc;
.implements Ldkt;
.implements Leti;


# static fields
.field private static final aZg:Lijp;

.field private static final aZh:Ldyl;


# instance fields
.field private aOw:Ljava/util/concurrent/Executor;

.field private aUe:Lcfc;

.field private final aZi:Landroid/database/DataSetObserver;

.field private aZj:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private aZk:Ldpe;

.field private aZl:Lclq;

.field private aZm:Lcmt;

.field private aZn:Z

.field private aZo:Ldah;

.field private aZp:Ldah;

.field private aZq:Z

.field private aZr:Lbyf;

.field aZs:Z

.field private anR:Z

.field private cq:Z

.field private final mActionWorker:Ldnm;

.field private final mAudioWorker:Ldnz;

.field private final mConfig:Lcjs;

.field final mContext:Landroid/content/Context;

.field private final mCookiesLock:Ldkq;

.field final mCoreServices:Lcfo;

.field private final mCorpora:Lcfu;

.field final mEventBus:Ldda;

.field private final mExternalQueryWorker:Ldoi;

.field private final mFactory:Lgpu;

.field private final mGsaConfig:Lchk;

.field final mHotwordState:Ldby;

.field private final mHotwordWorker:Ldoj;

.field private final mLocationWorker:Ldor;

.field private final mMessageManager:Lbxy;

.field private final mPumpkinWorker:Ldou;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mScreenStateHelper:Ldmh;

.field private mSearchResultFetcher:Lcmw;

.field private mService:Ldao;

.field private final mServiceWorker:Ldpb;

.field private final mSettings:Lcke;

.field private final mSettingsUtil:Lbyl;

.field private mSuggestionsPresenter:Ldfl;

.field private final mTtsWorker:Ldpg;

.field private mUrlHelper:Lcpn;

.field private mVoiceSearchController:Lhlf;

.field final mVoiceSearchServices:Lhhq;

.field private final mWebViewWorker:Ldpk;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 150
    const-string v0, "use_google_com"

    const-string v1, "search_domain"

    const-string v2, "debug_search_scheme_override"

    const-string v3, "debug_search_domain_override"

    const-string v4, "debug_js_injection_enabled"

    const-string v5, "debug_js_server_address"

    const/16 v6, 0xb

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "search_domain_scheme"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "search_domain_country_code"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "google_account"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "signed_out"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "personalized_search_bool"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "personalized_search"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "safe_search"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "web_corpora_config"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "gservices_overrides"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string v8, "webview_logged_in_account"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "webview_logged_in_domain"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lijp;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcjt;->aZg:Lijp;

    .line 169
    new-instance v0, Leec;

    invoke-direct {v0}, Leec;-><init>()V

    sput-object v0, Lcjt;->aZh:Ldyl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldao;Lcfo;Lhhq;Lgpu;Leqo;Ljava/util/concurrent/Executor;Ldaz;Leqp;Lbwo;)V
    .locals 5

    .prologue
    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Lcju;

    invoke-direct {v0, p0}, Lcju;-><init>(Lcjt;)V

    iput-object v0, p0, Lcjt;->aZi:Landroid/database/DataSetObserver;

    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcjt;->aZq:Z

    .line 278
    iput-object p1, p0, Lcjt;->mContext:Landroid/content/Context;

    .line 279
    iput-object p3, p0, Lcjt;->mCoreServices:Lcfo;

    .line 280
    iput-object p4, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    .line 281
    iput-object p5, p0, Lcjt;->mFactory:Lgpu;

    .line 282
    iput-object p7, p0, Lcjt;->aOw:Ljava/util/concurrent/Executor;

    .line 284
    iput-object p2, p0, Lcjt;->mService:Ldao;

    .line 286
    invoke-virtual {p3}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mSettings:Lcke;

    .line 287
    new-instance v0, Lbyl;

    invoke-direct {v0}, Lbyl;-><init>()V

    iput-object v0, p0, Lcjt;->mSettingsUtil:Lbyl;

    .line 288
    invoke-virtual {p3}, Lcfo;->DU()Lcfu;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mCorpora:Lcfu;

    .line 289
    invoke-virtual {p3}, Lcfo;->DD()Lcjs;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mConfig:Lcjs;

    .line 290
    invoke-virtual {p3}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mGsaConfig:Lchk;

    .line 291
    invoke-virtual {p3}, Lcfo;->DI()Lcpn;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mUrlHelper:Lcpn;

    .line 292
    invoke-virtual {p3}, Lcfo;->DK()Ldkq;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mCookiesLock:Ldkq;

    .line 293
    invoke-virtual {p5}, Lgpu;->aJx()Ldda;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mEventBus:Ldda;

    .line 294
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 295
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aal()Ldby;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mHotwordState:Ldby;

    .line 296
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    .line 297
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJM()Ldmh;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mScreenStateHelper:Ldmh;

    .line 298
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    iget-object v2, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v0, v1, v2}, Lgpu;->a(Ldda;Leoj;)Ldnm;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mActionWorker:Ldnm;

    .line 299
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, v1}, Lgpu;->i(Ldda;)Ldnz;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mAudioWorker:Ldnz;

    .line 300
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, v1}, Lgpu;->j(Ldda;)Ldou;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mPumpkinWorker:Ldou;

    .line 301
    iget-object v1, p0, Lcjt;->mFactory:Lgpu;

    iget-object v2, p0, Lcjt;->mEventBus:Ldda;

    iget-object v3, p0, Lcjt;->mAudioWorker:Ldnz;

    new-instance v4, Ldlu;

    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcgg;->aVv:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {v4, v0}, Ldlu;-><init>(Z)V

    iget-object v0, p0, Lcjt;->mScreenStateHelper:Ldmh;

    invoke-virtual {v1, v2, v3, v4, v0}, Lgpu;->a(Ldda;Ldnz;Ldlu;Ldmh;)Ldoj;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    .line 305
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, v1}, Lgpu;->k(Ldda;)Ldor;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mLocationWorker:Ldor;

    .line 306
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    iget-object v2, p0, Lcjt;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0, v1, v2, p8, p9}, Lgpu;->a(Ldda;Ldmh;Ldaz;Leqp;)Ldpb;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mServiceWorker:Ldpb;

    .line 308
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, v1}, Lgpu;->h(Ldda;)Ldpg;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mTtsWorker:Ldpg;

    .line 309
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    iget-object v2, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v0, v1, v2, p9}, Lgpu;->a(Ldda;Ldao;Leqp;)Ldoi;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mExternalQueryWorker:Ldoi;

    .line 311
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    iget-object v2, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v0, p0, v1, v2}, Lgpu;->a(Lcjt;Ldda;Leoj;)Ldpk;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    .line 312
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, v1}, Lcfo;->a(Ldda;)Lbxy;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mMessageManager:Lbxy;

    .line 316
    iget-object v0, p0, Lcjt;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    iget-object v2, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DD()Lcjs;

    move-result-object v2

    iget-object v3, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DO()Lgpp;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lgqo;->a(Landroid/content/Context;Lcke;Lcjs;Lgpp;)V

    .line 318
    invoke-virtual {p3}, Lcfo;->BL()Lchk;

    move-result-object v0

    new-instance v1, Lcka;

    invoke-direct {v1, p0}, Lcka;-><init>(Lcjt;)V

    invoke-virtual {v0, v1}, Lchk;->a(Lchm;)V

    .line 321
    return-void

    .line 301
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Bm()Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    return-object v0
.end method

.method private MT()Lhlf;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcjt;->mVoiceSearchController:Lhlf;

    if-nez v0, :cond_0

    .line 344
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJE()Lhlf;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mVoiceSearchController:Lhlf;

    .line 347
    :cond_0
    iget-object v0, p0, Lcjt;->mVoiceSearchController:Lhlf;

    return-object v0
.end method

.method private MU()V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    invoke-virtual {v0}, Ldcw;->Zs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPj()Lice;

    move-result-object v0

    invoke-virtual {v0}, Lice;->stop()V

    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPm()Lhix;

    move-result-object v0

    invoke-virtual {v0}, Lhix;->aPL()V

    .line 413
    :cond_0
    return-void
.end method

.method private Nd()V
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Nd()V

    .line 1064
    return-void
.end method

.method private Nh()V
    .locals 1

    .prologue
    .line 1130
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    if-eqz v0, :cond_0

    .line 1131
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    invoke-virtual {v0}, Ldfl;->aaV()V

    .line 1133
    :cond_0
    invoke-virtual {p0}, Lcjt;->Ng()V

    .line 1136
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xn()V

    .line 1137
    return-void
.end method

.method static synthetic a(Lcjt;)Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Lcjt;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcjt;)Lcke;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcjt;->mSettings:Lcke;

    return-object v0
.end method

.method private b(Lbwp;)V
    .locals 4

    .prologue
    .line 1634
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcgg;->aVv:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1635
    const-string v0, "SearchController"

    const-string v1, "Notification announcement but e100 not enabled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1637
    :cond_0
    invoke-virtual {p1}, Lbwp;->AZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1638
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    invoke-virtual {v0}, Lhik;->aPI()V

    .line 1651
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1657
    :cond_1
    :goto_0
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    iget-object v1, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lbwp;->a(Lcky;Lemp;)V

    .line 1659
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {p1}, Lbwp;->Ba()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 1660
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 1686
    const/16 v0, 0x1fff

    return v0
.end method

.method public final MR()V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    invoke-virtual {v0}, Lcfc;->requestCancelPrerender()V

    .line 332
    :cond_0
    return-void
.end method

.method public final MS()Lcfc;
    .locals 4

    .prologue
    .line 335
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    if-nez v0, :cond_0

    .line 336
    new-instance v0, Lcfc;

    iget-object v1, p0, Lcjt;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcjt;->mGsaConfig:Lchk;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v3

    iget-object v3, v3, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcfc;-><init>(Landroid/content/Context;Lchk;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcjt;->aUe:Lcfc;

    .line 339
    :cond_0
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    return-object v0
.end method

.method public final MV()Lcmw;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 701
    iget-object v0, p0, Lcjt;->mSearchResultFetcher:Lcmw;

    if-nez v0, :cond_0

    .line 702
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    invoke-virtual {p0}, Lcjt;->MX()Lcmt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgpu;->a(Lcmt;)Lcmw;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mSearchResultFetcher:Lcmw;

    .line 704
    :cond_0
    iget-object v0, p0, Lcjt;->mSearchResultFetcher:Lcmw;

    return-object v0
.end method

.method public final MW()Ldfl;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 709
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    if-nez v0, :cond_0

    .line 710
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    invoke-virtual {v0, p0}, Lgpu;->c(Lcjt;)Ldfl;

    move-result-object v0

    iput-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    .line 712
    :cond_0
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    return-object v0
.end method

.method public final MX()Lcmt;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 719
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    if-nez v0, :cond_0

    .line 720
    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJF()Lcmt;

    move-result-object v0

    iput-object v0, p0, Lcjt;->aZm:Lcmt;

    .line 722
    :cond_0
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    return-object v0
.end method

.method final MY()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 727
    invoke-virtual {p0}, Lcjt;->Nj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->dk(Z)V

    .line 734
    iget-object v0, p0, Lcjt;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->d(Ldkt;)V

    .line 737
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    .line 744
    :goto_0
    return-void

    .line 739
    :cond_0
    iget-object v0, p0, Lcjt;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->c(Ldkt;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 740
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->dk(Z)V

    goto :goto_0

    .line 742
    :cond_1
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->dk(Z)V

    goto :goto_0
.end method

.method public final MZ()V
    .locals 4

    .prologue
    .line 752
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->dk(Z)V

    .line 753
    iget-object v0, p0, Lcjt;->mSettings:Lcke;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lcke;->V(J)V

    .line 754
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    .line 756
    invoke-direct {p0}, Lcjt;->Nh()V

    .line 757
    return-void
.end method

.method public final Na()V
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->dk(Z)V

    .line 767
    return-void
.end method

.method public final Nb()Ldah;
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    return-object v0
.end method

.method public final Nc()V
    .locals 1

    .prologue
    .line 990
    iget-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WL()V

    .line 991
    return-void
.end method

.method public final Ne()Ldyl;
    .locals 2

    .prologue
    .line 1093
    const/4 v0, 0x0

    .line 1094
    iget-object v1, p0, Lcjt;->aZo:Ldah;

    if-eqz v1, :cond_0

    .line 1095
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->Ne()Ldyl;

    move-result-object v0

    .line 1097
    :cond_0
    if-nez v0, :cond_1

    sget-object v0, Lcjt;->aZh:Ldyl;

    :cond_1
    return-object v0
.end method

.method public final Nf()Ldda;
    .locals 1

    .prologue
    .line 1106
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    return-object v0
.end method

.method public final Ng()V
    .locals 1

    .prologue
    .line 1124
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    if-eqz v0, :cond_0

    .line 1125
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    invoke-virtual {v0}, Lcmt;->clear()V

    .line 1127
    :cond_0
    return-void
.end method

.method public final Ni()V
    .locals 1

    .prologue
    .line 1188
    iget-boolean v0, p0, Lcjt;->anR:Z

    if-eqz v0, :cond_0

    .line 1190
    invoke-direct {p0}, Lcjt;->Nh()V

    .line 1192
    :cond_0
    return-void
.end method

.method public final Nj()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1242
    iget-object v1, p0, Lcjt;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->NY()Ljava/lang/String;

    move-result-object v1

    .line 1243
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1245
    iget-object v2, p0, Lcjt;->mSettings:Lcke;

    invoke-interface {v2}, Lcke;->ND()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1247
    :cond_0
    return v0
.end method

.method public final Nk()V
    .locals 4

    .prologue
    .line 1255
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "refresh_search_history"

    iget-object v2, p0, Lcjt;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->Hz()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    .line 1258
    return-void
.end method

.method public final Nl()V
    .locals 5

    .prologue
    .line 1527
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1528
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1529
    iget-object v1, p0, Lcjt;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->j(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 1530
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 1531
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    .line 1532
    invoke-virtual {v1}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1534
    const-string v4, "tch"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1535
    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 1539
    :cond_1
    new-instance v0, Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcjt;->a(Lcom/google/android/search/shared/api/UriRequest;)V

    .line 1541
    :cond_2
    return-void
.end method

.method public final Nm()V
    .locals 1

    .prologue
    .line 1667
    iget-object v0, p0, Lcjt;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BF()V

    .line 1668
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Er()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1669
    return-void
.end method

.method public final Nn()V
    .locals 4

    .prologue
    .line 1676
    iget-object v0, p0, Lcjt;->mServiceWorker:Ldpb;

    iget-object v1, v0, Ldpb;->aMD:Leqo;

    iget-object v0, v0, Ldpb;->bEz:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v1, v0, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 1677
    return-void
.end method

.method public final No()V
    .locals 3

    .prologue
    .line 1680
    iget-object v0, p0, Lcjt;->mContext:Landroid/content/Context;

    const v1, 0x7f0a05d3

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1681
    return-void
.end method

.method protected a(Lcom/google/android/shared/search/Query;)Lhli;
    .locals 1

    .prologue
    .line 423
    new-instance v0, Lcjz;

    invoke-direct {v0, p0, p1}, Lcjz;-><init>(Lcjt;Lcom/google/android/shared/search/Query;)V

    return-object v0
.end method

.method public final varargs a(Landroid/content/Intent;I[Ljava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227
    iget-object v0, p0, Lcjt;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p2, p3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1228
    iget-object v1, p0, Lcjt;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1229
    if-eqz p1, :cond_0

    .line 1230
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcjw;

    invoke-direct {v2, p0, p1}, Lcjw;-><init>(Lcjt;Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1237
    :cond_0
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1238
    return-void
.end method

.method public final a(Lbwp;ZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    .line 1614
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Er()Ljava/util/Queue;

    move-result-object v0

    .line 1615
    if-nez p3, :cond_0

    invoke-virtual {p1}, Lbwp;->Bb()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1616
    const-string v0, "SearchController"

    const-string v1, "Duplicate notification: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lbwp;->Bb()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v5, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1631
    :goto_0
    return-void

    .line 1619
    :cond_0
    invoke-virtual {p1}, Lbwp;->Bb()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1620
    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v1

    if-le v1, v5, :cond_1

    .line 1621
    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 1623
    :cond_1
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aau()Ldbh;

    move-result-object v0

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    if-nez v0, :cond_3

    if-nez p2, :cond_2

    iget-object v0, p0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WC()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1625
    :cond_2
    invoke-direct {p0, p1}, Lcjt;->b(Lbwp;)V

    goto :goto_0

    .line 1628
    :cond_3
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ep()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/api/UriRequest;)V
    .locals 8

    .prologue
    const v7, 0x7f0a05df

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1200
    :try_start_0
    iget-object v0, p0, Lcjt;->mUrlHelper:Lcpn;

    invoke-virtual {p1}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpn;->p(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    .line 1201
    invoke-virtual {p1}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    .line 1202
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1203
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1204
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1205
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1219
    :catch_0
    move-exception v0

    .line 1220
    const-string v1, "SearchController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Malformed URI: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1221
    const/4 v0, 0x0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v7, v1}, Lcjt;->a(Landroid/content/Intent;I[Ljava/lang/Object;)V

    .line 1223
    :goto_1
    return-void

    .line 1207
    :cond_0
    :try_start_1
    const-string v0, "com.android.browser.headers"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1209
    :cond_1
    const-string v0, "com.google.android.shared.util.SimpleIntentStarter.USE_FAST_TRANSITION"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1210
    const-string v0, "com.google.android.shared.util.SimpleIntentStarter.ERROR_TOAST_ID"

    const v1, 0x7f0a05df

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1215
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->aac()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 1216
    :goto_2
    iget-object v1, p0, Lcjt;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v5, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v5}, Ldda;->aao()Ldcu;

    move-result-object v5

    invoke-virtual {v5}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v0, v2, v5}, Lhgn;->a(Landroid/content/Context;Landroid/content/Intent;ZZLjava/lang/String;)V

    .line 1218
    iget-object v0, p0, Lcjt;->mService:Ldao;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Ldao;->b([Landroid/content/Intent;)Z
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 1215
    goto :goto_2
.end method

.method public final a(Ldah;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 852
    invoke-static {}, Lenu;->auR()V

    .line 853
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    if-eq p1, v0, :cond_8

    .line 854
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    if-eqz v0, :cond_2

    .line 855
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    .line 856
    const/16 v1, 0xdd

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v2, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lege;->kQ(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Litu;->mH(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 862
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aAs()Lequ;

    move-result-object v0

    iget-object v1, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v1}, Ldah;->UE()Leqv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lequ;->b(Leqv;)V

    .line 864
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 865
    invoke-virtual {p0}, Lcjt;->MW()Ldfl;

    move-result-object v0

    iget-object v1, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v1}, Ldah;->UD()Ldfk;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldfl;->b(Ldfk;)V

    .line 868
    :cond_0
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->Ux()Z

    move-result v0

    if-nez v0, :cond_1

    .line 873
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yn()V

    .line 876
    :cond_1
    if-nez p1, :cond_9

    iget-object v0, p0, Lcjt;->aZo:Ldah;

    iget-object v1, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v0, v9, v9, v9, v1}, Ldah;->a(ZZZLecg;)V

    iget-object v0, p0, Lcjt;->aZo:Ldah;

    iput-object v0, p0, Lcjt;->aZp:Ldah;

    .line 878
    :cond_2
    :goto_0
    iput-object p1, p0, Lcjt;->aZo:Ldah;

    .line 881
    iget-object v0, p0, Lcjt;->mServiceWorker:Ldpb;

    iput-object p1, v0, Ldpb;->bpr:Ldah;

    .line 882
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    if-eqz v0, :cond_a

    .line 883
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    .line 884
    const/16 v1, 0xde

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v2, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lege;->kQ(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mH(I)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 889
    iget-object v1, p0, Lcjt;->aZp:Ldah;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcjt;->aZp:Ldah;

    iget-object v2, p0, Lcjt;->aZo:Ldah;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcjt;->aZp:Ldah;

    iget-object v2, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v1, v9, v10, v9, v2}, Ldah;->a(ZZZLecg;)V

    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcjt;->aZp:Ldah;

    .line 891
    iget-object v1, p0, Lcjt;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bP(Landroid/content/Context;)V

    .line 892
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anb()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 893
    invoke-direct {p0}, Lcjt;->Nd()V

    .line 897
    :cond_4
    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aag()V

    .line 898
    iget-object v1, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v1, v10}, Ldao;->cS(Z)V

    .line 899
    iget-object v1, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {p1}, Ldah;->getId()J

    move-result-wide v2

    invoke-virtual {p1}, Ldah;->Uy()J

    move-result-wide v4

    invoke-virtual {p1}, Ldah;->Uz()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p1}, Ldah;->UA()I

    move-result v7

    invoke-virtual {p1}, Ldah;->UB()Landroid/os/Bundle;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/search/core/state/QueryState;->a(JJLandroid/os/Bundle;ILandroid/os/Bundle;)V

    .line 903
    iget-object v1, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->aAs()Lequ;

    move-result-object v1

    invoke-virtual {p1}, Ldah;->UE()Leqv;

    move-result-object v2

    invoke-virtual {v1, v2}, Lequ;->a(Leqv;)V

    .line 905
    iget-object v1, p0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {p1}, Ldah;->Uw()Z

    move-result v2

    invoke-virtual {v1, v2}, Ldby;->dc(Z)V

    .line 909
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 910
    if-nez v1, :cond_5

    .line 911
    iget-object v2, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v2, v9}, Ldao;->cS(Z)V

    .line 916
    :cond_5
    iget-object v2, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aao()Ldcu;

    move-result-object v2

    .line 917
    invoke-virtual {v2, v0}, Ldcu;->a(Lcom/google/android/search/shared/service/ClientConfig;)V

    .line 919
    iget-object v2, p0, Lcjt;->aZo:Ldah;

    iget-object v3, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v2, v10, v9, v9, v3}, Ldah;->a(ZZZLecg;)V

    .line 921
    iget-object v2, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aan()Ldcy;

    move-result-object v2

    invoke-virtual {v2}, Ldcy;->ZG()V

    .line 924
    iget-object v2, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aah()V

    .line 926
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anc()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 927
    invoke-virtual {p0}, Lcjt;->MW()Ldfl;

    move-result-object v2

    iget-object v3, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v3}, Ldah;->UD()Ldfk;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldfl;->a(Ldfk;)V

    .line 929
    :cond_6
    iget-object v2, p0, Lcjt;->aUe:Lcfc;

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amU()Z

    move-result v0

    if-nez v0, :cond_7

    .line 931
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    invoke-virtual {v0}, Lcfc;->detach()V

    .line 934
    :cond_7
    if-eqz v1, :cond_8

    .line 935
    iget-object v0, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v0, v9}, Ldao;->cS(Z)V

    .line 950
    :cond_8
    :goto_1
    return-void

    .line 876
    :cond_9
    invoke-virtual {p1}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anr()Z

    move-result v0

    iget-object v1, p0, Lcjt;->aZo:Ldah;

    iget-object v2, p0, Lcjt;->mService:Ldao;

    invoke-virtual {v1, v9, v10, v0, v2}, Ldah;->a(ZZZLecg;)V

    goto/16 :goto_0

    .line 939
    :cond_a
    iget-object v0, p0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {v0, v9}, Ldby;->dc(Z)V

    .line 942
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    sget-object v1, Lcom/google/android/search/shared/service/ClientConfig;->bUT:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v0, v1}, Ldcu;->a(Lcom/google/android/search/shared/service/ClientConfig;)V

    .line 945
    iget-object v0, p0, Lcjt;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bQ(Landroid/content/Context;)V

    .line 947
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DW()Lcpq;

    move-result-object v0

    invoke-virtual {v0}, Lcpq;->PK()V

    goto :goto_1
.end method

.method public final a(Lddb;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 586
    iget-object v0, p0, Lcjt;->mExternalQueryWorker:Ldoi;

    invoke-virtual {v0}, Ldoi;->aee()V

    .line 587
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XW()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcjt;->anR:Z

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcjt;->anR:Z

    new-instance v0, Lcjx;

    iget-object v4, p0, Lcjt;->mCorpora:Lcfu;

    iget-object v5, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-direct {v0, v4, v5}, Lcjx;-><init>(Lcfu;Lcom/google/android/search/core/state/QueryState;)V

    invoke-virtual {v0}, Lcjx;->start()V

    invoke-virtual {p0}, Lcjt;->MW()Ldfl;

    move-result-object v0

    invoke-virtual {v0}, Ldfl;->cV()V

    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DZ()Lczz;

    move-result-object v0

    invoke-virtual {v0}, Lczz;->Uq()V

    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DW()Lcpq;

    move-result-object v0

    invoke-virtual {v0}, Lcpq;->PJ()V

    iget-object v0, p0, Lcjt;->mPumpkinWorker:Ldou;

    invoke-virtual {v0}, Ldou;->ed()V

    :cond_0
    iget-boolean v0, p0, Lcjt;->aZn:Z

    if-nez v0, :cond_1

    iput-boolean v2, p0, Lcjt;->aZn:Z

    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DP()Lcob;

    move-result-object v0

    invoke-interface {v0, p0}, Lcob;->a(Lcoc;)V

    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, p0}, Lcke;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 588
    :cond_1
    iget-object v0, p0, Lcjt;->mLocationWorker:Ldor;

    invoke-virtual {v0, p1}, Ldor;->a(Lddb;)V

    .line 589
    iget-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    sget-object v4, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-eq v0, v4, :cond_5

    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v4, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v4}, Lcom/google/android/search/core/state/QueryState;->L(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_2
    iget-object v0, p0, Lcjt;->aZl:Lclq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcjt;->aZl:Lclq;

    invoke-virtual {v0}, Lclq;->cancel()V

    iput-object v3, p0, Lcjt;->aZl:Lclq;

    :cond_3
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    invoke-virtual {v0}, Ldcw;->Zq()V

    :cond_4
    :goto_0
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    .line 590
    :cond_5
    :goto_1
    invoke-direct {p0}, Lcjt;->MU()V

    .line 591
    iget-object v0, p0, Lcjt;->mAudioWorker:Ldnz;

    invoke-virtual {v0}, Ldnz;->adY()V

    .line 592
    iget-object v0, p0, Lcjt;->mAudioWorker:Ldnz;

    invoke-virtual {v0}, Ldnz;->adZ()V

    .line 593
    invoke-virtual {p1}, Lddb;->aaB()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 594
    iget-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    invoke-virtual {v0}, Ldoj;->aeg()V

    .line 595
    iget-object v0, p0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WK()Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    .line 596
    if-eqz v0, :cond_6

    iget-object v4, p0, Lcjt;->aZo:Ldah;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v4}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/search/shared/service/ClientConfig;->anf()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 599
    iget-object v4, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v4, v0}, Ldah;->a(Lcom/google/android/shared/speech/HotwordResult;)V

    .line 601
    :cond_6
    iget-object v0, p0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WA()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcjt;->aZo:Ldah;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anf()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 603
    iget-object v0, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0}, Ldah;->ua()V

    .line 607
    :cond_7
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    invoke-virtual {v0, p1}, Ldpk;->a(Lddb;)V

    .line 611
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aau()Ldbh;

    move-result-object v0

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    if-nez v0, :cond_17

    .line 613
    iget-boolean v0, p0, Lcjt;->aZq:Z

    if-nez v0, :cond_8

    .line 616
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ep()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwp;

    .line 617
    if-eqz v0, :cond_16

    .line 618
    invoke-direct {p0, v0}, Lcjt;->b(Lbwp;)V

    .line 623
    :cond_8
    :goto_2
    iput-boolean v2, p0, Lcjt;->aZq:Z

    .line 628
    :goto_3
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 629
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    .line 630
    invoke-virtual {v0}, Ldcu;->YU()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 631
    iget-object v4, p0, Lcjt;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->k(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    .line 635
    const v5, 0x7f0a05b3

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, p0, Lcjt;->mSettings:Lcke;

    invoke-interface {v7}, Lcke;->ND()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {p0, v4, v5, v6}, Lcjt;->a(Landroid/content/Intent;I[Ljava/lang/Object;)V

    .line 636
    iget-object v4, p0, Lcjt;->mSettings:Lcke;

    invoke-virtual {v0}, Ldcu;->YT()I

    move-result v5

    invoke-interface {v4, v5}, Lcke;->eZ(I)V

    .line 639
    :cond_9
    iget-object v4, p0, Lcjt;->mUrlHelper:Lcpn;

    invoke-virtual {v0}, Ldcu;->YQ()Lgnu;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcpn;->d(Lgnu;)V

    .line 644
    :cond_a
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XD()Lcom/google/android/shared/search/Query;

    move-result-object v4

    .line 645
    if-eqz v4, :cond_b

    .line 646
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 647
    invoke-virtual {p0}, Lcjt;->MV()Lcmw;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcmw;->g(Lcom/google/android/shared/search/Query;)Lcmq;

    move-result-object v0

    if-eqz v0, :cond_18

    iget-object v1, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v4, v0}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcmq;)V

    .line 666
    :cond_b
    :goto_4
    iget-object v0, p0, Lcjt;->mPumpkinWorker:Ldou;

    invoke-virtual {v0}, Ldou;->aee()V

    .line 668
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 670
    iget-object v1, p0, Lcjt;->mActionWorker:Ldnm;

    invoke-virtual {v1, p1}, Ldnm;->a(Lddb;)V

    .line 671
    iget-object v1, p0, Lcjt;->mTtsWorker:Ldpg;

    invoke-virtual {v1, p1}, Ldpg;->a(Lddb;)V

    .line 672
    iget-object v1, p0, Lcjt;->mServiceWorker:Ldpb;

    invoke-virtual {v1, p1}, Ldpb;->a(Lddb;)V

    .line 674
    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    .line 675
    if-eqz v0, :cond_c

    .line 678
    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aat()Ldcf;

    move-result-object v1

    .line 679
    invoke-virtual {v1, v0}, Ldcf;->e(Lcom/google/android/velvet/ActionData;)I

    move-result v1

    .line 680
    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v2

    .line 681
    if-eqz v1, :cond_c

    if-eqz v2, :cond_c

    .line 682
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XG()Lcmq;

    move-result-object v0

    .line 683
    if-eqz v0, :cond_c

    .line 684
    invoke-virtual {v0}, Lcmq;->Ql()Lcnb;

    move-result-object v0

    iget-wide v4, v0, Lcnb;->amT:J

    invoke-static {v4, v5}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v4

    .line 685
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DQ()Lcpd;

    move-result-object v0

    iget-object v2, v2, Ljyw;->eMy:Ljyx;

    iget-object v5, p0, Lcjt;->mUrlHelper:Lcpn;

    const-wide/16 v6, -0x1

    const/4 v8, -0x1

    invoke-virtual/range {v0 .. v8}, Lcpd;->a(ILjyx;Ljava/lang/String;Ljava/lang/String;Lcpn;JI)V

    .line 693
    :cond_c
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 695
    invoke-virtual {p0}, Lcjt;->Nk()V

    .line 697
    :cond_d
    return-void

    .line 589
    :cond_e
    iget-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcjt;->aZk:Ldpe;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcjt;->aZk:Ldpe;

    invoke-virtual {v0}, Ldpe;->cancel()V

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v4

    if-nez v4, :cond_10

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v4

    if-nez v4, :cond_10

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VS()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XC()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_11
    move v0, v2

    :goto_5
    iget-object v4, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v4

    if-nez v4, :cond_13

    move v4, v2

    :goto_6
    and-int/2addr v4, v0

    invoke-direct {p0}, Lcjt;->MT()Lhlf;

    move-result-object v5

    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v6, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v6}, Lcom/google/android/search/core/state/QueryState;->M(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_14

    move v0, v2

    :goto_7
    invoke-virtual {v5, v4, v0}, Lhlf;->I(ZZ)V

    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v4, 0x6

    invoke-virtual {v0, v4}, Ldct;->fE(I)V

    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    invoke-virtual {v0}, Ldcw;->Zq()V

    goto/16 :goto_0

    :cond_12
    move v0, v1

    goto :goto_5

    :cond_13
    move v4, v1

    goto :goto_6

    :cond_14
    move v0, v1

    goto :goto_7

    :cond_15
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v4, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v4}, Lcom/google/android/search/core/state/QueryState;->S(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Lcjt;->aZs:Z

    invoke-direct {p0}, Lcjt;->MT()Lhlf;

    move-result-object v0

    iget-object v4, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v4}, Lenw;->auS()Lenw;

    iget-boolean v4, v0, Lhlf;->diX:Z

    if-eqz v4, :cond_5

    const/16 v4, 0x11

    invoke-static {v4}, Lege;->ht(I)V

    iget-object v4, v0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v4}, Lhhq;->aOR()Lgdb;

    move-result-object v4

    iget-object v0, v0, Lhlf;->bbe:Ljava/lang/String;

    invoke-interface {v4, v0}, Lgdb;->mr(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 620
    :cond_16
    iget-object v0, p0, Lcjt;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BE()V

    goto/16 :goto_2

    .line 625
    :cond_17
    iput-boolean v1, p0, Lcjt;->aZq:Z

    goto/16 :goto_3

    .line 647
    :cond_18
    new-instance v0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;

    const/16 v1, 0x190

    const-string v2, "Page not in cache"

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;-><init>(Lcom/google/android/shared/search/Query;ILjava/lang/String;)V

    iget-object v1, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v4, v0}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    goto/16 :goto_4

    .line 649
    :cond_19
    iput-object v4, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    .line 655
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v5

    if-nez v5, :cond_1f

    move v2, v1

    :goto_8
    if-nez v2, :cond_d

    .line 662
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqd()Z

    move-result v0

    if-nez v0, :cond_1d

    const/16 v0, 0x14

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqa()Z

    move-result v2

    if-nez v2, :cond_1a

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqi()Z

    move-result v2

    if-eqz v2, :cond_24

    :cond_1a
    const/16 v0, 0xb1

    :cond_1b
    :goto_9
    iget-object v2, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aao()Ldcu;

    move-result-object v2

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v5, p0, Lcjt;->mScreenStateHelper:Ldmh;

    invoke-virtual {v5}, Ldmh;->adp()I

    move-result v5

    invoke-virtual {v0, v5}, Litu;->mK(I)Litu;

    move-result-object v0

    invoke-direct {p0}, Lcjt;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lege;->kQ(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Litu;->mH(I)Litu;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v5

    invoke-virtual {v0, v5}, Litu;->ha(Z)Litu;

    move-result-object v0

    invoke-virtual {v2}, Ldcu;->YZ()Z

    move-result v5

    invoke-virtual {v0, v5}, Litu;->hb(Z)Litu;

    move-result-object v0

    iget-object v5, p0, Lcjt;->mSettingsUtil:Lbyl;

    invoke-virtual {v5}, Lbyl;->BU()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-virtual {v2}, Ldcu;->Zb()Z

    move-result v2

    invoke-virtual {v0, v2}, Litu;->hc(Z)Litu;

    :cond_1c
    invoke-static {v0}, Lege;->a(Litu;)V

    :cond_1d
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ee()Lhzj;

    move-result-object v0

    invoke-virtual {v0, v4}, Lhzj;->bd(Lcom/google/android/shared/search/Query;)V

    :goto_a
    iput-boolean v1, p0, Lcjt;->aZs:Z

    invoke-direct {p0}, Lcjt;->MT()Lhlf;

    move-result-object v0

    invoke-virtual {p0, v4}, Lcjt;->a(Lcom/google/android/shared/search/Query;)Lhli;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lhlf;->a(Lcom/google/android/shared/search/Query;Lhli;)V

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcjt;->aZr:Lbyf;

    if-nez v0, :cond_1e

    new-instance v0, Lbyf;

    iget-object v1, p0, Lcjt;->mEventBus:Ldda;

    iget-object v2, p0, Lcjt;->mService:Ldao;

    iget-object v4, p0, Lcjt;->mScreenStateHelper:Ldmh;

    invoke-direct {v0, v1, v2, v4}, Lbyf;-><init>(Ldda;Leqp;Ldmh;)V

    iput-object v0, p0, Lcjt;->aZr:Lbyf;

    :cond_1e
    iget-object v0, p0, Lcjt;->aZr:Lbyf;

    iput-object v3, v0, Lbyf;->aOJ:Lcom/google/android/shared/search/Query;

    iget-object v1, v0, Lbyf;->mEventBus:Ldda;

    invoke-virtual {v1, v0}, Ldda;->c(Lddc;)V

    goto/16 :goto_4

    .line 655
    :cond_1f
    invoke-virtual {v0}, Lhym;->aUa()Z

    move-result v5

    if-eqz v5, :cond_20

    move v2, v1

    goto/16 :goto_8

    :cond_20
    iget-object v5, p0, Lcjt;->mScreenStateHelper:Ldmh;

    invoke-virtual {v5}, Ldmh;->isScreenOn()Z

    move-result v5

    if-eqz v5, :cond_21

    move v2, v1

    goto/16 :goto_8

    :cond_21
    invoke-virtual {v0}, Lhym;->aUb()Z

    move-result v5

    if-eqz v5, :cond_22

    move v2, v1

    goto/16 :goto_8

    :cond_22
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v5

    if-eqz v5, :cond_23

    move v2, v1

    goto/16 :goto_8

    :cond_23
    invoke-virtual {v0}, Lhym;->aUc()V

    new-instance v0, Ldoh;

    iget-object v5, p0, Lcjt;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcjt;->mEventBus:Ldda;

    invoke-direct {v0, v5, v6}, Ldoh;-><init>(Landroid/content/Context;Ldda;)V

    invoke-virtual {v0, v4}, Ldoh;->aK(Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_8

    .line 662
    :cond_24
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqb()Z

    move-result v2

    if-eqz v2, :cond_25

    const/16 v0, 0xae

    goto/16 :goto_9

    :cond_25
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqg()Z

    move-result v2

    if-eqz v2, :cond_1b

    const/16 v0, 0xaf

    goto/16 :goto_9

    :cond_26
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ed()Lhzl;

    move-result-object v0

    invoke-virtual {v0, v4}, Lhzl;->bd(Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_a

    :cond_27
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcjt;->aZl:Lclq;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOU()Lclq;

    move-result-object v0

    iput-object v0, p0, Lcjt;->aZl:Lclq;

    iget-object v0, p0, Lcjt;->aZl:Lclq;

    new-instance v1, Lcjy;

    invoke-direct {v1, p0, v4}, Lcjy;-><init>(Lcjt;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {v0, v1, v4}, Lclq;->a(Lclt;Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_4

    :cond_28
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcjt;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LZ()Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcjt;->aZl:Lclq;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOU()Lclq;

    move-result-object v0

    iput-object v0, p0, Lcjt;->aZl:Lclq;

    iget-object v0, p0, Lcjt;->aZl:Lclq;

    new-instance v1, Lcjy;

    invoke-direct {v1, p0, v4}, Lcjy;-><init>(Lcjt;Lcom/google/android/shared/search/Query;)V

    invoke-virtual {v0, v1, v4}, Lclq;->a(Lclt;Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_4

    :cond_29
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    new-instance v1, Lcom/google/android/search/shared/actions/errors/SoundSearchUnavailableError;

    invoke-direct {v1, v4}, Lcom/google/android/search/shared/actions/errors/SoundSearchUnavailableError;-><init>(Lcom/google/android/shared/search/Query;)V

    invoke-virtual {v0, v4, v1}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    goto/16 :goto_4

    :cond_2a
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v0

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcjt;->aZk:Ldpe;

    if-nez v0, :cond_2b

    iget-object v0, p0, Lcjt;->mFactory:Lgpu;

    invoke-virtual {v0, p0}, Lgpu;->d(Lcjt;)Ldpe;

    move-result-object v0

    iput-object v0, p0, Lcjt;->aZk:Ldpe;

    :cond_2b
    iget-object v0, p0, Lcjt;->aZk:Ldpe;

    invoke-virtual {v0, v4}, Ldpe;->aM(Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_4

    :cond_2c
    const-string v0, "SearchController"

    const-string v2, "Unrecognized query type from takeQueryToCommitToMajel"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_4
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1546
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 1547
    const-string v0, "SearchController state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1548
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1549
    const-string v0, "Search Error"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v0}, Letn;->avS()Letn;

    move-result-object v0

    iget-object v1, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1553
    :cond_0
    const-string v0, "Active Client"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcjt;->aZo:Ldah;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 1554
    iget-object v0, p0, Lcjt;->mConfig:Lcjs;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 1555
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 1556
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 1557
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 1558
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 1559
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcjt;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NK()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    iget-object v0, p0, Lcjt;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjF:Landroid/net/Uri;

    move-object v3, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Leqw;->a(Ljava/lang/CharSequence;C)Leqw;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Leqw;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Leqw;->next()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Letj;->lu(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/SharedPreferences;Letj;)V

    .line 1560
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 1595
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    iget-object v1, v0, Ldpk;->bES:Ldyo;

    if-eqz v1, :cond_0

    iget-object v0, v0, Ldpk;->bES:Ldyo;

    invoke-virtual {v0, p1}, Ldyo;->b(Ljava/io/PrintWriter;)V

    .line 1596
    :goto_0
    return-void

    .line 1595
    :cond_0
    const-string v0, "[No WebPage exists]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a([BIII)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 996
    iget-object v9, p0, Lcjt;->mHotwordWorker:Ldoj;

    iget-object v0, v9, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFi()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HotwordWorker"

    const-string v1, "HotwordDetectedOnDsp but setting is disabled"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown verification mode"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, v9, Ldoj;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->ND()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v9, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v1, v0}, Lhym;->oD(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v9, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v1, v3}, Lhym;->gN(Z)V

    const-string v1, "HotwordWorker"

    const-string v2, "HotwordDetectedOnDsp No speaker ID model for %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-static {v1, v2, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-ltz p4, :cond_2

    new-instance v0, Lgfg;

    const v2, 0x8000

    const/4 v4, 0x0

    move v1, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lgfg;-><init>(IIZLgfn;ZI)V

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lgfg;->kj(I)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v0, Larv;

    iget-object v1, v9, Ldoj;->mContext:Landroid/content/Context;

    iget-object v2, v9, Ldoj;->mVoiceSearchServices:Lhhq;

    iget-object v3, v9, Ldoj;->mVoiceSearchServices:Lhhq;

    iget-object v3, v3, Lhhq;->mAsyncServices:Lema;

    iget-object v4, v9, Ldoj;->mVoiceSearchServices:Lhhq;

    iget-object v4, v4, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v5, v9, Ldoj;->mSearchSettings:Lcke;

    invoke-interface {v5}, Lcke;->ND()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v9, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v6}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v6

    if-eqz p3, :cond_4

    move v7, p3

    :goto_2
    const/4 v8, 0x2

    invoke-direct/range {v0 .. v8}, Larv;-><init>(Landroid/content/Context;Lhhq;Lema;Lchk;Ljava/lang/String;Ljava/lang/String;II)V

    iput-object v0, v9, Ldoj;->anl:Larv;

    iget-object v0, v9, Ldoj;->anl:Larv;

    iget-object v1, v9, Ldoj;->anq:Lary;

    invoke-virtual {v0, v10, v1}, Larv;->a(Ljava/util/List;Lary;)V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v0, "HotwordWorker"

    const-string v1, "No suitable method for verification"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    const/16 v7, 0x3e80

    goto :goto_2

    :pswitch_1
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0, p1, p3}, Lcom/google/android/shared/speech/HotwordResult;->a(F[BI)Lcom/google/android/shared/speech/HotwordResult;

    move-result-object v0

    invoke-virtual {v9, v0, v5}, Ldoj;->a(Lcom/google/android/shared/speech/HotwordResult;Z)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {v9, v0, v1}, Ldoj;->at(J)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 1261
    iget-object v0, p0, Lcjt;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->b(Landroid/graphics/Point;)V

    .line 1262
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 761
    invoke-virtual {p0}, Lcjt;->MX()Lcmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcmt;->e(Lcom/google/android/shared/search/Query;)V

    .line 762
    return-void
.end method

.method public final cN()Z
    .locals 2

    .prologue
    .line 1118
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    iget-object v1, v0, Ldpk;->aXc:Ldnb;

    if-eqz v1, :cond_0

    iget-object v0, v0, Ldpk;->aXc:Ldnb;

    invoke-virtual {v0}, Ldnb;->cN()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ca()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 771
    iget-boolean v0, p0, Lcjt;->cq:Z

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 773
    iget-object v0, p0, Lcjt;->mAudioWorker:Ldnz;

    invoke-virtual {v0}, Ldnz;->ca()V

    .line 774
    iget-object v0, p0, Lcjt;->mServiceWorker:Ldpb;

    iget-object v2, v0, Ldpb;->aMD:Leqo;

    iget-object v0, v0, Ldpb;->bEy:Ljava/lang/Runnable;

    invoke-interface {v2, v0}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 776
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcjt;->aUe:Lcfc;

    invoke-virtual {v0}, Lcfc;->detach()V

    .line 780
    :cond_0
    iget-boolean v0, p0, Lcjt;->aZn:Z

    if-eqz v0, :cond_1

    .line 781
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DP()Lcob;

    move-result-object v0

    invoke-interface {v0, p0}, Lcob;->b(Lcoc;)V

    .line 782
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, p0}, Lcke;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 785
    :cond_1
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    if-eqz v0, :cond_5

    .line 786
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    iget-object v2, v0, Ldfl;->bwk:Ldfe;

    iget-object v3, v2, Ldfe;->bvV:Lchg;

    if-eqz v3, :cond_2

    iget-object v3, v2, Ldfe;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->Em()Lcha;

    move-result-object v3

    iget-object v4, v2, Ldfe;->bvV:Lchg;

    invoke-virtual {v3, v4}, Lcha;->b(Lchg;)V

    iput-object v5, v2, Ldfe;->bvV:Lchg;

    :cond_2
    iget-object v3, v2, Ldfe;->bvW:Lctb;

    if-eqz v3, :cond_3

    iget-object v3, v2, Ldfe;->bvW:Lctb;

    invoke-virtual {v3}, Lctb;->unregister()V

    iput-object v5, v2, Ldfe;->bvW:Lctb;

    :cond_3
    iget-object v2, v0, Ldfl;->bwy:Lcqn;

    if-eqz v2, :cond_4

    iget-object v2, v0, Ldfl;->bwy:Lcqn;

    invoke-virtual {v2}, Lcqn;->close()V

    :cond_4
    iget-boolean v2, v0, Ldfl;->anR:Z

    if-eqz v2, :cond_5

    iget-object v2, v0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v2

    iget-object v3, v0, Ldfl;->bmB:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    iget-object v2, v0, Ldfl;->mSources:Ldgh;

    if-eqz v2, :cond_5

    iget-object v2, v0, Ldfl;->mSources:Ldgh;

    iget-object v0, v0, Ldfl;->bmB:Landroid/database/DataSetObserver;

    invoke-interface {v2, v0}, Ldgh;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 789
    :cond_5
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DH()Lcoy;

    move-result-object v0

    iget-boolean v2, v0, Lcoy;->bew:Z

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcoy;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, v0, Lcoy;->beA:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-boolean v1, v0, Lcoy;->bew:Z

    .line 791
    :cond_6
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    invoke-virtual {v0}, Ldpk;->ca()V

    .line 792
    return-void

    :cond_7
    move v0, v1

    .line 771
    goto/16 :goto_0
.end method

.method public final cl(Z)Landroid/view/View;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1102
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Ldpk;->aep()Ldnb;

    :cond_0
    iget-object v0, v0, Ldpk;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method public final eA()V
    .locals 2

    .prologue
    .line 1111
    iget-object v0, p0, Lcjt;->mWebViewWorker:Ldpk;

    iget-object v1, v0, Ldpk;->mWebView:Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Ldpk;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->freeMemory()V

    .line 1112
    :cond_0
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    if-eqz v0, :cond_1

    .line 1113
    iget-object v0, p0, Lcjt;->aZm:Lcmt;

    invoke-virtual {v0}, Lcmt;->eA()V

    .line 1115
    :cond_1
    return-void
.end method

.method public final eX(I)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 974
    const/16 v0, 0x20

    if-ne p1, v0, :cond_1

    .line 975
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0, v1}, Lhym;->lA(I)V

    .line 979
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Vb()V

    .line 987
    :cond_0
    :goto_0
    return-void

    .line 980
    :cond_1
    const/16 v0, 0x80

    if-ne p1, v0, :cond_0

    .line 981
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0, v1}, Lhym;->lB(I)V

    .line 985
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Vb()V

    goto :goto_0
.end method

.method public final isStarted()Z
    .locals 1

    .prologue
    .line 1495
    iget-boolean v0, p0, Lcjt;->cq:Z

    return v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1147
    sget-object v0, Lcjt;->aZg:Lijp;

    invoke-virtual {v0, p2}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcjt;->anR:Z

    if-eqz v0, :cond_3

    .line 1150
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    invoke-virtual {v0}, Lddk;->aaP()V

    .line 1151
    invoke-virtual {p0}, Lcjt;->MY()V

    .line 1152
    invoke-direct {p0}, Lcjt;->Nh()V

    .line 1169
    :cond_0
    :goto_0
    const-string v0, "google_account"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1171
    iget-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WL()V

    .line 1174
    :cond_1
    const-string v0, "opted_in_version_"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1175
    iget-object v0, p0, Lcjt;->mSettings:Lcke;

    iget-object v1, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v1

    invoke-virtual {v1}, Lcrh;->Su()Z

    move-result v1

    invoke-interface {v0, v1}, Lcke;->co(Z)V

    .line 1177
    iget-object v0, p0, Lcjt;->aOw:Ljava/util/concurrent/Executor;

    new-instance v1, Lcjv;

    const-string v2, "forceIcingResync"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lcjv;-><init>(Lcjt;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 1184
    :cond_2
    return-void

    .line 1153
    :cond_3
    const-string v0, "enable_corpus_"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcjt;->anR:Z

    if-eqz v0, :cond_4

    .line 1155
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcjt;->mSuggestionsPresenter:Ldfl;

    invoke-virtual {v0}, Ldfl;->aaV()V

    goto :goto_0

    .line 1158
    :cond_4
    const-string v0, "spoken-language-bcp-47"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "update_hotword_models"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    .line 1161
    iget-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    invoke-virtual {v0}, Ldoj;->aef()V

    .line 1162
    iget-object v0, p0, Lcjt;->mPumpkinWorker:Ldou;

    invoke-virtual {v0}, Ldou;->aej()V

    .line 1165
    iget-object v0, p0, Lcjt;->mActionWorker:Ldnm;

    const/4 v1, 0x0

    iput-object v1, v0, Ldnm;->bDF:Lhyq;

    goto :goto_0
.end method

.method public final start()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 797
    iget-boolean v0, p0, Lcjt;->cq:Z

    if-nez v0, :cond_2

    .line 798
    iget-object v0, p0, Lcjt;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Md()Z

    move-result v0

    invoke-static {v0}, Lgnm;->setEnabled(Z)V

    .line 800
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v4

    invoke-virtual {v4}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v5, v6}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v1

    :goto_0
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v0

    iget-object v3, v0, Ljyw;->eAp:Liwg;

    :cond_0
    if-eqz v5, :cond_1

    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v4, v2}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/velvet/ActionData;->aoM()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v7}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v7}, Leqt;->H(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcky;->a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;Liwg;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljava/lang/String;ZLjava/lang/String;)V

    .line 802
    :cond_1
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->c(Lddc;)V

    .line 804
    iget-object v0, p0, Lcjt;->mCookiesLock:Ldkq;

    iget-object v1, p0, Lcjt;->aZi:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Ldkq;->registerObserver(Ljava/lang/Object;)V

    .line 805
    invoke-virtual {p0}, Lcjt;->MY()V

    .line 807
    iput-boolean v8, p0, Lcjt;->cq:Z

    .line 809
    iget-object v0, p0, Lcjt;->mUrlHelper:Lcpn;

    invoke-virtual {v0}, Lcpn;->Rm()V

    .line 811
    iget-object v0, p0, Lcjt;->mAudioWorker:Ldnz;

    invoke-virtual {v0}, Ldnz;->Rm()V

    .line 812
    iget-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    invoke-virtual {v0}, Ldoj;->Rm()V

    .line 813
    iget-object v0, p0, Lcjt;->mServiceWorker:Ldpb;

    iget-object v0, v0, Ldpb;->bEx:Ldog;

    invoke-virtual {v0}, Ldog;->Rm()V

    .line 815
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DH()Lcoy;

    move-result-object v0

    iget-boolean v1, v0, Lcoy;->bew:Z

    if-nez v1, :cond_2

    new-instance v1, Lcpb;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, v0, v2}, Lcpb;-><init>(Lcoy;Landroid/os/Handler;)V

    iput-object v1, v0, Lcoy;->beA:Landroid/database/ContentObserver;

    iget-object v1, v0, Lcoy;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, v0, Lcoy;->bev:Landroid/net/Uri;

    iget-object v3, v0, Lcoy;->beA:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iput-boolean v8, v0, Lcoy;->bew:Z

    .line 817
    :cond_2
    return-void

    :cond_3
    move-object v1, v3

    .line 800
    goto/16 :goto_0
.end method

.method public final stop()V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1008
    iget-boolean v0, p0, Lcjt;->cq:Z

    if-eqz v0, :cond_4

    .line 1009
    iget-object v0, p0, Lcjt;->mCookiesLock:Ldkq;

    iget-object v1, p0, Lcjt;->aZi:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Ldkq;->unregisterObserver(Ljava/lang/Object;)V

    .line 1010
    iget-object v0, p0, Lcjt;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->d(Ldkt;)V

    .line 1012
    iget-object v0, p0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {v0, v3}, Ldby;->dc(Z)V

    .line 1014
    iget-object v0, p0, Lcjt;->mVoiceSearchController:Lhlf;

    if-eqz v0, :cond_1

    .line 1015
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aam()Ldcg;

    move-result-object v0

    invoke-virtual {v0}, Ldcg;->WS()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1016
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    invoke-virtual {v0, v4}, Ldct;->fE(I)V

    .line 1018
    iget-object v0, p0, Lcjt;->mVoiceSearchController:Lhlf;

    invoke-virtual {v0, v2, v2}, Lhlf;->I(ZZ)V

    .line 1020
    :cond_0
    iget-object v0, p0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPk()Lgkg;

    move-result-object v0

    invoke-virtual {v0}, Lgkg;->aGU()V

    .line 1022
    :cond_1
    iget-object v0, p0, Lcjt;->aZl:Lclq;

    if-eqz v0, :cond_2

    .line 1023
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    invoke-virtual {v0, v4}, Ldct;->fE(I)V

    .line 1025
    iget-object v0, p0, Lcjt;->aZl:Lclq;

    invoke-virtual {v0}, Lclq;->cancel()V

    .line 1026
    const/4 v0, 0x0

    iput-object v0, p0, Lcjt;->aZl:Lclq;

    .line 1028
    :cond_2
    iget-object v0, p0, Lcjt;->aZk:Ldpe;

    if-eqz v0, :cond_3

    .line 1029
    iget-object v0, p0, Lcjt;->aZk:Ldpe;

    invoke-virtual {v0}, Ldpe;->cancel()V

    .line 1033
    :cond_3
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    invoke-virtual {v0}, Ldcw;->Zr()V

    .line 1035
    iget-object v0, p0, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DG()Ldkx;

    move-result-object v0

    invoke-virtual {v0}, Ldkx;->Ce()V

    .line 1040
    invoke-direct {p0}, Lcjt;->Nd()V

    .line 1042
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    .line 1044
    iget-object v0, p0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZG()V

    .line 1048
    invoke-direct {p0}, Lcjt;->MU()V

    .line 1051
    iget-object v0, p0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjt;->aZj:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->e(Lcom/google/android/shared/search/Query;Z)V

    .line 1053
    iput-boolean v3, p0, Lcjt;->cq:Z

    .line 1055
    iget-object v0, p0, Lcjt;->mUrlHelper:Lcpn;

    invoke-virtual {v0}, Lcpn;->onStopped()V

    .line 1056
    iget-object v0, p0, Lcjt;->mAudioWorker:Ldnz;

    invoke-virtual {v0}, Ldnz;->onStopped()V

    .line 1057
    iget-object v0, p0, Lcjt;->mHotwordWorker:Ldoj;

    invoke-virtual {v0}, Ldoj;->onStopped()V

    .line 1058
    iget-object v0, p0, Lcjt;->mServiceWorker:Ldpb;

    iget-object v0, v0, Ldpb;->bEx:Ldog;

    invoke-virtual {v0}, Ldog;->onStopped()V

    .line 1060
    :cond_4
    return-void
.end method

.method public final w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1604
    iget-object v0, p0, Lcjt;->mMessageManager:Lbxy;

    invoke-virtual {v0, p1, p2}, Lbxy;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    return-void
.end method
