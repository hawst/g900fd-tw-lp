.class final Lgbl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field private final cEX:Landroid/net/Uri;

.field private final cEY:Landroid/view/View;

.field private final cEZ:Z

.field private final cFa:Ljava/util/List;

.field private final cFb:Ljava/util/List;

.field private final cvM:Ljava/lang/String;

.field private final mAccount:Landroid/accounts/Account;

.field private final mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V
    .locals 0
    .param p5    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput-object p1, p0, Lgbl;->mActivity:Landroid/app/Activity;

    .line 261
    iput-object p2, p0, Lgbl;->cvM:Ljava/lang/String;

    .line 262
    iput-object p3, p0, Lgbl;->mAccount:Landroid/accounts/Account;

    .line 263
    iput-object p4, p0, Lgbl;->cEX:Landroid/net/Uri;

    .line 264
    iput-object p5, p0, Lgbl;->cEY:Landroid/view/View;

    .line 265
    iput-boolean p6, p0, Lgbl;->cEZ:Z

    .line 266
    iput-object p7, p0, Lgbl;->cFa:Ljava/util/List;

    .line 267
    iput-object p8, p0, Lgbl;->cFb:Ljava/util/List;

    .line 268
    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 272
    iget-object v0, p0, Lgbl;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lgbl;->cvM:Ljava/lang/String;

    iget-object v2, p0, Lgbl;->mAccount:Landroid/accounts/Account;

    iget-object v3, p0, Lgbl;->cEX:Landroid/net/Uri;

    iget-object v4, p0, Lgbl;->cEY:Landroid/view/View;

    iget-boolean v5, p0, Lgbl;->cEZ:Z

    iget-object v6, p0, Lgbl;->cFa:Ljava/util/List;

    iget-object v7, p0, Lgbl;->cFb:Ljava/util/List;

    invoke-static/range {v0 .. v7}, Lgbk;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V

    .line 275
    const/4 v0, 0x1

    return v0
.end method
