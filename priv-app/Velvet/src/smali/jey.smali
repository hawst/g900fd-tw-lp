.class public final Ljey;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile egn:[Ljey;


# instance fields
.field private aeU:Ljava/lang/String;

.field private aez:I

.field private efY:Z

.field private ego:Ljava/lang/String;

.field private egp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9759
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 9760
    iput v1, p0, Ljey;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljey;->aeU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljey;->ego:Ljava/lang/String;

    iput-boolean v1, p0, Ljey;->efY:Z

    const-string v0, ""

    iput-object v0, p0, Ljey;->egp:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljey;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljey;->eCz:I

    .line 9761
    return-void
.end method

.method public static bjj()[Ljey;
    .locals 2

    .prologue
    .line 9661
    sget-object v0, Ljey;->egn:[Ljey;

    if-nez v0, :cond_1

    .line 9662
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 9664
    :try_start_0
    sget-object v0, Ljey;->egn:[Ljey;

    if-nez v0, :cond_0

    .line 9665
    const/4 v0, 0x0

    new-array v0, v0, [Ljey;

    sput-object v0, Ljey;->egn:[Ljey;

    .line 9667
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9669
    :cond_1
    sget-object v0, Ljey;->egn:[Ljey;

    return-object v0

    .line 9667
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9655
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljey;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljey;->aeU:Ljava/lang/String;

    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljey;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljey;->efY:Z

    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljey;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljey;->egp:Ljava/lang/String;

    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljey;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljey;->ego:Ljava/lang/String;

    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljey;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 9777
    iget v0, p0, Ljey;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9778
    const/4 v0, 0x1

    iget-object v1, p0, Ljey;->aeU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9780
    :cond_0
    iget v0, p0, Ljey;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 9781
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljey;->efY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9783
    :cond_1
    iget v0, p0, Ljey;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 9784
    const/4 v0, 0x3

    iget-object v1, p0, Ljey;->egp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9786
    :cond_2
    iget v0, p0, Ljey;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 9787
    const/4 v0, 0x4

    iget-object v1, p0, Ljey;->ego:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9789
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9790
    return-void
.end method

.method public final bjf()Z
    .locals 1

    .prologue
    .line 9721
    iget-boolean v0, p0, Ljey;->efY:Z

    return v0
.end method

.method public final bjk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9699
    iget-object v0, p0, Ljey;->ego:Ljava/lang/String;

    return-object v0
.end method

.method public final hX(Z)Ljey;
    .locals 1

    .prologue
    .line 9724
    iput-boolean p1, p0, Ljey;->efY:Z

    .line 9725
    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljey;->aez:I

    .line 9726
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 9794
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9795
    iget v1, p0, Ljey;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9796
    const/4 v1, 0x1

    iget-object v2, p0, Ljey;->aeU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9799
    :cond_0
    iget v1, p0, Ljey;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 9800
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljey;->efY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9803
    :cond_1
    iget v1, p0, Ljey;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 9804
    const/4 v1, 0x3

    iget-object v2, p0, Ljey;->egp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9807
    :cond_2
    iget v1, p0, Ljey;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 9808
    const/4 v1, 0x4

    iget-object v2, p0, Ljey;->ego:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9811
    :cond_3
    return v0
.end method

.method public final ul(Ljava/lang/String;)Ljey;
    .locals 1

    .prologue
    .line 9702
    if-nez p1, :cond_0

    .line 9703
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9705
    :cond_0
    iput-object p1, p0, Ljey;->ego:Ljava/lang/String;

    .line 9706
    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljey;->aez:I

    .line 9707
    return-object p0
.end method

.method public final um(Ljava/lang/String;)Ljey;
    .locals 1

    .prologue
    .line 9743
    if-nez p1, :cond_0

    .line 9744
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9746
    :cond_0
    iput-object p1, p0, Ljey;->egp:Ljava/lang/String;

    .line 9747
    iget v0, p0, Ljey;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljey;->aez:I

    .line 9748
    return-object p0
.end method
