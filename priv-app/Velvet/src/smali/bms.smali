.class final Lbms;
.super Lbit;


# instance fields
.field private final aAJ:Landroid/app/PendingIntent;

.field private final aAK:I

.field private final aFG:[Ljava/lang/String;

.field private final aFL:I


# direct methods
.method public constructor <init>(Lbmo;ILbrl;ILandroid/app/PendingIntent;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p3}, Lbit;-><init>(Lbir;Ljava/lang/Object;)V

    invoke-static {v0}, Lbip;->bF(Z)V

    iput v0, p0, Lbms;->aFL:I

    invoke-static {p4}, Lbrn;->ec(I)I

    move-result v0

    iput v0, p0, Lbms;->aAK:I

    iput-object p5, p0, Lbms;->aAJ:Landroid/app/PendingIntent;

    const/4 v0, 0x0

    iput-object v0, p0, Lbms;->aFG:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbmo;ILbrl;I[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p3}, Lbit;-><init>(Lbir;Ljava/lang/Object;)V

    const/4 v0, 0x1

    invoke-static {v0}, Lbip;->bF(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lbms;->aFL:I

    invoke-static {p4}, Lbrn;->ec(I)I

    move-result v0

    iput v0, p0, Lbms;->aAK:I

    iput-object p5, p0, Lbms;->aFG:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lbms;->aAJ:Landroid/app/PendingIntent;

    return-void
.end method


# virtual methods
.method protected final synthetic an(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lbrl;

    if-eqz p1, :cond_0

    iget v0, p0, Lbms;->aFL:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "LocationClientImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbms;->aFL:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lbms;->aAK:I

    iget-object v0, p0, Lbms;->aAJ:Landroid/app/PendingIntent;

    invoke-interface {p1}, Lbrl;->Af()V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lbms;->aAK:I

    iget-object v1, p0, Lbms;->aFG:[Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lbrl;->b(I[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final yU()V
    .locals 0

    return-void
.end method
