.class public final Lfvi;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfyj;


# instance fields
.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 37
    iput-object p4, p0, Lfvi;->mTimeToLeaveFactory:Lfyk;

    .line 38
    iput-object p5, p0, Lfvi;->mStringEvaluator:Lgbr;

    .line 39
    return-void
.end method

.method private static a(Landroid/content/Context;Ljal;)Ljava/lang/String;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p1}, Ljal;->bel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljal;->bek()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 80
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 81
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final M(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {v0}, Lgbf;->a(Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 6

    .prologue
    .line 43
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    .line 45
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    new-instance v2, Lftf;

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iget-object v4, p0, Lfuz;->mCardContainer:Lfmt;

    iget-object v5, p0, Lfvi;->mStringEvaluator:Lgbr;

    invoke-direct {v2, v0, v3, v4, v5}, Lftf;-><init>(Ljal;Lizj;Lfmt;Lgbr;)V

    .line 49
    iget-object v3, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfvi;->mTimeToLeaveFactory:Lfyk;

    invoke-virtual {v2, p1, v3, v4}, Lftf;->a(Landroid/content/Context;Lemp;Lfyk;)Lanh;

    move-result-object v3

    invoke-static {v1, v3}, Lfvi;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 52
    invoke-virtual {v2}, Lftf;->aDp()Lanh;

    move-result-object v3

    invoke-static {v1, v3}, Lfvi;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 54
    invoke-virtual {v2, p1}, Lftf;->aR(Landroid/content/Context;)Lanh;

    move-result-object v3

    .line 55
    if-eqz v3, :cond_1

    .line 56
    invoke-static {p1, v0}, Lfvi;->a(Landroid/content/Context;Ljal;)Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 58
    iget-object v5, v3, Lanh;->agw:Lany;

    invoke-virtual {v5, v4}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 60
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_1
    iget-object v3, p0, Lfuz;->mCardContainer:Lfmt;

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Lftf;->a(Landroid/content/Context;Lfmt;Z)Lanh;

    move-result-object v2

    invoke-static {v1, v2}, Lfvi;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 64
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v0, v0, Ljak;->dMX:[Ljbg;

    const/16 v3, 0x6f

    invoke-static {p1, v2, v0, v3}, Lfsx;->a(Landroid/content/Context;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    invoke-static {v1, v0}, Lfvi;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 68
    new-instance v2, Lang;

    invoke-direct {v2}, Lang;-><init>()V

    .line 69
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v2, Lang;->ags:[Lanh;

    .line 71
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lang;->aS(Z)Lang;

    .line 72
    return-object v2
.end method

.method public final aDC()Ljava/lang/CharSequence;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    .line 104
    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_0
    return-object v0

    .line 106
    :cond_0
    invoke-virtual {v0}, Ljbp;->bfb()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {v0}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bd(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {p1, v0}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final be(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    invoke-static {p1, v0}, Lfvi;->a(Landroid/content/Context;Ljal;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
