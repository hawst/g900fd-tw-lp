.class public final Ljwt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eJD:[Ljwt;


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private eyZ:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 587
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 588
    iput v1, p0, Ljwt;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljwt;->aeE:Ljava/lang/String;

    iput v1, p0, Ljwt;->eyZ:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljwt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwt;->eCz:I

    .line 589
    return-void
.end method

.method public static bvy()[Ljwt;
    .locals 2

    .prologue
    .line 533
    sget-object v0, Ljwt;->eJD:[Ljwt;

    if-nez v0, :cond_1

    .line 534
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 536
    :try_start_0
    sget-object v0, Ljwt;->eJD:[Ljwt;

    if-nez v0, :cond_0

    .line 537
    const/4 v0, 0x0

    new-array v0, v0, [Ljwt;

    sput-object v0, Ljwt;->eJD:[Ljwt;

    .line 539
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    :cond_1
    sget-object v0, Ljwt;->eJD:[Ljwt;

    return-object v0

    .line 539
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 523
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwt;->aeE:Ljava/lang/String;

    iget v0, p0, Ljwt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwt;->eyZ:I

    iget v0, p0, Ljwt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwt;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 603
    iget v0, p0, Ljwt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 604
    const/4 v0, 0x1

    iget-object v1, p0, Ljwt;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 606
    :cond_0
    iget v0, p0, Ljwt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 607
    const/4 v0, 0x2

    iget v1, p0, Ljwt;->eyZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 609
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 610
    return-void
.end method

.method public final getFormat()I
    .locals 1

    .prologue
    .line 571
    iget v0, p0, Ljwt;->eyZ:I

    return v0
.end method

.method public final getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Ljwt;->aeE:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 614
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 615
    iget v1, p0, Ljwt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 616
    const/4 v1, 0x1

    iget-object v2, p0, Ljwt;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 619
    :cond_0
    iget v1, p0, Ljwt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 620
    const/4 v1, 0x2

    iget v2, p0, Ljwt;->eyZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 623
    :cond_1
    return v0
.end method

.method public final sM(I)Ljwt;
    .locals 1

    .prologue
    .line 574
    iput p1, p0, Ljwt;->eyZ:I

    .line 575
    iget v0, p0, Ljwt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwt;->aez:I

    .line 576
    return-object p0
.end method

.method public final zy(Ljava/lang/String;)Ljwt;
    .locals 1

    .prologue
    .line 552
    if-nez p1, :cond_0

    .line 553
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 555
    :cond_0
    iput-object p1, p0, Ljwt;->aeE:Ljava/lang/String;

    .line 556
    iget v0, p0, Ljwt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwt;->aez:I

    .line 557
    return-object p0
.end method
