.class public final Ljhh;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afX:Ljava/lang/String;

.field private afZ:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private akt:Ljava/lang/String;

.field private aku:Z

.field private dZD:Ljava/lang/String;

.field public dZG:[Ljdq;

.field public eeb:[Ljcn;

.field private elF:Ljava/lang/String;

.field private elG:Ljava/lang/String;

.field public elH:[Ljhi;

.field private elI:I

.field private elJ:Z

.field private elK:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Ljhh;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljhh;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljhh;->afX:Ljava/lang/String;

    invoke-static {}, Ljcn;->bgG()[Ljcn;

    move-result-object v0

    iput-object v0, p0, Ljhh;->eeb:[Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljhh;->afZ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljhh;->elF:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljhh;->elG:Ljava/lang/String;

    invoke-static {}, Ljhi;->blL()[Ljhi;

    move-result-object v0

    iput-object v0, p0, Ljhh;->elH:[Ljhi;

    const-string v0, ""

    iput-object v0, p0, Ljhh;->akt:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljhh;->elI:I

    const-string v0, ""

    iput-object v0, p0, Ljhh;->dZD:Ljava/lang/String;

    invoke-static {}, Ljdq;->bhK()[Ljdq;

    move-result-object v0

    iput-object v0, p0, Ljhh;->dZG:[Ljdq;

    const-string v0, ""

    iput-object v0, p0, Ljhh;->aiK:Ljava/lang/String;

    iput-boolean v1, p0, Ljhh;->elJ:Z

    const-string v0, ""

    iput-object v0, p0, Ljhh;->elK:Ljava/lang/String;

    iput-boolean v1, p0, Ljhh;->aku:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljhh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhh;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->afh:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->afX:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhh;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhh;->eeb:[Ljcn;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljcn;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljhh;->eeb:[Ljcn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljcn;

    invoke-direct {v3}, Ljcn;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljhh;->eeb:[Ljcn;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljcn;

    invoke-direct {v3}, Ljcn;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhh;->eeb:[Ljcn;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->afZ:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhh;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->elF:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhh;->aez:I

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhh;->elH:[Ljhi;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhi;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljhh;->elH:[Ljhi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljhi;

    invoke-direct {v3}, Ljhi;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljhh;->elH:[Ljhi;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljhi;

    invoke-direct {v3}, Ljhi;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhh;->elH:[Ljhi;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->akt:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljhh;->elI:I

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->dZD:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhh;->dZG:[Ljdq;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdq;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljhh;->dZG:[Ljdq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Ljdq;

    invoke-direct {v3}, Ljdq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljhh;->dZG:[Ljdq;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Ljdq;

    invoke-direct {v3}, Ljdq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhh;->dZG:[Ljdq;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->aiK:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljhh;->elJ:Z

    iget v0, p0, Ljhh;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->elK:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhh;->elG:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljhh;->aku:Z

    iget v0, p0, Ljhh;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljhh;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
        0x58 -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x78 -> :sswitch_c
        0x82 -> :sswitch_d
        0x8a -> :sswitch_e
        0x90 -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v2, p0, Ljhh;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Ljhh;->afX:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljhh;->eeb:[Ljcn;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljhh;->eeb:[Ljcn;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljhh;->eeb:[Ljcn;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljhh;->eeb:[Ljcn;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget-object v2, p0, Ljhh;->afZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_4
    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget-object v2, p0, Ljhh;->elF:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p0, Ljhh;->elH:[Ljhi;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljhh;->elH:[Ljhi;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljhh;->elH:[Ljhi;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ljhh;->elH:[Ljhi;

    aget-object v2, v2, v0

    if-eqz v2, :cond_6

    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-object v2, p0, Ljhh;->akt:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_8
    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget v2, p0, Ljhh;->elI:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    :cond_9
    iget v0, p0, Ljhh;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    iget-object v2, p0, Ljhh;->dZD:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_a
    iget-object v0, p0, Ljhh;->dZG:[Ljdq;

    if-eqz v0, :cond_c

    iget-object v0, p0, Ljhh;->dZG:[Ljdq;

    array-length v0, v0

    if-lez v0, :cond_c

    :goto_2
    iget-object v0, p0, Ljhh;->dZG:[Ljdq;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    iget-object v0, p0, Ljhh;->dZG:[Ljdq;

    aget-object v0, v0, v1

    if-eqz v0, :cond_b

    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_c
    iget v0, p0, Ljhh;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    iget-object v1, p0, Ljhh;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_d
    iget v0, p0, Ljhh;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    iget-boolean v1, p0, Ljhh;->elJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_e
    iget v0, p0, Ljhh;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    iget-object v1, p0, Ljhh;->elK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_f
    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    iget-object v1, p0, Ljhh;->elG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_10
    iget v0, p0, Ljhh;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_11

    const/16 v0, 0x12

    iget-boolean v1, p0, Ljhh;->aku:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_11
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bfK()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->dZD:Ljava/lang/String;

    return-object v0
.end method

.method public final bfL()Z
    .locals 1

    iget v0, p0, Ljhh;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final blI()I
    .locals 1

    iget v0, p0, Ljhh;->elI:I

    return v0
.end method

.method public final blJ()Z
    .locals 1

    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final blK()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->elK:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Ljhh;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljhh;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Ljhh;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Ljhh;->afX:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Ljhh;->eeb:[Ljcn;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljhh;->eeb:[Ljcn;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ljhh;->eeb:[Ljcn;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Ljhh;->eeb:[Ljcn;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    :cond_4
    iget v2, p0, Ljhh;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    iget-object v3, p0, Ljhh;->afZ:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Ljhh;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_6

    const/4 v2, 0x5

    iget-object v3, p0, Ljhh;->elF:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Ljhh;->elH:[Ljhi;

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljhh;->elH:[Ljhi;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Ljhh;->elH:[Ljhi;

    array-length v3, v3

    if-ge v0, v3, :cond_8

    iget-object v3, p0, Ljhh;->elH:[Ljhi;

    aget-object v3, v3, v0

    if-eqz v3, :cond_7

    const/16 v4, 0x9

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    move v0, v2

    :cond_9
    iget v2, p0, Ljhh;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_a

    const/16 v2, 0xa

    iget-object v3, p0, Ljhh;->akt:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget v2, p0, Ljhh;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_b

    const/16 v2, 0xb

    iget v3, p0, Ljhh;->elI:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Ljhh;->aez:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_c

    const/16 v2, 0xc

    iget-object v3, p0, Ljhh;->dZD:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Ljhh;->dZG:[Ljdq;

    if-eqz v2, :cond_e

    iget-object v2, p0, Ljhh;->dZG:[Ljdq;

    array-length v2, v2

    if-lez v2, :cond_e

    :goto_2
    iget-object v2, p0, Ljhh;->dZG:[Ljdq;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    iget-object v2, p0, Ljhh;->dZG:[Ljdq;

    aget-object v2, v2, v1

    if-eqz v2, :cond_d

    const/16 v3, 0xd

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_e
    iget v1, p0, Ljhh;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_f

    const/16 v1, 0xe

    iget-object v2, p0, Ljhh;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Ljhh;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_10

    const/16 v1, 0xf

    iget-boolean v2, p0, Ljhh;->elJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_10
    iget v1, p0, Ljhh;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_11

    const/16 v1, 0x10

    iget-object v2, p0, Ljhh;->elK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Ljhh;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_12

    const/16 v1, 0x11

    iget-object v2, p0, Ljhh;->elG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget v1, p0, Ljhh;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_13

    const/16 v1, 0x12

    iget-boolean v2, p0, Ljhh;->aku:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_13
    return v0
.end method

.method public final ol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->afX:Ljava/lang/String;

    return-object v0
.end method

.method public final om()Z
    .locals 1

    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->afZ:Ljava/lang/String;

    return-object v0
.end method

.method public final pf()Z
    .locals 1

    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rE()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhh;->akt:Ljava/lang/String;

    return-object v0
.end method

.method public final rF()Z
    .locals 1

    iget v0, p0, Ljhh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rG()Z
    .locals 1

    iget-boolean v0, p0, Ljhh;->aku:Z

    return v0
.end method

.method public final uY(Ljava/lang/String;)Ljhh;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhh;->afh:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhh;->aez:I

    return-object p0
.end method

.method public final uZ(Ljava/lang/String;)Ljhh;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhh;->afX:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhh;->aez:I

    return-object p0
.end method

.method public final va(Ljava/lang/String;)Ljhh;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhh;->afZ:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhh;->aez:I

    return-object p0
.end method

.method public final vb(Ljava/lang/String;)Ljhh;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhh;->akt:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljhh;->aez:I

    return-object p0
.end method

.method public final vc(Ljava/lang/String;)Ljhh;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhh;->aiK:Ljava/lang/String;

    iget v0, p0, Ljhh;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljhh;->aez:I

    return-object p0
.end method
