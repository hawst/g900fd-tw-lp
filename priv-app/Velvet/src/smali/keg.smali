.class final Lkeg;
.super Ljava/io/OutputStream;
.source "PG"


# static fields
.field private static synthetic $assertionsDisabled:Z


# instance fields
.field private final buffer:[B

.field private eQb:Z

.field private eUw:Z

.field private eUx:I

.field private synthetic eUy:Lkee;

.field private pos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 556
    const-class v0, Lkee;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkeg;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lkee;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 556
    iput-object p1, p0, Lkeg;->eUy:Lkee;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 557
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lkeg;->buffer:[B

    .line 558
    iput v1, p0, Lkeg;->pos:I

    .line 574
    iput v1, p0, Lkeg;->eUx:I

    return-void
.end method

.method synthetic constructor <init>(Lkee;B)V
    .locals 0

    .prologue
    .line 556
    invoke-direct {p0, p1}, Lkeg;-><init>(Lkee;)V

    return-void
.end method

.method private O(IZ)V
    .locals 3

    .prologue
    .line 641
    :cond_0
    :try_start_0
    iget v0, p0, Lkeg;->eUx:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lkeg;->eUy:Lkee;

    invoke-static {v1}, Lkee;->h(Lkee;)I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 642
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 645
    if-nez p2, :cond_1

    iget-boolean v0, p0, Lkeg;->eQb:Z

    if-eqz v0, :cond_1

    .line 646
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 654
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 647
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lkeg;->eUw:Z

    if-eqz v0, :cond_2

    .line 648
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 649
    :cond_2
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 650
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stream was reset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lkeg;->eUy:Lkee;

    invoke-static {v2}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 655
    :cond_3
    return-void
.end method

.method static synthetic a(Lkeg;I)I
    .locals 1

    .prologue
    .line 556
    iget v0, p0, Lkeg;->eUx:I

    sub-int/2addr v0, p1

    iput v0, p0, Lkeg;->eUx:I

    return v0
.end method

.method static synthetic a(Lkeg;)Z
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lkeg;->eUw:Z

    return v0
.end method

.method static synthetic a(Lkeg;Z)Z
    .locals 0

    .prologue
    .line 556
    iput-boolean p1, p0, Lkeg;->eUw:Z

    return p1
.end method

.method static synthetic b(Lkeg;)Z
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lkeg;->eQb:Z

    return v0
.end method

.method private bxV()V
    .locals 4

    .prologue
    .line 659
    iget-object v1, p0, Lkeg;->eUy:Lkee;

    monitor-enter v1

    .line 660
    :try_start_0
    iget-boolean v0, p0, Lkeg;->eQb:Z

    if-eqz v0, :cond_0

    .line 661
    new-instance v0, Ljava/io/IOException;

    const-string v2, "stream closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 662
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lkeg;->eUw:Z

    if-eqz v0, :cond_1

    .line 663
    new-instance v0, Ljava/io/IOException;

    const-string v2, "stream finished"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 664
    :cond_1
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 665
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stream was reset: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lkeg;->eUy:Lkee;

    invoke-static {v3}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 667
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private jM(Z)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 622
    sget-boolean v0, Lkeg;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 624
    :cond_0
    iget v0, p0, Lkeg;->pos:I

    .line 625
    iget-object v1, p0, Lkeg;->eUy:Lkee;

    monitor-enter v1

    .line 626
    :try_start_0
    invoke-direct {p0, v0, p1}, Lkeg;->O(IZ)V

    .line 627
    iget v2, p0, Lkeg;->eUx:I

    add-int/2addr v0, v2

    iput v0, p0, Lkeg;->eUx:I

    .line 628
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->c(Lkee;)Lkdx;

    move-result-object v0

    iget-object v1, p0, Lkeg;->eUy:Lkee;

    invoke-static {v1}, Lkee;->b(Lkee;)I

    move-result v1

    iget-object v3, p0, Lkeg;->buffer:[B

    iget v5, p0, Lkeg;->pos:I

    move v2, p1

    invoke-virtual/range {v0 .. v5}, Lkdx;->a(IZ[BII)V

    .line 630
    iput v4, p0, Lkeg;->pos:I

    .line 631
    return-void

    .line 628
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final close()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 607
    sget-boolean v0, Lkeg;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 608
    :cond_0
    iget-object v1, p0, Lkeg;->eUy:Lkee;

    monitor-enter v1

    .line 609
    :try_start_0
    iget-boolean v0, p0, Lkeg;->eQb:Z

    if-eqz v0, :cond_1

    .line 610
    monitor-exit v1

    .line 619
    :goto_0
    return-void

    .line 612
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkeg;->eQb:Z

    .line 613
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->g(Lkee;)Lkeg;

    move-result-object v0

    iget-boolean v0, v0, Lkeg;->eUw:Z

    if-nez v0, :cond_2

    .line 615
    invoke-direct {p0, v2}, Lkeg;->jM(Z)V

    .line 617
    :cond_2
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->c(Lkee;)Lkdx;

    move-result-object v0

    invoke-virtual {v0}, Lkdx;->flush()V

    .line 618
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->f(Lkee;)V

    goto :goto_0

    .line 613
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 598
    sget-boolean v0, Lkeg;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 599
    :cond_0
    invoke-direct {p0}, Lkeg;->bxV()V

    .line 600
    iget v0, p0, Lkeg;->pos:I

    if-lez v0, :cond_1

    .line 601
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkeg;->jM(Z)V

    .line 602
    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Lkee;->c(Lkee;)Lkdx;

    move-result-object v0

    invoke-virtual {v0}, Lkdx;->flush()V

    .line 604
    :cond_1
    return-void
.end method

.method public final write(I)V
    .locals 0

    .prologue
    .line 577
    invoke-static {p0, p1}, Lkbt;->a(Ljava/io/OutputStream;I)V

    .line 578
    return-void
.end method

.method public final write([BII)V
    .locals 3

    .prologue
    .line 581
    sget-boolean v0, Lkeg;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lkeg;->eUy:Lkee;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 582
    :cond_0
    array-length v0, p1

    invoke-static {v0, p2, p3}, Lkbt;->D(III)V

    .line 583
    invoke-direct {p0}, Lkeg;->bxV()V

    .line 585
    :goto_0
    if-lez p3, :cond_2

    .line 586
    iget v0, p0, Lkeg;->pos:I

    iget-object v1, p0, Lkeg;->buffer:[B

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 587
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkeg;->jM(Z)V

    .line 589
    :cond_1
    iget-object v0, p0, Lkeg;->buffer:[B

    array-length v0, v0

    iget v1, p0, Lkeg;->pos:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 590
    iget-object v1, p0, Lkeg;->buffer:[B

    iget v2, p0, Lkeg;->pos:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 591
    iget v1, p0, Lkeg;->pos:I

    add-int/2addr v1, v0

    iput v1, p0, Lkeg;->pos:I

    .line 592
    add-int/2addr p2, v0

    .line 593
    sub-int/2addr p3, v0

    .line 594
    goto :goto_0

    .line 595
    :cond_2
    return-void
.end method
