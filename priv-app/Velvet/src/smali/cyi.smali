.class public Lcyi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcyg;


# instance fields
.field aVd:Ljava/util/Map;

.field aXn:I

.field private final bmR:Ljava/io/File;

.field final bmS:Ljava/lang/Object;

.field bmT:Z

.field final bmU:Ljava/util/Set;

.field bmV:Lcyn;

.field final dK:Ljava/lang/Object;

.field private final mFile:Ljava/io/File;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method protected constructor <init>(Ljava/io/File;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcyi;->dK:Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcyi;->dK:Ljava/lang/Object;

    iput-object v0, p0, Lcyi;->bmS:Ljava/lang/Object;

    .line 114
    iput-object p1, p0, Lcyi;->mFile:Ljava/io/File;

    .line 115
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".bak"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcyi;->bmR:Ljava/io/File;

    .line 116
    iput-object p2, p0, Lcyi;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 117
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcyi;->bmU:Ljava/util/Set;

    .line 118
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    .line 120
    new-instance v0, Lcyn;

    invoke-direct {v0, p0}, Lcyn;-><init>(Lcyi;)V

    iput-object v0, p0, Lcyi;->bmV:Lcyn;

    .line 121
    return-void
.end method

.method private TZ()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 289
    iget-boolean v0, p0, Lcyi;->bmT:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 291
    :goto_1
    iget-boolean v0, p0, Lcyi;->bmT:Z

    if-nez v0, :cond_1

    .line 293
    :try_start_0
    iget-object v0, p0, Lcyi;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 296
    :catch_0
    move-exception v0

    move v2, v1

    .line 297
    goto :goto_1

    :cond_0
    move v0, v2

    .line 289
    goto :goto_0

    .line 299
    :cond_1
    if-eqz v2, :cond_2

    .line 300
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 302
    :cond_2
    return-void
.end method

.method private Uc()Ljava/util/Map;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 550
    iget-object v0, p0, Lcyi;->bmR:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 552
    iget-object v0, p0, Lcyi;->bmR:Ljava/io/File;

    iget-object v2, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    const-string v0, "Search.SharedPreferencesProto"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to rename backup file to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 590
    :goto_0
    return-object v0

    .line 561
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcyi;->c(Ljava/io/File;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 563
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v0

    .line 565
    add-int/lit8 v0, v0, 0x1

    const/16 v4, 0x800

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v4, v0, [B

    .line 566
    invoke-virtual {v2, v4}, Ljava/io/FileInputStream;->read([B)I

    move-result v0

    move v6, v0

    move-object v0, v4

    move v4, v3

    move v3, v6

    .line 567
    :goto_1
    if-ltz v3, :cond_2

    .line 568
    add-int/2addr v4, v3

    .line 569
    array-length v3, v0

    if-ne v4, v3, :cond_1

    .line 570
    array-length v3, v0

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 572
    :cond_1
    array-length v3, v0

    sub-int/2addr v3, v4

    invoke-virtual {v2, v0, v4, v3}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    goto :goto_1

    .line 576
    :cond_2
    new-instance v3, Lcye;

    invoke-direct {v3}, Lcye;-><init>()V

    const/4 v5, 0x0

    invoke-static {v3, v0, v5, v4}, Ljsr;->b(Ljsr;[BII)Ljsr;

    move-result-object v0

    check-cast v0, Lcye;

    .line 578
    invoke-static {v0}, Lcyi;->a(Lcye;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 588
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 581
    :catch_0
    move-exception v0

    :goto_2
    :try_start_2
    const-string v0, "Search.SharedPreferencesProto"

    const-string v2, "load shared preferences: file not found"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 588
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 583
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 584
    :goto_3
    :try_start_3
    const-string v3, "Search.SharedPreferencesProto"

    const-string v4, "load shared preferences"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 588
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    :goto_4
    move-object v0, v1

    .line 590
    goto :goto_0

    .line 585
    :catch_2
    move-exception v0

    move-object v2, v1

    .line 586
    :goto_5
    :try_start_4
    const-string v3, "Search.SharedPreferencesProto"

    const-string v4, "load shared preferences"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 588
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_6
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_6

    .line 585
    :catch_3
    move-exception v0

    goto :goto_5

    .line 583
    :catch_4
    move-exception v0

    goto :goto_3

    .line 581
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public static a(Ljava/io/File;Ljava/util/concurrent/Executor;)Lcyi;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcyi;

    invoke-direct {v0, p0, p1}, Lcyi;-><init>(Ljava/io/File;Ljava/util/concurrent/Executor;)V

    .line 104
    invoke-virtual {v0}, Lcyi;->AO()V

    .line 105
    return-object v0
.end method

.method private static a(Lcye;)Ljava/util/Map;
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 311
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 312
    iget-object v6, p0, Lcye;->bmG:[Lcyf;

    array-length v7, v6

    move v3, v4

    :goto_0
    if-ge v3, v7, :cond_c

    aget-object v8, v6, v3

    .line 313
    invoke-virtual {v8}, Lcyf;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcyf;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 314
    :goto_1
    invoke-virtual {v8}, Lcyf;->TP()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 315
    invoke-virtual {v8}, Lcyf;->TO()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 313
    goto :goto_1

    .line 316
    :cond_1
    invoke-virtual {v8}, Lcyf;->TR()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 317
    invoke-virtual {v8}, Lcyf;->TQ()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 318
    :cond_2
    invoke-virtual {v8}, Lcyf;->TS()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 319
    invoke-virtual {v8}, Lcyf;->getIntValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 320
    :cond_3
    invoke-virtual {v8}, Lcyf;->TU()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 321
    invoke-virtual {v8}, Lcyf;->TT()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 322
    :cond_4
    invoke-virtual {v8}, Lcyf;->TW()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 323
    invoke-virtual {v8}, Lcyf;->TV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 324
    :cond_5
    iget-object v2, v8, Lcyf;->bmO:[Ljava/lang/String;

    array-length v2, v2

    if-eqz v2, :cond_9

    .line 325
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 326
    iget-object v2, v8, Lcyf;->bmO:[Ljava/lang/String;

    aget-object v2, v2, v4

    .line 327
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_6

    .line 328
    const-string v10, "null"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 329
    invoke-interface {v9, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 335
    :cond_6
    iget-object v2, v8, Lcyf;->bmO:[Ljava/lang/String;

    array-length v10, v2

    .line 336
    const/4 v2, 0x1

    :goto_3
    if-eq v2, v10, :cond_8

    .line 337
    iget-object v11, v8, Lcyf;->bmO:[Ljava/lang/String;

    aget-object v11, v11, v2

    invoke-interface {v9, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 331
    :cond_7
    const-string v8, "Search.SharedPreferencesProto"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "dataToMap: invalid nullTag: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "->"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 339
    :cond_8
    invoke-virtual {v5, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 340
    :cond_9
    invoke-virtual {v8}, Lcyf;->TY()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 341
    new-instance v2, Leqi;

    invoke-virtual {v8}, Lcyf;->TX()[B

    move-result-object v8

    invoke-direct {v2, v8}, Leqi;-><init>([B)V

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 342
    :cond_a
    iget-object v2, v8, Lcyf;->bmQ:[I

    array-length v2, v2

    if-eqz v2, :cond_b

    .line 343
    new-instance v2, Leqj;

    iget-object v8, v8, Lcyf;->bmQ:[I

    invoke-direct {v2, v8}, Leqj;-><init>([I)V

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 346
    :cond_b
    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 349
    :cond_c
    return-object v5
.end method

.method private static i(Ljava/util/Map;)Lcye;
    .locals 8

    .prologue
    .line 360
    new-instance v2, Lcye;

    invoke-direct {v2}, Lcye;-><init>()V

    .line 361
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 364
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 365
    new-instance v5, Lcyf;

    invoke-direct {v5}, Lcyf;-><init>()V

    .line 367
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 368
    if-eqz v1, :cond_0

    .line 369
    invoke-virtual {v5, v1}, Lcyf;->iZ(Ljava/lang/String;)Lcyf;

    .line 371
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 372
    if-eqz v0, :cond_1

    .line 374
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 375
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v5, v0}, Lcyf;->cP(Z)Lcyf;

    .line 399
    :cond_1
    :goto_1
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 376
    :cond_2
    instance-of v1, v0, Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 377
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v5, v0}, Lcyf;->C(F)Lcyf;

    goto :goto_1

    .line 378
    :cond_3
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 379
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v0}, Lcyf;->ft(I)Lcyf;

    goto :goto_1

    .line 380
    :cond_4
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 381
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Lcyf;->ai(J)Lcyf;

    goto :goto_1

    .line 382
    :cond_5
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 383
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcyf;->ja(Ljava/lang/String;)Lcyf;

    goto :goto_1

    .line 384
    :cond_6
    instance-of v1, v0, Ljava/util/Set;

    if-eqz v1, :cond_8

    .line 385
    check-cast v0, Ljava/util/Set;

    .line 387
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 388
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "null"

    :goto_2
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 391
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v5, Lcyf;->bmO:[Ljava/lang/String;

    goto :goto_1

    .line 388
    :cond_7
    const-string v1, ""

    goto :goto_2

    .line 392
    :cond_8
    instance-of v1, v0, Leqi;

    if-eqz v1, :cond_9

    .line 393
    check-cast v0, Leqi;

    iget-object v0, v0, Leqi;->chr:[B

    invoke-virtual {v5, v0}, Lcyf;->A([B)Lcyf;

    goto :goto_1

    .line 394
    :cond_9
    instance-of v1, v0, Leqj;

    if-eqz v1, :cond_a

    .line 395
    check-cast v0, Leqj;

    iget-object v0, v0, Leqj;->chs:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, v5, Lcyf;->bmQ:[I

    goto/16 :goto_1

    .line 397
    :cond_a
    const-string v1, "Search.SharedPreferencesProto"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mapToData: invalid entry class = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 401
    :cond_b
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcyf;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcyf;

    iput-object v0, v2, Lcye;->bmG:[Lcyf;

    .line 403
    return-object v2
.end method

.method private j(Ljava/util/Map;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 609
    iget-object v1, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 610
    iget-object v1, p0, Lcyi;->bmR:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 611
    iget-object v1, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 624
    :cond_0
    invoke-static {p1}, Lcyi;->i(Ljava/util/Map;)Lcye;

    move-result-object v1

    .line 625
    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    .line 627
    const/4 v2, 0x0

    .line 629
    :try_start_0
    iget-object v3, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {p0, v3}, Lcyi;->d(Ljava/io/File;)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 630
    invoke-virtual {v2, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 631
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 632
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V

    .line 633
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 635
    iget-object v1, p0, Lcyi;->bmR:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    const/4 v0, 0x1

    .line 644
    :goto_0
    return v0

    .line 612
    :cond_1
    iget-object v1, p0, Lcyi;->mFile:Ljava/io/File;

    iget-object v2, p0, Lcyi;->bmR:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 613
    const-string v1, "Search.SharedPreferencesProto"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to rename to backup file "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcyi;->bmR:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 617
    :cond_2
    iget-object v1, p0, Lcyi;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 618
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_0

    .line 619
    const-string v2, "Search.SharedPreferencesProto"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to create shared preferences directory "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 637
    :catch_0
    move-exception v1

    .line 638
    :try_start_1
    const-string v3, "Search.SharedPreferencesProto"

    const-string v4, "exception while writing to file: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 642
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 639
    :catch_1
    move-exception v1

    .line 640
    :try_start_2
    const-string v3, "Search.SharedPreferencesProto"

    const-string v4, "exception while writing to file: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 642
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
.end method

.method private jb(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 264
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 265
    :try_start_0
    iget-boolean v0, p0, Lcyi;->bmT:Z

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    .line 273
    :goto_0
    return-object v0

    .line 268
    :cond_0
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 270
    iget-object v2, p0, Lcyi;->bmS:Ljava/lang/Object;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 272
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcyi;->TZ()V

    .line 273
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final AO()V
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lcyj;

    const-string v1, "Search.SharedPreferencesProto"

    invoke-direct {v0, p0, v1}, Lcyj;-><init>(Lcyi;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcyj;->start()V

    .line 133
    return-void
.end method

.method public final EH()Lcyh;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcyl;

    invoke-direct {v0, p0}, Lcyl;-><init>(Lcyi;)V

    return-object v0
.end method

.method public final EI()V
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    iget v0, p0, Lcyi;->aXn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcyi;->aXn:I

    .line 140
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final EJ()V
    .locals 2

    .prologue
    .line 145
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_0
    iget v0, p0, Lcyi;->aXn:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcyi;->aXn:I

    .line 148
    iget v0, p0, Lcyi;->aXn:I

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcyi;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 151
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final Ua()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 414
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 416
    :try_start_0
    iget-boolean v2, p0, Lcyi;->bmT:Z

    .line 417
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    if-eqz v2, :cond_0

    move-object v2, v0

    .line 421
    :goto_0
    iget-object v5, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v5

    .line 423
    :try_start_1
    iget-boolean v0, p0, Lcyi;->bmT:Z

    iget-boolean v1, p0, Lcyi;->bmT:Z

    if-nez v1, :cond_5

    if-eqz v2, :cond_8

    iget-object v1, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v0

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    iget-object v7, p0, Lcyi;->bmS:Ljava/lang/Object;

    if-ne v0, v7, :cond_1

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v3, v4

    goto :goto_1

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 419
    :cond_0
    invoke-direct {p0}, Lcyi;->Uc()Ljava/util/Map;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 423
    :cond_1
    :try_start_2
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    if-nez v0, :cond_2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    :cond_2
    if-eqz v0, :cond_9

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_3
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v4

    :goto_2
    move v3, v0

    goto :goto_1

    :cond_4
    iput-object v2, p0, Lcyi;->aVd:Ljava/util/Map;

    move v0, v3

    :goto_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcyi;->bmT:Z

    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 424
    :cond_5
    if-nez v0, :cond_6

    .line 427
    iget-object v1, p0, Lcyi;->bmV:Lcyn;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    iget-object v1, p0, Lcyi;->bmV:Lcyn;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcyn;->bne:Z

    .line 429
    iget-object v1, p0, Lcyi;->bmV:Lcyn;

    iget-object v1, v1, Lcyn;->bnc:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 430
    const/4 v1, 0x0

    iput-object v1, p0, Lcyi;->bmV:Lcyn;

    .line 432
    :cond_6
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 434
    if-eqz v0, :cond_7

    .line 436
    invoke-virtual {p0}, Lcyi;->Ub()V

    .line 438
    :cond_7
    return-void

    .line 423
    :cond_8
    :try_start_3
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    iget-object v1, p0, Lcyi;->bmS:Ljava/lang/Object;

    invoke-static {v1}, Lifx;->bg(Ljava/lang/Object;)Lifw;

    move-result-object v1

    invoke-static {v0, v1}, Lior;->a(Ljava/util/Map;Lifw;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v4

    goto :goto_3

    .line 432
    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_9
    move v0, v3

    goto :goto_2
.end method

.method final Ub()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 455
    move v1, v2

    move-object v3, v0

    .line 459
    :goto_0
    iget-object v5, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v5

    .line 460
    if-eqz v3, :cond_1

    .line 462
    :try_start_0
    iget-object v6, v3, Lcyn;->bnd:Ljava/util/Map;

    if-ne v6, v0, :cond_0

    move v0, v4

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 463
    const/4 v0, 0x0

    iput-object v0, v3, Lcyn;->bnd:Ljava/util/Map;

    .line 464
    iput-boolean v1, v3, Lcyn;->bne:Z

    .line 465
    iget-object v0, v3, Lcyn;->bnc:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 466
    iget-object v0, p0, Lcyi;->bmV:Lcyn;

    if-ne v3, v0, :cond_1

    .line 468
    const/4 v0, 0x0

    iput-object v0, p0, Lcyi;->bmV:Lcyn;

    .line 469
    monitor-exit v5

    return-void

    :cond_0
    move v0, v2

    .line 462
    goto :goto_1

    .line 473
    :cond_1
    :goto_2
    iget v0, p0, Lcyi;->aXn:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 476
    :try_start_1
    iget-object v0, p0, Lcyi;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 480
    :catch_0
    move-exception v0

    goto :goto_2

    .line 483
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcyi;->bmV:Lcyn;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    iget-object v0, p0, Lcyi;->bmV:Lcyn;

    iget-object v0, v0, Lcyn;->bnd:Ljava/util/Map;

    if-nez v0, :cond_3

    move v0, v4

    :goto_3
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 485
    iget-object v3, p0, Lcyi;->bmV:Lcyn;

    .line 486
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    .line 487
    iget-object v1, p0, Lcyi;->bmV:Lcyn;

    iput-object v0, v1, Lcyn;->bnd:Ljava/util/Map;

    .line 488
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 489
    invoke-direct {p0, v0}, Lcyi;->j(Ljava/util/Map;)Z

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 484
    goto :goto_3

    .line 488
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0
.end method

.method final a(Ljava/util/Collection;Ljava/util/Set;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 660
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 661
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 663
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 664
    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_3

    .line 665
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 666
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 667
    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    move v0, v2

    .line 660
    goto :goto_0

    :cond_2
    move v1, v2

    .line 661
    goto :goto_1

    .line 671
    :cond_3
    iget-object v0, p0, Lcyi;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcyk;

    const-string v2, "Notify shared preference listeners"

    invoke-direct {v1, p0, v2, p1, p2}, Lcyk;-><init>(Lcyi;Ljava/lang/String;Ljava/util/Collection;Ljava/util/Set;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 678
    :cond_4
    return-void
.end method

.method public final b(Ljava/lang/String;[B)[B
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_0

    check-cast v0, Leqi;

    iget-object v0, v0, Leqi;->chr:[B

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Ljava/io/File;)Ljava/io/FileInputStream;
    .locals 1

    .prologue
    .line 649
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(Ljava/io/File;)Ljava/io/FileOutputStream;
    .locals 1

    .prologue
    .line 654
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public synthetic edit()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcyi;->EH()Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public getAll()Ljava/util/Map;
    .locals 2

    .prologue
    .line 167
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    iget-boolean v0, p0, Lcyi;->bmT:Z

    if-nez v0, :cond_0

    .line 169
    invoke-direct {p0}, Lcyi;->TZ()V

    .line 171
    :cond_0
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-static {v0}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p2

    :cond_0
    return p2
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_0
    return p2
.end method

.method public final getIntArray(Ljava/lang/String;)[I
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    check-cast v0, Leqj;

    iget-object v0, v0, Leqj;->chs:[I

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [I

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcyi;->jb(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_0

    check-cast v0, Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method public final gj(Ljava/lang/String;)Ljava/util/Map;
    .locals 6

    .prologue
    .line 177
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "keyPrefix must be non-empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    iget-object v2, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 182
    :try_start_0
    iget-boolean v0, p0, Lcyi;->bmT:Z

    if-nez v0, :cond_1

    .line 183
    invoke-direct {p0}, Lcyi;->TZ()V

    .line 185
    :cond_1
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v3

    .line 186
    iget-object v0, p0, Lcyi;->aVd:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 187
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 188
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 189
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 192
    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method public registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2

    .prologue
    .line 249
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lcyi;->bmU:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 252
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 2

    .prologue
    .line 258
    iget-object v1, p0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 259
    :try_start_0
    iget-object v0, p0, Lcyi;->bmU:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 260
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
