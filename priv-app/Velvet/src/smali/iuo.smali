.class public final Liuo;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 199
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    .line 290
    const/16 v0, 0xb

    new-array v0, v0, [D

    const/4 v1, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    const-wide v2, 0x42b3077775800000L    # 2.0922789888E13

    aput-wide v2, v0, v1

    const/4 v1, 0x2

    const-wide v2, 0x474956ad0aae33a4L    # 2.631308369336935E35

    aput-wide v2, v0, v1

    const/4 v1, 0x3

    const-wide v2, 0x4c9ee69a78d72cb6L    # 1.2413915592536073E61

    aput-wide v2, v0, v1

    const/4 v1, 0x4

    const-wide v2, 0x526fe478ee34844aL    # 1.2688693218588417E89

    aput-wide v2, v0, v1

    const/4 v1, 0x5

    const-wide v2, 0x589c619094edabffL    # 7.156945704626381E118

    aput-wide v2, v0, v1

    const/4 v1, 0x6

    const-wide v2, 0x5f13638dd7bd6347L    # 9.916779348709496E149

    aput-wide v2, v0, v1

    const/4 v1, 0x7

    const-wide v2, 0x65c7cac197cfe503L    # 1.974506857221074E182

    aput-wide v2, v0, v1

    const/16 v1, 0x8

    const-wide v2, 0x6cb1e5dfc140e1e5L    # 3.856204823625804E215

    aput-wide v2, v0, v1

    const/16 v1, 0x9

    const-wide v2, 0x73c8ce85fadb707eL    # 5.5502938327393044E249

    aput-wide v2, v0, v1

    const/16 v1, 0xa

    const-wide v2, 0x7b095d5f3d928edeL    # 4.7147236359920616E284

    aput-wide v2, v0, v1

    return-void
.end method

.method public static a(DLjava/math/RoundingMode;)I
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    const-wide/16 v4, 0x0

    .line 112
    invoke-static {p0, p1}, Liuq;->l(D)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "input is infinite or NaN"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v2, Liup;->dKV:[I

    invoke-virtual {p2}, Ljava/math/RoundingMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-static {p0, p1}, Liuo;->j(D)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "mode was UNNECESSARY, but rounding was necessary"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    cmpl-double v2, p0, v4

    if-ltz v2, :cond_2

    .line 113
    :cond_1
    :goto_0
    :pswitch_2
    const-wide v2, -0x3e1fffffffe00000L    # -2.147483649E9

    cmpl-double v2, p0, v2

    if-lez v2, :cond_6

    move v2, v0

    :goto_1
    const-wide/high16 v4, 0x41e0000000000000L    # 2.147483648E9

    cmpg-double v3, p0, v4

    if-gez v3, :cond_7

    :goto_2
    and-int/2addr v0, v2

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "not in range"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide p0

    goto :goto_0

    :pswitch_3
    cmpl-double v2, p0, v4

    if-ltz v2, :cond_1

    invoke-static {p0, p1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p0

    goto :goto_0

    :pswitch_4
    cmpl-double v2, p0, v4

    if-ltz v2, :cond_3

    invoke-static {p0, p1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p0

    goto :goto_0

    :cond_3
    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide p0

    goto :goto_0

    :pswitch_5
    invoke-static {p0, p1}, Ljava/lang/Math;->rint(D)D

    move-result-wide p0

    goto :goto_0

    :pswitch_6
    invoke-static {p0, p1}, Liuo;->j(D)Z

    move-result v2

    if-nez v2, :cond_1

    cmpl-double v2, p0, v4

    if-ltz v2, :cond_4

    add-double/2addr p0, v6

    goto :goto_0

    :cond_4
    sub-double/2addr p0, v6

    goto :goto_0

    :pswitch_7
    invoke-static {p0, p1}, Liuo;->j(D)Z

    move-result v2

    if-nez v2, :cond_1

    cmpl-double v2, p0, v4

    if-ltz v2, :cond_5

    add-double v2, p0, v6

    cmpl-double v4, v2, p0

    if-eqz v4, :cond_1

    invoke-static {v2, v3, v1}, Liuq;->a(DZ)D

    move-result-wide p0

    goto :goto_0

    :cond_5
    sub-double v2, p0, v6

    cmpl-double v4, v2, p0

    if-eqz v4, :cond_1

    invoke-static {v2, v3, v0}, Liuq;->a(DZ)D

    move-result-wide p0

    goto :goto_0

    :cond_6
    move v2, v1

    .line 113
    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2

    .line 114
    :cond_8
    double-to-int v0, p0

    return v0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static j(D)Z
    .locals 2

    .prologue
    .line 257
    invoke-static {p0, p1}, Liuq;->l(D)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Liuq;->k(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x34

    invoke-static {p0, p1}, Liuq;->getExponent(D)I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
