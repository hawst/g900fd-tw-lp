.class public final Liei;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dAj:[Liei;


# instance fields
.field private aeU:Ljava/lang/String;

.field private aez:I

.field private afh:Ljava/lang/String;

.field private dAk:Ljava/lang/String;

.field private dAl:I

.field private dAm:[Liej;

.field private dAn:[Ljava/lang/String;

.field private dAo:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3079
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3080
    iput v1, p0, Liei;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liei;->aeU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liei;->dAk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liei;->afh:Ljava/lang/String;

    iput v1, p0, Liei;->dAl:I

    invoke-static {}, Liej;->aVH()[Liej;

    move-result-object v0

    iput-object v0, p0, Liei;->dAm:[Liej;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Liei;->dAn:[Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Liei;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liei;->eCz:I

    .line 3081
    return-void
.end method

.method public static aVG()[Liei;
    .locals 2

    .prologue
    .line 2972
    sget-object v0, Liei;->dAj:[Liei;

    if-nez v0, :cond_1

    .line 2973
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2975
    :try_start_0
    sget-object v0, Liei;->dAj:[Liei;

    if-nez v0, :cond_0

    .line 2976
    const/4 v0, 0x0

    new-array v0, v0, [Liei;

    sput-object v0, Liei;->dAj:[Liei;

    .line 2978
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2980
    :cond_1
    sget-object v0, Liei;->dAj:[Liei;

    return-object v0

    .line 2978
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2794
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liei;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liei;->aeU:Ljava/lang/String;

    iget v0, p0, Liei;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liei;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liei;->dAk:Ljava/lang/String;

    iget v0, p0, Liei;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liei;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liei;->afh:Ljava/lang/String;

    iget v0, p0, Liei;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liei;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liei;->dAl:I

    iget v0, p0, Liei;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Liei;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Liei;->dAm:[Liej;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liej;

    if-eqz v0, :cond_1

    iget-object v3, p0, Liei;->dAm:[Liej;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Liej;

    invoke-direct {v3}, Liej;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Liei;->dAm:[Liej;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Liej;

    invoke-direct {v3}, Liej;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Liei;->dAm:[Liej;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Liei;->dAn:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Liei;->dAn:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Liei;->dAn:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Liei;->dAn:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v3, p0, Liei;->dAo:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Liei;->dAo:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3100
    iget v0, p0, Liei;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3101
    const/4 v0, 0x1

    iget-object v2, p0, Liei;->aeU:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3103
    :cond_0
    iget v0, p0, Liei;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3104
    const/4 v0, 0x2

    iget-object v2, p0, Liei;->dAk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3106
    :cond_1
    iget v0, p0, Liei;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3107
    const/4 v0, 0x3

    iget-object v2, p0, Liei;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3109
    :cond_2
    iget v0, p0, Liei;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 3110
    const/4 v0, 0x4

    iget v2, p0, Liei;->dAl:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 3112
    :cond_3
    iget-object v0, p0, Liei;->dAm:[Liej;

    if-eqz v0, :cond_5

    iget-object v0, p0, Liei;->dAm:[Liej;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 3113
    :goto_0
    iget-object v2, p0, Liei;->dAm:[Liej;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 3114
    iget-object v2, p0, Liei;->dAm:[Liej;

    aget-object v2, v2, v0

    .line 3115
    if-eqz v2, :cond_4

    .line 3116
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 3113
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3120
    :cond_5
    iget-object v0, p0, Liei;->dAn:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Liei;->dAn:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 3121
    :goto_1
    iget-object v2, p0, Liei;->dAn:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 3122
    iget-object v2, p0, Liei;->dAn:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 3123
    if-eqz v2, :cond_6

    .line 3124
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3121
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3128
    :cond_7
    iget-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_9

    .line 3129
    :goto_2
    iget-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_9

    .line 3130
    iget-object v0, p0, Liei;->dAo:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 3131
    if-eqz v0, :cond_8

    .line 3132
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 3129
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3136
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3137
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3141
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3142
    iget v2, p0, Liei;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 3143
    const/4 v2, 0x1

    iget-object v3, p0, Liei;->aeU:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3146
    :cond_0
    iget v2, p0, Liei;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 3147
    const/4 v2, 0x2

    iget-object v3, p0, Liei;->dAk:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3150
    :cond_1
    iget v2, p0, Liei;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 3151
    const/4 v2, 0x3

    iget-object v3, p0, Liei;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3154
    :cond_2
    iget v2, p0, Liei;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 3155
    const/4 v2, 0x4

    iget v3, p0, Liei;->dAl:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3158
    :cond_3
    iget-object v2, p0, Liei;->dAm:[Liej;

    if-eqz v2, :cond_6

    iget-object v2, p0, Liei;->dAm:[Liej;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 3159
    :goto_0
    iget-object v3, p0, Liei;->dAm:[Liej;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 3160
    iget-object v3, p0, Liei;->dAm:[Liej;

    aget-object v3, v3, v0

    .line 3161
    if-eqz v3, :cond_4

    .line 3162
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3159
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 3167
    :cond_6
    iget-object v2, p0, Liei;->dAn:[Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Liei;->dAn:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    move v3, v1

    move v4, v1

    .line 3170
    :goto_1
    iget-object v5, p0, Liei;->dAn:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_8

    .line 3171
    iget-object v5, p0, Liei;->dAn:[Ljava/lang/String;

    aget-object v5, v5, v2

    .line 3172
    if-eqz v5, :cond_7

    .line 3173
    add-int/lit8 v4, v4, 0x1

    .line 3174
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 3170
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3178
    :cond_8
    add-int/2addr v0, v3

    .line 3179
    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    .line 3181
    :cond_9
    iget-object v2, p0, Liei;->dAo:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Liei;->dAo:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v1

    move v3, v1

    .line 3184
    :goto_2
    iget-object v4, p0, Liei;->dAo:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_b

    .line 3185
    iget-object v4, p0, Liei;->dAo:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 3186
    if-eqz v4, :cond_a

    .line 3187
    add-int/lit8 v3, v3, 0x1

    .line 3188
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3184
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3192
    :cond_b
    add-int/2addr v0, v2

    .line 3193
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 3195
    :cond_c
    return v0
.end method
