.class final Lcjy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lclt;


# instance fields
.field private synthetic aZt:Lcjt;

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Lcjt;Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 1435
    iput-object p1, p0, Lcjy;->aZt:Lcjt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1436
    iput-object p2, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1437
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 2

    .prologue
    .line 1489
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->U(Lcom/google/android/shared/search/Query;)V

    .line 1490
    return-void
.end method

.method public final Np()V
    .locals 2

    .prologue
    .line 1471
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1477
    :goto_0
    return-void

    .line 1474
    :cond_0
    const-string v0, "SPEAK_NOW"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 1475
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    goto :goto_0
.end method

.method public final a(Leis;)V
    .locals 4

    .prologue
    .line 1451
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1457
    :goto_0
    return-void

    .line 1454
    :cond_0
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1456
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v2, Lcom/google/android/search/shared/actions/errors/SoundSearchError;

    iget-object v3, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {v2, v3, p1}, Lcom/google/android/search/shared/actions/errors/SoundSearchError;-><init>(Lcom/google/android/shared/search/Query;Leis;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    goto :goto_0
.end method

.method public final a(Lieb;)V
    .locals 3

    .prologue
    .line 1441
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1447
    :goto_0
    return-void

    .line 1444
    :cond_0
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1446
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {p1}, Lcom/google/android/velvet/ActionData;->c(Lieb;)Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->c(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    goto :goto_0
.end method

.method public final b(Leis;)V
    .locals 4

    .prologue
    .line 1461
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1467
    :goto_0
    return-void

    .line 1464
    :cond_0
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1466
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v2, Lcom/google/android/search/shared/actions/errors/SoundSearchError;

    iget-object v3, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {v2, v3, p1}, Lcom/google/android/search/shared/actions/errors/SoundSearchError;-><init>(Lcom/google/android/shared/search/Query;Leis;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    goto :goto_0
.end method

.method public final p([B)V
    .locals 2

    .prologue
    .line 1481
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1485
    :goto_0
    return-void

    .line 1484
    :cond_0
    iget-object v0, p0, Lcjy;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    iget-object v1, p0, Lcjy;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, p1}, Ldcw;->a(Lcom/google/android/shared/search/Query;[B)V

    goto :goto_0
.end method
