.class public Laqb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Iterable;


# static fields
.field private static synthetic $assertionsDisabled:Z


# instance fields
.field public final amj:Ljava/util/List;

.field amk:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Laqb;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Laqb;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laqb;->amj:Ljava/util/List;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Laqb;->amk:I

    .line 25
    return-void
.end method

.method private tK()V
    .locals 2

    .prologue
    .line 87
    sget-boolean v0, Laqb;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Laqb;->amk:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 90
    :cond_0
    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 91
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 92
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 93
    if-nez v1, :cond_1

    .line 94
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 96
    :cond_2
    return-void
.end method


# virtual methods
.method public final ag(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 35
    if-eqz p1, :cond_0

    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final ah(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 50
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 59
    :goto_0
    return-void

    .line 53
    :cond_0
    iget v1, p0, Laqb;->amk:I

    if-nez v1, :cond_1

    .line 55
    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    :cond_1
    iget-object v1, p0, Laqb;->amj:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final ai(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final cX(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Laqb;->amj:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Laqc;

    invoke-direct {v0, p0}, Laqc;-><init>(Laqb;)V

    return-object v0
.end method

.method final tL()V
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Laqb;->amk:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Laqb;->amk:I

    .line 101
    sget-boolean v0, Laqb;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Laqb;->amk:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 102
    :cond_0
    iget v0, p0, Laqb;->amk:I

    if-nez v0, :cond_1

    .line 103
    invoke-direct {p0}, Laqb;->tK()V

    .line 104
    :cond_1
    return-void
.end method
