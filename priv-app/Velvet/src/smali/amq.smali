.class public final Lamq;
.super Ljsl;
.source "PG"


# instance fields
.field private aeI:D

.field private aeJ:D

.field private aez:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 529
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 530
    const/4 v0, 0x0

    iput v0, p0, Lamq;->aez:I

    iput-wide v2, p0, Lamq;->aeI:D

    iput-wide v2, p0, Lamq;->aeJ:D

    const/4 v0, 0x0

    iput-object v0, p0, Lamq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamq;->eCz:I

    .line 531
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 472
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lamq;->aeI:D

    iget v0, p0, Lamq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lamq;->aeJ:D

    iget v0, p0, Lamq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 545
    iget v0, p0, Lamq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 546
    const/4 v0, 0x1

    iget-wide v2, p0, Lamq;->aeI:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 548
    :cond_0
    iget v0, p0, Lamq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 549
    const/4 v0, 0x2

    iget-wide v2, p0, Lamq;->aeJ:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 551
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 552
    return-void
.end method

.method public final d(D)Lamq;
    .locals 1

    .prologue
    .line 497
    iput-wide p1, p0, Lamq;->aeI:D

    .line 498
    iget v0, p0, Lamq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamq;->aez:I

    .line 499
    return-object p0
.end method

.method public final e(D)Lamq;
    .locals 1

    .prologue
    .line 516
    iput-wide p1, p0, Lamq;->aeJ:D

    .line 517
    iget v0, p0, Lamq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamq;->aez:I

    .line 518
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 556
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 557
    iget v1, p0, Lamq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 558
    const/4 v1, 0x1

    iget-wide v2, p0, Lamq;->aeI:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 561
    :cond_0
    iget v1, p0, Lamq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 562
    const/4 v1, 0x2

    iget-wide v2, p0, Lamq;->aeJ:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 565
    :cond_1
    return v0
.end method

.method public final mR()D
    .locals 2

    .prologue
    .line 494
    iget-wide v0, p0, Lamq;->aeI:D

    return-wide v0
.end method

.method public final mS()D
    .locals 2

    .prologue
    .line 513
    iget-wide v0, p0, Lamq;->aeJ:D

    return-wide v0
.end method

.method public final nH()Z
    .locals 1

    .prologue
    .line 502
    iget v0, p0, Lamq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nI()Z
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lamq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
