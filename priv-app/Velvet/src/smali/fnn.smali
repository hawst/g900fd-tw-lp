.class public final Lfnn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cwU:J


# instance fields
.field private final RV:Landroid/graphics/Rect;

.field private bpx:Z

.field private ccW:I

.field private final cwV:[I

.field private cwW:Landroid/widget/PopupWindow;

.field private final cwX:Lesk;

.field private final mContext:Landroid/content/Context;

.field private final mRunner:Lerp;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lfnn;->cwU:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lerp;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfnn;->RV:Landroid/graphics/Rect;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lfnn;->cwV:[I

    .line 48
    iput-boolean v1, p0, Lfnn;->bpx:Z

    .line 50
    iput v1, p0, Lfnn;->ccW:I

    .line 52
    new-instance v0, Lfno;

    const-string v1, "Auto dismiss"

    invoke-direct {v0, p0, v1}, Lfno;-><init>(Lfnn;Ljava/lang/String;)V

    iput-object v0, p0, Lfnn;->cwX:Lesk;

    .line 60
    iput-object p1, p0, Lfnn;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lfnn;->mRunner:Lerp;

    .line 62
    return-void
.end method

.method static synthetic a(Lfnn;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    return-object v0
.end method


# virtual methods
.method public final a(Lejw;Lizj;)V
    .locals 10
    .param p2    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x1

    .line 89
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lejw;->ccp:Z

    if-eqz v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0, v3}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    .line 99
    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_2

    .line 100
    const-string v0, "UndoDismissManager"

    const-string v1, "View not attached"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105
    :cond_2
    if-eqz p2, :cond_6

    .line 106
    iget-object v2, p2, Lizj;->dUo:[Liwk;

    array-length v5, v2

    move v0, v3

    :goto_1
    if-ge v0, v5, :cond_6

    aget-object v6, v2, v0

    .line 107
    invoke-virtual {v6}, Liwk;->getType()I

    move-result v7

    if-ne v7, v8, :cond_5

    .line 108
    invoke-virtual {v6}, Liwk;->aZr()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109
    invoke-virtual {v6}, Liwk;->aZq()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 116
    :goto_2
    invoke-virtual {p0}, Lfnn;->aBm()V

    .line 117
    iput-boolean v8, p1, Lejw;->ccq:Z

    .line 118
    iget-boolean v0, p0, Lfnn;->bpx:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lfnn;->mContext:Landroid/content/Context;

    new-instance v5, Lfnr;

    invoke-direct {v5, p0}, Lfnr;-><init>(Lfnn;)V

    new-instance v6, Landroid/content/IntentFilter;

    const-string v7, "com.google.android.apps.now.DEFERRED_ACTIONS_COMMITTED"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v8, p0, Lfnn;->bpx:Z

    :cond_3
    iget-object v0, p0, Lfnn;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v5, 0x7f0401b4

    invoke-virtual {v0, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;

    new-instance v5, Landroid/widget/PopupWindow;

    const/4 v1, -0x1

    invoke-direct {v5, v0, v1, v9, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const v1, 0x7f1101fb

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    const v1, 0x7f110484

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lfnp;

    invoke-direct {v2, p0, p1, v5}, Lfnp;-><init>(Lfnn;Lejw;Landroid/widget/PopupWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x1030004

    invoke-virtual {v5, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    new-instance v1, Lfnq;

    invoke-direct {v1, p0, p1}, Lfnq;-><init>(Lfnn;Lejw;)V

    invoke-virtual {v5, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v1, p0, Lfnn;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d028b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, p0, Lfnn;->ccW:I

    add-int/2addr v1, v2

    const/16 v2, 0x51

    invoke-virtual {v5, v4, v2, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v1, p0, Lfnn;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v9, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-interface {v1, v0, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v5, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    .line 119
    iget-object v0, p0, Lfnn;->mRunner:Lerp;

    iget-object v1, p0, Lfnn;->cwX:Lesk;

    sget-wide v2, Lfnn;->cwU:J

    invoke-interface {v0, v1, v2, v3}, Lerp;->a(Lesk;J)V

    goto/16 :goto_0

    .line 106
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_6
    move-object v2, v1

    goto/16 :goto_2
.end method

.method public final aBm()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lfnn;->mRunner:Lerp;

    iget-object v1, p0, Lfnn;->cwX:Lesk;

    invoke-interface {v0, v1}, Lerp;->b(Lesk;)V

    .line 128
    iget-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 130
    :try_start_0
    iget-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    .line 138
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final ib(I)V
    .locals 0

    .prologue
    .line 165
    iput p1, p0, Lfnn;->ccW:I

    .line 166
    return-void
.end method

.method public final l(Landroid/view/MotionEvent;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 146
    iget-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lfnn;->cwW:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lfnn;->cwV:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 152
    iget-object v1, p0, Lfnn;->RV:Landroid/graphics/Rect;

    iget-object v2, p0, Lfnn;->cwV:[I

    aget v2, v2, v5

    iget-object v3, p0, Lfnn;->cwV:[I

    aget v3, v3, v6

    iget-object v4, p0, Lfnn;->cwV:[I

    aget v4, v4, v5

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lfnn;->cwV:[I

    aget v5, v5, v6

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 155
    iget-object v0, p0, Lfnn;->RV:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lfnn;->aBm()V

    goto :goto_0
.end method
