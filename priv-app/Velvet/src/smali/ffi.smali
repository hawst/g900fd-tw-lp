.class public final Lffi;
.super Lfes;
.source "PG"


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lfes;-><init>(Ljava/util/Collection;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100039

    iget-object v2, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aG(Landroid/content/Context;)Lbk;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    new-instance v1, Lba;

    invoke-direct {v1}, Lba;-><init>()V

    .line 43
    iget-object v0, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 44
    iget-object v0, v0, Lizj;->dSU:Ljdx;

    .line 45
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljdx;->bhX()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    invoke-virtual {v0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lba;->g(Ljava/lang/CharSequence;)Lba;

    goto :goto_0

    .line 49
    :cond_1
    return-object v1
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 36
    const-string v0, ""

    return-object v0
.end method

.method public final ayR()Z
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 80
    iget-object v2, v0, Lizj;->dUr:Ljcg;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-static {v0}, Lfgi;->c(Ljcg;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ayW()I
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 92
    const/4 v0, 0x1

    .line 93
    iget-object v1, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 94
    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    .line 95
    if-ne v0, v2, :cond_0

    move v0, v2

    .line 102
    :goto_1
    return v0

    .line 98
    :cond_0
    if-eq v0, v3, :cond_3

    .line 99
    const/4 v0, 0x0

    :goto_2
    move v1, v0

    .line 101
    goto :goto_0

    .line 102
    :cond_1
    if-eqz v1, :cond_2

    move v0, v3

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f0201cd

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lffi;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfgb;->cqz:Lfgb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfgb;->cqE:Lfgb;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 61
    iget-object v0, v0, Lizj;->dSU:Ljdx;

    .line 62
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
