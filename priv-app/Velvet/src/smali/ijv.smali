.class final Lijv;
.super Lijj;
.source "PG"

# interfaces
.implements Lird;


# instance fields
.field private final transient dDh:Liki;

.field private final transient dDi:Lijj;


# direct methods
.method constructor <init>(Liki;Lijj;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lijj;-><init>()V

    .line 36
    iput-object p1, p0, Lijv;->dDh:Liki;

    .line 37
    iput-object p2, p0, Lijv;->dDi:Lijj;

    .line 38
    return-void
.end method


# virtual methods
.method final aWH()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->aWH()Z

    move-result v0

    return v0
.end method

.method public final aWI()Lirv;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->aWI()Lirv;

    move-result-object v0

    return-object v0
.end method

.method public final aWJ()Lirw;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->aWJ()Lirw;

    move-result-object v0

    return-object v0
.end method

.method public final bm(II)Lijj;
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lijv;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lifv;->C(III)V

    .line 63
    if-ne p1, p2, :cond_0

    sget-object v0, Liil;->dCO:Liil;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Liqn;

    iget-object v1, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v1, p1, p2}, Lijj;->bm(II)Lijj;

    move-result-object v1

    iget-object v2, p0, Lijv;->dDh:Liki;

    invoke-virtual {v2}, Liki;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Liqn;-><init>(Lijj;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Liqn;->aWU()Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lijv;->dDh:Liki;

    invoke-virtual {v0}, Liki;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 48
    iget-object v0, p0, Lijv;->dDh:Liki;

    invoke-virtual {v0, p1}, Liki;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 95
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0, p1}, Lijj;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0, p1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->hashCode()I

    move-result v0

    return v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    iget-object v0, p0, Lijv;->dDh:Liki;

    invoke-virtual {v0, p1}, Liki;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->aWI()Lirv;

    move-result-object v0

    return-object v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 56
    iget-object v0, p0, Lijv;->dDh:Liki;

    invoke-virtual {v0, p1}, Liki;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->aWJ()Lirw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lijv;->ma(I)Lirw;

    move-result-object v0

    return-object v0
.end method

.method public final ma(I)Lirw;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0, p1}, Lijj;->ma(I)Lirw;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lijv;->dDi:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lijv;->bm(II)Lijj;

    move-result-object v0

    return-object v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lijc;

    iget-object v1, p0, Lijv;->dDh:Liki;

    invoke-direct {v0, v1}, Lijc;-><init>(Lijd;)V

    return-object v0
.end method
