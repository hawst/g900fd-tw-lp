.class public final Lgkg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cNG:Ljava/util/Set;

.field private cNH:Ljava/lang/String;

.field private final mAsyncServices:Lema;

.field volatile mCallback:Lefk;

.field final mContext:Landroid/content/Context;

.field final mGreco3DataManager:Lgix;

.field final mSettings:Lgdo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgix;Lgdo;Lema;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    .line 57
    iput-object p1, p0, Lgkg;->mContext:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lgkg;->mGreco3DataManager:Lgix;

    .line 59
    iput-object p3, p0, Lgkg;->mSettings:Lgdo;

    .line 60
    iput-object p4, p0, Lgkg;->mAsyncServices:Lema;

    .line 61
    return-void
.end method

.method private kp(I)V
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lgkg;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    new-instance v1, Lgki;

    const-string v2, "Handle callback"

    invoke-direct {v1, p0, v2, p1}, Lgki;-><init>(Lgkg;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 146
    return-void
.end method


# virtual methods
.method public final varargs declared-synchronized a(Lefk;Ljava/lang/String;[Lgjl;)V
    .locals 3

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lenu;->auR()V

    .line 69
    iget-object v0, p0, Lgkg;->mCallback:Lefk;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 82
    :goto_0
    monitor-exit p0

    return-void

    .line 73
    :cond_0
    :try_start_1
    iput-object p2, p0, Lgkg;->cNH:Ljava/lang/String;

    .line 74
    iput-object p1, p0, Lgkg;->mCallback:Lefk;

    .line 75
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 77
    iget-object v0, p0, Lgkg;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    invoke-virtual {p0, p3}, Lgkg;->a([Lgjl;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 80
    :cond_1
    :try_start_2
    iget-object v0, p0, Lgkg;->mGreco3DataManager:Lgix;

    new-instance v1, Lgkh;

    const-string v2, "Init grammars"

    invoke-direct {v1, p0, v2, p3}, Lgkh;-><init>(Lgkg;Ljava/lang/String;[Lgjl;)V

    invoke-virtual {v0, v1}, Lgix;->h(Lesk;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lgjl;Z)V
    .locals 1

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkg;->mCallback:Lefk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 118
    :cond_1
    if-nez p2, :cond_2

    .line 120
    :try_start_1
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 121
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lgkg;->kp(I)V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lgkg;->mCallback:Lefk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 126
    :cond_2
    :try_start_2
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgkg;->kp(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final varargs declared-synchronized a([Lgjl;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 150
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgkg;->mCallback:Lefk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 154
    :cond_1
    :try_start_1
    array-length v4, p1

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v5, p1, v3

    .line 155
    iget-object v0, p0, Lgkg;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lgkg;->mGreco3DataManager:Lgix;

    iget-object v6, p0, Lgkg;->cNH:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lgix;->mN(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgkg;->mGreco3DataManager:Lgix;

    iget-object v6, p0, Lgkg;->cNH:Ljava/lang/String;

    invoke-virtual {v0, v6, v5}, Lgix;->b(Ljava/lang/String;Lgjl;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 157
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 154
    :goto_3
    :pswitch_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 155
    :cond_2
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lgkg;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lgkg;->cNH:Ljava/lang/String;

    invoke-static {v0, v6, v5}, Lcom/google/android/speech/grammar/GrammarCompilationService;->a(Landroid/content/Context;Ljava/lang/String;Lgjl;)V

    const/4 v0, 0x2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_2

    .line 164
    :pswitch_1
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 169
    :pswitch_2
    :try_start_2
    iget-object v0, p0, Lgkg;->mCallback:Lefk;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lefk;->ar(Ljava/lang/Object;)V

    goto :goto_0

    .line 174
    :cond_4
    iget-object v0, p0, Lgkg;->cNG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lgkg;->mCallback:Lefk;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lefk;->ar(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized aGT()V
    .locals 0

    .prologue
    .line 103
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized aGU()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lgkg;->mSettings:Lgdo;

    invoke-interface {v1}, Lgdo;->aFc()Ljava/lang/String;

    move-result-object v4

    .line 207
    iget-object v1, p0, Lgkg;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    .line 208
    invoke-static {}, Lgjl;->values()[Lgjl;

    move-result-object v8

    array-length v9, v8

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_2

    aget-object v5, v8, v6

    .line 209
    invoke-static {}, Lcom/google/android/speech/grammar/GrammarCompilationService;->aHd()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgkg;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    :cond_0
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 209
    :cond_1
    new-instance v0, Lgkj;

    const-string v2, "Maybe schedule grammar compilation"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lgkj;-><init>(Lgkg;Ljava/lang/String;[ILjava/lang/String;Lgjl;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 211
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c(Lefk;)V
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lenu;->auR()V

    .line 97
    iget-object v0, p0, Lgkg;->mCallback:Lefk;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lgkg;->mCallback:Lefk;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lgkg;->mCallback:Lefk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit p0

    return-void

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
