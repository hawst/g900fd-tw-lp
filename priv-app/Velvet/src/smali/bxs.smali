.class final Lbxs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/RemoteController$OnClientUpdateListener;


# instance fields
.field private synthetic aOl:Lbxr;


# direct methods
.method constructor <init>(Lbxr;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lbxs;->aOl:Lbxr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClientChange(Z)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 65
    iget-object v0, p0, Lbxs;->aOl:Lbxr;

    iget-object v0, v0, Lbxr;->aOk:Landroid/widget/ImageView;

    const v1, 0x7f02015c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 67
    :cond_0
    return-void
.end method

.method public final onClientMetadataUpdate(Landroid/media/RemoteController$MetadataEditor;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 88
    iget-object v0, p0, Lbxs;->aOl:Lbxr;

    iget-object v0, v0, Lbxr;->aNt:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lbxs;->aOl:Lbxr;

    iget-object v0, v0, Lbxr;->aOg:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    iget-object v1, p0, Lbxs;->aOl:Lbxr;

    new-instance v0, Lbxv;

    const/16 v2, 0xd

    invoke-virtual {p1, v2, v6}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {p1, v3, v6}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v6}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {p1, v5, v6}, Landroid/media/RemoteController$MetadataEditor;->getBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    const/16 v6, 0x9

    const-wide/16 v8, -0x1

    invoke-virtual {p1, v6, v8, v9}, Landroid/media/RemoteController$MetadataEditor;->getLong(IJ)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lbxv;-><init>(Lbxr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;J)V

    .line 91
    return-void
.end method

.method public final onClientPlaybackStateUpdate(I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbxs;->aOl:Lbxr;

    invoke-static {v0, p1}, Lbxr;->a(Lbxr;I)V

    .line 78
    return-void
.end method

.method public final onClientPlaybackStateUpdate(IJJF)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lbxs;->aOl:Lbxr;

    invoke-static {v0, p1}, Lbxr;->a(Lbxr;I)V

    .line 73
    return-void
.end method

.method public final onClientTransportControlUpdate(I)V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lbxs;->aOl:Lbxr;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lbxr;->ae(II)Z

    move-result v0

    iput-boolean v0, v1, Lbxr;->aOh:Z

    const/16 v0, 0x80

    invoke-static {p1, v0}, Lbxr;->ae(II)Z

    move-result v0

    iput-boolean v0, v1, Lbxr;->aOi:Z

    iget-boolean v0, v1, Lbxr;->aOh:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v0, v1, Lbxr;->aOe:Ljava/util/Vector;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v2, v0}, Lbxr;->a(Ljava/lang/Boolean;Landroid/view/View;)V

    iget-boolean v0, v1, Lbxr;->aOi:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v0, v1, Lbxr;->aOe:Ljava/util/Vector;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v2, v0}, Lbxr;->a(Ljava/lang/Boolean;Landroid/view/View;)V

    .line 84
    return-void
.end method
