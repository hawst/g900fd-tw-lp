.class final Lfzr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Ljava/util/Observer;


# instance fields
.field private synthetic cDZ:Lfzp;


# direct methods
.method private constructor <init>(Lfzp;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lfzr;->cDZ:Lfzp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lfzp;B)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lfzr;-><init>(Lfzp;)V

    return-void
.end method


# virtual methods
.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lfzr;->cDZ:Lfzp;

    iget-object v0, v0, Lfzp;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->wq()Lfnm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfnm;->addObserver(Ljava/util/Observer;)V

    .line 171
    iget-object v0, p0, Lfzr;->cDZ:Lfzp;

    invoke-static {v0}, Lfzp;->b(Lfzp;)V

    .line 172
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lfzr;->cDZ:Lfzp;

    iget-object v0, v0, Lfzp;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->wq()Lfnm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfnm;->deleteObserver(Ljava/util/Observer;)V

    .line 177
    return-void
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lfzr;->cDZ:Lfzp;

    invoke-static {v0}, Lfzp;->b(Lfzp;)V

    .line 182
    return-void
.end method
