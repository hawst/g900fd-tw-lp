.class public final Lkmq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile faJ:[Lkmq;


# instance fields
.field private aez:I

.field private dzl:F

.field private eEf:Ljava/lang/String;

.field public faK:[Lkmr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 409
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 410
    const/4 v0, 0x0

    iput v0, p0, Lkmq;->aez:I

    invoke-static {}, Lkmr;->bCo()[Lkmr;

    move-result-object v0

    iput-object v0, p0, Lkmq;->faK:[Lkmr;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lkmq;->dzl:F

    const-string v0, ""

    iput-object v0, p0, Lkmq;->eEf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lkmq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkmq;->eCz:I

    .line 411
    return-void
.end method

.method public static bCn()[Lkmq;
    .locals 2

    .prologue
    .line 352
    sget-object v0, Lkmq;->faJ:[Lkmq;

    if-nez v0, :cond_1

    .line 353
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 355
    :try_start_0
    sget-object v0, Lkmq;->faJ:[Lkmq;

    if-nez v0, :cond_0

    .line 356
    const/4 v0, 0x0

    new-array v0, v0, [Lkmq;

    sput-object v0, Lkmq;->faJ:[Lkmq;

    .line 358
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    :cond_1
    sget-object v0, Lkmq;->faJ:[Lkmq;

    return-object v0

    .line 358
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 346
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkmq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lkmq;->faK:[Lkmr;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lkmr;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lkmq;->faK:[Lkmr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lkmr;

    invoke-direct {v3}, Lkmr;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lkmq;->faK:[Lkmr;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lkmr;

    invoke-direct {v3}, Lkmr;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lkmq;->faK:[Lkmr;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lkmq;->dzl:F

    iget v0, p0, Lkmq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkmq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkmq;->eEf:Ljava/lang/String;

    iget v0, p0, Lkmq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkmq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 426
    iget-object v0, p0, Lkmq;->faK:[Lkmr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkmq;->faK:[Lkmr;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 427
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lkmq;->faK:[Lkmr;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 428
    iget-object v1, p0, Lkmq;->faK:[Lkmr;

    aget-object v1, v1, v0

    .line 429
    if-eqz v1, :cond_0

    .line 430
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 427
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 434
    :cond_1
    iget v0, p0, Lkmq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 435
    const/4 v0, 0x2

    iget v1, p0, Lkmq;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 437
    :cond_2
    iget v0, p0, Lkmq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 438
    const/4 v0, 0x3

    iget-object v1, p0, Lkmq;->eEf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 440
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 441
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 445
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 446
    iget-object v0, p0, Lkmq;->faK:[Lkmr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkmq;->faK:[Lkmr;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 447
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lkmq;->faK:[Lkmr;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 448
    iget-object v2, p0, Lkmq;->faK:[Lkmr;

    aget-object v2, v2, v0

    .line 449
    if-eqz v2, :cond_0

    .line 450
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 447
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 455
    :cond_1
    iget v0, p0, Lkmq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 456
    const/4 v0, 0x2

    iget v2, p0, Lkmq;->dzl:F

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    .line 459
    :cond_2
    iget v0, p0, Lkmq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 460
    const/4 v0, 0x3

    iget-object v2, p0, Lkmq;->eEf:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 463
    :cond_3
    return v1
.end method
