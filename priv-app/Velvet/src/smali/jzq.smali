.class public final Ljzq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eOi:[Ljzq;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private eOj:Ljava/lang/String;

.field private eOk:Z

.field private eOl:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4174
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4175
    iput v1, p0, Ljzq;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzq;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljzq;->eOj:Ljava/lang/String;

    iput-boolean v1, p0, Ljzq;->eOk:Z

    iput v1, p0, Ljzq;->eOl:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljzq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzq;->eCz:I

    .line 4176
    return-void
.end method

.method public static bxp()[Ljzq;
    .locals 2

    .prologue
    .line 4079
    sget-object v0, Ljzq;->eOi:[Ljzq;

    if-nez v0, :cond_1

    .line 4080
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 4082
    :try_start_0
    sget-object v0, Ljzq;->eOi:[Ljzq;

    if-nez v0, :cond_0

    .line 4083
    const/4 v0, 0x0

    new-array v0, v0, [Ljzq;

    sput-object v0, Ljzq;->eOi:[Ljzq;

    .line 4085
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4087
    :cond_1
    sget-object v0, Ljzq;->eOi:[Ljzq;

    return-object v0

    .line 4085
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4073
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzq;->agq:Ljava/lang/String;

    iget v0, p0, Ljzq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzq;->eOj:Ljava/lang/String;

    iget v0, p0, Ljzq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzq;->eOk:Z

    iget v0, p0, Ljzq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzq;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzq;->eOl:I

    iget v0, p0, Ljzq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4192
    iget v0, p0, Ljzq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4193
    const/4 v0, 0x1

    iget-object v1, p0, Ljzq;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4195
    :cond_0
    iget v0, p0, Ljzq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4196
    const/4 v0, 0x2

    iget-object v1, p0, Ljzq;->eOj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4198
    :cond_1
    iget v0, p0, Ljzq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 4199
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljzq;->eOk:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4201
    :cond_2
    iget v0, p0, Ljzq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 4202
    const/4 v0, 0x4

    iget v1, p0, Ljzq;->eOl:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4204
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4205
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4209
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4210
    iget v1, p0, Ljzq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4211
    const/4 v1, 0x1

    iget-object v2, p0, Ljzq;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4214
    :cond_0
    iget v1, p0, Ljzq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4215
    const/4 v1, 0x2

    iget-object v2, p0, Ljzq;->eOj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4218
    :cond_1
    iget v1, p0, Ljzq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 4219
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljzq;->eOk:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4222
    :cond_2
    iget v1, p0, Ljzq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 4223
    const/4 v1, 0x4

    iget v2, p0, Ljzq;->eOl:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4226
    :cond_3
    return v0
.end method
