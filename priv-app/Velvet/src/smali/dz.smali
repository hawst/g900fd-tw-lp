.class public Ldz;
.super Lem;
.source "PG"

# interfaces
.implements Ljava/util/Map;


# instance fields
.field private eN:Leg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lem;-><init>()V

    .line 55
    return-void
.end method

.method private at()Leg;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Ldz;->eN:Leg;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lea;

    invoke-direct {v0, p0}, Lea;-><init>(Ldz;)V

    iput-object v0, p0, Ldz;->eN:Leg;

    .line 120
    :cond_0
    iget-object v0, p0, Ldz;->eN:Leg;

    return-object v0
.end method


# virtual methods
.method public entrySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 179
    invoke-direct {p0}, Ldz;->at()Leg;

    move-result-object v0

    iget-object v1, v0, Leg;->fc:Lei;

    if-nez v1, :cond_0

    new-instance v1, Lei;

    invoke-direct {v1, v0}, Lei;-><init>(Leg;)V

    iput-object v1, v0, Leg;->fc:Lei;

    :cond_0
    iget-object v0, v0, Leg;->fc:Lei;

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 191
    invoke-direct {p0}, Ldz;->at()Leg;

    move-result-object v0

    iget-object v1, v0, Leg;->fd:Lej;

    if-nez v1, :cond_0

    new-instance v1, Lej;

    invoke-direct {v1, v0}, Lej;-><init>(Leg;)V

    iput-object v1, v0, Leg;->fd:Lej;

    :cond_0
    iget-object v0, v0, Leg;->fd:Lej;

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    iget v0, p0, Ldz;->eX:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lem;->fo:[I

    array-length v1, v1

    if-ge v1, v0, :cond_1

    iget-object v1, p0, Lem;->fo:[I

    iget-object v2, p0, Lem;->fp:[Ljava/lang/Object;

    invoke-super {p0, v0}, Lem;->p(I)V

    iget v0, p0, Lem;->eX:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lem;->fo:[I

    iget v3, p0, Lem;->eX:I

    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lem;->fp:[Ljava/lang/Object;

    iget v3, p0, Lem;->eX:I

    shl-int/lit8 v3, v3, 0x1

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iget v0, p0, Lem;->eX:I

    invoke-static {v1, v2, v0}, Lem;->a([I[Ljava/lang/Object;I)V

    .line 140
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 141
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Ldz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 143
    :cond_2
    return-void
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 161
    invoke-static {p0, p1}, Leg;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 203
    invoke-direct {p0}, Ldz;->at()Leg;

    move-result-object v0

    iget-object v1, v0, Leg;->fe:Lel;

    if-nez v1, :cond_0

    new-instance v1, Lel;

    invoke-direct {v1, v0}, Lel;-><init>(Leg;)V

    iput-object v1, v0, Leg;->fe:Lel;

    :cond_0
    iget-object v0, v0, Leg;->fe:Lel;

    return-object v0
.end method
