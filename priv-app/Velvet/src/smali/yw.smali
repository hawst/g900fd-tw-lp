.class public final Lyw;
.super Landroid/appwidget/AppWidgetHost;
.source "PG"


# instance fields
.field private final Mn:Ljava/util/ArrayList;

.field private xZ:Lcom/android/launcher3/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;I)V
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x400

    invoke-direct {p0, p1, v0}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lyw;->Mn:Ljava/util/ArrayList;

    .line 40
    iput-object p1, p0, Lyw;->xZ:Lcom/android/launcher3/Launcher;

    .line 41
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lyw;->Mn:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public final c(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lyw;->Mn:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 77
    return-void
.end method

.method protected final onCreateView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lyx;

    invoke-direct {v0, p1}, Lyx;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected final onProvidersChanged()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lyw;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lyw;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1}, Lzi;->t(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Launcher;->o(Ljava/util/ArrayList;)V

    .line 84
    iget-object v0, p0, Lyw;->Mn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 85
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method

.method public final startListening()V
    .locals 2

    .prologue
    .line 52
    :try_start_0
    invoke-super {p0}, Landroid/appwidget/AppWidgetHost;->startListening()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 54
    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Landroid/os/TransactionTooLargeException;

    if-nez v1, :cond_0

    .line 60
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final stopListening()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Landroid/appwidget/AppWidgetHost;->stopListening()V

    .line 68
    invoke-virtual {p0}, Lyw;->clearViews()V

    .line 69
    return-void
.end method
