.class public final Lbkm;
.super Lbir;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbgp;Lbgq;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lbir;-><init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Lbjm;Lbiv;)V
    .locals 2

    const v0, 0x5d3f18

    iget-object v1, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lbjm;->a(Lbjj;ILjava/lang/String;)V

    return-void
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lbko;->h(Landroid/os/IBinder;)Lbkn;

    move-result-object v0

    return-object v0
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.icing.INDEX_SERVICE"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.appdatasearch.internal.IAppDataSearch"

    return-object v0
.end method

.method public final zb()Lbkn;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lbkm;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbkn;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    invoke-virtual {v1, v0}, Landroid/os/RemoteException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method
