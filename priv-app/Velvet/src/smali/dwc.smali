.class public final Ldwc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Leei;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Ldwc;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 35
    return-void
.end method

.method private b(Ljqd;Ldxb;Ldxd;)Landroid/content/Intent;
    .locals 5
    .param p1    # Ljqd;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 192
    iget-object v1, p1, Ljqd;->eyr:Ljqv;

    if-nez v1, :cond_1

    .line 193
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    .line 215
    :cond_0
    :goto_0
    return-object v0

    .line 198
    :cond_1
    :try_start_0
    iget-object v1, p1, Ljqd;->eyr:Ljqv;

    invoke-virtual {p3, v1, p2}, Ldxd;->a(Ljqv;Ldxb;)Ldws;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 209
    invoke-virtual {v1}, Ldws;->Pd()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215
    iget-object v0, p0, Ldwc;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Ldws;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 199
    :catch_0
    move-exception v1

    .line 203
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p1, Ljqd;->eyr:Ljqv;

    invoke-virtual {v2, v3, v4, v1}, Ldwd;->a(ILjqv;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static b(Ljqs;)Ljava/util/List;
    .locals 6

    .prologue
    .line 237
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 238
    sget-object v0, Ljqd;->eyo:Ljsm;

    invoke-virtual {p0, v0}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqd;

    .line 240
    if-nez v0, :cond_0

    move-object v0, v1

    .line 258
    :goto_0
    return-object v0

    .line 245
    :cond_0
    iget-object v2, v0, Ljqd;->eys:Ljqv;

    if-eqz v2, :cond_1

    .line 246
    iget-object v2, v0, Ljqd;->eys:Ljqv;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    :cond_1
    iget-object v2, v0, Ljqd;->eyr:Ljqv;

    if-eqz v2, :cond_2

    .line 249
    iget-object v2, v0, Ljqd;->eyr:Ljqv;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    :cond_2
    iget-object v2, v0, Ljqd;->eyu:[Ljqe;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v4, v2, v0

    .line 253
    iget-object v5, v4, Ljqe;->eyz:Ljqv;

    if-eqz v5, :cond_3

    .line 254
    iget-object v4, v4, Ljqe;->eyz:Ljqv;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 258
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljqd;Ldxb;Ldxd;)Landroid/content/Intent;
    .locals 10
    .param p1    # Ljqd;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-virtual {p1}, Ljqd;->bsj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Ldwc;->b(Ljqd;Ldxb;Ldxd;)Landroid/content/Intent;

    move-result-object v1

    .line 186
    :goto_0
    return-object v1

    .line 49
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p1}, Ljqd;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Ljqd;->bsi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p1}, Ljqd;->getFlags()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 55
    :cond_1
    iget-object v4, p1, Ljqd;->dOb:[Ljava/lang/String;

    array-length v5, v4

    move v0, v3

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 56
    invoke-virtual {v2, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62
    :cond_2
    iget-object v0, p1, Ljqd;->eyr:Ljqv;

    if-eqz v0, :cond_3

    .line 64
    :try_start_0
    iget-object v0, p1, Ljqd;->eyr:Ljqv;

    invoke-virtual {p3, v0, p2}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 66
    invoke-virtual {v0}, Ldws;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :cond_3
    iget-object v0, p1, Ljqd;->eyt:Ljqv;

    if-eqz v0, :cond_4

    .line 81
    :try_start_1
    iget-object v0, p1, Ljqd;->eyt:Ljqv;

    invoke-virtual {p3, v0, p2}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 83
    invoke-virtual {v0}, Ldws;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 97
    :cond_4
    iget-object v0, p1, Ljqd;->eys:Ljqv;

    if-eqz v0, :cond_d

    .line 99
    :try_start_2
    iget-object v0, p1, Ljqd;->eys:Ljqv;

    invoke-virtual {p3, v0, p2}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 101
    invoke-virtual {v0}, Ldws;->getUri()Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 116
    :goto_2
    if-eqz v0, :cond_8

    .line 117
    invoke-virtual {p1}, Ljqd;->oP()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 118
    invoke-virtual {p1}, Ljqd;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    :cond_5
    :goto_3
    iget-object v4, p1, Ljqd;->eyu:[Ljqe;

    array-length v5, v4

    move v0, v3

    :goto_4
    if-ge v0, v5, :cond_b

    aget-object v3, v4, v0

    .line 129
    invoke-virtual {v3}, Ljqe;->oK()Z

    move-result v6

    if-nez v6, :cond_9

    .line 130
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    .line 128
    :cond_6
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 68
    :catch_0
    move-exception v0

    .line 72
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p1, Ljqd;->eyr:Ljqv;

    invoke-virtual {v2, v3, v4, v0}, Ldwd;->a(ILjqv;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 85
    :catch_1
    move-exception v0

    .line 89
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    move-result-object v2

    const/4 v3, 0x4

    iget-object v4, p1, Ljqd;->eyt:Ljqv;

    invoke-virtual {v2, v3, v4, v0}, Ldwd;->a(ILjqv;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 103
    :catch_2
    move-exception v0

    .line 107
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p1, Ljqd;->eys:Ljqv;

    invoke-virtual {v2, v3, v4, v0}, Ldwd;->a(ILjqv;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 120
    :cond_7
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_3

    .line 123
    :cond_8
    invoke-virtual {p1}, Ljqd;->oP()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 124
    invoke-virtual {p1}, Ljqd;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 132
    :cond_9
    iget-object v6, v3, Ljqe;->eyz:Ljqv;

    if-nez v6, :cond_a

    .line 133
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    goto :goto_5

    .line 137
    :cond_a
    :try_start_3
    iget-object v6, v3, Ljqe;->eyz:Ljqv;

    invoke-virtual {p3, v6, p2}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v6

    .line 138
    invoke-virtual {v6}, Ldws;->Pd()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 142
    invoke-virtual {v3}, Ljqe;->getType()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 164
    :goto_6
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    invoke-virtual {v3}, Ljqe;->getType()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unknown extra type: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_5

    .line 166
    :catch_3
    move-exception v0

    .line 170
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    move-result-object v2

    const/4 v4, 0x3

    iget-object v3, v3, Ljqe;->eyz:Ljqv;

    invoke-virtual {v2, v4, v3, v0}, Ldwd;->a(ILjqv;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 144
    :pswitch_0
    :try_start_4
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->getString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 147
    :pswitch_1
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->ajU()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 150
    :pswitch_2
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->ajV()I

    move-result v6

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_5

    .line 153
    :pswitch_3
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->getLong()J

    move-result-wide v8

    invoke-virtual {v2, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto/16 :goto_5

    .line 156
    :pswitch_4
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->ajW()Z

    move-result v6

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_5

    .line 159
    :pswitch_5
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_5

    .line 162
    :pswitch_6
    invoke-virtual {v3}, Ljqe;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ldws;->getDouble()D

    move-result-wide v8

    invoke-virtual {v2, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_6

    .line 178
    :cond_b
    invoke-virtual {p1}, Ljqd;->bsl()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 183
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    goto/16 :goto_0

    :cond_c
    move-object v1, v2

    .line 186
    goto/16 :goto_0

    :cond_d
    move-object v0, v1

    goto/16 :goto_2

    .line 142
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_6
    .end packed-switch
.end method
