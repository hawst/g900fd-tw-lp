.class final Ldnk;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private final bDy:Ljava/util/concurrent/Executor;

.field private final bDz:Ljava/lang/Runnable;

.field private final mDelegate:Ljava/io/InputStream;

.field private final mQuery:Lcom/google/android/shared/search/Query;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;


# direct methods
.method constructor <init>(Lcom/google/android/shared/search/Query;Ljava/io/InputStream;Lcom/google/android/search/core/state/QueryState;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 27
    new-instance v0, Ldnl;

    const-string v1, "report error"

    invoke-direct {v0, p0, v1}, Ldnl;-><init>(Ldnk;Ljava/lang/String;)V

    iput-object v0, p0, Ldnk;->bDz:Ljava/lang/Runnable;

    .line 38
    iput-object p1, p0, Ldnk;->mQuery:Lcom/google/android/shared/search/Query;

    .line 39
    iput-object p2, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    .line 40
    iput-object p3, p0, Ldnk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 41
    iput-object p4, p0, Ldnk;->bDy:Ljava/util/concurrent/Executor;

    .line 42
    return-void
.end method

.method static synthetic a(Ldnk;)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Ldnk;->mQuery:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method private a(Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 122
    const-string v0, "Velvet.WebViewInputStream"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error reported from delegate stream: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Ldnk;->bDy:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Ldnk;->bDz:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 124
    return-void
.end method

.method static synthetic b(Ldnk;)Lcom/google/android/search/core/state/QueryState;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Ldnk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    return-object v0
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 47
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 50
    :goto_0
    return v0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    .line 50
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 57
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    goto :goto_0
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 66
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 69
    :goto_0
    return v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    .line 69
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final read([B)I
    .locals 1

    .prologue
    .line 76
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 79
    :goto_0
    return v0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    .line 79
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 86
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 89
    :goto_0
    return v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    .line 89
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 96
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    .line 102
    throw v0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 109
    :try_start_0
    iget-object v0, p0, Ldnk;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    invoke-direct {p0, v0}, Ldnk;->a(Ljava/io/IOException;)V

    .line 116
    throw v0
.end method
