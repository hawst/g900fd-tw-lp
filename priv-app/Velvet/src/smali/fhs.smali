.class public Lfhs;
.super Lcxh;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lemy;
.implements Lfhr;


# instance fields
.field private aOR:Landroid/os/Bundle;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crT:Ljhq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crU:I

.field crW:Lfic;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crX:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crY:[I
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private csa:I

.field private csb:Ljhr;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field csc:Ljava/util/Set;

.field private mCalendarProvider:Leym;

.field private mTrainingQuestionManager:Lfdn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcxh;-><init>()V

    .line 1704
    return-void
.end method

.method private R(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 754
    invoke-direct {p0, p1}, Lfhs;->S(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 755
    invoke-virtual {p0}, Lfhs;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lfhs;->S(Landroid/os/Bundle;)Z

    .line 762
    :cond_0
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 763
    const-string v0, "TRAINING_CLOSET_FETCHER"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhp;

    .line 766
    if-nez v0, :cond_1

    .line 767
    new-instance v0, Lfhp;

    invoke-direct {v0}, Lfhp;-><init>()V

    .line 768
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "TRAINING_CLOSET_FETCHER"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 771
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lfhp;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 773
    iget-boolean v1, v0, Lfhp;->crS:Z

    if-eqz v1, :cond_3

    .line 774
    invoke-virtual {p0}, Lfhs;->onError()V

    .line 780
    :cond_2
    :goto_0
    return-void

    .line 776
    :cond_3
    iget-object v1, v0, Lfhp;->crT:Ljhq;

    if-eqz v1, :cond_2

    .line 777
    iget-object v1, v0, Lfhp;->crT:Ljhq;

    iget v0, v0, Lfhp;->crU:I

    invoke-virtual {p0, v1, v0}, Lfhs;->a(Ljhq;I)V

    goto :goto_0
.end method

.method private S(Landroid/os/Bundle;)Z
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 805
    if-nez p1, :cond_0

    move v0, v1

    .line 823
    :goto_0
    return v0

    .line 809
    :cond_0
    const-string v0, "TRAINING_CLOSET_QUESTION_GROUP_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 810
    const-string v0, "TRAINING_CLOSET_QUESTION_GROUP_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lfhs;->crY:[I

    move v0, v2

    .line 811
    goto :goto_0

    .line 814
    :cond_1
    const-string v0, "com.google.android.search.core.preferences.ARGUMENT_QUESTION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 815
    const-string v0, "com.google.android.search.core.preferences.ARGUMENT_QUESTION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    iput-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 816
    iget-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    if-eqz v0, :cond_2

    move v0, v2

    .line 817
    goto :goto_0

    .line 821
    :cond_2
    const-string v0, "com.google.android.search.core.preferences.ARGUMENT_ATTRIBUTE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfhs;->csa:I

    move v0, v1

    .line 823
    goto :goto_0
.end method

.method public static a(Lfpr;Lfpr;)I
    .locals 2

    .prologue
    .line 1152
    iget-object v0, p0, Lfpr;->czb:Ljdg;

    invoke-virtual {v0}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1153
    iget-object v1, p1, Lfpr;->czb:Ljdg;

    invoke-virtual {v1}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1154
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 1155
    const/4 v0, 0x0

    .line 1168
    :cond_0
    :goto_0
    return v0

    .line 1156
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 1157
    const/4 v0, -0x1

    goto :goto_0

    .line 1158
    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    .line 1159
    const/4 v0, 0x1

    goto :goto_0

    .line 1161
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 1162
    if-nez v0, :cond_0

    .line 1168
    invoke-virtual {p0}, Lfpr;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1}, Lfpr;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljhr;Ljava/util/Collection;)I
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1401
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 1402
    invoke-virtual {v6, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 1405
    :goto_0
    invoke-virtual {v6}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1406
    invoke-virtual {v6}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhr;

    .line 1407
    iget-object v3, v0, Ljhr;->ecf:[I

    const/4 v5, 0x4

    invoke-static {v3, v5}, Lius;->d([II)Z

    move-result v7

    .line 1410
    iget-object v8, v0, Ljhr;->emf:[Ljdj;

    array-length v9, v8

    move v5, v2

    move v3, v1

    :goto_1
    if-ge v5, v9, :cond_6

    aget-object v1, v8, v5

    .line 1416
    iget-object v10, v1, Ljdj;->ame:Ljde;

    iget-object v10, v10, Ljde;->ebz:Ljdf;

    if-eqz v10, :cond_3

    invoke-static {v0}, Lfhs;->e(Ljhr;)Z

    move-result v10

    if-nez v10, :cond_3

    if-eqz v7, :cond_0

    iget-object v10, v1, Ljdj;->ame:Ljde;

    iget-object v10, v10, Ljde;->ebz:Ljdf;

    invoke-virtual {v10}, Ljdf;->bhh()Z

    move-result v10

    if-ne v10, v4, :cond_3

    :cond_0
    iget-object v10, v1, Ljdj;->ame:Ljde;

    iget-object v10, v10, Ljde;->ebv:[Ljdg;

    array-length v10, v10

    if-lez v10, :cond_2

    iget-object v1, v1, Ljdj;->ame:Ljde;

    iget-object v10, v1, Ljde;->ebv:[Ljdg;

    array-length v11, v10

    move v1, v2

    :goto_2
    if-ge v1, v11, :cond_2

    aget-object v12, v10, v1

    invoke-virtual {v12}, Ljdg;->bho()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-virtual {v12}, Ljdg;->bhn()Ljava/lang/String;

    move-result-object v10

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lamm;

    invoke-virtual {v1}, Lamm;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfzy;->mg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    move v1, v2

    :goto_3
    if-nez v1, :cond_3

    .line 1421
    add-int/lit8 v3, v3, 0x1

    .line 1410
    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_4
    move v1, v4

    .line 1416
    goto :goto_3

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1424
    :cond_6
    iget-object v0, v0, Ljhr;->emg:[Ljhr;

    invoke-static {v6, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    move v1, v3

    .line 1425
    goto/16 :goto_0

    .line 1426
    :cond_7
    return v1
.end method

.method private static a([Ljhr;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1460
    .line 1461
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, p0, v1

    .line 1462
    invoke-static {v2}, Lfhs;->e(Ljhr;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1467
    :goto_1
    return v0

    .line 1465
    :cond_0
    add-int/lit8 v2, v0, 0x1

    .line 1461
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 1467
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdg;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;
    .locals 2

    .prologue
    .line 564
    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljdf;->hH(Z)Ljdf;

    move-result-object v0

    .line 565
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdg;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v1

    .line 567
    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdf;)V

    .line 568
    return-object v1
.end method

.method private a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)Ljde;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 457
    new-array v0, v3, [Ljhr;

    const/4 v1, 0x0

    iget-object v2, p0, Lfhs;->csb:Ljhr;

    aput-object v2, v0, v1

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    new-instance v2, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v2, v1}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    new-instance v1, Lfhx;

    invoke-direct {v1, v2}, Lfhx;-><init>(Lcom/google/android/sidekick/shared/training/QuestionKey;)V

    invoke-static {v0, v1}, Lfhs;->a([Ljhr;Lifw;)Ljhr;

    move-result-object v1

    .line 460
    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v0

    .line 461
    if-eqz v1, :cond_0

    .line 462
    invoke-direct {p0, v1, p2, v3}, Lfhs;->a(Ljhr;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Z)Ljde;

    move-result-object v0

    .line 463
    invoke-direct {p0}, Lfhs;->aAb()V

    .line 467
    :goto_0
    return-object v0

    .line 465
    :cond_0
    const-string v1, "TrainingClosetFragment"

    const-string v2, "Could not find add entity QuestionGroup"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Ljhr;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Z)Ljde;
    .locals 16

    .prologue
    .line 478
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCn()Ljdg;

    move-result-object v5

    .line 479
    new-instance v7, Lfpr;

    invoke-direct {v7, v5}, Lfpr;-><init>(Ljdg;)V

    .line 480
    invoke-static {v5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v6

    .line 483
    move-object/from16 v0, p1

    iget-object v1, v0, Ljhr;->emf:[Ljdj;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v8

    const/4 v2, 0x0

    new-instance v9, Lfpr;

    invoke-direct {v9, v5}, Lfpr;-><init>(Ljdg;)V

    move-object/from16 v0, p1

    iget-object v10, v0, Ljhr;->emf:[Ljdj;

    array-length v11, v10

    const/4 v1, 0x0

    move v4, v1

    move-object v3, v6

    :goto_0
    if-ge v4, v11, :cond_3

    aget-object v12, v10, v4

    if-nez v2, :cond_c

    const/4 v1, 0x0

    :goto_1
    iget-object v13, v12, Ljdj;->ame:Ljde;

    iget-object v13, v13, Ljde;->ebv:[Ljdg;

    array-length v13, v13

    if-ge v1, v13, :cond_c

    iget-object v13, v12, Ljdj;->ame:Ljde;

    iget-object v13, v13, Ljde;->ebv:[Ljdg;

    aget-object v13, v13, v1

    new-instance v14, Lfpr;

    invoke-direct {v14, v13}, Lfpr;-><init>(Ljdg;)V

    invoke-virtual {v9, v14}, Lfpr;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    iget-object v2, v12, Ljdj;->ame:Ljde;

    iget-object v3, v6, Ljde;->ebz:Ljdf;

    iput-object v3, v2, Ljde;->ebz:Ljdf;

    iget-object v2, v12, Ljdj;->ame:Ljde;

    iget-object v2, v2, Ljde;->ebv:[Ljdg;

    aput-object v5, v2, v1

    iget-object v2, v12, Ljdj;->ame:Ljde;

    const/4 v1, 0x1

    :goto_2
    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move v2, v1

    goto :goto_0

    :cond_0
    invoke-static {v14, v9}, Lfhs;->a(Lfpr;Lfpr;)I

    move-result v14

    if-gtz v14, :cond_1

    invoke-virtual {v13}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_2

    :cond_1
    new-instance v1, Ljdj;

    invoke-direct {v1}, Ljdj;-><init>()V

    iput-object v6, v1, Ljdj;->ame:Ljde;

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    move-object v2, v3

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljdj;

    invoke-interface {v8, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljdj;

    move-object/from16 v0, p1

    iput-object v1, v0, Ljhr;->emf:[Ljdj;

    .line 484
    move-object/from16 v0, p0

    iget-object v1, v0, Lfhs;->csb:Ljhr;

    if-nez v1, :cond_4

    .line 528
    :goto_3
    return-object v3

    .line 490
    :cond_4
    new-instance v1, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-static {}, Ljdf;->bhg()[Ljdf;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-direct {v1, v0, v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;-><init>(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;[Ljdf;)V

    sget-object v2, Lfif;->csB:Lfif;

    invoke-static {v2, v1}, Lfie;->a(Lfif;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)Lfie;

    move-result-object v8

    .line 491
    invoke-virtual/range {p0 .. p0}, Lfhs;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v9

    .line 492
    add-int/lit8 v1, v9, 0x1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 493
    const/4 v5, 0x1

    .line 494
    const/4 v2, -0x1

    .line 495
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v9, :cond_9

    .line 496
    invoke-virtual/range {p0 .. p0}, Lfhs;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfie;

    .line 497
    if-eqz v5, :cond_5

    .line 498
    iget-object v11, v1, Lfie;->cso:Lfif;

    sget-object v12, Lfif;->csB:Lfif;

    if-ne v11, v12, :cond_5

    .line 500
    iget-object v11, v1, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 501
    invoke-virtual {v11}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v2

    iget-object v12, v2, Ljde;->ebv:[Ljdg;

    array-length v13, v12

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v13, :cond_b

    aget-object v14, v12, v2

    .line 502
    new-instance v15, Lfpr;

    invoke-direct {v15, v14}, Lfpr;-><init>(Ljdg;)V

    .line 503
    invoke-virtual {v7, v15}, Lfpr;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 504
    invoke-virtual {v11}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    iget-object v5, v6, Ljde;->ebz:Ljdf;

    invoke-virtual {v2, v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdf;)V

    .line 505
    const/4 v2, 0x0

    move v5, v2

    move v2, v4

    .line 517
    :cond_5
    :goto_6
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 518
    iget-object v1, v1, Lfie;->cso:Lfif;

    sget-object v11, Lfif;->csx:Lfif;

    if-ne v1, v11, :cond_6

    move v2, v4

    .line 495
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 508
    :cond_7
    invoke-static {v15, v7}, Lfhs;->a(Lfpr;Lfpr;)I

    move-result v14

    .line 509
    if-lez v14, :cond_8

    .line 510
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 511
    const/4 v2, 0x0

    move v5, v2

    move v2, v4

    .line 512
    goto :goto_6

    .line 501
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 522
    :cond_9
    if-eqz v5, :cond_a

    .line 524
    add-int/lit8 v1, v2, 0x1

    invoke-interface {v10, v1, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 527
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lfhs;->crW:Lfic;

    move/from16 v0, p3

    invoke-virtual {v1, v10, v0}, Lfic;->e(Ljava/util/List;Z)V

    goto/16 :goto_3

    :cond_b
    move v2, v4

    goto :goto_6

    :cond_c
    move v1, v2

    move-object v2, v3

    goto/16 :goto_2
.end method

.method static a(Ljhr;Lcom/google/android/sidekick/shared/training/QuestionKey;)Ljde;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 944
    iget-object v1, p0, Ljhr;->emf:[Ljdj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 945
    new-instance v4, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v5, v3, Ljdj;->ame:Ljde;

    invoke-direct {v4, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {p1, v4}, Lcom/google/android/sidekick/shared/training/QuestionKey;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 946
    iget-object v0, v3, Ljdj;->ame:Ljde;

    .line 949
    :goto_1
    return-object v0

    .line 944
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 949
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a([Ljhr;Lifw;)Ljhr;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 871
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 872
    invoke-static {v1, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 874
    :goto_0
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 875
    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhr;

    .line 876
    invoke-interface {p1, v0}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 882
    :goto_1
    return-object v0

    .line 879
    :cond_0
    iget-object v0, v0, Ljhr;->emg:[Ljhr;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_0

    .line 882
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lfie;)V
    .locals 3

    .prologue
    .line 400
    iget-object v0, p1, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    const-string v1, "stock_fetcher"

    invoke-static {v0, p0, v1}, Lczc;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfhs;Ljava/lang/String;)Lczc;

    move-result-object v0

    .line 402
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "add_stock_dialog_tag"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 403
    return-void
.end method

.method private a(Ljava/util/List;Ljhr;Ljava/util/Map;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1305
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 1306
    iget-object v4, p2, Ljhr;->emf:[Ljdj;

    array-length v5, v4

    move v1, v6

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v0, v4, v1

    .line 1307
    new-instance v7, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v0, v0, Ljdj;->ame:Ljde;

    invoke-direct {v7, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {p3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 1309
    if-eqz v0, :cond_0

    .line 1310
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1306
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1313
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 1314
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "TRAINING_IB_"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1316
    iget-object v1, p0, Lfhs;->aOR:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfhs;->aOR:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1317
    iget-object v1, p0, Lfhs;->aOR:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    .line 1318
    iget-object v1, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->a(Lfdn;)V

    move-object v4, v0

    .line 1323
    :goto_1
    new-instance v0, Lfie;

    sget-object v1, Lfif;->csA:Lfif;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lfie;-><init>(Lfif;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;Ljhr;I)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1324
    return-void

    .line 1320
    :cond_2
    new-instance v4, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    const/4 v0, 0x2

    iget-object v1, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    invoke-direct {v4, v0, v1, v3}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;-><init>(ILfdn;Ljava/util/Collection;)V

    goto :goto_1
.end method

.method private static a(Ljava/util/LinkedList;Ljhr;[Ljhr;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 725
    move v0, v1

    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_0

    .line 727
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 728
    aget-object v3, p2, v0

    .line 729
    if-ne v3, p1, :cond_1

    move v1, v2

    .line 743
    :cond_0
    :goto_1
    return v1

    .line 734
    :cond_1
    iget-object v3, v3, Ljhr;->emg:[Ljhr;

    invoke-static {p0, p1, v3}, Lfhs;->a(Ljava/util/LinkedList;Ljhr;[Ljhr;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 736
    goto :goto_1

    .line 740
    :cond_2
    invoke-virtual {p0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    .line 725
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Ljhr;)[I
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 705
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 706
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    invoke-static {v3, p1, v0}, Lfhs;->a(Ljava/util/LinkedList;Ljhr;[Ljhr;)Z

    .line 707
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    const/4 v0, 0x0

    .line 715
    :goto_0
    return-object v0

    .line 710
    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 711
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 712
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 713
    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    .line 712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 715
    goto :goto_0
.end method

.method private aAb()V
    .locals 3

    .prologue
    .line 1037
    iget-object v1, p0, Lfhs;->crT:Ljhq;

    invoke-virtual {p0}, Lfhs;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "TRAINING_CLOSET_FETCHER"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhp;

    if-eqz v0, :cond_0

    iput-object v1, v0, Lfhp;->crT:Ljhq;

    .line 1040
    :cond_0
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    iput-object v0, p0, Lfhs;->crX:[B

    .line 1041
    return-void
.end method

.method private aAc()V
    .locals 3

    .prologue
    .line 1082
    const/4 v0, 0x0

    .line 1084
    iget-object v1, p0, Lfhs;->csb:Ljhr;

    if-nez v1, :cond_1

    .line 1085
    iget-object v1, p0, Lfhs;->crT:Ljhq;

    iget-object v1, v1, Ljhq;->emd:[Ljhr;

    invoke-static {v1}, Lfhs;->a([Ljhr;)I

    move-result v1

    .line 1087
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1088
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    aget-object v0, v0, v1

    invoke-static {v0}, Lfhs;->b(Ljhr;)Ljava/util/Collection;

    move-result-object v0

    .line 1095
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 1096
    iget-object v1, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v1, v0, p0}, Lfdn;->a(Ljava/util/Collection;Lemy;)V

    .line 1100
    :goto_1
    return-void

    .line 1092
    :cond_1
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    invoke-static {v0}, Lfhs;->b(Ljhr;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    .line 1098
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lfhs;->v(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method private aAd()V
    .locals 2

    .prologue
    .line 1197
    invoke-virtual {p0}, Lfhs;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1198
    invoke-virtual {p0}, Lfhs;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 1199
    return-void
.end method

.method static b(Ljhr;)Ljava/util/Collection;
    .locals 6

    .prologue
    .line 1179
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 1180
    iget-object v0, p0, Ljhr;->emf:[Ljdj;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 1181
    iget-object v2, p0, Ljhr;->emg:[Ljhr;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1182
    invoke-static {v4}, Lfhs;->d(Ljhr;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1183
    iget-object v4, v4, Ljhr;->emf:[Ljdj;

    invoke-static {v1, v4}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 1181
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1187
    :cond_1
    return-object v1
.end method

.method private b(Lfie;)V
    .locals 3

    .prologue
    .line 409
    iget-object v0, p1, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    const-string v1, "sport_team_fetcher"

    invoke-static {v0, p0, v1}, Lcyx;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfhs;Ljava/lang/String;)Lcyx;

    move-result-object v0

    .line 412
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "add_sport_team_dialog_tag"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 413
    return-void
.end method

.method private b(Ljava/util/List;Ljhr;Ljava/util/Map;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1332
    .line 1333
    iget-object v0, p2, Ljhr;->ecf:[I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lius;->d([II)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1334
    sget-object v0, Lfif;->csB:Lfif;

    move-object v1, v0

    .line 1336
    :goto_0
    iget-object v6, p2, Ljhr;->emf:[Ljdj;

    array-length v7, v6

    move v4, v5

    :goto_1
    if-ge v4, v7, :cond_6

    aget-object v0, v6, v4

    .line 1337
    new-instance v8, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v0, v0, Ljdj;->ame:Ljde;

    invoke-direct {v8, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 1338
    invoke-interface {p3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 1339
    if-eqz v0, :cond_0

    .line 1340
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move-object v3, v2

    .line 1344
    :goto_2
    if-nez v3, :cond_1

    .line 1345
    const-string v3, "TrainingClosetFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Unexpected question type in closet: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    :cond_0
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1340
    :pswitch_1
    sget-object v3, Lfif;->cst:Lfif;

    goto :goto_2

    :pswitch_2
    sget-object v3, Lfif;->csu:Lfif;

    goto :goto_2

    :pswitch_3
    sget-object v3, Lfif;->csC:Lfif;

    goto :goto_2

    :pswitch_4
    sget-object v3, Lfif;->csy:Lfif;

    goto :goto_2

    :pswitch_5
    sget-object v3, Lfif;->csz:Lfif;

    goto :goto_2

    :pswitch_6
    sget-object v3, Lfif;->csD:Lfif;

    goto :goto_2

    :pswitch_7
    sget-object v3, Lfif;->csE:Lfif;

    goto :goto_2

    .line 1349
    :cond_1
    sget-object v9, Lfif;->cst:Lfif;

    if-ne v3, v9, :cond_2

    if-eqz v1, :cond_2

    move-object v3, v1

    .line 1352
    :cond_2
    invoke-static {v3, v0}, Lfie;->a(Lfif;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)Lfie;

    move-result-object v0

    .line 1353
    iget-object v9, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v9, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v0}, Lfie;->aAf()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1354
    :cond_3
    sget-object v8, Lfif;->csz:Lfif;

    if-eq v3, v8, :cond_4

    sget-object v8, Lfif;->csy:Lfif;

    if-ne v3, v8, :cond_5

    .line 1356
    :cond_4
    invoke-interface {p1, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 1358
    :cond_5
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1362
    :cond_6
    return-void

    :cond_7
    move-object v1, v2

    goto/16 :goto_0

    .line 1340
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static bi(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 647
    const v0, 0x7f110466

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 648
    const v0, 0x7f110468

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 649
    if-eqz v0, :cond_0

    .line 650
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 652
    :cond_0
    return-void
.end method

.method private c(Ljhr;)Lfie;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1392
    invoke-virtual {p1}, Ljhr;->bme()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v6, p0, Lfhs;->crU:I

    .line 1395
    :goto_0
    new-instance v0, Lfie;

    sget-object v1, Lfif;->csv:Lfif;

    move-object v3, v2

    move-object v4, v2

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lfie;-><init>(Lfif;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;Ljhr;I)V

    return-object v0

    .line 1392
    :cond_0
    iget-object v0, p0, Lfhs;->mCalendarProvider:Leym;

    invoke-interface {v0}, Leym;->aww()Ljava/util/Collection;

    move-result-object v0

    invoke-static {p1, v0}, Lfhs;->a(Ljhr;Ljava/util/Collection;)I

    move-result v6

    goto :goto_0
.end method

.method private static d(Ljhr;)Z
    .locals 1

    .prologue
    .line 1452
    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Ljhr;)Z
    .locals 2

    .prologue
    .line 1474
    invoke-virtual {p0}, Ljhr;->bmf()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljhr;->bme()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v(Ljava/util/Collection;)V
    .locals 14

    .prologue
    .line 1203
    invoke-virtual {p0}, Lfhs;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1297
    :cond_0
    :goto_0
    return-void

    .line 1207
    :cond_1
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v9

    .line 1208
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 1209
    new-instance v2, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v9, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1212
    :cond_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1214
    const/4 v7, 0x0

    .line 1215
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    if-nez v0, :cond_5

    .line 1218
    const/4 v7, 0x1

    .line 1220
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    invoke-static {v0}, Lfhs;->a([Ljhr;)I

    move-result v1

    .line 1223
    const/4 v0, -0x1

    if-eq v1, v0, :cond_3

    .line 1224
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    aget-object v0, v0, v1

    invoke-direct {p0, v10, v0, v9}, Lfhs;->a(Ljava/util/List;Ljhr;Ljava/util/Map;)V

    .line 1230
    :cond_3
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    array-length v2, v0

    .line 1231
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_a

    .line 1232
    if-eq v0, v1, :cond_4

    .line 1233
    iget-object v3, p0, Lfhs;->crT:Ljhq;

    iget-object v3, v3, Ljhq;->emd:[Ljhr;

    aget-object v3, v3, v0

    invoke-direct {p0, v3}, Lfhs;->c(Ljhr;)Lfie;

    move-result-object v3

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1231
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1237
    :cond_5
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    invoke-static {v0}, Lfhs;->e(Ljhr;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1238
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    invoke-direct {p0, v10, v0, v9}, Lfhs;->a(Ljava/util/List;Ljhr;Ljava/util/Map;)V

    .line 1246
    :goto_3
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    iget-object v11, v0, Ljhr;->emg:[Ljhr;

    array-length v12, v11

    const/4 v0, 0x0

    move v8, v0

    :goto_4
    if-ge v8, v12, :cond_a

    aget-object v13, v11, v8

    .line 1247
    invoke-static {v13}, Lfhs;->d(Ljhr;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1248
    iget-object v0, v13, Ljhr;->ecf:[I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lius;->d([II)Z

    move-result v1

    new-instance v0, Lfie;

    if-eqz v1, :cond_7

    sget-object v1, Lfif;->csx:Lfif;

    :goto_5
    invoke-virtual {v13}, Ljhr;->getTitle()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lfie;-><init>(Lfif;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;Ljhr;I)V

    .line 1249
    iget-object v1, v0, Lfie;->cso:Lfif;

    sget-object v2, Lfif;->csx:Lfif;

    if-ne v1, v2, :cond_15

    .line 1251
    const/4 v1, 0x1

    .line 1253
    :goto_6
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1255
    invoke-static {v13}, Lfhs;->e(Ljhr;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1256
    invoke-direct {p0, v10, v13, v9}, Lfhs;->a(Ljava/util/List;Ljhr;Ljava/util/Map;)V

    .line 1246
    :goto_7
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v7, v1

    goto :goto_4

    .line 1240
    :cond_6
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    invoke-direct {p0, v10, v0, v9}, Lfhs;->b(Ljava/util/List;Ljhr;Ljava/util/Map;)V

    goto :goto_3

    .line 1248
    :cond_7
    sget-object v1, Lfif;->csw:Lfif;

    goto :goto_5

    .line 1258
    :cond_8
    invoke-direct {p0, v10, v13, v9}, Lfhs;->b(Ljava/util/List;Ljhr;Ljava/util/Map;)V

    goto :goto_7

    .line 1261
    :cond_9
    invoke-direct {p0, v13}, Lfhs;->c(Ljhr;)Lfie;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v7

    goto :goto_7

    .line 1266
    :cond_a
    if-eqz v7, :cond_b

    .line 1267
    invoke-direct {p0}, Lfhs;->aAd()V

    .line 1270
    :cond_b
    iget-object v0, p0, Lfhs;->crW:Lfic;

    if-nez v0, :cond_d

    .line 1271
    new-instance v0, Lfic;

    invoke-direct {v0, p0, v10}, Lfic;-><init>(Lfhs;Ljava/util/List;)V

    iput-object v0, p0, Lfhs;->crW:Lfic;

    .line 1272
    iget-object v0, p0, Lfhs;->crW:Lfic;

    invoke-virtual {p0, v0}, Lfhs;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 1276
    :goto_8
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    iget-object v4, v0, Lfie;->cso:Lfif;

    sget-object v5, Lfif;->csz:Lfif;

    if-ne v4, v5, :cond_c

    const/4 v1, 0x1

    :cond_c
    iget-object v0, v0, Lfie;->cso:Lfif;

    sget-object v4, Lfif;->csy:Lfif;

    if-ne v0, v4, :cond_14

    const/4 v0, 0x1

    :goto_a
    move v2, v0

    goto :goto_9

    .line 1274
    :cond_d
    iget-object v0, p0, Lfhs;->crW:Lfic;

    invoke-virtual {v0, v10}, Lfic;->X(Ljava/util/List;)V

    goto :goto_8

    .line 1276
    :cond_e
    if-eqz v1, :cond_f

    invoke-virtual {p0}, Lfhs;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v0, "stock_fetcher"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcze;

    if-nez v0, :cond_f

    new-instance v0, Lcze;

    invoke-direct {v0}, Lcze;-><init>()V

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v3, "stock_fetcher"

    invoke-virtual {v1, v0, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_f
    if-eqz v2, :cond_10

    invoke-virtual {p0}, Lfhs;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v0, "sport_team_fetcher"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcyy;

    if-nez v0, :cond_10

    new-instance v0, Lcyy;

    invoke-direct {v0}, Lcyy;-><init>()V

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "sport_team_fetcher"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 1280
    :cond_10
    iget-object v0, p0, Lfhs;->aOR:Landroid/os/Bundle;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    if-eqz v0, :cond_0

    .line 1281
    iget-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v1

    .line 1282
    const/4 v0, -0x1

    if-eq v1, v0, :cond_11

    const/4 v0, -0x2

    if-ne v1, v0, :cond_0

    .line 1284
    :cond_11
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    .line 1285
    iget-object v3, v0, Lfie;->cso:Lfif;

    sget-object v4, Lfif;->csy:Lfif;

    if-ne v3, v4, :cond_13

    const/4 v3, -0x1

    if-ne v1, v3, :cond_13

    .line 1287
    invoke-direct {p0, v0}, Lfhs;->b(Lfie;)V

    goto/16 :goto_0

    .line 1289
    :cond_13
    iget-object v3, v0, Lfie;->cso:Lfif;

    sget-object v4, Lfif;->csz:Lfif;

    if-ne v3, v4, :cond_12

    const/4 v3, -0x2

    if-ne v1, v3, :cond_12

    .line 1291
    invoke-direct {p0, v0}, Lfhs;->a(Lfie;)V

    goto/16 :goto_0

    :cond_14
    move v0, v2

    goto/16 :goto_a

    :cond_15
    move v1, v7

    goto/16 :goto_6
.end method


# virtual methods
.method final a(Landroid/view/View;Lfie;)V
    .locals 2

    .prologue
    .line 362
    new-instance v0, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v1, p2, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 366
    const v1, 0x7f110468

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 367
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 368
    :cond_0
    iget-object v1, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 369
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2}, Lfhs;->a(Landroid/view/ViewGroup;Lfie;)V

    .line 374
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v1, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 372
    invoke-static {p1}, Lfhs;->bi(Landroid/view/View;)V

    goto :goto_0
.end method

.method final a(Landroid/view/ViewGroup;Lfie;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 585
    invoke-virtual {p0}, Lfhs;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->jumpDrawablesToCurrentState()V

    .line 587
    iget-object v0, p2, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    .line 588
    const v0, 0x7f110466

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 589
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 591
    const v0, 0x7f110467

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 592
    const/4 v1, 0x0

    .line 593
    if-eqz v0, :cond_1

    .line 594
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lfqk;->F(IZ)Ljava/lang/Integer;

    move-result-object v3

    .line 596
    if-eqz v3, :cond_2

    .line 597
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 598
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 599
    invoke-virtual {p0}, Lfhs;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lfqk;->a(Landroid/view/View;Landroid/content/res/Resources;)V

    .line 601
    check-cast v0, Lfqb;

    .line 608
    :goto_0
    if-eqz v0, :cond_0

    .line 609
    invoke-interface {v0, v2}, Lfqb;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V

    .line 610
    const v1, 0x7f0b00af

    invoke-interface {v0, v1, v5}, Lfqb;->aU(II)V

    .line 611
    new-instance v1, Lfht;

    invoke-direct {v1, p0, v2, p1, p2}, Lfht;-><init>(Lfhs;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Landroid/view/ViewGroup;Lfie;)V

    invoke-interface {v0, v1}, Lfqb;->a(Lfqc;)V

    .line 632
    check-cast v0, Landroid/view/View;

    .line 633
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 636
    new-instance v1, Lfhu;

    invoke-direct {v1, p0, p1, p2}, Lfhu;-><init>(Lfhs;Landroid/view/ViewGroup;Lfie;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 643
    :cond_0
    return-void

    .line 606
    :cond_1
    const v0, 0x7f110468

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfqb;

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V
    .locals 0

    .prologue
    .line 254
    invoke-virtual {p0, p1, p2}, Lfhs;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V

    .line 255
    invoke-direct {p0}, Lfhs;->aAc()V

    .line 256
    return-void
.end method

.method public final a(Lczb;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 423
    invoke-virtual {p1}, Lczb;->Ue()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    if-nez v0, :cond_2

    move-object v0, v1

    .line 425
    :goto_1
    if-nez v0, :cond_3

    .line 431
    :goto_2
    return-void

    .line 423
    :cond_0
    iget-object v0, p1, Lczb;->bnD:Ljar;

    invoke-virtual {v0}, Ljar;->bez()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lczb;->bnD:Ljar;

    invoke-virtual {v0}, Ljar;->bey()I

    move-result v0

    :goto_3
    new-instance v2, Ljfh;

    invoke-direct {v2}, Ljfh;-><init>()V

    iget-object v3, p1, Lczb;->bnD:Ljar;

    invoke-virtual {v3}, Ljar;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljfh;->un(Ljava/lang/String;)Ljfh;

    move-result-object v2

    iget-object v3, p1, Lczb;->bnD:Ljar;

    invoke-virtual {v3}, Ljar;->bev()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljfh;->uo(Ljava/lang/String;)Ljfh;

    move-result-object v2

    iget-object v3, p1, Lczb;->bnD:Ljar;

    invoke-virtual {v3}, Ljar;->bew()I

    move-result v3

    invoke-virtual {v2, v3}, Ljfh;->pt(I)Ljfh;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljfh;->pu(I)Ljfh;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_3

    :cond_2
    new-instance v2, Ljdg;

    invoke-direct {v2}, Ljdg;-><init>()V

    invoke-virtual {p1}, Lczb;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljdg;->tz(Ljava/lang/String;)Ljdg;

    move-result-object v2

    iput-object v0, v2, Ljdg;->ebK:Ljfh;

    invoke-static {p2, v2}, Lfhs;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdg;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    goto :goto_1

    .line 428
    :cond_3
    invoke-direct {p0, p2, v0}, Lfhs;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)Ljde;

    move-result-object v2

    .line 430
    iget-object v3, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    invoke-interface {v3, v2, v0, v1}, Lfdn;->a(Ljde;Ljdf;Lizj;)V

    goto :goto_2
.end method

.method public final a(Lczd;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V
    .locals 4

    .prologue
    .line 442
    new-instance v0, Ljdg;

    invoke-direct {v0}, Ljdg;-><init>()V

    iget-object v1, p1, Lczd;->bnG:Ljfl;

    invoke-virtual {v1}, Ljfl;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljdg;->tz(Ljava/lang/String;)Ljdg;

    move-result-object v0

    iget-object v1, p1, Lczd;->bnG:Ljfl;

    iput-object v1, v0, Ljdg;->bnG:Ljfl;

    invoke-static {p2, v0}, Lfhs;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdg;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    .line 443
    invoke-direct {p0, p2, v0}, Lfhs;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)Ljde;

    move-result-object v1

    .line 445
    iget-object v2, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v2, v1, v0, v3}, Lfdn;->a(Ljde;Ljdf;Lizj;)V

    .line 446
    return-void
.end method

.method public final a(Ljhq;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1045
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1048
    invoke-virtual {p0}, Lfhs;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1075
    :cond_0
    :goto_0
    return-void

    .line 1052
    :cond_1
    iget-object v0, p0, Lfhs;->crY:[I

    if-nez v0, :cond_2

    iget-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    if-nez v0, :cond_2

    iget v0, p0, Lfhs;->csa:I

    if-eqz v0, :cond_8

    :cond_2
    iget-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    if-eqz v0, :cond_5

    iget-object v0, p1, Ljhq;->emd:[Ljhr;

    iget-object v1, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    new-instance v2, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v2, v1}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    new-instance v1, Lfhv;

    invoke-direct {v1, v2}, Lfhv;-><init>(Lcom/google/android/sidekick/shared/training/QuestionKey;)V

    invoke-static {v0, v1}, Lfhs;->a([Ljhr;Lifw;)Ljhr;

    move-result-object v0

    :cond_3
    :goto_1
    iput-object v0, p0, Lfhs;->csb:Ljhr;

    .line 1053
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    if-nez v0, :cond_a

    .line 1054
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    const v1, 0x7f0a044f

    invoke-virtual {p0, v1}, Lfhs;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->lO(Ljava/lang/String;)V

    .line 1056
    invoke-direct {p0}, Lfhs;->aAd()V

    .line 1061
    :goto_2
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 1062
    iget v1, p0, Lfhs;->crU:I

    if-ne v1, p2, :cond_4

    iget-object v1, p0, Lfhs;->crX:[B

    if-eqz v1, :cond_4

    iget-object v1, p0, Lfhs;->crX:[B

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1070
    :cond_4
    iput-object p1, p0, Lfhs;->crT:Ljhq;

    .line 1071
    iput p2, p0, Lfhs;->crU:I

    .line 1072
    iput-object v0, p0, Lfhs;->crX:[B

    .line 1074
    invoke-direct {p0}, Lfhs;->aAc()V

    goto :goto_0

    .line 1052
    :cond_5
    iget v0, p0, Lfhs;->csa:I

    if-eqz v0, :cond_6

    iget-object v0, p1, Ljhq;->emd:[Ljhr;

    iget v1, p0, Lfhs;->csa:I

    new-instance v2, Lfhw;

    invoke-direct {v2, v1}, Lfhw;-><init>(I)V

    invoke-static {v0, v2}, Lfhs;->a([Ljhr;Lifw;)Ljhr;

    move-result-object v0

    goto :goto_1

    :cond_6
    iget-object v3, p0, Lfhs;->crY:[I

    iget-object v0, p1, Ljhq;->emd:[Ljhr;

    move v1, v2

    :goto_3
    array-length v4, v3

    if-ge v1, v4, :cond_8

    aget v4, v3, v1

    if-ltz v4, :cond_7

    array-length v5, v0

    if-lt v4, v5, :cond_9

    :cond_7
    const-string v0, "TrainingClosetFragment"

    const-string v4, "Invalid index at depth: %d, path: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v1, 0x1

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    :cond_9
    aget-object v0, v0, v4

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-eq v1, v4, :cond_3

    iget-object v0, v0, Ljhr;->emg:[Ljhr;

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1058
    :cond_a
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    iget-object v1, p0, Lfhs;->csb:Ljhr;

    invoke-virtual {v1}, Ljhr;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->lO(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final aAa()Z
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    const/4 v0, 0x0

    .line 324
    :goto_0
    return v0

    .line 320
    :cond_0
    iget-object v0, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 321
    iget-object v0, p0, Lfhs;->crW:Lfic;

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p0, Lfhs;->crW:Lfic;

    invoke-virtual {v0}, Lfic;->notifyDataSetChanged()V

    .line 324
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 87
    check-cast p1, Ljava/util/Collection;

    invoke-direct {p0, p1}, Lfhs;->v(Ljava/util/Collection;)V

    const/4 v0, 0x1

    return v0
.end method

.method final b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, p2, v2}, Lfdn;->a(Ljde;Ljdf;Lizj;)V

    .line 262
    invoke-virtual {p0, p1, p2}, Lfhs;->c(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V

    .line 263
    return-void
.end method

.method final b(Lfhn;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1486
    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCo()[I

    move-result-object v1

    .line 1487
    const/4 v0, 0x0

    .line 1491
    array-length v3, v1

    if-lez v3, :cond_0

    .line 1492
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lfhy;

    invoke-direct {v3, v1}, Lfhy;-><init>([I)V

    invoke-static {v0, v3}, Lfhs;->a([Ljhr;Lifw;)Ljhr;

    move-result-object v0

    .line 1497
    :cond_0
    if-nez v0, :cond_b

    .line 1498
    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v0, v0, Ljhq;->emd:[Ljhr;

    new-instance v1, Lfhz;

    invoke-direct {v1, v6}, Lfhz;-><init>(I)V

    invoke-static {v0, v1}, Lfhs;->a([Ljhr;Lifw;)Ljhr;

    move-result-object v0

    move-object v5, v0

    .line 1502
    :goto_0
    if-eqz v5, :cond_9

    .line 1503
    invoke-virtual {p2, p3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdf;)V

    .line 1504
    iget-object v0, v5, Ljhr;->ecf:[I

    invoke-static {v0, v6}, Lius;->d([II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1505
    invoke-direct {p0, v5, p2, v4}, Lfhs;->a(Ljhr;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Z)Ljde;

    .line 1514
    :goto_1
    iget-object v0, p0, Lfhs;->crW:Lfic;

    invoke-virtual {v0}, Lfic;->aAe()Ljava/util/List;

    move-result-object v0

    .line 1516
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lfie;

    .line 1517
    iget-object v0, v1, Lfie;->cso:Lfif;

    sget-object v7, Lfif;->csv:Lfif;

    if-ne v0, v7, :cond_a

    iget-object v0, v1, Lfie;->csr:Ljhr;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v7, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_3
    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v7}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhr;

    if-ne v0, v5, :cond_2

    move v0, v2

    :goto_4
    if-eqz v0, :cond_a

    .line 1519
    iget-object v0, p0, Lfhs;->mCalendarProvider:Leym;

    invoke-interface {v0}, Leym;->aww()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v5, v0}, Lfhs;->a(Ljhr;Ljava/util/Collection;)I

    move-result v0

    iput v0, v1, Lfie;->css:I

    move v0, v2

    :goto_5
    move v3, v0

    .line 1523
    goto :goto_2

    .line 1507
    :cond_1
    new-instance v0, Ljdj;

    invoke-direct {v0}, Ljdj;-><init>()V

    .line 1508
    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    iput-object v1, v0, Ljdj;->ame:Ljde;

    .line 1509
    iget-object v1, v5, Ljhr;->emf:[Ljdj;

    invoke-static {v1, v0}, Leqh;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljdj;

    iput-object v0, v5, Ljhr;->emf:[Ljdj;

    goto :goto_1

    .line 1517
    :cond_2
    iget-object v0, v0, Ljhr;->emg:[Ljhr;

    invoke-static {v7, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    move v0, v4

    goto :goto_4

    .line 1524
    :cond_4
    iget-object v0, p0, Lfhs;->csb:Ljhr;

    if-nez v0, :cond_7

    :cond_5
    :goto_6
    or-int v0, v3, v4

    .line 1527
    invoke-direct {p0}, Lfhs;->aAb()V

    .line 1529
    if-eqz v0, :cond_6

    .line 1530
    new-instance v0, Lfia;

    invoke-direct {v0, p0}, Lfia;-><init>(Lfhs;)V

    invoke-static {}, Lenu;->auR()V

    iget-object v1, p1, Lfhn;->crO:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lfhn;->azW()V

    .line 1540
    :cond_6
    :goto_7
    return-void

    .line 1524
    :cond_7
    invoke-static {v5}, Lfhs;->e(Ljhr;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v5}, Lfhs;->d(Ljhr;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfhs;->csb:Ljhr;

    if-eq v5, v0, :cond_8

    iget-object v0, p0, Lfhs;->csb:Ljhr;

    iget-object v0, v0, Ljhr;->emg:[Ljhr;

    array-length v1, v0

    :goto_8
    if-ge v4, v1, :cond_8

    aget-object v6, v0, v4

    if-eq v5, v6, :cond_8

    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_8
    move v4, v2

    goto :goto_6

    .line 1538
    :cond_9
    const-string v0, "TrainingClosetFragment"

    const-string v1, "Could not find target group for icebreaker question"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :cond_a
    move v0, v3

    goto :goto_5

    :cond_b
    move-object v5, v0

    goto/16 :goto_0
.end method

.method final c(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 660
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p0, Lfhs;->crT:Ljhq;

    iget-object v1, v0, Ljhq;->emd:[Ljhr;

    array-length v4, v1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v1, v0

    invoke-virtual {v3, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v4, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    :goto_1
    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhr;

    iget-object v6, v0, Ljhr;->emf:[Ljdj;

    array-length v7, v6

    move v1, v2

    :goto_2
    if-ge v1, v7, :cond_2

    aget-object v8, v6, v1

    new-instance v9, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v10, v8, Ljdj;->ame:Ljde;

    invoke-direct {v9, v10}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {v4, v9}, Lcom/google/android/sidekick/shared/training/QuestionKey;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v8, v8, Ljdj;->ame:Ljde;

    invoke-interface {v5, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, v0, Ljhr;->emg:[Ljhr;

    invoke-static {v3, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_1

    .line 661
    :cond_3
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 662
    const-string v0, "TrainingClosetFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find raw question: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v2

    invoke-virtual {v2}, Ljde;->bhe()[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    :goto_3
    return-void

    .line 667
    :cond_4
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljde;

    .line 668
    iput-object p2, v0, Ljde;->ebz:Ljdf;

    goto :goto_4

    .line 670
    :cond_5
    invoke-direct {p0}, Lfhs;->aAb()V

    goto :goto_3
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 226
    invoke-super {p0, p1}, Lcxh;->onActivityCreated(Landroid/os/Bundle;)V

    .line 228
    invoke-virtual {p0}, Lfhs;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 230
    iput-object p1, p0, Lfhs;->aOR:Landroid/os/Bundle;

    .line 232
    if-eqz p1, :cond_0

    const-string v0, "TRAINING_CLOSET_EXPANDED_QUESTIONS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "TRAINING_CLOSET_EXPANDED_QUESTIONS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 235
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfhs;->csc:Ljava/util/Set;

    .line 236
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 237
    iget-object v4, p0, Lfhs;->csc:Ljava/util/Set;

    check-cast v0, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241
    :cond_0
    invoke-direct {p0, p1}, Lfhs;->R(Landroid/os/Bundle;)V

    .line 242
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcxh;->onCreate(Landroid/os/Bundle;)V

    .line 188
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v1

    iput-object v1, p0, Lfhs;->mTrainingQuestionManager:Lfdn;

    .line 190
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lfhs;->csc:Ljava/util/Set;

    .line 191
    invoke-virtual {v0}, Lfdb;->axN()Leym;

    move-result-object v0

    iput-object v0, p0, Lfhs;->mCalendarProvider:Leym;

    .line 192
    return-void
.end method

.method public final onError()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1577
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a045b

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1578
    new-instance v0, Ljhq;

    invoke-direct {v0}, Ljhq;-><init>()V

    invoke-virtual {p0, v0, v2}, Lfhs;->a(Ljhq;I)V

    .line 1579
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 329
    invoke-virtual {p0}, Lfhs;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    .line 330
    sget-object v1, Lfib;->csk:[I

    iget-object v2, v0, Lfie;->cso:Lfif;

    invoke-virtual {v2}, Lfif;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 351
    :goto_0
    return-void

    .line 332
    :pswitch_0
    iget-object v0, v0, Lfie;->csr:Ljhr;

    invoke-virtual {v0}, Ljhr;->bme()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->azZ()V

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lfhs;->crT:Ljhq;

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Lfhs;->a(Ljhr;)[I

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "TRAINING_CLOSET_QUESTION_GROUP_PATH"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    :cond_1
    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->Q(Landroid/os/Bundle;)V

    goto :goto_0

    .line 338
    :pswitch_1
    invoke-virtual {p0, p2, v0}, Lfhs;->a(Landroid/view/View;Lfie;)V

    goto :goto_0

    .line 341
    :pswitch_2
    iget-object v0, v0, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-static {v0, p0}, Lfhj;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Landroid/app/Fragment;)Lfhj;

    move-result-object v0

    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "multiple_select_dialog_tag"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 344
    :pswitch_3
    iget-object v0, v0, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    const-string v1, "tv_lineup_worker"

    const-string v2, "tv_lineup_zipcode_dialog"

    invoke-static {v0, v1, v2}, Lczj;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljava/lang/String;Ljava/lang/String;)Lczj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "tv_lineup_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 347
    :pswitch_4
    invoke-direct {p0, v0}, Lfhs;->b(Lfie;)V

    goto :goto_0

    .line 350
    :pswitch_5
    invoke-direct {p0, v0}, Lfhs;->a(Lfie;)V

    goto :goto_0

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1584
    instance-of v0, p1, Lfhf;

    if-eqz v0, :cond_0

    .line 1585
    check-cast p1, Lfhf;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lfhf;->a(Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;)V

    .line 1587
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 246
    invoke-super {p0}, Lcxh;->onResume()V

    .line 249
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfhs;->R(Landroid/os/Bundle;)V

    .line 250
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 196
    iget-object v0, p0, Lfhs;->crY:[I

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "TRAINING_CLOSET_QUESTION_GROUP_PATH"

    iget-object v1, p0, Lfhs;->crY:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 199
    :cond_0
    iget-object v0, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    if-eqz v0, :cond_1

    .line 200
    const-string v0, "com.google.android.search.core.preferences.ARGUMENT_QUESTION"

    iget-object v1, p0, Lfhs;->crZ:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 202
    :cond_1
    const-string v0, "com.google.android.search.core.preferences.ARGUMENT_ATTRIBUTE"

    iget v1, p0, Lfhs;->csa:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 204
    iget-object v0, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v3, v0, [Landroid/os/Parcelable;

    .line 206
    iget-object v0, p0, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/training/QuestionKey;

    .line 207
    aput-object v0, v3, v1

    .line 208
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 209
    goto :goto_0

    .line 210
    :cond_2
    const-string v0, "TRAINING_CLOSET_EXPANDED_QUESTIONS"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 212
    invoke-virtual {p0}, Lfhs;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 213
    if-eqz v1, :cond_4

    .line 214
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    .line 215
    :goto_1
    if-ge v2, v3, :cond_4

    .line 216
    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    .line 217
    iget-object v4, v0, Lfie;->cso:Lfif;

    sget-object v5, Lfif;->csA:Lfif;

    if-ne v4, v5, :cond_3

    .line 218
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TRAINING_IB_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lfie;->csq:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {p1, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 215
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 222
    :cond_4
    return-void
.end method
