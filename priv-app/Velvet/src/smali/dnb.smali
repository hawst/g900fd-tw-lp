.class public final Ldnb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field static final bCP:[B


# instance fields
.field final atG:Leqo;

.field private final bAZ:Ligi;

.field private bCM:Ljava/lang/String;

.field private final bCN:Ljava/util/List;

.field private volatile bCO:Z

.field private final bCQ:Lesk;

.field private final bCR:Lesk;

.field private final bCS:Landroid/webkit/WebView$PictureListener;

.field final bCT:Ldnj;

.field private final bCU:Ldng;

.field final bCV:Ljava/util/List;

.field private bCW:I

.field private final bCX:Ljava/util/Queue;

.field private final bCY:Ljava/io/File;

.field private bCZ:I

.field private bDa:J

.field bDb:Z

.field private bDc:Z

.field private bDd:Landroid/net/Uri;

.field private bDe:Ljava/util/Map;

.field private final bDf:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final bDg:Ljava/lang/Object;

.field private bDh:Ljava/lang/String;

.field private bDi:Ljava/lang/String;

.field bDj:Ldyo;

.field bDk:Lcnb;

.field bDl:Lcom/google/android/shared/search/Query;

.field private final bDm:Ljava/lang/Object;

.field private bDn:Legl;

.field final bDo:Ljava/lang/Object;

.field private bDp:Ldyo;

.field private bDq:Ljava/lang/String;

.field private final bDr:Ljava/util/concurrent/atomic/AtomicReference;

.field private final mClock:Lemp;

.field private mCommittedQuery:Lcom/google/android/shared/search/Query;

.field final mConfig:Lcjs;

.field private mGsaCommunicationJsHelper:Ldmv;

.field final mGsaConfig:Lchk;

.field private final mLocationSettings:Lcob;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mSettings:Lcke;

.field final mUrlHelper:Lcpn;

.field private mWebAppController:Ldmz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final mWebAppState:Lddk;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Ldnb;->bCP:[B

    return-void
.end method

.method public constructor <init>(Lcke;Lcjs;Lchk;Lemp;Lcpn;Leqo;Ldnj;Lcom/google/android/search/core/state/QueryState;Lddk;Lcob;Ligi;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const-string v0, ""

    iput-object v0, p0, Ldnb;->bCM:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldnb;->bCN:Ljava/util/List;

    .line 152
    new-instance v0, Ldnc;

    const-string v1, "mHandleInternalEvents"

    invoke-direct {v0, p0, v1}, Ldnc;-><init>(Ldnb;Ljava/lang/String;)V

    iput-object v0, p0, Ldnb;->bCQ:Lesk;

    .line 161
    new-instance v0, Ldnd;

    const-string v1, "mEndPreviousResultsSuppression"

    invoke-direct {v0, p0, v1}, Ldnd;-><init>(Ldnb;Ljava/lang/String;)V

    iput-object v0, p0, Ldnb;->bCR:Lesk;

    .line 172
    new-instance v0, Ldne;

    invoke-direct {v0, p0}, Ldne;-><init>(Ldnb;)V

    iput-object v0, p0, Ldnb;->bCS:Landroid/webkit/WebView$PictureListener;

    .line 256
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldnb;->bDf:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 263
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldnb;->bDg:Ljava/lang/Object;

    .line 290
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldnb;->bDm:Ljava/lang/Object;

    .line 297
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldnb;->bDo:Ljava/lang/Object;

    .line 301
    const-string v0, ""

    iput-object v0, p0, Ldnb;->bDq:Ljava/lang/String;

    .line 303
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldnb;->bDr:Ljava/util/concurrent/atomic/AtomicReference;

    .line 311
    iput-object p1, p0, Ldnb;->mSettings:Lcke;

    .line 312
    iput-object p2, p0, Ldnb;->mConfig:Lcjs;

    .line 313
    iput-object p3, p0, Ldnb;->mGsaConfig:Lchk;

    .line 314
    iput-object p4, p0, Ldnb;->mClock:Lemp;

    .line 315
    iput-object p5, p0, Ldnb;->mUrlHelper:Lcpn;

    .line 316
    iput-object p6, p0, Ldnb;->atG:Leqo;

    .line 317
    iput-object p10, p0, Ldnb;->mLocationSettings:Lcob;

    .line 318
    iput-object p7, p0, Ldnb;->bCT:Ldnj;

    .line 319
    iput-object p11, p0, Ldnb;->bAZ:Ligi;

    .line 321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldnb;->bCV:Ljava/util/List;

    .line 322
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Ldnb;->bCX:Ljava/util/Queue;

    .line 324
    new-instance v0, Ldng;

    invoke-direct {v0, p0}, Ldng;-><init>(Ldnb;)V

    iput-object v0, p0, Ldnb;->bCU:Ldng;

    .line 326
    invoke-direct {p0, v2}, Ldnb;->gd(I)V

    .line 327
    iput-object p8, p0, Ldnb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 328
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Ldnb;->O(Lcom/google/android/shared/search/Query;)V

    .line 329
    iput-object p9, p0, Ldnb;->mWebAppState:Lddk;

    .line 331
    const-string v0, "webview_geolocation"

    invoke-virtual {p12, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ldnb;->bCY:Ljava/io/File;

    .line 332
    return-void
.end method

.method static synthetic a(Ldnb;)Legl;
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ldnb;->adH()Legl;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/String;Lcom/google/android/shared/search/Query;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 1363
    iget-object v1, p0, Ldnb;->bCV:Ljava/util/List;

    monitor-enter v1

    .line 1364
    :try_start_0
    iget v0, p0, Ldnb;->bCW:I

    iget-object v2, p0, Ldnb;->bCV:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 1365
    iget-object v0, p0, Ldnb;->bCV:Ljava/util/List;

    new-instance v2, Ldnh;

    invoke-direct {v2, p0}, Ldnh;-><init>(Ldnb;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1367
    :cond_0
    iget-object v0, p0, Ldnb;->bCV:Ljava/util/List;

    iget v2, p0, Ldnb;->bCW:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnh;

    .line 1368
    iget v2, p0, Ldnb;->bCW:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldnb;->bCW:I

    .line 1369
    iput p1, v0, Ldnh;->bDu:I

    .line 1370
    iput-object p2, v0, Ldnh;->aZF:Ljava/lang/String;

    .line 1371
    iput-object p3, v0, Ldnh;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1372
    iput p4, v0, Ldnh;->bDv:I

    .line 1373
    iput-object p5, v0, Ldnh;->bDw:Ljava/lang/String;

    .line 1374
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1375
    iget-object v0, p0, Ldnb;->atG:Leqo;

    iget-object v1, p0, Ldnb;->bCQ:Lesk;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 1376
    iget-object v0, p0, Ldnb;->atG:Leqo;

    iget-object v1, p0, Ldnb;->bCQ:Lesk;

    const-wide/16 v2, 0xa

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 1377
    return-void

    .line 1374
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Ldnb;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Ldnb;->jU(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Ldnb;Z)Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldnb;->bCO:Z

    return v0
.end method

.method private aF(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 1279
    iget-object v0, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcpn;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1284
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v0, p1}, Ldnj;->aG(Lcom/google/android/shared/search/Query;)V

    .line 1288
    :cond_0
    return-void
.end method

.method private adH()Legl;
    .locals 6

    .prologue
    .line 565
    iget-object v1, p0, Ldnb;->bDm:Ljava/lang/Object;

    monitor-enter v1

    .line 566
    :try_start_0
    iget-object v0, p0, Ldnb;->bDn:Legl;

    if-nez v0, :cond_0

    .line 567
    new-instance v0, Legl;

    const-wide/16 v2, 0x0

    sget-object v4, Leoi;->cgG:Leoi;

    invoke-virtual {v4}, Leoi;->auU()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Legl;-><init>(JJ)V

    iput-object v0, p0, Ldnb;->bDn:Legl;

    .line 570
    :cond_0
    iget-object v0, p0, Ldnb;->bDn:Legl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private adJ()V
    .locals 4

    .prologue
    .line 665
    iget-object v0, p0, Ldnb;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Jc()I

    move-result v0

    .line 666
    iget-object v1, p0, Ldnb;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->uptimeMillis()J

    move-result-wide v2

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldnb;->bDa:J

    .line 667
    return-void
.end method

.method private adL()Z
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private adO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private adQ()V
    .locals 2

    .prologue
    .line 1272
    :goto_0
    iget v0, p0, Ldnb;->bCW:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 1273
    iget-object v0, p0, Ldnb;->bCV:Ljava/util/List;

    iget v1, p0, Ldnb;->bCW:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ldnb;->bCW:I

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 1275
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Ldnb;->bCW:I

    .line 1276
    return-void
.end method

.method private adS()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1440
    iget-object v0, p0, Ldnb;->bDr:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livy;

    .line 1441
    if-nez v0, :cond_0

    .line 1442
    const-string v0, "No loadUrl() call"

    .line 1457
    :goto_0
    return-object v0

    .line 1444
    :cond_0
    invoke-virtual {v0}, Livy;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1445
    const-string v0, "Preempted"

    goto :goto_0

    .line 1447
    :cond_1
    invoke-virtual {v0}, Livy;->isDone()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1449
    :try_start_0
    invoke-virtual {v0}, Livy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1450
    const-string v0, "Loaded successfully"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1452
    :cond_2
    const-string v0, "Timeout exceeded"

    goto :goto_0

    .line 1454
    :catch_0
    move-exception v0

    const-string v0, "Unknown"

    goto :goto_0

    .line 1457
    :cond_3
    const-string v0, "Timeout not exceeded"

    goto :goto_0
.end method

.method static synthetic b(Ldnb;)Ljava/util/List;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldnb;->bCN:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Ldnb;)Ldyo;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldnb;->bDp:Ldyo;

    return-object v0
.end method

.method private c(Lcom/google/android/shared/search/Query;I)V
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Ldnb;->bCZ:I

    if-eq p2, v0, :cond_1

    .line 641
    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    .line 642
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v0, p1}, Ldnj;->aH(Lcom/google/android/shared/search/Query;)V

    .line 647
    :cond_0
    :goto_0
    invoke-direct {p0, p2}, Ldnb;->gd(I)V

    .line 648
    invoke-virtual {p0}, Ldnb;->adK()V

    .line 650
    :cond_1
    return-void

    .line 643
    :cond_2
    if-nez p2, :cond_0

    .line 644
    invoke-direct {p0}, Ldnb;->adJ()V

    .line 645
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v0, p1}, Ldnj;->aI(Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method static synthetic d(Ldnb;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ldnb;->bDr:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method private e(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    .locals 6

    .prologue
    .line 1339
    const/4 v1, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Ldnb;->a(ILjava/lang/String;Lcom/google/android/shared/search/Query;ILjava/lang/String;)V

    .line 1340
    return-void
.end method

.method static synthetic e(Ldnb;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Ldnb;->bCO:Z

    return v0
.end method

.method private gd(I)V
    .locals 2

    .prologue
    .line 657
    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 658
    iget-object v0, p0, Ldnb;->bDf:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 661
    :cond_0
    iput p1, p0, Ldnb;->bCZ:I

    .line 662
    return-void
.end method

.method private jU(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Ldnb;->bDq:Ljava/lang/String;

    .line 594
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 596
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static jW(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 834
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic kb(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 93
    invoke-static {p0}, Ldnb;->jW(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private r(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1305
    invoke-virtual {p0, p1}, Ldnb;->jY(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1306
    const-string v0, "GsaWebView"

    const-string v1, "Ignoring load event for old query."

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1325
    :goto_0
    return-void

    .line 1310
    :cond_0
    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1311
    iget-object v1, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 1312
    if-eqz v1, :cond_1

    .line 1320
    invoke-direct {p0, v1}, Ldnb;->aF(Lcom/google/android/shared/search/Query;)V

    .line 1321
    invoke-direct {p0, v0, p2}, Ldnb;->c(Lcom/google/android/shared/search/Query;I)V

    goto :goto_0

    .line 1323
    :cond_1
    const-string v0, "GsaWebView"

    const-string v1, "handleLoadStartedOrFinished: query was null"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method final B(Landroid/net/Uri;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1164
    iget-object v0, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1165
    if-eqz v0, :cond_0

    .line 1170
    const-string v1, "GsaWebView"

    const-string v2, "URL change initiated from the page"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1176
    invoke-direct {p0, v6, v0}, Ldnb;->e(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 1182
    :goto_0
    return v5

    .line 1181
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Ldnb;->e(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final If()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 589
    iget-object v0, p0, Ldnb;->bDq:Ljava/lang/String;

    return-object v0
.end method

.method public final O(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 888
    invoke-static {}, Lenu;->auR()V

    .line 889
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 890
    :try_start_0
    iput-object p1, p0, Ldnb;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 891
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Xe()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 897
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 898
    :try_start_0
    iget-object v0, p0, Ldnb;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 899
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(ILcom/google/android/shared/search/Query;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1344
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p2

    move v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ldnb;->a(ILjava/lang/String;Lcom/google/android/shared/search/Query;ILjava/lang/String;)V

    .line 1345
    return-void
.end method

.method public final a(Landroid/webkit/WebView;Ldmv;Ldmz;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 341
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "WebView may only be set once."

    invoke-static {v0, v3}, Lifv;->d(ZLjava/lang/Object;)V

    .line 343
    iput-object p1, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    .line 346
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0074

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 347
    iput-object p2, p0, Ldnb;->mGsaCommunicationJsHelper:Ldmv;

    .line 348
    iput-object p3, p0, Ldnb;->mWebAppController:Ldmz;

    .line 349
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    iget-object v0, p0, Ldnb;->bAZ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    iget-object v0, p0, Ldnb;->bCY:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setGeolocationDatabasePath(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    :cond_0
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Ldnb;->bCU:Ldng;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Ldna;

    iget-object v3, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    iget-object v4, p0, Ldnb;->mLocationSettings:Lcob;

    invoke-direct {v2, v3, v4, p0}, Ldna;-><init>(Landroid/webkit/WebView;Lcob;Ldnb;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Ldnb;->bCS:Landroid/webkit/WebView$PictureListener;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_1

    iget-object v0, p0, Ldnb;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NF()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    .line 350
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 341
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 349
    goto :goto_1
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ldyo;Lcnb;)V
    .locals 10
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ldyo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcnb;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 433
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    invoke-direct {p0}, Ldnb;->adL()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 438
    invoke-virtual {p0, p1}, Ldnb;->O(Lcom/google/android/shared/search/Query;)V

    .line 440
    iget-object v1, p0, Ldnb;->bCV:Ljava/util/List;

    monitor-enter v1

    .line 441
    :try_start_0
    invoke-direct {p0}, Ldnb;->adQ()V

    .line 442
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 447
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    iput-boolean v8, p0, Ldnb;->bDc:Z

    .line 451
    :cond_0
    invoke-virtual {p0, p2}, Ldnb;->c(Ldyo;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 453
    invoke-virtual {p2}, Ldyo;->alJ()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->alI()Ljava/util/Map;

    move-result-object v2

    .line 454
    iput-object v2, p0, Ldnb;->bDe:Ljava/util/Map;

    .line 456
    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {p0, v1, p2, p3, v0}, Ldnb;->a(Ljava/lang/String;Ldyo;Lcnb;Lcom/google/android/shared/search/Query;)V

    .line 459
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Ldnb;->mWebAppController:Ldmz;

    invoke-virtual {v0, v1}, Ldmz;->jT(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 461
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apu()Lcom/google/android/shared/search/Query;

    .line 469
    :goto_0
    return-void

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 465
    :cond_1
    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldnb;->mWebAppState:Lddk;

    invoke-virtual {v0}, Lddk;->aaP()V

    invoke-virtual {p0}, Ldnb;->adM()V

    iput-boolean v8, p0, Ldnb;->bCO:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Loading URL, hash="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ldnb;->jW(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v3

    iget-object v0, p0, Ldnb;->bDr:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livy;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v8}, Livy;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x100f0c3

    invoke-static {v0}, Lhwt;->lx(I)V

    const-string v0, "GsaWebView"

    const-string v4, "loadUrl() haven\'t received a callback before a new call"

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v6, 0x5

    invoke-static {v6, v0, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Ldnb;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    new-instance v4, Ldnf;

    const-string v5, "loadUrl() timeout"

    invoke-direct {v4, p0, v5, v3, v0}, Ldnf;-><init>(Ldnb;Ljava/lang/String;Livy;Lcom/google/android/shared/search/Query;)V

    iget-object v0, p0, Ldnb;->atG:Leqo;

    iget-object v3, p0, Ldnb;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->Ko()I

    move-result v3

    int-to-long v6, v3

    invoke-interface {v0, v4, v6, v7}, Leqo;->a(Ljava/lang/Runnable;J)V

    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 466
    iget-object v0, p0, Ldnb;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Ldnb;->atG:Leqo;

    iget-object v3, p0, Ldnb;->bCR:Lesk;

    invoke-interface {v2, v3}, Leqo;->i(Ljava/lang/Runnable;)V

    iget-wide v2, p0, Ldnb;->bDa:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_4

    iput-boolean v8, p0, Ldnb;->bDb:Z

    iget-object v2, p0, Ldnb;->atG:Leqo;

    iget-object v3, p0, Ldnb;->bCR:Lesk;

    iget-wide v4, p0, Ldnb;->bDa:J

    sub-long v0, v4, v0

    invoke-interface {v2, v3, v0, v1}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 468
    :cond_3
    :goto_1
    invoke-direct {p0, v8}, Ldnb;->gd(I)V

    goto/16 :goto_0

    .line 466
    :cond_4
    iput-boolean v9, p0, Ldnb;->bDb:Z

    goto :goto_1
.end method

.method public final a(Letj;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 1487
    const-string v0, "GsaWebViewController state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1488
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    .line 1489
    const-string v0, "Last load"

    invoke-virtual {v2, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1490
    invoke-virtual {p0}, Ldnb;->adN()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Ldnb;->adO()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Ldnb;->bDe:Ljava/util/Map;

    const-string v4, "Requested URL"

    invoke-virtual {v2, v4}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v4

    invoke-static {v0}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v10}, Letn;->d(Ljava/lang/CharSequence;Z)V

    if-eqz v1, :cond_0

    const-string v0, "Loaded URL"

    invoke-virtual {v2, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-static {v1}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Letn;->d(Ljava/lang/CharSequence;Z)V

    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v2}, Letj;->avJ()Letj;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Headers (without auth or cookies): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Letj;->lt(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v6

    invoke-static {v0, v1}, Lcpn;->Q(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0, v7}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_1
    const-string v0, "Headers: null"

    invoke-virtual {v2, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 1491
    :cond_2
    const-string v0, "SuppressPreviousResults"

    invoke-virtual {v2, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Ldnb;->bDb:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Ldnb;->bDa:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 1494
    const-string v0, "mLoadState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    iget v0, p0, Ldnb;->bCZ:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_1
    invoke-virtual {v1, v0, v7}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1495
    const-string v0, "mHasPendingHistoryClear"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldnb;->bDc:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 1496
    const-string v0, "mCommittedQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 1497
    const-string v0, "mUserAgent"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    iget-object v0, p0, Ldnb;->bAZ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0, v7}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1501
    const-string v0, "Last URL"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldnb;->bCM:Ljava/lang/String;

    invoke-virtual {v0, v1, v10}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1503
    iget-object v1, p0, Ldnb;->bCN:Ljava/util/List;

    monitor-enter v1

    .line 1504
    :try_start_0
    const-string v0, ", "

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    const-string v3, "(null)"

    invoke-virtual {v0, v3}, Lifj;->pq(Ljava/lang/String;)Lifj;

    move-result-object v0

    iget-object v3, p0, Ldnb;->bCN:Ljava/util/List;

    invoke-virtual {v0, v3}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 1505
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1506
    const-string v1, "URLs loaded by Webview"

    invoke-virtual {p1, v1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    invoke-virtual {v1, v0, v10}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1508
    const-string v0, "loadUrl() state"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-direct {p0}, Ldnb;->adS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1511
    const-string v0, "mDbgHandledInternalEvents"

    invoke-virtual {v2, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1512
    invoke-static {}, Lesp;->avE()Ljava/text/DateFormat;

    move-result-object v1

    .line 1513
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Ldnb;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 1514
    iget-object v0, p0, Ldnb;->bCX:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnh;

    .line 1515
    new-instance v6, Ljava/util/Date;

    iget-wide v8, v0, Ldnh;->bDx:J

    add-long/2addr v8, v4

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0, v10}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_2

    .line 1494
    :pswitch_0
    const-string v0, "Idle"

    goto/16 :goto_1

    :pswitch_1
    const-string v0, "Loading"

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "Started"

    goto/16 :goto_1

    :pswitch_3
    const-string v0, "ReadyToShow"

    goto/16 :goto_1

    .line 1505
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1518
    :cond_3
    return-void

    .line 1494
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/Exception;)V
    .locals 4
    .param p3    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148
    const/16 v0, 0x1bc

    .line 1149
    if-nez p3, :cond_0

    .line 1150
    const-string v1, "GsaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not load page from cache: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1158
    :goto_0
    const-string v1, "No response"

    invoke-virtual {p0, v0, p2, v1}, Ldnb;->a(ILcom/google/android/shared/search/Query;Ljava/lang/String;)V

    .line 1159
    return-void

    .line 1152
    :cond_0
    instance-of v1, p3, Left;

    if-eqz v1, :cond_1

    move-object v0, p3

    .line 1153
    check-cast v0, Left;

    invoke-virtual {v0}, Left;->getErrorCode()I

    move-result v0

    .line 1155
    :cond_1
    const-string v1, "GsaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading page: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p3}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ldyo;Lcnb;Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 813
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 816
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 817
    :try_start_0
    iput-object p1, p0, Ldnb;->bDh:Ljava/lang/String;

    .line 818
    iget-object v0, p0, Ldnb;->bDh:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnb;->bDi:Ljava/lang/String;

    .line 820
    iput-object p1, p0, Ldnb;->bCM:Ljava/lang/String;

    .line 821
    iget-object v2, p0, Ldnb;->bCN:Ljava/util/List;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 823
    :try_start_1
    iget-object v0, p0, Ldnb;->bCN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 824
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 826
    :try_start_2
    iput-object p2, p0, Ldnb;->bDj:Ldyo;

    .line 827
    iput-object p3, p0, Ldnb;->bDk:Lcnb;

    .line 828
    iput-object p4, p0, Ldnb;->bDl:Lcom/google/android/shared/search/Query;

    .line 829
    monitor-exit v1

    return-void

    .line 813
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 824
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 829
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcki;)Z
    .locals 2

    .prologue
    .line 551
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcki;->OO()Ldyo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 552
    iget-object v1, p0, Ldnb;->bDo:Ljava/lang/Object;

    monitor-enter v1

    .line 553
    :try_start_0
    invoke-virtual {p1}, Lcki;->OO()Ldyo;

    move-result-object v0

    iput-object v0, p0, Ldnb;->bDp:Ldyo;

    .line 554
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555
    invoke-virtual {p0}, Ldnb;->adG()Z

    move-result v0

    .line 557
    :goto_0
    return v0

    .line 554
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 557
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final adC()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 363
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 364
    :try_start_0
    iget-object v0, p0, Ldnb;->bDl:Lcom/google/android/shared/search/Query;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 365
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final adD()Ljava/lang/String;
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Ldnb;->adN()Ljava/lang/String;

    move-result-object v0

    .line 374
    iget-object v1, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->hJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final adE()V
    .locals 2

    .prologue
    .line 425
    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldnb;->c(Lcom/google/android/shared/search/Query;I)V

    .line 426
    return-void
.end method

.method public final adF()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-interface {v0, v1}, Ldnj;->aJ(Lcom/google/android/shared/search/Query;)V

    .line 524
    return-void
.end method

.method public final adG()Z
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Ldnb;->bDq:Ljava/lang/String;

    .line 534
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 536
    const/4 v0, 0x0

    .line 542
    :goto_0
    return v0

    .line 538
    :cond_0
    iget-object v1, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 539
    const/16 v0, 0x146

    invoke-direct {p0}, Ldnb;->adH()Legl;

    move-result-object v1

    invoke-static {v0, v1}, Lege;->a(ILegl;)V

    .line 542
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final adI()V
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Ldnb;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->If()Ljava/lang/String;

    move-result-object v0

    .line 580
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 581
    iget-object v1, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->hM(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnb;->bDq:Ljava/lang/String;

    .line 586
    :goto_0
    return-void

    .line 583
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldnb;->bDq:Ljava/lang/String;

    goto :goto_0
.end method

.method final adK()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 689
    iget v0, p0, Ldnb;->bCZ:I

    if-eqz v0, :cond_1

    move v0, v1

    .line 690
    :goto_0
    invoke-direct {p0}, Ldnb;->adL()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 691
    invoke-direct {p0}, Ldnb;->adJ()V

    .line 694
    :cond_0
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    iget-boolean v3, p0, Ldnb;->bDb:Z

    if-nez v3, :cond_2

    :goto_1
    invoke-interface {v0, v1}, Ldnj;->dG(Z)V

    .line 699
    return-void

    :cond_1
    move v0, v2

    .line 689
    goto :goto_0

    :cond_2
    move v1, v2

    .line 694
    goto :goto_1
.end method

.method public final adM()V
    .locals 2

    .prologue
    .line 736
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 741
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 743
    :cond_0
    return-void
.end method

.method public final adN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 839
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 840
    :try_start_0
    iget-object v0, p0, Ldnb;->bDh:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 841
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final adP()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x3

    const/4 v9, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1190
    move v2, v3

    move v4, v3

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    .line 1200
    :goto_0
    iget v0, p0, Ldnb;->bCW:I

    if-ge v2, v0, :cond_f

    .line 1201
    iget-object v0, p0, Ldnb;->bCV:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnh;

    .line 1204
    iget-object v10, p0, Ldnb;->mClock:Lemp;

    invoke-interface {v10}, Lemp;->elapsedRealtime()J

    move-result-wide v10

    iput-wide v10, v0, Ldnh;->bDx:J

    .line 1205
    :goto_1
    iget-object v10, p0, Ldnb;->bCX:Ljava/util/Queue;

    invoke-interface {v10, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1206
    iget-object v10, p0, Ldnb;->bCX:Ljava/util/Queue;

    invoke-interface {v10}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_1

    .line 1209
    :cond_0
    iget v10, v0, Ldnh;->bDu:I

    if-ne v10, v12, :cond_1

    move v4, v9

    .line 1212
    :cond_1
    iget v10, v0, Ldnh;->bDu:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_a

    .line 1213
    if-nez v6, :cond_2

    move-object v6, v0

    .line 1230
    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    .line 1232
    iget-object v0, p0, Ldnb;->mGsaCommunicationJsHelper:Ldmv;

    invoke-virtual {v0}, Ldmv;->adB()V

    .line 1234
    :cond_3
    if-eqz v8, :cond_4

    .line 1235
    iget-object v0, v8, Ldnh;->aZF:Ljava/lang/String;

    invoke-direct {p0, v0, v13}, Ldnb;->r(Ljava/lang/String;I)V

    .line 1237
    :cond_4
    if-eqz v7, :cond_7

    .line 1238
    iget-object v0, v7, Ldnh;->aZF:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1239
    iget-object v10, p0, Ldnb;->mUrlHelper:Lcpn;

    iget-object v11, p0, Ldnb;->bDd:Landroid/net/Uri;

    invoke-virtual {v10, v11, v0}, Lcpn;->d(Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 1240
    iput-boolean v9, p0, Ldnb;->bDc:Z

    .line 1242
    :cond_5
    iput-object v0, p0, Ldnb;->bDd:Landroid/net/Uri;

    .line 1243
    iget-boolean v0, p0, Ldnb;->bDc:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Ldnb;->adL()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    iput-boolean v3, p0, Ldnb;->bDc:Z

    .line 1244
    :cond_6
    iget-object v0, v7, Ldnh;->aZF:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Ldnb;->r(Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Ldnb;->jU(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldnb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xl()Z

    iget-object v0, p0, Ldnb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xk()V

    .line 1245
    :cond_7
    if-eqz v6, :cond_8

    .line 1250
    iget-object v0, v6, Ldnh;->aZF:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1255
    iget-object v0, v6, Ldnh;->aZF:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1256
    invoke-virtual {p0}, Ldnb;->adN()Ljava/lang/String;

    move-result-object v0

    .line 1257
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_d

    move-object v0, v1

    .line 1258
    :goto_3
    iget-object v11, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v11, v10, v0}, Ldnj;->g(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1263
    :cond_8
    :goto_4
    if-eqz v5, :cond_9

    iget v0, p0, Ldnb;->bCZ:I

    if-ne v0, v13, :cond_9

    .line 1264
    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {p0, v0, v12}, Ldnb;->c(Lcom/google/android/shared/search/Query;I)V

    .line 1200
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 1216
    :cond_a
    iget v10, v0, Ldnh;->bDu:I

    if-eqz v10, :cond_f

    .line 1217
    iget v10, v0, Ldnh;->bDu:I

    if-ne v10, v9, :cond_b

    move-object v7, v1

    move-object v8, v0

    .line 1223
    goto/16 :goto_2

    .line 1224
    :cond_b
    iget v10, v0, Ldnh;->bDu:I

    if-ne v10, v12, :cond_c

    move-object v7, v0

    .line 1226
    goto/16 :goto_2

    .line 1227
    :cond_c
    iget v10, v0, Ldnh;->bDu:I

    const/4 v11, 0x5

    if-ne v10, v11, :cond_2

    move-object v5, v0

    .line 1228
    goto/16 :goto_2

    .line 1257
    :cond_d
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_3

    .line 1260
    :cond_e
    iget-object v0, v6, Ldnh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v0}, Ldnb;->aF(Lcom/google/android/shared/search/Query;)V

    goto :goto_4

    .line 1267
    :cond_f
    invoke-direct {p0}, Ldnb;->adQ()V

    .line 1268
    return-void
.end method

.method final adR()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1353
    iget-object v0, p0, Ldnb;->bDf:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1354
    iget-object v0, p0, Ldnb;->bDf:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1355
    const/4 v1, 0x5

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Ldnb;->a(ILjava/lang/String;Lcom/google/android/shared/search/Query;ILjava/lang/String;)V

    .line 1357
    :cond_0
    return-void
.end method

.method public final adT()V
    .locals 1

    .prologue
    .line 1522
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldnb;->bDc:Z

    .line 1523
    return-void
.end method

.method public final adU()Ldmz;
    .locals 1

    .prologue
    .line 1527
    iget-object v0, p0, Ldnb;->mWebAppController:Ldmz;

    return-object v0
.end method

.method public final c(Ldyo;)Landroid/net/Uri;
    .locals 8
    .param p1    # Ldyo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 758
    invoke-virtual {p1}, Ldyo;->alJ()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v3

    .line 763
    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->ara()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->arb()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    const-string v1, "qsubts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    const-string v1, "q"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v5, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    :cond_3
    const-string v1, "GsaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Committed query has no submission time: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v1, v0, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_5

    const-string v0, "qsubts"

    invoke-virtual {v5, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_5
    const-string v0, "q"

    invoke-virtual {v5, v0, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 765
    return-object v0
.end method

.method public final cN()Z
    .locals 12

    .prologue
    const/4 v10, 0x5

    const/4 v0, 0x0

    .line 382
    invoke-virtual {p0}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 383
    iget-object v1, p0, Ldnb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v0

    .line 388
    :cond_1
    iget-object v1, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ldnb;->bDc:Z

    if-nez v1, :cond_0

    .line 389
    iget-object v1, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v4

    .line 390
    const/4 v2, 0x0

    .line 391
    invoke-virtual {v4}, Landroid/webkit/WebBackForwardList;->getCurrentIndex()I

    move-result v5

    .line 393
    add-int/lit8 v1, v5, -0x1

    move v11, v1

    move-object v1, v2

    move v2, v11

    .line 394
    :goto_1
    if-ltz v2, :cond_2

    .line 395
    invoke-virtual {v4, v2}, Landroid/webkit/WebBackForwardList;->getItemAtIndex(I)Landroid/webkit/WebHistoryItem;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebHistoryItem;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 396
    iget-object v1, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v3, v6}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 397
    if-nez v1, :cond_2

    .line 398
    const-string v7, "GsaWebView"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Went back to non-search URL:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v8, v0, [Ljava/lang/Object;

    invoke-static {v10, v7, v6, v8}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 394
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 402
    :cond_2
    if-eqz v1, :cond_0

    .line 406
    iget-object v4, p0, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {v4, v1, v3}, Lcpn;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 407
    const-string v1, "GsaWebView"

    const-string v2, "WebView back wants to change the query"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v10, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 411
    :cond_3
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v0, v3}, Ldnj;->aH(Lcom/google/android/shared/search/Query;)V

    .line 412
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    sub-int v1, v2, v5

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->goBackOrForward(I)V

    .line 413
    iget-object v0, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v0, v3}, Ldnj;->aI(Lcom/google/android/shared/search/Query;)V

    .line 414
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized ca()V
    .locals 2

    .prologue
    .line 354
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ldnb;->adQ()V

    .line 355
    iget-object v0, p0, Ldnb;->atG:Leqo;

    iget-object v1, p0, Ldnb;->bCQ:Lesk;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 356
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 358
    const/4 v0, 0x0

    iput-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    :cond_0
    monitor-exit p0

    return-void

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final jV(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 711
    invoke-direct {p0}, Ldnb;->adL()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 713
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 720
    :goto_0
    return-void

    .line 715
    :cond_0
    iget-object v0, p0, Ldnb;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 718
    :cond_1
    const-string v0, "GsaWebView"

    const-string v1, "evaluateJavascript called before WebView is created."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final jX(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 856
    invoke-static {}, Lenu;->auR()V

    .line 857
    invoke-direct {p0}, Ldnb;->adO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldnb;->jY(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 860
    :try_start_0
    iget-object v0, p0, Ldnb;->bDk:Lcnb;

    iget-boolean v0, v0, Lcnb;->bcM:Z

    .line 861
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 862
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 865
    iget-object v1, p0, Ldnb;->bCT:Ldnj;

    invoke-interface {v1, p1, v0}, Ldnj;->n(Ljava/lang/String;Z)V

    .line 870
    :cond_0
    :goto_0
    return-void

    .line 861
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 867
    :cond_1
    const-string v0, "GsaWebView"

    const-string v1, "Could not get event id from SRP"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final jY(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 873
    iget-object v1, p0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v1

    .line 877
    :try_start_0
    iget-object v0, p0, Ldnb;->bDi:Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 878
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final jZ(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1329
    const/4 v1, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Ldnb;->a(ILjava/lang/String;Lcom/google/android/shared/search/Query;ILjava/lang/String;)V

    .line 1330
    return-void
.end method

.method final ka(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1334
    const/4 v1, 0x3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Ldnb;->a(ILjava/lang/String;Lcom/google/android/shared/search/Query;ILjava/lang/String;)V

    .line 1335
    return-void
.end method
