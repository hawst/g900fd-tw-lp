.class public final Leay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 88
    iget-object v1, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-object v1, v1, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bST:Landroid/graphics/Paint;

    const/high16 v2, 0x43190000    # 153.0f

    mul-float/2addr v2, v0

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 90
    iget-object v1, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget-object v2, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget v2, v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSV:I

    iget-object v3, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget v3, v3, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSW:I

    iget-object v4, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iget v4, v4, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSV:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    add-int/2addr v0, v2

    iput v0, v1, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSU:I

    .line 93
    iget-object v0, p0, Leay;->bTb:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-virtual {v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->invalidate()V

    .line 94
    return-void
.end method
