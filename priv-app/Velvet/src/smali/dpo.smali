.class final Ldpo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldnj;


# instance fields
.field final synthetic bET:Ldpk;


# direct methods
.method constructor <init>(Ldpk;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Ldpo;->bET:Ldpk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final aG(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->ab(Lcom/google/android/shared/search/Query;)Z

    .line 373
    return-void
.end method

.method public final aH(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->Z(Lcom/google/android/shared/search/Query;)V

    .line 379
    return-void
.end method

.method public final aI(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->aa(Lcom/google/android/shared/search/Query;)V

    .line 385
    return-void
.end method

.method public final aJ(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 466
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mUiThread:Leqo;

    new-instance v1, Ldpp;

    const-string v2, "onCloseWindow"

    invoke-direct {v1, p0, v2, p1}, Ldpp;-><init>(Ldpo;Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 472
    return-void
.end method

.method public final adV()V
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->MZ()V

    .line 462
    return-void
.end method

.method public final dG(Z)V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->dl(Z)V

    .line 366
    return-void
.end method

.method public final g(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 8
    .param p2    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    .line 391
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->bEM:Lceu;

    iget-object v1, v0, Lceu;->mUiThread:Leqo;

    invoke-interface {v1}, Leqo;->Du()Z

    move-result v1

    invoke-static {v1}, Lifv;->gY(Z)V

    const/4 v1, 0x0

    iput-object v1, v0, Lceu;->aTL:Ligi;

    .line 392
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->r(Landroid/net/Uri;)Lcpo;

    move-result-object v5

    .line 394
    if-eqz v5, :cond_0

    .line 399
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->MR()V

    .line 400
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v1, v0, Ldpk;->bEM:Lceu;

    iget-object v0, v1, Lceu;->mUiThread:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    iput-object v5, v1, Lceu;->aTL:Ligi;

    iget-object v6, v1, Lceu;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcev;

    const-string v2, "ad-click"

    new-array v3, v7, [I

    fill-array-data v3, :array_0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcev;-><init>(Lceu;Ljava/lang/String;[ILandroid/net/Uri;Ligi;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 401
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Ym()V

    .line 418
    :goto_0
    return-void

    .line 403
    :cond_0
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p1, p2}, Lcpn;->b(Landroid/net/Uri;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v1

    .line 406
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 407
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DQ()Lcpd;

    move-result-object v2

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ligi;

    iget-object v3, v2, Lcpd;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcpe;

    const-string v5, "logResultClick"

    new-array v6, v7, [I

    fill-array-data v6, :array_1

    invoke-direct {v4, v2, v5, v6, v0}, Lcpe;-><init>(Lcpd;Ljava/lang/String;[ILigi;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 414
    :cond_1
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nk()V

    .line 416
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v2, v0, Ldpk;->mSearchController:Lcjt;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v2, v0}, Lcjt;->a(Lcom/google/android/search/shared/api/UriRequest;)V

    goto :goto_0

    .line 400
    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data

    .line 407
    :array_1
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final n(Ljava/lang/String;Z)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 431
    invoke-static {}, Lenu;->auR()V

    .line 434
    if-eqz p2, :cond_0

    .line 439
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v5

    .line 440
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0, v5}, Lcjt;->b(Lcom/google/android/shared/search/Query;)V

    .line 441
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DQ()Lcpd;

    move-result-object v1

    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v4, v0, Ldpk;->mUrlHelper:Lcpn;

    iget-object v7, v1, Lcpd;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcpg;

    const-string v2, "sendGen204"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcpg;-><init>(Lcpd;Ljava/lang/String;[ILcpn;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 445
    :cond_0
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 446
    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    .line 448
    iget-object v1, p0, Ldpo;->bET:Ldpk;

    iget-object v1, v1, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aat()Ldcf;

    move-result-object v1

    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    invoke-virtual {v1, v3}, Ldcf;->f(Lcom/google/android/velvet/ActionData;)I

    move-result v1

    .line 451
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v6

    .line 452
    :goto_0
    iget-object v2, p0, Ldpo;->bET:Ldpk;

    iget-object v2, v2, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DQ()Lcpd;

    move-result-object v2

    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v3, Lcpk;

    invoke-direct {v3, v2}, Lcpk;-><init>(Lcpd;)V

    invoke-interface {v0, v3}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 454
    :goto_1
    iget-object v0, p0, Ldpo;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DQ()Lcpd;

    move-result-object v0

    iget-object v2, p0, Ldpo;->bET:Ldpk;

    iget-object v5, v2, Ldpk;->mUrlHelper:Lcpn;

    move-object v2, v9

    move-object v3, p1

    move-object v4, v9

    invoke-virtual/range {v0 .. v8}, Lcpd;->a(ILjyx;Ljava/lang/String;Ljava/lang/String;Lcpn;JI)V

    .line 457
    return-void

    .line 451
    :cond_1
    const-wide/16 v6, -0x1

    goto :goto_0

    .line 452
    :cond_2
    const/4 v8, -0x1

    goto :goto_1

    .line 441
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method
