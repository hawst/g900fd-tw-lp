.class public final Ljjt;
.super Ljsl;
.source "PG"


# instance fields
.field public epq:[Ljava/lang/String;

.field public epr:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 427
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 428
    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjt;->eCz:I

    .line 429
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 404
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljjt;->epq:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljjt;->epq:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljjt;->epr:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljjt;->epr:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 442
    iget-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 443
    :goto_0
    iget-object v2, p0, Ljjt;->epq:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 444
    iget-object v2, p0, Ljjt;->epq:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 445
    if-eqz v2, :cond_0

    .line 446
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 443
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 450
    :cond_1
    iget-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 451
    :goto_1
    iget-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 452
    iget-object v0, p0, Ljjt;->epr:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 453
    if-eqz v0, :cond_2

    .line 454
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 451
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 458
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 459
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 463
    invoke-super {p0}, Ljsl;->lF()I

    move-result v4

    .line 464
    iget-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljjt;->epq:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    move v2, v1

    move v3, v1

    .line 467
    :goto_0
    iget-object v5, p0, Ljjt;->epq:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 468
    iget-object v5, p0, Ljjt;->epq:[Ljava/lang/String;

    aget-object v5, v5, v0

    .line 469
    if-eqz v5, :cond_0

    .line 470
    add-int/lit8 v3, v3, 0x1

    .line 471
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 467
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 475
    :cond_1
    add-int v0, v4, v2

    .line 476
    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 478
    :goto_1
    iget-object v2, p0, Ljjt;->epr:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljjt;->epr:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 481
    :goto_2
    iget-object v4, p0, Ljjt;->epr:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 482
    iget-object v4, p0, Ljjt;->epr:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 483
    if-eqz v4, :cond_2

    .line 484
    add-int/lit8 v3, v3, 0x1

    .line 485
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 481
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 489
    :cond_3
    add-int/2addr v0, v2

    .line 490
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 492
    :cond_4
    return v0

    :cond_5
    move v0, v4

    goto :goto_1
.end method
