.class public final Lgbd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;


# direct methods
.method public constructor <init>(Lgan;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lgbd;->mFifeImageUrlUtil:Lgan;

    .line 38
    return-void
.end method

.method public static a(ILjcn;Laoi;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x4c

    .line 66
    .line 67
    invoke-virtual {p1}, Ljcn;->bgL()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 68
    new-instance v1, Lfsa;

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    invoke-virtual {p1}, Ljcn;->bgK()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 72
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 73
    iput-object v0, p2, Laoi;->aju:Lani;

    .line 75
    :cond_1
    return-void

    .line 69
    :cond_2
    iget-object v1, p1, Ljcn;->ahD:Lixx;

    if-eqz v1, :cond_0

    .line 70
    new-instance v0, Lfsa;

    invoke-direct {v0, v2}, Lfsa;-><init>(I)V

    iget-object v1, p1, Ljcn;->ahD:Lixx;

    invoke-virtual {v0, v1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljcn;Laoi;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Ljcn;->eaQ:Lixa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljcn;->eaQ:Lixa;

    invoke-virtual {v0}, Lixa;->qG()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    new-instance v0, Lfsa;

    const/16 v1, 0x79

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    iget-object v1, p0, Ljcn;->eaQ:Lixa;

    invoke-virtual {v1}, Lixa;->getUrl()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 88
    iput-object v0, p1, Laoi;->ajv:Lani;

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/content/Context;Ljcn;II)Laoi;
    .locals 2

    .prologue
    .line 46
    new-instance v1, Laoi;

    invoke-direct {v1}, Laoi;-><init>()V

    .line 47
    iget-object v0, p2, Ljcn;->eaQ:Lixa;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p2, Ljcn;->eaQ:Lixa;

    invoke-virtual {v0}, Lixa;->pb()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Ljcn;->eaQ:Lixa;

    invoke-virtual {v0}, Lixa;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 50
    :goto_0
    invoke-virtual {v1, v0}, Laoi;->bR(Ljava/lang/String;)Laoi;

    .line 53
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_1

    .line 55
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laoi;->bQ(Ljava/lang/String;)Laoi;

    .line 58
    :cond_1
    return-object v1

    .line 48
    :cond_2
    iget-object v0, p2, Ljcn;->eaQ:Lixa;

    invoke-virtual {v0}, Lixa;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 105
    invoke-static {p2}, Lgbm;->c(Ljcn;)Ljava/lang/String;

    move-result-object v0

    .line 107
    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 123
    :goto_0
    return-object v0

    .line 112
    :cond_0
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p2}, Ljcn;->bgJ()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 116
    iget-object v2, p0, Lgbd;->mFifeImageUrlUtil:Lgan;

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v2, v3, v1, v0}, Lgan;->c(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
