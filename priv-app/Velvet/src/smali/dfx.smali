.class public final Ldfx;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;J)Lcom/google/android/shared/search/Suggestion;
    .locals 2

    .prologue
    .line 98
    .line 99
    const/4 v0, 0x0

    .line 100
    invoke-static {p1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    :goto_0
    invoke-static {p2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    :goto_1
    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    iput-object p1, v0, Lded;->bux:Ljava/lang/CharSequence;

    iput-object p2, v0, Lded;->buy:Ljava/lang/CharSequence;

    const-string v1, "android.intent.action.WEB_SEARCH"

    iput-object v1, v0, Lded;->buE:Ljava/lang/String;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buJ:Ljava/lang/String;

    iput-object p3, v0, Lded;->buK:Ljava/lang/String;

    iput-object p4, v0, Lded;->buA:Ljava/lang/String;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Lded;->fI(I)Lded;

    move-result-object v0

    iput-object p5, v0, Lded;->buM:Ljava/util/ArrayList;

    iput-wide p6, v0, Lded;->bjh:J

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    return-object v0

    :cond_0
    move-object p2, v0

    goto :goto_1

    :cond_1
    move-object p1, p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/CharSequence;ZLjava/util/ArrayList;J)Lcom/google/android/shared/search/Suggestion;
    .locals 3

    .prologue
    .line 29
    if-eqz p1, :cond_0

    const/16 v0, 0x19

    .line 32
    :goto_0
    new-instance v1, Lded;

    invoke-direct {v1}, Lded;-><init>()V

    iput-object p0, v1, Lded;->bux:Ljava/lang/CharSequence;

    const-string v2, "android.intent.action.WEB_SEARCH"

    iput-object v2, v1, Lded;->buE:Ljava/lang/String;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lded;->buJ:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lded;->fI(I)Lded;

    move-result-object v0

    iput-object p2, v0, Lded;->buM:Ljava/util/ArrayList;

    iput-boolean p1, v0, Lded;->buN:Z

    iput-wide p3, v0, Lded;->bjh:J

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    return-object v0

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/net/Uri;ZLjava/util/ArrayList;J)Lcom/google/android/shared/search/Suggestion;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    iput-object p1, v0, Lded;->bux:Ljava/lang/CharSequence;

    iput-object p0, v0, Lded;->buz:Ljava/lang/String;

    const-string v1, "android.intent.action.VIEW"

    iput-object v1, v0, Lded;->buE:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buF:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buJ:Ljava/lang/String;

    iput-boolean p3, v0, Lded;->buN:Z

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lded;->fI(I)Lded;

    move-result-object v0

    iput-object p4, v0, Lded;->buM:Ljava/util/ArrayList;

    iput-wide p5, v0, Lded;->bjh:J

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public static o(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Suggestion;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    iput-object p0, v0, Lded;->bux:Ljava/lang/CharSequence;

    const-string v1, "android.intent.action.WEB_SEARCH"

    iput-object v1, v0, Lded;->buE:Ljava/lang/String;

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buJ:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Lded;->fI(I)Lded;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lded;->buN:Z

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    return-object v0
.end method
