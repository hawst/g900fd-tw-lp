.class public final Lant;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiA:Ljava/lang/String;

.field private aiB:Ljava/lang/String;

.field public aiz:Laos;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3108
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3109
    const/4 v0, 0x0

    iput v0, p0, Lant;->aez:I

    iput-object v1, p0, Lant;->aiz:Laos;

    const-string v0, ""

    iput-object v0, p0, Lant;->aiA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lant;->aiB:Ljava/lang/String;

    iput-object v1, p0, Lant;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lant;->eCz:I

    .line 3110
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3042
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lant;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lant;->aiz:Laos;

    if-nez v0, :cond_1

    new-instance v0, Laos;

    invoke-direct {v0}, Laos;-><init>()V

    iput-object v0, p0, Lant;->aiz:Laos;

    :cond_1
    iget-object v0, p0, Lant;->aiz:Laos;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lant;->aiA:Ljava/lang/String;

    iget v0, p0, Lant;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lant;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lant;->aiB:Ljava/lang/String;

    iget v0, p0, Lant;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lant;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3125
    iget-object v0, p0, Lant;->aiz:Laos;

    if-eqz v0, :cond_0

    .line 3126
    const/4 v0, 0x1

    iget-object v1, p0, Lant;->aiz:Laos;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3128
    :cond_0
    iget v0, p0, Lant;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 3129
    const/4 v0, 0x2

    iget-object v1, p0, Lant;->aiA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3131
    :cond_1
    iget v0, p0, Lant;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 3132
    const/4 v0, 0x3

    iget-object v1, p0, Lant;->aiB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3134
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3135
    return-void
.end method

.method public final aV(Ljava/lang/String;)Lant;
    .locals 1

    .prologue
    .line 3070
    if-nez p1, :cond_0

    .line 3071
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3073
    :cond_0
    iput-object p1, p0, Lant;->aiA:Ljava/lang/String;

    .line 3074
    iget v0, p0, Lant;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lant;->aez:I

    .line 3075
    return-object p0
.end method

.method public final aW(Ljava/lang/String;)Lant;
    .locals 1

    .prologue
    .line 3092
    if-nez p1, :cond_0

    .line 3093
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3095
    :cond_0
    iput-object p1, p0, Lant;->aiB:Ljava/lang/String;

    .line 3096
    iget v0, p0, Lant;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lant;->aez:I

    .line 3097
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3139
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3140
    iget-object v1, p0, Lant;->aiz:Laos;

    if-eqz v1, :cond_0

    .line 3141
    const/4 v1, 0x1

    iget-object v2, p0, Lant;->aiz:Laos;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3144
    :cond_0
    iget v1, p0, Lant;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 3145
    const/4 v1, 0x2

    iget-object v2, p0, Lant;->aiA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3148
    :cond_1
    iget v1, p0, Lant;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 3149
    const/4 v1, 0x3

    iget-object v2, p0, Lant;->aiB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3152
    :cond_2
    return v0
.end method

.method public final pO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3067
    iget-object v0, p0, Lant;->aiA:Ljava/lang/String;

    return-object v0
.end method

.method public final pP()Z
    .locals 1

    .prologue
    .line 3078
    iget v0, p0, Lant;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3089
    iget-object v0, p0, Lant;->aiB:Ljava/lang/String;

    return-object v0
.end method

.method public final pR()Z
    .locals 1

    .prologue
    .line 3100
    iget v0, p0, Lant;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
