.class public final Ldee;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bvh:Leeh;

.field private final mAsyncServices:Lema;

.field public final mContext:Landroid/content/Context;

.field private final mGlobalSearchServices:Lcgh;

.field private final mGsaConfigFlags:Lchk;

.field private final mIntentStarter:Leqp;

.field public final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field public final mServiceState:Ldcu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Lcgh;Lema;Leqp;Lcom/google/android/search/core/state/QueryState;Ldcu;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Ldee;->mContext:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Ldee;->mGsaConfigFlags:Lchk;

    .line 53
    iput-object p4, p0, Ldee;->mAsyncServices:Lema;

    .line 54
    new-instance v0, Leeh;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Leeh;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldee;->bvh:Leeh;

    .line 55
    iput-object p3, p0, Ldee;->mGlobalSearchServices:Lcgh;

    .line 56
    iput-object p5, p0, Ldee;->mIntentStarter:Leqp;

    .line 57
    iput-object p6, p0, Ldee;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 58
    iput-object p7, p0, Ldee;->mServiceState:Ldcu;

    .line 59
    return-void
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;ZLcom/google/android/shared/search/SearchBoxStats;)Litu;
    .locals 3

    .prologue
    .line 189
    new-instance v1, Liub;

    invoke-direct {v1}, Liub;-><init>()V

    .line 190
    invoke-static {p1, p2}, Lcpd;->O(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Liub;->mQ(I)Liub;

    .line 192
    invoke-virtual {v1, p1}, Liub;->pT(Ljava/lang/String;)Liub;

    .line 193
    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Liub;->mR(I)Liub;

    .line 196
    invoke-static {p0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p4}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lege;->kQ(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Litu;->mH(I)Litu;

    move-result-object v0

    .line 198
    iput-object v1, v0, Litu;->dIE:Liub;

    .line 199
    const/16 v1, 0x69

    if-ne p0, v1, :cond_0

    .line 200
    invoke-static {p4}, Lcod;->a(Lcom/google/android/shared/search/SearchBoxStats;)Lith;

    move-result-object v1

    iput-object v1, v0, Litu;->dJd:Lith;

    .line 203
    :cond_0
    return-object v0

    .line 193
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;I)Lcom/google/android/shared/search/SearchBoxStats;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 81
    invoke-virtual {p2}, Lcom/google/android/shared/search/SearchBoxStats;->arU()Lehj;

    move-result-object v0

    iget-object v3, p0, Ldee;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhgn;->aV(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    iput v3, v0, Lehj;->bZC:I

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v3

    .line 86
    if-nez p1, :cond_0

    .line 88
    const-string v0, "QSB.SuggestionLauncher"

    const-string v1, "Null suggestion; ignoring click"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 126
    :goto_0
    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Ldee;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Gf()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asw()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldee;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Gg()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    .line 96
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asm()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asC()Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asD()Ljava/lang/String;

    move-result-object v4

    .line 99
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 100
    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentId;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arX()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asC()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asD()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v4, Lcom/google/android/gms/appdatasearch/UsageInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const/4 v5, 0x2

    invoke-direct {v4, v0, v6, v7, v5}, Lcom/google/android/gms/appdatasearch/UsageInfo;-><init>(Lcom/google/android/gms/appdatasearch/DocumentId;JI)V

    .line 104
    iget-object v0, p0, Ldee;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v5, Ldif;

    iget-object v6, p0, Ldee;->mContext:Landroid/content/Context;

    new-array v2, v2, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    aput-object v4, v2, v1

    invoke-direct {v5, v6, v2}, Ldif;-><init>(Landroid/content/Context;[Lcom/google/android/gms/appdatasearch/UsageInfo;)V

    invoke-interface {v0, v5}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 110
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 111
    const-string v0, "navsuggestion"

    const/4 v2, 0x0

    invoke-static {p3, v0, v2, v1, v3}, Ldee;->a(ILjava/lang/String;Ljava/lang/String;ZLcom/google/android/shared/search/SearchBoxStats;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 123
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 124
    iget-object v0, p0, Ldee;->mGlobalSearchServices:Lcgh;

    invoke-interface {v0}, Lcgh;->ES()Lcks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcks;->a(Lcom/google/android/shared/search/Suggestion;)V

    :cond_4
    move-object v0, v3

    .line 126
    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 93
    goto :goto_1

    .line 113
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arX()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asm()Z

    move-result v2

    invoke-static {p3, v0, v1, v2, v3}, Ldee;->a(ILjava/lang/String;Ljava/lang/String;ZLcom/google/android/shared/search/SearchBoxStats;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    goto :goto_2
.end method

.method public q(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    if-nez p1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 67
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldee;->mIntentStarter:Leqp;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-interface {v0, v1}, Leqp;->b([Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 71
    const-string v1, "QSB.SuggestionLauncher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to start "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
