.class public final Lfwu;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfty;


# instance fields
.field private final cCl:Lftx;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizq;Lftz;Lemp;)V

    .line 49
    new-instance v0, Lftx;

    invoke-direct {v0, p0}, Lftx;-><init>(Lfty;)V

    iput-object v0, p0, Lfwu;->cCl:Lftx;

    .line 50
    return-void
.end method

.method private static a(Landroid/content/Context;Ljau;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljau;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 274
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 276
    if-nez p1, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-object v0

    .line 279
    :cond_1
    iget-object v1, p1, Ljau;->dQX:Ljhe;

    if-eqz v1, :cond_0

    .line 282
    iget-object v0, p1, Ljau;->dQX:Ljhe;

    invoke-virtual {v0}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static g(Lizq;)[Ljcn;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 207
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 208
    iget-object v3, p0, Lizq;->dUX:[Lizj;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 209
    iget-object v6, v5, Lizj;->dTF:Ljhh;

    if-eqz v6, :cond_1

    iget-object v6, v5, Lizj;->dTF:Ljhh;

    iget-object v6, v6, Ljhh;->eeb:[Ljcn;

    array-length v6, v6

    if-lez v6, :cond_1

    .line 210
    iget-object v5, v5, Lizj;->dTF:Ljhh;

    iget-object v5, v5, Ljhh;->eeb:[Ljcn;

    aget-object v5, v5, v1

    .line 211
    invoke-virtual {v5}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 212
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    .line 215
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljcn;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljcn;

    goto :goto_1
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 3

    .prologue
    .line 54
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lfpm;->q(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpm;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lfwu;->cCl:Lftx;

    invoke-virtual {v1, p1}, Lftx;->aS(Landroid/content/Context;)Lang;

    move-result-object v1

    .line 58
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v2}, Lfwu;->a(Landroid/content/Context;Lfpm;Ljava/util/Locale;)Lanh;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    iget-object v2, v1, Lang;->ags:[Lanh;

    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-static {v2, v0}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 66
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/content/Context;Lfpm;Ljava/util/Locale;)Lanh;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizq;

    .line 235
    iget-object v2, v0, Lizq;->dUX:[Lizj;

    array-length v2, v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 258
    :goto_0
    return-object v0

    .line 238
    :cond_0
    iget-object v2, v0, Lizq;->dUX:[Lizj;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lizj;->getType()I

    move-result v2

    const/16 v3, 0x85

    if-ne v2, v3, :cond_1

    move-object v0, v1

    .line 239
    goto :goto_0

    .line 242
    :cond_1
    invoke-virtual {p2, p3}, Lfpm;->f(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 243
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 244
    goto :goto_0

    .line 247
    :cond_2
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    const v4, 0x7f0a044d

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Laoj;->bS(Ljava/lang/String;)Laoj;

    move-result-object v4

    .line 250
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 251
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Lanh;->cm(I)Lanh;

    .line 252
    iget-object v0, v0, Lizq;->dUZ:Lizj;

    iput-object v0, v2, Lanh;->ahu:Lizj;

    .line 253
    iput-object v4, v2, Lanh;->agz:Laoj;

    .line 254
    new-instance v0, Lfsa;

    const/16 v4, 0x6a

    invoke-direct {v0, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f020239

    invoke-virtual {v0, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    invoke-virtual {v0, v3, v1, v1}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v2, Lanh;->ahs:Lani;

    move-object v0, v2

    .line 258
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 11

    .prologue
    const/16 v10, 0xf6

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p2, Lizj;->dTF:Ljhh;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    .line 88
    const/16 v0, 0x29

    invoke-virtual {v3, v0}, Lanh;->cm(I)Lanh;

    .line 89
    iput-object p2, v3, Lanh;->ahu:Lizj;

    .line 91
    iget-object v4, p2, Lizj;->dTF:Ljhh;

    .line 93
    new-instance v0, Laox;

    invoke-direct {v0}, Laox;-><init>()V

    invoke-virtual {v4}, Ljhh;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laox;->cw(Ljava/lang/String;)Laox;

    move-result-object v5

    .line 96
    invoke-virtual {v4}, Ljhh;->rF()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {v4}, Ljhh;->rE()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Laox;->cx(Ljava/lang/String;)Laox;

    .line 100
    :cond_0
    invoke-virtual {v4}, Ljhh;->rG()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {v5, v9}, Laox;->bi(Z)Laox;

    .line 104
    :cond_1
    invoke-virtual {v4}, Ljhh;->om()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    invoke-virtual {v4}, Ljhh;->ol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Laox;->cy(Ljava/lang/String;)Laox;

    .line 108
    :cond_2
    invoke-virtual {v4}, Ljhh;->pf()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    invoke-virtual {v4}, Ljhh;->oo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Laox;->cz(Ljava/lang/String;)Laox;

    .line 112
    :cond_3
    iget-object v6, v4, Ljhh;->dZG:[Ljdq;

    if-eqz v6, :cond_8

    array-length v7, v6

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_8

    aget-object v0, v6, v1

    invoke-virtual {v0}, Ljdq;->getSource()I

    move-result v8

    if-ne v8, v9, :cond_7

    .line 113
    :goto_1
    if-eqz v0, :cond_4

    .line 114
    const/16 v1, 0x89

    invoke-static {p1, v0, v1}, Lgbh;->a(Landroid/content/Context;Ljdq;I)Laok;

    move-result-object v0

    iput-object v0, v5, Laox;->ahN:Laok;

    .line 118
    :cond_4
    iget-object v0, v4, Ljhh;->eeb:[Ljcn;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 119
    new-instance v0, Laoi;

    invoke-direct {v0}, Laoi;-><init>()V

    iget-object v1, v4, Ljhh;->eeb:[Ljcn;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoi;->bQ(Ljava/lang/String;)Laoi;

    move-result-object v0

    iput-object v0, v5, Laox;->ahM:Laoi;

    .line 123
    :cond_5
    new-array v0, v2, [I

    invoke-static {p2, v10, v0}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_6

    .line 126
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v1, v1, Lizq;->dUZ:Lizj;

    iget-object v1, v1, Lizj;->dTE:Ljau;

    invoke-static {p1, v1}, Lfwu;->a(Landroid/content/Context;Ljau;)Ljava/lang/String;

    move-result-object v1

    .line 128
    new-instance v2, Ljei;

    invoke-direct {v2}, Ljei;-><init>()V

    invoke-virtual {v2, v1}, Ljei;->ud(Ljava/lang/String;)Ljei;

    move-result-object v1

    .line 131
    new-instance v2, Lfsa;

    invoke-direct {v2, v10}, Lfsa;-><init>(I)V

    invoke-virtual {v2, p2, v0, v1}, Lfsa;->a(Lizj;Liwk;Ljei;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    .line 135
    iget-object v0, v3, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahH:Laom;

    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    invoke-static {v1}, Lfwu;->g(Lizq;)[Ljcn;

    move-result-object v1

    iput-object v1, v0, Laom;->ajH:[Ljcn;

    .line 139
    :cond_6
    iput-object v5, v3, Lanh;->ahe:Laox;

    .line 141
    return-object v3

    .line 112
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final aDq()Z
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    return v0
.end method

.method public final aT(Landroid/content/Context;)Lanh;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUZ:Lizj;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 73
    iget-object v2, p0, Lfwu;->cCl:Lftx;

    iget-object v2, v0, Lizj;->dTE:Ljau;

    invoke-static {p1, v2}, Lfwu;->a(Landroid/content/Context;Ljau;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lizj;->dTE:Ljau;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    invoke-static {v2, v1}, Lftx;->aN(Ljava/lang/String;Ljava/lang/String;)Lanh;

    move-result-object v1

    .line 77
    iput-object v0, v1, Lanh;->ahu:Lizj;

    .line 79
    return-object v1

    .line 73
    :cond_1
    iget-object v4, v3, Ljau;->dXq:Ljhe;

    if-eqz v4, :cond_0

    iget-object v1, v3, Ljau;->dXq:Ljhe;

    invoke-virtual {v1}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/16 v7, 0xa2

    const/4 v6, 0x0

    .line 162
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizq;

    .line 164
    iget-object v2, v0, Lizq;->dUZ:Lizj;

    new-array v3, v6, [I

    invoke-static {v2, v7, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v3

    .line 166
    if-nez v3, :cond_0

    .line 202
    :goto_0
    return-object v1

    .line 170
    :cond_0
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    const v4, 0x7f0a0380

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Laoj;->bS(Ljava/lang/String;)Laoj;

    move-result-object v4

    .line 174
    invoke-virtual {v3}, Liwk;->aZp()Z

    move-result v2

    if-nez v2, :cond_1

    .line 175
    iget-object v1, v0, Lizq;->dUZ:Lizj;

    iget-object v1, v1, Lizj;->dTE:Ljau;

    invoke-static {p1, v1}, Lfwu;->a(Landroid/content/Context;Ljau;)Ljava/lang/String;

    move-result-object v1

    .line 177
    new-instance v2, Ljei;

    invoke-direct {v2}, Ljei;-><init>()V

    invoke-virtual {v2, v1}, Ljei;->ud(Ljava/lang/String;)Ljei;

    move-result-object v1

    .line 180
    :cond_1
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 181
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Lanh;->cm(I)Lanh;

    .line 182
    iget-object v5, v0, Lizq;->dUZ:Lizj;

    iput-object v5, v2, Lanh;->ahu:Lizj;

    .line 183
    iput-object v4, v2, Lanh;->agz:Laoj;

    .line 184
    new-instance v4, Lfsa;

    invoke-direct {v4, v7}, Lfsa;-><init>(I)V

    const v5, 0x7f0200ed

    invoke-virtual {v4, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v4

    invoke-virtual {v4, v0, v3, v1}, Lfsa;->a(Lizq;Liwk;Ljei;)Lani;

    move-result-object v1

    iput-object v1, v2, Lanh;->ahs:Lani;

    .line 189
    iget-object v1, v3, Liwk;->afA:Ljbj;

    if-eqz v1, :cond_3

    iget-object v1, v3, Liwk;->afA:Ljbj;

    iget-object v1, v1, Ljbj;->dYx:[I

    if-eqz v1, :cond_3

    iget-object v1, v3, Liwk;->afA:Ljbj;

    iget-object v1, v1, Ljbj;->dYx:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 192
    iget-object v1, v3, Liwk;->afA:Ljbj;

    iget-object v1, v1, Ljbj;->dYx:[I

    aget v1, v1, v6

    .line 193
    const/16 v3, 0x4e

    if-eq v1, v3, :cond_2

    const/16 v3, 0x84

    if-ne v1, v3, :cond_3

    .line 195
    :cond_2
    iget-object v1, v2, Lanh;->ahs:Lani;

    iget-object v1, v1, Lani;->ahH:Laom;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Laom;->bc(Z)Laom;

    .line 200
    :cond_3
    iget-object v1, v2, Lanh;->ahs:Lani;

    iget-object v1, v1, Lani;->ahH:Laom;

    invoke-static {v0}, Lfwu;->g(Lizq;)[Ljcn;

    move-result-object v0

    iput-object v0, v1, Laom;->ajH:[Ljcn;

    move-object v1, v2

    .line 202
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/util/List;)Lanh;
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    return-object v0
.end method
