.class public final Lfxu;
.super Landroid/graphics/drawable/ColorDrawable;
.source "PG"


# instance fields
.field private bVq:Landroid/graphics/drawable/shapes/Shape;

.field private bVs:F

.field private ob:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 30
    int-to-float v0, p2

    iput v0, p0, Lfxu;->bVs:F

    .line 31
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lfxu;->bVq:Landroid/graphics/drawable/shapes/Shape;

    if-nez v0, :cond_0

    .line 36
    invoke-super {p0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 45
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lfxu;->ob:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lfxu;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    invoke-virtual {p0}, Lfxu;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lfxu;->bVq:Landroid/graphics/drawable/shapes/Shape;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    .line 43
    iget-object v0, p0, Lfxu;->bVq:Landroid/graphics/drawable/shapes/Shape;

    iget-object v1, p0, Lfxu;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/shapes/Shape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final hi(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    iget v0, p0, Lfxu;->bVs:F

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    if-lez p1, :cond_4

    const/16 v1, 0x8

    new-array v1, v1, [F

    and-int/lit8 v2, p1, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    aput v0, v1, v2

    aput v0, v1, v4

    :cond_0
    and-int/lit8 v2, p1, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    aput v0, v1, v2

    const/4 v2, 0x3

    aput v0, v1, v2

    :cond_1
    and-int/lit8 v2, p1, 0x4

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    aput v0, v1, v2

    const/4 v2, 0x5

    aput v0, v1, v2

    :cond_2
    and-int/lit8 v2, p1, 0x8

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    aput v0, v1, v2

    const/4 v2, 0x7

    aput v0, v1, v2

    :cond_3
    new-instance v0, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v0, v1, v3, v3}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iput-object v0, p0, Lfxu;->bVq:Landroid/graphics/drawable/shapes/Shape;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lfxu;->ob:Landroid/graphics/Paint;

    iget-object v0, p0, Lfxu;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 56
    :goto_0
    invoke-virtual {p0}, Lfxu;->invalidateSelf()V

    .line 57
    return-void

    .line 53
    :cond_4
    iput-object v3, p0, Lfxu;->bVq:Landroid/graphics/drawable/shapes/Shape;

    iput-object v3, p0, Lfxu;->ob:Landroid/graphics/Paint;

    goto :goto_0
.end method
