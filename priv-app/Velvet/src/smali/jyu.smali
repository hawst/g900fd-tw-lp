.class public final Ljyu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dIC:I

.field private dIk:Ljava/lang/String;

.field private eMw:I

.field private eMx:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 621
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 622
    iput v0, p0, Ljyu;->aez:I

    iput v0, p0, Ljyu;->eMw:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljyu;->eMx:J

    const-string v0, ""

    iput-object v0, p0, Ljyu;->dIk:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljyu;->dIC:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljyu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyu;->eCz:I

    .line 623
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 523
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyu;->eMw:I

    iget v0, p0, Ljyu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljyu;->eMx:J

    iget v0, p0, Ljyu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyu;->dIk:Ljava/lang/String;

    iget v0, p0, Ljyu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyu;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyu;->dIC:I

    iget v0, p0, Ljyu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljyu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 639
    iget v0, p0, Ljyu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 640
    const/4 v0, 0x1

    iget v1, p0, Ljyu;->eMw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 642
    :cond_0
    iget v0, p0, Ljyu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 643
    const/4 v0, 0x2

    iget-wide v2, p0, Ljyu;->eMx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 645
    :cond_1
    iget v0, p0, Ljyu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 646
    const/4 v0, 0x3

    iget-object v1, p0, Ljyu;->dIk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 648
    :cond_2
    iget v0, p0, Ljyu;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 649
    const/4 v0, 0x4

    iget v1, p0, Ljyu;->dIC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 651
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 652
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 656
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 657
    iget v1, p0, Ljyu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 658
    const/4 v1, 0x1

    iget v2, p0, Ljyu;->eMw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 661
    :cond_0
    iget v1, p0, Ljyu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 662
    const/4 v1, 0x2

    iget-wide v2, p0, Ljyu;->eMx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 665
    :cond_1
    iget v1, p0, Ljyu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 666
    const/4 v1, 0x3

    iget-object v2, p0, Ljyu;->dIk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 669
    :cond_2
    iget v1, p0, Ljyu;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 670
    const/4 v1, 0x4

    iget v2, p0, Ljyu;->dIC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 673
    :cond_3
    return v0
.end method
