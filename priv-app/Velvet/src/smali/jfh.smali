.class public final Ljfh;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ehU:[Ljfh;


# instance fields
.field private aez:I

.field private agv:I

.field private dXd:I

.field private ehV:Ljava/lang/String;

.field private ehW:Ljava/lang/String;

.field private ehX:Z

.field private ehY:Ljava/lang/String;

.field private ehZ:Ljava/lang/String;

.field private eia:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11229
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 11230
    iput v1, p0, Ljfh;->aez:I

    iput v1, p0, Ljfh;->dXd:I

    const-string v0, ""

    iput-object v0, p0, Ljfh;->ehV:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljfh;->ehW:Ljava/lang/String;

    iput-boolean v1, p0, Ljfh;->ehX:Z

    const-string v0, ""

    iput-object v0, p0, Ljfh;->ehY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljfh;->ehZ:Ljava/lang/String;

    iput-boolean v1, p0, Ljfh;->eia:Z

    const/4 v0, 0x1

    iput v0, p0, Ljfh;->agv:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljfh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfh;->eCz:I

    .line 11231
    return-void
.end method

.method public static bjn()[Ljfh;
    .locals 2

    .prologue
    .line 11052
    sget-object v0, Ljfh;->ehU:[Ljfh;

    if-nez v0, :cond_1

    .line 11053
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 11055
    :try_start_0
    sget-object v0, Ljfh;->ehU:[Ljfh;

    if-nez v0, :cond_0

    .line 11056
    const/4 v0, 0x0

    new-array v0, v0, [Ljfh;

    sput-object v0, Ljfh;->ehU:[Ljfh;

    .line 11058
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11060
    :cond_1
    sget-object v0, Ljfh;->ehU:[Ljfh;

    return-object v0

    .line 11058
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11041
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfh;->dXd:I

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfh;->ehV:Ljava/lang/String;

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfh;->ehW:Ljava/lang/String;

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfh;->ehX:Z

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfh;->ehY:Ljava/lang/String;

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfh;->ehZ:Ljava/lang/String;

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfh;->eia:Z

    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljfh;->agv:I

    iget v0, p0, Ljfh;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljfh;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 11251
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11252
    const/4 v0, 0x1

    iget v1, p0, Ljfh;->dXd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 11254
    :cond_0
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 11255
    const/4 v0, 0x2

    iget-object v1, p0, Ljfh;->ehV:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11257
    :cond_1
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 11258
    const/4 v0, 0x3

    iget-object v1, p0, Ljfh;->ehW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11260
    :cond_2
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 11261
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfh;->ehX:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11263
    :cond_3
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 11264
    const/4 v0, 0x5

    iget-object v1, p0, Ljfh;->ehY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11266
    :cond_4
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 11267
    const/4 v0, 0x7

    iget-object v1, p0, Ljfh;->ehZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11269
    :cond_5
    iget v0, p0, Ljfh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 11270
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljfh;->eia:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11272
    :cond_6
    iget v0, p0, Ljfh;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 11273
    const/16 v0, 0x9

    iget v1, p0, Ljfh;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 11275
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 11276
    return-void
.end method

.method public final bew()I
    .locals 1

    .prologue
    .line 11068
    iget v0, p0, Ljfh;->dXd:I

    return v0
.end method

.method public final bjo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11172
    iget-object v0, p0, Ljfh;->ehZ:Ljava/lang/String;

    return-object v0
.end method

.method public final ib(Z)Ljfh;
    .locals 1

    .prologue
    .line 11134
    iput-boolean p1, p0, Ljfh;->ehX:Z

    .line 11135
    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfh;->aez:I

    .line 11136
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 11280
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 11281
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11282
    const/4 v1, 0x1

    iget v2, p0, Ljfh;->dXd:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11285
    :cond_0
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 11286
    const/4 v1, 0x2

    iget-object v2, p0, Ljfh;->ehV:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11289
    :cond_1
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 11290
    const/4 v1, 0x3

    iget-object v2, p0, Ljfh;->ehW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11293
    :cond_2
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 11294
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfh;->ehX:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11297
    :cond_3
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 11298
    const/4 v1, 0x5

    iget-object v2, p0, Ljfh;->ehY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11301
    :cond_4
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 11302
    const/4 v1, 0x7

    iget-object v2, p0, Ljfh;->ehZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11305
    :cond_5
    iget v1, p0, Ljfh;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 11306
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljfh;->eia:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 11309
    :cond_6
    iget v1, p0, Ljfh;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 11310
    const/16 v1, 0x9

    iget v2, p0, Ljfh;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11313
    :cond_7
    return v0
.end method

.method public final pt(I)Ljfh;
    .locals 1

    .prologue
    .line 11071
    iput p1, p0, Ljfh;->dXd:I

    .line 11072
    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfh;->aez:I

    .line 11073
    return-object p0
.end method

.method public final pu(I)Ljfh;
    .locals 1

    .prologue
    .line 11216
    iput p1, p0, Ljfh;->agv:I

    .line 11217
    iget v0, p0, Ljfh;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljfh;->aez:I

    .line 11218
    return-object p0
.end method

.method public final un(Ljava/lang/String;)Ljfh;
    .locals 1

    .prologue
    .line 11090
    if-nez p1, :cond_0

    .line 11091
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11093
    :cond_0
    iput-object p1, p0, Ljfh;->ehV:Ljava/lang/String;

    .line 11094
    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfh;->aez:I

    .line 11095
    return-object p0
.end method

.method public final uo(Ljava/lang/String;)Ljfh;
    .locals 1

    .prologue
    .line 11175
    if-nez p1, :cond_0

    .line 11176
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11178
    :cond_0
    iput-object p1, p0, Ljfh;->ehZ:Ljava/lang/String;

    .line 11179
    iget v0, p0, Ljfh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljfh;->aez:I

    .line 11180
    return-object p0
.end method
