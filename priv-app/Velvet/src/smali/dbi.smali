.class public final Ldbi;
.super Lddj;
.source "PG"


# instance fields
.field private bcr:Ljyl;

.field private bqI:Z

.field private bqJ:Ljava/util/List;

.field private bqv:Lcom/google/android/shared/search/Query;

.field private final mEventBus:Ldda;


# direct methods
.method public constructor <init>(Ldda;)V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x400

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 23
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldbi;->bqv:Lcom/google/android/shared/search/Query;

    .line 29
    iput-object p1, p0, Ldbi;->mEventBus:Ldda;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbi;->bqJ:Ljava/util/List;

    .line 31
    return-void
.end method


# virtual methods
.method public final Qv()Ljyl;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldbi;->bcr:Ljyl;

    return-object v0
.end method

.method public final Wj()Ljava/util/List;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Ldbi;->bqJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 62
    iget-object v0, p0, Ldbi;->bqJ:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 63
    iget-object v1, p0, Ldbi;->bqJ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Wk()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ldbi;->bqv:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V
    .locals 4

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    iget-object v2, p0, Ldbi;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Ldbi;->bqJ:Ljava/util/List;

    invoke-static {p2, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    invoke-virtual {p0}, Ldbi;->notifyChanged()V

    .line 75
    :cond_0
    return-void
.end method

.method public final a(Lddb;)V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 40
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    iget-object v0, p0, Ldbi;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anm()Z

    move-result v0

    iput-boolean v0, p0, Ldbi;->bqI:Z

    .line 43
    iget-boolean v0, p0, Ldbi;->bqI:Z

    or-int/lit8 v0, v0, 0x0

    .line 45
    :cond_0
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_3

    .line 46
    :cond_1
    iget-object v0, p0, Ldbi;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 47
    iget-object v1, p0, Ldbi;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 48
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iput-object v1, p0, Ldbi;->bqv:Lcom/google/android/shared/search/Query;

    .line 49
    const/4 v1, 0x0

    iput-object v1, p0, Ldbi;->bcr:Ljyl;

    .line 50
    iget-object v1, p0, Ldbi;->bqJ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 52
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Qv()Ljyl;

    move-result-object v0

    .line 53
    iget-boolean v1, p0, Ldbi;->bqI:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldbi;->bcr:Ljyl;

    if-eq v0, v1, :cond_3

    .line 54
    iput-object v0, p0, Ldbi;->bcr:Ljyl;

    .line 55
    invoke-virtual {p0}, Ldbi;->notifyChanged()V

    .line 58
    :cond_3
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Clockwork{"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v0, "mCurrentQuery="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldbi;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 85
    const-string v0, " mHandleClockworkResults="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Ldbi;->bqI:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 86
    const-string v0, " mClockworkResult="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Ldbi;->bcr:Ljyl;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    const-string v0, " mTranscriptions.size()="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldbi;->bqJ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 88
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 86
    :cond_0
    iget-object v0, p0, Ldbi;->bcr:Ljyl;

    goto :goto_0
.end method
