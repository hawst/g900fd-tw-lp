.class public final Lfuc;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 8

    .prologue
    const v7, 0x7f110379

    const v6, 0x7f110307

    .line 33
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v1, v0, Lanh;->ahm:Laob;

    .line 34
    iget-object v2, p0, Lfro;->mView:Landroid/view/View;

    .line 37
    const v0, 0x7f1101a9

    invoke-virtual {v1}, Laob;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 40
    iget-object v0, v1, Laob;->ahN:Laok;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lfuc;->mContext:Landroid/content/Context;

    iget-object v3, v1, Laob;->ahN:Laok;

    invoke-static {v0, v3}, Lfxt;->a(Landroid/content/Context;Laok;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 43
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 44
    const-string v3, " \u00b7 "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {v1}, Laob;->qj()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 47
    invoke-static {v2, v6, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 48
    iget-object v0, v1, Laob;->ahN:Laok;

    iget-object v0, v0, Laok;->ahs:Lani;

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 50
    iget-object v3, v1, Laob;->ahN:Laok;

    iget-object v3, v3, Laok;->ahs:Lani;

    invoke-virtual {p0, v0, v3}, Lfuc;->a(Landroid/view/View;Lani;)V

    .line 55
    :cond_0
    const v0, 0x7f110305

    invoke-virtual {v1}, Laob;->qk()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 56
    const v0, 0x7f110306

    invoke-virtual {v1}, Laob;->ql()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 59
    iget-object v0, v1, Laob;->aiX:Ljcn;

    if-eqz v0, :cond_1

    iget-object v0, v1, Laob;->aiX:Ljcn;

    invoke-virtual {v0}, Ljcn;->qG()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, v1, Laob;->aiX:Ljcn;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v7, v0}, Lfuc;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 61
    iget-object v0, v1, Laob;->aiY:Lani;

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 63
    iget-object v1, v1, Laob;->aiY:Lani;

    invoke-virtual {p0, v0, v1}, Lfuc;->a(Landroid/view/View;Lani;)V

    .line 66
    :cond_1
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 28
    const v0, 0x7f040115

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
