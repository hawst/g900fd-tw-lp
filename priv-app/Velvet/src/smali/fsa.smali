.class public final Lfsa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cBw:I

.field cBx:Ljava/lang/String;

.field cBy:Ljava/lang/String;

.field ceW:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lfsa;->cBw:I

    .line 51
    return-void
.end method

.method static a(Lani;Lixx;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 330
    iput-object p1, p0, Lani;->ahD:Lixx;

    .line 333
    iget-object v0, p0, Lani;->ahC:Lanb;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lixx;->bbd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    invoke-virtual {p1}, Lixx;->bbd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lixx;->bbc()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v1

    .line 335
    :goto_0
    if-eq v0, v1, :cond_1

    .line 336
    new-instance v1, Lanb;

    invoke-direct {v1}, Lanb;-><init>()V

    iput-object v1, p0, Lani;->ahC:Lanb;

    .line 337
    iget-object v1, p0, Lani;->ahC:Lanb;

    invoke-virtual {v1, v0}, Lanb;->ch(I)Lanb;

    .line 340
    :cond_1
    return-void

    .line 334
    :pswitch_1
    const v0, 0x7f0200ed

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02012d

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0201aa

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0200f7

    goto :goto_0

    :pswitch_5
    const v0, 0x7f020118

    goto :goto_0

    :pswitch_6
    const v0, 0x7f02023b

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0201bb

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0201e4

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0201b8

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final D(Ljava/lang/String;I)Lani;
    .locals 2

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 144
    iget-object v1, v0, Lani;->ahE:Laoe;

    invoke-virtual {v1, p2}, Laoe;->cB(I)Laoe;

    .line 145
    return-object v0
.end method

.method public final L(Landroid/content/Intent;)Lani;
    .locals 2

    .prologue
    .line 153
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    return-object v0
.end method

.method public final a(Laoo;)Lani;
    .locals 1

    .prologue
    .line 360
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lfsa;->jC(I)Lani;

    move-result-object v0

    .line 361
    iput-object p1, v0, Lani;->ahJ:Laoo;

    .line 362
    return-object v0
.end method

.method public final a(Lizj;Liwk;Ljei;)Lani;
    .locals 3
    .param p3    # Ljei;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161
    new-instance v0, Lizq;

    invoke-direct {v0}, Lizq;-><init>()V

    .line 162
    const/4 v1, 0x1

    new-array v1, v1, [Lizj;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    iput-object v1, v0, Lizq;->dUX:[Lizj;

    .line 163
    invoke-virtual {p0, v0, p2, p3}, Lfsa;->a(Lizq;Liwk;Ljei;)Lani;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lizq;Liwk;Ljei;)Lani;
    .locals 2
    .param p3    # Ljei;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 190
    new-instance v0, Laom;

    invoke-direct {v0}, Laom;-><init>()V

    .line 191
    iput-object p1, v0, Laom;->ajK:Lizq;

    .line 192
    if-eqz p3, :cond_0

    .line 193
    iput-object p3, v0, Laom;->ajE:Ljei;

    .line 195
    :cond_0
    iget-object v1, p2, Liwk;->afA:Ljbj;

    if-eqz v1, :cond_1

    .line 196
    iget-object v1, p2, Liwk;->afA:Ljbj;

    iput-object v1, v0, Laom;->afA:Ljbj;

    .line 201
    :cond_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljei;->pb()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 202
    invoke-virtual {p3}, Ljei;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laom;->bY(Ljava/lang/String;)Laom;

    .line 207
    :cond_2
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lfsa;->jC(I)Lani;

    move-result-object v1

    .line 208
    iput-object v0, v1, Lani;->ahH:Laom;

    .line 209
    return-object v1

    .line 203
    :cond_3
    invoke-virtual {p2}, Liwk;->aZp()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 204
    invoke-virtual {p2}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laom;->bY(Ljava/lang/String;)Laom;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lfmt;)Lani;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 173
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->h(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;

    move-result-object v1

    .line 176
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->auH()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 181
    :goto_0
    return-object v0

    .line 180
    :cond_0
    const-string v1, "ClientActionBuilder"

    const-string v2, "Didn\'t get a capability context for showing Phone number"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;)Lani;
    .locals 3

    .prologue
    .line 237
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lfsa;->jC(I)Lani;

    move-result-object v0

    .line 238
    new-instance v1, Lapw;

    invoke-direct {v1}, Lapw;-><init>()V

    invoke-virtual {v1, p1}, Lapw;->dU(Ljava/lang/String;)Lapw;

    move-result-object v1

    invoke-virtual {v1, p2}, Lapw;->dV(Ljava/lang/String;)Lapw;

    move-result-object v1

    invoke-virtual {v1, p3}, Lapw;->dW(Ljava/lang/String;)Lapw;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lapw;->bm(Z)Lapw;

    move-result-object v1

    invoke-virtual {v1, p5}, Lapw;->bn(Z)Lapw;

    move-result-object v1

    iput-object v1, v0, Lani;->ahI:Lapw;

    .line 244
    iget-object v1, v0, Lani;->ahI:Lapw;

    iput-object p6, v1, Lapw;->amb:[Ljava/lang/String;

    .line 245
    return-object v0
.end method

.method a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;
    .locals 2
    .param p2    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    new-instance v0, Laol;

    invoke-direct {v0}, Laol;-><init>()V

    .line 106
    invoke-virtual {v0, p1}, Laol;->bW(Ljava/lang/String;)Laol;

    .line 107
    if-eqz p2, :cond_0

    .line 108
    iput-object p2, v0, Laol;->ajC:Ljbp;

    .line 110
    :cond_0
    if-eqz p3, :cond_1

    .line 111
    invoke-virtual {v0, p3}, Laol;->bX(Ljava/lang/String;)Laol;

    .line 114
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lfsa;->jC(I)Lani;

    move-result-object v1

    .line 115
    iput-object v0, v1, Lani;->ahF:Laol;

    .line 116
    return-object v1
.end method

.method public final aM(Ljava/lang/String;Ljava/lang/String;)Lani;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 127
    new-instance v0, Laoe;

    invoke-direct {v0}, Laoe;-><init>()V

    .line 128
    invoke-virtual {v0, p1}, Laoe;->bG(Ljava/lang/String;)Laoe;

    .line 129
    if-eqz p2, :cond_0

    .line 130
    invoke-virtual {v0, p2}, Laoe;->bH(Ljava/lang/String;)Laoe;

    .line 133
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lfsa;->jC(I)Lani;

    move-result-object v1

    .line 134
    iput-object v0, v1, Lani;->ahE:Laoe;

    .line 135
    return-object v1
.end method

.method public final b(Lixx;)Lani;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 259
    .line 260
    invoke-virtual {p1}, Lixx;->baX()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 261
    invoke-virtual {p1}, Lixx;->avd()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lixx;->bbe()Ljava/lang/String;

    move-result-object v0

    .line 264
    :cond_0
    invoke-virtual {p1}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 289
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 290
    invoke-static {v0, p1}, Lfsa;->a(Lani;Lixx;)V

    .line 293
    :cond_2
    return-object v0

    .line 265
    :cond_3
    invoke-virtual {p1}, Lixx;->bba()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 266
    invoke-virtual {p1}, Lixx;->qQ()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 267
    invoke-virtual {p1}, Lixx;->baZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lixx;->qP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    goto :goto_0

    .line 270
    :cond_4
    invoke-virtual {p1}, Lixx;->baZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v0}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    goto :goto_0

    .line 272
    :cond_5
    invoke-virtual {p1}, Lixx;->baY()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p1}, Lixx;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    const/high16 v1, 0x10000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 276
    iget-object v3, p1, Lixx;->dOR:[Lixy;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_9

    aget-object v5, v3, v1

    .line 277
    invoke-virtual {v5}, Lixy;->TW()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 278
    invoke-virtual {v5}, Lixy;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lixy;->TV()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    :cond_6
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 279
    :cond_7
    invoke-virtual {v5}, Lixy;->TU()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 280
    invoke-virtual {v5}, Lixy;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lixy;->TT()J

    move-result-wide v8

    invoke-virtual {v2, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_2

    .line 281
    :cond_8
    invoke-virtual {v5}, Lixy;->TP()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 282
    invoke-virtual {v5}, Lixy;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lixy;->TO()Z

    move-result v5

    invoke-virtual {v2, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 286
    :cond_9
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final jB(I)Lfsa;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lfsa;->cBx:Ljava/lang/String;

    .line 59
    iput p1, p0, Lfsa;->ceW:I

    .line 60
    return-object p0
.end method

.method jC(I)Lani;
    .locals 3

    .prologue
    .line 366
    new-instance v0, Lani;

    invoke-direct {v0}, Lani;-><init>()V

    .line 367
    invoke-virtual {v0, p1}, Lani;->cp(I)Lani;

    .line 368
    iget v1, p0, Lfsa;->cBw:I

    invoke-virtual {v0, v1}, Lani;->cq(I)Lani;

    .line 370
    iget v1, p0, Lfsa;->ceW:I

    if-eqz v1, :cond_2

    .line 371
    new-instance v1, Lanb;

    invoke-direct {v1}, Lanb;-><init>()V

    iput-object v1, v0, Lani;->ahC:Lanb;

    .line 372
    iget-object v1, v0, Lani;->ahC:Lanb;

    iget v2, p0, Lfsa;->ceW:I

    invoke-virtual {v1, v2}, Lanb;->ch(I)Lanb;

    .line 377
    :cond_0
    :goto_0
    iget-object v1, p0, Lfsa;->cBy:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 378
    iget-object v1, p0, Lfsa;->cBy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lani;->an(Ljava/lang/String;)Lani;

    .line 381
    :cond_1
    return-object v0

    .line 373
    :cond_2
    iget-object v1, p0, Lfsa;->cBx:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 374
    new-instance v1, Lanb;

    invoke-direct {v1}, Lanb;-><init>()V

    iput-object v1, v0, Lani;->ahC:Lanb;

    .line 375
    iget-object v1, v0, Lani;->ahC:Lanb;

    iget-object v2, p0, Lfsa;->cBx:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lanb;->Y(Ljava/lang/String;)Lanb;

    goto :goto_0
.end method
