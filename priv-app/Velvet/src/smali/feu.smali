.class public abstract Lfeu;
.super Lfes;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 1

    .prologue
    .line 15
    invoke-static {p1}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v0

    invoke-direct {p0, v0}, Lfes;-><init>(Ljava/util/Collection;)V

    .line 16
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->oP()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 18
    return-void
.end method


# virtual methods
.method public ayU()Ljava/lang/String;
    .locals 3

    .prologue
    .line 40
    invoke-super {p0}, Lfes;->ayU()Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lfeu;->getEntry()Lizj;

    move-result-object v1

    invoke-static {v1}, Lgbm;->M(Lizj;)Ljava/lang/String;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43
    :cond_0
    return-object v0
.end method

.method public ayW()I
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lfeu;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    return v0
.end method

.method public final ayX()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lfeu;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    .line 35
    invoke-virtual {v0}, Ljcg;->bge()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljcg;->bgd()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getEntry()Lizj;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lfes;->ckC:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    return-object v0
.end method
