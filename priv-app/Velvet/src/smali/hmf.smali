.class public final Lhmf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlk;


# instance fields
.field private final mAppSelectionHelper:Libo;

.field private final mIntentStarter:Leoj;


# direct methods
.method public constructor <init>(Leoj;Libo;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lhmf;->mIntentStarter:Leoj;

    .line 29
    iput-object p2, p0, Lhmf;->mAppSelectionHelper:Libo;

    .line 30
    return-void
.end method


# virtual methods
.method public final synthetic G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avb()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/PlayMediaAction;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 44
    packed-switch p2, :pswitch_data_0

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 47
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahW()Lcom/google/android/shared/util/App;

    move-result-object v1

    .line 48
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->getMimeType()Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahV()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 50
    iget-object v3, p0, Lhmf;->mAppSelectionHelper:Libo;

    invoke-virtual {v1}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Libo;->bc(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahP()Lcom/google/android/shared/util/App;

    move-result-object v2

    if-ne v2, v1, :cond_2

    .line 53
    const/16 v1, 0x5b

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->uF()I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mC(I)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 57
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahW()Lcom/google/android/shared/util/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/App;->auq()Landroid/content/Intent;

    move-result-object v1

    .line 58
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 68
    :goto_1
    if-eqz v1, :cond_0

    .line 69
    iget-object v2, p0, Lhmf;->mIntentStarter:Leoj;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    aput-object v1, v3, v0

    invoke-interface {v2, v3}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 61
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahR()Landroid/content/Intent;

    move-result-object v1

    goto :goto_1

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {p0, p1, p2}, Lhmf;->a(Lcom/google/android/search/shared/actions/PlayMediaAction;I)Z

    move-result v0

    return v0
.end method
