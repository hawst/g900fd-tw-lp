.class public final Lgar;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leol;


# instance fields
.field private final aXe:Ljava/util/concurrent/Executor;

.field private final cEu:Lifq;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 125
    invoke-static {}, Lifq;->aVX()Lifq;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lgar;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lifq;)V

    .line 126
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lifq;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lgar;->mContext:Landroid/content/Context;

    .line 131
    iput-object p2, p0, Lgar;->aXe:Ljava/util/concurrent/Executor;

    .line 132
    iput-object p3, p0, Lgar;->cEu:Lifq;

    .line 133
    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;Landroid/content/Context;)Z
    .locals 5

    .prologue
    .line 137
    packed-switch p1, :pswitch_data_0

    .line 145
    const-string v0, "GooglePlusIntents"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Google+ location settings Intent failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lgar;->cEu:Lifq;

    invoke-virtual {v0}, Lifq;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lgar;->cEu:Lifq;

    invoke-virtual {v0}, Lifq;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leol;

    invoke-interface {v0, p1, p2, p3}, Leol;->a(ILandroid/content/Intent;Landroid/content/Context;)Z

    .line 151
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 142
    :pswitch_1
    iget-object v0, p0, Lgar;->mContext:Landroid/content/Context;

    const v1, 0x7f0a016a

    iget-object v2, p0, Lgar;->aXe:Ljava/util/concurrent/Executor;

    new-instance v3, Lgas;

    const-string v4, "Show toast: G+"

    invoke-direct {v3, p0, v4, v0, v1}, Lgas;-><init>(Lgar;Ljava/lang/String;Landroid/content/Context;I)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
