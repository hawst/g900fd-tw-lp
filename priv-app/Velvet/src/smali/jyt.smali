.class public final Ljyt;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public clientExperimentIds:[I

.field private dRy:Ljava/lang/String;

.field private eCg:J

.field private eHj:Ljava/lang/String;

.field private eHl:Ljava/lang/String;

.field private eHm:I

.field private eHn:I

.field private eMr:[Ljsw;

.field private eMs:Ljyu;

.field public eMt:Litu;

.field private eMu:Ljava/lang/String;

.field private eMv:Ljava/lang/String;

.field public serverExperimentIds:[I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 209
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 210
    iput v2, p0, Ljyt;->aez:I

    invoke-static {}, Ljsw;->bug()[Ljsw;

    move-result-object v0

    iput-object v0, p0, Ljyt;->eMr:[Ljsw;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljyt;->eCg:J

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljyt;->clientExperimentIds:[I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljyt;->serverExperimentIds:[I

    iput-object v3, p0, Ljyt;->eMs:Ljyu;

    iput-object v3, p0, Ljyt;->eMt:Litu;

    const-string v0, ""

    iput-object v0, p0, Ljyt;->eMu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljyt;->eMv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljyt;->dRy:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljyt;->eHj:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljyt;->eHl:Ljava/lang/String;

    iput v2, p0, Ljyt;->eHm:I

    iput v2, p0, Ljyt;->eHn:I

    iput-object v3, p0, Ljyt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyt;->eCz:I

    .line 211
    return-void
.end method


# virtual methods
.method public final Cu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ljyt;->eMu:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyt;->eMr:[Ljsw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljyt;->eMr:[Ljsw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljsw;

    invoke-direct {v3}, Ljsw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljyt;->eMr:[Ljsw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljsw;

    invoke-direct {v3}, Ljsw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljyt;->eMr:[Ljsw;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v2

    iput-wide v2, p0, Ljyt;->eCg:J

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyt;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1d

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljyt;->clientExperimentIds:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljyt;->clientExperimentIds:[I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v3, v0

    new-array v3, v3, [I

    if-eqz v0, :cond_7

    iget-object v4, p0, Ljyt;->clientExperimentIds:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v4, v3

    if-ge v0, v4, :cond_9

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    array-length v0, v0

    goto :goto_5

    :cond_9
    iput-object v3, p0, Ljyt;->clientExperimentIds:[I

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljyt;->eMs:Ljyu;

    if-nez v0, :cond_a

    new-instance v0, Ljyu;

    invoke-direct {v0}, Ljyu;-><init>()V

    iput-object v0, p0, Ljyt;->eMs:Ljyu;

    :cond_a
    iget-object v0, p0, Ljyt;->eMs:Ljyu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyt;->eMu:Ljava/lang/String;

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyt;->eMv:Ljava/lang/String;

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljyt;->eMt:Litu;

    if-nez v0, :cond_b

    new-instance v0, Litu;

    invoke-direct {v0}, Litu;-><init>()V

    iput-object v0, p0, Ljyt;->eMt:Litu;

    :cond_b
    iget-object v0, p0, Ljyt;->eMt:Litu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyt;->dRy:Ljava/lang/String;

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyt;->eHj:Ljava/lang/String;

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyt;->eHl:Ljava/lang/String;

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyt;->eHm:I

    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyt;->eHn:I

    iget v0, p0, Ljyt;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljyt;->aez:I

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x6d

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    if-nez v0, :cond_d

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_c

    iget-object v3, p0, Ljyt;->serverExperimentIds:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_e

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    array-length v0, v0

    goto :goto_7

    :cond_e
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljyt;->serverExperimentIds:[I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    if-nez v0, :cond_10

    move v0, v1

    :goto_9
    add-int/2addr v3, v0

    new-array v3, v3, [I

    if-eqz v0, :cond_f

    iget-object v4, p0, Ljyt;->serverExperimentIds:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    :goto_a
    array-length v4, v3

    if-ge v0, v4, :cond_11

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_10
    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    array-length v0, v0

    goto :goto_9

    :cond_11
    iput-object v3, p0, Ljyt;->serverExperimentIds:[I

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_4
        0x1d -> :sswitch_3
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x58 -> :sswitch_c
        0x60 -> :sswitch_d
        0x6a -> :sswitch_f
        0x6d -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 236
    iget-object v0, p0, Ljyt;->eMr:[Ljsw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljyt;->eMr:[Ljsw;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 237
    :goto_0
    iget-object v2, p0, Ljyt;->eMr:[Ljsw;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 238
    iget-object v2, p0, Ljyt;->eMr:[Ljsw;

    aget-object v2, v2, v0

    .line 239
    if-eqz v2, :cond_0

    .line 240
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 237
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_1
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 245
    const/4 v0, 0x2

    iget-wide v2, p0, Ljyt;->eCg:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 247
    :cond_2
    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 248
    :goto_1
    iget-object v2, p0, Ljyt;->clientExperimentIds:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 249
    const/4 v2, 0x3

    iget-object v3, p0, Ljyt;->clientExperimentIds:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->br(II)V

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 252
    :cond_3
    iget-object v0, p0, Ljyt;->eMs:Ljyu;

    if-eqz v0, :cond_4

    .line 253
    const/4 v0, 0x4

    iget-object v2, p0, Ljyt;->eMs:Ljyu;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 255
    :cond_4
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 256
    const/4 v0, 0x5

    iget-object v2, p0, Ljyt;->eMu:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 258
    :cond_5
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 259
    const/4 v0, 0x6

    iget-object v2, p0, Ljyt;->eMv:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 261
    :cond_6
    iget-object v0, p0, Ljyt;->eMt:Litu;

    if-eqz v0, :cond_7

    .line 262
    const/4 v0, 0x7

    iget-object v2, p0, Ljyt;->eMt:Litu;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 264
    :cond_7
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 265
    const/16 v0, 0x8

    iget-object v2, p0, Ljyt;->dRy:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 267
    :cond_8
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 268
    const/16 v0, 0x9

    iget-object v2, p0, Ljyt;->eHj:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 270
    :cond_9
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    .line 271
    const/16 v0, 0xa

    iget-object v2, p0, Ljyt;->eHl:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 273
    :cond_a
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_b

    .line 274
    const/16 v0, 0xb

    iget v2, p0, Ljyt;->eHm:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 276
    :cond_b
    iget v0, p0, Ljyt;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_c

    .line 277
    const/16 v0, 0xc

    iget v2, p0, Ljyt;->eHn:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 279
    :cond_c
    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    array-length v0, v0

    if-lez v0, :cond_d

    .line 280
    :goto_2
    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 281
    const/16 v0, 0xd

    iget-object v2, p0, Ljyt;->serverExperimentIds:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->br(II)V

    .line 280
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 284
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 285
    return-void
.end method

.method public final buX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Ljyt;->eHl:Ljava/lang/String;

    return-object v0
.end method

.method public final bww()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ljyt;->eMv:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 289
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 290
    iget-object v0, p0, Ljyt;->eMr:[Ljsw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljyt;->eMr:[Ljsw;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 291
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljyt;->eMr:[Ljsw;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 292
    iget-object v2, p0, Ljyt;->eMr:[Ljsw;

    aget-object v2, v2, v0

    .line 293
    if-eqz v2, :cond_0

    .line 294
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 291
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 299
    :cond_1
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 300
    const/4 v0, 0x2

    iget-wide v2, p0, Ljyt;->eCg:J

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v1, v0

    .line 303
    :cond_2
    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 304
    iget-object v0, p0, Ljyt;->clientExperimentIds:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    .line 305
    add-int/2addr v0, v1

    .line 306
    iget-object v1, p0, Ljyt;->clientExperimentIds:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 308
    :cond_3
    iget-object v0, p0, Ljyt;->eMs:Ljyu;

    if-eqz v0, :cond_4

    .line 309
    const/4 v0, 0x4

    iget-object v2, p0, Ljyt;->eMs:Ljyu;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 312
    :cond_4
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 313
    const/4 v0, 0x5

    iget-object v2, p0, Ljyt;->eMu:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 316
    :cond_5
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 317
    const/4 v0, 0x6

    iget-object v2, p0, Ljyt;->eMv:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 320
    :cond_6
    iget-object v0, p0, Ljyt;->eMt:Litu;

    if-eqz v0, :cond_7

    .line 321
    const/4 v0, 0x7

    iget-object v2, p0, Ljyt;->eMt:Litu;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 324
    :cond_7
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 325
    const/16 v0, 0x8

    iget-object v2, p0, Ljyt;->dRy:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 328
    :cond_8
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 329
    const/16 v0, 0x9

    iget-object v2, p0, Ljyt;->eHj:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 332
    :cond_9
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    .line 333
    const/16 v0, 0xa

    iget-object v2, p0, Ljyt;->eHl:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 336
    :cond_a
    iget v0, p0, Ljyt;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_b

    .line 337
    const/16 v0, 0xb

    iget v2, p0, Ljyt;->eHm:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 340
    :cond_b
    iget v0, p0, Ljyt;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_c

    .line 341
    const/16 v0, 0xc

    iget v2, p0, Ljyt;->eHn:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 344
    :cond_c
    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    array-length v0, v0

    if-lez v0, :cond_d

    .line 345
    iget-object v0, p0, Ljyt;->serverExperimentIds:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    .line 346
    add-int/2addr v0, v1

    .line 347
    iget-object v1, p0, Ljyt;->serverExperimentIds:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 349
    :cond_d
    return v1
.end method

.method public final tn(I)Ljyt;
    .locals 1

    .prologue
    .line 177
    iput p1, p0, Ljyt;->eHm:I

    .line 178
    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljyt;->aez:I

    .line 179
    return-object p0
.end method

.method public final to(I)Ljyt;
    .locals 1

    .prologue
    .line 196
    iput p1, p0, Ljyt;->eHn:I

    .line 197
    iget v0, p0, Ljyt;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljyt;->aez:I

    .line 198
    return-object p0
.end method

.method public final xU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Ljyt;->dRy:Ljava/lang/String;

    return-object v0
.end method

.method public final zU(Ljava/lang/String;)Ljyt;
    .locals 1

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Ljyt;->eMu:Ljava/lang/String;

    .line 71
    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyt;->aez:I

    .line 72
    return-object p0
.end method

.method public final zV(Ljava/lang/String;)Ljyt;
    .locals 1

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 92
    :cond_0
    iput-object p1, p0, Ljyt;->eMv:Ljava/lang/String;

    .line 93
    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyt;->aez:I

    .line 94
    return-object p0
.end method

.method public final zW(Ljava/lang/String;)Ljyt;
    .locals 1

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 114
    :cond_0
    iput-object p1, p0, Ljyt;->dRy:Ljava/lang/String;

    .line 115
    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljyt;->aez:I

    .line 116
    return-object p0
.end method

.method public final zX(Ljava/lang/String;)Ljyt;
    .locals 1

    .prologue
    .line 133
    if-nez p1, :cond_0

    .line 134
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 136
    :cond_0
    iput-object p1, p0, Ljyt;->eHj:Ljava/lang/String;

    .line 137
    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljyt;->aez:I

    .line 138
    return-object p0
.end method

.method public final zY(Ljava/lang/String;)Ljyt;
    .locals 1

    .prologue
    .line 155
    if-nez p1, :cond_0

    .line 156
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 158
    :cond_0
    iput-object p1, p0, Ljyt;->eHl:Ljava/lang/String;

    .line 159
    iget v0, p0, Ljyt;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljyt;->aez:I

    .line 160
    return-object p0
.end method
