.class public final Ljgk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ejZ:I

.field private eka:I

.field private ekb:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51779
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 51780
    iput v0, p0, Ljgk;->aez:I

    iput v0, p0, Ljgk;->ejZ:I

    iput v0, p0, Ljgk;->eka:I

    iput v0, p0, Ljgk;->ekb:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljgk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgk;->eCz:I

    .line 51781
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 51703
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgk;->ejZ:I

    iget v0, p0, Ljgk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgk;->eka:I

    iget v0, p0, Ljgk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgk;->ekb:I

    iget v0, p0, Ljgk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 51796
    iget v0, p0, Ljgk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 51797
    const/4 v0, 0x1

    iget v1, p0, Ljgk;->ejZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 51799
    :cond_0
    iget v0, p0, Ljgk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 51800
    const/4 v0, 0x2

    iget v1, p0, Ljgk;->eka:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 51802
    :cond_1
    iget v0, p0, Ljgk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 51803
    const/4 v0, 0x3

    iget v1, p0, Ljgk;->ekb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 51805
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 51806
    return-void
.end method

.method public final bkl()I
    .locals 1

    .prologue
    .line 51725
    iget v0, p0, Ljgk;->ejZ:I

    return v0
.end method

.method public final bkm()I
    .locals 1

    .prologue
    .line 51763
    iget v0, p0, Ljgk;->ekb:I

    return v0
.end method

.method public final getHits()I
    .locals 1

    .prologue
    .line 51744
    iget v0, p0, Ljgk;->eka:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 51810
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 51811
    iget v1, p0, Ljgk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 51812
    const/4 v1, 0x1

    iget v2, p0, Ljgk;->ejZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51815
    :cond_0
    iget v1, p0, Ljgk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 51816
    const/4 v1, 0x2

    iget v2, p0, Ljgk;->eka:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51819
    :cond_1
    iget v1, p0, Ljgk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 51820
    const/4 v1, 0x3

    iget v2, p0, Ljgk;->ekb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51823
    :cond_2
    return v0
.end method

.method public final pA(I)Ljgk;
    .locals 1

    .prologue
    .line 51766
    iput p1, p0, Ljgk;->ekb:I

    .line 51767
    iget v0, p0, Ljgk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgk;->aez:I

    .line 51768
    return-object p0
.end method

.method public final py(I)Ljgk;
    .locals 1

    .prologue
    .line 51728
    iput p1, p0, Ljgk;->ejZ:I

    .line 51729
    iget v0, p0, Ljgk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgk;->aez:I

    .line 51730
    return-object p0
.end method

.method public final pz(I)Ljgk;
    .locals 1

    .prologue
    .line 51747
    iput p1, p0, Ljgk;->eka:I

    .line 51748
    iget v0, p0, Ljgk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgk;->aez:I

    .line 51749
    return-object p0
.end method
