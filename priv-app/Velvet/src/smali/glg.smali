.class public final Lglg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final COLS:[Ljava/lang/String;


# instance fields
.field private final mConfig:Lcjs;

.field private mContactRetriever:Lghd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "times_contacted"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "last_time_contacted"

    aput-object v2, v0, v1

    sput-object v0, Lglg;->COLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Lcjs;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lghd;

    invoke-direct {v0, p1}, Lghd;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lglg;->mContactRetriever:Lghd;

    .line 56
    iput-object p2, p0, Lglg;->mConfig:Lcjs;

    .line 57
    return-void
.end method


# virtual methods
.method public final aHe()Ljava/util/List;
    .locals 5

    .prologue
    .line 60
    new-instance v0, Lglh;

    invoke-direct {v0}, Lglh;-><init>()V

    .line 61
    iget-object v1, p0, Lglg;->mContactRetriever:Lghd;

    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lglg;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->Mg()I

    move-result v3

    sget-object v4, Lglg;->COLS:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lghd;->a(Landroid/net/Uri;I[Ljava/lang/String;Lhgm;)V

    .line 64
    iget-object v0, v0, Lglh;->bxW:Ljava/util/List;

    return-object v0
.end method
