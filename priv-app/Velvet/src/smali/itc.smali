.class public final Litc;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dHt:[Litc;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 806
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 807
    const/4 v0, 0x0

    iput v0, p0, Litc;->aez:I

    const-string v0, ""

    iput-object v0, p0, Litc;->dHr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Litc;->akf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Litc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litc;->eCz:I

    .line 808
    return-void
.end method

.method public static aYD()[Litc;
    .locals 2

    .prologue
    .line 749
    sget-object v0, Litc;->dHt:[Litc;

    if-nez v0, :cond_1

    .line 750
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 752
    :try_start_0
    sget-object v0, Litc;->dHt:[Litc;

    if-nez v0, :cond_0

    .line 753
    const/4 v0, 0x0

    new-array v0, v0, [Litc;

    sput-object v0, Litc;->dHt:[Litc;

    .line 755
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 757
    :cond_1
    sget-object v0, Litc;->dHt:[Litc;

    return-object v0

    .line 755
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 743
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Litc;->dHr:Ljava/lang/String;

    iget v0, p0, Litc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Litc;->akf:Ljava/lang/String;

    iget v0, p0, Litc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 822
    iget v0, p0, Litc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 823
    const/4 v0, 0x1

    iget-object v1, p0, Litc;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 825
    :cond_0
    iget v0, p0, Litc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 826
    const/4 v0, 0x2

    iget-object v1, p0, Litc;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 828
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 829
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 833
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 834
    iget v1, p0, Litc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 835
    const/4 v1, 0x1

    iget-object v2, p0, Litc;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 838
    :cond_0
    iget v1, p0, Litc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 839
    const/4 v1, 0x2

    iget-object v2, p0, Litc;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 842
    :cond_1
    return v0
.end method

.method public final pD(Ljava/lang/String;)Litc;
    .locals 1

    .prologue
    .line 768
    if-nez p1, :cond_0

    .line 769
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 771
    :cond_0
    iput-object p1, p0, Litc;->dHr:Ljava/lang/String;

    .line 772
    iget v0, p0, Litc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litc;->aez:I

    .line 773
    return-object p0
.end method

.method public final pE(Ljava/lang/String;)Litc;
    .locals 1

    .prologue
    .line 790
    if-nez p1, :cond_0

    .line 791
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 793
    :cond_0
    iput-object p1, p0, Litc;->akf:Ljava/lang/String;

    .line 794
    iget v0, p0, Litc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litc;->aez:I

    .line 795
    return-object p0
.end method
