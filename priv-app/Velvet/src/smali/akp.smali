.class final Lakp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lakb;
.implements Lakc;
.implements Laly;


# instance fields
.field private final abG:Landroid/content/Context;

.field private abH:Lakd;

.field private final abI:Lakf;

.field private abK:Z

.field volatile abR:J

.field volatile abS:Lakt;

.field private volatile abT:Lajy;

.field private abU:Lakd;

.field final abV:Ljava/util/Queue;

.field private volatile abW:I

.field private volatile abX:Ljava/util/Timer;

.field private volatile abY:Ljava/util/Timer;

.field volatile abZ:Ljava/util/Timer;

.field private aca:Z

.field acb:Lakh;

.field acc:J


# direct methods
.method constructor <init>(Landroid/content/Context;Lakf;)V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lakp;-><init>(Landroid/content/Context;Lakf;Lakd;)V

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lakf;Lakd;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lakp;->abV:Ljava/util/Queue;

    .line 64
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lakp;->acc:J

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lakp;->abU:Lakd;

    .line 69
    iput-object p1, p0, Lakp;->abG:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lakp;->abI:Lakf;

    .line 71
    new-instance v0, Lakq;

    invoke-direct {v0, p0}, Lakq;-><init>(Lakp;)V

    iput-object v0, p0, Lakp;->acb:Lakh;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lakp;->abW:I

    .line 78
    sget-object v0, Lakt;->acl:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 79
    return-void
.end method

.method private static a(Ljava/util/Timer;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 141
    if-eqz p0, :cond_0

    .line 142
    invoke-virtual {p0}, Ljava/util/Timer;->cancel()V

    .line 144
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private mB()V
    .locals 4

    .prologue
    .line 339
    iget-object v0, p0, Lakp;->abX:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lakp;->abX:Ljava/util/Timer;

    .line 340
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Service Reconnect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lakp;->abX:Ljava/util/Timer;

    .line 341
    iget-object v0, p0, Lakp;->abX:Ljava/util/Timer;

    new-instance v1, Lakx;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lakx;-><init>(Lakp;B)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 342
    return-void
.end method

.method private mu()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lakp;->abX:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lakp;->abX:Ljava/util/Timer;

    .line 149
    iget-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    .line 150
    iget-object v0, p0, Lakp;->abZ:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lakp;->abZ:Ljava/util/Timer;

    .line 151
    return-void
.end method

.method private mx()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lakp;->abH:Lakd;

    invoke-interface {v0}, Lakd;->mp()V

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lakp;->abK:Z

    .line 247
    return-void
.end method


# virtual methods
.method public final b(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    .locals 8

    .prologue
    .line 95
    const-string v0, "putHit called"

    .line 97
    iget-object v6, p0, Lakp;->abV:Ljava/util/Queue;

    new-instance v0, Lakw;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lakw;-><init>(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-virtual {p0}, Lakp;->mw()V

    .line 99
    return-void
.end method

.method public final declared-synchronized bY(I)V
    .locals 2

    .prologue
    .line 328
    monitor-enter p0

    :try_start_0
    sget-object v0, Lakt;->acj:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 329
    iget v0, p0, Lakp;->abW:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service unavailable (code="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), will retry."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalm;->F(Ljava/lang/String;)I

    .line 331
    invoke-direct {p0}, Lakp;->mB()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    :goto_0
    monitor-exit p0

    return-void

    .line 333
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service unavailable (code="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), using local store."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalm;->F(Ljava/lang/String;)I

    .line 334
    invoke-virtual {p0}, Lakp;->my()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized mA()V
    .locals 2

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakp;->abT:Lajy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lakp;->abS:Lakt;

    sget-object v1, Lakt;->acg:Lakt;

    if-ne v0, v1, :cond_0

    .line 292
    sget-object v0, Lakt;->ack:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 293
    iget-object v0, p0, Lakp;->abT:Lajy;

    invoke-interface {v0}, Lajy;->disconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :cond_0
    monitor-exit p0

    return-void

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final mp()V
    .locals 2

    .prologue
    .line 103
    sget-object v0, Laks;->ace:[I

    iget-object v1, p0, Lakp;->abS:Lakt;

    invoke-virtual {v1}, Lakt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakp;->abK:Z

    .line 113
    :goto_0
    :pswitch_0
    return-void

    .line 105
    :pswitch_1
    invoke-direct {p0}, Lakp;->mx()V

    goto :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final mv()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lakp;->abT:Lajy;

    if-eqz v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    new-instance v0, Lajz;

    iget-object v1, p0, Lakp;->abG:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Lajz;-><init>(Landroid/content/Context;Lakb;Lakc;)V

    iput-object v0, p0, Lakp;->abT:Lajy;

    .line 163
    invoke-virtual {p0}, Lakp;->mz()V

    goto :goto_0
.end method

.method declared-synchronized mw()V
    .locals 8

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lakp;->abI:Lakf;

    invoke-interface {v3}, Lakf;->getThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 195
    iget-object v2, p0, Lakp;->abI:Lakf;

    invoke-interface {v2}, Lakf;->getQueue()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v2

    new-instance v3, Lakr;

    invoke-direct {v3, p0}, Lakr;-><init>(Lakp;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 203
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lakp;->aca:Z

    if-eqz v2, :cond_2

    .line 204
    const-string v2, "clearHits called"

    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->clear()V

    sget-object v2, Laks;->ace:[I

    iget-object v3, p0, Lakp;->abS:Lakt;

    invoke-virtual {v3}, Lakt;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lakp;->aca:Z

    .line 206
    :cond_2
    :goto_1
    sget-object v2, Laks;->ace:[I

    iget-object v3, p0, Lakp;->abS:Lakt;

    invoke-virtual {v3}, Lakt;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 208
    :goto_2
    :pswitch_0
    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 209
    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lakw;

    move-object v7, v0

    .line 210
    const-string v2, "Sending hit to store"

    .line 211
    iget-object v2, p0, Lakp;->abH:Lakd;

    iget-object v3, v7, Lakw;->acn:Ljava/util/Map;

    iget-wide v4, v7, Lakw;->aco:J

    iget-object v6, v7, Lakw;->acp:Ljava/lang/String;

    iget-object v7, v7, Lakw;->acq:Ljava/util/List;

    invoke-interface/range {v2 .. v7}, Lakd;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 194
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 204
    :pswitch_1
    :try_start_2
    iget-object v2, p0, Lakp;->abH:Lakd;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v4, v5}, Lakd;->n(J)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lakp;->aca:Z

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lakp;->abT:Lajy;

    invoke-interface {v2}, Lajy;->mn()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lakp;->aca:Z

    goto :goto_1

    .line 215
    :cond_3
    iget-boolean v2, p0, Lakp;->abK:Z

    if-eqz v2, :cond_0

    .line 216
    invoke-direct {p0}, Lakp;->mx()V

    goto :goto_0

    .line 220
    :goto_3
    :pswitch_3
    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 221
    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lakw;

    move-object v7, v0

    .line 222
    const-string v2, "Sending hit to service"

    .line 223
    iget-object v2, p0, Lakp;->abT:Lajy;

    iget-object v3, v7, Lakw;->acn:Ljava/util/Map;

    iget-wide v4, v7, Lakw;->aco:J

    iget-object v6, v7, Lakw;->acp:Ljava/lang/String;

    iget-object v7, v7, Lakw;->acq:Ljava/util/List;

    invoke-interface/range {v2 .. v7}, Lajy;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    .line 225
    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_3

    .line 227
    :cond_4
    iget-object v2, p0, Lakp;->acb:Lakh;

    invoke-interface {v2}, Lakh;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lakp;->abR:J

    goto/16 :goto_0

    .line 230
    :pswitch_4
    const-string v2, "Need to reconnect"

    .line 231
    iget-object v2, p0, Lakp;->abV:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 232
    invoke-virtual {p0}, Lakp;->mz()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 206
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method declared-synchronized my()V
    .locals 3

    .prologue
    .line 253
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakp;->abS:Lakt;

    sget-object v1, Lakt;->ach:Lakt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 268
    :goto_0
    monitor-exit p0

    return-void

    .line 257
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lakp;->mu()V

    .line 258
    const-string v0, "falling back to local store"

    .line 259
    iget-object v0, p0, Lakp;->abU:Lakd;

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lakp;->abU:Lakd;

    iput-object v0, p0, Lakp;->abH:Lakd;

    .line 266
    :goto_1
    sget-object v0, Lakt;->ach:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 267
    invoke-virtual {p0}, Lakp;->mw()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 262
    :cond_1
    :try_start_2
    invoke-static {}, Lakm;->mr()Lakm;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lakp;->abG:Landroid/content/Context;

    iget-object v2, p0, Lakp;->abI:Lakf;

    invoke-virtual {v0, v1, v2}, Lakm;->a(Landroid/content/Context;Lakf;)V

    .line 264
    invoke-virtual {v0}, Lakm;->ms()Lakd;

    move-result-object v0

    iput-object v0, p0, Lakp;->abH:Lakd;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method declared-synchronized mz()V
    .locals 4

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakp;->abT:Lajy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lakp;->abS:Lakt;

    sget-object v1, Lakt;->ach:Lakt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 273
    :try_start_1
    iget v0, p0, Lakp;->abW:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lakp;->abW:I

    .line 274
    iget-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    .line 275
    sget-object v0, Lakt;->acf:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 276
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Failed Connect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    .line 277
    iget-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    new-instance v1, Lakv;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lakv;-><init>(Lakp;B)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 278
    const-string v0, "connecting to Analytics service"

    .line 279
    iget-object v0, p0, Lakp;->abT:Lajy;

    invoke-interface {v0}, Lajy;->connect()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :goto_0
    monitor-exit p0

    return-void

    .line 281
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "security exception on connectToService"

    invoke-static {v0}, Lalm;->F(Ljava/lang/String;)I

    .line 282
    invoke-virtual {p0}, Lakp;->my()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 285
    :cond_0
    :try_start_3
    const-string v0, "client not initialized."

    invoke-static {v0}, Lalm;->F(Ljava/lang/String;)I

    .line 286
    invoke-virtual {p0}, Lakp;->my()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized onConnected()V
    .locals 4

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lakp;->abY:Ljava/util/Timer;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lakp;->abW:I

    .line 301
    const-string v0, "Connected to service"

    .line 302
    sget-object v0, Lakt;->acg:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 303
    invoke-virtual {p0}, Lakp;->mw()V

    .line 304
    iget-object v0, p0, Lakp;->abZ:Ljava/util/Timer;

    invoke-static {v0}, Lakp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lakp;->abZ:Ljava/util/Timer;

    .line 305
    new-instance v0, Ljava/util/Timer;

    const-string v1, "disconnect check"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lakp;->abZ:Ljava/util/Timer;

    .line 306
    iget-object v0, p0, Lakp;->abZ:Ljava/util/Timer;

    new-instance v1, Laku;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Laku;-><init>(Lakp;B)V

    iget-wide v2, p0, Lakp;->acc:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized onDisconnected()V
    .locals 2

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakp;->abS:Lakt;

    sget-object v1, Lakt;->ack:Lakt;

    if-ne v0, v1, :cond_0

    .line 312
    const-string v0, "Disconnected from service"

    .line 313
    invoke-direct {p0}, Lakp;->mu()V

    .line 314
    sget-object v0, Lakt;->acl:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :goto_0
    monitor-exit p0

    return-void

    .line 316
    :cond_0
    :try_start_1
    const-string v0, "Unexpected disconnect."

    .line 317
    sget-object v0, Lakt;->acj:Lakt;

    iput-object v0, p0, Lakp;->abS:Lakt;

    .line 318
    iget v0, p0, Lakp;->abW:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 319
    invoke-direct {p0}, Lakp;->mB()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 321
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lakp;->my()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
