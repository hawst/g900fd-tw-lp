.class public final Ladp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static KQ:Z

.field private static Tp:I

.field private static Tq:I

.field private static Tr:I

.field private static Ts:I

.field private static final Tt:Landroid/graphics/Rect;

.field private static final Tu:Landroid/graphics/Canvas;

.field private static Tv:[I

.field private static Tw:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v3, 0x2

    .line 55
    sput v0, Ladp;->Tp:I

    .line 56
    sput v0, Ladp;->Tq:I

    .line 57
    sput v0, Ladp;->Tr:I

    .line 58
    sput v0, Ladp;->Ts:I

    .line 60
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Ladp;->Tt:Landroid/graphics/Rect;

    .line 61
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    .line 64
    sput-object v0, Ladp;->Tu:Landroid/graphics/Canvas;

    new-instance v1, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v3}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/high16 v2, -0x10000

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, -0xff0100

    aput v2, v0, v1

    const v1, -0xffff01

    aput v1, v0, v3

    .line 68
    new-array v0, v3, [I

    sput-object v0, Ladp;->Tv:[I

    .line 71
    new-array v0, v3, [I

    sput-object v0, Ladp;->Tw:[I

    .line 76
    const-string v0, "launcher_force_rotate"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Ladp;->KQ:Z

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;[I)F
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 284
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 286
    const/4 v0, 0x2

    new-array v5, v0, [F

    aget v0, p2, v9

    int-to-float v0, v0

    aput v0, v5, v9

    aget v0, p2, v10

    int-to-float v0, v0

    aput v0, v5, v10

    .line 289
    :goto_0
    if-eq p0, p1, :cond_0

    .line 290
    invoke-virtual {v4, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    goto :goto_0

    .line 293
    :cond_0
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    const/high16 v2, 0x3f800000    # 1.0f

    .line 296
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 297
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 298
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_2

    .line 299
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 300
    if-lez v3, :cond_1

    add-int/lit8 v1, v3, -0x1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 302
    :goto_2
    aget v7, v5, v9

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v9

    .line 303
    aget v7, v5, v10

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v7

    aput v0, v5, v10

    .line 305
    if-eqz v1, :cond_3

    .line 306
    aget v0, v5, v9

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v0, v7

    aput v0, v5, v9

    .line 307
    aget v0, v5, v10

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v0, v7

    aput v0, v5, v10

    .line 308
    invoke-virtual {v1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 309
    invoke-virtual {v6, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 310
    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    move-result v0

    mul-float/2addr v0, v2

    .line 298
    :goto_3
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 300
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 314
    :cond_2
    aget v0, v5, v9

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, p2, v9

    .line 315
    aget v0, v5, v10

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, p2, v10

    .line 316
    return v2

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method public static a(Landroid/view/View;Landroid/view/View;[IZ)F
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 246
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 248
    const/4 v0, 0x2

    new-array v5, v0, [F

    aget v0, p2, v2

    int-to-float v0, v0

    aput v0, v5, v2

    aget v0, p2, v9

    int-to-float v0, v0

    aput v0, v5, v9

    move-object v0, p0

    .line 251
    :goto_0
    if-eq v0, p1, :cond_0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 255
    :cond_0
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    const/high16 v0, 0x3f800000    # 1.0f

    .line 258
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    move v3, v0

    .line 259
    :goto_1
    if-ge v1, v6, :cond_3

    .line 260
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 263
    if-ne v0, p0, :cond_1

    if-eqz p3, :cond_2

    .line 264
    :cond_1
    aget v7, v5, v2

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v2

    .line 265
    aget v7, v5, v9

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    aput v7, v5, v9

    .line 268
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 269
    aget v7, v5, v2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v2

    .line 270
    aget v7, v5, v9

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    aput v7, v5, v9

    .line 271
    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    mul-float/2addr v3, v0

    .line 259
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 274
    :cond_3
    aget v0, v5, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, p2, v2

    .line 275
    aget v0, v5, v9

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, p2, v9

    .line 276
    return v3
.end method

.method static a(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 118
    sget v0, Ladp;->Tr:I

    .line 119
    sget v1, Ladp;->Ts:I

    .line 120
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 121
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 122
    if-le v2, v0, :cond_1

    if-le v3, v1, :cond_1

    .line 124
    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    invoke-static {p0, v2, v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 134
    :cond_0
    :goto_0
    return-object p0

    .line 128
    :cond_1
    if-ne v2, v0, :cond_2

    if-eq v3, v1, :cond_0

    .line 133
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 134
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0, p0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-static {v1, p1}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    .line 142
    sget-object v4, Ladp;->Tu:Landroid/graphics/Canvas;

    monitor-enter v4

    .line 143
    :try_start_0
    sget v1, Ladp;->Tp:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 144
    invoke-static {p1}, Ladp;->z(Landroid/content/Context;)V

    .line 147
    :cond_0
    sget v3, Ladp;->Tp:I

    .line 148
    sget v2, Ladp;->Tq:I

    .line 150
    instance-of v1, p0, Landroid/graphics/drawable/PaintDrawable;

    if-eqz v1, :cond_2

    .line 151
    move-object v0, p0

    check-cast v0, Landroid/graphics/drawable/PaintDrawable;

    move-object v1, v0

    .line 152
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/PaintDrawable;->setIntrinsicWidth(I)V

    .line 153
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/PaintDrawable;->setIntrinsicHeight(I)V

    .line 162
    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 163
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 164
    if-lez v1, :cond_4

    if-lez v5, :cond_4

    .line 166
    int-to-float v6, v1

    int-to-float v7, v5

    div-float/2addr v6, v7

    .line 167
    if-le v1, v5, :cond_3

    .line 168
    int-to-float v1, v3

    div-float/2addr v1, v6

    float-to-int v1, v1

    move v2, v3

    .line 175
    :goto_1
    sget v3, Ladp;->Tr:I

    .line 176
    sget v5, Ladp;->Ts:I

    .line 178
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 180
    sget-object v7, Ladp;->Tu:Landroid/graphics/Canvas;

    .line 181
    invoke-virtual {v7, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 183
    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    .line 184
    sub-int/2addr v5, v1

    div-int/lit8 v5, v5, 0x2

    .line 187
    sget-object v8, Ladp;->Tt:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 198
    add-int/2addr v2, v3

    add-int/2addr v1, v5

    invoke-virtual {p0, v3, v5, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 199
    invoke-virtual {p0, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 200
    sget-object v1, Ladp;->Tt:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 201
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 203
    monitor-exit v4

    return-object v6

    .line 154
    :cond_2
    instance-of v1, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 156
    move-object v0, p0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v1, v0

    .line 157
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 158
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v5

    if-nez v5, :cond_1

    .line 159
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(Landroid/util/DisplayMetrics;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v1

    monitor-exit v4

    throw v1

    .line 169
    :cond_3
    if-le v5, v1, :cond_4

    .line 170
    int-to-float v1, v2

    mul-float/2addr v1, v6

    float-to-int v1, v1

    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_1

    :cond_4
    move v1, v2

    move v2, v3

    goto :goto_1
.end method

.method static a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Landroid/util/Pair;
    .locals 5

    .prologue
    .line 501
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 502
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 503
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_0

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 505
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 507
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 508
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 514
    :goto_1
    return-object v0

    .line 510
    :catch_0
    move-exception v2

    const-string v2, "Launcher.Utilities"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to find resources for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 514
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;I)V
    .locals 4

    .prologue
    const v2, 0x7f0a0085

    const/4 v1, 0x0

    .line 380
    :try_start_0
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 389
    :goto_0
    return-void

    .line 382
    :catch_0
    move-exception v0

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 383
    :catch_1
    move-exception v0

    .line 384
    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 385
    const-string v1, "Launcher.Utilities"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Launcher does not have the permission to launch "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Make sure to create a MAIN intent-filter for the corresponding activity or use the exported attribute for this activity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Rect;F)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 370
    invoke-virtual {p0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    .line 371
    invoke-virtual {p0}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    .line 372
    neg-int v2, v0

    neg-int v3, v1

    invoke-virtual {p0, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 373
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, p1, v2

    if-eqz v2, :cond_0

    iget v2, p0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->bottom:I

    .line 374
    :cond_0
    invoke-virtual {p0, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 375
    return-void
.end method

.method public static a(Landroid/view/View;FFF)Z
    .locals 1

    .prologue
    .line 326
    neg-float v0, p3

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    neg-float v0, p3

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 217
    sget-object v1, Ladp;->Tu:Landroid/graphics/Canvas;

    monitor-enter v1

    .line 218
    :try_start_0
    sget v0, Ladp;->Tp:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 219
    invoke-static {p1}, Ladp;->z(Landroid/content/Context;)V

    .line 222
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v2, Ladp;->Tp:I

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget v2, Ladp;->Tq:I

    if-ne v0, v2, :cond_1

    .line 223
    monitor-exit v1

    .line 226
    :goto_0
    return-object p0

    .line 225
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 226
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v0, p0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-static {v2, p1}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object p0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 392
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 393
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    .line 394
    const/4 v0, 0x0

    .line 395
    if-nez v3, :cond_1

    .line 396
    const/high16 v3, 0x10000

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 397
    if-eqz v3, :cond_0

    iget-object v4, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v4, :cond_0

    .line 398
    iget-object v0, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 403
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 405
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 406
    if-eqz v0, :cond_2

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_2

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 412
    :goto_1
    return v0

    .line 401
    :cond_1
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 406
    goto :goto_1

    .line 409
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 412
    goto :goto_1
.end method

.method public static b(Landroid/view/View;Landroid/view/View;[I)[I
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 351
    sget-object v0, Ladp;->Tv:[I

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 352
    sget-object v0, Ladp;->Tw:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 354
    sget-object v0, Ladp;->Tv:[I

    aget v1, v0, v4

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v6

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v4

    .line 355
    sget-object v0, Ladp;->Tv:[I

    aget v1, v0, v5

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getScaleY()F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v6

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v5

    .line 356
    sget-object v0, Ladp;->Tw:[I

    aget v1, v0, v4

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v6

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v4

    .line 357
    sget-object v0, Ladp;->Tw:[I

    aget v1, v0, v5

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v2, v6

    add-float/2addr v1, v2

    float-to-int v1, v1

    aput v1, v0, v5

    .line 359
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 363
    sget-object v1, Ladp;->Tw:[I

    aget v1, v1, v4

    sget-object v2, Ladp;->Tv:[I

    aget v2, v2, v4

    sub-int/2addr v1, v2

    aput v1, v0, v4

    .line 364
    sget-object v1, Ladp;->Tw:[I

    aget v1, v1, v5

    sget-object v2, Ladp;->Tv:[I

    aget v2, v2, v5

    sub-int/2addr v1, v2

    aput v1, v0, v5

    .line 366
    return-object v0
.end method

.method public static bK(I)V
    .locals 0

    .prologue
    .line 337
    sput p0, Ladp;->Tq:I

    sput p0, Ladp;->Tp:I

    .line 338
    sput p0, Ladp;->Ts:I

    sput p0, Ladp;->Tr:I

    .line 339
    return-void
.end method

.method public static c(Landroid/graphics/Bitmap;I)I
    .locals 15

    .prologue
    .line 422
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    .line 423
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 424
    mul-int v0, v9, v10

    div-int/lit8 v0, v0, 0x14

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 425
    if-gtz v0, :cond_7

    .line 426
    const/4 v0, 0x1

    move v1, v0

    .line 430
    :goto_0
    const/4 v0, 0x3

    new-array v11, v0, [F

    .line 434
    const/16 v0, 0x168

    new-array v6, v0, [F

    .line 435
    const/high16 v3, -0x40800000    # -1.0f

    .line 436
    const/4 v8, -0x1

    .line 438
    const/4 v0, 0x0

    move v5, v0

    :goto_1
    if-ge v5, v9, :cond_1

    .line 439
    const/4 v0, 0x0

    move v4, v0

    move v2, v8

    :goto_2
    if-ge v4, v10, :cond_0

    .line 440
    invoke-virtual {p0, v4, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 441
    shr-int/lit8 v7, v0, 0x18

    and-int/lit16 v7, v7, 0xff

    .line 442
    const/16 v8, 0x80

    if-lt v7, v8, :cond_6

    .line 444
    const/high16 v7, -0x1000000

    or-int/2addr v0, v7

    .line 448
    invoke-static {v0, v11}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 450
    const/4 v0, 0x0

    aget v0, v11, v0

    float-to-int v0, v0

    .line 451
    if-ltz v0, :cond_6

    array-length v7, v6

    if-ge v0, v7, :cond_6

    .line 453
    const/4 v7, 0x1

    aget v7, v11, v7

    const/4 v8, 0x2

    aget v8, v11, v8

    mul-float/2addr v7, v8

    .line 456
    aget v8, v6, v0

    add-float/2addr v7, v8

    aput v7, v6, v0

    .line 457
    aget v7, v6, v0

    cmpl-float v7, v7, v3

    if-lez v7, :cond_6

    .line 458
    aget v2, v6, v0

    .line 439
    :goto_3
    add-int v3, v4, v1

    move v4, v3

    move v3, v2

    move v2, v0

    goto :goto_2

    .line 438
    :cond_0
    add-int v0, v5, v1

    move v5, v0

    move v8, v2

    goto :goto_1

    .line 464
    :cond_1
    new-instance v12, Landroid/util/SparseArray;

    invoke-direct {v12}, Landroid/util/SparseArray;-><init>()V

    .line 465
    const/high16 v4, -0x1000000

    .line 466
    const/high16 v5, -0x40800000    # -1.0f

    .line 470
    const/4 v0, 0x0

    move v6, v0

    :goto_4
    if-ge v6, v9, :cond_4

    .line 471
    const/4 v0, 0x0

    move v7, v0

    :goto_5
    if-ge v7, v10, :cond_3

    .line 472
    invoke-virtual {p0, v7, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    const/high16 v2, -0x1000000

    or-int v3, v0, v2

    .line 473
    invoke-static {v3, v11}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 474
    const/4 v0, 0x0

    aget v0, v11, v0

    float-to-int v0, v0

    .line 475
    if-ne v0, v8, :cond_5

    .line 476
    const/4 v0, 0x1

    aget v0, v11, v0

    .line 477
    const/4 v2, 0x2

    aget v2, v11, v2

    .line 478
    const/high16 v13, 0x42c80000    # 100.0f

    mul-float/2addr v13, v0

    float-to-int v13, v13

    const v14, 0x461c4000    # 10000.0f

    mul-float/2addr v14, v2

    float-to-int v14, v14

    add-int/2addr v13, v14

    .line 480
    mul-float/2addr v2, v0

    .line 481
    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 482
    if-nez v0, :cond_2

    move v0, v2

    .line 483
    :goto_6
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v12, v13, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 484
    cmpl-float v2, v0, v5

    if-lez v2, :cond_5

    move v2, v0

    move v0, v3

    .line 471
    :goto_7
    add-int v3, v7, v1

    move v7, v3

    move v4, v0

    move v5, v2

    goto :goto_5

    .line 482
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v2

    goto :goto_6

    .line 470
    :cond_3
    add-int v0, v6, v1

    move v6, v0

    goto :goto_4

    .line 492
    :cond_4
    return v4

    :cond_5
    move v0, v4

    move v2, v5

    goto :goto_7

    :cond_6
    move v0, v2

    move v2, v3

    goto :goto_3

    :cond_7
    move v1, v0

    goto/16 :goto_0
.end method

.method public static c(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    sget v0, Ladp;->Tr:I

    sget v1, Ladp;->Ts:I

    invoke-virtual {p0, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 93
    return-void
.end method

.method public static i(Landroid/graphics/Bitmap;)Lus;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lus;

    invoke-direct {v0, p0}, Lus;-><init>(Landroid/graphics/Bitmap;)V

    .line 83
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lus;->setFilterBitmap(Z)V

    .line 84
    invoke-static {v0}, Ladp;->c(Landroid/graphics/drawable/Drawable;)V

    .line 85
    return-object v0
.end method

.method public static km()Z
    .locals 2

    .prologue
    .line 109
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static y(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 100
    sget-boolean v0, Ladp;->KQ:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    .line 100
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static z(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 331
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 332
    const v1, 0x7f0d005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Ladp;->Tq:I

    .line 333
    sput v0, Ladp;->Tp:I

    sput v0, Ladp;->Ts:I

    sput v0, Ladp;->Tr:I

    .line 334
    return-void
.end method
