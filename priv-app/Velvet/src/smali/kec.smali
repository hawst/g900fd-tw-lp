.class final Lkec;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lkdd;


# instance fields
.field final synthetic eUh:Lkdx;


# direct methods
.method private constructor <init>(Lkdx;)V
    .locals 0

    .prologue
    .line 429
    iput-object p1, p0, Lkec;->eUh:Lkdx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lkdx;B)V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0, p1}, Lkec;-><init>(Lkdx;)V

    return-void
.end method


# virtual methods
.method public final a(ILkdb;)V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-virtual {v0, p1}, Lkdx;->tT(I)Lkee;

    move-result-object v0

    .line 518
    if-eqz v0, :cond_0

    .line 519
    invoke-virtual {v0, p2}, Lkee;->d(Lkdb;)V

    .line 521
    :cond_0
    return-void
.end method

.method public final a(ZILjava/io/InputStream;I)V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0, p2}, Lkdx;->a(Lkdx;I)Lkee;

    move-result-object v0

    .line 452
    if-nez v0, :cond_1

    .line 453
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    sget-object v1, Lkdb;->eSW:Lkdb;

    invoke-virtual {v0, p2, v1}, Lkdx;->c(ILkdb;)V

    .line 454
    int-to-long v0, p4

    invoke-static {p3, v0, v1}, Lkbt;->b(Ljava/io/InputStream;J)J

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    invoke-virtual {v0, p3, p4}, Lkee;->c(Ljava/io/InputStream;I)V

    .line 458
    if-eqz p1, :cond_0

    .line 459
    invoke-virtual {v0}, Lkee;->bzr()V

    goto :goto_0
.end method

.method public final a(ZLkdt;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 524
    const/4 v0, 0x0

    .line 525
    iget-object v3, p0, Lkec;->eUh:Lkdx;

    monitor-enter v3

    .line 526
    :try_start_0
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    iget-object v2, v2, Lkdx;->eUe:Lkdt;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_2

    .line 527
    :cond_0
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    iput-object p2, v2, Lkdx;->eUe:Lkdt;

    .line 531
    :cond_1
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    invoke-static {v2}, Lkdx;->e(Lkdx;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 532
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0}, Lkdx;->e(Lkdx;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lkec;->eUh:Lkdx;

    invoke-static {v2}, Lkdx;->e(Lkdx;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Lkee;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkee;

    move-object v2, v0

    .line 534
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    if-eqz v2, :cond_4

    .line 536
    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v1, v2, v0

    .line 541
    monitor-enter v1

    .line 542
    :try_start_1
    iget-object v4, p0, Lkec;->eUh:Lkdx;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 543
    :try_start_2
    iget-object v5, p0, Lkec;->eUh:Lkdx;

    iget-object v5, v5, Lkdx;->eUe:Lkdt;

    invoke-virtual {v1, v5}, Lkee;->c(Lkdt;)V

    .line 544
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 545
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 536
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 529
    :cond_2
    :try_start_4
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    iget-object v4, v2, Lkdx;->eUe:Lkdt;

    move v2, v1

    :goto_2
    const/16 v5, 0xa

    if-ge v2, v5, :cond_1

    invoke-virtual {p2, v2}, Lkdt;->isSet(I)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p2, v2}, Lkdt;->tR(I)I

    move-result v5

    iget-object v6, p2, Lkdt;->eTR:[I

    aget v6, v6, v2

    invoke-virtual {v4, v2, v5, v6}, Lkdt;->set(III)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 534
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 544
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v4

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 545
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 548
    :cond_4
    return-void

    :cond_5
    move-object v2, v0

    goto :goto_0
.end method

.method public final a(ZZIILjava/util/List;Lkdf;)V
    .locals 9

    .prologue
    .line 467
    iget-object v8, p0, Lkec;->eUh:Lkdx;

    monitor-enter v8

    .line 469
    :try_start_0
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0}, Lkdx;->b(Lkdx;)Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v8

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0, p3}, Lkdx;->a(Lkdx;I)Lkee;

    move-result-object v0

    .line 473
    if-nez v0, :cond_5

    .line 475
    invoke-virtual {p6}, Lkdf;->bzg()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 476
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    sget-object v1, Lkdb;->eSW:Lkdb;

    invoke-virtual {v0, p3, v1}, Lkdx;->c(ILkdb;)V

    .line 477
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 502
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 481
    :cond_2
    :try_start_1
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0}, Lkdx;->c(Lkdx;)I

    move-result v0

    if-gt p3, v0, :cond_3

    monitor-exit v8

    goto :goto_0

    .line 484
    :cond_3
    rem-int/lit8 v0, p3, 0x2

    iget-object v1, p0, Lkec;->eUh:Lkdx;

    invoke-static {v1}, Lkdx;->d(Lkdx;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_4

    monitor-exit v8

    goto :goto_0

    .line 487
    :cond_4
    new-instance v0, Lkee;

    iget-object v2, p0, Lkec;->eUh:Lkdx;

    iget-object v1, p0, Lkec;->eUh:Lkdx;

    iget-object v7, v1, Lkdx;->eUe:Lkdt;

    move v1, p3

    move v3, p1

    move v4, p2

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lkee;-><init>(ILkdx;ZZILjava/util/List;Lkdt;)V

    .line 489
    iget-object v1, p0, Lkec;->eUh:Lkdx;

    invoke-static {v1, p3}, Lkdx;->b(Lkdx;I)I

    .line 490
    iget-object v1, p0, Lkec;->eUh:Lkdx;

    invoke-static {v1}, Lkdx;->e(Lkdx;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    invoke-static {}, Lkdx;->bzo()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lked;

    const-string v3, "OkHttp Callback %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lkec;->eUh:Lkdx;

    invoke-static {v6}, Lkdx;->f(Lkdx;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v2, p0, v3, v4, v0}, Lked;-><init>(Lkec;Ljava/lang/String;[Ljava/lang/Object;Lkee;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 500
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 502
    :cond_5
    monitor-exit v8

    .line 505
    invoke-virtual {p6}, Lkdf;->bzh()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 506
    sget-object v1, Lkdb;->eSV:Lkdb;

    invoke-virtual {v0, v1}, Lkee;->b(Lkdb;)V

    .line 507
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-virtual {v0, p3}, Lkdx;->tT(I)Lkee;

    goto/16 :goto_0

    .line 512
    :cond_6
    invoke-virtual {v0, p5, p6}, Lkee;->a(Ljava/util/List;Lkdf;)V

    .line 513
    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lkee;->bzr()V

    goto/16 :goto_0
.end method

.method public final bA(II)V
    .locals 1

    .prologue
    .line 583
    if-nez p1, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0, p1}, Lkdx;->a(Lkdx;I)Lkee;

    move-result-object v0

    .line 590
    if-eqz v0, :cond_0

    .line 591
    invoke-virtual {v0, p2}, Lkee;->tV(I)V

    goto :goto_0
.end method

.method public final c(ZII)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 554
    if-eqz p1, :cond_3

    .line 555
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0, p2}, Lkdx;->c(Lkdx;I)Lkds;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_2

    .line 557
    iget-wide v2, v0, Lkds;->eTN:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, v0, Lkds;->eTM:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v0, Lkds;->eTN:J

    iget-object v0, v0, Lkds;->eTL:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 563
    :cond_2
    :goto_0
    return-void

    .line 561
    :cond_3
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, p3, v2}, Lkdx;->b(Lkdx;ZIILkds;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 5

    .prologue
    .line 431
    sget-object v0, Lkdb;->eTa:Lkdb;

    .line 432
    sget-object v2, Lkdb;->eTa:Lkdb;

    .line 434
    :cond_0
    :try_start_0
    iget-object v1, p0, Lkec;->eUh:Lkdx;

    invoke-static {v1}, Lkdx;->a(Lkdx;)Lkdc;

    move-result-object v1

    invoke-interface {v1, p0}, Lkdc;->a(Lkdd;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 436
    sget-object v0, Lkdb;->eSU:Lkdb;

    .line 437
    sget-object v1, Lkdb;->eTf:Lkdb;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443
    :try_start_1
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    invoke-static {v2, v0, v1}, Lkdx;->a(Lkdx;Lkdb;Lkdb;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 446
    :goto_0
    return-void

    .line 439
    :catch_0
    move-exception v1

    :try_start_2
    sget-object v1, Lkdb;->eSV:Lkdb;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 440
    :try_start_3
    sget-object v0, Lkdb;->eSV:Lkdb;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 443
    :try_start_4
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    invoke-static {v2, v1, v0}, Lkdx;->a(Lkdx;Lkdb;Lkdb;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 446
    :catch_1
    move-exception v0

    goto :goto_0

    .line 442
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 443
    :goto_1
    :try_start_5
    iget-object v3, p0, Lkec;->eUh:Lkdx;

    invoke-static {v3, v1, v2}, Lkdx;->a(Lkdx;Lkdb;Lkdb;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 445
    :goto_2
    throw v0

    :catch_2
    move-exception v1

    goto :goto_2

    .line 442
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 446
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public final tP(I)V
    .locals 4

    .prologue
    .line 566
    iget-object v2, p0, Lkec;->eUh:Lkdx;

    monitor-enter v2

    .line 567
    :try_start_0
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkdx;->a(Lkdx;Z)Z

    .line 570
    iget-object v0, p0, Lkec;->eUh:Lkdx;

    invoke-static {v0}, Lkdx;->e(Lkdx;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 571
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 573
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 574
    if-le v1, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkee;

    invoke-virtual {v1}, Lkee;->bzp()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 575
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkee;

    sget-object v1, Lkdb;->eTe:Lkdb;

    invoke-virtual {v0, v1}, Lkee;->d(Lkdb;)V

    .line 576
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 579
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
