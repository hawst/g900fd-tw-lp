.class public abstract enum Lirg;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum dGA:Lirg;

.field public static final enum dGB:Lirg;

.field public static final enum dGC:Lirg;

.field private static final synthetic dGD:[Lirg;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, Lirh;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, Lirh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirg;->dGA:Lirg;

    .line 154
    new-instance v0, Liri;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, Liri;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirg;->dGB:Lirg;

    .line 172
    new-instance v0, Lirj;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, Lirj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lirg;->dGC:Lirg;

    .line 139
    const/4 v0, 0x3

    new-array v0, v0, [Lirg;

    sget-object v1, Lirg;->dGA:Lirg;

    aput-object v1, v0, v2

    sget-object v1, Lirg;->dGB:Lirg;

    aput-object v1, v0, v3

    sget-object v1, Lirg;->dGC:Lirg;

    aput-object v1, v0, v4

    sput-object v0, Lirg;->dGD:[Lirg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lirg;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lirg;
    .locals 1

    .prologue
    .line 139
    const-class v0, Lirg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lirg;

    return-object v0
.end method

.method public static values()[Lirg;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lirg;->dGD:[Lirg;

    invoke-virtual {v0}, [Lirg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lirg;

    return-object v0
.end method


# virtual methods
.method abstract mn(I)I
.end method
