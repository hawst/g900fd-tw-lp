.class public final Lfgc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfga;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;

.field private final mEntryProvider:Lfaq;

.field private final mLocationOracle:Lfdr;

.field private final mNetworkClient:Lfcx;

.field private final mNotificationManager:Lfcz;

.field private final mNotificationStore:Lffp;

.field private final mPendingIntentFactory:Lfda;

.field private final mPrefController:Lchr;

.field private final mRenderingContextAdapterFactory:Lfjs;

.field private final mRenderingContextProviders:Lezg;

.field private final mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemp;Lfaq;Lchr;Lcpx;Lfcz;Lfcx;Lfda;Lfjs;Lfdr;Lezg;Lffp;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lfgc;->mClock:Lemp;

    .line 84
    iput-object p3, p0, Lfgc;->mEntryProvider:Lfaq;

    .line 85
    iput-object p4, p0, Lfgc;->mPrefController:Lchr;

    .line 86
    iput-object p5, p0, Lfgc;->mUserInteractionLogger:Lcpx;

    .line 87
    iput-object p6, p0, Lfgc;->mNotificationManager:Lfcz;

    .line 88
    iput-object p7, p0, Lfgc;->mNetworkClient:Lfcx;

    .line 89
    iput-object p8, p0, Lfgc;->mPendingIntentFactory:Lfda;

    .line 90
    iput-object p9, p0, Lfgc;->mRenderingContextAdapterFactory:Lfjs;

    .line 91
    iput-object p10, p0, Lfgc;->mLocationOracle:Lfdr;

    .line 92
    iput-object p11, p0, Lfgc;->mRenderingContextProviders:Lezg;

    .line 93
    iput-object p12, p0, Lfgc;->mNotificationStore:Lffp;

    .line 94
    return-void
.end method

.method public static a(Landroid/app/Notification;I)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 347
    new-instance v3, Landroid/content/Intent;

    const-string v0, "com.google.android.apps.now.cards_remote_broadcast"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 349
    const-string v0, "type"

    const/16 v4, 0x8

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 351
    const-string v0, "change_id"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 355
    iget v0, p0, Landroid/app/Notification;->defaults:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    .line 356
    :goto_0
    const-string v4, "notification_vibrate"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 359
    iget v0, p0, Landroid/app/Notification;->defaults:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 361
    :cond_1
    const-string v0, "notification_sound"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 364
    return-object v3

    :cond_2
    move v0, v2

    .line 355
    goto :goto_0
.end method


# virtual methods
.method public final a(Lfey;Landroid/app/PendingIntent;Z)Landroid/app/Notification;
    .locals 6
    .param p2    # Landroid/app/PendingIntent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-interface {p1}, Lfey;->ayW()I

    move-result v3

    .line 100
    const/4 v0, -0x1

    if-ne v3, v0, :cond_0

    move-object v0, v2

    .line 140
    :goto_0
    return-object v0

    .line 105
    :cond_0
    iget-object v0, p0, Lfgc;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->axg()Ljbp;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iget-object v4, p0, Lfgc;->mLocationOracle:Lfdr;

    invoke-interface {v4}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v4

    invoke-direct {v1, v4, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>(Landroid/location/Location;Landroid/location/Location;)V

    .line 111
    invoke-interface {p1}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    .line 112
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 113
    iget-object v5, p0, Lfgc;->mRenderingContextAdapterFactory:Lfjs;

    invoke-interface {v5, v0}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezi;

    .line 115
    if-eqz v0, :cond_1

    .line 116
    iget-object v5, p0, Lfgc;->mRenderingContextProviders:Lezg;

    invoke-interface {v0, v1, v5}, Lezi;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lezg;)V

    goto :goto_1

    .line 121
    :cond_2
    const/4 v0, 0x4

    if-ne v3, v0, :cond_4

    .line 123
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 124
    const/16 v3, 0x10

    if-ge v0, v3, :cond_3

    move-object v0, v2

    .line 125
    goto :goto_0

    .line 130
    :cond_3
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p1, v0, v1}, Lfey;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v2

    .line 132
    goto :goto_0

    .line 136
    :cond_4
    new-instance v2, Lay;

    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, v0}, Lay;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    .line 137
    invoke-virtual/range {v0 .. v5}, Lfgc;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lay;Lfey;Landroid/app/PendingIntent;Z)V

    .line 140
    invoke-static {}, Lat;->af()Lbb;

    move-result-object v0

    invoke-interface {v0, v2}, Lbb;->b(Lay;)Landroid/app/Notification;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/app/Notification;Lfey;)V
    .locals 3

    .prologue
    .line 324
    iget-object v0, p0, Lfgc;->mNotificationManager:Lfcz;

    invoke-interface {p2}, Lfey;->ayZ()Lfgb;

    move-result-object v1

    invoke-virtual {v1}, Lfgb;->azC()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lfcz;->notify(ILandroid/app/Notification;)V

    .line 329
    invoke-interface {p2}, Lfey;->ayQ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    invoke-interface {p2}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 331
    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v1

    invoke-virtual {v1}, Lgbg;->hashCode()I

    move-result v1

    .line 332
    iget-object v2, p0, Lfgc;->mNotificationStore:Lffp;

    invoke-virtual {v2, v0, v1}, Lffp;->g(Lizj;I)V

    .line 333
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lfgc;->a(Landroid/app/Notification;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p2, v0}, Lfey;->aH(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 337
    if-eqz v0, :cond_1

    invoke-interface {p2}, Lfey;->ayR()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 341
    iget-object v1, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 343
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lay;Lfey;Landroid/app/PendingIntent;Z)V
    .locals 8
    .param p4    # Landroid/app/PendingIntent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lay;->c(Z)Lay;

    .line 150
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lay;->d(Z)Lay;

    .line 151
    iget-object v0, p0, Lfgc;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lay;->a(J)Lay;

    .line 152
    invoke-interface {p3}, Lfey;->getVisibility()I

    move-result v0

    iput v0, p2, Lay;->cT:I

    .line 153
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p2, Lay;->cS:I

    .line 154
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v0}, Lfey;->aF(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p2, Lay;->cK:Landroid/app/PendingIntent;

    .line 155
    invoke-interface {p3}, Lfey;->ayY()I

    move-result v0

    invoke-virtual {p2, v0}, Lay;->g(I)Lay;

    .line 156
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v0, p1}, Lfey;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lay;->b(Ljava/lang/CharSequence;)Lay;

    .line 158
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v0}, Lfey;->aI(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 160
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    invoke-virtual {p2, v0}, Lay;->c(Ljava/lang/CharSequence;)Lay;

    .line 164
    :cond_0
    invoke-interface {p3}, Lfey;->ayS()Ljava/lang/String;

    move-result-object v0

    .line 165
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    iput-object v0, p2, Lay;->cR:Ljava/lang/String;

    .line 169
    :cond_1
    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v0}, Lfey;->aG(Landroid/content/Context;)Lbk;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_2

    .line 171
    invoke-virtual {p2, v0}, Lay;->a(Lbk;)Lay;

    .line 174
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.NOTIFICATION_DISMISS_ACTION"

    const/4 v2, 0x0

    iget-object v3, p0, Lfgc;->mAppContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/sidekick/main/NotificationReceiver;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notification_id://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p3}, Lfey;->ayZ()Lfgb;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "notification_entries"

    invoke-interface {p3}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v1, "notificationIdKey"

    invoke-interface {p3}, Lfey;->ayZ()Lfgb;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    if-eqz p4, :cond_3

    const-string v1, "notificationDismissCallback"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_3
    iget-object v1, p0, Lfgc;->mPendingIntentFactory:Lfda;

    const/4 v2, 0x0

    const/high16 v3, 0x48000000    # 131072.0f

    invoke-interface {v1, v2, v0, v3}, Lfda;->a(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p2, v0}, Lay;->a(Landroid/app/PendingIntent;)Lay;

    .line 176
    new-instance v3, Lbl;

    invoke-direct {v3}, Lbl;-><init>()V

    .line 178
    const/4 v2, 0x0

    .line 179
    invoke-interface {p3, p1}, Lfey;->d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffk;

    .line 180
    invoke-interface {v0}, Lffk;->isActive()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 181
    add-int/lit8 v1, v2, 0x1

    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lfgc;->mAppContext:Landroid/content/Context;

    const-class v7, Lcom/google/android/sidekick/main/NotificationReceiver;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "com.google.android.apps.sidekick.NOTIFICATION_CALLBACK_ACTION"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "notification_action://"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p3}, Lfey;->ayZ()Lfgb;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v2, "notification_entries"

    invoke-interface {p3}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v6

    invoke-static {v5, v2, v6}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    const-string v2, "notificationIdKey"

    invoke-interface {p3}, Lfey;->ayZ()Lfgb;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "notificationLogActionKey"

    invoke-interface {v0}, Lffk;->aza()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "notificationLoggingNameKey"

    invoke-interface {p3}, Lfey;->ayU()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "notification_callback"

    iget-object v6, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {v0, v6}, Lffk;->aK(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "callback_type"

    invoke-interface {v0}, Lffk;->azb()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lfgc;->mPendingIntentFactory:Lfda;

    const/4 v6, 0x0

    const/high16 v7, 0x8000000

    invoke-interface {v2, v6, v5, v7}, Lfda;->a(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 183
    invoke-interface {v0}, Lffk;->aif()I

    move-result v5

    .line 184
    iget-object v6, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {v0, v6}, Lffk;->aL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 186
    new-instance v7, Lau;

    invoke-direct {v7, v5, v6, v2}, Lau;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 190
    iget-object v2, p2, Lay;->cP:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-interface {v0}, Lffk;->azc()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    iget-object v0, v3, Lbl;->cP:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move v0, v1

    :goto_1
    move v2, v0

    .line 195
    goto/16 :goto_0

    .line 197
    :cond_5
    invoke-interface {p3}, Lfey;->ayW()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_b

    .line 198
    const/4 v0, 0x0

    iget-object v1, p0, Lfgc;->mEntryProvider:Lfaq;

    invoke-virtual {v1}, Lfaq;->axd()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f100034

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {p2, v0}, Lay;->d(Ljava/lang/CharSequence;)Lay;

    :cond_7
    const/4 v0, -0x2

    iput v0, p2, Lay;->cM:I

    .line 204
    :goto_2
    invoke-interface {p3}, Lfey;->ayV()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 207
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 208
    const-string v1, "time_to_leave"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 209
    if-eqz v0, :cond_8

    iget-object v1, p2, Lay;->cF:Landroid/os/Bundle;

    if-nez v1, :cond_f

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v1, p2, Lay;->cF:Landroid/os/Bundle;

    .line 211
    :cond_8
    :goto_3
    invoke-static {}, Lesi;->avC()Z

    move-result v0

    if-eqz v0, :cond_10

    const v0, 0x7f020315

    .line 214
    :goto_4
    iget-object v1, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 216
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_9

    .line 217
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v3, Lbl;->dc:Landroid/graphics/Bitmap;

    .line 219
    :cond_9
    iget v0, v3, Lbl;->cZ:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lbl;->cZ:I

    .line 221
    :cond_a
    invoke-virtual {p2, v3}, Lay;->a(Laz;)Lay;

    .line 222
    return-void

    .line 200
    :cond_b
    const/4 v0, 0x4

    if-eqz p5, :cond_d

    invoke-interface {p3}, Lfey;->ayR()Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lfgc;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    iget-object v2, p0, Lfgc;->mAppContext:Landroid/content/Context;

    const v4, 0x7f0a013b

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    const/4 v0, 0x5

    :cond_c
    :goto_5
    iget-object v1, p0, Lfgc;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    iget-object v2, p0, Lfgc;->mAppContext:Landroid/content/Context;

    const v4, 0x7f0a013a

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_d

    or-int/lit8 v0, v0, 0x2

    :cond_d
    invoke-virtual {p2, v0}, Lay;->h(I)Lay;

    iget-object v0, p0, Lfgc;->mAppContext:Landroid/content/Context;

    invoke-interface {p3, v0, p1}, Lfey;->b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lay;->e(Ljava/lang/CharSequence;)Lay;

    iget-object v0, p0, Lfgc;->mUserInteractionLogger:Lcpx;

    const-string v1, "NOTIFY"

    invoke-virtual {v0, v1, p3}, Lcpx;->a(Ljava/lang/String;Lfey;)V

    goto/16 :goto_2

    :cond_e
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Lay;->a(Landroid/net/Uri;)Lay;

    goto :goto_5

    .line 209
    :cond_f
    iget-object v1, p2, Lay;->cF:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto/16 :goto_3

    .line 211
    :cond_10
    const v0, 0x7f020314

    goto/16 :goto_4

    :cond_11
    move v0, v2

    goto/16 :goto_1
.end method

.method public final a(Lfgb;)V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lfgc;->mNotificationManager:Lfcz;

    invoke-virtual {p1}, Lfgb;->azC()I

    move-result v1

    invoke-interface {v0, v1}, Lfcz;->cancel(I)V

    .line 370
    return-void
.end method

.method public final a(Ljava/util/Collection;Lfgb;)V
    .locals 8

    .prologue
    .line 424
    invoke-virtual {p0, p2}, Lfgc;->a(Lfgb;)V

    .line 425
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizj;

    .line 426
    iget-object v4, v2, Lizj;->dUo:[Liwk;

    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v3, v4, v0

    .line 427
    invoke-virtual {v3}, Liwk;->getType()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 428
    new-instance v0, Lewv;

    iget-object v1, p0, Lfgc;->mAppContext:Landroid/content/Context;

    iget-object v4, p0, Lfgc;->mNetworkClient:Lfcx;

    iget-object v5, p0, Lfgc;->mClock:Lemp;

    invoke-direct/range {v0 .. v5}, Lewv;-><init>(Landroid/content/Context;Lizj;Liwk;Lfcx;Lemp;)V

    .line 431
    invoke-virtual {v0}, Lewv;->run()V

    .line 436
    :cond_1
    return-void

    .line 426
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final azA()V
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lfgc;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 380
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_notification_time"

    iget-object v2, p0, Lfgc;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 383
    return-void
.end method

.method public final azB()J
    .locals 4

    .prologue
    .line 387
    iget-object v0, p0, Lfgc;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 388
    const-string v1, "last_notification_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final cancelAll()V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lfgc;->mNotificationManager:Lfcz;

    invoke-interface {v0}, Lfcz;->cancelAll()V

    .line 375
    return-void
.end method

.method public final p(Lizj;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/4 v3, 0x0

    .line 394
    if-nez p1, :cond_0

    .line 420
    :goto_0
    return-void

    .line 396
    :cond_0
    const/4 v1, 0x0

    .line 400
    iget-object v4, p1, Lizj;->dUo:[Liwk;

    array-length v5, v4

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    .line 401
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v6

    if-ne v6, v7, :cond_2

    .line 412
    :goto_2
    if-nez v0, :cond_1

    .line 413
    new-instance v0, Liwk;

    invoke-direct {v0}, Liwk;-><init>()V

    invoke-virtual {v0, v7}, Liwk;->nb(I)Liwk;

    move-result-object v0

    .line 418
    :cond_1
    new-instance v1, Lexo;

    iget-object v2, p0, Lfgc;->mNetworkClient:Lfcx;

    iget-object v4, p0, Lfgc;->mClock:Lemp;

    invoke-direct {v1, v2, p1, v0, v4}, Lexo;-><init>(Lfcx;Lizj;Liwk;Lemp;)V

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lexo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 400
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
