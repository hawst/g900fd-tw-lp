.class public final Lfzm;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 11

    .prologue
    const v10, 0x7f11019f

    const/4 v2, 0x0

    .line 40
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 42
    const v1, 0x7f11035d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 43
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->ahf:Lapl;

    iget-object v5, v1, Lapl;->alq:[Lapm;

    array-length v6, v5

    move v1, v2

    move v3, v2

    :goto_0
    if-ge v1, v6, :cond_4

    aget-object v7, v5, v1

    .line 44
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 45
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 46
    invoke-virtual {v7}, Lapm;->sV()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 47
    const v8, 0x7f11035e

    invoke-virtual {v7}, Lapm;->sU()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 48
    const v8, 0x7f11035f

    const-string v9, " \u00b7 "

    invoke-static {v3, v8, v9}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 50
    :cond_0
    const v8, 0x7f110360

    invoke-virtual {v7}, Lapm;->sW()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 51
    const v8, 0x7f110361

    invoke-virtual {v7}, Lapm;->sX()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 53
    invoke-virtual {v7}, Lapm;->pl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v10, v8}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 54
    const v8, 0x7f11007c

    invoke-virtual {v7}, Lapm;->sY()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 55
    invoke-virtual {v7}, Lapm;->pm()Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v7}, Lapm;->sZ()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 56
    :cond_1
    const v8, 0x7f110362

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/View;->setVisibility(I)V

    .line 58
    :cond_2
    invoke-virtual {v7}, Lapm;->pl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v10, v8}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 59
    invoke-virtual {v7}, Lapm;->tb()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 60
    const v8, 0x7f110364

    invoke-virtual {v7}, Lapm;->ta()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v8, v7}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 61
    const v7, 0x7f110363

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 43
    :cond_3
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    goto/16 :goto_0

    .line 64
    :cond_4
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 30
    const v0, 0x7f04013d

    iget-object v2, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v2}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v2

    invoke-virtual {p1, v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 31
    const v0, 0x7f11035d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 32
    iget-object v3, p0, Lfro;->cBc:Lanh;

    iget-object v3, v3, Lanh;->ahf:Lapl;

    iget-object v3, v3, Lapl;->alq:[Lapm;

    array-length v3, v3

    :goto_0
    if-ge v1, v3, :cond_0

    .line 33
    const v4, 0x7f04013e

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-object v2
.end method
