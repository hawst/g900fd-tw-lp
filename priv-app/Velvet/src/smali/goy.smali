.class public final Lgoy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cRJ:Lgor;

.field private final cRK:Ljava/lang/ClassLoader;

.field private final cRL:Lgox;

.field final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgor;Ljava/lang/ClassLoader;Lgox;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lgoy;->mContext:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lgoy;->cRJ:Lgor;

    .line 31
    iput-object p3, p0, Lgoy;->cRK:Ljava/lang/ClassLoader;

    .line 32
    iput-object p4, p0, Lgoy;->cRL:Lgox;

    .line 33
    return-void
.end method

.method private a(Ljava/io/File;Ljava/util/jar/Attributes;)Lgoz;
    .locals 7

    .prologue
    .line 126
    :try_start_0
    new-instance v0, Lgoz;

    iget-object v1, p0, Lgoy;->cRJ:Lgor;

    iget-object v2, p0, Lgoy;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lgoy;->cRK:Ljava/lang/ClassLoader;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lgoz;-><init>(Lgor;Landroid/content/Context;Ljava/io/File;Ljava/lang/ClassLoader;Ljava/util/jar/Attributes;)V

    .line 133
    new-instance v1, Lgou;

    iget-object v2, v0, Lgoz;->mContext:Landroid/content/Context;

    iget-object v3, v0, Lgoz;->cRM:Ljava/io/File;

    iget-object v4, v0, Lgoz;->cRG:Ljava/lang/ClassLoader;

    iget-object v5, v0, Lgoz;->cRM:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lgou;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/ClassLoader;Ljava/io/File;)V

    iput-object v1, v0, Lgoz;->cRO:Lgou;
    :try_end_0
    .catch Lgoo; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v1, v0, Lgoz;->cRO:Lgou;

    iget-object v2, v1, Lgou;->cRH:Ljava/lang/ClassLoader;

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lgoo; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lgoo;

    const-string v2, "Failed to init JarLoader"

    invoke-direct {v1, v2, v0}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Lgoo; {:try_start_2 .. :try_end_2} :catch_1

    .line 135
    :catch_1
    move-exception v0

    .line 136
    const-string v1, "Failed creating loader"

    invoke-virtual {p0, v1, v0}, Lgoy;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 137
    new-instance v1, Lgoo;

    const-string v2, "Failed creating loader"

    invoke-direct {v1, v2, v0}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 133
    :cond_0
    :try_start_3
    iget-object v2, v1, Lgou;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    new-instance v3, Lgol;

    iget-object v4, v1, Lgou;->mFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lgou;->cRI:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lgou;->cRG:Ljava/lang/ClassLoader;

    invoke-direct {v3, v4, v5, v2, v6}, Lgol;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    iput-object v3, v1, Lgou;->cRH:Ljava/lang/ClassLoader;

    new-instance v2, Ljava/util/jar/JarFile;

    iget-object v3, v1, Lgou;->mFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V

    iput-object v2, v1, Lgou;->cRn:Ljava/util/jar/JarFile;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lgoo; {:try_start_3 .. :try_end_3} :catch_1

    .line 134
    return-object v0
.end method


# virtual methods
.method public final d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 52
    move-object v2, p2

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "ENOSPC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 53
    new-instance v1, Lgos;

    invoke-direct {v1, p1, p2, v0}, Lgos;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v1

    .line 52
    :cond_0
    instance-of v3, v2, Lgos;

    if-eqz v3, :cond_1

    move v2, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    .line 54
    :cond_3
    iget-object v2, p0, Lgoy;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_4

    :goto_2
    if-eqz v0, :cond_5

    .line 59
    new-instance v0, Lgos;

    invoke-direct {v0, p1, p2, v1}, Lgos;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    throw v0

    :cond_4
    move v0, v1

    .line 54
    goto :goto_2

    .line 61
    :cond_5
    return-void
.end method

.method public final r(Ljava/io/File;)Lgop;
    .locals 4

    .prologue
    .line 39
    :try_start_0
    invoke-static {p1}, Lgoq;->q(Ljava/io/File;)Ljava/util/jar/Attributes;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lgoy;->cRL:Lgox;

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lgoy;->cRL:Lgox;

    invoke-interface {v1, v0}, Lgox;->a(Ljava/util/jar/Attributes;)V

    .line 43
    :cond_0
    invoke-direct {p0, p1, v0}, Lgoy;->a(Ljava/io/File;Ljava/util/jar/Attributes;)Lgoz;

    move-result-object v0

    iget-object v1, v0, Lgoz;->cRJ:Lgor;

    iget-object v1, v1, Lgor;->cRE:Ljava/lang/String;

    iget-object v2, v0, Lgoz;->cRN:Ljava/util/jar/Attributes;

    invoke-virtual {v2, v1}, Ljava/util/jar/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Lgoo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No factory class name in attribute "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lgoz;->cRJ:Lgor;

    iget-object v3, v3, Lgor;->cRE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in JAR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lgoz;->cRM:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgoo;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lgoo; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot load jar: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgoy;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    new-instance v1, Lgoo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot load jar: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 43
    :cond_1
    :try_start_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Lgoo;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got empty classname from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lgoz;->cRM:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgoo;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v0, v0, Lgoz;->cRO:Lgou;

    invoke-virtual {v0, v1}, Lgou;->nq(Ljava/lang/String;)Lgop;
    :try_end_1
    .catch Lgoo; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method public final s(Ljava/io/File;)Ljava/util/jar/Attributes;
    .locals 6

    .prologue
    .line 100
    const/4 v2, 0x0

    .line 102
    :try_start_0
    new-instance v1, Ljava/util/jar/JarFile;

    invoke-direct {v1, p1}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lgoo; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 103
    :try_start_1
    new-instance v2, Lgpa;

    iget-object v0, p0, Lgoy;->mContext:Landroid/content/Context;

    invoke-direct {v2, v0, v1}, Lgpa;-><init>(Landroid/content/Context;Ljava/util/jar/JarFile;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lgoo; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-string v0, "Velour.JarSignatureChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Verifying JAR "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lgpa;->cRn:Ljava/util/jar/JarFile;

    invoke-virtual {v4}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Lgpa;->aIx()Ljava/security/cert/Certificate;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgpa;->a(Ljava/security/cert/Certificate;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lgoo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Not signed with an accepted Google certificate: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lgpa;->cRn:Ljava/util/jar/JarFile;

    invoke-virtual {v4}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lgoo;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lgoo; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v3, Lgoo;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to verify JAR: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lgpa;->cRn:Ljava/util/jar/JarFile;

    invoke-virtual {v2}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2, v0}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lgoo; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 109
    :catch_1
    move-exception v0

    .line 110
    :goto_0
    :try_start_4
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 111
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to open jar file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lgoy;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    new-instance v2, Lgoo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to open jar file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 118
    :catchall_0
    move-exception v0

    :goto_1
    invoke-static {v1}, Lgom;->a(Ljava/util/jar/JarFile;)V

    throw v0

    .line 103
    :cond_0
    :try_start_5
    const-string v0, "Velour.JarSignatureChecker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Verified JAR "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lgpa;->cRn:Ljava/util/jar/JarFile;

    invoke-virtual {v4}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lgoo; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 104
    :try_start_6
    invoke-static {v1}, Lgoq;->b(Ljava/util/jar/JarFile;)Ljava/util/jar/Attributes;

    move-result-object v0

    .line 105
    iget-object v2, p0, Lgoy;->cRL:Lgox;

    if-eqz v2, :cond_1

    .line 106
    iget-object v2, p0, Lgoy;->cRL:Lgox;

    invoke-interface {v2, v0}, Lgox;->a(Ljava/util/jar/Attributes;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lgoo; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 118
    :cond_1
    invoke-static {v1}, Lgom;->a(Ljava/util/jar/JarFile;)V

    return-object v0

    .line 113
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 114
    :goto_2
    :try_start_7
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Signature check/validation failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgoy;->cRJ:Lgor;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lgoy;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116
    new-instance v2, Lgoo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Signature check/validation failed for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lgoy;->cRJ:Lgor;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lgoo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 118
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    .line 113
    :catch_3
    move-exception v0

    goto :goto_2

    .line 109
    :catch_4
    move-exception v0

    move-object v1, v2

    goto/16 :goto_0
.end method
