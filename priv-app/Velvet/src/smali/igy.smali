.class final Ligy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private dBQ:Ljava/util/Map$Entry;

.field private synthetic dBW:Ljava/util/Iterator;

.field private synthetic dBX:Ligx;


# direct methods
.method constructor <init>(Ligx;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Ligy;->dBX:Ligx;

    iput-object p2, p0, Ligy;->dBW:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Ligy;->dBW:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Ligy;->dBW:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Ligy;->dBQ:Ljava/util/Map$Entry;

    .line 354
    iget-object v0, p0, Ligy;->dBQ:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 359
    iget-object v0, p0, Ligy;->dBQ:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "no calls to next() since the last call to remove()"

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 361
    iget-object v0, p0, Ligy;->dBX:Ligx;

    iget-object v2, v0, Ligx;->dBS:Ligu;

    iget-object v0, p0, Ligy;->dBQ:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liik;

    invoke-virtual {v0, v1}, Liik;->getAndSet(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v0, v1}, Ligu;->a(Ligu;J)J

    .line 362
    iget-object v0, p0, Ligy;->dBW:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 363
    const/4 v0, 0x0

    iput-object v0, p0, Ligy;->dBQ:Ljava/util/Map$Entry;

    .line 364
    return-void

    :cond_0
    move v0, v1

    .line 359
    goto :goto_0
.end method
