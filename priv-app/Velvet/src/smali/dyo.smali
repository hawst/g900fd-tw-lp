.class public Ldyo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field private static bQR:Ljava/nio/charset/Charset;

.field private static bQS:Ljava/nio/charset/Charset;


# instance fields
.field private final bQT:Ljava/io/InputStream;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bQU:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final bfD:Leeb;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mUriRequest:Lcom/google/android/search/shared/api/UriRequest;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    sput-object v0, Ldyo;->bQR:Ljava/nio/charset/Charset;

    .line 56
    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    sput-object v0, Ldyo;->bQS:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V
    .locals 2
    .param p1    # Lcom/google/android/search/shared/api/UriRequest;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Leeb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldyo;->bQU:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 74
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/api/UriRequest;

    iput-object v0, p0, Ldyo;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    .line 75
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Ldyo;->bQT:Ljava/io/InputStream;

    .line 76
    invoke-static {p2}, Ldyo;->b(Leeb;)Leeb;

    move-result-object v0

    iput-object v0, p0, Ldyo;->bfD:Leeb;

    .line 77
    return-void
.end method

.method public static alM()Leeb;
    .locals 3

    .prologue
    .line 175
    new-instance v0, Leeb;

    const-string v1, "text/html"

    sget-object v2, Ldyo;->bQR:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Leeb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Leeb;)Leeb;
    .locals 4
    .param p0    # Leeb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 135
    if-nez p0, :cond_1

    const-string v0, "text/html"

    move-object v1, v0

    .line 136
    :goto_0
    if-nez p0, :cond_2

    sget-object v0, Ldyo;->bQR:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    .line 143
    :goto_1
    :try_start_0
    const-string v2, "application/json"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 145
    if-eqz v0, :cond_0

    sget-object v1, Ldyo;->bQS:Ljava/nio/charset/Charset;

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/charset/Charset;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 146
    :cond_0
    const-string v1, "Velvet.WebPage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected charset "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Ldyo;->bQS:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-static {}, Ldyo;->alM()Leeb;
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 170
    :goto_2
    return-object v0

    .line 135
    :cond_1
    iget-object v0, p0, Leeb;->bWw:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Leeb;->bWx:Ljava/lang/String;

    goto :goto_1

    .line 149
    :cond_3
    :try_start_1
    sget-object v0, Ldyo;->bQR:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
    :try_end_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 169
    :goto_3
    const-string v2, "text/html"

    .line 170
    new-instance v1, Leeb;

    invoke-direct {v1, v2, v0}, Leeb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_2

    .line 150
    :cond_4
    :try_start_2
    const-string v2, "application/protobuf"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 151
    sget-object v0, Ldyo;->bQR:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 152
    :cond_5
    const-string v2, "text/html"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 154
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_2
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 160
    :catch_0
    move-exception v0

    .line 161
    const-string v1, "Velvet.WebPage"

    const-string v2, "Unknown charset"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 162
    invoke-static {}, Ldyo;->alM()Leeb;

    move-result-object v0

    goto :goto_2

    .line 156
    :cond_6
    :try_start_3
    const-string v0, "Velvet.WebPage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected response MIME type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    sget-object v0, Ldyo;->bQR:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
    :try_end_3
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto :goto_3

    .line 163
    :catch_1
    move-exception v0

    .line 164
    const-string v1, "Velvet.WebPage"

    const-string v2, "Unsupported charset"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 165
    invoke-static {}, Ldyo;->alM()Leeb;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 185
    const-string v0, "WebPage"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 186
    const-string v0, "mContentType"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldyo;->bfD:Leeb;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 188
    const-string v0, "mContentStream"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldyo;->bQT:Ljava/io/InputStream;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 189
    const-string v0, "mContentStreamUsed"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldyo;->bQU:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 190
    return-void
.end method

.method public final alJ()Lcom/google/android/search/shared/api/UriRequest;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Ldyo;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    return-object v0
.end method

.method public final alK()Ljava/io/InputStream;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Ldyo;->bQT:Ljava/io/InputStream;

    return-object v0
.end method

.method public final alL()Landroid/webkit/WebResourceResponse;
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Ldyo;->bQU:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    .line 107
    if-nez v0, :cond_0

    .line 109
    :try_start_0
    iget-object v0, p0, Ldyo;->bQT:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_0
    new-instance v0, Landroid/webkit/WebResourceResponse;

    iget-object v1, p0, Ldyo;->bfD:Leeb;

    iget-object v1, v1, Leeb;->bWw:Ljava/lang/String;

    iget-object v2, p0, Ldyo;->bfD:Leeb;

    iget-object v2, v2, Leeb;->bWx:Ljava/lang/String;

    iget-object v3, p0, Ldyo;->bQT:Ljava/io/InputStream;

    invoke-direct {v0, v1, v2, v3}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const-string v1, "Velvet.WebPage"

    const-string v2, "Could not reset input stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 196
    invoke-virtual {p0}, Ldyo;->alL()Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 197
    if-nez v0, :cond_0

    .line 198
    const-string v0, "[Error dumping response content]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 213
    :goto_0
    return-void

    .line 201
    :cond_0
    const/4 v2, 0x0

    .line 203
    :try_start_0
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Landroid/webkit/WebResourceResponse;->getData()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v0}, Landroid/webkit/WebResourceResponse;->getEncoding()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :try_start_1
    invoke-static {v1, p1}, Lisp;->a(Ljava/lang/Readable;Ljava/lang/Appendable;)J
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 212
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 206
    :goto_1
    :try_start_2
    const-string v2, "[Error dumping response content]"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 207
    const-string v2, "Velvet.WebPage"

    const-string v3, "Unsupported charset"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 208
    :catch_1
    move-exception v0

    .line 209
    :goto_2
    :try_start_3
    const-string v1, "[Error dumping response content]"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 210
    const-string v1, "Velvet.WebPage"

    const-string v3, "Error dumping response content"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 212
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 208
    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    .line 205
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebPage{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldyo;->bfD:Leeb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
