.class public final Lezt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContextProviders:Lezg;

.field private final mFactory:Lfjs;

.field private final mTrainingQuestionManager:Lfdn;


# direct methods
.method public constructor <init>(Lezg;Lfjs;Lfdn;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lezt;->mContextProviders:Lezg;

    .line 29
    iput-object p2, p0, Lezt;->mFactory:Lfjs;

    .line 30
    iput-object p3, p0, Lezt;->mTrainingQuestionManager:Lfdn;

    .line 31
    return-void
.end method

.method private a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p2, Lizj;->dSa:Ljdj;

    if-nez v0, :cond_1

    iget-object v0, p2, Lizj;->dTo:Ljdi;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lizj;->dTo:Ljdi;

    iget-object v0, v0, Ljdi;->eca:Ljdj;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p2, Lizj;->dSa:Ljdj;

    if-nez v0, :cond_2

    iget-object v0, p2, Lizj;->dTo:Ljdi;

    iget-object v0, v0, Ljdi;->eca:Ljdj;

    .line 87
    :goto_1
    iget-object v1, p0, Lezt;->mTrainingQuestionManager:Lfdn;

    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-interface {v1, v0}, Lfdn;->t(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 89
    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->w(Ljava/util/Collection;)V

    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p2, Lizj;->dSa:Ljdj;

    goto :goto_1
.end method

.method private a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizq;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 54
    iget-object v1, p2, Lizq;->dUZ:Lizj;

    if-eqz v1, :cond_2

    .line 55
    iget-object v0, p0, Lezt;->mFactory:Lfjs;

    invoke-interface {v0, p2}, Lfjs;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezi;

    .line 56
    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lezt;->mContextProviders:Lezg;

    invoke-interface {v0, p1, v1}, Lezi;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lezg;)V

    .line 59
    :cond_0
    iget-object v0, p2, Lizq;->dUZ:Lizj;

    invoke-direct {p0, p1, v0}, Lezt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;)V

    .line 73
    :cond_1
    return-void

    .line 60
    :cond_2
    iget-object v1, p2, Lizq;->dUX:[Lizj;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 61
    iget-object v2, p2, Lizq;->dUX:[Lizj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 62
    iget-object v0, p0, Lezt;->mFactory:Lfjs;

    invoke-interface {v0, v4}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezi;

    .line 63
    if-eqz v0, :cond_3

    .line 64
    iget-object v5, p0, Lezt;->mContextProviders:Lezg;

    invoke-interface {v0, p1, v5}, Lezi;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lezg;)V

    .line 66
    :cond_3
    invoke-direct {p0, p1, v4}, Lezt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;)V

    .line 61
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 68
    :cond_4
    iget-object v1, p2, Lizq;->dUW:[Lizq;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 69
    iget-object v1, p2, Lizq;->dUW:[Lizq;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 70
    invoke-direct {p0, p1, v3}, Lezt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizq;)V

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizo;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lezt;->mContextProviders:Lezg;

    iget-object v0, v0, Lezg;->mDeviceCapabilityContextProvider:Lezh;

    invoke-virtual {v0, p1}, Lezh;->c(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    .line 35
    iget-object v0, p2, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p2, Lizo;->dUQ:Lizq;

    .line 37
    iget-object v1, v0, Lizq;->dUW:[Lizq;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 38
    invoke-direct {p0, p1, v3}, Lezt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizq;)V

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method
