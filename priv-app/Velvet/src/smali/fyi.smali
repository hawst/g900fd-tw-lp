.class public final Lfyi;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 23
    return-void
.end method

.method private static b(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 99
    const-string v0, "  "

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 7

    .prologue
    .line 34
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 36
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->ahe:Laox;

    .line 37
    invoke-virtual {v1}, Laox;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 38
    const v2, 0x7f110082

    invoke-virtual {v1}, Laox;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 41
    :cond_0
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1}, Laox;->rG()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lfyi;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lfyi;->mContext:Landroid/content/Context;

    const v5, 0x7f0a04d1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lfyi;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00b9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Lgab;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    invoke-virtual {v1}, Laox;->pe()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Lfyi;->b(Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v1}, Laox;->on()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    const v3, 0x7f1102f0

    invoke-static {v0, v3, v2}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 43
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v3, v1, Laox;->ahN:Laok;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lfyi;->mContext:Landroid/content/Context;

    iget-object v4, v1, Laox;->ahN:Laok;

    invoke-static {v3, v4}, Lfxt;->a(Landroid/content/Context;Laok;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    invoke-virtual {v1}, Laox;->rF()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v2}, Lfyi;->b(Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v1}, Laox;->rE()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    const v3, 0x7f1102ee

    invoke-static {v0, v3, v2}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, v1, Laox;->ahN:Laok;

    if-eqz v3, :cond_5

    iget-object v3, v1, Laox;->ahN:Laok;

    iget-object v3, v3, Laok;->ahs:Lani;

    if-eqz v3, :cond_5

    iget-object v3, v1, Laox;->ahN:Laok;

    iget-object v3, v3, Laok;->ahs:Lani;

    invoke-virtual {p0, v2, v3}, Lfyi;->a(Landroid/view/View;Lani;)V

    .line 45
    :cond_5
    invoke-virtual {v1}, Laox;->pf()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 46
    const v2, 0x7f1100b0

    invoke-virtual {v1}, Laox;->oo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 49
    :cond_6
    iget-object v1, v1, Laox;->ahM:Laoi;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lfyi;->a(Landroid/view/View;Laoi;Z)V

    .line 50
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f040131

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
