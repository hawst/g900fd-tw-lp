.class public final Leoy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public KU:Landroid/content/BroadcastReceiver;

.field bmU:Ljava/util/Set;

.field private final cgR:Ljava/lang/String;

.field final cgS:Leoz;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Leoz;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p2, p0, Leoy;->cgR:Ljava/lang/String;

    .line 30
    iput-object p1, p0, Leoy;->mContext:Landroid/content/Context;

    .line 31
    iput-object p3, p0, Leoy;->cgS:Leoz;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Leoy;->bmU:Ljava/util/Set;

    .line 33
    new-instance v0, Lepa;

    invoke-direct {v0, p0}, Lepa;-><init>(Leoy;)V

    iput-object v0, p0, Leoy;->KU:Landroid/content/BroadcastReceiver;

    .line 34
    return-void
.end method


# virtual methods
.method public final aK(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 37
    iget-object v1, p0, Leoy;->bmU:Ljava/util/Set;

    monitor-enter v1

    .line 38
    :try_start_0
    iget-object v0, p0, Leoy;->bmU:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 41
    iget-object v2, p0, Leoy;->cgR:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 42
    iget-object v2, p0, Leoy;->mContext:Landroid/content/Context;

    iget-object v3, p0, Leoy;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 44
    :cond_0
    iget-object v0, p0, Leoy;->bmU:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aL(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 49
    iget-object v1, p0, Leoy;->bmU:Ljava/util/Set;

    monitor-enter v1

    .line 50
    :try_start_0
    iget-object v0, p0, Leoy;->bmU:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leoy;->bmU:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Leoy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Leoy;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 53
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Leoy;->bmU:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method
