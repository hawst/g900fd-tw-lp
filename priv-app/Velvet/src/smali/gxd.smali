.class public final enum Lgxd;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cYe:Lgxd;

.field private static enum cYf:Lgxd;

.field private static final synthetic cYh:[Lgxd;


# instance fields
.field private final cYg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lgxd;

    const-string v1, "INTRO_CARD"

    const-string v2, "intro_"

    invoke-direct {v0, v1, v3, v2}, Lgxd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lgxd;->cYe:Lgxd;

    .line 23
    new-instance v0, Lgxd;

    const-string v1, "SWIPE_TUTORIAL_CARD"

    const-string v2, "swipe_"

    invoke-direct {v0, v1, v4, v2}, Lgxd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lgxd;->cYf:Lgxd;

    .line 21
    const/4 v0, 0x2

    new-array v0, v0, [Lgxd;

    sget-object v1, Lgxd;->cYe:Lgxd;

    aput-object v1, v0, v3

    sget-object v1, Lgxd;->cYf:Lgxd;

    aput-object v1, v0, v4

    sput-object v0, Lgxd;->cYh:[Lgxd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lgxd;->cYg:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static kG(I)Lgxd;
    .locals 1

    .prologue
    .line 28
    packed-switch p0, :pswitch_data_0

    .line 34
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 30
    :pswitch_1
    sget-object v0, Lgxd;->cYe:Lgxd;

    goto :goto_0

    .line 32
    :pswitch_2
    sget-object v0, Lgxd;->cYf:Lgxd;

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lgxd;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lgxd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgxd;

    return-object v0
.end method

.method public static values()[Lgxd;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lgxd;->cYh:[Lgxd;

    invoke-virtual {v0}, [Lgxd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgxd;

    return-object v0
.end method


# virtual methods
.method public final aLm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lgxd;->cYg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "card_dismissed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aLn()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lgxd;->cYg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "card_first_view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
