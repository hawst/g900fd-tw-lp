.class public Lhvn;
.super Lgly;
.source "PG"


# instance fields
.field private aTn:Z

.field private synthetic dsg:Lhvi;


# direct methods
.method public constructor <init>(Lhvi;)V
    .locals 1

    .prologue
    .line 770
    iput-object p1, p0, Lhvn;->dsg:Lhvi;

    invoke-direct {p0}, Lgly;-><init>()V

    .line 771
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvn;->aTn:Z

    .line 772
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 1

    .prologue
    .line 939
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-eqz v0, :cond_0

    .line 945
    :goto_0
    return-void

    .line 942
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvn;->aTn:Z

    .line 944
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aQs()V

    goto :goto_0
.end method

.method public final Nr()V
    .locals 1

    .prologue
    .line 787
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drX:Lhvz;

    invoke-virtual {v0}, Lhvz;->aSZ()V

    .line 788
    return-void
.end method

.method public final Nw()V
    .locals 1

    .prologue
    .line 849
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-eqz v0, :cond_0

    .line 858
    :goto_0
    return-void

    .line 853
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvn;->aTn:Z

    .line 855
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 856
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->aSX()V

    .line 857
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSN()V

    goto :goto_0
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 806
    iget-boolean v1, p0, Lhvn;->aTn:Z

    if-eqz v1, :cond_1

    .line 843
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    iget-object v1, p1, Ljvv;->eIn:Ljvu;

    if-nez v1, :cond_3

    .line 812
    :cond_2
    :goto_1
    iget-object v1, p1, Ljvv;->eIm:Ljvw;

    if-eqz v1, :cond_9

    .line 813
    iget-object v1, p1, Ljvv;->eIm:Ljvw;

    .line 814
    iget-object v3, v1, Ljvw;->eIk:[Ljvs;

    array-length v3, v3

    if-nez v3, :cond_6

    .line 817
    const-string v0, "VoiceInputMethodManager"

    const-string v1, "No hypothesis in recognition result."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 810
    :cond_3
    iget-object v3, p1, Ljvv;->eIn:Ljvu;

    iget-object v1, v3, Ljvu;->eIb:[Ljvt;

    array-length v4, v1

    if-eqz v4, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lhvn;->dsg:Lhvi;

    iget-object v1, v1, Lhvi;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    iget-object v1, v1, Ljze;->eNf:Ljzi;

    invoke-virtual {v1}, Ljzi;->bwV()F

    move-result v6

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_5

    iget-object v7, v3, Ljvu;->eIb:[Ljvt;

    aget-object v7, v7, v1

    invoke-virtual {v7}, Ljvt;->hasText()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v7}, Ljvt;->bvf()D

    move-result-wide v8

    float-to-double v10, v6

    cmpl-double v8, v8, v10

    if-ltz v8, :cond_5

    invoke-virtual {v7}, Ljvt;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 820
    :cond_6
    iget-object v1, v1, Ljvw;->eIk:[Ljvs;

    aget-object v1, v1, v2

    .line 823
    iget-object v3, p0, Lhvn;->dsg:Lhvi;

    iget-object v3, v3, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v3}, Lhvs;->aSV()Lgnj;

    move-result-object v3

    .line 826
    invoke-virtual {v3}, Lgnj;->aHD()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v1, Ljvs;->eHY:Ljtn;

    if-eqz v3, :cond_8

    .line 827
    invoke-virtual {v1}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Ljvs;->eHY:Ljtn;

    iget-object v1, v1, Ljtn;->eDA:[Ljtm;

    invoke-static {v3, v1}, Lcom/google/android/shared/speech/Hypothesis;->a(Ljava/lang/CharSequence;[Ljtm;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v1

    .line 833
    :goto_3
    iget-object v3, p0, Lhvn;->dsg:Lhvi;

    iget-object v4, v3, Lhvi;->drY:Lhuy;

    iput-boolean v2, v4, Lhuy;->Kn:Z

    iget-object v2, v3, Lhvi;->drZ:Lhup;

    invoke-virtual {v2, v1, v0}, Lhup;->a(Lcom/google/android/shared/speech/Hypothesis;Ljava/lang/String;)V

    .line 838
    :cond_7
    :goto_4
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v12, :cond_0

    .line 840
    iput-boolean v12, p0, Lhvn;->aTn:Z

    .line 841
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aQs()V

    goto/16 :goto_0

    .line 830
    :cond_8
    invoke-virtual {v1}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/shared/speech/Hypothesis;->E(Ljava/lang/CharSequence;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v1

    goto :goto_3

    .line 834
    :cond_9
    if-eqz v0, :cond_7

    .line 835
    iget-object v1, p0, Lhvn;->dsg:Lhvi;

    iget-object v2, v1, Lhvi;->drY:Lhuy;

    iput-boolean v12, v2, Lhuy;->Kn:Z

    if-eqz v0, :cond_7

    iget-object v1, v1, Lhvi;->drZ:Lhup;

    invoke-virtual {v1, v0}, Lhup;->oo(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public final aEM()V
    .locals 2

    .prologue
    .line 794
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-eqz v0, :cond_0

    .line 800
    :goto_0
    return-void

    .line 797
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvn;->aTn:Z

    .line 798
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 799
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drX:Lhvz;

    iget-object v1, p0, Lhvn;->dsg:Lhvi;

    iget-object v1, v1, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v1}, Lhvs;->aSW()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhvz;->gL(Z)V

    goto :goto_0
.end method

.method public final au(J)V
    .locals 1

    .prologue
    .line 863
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-eqz v0, :cond_0

    .line 868
    :goto_0
    return-void

    .line 867
    :cond_0
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drX:Lhvz;

    invoke-virtual {v0}, Lhvz;->aTa()V

    goto :goto_0
.end method

.method public final c(Leiq;)V
    .locals 3

    .prologue
    .line 885
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-eqz v0, :cond_0

    .line 893
    :goto_0
    return-void

    .line 888
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvn;->aTn:Z

    .line 890
    const-string v0, "VoiceInputMethodManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onError: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Leiq;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPH()V

    .line 892
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    invoke-virtual {v0, p1}, Lhvi;->l(Leiq;)V

    goto :goto_0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 778
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvn;->aTn:Z

    .line 779
    return-void
.end method

.method public final isValid()Z
    .locals 1

    .prologue
    .line 782
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onEndOfSpeech()V
    .locals 1

    .prologue
    .line 873
    iget-boolean v0, p0, Lhvn;->aTn:Z

    if-eqz v0, :cond_0

    .line 880
    :goto_0
    return-void

    .line 877
    :cond_0
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPE()V

    .line 878
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->stopListening()V

    .line 879
    iget-object v0, p0, Lhvn;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSN()V

    goto :goto_0
.end method
