.class final Lczf;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final bnH:Ljava/lang/String;

.field private synthetic bnJ:Lcze;

.field private final mHttpHelper:Ldkx;


# direct methods
.method constructor <init>(Lcze;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 180
    iput-object p1, p0, Lczf;->bnJ:Lcze;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 181
    iput-object p2, p0, Lczf;->bnH:Ljava/lang/String;

    .line 182
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DG()Ldkx;

    move-result-object v0

    iput-object v0, p0, Lczf;->mHttpHelper:Ldkx;

    .line 184
    return-void
.end method

.method private static e(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 250
    :try_start_0
    const-string v1, "matches"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    move v3, v2

    .line 251
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v3, v1, :cond_2

    .line 252
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 253
    invoke-virtual {v5}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v6

    .line 254
    new-instance v7, Ljfl;

    invoke-direct {v7}, Ljfl;-><init>()V

    move v1, v2

    .line 255
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v1, v8, :cond_5

    .line 256
    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 257
    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 259
    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 260
    const-string v10, "id"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 264
    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljfl;->dd(J)Ljfl;

    .line 255
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 265
    :cond_1
    const-string v10, "t"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 266
    invoke-virtual {v7, v9}, Ljfl;->uq(Ljava/lang/String;)Ljfl;

    goto :goto_2

    .line 279
    :catch_0
    move-exception v1

    .line 281
    :cond_2
    return-object v0

    .line 267
    :cond_3
    const-string v10, "e"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 268
    invoke-virtual {v7, v9}, Ljfl;->up(Ljava/lang/String;)Ljfl;

    goto :goto_2

    .line 269
    :cond_4
    const-string v10, "n"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 270
    invoke-virtual {v7, v9}, Ljfl;->ur(Ljava/lang/String;)Ljfl;

    goto :goto_2

    .line 273
    :cond_5
    invoke-virtual {v7}, Ljfl;->bjw()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v7}, Ljfl;->pX()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v7}, Ljfl;->bjt()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v7}, Ljfl;->bju()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 275
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0
.end method

.method private je(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 210
    :try_start_0
    new-instance v1, Ldlb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://www.google.com/finance/match?q="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "UTF-8"

    invoke-static {p1, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ldlb;-><init>(Ljava/lang/String;)V

    .line 213
    iget-object v2, p0, Lczf;->mHttpHelper:Ldkx;

    const/16 v3, 0x8

    invoke-virtual {v2, v1, v3}, Ldkx;->c(Ldlb;I)[B

    move-result-object v2

    .line 214
    if-nez v2, :cond_0

    .line 237
    :goto_0
    return-object v0

    .line 218
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 228
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 234
    goto :goto_0

    .line 235
    :catch_0
    move-exception v1

    .line 236
    const-string v2, "AddStockDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Network error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 226
    :catch_1
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lczf;->bnH:Ljava/lang/String;

    invoke-direct {p0, v0}, Lczf;->je(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v2, p0, Lczf;->bnJ:Lcze;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Lcze;->bnA:Z

    iget-object v0, p0, Lczf;->bnJ:Lcze;

    iget-boolean v0, v0, Lcze;->bnA:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lczf;->e(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 174
    check-cast p1, Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfl;

    new-instance v3, Lczd;

    invoke-virtual {v0}, Ljfl;->getSymbol()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lczd;-><init>(Ljava/lang/String;Ljfl;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lczf;->bnJ:Lcze;

    iget-object v0, v0, Lcze;->bnI:Ldjr;

    iget-object v2, p0, Lczf;->bnH:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ldjr;->a(Ljava/util/List;Ljava/lang/String;)V

    iget-object v0, p0, Lczf;->bnJ:Lcze;

    iget-object v0, v0, Lcze;->bnI:Ldjr;

    invoke-virtual {v0}, Ldjr;->notifyDataSetChanged()V

    return-void
.end method
