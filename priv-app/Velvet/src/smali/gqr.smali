.class public final Lgqr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cTe:I

.field private final cTf:I

.field public final cTg:Z

.field private final cTh:[Ljava/lang/Object;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(IILandroid/content/res/Resources;Z[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lgqr;->cTe:I

    .line 44
    iput p2, p0, Lgqr;->cTf:I

    .line 45
    iput-object p3, p0, Lgqr;->mResources:Landroid/content/res/Resources;

    .line 46
    iput-boolean p4, p0, Lgqr;->cTg:Z

    .line 47
    iput-object p5, p0, Lgqr;->cTh:[Ljava/lang/Object;

    .line 48
    return-void
.end method

.method private getString(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 75
    :try_start_0
    iget-object v0, p0, Lgqr;->mResources:Landroid/content/res/Resources;

    iget-object v1, p0, Lgqr;->cTh:[Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "Velvet.Actions.ActionPrompt"

    const-string v2, "Failed to format resource %s (%s) with args %s. Returning empty string."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lgqr;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-object v5, p0, Lgqr;->cTh:[Ljava/lang/Object;

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 80
    const-string v0, " "

    goto :goto_0

    .line 82
    :catch_1
    move-exception v0

    const-string v0, "Velvet.Actions.ActionPrompt"

    const-string v1, "Tried to retrieve non-existent string resource: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 87
    const-string v0, " "

    goto :goto_0
.end method


# virtual methods
.method public final Bb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lgqr;->cTf:I

    invoke-direct {p0, v0}, Lgqr;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aKc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lgqr;->cTe:I

    invoke-direct {p0, v0}, Lgqr;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
