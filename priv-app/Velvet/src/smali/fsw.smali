.class public final Lfsw;
.super Lfrk;
.source "PG"


# instance fields
.field private cBB:Ljava/text/NumberFormat;

.field private cBC:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lfrk;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 31
    return-void
.end method

.method private static mb(Ljava/lang/String;)Ljava/text/NumberFormat;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x2

    .line 83
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 84
    invoke-static {p0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 87
    :cond_0
    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 88
    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 89
    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Landroid/widget/EditText;)V
    .locals 4

    .prologue
    .line 41
    :try_start_0
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agR:Lann;

    .line 42
    iget-object v1, p0, Lfsw;->cBC:Ljava/text/NumberFormat;

    if-nez v1, :cond_0

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agR:Lann;

    invoke-virtual {v1}, Lann;->py()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfsw;->mb(Ljava/lang/String;)Ljava/text/NumberFormat;

    move-result-object v1

    iput-object v1, p0, Lfsw;->cBC:Ljava/text/NumberFormat;

    :cond_0
    iget-object v1, p0, Lfsw;->cBC:Ljava/text/NumberFormat;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0}, Lann;->pv()F

    move-result v0

    mul-float/2addr v0, v2

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 46
    new-instance v1, Lfrn;

    invoke-direct {v1, v0}, Lfrn;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method protected final aDa()Lank;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agR:Lann;

    iget-object v0, v0, Lann;->aie:Lank;

    return-object v0
.end method

.method protected final b(Ljava/lang/String;Landroid/widget/EditText;)V
    .locals 4

    .prologue
    .line 53
    :try_start_0
    iget-object v0, p0, Lfsw;->cBB:Ljava/text/NumberFormat;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agR:Lann;

    invoke-virtual {v0}, Lann;->pw()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfsw;->mb(Ljava/lang/String;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lfsw;->cBB:Ljava/text/NumberFormat;

    :cond_0
    iget-object v0, p0, Lfsw;->cBB:Ljava/text/NumberFormat;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lfro;->cBc:Lanh;

    iget-object v2, v2, Lanh;->agR:Lann;

    invoke-virtual {v2}, Lann;->pv()F

    move-result v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    new-instance v1, Lfrn;

    invoke-direct {v1, v0}, Lfrn;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
