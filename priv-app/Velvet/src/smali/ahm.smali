.class public final Lahm;
.super Lahk;
.source "PG"


# instance fields
.field private Yr:Landroid/content/pm/LauncherActivityInfo;


# direct methods
.method constructor <init>(Landroid/content/pm/LauncherActivityInfo;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lahk;-><init>()V

    .line 30
    iput-object p1, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    .line 31
    return-void
.end method


# virtual methods
.method public final getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/LauncherActivityInfo;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getBadgedIcon(I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    invoke-virtual {v0, p1}, Landroid/content/pm/LauncherActivityInfo;->getBadgedIcon(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getComponentName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/LauncherActivityInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public final getFirstInstallTime()J
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/LauncherActivityInfo;->getFirstInstallTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/LauncherActivityInfo;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final lI()Lahz;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lahm;->Yr:Landroid/content/pm/LauncherActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/LauncherActivityInfo;->getUser()Landroid/os/UserHandle;

    move-result-object v0

    invoke-static {v0}, Lahz;->a(Landroid/os/UserHandle;)Lahz;

    move-result-object v0

    return-object v0
.end method
