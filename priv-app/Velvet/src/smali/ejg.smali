.class public final Lejg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leka;


# instance fields
.field final cbt:I

.field private cbu:Z

.field private cbv:Z

.field private cbw:Z

.field cbx:I

.field cby:I

.field public cbz:Landroid/animation/ValueAnimator;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final iA:Landroid/graphics/Rect;

.field final mCallback:Lejh;

.field private final mHandleView:Landroid/view/View;

.field final mLayout:Landroid/view/ViewGroup;

.field final mScrollView:Lekf;

.field private final mTouchSlop:I


# direct methods
.method public constructor <init>(Lekf;Landroid/view/ViewGroup;Landroid/view/View;IILejh;)V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lejg;->iA:Landroid/graphics/Rect;

    .line 102
    invoke-interface {p1, p2}, Lekf;->aI(Landroid/view/View;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 103
    invoke-virtual {p3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 104
    iput-object p1, p0, Lejg;->mScrollView:Lekf;

    .line 105
    iput-object p2, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    .line 106
    iput-object p3, p0, Lejg;->mHandleView:Landroid/view/View;

    .line 107
    iput p5, p0, Lejg;->cbt:I

    .line 108
    iput-object p6, p0, Lejg;->mCallback:Lejh;

    .line 109
    iput p4, p0, Lejg;->mTouchSlop:I

    .line 110
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B(IZ)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 149
    iget-boolean v1, p0, Lejg;->cbu:Z

    if-nez v1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    iget-object v1, p0, Lejg;->mScrollView:Lekf;

    iget-object v2, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-interface {v1, v2}, Lekf;->aH(Landroid/view/View;)I

    move-result v1

    .line 152
    iget-object v2, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v2}, Lejh;->atw()I

    move-result v2

    add-int/2addr v2, v1

    .line 153
    iget-object v3, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v3}, Lejh;->atx()I

    move-result v3

    add-int/2addr v3, v2

    .line 154
    invoke-virtual {p0}, Lejg;->atv()I

    move-result v4

    .line 155
    sub-int v5, v3, v4

    .line 156
    iget v6, p0, Lejg;->cbx:I

    add-int/2addr v6, p1

    if-le v6, v5, :cond_3

    .line 158
    iput-boolean v7, p0, Lejg;->cbv:Z

    .line 159
    iput-boolean v7, p0, Lejg;->cbw:Z

    .line 161
    if-eqz p2, :cond_2

    .line 169
    :goto_1
    iget-object v5, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v5}, Lejh;->aty()I

    move-result v5

    add-int/2addr v1, v5

    .line 170
    sub-int/2addr v3, v1

    iput v3, p0, Lejg;->cbx:I

    .line 171
    sub-int/2addr v2, v1

    iput v2, p0, Lejg;->cby:I

    .line 172
    iget-object v2, p0, Lejg;->mCallback:Lejh;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, Lejh;->J(F)V

    .line 173
    iget-object v2, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v2}, Lejh;->atz()V

    .line 174
    iget-object v2, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    .line 178
    sub-int/2addr v1, v4

    add-int/2addr v0, v1

    goto :goto_0

    .line 161
    :cond_2
    iget v0, p0, Lejg;->cbx:I

    add-int/2addr v0, p1

    sub-int/2addr v0, v5

    goto :goto_1

    .line 181
    :cond_3
    iget v1, p0, Lejg;->cbx:I

    add-int/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lejg;->cbx:I

    .line 182
    iget-boolean v1, p0, Lejg;->cbw:Z

    if-nez v1, :cond_4

    iget v1, p0, Lejg;->cbx:I

    iget v2, p0, Lejg;->mTouchSlop:I

    if-le v1, v2, :cond_0

    .line 183
    :cond_4
    iput-boolean v7, p0, Lejg;->cbw:Z

    .line 186
    iget-object v1, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v1}, Lejh;->atx()I

    move-result v1

    sub-int/2addr v1, v5

    iget v2, p0, Lejg;->cbx:I

    neg-int v2, v2

    mul-int/2addr v1, v2

    div-int/2addr v1, v5

    iput v1, p0, Lejg;->cby:I

    .line 188
    iget-object v1, p0, Lejg;->mCallback:Lejh;

    iget v2, p0, Lejg;->cbx:I

    int-to-float v2, v2

    int-to-float v3, v5

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, Lejh;->J(F)V

    .line 189
    iget-object v1, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    goto :goto_0
.end method


# virtual methods
.method public final aJ(Landroid/view/View;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 223
    iget-boolean v1, p0, Lejg;->cbw:Z

    if-nez v1, :cond_0

    .line 226
    :goto_0
    return v0

    .line 225
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v2}, Lejh;->atw()I

    move-result v2

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    .line 226
    :cond_1
    if-eqz v0, :cond_2

    iget v0, p0, Lejg;->cbx:I

    neg-int v0, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lejg;->cby:I

    neg-int v0, v0

    goto :goto_0
.end method

.method public final att()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lejg;->cbv:Z

    return v0
.end method

.method public final atu()V
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lejg;->cbv:Z

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v0}, Lejh;->atA()V

    .line 205
    invoke-virtual {p0}, Lejg;->reset()V

    .line 219
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-boolean v0, p0, Lejg;->cbw:Z

    if-eqz v0, :cond_1

    .line 211
    new-instance v0, Leji;

    invoke-direct {v0, p0}, Leji;-><init>(Lejg;)V

    iput-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    .line 212
    iget-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lejg;->mHandleView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    goto :goto_0
.end method

.method public final atv()I
    .locals 5

    .prologue
    .line 243
    iget-object v0, p0, Lejg;->mScrollView:Lekf;

    iget-object v1, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Lekf;->aH(Landroid/view/View;)I

    move-result v0

    .line 244
    iget-object v1, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v1}, Lejh;->atw()I

    move-result v1

    add-int/2addr v1, v0

    .line 245
    iget-object v2, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v2}, Lejh;->atx()I

    move-result v2

    .line 246
    div-int/lit8 v3, v2, 0x2

    add-int/2addr v3, v1

    .line 249
    iget-object v4, p0, Lejg;->mCallback:Lejh;

    invoke-interface {v4}, Lejh;->aty()I

    move-result v4

    add-int/2addr v4, v0

    .line 250
    add-int v0, v4, v1

    .line 251
    if-le v3, v0, :cond_0

    .line 260
    :goto_0
    return v0

    .line 257
    :cond_0
    add-int v0, v1, v2

    .line 258
    sub-int v2, v0, v4

    .line 259
    iget-object v4, p0, Lejg;->mScrollView:Lekf;

    invoke-interface {v4}, Lekf;->getMaxScrollY()I

    move-result v4

    sub-int v1, v4, v1

    add-int/2addr v1, v2

    .line 260
    sub-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final hO(I)I
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lejg;->cbv:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 135
    :cond_0
    :goto_0
    return p1

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lejg;->B(IZ)I

    move-result p1

    goto :goto_0
.end method

.method public final hP(I)I
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lejg;->cbv:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 145
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lejg;->B(IZ)I

    move-result v0

    goto :goto_0
.end method

.method public final reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    iget-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 266
    iput-boolean v1, p0, Lejg;->cbv:Z

    .line 267
    iput-boolean v1, p0, Lejg;->cbu:Z

    .line 268
    iput v1, p0, Lejg;->cbx:I

    .line 269
    iput v1, p0, Lejg;->cby:I

    .line 270
    iput-boolean v1, p0, Lejg;->cbw:Z

    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    .line 272
    iget-object v0, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    .line 273
    return-void

    :cond_1
    move v0, v1

    .line 265
    goto :goto_0
.end method

.method public final t(FF)I
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 116
    iget-object v0, p0, Lejg;->mHandleView:Landroid/view/View;

    iget-object v1, p0, Lejg;->iA:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 117
    iget-object v0, p0, Lejg;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    add-float v1, p2, v3

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lejg;->mScrollView:Lekf;

    iget-object v2, p0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-interface {v1, v2}, Lekf;->aH(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 119
    iget-object v1, p0, Lejg;->iA:Landroid/graphics/Rect;

    add-float v2, p1, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    iput-boolean v0, p0, Lejg;->cbu:Z

    .line 122
    iget-boolean v0, p0, Lejg;->cbu:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 126
    :cond_0
    iget-boolean v0, p0, Lejg;->cbu:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
