.class public final Lcrc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcrf;


# instance fields
.field private final bgT:Lcrf;

.field private final bgU:Lcrf;

.field private bgV:Ljava/lang/Object;

.field private bgW:Lcrf;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcrf;Lcrf;Lcha;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcrc;->bgV:Ljava/lang/Object;

    .line 46
    iput-object p1, p0, Lcrc;->bgT:Lcrf;

    .line 47
    iput-object p2, p0, Lcrc;->bgU:Lcrf;

    .line 48
    new-instance v0, Lcrd;

    invoke-direct {v0, p0}, Lcrd;-><init>(Lcrc;)V

    invoke-virtual {p3, v0}, Lcha;->a(Lchg;)V

    .line 55
    new-instance v0, Lcre;

    invoke-direct {v0, p0}, Lcre;-><init>(Lcrc;)V

    invoke-virtual {p3, v0}, Lcha;->b(Lemy;)V

    .line 62
    return-void
.end method

.method private Sp()Lcrf;
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lcrc;->bgV:Ljava/lang/Object;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lcrc;->bgW:Lcrf;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcrf;)V
    .locals 2

    .prologue
    .line 71
    iget-object v1, p0, Lcrc;->bgV:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iput-object p1, p0, Lcrc;->bgW:Lcrf;

    .line 73
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final So()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 89
    invoke-direct {p0}, Lcrc;->Sp()Lcrf;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    .line 92
    :try_start_0
    const-string v1, "FallingBackGoogleAuthAdapter: try chosen adapter"

    invoke-virtual {p5, v1}, Leua;->beginSection(Ljava/lang/String;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 93
    invoke-interface/range {v0 .. v5}, Lcrf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 96
    invoke-virtual {p5}, Leua;->endSection()V

    .line 113
    :goto_0
    return-object v0

    .line 96
    :catchall_0
    move-exception v0

    invoke-virtual {p5}, Leua;->endSection()V

    throw v0

    .line 101
    :cond_0
    :try_start_1
    const-string v0, "FallingBackGoogleAuthAdapter: try GMS core"

    invoke-virtual {p5, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcrc;->bgT:Lcrf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcrf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 105
    :try_start_2
    invoke-virtual {p5}, Leua;->endSection()V
    :try_end_2
    .catch Lbfb; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 109
    :try_start_3
    const-string v0, "FallingBackGoogleAuthAdapter: try account manager"

    invoke-virtual {p5, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcrc;->bgU:Lcrf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcrf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    .line 113
    invoke-virtual {p5}, Leua;->endSection()V

    goto :goto_0

    .line 105
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-virtual {p5}, Leua;->endSection()V

    throw v0
    :try_end_4
    .catch Lbfb; {:try_start_4 .. :try_end_4} :catch_0

    .line 113
    :catchall_2
    move-exception v0

    invoke-virtual {p5}, Leua;->endSection()V

    throw v0
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcrc;->Sp()Lcrf;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-direct {p0}, Lcrc;->Sp()Lcrf;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcrf;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcrc;->bgT:Lcrf;

    invoke-interface {v0, p1, p2}, Lcrf;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcrc;->bgU:Lcrf;

    invoke-interface {v0, p1, p2}, Lcrf;->d(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final fo(I)V
    .locals 1

    .prologue
    .line 78
    if-eqz p1, :cond_0

    .line 79
    iget-object v0, p0, Lcrc;->bgU:Lcrf;

    invoke-direct {p0, v0}, Lcrc;->a(Lcrf;)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcrc;->bgT:Lcrf;

    invoke-direct {p0, v0}, Lcrc;->a(Lcrf;)V

    goto :goto_0
.end method
