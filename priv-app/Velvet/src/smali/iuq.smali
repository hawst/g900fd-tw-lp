.class final Liuq;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 180
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    return-void
.end method

.method static a(DZ)D
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    const-wide/16 v4, 0x0

    .line 39
    cmpl-double v0, p0, v4

    if-nez v0, :cond_1

    .line 40
    if-eqz p2, :cond_0

    const-wide/16 v0, 0x1

    .line 48
    :goto_0
    return-wide v0

    .line 40
    :cond_0
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0

    .line 42
    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v2

    .line 43
    cmpg-double v0, p0, v4

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-ne v0, p2, :cond_3

    .line 44
    sub-long v0, v2, v6

    .line 48
    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    goto :goto_0

    .line 43
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 46
    :cond_3
    add-long v0, v2, v6

    goto :goto_2
.end method

.method static getExponent(D)I
    .locals 4

    .prologue
    .line 79
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 80
    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    and-long/2addr v0, v2

    const/16 v2, 0x34

    shr-long/2addr v0, v2

    long-to-int v0, v0

    .line 81
    add-int/lit16 v0, v0, -0x3ff

    .line 82
    return v0
.end method

.method static k(D)J
    .locals 6

    .prologue
    .line 110
    invoke-static {p0, p1}, Liuq;->l(D)Z

    move-result v0

    const-string v1, "not a normal value"

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 111
    invoke-static {p0, p1}, Liuq;->getExponent(D)I

    move-result v0

    .line 112
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v2

    .line 113
    const-wide v4, 0xfffffffffffffL

    and-long/2addr v2, v4

    .line 114
    const/16 v1, -0x3ff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    shl-long v0, v2, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x10000000000000L

    or-long/2addr v0, v2

    goto :goto_0
.end method

.method static l(D)Z
    .locals 2

    .prologue
    .line 120
    invoke-static {p0, p1}, Liuq;->getExponent(D)I

    move-result v0

    const/16 v1, 0x3ff

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
