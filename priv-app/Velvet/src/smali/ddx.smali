.class public Lddx;
.super Ldei;
.source "PG"

# interfaces
.implements Lddw;


# direct methods
.method public constructor <init>(Ldef;)V
    .locals 2

    .prologue
    .line 41
    invoke-interface {p1}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 42
    invoke-virtual {p0, p1}, Lddx;->c(Ljava/lang/Iterable;)I

    .line 43
    invoke-interface {p1}, Ldef;->abd()Z

    move-result v0

    iput-boolean v0, p0, Ldei;->bvo:Z

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(ILcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lddx;->bub:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method

.method public final aaZ()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddx;->bvn:Z

    .line 79
    return-void
.end method

.method public final c(Ljava/lang/Iterable;)I
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 69
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 70
    invoke-virtual {p0, v0}, Lddx;->d(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 71
    goto :goto_0

    .line 73
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public d(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lddx;->bub:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public remove(I)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lddx;->bub:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 55
    return-void
.end method
