.class public final Lcds;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lbzh;Ljava/lang/String;I)I
    .locals 6

    .prologue
    .line 40
    :try_start_0
    invoke-interface {p0}, Lbzh;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const-string v1, "S3NetworkUtils"

    const-string v2, "[%s] error getting response code: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 43
    new-instance v1, Leie;

    invoke-direct {v1, v0, p2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method

.method public static a(ILjava/net/URL;Lbzh;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 61
    invoke-interface {p2}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    const-string v1, "S3NetworkUtils"

    const-string v2, "[%s] unexpected redirect to %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p3, v3, v5

    aput-object v0, v3, v6

    invoke-static {v8, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 66
    new-instance v1, Leie;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected redirect to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, 0x1000f

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 69
    :cond_0
    const/16 v0, 0xc8

    if-eq p0, v0, :cond_3

    .line 70
    const-string v0, "X-Speech-S3-Res-Code"

    invoke-interface {p2, v0}, Lbzh;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 74
    :goto_0
    if-eqz v0, :cond_2

    .line 75
    const-string v2, "S3NetworkUtils"

    const-string v3, "[%s] response code: %d, internal error header: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p3, v4, v5

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    invoke-static {v8, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 77
    new-instance v1, Leii;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v0}, Leii;-><init>(I)V

    throw v1

    .line 72
    :cond_1
    invoke-static {v1}, Lcds;->gc(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_2
    const-string v0, "S3NetworkUtils"

    const-string v1, "[%s] response code: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p3, v2, v5

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v8, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 80
    new-instance v0, Leif;

    invoke-direct {v0, p0}, Leif;-><init>(I)V

    throw v0

    .line 87
    :cond_3
    return-void
.end method

.method private static gc(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 97
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 99
    :catch_0
    move-exception v0

    const-string v0, "S3NetworkUtils"

    const-string v1, "Failed to parse error header: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 100
    const/4 v0, 0x0

    goto :goto_0
.end method
