.class final Laae;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic Nf:Lzi;


# direct methods
.method private constructor <init>(Lzi;)V
    .locals 0

    .prologue
    .line 2926
    iput-object p1, p0, Laae;->Nf:Lzi;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lzi;B)V
    .locals 0

    .prologue
    .line 2926
    invoke-direct {p0, p1}, Laae;-><init>(Lzi;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 2930
    sget-object v2, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v2

    .line 2931
    :try_start_0
    iget-object v0, p0, Laae;->Nf:Lzi;

    invoke-static {v0}, Lzi;->j(Lzi;)Lyu;

    sget-object v0, Lyu;->Mi:Landroid/content/Context;

    invoke-static {v0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v3

    .line 2934
    sget-object v0, Lzi;->Nc:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2935
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lahz;

    .line 2936
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2937
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2938
    invoke-virtual {v3, v0, v1}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2939
    const-string v7, "Launcher.Model"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Package not found: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2940
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2949
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2943
    :cond_2
    :try_start_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2944
    iget-object v0, p0, Laae;->Nf:Lzi;

    new-instance v6, Laau;

    iget-object v7, p0, Laae;->Nf:Lzi;

    const/4 v8, 0x3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v6, v7, v8, v0, v1}, Laau;-><init>(Lzi;I[Ljava/lang/String;Lahz;)V

    invoke-static {v6}, Lzi;->a(Laau;)V

    goto :goto_0

    .line 2948
    :cond_3
    sget-object v0, Lzi;->Nc:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2949
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
