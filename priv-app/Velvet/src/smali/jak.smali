.class public final Ljak;
.super Ljsl;
.source "PG"


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private afZ:Ljava/lang/String;

.field public clz:Ljcu;

.field private dJD:I

.field private dLZ:[B

.field public dMX:[Ljbg;

.field private dMY:I

.field private dWG:Z

.field public dWH:[Ljcu;

.field private dWI:Z

.field private dWJ:Ljava/lang/String;

.field private dWK:I

.field private dWw:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 21215
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 21216
    iput v1, p0, Ljak;->aez:I

    iput-object v2, p0, Ljak;->aeB:Ljbp;

    iput-object v2, p0, Ljak;->clz:Ljcu;

    iput-boolean v1, p0, Ljak;->dWG:Z

    invoke-static {}, Ljcu;->bgP()[Ljcu;

    move-result-object v0

    iput-object v0, p0, Ljak;->dWH:[Ljcu;

    iput v3, p0, Ljak;->dJD:I

    invoke-static {}, Ljbg;->beN()[Ljbg;

    move-result-object v0

    iput-object v0, p0, Ljak;->dMX:[Ljbg;

    iput v1, p0, Ljak;->dMY:I

    iput-boolean v1, p0, Ljak;->dWI:Z

    const-string v0, ""

    iput-object v0, p0, Ljak;->dWJ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljak;->dWw:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljak;->afZ:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljak;->dLZ:[B

    iput v3, p0, Ljak;->dWK:I

    iput-object v2, p0, Ljak;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljak;->eCz:I

    .line 21217
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20984
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljak;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljak;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljak;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljak;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljak;->clz:Ljcu;

    if-nez v0, :cond_2

    new-instance v0, Ljcu;

    invoke-direct {v0}, Ljcu;-><init>()V

    iput-object v0, p0, Ljak;->clz:Ljcu;

    :cond_2
    iget-object v0, p0, Ljak;->clz:Ljcu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljak;->dWG:Z

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljak;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x62

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljak;->dWH:[Ljcu;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljcu;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljak;->dWH:[Ljcu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljcu;

    invoke-direct {v3}, Ljcu;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljak;->dWH:[Ljcu;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljcu;

    invoke-direct {v3}, Ljcu;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljak;->dWH:[Ljcu;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    :pswitch_1
    iput v0, p0, Ljak;->dJD:I

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x72

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljak;->dMX:[Ljbg;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbg;

    if-eqz v0, :cond_6

    iget-object v3, p0, Ljak;->dMX:[Ljbg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Ljbg;

    invoke-direct {v3}, Ljbg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ljak;->dMX:[Ljbg;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Ljbg;

    invoke-direct {v3}, Ljbg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljak;->dMX:[Ljbg;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljak;->dWI:Z

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljak;->dWJ:Ljava/lang/String;

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljak;->afZ:Ljava/lang/String;

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljak;->dLZ:[B

    iget v0, p0, Ljak;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Ljak;->dWK:I

    iget v0, p0, Ljak;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljak;->dWw:Ljava/lang/String;

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljak;->dMY:I

    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljak;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x52 -> :sswitch_2
        0x58 -> :sswitch_3
        0x62 -> :sswitch_4
        0x68 -> :sswitch_5
        0x72 -> :sswitch_6
        0x78 -> :sswitch_7
        0x82 -> :sswitch_8
        0x8a -> :sswitch_9
        0x92 -> :sswitch_a
        0x98 -> :sswitch_b
        0xa2 -> :sswitch_c
        0xb0 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 21242
    iget-object v0, p0, Ljak;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 21243
    const/4 v0, 0x2

    iget-object v2, p0, Ljak;->aeB:Ljbp;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 21245
    :cond_0
    iget-object v0, p0, Ljak;->clz:Ljcu;

    if-eqz v0, :cond_1

    .line 21246
    const/16 v0, 0xa

    iget-object v2, p0, Ljak;->clz:Ljcu;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 21248
    :cond_1
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 21249
    const/16 v0, 0xb

    iget-boolean v2, p0, Ljak;->dWG:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 21251
    :cond_2
    iget-object v0, p0, Ljak;->dWH:[Ljcu;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljak;->dWH:[Ljcu;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 21252
    :goto_0
    iget-object v2, p0, Ljak;->dWH:[Ljcu;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 21253
    iget-object v2, p0, Ljak;->dWH:[Ljcu;

    aget-object v2, v2, v0

    .line 21254
    if-eqz v2, :cond_3

    .line 21255
    const/16 v3, 0xc

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 21252
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 21259
    :cond_4
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 21260
    const/16 v0, 0xd

    iget v2, p0, Ljak;->dJD:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 21262
    :cond_5
    iget-object v0, p0, Ljak;->dMX:[Ljbg;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljak;->dMX:[Ljbg;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 21263
    :goto_1
    iget-object v0, p0, Ljak;->dMX:[Ljbg;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 21264
    iget-object v0, p0, Ljak;->dMX:[Ljbg;

    aget-object v0, v0, v1

    .line 21265
    if-eqz v0, :cond_6

    .line 21266
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 21263
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21270
    :cond_7
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 21271
    const/16 v0, 0xf

    iget-boolean v1, p0, Ljak;->dWI:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 21273
    :cond_8
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 21274
    const/16 v0, 0x10

    iget-object v1, p0, Ljak;->dWJ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 21276
    :cond_9
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_a

    .line 21277
    const/16 v0, 0x11

    iget-object v1, p0, Ljak;->afZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 21279
    :cond_a
    iget v0, p0, Ljak;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_b

    .line 21280
    const/16 v0, 0x12

    iget-object v1, p0, Ljak;->dLZ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 21282
    :cond_b
    iget v0, p0, Ljak;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_c

    .line 21283
    const/16 v0, 0x13

    iget v1, p0, Ljak;->dWK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 21285
    :cond_c
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_d

    .line 21286
    const/16 v0, 0x14

    iget-object v1, p0, Ljak;->dWw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 21288
    :cond_d
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_e

    .line 21289
    const/16 v0, 0x16

    iget v1, p0, Ljak;->dMY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 21291
    :cond_e
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 21292
    return-void
.end method

.method public final bdS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21133
    iget-object v0, p0, Ljak;->dWw:Ljava/lang/String;

    return-object v0
.end method

.method public final bdT()Z
    .locals 1

    .prologue
    .line 21144
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bed()I
    .locals 1

    .prologue
    .line 21051
    iget v0, p0, Ljak;->dJD:I

    return v0
.end method

.method public final bee()Z
    .locals 1

    .prologue
    .line 21059
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bef()Z
    .locals 1

    .prologue
    .line 21092
    iget-boolean v0, p0, Ljak;->dWI:Z

    return v0
.end method

.method public final beg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21111
    iget-object v0, p0, Ljak;->dWJ:Ljava/lang/String;

    return-object v0
.end method

.method public final beh()Z
    .locals 1

    .prologue
    .line 21122
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bei()I
    .locals 1

    .prologue
    .line 21199
    iget v0, p0, Ljak;->dWK:I

    return v0
.end method

.method public final bej()Z
    .locals 1

    .prologue
    .line 21207
    iget v0, p0, Ljak;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hA(Z)Ljak;
    .locals 1

    .prologue
    .line 21095
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljak;->dWI:Z

    .line 21096
    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljak;->aez:I

    .line 21097
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 21296
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 21297
    iget-object v2, p0, Ljak;->aeB:Ljbp;

    if-eqz v2, :cond_0

    .line 21298
    const/4 v2, 0x2

    iget-object v3, p0, Ljak;->aeB:Ljbp;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 21301
    :cond_0
    iget-object v2, p0, Ljak;->clz:Ljcu;

    if-eqz v2, :cond_1

    .line 21302
    const/16 v2, 0xa

    iget-object v3, p0, Ljak;->clz:Ljcu;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 21305
    :cond_1
    iget v2, p0, Ljak;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 21306
    const/16 v2, 0xb

    iget-boolean v3, p0, Ljak;->dWG:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 21309
    :cond_2
    iget-object v2, p0, Ljak;->dWH:[Ljcu;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljak;->dWH:[Ljcu;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 21310
    :goto_0
    iget-object v3, p0, Ljak;->dWH:[Ljcu;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 21311
    iget-object v3, p0, Ljak;->dWH:[Ljcu;

    aget-object v3, v3, v0

    .line 21312
    if-eqz v3, :cond_3

    .line 21313
    const/16 v4, 0xc

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 21310
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 21318
    :cond_5
    iget v2, p0, Ljak;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_6

    .line 21319
    const/16 v2, 0xd

    iget v3, p0, Ljak;->dJD:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 21322
    :cond_6
    iget-object v2, p0, Ljak;->dMX:[Ljbg;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljak;->dMX:[Ljbg;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 21323
    :goto_1
    iget-object v2, p0, Ljak;->dMX:[Ljbg;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 21324
    iget-object v2, p0, Ljak;->dMX:[Ljbg;

    aget-object v2, v2, v1

    .line 21325
    if-eqz v2, :cond_7

    .line 21326
    const/16 v3, 0xe

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 21323
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 21331
    :cond_8
    iget v1, p0, Ljak;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_9

    .line 21332
    const/16 v1, 0xf

    iget-boolean v2, p0, Ljak;->dWI:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 21335
    :cond_9
    iget v1, p0, Ljak;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 21336
    const/16 v1, 0x10

    iget-object v2, p0, Ljak;->dWJ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21339
    :cond_a
    iget v1, p0, Ljak;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_b

    .line 21340
    const/16 v1, 0x11

    iget-object v2, p0, Ljak;->afZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21343
    :cond_b
    iget v1, p0, Ljak;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_c

    .line 21344
    const/16 v1, 0x12

    iget-object v2, p0, Ljak;->dLZ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 21347
    :cond_c
    iget v1, p0, Ljak;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_d

    .line 21348
    const/16 v1, 0x13

    iget v2, p0, Ljak;->dWK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 21351
    :cond_d
    iget v1, p0, Ljak;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_e

    .line 21352
    const/16 v1, 0x14

    iget-object v2, p0, Ljak;->dWw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21355
    :cond_e
    iget v1, p0, Ljak;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_f

    .line 21356
    const/16 v1, 0x16

    iget v2, p0, Ljak;->dMY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 21359
    :cond_f
    return v0
.end method

.method public final oh(I)Ljak;
    .locals 1

    .prologue
    .line 21054
    iput p1, p0, Ljak;->dJD:I

    .line 21055
    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljak;->aez:I

    .line 21056
    return-object p0
.end method

.method public final oo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21155
    iget-object v0, p0, Ljak;->afZ:Ljava/lang/String;

    return-object v0
.end method

.method public final pf()Z
    .locals 1

    .prologue
    .line 21166
    iget v0, p0, Ljak;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final sd(Ljava/lang/String;)Ljak;
    .locals 1

    .prologue
    .line 21114
    if-nez p1, :cond_0

    .line 21115
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21117
    :cond_0
    iput-object p1, p0, Ljak;->dWJ:Ljava/lang/String;

    .line 21118
    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljak;->aez:I

    .line 21119
    return-object p0
.end method

.method public final se(Ljava/lang/String;)Ljak;
    .locals 1

    .prologue
    .line 21136
    if-nez p1, :cond_0

    .line 21137
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21139
    :cond_0
    iput-object p1, p0, Ljak;->dWw:Ljava/lang/String;

    .line 21140
    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljak;->aez:I

    .line 21141
    return-object p0
.end method

.method public final sf(Ljava/lang/String;)Ljak;
    .locals 1

    .prologue
    .line 21158
    if-nez p1, :cond_0

    .line 21159
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21161
    :cond_0
    iput-object p1, p0, Ljak;->afZ:Ljava/lang/String;

    .line 21162
    iget v0, p0, Ljak;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljak;->aez:I

    .line 21163
    return-object p0
.end method
