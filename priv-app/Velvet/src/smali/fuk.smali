.class public final Lfuk;
.super Lfro;
.source "PG"


# static fields
.field private static final cCe:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    invoke-static {}, Lijm;->aWZ()Lijn;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b00ba

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b00b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b00b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0b00b9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    move-result-object v0

    sput-object v0, Lfuk;->cCe:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 6

    .prologue
    .line 41
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v2, v0, Lanh;->agT:Laog;

    .line 43
    invoke-virtual {v2}, Laog;->pb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const v0, 0x7f1101a9

    invoke-virtual {v2}, Laog;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 47
    :cond_0
    iget-object v0, v2, Laog;->ajr:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v2, Laog;->ajr:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 48
    iget-object v0, v2, Laog;->ajr:[Ljava/lang/String;

    array-length v0, v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 49
    iget-object v0, v2, Laog;->ajr:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    iget-object v4, p0, Lfuk;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v0, v4}, Lgab;->d(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    const/4 v0, 0x1

    :goto_0
    iget-object v4, v2, Laog;->ajr:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 53
    iget-object v4, v2, Laog;->ajr:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_1
    const v0, 0x7f110080

    const-string v4, " \u00b7 "

    invoke-static {v4, v3}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 60
    :cond_2
    invoke-virtual {v2}, Laog;->pC()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    iget-object v3, p0, Lfuk;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Laog;->pB()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lfuk;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v0, Lfuk;->cCe:Ljava/util/Map;

    invoke-virtual {v2}, Laog;->getStatusCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v3, v4, v0}, Lgab;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 65
    const v2, 0x7f110321

    invoke-static {v1, v2, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 67
    :cond_3
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 36
    const v0, 0x7f040120

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
