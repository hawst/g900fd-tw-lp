.class public interface abstract Lbkn;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
.end method

.method public abstract a(Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
.end method

.method public abstract a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/SuggestSpecification;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
.end method

.method public abstract a(Lcom/google/android/gms/appdatasearch/ac;)Z
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
.end method

.method public abstract a([BZ)Z
.end method

.method public abstract b(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
.end method

.method public abstract b(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
.end method

.method public abstract c(Ljava/lang/String;Z)V
.end method

.method public abstract d([Ljava/lang/String;)V
.end method

.method public abstract e([Ljava/lang/String;)V
.end method

.method public abstract fl(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public abstract fm(Ljava/lang/String;)V
.end method

.method public abstract fn(Ljava/lang/String;)Z
.end method

.method public abstract fo(Ljava/lang/String;)V
.end method

.method public abstract fp(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public abstract fq(Ljava/lang/String;)[Ljava/lang/String;
.end method

.method public abstract p(Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method public abstract p(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
.end method

.method public abstract q(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract r(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
.end method

.method public abstract wO()[I
.end method

.method public abstract wP()[I
.end method

.method public abstract zc()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
.end method

.method public abstract zd()V
.end method

.method public abstract ze()Lcom/google/android/gms/appdatasearch/StorageStats;
.end method

.method public abstract zf()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
.end method

.method public abstract zg()Z
.end method

.method public abstract zh()Lcom/google/android/gms/appdatasearch/NativeApiInfo;
.end method
