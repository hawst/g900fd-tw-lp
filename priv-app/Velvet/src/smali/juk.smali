.class public final Ljuk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eFl:Ljava/lang/String;

.field private eFm:Ljava/lang/String;

.field private eFn:Ljava/lang/String;

.field private eFo:Ljava/lang/String;

.field private eFp:Ljava/lang/String;

.field private eFq:[Ljuj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 590
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 591
    const/4 v0, 0x0

    iput v0, p0, Ljuk;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljuk;->eFl:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuk;->eFm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuk;->eFn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuk;->eFo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuk;->eFp:Ljava/lang/String;

    invoke-static {}, Ljuj;->buI()[Ljuj;

    move-result-object v0

    iput-object v0, p0, Ljuk;->eFq:[Ljuj;

    const/4 v0, 0x0

    iput-object v0, p0, Ljuk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuk;->eCz:I

    .line 592
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 515
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuk;->eFl:Ljava/lang/String;

    iget v0, p0, Ljuk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuk;->eFm:Ljava/lang/String;

    iget v0, p0, Ljuk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuk;->eFn:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuk;->eFo:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuk;->eFp:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljuk;->eFq:[Ljuj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljuj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljuk;->eFq:[Ljuj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljuj;

    invoke-direct {v3}, Ljuj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljuk;->eFq:[Ljuj;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljuj;

    invoke-direct {v3}, Ljuj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljuk;->eFq:[Ljuj;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 610
    iget v0, p0, Ljuk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 611
    const/4 v0, 0x1

    iget-object v1, p0, Ljuk;->eFl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 613
    :cond_0
    iget v0, p0, Ljuk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 614
    const/4 v0, 0x2

    iget-object v1, p0, Ljuk;->eFm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 616
    :cond_1
    const/4 v0, 0x3

    iget-object v1, p0, Ljuk;->eFn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 617
    const/4 v0, 0x4

    iget-object v1, p0, Ljuk;->eFo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 618
    const/4 v0, 0x5

    iget-object v1, p0, Ljuk;->eFp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 619
    iget-object v0, p0, Ljuk;->eFq:[Ljuj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljuk;->eFq:[Ljuj;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 620
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljuk;->eFq:[Ljuj;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 621
    iget-object v1, p0, Ljuk;->eFq:[Ljuj;

    aget-object v1, v1, v0

    .line 622
    if-eqz v1, :cond_2

    .line 623
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 620
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 627
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 628
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 632
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 633
    iget v1, p0, Ljuk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 634
    const/4 v1, 0x1

    iget-object v2, p0, Ljuk;->eFl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 637
    :cond_0
    iget v1, p0, Ljuk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 638
    const/4 v1, 0x2

    iget-object v2, p0, Ljuk;->eFm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 641
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Ljuk;->eFn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 643
    const/4 v1, 0x4

    iget-object v2, p0, Ljuk;->eFo:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 645
    const/4 v1, 0x5

    iget-object v2, p0, Ljuk;->eFp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v1, v0

    .line 647
    iget-object v0, p0, Ljuk;->eFq:[Ljuj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljuk;->eFq:[Ljuj;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 648
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljuk;->eFq:[Ljuj;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 649
    iget-object v2, p0, Ljuk;->eFq:[Ljuj;

    aget-object v2, v2, v0

    .line 650
    if-eqz v2, :cond_2

    .line 651
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 648
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 656
    :cond_3
    return v1
.end method
