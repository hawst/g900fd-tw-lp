.class public final Lbyl;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static BJ()Lhym;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    return-object v0
.end method

.method public static BK()Lcke;
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    return-object v0
.end method

.method private static BL()Lchk;
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    return-object v0
.end method

.method public static BM()Z
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcgg;->aVr:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->HO()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTP()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static BW()Z
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DX()Lenm;

    move-result-object v0

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTG()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->JJ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static BX()Z
    .locals 2

    .prologue
    .line 135
    sget-object v0, Lcgg;->aVv:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTG()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final BN()Z
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Lbyl;->BM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-static {}, Lbyl;->BK()Lcke;

    move-result-object v1

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->oC(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BO()Z
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTP()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BP()Z
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcgg;->aVw:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTU()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BQ()Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcgg;->aVx:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BX()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTV()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BR()Z
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lbyl;->BT()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->FG()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTZ()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aTW()I

    move-result v0

    invoke-static {}, Lbyl;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->JM()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BS()Z
    .locals 3

    .prologue
    .line 94
    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-virtual {p0}, Lbyl;->BT()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lbyl;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v2}, Lchk;->FG()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lhym;->oN(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BT()Z
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->aUa()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BU()Z
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->BU()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final BV()Z
    .locals 1

    .prologue
    .line 112
    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-virtual {v0}, Lhym;->BV()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
