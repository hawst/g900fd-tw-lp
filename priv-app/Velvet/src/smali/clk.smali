.class public final Lclk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "heard"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.ears.heard.EarsContentProvider"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lclk;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lclk;->mContentResolver:Landroid/content/ContentResolver;

    .line 64
    iput-object p2, p0, Lclk;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 65
    return-void
.end method

.method private a(Lieb;JZZ)J
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 92
    iget-object v0, p1, Lieb;->dzA:[Lidz;

    invoke-static {v0}, Lcll;->a([Lidz;)Lidz;

    move-result-object v4

    .line 93
    iget-object v0, v4, Lidz;->dzv:Liee;

    if-nez v0, :cond_1

    move-wide p2, v2

    .line 110
    :cond_0
    :goto_0
    return-wide p2

    .line 97
    :cond_1
    invoke-virtual {p1}, Lieb;->aVg()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    iget-object v7, v4, Lidz;->dzv:Liee;

    const-string v0, "_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "resultType"

    iget-object v0, v4, Lidz;->dzv:Liee;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "deleted"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "synced"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "refId"

    invoke-virtual {v4}, Lidz;->aVf()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "album"

    invoke-virtual {v7}, Liee;->aVp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "albumArtUrl"

    invoke-virtual {v7}, Liee;->aVr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "signedInAlbumArtUrl"

    invoke-virtual {v7}, Liee;->aVt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "artist"

    invoke-virtual {v7}, Liee;->aVj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Liee;->aVm()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "artistId"

    invoke-virtual {v7}, Liee;->aVl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v0, "track"

    invoke-virtual {v7}, Liee;->aVn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7}, Lcll;->a(Liee;)Lief;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "productId"

    invoke-virtual {v0}, Lief;->aVx()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "productParentId"

    invoke-virtual {v0}, Lief;->aVy()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lclk;->Pz()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "countryCode"

    invoke-virtual {v6, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_4
    const/4 v0, 0x0

    .line 102
    :try_start_0
    iget-object v1, p0, Lclk;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lclk;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v4, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 110
    :goto_2
    if-nez v0, :cond_0

    move-wide p2, v2

    goto/16 :goto_0

    .line 97
    :cond_5
    iget-object v0, v4, Lidz;->dzw:Lieh;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_6
    const/4 v0, 0x2

    goto/16 :goto_1

    :catch_0
    move-exception v1

    goto :goto_2

    .line 109
    :catch_1
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public final Pz()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 77
    :try_start_0
    iget-object v1, p0, Lclk;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v2, "com.google.android.ears"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 83
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v2, 0xa

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    .line 81
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b(Lieb;)J
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 71
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    .line 72
    invoke-direct/range {v0 .. v5}, Lclk;->a(Lieb;JZZ)J

    move-result-wide v0

    return-wide v0
.end method
