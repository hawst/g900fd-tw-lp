.class final Lctn;
.super Lcte;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 219
    const-string v0, "data1"

    const-string v1, "data2"

    const-string v2, "data3"

    invoke-direct {p0, p1, v0, v1, v2}, Lcte;-><init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-static {p1, p2, p3}, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcud;JLjava/lang/String;ILjava/lang/String;I)V
    .locals 10

    .prologue
    .line 226
    iget-wide v0, p1, Lcud;->biV:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "contactId must be set"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    iget-object v0, p1, Lcud;->biR:Lcue;

    iget-object v0, v0, Lcue;->bjc:Ljava/lang/StringBuilder;

    invoke-static {v0, p4}, Lcue;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    iget-object v8, p1, Lcud;->biU:Lcuh;

    iget-wide v0, p1, Lcud;->biV:J

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v7}, Lcto;->c(JJLjava/lang/String;ILjava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, v8, Lcuh;->bji:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
