.class public final Ljzg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eNw:[Ljzg;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private eNi:Ljzx;

.field public eNj:Ljzt;

.field private eNk:Ljzm;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4922
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4923
    const/4 v0, 0x0

    iput v0, p0, Ljzg;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzg;->akf:Ljava/lang/String;

    iput-object v1, p0, Ljzg;->eNi:Ljzx;

    iput-object v1, p0, Ljzg;->eNk:Ljzm;

    iput-object v1, p0, Ljzg;->eNj:Ljzt;

    iput-object v1, p0, Ljzg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzg;->eCz:I

    .line 4924
    return-void
.end method

.method public static bwO()[Ljzg;
    .locals 2

    .prologue
    .line 4878
    sget-object v0, Ljzg;->eNw:[Ljzg;

    if-nez v0, :cond_1

    .line 4879
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 4881
    :try_start_0
    sget-object v0, Ljzg;->eNw:[Ljzg;

    if-nez v0, :cond_0

    .line 4882
    const/4 v0, 0x0

    new-array v0, v0, [Ljzg;

    sput-object v0, Ljzg;->eNw:[Ljzg;

    .line 4884
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4886
    :cond_1
    sget-object v0, Ljzg;->eNw:[Ljzg;

    return-object v0

    .line 4884
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Ac(Ljava/lang/String;)Ljzg;
    .locals 1

    .prologue
    .line 4897
    if-nez p1, :cond_0

    .line 4898
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4900
    :cond_0
    iput-object p1, p0, Ljzg;->akf:Ljava/lang/String;

    .line 4901
    iget v0, p0, Ljzg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzg;->aez:I

    .line 4902
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4872
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzg;->akf:Ljava/lang/String;

    iget v0, p0, Ljzg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzg;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljzg;->eNi:Ljzx;

    if-nez v0, :cond_1

    new-instance v0, Ljzx;

    invoke-direct {v0}, Ljzx;-><init>()V

    iput-object v0, p0, Ljzg;->eNi:Ljzx;

    :cond_1
    iget-object v0, p0, Ljzg;->eNi:Ljzx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljzg;->eNk:Ljzm;

    if-nez v0, :cond_2

    new-instance v0, Ljzm;

    invoke-direct {v0}, Ljzm;-><init>()V

    iput-object v0, p0, Ljzg;->eNk:Ljzm;

    :cond_2
    iget-object v0, p0, Ljzg;->eNk:Ljzm;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljzg;->eNj:Ljzt;

    if-nez v0, :cond_3

    new-instance v0, Ljzt;

    invoke-direct {v0}, Ljzt;-><init>()V

    iput-object v0, p0, Ljzg;->eNj:Ljzt;

    :cond_3
    iget-object v0, p0, Ljzg;->eNj:Ljzt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4940
    iget v0, p0, Ljzg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4941
    const/4 v0, 0x1

    iget-object v1, p0, Ljzg;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4943
    :cond_0
    iget-object v0, p0, Ljzg;->eNi:Ljzx;

    if-eqz v0, :cond_1

    .line 4944
    const/4 v0, 0x2

    iget-object v1, p0, Ljzg;->eNi:Ljzx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4946
    :cond_1
    iget-object v0, p0, Ljzg;->eNk:Ljzm;

    if-eqz v0, :cond_2

    .line 4947
    const/4 v0, 0x3

    iget-object v1, p0, Ljzg;->eNk:Ljzm;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4949
    :cond_2
    iget-object v0, p0, Ljzg;->eNj:Ljzt;

    if-eqz v0, :cond_3

    .line 4950
    const/4 v0, 0x4

    iget-object v1, p0, Ljzg;->eNj:Ljzt;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4952
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4953
    return-void
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4894
    iget-object v0, p0, Ljzg;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4957
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4958
    iget v1, p0, Ljzg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4959
    const/4 v1, 0x1

    iget-object v2, p0, Ljzg;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4962
    :cond_0
    iget-object v1, p0, Ljzg;->eNi:Ljzx;

    if-eqz v1, :cond_1

    .line 4963
    const/4 v1, 0x2

    iget-object v2, p0, Ljzg;->eNi:Ljzx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4966
    :cond_1
    iget-object v1, p0, Ljzg;->eNk:Ljzm;

    if-eqz v1, :cond_2

    .line 4967
    const/4 v1, 0x3

    iget-object v2, p0, Ljzg;->eNk:Ljzm;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4970
    :cond_2
    iget-object v1, p0, Ljzg;->eNj:Ljzt;

    if-eqz v1, :cond_3

    .line 4971
    const/4 v1, 0x4

    iget-object v2, p0, Ljzg;->eNj:Ljzt;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4974
    :cond_3
    return v0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 4905
    iget v0, p0, Ljzg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
