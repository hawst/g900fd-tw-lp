.class public final Lhxl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lhxl;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public final aTr()Ljava/util/List;
    .locals 6

    .prologue
    .line 41
    invoke-static {}, Lenu;->auQ()V

    .line 42
    const/4 v0, 0x0

    .line 43
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 45
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    iget-object v3, p0, Lhxl;->mContext:Landroid/content/Context;

    const-string v4, "event_logger_buffer"

    invoke-virtual {v3, v4}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 49
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 50
    new-instance v4, Ljvi;

    invoke-direct {v4}, Ljvi;-><init>()V

    .line 51
    invoke-static {v4, v0}, Leqh;->b(Ljsr;[B)Ljsr;

    .line 52
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 63
    :goto_2
    invoke-virtual {p0}, Lhxl;->aTs()V

    .line 64
    return-object v2

    .line 61
    :cond_0
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_2

    .line 56
    :catch_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 57
    :goto_3
    :try_start_2
    const-string v3, "LogsBackupManager"

    const-string v4, "Unable to read the stored client logs"

    invoke-static {v3, v4, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 61
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_2

    .line 58
    :catch_2
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 59
    :goto_4
    :try_start_3
    const-string v3, "LogsBackupManager"

    const-string v4, "Unable to read the stored client logs"

    invoke-static {v3, v4, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 61
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_5
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_5

    .line 58
    :catch_3
    move-exception v0

    goto :goto_4

    .line 56
    :catch_4
    move-exception v0

    goto :goto_3

    .line 61
    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method public final aTs()V
    .locals 3

    .prologue
    .line 69
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lhxl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "event_logger_buffer"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 70
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 73
    :cond_0
    return-void
.end method

.method public final ay(Ljava/util/List;)V
    .locals 8

    .prologue
    .line 77
    invoke-static {}, Lenu;->auQ()V

    .line 78
    const/4 v1, 0x0

    .line 80
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lhxl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "event_logger_buffer"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 81
    const-wide/16 v4, 0x0

    .line 82
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 86
    :cond_0
    new-instance v2, Ljava/io/ObjectOutputStream;

    iget-object v0, p0, Lhxl;->mContext:Landroid/content/Context;

    const-string v3, "event_logger_buffer"

    const v6, 0x8000

    invoke-virtual {v0, v3, v6}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 90
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const-wide/16 v6, 0x2800

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    .line 91
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 92
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    array-length v0, v0

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {v2, v3}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 97
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->flush()V

    .line 98
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 103
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 104
    :goto_1
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    :goto_2
    :try_start_2
    const-string v2, "LogsBackupManager"

    const-string v3, "Error saving logs"

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 100
    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
