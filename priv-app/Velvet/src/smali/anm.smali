.class public final Lanm;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private aib:Ljava/lang/String;

.field public aic:[Ljava/lang/String;

.field private aid:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6618
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6619
    iput v1, p0, Lanm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lanm;->aib:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lanm;->agq:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lanm;->aic:[Ljava/lang/String;

    iput-boolean v1, p0, Lanm;->aid:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lanm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lanm;->eCz:I

    .line 6620
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6533
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lanm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanm;->aib:Ljava/lang/String;

    iget v0, p0, Lanm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanm;->agq:Ljava/lang/String;

    iget v0, p0, Lanm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanm;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lanm;->aic:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lanm;->aic:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lanm;->aic:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lanm;->aic:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lanm;->aid:Z

    iget v0, p0, Lanm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lanm;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 6636
    iget v0, p0, Lanm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6637
    const/4 v0, 0x1

    iget-object v1, p0, Lanm;->aib:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6639
    :cond_0
    iget v0, p0, Lanm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6640
    const/4 v0, 0x2

    iget-object v1, p0, Lanm;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6642
    :cond_1
    iget-object v0, p0, Lanm;->aic:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lanm;->aic:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 6643
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lanm;->aic:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 6644
    iget-object v1, p0, Lanm;->aic:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 6645
    if-eqz v1, :cond_2

    .line 6646
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6643
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6650
    :cond_3
    iget v0, p0, Lanm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 6651
    const/4 v0, 0x5

    iget-boolean v1, p0, Lanm;->aid:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 6653
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6654
    return-void
.end method

.method public final aB(Ljava/lang/String;)Lanm;
    .locals 1

    .prologue
    .line 6558
    if-nez p1, :cond_0

    .line 6559
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6561
    :cond_0
    iput-object p1, p0, Lanm;->aib:Ljava/lang/String;

    .line 6562
    iget v0, p0, Lanm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanm;->aez:I

    .line 6563
    return-object p0
.end method

.method public final aC(Ljava/lang/String;)Lanm;
    .locals 1

    .prologue
    .line 6580
    if-nez p1, :cond_0

    .line 6581
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6583
    :cond_0
    iput-object p1, p0, Lanm;->agq:Ljava/lang/String;

    .line 6584
    iget v0, p0, Lanm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanm;->aez:I

    .line 6585
    return-object p0
.end method

.method public final aZ(Z)Lanm;
    .locals 1

    .prologue
    .line 6605
    iput-boolean p1, p0, Lanm;->aid:Z

    .line 6606
    iget v0, p0, Lanm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lanm;->aez:I

    .line 6607
    return-object p0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6577
    iget-object v0, p0, Lanm;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6658
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6659
    iget v2, p0, Lanm;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 6660
    const/4 v2, 0x1

    iget-object v3, p0, Lanm;->aib:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6663
    :cond_0
    iget v2, p0, Lanm;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 6664
    const/4 v2, 0x2

    iget-object v3, p0, Lanm;->agq:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6667
    :cond_1
    iget-object v2, p0, Lanm;->aic:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lanm;->aic:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 6670
    :goto_0
    iget-object v4, p0, Lanm;->aic:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 6671
    iget-object v4, p0, Lanm;->aic:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 6672
    if-eqz v4, :cond_2

    .line 6673
    add-int/lit8 v3, v3, 0x1

    .line 6674
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6670
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6678
    :cond_3
    add-int/2addr v0, v2

    .line 6679
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 6681
    :cond_4
    iget v1, p0, Lanm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 6682
    const/4 v1, 0x5

    iget-boolean v2, p0, Lanm;->aid:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6685
    :cond_5
    return v0
.end method

.method public final ps()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6555
    iget-object v0, p0, Lanm;->aib:Ljava/lang/String;

    return-object v0
.end method

.method public final pt()Z
    .locals 1

    .prologue
    .line 6566
    iget v0, p0, Lanm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pu()Z
    .locals 1

    .prologue
    .line 6602
    iget-boolean v0, p0, Lanm;->aid:Z

    return v0
.end method
