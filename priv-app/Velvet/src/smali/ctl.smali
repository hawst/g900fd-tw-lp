.class abstract Lctl;
.super Lctf;
.source "PG"


# instance fields
.field private final bix:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lctf;-><init>()V

    .line 115
    iput-object p1, p0, Lctl;->bix:Ljava/lang/String;

    .line 116
    return-void
.end method


# virtual methods
.method public final a(Lcud;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lctl;->bix:Ljava/lang/String;

    invoke-static {p2, v0}, Lctl;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    invoke-virtual {p0, p1, v0}, Lctl;->a(Lcud;Ljava/lang/String;)V

    .line 129
    :cond_0
    return-void
.end method

.method protected abstract a(Lcud;Ljava/lang/String;)V
.end method

.method public final h(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lctl;->bix:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 121
    return-void
.end method
