.class public final Lfxl;
.super Lfuz;
.source "PG"


# instance fields
.field private final cDc:I

.field private final cDd:I

.field private final ceW:I


# direct methods
.method public constructor <init>(Lftz;Lemp;III)V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-direct {p0, v0, p1, p2}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 40
    iput p3, p0, Lfxl;->cDc:I

    .line 41
    iput p4, p0, Lfxl;->ceW:I

    .line 42
    iput p5, p0, Lfxl;->cDd:I

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 47
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    move-result-object v0

    .line 49
    new-instance v1, Lapn;

    invoke-direct {v1}, Lapn;-><init>()V

    iget v2, p0, Lfxl;->cDc:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lapn;->dp(Ljava/lang/String;)Lapn;

    move-result-object v1

    iget v2, p0, Lfxl;->ceW:I

    invoke-virtual {v1, v2}, Lapn;->cW(I)Lapn;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahk:Lapn;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lanh;->cn(I)Lanh;

    .line 55
    const-string v1, "GOT_IT"

    const v2, 0x7f0a0998

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "QUESTION"

    const v4, 0x7f0a051d

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lijm;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijm;

    move-result-object v1

    .line 58
    new-instance v2, Ljdk;

    invoke-direct {v2}, Ljdk;-><init>()V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljdk;->oN(I)Ljdk;

    move-result-object v2

    const-string v3, "QUESTION"

    invoke-virtual {v2, v3}, Ljdk;->tE(Ljava/lang/String;)Ljdk;

    move-result-object v2

    .line 61
    new-array v3, v7, [Ljdl;

    new-instance v4, Ljdl;

    invoke-direct {v4}, Ljdl;-><init>()V

    const-string v5, "GOT_IT"

    invoke-virtual {v4, v5}, Ljdl;->tH(Ljava/lang/String;)Ljdl;

    move-result-object v4

    aput-object v4, v3, v6

    iput-object v3, v2, Ljdk;->ecm:[Ljdl;

    .line 64
    new-instance v3, Ljde;

    invoke-direct {v3}, Ljde;-><init>()V

    .line 65
    new-instance v4, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;-><init>(Ljava/util/Map;Ljdk;Ljde;)V

    .line 66
    new-instance v1, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    const/4 v2, 0x0

    invoke-direct {v1, v4, v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;-><init>(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;[Ljdf;)V

    .line 67
    invoke-static {v1}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v1

    iput-object v1, p0, Lfuz;->cCr:Ljava/util/List;

    .line 69
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 70
    new-array v2, v7, [Lanh;

    aput-object v0, v2, v6

    iput-object v2, v1, Lang;->ags:[Lanh;

    .line 71
    return-object v1
.end method

.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 83
    invoke-super/range {p0 .. p5}, Lfuz;->a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 84
    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget v1, p0, Lfxl;->cDd:I

    invoke-virtual {v0, v1}, Lfml;->iJ(I)V

    .line 85
    return-void
.end method

.method public final b(Landroid/content/Context;Lfmt;)V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0, p1, p2}, Lfuz;->b(Landroid/content/Context;Lfmt;)V

    .line 77
    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget v1, p0, Lfxl;->cDd:I

    invoke-virtual {v0, v1}, Lfml;->iK(I)V

    .line 78
    return-void
.end method
