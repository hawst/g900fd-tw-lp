.class public final Lhrw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhrs;
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final doQ:Lhrk;

.field private final mNetworkInformation:Lgno;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lhrk;Lgno;Lhym;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lhrw;->doQ:Lhrk;

    .line 26
    iput-object p2, p0, Lhrw;->mNetworkInformation:Lgno;

    .line 27
    iput-object p3, p0, Lhrw;->mSettings:Lhym;

    .line 28
    return-void
.end method

.method private a(Ljava/util/Collection;Ljava/util/Map;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 69
    iget-object v0, p0, Lhrw;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    .line 70
    iget-object v0, p0, Lhrw;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0, v1}, Lhrk;->oj(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 75
    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 77
    iget-object v3, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v3, v0, v5}, Lhrk;->b(Ljzp;Z)V

    goto :goto_0

    .line 87
    :cond_1
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 88
    iget-object v3, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lhrk;->mP(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 90
    :cond_3
    iget-object v3, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v3, v0}, Lhrk;->f(Ljzp;)Ljzp;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_2

    .line 93
    iget-object v3, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v3, v0, v5}, Lhrk;->b(Ljzp;Z)V

    goto :goto_1

    .line 98
    :cond_4
    return-void
.end method


# virtual methods
.method public final aRM()V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0, p0}, Lhrk;->b(Lhrs;)V

    .line 33
    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v1}, Lhrk;->aGp()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lhrw;->a(Ljava/util/Collection;Ljava/util/Map;)V

    .line 35
    return-void
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lhrw;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljzp;)V
    .locals 3

    .prologue
    .line 39
    const-string v0, "UpdateLanguagePacksTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Download failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    return-void
.end method

.method public final ub()Ljava/lang/Void;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    iget-object v1, p0, Lhrw;->mNetworkInformation:Lgno;

    invoke-virtual {v1}, Lgno;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lhrw;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUs()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "UpdateLanguagePacksTask"

    const-string v2, "Unexpected download strategy."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :pswitch_0
    if-eqz v0, :cond_2

    const-string v0, "UpdateLanguagePacksTask"

    const-string v1, "Checking for language pack updates."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v1}, Lhrk;->aGp()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lhrw;->a(Ljava/util/Collection;Ljava/util/Map;)V

    .line 48
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 46
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lhrw;->mNetworkInformation:Lgno;

    invoke-virtual {v0}, Lgno;->aAk()Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0, p0}, Lhrk;->a(Lhrs;)V

    iget-object v0, p0, Lhrw;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->cV()V

    goto :goto_1

    :cond_2
    const-string v0, "UpdateLanguagePacksTask"

    const-string v1, "Language pack update policy prevents downloading now."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
