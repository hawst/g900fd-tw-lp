.class public final Lixb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dMI:Ljava/lang/String;

.field private dMJ:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27895
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 27896
    const/4 v0, 0x0

    iput v0, p0, Lixb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lixb;->dMI:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lixb;->dMJ:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lixb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixb;->eCz:I

    .line 27897
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 27832
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixb;->dMI:Ljava/lang/String;

    iget v0, p0, Lixb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lixb;->dMJ:[B

    iget v0, p0, Lixb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 27911
    iget v0, p0, Lixb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 27912
    const/4 v0, 0x1

    iget-object v1, p0, Lixb;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 27914
    :cond_0
    iget v0, p0, Lixb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 27915
    const/4 v0, 0x2

    iget-object v1, p0, Lixb;->dMJ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 27917
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 27918
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 27922
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 27923
    iget v1, p0, Lixb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 27924
    const/4 v1, 0x1

    iget-object v2, p0, Lixb;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27927
    :cond_0
    iget v1, p0, Lixb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 27928
    const/4 v1, 0x2

    iget-object v2, p0, Lixb;->dMJ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 27931
    :cond_1
    return v0
.end method
