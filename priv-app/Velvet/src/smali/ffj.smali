.class public final Lffj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lffk;


# instance fields
.field private final mDestination:Ljbp;

.field private final mDirectionsLauncher:Lgah;

.field private final mNavigationContext:Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

.field private final mRoute:Liyg;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Lgah;Ljbp;Liyg;)V
    .locals 0
    .param p1    # Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Liyg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lffj;->mNavigationContext:Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    .line 29
    iput-object p3, p0, Lffj;->mDestination:Ljbp;

    .line 30
    iput-object p2, p0, Lffj;->mDirectionsLauncher:Lgah;

    .line 31
    iput-object p4, p0, Lffj;->mRoute:Liyg;

    .line 32
    return-void
.end method

.method private azf()Z
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lffj;->mNavigationContext:Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    if-nez v0, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    .line 88
    :cond_0
    iget-object v0, p0, Lffj;->mDirectionsLauncher:Lgah;

    iget-object v0, p0, Lffj;->mNavigationContext:Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    iget-object v1, p0, Lffj;->mRoute:Liyg;

    invoke-static {v0, v1}, Lgah;->a(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Liyg;)Lgba;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lffj;->mDirectionsLauncher:Lgah;

    invoke-static {v0}, Lgah;->a(Lgba;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final aK(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lffj;->mDestination:Ljbp;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lffj;->mRoute:Liyg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lffj;->mNavigationContext:Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    if-eqz v0, :cond_3

    .line 41
    iget-object v0, p0, Lffj;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPJ:[Ljbp;

    array-length v0, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lffj;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPJ:[Ljbp;

    .line 43
    :goto_0
    iget-object v2, p0, Lffj;->mDirectionsLauncher:Lgah;

    iget-object v2, p0, Lffj;->mNavigationContext:Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    iget-object v3, p0, Lffj;->mRoute:Liyg;

    invoke-static {v2, v3}, Lgah;->a(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Liyg;)Lgba;

    move-result-object v3

    .line 44
    iget-object v2, p0, Lffj;->mRoute:Liyg;

    invoke-virtual {v2}, Liyg;->bbM()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lffj;->mRoute:Liyg;

    invoke-virtual {v1}, Liyg;->bbL()Ljava/lang/String;

    move-result-object v1

    :cond_0
    move-object v5, v1

    move-object v2, v0

    .line 46
    :goto_1
    iget-object v0, p0, Lffj;->mDirectionsLauncher:Lgah;

    iget-object v1, p0, Lffj;->mDestination:Ljbp;

    iget-object v4, p0, Lffj;->mRoute:Liyg;

    invoke-static {v4}, Lgaz;->c(Liyg;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lgah;->b(Ljbp;[Ljbp;Lgba;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 48
    const-string v0, "callback_type"

    const-string v2, "activity"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    .line 41
    goto :goto_0

    :cond_3
    move-object v5, v1

    move-object v3, v1

    move-object v2, v1

    goto :goto_1
.end method

.method public final aL(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lffj;->azf()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0154

    .line 65
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 64
    :cond_0
    const v0, 0x7f0a0155

    goto :goto_0
.end method

.method public final aif()I
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lffj;->azf()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0200ba

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0202d0

    goto :goto_0
.end method

.method public final aza()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lffj;->azf()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "NAVIGATE"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "GET_DIRECTIONS"

    goto :goto_0
.end method

.method public final azb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "activity"

    return-object v0
.end method

.method public final azc()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public final isActive()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lffj;->mDestination:Ljbp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
