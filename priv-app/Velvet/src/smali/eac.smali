.class public final Leac;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic bSl:Lcom/google/android/search/shared/contact/PersonSelectItem;

.field private final bSm:Landroid/widget/QuickContactBadge;

.field private final bSn:Ljava/lang/Runnable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonSelectItem;Landroid/widget/QuickContactBadge;Ljava/lang/Runnable;)V
    .locals 0
    .param p3    # Ljava/lang/Runnable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 488
    iput-object p1, p0, Leac;->bSl:Lcom/google/android/search/shared/contact/PersonSelectItem;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 489
    iput-object p2, p0, Leac;->bSm:Landroid/widget/QuickContactBadge;

    .line 490
    iput-object p3, p0, Leac;->bSn:Ljava/lang/Runnable;

    .line 491
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 484
    check-cast p1, [Ljava/lang/Long;

    aget-object v0, p1, v4

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v1, p0, Leac;->bSl:Lcom/google/android/search/shared/contact/PersonSelectItem;

    iget-object v1, v1, Lcom/google/android/search/shared/contact/PersonSelectItem;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3, v4}, Leng;->a(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRT:Landroid/util/LruCache;

    sget-object v2, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSj:Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {v1, v0, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSj:Landroid/graphics/drawable/Drawable$ConstantState;

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Leac;->bSl:Lcom/google/android/search/shared/contact/PersonSelectItem;

    iget-object v3, v3, Lcom/google/android/search/shared/contact/PersonSelectItem;->mResources:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRT:Landroid/util/LruCache;

    invoke-virtual {v2, v0, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 484
    check-cast p1, Landroid/graphics/drawable/Drawable$ConstantState;

    iget-object v0, p0, Leac;->bSl:Lcom/google/android/search/shared/contact/PersonSelectItem;

    iget-object v0, p0, Leac;->bSm:Landroid/widget/QuickContactBadge;

    invoke-static {v0, p1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V

    iget-object v0, p0, Leac;->bSn:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leac;->bSn:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v0, p0, Leac;->bSl:Lcom/google/android/search/shared/contact/PersonSelectItem;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRU:Landroid/os/AsyncTask;

    return-void
.end method
