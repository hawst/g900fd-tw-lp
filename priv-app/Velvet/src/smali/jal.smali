.class public final Ljal;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dIj:I

.field public dMF:[Liyg;

.field public dOp:Ljck;

.field public dWL:Ljak;

.field private dWM:J

.field private dWN:J

.field private dWO:Ljava/lang/String;

.field private dWP:Z

.field private dWQ:Ljcn;

.field private dWR:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44962
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 44963
    iput v2, p0, Ljal;->aez:I

    iput-object v1, p0, Ljal;->dWL:Ljak;

    invoke-static {}, Liyg;->bbv()[Liyg;

    move-result-object v0

    iput-object v0, p0, Ljal;->dMF:[Liyg;

    const/4 v0, 0x1

    iput v0, p0, Ljal;->dIj:I

    iput-wide v4, p0, Ljal;->dWM:J

    iput-wide v4, p0, Ljal;->dWN:J

    const-string v0, ""

    iput-object v0, p0, Ljal;->dWO:Ljava/lang/String;

    iput-boolean v2, p0, Ljal;->dWP:Z

    iput-object v1, p0, Ljal;->dWQ:Ljcn;

    iput v2, p0, Ljal;->dWR:I

    iput-object v1, p0, Ljal;->dOp:Ljck;

    iput-object v1, p0, Ljal;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljal;->eCz:I

    .line 44964
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 44805
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljal;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljal;->dWL:Ljak;

    if-nez v0, :cond_1

    new-instance v0, Ljak;

    invoke-direct {v0}, Ljak;-><init>()V

    iput-object v0, p0, Ljal;->dWL:Ljak;

    :cond_1
    iget-object v0, p0, Ljal;->dWL:Ljak;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljal;->dMF:[Liyg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liyg;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljal;->dMF:[Liyg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljal;->dMF:[Liyg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljal;->dMF:[Liyg;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljal;->dIj:I

    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljal;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljal;->dWM:J

    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljal;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljal;->dWQ:Ljcn;

    if-nez v0, :cond_5

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljal;->dWQ:Ljcn;

    :cond_5
    iget-object v0, p0, Ljal;->dWQ:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljal;->dWR:I

    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljal;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljal;->dWN:J

    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljal;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljal;->dWO:Ljava/lang/String;

    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljal;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljal;->dWP:Z

    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljal;->aez:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljal;->dOp:Ljck;

    if-nez v0, :cond_6

    new-instance v0, Ljck;

    invoke-direct {v0}, Ljck;-><init>()V

    iput-object v0, p0, Ljal;->dOp:Ljck;

    :cond_6
    iget-object v0, p0, Ljal;->dOp:Ljck;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 44986
    iget-object v0, p0, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_0

    .line 44987
    const/4 v0, 0x1

    iget-object v1, p0, Ljal;->dWL:Ljak;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 44989
    :cond_0
    iget-object v0, p0, Ljal;->dMF:[Liyg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 44990
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljal;->dMF:[Liyg;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 44991
    iget-object v1, p0, Ljal;->dMF:[Liyg;

    aget-object v1, v1, v0

    .line 44992
    if-eqz v1, :cond_1

    .line 44993
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 44990
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44997
    :cond_2
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 44998
    const/4 v0, 0x3

    iget v1, p0, Ljal;->dIj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 45000
    :cond_3
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 45001
    const/4 v0, 0x4

    iget-wide v2, p0, Ljal;->dWM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 45003
    :cond_4
    iget-object v0, p0, Ljal;->dWQ:Ljcn;

    if-eqz v0, :cond_5

    .line 45004
    const/4 v0, 0x5

    iget-object v1, p0, Ljal;->dWQ:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 45006
    :cond_5
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 45007
    const/4 v0, 0x6

    iget v1, p0, Ljal;->dWR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 45009
    :cond_6
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    .line 45010
    const/4 v0, 0x7

    iget-wide v2, p0, Ljal;->dWN:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 45012
    :cond_7
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 45013
    const/16 v0, 0x8

    iget-object v1, p0, Ljal;->dWO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 45015
    :cond_8
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 45016
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljal;->dWP:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 45018
    :cond_9
    iget-object v0, p0, Ljal;->dOp:Ljck;

    if-eqz v0, :cond_a

    .line 45019
    const/16 v0, 0xa

    iget-object v1, p0, Ljal;->dOp:Ljck;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 45021
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 45022
    return-void
.end method

.method public final bek()J
    .locals 2

    .prologue
    .line 44861
    iget-wide v0, p0, Ljal;->dWM:J

    return-wide v0
.end method

.method public final bel()Z
    .locals 1

    .prologue
    .line 44869
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bem()J
    .locals 2

    .prologue
    .line 44880
    iget-wide v0, p0, Ljal;->dWN:J

    return-wide v0
.end method

.method public final ben()Z
    .locals 1

    .prologue
    .line 44888
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44899
    iget-object v0, p0, Ljal;->dWO:Ljava/lang/String;

    return-object v0
.end method

.method public final bep()Z
    .locals 1

    .prologue
    .line 44921
    iget-boolean v0, p0, Ljal;->dWP:Z

    return v0
.end method

.method public final beq()I
    .locals 1

    .prologue
    .line 44943
    iget v0, p0, Ljal;->dWR:I

    return v0
.end method

.method public final ber()Z
    .locals 1

    .prologue
    .line 44951
    iget v0, p0, Ljal;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cC(J)Ljal;
    .locals 1

    .prologue
    .line 44864
    iput-wide p1, p0, Ljal;->dWM:J

    .line 44865
    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljal;->aez:I

    .line 44866
    return-object p0
.end method

.method public final getEventType()I
    .locals 1

    .prologue
    .line 44842
    iget v0, p0, Ljal;->dIj:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 45026
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 45027
    iget-object v1, p0, Ljal;->dWL:Ljak;

    if-eqz v1, :cond_0

    .line 45028
    const/4 v1, 0x1

    iget-object v2, p0, Ljal;->dWL:Ljak;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45031
    :cond_0
    iget-object v1, p0, Ljal;->dMF:[Liyg;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljal;->dMF:[Liyg;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 45032
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljal;->dMF:[Liyg;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 45033
    iget-object v2, p0, Ljal;->dMF:[Liyg;

    aget-object v2, v2, v0

    .line 45034
    if-eqz v2, :cond_1

    .line 45035
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 45032
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 45040
    :cond_3
    iget v1, p0, Ljal;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    .line 45041
    const/4 v1, 0x3

    iget v2, p0, Ljal;->dIj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 45044
    :cond_4
    iget v1, p0, Ljal;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 45045
    const/4 v1, 0x4

    iget-wide v2, p0, Ljal;->dWM:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 45048
    :cond_5
    iget-object v1, p0, Ljal;->dWQ:Ljcn;

    if-eqz v1, :cond_6

    .line 45049
    const/4 v1, 0x5

    iget-object v2, p0, Ljal;->dWQ:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45052
    :cond_6
    iget v1, p0, Ljal;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    .line 45053
    const/4 v1, 0x6

    iget v2, p0, Ljal;->dWR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 45056
    :cond_7
    iget v1, p0, Ljal;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    .line 45057
    const/4 v1, 0x7

    iget-wide v2, p0, Ljal;->dWN:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 45060
    :cond_8
    iget v1, p0, Ljal;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_9

    .line 45061
    const/16 v1, 0x8

    iget-object v2, p0, Ljal;->dWO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45064
    :cond_9
    iget v1, p0, Ljal;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 45065
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljal;->dWP:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 45068
    :cond_a
    iget-object v1, p0, Ljal;->dOp:Ljck;

    if-eqz v1, :cond_b

    .line 45069
    const/16 v1, 0xa

    iget-object v2, p0, Ljal;->dOp:Ljck;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45072
    :cond_b
    return v0
.end method

.method public final oi(I)Ljal;
    .locals 1

    .prologue
    .line 44845
    iput p1, p0, Ljal;->dIj:I

    .line 44846
    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljal;->aez:I

    .line 44847
    return-object p0
.end method

.method public final oj(I)Ljal;
    .locals 1

    .prologue
    .line 44946
    const/16 v0, 0x320

    iput v0, p0, Ljal;->dWR:I

    .line 44947
    iget v0, p0, Ljal;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljal;->aez:I

    .line 44948
    return-object p0
.end method
