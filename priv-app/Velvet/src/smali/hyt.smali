.class final Lhyt;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic aSa:Lcom/google/android/shared/search/Query;

.field private synthetic duV:Lcom/google/android/velvet/ActionData;

.field private synthetic duW:Lhyq;

.field private synthetic duX:Lcom/google/android/speech/embedded/TaggerResult;


# direct methods
.method constructor <init>(Lhyq;Lcom/google/android/shared/search/Query;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/velvet/ActionData;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lhyt;->duW:Lhyq;

    iput-object p2, p0, Lhyt;->aSa:Lcom/google/android/shared/search/Query;

    iput-object p3, p0, Lhyt;->duX:Lcom/google/android/speech/embedded/TaggerResult;

    iput-object p4, p0, Lhyt;->duV:Lcom/google/android/velvet/ActionData;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 370
    iget-object v0, p0, Lhyt;->aSa:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lhyt;->duW:Lhyq;

    iget-object v1, v1, Lhyq;->mContactLabelConverter:Ldyv;

    iget-object v2, p0, Lhyt;->duW:Lhyq;

    iget-object v2, v2, Lhyq;->mRelationshipManager:Lcjg;

    iget-object v3, p0, Lhyt;->duW:Lhyq;

    iget-object v3, v3, Lhyq;->mPersonShortcutManager:Lciy;

    iget-object v4, p0, Lhyt;->duW:Lhyq;

    iget-object v4, v4, Lhyq;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lhyt;->duX:Lcom/google/android/speech/embedded/TaggerResult;

    iget-object v6, p0, Lhyt;->duW:Lhyq;

    iget-object v6, v6, Lhyq;->mContactLookup:Lghy;

    sget-object v7, Ldzb;->bRq:Ldzb;

    iget-object v8, p0, Lhyt;->duW:Lhyq;

    iget-object v8, v8, Lhyq;->mDiscourseContextSupplier:Ligi;

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Licv;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Lghy;Ldzb;Ligi;Lifw;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 370
    check-cast p1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iget-object v0, p0, Lhyt;->duW:Lhyq;

    iget-object v1, p0, Lhyt;->duV:Lcom/google/android/velvet/ActionData;

    new-instance v2, Lcom/google/android/search/shared/actions/PhoneCallAction;

    invoke-direct {v2, p1}, Lcom/google/android/search/shared/actions/PhoneCallAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    iget-object v3, p0, Lhyt;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2, v3}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    return-void
.end method
