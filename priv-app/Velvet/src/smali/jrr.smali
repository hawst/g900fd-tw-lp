.class public final Ljrr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dZf:Ljava/lang/String;

.field private eBB:Ljava/lang/String;

.field private eBC:[B

.field private eBD:Ljava/lang/String;

.field private eBE:Ljava/lang/String;

.field private eBh:[Ljnk;

.field private ekR:Ljava/lang/String;

.field private els:Ljava/lang/String;

.field private eso:Z

.field private ess:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 222
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 223
    iput v1, p0, Ljrr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljrr;->ekR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrr;->dZf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrr;->eBB:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljrr;->eBC:[B

    iput-boolean v1, p0, Ljrr;->eso:Z

    iput-boolean v1, p0, Ljrr;->ess:Z

    const-string v0, ""

    iput-object v0, p0, Ljrr;->eBD:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrr;->eBE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrr;->els:Ljava/lang/String;

    invoke-static {}, Ljnk;->bqH()[Ljnk;

    move-result-object v0

    iput-object v0, p0, Ljrr;->eBh:[Ljnk;

    const/4 v0, 0x0

    iput-object v0, p0, Ljrr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrr;->eCz:I

    .line 224
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrr;->ekR:Ljava/lang/String;

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrr;->eBB:Ljava/lang/String;

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljrr;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrr;->eBh:[Ljnk;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljnk;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljrr;->eBh:[Ljnk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljnk;

    invoke-direct {v3}, Ljnk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljrr;->eBh:[Ljnk;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljnk;

    invoke-direct {v3}, Ljnk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrr;->eBh:[Ljnk;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrr;->eBD:Ljava/lang/String;

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljrr;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljrr;->eBC:[B

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljrr;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrr;->dZf:Ljava/lang/String;

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrr;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrr;->eBE:Ljava/lang/String;

    iget v0, p0, Ljrr;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljrr;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrr;->els:Ljava/lang/String;

    iget v0, p0, Ljrr;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljrr;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljrr;->eso:Z

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljrr;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljrr;->ess:Z

    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljrr;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 246
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 247
    const/4 v0, 0x1

    iget-object v1, p0, Ljrr;->ekR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 249
    :cond_0
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 250
    const/4 v0, 0x2

    iget-object v1, p0, Ljrr;->eBB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 252
    :cond_1
    iget-object v0, p0, Ljrr;->eBh:[Ljnk;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljrr;->eBh:[Ljnk;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 253
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljrr;->eBh:[Ljnk;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 254
    iget-object v1, p0, Ljrr;->eBh:[Ljnk;

    aget-object v1, v1, v0

    .line 255
    if-eqz v1, :cond_2

    .line 256
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 253
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_3
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_4

    .line 261
    const/4 v0, 0x4

    iget-object v1, p0, Ljrr;->eBD:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 263
    :cond_4
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 264
    const/4 v0, 0x5

    iget-object v1, p0, Ljrr;->eBC:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 266
    :cond_5
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 267
    const/4 v0, 0x6

    iget-object v1, p0, Ljrr;->dZf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 269
    :cond_6
    iget v0, p0, Ljrr;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 270
    const/4 v0, 0x7

    iget-object v1, p0, Ljrr;->eBE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 272
    :cond_7
    iget v0, p0, Ljrr;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    .line 273
    const/16 v0, 0x8

    iget-object v1, p0, Ljrr;->els:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 275
    :cond_8
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 276
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljrr;->eso:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 278
    :cond_9
    iget v0, p0, Ljrr;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    .line 279
    const/16 v0, 0xa

    iget-boolean v1, p0, Ljrr;->ess:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 281
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 282
    return-void
.end method

.method public final bpl()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Ljrr;->eso:Z

    return v0
.end method

.method public final btB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljrr;->ekR:Ljava/lang/String;

    return-object v0
.end method

.method public final btC()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Ljrr;->ess:Z

    return v0
.end method

.method public final iZ(Z)Ljrr;
    .locals 1

    .prologue
    .line 121
    iput-boolean p1, p0, Ljrr;->eso:Z

    .line 122
    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljrr;->aez:I

    .line 123
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 286
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 287
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 288
    const/4 v1, 0x1

    iget-object v2, p0, Ljrr;->ekR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_0
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 292
    const/4 v1, 0x2

    iget-object v2, p0, Ljrr;->eBB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_1
    iget-object v1, p0, Ljrr;->eBh:[Ljnk;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljrr;->eBh:[Ljnk;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 296
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljrr;->eBh:[Ljnk;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 297
    iget-object v2, p0, Ljrr;->eBh:[Ljnk;

    aget-object v2, v2, v0

    .line 298
    if-eqz v2, :cond_2

    .line 299
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 296
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 304
    :cond_4
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 305
    const/4 v1, 0x4

    iget-object v2, p0, Ljrr;->eBD:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_5
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 309
    const/4 v1, 0x5

    iget-object v2, p0, Ljrr;->eBC:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_6
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_7

    .line 313
    const/4 v1, 0x6

    iget-object v2, p0, Ljrr;->dZf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    :cond_7
    iget v1, p0, Ljrr;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 317
    const/4 v1, 0x7

    iget-object v2, p0, Ljrr;->eBE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    :cond_8
    iget v1, p0, Ljrr;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_9

    .line 321
    const/16 v1, 0x8

    iget-object v2, p0, Ljrr;->els:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    :cond_9
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 325
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljrr;->eso:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 328
    :cond_a
    iget v1, p0, Ljrr;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_b

    .line 329
    const/16 v1, 0xa

    iget-boolean v2, p0, Ljrr;->ess:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 332
    :cond_b
    return v0
.end method

.method public final yA(Ljava/lang/String;)Ljrr;
    .locals 1

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Ljrr;->ekR:Ljava/lang/String;

    .line 37
    iget v0, p0, Ljrr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrr;->aez:I

    .line 38
    return-object p0
.end method
