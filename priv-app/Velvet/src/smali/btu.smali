.class final Lbtu;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private aJM:Lbtt;

.field private aJN:Landroid/os/ParcelFileDescriptor;

.field private aJO:Landroid/graphics/Bitmap;

.field private synthetic aJP:Lbts;

.field private ny:I


# direct methods
.method constructor <init>(Lbts;Lbtt;Landroid/os/ParcelFileDescriptor;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lbtu;->aJP:Lbts;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 204
    iput-object p2, p0, Lbtu;->aJM:Lbtt;

    .line 205
    iput-object p3, p0, Lbtu;->aJN:Landroid/os/ParcelFileDescriptor;

    .line 206
    iput-object p4, p0, Lbtu;->aJO:Landroid/graphics/Bitmap;

    .line 207
    iput p5, p0, Lbtu;->ny:I

    .line 208
    return-void
.end method

.method private varargs kB()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 212
    :try_start_0
    iget-object v0, p0, Lbtu;->aJN:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lbtd;->b(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lbtu;->aJP:Lbts;

    invoke-static {v1}, Lbts;->a(Lbts;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 217
    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lbtu;->aJO:Landroid/graphics/Bitmap;

    .line 220
    :cond_0
    invoke-static {v0}, Lbtl;->m(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 228
    :goto_0
    iget-object v0, p0, Lbtu;->aJP:Lbts;

    invoke-static {v0}, Lbts;->d(Lbts;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    iget-object v2, p0, Lbtu;->aJM:Lbtt;

    iget-object v2, v2, Lbtt;->aGe:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    iget-object v0, p0, Lbtu;->aJN:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_1

    .line 232
    :try_start_1
    iget-object v0, p0, Lbtu;->aJN:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 238
    :cond_1
    :goto_1
    return-object v1

    .line 222
    :cond_2
    if-nez v0, :cond_3

    .line 223
    :try_start_2
    iget-object v0, p0, Lbtu;->aJO:Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_0

    .line 225
    :cond_3
    iget v1, p0, Lbtu;->ny:I

    iget-object v2, p0, Lbtu;->aJP:Lbts;

    invoke-static {v2}, Lbts;->b(Lbts;)F

    move-result v2

    iget-object v3, p0, Lbtu;->aJP:Lbts;

    invoke-static {v3}, Lbts;->c(Lbts;)F

    move-result v3

    div-float/2addr v2, v3

    invoke-static {v0, v1, v2}, Lbts;->a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_1

    .line 230
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbtu;->aJN:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_4

    .line 232
    :try_start_3
    iget-object v1, p0, Lbtu;->aJN:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 235
    :cond_4
    :goto_2
    throw v0

    .line 233
    :catch_1
    move-exception v1

    .line 234
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Lbtu;->kB()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 197
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lbtu;->aJM:Lbtt;

    iget-object v0, v0, Lbtt;->aJL:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lbtu;->aJM:Lbtt;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbtu;->aJP:Lbts;

    iget-object v1, p0, Lbtu;->aJM:Lbtt;

    iget-object v1, v1, Lbtt;->aJL:Landroid/widget/ImageView;

    invoke-static {v0, v1, p1}, Lbts;->a(Lbts;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
