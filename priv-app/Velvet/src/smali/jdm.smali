.class public final Ljdm;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ecv:I

.field private ecw:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljdm;->aez:I

    iput v1, p0, Ljdm;->ecv:I

    iput v1, p0, Ljdm;->ecw:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljdm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdm;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdm;->ecv:I

    iget v0, p0, Ljdm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljdm;->ecw:I

    iget v0, p0, Ljdm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdm;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljdm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdm;->ecv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget v0, p0, Ljdm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljdm;->ecw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhF()I
    .locals 1

    iget v0, p0, Ljdm;->ecv:I

    return v0
.end method

.method public final bhG()Z
    .locals 1

    iget v0, p0, Ljdm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhH()I
    .locals 1

    iget v0, p0, Ljdm;->ecw:I

    return v0
.end method

.method public final bhI()Z
    .locals 1

    iget v0, p0, Ljdm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljdm;->ecv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljdm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljdm;->ecw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public final oS(I)Ljdm;
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Ljdm;->ecv:I

    iget v0, p0, Ljdm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdm;->aez:I

    return-object p0
.end method

.method public final oT(I)Ljdm;
    .locals 1

    const/4 v0, 0x4

    iput v0, p0, Ljdm;->ecw:I

    iget v0, p0, Ljdm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdm;->aez:I

    return-object p0
.end method
