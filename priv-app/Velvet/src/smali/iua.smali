.class public final Liua;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dJA:I

.field private dJB:[Litz;

.field private dJy:I

.field private dJz:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2792
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2793
    iput v0, p0, Liua;->aez:I

    iput v0, p0, Liua;->dJy:I

    iput-boolean v0, p0, Liua;->dJz:Z

    iput v0, p0, Liua;->dJA:I

    invoke-static {}, Litz;->aZa()[Litz;

    move-result-object v0

    iput-object v0, p0, Liua;->dJB:[Litz;

    const/4 v0, 0x0

    iput-object v0, p0, Liua;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liua;->eCz:I

    .line 2794
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2713
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liua;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liua;->dJy:I

    iget v0, p0, Liua;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liua;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Liua;->dJz:Z

    iget v0, p0, Liua;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liua;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liua;->dJA:I

    iget v0, p0, Liua;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liua;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Liua;->dJB:[Litz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Litz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Liua;->dJB:[Litz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Litz;

    invoke-direct {v3}, Litz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Liua;->dJB:[Litz;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Litz;

    invoke-direct {v3}, Litz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Liua;->dJB:[Litz;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2810
    iget v0, p0, Liua;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2811
    const/4 v0, 0x1

    iget v1, p0, Liua;->dJy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2813
    :cond_0
    iget v0, p0, Liua;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2814
    const/4 v0, 0x2

    iget-boolean v1, p0, Liua;->dJz:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2816
    :cond_1
    iget v0, p0, Liua;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2817
    const/4 v0, 0x3

    iget v1, p0, Liua;->dJA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2819
    :cond_2
    iget-object v0, p0, Liua;->dJB:[Litz;

    if-eqz v0, :cond_4

    iget-object v0, p0, Liua;->dJB:[Litz;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 2820
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Liua;->dJB:[Litz;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 2821
    iget-object v1, p0, Liua;->dJB:[Litz;

    aget-object v1, v1, v0

    .line 2822
    if-eqz v1, :cond_3

    .line 2823
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 2820
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2827
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2828
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 2832
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2833
    iget v1, p0, Liua;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2834
    const/4 v1, 0x1

    iget v2, p0, Liua;->dJy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2837
    :cond_0
    iget v1, p0, Liua;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2838
    const/4 v1, 0x2

    iget-boolean v2, p0, Liua;->dJz:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2841
    :cond_1
    iget v1, p0, Liua;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2842
    const/4 v1, 0x3

    iget v2, p0, Liua;->dJA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2845
    :cond_2
    iget-object v1, p0, Liua;->dJB:[Litz;

    if-eqz v1, :cond_5

    iget-object v1, p0, Liua;->dJB:[Litz;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 2846
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Liua;->dJB:[Litz;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 2847
    iget-object v2, p0, Liua;->dJB:[Litz;

    aget-object v2, v2, v0

    .line 2848
    if-eqz v2, :cond_3

    .line 2849
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2846
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2854
    :cond_5
    return v0
.end method
