.class public abstract Lcqu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lcjs;Lemp;Lcke;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjs;

    iput-object v0, p0, Lcqu;->mConfig:Lcjs;

    .line 72
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemp;

    iput-object v0, p0, Lcqu;->mClock:Lemp;

    .line 73
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcke;

    iput-object v0, p0, Lcqu;->mSearchSettings:Lcke;

    .line 74
    return-void
.end method

.method private a(Lcom/google/android/shared/search/Query;Lorg/json/JSONArray;Ljava/lang/String;)Ldfw;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-virtual {p2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 135
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    .line 136
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 138
    :try_start_0
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v4

    .line 139
    invoke-virtual {p0, v4, v3}, Lcqu;->a(Lorg/json/JSONArray;Ljava/util/List;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 141
    :catch_0
    move-exception v4

    const-string v4, "Search.SuggestionParser"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not parse suggestion at position "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 145
    :cond_0
    iget-object v0, p0, Lcqu;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Ls()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    :try_start_1
    invoke-direct {p0, p2, v3, v1}, Lcqu;->a(Lorg/json/JSONArray;Ljava/util/List;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 154
    :cond_1
    :goto_2
    invoke-virtual {p1, v1}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 155
    new-instance v0, Ldfw;

    const-string v1, "complete-server"

    iget-object v4, p0, Lcqu;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->uptimeMillis()J

    move-result-wide v4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ldfw;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;JLjava/lang/String;)V

    return-object v0

    .line 149
    :catch_1
    move-exception v0

    const-string v0, "Search.SuggestionParser"

    const-string v2, "Error parsing JSON correction span data"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private a(Lorg/json/JSONArray;Ljava/util/List;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 173
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 175
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 177
    const-string v2, "o"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "p"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 179
    const-string v2, "o"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcqu;->iw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    const-string v3, "p"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcqu;->iw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 184
    const/4 v0, 0x0

    .line 185
    sget-object v4, Lcqi;->bgh:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 187
    sget-object v4, Lcqi;->bgg:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 190
    :goto_0
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 192
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 193
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v5

    sub-int/2addr v5, v0

    .line 194
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v5

    .line 195
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v7

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int v4, v7, v4

    add-int/2addr v0, v4

    .line 197
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    invoke-static {v4}, Lifv;->gY(Z)V

    .line 198
    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 199
    new-instance v7, Lcom/google/android/shared/util/CorrectionSpan;

    invoke-direct {v7, v4}, Lcom/google/android/shared/util/CorrectionSpan;-><init>(Ljava/lang/String;)V

    .line 200
    const/16 v4, 0x21

    invoke-virtual {v1, v7, v5, v6, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 203
    :cond_0
    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    iput-object v1, v0, Lded;->bux:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buJ:Ljava/lang/String;

    iput-boolean v9, v0, Lded;->buU:Z

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    return-void
.end method

.method private static iw(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    sget-object v0, Lcqi;->bgi:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "&lt;$1&gt;"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;)Lcqv;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 83
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0, p1, v0, p3}, Lcqu;->a(Lcom/google/android/shared/search/Query;Lorg/json/JSONArray;Ljava/lang/String;)Ldfw;

    move-result-object v3

    .line 88
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 91
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 92
    if-eqz v4, :cond_4

    .line 93
    iget-object v0, p0, Lcqu;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lr()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 96
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :try_start_1
    const-string v1, "m"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 99
    if-eqz v5, :cond_0

    move v1, v2

    .line 102
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 104
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v6

    .line 105
    invoke-direct {p0, p1, v6, p3}, Lcqu;->a(Lcom/google/android/shared/search/Query;Lorg/json/JSONArray;Ljava/lang/String;)Ldfw;

    move-result-object v6

    .line 108
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 103
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    const-string v1, "Search.SuggestionParser"

    const-string v5, "Error parsing JSON look ahead suggestion data"

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    :goto_2
    const-string v1, "u"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 119
    if-eqz v4, :cond_2

    .line 120
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v1

    new-array v5, v1, [I

    move v1, v2

    .line 121
    :goto_3
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 122
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->optInt(I)I

    move-result v2

    aput v2, v5, v1

    .line 121
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 124
    :cond_1
    iget-object v1, p0, Lcqu;->mSearchSettings:Lcke;

    iget-object v2, p0, Lcqu;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v1, v5, v6, v7}, Lcke;->a([IJ)V

    .line 128
    :cond_2
    :goto_4
    new-instance v1, Lcqv;

    invoke-direct {v1, v3, v0}, Lcqv;-><init>(Ldfw;Ljava/util/List;)V

    return-object v1

    .line 112
    :catch_1
    move-exception v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_4
.end method

.method protected abstract a(Lorg/json/JSONArray;Ljava/util/List;)V
.end method
