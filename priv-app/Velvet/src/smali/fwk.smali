.class public final Lfwk;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 54
    return-void
.end method

.method public static a(Ljek;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "presentation.count"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljek;->biK()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Lfmt;Ljek;Z)V
    .locals 4

    .prologue
    .line 251
    invoke-interface {p0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {p1}, Ljek;->biK()J

    move-result-wide v2

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, v2, v3, p2}, Lfor;->b(JZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :cond_0
    :goto_0
    new-instance v0, Lgad;

    invoke-direct {v0, p0}, Lgad;-><init>(Lfmt;)V

    invoke-static {p1}, Lfwk;->a(Ljek;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgad;->mh(Ljava/lang/String;)Lgad;

    move-result-object v0

    invoke-virtual {v0}, Lgad;->apply()V

    .line 256
    return-void

    .line 251
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making save preferences request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static b(Ljek;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p0}, Ljek;->biK()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 18

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lfuz;->mEntry:Lizj;

    iget-object v10, v2, Lizj;->dTd:Ljek;

    .line 59
    const/4 v2, 0x3

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 62
    iget-object v2, v10, Ljek;->eeL:Ljbp;

    if-eqz v2, :cond_2

    .line 63
    iget-object v2, v10, Ljek;->eeL:Ljbp;

    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    invoke-static {v2, v3}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v2

    check-cast v2, Ljbp;

    invoke-virtual {v10}, Ljek;->biJ()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    move-result-object v2

    new-instance v3, Ljal;

    invoke-direct {v3}, Ljal;-><init>()V

    new-instance v4, Ljak;

    invoke-direct {v4}, Ljak;-><init>()V

    iput-object v4, v3, Ljal;->dWL:Ljak;

    iget-object v4, v3, Ljal;->dWL:Ljak;

    iput-object v2, v4, Ljak;->aeB:Ljbp;

    invoke-interface/range {p2 .. p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBP()Landroid/location/Location;

    move-result-object v2

    invoke-static {v2}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v4

    const/4 v2, 0x0

    iget-object v5, v10, Ljek;->eeJ:Ljcn;

    if-eqz v5, :cond_0

    iget-object v2, v10, Ljek;->eeJ:Ljcn;

    invoke-static {v2}, Lgbm;->c(Ljcn;)Ljava/lang/String;

    move-result-object v2

    :cond_0
    iget-object v5, v10, Ljek;->eeL:Ljbp;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v5, v6, v7, v8, v9}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lanh;

    invoke-direct {v6}, Lanh;-><init>()V

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lanh;->cm(I)Lanh;

    new-instance v7, Lfsa;

    const/4 v8, 0x3

    invoke-direct {v7, v8}, Lfsa;-><init>(I)V

    const/4 v8, 0x0

    invoke-virtual {v7, v5, v8}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v5

    iput-object v5, v6, Lanh;->ahs:Lani;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfuz;->mEntry:Lizj;

    iput-object v5, v6, Lanh;->ahu:Lizj;

    new-instance v5, Laor;

    invoke-direct {v5}, Laor;-><init>()V

    iput-object v5, v6, Lanh;->agx:Laor;

    iput-object v4, v5, Laor;->ajY:Ljbp;

    iput-object v3, v5, Laor;->ajZ:Ljal;

    const/4 v3, 0x1

    invoke-virtual {v5, v3}, Laor;->bf(Z)Laor;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v5, v2}, Laor;->cl(Ljava/lang/String;)Laor;

    :cond_1
    invoke-interface {v11, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_2
    invoke-virtual {v10}, Ljek;->biO()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    new-instance v2, Lftn;

    invoke-virtual {v10}, Ljek;->biN()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lftn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lftn;->aDh()Lanh;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v10}, Ljek;->biI()I

    move-result v3

    iget-object v4, v10, Ljek;->eeP:Liyg;

    invoke-virtual {v10}, Ljek;->biL()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    if-ne v3, v6, :cond_a

    if-eqz v4, :cond_a

    invoke-static {v5}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    invoke-virtual {v4}, Liyg;->bbC()I

    move-result v2

    invoke-virtual {v4}, Liyg;->bby()I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0159

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v4, v6

    const/4 v2, 0x1

    invoke-static {v5}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v9, v2

    :goto_0
    const/4 v6, 0x0

    invoke-virtual {v10}, Ljek;->biM()J

    move-result-wide v12

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lfuz;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    sub-long v14, v4, v2

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v16, 0x1

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v16

    cmp-long v7, v14, v16

    if-lez v7, :cond_5

    const-wide/32 v6, 0xea60

    const/high16 v8, 0x40000

    invoke-static/range {v2 .. v8}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v2

    const v3, 0x7f0a0158

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v2, 0x0

    .line 73
    :goto_2
    if-eqz v2, :cond_4

    .line 74
    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_4
    new-instance v3, Lang;

    invoke-direct {v3}, Lang;-><init>()V

    .line 81
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lanh;

    invoke-interface {v11, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lanh;

    iput-object v2, v3, Lang;->ags:[Lanh;

    .line 82
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Lang;->aS(Z)Lang;

    .line 83
    return-object v3

    .line 72
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-lez v2, :cond_9

    const v2, 0x7f0a015a

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_6
    iget-object v3, v10, Ljek;->eeL:Ljbp;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v3, v4, v5, v6, v7}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/16 v5, 0xc

    invoke-virtual {v3, v5}, Lanh;->cm(I)Lanh;

    new-instance v5, Lfsa;

    const/4 v6, 0x3

    invoke-direct {v5, v6}, Lfsa;-><init>(I)V

    const v6, 0x7f0201b8

    invoke-virtual {v5, v6}, Lfsa;->jB(I)Lfsa;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v4

    iput-object v4, v3, Lanh;->ahs:Lani;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfuz;->mEntry:Lizj;

    iput-object v4, v3, Lanh;->ahu:Lizj;

    new-instance v4, Laon;

    invoke-direct {v4}, Laon;-><init>()V

    iput-object v4, v3, Lanh;->agA:Laon;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, v3, Lanh;->agA:Laon;

    invoke-virtual {v4, v9}, Laon;->ca(Ljava/lang/String;)Laon;

    :cond_7
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, v3, Lanh;->agA:Laon;

    invoke-virtual {v4, v2}, Laon;->cb(Ljava/lang/String;)Laon;

    :cond_8
    move-object v2, v3

    goto :goto_2

    :cond_9
    move-object v2, v6

    goto/16 :goto_1

    :cond_a
    move-object v9, v2

    goto/16 :goto_0
.end method

.method public final a(Landroid/app/Activity;Lfmt;)Lfry;
    .locals 7

    .prologue
    .line 185
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v6, v0, Lizj;->dTd:Ljek;

    .line 186
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/renderingcontext/SharedTrafficContext;->p(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/SharedTrafficContext;

    move-result-object v0

    invoke-virtual {v6}, Ljek;->biK()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/renderingcontext/SharedTrafficContext;->lY(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lfpk;->o(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpk;

    move-result-object v0

    invoke-static {v6}, Lfwk;->a(Ljek;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfpk;->lX(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    new-instance v2, Lgad;

    invoke-direct {v2, p2}, Lgad;-><init>(Lfmt;)V

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v1, v0}, Lgad;->E(Ljava/lang/String;I)Lgad;

    move-result-object v0

    invoke-virtual {v0}, Lgad;->apply()V

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v6}, Ljek;->biQ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    const v0, 0x7f0a0333

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 190
    const v1, 0x7f0a0335

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljek;->biP()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <a href=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"><u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</u></a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 195
    new-instance v0, Lfry;

    const v2, 0x7f0a083c

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a083b

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lfwl;

    invoke-direct {v4, p0, p2, v6}, Lfwl;-><init>(Lfwk;Lfmt;Ljek;)V

    new-instance v5, Lfwm;

    invoke-direct {v5, p0, p2, v6}, Lfwm;-><init>(Lfwk;Lfmt;Ljek;)V

    invoke-direct/range {v0 .. v5}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;)V

    .line 212
    :goto_1
    return-object v0

    .line 186
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
