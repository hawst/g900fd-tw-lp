.class public final Ljtl;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHy:I

.field private eDw:I

.field private eDx:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 89
    iput v0, p0, Ljtl;->aez:I

    iput v0, p0, Ljtl;->eDw:I

    iput v0, p0, Ljtl;->eDx:I

    iput v0, p0, Ljtl;->dHy:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljtl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljtl;->eCz:I

    .line 90
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljtl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtl;->eDw:I

    iget v0, p0, Ljtl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtl;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtl;->eDx:I

    iget v0, p0, Ljtl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljtl;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtl;->dHy:I

    iget v0, p0, Ljtl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljtl;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 105
    iget v0, p0, Ljtl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    iget v1, p0, Ljtl;->eDw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 108
    :cond_0
    iget v0, p0, Ljtl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x2

    iget v1, p0, Ljtl;->eDx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 111
    :cond_1
    iget v0, p0, Ljtl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 112
    const/4 v0, 0x3

    iget v1, p0, Ljtl;->dHy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 114
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 115
    return-void
.end method

.method public final buv()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Ljtl;->eDw:I

    return v0
.end method

.method public final buw()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Ljtl;->eDx:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 119
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 120
    iget v1, p0, Ljtl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 121
    const/4 v1, 0x1

    iget v2, p0, Ljtl;->eDw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_0
    iget v1, p0, Ljtl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 125
    const/4 v1, 0x2

    iget v2, p0, Ljtl;->eDx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_1
    iget v1, p0, Ljtl;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 129
    const/4 v1, 0x3

    iget v2, p0, Ljtl;->dHy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_2
    return v0
.end method

.method public final ss(I)Ljtl;
    .locals 1

    .prologue
    .line 37
    iput p1, p0, Ljtl;->eDw:I

    .line 38
    iget v0, p0, Ljtl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtl;->aez:I

    .line 39
    return-object p0
.end method

.method public final st(I)Ljtl;
    .locals 1

    .prologue
    .line 56
    iput p1, p0, Ljtl;->eDx:I

    .line 57
    iget v0, p0, Ljtl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljtl;->aez:I

    .line 58
    return-object p0
.end method

.method public final su(I)Ljtl;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    iput v0, p0, Ljtl;->dHy:I

    .line 76
    iget v0, p0, Ljtl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljtl;->aez:I

    .line 77
    return-object p0
.end method
