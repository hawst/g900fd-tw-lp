.class public final Lgcp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdi;


# instance fields
.field private final cFZ:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lgcp;->cFZ:Ljava/io/ByteArrayOutputStream;

    return-void
.end method

.method public static a(Ljxe;)Ljps;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186
    new-instance v0, Ljps;

    invoke-direct {v0}, Ljps;-><init>()V

    .line 187
    invoke-virtual {p0}, Ljxe;->bvG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "GsaS3ResponseProcessor"

    const-string v1, "Received compressed majel response"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 189
    const/4 v0, 0x0

    .line 194
    :goto_0
    return-object v0

    .line 191
    :cond_0
    iget-object v0, p0, Ljxe;->eKp:Ljps;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljww;Lglx;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 43
    :pswitch_0
    sget-object v0, Ljxo;->eKU:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxo;

    .line 44
    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {v0}, Ljxo;->bvB()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lgcp;->cFZ:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v2

    if-lez v2, :cond_7

    iget-object v0, p0, Lgcp;->cFZ:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p2, v0}, Lglx;->v([B)V

    .line 48
    :cond_1
    :goto_1
    sget-object v0, Ljxg;->eKr:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxg;

    .line 50
    if-eqz v0, :cond_2

    .line 52
    iget-object v1, v0, Ljxg;->eKs:Ljvv;

    invoke-virtual {v0}, Ljxg;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Lglx;->a(Ljvv;Ljava/lang/String;)V

    .line 55
    :cond_2
    sget-object v0, Ljxe;->eKo:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxe;

    .line 57
    if-eqz v0, :cond_3

    .line 58
    invoke-static {v0}, Lgcp;->a(Ljxe;)Ljps;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p2, v0}, Lglx;->a(Ljps;)V

    .line 61
    :cond_3
    sget-object v0, Ljxl;->eKR:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxl;

    .line 63
    if-eqz v0, :cond_4

    .line 64
    iget-object v1, v0, Ljxl;->eKS:Lieb;

    if-eqz v1, :cond_4

    iget-object v0, v0, Ljxl;->eKS:Lieb;

    invoke-interface {p2, v0}, Lglx;->a(Lieb;)V

    .line 67
    :cond_4
    sget-object v0, Ljwp;->eJn:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwp;

    .line 68
    if-eqz v0, :cond_5

    .line 69
    invoke-interface {p2, v0}, Lglx;->b(Ljwp;)V

    .line 72
    :cond_5
    sget-object v0, Ljwi;->eIZ:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwi;

    .line 74
    if-eqz v0, :cond_6

    .line 75
    iget-object v0, v0, Ljwi;->eJa:Ljyl;

    invoke-interface {p2, v0}, Lglx;->d(Ljyl;)V

    .line 77
    :cond_6
    sget-object v0, Ljxb;->eKa:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxb;

    .line 79
    if-eqz v0, :cond_0

    .line 80
    invoke-interface {p2, v0}, Lglx;->a(Ljxb;)V

    goto/16 :goto_0

    .line 45
    :cond_7
    invoke-virtual {v0}, Ljxo;->asV()[B

    move-result-object v0

    iget-object v2, p0, Lgcp;->cFZ:Ljava/io/ByteArrayOutputStream;

    array-length v3, v0

    invoke-virtual {v2, v0, v1, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 85
    :pswitch_1
    iget-object v2, p1, Ljww;->eJN:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_8

    aget-object v1, v2, v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DBG: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    invoke-interface {p2}, Lglx;->CZ()V

    goto/16 :goto_0

    .line 90
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error S3Response received via onResult"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :pswitch_3
    const-string v0, "GsaS3ResponseProcessor"

    const-string v2, "NOT_STARTED received"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 94
    new-instance v0, Leii;

    invoke-direct {v0, v1}, Leii;-><init>(I)V

    invoke-interface {p2, v0}, Lglx;->c(Leiq;)V

    goto/16 :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
