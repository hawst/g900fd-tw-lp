.class public final Lcaf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public aQf:I

.field public aQg:I

.field public aQh:Landroid/view/View;

.field public aQi:I

.field public aQj:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput v1, p0, Lcaf;->aQf:I

    .line 40
    iput v2, p0, Lcaf;->aQg:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcaf;->aQh:Landroid/view/View;

    .line 42
    iput v1, p0, Lcaf;->aQi:I

    .line 43
    iput-boolean v2, p0, Lcaf;->aQj:Z

    .line 44
    return-void
.end method


# virtual methods
.method public final b(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 59
    if-nez p2, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    instance-of v0, p2, Lcom/android/launcher3/CellLayout;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcaf;->aQj:Z

    if-eqz v0, :cond_0

    .line 65
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcaf;->c(ILandroid/view/View;)V

    goto :goto_0
.end method

.method public final c(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 84
    iput p1, p0, Lcaf;->aQf:I

    .line 86
    const v0, 0x7f0c0145

    invoke-static {p2, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 87
    iput-object p2, p0, Lcaf;->aQh:Landroid/view/View;

    .line 88
    return-void
.end method

.method public final ew(I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    iput v2, p0, Lcaf;->aQg:I

    .line 98
    iget v0, p0, Lcaf;->aQf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 99
    iget v0, p0, Lcaf;->aQf:I

    if-le v0, p1, :cond_0

    .line 101
    const/4 v0, 0x1

    iput v0, p0, Lcaf;->aQg:I

    .line 110
    :goto_0
    iget v0, p0, Lcaf;->aQg:I

    return v0

    .line 102
    :cond_0
    iget v0, p0, Lcaf;->aQf:I

    if-ge v0, p1, :cond_1

    .line 103
    const/4 v0, 0x2

    iput v0, p0, Lcaf;->aQg:I

    goto :goto_0

    .line 108
    :cond_1
    iput v2, p0, Lcaf;->aQg:I

    goto :goto_0
.end method
