.class public final Liev;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dAX:Lieo;

.field private dAY:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Liev;->aez:I

    iput-object v2, p0, Liev;->dAX:Lieo;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liev;->dAY:J

    iput-object v2, p0, Liev;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liev;->eCz:I

    .line 83
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 40
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liev;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Liev;->dAX:Lieo;

    if-nez v0, :cond_1

    new-instance v0, Lieo;

    invoke-direct {v0}, Lieo;-><init>()V

    iput-object v0, p0, Liev;->dAX:Lieo;

    :cond_1
    iget-object v0, p0, Liev;->dAX:Lieo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Liev;->dAY:J

    iget v0, p0, Liev;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liev;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Liev;->dAX:Lieo;

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    iget-object v1, p0, Liev;->dAX:Lieo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 100
    :cond_0
    iget v0, p0, Liev;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 101
    const/4 v0, 0x2

    iget-wide v2, p0, Liev;->dAY:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 103
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 104
    return-void
.end method

.method public final aVT()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Liev;->dAY:J

    return-wide v0
.end method

.method public final aVU()Z
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Liev;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bQ(J)Liev;
    .locals 2

    .prologue
    .line 68
    const-wide v0, 0x138bebdb4eaL

    iput-wide v0, p0, Liev;->dAY:J

    .line 69
    iget v0, p0, Liev;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liev;->aez:I

    .line 70
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 108
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 109
    iget-object v1, p0, Liev;->dAX:Lieo;

    if-eqz v1, :cond_0

    .line 110
    const/4 v1, 0x1

    iget-object v2, p0, Liev;->dAX:Lieo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    :cond_0
    iget v1, p0, Liev;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 114
    const/4 v1, 0x2

    iget-wide v2, p0, Liev;->dAY:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_1
    return v0
.end method
