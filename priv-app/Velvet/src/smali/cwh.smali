.class public final Lcwh;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final blH:Ljava/util/Set;

.field private static blI:I


# instance fields
.field private final blJ:Landroid/app/Activity;

.field private blK:Landroid/preference/Preference;

.field private blL:Landroid/preference/Preference;

.field private blM:Landroid/preference/Preference;

.field private blN:Landroid/preference/Preference;

.field private blO:Landroid/preference/Preference;

.field private blP:Landroid/preference/Preference;

.field private blQ:Landroid/preference/Preference;

.field private blR:Landroid/preference/Preference;

.field private final mGsaConfig:Lchk;

.field private final mVoiceSettings:Lhym;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 74
    const-string v0, "trigger_e100_driving"

    const-string v1, "trigger_e100_nondriving"

    const-string v2, "trigger_e100_exiting_vehicle"

    const-string v3, "reset_eyes_free_prefs"

    invoke-static {v0, v1, v2, v3}, Lijp;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcwh;->blH:Ljava/util/Set;

    .line 82
    const/4 v0, 0x0

    sput v0, Lcwh;->blI:I

    return-void
.end method

.method public constructor <init>(Lhym;Landroid/app/Activity;Lchk;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 99
    iput-object p1, p0, Lcwh;->mVoiceSettings:Lhym;

    .line 100
    iput-object p2, p0, Lcwh;->blJ:Landroid/app/Activity;

    .line 101
    iput-object p3, p0, Lcwh;->mGsaConfig:Lchk;

    .line 102
    return-void
.end method

.method private Dz()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcwh;->blK:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcwh;->blK:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcwh;->blL:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    .line 343
    iget-object v1, p0, Lcwh;->blL:Landroid/preference/Preference;

    iget-object v0, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 347
    :cond_1
    iget-object v0, p0, Lcwh;->blM:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    .line 348
    iget-object v0, p0, Lcwh;->blM:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTU()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 350
    iget-object v0, p0, Lcwh;->blM:Landroid/preference/Preference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 353
    :cond_2
    iget-object v0, p0, Lcwh;->blN:Landroid/preference/Preference;

    if-eqz v0, :cond_3

    .line 354
    iget-object v0, p0, Lcwh;->blN:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTV()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 359
    :cond_3
    iget-object v0, p0, Lcwh;->blO:Landroid/preference/Preference;

    if-eqz v0, :cond_4

    .line 360
    iget-object v0, p0, Lcwh;->blO:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->BU()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 362
    iget-object v0, p0, Lcwh;->blO:Landroid/preference/Preference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 366
    :cond_4
    iget-object v0, p0, Lcwh;->blP:Landroid/preference/Preference;

    if-eqz v0, :cond_5

    .line 367
    iget-object v0, p0, Lcwh;->blP:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->BV()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 369
    iget-object v0, p0, Lcwh;->blP:Landroid/preference/Preference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 373
    :cond_5
    iget-object v0, p0, Lcwh;->blQ:Landroid/preference/Preference;

    if-eqz v0, :cond_6

    .line 374
    iget-object v0, p0, Lcwh;->blQ:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTX()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 379
    :cond_6
    iget-object v0, p0, Lcwh;->blR:Landroid/preference/Preference;

    if-eqz v0, :cond_7

    .line 380
    iget-object v0, p0, Lcwh;->blR:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTY()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 383
    :cond_7
    return-void

    .line 343
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private Tp()J
    .locals 4

    .prologue
    .line 316
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 317
    iget-object v2, p0, Lcwh;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->Jz()I

    move-result v2

    .line 318
    sget v3, Lcwh;->blI:I

    mul-int/2addr v2, v3

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 319
    sget v2, Lcwh;->blI:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcwh;->blI:I

    .line 320
    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 3

    .prologue
    .line 106
    instance-of v0, p1, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_1

    .line 107
    check-cast p1, Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcwh;->d(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lcwh;->a(Landroid/preference/Preference;)V

    goto :goto_1

    .line 109
    :cond_1
    const-string v0, "hands_free_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 110
    iput-object p1, p0, Lcwh;->blK:Landroid/preference/Preference;

    .line 111
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 135
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcwh;->Dz()V

    .line 137
    :cond_3
    return-void

    .line 112
    :cond_4
    const-string v0, "hands_free_bluetooth_setup"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 113
    iput-object p1, p0, Lcwh;->blL:Landroid/preference/Preference;

    .line 114
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_2

    .line 115
    :cond_5
    const-string v0, "hands_free_read_notifications_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 116
    iput-object p1, p0, Lcwh;->blM:Landroid/preference/Preference;

    .line 117
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    .line 118
    :cond_6
    const-string v0, "headset_notifications_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 119
    iput-object p1, p0, Lcwh;->blN:Landroid/preference/Preference;

    .line 120
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    .line 121
    :cond_7
    const-string v0, "hands_free_auto_driving_car_mode_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 122
    iput-object p1, p0, Lcwh;->blO:Landroid/preference/Preference;

    .line 123
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    .line 124
    :cond_8
    const-string v0, "hands_free_auto_nav_car_mode_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 125
    iput-object p1, p0, Lcwh;->blP:Landroid/preference/Preference;

    .line 126
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    .line 127
    :cond_9
    const-string v0, "lockscreen_search_bluetooth"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 128
    iput-object p1, p0, Lcwh;->blQ:Landroid/preference/Preference;

    goto :goto_2

    .line 129
    :cond_a
    const-string v0, "lockscreen_search_headset"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 130
    iput-object p1, p0, Lcwh;->blR:Landroid/preference/Preference;

    goto :goto_2

    .line 131
    :cond_b
    sget-object v0, Lcwh;->blH:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_2
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 154
    const-string v2, "notification_preference_header"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 155
    sget-object v2, Lcgg;->aVw:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcgg;->aVx:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 155
    goto :goto_0

    .line 157
    :cond_2
    const-string v2, "hands_free_read_notifications_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 158
    sget-object v2, Lcgg;->aVw:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 159
    :cond_3
    const-string v2, "headset_notifications_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 160
    sget-object v2, Lcgg;->aVx:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 161
    :cond_4
    const-string v2, "search_lockscreen_pref_header"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "lockscreen_search_bluetooth"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "lockscreen_search_headset"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 164
    :cond_5
    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 165
    :cond_6
    sget-object v2, Lcwh;->blH:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 166
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v2

    invoke-virtual {v2}, Lckw;->Pa()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 167
    :cond_7
    const-string v2, "hands_free_auto_driving_car_mode_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 168
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcwh;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JK()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 171
    :cond_8
    new-instance v2, Lbyl;

    invoke-direct {v2}, Lbyl;-><init>()V

    invoke-static {}, Lbyl;->BW()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0}, Lcwh;->Dz()V

    .line 392
    return-void
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 180
    const-string v2, "hands_free_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 182
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcwh;->blJ:Landroid/app/Activity;

    const-class v4, Lhdv;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    const-string v3, "skip_overlay"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 185
    iget-object v1, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 244
    :goto_0
    return v0

    .line 191
    :cond_0
    iget-object v0, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUg()V

    .line 192
    invoke-direct {p0}, Lcwh;->Dz()V

    move v0, v1

    .line 193
    goto :goto_0

    .line 197
    :cond_1
    const-string v2, "hands_free_auto_driving_car_mode_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 198
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 200
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 201
    const-string v2, "summaryId"

    const v3, 0x7f0a09a9

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 203
    const-string v2, "settingKey"

    const-string v3, "hands_free_auto_driving_car_mode_enabled"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    new-instance v2, Lcwi;

    invoke-direct {v2}, Lcwi;-><init>()V

    .line 206
    invoke-virtual {v2, v1}, Lcwi;->setArguments(Landroid/os/Bundle;)V

    .line 207
    iget-object v1, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "HandsFreeSettingsAutoDrivingDialog"

    invoke-virtual {v2, v1, v3}, Lcwi;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 208
    invoke-virtual {v2, p0}, Lcwi;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 213
    goto :goto_0

    .line 216
    :cond_3
    const-string v2, "hands_free_auto_nav_car_mode_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 217
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 219
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 220
    const-string v2, "summaryId"

    const v3, 0x7f0a09aa

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 222
    const-string v2, "settingKey"

    const-string v3, "hands_free_auto_nav_car_mode_enabled"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    new-instance v2, Lcwi;

    invoke-direct {v2}, Lcwi;-><init>()V

    .line 225
    invoke-virtual {v2, v1}, Lcwi;->setArguments(Landroid/os/Bundle;)V

    .line 226
    iget-object v1, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "HandsFreeSettingsAutoNavDialog"

    invoke-virtual {v2, v1, v3}, Lcwi;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2, p0}, Lcwi;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 232
    goto/16 :goto_0

    .line 235
    :cond_5
    const-string v0, "hands_free_read_notifications_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "headset_notifications_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "hands_free_auto_driving_car_mode_enabled"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 240
    :cond_6
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_1
    move v0, v1

    .line 244
    goto/16 :goto_0

    .line 242
    :cond_7
    const-string v0, "HandsFreeSettingsController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected preference change: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8

    .prologue
    const/16 v5, 0x4b

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 249
    const-string v0, "hands_free_bluetooth_setup"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    const-string v1, "android.intent.action.VOICE_COMMAND"

    invoke-static {v0, v1}, Lhsh;->v(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 254
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcwh;->blJ:Landroid/app/Activity;

    const-class v3, Lhcu;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 255
    const-string v2, "entry-point"

    if-eqz v0, :cond_0

    const-string v0, "settings"

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const-string v0, "bt-address"

    iget-object v2, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    move v0, v6

    .line 312
    :goto_1
    return v0

    .line 255
    :cond_0
    const-string v0, "multi-assistant"

    goto :goto_0

    .line 264
    :cond_1
    const-string v0, "reset_eyes_free_prefs"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 265
    iget-object v0, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUh()V

    .line 266
    new-instance v0, Lbwo;

    iget-object v1, p0, Lcwh;->mVoiceSettings:Lhym;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    iget-object v2, v2, Lgql;->mClock:Lemp;

    invoke-direct {v0, v1, v2}, Lbwo;-><init>(Lhym;Lemp;)V

    .line 268
    iget-object v1, v0, Lbwo;->mSettings:Lhym;

    const-string v2, "read-message"

    invoke-virtual {v1, v2, v4}, Lhym;->M(Ljava/lang/String;I)V

    iget-object v0, v0, Lbwo;->mSettings:Lhym;

    const-string v1, "reply"

    invoke-virtual {v0, v1, v4}, Lhym;->M(Ljava/lang/String;I)V

    .line 269
    invoke-direct {p0}, Lcwh;->Dz()V

    .line 270
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 271
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    const-string v1, "Eyes free settings reset!"

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v6

    .line 273
    goto :goto_1

    .line 276
    :cond_2
    const-string v0, "trigger_e100_driving"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 280
    invoke-direct {p0}, Lcwh;->Tp()J

    move-result-wide v2

    .line 281
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    invoke-direct {v1, v4, v5}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    .line 283
    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 284
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    move v0, v6

    .line 285
    goto :goto_1

    .line 288
    :cond_3
    const-string v0, "trigger_e100_nondriving"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 289
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 292
    invoke-direct {p0}, Lcwh;->Tp()J

    move-result-wide v2

    .line 293
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v4, 0x2

    invoke-direct {v1, v4, v5}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    .line 295
    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 296
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    move v0, v6

    .line 297
    goto/16 :goto_1

    .line 300
    :cond_4
    const-string v0, "trigger_e100_exiting_vehicle"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 301
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 304
    invoke-direct {p0}, Lcwh;->Tp()J

    move-result-wide v2

    .line 305
    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v4, 0x6

    const/16 v5, 0x64

    invoke-direct {v1, v4, v5}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    .line 307
    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 308
    iget-object v0, p0, Lcwh;->blJ:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    move v0, v6

    .line 309
    goto/16 :goto_1

    :cond_5
    move v0, v4

    .line 312
    goto/16 :goto_1
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 325
    invoke-super {p0}, Lcyc;->onResume()V

    .line 328
    invoke-direct {p0}, Lcwh;->Dz()V

    .line 329
    return-void
.end method
