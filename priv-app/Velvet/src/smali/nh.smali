.class public final Lnh;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lng;


# static fields
.field private static mq:Ljava/text/SimpleDateFormat;

.field private static mr:Ljava/text/SimpleDateFormat;


# instance fields
.field private mA:Landroid/widget/TextView;

.field private mB:Lnl;

.field private mC:Lnw;

.field private mD:Landroid/widget/Button;

.field private mE:I

.field private mF:I

.field private mG:I

.field private mH:I

.field private mI:Ljava/util/Calendar;

.field private mJ:Lnd;

.field private mK:Z

.field private mL:Ljava/lang/String;

.field private mM:Ljava/lang/String;

.field private mN:Ljava/lang/String;

.field private mO:Ljava/lang/String;

.field private final ms:Ljava/util/Calendar;

.field private mt:Lnk;

.field private mu:Ljava/util/HashSet;

.field private mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

.field private mw:Landroid/widget/TextView;

.field private mx:Landroid/widget/LinearLayout;

.field private my:Landroid/widget/TextView;

.field private mz:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lnh;->mq:Ljava/text/SimpleDateFormat;

    .line 78
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lnh;->mr:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 80
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    .line 82
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lnh;->mu:Ljava/util/HashSet;

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lnh;->mE:I

    .line 97
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    iput v0, p0, Lnh;->mF:I

    .line 98
    const/16 v0, 0x76c

    iput v0, p0, Lnh;->mG:I

    .line 99
    const/16 v0, 0x834

    iput v0, p0, Lnh;->mH:I

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnh;->mK:Z

    .line 139
    return-void
.end method

.method public static a(Lnk;III)Lnh;
    .locals 3

    .prologue
    .line 150
    new-instance v0, Lnh;

    invoke-direct {v0}, Lnh;-><init>()V

    .line 151
    iput-object p0, v0, Lnh;->mt:Lnk;

    iget-object v1, v0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Ljava/util/Calendar;->set(II)V

    iget-object v1, v0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2}, Ljava/util/Calendar;->set(II)V

    iget-object v1, v0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p3}, Ljava/util/Calendar;->set(II)V

    .line 152
    return-object v0
.end method

.method static synthetic a(Lnh;)Lnk;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lnh;->mt:Lnk;

    return-object v0
.end method

.method private ad(I)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 288
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 290
    packed-switch p1, :pswitch_data_0

    .line 332
    :goto_0
    return-void

    .line 292
    :pswitch_0
    iget-object v2, p0, Lnh;->mx:Landroid/widget/LinearLayout;

    const v3, 0x3f666666    # 0.9f

    const v4, 0x3f866666    # 1.05f

    invoke-static {v2, v3, v4}, Lnf;->b(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 294
    iget-boolean v3, p0, Lnh;->mK:Z

    if-eqz v3, :cond_0

    .line 295
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 296
    iput-boolean v5, p0, Lnh;->mK:Z

    .line 298
    :cond_0
    iget-object v3, p0, Lnh;->mB:Lnl;

    invoke-virtual {v3}, Lnl;->cp()V

    .line 299
    iget v3, p0, Lnh;->mE:I

    if-eq v3, p1, :cond_1

    .line 300
    iget-object v3, p0, Lnh;->mx:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 301
    iget-object v3, p0, Lnh;->mA:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 302
    iget-object v3, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v3, v5}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setDisplayedChild(I)V

    .line 303
    iput p1, p0, Lnh;->mE:I

    .line 305
    :cond_1
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 307
    invoke-virtual {p0}, Lnh;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v0, v1, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lnh;->mL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 310
    iget-object v0, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v1, p0, Lnh;->mM:Ljava/lang/String;

    invoke-static {v0, v1}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 313
    :pswitch_1
    iget-object v2, p0, Lnh;->mA:Landroid/widget/TextView;

    const v3, 0x3f59999a    # 0.85f

    const v4, 0x3f8ccccd    # 1.1f

    invoke-static {v2, v3, v4}, Lnf;->b(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 314
    iget-boolean v3, p0, Lnh;->mK:Z

    if-eqz v3, :cond_2

    .line 315
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 316
    iput-boolean v5, p0, Lnh;->mK:Z

    .line 318
    :cond_2
    iget-object v3, p0, Lnh;->mC:Lnw;

    invoke-virtual {v3}, Lnw;->cp()V

    .line 319
    iget v3, p0, Lnh;->mE:I

    if-eq v3, p1, :cond_3

    .line 320
    iget-object v3, p0, Lnh;->mx:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 321
    iget-object v3, p0, Lnh;->mA:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 322
    iget-object v3, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v3, v6}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setDisplayedChild(I)V

    .line 323
    iput p1, p0, Lnh;->mE:I

    .line 325
    :cond_3
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 327
    sget-object v2, Lnh;->mq:Ljava/text/SimpleDateFormat;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 328
    iget-object v1, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lnh;->mN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v0, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v1, p0, Lnh;->mO:Ljava/lang/String;

    invoke-static {v0, v1}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lnh;)Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    return-object v0
.end method

.method private co()V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lnh;->mu:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 469
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnj;

    invoke-interface {v0}, Lnj;->cp()V

    goto :goto_0

    .line 472
    :cond_0
    return-void
.end method

.method private l(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 335
    iget-object v0, p0, Lnh;->mw:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lnh;->mw:Landroid/widget/TextView;

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    :cond_0
    iget-object v0, p0, Lnh;->my:Landroid/widget/TextView;

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v4, v2, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    iget-object v0, p0, Lnh;->mz:Landroid/widget/TextView;

    sget-object v1, Lnh;->mr:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v0, p0, Lnh;->mA:Landroid/widget/TextView;

    sget-object v1, Lnh;->mq:Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 347
    iget-object v2, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v2, v0, v1}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->c(J)V

    .line 348
    invoke-virtual {p0}, Lnh;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/16 v3, 0x18

    invoke-static {v2, v0, v1, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 350
    iget-object v3, p0, Lnh;->mx:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 352
    if-eqz p1, :cond_1

    .line 353
    invoke-virtual {p0}, Lnh;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/16 v3, 0x14

    invoke-static {v2, v0, v1, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-static {v1, v0}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 357
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 1

    .prologue
    .line 387
    iput-object p1, p0, Lnh;->mI:Ljava/util/Calendar;

    .line 389
    iget-object v0, p0, Lnh;->mB:Lnl;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lnh;->mB:Lnl;

    invoke-virtual {v0}, Lnl;->cq()V

    .line 392
    :cond_0
    return-void
.end method

.method public final a(Lnj;)V
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lnh;->mu:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 498
    return-void
.end method

.method public final a(Lnk;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lnh;->mt:Lnk;

    .line 425
    return-void
.end method

.method public final ac(I)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x1

    .line 451
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v0, p1}, Lnf;->m(II)I

    move-result v0

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v1, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 452
    :cond_0
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->set(II)V

    .line 453
    invoke-direct {p0}, Lnh;->co()V

    .line 454
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lnh;->ad(I)V

    .line 455
    invoke-direct {p0, v2}, Lnh;->l(Z)V

    .line 456
    return-void
.end method

.method public final c(III)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 460
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->set(II)V

    .line 461
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 462
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 463
    invoke-direct {p0}, Lnh;->co()V

    .line 464
    invoke-direct {p0, v2}, Lnh;->l(Z)V

    .line 465
    return-void
.end method

.method public final ci()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lnh;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->ci()V

    .line 508
    return-void
.end method

.method public final cj()Lnp;
    .locals 2

    .prologue
    .line 477
    new-instance v0, Lnp;

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-direct {v0, v1}, Lnp;-><init>(Ljava/util/Calendar;)V

    return-object v0
.end method

.method public final ck()I
    .locals 1

    .prologue
    .line 482
    iget v0, p0, Lnh;->mG:I

    return v0
.end method

.method public final cl()I
    .locals 1

    .prologue
    .line 487
    iget v0, p0, Lnh;->mH:I

    return v0
.end method

.method public final cm()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lnh;->mI:Ljava/util/Calendar;

    return-object v0
.end method

.method public final cn()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getFirstDayOfWeek()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Lnh;->mF:I

    return v0
.end method

.method public final n(II)V
    .locals 1

    .prologue
    .line 371
    const/16 v0, 0x7b2

    iput v0, p0, Lnh;->mG:I

    .line 375
    const/16 v0, 0x7f4

    iput v0, p0, Lnh;->mH:I

    .line 376
    iget-object v0, p0, Lnh;->mB:Lnl;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lnh;->mB:Lnl;

    invoke-virtual {v0}, Lnl;->cq()V

    .line 379
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lnh;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->ci()V

    .line 442
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f110148

    if-ne v0, v1, :cond_1

    .line 443
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lnh;->ad(I)V

    .line 447
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f110145

    if-ne v0, v1, :cond_0

    .line 445
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lnh;->ad(I)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lnh;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 168
    if-eqz p1, :cond_0

    .line 169
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v1, 0x1

    const-string v2, "year"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 170
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v1, 0x2

    const-string v2, "month"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 171
    iget-object v0, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v1, 0x5

    const-string v2, "day"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 173
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    .line 198
    invoke-virtual {p0}, Lnh;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 201
    const v0, 0x7f040054

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 203
    const v0, 0x7f110143

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnh;->mw:Landroid/widget/TextView;

    .line 204
    const v0, 0x7f110145

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnh;->mx:Landroid/widget/LinearLayout;

    .line 205
    iget-object v0, p0, Lnh;->mx:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    const v0, 0x7f110146

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnh;->my:Landroid/widget/TextView;

    .line 207
    const v0, 0x7f110147

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnh;->mz:Landroid/widget/TextView;

    .line 208
    const v0, 0x7f110148

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnh;->mA:Landroid/widget/TextView;

    .line 209
    iget-object v0, p0, Lnh;->mA:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    const/4 v2, -0x1

    .line 212
    const/4 v1, 0x0

    .line 213
    const/4 v0, 0x0

    .line 214
    if-eqz p3, :cond_2

    .line 215
    const-string v0, "week_start"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnh;->mF:I

    .line 216
    const-string v0, "year_start"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnh;->mG:I

    .line 217
    const-string v0, "year_end"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnh;->mH:I

    .line 218
    const-string v0, "current_view"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 219
    const-string v1, "list_position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 220
    const-string v1, "list_position_offset"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 223
    :goto_0
    invoke-virtual {p0}, Lnh;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 224
    new-instance v0, Lnt;

    invoke-direct {v0, v5, p0}, Lnt;-><init>(Landroid/content/Context;Lng;)V

    iput-object v0, p0, Lnh;->mB:Lnl;

    .line 225
    new-instance v0, Lnw;

    invoke-direct {v0, v5, p0}, Lnw;-><init>(Landroid/content/Context;Lng;)V

    iput-object v0, p0, Lnh;->mC:Lnw;

    .line 227
    invoke-virtual {p0}, Lnh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 228
    const v6, 0x7f0a0060

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnh;->mL:Ljava/lang/String;

    .line 229
    const v6, 0x7f0a0062

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnh;->mM:Ljava/lang/String;

    .line 230
    const v6, 0x7f0a0061

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnh;->mN:Ljava/lang/String;

    .line 231
    const v6, 0x7f0a0063

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnh;->mO:Ljava/lang/String;

    .line 233
    const v0, 0x7f110149

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iput-object v0, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    .line 234
    iget-object v0, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v6, p0, Lnh;->mB:Lnl;

    invoke-virtual {v0, v6}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->addView(Landroid/view/View;)V

    .line 235
    iget-object v0, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v6, p0, Lnh;->mC:Lnw;

    invoke-virtual {v0, v6}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->addView(Landroid/view/View;)V

    .line 236
    iget-object v0, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v6, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->c(J)V

    .line 238
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 239
    const-wide/16 v6, 0x12c

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 240
    iget-object v6, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v6, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 242
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    invoke-direct {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 243
    const-wide/16 v6, 0x12c

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 244
    iget-object v6, p0, Lnh;->mv:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v6, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 246
    const v0, 0x7f110142

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnh;->mD:Landroid/widget/Button;

    .line 247
    iget-object v0, p0, Lnh;->mD:Landroid/widget/Button;

    new-instance v6, Lni;

    invoke-direct {v6, p0}, Lni;-><init>(Lnh;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lnh;->l(Z)V

    .line 261
    invoke-direct {p0, v1}, Lnh;->ad(I)V

    .line 263
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    .line 264
    if-nez v1, :cond_1

    .line 265
    iget-object v0, p0, Lnh;->mB:Lnl;

    invoke-virtual {v0, v3}, Lnl;->ae(I)V

    .line 271
    :cond_0
    :goto_1
    new-instance v0, Lnd;

    invoke-direct {v0, v5}, Lnd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnh;->mJ:Lnd;

    .line 272
    return-object v4

    .line 266
    :cond_1
    const/4 v0, 0x1

    if-ne v1, v0, :cond_0

    .line 267
    iget-object v0, p0, Lnh;->mC:Lnw;

    invoke-virtual {v0, v3, v2}, Lnw;->o(II)V

    goto :goto_1

    :cond_2
    move v3, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 283
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 284
    iget-object v0, p0, Lnh;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->stop()V

    .line 285
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 277
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 278
    iget-object v0, p0, Lnh;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->start()V

    .line 279
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 177
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 178
    const-string v0, "year"

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    const-string v0, "month"

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    const-string v0, "day"

    iget-object v1, p0, Lnh;->ms:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    const-string v0, "week_start"

    iget v1, p0, Lnh;->mF:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v0, "year_start"

    iget v1, p0, Lnh;->mG:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 183
    const-string v0, "year_end"

    iget v1, p0, Lnh;->mH:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v0, "current_view"

    iget v1, p0, Lnh;->mE:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 185
    const/4 v0, -0x1

    .line 186
    iget v1, p0, Lnh;->mE:I

    if-nez v1, :cond_1

    .line 187
    iget-object v0, p0, Lnh;->mB:Lnl;

    invoke-virtual {v0}, Lnl;->cs()I

    move-result v0

    .line 192
    :cond_0
    :goto_0
    const-string v1, "list_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    return-void

    .line 188
    :cond_1
    iget v1, p0, Lnh;->mE:I

    if-ne v1, v3, :cond_0

    .line 189
    iget-object v0, p0, Lnh;->mC:Lnw;

    invoke-virtual {v0}, Lnw;->getFirstVisiblePosition()I

    move-result v0

    .line 190
    const-string v1, "list_position_offset"

    iget-object v2, p0, Lnh;->mC:Lnw;

    invoke-virtual {v2}, Lnw;->cz()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final setFirstDayOfWeek(I)V
    .locals 2

    .prologue
    .line 360
    if-lez p1, :cond_0

    const/4 v0, 0x7

    if-le p1, v0, :cond_1

    .line 361
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value must be between Calendar.SUNDAY and Calendar.SATURDAY"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_1
    iput p1, p0, Lnh;->mF:I

    .line 365
    iget-object v0, p0, Lnh;->mB:Lnl;

    if-eqz v0, :cond_2

    .line 366
    iget-object v0, p0, Lnh;->mB:Lnl;

    invoke-virtual {v0}, Lnl;->cq()V

    .line 368
    :cond_2
    return-void
.end method
