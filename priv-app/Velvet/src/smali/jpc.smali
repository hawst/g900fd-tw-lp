.class public final Ljpc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ewZ:Ljava/lang/String;

.field private exa:Ljava/lang/String;

.field public exb:Ljpf;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4224
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4225
    const/4 v0, 0x0

    iput v0, p0, Ljpc;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljpc;->ewZ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljpc;->exa:Ljava/lang/String;

    iput-object v1, p0, Ljpc;->exb:Ljpf;

    iput-object v1, p0, Ljpc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpc;->eCz:I

    .line 4226
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4158
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpc;->ewZ:Ljava/lang/String;

    iget v0, p0, Ljpc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpc;->exa:Ljava/lang/String;

    iget v0, p0, Ljpc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpc;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljpc;->exb:Ljpf;

    if-nez v0, :cond_1

    new-instance v0, Ljpf;

    invoke-direct {v0}, Ljpf;-><init>()V

    iput-object v0, p0, Ljpc;->exb:Ljpf;

    :cond_1
    iget-object v0, p0, Ljpc;->exb:Ljpf;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4241
    iget v0, p0, Ljpc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4242
    const/4 v0, 0x1

    iget-object v1, p0, Ljpc;->ewZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4244
    :cond_0
    iget v0, p0, Ljpc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4245
    const/4 v0, 0x2

    iget-object v1, p0, Ljpc;->exa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4247
    :cond_1
    iget-object v0, p0, Ljpc;->exb:Ljpf;

    if-eqz v0, :cond_2

    .line 4248
    const/4 v0, 0x3

    iget-object v1, p0, Ljpc;->exb:Ljpf;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4250
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4251
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4255
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4256
    iget v1, p0, Ljpc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4257
    const/4 v1, 0x1

    iget-object v2, p0, Ljpc;->ewZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4260
    :cond_0
    iget v1, p0, Ljpc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4261
    const/4 v1, 0x2

    iget-object v2, p0, Ljpc;->exa:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4264
    :cond_1
    iget-object v1, p0, Ljpc;->exb:Ljpf;

    if-eqz v1, :cond_2

    .line 4265
    const/4 v1, 0x3

    iget-object v2, p0, Ljpc;->exb:Ljpf;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4268
    :cond_2
    return v0
.end method
