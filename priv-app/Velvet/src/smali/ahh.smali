.class public abstract Lahh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final Yl:Ljava/lang/Object;

.field private static Ym:Lahh;


# instance fields
.field final Yn:Landroid/appwidget/AppWidgetManager;

.field final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lahh;->Yl:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lahh;->mContext:Landroid/content/Context;

    .line 57
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lahh;->Yn:Landroid/appwidget/AppWidgetManager;

    .line 58
    return-void
.end method

.method public static A(Landroid/content/Context;)Lahh;
    .locals 3

    .prologue
    .line 40
    sget-object v1, Lahh;->Yl:Ljava/lang/Object;

    monitor-enter v1

    .line 41
    :try_start_0
    sget-object v0, Lahh;->Ym:Lahh;

    if-nez v0, :cond_0

    .line 42
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    new-instance v0, Lahj;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lahj;-><init>(Landroid/content/Context;)V

    sput-object v0, Lahh;->Ym:Lahh;

    .line 48
    :cond_0
    :goto_0
    sget-object v0, Lahh;->Ym:Lahh;

    monitor-exit v1

    return-object v0

    .line 45
    :cond_1
    new-instance v0, Lahi;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lahi;-><init>(Landroid/content/Context;)V

    sput-object v0, Lahh;->Ym:Lahh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a(Landroid/appwidget/AppWidgetProviderInfo;Lwi;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract a(Landroid/appwidget/AppWidgetProviderInfo;ILandroid/app/Activity;Landroid/appwidget/AppWidgetHost;I)V
.end method

.method public abstract a(ILandroid/appwidget/AppWidgetProviderInfo;Landroid/os/Bundle;)Z
.end method

.method public abstract b(Landroid/appwidget/AppWidgetProviderInfo;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
.end method

.method public abstract b(Landroid/appwidget/AppWidgetProviderInfo;)Ljava/lang/String;
.end method

.method public abstract c(Landroid/appwidget/AppWidgetProviderInfo;)Lahz;
.end method

.method public abstract d(Landroid/appwidget/AppWidgetProviderInfo;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getAllProviders()Ljava/util/List;
.end method

.method public final getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lahh;->Yn:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v0, p1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    return-object v0
.end method
