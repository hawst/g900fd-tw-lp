.class public final Lidi;
.super Lidh;
.source "PG"


# direct methods
.method public constructor <init>(Lchk;Lcke;Lcrh;Ldyv;)V
    .locals 0
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcrh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lidh;-><init>(Lchk;Lcke;Lcrh;Ldyv;)V

    .line 46
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->d(Lcom/google/android/search/shared/actions/AddEventAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->b(Lcom/google/android/search/shared/actions/AgendaAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->b(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->b(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->c(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->d(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->c(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lidi;->b(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final aUT()Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Lcom/google/android/search/shared/actions/AgendaAction;)Ljkt;
    .locals 4

    .prologue
    .line 95
    new-instance v1, Ljkx;

    invoke-direct {v1}, Ljkx;-><init>()V

    .line 96
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agV()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljnm;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljnm;

    iput-object v0, v1, Ljkx;->eqJ:[Ljnm;

    .line 97
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agS()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljkx;->iv(Z)Ljkx;

    .line 98
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agR()I

    move-result v0

    invoke-virtual {v1, v0}, Ljkx;->qA(I)Ljkx;

    .line 99
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agU()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljkx;->iw(Z)Ljkx;

    .line 100
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agZ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljkx;->ww(Ljava/lang/String;)Ljkx;

    .line 102
    new-instance v0, Ljns;

    invoke-direct {v0}, Ljns;-><init>()V

    iput-object v0, v1, Ljkx;->eqI:Ljns;

    .line 103
    iget-object v0, v1, Ljkx;->eqI:Ljns;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->getStartTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljns;->dB(J)Ljns;

    .line 104
    iget-object v0, v1, Ljkx;->eqI:Ljns;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agT()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljns;->dC(J)Ljns;

    .line 105
    iget-object v0, v1, Ljkx;->eqI:Ljns;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agW()I

    move-result v2

    invoke-virtual {v0, v2}, Ljns;->qW(I)Ljns;

    .line 107
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 108
    sget-object v2, Ljkx;->eqH:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 109
    return-object v0
.end method

.method public final b(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljkt;
    .locals 3

    .prologue
    .line 207
    new-instance v0, Ljme;

    invoke-direct {v0}, Ljme;-><init>()V

    .line 208
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ContactOptInAction;->afW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljme;->wP(Ljava/lang/String;)Ljme;

    .line 212
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    .line 213
    sget-object v2, Ljme;->etr:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 214
    return-object v1
.end method

.method public final b(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljkt;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Ljmd;

    invoke-direct {v0}, Ljmd;-><init>()V

    .line 115
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/OpenUrlAction;->ahK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljmd;->wN(Ljava/lang/String;)Ljmd;

    .line 116
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/OpenUrlAction;->ahL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljmd;->wM(Ljava/lang/String;)Ljmd;

    .line 117
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/OpenUrlAction;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljmd;->wO(Ljava/lang/String;)Ljmd;

    .line 119
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    .line 120
    sget-object v2, Ljmd;->etp:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 121
    return-object v1
.end method

.method public final b(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljkt;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 126
    new-instance v0, Ljmr;

    invoke-direct {v0}, Ljmr;-><init>()V

    .line 128
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    .line 129
    if-eqz v1, :cond_0

    .line 130
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    iget-object v2, p0, Lidi;->mContactLabelConverter:Ldyv;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Ldyv;Z)Ljoq;

    move-result-object v1

    iput-object v1, v0, Ljmr;->etx:Ljoq;

    .line 135
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->aiF()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 147
    :goto_0
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    .line 148
    sget-object v2, Ljmr;->eur:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 151
    return-object v1

    .line 137
    :pswitch_0
    invoke-virtual {v0, v3}, Ljmr;->iI(Z)Ljmr;

    goto :goto_0

    .line 140
    :pswitch_1
    invoke-virtual {v0, v3}, Ljmr;->iJ(Z)Ljmr;

    goto :goto_0

    .line 143
    :pswitch_2
    invoke-virtual {v0, v3}, Ljmr;->iK(Z)Ljmr;

    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljkt;
    .locals 3

    .prologue
    .line 167
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 168
    sget-object v1, Ljmh;->ety:Ljsm;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 169
    return-object v0
.end method

.method public final c(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljkt;
    .locals 3

    .prologue
    .line 156
    new-instance v0, Ljmq;

    invoke-direct {v0}, Ljmq;-><init>()V

    .line 157
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljmq;->iH(Z)Ljmq;

    .line 158
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetTimerAction;->aiD()I

    move-result v1

    invoke-virtual {v0, v1}, Ljmq;->qO(I)Ljmq;

    .line 160
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    .line 161
    sget-object v2, Ljmq;->eul:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 162
    return-object v1
.end method

.method public final d(Lcom/google/android/search/shared/actions/AddEventAction;)Ljkt;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 50
    new-instance v3, Ljnp;

    invoke-direct {v3}, Ljnp;-><init>()V

    .line 51
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->pO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljnp;->xq(Ljava/lang/String;)Ljnp;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->getLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljnp;->xr(Ljava/lang/String;)Ljnp;

    .line 53
    new-instance v0, Ljno;

    invoke-direct {v0}, Ljno;-><init>()V

    iput-object v0, v3, Ljnp;->euY:Ljno;

    .line 54
    iget-object v0, v3, Ljnp;->euY:Ljno;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agN()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljno;->dA(J)Ljno;

    .line 55
    new-instance v0, Ljno;

    invoke-direct {v0}, Ljno;-><init>()V

    iput-object v0, v3, Ljnp;->euZ:Ljno;

    .line 56
    iget-object v0, v3, Ljnp;->euZ:Ljno;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agQ()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljno;->dA(J)Ljno;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agM()Ljava/util/List;

    move-result-object v4

    .line 59
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 60
    if-lez v5, :cond_0

    .line 61
    new-array v0, v5, [Ljnr;

    iput-object v0, v3, Ljnp;->evt:[Ljnr;

    move v2, v1

    .line 62
    :goto_0
    if-ge v2, v5, :cond_0

    .line 63
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/util/Reminder;

    .line 64
    new-instance v6, Ljnr;

    invoke-direct {v6}, Ljnr;-><init>()V

    .line 65
    invoke-virtual {v0}, Lcom/google/android/search/shared/util/Reminder;->aoa()I

    move-result v7

    invoke-virtual {v6, v7}, Ljnr;->qU(I)Ljnr;

    .line 66
    invoke-virtual {v0}, Lcom/google/android/search/shared/util/Reminder;->getMethod()I

    move-result v0

    invoke-virtual {v6, v0}, Ljnr;->qV(I)Ljnr;

    .line 67
    iget-object v0, v3, Ljnp;->evt:[Ljnr;

    aput-object v6, v0, v2

    .line 62
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 71
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AddEventAction;->agL()Ljava/util/List;

    move-result-object v2

    .line 72
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 73
    if-lez v4, :cond_1

    .line 74
    new-array v0, v4, [Ljnq;

    iput-object v0, v3, Ljnp;->evr:[Ljnq;

    .line 75
    :goto_1
    if-ge v1, v4, :cond_1

    .line 76
    new-instance v5, Ljnq;

    invoke-direct {v5}, Ljnq;-><init>()V

    .line 77
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljnq;->xs(Ljava/lang/String;)Ljnq;

    .line 78
    iget-object v0, v3, Ljnp;->evr:[Ljnq;

    aput-object v5, v0, v1

    .line 75
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 82
    :cond_1
    new-instance v0, Ljkv;

    invoke-direct {v0}, Ljkv;-><init>()V

    .line 84
    iput-object v3, v0, Ljkv;->eqv:Ljnp;

    .line 86
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    .line 87
    sget-object v2, Ljkv;->equ:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 90
    return-object v1
.end method

.method public final d(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljkt;
    .locals 4

    .prologue
    .line 174
    invoke-super {p0, p1}, Lidh;->d(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljkt;

    move-result-object v1

    .line 175
    sget-object v0, Ljkw;->eqw:Ljsm;

    invoke-virtual {v1, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljkw;

    .line 179
    iget-object v2, v0, Ljkw;->eqz:Ljkr;

    if-eqz v2, :cond_0

    iget-object v2, v0, Ljkw;->eqz:Ljkr;

    invoke-virtual {v2}, Ljkr;->bop()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    const/4 v2, 0x0

    iput-object v2, v0, Ljkw;->eqz:Ljkr;

    .line 184
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 185
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiA()Ljpd;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 186
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiA()Ljpd;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiB()Ljpd;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 189
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiB()Ljpd;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 193
    iget-object v3, v0, Ljkw;->eqA:Ljls;

    if-nez v3, :cond_3

    .line 194
    new-instance v3, Ljls;

    invoke-direct {v3}, Ljls;-><init>()V

    iput-object v3, v0, Ljkw;->eqA:Ljls;

    .line 196
    :cond_3
    iget-object v0, v0, Ljkw;->eqA:Ljls;

    .line 197
    new-instance v3, Ljln;

    invoke-direct {v3}, Ljln;-><init>()V

    iput-object v3, v0, Ljls;->esM:Ljln;

    .line 198
    iget-object v3, v0, Ljls;->esM:Ljln;

    const/4 v0, 0x0

    new-array v0, v0, [Ljpd;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljpd;

    iput-object v0, v3, Ljln;->esu:[Ljpd;

    .line 202
    :cond_4
    return-object v1
.end method
