.class public final enum Ldvl;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum bNA:Ldvl;

.field public static final enum bNB:Ldvl;

.field private static final synthetic bNF:[Ldvl;


# instance fields
.field public final bNC:Ljava/lang/String;

.field private bND:I

.field private bNE:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Ldvl;

    const-string v1, "GOOGLE_PLUS"

    const-string v3, "com.google.android.apps.plus"

    const v4, 0x7f0a08cb

    const v5, 0x7f0a08cd

    invoke-direct/range {v0 .. v5}, Ldvl;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Ldvl;->bNA:Ldvl;

    .line 27
    new-instance v3, Ldvl;

    const-string v4, "TWITTER"

    const-string v6, "com.twitter.android"

    const v7, 0x7f0a08cc

    const v8, 0x7f0a08ce

    move v5, v9

    invoke-direct/range {v3 .. v8}, Ldvl;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Ldvl;->bNB:Ldvl;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Ldvl;

    sget-object v1, Ldvl;->bNA:Ldvl;

    aput-object v1, v0, v2

    sget-object v1, Ldvl;->bNB:Ldvl;

    aput-object v1, v0, v9

    sput-object v0, Ldvl;->bNF:[Ldvl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Ldvl;->bNC:Ljava/lang/String;

    .line 34
    iput p4, p0, Ldvl;->bND:I

    .line 35
    iput p5, p0, Ldvl;->bNE:I

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldvl;
    .locals 1

    .prologue
    .line 23
    const-class v0, Ldvl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldvl;

    return-object v0
.end method

.method public static values()[Ldvl;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Ldvl;->bNF:[Ldvl;

    invoke-virtual {v0}, [Ldvl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldvl;

    return-object v0
.end method


# virtual methods
.method public final aiO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldvl;->bNC:Ljava/lang/String;

    return-object v0
.end method

.method public final aiP()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Ldvl;->bNE:I

    return v0
.end method

.method public final getNameResId()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Ldvl;->bND:I

    return v0
.end method
