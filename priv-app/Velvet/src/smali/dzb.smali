.class public final enum Ldzb;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum bRp:Ldzb;

.field public static final enum bRq:Ldzb;

.field public static final enum bRr:Ldzb;

.field public static final enum bRs:Ldzb;

.field public static final enum bRt:Ldzb;

.field private static final synthetic bRv:[Ldzb;


# instance fields
.field public final bRu:Z

.field private final bvb:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 12
    new-instance v0, Ldzb;

    const-string v1, "EMAIL"

    const-string v2, "emails_data_id"

    invoke-direct {v0, v1, v4, v2, v3}, Ldzb;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Ldzb;->bRp:Ldzb;

    .line 13
    new-instance v0, Ldzb;

    const-string v1, "PHONE_NUMBER"

    const-string v2, "phones_data_id"

    invoke-direct {v0, v1, v3, v2, v3}, Ldzb;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Ldzb;->bRq:Ldzb;

    .line 14
    new-instance v0, Ldzb;

    const-string v1, "POSTAL_ADDRESS"

    const-string v2, "postals_data_id"

    invoke-direct {v0, v1, v5, v2, v3}, Ldzb;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Ldzb;->bRr:Ldzb;

    .line 15
    new-instance v0, Ldzb;

    const-string v1, "GAIA_ID"

    const-string v2, ""

    invoke-direct {v0, v1, v6, v2, v4}, Ldzb;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Ldzb;->bRs:Ldzb;

    .line 16
    new-instance v0, Ldzb;

    const-string v1, "PERSON"

    const-string v2, "contacts"

    invoke-direct {v0, v1, v7, v2, v4}, Ldzb;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Ldzb;->bRt:Ldzb;

    .line 11
    const/4 v0, 0x5

    new-array v0, v0, [Ldzb;

    sget-object v1, Ldzb;->bRp:Ldzb;

    aput-object v1, v0, v4

    sget-object v1, Ldzb;->bRq:Ldzb;

    aput-object v1, v0, v3

    sget-object v1, Ldzb;->bRr:Ldzb;

    aput-object v1, v0, v5

    sget-object v1, Ldzb;->bRs:Ldzb;

    aput-object v1, v0, v6

    sget-object v1, Ldzb;->bRt:Ldzb;

    aput-object v1, v0, v7

    sput-object v0, Ldzb;->bRv:[Ldzb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput-object p3, p0, Ldzb;->bvb:Ljava/lang/String;

    .line 28
    iput-boolean p4, p0, Ldzb;->bRu:Z

    .line 29
    return-void
.end method

.method public static a(Ljop;)Ldzb;
    .locals 2
    .param p0    # Ljop;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 58
    iget-object v0, p0, Ljop;->dQP:[I

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 59
    iget-object v0, p0, Ljop;->dQP:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 60
    packed-switch v0, :pswitch_data_0

    .line 69
    :cond_0
    sget-object v0, Ldzb;->bRt:Ldzb;

    :goto_0
    return-object v0

    .line 62
    :pswitch_0
    sget-object v0, Ldzb;->bRq:Ldzb;

    goto :goto_0

    .line 64
    :pswitch_1
    sget-object v0, Ldzb;->bRp:Ldzb;

    goto :goto_0

    .line 66
    :pswitch_2
    sget-object v0, Ldzb;->bRr:Ldzb;

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Ldzb;)I
    .locals 2

    .prologue
    .line 45
    sget-object v0, Ldzc;->bRb:[I

    invoke-virtual {p0}, Ldzb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 47
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 51
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static kx(Ljava/lang/String;)Ldzb;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 32
    sget-object v0, Ldzb;->bRq:Ldzb;

    iget-object v0, v0, Ldzb;->bvb:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    sget-object v0, Ldzb;->bRq:Ldzb;

    .line 41
    :goto_0
    return-object v0

    .line 35
    :cond_0
    sget-object v0, Ldzb;->bRp:Ldzb;

    iget-object v0, v0, Ldzb;->bvb:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    sget-object v0, Ldzb;->bRp:Ldzb;

    goto :goto_0

    .line 38
    :cond_1
    sget-object v0, Ldzb;->bRr:Ldzb;

    iget-object v0, v0, Ldzb;->bvb:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39
    sget-object v0, Ldzb;->bRr:Ldzb;

    goto :goto_0

    .line 41
    :cond_2
    sget-object v0, Ldzb;->bRt:Ldzb;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Ldzb;
    .locals 1

    .prologue
    .line 11
    const-class v0, Ldzb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldzb;

    return-object v0
.end method

.method public static values()[Ldzb;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Ldzb;->bRv:[Ldzb;

    invoke-virtual {v0}, [Ldzb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldzb;

    return-object v0
.end method
