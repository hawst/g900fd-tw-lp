.class public final Leut;
.super Landroid/webkit/WebViewClient;
.source "PG"


# instance fields
.field private synthetic cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 269
    const-string v0, "GoogleServiceWebviewWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received error while loading page("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, v1, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->J(Landroid/net/Uri;)V

    .line 271
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    .line 272
    return-void
.end method

.method public final onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 277
    const-string v0, "GoogleServiceWebviewWrapper"

    const-string v1, "Auth error while loading page"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, v1, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->J(Landroid/net/Uri;)V

    .line 280
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    .line 281
    return-void
.end method

.method public final onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 286
    const-string v0, "GoogleServiceWebviewWrapper"

    const-string v1, "Login Request while loading page"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, v1, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->J(Landroid/net/Uri;)V

    .line 288
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    .line 289
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2

    .prologue
    .line 293
    const-string v0, "GoogleServiceWebviewWrapper"

    const-string v1, "Ssl Error while loading page"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, v1, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->J(Landroid/net/Uri;)V

    .line 295
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    .line 296
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 214
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 215
    if-nez v3, :cond_0

    move v0, v1

    .line 263
    :goto_0
    return v0

    .line 221
    :cond_0
    sget-object v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->cjn:Ljava/util/Set;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->J(Landroid/net/Uri;)V

    move v0, v2

    .line 224
    goto :goto_0

    .line 228
    :cond_1
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    .line 229
    if-nez v4, :cond_2

    move v0, v1

    .line 231
    goto :goto_0

    .line 235
    :cond_2
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 236
    if-nez v0, :cond_3

    .line 237
    const-string v0, ""

    .line 239
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 241
    const-string v0, "accounts.google."

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 248
    goto :goto_0

    .line 252
    :cond_4
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v0, v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->cjm:[Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 253
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v5, v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->cjm:[Ljava/lang/String;

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_7

    aget-object v7, v5, v0

    .line 254
    const-string v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_5
    move v0, v1

    .line 255
    goto :goto_0

    .line 253
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 262
    :cond_7
    iget-object v0, p0, Leut;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->J(Landroid/net/Uri;)V

    move v0, v2

    .line 263
    goto :goto_0
.end method
