.class final Ldky;
.super Ldlg;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 612
    invoke-direct {p0}, Ldlg;-><init>()V

    return-void
.end method

.method private c(Lbzh;[BI)[B
    .locals 2
    .param p1    # Lbzh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 617
    :try_start_0
    invoke-static {p3}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 618
    invoke-static {p1, p2}, Ldky;->a(Lbzh;[B)V

    .line 619
    invoke-interface {p1}, Lbzh;->getContentLength()I

    move-result v0

    .line 620
    invoke-static {p1}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;

    move-result-object v1

    .line 621
    invoke-static {v1, v0}, Ldkx;->a(Ljava/io/InputStream;I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 623
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 624
    invoke-interface {p1}, Lbzh;->disconnect()V

    return-object v0

    .line 623
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 624
    invoke-interface {p1}, Lbzh;->disconnect()V

    throw v0
.end method


# virtual methods
.method public final synthetic b(Lbzh;[BI)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 611
    invoke-direct {p0, p1, p2, p3}, Ldky;->c(Lbzh;[BI)[B

    move-result-object v0

    return-object v0
.end method
