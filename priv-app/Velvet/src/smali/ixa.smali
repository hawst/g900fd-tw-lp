.class public final Lixa;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private ajk:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59960
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 59961
    const/4 v0, 0x0

    iput v0, p0, Lixa;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lixa;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lixa;->ajk:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lixa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixa;->eCz:I

    .line 59962
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 59897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixa;->afh:Ljava/lang/String;

    iget v0, p0, Lixa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixa;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixa;->ajk:Ljava/lang/String;

    iget v0, p0, Lixa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixa;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 59976
    iget v0, p0, Lixa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 59977
    const/4 v0, 0x1

    iget-object v1, p0, Lixa;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59979
    :cond_0
    iget v0, p0, Lixa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 59980
    const/4 v0, 0x2

    iget-object v1, p0, Lixa;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59982
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 59983
    return-void
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59919
    iget-object v0, p0, Lixa;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59941
    iget-object v0, p0, Lixa;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 59987
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 59988
    iget v1, p0, Lixa;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 59989
    const/4 v1, 0x1

    iget-object v2, p0, Lixa;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59992
    :cond_0
    iget v1, p0, Lixa;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 59993
    const/4 v1, 0x2

    iget-object v2, p0, Lixa;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59996
    :cond_1
    return v0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 59930
    iget v0, p0, Lixa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qG()Z
    .locals 1

    .prologue
    .line 59952
    iget v0, p0, Lixa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qe(Ljava/lang/String;)Lixa;
    .locals 1

    .prologue
    .line 59922
    if-nez p1, :cond_0

    .line 59923
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59925
    :cond_0
    iput-object p1, p0, Lixa;->afh:Ljava/lang/String;

    .line 59926
    iget v0, p0, Lixa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixa;->aez:I

    .line 59927
    return-object p0
.end method

.method public final qf(Ljava/lang/String;)Lixa;
    .locals 1

    .prologue
    .line 59944
    if-nez p1, :cond_0

    .line 59945
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59947
    :cond_0
    iput-object p1, p0, Lixa;->ajk:Ljava/lang/String;

    .line 59948
    iget v0, p0, Lixa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixa;->aez:I

    .line 59949
    return-object p0
.end method
