.class public final Ljrt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eBN:[Ljrt;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private eBO:[B

.field private eBP:I

.field private eBd:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1389
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1390
    iput v1, p0, Ljrt;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljrt;->ajk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrt;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrt;->eBd:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljrt;->eBO:[B

    iput v1, p0, Ljrt;->eBP:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljrt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrt;->eCz:I

    .line 1391
    return-void
.end method

.method public static btF()[Ljrt;
    .locals 2

    .prologue
    .line 1269
    sget-object v0, Ljrt;->eBN:[Ljrt;

    if-nez v0, :cond_1

    .line 1270
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1272
    :try_start_0
    sget-object v0, Ljrt;->eBN:[Ljrt;

    if-nez v0, :cond_0

    .line 1273
    const/4 v0, 0x0

    new-array v0, v0, [Ljrt;

    sput-object v0, Ljrt;->eBN:[Ljrt;

    .line 1275
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1277
    :cond_1
    sget-object v0, Ljrt;->eBN:[Ljrt;

    return-object v0

    .line 1275
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrt;->ajk:Ljava/lang/String;

    iget v0, p0, Ljrt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrt;->afh:Ljava/lang/String;

    iget v0, p0, Ljrt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrt;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrt;->eBd:Ljava/lang/String;

    iget v0, p0, Ljrt;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljrt;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljrt;->eBO:[B

    iget v0, p0, Ljrt;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljrt;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrt;->eBP:I

    iget v0, p0, Ljrt;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljrt;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1408
    iget v0, p0, Ljrt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1409
    const/4 v0, 0x1

    iget-object v1, p0, Ljrt;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1411
    :cond_0
    iget v0, p0, Ljrt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1412
    const/4 v0, 0x2

    iget-object v1, p0, Ljrt;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1414
    :cond_1
    iget v0, p0, Ljrt;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1415
    const/4 v0, 0x3

    iget-object v1, p0, Ljrt;->eBd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1417
    :cond_2
    iget v0, p0, Ljrt;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1418
    const/4 v0, 0x4

    iget-object v1, p0, Ljrt;->eBO:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 1420
    :cond_3
    iget v0, p0, Ljrt;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1421
    const/4 v0, 0x5

    iget v1, p0, Ljrt;->eBP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1423
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1424
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1428
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1429
    iget v1, p0, Ljrt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1430
    const/4 v1, 0x1

    iget-object v2, p0, Ljrt;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1433
    :cond_0
    iget v1, p0, Ljrt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1434
    const/4 v1, 0x2

    iget-object v2, p0, Ljrt;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1437
    :cond_1
    iget v1, p0, Ljrt;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1438
    const/4 v1, 0x3

    iget-object v2, p0, Ljrt;->eBd:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1441
    :cond_2
    iget v1, p0, Ljrt;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1442
    const/4 v1, 0x4

    iget-object v2, p0, Ljrt;->eBO:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1445
    :cond_3
    iget v1, p0, Ljrt;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1446
    const/4 v1, 0x5

    iget v2, p0, Ljrt;->eBP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1449
    :cond_4
    return v0
.end method
