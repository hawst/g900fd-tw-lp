.class final Lqv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final vP:Lqp;


# direct methods
.method constructor <init>(Lqp;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lqv;->vP:Lqp;

    .line 35
    return-void
.end method


# virtual methods
.method protected final c(Ljava/io/InputStream;)Lqo;
    .locals 5

    .prologue
    .line 46
    iget-object v0, p0, Lqv;->vP:Lqp;

    invoke-static {p1, v0}, Lqr;->a(Ljava/io/InputStream;Lqp;)Lqr;

    move-result-object v1

    .line 47
    new-instance v2, Lqo;

    invoke-virtual {v1}, Lqr;->dl()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-direct {v2, v0}, Lqo;-><init>(Ljava/nio/ByteOrder;)V

    .line 48
    invoke-virtual {v1}, Lqr;->next()I

    move-result v0

    .line 51
    :goto_0
    const/4 v3, 0x5

    if-eq v0, v3, :cond_4

    .line 52
    packed-switch v0, :pswitch_data_0

    .line 88
    :goto_1
    invoke-virtual {v1}, Lqr;->next()I

    move-result v0

    goto :goto_0

    .line 54
    :pswitch_0
    new-instance v0, Lqx;

    invoke-virtual {v1}, Lqr;->ds()I

    move-result v3

    invoke-direct {v0, v3}, Lqx;-><init>(I)V

    invoke-virtual {v2, v0}, Lqo;->a(Lqx;)V

    goto :goto_1

    .line 57
    :pswitch_1
    invoke-virtual {v1}, Lqr;->dr()Lqw;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lqw;->hasValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 59
    invoke-virtual {v1, v0}, Lqr;->a(Lqw;)V

    goto :goto_1

    .line 61
    :cond_0
    invoke-virtual {v0}, Lqw;->dy()I

    move-result v3

    invoke-virtual {v2, v3}, Lqo;->aA(I)Lqx;

    move-result-object v3

    invoke-virtual {v3, v0}, Lqx;->d(Lqw;)Lqw;

    goto :goto_1

    .line 65
    :pswitch_2
    invoke-virtual {v1}, Lqr;->dr()Lqw;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lqw;->dA()S

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_1

    .line 67
    invoke-virtual {v1, v0}, Lqr;->c(Lqw;)V

    .line 69
    :cond_1
    invoke-virtual {v0}, Lqw;->dy()I

    move-result v3

    invoke-virtual {v2, v3}, Lqo;->aA(I)Lqx;

    move-result-object v3

    invoke-virtual {v3, v0}, Lqx;->d(Lqw;)Lqw;

    goto :goto_1

    .line 72
    :pswitch_3
    invoke-virtual {v1}, Lqr;->dv()I

    move-result v0

    new-array v0, v0, [B

    .line 73
    array-length v3, v0

    invoke-virtual {v1, v0}, Lqr;->read([B)I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 74
    invoke-virtual {v2, v0}, Lqo;->a([B)V

    goto :goto_1

    .line 76
    :cond_2
    const-string v0, "ExifReader"

    const-string v3, "Failed to read the compressed thumbnail"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 80
    :pswitch_4
    invoke-virtual {v1}, Lqr;->du()I

    move-result v0

    new-array v0, v0, [B

    .line 81
    array-length v3, v0

    invoke-virtual {v1, v0}, Lqr;->read([B)I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 82
    invoke-virtual {v1}, Lqr;->dt()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lqo;->a(I[B)V

    goto :goto_1

    .line 84
    :cond_3
    const-string v0, "ExifReader"

    const-string v3, "Failed to read the strip bytes"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 90
    :cond_4
    return-object v2

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
