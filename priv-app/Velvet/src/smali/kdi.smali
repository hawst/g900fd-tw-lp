.class final Lkdi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final out:Ljava/io/OutputStream;


# direct methods
.method constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    iput-object p1, p0, Lkdi;->out:Ljava/io/OutputStream;

    .line 319
    return-void
.end method


# virtual methods
.method public final aI(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 323
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 324
    iget-object v0, p0, Lkdi;->out:Ljava/io/OutputStream;

    const/16 v3, 0x60

    invoke-virtual {v0, v3}, Ljava/io/OutputStream;->write(I)V

    .line 325
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkdi;->writeString(Ljava/lang/String;)V

    .line 326
    add-int/lit8 v0, v1, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkdi;->writeString(Ljava/lang/String;)V

    .line 323
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_0

    .line 328
    :cond_0
    return-void
.end method

.method public final writeString(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 355
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 356
    array-length v0, v1

    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lkdi;->out:Ljava/io/OutputStream;

    or-int/lit8 v0, v0, 0x0

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write(I)V

    .line 357
    :goto_0
    iget-object v0, p0, Lkdi;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 358
    return-void

    .line 356
    :cond_0
    iget-object v2, p0, Lkdi;->out:Ljava/io/OutputStream;

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    add-int/lit16 v0, v0, -0xff

    :goto_1
    const/16 v2, 0x80

    if-lt v0, v2, :cond_1

    and-int/lit8 v2, v0, 0x7f

    iget-object v3, p0, Lkdi;->out:Ljava/io/OutputStream;

    or-int/lit16 v2, v2, 0x80

    invoke-virtual {v3, v2}, Ljava/io/OutputStream;->write(I)V

    ushr-int/lit8 v0, v0, 0x7

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lkdi;->out:Ljava/io/OutputStream;

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method
