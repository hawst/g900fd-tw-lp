.class public final Ljng;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile euM:[Ljng;


# instance fields
.field private aez:I

.field private euN:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 47
    iput v0, p0, Ljng;->aez:I

    iput v0, p0, Ljng;->euN:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljng;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljng;->eCz:I

    .line 48
    return-void
.end method

.method public static bqB()[Ljng;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljng;->euM:[Ljng;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljng;->euM:[Ljng;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljng;

    sput-object v0, Ljng;->euM:[Ljng;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljng;->euM:[Ljng;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljng;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljng;->euN:I

    iget v0, p0, Ljng;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljng;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Ljng;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    iget v1, p0, Ljng;->euN:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 64
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 65
    return-void
.end method

.method public final bqC()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Ljng;->euN:I

    return v0
.end method

.method public final bqD()Z
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Ljng;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 69
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 70
    iget v1, p0, Ljng;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 71
    const/4 v1, 0x1

    iget v2, p0, Ljng;->euN:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_0
    return v0
.end method

.method public final qQ(I)Ljng;
    .locals 1

    .prologue
    .line 33
    iput p1, p0, Ljng;->euN:I

    .line 34
    iget v0, p0, Ljng;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljng;->aez:I

    .line 35
    return-object p0
.end method
