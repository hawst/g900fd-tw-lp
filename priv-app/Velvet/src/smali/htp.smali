.class final Lhtp;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lhto;


# instance fields
.field private final dqN:Landroid/widget/TextView;

.field private final dqO:Landroid/widget/TextView;

.field private final dqP:Lhuc;

.field dqQ:Lhtl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-virtual {p0}, Lhtp;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 34
    const v1, 0x7f040097

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 36
    const v0, 0x7f1101f5

    invoke-virtual {p0, v0}, Lhtp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lhtq;

    invoke-direct {v1, p0}, Lhtq;-><init>(Lhtp;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    const v0, 0x7f110140

    invoke-virtual {p0, v0}, Lhtp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lhtr;

    invoke-direct {v1, p0}, Lhtr;-><init>(Lhtp;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v0, 0x7f110120

    invoke-virtual {p0, v0}, Lhtp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhtp;->dqO:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f110122

    invoke-virtual {p0, v0}, Lhtp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhtp;->dqN:Landroid/widget/TextView;

    .line 55
    new-instance v0, Lhuc;

    invoke-direct {v0, p0}, Lhuc;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lhtp;->dqP:Lhuc;

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Lhtl;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lhtp;->dqQ:Lhtl;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 71
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtl;

    iput-object v0, p0, Lhtp;->dqQ:Lhtl;

    .line 72
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aQo()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lhtp;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQo()V

    .line 77
    return-void
.end method

.method public final aQp()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lhtp;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQp()V

    .line 82
    return-void
.end method

.method public final l(Lcom/google/android/search/shared/contact/Contact;)V
    .locals 2

    .prologue
    .line 60
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-static {}, Lenu;->auR()V

    .line 63
    iget-object v0, p0, Lhtp;->dqO:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lhtp;->dqN:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-void
.end method

.method public final oa(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lhtp;->dqP:Lhuc;

    invoke-virtual {v0, p1}, Lhuc;->oa(Ljava/lang/String;)V

    .line 87
    return-void
.end method
