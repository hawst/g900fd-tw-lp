.class final Lbkg;
.super Ljava/lang/Object;

# interfaces
.implements Lbkj;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1

    const-string v0, "Package manager must not be null."

    invoke-static {p1, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Package name must not be empty."

    invoke-static {p2, v0}, Lbjr;->b(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p1, p2}, Lbgt;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
