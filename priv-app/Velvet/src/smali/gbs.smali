.class public final Lgbs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cFk:Lizj;

.field private cFl:Landroid/view/View;

.field private cFm:Landroid/view/View;

.field mCardContainer:Lfmt;

.field private final mClock:Lemp;

.field final mEntry:Lizj;

.field mNotificationContext:Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

.field private final mTravelReport:Lgca;


# direct methods
.method public constructor <init>(Lizj;Lgca;Lemp;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;-><init>()V

    iput-object v0, p0, Lgbs;->mNotificationContext:Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    .line 50
    iput-object p1, p0, Lgbs;->mEntry:Lizj;

    .line 51
    iget-object v0, p0, Lgbs;->mEntry:Lizj;

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lgbs;->cFk:Lizj;

    .line 52
    iput-object p2, p0, Lgbs;->mTravelReport:Lgca;

    .line 53
    iput-object p3, p0, Lgbs;->mClock:Lemp;

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/view/LayoutInflater;Lfmt;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 59
    const v0, 0x7f0401a5

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    .line 60
    iput-object p3, p0, Lgbs;->mCardContainer:Lfmt;

    .line 61
    iget-object v0, p0, Lgbs;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->n(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    iput-object v0, p0, Lgbs;->mNotificationContext:Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    .line 66
    :cond_0
    iget-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    const v1, 0x7f110470

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbs;->cFl:Landroid/view/View;

    .line 67
    invoke-virtual {p0, p1}, Lgbs;->bp(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final aEv()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Lgbs;->mCardContainer:Lfmt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbs;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v0

    .line 101
    :goto_0
    iget-object v2, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v2, v0}, Lgca;->p(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 120
    :goto_1
    return v0

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aED()Liyk;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_2

    iget-object v2, v0, Liyk;->dQv:Ljbp;

    if-eqz v2, :cond_2

    iget-object v2, v0, Liyk;->dQv:Ljbp;

    invoke-virtual {v2}, Ljbp;->oK()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Liyk;->dQw:[J

    array-length v2, v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 111
    goto :goto_1

    .line 114
    :cond_3
    iget-object v2, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v2, v0}, Lgca;->e(Liyk;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 115
    goto :goto_1

    .line 117
    :cond_4
    iget-object v2, p0, Lgbs;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 118
    invoke-static {v0}, Lgca;->a(Liyk;)J

    move-result-wide v4

    .line 120
    const-wide/16 v6, 0xb4

    add-long/2addr v2, v6

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final aEw()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 128
    invoke-virtual {p0}, Lgbs;->aEv()Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return v0

    .line 134
    :cond_1
    iget-object v1, p0, Lgbs;->mEntry:Lizj;

    const/16 v2, 0xb1

    new-array v3, v0, [I

    invoke-static {v1, v2, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v1}, Lgca;->aED()Liyk;

    move-result-object v1

    .line 142
    if-eqz v1, :cond_0

    iget-object v1, v1, Liyk;->dQx:Ljcg;

    if-eqz v1, :cond_0

    .line 146
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final aEx()Z
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lgbs;->cFk:Lizj;

    iget-object v1, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v1}, Lgca;->aED()Liyk;

    move-result-object v1

    iget-object v1, v1, Liyk;->dQx:Ljcg;

    iput-object v1, v0, Lizj;->dUr:Ljcg;

    .line 191
    iget-object v0, p0, Lgbs;->mNotificationContext:Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    iget-object v1, p0, Lgbs;->cFk:Lizj;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->y(Lizj;)Z

    move-result v0

    return v0
.end method

.method public final bn(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 174
    iget-object v0, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aED()Liyk;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_0

    iget-object v1, v0, Liyk;->dQx:Ljcg;

    if-eqz v1, :cond_0

    iget-object v1, v0, Liyk;->dQx:Ljcg;

    iget-object v1, v1, Ljcg;->dRY:Ljie;

    if-eqz v1, :cond_0

    iget-object v1, v0, Liyk;->dQx:Ljcg;

    iget-object v1, v1, Ljcg;->dRY:Ljie;

    invoke-virtual {v1}, Ljie;->bkv()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    iget-object v0, v0, Liyk;->dQx:Ljcg;

    iget-object v0, v0, Ljcg;->dRY:Ljie;

    invoke-virtual {v0}, Ljie;->bmV()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 181
    const v2, 0x7f0a01d4

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v4}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final bp(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgbs;->aEv()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x0

    .line 84
    :goto_0
    return-object v0

    .line 74
    :cond_1
    iget-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    const v1, 0x7f11046f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    invoke-virtual {p0, p1}, Lgbs;->bq(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    const v1, 0x7f110472

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 80
    invoke-virtual {p0}, Lgbs;->aEx()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f020088

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 81
    new-instance v1, Lgbt;

    invoke-direct {v1, p0, p1}, Lgbt;-><init>(Lgbs;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {p0}, Lgbs;->aEx()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, Lgbs;->bn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    invoke-virtual {p0}, Lgbs;->aEw()Z

    move-result v0

    invoke-virtual {p0, v0}, Lgbs;->fH(Z)V

    .line 84
    iget-object v0, p0, Lgbs;->cFm:Landroid/view/View;

    goto :goto_0

    .line 80
    :cond_2
    const v1, 0x7f020089

    goto :goto_1

    .line 82
    :cond_3
    const v1, 0x7f0a03f8

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public final bq(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 155
    iget-object v0, p0, Lgbs;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aED()Liyk;

    move-result-object v0

    .line 156
    if-nez v0, :cond_0

    .line 157
    const-string v0, ""

    .line 164
    :goto_0
    return-object v0

    .line 159
    :cond_0
    iget-object v1, v0, Liyk;->dQw:[J

    invoke-static {v1}, Liuu;->c([J)J

    move-result-wide v2

    .line 161
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {p1, v2, v3, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    .line 163
    iget-object v0, v0, Liyk;->dQv:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 164
    const v2, 0x7f0a01d3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final fH(Z)V
    .locals 2

    .prologue
    .line 89
    if-eqz p1, :cond_0

    .line 90
    iget-object v0, p0, Lgbs;->cFl:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lgbs;->cFl:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
