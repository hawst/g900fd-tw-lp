.class public final Leks;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leky;


# instance fields
.field private cdW:Lekl;

.field private synthetic cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/shared/ui/SuggestionGridLayout;)V
    .locals 0

    .prologue
    .line 1279
    iput-object p1, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final aR(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1322
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1330
    :cond_0
    :goto_0
    return v0

    .line 1326
    :cond_1
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v1

    .line 1327
    if-eqz v1, :cond_0

    .line 1328
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-static {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v0

    iget-boolean v0, v0, Lekm;->cdu:Z

    goto :goto_0
.end method

.method public final aS(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1336
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1338
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    iput-object v0, p0, Leks;->cdW:Lekl;

    .line 1342
    iget-object v0, p0, Leks;->cdW:Lekl;

    if-eqz v0, :cond_0

    .line 1343
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    iget-object v1, p0, Leks;->cdW:Lekl;

    iget-object v1, v1, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344
    iget-object v0, p0, Leks;->cdW:Lekl;

    invoke-virtual {v0, v3}, Lekl;->id(I)V

    .line 1348
    :goto_0
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iput-boolean v2, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->RJ:Z

    .line 1349
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->invalidate()V

    .line 1350
    return-void

    .line 1346
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v3, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final aT(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1354
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    .line 1356
    if-eqz v0, :cond_2

    .line 1357
    new-instance v1, Lekr;

    iget-object v2, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-direct {v1, v2, v0}, Lekr;-><init>(Lcom/google/android/shared/ui/SuggestionGridLayout;Lekl;)V

    .line 1358
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    iget-object v2, v1, Lekr;->cdV:Lekl;

    iget-object v2, v2, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1359
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v1, Lekr;->cdV:Lekl;

    invoke-static {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v0

    iget-boolean v0, v0, Lekm;->cdx:Z

    if-eqz v0, :cond_0

    .line 1360
    iget-object v0, v1, Lekr;->cdV:Lekl;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lekl;->fb(Z)V

    .line 1363
    :cond_0
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cda:Lekq;

    if-eqz v0, :cond_1

    .line 1364
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cda:Lekq;

    invoke-interface {v0, v1}, Lekq;->a(Lejw;)V

    .line 1367
    :cond_1
    iget-boolean v0, v1, Lejw;->ccq:Z

    if-nez v0, :cond_2

    .line 1368
    invoke-virtual {v1}, Lekr;->commit()V

    .line 1371
    :cond_2
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->RJ:Z

    .line 1372
    const/4 v0, 0x0

    iput-object v0, p0, Leks;->cdW:Lekl;

    .line 1374
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->invalidate()V

    .line 1375
    return-void
.end method

.method public final aU(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1383
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, v1, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    iget-object v2, v0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0, v3}, Lekl;->id(I)V

    :goto_0
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iput-boolean v3, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->RJ:Z

    iput-object v4, p0, Leks;->cdW:Lekl;

    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->invalidate()V

    .line 1384
    return-void

    .line 1383
    :cond_0
    invoke-virtual {p1, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final auh()V
    .locals 5

    .prologue
    .line 1404
    iget-object v0, p0, Leks;->cdW:Lekl;

    if-nez v0, :cond_1

    .line 1416
    :cond_0
    :goto_0
    return-void

    .line 1407
    :cond_1
    iget-object v0, p0, Leks;->cdW:Lekl;

    iget-object v0, v0, Lekl;->cdr:Landroid/view/View;

    .line 1408
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1409
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v1, v4

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    sub-float v0, v3, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1410
    iget-object v1, p0, Leks;->cdW:Lekl;

    iget-object v1, v1, Lekl;->bXk:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1411
    iget-object v1, p0, Leks;->cdW:Lekl;

    iget-object v1, v1, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1413
    :cond_2
    iget-object v1, p0, Leks;->cdW:Lekl;

    iget-object v1, v1, Lekl;->bXl:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1414
    iget-object v1, p0, Leks;->cdW:Lekl;

    iget-object v1, v1, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public final p(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f000000    # 0.5f

    .line 1288
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-boolean v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->RJ:Z

    if-eqz v0, :cond_1

    move-object v1, v2

    .line 1316
    :cond_0
    :goto_0
    return-object v1

    .line 1290
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v4, v0

    .line 1291
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v5, v0

    .line 1293
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 1294
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_4

    .line 1295
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1296
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 1297
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->iA:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1300
    iget-object v0, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->iA:Landroid/graphics/Rect;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1302
    instance-of v0, v1, Lekk;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 1303
    check-cast v0, Lekk;

    iget-object v6, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-interface {v0, v6, p1}, Lekk;->a(Lcom/google/android/shared/ui/SuggestionGridLayout;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1305
    :cond_2
    invoke-static {v1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    .line 1310
    if-eqz v0, :cond_3

    iget-object v6, v0, Lekl;->cdr:Landroid/view/View;

    if-ne v6, v1, :cond_3

    iget-object v6, p0, Leks;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-static {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v0

    iget-boolean v0, v0, Lekm;->cdv:Z

    if-nez v0, :cond_0

    .line 1294
    :cond_3
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move-object v1, v2

    .line 1316
    goto :goto_0
.end method
