.class final Laee;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lti;


# instance fields
.field final synthetic Un:Ladz;

.field private Us:Landroid/view/ViewPropertyAnimator;


# direct methods
.method constructor <init>(Ladz;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Laee;->Un:Ladz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dZ()V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Laee;->Un:Ladz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ladz;->b(Ladz;Z)Z

    .line 406
    return-void
.end method

.method public final fv()V
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 389
    :cond_0
    iget-object v0, p0, Laee;->Un:Ladz;

    invoke-static {v0}, Ladz;->c(Ladz;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 390
    iget-object v0, p0, Laee;->Un:Ladz;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ladz;->b(Ladz;Z)Z

    .line 392
    :cond_1
    iget-object v0, p0, Laee;->Un:Ladz;

    invoke-static {v0}, Ladz;->c(Ladz;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    .line 393
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Laef;

    invoke-direct {v1, p0}, Laef;-><init>(Laee;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 400
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v1, v2}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 401
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 402
    return-void
.end method

.method public final fw()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 409
    iget-object v0, p0, Laee;->Un:Ladz;

    invoke-static {v0}, Ladz;->d(Ladz;)Z

    move-result v0

    .line 410
    iget-object v1, p0, Laee;->Un:Ladz;

    invoke-static {v1, v2}, Ladz;->b(Ladz;Z)Z

    .line 411
    if-nez v0, :cond_1

    .line 412
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 415
    :cond_0
    iget-object v0, p0, Laee;->Un:Ladz;

    invoke-static {v0}, Ladz;->c(Ladz;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Laee;->Un:Ladz;

    invoke-static {v0}, Ladz;->c(Ladz;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    .line 417
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 420
    iget-object v0, p0, Laee;->Us:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 422
    :cond_1
    return-void
.end method
