.class public abstract Lepm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leri;


# instance fields
.field private final aWp:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final aWq:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final chg:I

.field private final chj:Ljava/lang/Class;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mName:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method varargs constructor <init>(Ljava/lang/Class;Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lepm;->aWq:Ljava/lang/String;

    .line 62
    iput-object p1, p0, Lepm;->chj:Ljava/lang/Class;

    .line 63
    iput-object p2, p0, Lepm;->aWp:Ljava/lang/String;

    .line 64
    invoke-static {p3}, Lesp;->q([I)I

    move-result v0

    iput v0, p0, Lepm;->chg:I

    .line 65
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lepm;->aWq:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lepm;->chj:Ljava/lang/Class;

    .line 53
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lepm;->aWp:Ljava/lang/String;

    .line 54
    invoke-static {p3}, Lesp;->q([I)I

    move-result v0

    iput v0, p0, Lepm;->chg:I

    .line 55
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;[I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lepm;-><init>(Ljava/lang/String;Ljava/lang/String;[I)V

    .line 43
    return-void
.end method


# virtual methods
.method public final getPermissions()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lepm;->chg:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lepm;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lepm;->chj:Ljava/lang/Class;

    iget-object v1, p0, Lepm;->aWq:Ljava/lang/String;

    iget-object v2, p0, Lepm;->aWp:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lesp;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepm;->mName:Ljava/lang/String;

    .line 84
    :cond_0
    iget-object v0, p0, Lepm;->mName:Ljava/lang/String;

    return-object v0
.end method
