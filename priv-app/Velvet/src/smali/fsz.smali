.class public abstract Lfsz;
.super Lfro;
.source "PG"


# instance fields
.field private aod:Z

.field private cBD:Landroid/graphics/Point;

.field private cBE:Lfra;

.field private cBF:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfsz;->aod:Z

    .line 64
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfsz;->cBD:Landroid/graphics/Point;

    .line 74
    return-void
.end method

.method static synthetic a(Lfsz;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lfsz;->cBD:Landroid/graphics/Point;

    return-object v0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;Z)V
    .locals 3

    .prologue
    const v1, 0x7f110271

    const v2, 0x7f11005e

    .line 249
    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 250
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 251
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 252
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 254
    if-eqz p2, :cond_0

    const/16 v2, 0xb4

    :goto_0
    int-to-float v2, v2

    .line 255
    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->rotationBy(F)Landroid/view/ViewPropertyAnimator;

    .line 256
    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->rotationBy(F)Landroid/view/ViewPropertyAnimator;

    .line 257
    return-void

    .line 254
    :cond_0
    const/16 v2, -0xb4

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 263
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lfsz;->mContext:Landroid/content/Context;

    const v2, 0x7f090178

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 265
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 266
    iget-object v0, p0, Lfsz;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lfsz;->jx(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 270
    const v0, 0x7f110271

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 272
    invoke-virtual {p0}, Lfsz;->aDo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 273
    const v1, 0x7f110272

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lfsz;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 275
    const v1, 0x7f11005e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lfsz;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 279
    iget-object v1, p0, Lfsz;->cBE:Lfra;

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lfsz;->cBE:Lfra;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    :cond_0
    iget-object v1, p0, Lfsz;->cBF:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_1

    .line 283
    iget-object v1, p0, Lfsz;->cBF:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 286
    :cond_1
    invoke-virtual {p0, v2, p2}, Lfsz;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 287
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-eqz v0, :cond_2

    .line 288
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    invoke-virtual {p0, v1, v0}, Lfsz;->a(Landroid/view/View;Lani;)V

    .line 290
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lfsz;->d(Landroid/view/View;Z)V

    .line 291
    invoke-virtual {p0, v2, p1}, Lfsz;->c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 292
    if-eqz v3, :cond_3

    .line 293
    invoke-virtual {p0, v3}, Lfsz;->bw(Landroid/view/View;)V

    .line 298
    :cond_3
    const v0, 0x7f1102eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 299
    if-eqz v0, :cond_4

    .line 300
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 301
    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 305
    :cond_4
    invoke-direct {p0, v1}, Lfsz;->bu(Landroid/view/View;)V

    .line 306
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 309
    const v0, 0x7f1101d1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 311
    invoke-virtual {p0, v2, v0}, Lfsz;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 312
    invoke-virtual {p0, v1}, Lfsz;->bv(Landroid/view/View;)V

    .line 313
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 315
    if-eqz v3, :cond_5

    .line 316
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 318
    :cond_5
    return-void
.end method

.method private bu(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lfsz;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0142

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 328
    invoke-static {v0, v2, v2, v1, v2}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 329
    return-void
.end method


# virtual methods
.method public final W(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 374
    const-string v0, "ExpandableModulePresenter.Expanded"

    iget-boolean v1, p0, Lfsz;->aod:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 375
    return-void
.end method

.method protected abstract a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected aCZ()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 333
    iget-object v3, p0, Lfro;->mView:Landroid/view/View;

    .line 336
    const v0, 0x7f1101ce

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 337
    invoke-virtual {p0, v0, v2}, Lfsz;->d(Landroid/view/View;Z)V

    .line 340
    const v0, 0x7f1101d0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 341
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-le v4, v1, :cond_0

    .line 342
    invoke-virtual {p0, v0, v1}, Lfsz;->d(Landroid/view/View;Z)V

    .line 344
    const v0, 0x7f1101d1

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 345
    invoke-virtual {p0, v0}, Lfsz;->bv(Landroid/view/View;)V

    .line 349
    :cond_0
    const v0, 0x7f1101cd

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 350
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-le v4, v1, :cond_1

    .line 351
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfsz;->bw(Landroid/view/View;)V

    .line 355
    :cond_1
    const v0, 0x7f1101cf

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 356
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_2

    .line 357
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfsz;->bw(Landroid/view/View;)V

    .line 362
    :cond_2
    iget-boolean v0, p0, Lfsz;->aod:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lfsz;->fA(Z)V

    .line 363
    return-void

    :cond_3
    move v0, v2

    .line 362
    goto :goto_0
.end method

.method protected final aDb()V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method protected final aDc()V
    .locals 8

    .prologue
    .line 391
    invoke-virtual {p0}, Lfsz;->aDd()V

    .line 392
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-nez v0, :cond_0

    .line 395
    iget-object v7, p0, Lfro;->mView:Landroid/view/View;

    new-instance v0, Lftd;

    iget-object v2, p0, Lfsz;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v3, v1, Lanh;->ahu:Lizj;

    const/16 v4, 0xf7

    const/4 v5, 0x0

    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lftd;-><init>(Lfsz;Lfmt;Lizj;ILixx;Ljava/lang/Integer;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    :cond_0
    return-void
.end method

.method protected final aDm()V
    .locals 15

    .prologue
    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    const/4 v10, 0x3

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 158
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 159
    const v1, 0x7f1101cf

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 161
    const v2, 0x7f1101d1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 162
    const v3, 0x7f1101d0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 163
    const v4, 0x7f1101ce

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 168
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v7

    .line 169
    if-eqz v7, :cond_0

    .line 170
    iget-object v8, p0, Lfsz;->mContext:Landroid/content/Context;

    const v9, 0x7f050003

    invoke-static {v8, v9}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 172
    invoke-virtual {v7, v10}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 173
    const-wide/16 v8, 0x0

    invoke-virtual {v7, v10, v8, v9}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 176
    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 177
    invoke-virtual {v7, v6}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 178
    invoke-virtual {v7, v5}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 181
    :cond_0
    iget-boolean v7, p0, Lfsz;->aod:Z

    if-eqz v7, :cond_1

    .line 182
    invoke-virtual {p0, v5}, Lfsz;->fA(Z)V

    .line 185
    invoke-virtual {v2}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    rsub-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 188
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 191
    invoke-static {v4, v3, v6}, Lfsz;->a(Landroid/view/View;Landroid/view/View;Z)V

    .line 241
    :goto_0
    iget-boolean v0, p0, Lfsz;->aod:Z

    if-nez v0, :cond_4

    move v0, v5

    :goto_1
    iput-boolean v0, p0, Lfsz;->aod:Z

    .line 242
    return-void

    .line 193
    :cond_1
    invoke-virtual {p0, v6}, Lfsz;->fA(Z)V

    .line 197
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    if-ne v7, v5, :cond_2

    .line 198
    invoke-direct {p0, v1, v3}, Lfsz;->a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 201
    :cond_2
    invoke-virtual {v1, v13}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 202
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 205
    sget v7, Lesp;->SDK_INT:I

    const/16 v8, 0x15

    if-lt v7, v8, :cond_3

    .line 208
    const v7, 0x7f110271

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 209
    new-instance v8, Landroid/graphics/Rect;

    iget-object v9, p0, Lfsz;->cBD:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->x:I

    iget-object v10, p0, Lfsz;->cBD:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lfsz;->cBD:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->x:I

    add-int/lit8 v11, v11, 0x1

    iget-object v12, p0, Lfsz;->cBD:Landroid/graphics/Point;

    iget v12, v12, Landroid/graphics/Point;->y:I

    add-int/lit8 v12, v12, 0x1

    invoke-direct {v8, v9, v10, v11, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 211
    invoke-virtual {v0, v7, v8}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 214
    new-instance v0, Lftc;

    invoke-direct {v0, p0, v1, v8}, Lftc;-><init>(Lfsz;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 232
    :goto_2
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 233
    invoke-virtual {v2, v14}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 234
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 235
    invoke-virtual {v2}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 238
    invoke-static {v4, v3, v5}, Lfsz;->a(Landroid/view/View;Landroid/view/View;Z)V

    goto :goto_0

    .line 228
    :cond_3
    invoke-virtual {v1}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v14}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    :cond_4
    move v0, v6

    .line 241
    goto :goto_1
.end method

.method protected abstract aDn()Ljava/lang/String;
.end method

.method protected abstract aDo()Ljava/lang/String;
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f110271

    const/4 v6, 0x0

    .line 78
    const v0, 0x7f040086

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    invoke-virtual {p1, v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 80
    const v0, 0x7f1101cd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 81
    const v1, 0x7f1101ce

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 83
    invoke-virtual {p0, p1, v1}, Lfsz;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 84
    iget-object v4, p0, Lfro;->cBc:Lanh;

    iget-object v4, v4, Lanh;->ahs:Lani;

    if-eqz v4, :cond_0

    .line 85
    iget-object v4, p0, Lfro;->cBc:Lanh;

    iget-object v4, v4, Lanh;->ahs:Lani;

    invoke-virtual {p0, v3, v4}, Lfsz;->a(Landroid/view/View;Lani;)V

    .line 87
    :cond_0
    invoke-direct {p0, v3}, Lfsz;->bu(Landroid/view/View;)V

    .line 88
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 89
    invoke-virtual {p0, p1, v0}, Lfsz;->c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 90
    if-eqz v3, :cond_1

    .line 91
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 96
    :cond_1
    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lfsz;->jx(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 98
    if-eqz v3, :cond_2

    .line 99
    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->getBackgroundColor()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 102
    :cond_2
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->aht:Lani;

    .line 103
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lani;->getType()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_4

    .line 105
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 106
    new-instance v3, Lfta;

    invoke-direct {v3, p0}, Lfta;-><init>(Lfsz;)V

    iput-object v3, p0, Lfsz;->cBF:Landroid/view/View$OnTouchListener;

    .line 114
    iget-object v3, p0, Lfsz;->cBF:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 116
    new-instance v3, Lftb;

    iget-object v4, p0, Lfsz;->mCardContainer:Lfmt;

    iget-object v5, p0, Lfro;->cBc:Lanh;

    iget-object v5, v5, Lanh;->ahu:Lizj;

    invoke-virtual {v0}, Lani;->oY()I

    move-result v0

    invoke-direct {v3, p0, v4, v5, v0}, Lftb;-><init>(Lfsz;Lfmt;Lizj;I)V

    iput-object v3, p0, Lfsz;->cBE:Lfra;

    .line 123
    iget-object v0, p0, Lfsz;->cBE:Lfra;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-virtual {p0}, Lfsz;->aDn()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    :goto_0
    iget-object v0, p0, Lfsz;->cBb:Lfuz;

    invoke-virtual {v0}, Lfuz;->aDB()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ExpandableModulePresenter.Expanded"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    const v0, 0x7f1101cf

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 136
    const v1, 0x7f1101d0

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 138
    invoke-direct {p0, v0, v1}, Lfsz;->a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 139
    const v3, 0x7f1101d1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 140
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 142
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 143
    invoke-virtual {p0}, Lfsz;->aDo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    const v1, 0x7f11005e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 145
    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfsz;->aod:Z

    .line 150
    :cond_3
    return-object v2

    .line 126
    :cond_4
    const-string v1, "ExpandableModulePresenter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected action type on expandable module: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lani;->oY()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected abstract bv(Landroid/view/View;)V
.end method

.method protected bw(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 444
    return-void
.end method

.method protected c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 436
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract d(Landroid/view/View;Z)V
.end method
