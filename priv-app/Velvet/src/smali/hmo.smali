.class public final Lhmo;
.super Lhlo;
.source "PG"


# instance fields
.field private djY:Z

.field private final dkX:Leol;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Leoj;Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lhlo;-><init>(Leoj;)V

    .line 49
    new-instance v0, Lhmp;

    invoke-direct {v0, p0}, Lhmp;-><init>(Lhmo;)V

    iput-object v0, p0, Lhmo;->dkX:Leol;

    .line 60
    iput-object p2, p0, Lhmo;->mContext:Landroid/content/Context;

    .line 61
    iput-boolean p3, p0, Lhmo;->djY:Z

    .line 62
    return-void
.end method

.method private b(Lcom/google/android/search/shared/actions/SmsAction;)[Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 144
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 145
    const-string v0, "com.google.android.apps.googlevoice.action.AUTO_SEND"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 149
    invoke-static {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->c(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 150
    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    move-object v0, v1

    .line 155
    :goto_0
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 156
    const-string v1, "smsto"

    const/4 v3, 0x0

    invoke-static {v1, v0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 158
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lhmo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->aiM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    const-string v0, "com.google.android.e100.FAILURE_TTS"

    iget-object v1, p0, Lhmo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a0855

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    :cond_1
    new-array v0, v5, [Landroid/content/Intent;

    aput-object v2, v0, v4

    return-object v0

    .line 152
    :cond_2
    new-array v0, v4, [Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 3

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/search/shared/actions/SmsAction;

    iget-boolean v0, p0, Lhmo;->djY:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhmo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lhmo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2, v0, v1}, Lcom/google/android/shared/util/App;->a(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;Landroid/content/pm/PackageManager;)Lcom/google/android/shared/util/App;

    move-result-object v1

    new-instance v0, Lcom/google/android/shared/util/MatchingAppInfo;

    invoke-static {v1}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/android/shared/util/MatchingAppInfo;-><init>(Ljava/util/List;Lcom/google/android/shared/util/App;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lhlo;->G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 6

    .prologue
    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SENDTO"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "smsto"

    const-string v4, "0123456789"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    check-cast p1, Lcom/google/android/search/shared/actions/SmsAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    iget-object v2, p0, Lhmo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v3, [Landroid/content/Intent;

    goto :goto_0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    check-cast p1, Lcom/google/android/search/shared/actions/SmsAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->c(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lico;->be(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    new-array v0, v1, [Landroid/content/Intent;

    aput-object v3, v0, v2

    :goto_1
    return-object v0

    :cond_0
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, Lifv;->gY(Z)V

    new-array v1, v1, [Landroid/content/Intent;

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-static {v0}, Lico;->l(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v1, v2

    move-object v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    invoke-static {v3}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->c(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/search/shared/actions/SmsAction;

    invoke-direct {p0, p1}, Lhmo;->b(Lcom/google/android/search/shared/actions/SmsAction;)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/SmsAction;I)Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 120
    if-ne p2, v7, :cond_4

    .line 121
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->aiM()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->c(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-array v2, v7, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    move-object v6, v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->getBody()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    invoke-virtual {v0, v1}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    array-length v9, v6

    move v8, v4

    :goto_1
    if-ge v8, v9, :cond_2

    aget-object v1, v6, v8

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    :cond_1
    new-array v1, v4, [Ljava/lang/String;

    move-object v6, v1

    goto :goto_0

    :cond_2
    move v0, v7

    .line 139
    :goto_3
    return v0

    .line 132
    :cond_3
    iget-object v0, p0, Lhmo;->mIntentStarter:Leoj;

    invoke-interface {v0}, Leoj;->CR()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhmo;->mIntentStarter:Leoj;

    invoke-direct {p0, p1}, Lhmo;->b(Lcom/google/android/search/shared/actions/SmsAction;)[Landroid/content/Intent;

    move-result-object v1

    aget-object v1, v1, v4

    iget-object v2, p0, Lhmo;->dkX:Leol;

    invoke-interface {v0, v1, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v7

    .line 135
    goto :goto_3

    .line 139
    :cond_4
    invoke-super {p0, p1, p2}, Lhlo;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    goto :goto_3

    :catch_0
    move-exception v1

    goto :goto_2
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 1

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/search/shared/actions/SmsAction;

    invoke-virtual {p0, p1, p2}, Lhmo;->a(Lcom/google/android/search/shared/actions/SmsAction;I)Z

    move-result v0

    return v0
.end method
