.class public final Ldog;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private cq:Z

.field private final mAudioState:Ldbh;

.field private final mContext:Landroid/content/Context;

.field private final mGsaConfigFlags:Lchk;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mServiceState:Ldcu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Ldcu;Lcom/google/android/search/core/state/QueryState;Ldbh;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldog;->cq:Z

    .line 49
    iput-object p1, p0, Ldog;->mContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Ldog;->mGsaConfigFlags:Lchk;

    .line 51
    iput-object p3, p0, Ldog;->mServiceState:Ldcu;

    .line 52
    iput-object p4, p0, Ldog;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 53
    iput-object p5, p0, Ldog;->mAudioState:Ldbh;

    .line 54
    return-void
.end method

.method private static v(Landroid/content/Intent;)I
    .locals 1

    .prologue
    .line 100
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v0, "networkInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 106
    if-eqz v0, :cond_0

    invoke-static {v0}, Lgno;->a(Landroid/net/NetworkInfo;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final Rm()V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldog;->cq:Z

    .line 91
    iget-object v0, p0, Ldog;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_0

    .line 93
    invoke-static {v0}, Ldog;->v(Landroid/content/Intent;)I

    move-result v0

    .line 95
    iget-object v1, p0, Ldog;->mServiceState:Ldcu;

    invoke-virtual {v1, v0}, Ldcu;->fF(I)V

    .line 97
    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 64
    iget-boolean v0, p0, Ldog;->cq:Z

    if-eqz v0, :cond_0

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-static {p2}, Ldog;->v(Landroid/content/Intent;)I

    move-result v0

    .line 70
    iget-object v1, p0, Ldog;->mServiceState:Ldcu;

    invoke-virtual {v1, v0}, Ldcu;->fF(I)V

    .line 77
    iget-object v1, p0, Ldog;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->JQ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Ldog;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->XA()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ldog;->mAudioState:Ldbh;

    invoke-virtual {v1}, Ldbh;->Wg()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    :cond_2
    const/16 v1, 0x136

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v2, p0, Ldog;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v1, v0}, Litu;->mG(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    goto :goto_0
.end method

.method public final onStopped()V
    .locals 2

    .prologue
    .line 113
    iget-boolean v0, p0, Ldog;->cq:Z

    if-nez v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Ldog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldog;->cq:Z

    .line 123
    iget-object v0, p0, Ldog;->mServiceState:Ldcu;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Ldcu;->fF(I)V

    goto :goto_0
.end method
