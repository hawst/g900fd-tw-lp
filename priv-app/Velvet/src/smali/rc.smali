.class public Lrc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lrb;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final wS:Lrh;

.field private static final ww:[F


# instance fields
.field private ny:I

.field private wA:I

.field private wB:I

.field private wC:[F

.field private wD:I

.field private wE:I

.field private wF:I

.field private wG:[Lre;

.field private wH:[Lre;

.field private wI:[Lre;

.field private wJ:[Lre;

.field private final wK:Lrm;

.field private wL:I

.field private wM:Ljava/util/ArrayList;

.field private final wN:[F

.field private final wO:Landroid/graphics/RectF;

.field private final wP:Landroid/graphics/RectF;

.field private final wQ:[F

.field private final wR:[I

.field private wo:I

.field private wx:[F

.field private wy:[F

.field private wz:Lrm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 37
    const-class v0, Lrc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lrc;->TAG:Ljava/lang/String;

    .line 51
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lrc;->ww:[F

    .line 64
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v3, v0, v1

    const/4 v1, 0x4

    aput v3, v0, v1

    const/4 v1, 0x5

    aput v3, v0, v1

    const/4 v1, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v3, v0, v1

    .line 267
    new-instance v0, Lrg;

    invoke-direct {v0}, Lrg;-><init>()V

    sput-object v0, Lrc;->wS:Lrh;

    return-void

    .line 51
    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const/16 v0, 0x80

    new-array v0, v0, [F

    iput-object v0, p0, Lrc;->wx:[F

    .line 140
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lrc;->wy:[F

    .line 141
    new-instance v0, Lrm;

    invoke-direct {v0}, Lrm;-><init>()V

    iput-object v0, p0, Lrc;->wz:Lrm;

    .line 143
    iput v3, p0, Lrc;->wA:I

    .line 144
    iput v3, p0, Lrc;->wB:I

    .line 151
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lrc;->wC:[F

    .line 216
    new-array v0, v5, [Lre;

    new-instance v1, Lrd;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lrd;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lrf;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lrf;

    const-string v2, "uColor"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    iput-object v0, p0, Lrc;->wG:[Lre;

    .line 221
    const/4 v0, 0x5

    new-array v0, v0, [Lre;

    new-instance v1, Lrd;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lrd;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lrf;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lrf;

    const-string v2, "uTextureMatrix"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lrf;

    const-string v2, "uTextureSampler"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lrf;

    const-string v2, "uAlpha"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v0, p0, Lrc;->wH:[Lre;

    .line 228
    const/4 v0, 0x5

    new-array v0, v0, [Lre;

    new-instance v1, Lrd;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lrd;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lrf;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lrf;

    const-string v2, "uTextureMatrix"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lrf;

    const-string v2, "uTextureSampler"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lrf;

    const-string v2, "uAlpha"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v0, p0, Lrc;->wI:[Lre;

    .line 235
    const/4 v0, 0x5

    new-array v0, v0, [Lre;

    new-instance v1, Lrd;

    const-string v2, "aPosition"

    invoke-direct {v1, v2}, Lrd;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v3

    new-instance v1, Lrf;

    const-string v2, "uMatrix"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lrd;

    const-string v2, "aTextureCoordinate"

    invoke-direct {v1, v2}, Lrd;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lrf;

    const-string v2, "uTextureSampler"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lrf;

    const-string v2, "uAlpha"

    invoke-direct {v1, v2}, Lrf;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v0, p0, Lrc;->wJ:[Lre;

    .line 243
    new-instance v0, Lrm;

    invoke-direct {v0}, Lrm;-><init>()V

    iput-object v0, p0, Lrc;->wK:Lrm;

    .line 244
    new-instance v0, Lrm;

    invoke-direct {v0}, Lrm;-><init>()V

    .line 247
    iput v3, p0, Lrc;->wL:I

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lrc;->wM:Ljava/util/ArrayList;

    .line 260
    const/16 v0, 0x20

    new-array v0, v0, [F

    iput-object v0, p0, Lrc;->wN:[F

    .line 261
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lrc;->wO:Landroid/graphics/RectF;

    .line 263
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lrc;->wP:Landroid/graphics/RectF;

    .line 264
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lrc;->wQ:[F

    .line 265
    new-array v0, v7, [I

    iput-object v0, p0, Lrc;->wR:[I

    .line 270
    iget-object v0, p0, Lrc;->wQ:[F

    invoke-static {v0, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 271
    iget-object v0, p0, Lrc;->wx:[F

    iget v1, p0, Lrc;->wB:I

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 272
    iget-object v0, p0, Lrc;->wy:[F

    iget v1, p0, Lrc;->wA:I

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 273
    iget-object v0, p0, Lrc;->wM:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    sget-object v0, Lrc;->ww:[F

    array-length v1, v0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    array-length v2, v0

    invoke-virtual {v1, v0, v3, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 276
    sget-object v0, Lrc;->wS:Lrh;

    iget-object v2, p0, Lrc;->wR:[I

    invoke-interface {v0, v7, v2, v3}, Lrh;->glGenBuffers(I[II)V

    invoke-static {}, Lrc;->checkError()V

    iget-object v0, p0, Lrc;->wR:[I

    aget v0, v0, v3

    const v2, 0x8892

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lrc;->checkError()V

    const v2, 0x8892

    invoke-virtual {v1}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    const v4, 0x88e4

    invoke-static {v2, v3, v1, v4}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    invoke-static {}, Lrc;->checkError()V

    iput v0, p0, Lrc;->wF:I

    .line 278
    const v0, 0x8b31

    const-string v1, "uniform mat4 uMatrix;\nattribute vec2 aPosition;\nvoid main() {\n  vec4 pos = vec4(aPosition, 0.0, 1.0);\n  gl_Position = uMatrix * pos;\n}\n"

    invoke-static {v0, v1}, Lrc;->b(ILjava/lang/String;)I

    move-result v0

    .line 279
    const v1, 0x8b31

    const-string v2, "uniform mat4 uMatrix;\nuniform mat4 uTextureMatrix;\nattribute vec2 aPosition;\nvarying vec2 vTextureCoord;\nvoid main() {\n  vec4 pos = vec4(aPosition, 0.0, 1.0);\n  gl_Position = uMatrix * pos;\n  vTextureCoord = (uTextureMatrix * pos).xy;\n}\n"

    invoke-static {v1, v2}, Lrc;->b(ILjava/lang/String;)I

    move-result v1

    .line 280
    const v2, 0x8b31

    const-string v3, "uniform mat4 uMatrix;\nattribute vec2 aPosition;\nattribute vec2 aTextureCoordinate;\nvarying vec2 vTextureCoord;\nvoid main() {\n  vec4 pos = vec4(aPosition, 0.0, 1.0);\n  gl_Position = uMatrix * pos;\n  vTextureCoord = aTextureCoordinate;\n}\n"

    invoke-static {v2, v3}, Lrc;->b(ILjava/lang/String;)I

    move-result v2

    .line 281
    const v3, 0x8b30

    const-string v4, "precision mediump float;\nuniform vec4 uColor;\nvoid main() {\n  gl_FragColor = uColor;\n}\n"

    invoke-static {v3, v4}, Lrc;->b(ILjava/lang/String;)I

    move-result v3

    .line 282
    const v4, 0x8b30

    const-string v5, "precision mediump float;\nvarying vec2 vTextureCoord;\nuniform float uAlpha;\nuniform sampler2D uTextureSampler;\nvoid main() {\n  gl_FragColor = texture2D(uTextureSampler, vTextureCoord);\n  gl_FragColor *= uAlpha;\n}\n"

    invoke-static {v4, v5}, Lrc;->b(ILjava/lang/String;)I

    move-result v4

    .line 283
    const v5, 0x8b30

    const-string v6, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform float uAlpha;\nuniform samplerExternalOES uTextureSampler;\nvoid main() {\n  gl_FragColor = texture2D(uTextureSampler, vTextureCoord);\n  gl_FragColor *= uAlpha;\n}\n"

    invoke-static {v5, v6}, Lrc;->b(ILjava/lang/String;)I

    move-result v5

    .line 286
    iget-object v6, p0, Lrc;->wG:[Lre;

    invoke-direct {p0, v0, v3, v6}, Lrc;->a(II[Lre;)I

    .line 287
    iget-object v0, p0, Lrc;->wH:[Lre;

    invoke-direct {p0, v1, v4, v0}, Lrc;->a(II[Lre;)I

    move-result v0

    iput v0, p0, Lrc;->wD:I

    .line 289
    iget-object v0, p0, Lrc;->wI:[Lre;

    invoke-direct {p0, v1, v5, v0}, Lrc;->a(II[Lre;)I

    move-result v0

    iput v0, p0, Lrc;->wE:I

    .line 291
    iget-object v0, p0, Lrc;->wJ:[Lre;

    invoke-direct {p0, v2, v4, v0}, Lrc;->a(II[Lre;)I

    .line 292
    const/16 v0, 0x303

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 293
    invoke-static {}, Lrc;->checkError()V

    .line 294
    return-void
.end method

.method private a(II[Lre;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 306
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    .line 307
    invoke-static {}, Lrc;->checkError()V

    .line 308
    if-nez v0, :cond_0

    .line 309
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create GL program: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_0
    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 312
    invoke-static {}, Lrc;->checkError()V

    .line 313
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 314
    invoke-static {}, Lrc;->checkError()V

    .line 315
    invoke-static {v0}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 316
    invoke-static {}, Lrc;->checkError()V

    .line 317
    iget-object v2, p0, Lrc;->wR:[I

    .line 318
    const v3, 0x8b82

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 319
    aget v2, v2, v1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 320
    sget-object v2, Lrc;->TAG:Ljava/lang/String;

    const-string v3, "Could not link program: "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    sget-object v2, Lrc;->TAG:Ljava/lang/String;

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    move v0, v1

    .line 325
    :cond_1
    :goto_0
    array-length v2, p3

    if-ge v1, v2, :cond_2

    .line 326
    aget-object v2, p3, v1

    invoke-virtual {v2, v0}, Lre;->aL(I)V

    .line 325
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 328
    :cond_2
    return v0
.end method

.method private static a(Landroid/graphics/RectF;Landroid/graphics/RectF;Lqz;)V
    .locals 6

    .prologue
    .line 644
    invoke-virtual {p2}, Lqz;->getWidth()I

    move-result v0

    .line 645
    invoke-virtual {p2}, Lqz;->getHeight()I

    move-result v1

    .line 646
    invoke-virtual {p2}, Lqz;->dH()I

    move-result v2

    .line 647
    invoke-virtual {p2}, Lqz;->dI()I

    move-result v3

    .line 649
    iget v4, p0, Landroid/graphics/RectF;->left:F

    int-to-float v5, v2

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->left:F

    .line 650
    iget v4, p0, Landroid/graphics/RectF;->right:F

    int-to-float v5, v2

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->right:F

    .line 651
    iget v4, p0, Landroid/graphics/RectF;->top:F

    int-to-float v5, v3

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->top:F

    .line 652
    iget v4, p0, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v3

    div-float/2addr v4, v5

    iput v4, p0, Landroid/graphics/RectF;->bottom:F

    .line 655
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 656
    iget v2, p0, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v0

    if-lez v2, :cond_0

    .line 657
    iget v2, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget v5, p0, Landroid/graphics/RectF;->left:F

    sub-float v5, v0, v5

    mul-float/2addr v4, v5

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v4, v5

    add-float/2addr v2, v4

    iput v2, p1, Landroid/graphics/RectF;->right:F

    .line 658
    iput v0, p0, Landroid/graphics/RectF;->right:F

    .line 660
    :cond_0
    int-to-float v0, v1

    int-to-float v1, v3

    div-float/2addr v0, v1

    .line 661
    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_1

    .line 662
    iget v1, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget v3, p0, Landroid/graphics/RectF;->top:F

    sub-float v3, v0, v3

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p1, Landroid/graphics/RectF;->bottom:F

    .line 663
    iput v0, p0, Landroid/graphics/RectF;->bottom:F

    .line 665
    :cond_1
    return-void
.end method

.method private static b(ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 334
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    .line 337
    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 338
    invoke-static {}, Lrc;->checkError()V

    .line 339
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 340
    invoke-static {}, Lrc;->checkError()V

    .line 342
    return v0
.end method

.method private b(Lqz;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 12

    .prologue
    const/4 v1, 0x2

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 629
    iget-object v0, p0, Lrc;->wQ:[F

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    aput v2, v0, v3

    iget-object v0, p0, Lrc;->wQ:[F

    const/4 v2, 0x5

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v4

    aput v4, v0, v2

    iget-object v0, p0, Lrc;->wQ:[F

    const/16 v2, 0xc

    iget v4, p2, Landroid/graphics/RectF;->left:F

    aput v4, v0, v2

    iget-object v0, p0, Lrc;->wQ:[F

    const/16 v2, 0xd

    iget v4, p2, Landroid/graphics/RectF;->top:F

    aput v4, v0, v2

    .line 630
    iget-object v6, p0, Lrc;->wQ:[F

    invoke-virtual {p1}, Lqz;->dK()I

    iget-object v10, p0, Lrc;->wH:[Lre;

    iget v0, p0, Lrc;->wD:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    invoke-static {}, Lrc;->checkError()V

    invoke-virtual {p1}, Lqz;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lrc;->getAlpha()F

    move-result v0

    const v2, 0x3f733333    # 0.95f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    :cond_0
    move v0, v9

    :goto_0
    if-eqz v0, :cond_4

    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    invoke-static {}, Lrc;->checkError()V

    :goto_1
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    invoke-static {}, Lrc;->checkError()V

    invoke-virtual {p1, p0}, Lqz;->b(Lrb;)Z

    invoke-virtual {p1}, Lqz;->dK()I

    move-result v0

    invoke-virtual {p1}, Lqz;->getId()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    invoke-static {}, Lrc;->checkError()V

    const/4 v0, 0x3

    aget-object v0, v10, v0

    iget v0, v0, Lre;->handle:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {}, Lrc;->checkError()V

    const/4 v0, 0x4

    aget-object v0, v10, v0

    iget v0, v0, Lre;->handle:I

    invoke-direct {p0}, Lrc;->getAlpha()F

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    invoke-static {}, Lrc;->checkError()V

    const v0, 0x8892

    iget v2, p0, Lrc;->wF:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lrc;->checkError()V

    aget-object v0, v10, v3

    iget v0, v0, Lre;->handle:I

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    invoke-static {}, Lrc;->checkError()V

    const v0, 0x8892

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    invoke-static {}, Lrc;->checkError()V

    aget-object v0, v10, v1

    iget v0, v0, Lre;->handle:I

    invoke-static {v0, v9, v3, v6, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {}, Lrc;->checkError()V

    invoke-virtual {p1}, Lqz;->dG()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lrc;->aK(I)V

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-virtual {p0, v8, v0}, Lrc;->translate(FF)V

    iget-object v0, p0, Lrc;->wx:[F

    iget v1, p0, Lrc;->wB:I

    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v0, v1, v11, v2, v11}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p0, v8, v0}, Lrc;->translate(FF)V

    :cond_1
    iget v6, p3, Landroid/graphics/RectF;->left:F

    iget v7, p3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lrc;->wN:[F

    iget-object v4, p0, Lrc;->wx:[F

    iget v5, p0, Lrc;->wB:I

    invoke-static/range {v2 .. v8}, Landroid/opengl/Matrix;->translateM([FI[FIFFF)V

    iget-object v2, p0, Lrc;->wN:[F

    invoke-static {v2, v3, v0, v1, v11}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    iget-object v0, p0, Lrc;->wN:[F

    const/16 v1, 0x10

    iget-object v2, p0, Lrc;->wC:[F

    iget-object v4, p0, Lrc;->wN:[F

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    aget-object v0, v10, v9

    iget v0, v0, Lre;->handle:I

    iget-object v1, p0, Lrc;->wN:[F

    const/16 v2, 0x10

    invoke-static {v0, v9, v3, v1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {}, Lrc;->checkError()V

    aget-object v0, v10, v3

    iget v0, v0, Lre;->handle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    invoke-static {}, Lrc;->checkError()V

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-static {v1, v3, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    invoke-static {}, Lrc;->checkError()V

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    invoke-static {}, Lrc;->checkError()V

    invoke-virtual {p1}, Lqz;->dG()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lrc;->restore()V

    :cond_2
    iget v0, p0, Lrc;->wL:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lrc;->wL:I

    .line 631
    return-void

    :cond_3
    move v0, v3

    .line 630
    goto/16 :goto_0

    :cond_4
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    invoke-static {}, Lrc;->checkError()V

    goto/16 :goto_1
.end method

.method public static checkError()V
    .locals 5

    .prologue
    .line 964
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 965
    if-eqz v0, :cond_0

    .line 966
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    .line 967
    sget-object v2, Lrc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GL error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 969
    :cond_0
    return-void
.end method

.method public static dO()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v1, v1, v1, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 364
    invoke-static {}, Lrc;->checkError()V

    .line 365
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 366
    invoke-static {}, Lrc;->checkError()V

    .line 367
    return-void
.end method

.method private getAlpha()F
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lrc;->wy:[F

    iget v1, p0, Lrc;->wA:I

    aget v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a(Lqz;II)V
    .locals 9

    .prologue
    const/16 v0, 0xde1

    const/4 v1, 0x0

    .line 916
    invoke-virtual {p1}, Lqz;->dK()I

    .line 917
    invoke-virtual {p1}, Lqz;->getId()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 918
    invoke-static {}, Lrc;->checkError()V

    .line 919
    invoke-virtual {p1}, Lqz;->dH()I

    move-result v3

    .line 920
    invoke-virtual {p1}, Lqz;->dI()I

    move-result v4

    .line 921
    const/4 v8, 0x0

    move v2, p2

    move v5, v1

    move v6, p2

    move v7, p3

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 922
    return-void
.end method

.method public final a(Lqz;IIII)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 583
    if-lez p4, :cond_0

    if-gtz p5, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    iget-object v4, p0, Lrc;->wO:Landroid/graphics/RectF;

    invoke-virtual {p1}, Lqz;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Lqz;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Lqz;->dJ()Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    move v3, v2

    :goto_1
    int-to-float v3, v3

    int-to-float v2, v2

    int-to-float v1, v1

    int-to-float v0, v0

    invoke-virtual {v4, v3, v2, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 587
    iget-object v0, p0, Lrc;->wP:Landroid/graphics/RectF;

    int-to-float v1, p2

    int-to-float v2, p3

    add-int v3, p2, p4

    int-to-float v3, v3

    add-int v4, p3, p5

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 588
    iget-object v0, p0, Lrc;->wO:Landroid/graphics/RectF;

    iget-object v1, p0, Lrc;->wP:Landroid/graphics/RectF;

    invoke-static {v0, v1, p1}, Lrc;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Lqz;)V

    .line 589
    iget-object v0, p0, Lrc;->wO:Landroid/graphics/RectF;

    iget-object v1, p0, Lrc;->wP:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0, v1}, Lrc;->b(Lqz;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public final a(Lqz;IILandroid/graphics/Bitmap;II)V
    .locals 7

    .prologue
    const/16 v0, 0xde1

    .line 935
    invoke-virtual {p1}, Lqz;->dK()I

    .line 936
    invoke-virtual {p1}, Lqz;->getId()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 937
    invoke-static {}, Lrc;->checkError()V

    .line 938
    const/4 v1, 0x0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLUtils;->texSubImage2D(IIIILandroid/graphics/Bitmap;II)V

    .line 939
    return-void
.end method

.method public final a(Lqz;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/16 v2, 0xde1

    const/4 v1, 0x0

    .line 926
    invoke-virtual {p1}, Lqz;->dK()I

    .line 927
    invoke-virtual {p1}, Lqz;->getId()I

    move-result v0

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 928
    invoke-static {}, Lrc;->checkError()V

    .line 929
    invoke-static {v2, v1, p2, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 930
    return-void
.end method

.method public final a(Lqz;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 608
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    iget-object v0, p0, Lrc;->wO:Landroid/graphics/RectF;

    invoke-virtual {v0, p2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 612
    iget-object v0, p0, Lrc;->wP:Landroid/graphics/RectF;

    invoke-virtual {v0, p3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 614
    iget-object v0, p0, Lrc;->wO:Landroid/graphics/RectF;

    iget-object v1, p0, Lrc;->wP:Landroid/graphics/RectF;

    invoke-static {v0, v1, p1}, Lrc;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Lqz;)V

    .line 615
    iget-object v0, p0, Lrc;->wO:Landroid/graphics/RectF;

    iget-object v1, p0, Lrc;->wP:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0, v1}, Lrc;->b(Lqz;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public final a(Lqz;)Z
    .locals 4

    .prologue
    .line 787
    invoke-virtual {p1}, Lqz;->isLoaded()Z

    move-result v0

    .line 788
    if-eqz v0, :cond_0

    .line 789
    iget-object v1, p0, Lrc;->wK:Lrm;

    monitor-enter v1

    .line 790
    :try_start_0
    iget-object v2, p0, Lrc;->wK:Lrm;

    invoke-virtual {p1}, Lqz;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Lrm;->aM(I)V

    .line 791
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793
    :cond_0
    return v0

    .line 791
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aK(I)V
    .locals 5

    .prologue
    .line 444
    iget v0, p0, Lrc;->wB:I

    .line 456
    iget v1, p0, Lrc;->wB:I

    add-int/lit8 v1, v1, 0x10

    iput v1, p0, Lrc;->wB:I

    .line 457
    iget-object v1, p0, Lrc;->wx:[F

    array-length v1, v1

    iget v2, p0, Lrc;->wB:I

    if-gt v1, v2, :cond_0

    .line 458
    iget-object v1, p0, Lrc;->wx:[F

    iget-object v2, p0, Lrc;->wx:[F

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    iput-object v1, p0, Lrc;->wx:[F

    .line 460
    :cond_0
    iget-object v1, p0, Lrc;->wx:[F

    iget-object v2, p0, Lrc;->wx:[F

    iget v3, p0, Lrc;->wB:I

    const/16 v4, 0x10

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 462
    iget-object v0, p0, Lrc;->wz:Lrm;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lrm;->aM(I)V

    .line 463
    return-void
.end method

.method public final b(Lqz;)V
    .locals 4

    .prologue
    const v3, 0x812f

    const v2, 0x46180400    # 9729.0f

    const/16 v1, 0xde1

    .line 905
    invoke-virtual {p1}, Lqz;->dK()I

    .line 906
    invoke-virtual {p1}, Lqz;->getId()I

    move-result v0

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 907
    invoke-static {}, Lrc;->checkError()V

    .line 908
    const/16 v0, 0x2802

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 909
    const/16 v0, 0x2803

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 910
    const/16 v0, 0x2801

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 911
    const/16 v0, 0x2800

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 912
    return-void
.end method

.method public final dN()Lrh;
    .locals 1

    .prologue
    .line 1007
    sget-object v0, Lrc;->wS:Lrh;

    return-object v0
.end method

.method public final restore()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 467
    iget-object v2, p0, Lrc;->wz:Lrm;

    iget v3, v2, Lrm;->eX:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lrm;->eX:I

    iget-object v3, v2, Lrm;->mData:[I

    iget v2, v2, Lrm;->eX:I

    aget v3, v3, v2

    .line 468
    and-int/lit8 v2, v3, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    .line 469
    :goto_0
    if-eqz v2, :cond_0

    .line 470
    iget v2, p0, Lrc;->wA:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lrc;->wA:I

    .line 472
    :cond_0
    and-int/lit8 v2, v3, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 473
    :goto_1
    if-eqz v0, :cond_1

    .line 474
    iget v0, p0, Lrc;->wB:I

    add-int/lit8 v0, v0, -0x10

    iput v0, p0, Lrc;->wB:I

    .line 476
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 468
    goto :goto_0

    :cond_3
    move v0, v1

    .line 472
    goto :goto_1
.end method

.method public final rotate(FFFF)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x10

    const/4 v3, 0x0

    .line 417
    cmpl-float v0, p1, v3

    if-nez v0, :cond_0

    .line 426
    :goto_0
    return-void

    .line 420
    :cond_0
    iget-object v0, p0, Lrc;->wN:[F

    .line 421
    const/high16 v5, 0x3f800000    # 1.0f

    move v2, p1

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 422
    iget-object v4, p0, Lrc;->wx:[F

    .line 423
    iget v5, p0, Lrc;->wB:I

    move-object v2, v0

    move v3, v8

    move-object v6, v0

    move v7, v1

    .line 424
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 425
    invoke-static {v0, v8, v4, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final setSize(II)V
    .locals 8

    .prologue
    const/high16 v6, -0x40800000    # -1.0f

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 347
    iput p1, p0, Lrc;->ny:I

    .line 348
    iput p2, p0, Lrc;->wo:I

    .line 349
    iget v0, p0, Lrc;->ny:I

    iget v3, p0, Lrc;->wo:I

    invoke-static {v1, v1, v0, v3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 350
    invoke-static {}, Lrc;->checkError()V

    .line 351
    iget-object v0, p0, Lrc;->wx:[F

    iget v3, p0, Lrc;->wB:I

    invoke-static {v0, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 352
    iget-object v0, p0, Lrc;->wC:[F

    int-to-float v3, p1

    int-to-float v5, p2

    move v4, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 353
    iget-object v0, p0, Lrc;->wM:Ljava/util/ArrayList;

    iget-object v1, p0, Lrc;->wM:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lri;

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lrc;->wx:[F

    iget v1, p0, Lrc;->wB:I

    int-to-float v3, p2

    invoke-static {v0, v1, v2, v3, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 357
    iget-object v0, p0, Lrc;->wx:[F

    iget v1, p0, Lrc;->wB:I

    invoke-static {v0, v1, v7, v6, v7}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 359
    :cond_0
    return-void
.end method

.method public final translate(FF)V
    .locals 6

    .prologue
    .line 402
    iget v0, p0, Lrc;->wB:I

    .line 403
    iget-object v1, p0, Lrc;->wx:[F

    .line 404
    add-int/lit8 v2, v0, 0xc

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x0

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v5, v0, 0x4

    aget v5, v1, v5

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 405
    add-int/lit8 v2, v0, 0xd

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x1

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v5, v0, 0x5

    aget v5, v1, v5

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 406
    add-int/lit8 v2, v0, 0xe

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x2

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v5, v0, 0x6

    aget v5, v1, v5

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    aput v3, v1, v2

    .line 407
    add-int/lit8 v2, v0, 0xf

    aget v3, v1, v2

    add-int/lit8 v4, v0, 0x3

    aget v4, v1, v4

    mul-float/2addr v4, p1

    add-int/lit8 v0, v0, 0x7

    aget v0, v1, v0

    mul-float/2addr v0, p2

    add-float/2addr v0, v4

    add-float/2addr v0, v3

    aput v0, v1, v2

    .line 408
    return-void
.end method
