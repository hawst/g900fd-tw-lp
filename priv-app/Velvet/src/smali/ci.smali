.class final Lci;
.super Lcr;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field dv:Z

.field private dw:Ljava/util/concurrent/CountDownLatch;

.field private synthetic dx:Lch;

.field private result:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lch;)V
    .locals 2

    .prologue
    .line 40
    iput-object p1, p0, Lci;->dx:Lch;

    invoke-direct {p0}, Lcr;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lci;->dw:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lci;->dx:Lch;

    invoke-virtual {v0}, Lch;->loadInBackground()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lci;->result:Ljava/lang/Object;

    iget-object v0, p0, Lci;->result:Ljava/lang/Object;

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 2

    .prologue
    .line 71
    :try_start_0
    iget-object v0, p0, Lci;->dx:Lch;

    iget-object v1, p0, Lci;->result:Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Lch;->a(Lci;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    iget-object v0, p0, Lci;->dw:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lci;->dw:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 61
    :try_start_0
    iget-object v0, p0, Lci;->dx:Lch;

    iget-object v1, v0, Lch;->ds:Lci;

    if-eq v1, p0, :cond_0

    invoke-virtual {v0, p0, p1}, Lch;->a(Lci;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    :goto_0
    iget-object v0, p0, Lci;->dw:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 64
    return-void

    .line 61
    :cond_0
    :try_start_1
    iget-boolean v1, v0, Lck;->dC:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0, p1}, Lch;->onCanceled(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lci;->dw:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0

    .line 61
    :cond_1
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, v0, Lck;->dF:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lch;->du:J

    const/4 v1, 0x0

    iput-object v1, v0, Lch;->ds:Lci;

    invoke-virtual {v0, p1}, Lch;->deliverResult(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lci;->dv:Z

    .line 80
    iget-object v0, p0, Lci;->dx:Lch;

    invoke-virtual {v0}, Lch;->ai()V

    .line 81
    return-void
.end method
