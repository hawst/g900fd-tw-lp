.class public final Levn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdh;


# instance fields
.field private cjS:Levo;

.field private cjT:Ljava/security/SecureRandom;

.field private final dK:Ljava/lang/Object;

.field private final mPrefController:Lchr;

.field private ok:Z


# direct methods
.method public constructor <init>(Lchr;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Levn;->dK:Ljava/lang/Object;

    .line 65
    iput-object p1, p0, Levn;->mPrefController:Lchr;

    .line 66
    return-void
.end method

.method private static a([BIILjavax/crypto/spec/SecretKeySpec;)Ljavax/crypto/Mac;
    .locals 3

    .prologue
    .line 195
    const-string v0, "HmacSHA1"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 196
    invoke-virtual {v0, p3}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 197
    const/16 v1, 0x24

    const/4 v2, 0x3

    invoke-static {p0, v1, p2, v2}, Landroid/util/Base64;->encode([BIII)[B

    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update([B)V

    .line 199
    return-object v0
.end method

.method private awe()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 69
    iget-boolean v1, p0, Levn;->ok:Z

    if-eqz v1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v1, p0, Levn;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v2

    .line 78
    invoke-static {v2}, Levn;->c(Landroid/content/SharedPreferences;)Levo;

    move-result-object v1

    .line 79
    if-nez v1, :cond_1

    .line 80
    invoke-static {v2}, Levn;->d(Landroid/content/SharedPreferences;)Levo;

    move-result-object v1

    .line 84
    :cond_1
    :try_start_0
    const-string v2, "SHA1PRNG"

    invoke-static {v2}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 92
    :goto_1
    iput-object v1, p0, Levn;->cjS:Levo;

    .line 93
    iput-object v0, p0, Levn;->cjT:Ljava/security/SecureRandom;

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Levn;->ok:Z

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    const-string v1, "SignedCipherHelperImpl"

    const-string v2, "Cannot create SecureRandom for SHA1PRNG"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 89
    goto :goto_1
.end method

.method private static c(Landroid/content/SharedPreferences;)Levo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 206
    const-string v0, "winston"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    const-string v2, "wolf"

    invoke-interface {p0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 208
    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 220
    :goto_0
    return-object v0

    .line 211
    :cond_1
    const/4 v3, 0x3

    :try_start_0
    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 213
    const/4 v0, 0x3

    invoke-static {v2, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 215
    new-instance v0, Levo;

    invoke-direct {v0, v3, v2}, Levo;-><init>([B[B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "winston"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "wolf"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 219
    const-string v0, "SignedCipherHelperImpl"

    const-string v2, "Failed to read keys successfully; clearing old ones"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 220
    goto :goto_0
.end method

.method private static d(Landroid/content/SharedPreferences;)Levo;
    .locals 6

    .prologue
    const/4 v4, 0x3

    .line 231
    :try_start_0
    const-string v0, "AES"

    invoke-static {v0}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 237
    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Ljavax/crypto/KeyGenerator;->init(I)V

    .line 238
    invoke-virtual {v0}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 239
    invoke-virtual {v0}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 241
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    invoke-static {v0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 243
    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v3

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 246
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 247
    const-string v5, "winston"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 248
    const-string v0, "wolf"

    invoke-interface {v4, v0, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 249
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 252
    new-instance v0, Levo;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Levo;-><init>([B[B)V

    :goto_0
    return-object v0

    .line 233
    :catch_0
    move-exception v0

    const-string v0, "SignedCipherHelperImpl"

    const-string v1, "Cannot create KeyGenerator for AES"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final I([B)[B
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 99
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Levn;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 103
    :try_start_0
    invoke-direct {p0}, Levn;->awe()V

    .line 104
    iget-object v0, p0, Levn;->cjS:Levo;

    if-nez v0, :cond_0

    .line 105
    const-string v0, "SignedCipherHelperImpl"

    const-string v2, "No key pair"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    monitor-exit v1

    move-object v4, v6

    .line 144
    :goto_0
    return-object v4

    .line 108
    :cond_0
    iget-object v9, p0, Levn;->cjS:Levo;

    .line 109
    iget-object v0, p0, Levn;->cjT:Ljava/security/SecureRandom;

    .line 110
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :try_start_1
    array-length v1, p1

    div-int/lit8 v1, v1, 0x10

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v10, v1, 0x10

    .line 115
    add-int/lit8 v1, v10, 0x24

    new-array v4, v1, [B

    .line 118
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 119
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 120
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/16 v3, 0x10

    invoke-static {v1, v0, v4, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    const/4 v0, 0x0

    const/16 v2, 0x10

    invoke-direct {v1, v4, v0, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([BII)V

    .line 124
    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 125
    const/4 v2, 0x1

    iget-object v3, v9, Levo;->cjU:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0, v2, v3, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 128
    const/4 v2, 0x0

    array-length v3, p1

    const/16 v5, 0x24

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v0

    .line 130
    if-ne v10, v0, :cond_1

    move v0, v7

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 133
    const/16 v0, 0x24

    iget-object v1, v9, Levo;->cjV:Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {v4, v0, v10, v1}, Levn;->a([BIILjavax/crypto/spec/SecretKeySpec;)Ljavax/crypto/Mac;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Ljavax/crypto/Mac;->getMacLength()I

    move-result v1

    const/16 v2, 0x14

    if-eq v1, v2, :cond_2

    .line 135
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "hmac size unexpected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    const-string v1, "SignedCipherHelperImpl"

    const-string v2, "Failed to encrypt"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v6

    .line 141
    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    move v0, v8

    .line 130
    goto :goto_1

    .line 137
    :cond_2
    const/16 v1, 0x10

    :try_start_2
    invoke-virtual {v0, v4, v1}, Ljavax/crypto/Mac;->doFinal([BI)V
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 142
    :catch_1
    move-exception v0

    .line 143
    const-string v1, "SignedCipherHelperImpl"

    const-string v2, "Failed to encrypt"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v4, v6

    .line 144
    goto :goto_0
.end method

.method public final J([B)[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 150
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v1, p0, Levn;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    :try_start_0
    invoke-direct {p0}, Levn;->awe()V

    .line 154
    iget-object v2, p0, Levn;->cjS:Levo;

    if-nez v2, :cond_0

    .line 155
    const-string v2, "SignedCipherHelperImpl"

    const-string v3, "No key pair"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    monitor-exit v1

    .line 188
    :goto_0
    return-object v0

    .line 158
    :cond_0
    iget-object v2, p0, Levn;->cjS:Levo;

    .line 159
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :try_start_1
    array-length v1, p1

    add-int/lit8 v1, v1, -0x10

    add-int/lit8 v1, v1, -0x14

    .line 162
    if-gez v1, :cond_1

    .line 163
    const-string v1, "SignedCipherHelperImpl"

    const-string v2, "Failed to decrypt: bad data"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v1

    .line 187
    const-string v2, "SignedCipherHelperImpl"

    const-string v3, "Failed to decrypt"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 168
    :cond_1
    const/16 v3, 0x24

    :try_start_2
    iget-object v4, v2, Levo;->cjV:Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {p1, v3, v1, v4}, Levn;->a([BIILjavax/crypto/spec/SecretKeySpec;)Ljavax/crypto/Mac;

    move-result-object v3

    .line 172
    invoke-virtual {v3}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v3

    .line 173
    const/16 v4, 0x10

    const/16 v5, 0x24

    invoke-static {p1, v4, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    .line 174
    invoke-static {v4, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_2

    .line 175
    const-string v1, "SignedCipherHelperImpl"

    const-string v2, "Signature mismatch"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 180
    :cond_2
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-direct {v3, p1, v4, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([BII)V

    .line 181
    const-string v4, "AES/CBC/PKCS5Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    .line 182
    const/4 v5, 0x2

    iget-object v2, v2, Levo;->cjU:Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v4, v5, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 185
    const/16 v2, 0x24

    invoke-virtual {v4, p1, v2, v1}, Ljavax/crypto/Cipher;->doFinal([BII)[B
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0
.end method
