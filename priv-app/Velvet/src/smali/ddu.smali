.class public final Lddu;
.super Lddx;
.source "PG"


# instance fields
.field private final aUv:Lddv;


# direct methods
.method public constructor <init>(Ldef;Lddv;)V
    .locals 2

    .prologue
    .line 26
    invoke-interface {p1}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lddu;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Lddv;)V

    .line 27
    invoke-virtual {p0, p1}, Lddu;->c(Ljava/lang/Iterable;)I

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Lddv;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 21
    iput-object p3, p0, Lddu;->aUv:Lddv;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/shared/search/Suggestion;)V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Replace not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lddu;->aUv:Lddv;

    invoke-virtual {v0, p1}, Lddv;->f(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lddx;->d(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v0

    goto :goto_0
.end method
