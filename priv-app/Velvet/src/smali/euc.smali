.class public final Leuc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cjd:Ligi;

.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ligi;Lemp;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Leuc;->mAppContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Leuc;->cjd:Ligi;

    .line 64
    iput-object p3, p0, Leuc;->mClock:Lemp;

    .line 65
    return-void
.end method

.method private ly(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 100
    const-string v1, "com.google.android.apps.sidekick.DATA_BACKEND_VERSION_STORE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v1, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v1, "reminder_updated"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 103
    iget-object v1, p0, Leuc;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 104
    return-void
.end method


# virtual methods
.method public final a([Ljbt;)V
    .locals 8

    .prologue
    .line 85
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 86
    invoke-virtual {v0}, Ljbt;->bfv()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 87
    iget-object v3, p0, Leuc;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x493e0

    add-long/2addr v4, v6

    .line 88
    invoke-virtual {v0}, Ljbt;->getVersion()Ljava/lang/String;

    move-result-object v3

    .line 89
    iget-object v0, p0, Leuc;->cjd:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v6, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO"

    invoke-interface {v0, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v6, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO_EXPIRATION"

    invoke-interface {v0, v6, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 92
    invoke-direct {p0, v3}, Leuc;->ly(Ljava/lang/String;)V

    .line 85
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 96
    :cond_1
    return-void
.end method

.method public final avW()Ljava/util/Collection;
    .locals 6

    .prologue
    .line 110
    iget-object v0, p0, Leuc;->cjd:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 111
    const-string v1, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_1

    .line 113
    const-string v2, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO_EXPIRATION"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 115
    iget-object v4, p0, Leuc;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 116
    new-instance v0, Ljbt;

    invoke-direct {v0}, Ljbt;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljbt;->oz(I)Ljbt;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljbt;->sA(Ljava/lang/String;)Ljbt;

    move-result-object v0

    invoke-static {v0}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    .line 121
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO_EXPIRATION"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 125
    :cond_1
    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v0

    goto :goto_0
.end method

.method public final lx(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 74
    iget-object v0, p0, Leuc;->cjd:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.KANSAS_VERSION_INFO_EXPIRATION"

    iget-object v2, p0, Leuc;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 78
    invoke-direct {p0, p1}, Leuc;->ly(Ljava/lang/String;)V

    .line 79
    return-void
.end method
