.class public final Ldgz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bxE:Ldha;

.field private final bxF:Z


# direct methods
.method public constructor <init>(Ldha;Z)V
    .locals 0

    .prologue
    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    iput-object p1, p0, Ldgz;->bxE:Ldha;

    .line 525
    iput-boolean p2, p0, Ldgz;->bxF:Z

    .line 526
    return-void
.end method


# virtual methods
.method public final a([BZ)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 654
    iget-object v1, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v1}, Ldha;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 655
    const-string v1, "Search.IcingConnection"

    const-string v2, "setExperimentIds called when not connected."

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 658
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0, p1, p2}, Ldha;->a([BZ)Z

    move-result v0

    goto :goto_0
.end method

.method public final abX()V
    .locals 4

    .prologue
    .line 530
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 531
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    sget-wide v2, Ldgm;->bwY:J

    invoke-virtual {v0, v2, v3}, Ldha;->G(J)Lbgm;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbgm;->yk()Z

    move-result v1

    if-nez v1, :cond_1

    .line 533
    :cond_0
    const-string v1, "Search.IcingConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not connect to Icing. Error code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_2

    const-string v0, "Unknown."

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 537
    :cond_1
    return-void

    .line 533
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lbgm;->getErrorCode()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 560
    iget-object v1, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v1}, Ldha;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 561
    const-string v1, "Search.IcingConnection"

    const-string v2, "queryGlobalSearch when not connected."

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 586
    :goto_0
    return-object v0

    .line 565
    :cond_0
    iget-boolean v1, p0, Ldgz;->bxF:Z

    if-eqz v1, :cond_1

    .line 566
    const/16 v1, 0x14f

    invoke-static {v1}, Lege;->ht(I)V

    .line 570
    :cond_1
    iget-object v1, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v1, p1, v5, p2, p3}, Ldha;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v1

    .line 573
    iget-boolean v2, p0, Ldgz;->bxF:Z

    if-eqz v2, :cond_2

    .line 574
    const/16 v2, 0x150

    invoke-static {v2}, Lege;->ht(I)V

    .line 578
    :cond_2
    if-nez v1, :cond_3

    .line 579
    const-string v1, "Search.IcingConnection"

    const-string v2, "Got null results from queryGlobalSearch."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 582
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->hasError()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 583
    const-string v2, "Search.IcingConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got error for search: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->wW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 586
    goto :goto_0
.end method

.method public final wO()[I
    .locals 4

    .prologue
    .line 662
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 663
    const-string v0, "Search.IcingConnection"

    const-string v1, "getCurrentExperimentIds called query when not connected."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 664
    const/4 v0, 0x0

    .line 666
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->wO()[I

    move-result-object v0

    goto :goto_0
.end method

.method public final wP()[I
    .locals 4

    .prologue
    .line 670
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 671
    const-string v0, "Search.IcingConnection"

    const-string v1, "getPendingExperimentIds called when not connected."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 672
    const/4 v0, 0x0

    .line 674
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->wP()[I

    move-result-object v0

    goto :goto_0
.end method
