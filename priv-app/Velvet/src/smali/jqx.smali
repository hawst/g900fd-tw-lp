.class public final Ljqx;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezE:Ljsm;


# instance fields
.field private aez:I

.field public ezF:[Ljqy;

.field private ezG:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3383
    const/16 v0, 0xb

    const-class v1, Ljqx;

    const v2, 0x1f60adc2

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqx;->ezE:Ljsm;

    .line 3714
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3743
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3744
    iput v1, p0, Ljqx;->aez:I

    invoke-static {}, Ljqy;->bsL()[Ljqy;

    move-result-object v0

    iput-object v0, p0, Ljqx;->ezF:[Ljqy;

    iput v1, p0, Ljqx;->ezG:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqx;->eCz:I

    .line 3745
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqx;->ezF:[Ljqy;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljqy;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqx;->ezF:[Ljqy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljqy;

    invoke-direct {v3}, Ljqy;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqx;->ezF:[Ljqy;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljqy;

    invoke-direct {v3}, Ljqy;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqx;->ezF:[Ljqy;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqx;->ezG:I

    iget v0, p0, Ljqx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqx;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 3759
    iget-object v0, p0, Ljqx;->ezF:[Ljqy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljqx;->ezF:[Ljqy;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3760
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljqx;->ezF:[Ljqy;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3761
    iget-object v1, p0, Ljqx;->ezF:[Ljqy;

    aget-object v1, v1, v0

    .line 3762
    if-eqz v1, :cond_0

    .line 3763
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 3760
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3767
    :cond_1
    iget v0, p0, Ljqx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 3768
    const/4 v0, 0x2

    iget v1, p0, Ljqx;->ezG:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3770
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3771
    return-void
.end method

.method public final bsJ()I
    .locals 1

    .prologue
    .line 3727
    iget v0, p0, Ljqx;->ezG:I

    return v0
.end method

.method public final bsK()Z
    .locals 1

    .prologue
    .line 3735
    iget v0, p0, Ljqx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 3775
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 3776
    iget-object v0, p0, Ljqx;->ezF:[Ljqy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljqx;->ezF:[Ljqy;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3777
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljqx;->ezF:[Ljqy;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3778
    iget-object v2, p0, Ljqx;->ezF:[Ljqy;

    aget-object v2, v2, v0

    .line 3779
    if-eqz v2, :cond_0

    .line 3780
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3777
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3785
    :cond_1
    iget v0, p0, Ljqx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 3786
    const/4 v0, 0x2

    iget v2, p0, Ljqx;->ezG:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 3789
    :cond_2
    return v1
.end method

.method public final rE(I)Ljqx;
    .locals 1

    .prologue
    .line 3730
    iput p1, p0, Ljqx;->ezG:I

    .line 3731
    iget v0, p0, Ljqx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqx;->aez:I

    .line 3732
    return-object p0
.end method
