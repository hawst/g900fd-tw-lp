.class public final Lcjg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aYA:Lcjl;

.field final aYw:Ljava/util/Map;

.field final aYx:Ljava/util/Map;

.field final aYy:Ljava/util/Map;

.field final aYz:Ljava/util/Map;

.field final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mRelationshipNameLookup:Leai;

.field final mSearchSettings:Lcke;

.field final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcke;Leai;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcjl;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcjg;->aYw:Ljava/util/Map;

    .line 59
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcjg;->aYx:Ljava/util/Map;

    .line 60
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcjg;->aYy:Ljava/util/Map;

    .line 61
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcjg;->aYz:Ljava/util/Map;

    .line 62
    iput-object p2, p0, Lcjg;->mRelationshipNameLookup:Leai;

    .line 63
    iput-object p1, p0, Lcjg;->mSearchSettings:Lcke;

    .line 64
    iput-object p3, p0, Lcjg;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 65
    iput-object p4, p0, Lcjg;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 66
    invoke-direct {p0}, Lcjg;->KV()V

    .line 68
    iput-object p5, p0, Lcjg;->aYA:Lcjl;

    .line 69
    return-void
.end method

.method private F(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 202
    iget-object v0, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 211
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 206
    iget-object v3, p0, Lcjg;->mRelationshipNameLookup:Leai;

    invoke-interface {v3, v0}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 207
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 211
    goto :goto_0
.end method

.method private KV()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 73
    iget-object v1, p0, Lcjg;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->Om()Lcjd;

    move-result-object v2

    .line 74
    iget-object v3, v2, Lcjd;->aYh:[Lcje;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 75
    invoke-virtual {v5}, Lcje;->KU()Ljava/lang/String;

    move-result-object v6

    .line 76
    invoke-virtual {v5}, Lcje;->KT()Ljava/lang/String;

    move-result-object v5

    .line 81
    iget-object v7, p0, Lcjg;->mRelationshipNameLookup:Leai;

    invoke-interface {v7, v5}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v5

    .line 82
    if-eqz v5, :cond_0

    .line 83
    invoke-direct {p0, v5, v6}, Lcjg;->a(Lcom/google/android/search/shared/contact/Relationship;Ljava/lang/String;)V

    .line 74
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    :cond_1
    iget-object v1, v2, Lcjd;->aYi:[Lcje;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 87
    invoke-virtual {v3}, Lcje;->KU()Ljava/lang/String;

    move-result-object v4

    .line 88
    invoke-virtual {v3}, Lcje;->KT()Ljava/lang/String;

    move-result-object v3

    .line 94
    iget-object v5, p0, Lcjg;->mRelationshipNameLookup:Leai;

    invoke-interface {v5, v3}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v3

    .line 95
    if-eqz v3, :cond_2

    .line 96
    invoke-direct {p0, v3, v4}, Lcjg;->b(Lcom/google/android/search/shared/contact/Relationship;Ljava/lang/String;)V

    .line 86
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_3
    return-void
.end method

.method private a(Lcom/google/android/search/shared/contact/Relationship;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 160
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-direct {p0, v0, p2}, Lcjg;->F(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    iget-object v2, p0, Lcjg;->aYx:Ljava/util/Map;

    invoke-static {v1, p2, v2}, Lcjg;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 172
    iget-object v2, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-static {p2, v1, v2}, Lcjg;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 174
    :cond_0
    iget-object v1, p0, Lcjg;->aYw:Ljava/util/Map;

    invoke-static {v0, p2, v1}, Lcjg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 175
    iget-object v1, p0, Lcjg;->aYz:Ljava/util/Map;

    invoke-static {p2, v0, v1}, Lcjg;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 177
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Relationship;->amp()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcjg;->aYx:Ljava/util/Map;

    invoke-static {v0, p2, v1}, Lcjg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 178
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Relationship;->amp()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-static {p2, v0, v1}, Lcjg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 182
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 102
    invoke-interface {p2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 106
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-interface {p2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;Lcjj;)V
    .locals 5

    .prologue
    .line 116
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 117
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 118
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v4, v3}, Lcjj;->i(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_1
    return-void
.end method

.method private b(Lcom/google/android/search/shared/contact/Relationship;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 219
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-direct {p0, v0, p2}, Lcjg;->F(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 222
    if-eqz v1, :cond_0

    .line 223
    iget-object v2, p0, Lcjg;->aYx:Ljava/util/Map;

    invoke-static {v1, p2, v2}, Lcjg;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 224
    iget-object v2, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-static {p2, v1, v2}, Lcjg;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 225
    iget-object v1, p0, Lcjg;->aYw:Ljava/util/Map;

    invoke-static {v0, p2, v1}, Lcjg;->b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 227
    :cond_0
    iget-object v1, p0, Lcjg;->aYz:Ljava/util/Map;

    invoke-static {p2, v0, v1}, Lcjg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 234
    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 192
    invoke-interface {p2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 196
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 197
    invoke-interface {p2, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method KW()V
    .locals 5

    .prologue
    .line 125
    new-instance v1, Lcjd;

    invoke-direct {v1}, Lcjd;-><init>()V

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 127
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 128
    iget-object v0, p0, Lcjg;->aYx:Ljava/util/Map;

    new-instance v4, Lcjh;

    invoke-direct {v4, p0, v2}, Lcjh;-><init>(Lcjg;Ljava/util/List;)V

    invoke-static {v0, v4}, Lcjg;->a(Ljava/util/Map;Lcjj;)V

    .line 138
    iget-object v0, p0, Lcjg;->aYz:Ljava/util/Map;

    new-instance v4, Lcji;

    invoke-direct {v4, p0, v3}, Lcji;-><init>(Lcjg;Ljava/util/List;)V

    invoke-static {v0, v4}, Lcjg;->a(Ljava/util/Map;Lcjj;)V

    .line 150
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcje;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcje;

    iput-object v0, v1, Lcjd;->aYh:[Lcje;

    .line 152
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcje;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcje;

    iput-object v0, v1, Lcjd;->aYi:[Lcje;

    .line 155
    new-instance v0, Lcjk;

    invoke-direct {v0, p0}, Lcjk;-><init>(Lcjg;)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcjd;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcjk;->a([Ljava/lang/Object;)Lenp;

    .line 156
    return-void
.end method

.method public final KX()Ljava/util/Set;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/contact/Relationship;Lcom/google/android/search/shared/contact/Person;)V
    .locals 1

    .prologue
    .line 185
    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcjg;->a(Lcom/google/android/search/shared/contact/Relationship;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p2, p1}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Relationship;)V

    .line 187
    invoke-virtual {p0}, Lcjg;->KW()V

    .line 188
    iget-object v0, p0, Lcjg;->aYA:Lcjl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjg;->aYA:Lcjl;

    invoke-virtual {v0, p2, p1}, Lcjl;->c(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    .line 189
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/search/shared/contact/Relationship;Lcom/google/android/search/shared/contact/Person;)V
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcjg;->b(Lcom/google/android/search/shared/contact/Relationship;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2, p1}, Lcom/google/android/search/shared/contact/Person;->e(Lcom/google/android/search/shared/contact/Relationship;)V

    .line 239
    invoke-virtual {p0}, Lcjg;->KW()V

    .line 240
    iget-object v0, p0, Lcjg;->aYA:Lcjl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjg;->aYA:Lcjl;

    invoke-virtual {v0, p2, p1}, Lcjl;->d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    .line 241
    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/search/shared/contact/Person;)Ljava/util/Set;
    .locals 4
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 350
    iget-object v0, p0, Lcjg;->aYz:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 358
    :goto_0
    return-object v0

    .line 353
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 355
    iget-object v0, p0, Lcjg;->aYz:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 356
    new-instance v3, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v3, v0}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 358
    goto :goto_0
.end method

.method public final f(Ljava/util/Collection;)V
    .locals 4
    .param p1    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 335
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 336
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcjg;->gG(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 338
    invoke-virtual {p0, v0}, Lcjg;->d(Lcom/google/android/search/shared/contact/Person;)Ljava/util/Set;

    move-result-object v3

    .line 343
    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/contact/Person;->m(Ljava/util/Collection;)V

    .line 344
    invoke-virtual {v0, v3}, Lcom/google/android/search/shared/contact/Person;->n(Ljava/util/Collection;)V

    goto :goto_0

    .line 346
    :cond_0
    return-void
.end method

.method public final gF(Ljava/lang/String;)Ljava/util/Set;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 268
    iget-object v0, p0, Lcjg;->mRelationshipNameLookup:Leai;

    invoke-interface {v0, p1}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 271
    if-nez v1, :cond_0

    move-object v0, v2

    .line 300
    :goto_0
    return-object v0

    .line 276
    :cond_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 279
    iget-object v0, p0, Lcjg;->aYx:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 280
    if-eqz v0, :cond_1

    .line 281
    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 283
    :cond_1
    iget-object v4, p0, Lcjg;->aYw:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 285
    if-eqz v1, :cond_3

    .line 286
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 287
    if-eqz v0, :cond_2

    .line 288
    invoke-interface {v4, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 290
    :cond_2
    invoke-interface {v3, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 300
    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v2

    goto :goto_0

    :cond_4
    move-object v0, v3

    goto :goto_0
.end method

.method public final gG(Ljava/lang/String;)Ljava/util/Set;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 304
    iget-object v0, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 325
    :goto_0
    return-object v0

    .line 309
    :cond_0
    iget-object v0, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 317
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 318
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 319
    iget-object v4, p0, Lcjg;->mRelationshipNameLookup:Leai;

    invoke-interface {v4, v0}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 320
    if-eqz v4, :cond_1

    .line 321
    new-instance v5, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v5, v0, v4}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 325
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Relationship to Contact: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcjg;->aYx:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nCanonical to Contact: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcjg;->aYw:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nContact to relationship: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcjg;->aYy:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nContact to removed relationship: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcjg;->aYz:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
