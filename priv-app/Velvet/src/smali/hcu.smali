.class public Lhcu;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Lbxg;
.implements Lhgb;


# instance fields
.field private aNq:Lcse;

.field bsq:Ljava/lang/String;

.field dcT:Ljava/lang/String;

.field private dcU:Lbwu;

.field dcV:Lbxa;

.field dcW:Ljava/util/Stack;

.field dcX:Ljava/lang/String;

.field dcY:Z

.field private dcZ:Z

.field private final dda:Lesk;

.field private final ddb:Lesk;

.field private final ddc:Lesk;

.field final ddd:Lesk;

.field private dde:Lbwu;

.field private final ddf:Lesk;

.field private ddg:Lbwu;

.field private final ddh:Lbwu;

.field final ddi:Lesk;

.field final ddj:Lesk;

.field final ddk:Lbwu;

.field final ddl:Lbwu;

.field private final ddm:Lesk;

.field private final ddn:Lesk;

.field final ddo:Lbwu;

.field private final ddp:Lbwu;

.field final ddq:Lbwu;

.field mSettings:Lhym;

.field mSettingsUtil:Lbyl;


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 105
    new-instance v0, Lhcv;

    const-string v1, "Assistant opt in"

    invoke-direct {v0, p0, v1}, Lhcv;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->dda:Lesk;

    .line 124
    new-instance v0, Lhdd;

    const-string v1, "Start intent picker"

    invoke-direct {v0, p0, v1}, Lhdd;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddb:Lesk;

    .line 135
    new-instance v0, Lhde;

    const-string v1, "Opt out"

    invoke-direct {v0, p0, v1}, Lhde;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddc:Lesk;

    .line 147
    new-instance v0, Lhdf;

    const-string v1, "Kill activity"

    invoke-direct {v0, p0, v1}, Lhdf;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddd:Lesk;

    .line 158
    new-instance v0, Lhdg;

    const v3, 0x7f0b0094

    const v4, 0x7f0a06b6

    const v5, 0x7f02005f

    const v6, 0x7f0a06ba

    iget-object v7, p0, Lhcu;->dda:Lesk;

    const v8, 0x7f0a06bb

    iget-object v9, p0, Lhcu;->ddc:Lesk;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhdg;-><init>(Lhcu;Landroid/app/Activity;IIIILesk;ILesk;)V

    iput-object v0, p0, Lhcu;->dde:Lbwu;

    .line 192
    new-instance v0, Lhdh;

    const-string v1, "Setup car"

    invoke-direct {v0, p0, v1}, Lhdh;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddf:Lesk;

    .line 207
    new-instance v0, Lhdi;

    const v3, 0x7f0b0094

    const v4, 0x7f0a06b6

    const v5, 0x7f020060

    const v6, 0x7f0a06ba

    iget-object v7, p0, Lhcu;->ddf:Lesk;

    const v8, 0x7f0a06bb

    iget-object v9, p0, Lhcu;->ddc:Lesk;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhdi;-><init>(Lhcu;Landroid/app/Activity;IIIILesk;ILesk;)V

    iput-object v0, p0, Lhcu;->ddg:Lbwu;

    .line 231
    new-instance v0, Lhdj;

    const v3, 0x7f0b0095

    const v4, 0x7f0a06bc

    const v5, 0x7f02005f

    const v6, 0x7f0a06be

    iget-object v7, p0, Lhcu;->ddb:Lesk;

    const v8, 0x7f0a06bf

    iget-object v9, p0, Lhcu;->ddd:Lesk;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhdj;-><init>(Lhcu;Landroid/app/Activity;IIIILesk;ILesk;)V

    iput-object v0, p0, Lhcu;->ddh:Lbwu;

    .line 254
    new-instance v0, Lhdk;

    const-string v1, "Yes car"

    invoke-direct {v0, p0, v1}, Lhdk;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddi:Lesk;

    .line 286
    new-instance v0, Lhcw;

    const-string v1, "No car"

    invoke-direct {v0, p0, v1}, Lhcw;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddj:Lesk;

    .line 357
    new-instance v0, Lhdl;

    const v3, 0x7f0b0094

    const v4, 0x7f0a06c3

    const/4 v5, 0x0

    const v6, 0x7f020061

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lhdl;-><init>(Lhcu;Landroid/app/Activity;IIII)V

    iput-object v0, p0, Lhcu;->ddk:Lbwu;

    .line 368
    new-instance v0, Lhdl;

    const v3, 0x7f0b0096

    const v4, 0x7f0a06c2

    const v5, 0x7f0a06c3

    const v6, 0x7f020060

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lhdl;-><init>(Lhcu;Landroid/app/Activity;IIII)V

    iput-object v0, p0, Lhcu;->ddl:Lbwu;

    .line 379
    new-instance v0, Lhcx;

    const-string v1, "Start test"

    invoke-direct {v0, p0, v1}, Lhcx;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddm:Lesk;

    .line 394
    new-instance v0, Lhcy;

    const-string v1, "Decline test"

    invoke-direct {v0, p0, v1}, Lhcy;-><init>(Lhcu;Ljava/lang/String;)V

    iput-object v0, p0, Lhcu;->ddn:Lesk;

    .line 404
    new-instance v0, Lhcz;

    const v3, 0x7f0b0094

    const v4, 0x7f0a06c6

    const/4 v5, 0x0

    const v6, 0x7f0a06c8

    iget-object v7, p0, Lhcu;->ddm:Lesk;

    const v8, 0x7f0a06c9

    iget-object v9, p0, Lhcu;->ddn:Lesk;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhcz;-><init>(Lhcu;Landroid/app/Activity;IIIILesk;ILesk;)V

    .line 465
    new-instance v0, Lhda;

    invoke-direct {v0, p0, p0}, Lhda;-><init>(Lhcu;Landroid/app/Activity;)V

    iput-object v0, p0, Lhcu;->ddo:Lbwu;

    .line 492
    new-instance v0, Lhdb;

    invoke-direct {v0, p0, p0}, Lhdb;-><init>(Lhcu;Landroid/app/Activity;)V

    iput-object v0, p0, Lhcu;->ddp:Lbwu;

    .line 521
    new-instance v0, Lhdc;

    invoke-direct {v0, p0, p0}, Lhdc;-><init>(Lhcu;Landroid/app/Activity;)V

    iput-object v0, p0, Lhcu;->ddq:Lbwu;

    return-void
.end method

.method private S(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 608
    const-string v1, "bt-address"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhcu;->dcT:Ljava/lang/String;

    .line 609
    iget-object v1, p0, Lhcu;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v1

    .line 610
    iget-object v2, p0, Lhcu;->dcT:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 611
    const-string v2, "E100OnboardBluetoothSetup"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Address "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lhcu;->dcT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mismatch from settings: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ". Fixing."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iget-object v1, p0, Lhcu;->mSettings:Lhym;

    iget-object v2, p0, Lhcu;->dcT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lhym;->oH(Ljava/lang/String;)V

    .line 617
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    .line 619
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-le v2, v0, :cond_2

    :goto_0
    iput-boolean v0, p0, Lhcu;->dcY:Z

    .line 621
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 622
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhcu;->dcT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 625
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcu;->dcX:Ljava/lang/String;

    goto :goto_1

    .line 619
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 628
    :cond_3
    return-void
.end method


# virtual methods
.method public final Bk()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lhcu;->dcV:Lbxa;

    iget-object v0, v0, Lbxa;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUg()V

    .line 673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhcu;->dcZ:Z

    .line 674
    invoke-virtual {p0}, Lhcu;->finish()V

    .line 675
    return-void
.end method

.method public final Bl()V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lhcu;->dcV:Lbxa;

    invoke-virtual {v0}, Lbxa;->Bj()V

    .line 683
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhcu;->dcZ:Z

    .line 684
    iget-object v0, p0, Lhcu;->ddq:Lbwu;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhcu;->a(Lbwu;Z)V

    .line 685
    return-void
.end method

.method final a(Lbwu;Z)V
    .locals 2

    .prologue
    .line 642
    if-nez p2, :cond_0

    iget-object v0, p0, Lhcu;->dcU:Lbwu;

    iget-object v1, p0, Lhcu;->ddh:Lbwu;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lhcu;->dcZ:Z

    if-nez v0, :cond_0

    .line 643
    iget-object v0, p0, Lhcu;->dcW:Ljava/util/Stack;

    iget-object v1, p0, Lhcu;->dcU:Lbwu;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    :cond_0
    iput-object p1, p0, Lhcu;->dcU:Lbwu;

    .line 648
    iget-object v0, p0, Lhcu;->dcU:Lbwu;

    if-eqz v0, :cond_1

    .line 649
    iget-object v0, p0, Lhcu;->dcU:Lbwu;

    invoke-virtual {v0}, Lbwu;->getLayoutId()I

    move-result v0

    invoke-virtual {p0, v0}, Lhcu;->setContentView(I)V

    .line 650
    iget-object v0, p0, Lhcu;->dcU:Lbwu;

    invoke-virtual {v0}, Lbwu;->Be()V

    .line 652
    :cond_1
    return-void
.end method

.method public final aNJ()V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lhcu;->ddp:Lbwu;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhcu;->a(Lbwu;Z)V

    .line 305
    return-void
.end method

.method final aNK()Z
    .locals 1

    .prologue
    .line 631
    const-string v0, "android.intent.action.VOICE_COMMAND"

    invoke-static {p0, v0}, Lhsh;->w(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method final aNL()V
    .locals 3

    .prologue
    .line 692
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VOICE_COMMAND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 694
    const-string v1, "com.google.android.voicesearch.PICKER_START"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 695
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhcu;->startActivityForResult(Landroid/content/Intent;I)V

    .line 696
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 657
    if-nez p1, :cond_0

    .line 658
    const/16 v0, 0x2a

    if-ne p2, v0, :cond_0

    .line 659
    const-string v0, "android.intent.action.VOICE_COMMAND"

    invoke-static {p0, v0}, Lhsh;->w(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhcu;->ddl:Lbwu;

    .line 661
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhcu;->a(Lbwu;Z)V

    .line 664
    :cond_0
    return-void

    .line 659
    :cond_1
    iget-object v0, p0, Lhcu;->ddh:Lbwu;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 593
    iget-object v0, p0, Lhcu;->dcW:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 594
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 595
    invoke-virtual {p0}, Lhcu;->finish()V

    .line 604
    :goto_0
    return-void

    .line 596
    :cond_0
    iget-boolean v0, p0, Lhcu;->dcZ:Z

    if-eqz v0, :cond_1

    .line 597
    iget-object v0, p0, Lhcu;->dcV:Lbxa;

    invoke-virtual {v0}, Lbxa;->Bg()V

    .line 598
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhcu;->dcZ:Z

    goto :goto_0

    .line 601
    :cond_1
    iget-object v0, p0, Lhcu;->dcW:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwu;

    .line 602
    invoke-virtual {p0, v0, v1}, Lhcu;->a(Lbwu;Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 549
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 552
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    .line 553
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lhcu;->mSettings:Lhym;

    .line 554
    new-instance v0, Lbyl;

    invoke-direct {v0}, Lbyl;-><init>()V

    iput-object v0, p0, Lhcu;->mSettingsUtil:Lbyl;

    .line 555
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iput-object v0, p0, Lhcu;->aNq:Lcse;

    .line 556
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lhcu;->dcW:Ljava/util/Stack;

    .line 559
    iget-object v0, p0, Lhcu;->aNq:Lcse;

    iget-object v1, p0, Lhcu;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v0}, Lgcq;->getPrompt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcu;->bsq:Ljava/lang/String;

    .line 562
    new-instance v0, Lbxa;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p0}, Lbxa;-><init>(Landroid/app/Activity;ZLbxg;)V

    iput-object v0, p0, Lhcu;->dcV:Lbxa;

    .line 564
    invoke-virtual {p0}, Lhcu;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 565
    const-string v1, "entry-point"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 569
    invoke-direct {p0, v0}, Lhcu;->S(Landroid/content/Intent;)V

    .line 571
    const-string v0, "multi-assistant"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lhcu;->dde:Lbwu;

    .line 573
    const/16 v1, 0xe7

    invoke-static {v1}, Lege;->ht(I)V

    .line 588
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhcu;->a(Lbwu;Z)V

    .line 589
    :goto_1
    return-void

    .line 575
    :cond_0
    const-string v0, "car-bluetooth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 576
    iget-object v0, p0, Lhcu;->ddg:Lbwu;

    .line 577
    const/16 v1, 0x142

    invoke-static {v1}, Lege;->ht(I)V

    goto :goto_0

    .line 579
    :cond_1
    const-string v0, "settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 580
    iget-object v0, p0, Lhcu;->ddk:Lbwu;

    .line 581
    const/16 v1, 0x143

    invoke-static {v1}, Lege;->ht(I)V

    goto :goto_0

    .line 584
    :cond_2
    const-string v0, "E100OnboardBluetoothSetup"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Intent has extra or null entry point value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    invoke-virtual {p0}, Lhcu;->finish()V

    goto :goto_1
.end method
