.class public final Ldqk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field bFd:F

.field final bFp:Landroid/view/View;

.field private final bFq:Landroid/view/View;

.field final bFr:Landroid/animation/Animator$AnimatorListener;

.field private bFs:I

.field private bFt:I

.field bFu:Z

.field private bso:I

.field hN:Z

.field private yk:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ldql;

    invoke-direct {v0, p0}, Ldql;-><init>(Ldqk;)V

    iput-object v0, p0, Ldqk;->bFr:Landroid/animation/Animator$AnimatorListener;

    .line 47
    iput-object p1, p0, Ldqk;->bFp:Landroid/view/View;

    .line 48
    iput-object p2, p0, Ldqk;->bFq:Landroid/view/View;

    .line 49
    return-void
.end method

.method private aeK()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Ldqk;->bFq:Landroid/view/View;

    if-eqz v0, :cond_1

    iget v0, p0, Ldqk;->yk:I

    if-lez v0, :cond_1

    .line 124
    const/4 v0, 0x0

    .line 125
    iget v1, p0, Ldqk;->bso:I

    if-nez v1, :cond_0

    .line 126
    iget v0, p0, Ldqk;->bFd:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    iget v1, p0, Ldqk;->yk:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 128
    :cond_0
    iget-object v1, p0, Ldqk;->bFp:Landroid/view/View;

    invoke-static {v1}, Leot;->ay(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, -0x1

    .line 129
    :goto_0
    iget-object v2, p0, Ldqk;->bFp:Landroid/view/View;

    int-to-float v3, v1

    mul-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 131
    iget-object v0, p0, Ldqk;->bFq:Landroid/view/View;

    iget v2, p0, Ldqk;->bFd:F

    iget v3, p0, Ldqk;->yk:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 133
    :cond_1
    return-void

    .line 128
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final D(F)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Ldqk;->bFd:F

    .line 59
    invoke-direct {p0}, Ldqk;->aeK()V

    .line 60
    invoke-virtual {p0}, Ldqk;->aeJ()V

    .line 61
    return-void
.end method

.method public final aeI()Z
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Ldqk;->bFd:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aeJ()V
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Ldqk;->bFu:Z

    if-nez v0, :cond_0

    .line 96
    invoke-virtual {p0}, Ldqk;->getTranslationY()F

    move-result v0

    .line 97
    iget-boolean v1, p0, Ldqk;->hN:Z

    if-nez v1, :cond_1

    .line 99
    iget-object v1, p0, Ldqk;->bFp:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v1, p0, Ldqk;->bFp:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 104
    iget-object v0, p0, Ldqk;->bFp:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    goto :goto_0
.end method

.method getTranslationY()F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 110
    iget-boolean v1, p0, Ldqk;->bFu:Z

    if-eqz v1, :cond_1

    .line 111
    iget v0, p0, Ldqk;->bFt:I

    neg-int v0, v0

    int-to-float v0, v0

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    iget v1, p0, Ldqk;->bso:I

    if-nez v1, :cond_0

    .line 113
    iget v1, p0, Ldqk;->bFd:F

    .line 114
    iget-object v2, p0, Ldqk;->bFq:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 115
    iget v1, p0, Ldqk;->bFd:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_2

    .line 117
    :goto_1
    iget v1, p0, Ldqk;->bFt:I

    int-to-float v1, v1

    iget v2, p0, Ldqk;->bFs:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    neg-float v0, v0

    goto :goto_0

    .line 115
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final gf(I)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Ldqk;->bso:I

    .line 53
    invoke-direct {p0}, Ldqk;->aeK()V

    .line 54
    invoke-virtual {p0}, Ldqk;->aeJ()V

    .line 55
    return-void
.end method

.method public final gg(I)V
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Ldqk;->bFt:I

    .line 65
    invoke-virtual {p0}, Ldqk;->aeJ()V

    .line 66
    return-void
.end method

.method public final gh(I)V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Ldqk;->bFt:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ldqk;->bFs:I

    .line 70
    invoke-virtual {p0}, Ldqk;->aeJ()V

    .line 71
    return-void
.end method

.method public final setContentWidth(I)V
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Ldqk;->yk:I

    .line 75
    invoke-direct {p0}, Ldqk;->aeK()V

    .line 76
    return-void
.end method
