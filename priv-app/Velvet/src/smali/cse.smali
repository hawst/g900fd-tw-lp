.class public final Lcse;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcrz;


# instance fields
.field anD:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bhA:Landroid/content/res/AssetManager;

.field final bhB:Lgdm;

.field private final bhC:Ljava/lang/String;

.field private final bhD:Z

.field bhE:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bhl:Ljava/util/concurrent/Executor;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field final mContext:Landroid/content/Context;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgdm;Lgdo;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcse;->anD:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcse;->bhE:[B

    .line 60
    iput-object p1, p0, Lcse;->mContext:Landroid/content/Context;

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcse;->bhA:Landroid/content/res/AssetManager;

    .line 62
    iput-object p2, p0, Lcse;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 63
    iput-object p3, p0, Lcse;->bhl:Ljava/util/concurrent/Executor;

    .line 64
    iput-object p4, p0, Lcse;->bhB:Lgdm;

    .line 65
    iput-object p5, p0, Lcse;->mSpeechSettings:Lgdo;

    .line 66
    iput-object p6, p0, Lcse;->bhC:Ljava/lang/String;

    .line 67
    iput-boolean p7, p0, Lcse;->bhD:Z

    .line 68
    return-void
.end method

.method public static ab(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p1}, Lcse;->ac(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/hotword.data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static ac(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 7
    .param p2    # Ljava/lang/Runnable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcse;->bhE:[B

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcse;->anD:Ljava/lang/String;

    .line 80
    iget-object v6, p0, Lcse;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcsg;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HotwordDataManager - loadResources: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcsg;-><init>(Lcse;Ljava/lang/String;[ILjava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a([BLjava/lang/String;)V
    .locals 4
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcse;->bhD:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 137
    :goto_0
    monitor-exit p0

    return-void

    .line 131
    :cond_0
    if-eqz p2, :cond_1

    .line 133
    :try_start_1
    iget-object v0, p0, Lcse;->mSpeechSettings:Lgdo;

    invoke-interface {v0, p2, p1}, Lgdo;->i(Ljava/lang/String;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 135
    :cond_1
    :try_start_2
    const-string v0, "HotwordDataManager"

    const-string v1, "Shouldn\'t be setting speaker models without an account!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized iE(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcse;->anD:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcse;->anD:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized iF(Ljava/lang/String;)[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcse;->iE(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 114
    iget-object v0, p0, Lcse;->bhE:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized iG(Ljava/lang/String;)[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcse;->mSpeechSettings:Lgdo;

    invoke-interface {v0, p1}, Lgdo;->iG(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized iI(Ljava/lang/String;)Lgcq;
    .locals 4

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcse;->bhB:Lgdm;

    invoke-interface {v0}, Lgdm;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 87
    if-eqz v0, :cond_2

    .line 92
    iget-object v1, p0, Lcse;->anD:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    iget-object v1, p0, Lcse;->bhE:[B

    if-eqz v1, :cond_0

    .line 94
    new-instance v1, Lgcq;

    invoke-virtual {v0}, Lcsi;->getPrompt()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcsi;->SD()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lgcq;-><init>(Ljava/lang/String;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 105
    :goto_0
    monitor-exit p0

    return-object v0

    .line 97
    :cond_0
    :try_start_1
    new-instance v1, Lgcq;

    invoke-virtual {v0}, Lcsi;->getPrompt()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcsi;->SD()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lgcq;-><init>(Ljava/lang/String;IZ)V

    move-object v0, v1

    goto :goto_0

    .line 101
    :cond_1
    new-instance v1, Lgcq;

    invoke-virtual {v0}, Lcsi;->getPrompt()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcsi;->SD()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lgcq;-><init>(Ljava/lang/String;IZ)V

    move-object v0, v1

    goto :goto_0

    .line 105
    :cond_2
    sget-object v0, Lgcq;->cGc:Lgcq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final iJ(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    if-nez p1, :cond_0

    .line 142
    const-string v0, "HotwordDataManager"

    const-string v1, "Called deleteEnrollmentUtterances for null account"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 151
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcse;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcsf;

    const-string v2, "Delete Enrollment Utterances"

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3, p1}, Lcsf;-><init>(Lcse;Ljava/lang/String;[ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method iK(Ljava/lang/String;)[B
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 199
    :try_start_0
    iget-object v1, p0, Lcse;->bhA:Landroid/content/res/AssetManager;

    invoke-static {v1, p1}, Lesp;->a(Landroid/content/res/AssetManager;Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 203
    :goto_0
    return-object v0

    .line 200
    :catch_0
    move-exception v1

    .line 201
    const-string v2, "HotwordDataManager"

    const-string v3, "No model available."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method iL(Ljava/lang/String;)[B
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 208
    const/4 v0, 0x0

    .line 209
    iget-object v1, p0, Lcse;->bhC:Ljava/lang/String;

    invoke-static {v1, p1}, Lcse;->ab(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 211
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lisr;->e(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    :goto_0
    return-object v0

    .line 212
    :catch_0
    move-exception v1

    .line 213
    const-string v3, "HotwordDataManager"

    const-string v4, "No model available at %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v1, v4, v5}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
