.class public final Ljiz;
.super Ljsl;
.source "PG"


# instance fields
.field public eoe:[Ljja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 858
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 859
    invoke-static {}, Ljja;->bnA()[Ljja;

    move-result-object v0

    iput-object v0, p0, Ljiz;->eoe:[Ljja;

    const/4 v0, 0x0

    iput-object v0, p0, Ljiz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljiz;->eCz:I

    .line 860
    return-void
.end method

.method public static an([B)Ljiz;
    .locals 1

    .prologue
    .line 939
    new-instance v0, Ljiz;

    invoke-direct {v0}, Ljiz;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljiz;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljiz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljiz;->eoe:[Ljja;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljja;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljiz;->eoe:[Ljja;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljja;

    invoke-direct {v3}, Ljja;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljiz;->eoe:[Ljja;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljja;

    invoke-direct {v3}, Ljja;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljiz;->eoe:[Ljja;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 872
    iget-object v0, p0, Ljiz;->eoe:[Ljja;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljiz;->eoe:[Ljja;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 873
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljiz;->eoe:[Ljja;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 874
    iget-object v1, p0, Ljiz;->eoe:[Ljja;

    aget-object v1, v1, v0

    .line 875
    if-eqz v1, :cond_0

    .line 876
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 873
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 880
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 881
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 885
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 886
    iget-object v0, p0, Ljiz;->eoe:[Ljja;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljiz;->eoe:[Ljja;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 887
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljiz;->eoe:[Ljja;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 888
    iget-object v2, p0, Ljiz;->eoe:[Ljja;

    aget-object v2, v2, v0

    .line 889
    if-eqz v2, :cond_0

    .line 890
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 887
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 895
    :cond_1
    return v1
.end method
