.class final Ldkz;
.super Ldlg;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 630
    invoke-direct {p0}, Ldlg;-><init>()V

    return-void
.end method

.method private d(Lbzh;[BI)Ldjy;
    .locals 4

    .prologue
    .line 637
    :try_start_0
    invoke-static {p3}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 638
    invoke-static {p1, p2}, Ldkz;->a(Lbzh;[B)V

    .line 639
    invoke-interface {p1}, Lbzh;->getContentLength()I

    move-result v0

    .line 640
    invoke-static {p1}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;

    move-result-object v1

    .line 641
    invoke-static {v1, v0}, Ldkx;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 642
    new-instance v1, Ldjy;

    new-instance v2, Ldyj;

    invoke-interface {p1}, Lbzh;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v3}, Ldyj;-><init>(Ljava/util/Map;)V

    invoke-direct {v1, v0, v2}, Ldjy;-><init>([BLdyj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 645
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 646
    invoke-interface {p1}, Lbzh;->disconnect()V

    return-object v1

    .line 645
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 646
    invoke-interface {p1}, Lbzh;->disconnect()V

    throw v0
.end method


# virtual methods
.method public final synthetic b(Lbzh;[BI)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 629
    invoke-direct {p0, p1, p2, p3}, Ldkz;->d(Lbzh;[BI)Ldjy;

    move-result-object v0

    return-object v0
.end method
