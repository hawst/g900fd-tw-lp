.class public final Lfqe;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljdj;Ljava/util/Collection;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Ljdj;->ame:Ljde;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 50
    :goto_0
    return-object v0

    .line 44
    :cond_0
    new-instance v2, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v0, p0, Ljdj;->ame:Ljde;

    invoke-direct {v2, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 45
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 46
    new-instance v4, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {v2, v4}, Lcom/google/android/sidekick/shared/training/QuestionKey;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 50
    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILizj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 114
    new-array v0, v3, [I

    invoke-static {p2, p1, v0}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 115
    const/4 v0, 0x0

    .line 116
    const/4 v2, 0x5

    if-ne p1, v2, :cond_0

    .line 117
    const/16 v0, 0xe

    new-array v2, v3, [I

    invoke-static {p2, v0, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    .line 119
    :cond_0
    if-eqz v1, :cond_2

    .line 120
    invoke-static {}, Lfzv;->aDW()Landroid/content/Intent;

    move-result-object v2

    .line 121
    const-string v3, "action_type"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 122
    const-string v3, "entry"

    invoke-static {v2, v3, p2}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 123
    const-string v3, "action"

    invoke-static {v2, v3, v1}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 124
    if-eqz v0, :cond_1

    .line 125
    const-string v1, "delete_action"

    invoke-static {v2, v1, v0}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 128
    :cond_1
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_2
    const-string v0, "TrainingModeUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing action type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;ILandroid/content/Context;Lizj;)V
    .locals 3

    .prologue
    .line 79
    invoke-static {p1}, Lfqe;->jh(I)Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 81
    packed-switch p1, :pswitch_data_0

    .line 101
    :pswitch_0
    const-string v0, "TrainingModeUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized client action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 104
    :goto_0
    return-void

    .line 84
    :pswitch_1
    const-string v0, "com.google.android.googlequicksearchbox.TRAINING_CLOSET"

    invoke-static {v0}, Lgaw;->ml(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz p0, :cond_0

    const-string v1, "com.google.android.googlequicksearchbox.EXTRA_TRAINING_CLOSET_QUESTION"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 87
    :pswitch_2
    const-string v0, "com.google.android.googlequicksearchbox.MY_PLACES"

    invoke-static {p2, v0}, Lgaw;->m(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :pswitch_3
    const-string v0, "com.google.android.googlequicksearchbox.TRAFFIC_CARD_SETTINGS"

    invoke-static {p2, v0}, Lgaw;->m(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_4
    const/4 v0, 0x5

    invoke-static {p2, v0, p3}, Lfqe;->a(Landroid/content/Context;ILizj;)V

    goto :goto_0

    .line 98
    :pswitch_5
    const/16 v0, 0xe

    invoke-static {p2, v0, p3}, Lfqe;->a(Landroid/content/Context;ILizj;)V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static jh(I)Z
    .locals 1

    .prologue
    .line 57
    packed-switch p0, :pswitch_data_0

    .line 66
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 64
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
