.class public final Lhmj;
.super Lhlu;
.source "PG"


# instance fields
.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Leoj;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 23
    iput-object p2, p0, Lhmj;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 24
    return-void
.end method

.method private static a(Lcom/google/android/search/shared/actions/SetAlarmAction;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 36
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_ALARM"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetAlarmAction;->sZ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    const-string v1, "android.intent.extra.alarm.HOUR"

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetAlarmAction;->getHour()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 39
    const-string v1, "android.intent.extra.alarm.MINUTES"

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetAlarmAction;->getMinute()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetAlarmAction;->getLabel()Ljava/lang/String;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_1

    .line 43
    const-string v2, "android.intent.extra.alarm.MESSAGE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    :cond_1
    if-eqz p1, :cond_2

    .line 46
    const-string v1, "android.intent.extra.alarm.SKIP_UI"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    :cond_2
    return-object v0
.end method

.method private static aQJ()[Landroid/content/Intent;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SET_ALARM"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const-string v0, "com.google.android.deskclock"

    return-object v0
.end method

.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Lhmj;->aQJ()[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 16
    check-cast p1, Lcom/google/android/search/shared/actions/SetAlarmAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetAlarmAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhmj;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SHOW_ALARMS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    aput-object v2, v0, v4

    aput-object v1, v0, v5

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v5, [Landroid/content/Intent;

    aput-object v1, v0, v4

    goto :goto_0

    :cond_1
    new-array v0, v4, [Landroid/content/Intent;

    goto :goto_0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    check-cast p1, Lcom/google/android/search/shared/actions/SetAlarmAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    invoke-static {p1, v2}, Lhmj;->a(Lcom/google/android/search/shared/actions/SetAlarmAction;Z)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    return-object v0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 16
    check-cast p1, Lcom/google/android/search/shared/actions/SetAlarmAction;

    new-array v0, v2, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-static {p1, v2}, Lhmj;->a(Lcom/google/android/search/shared/actions/SetAlarmAction;Z)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 2

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/search/shared/actions/SetAlarmAction;

    const/16 v0, 0x65

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lhmj;->mIntentStarter:Leoj;

    invoke-static {}, Lhmj;->aQJ()[Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhlu;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    goto :goto_0
.end method
