.class public final Ljyl;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ail:I

.field private dHR:I

.field public eLN:Ljye;

.field private eLO:Ljyg;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 788
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 789
    iput v0, p0, Ljyl;->aez:I

    iput v0, p0, Ljyl;->ail:I

    iput-object v1, p0, Ljyl;->eLN:Ljye;

    iput-object v1, p0, Ljyl;->eLO:Ljyg;

    iput v0, p0, Ljyl;->dHR:I

    iput-object v1, p0, Ljyl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyl;->eCz:I

    .line 790
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 712
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljyl;->ail:I

    iget v0, p0, Ljyl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyl;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljyl;->eLN:Ljye;

    if-nez v0, :cond_1

    new-instance v0, Ljye;

    invoke-direct {v0}, Ljye;-><init>()V

    iput-object v0, p0, Ljyl;->eLN:Ljye;

    :cond_1
    iget-object v0, p0, Ljyl;->eLN:Ljye;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljyl;->eLO:Ljyg;

    if-nez v0, :cond_2

    new-instance v0, Ljyg;

    invoke-direct {v0}, Ljyg;-><init>()V

    iput-object v0, p0, Ljyl;->eLO:Ljyg;

    :cond_2
    iget-object v0, p0, Ljyl;->eLO:Ljyg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljyl;->dHR:I

    iget v0, p0, Ljyl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyl;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 806
    iget v0, p0, Ljyl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 807
    const/4 v0, 0x1

    iget v1, p0, Ljyl;->ail:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 809
    :cond_0
    iget-object v0, p0, Ljyl;->eLN:Ljye;

    if-eqz v0, :cond_1

    .line 810
    const/4 v0, 0x2

    iget-object v1, p0, Ljyl;->eLN:Ljye;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 812
    :cond_1
    iget-object v0, p0, Ljyl;->eLO:Ljyg;

    if-eqz v0, :cond_2

    .line 813
    const/4 v0, 0x3

    iget-object v1, p0, Ljyl;->eLO:Ljyg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 815
    :cond_2
    iget v0, p0, Ljyl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 816
    const/4 v0, 0x4

    iget v1, p0, Ljyl;->dHR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 818
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 819
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 823
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 824
    iget v1, p0, Ljyl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 825
    const/4 v1, 0x1

    iget v2, p0, Ljyl;->ail:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 828
    :cond_0
    iget-object v1, p0, Ljyl;->eLN:Ljye;

    if-eqz v1, :cond_1

    .line 829
    const/4 v1, 0x2

    iget-object v2, p0, Ljyl;->eLN:Ljye;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 832
    :cond_1
    iget-object v1, p0, Ljyl;->eLO:Ljyg;

    if-eqz v1, :cond_2

    .line 833
    const/4 v1, 0x3

    iget-object v2, p0, Ljyl;->eLO:Ljyg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 836
    :cond_2
    iget v1, p0, Ljyl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 837
    const/4 v1, 0x4

    iget v2, p0, Ljyl;->dHR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 840
    :cond_3
    return v0
.end method

.method public final th(I)Ljyl;
    .locals 1

    .prologue
    .line 750
    const/4 v0, 0x0

    iput v0, p0, Ljyl;->ail:I

    .line 751
    iget v0, p0, Ljyl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyl;->aez:I

    .line 752
    return-object p0
.end method

.method public final ti(I)Ljyl;
    .locals 1

    .prologue
    .line 775
    const/4 v0, 0x1

    iput v0, p0, Ljyl;->dHR:I

    .line 776
    iget v0, p0, Ljyl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyl;->aez:I

    .line 777
    return-object p0
.end method
