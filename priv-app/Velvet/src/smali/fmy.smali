.class final Lfmy;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic cwK:Lfmv;


# direct methods
.method constructor <init>(Lfmv;)V
    .locals 0

    .prologue
    .line 807
    iput-object p1, p0, Lfmy;->cwK:Lfmv;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private C(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 938
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 939
    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 940
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 941
    iget-object v1, p0, Lfmy;->cwK:Lfmv;

    iget-object v1, v1, Lfmv;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 942
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/high16 v7, 0x10000000

    const/4 v6, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 810
    if-nez p2, :cond_1

    .line 932
    :cond_0
    :goto_0
    return-void

    .line 816
    :cond_1
    iget-object v2, p0, Lfmy;->cwK:Lfmv;

    iget-object v2, v2, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v2}, Lfnc;->aAU()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 820
    const-string v2, "type"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 824
    const-string v3, "refresh_type"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 827
    if-eqz v2, :cond_2

    const/4 v4, 0x6

    if-ne v2, v4, :cond_3

    .line 829
    :cond_2
    if-ne v3, v0, :cond_3

    iget-object v4, p0, Lfmy;->cwK:Lfmv;

    iget-object v4, v4, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v4}, Lfnc;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lfmy;->cwK:Lfmv;

    iget-object v4, v4, Lfmv;->cwA:Lizo;

    if-nez v4, :cond_0

    .line 839
    :cond_3
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 845
    :pswitch_0
    const/4 v0, 0x5

    if-ne v3, v0, :cond_4

    .line 846
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v1, v0, Lfmv;->cwx:Ljava/lang/Object;

    monitor-enter v1

    .line 847
    :try_start_0
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    const/4 v2, 0x0

    iput-object v2, v0, Lfmv;->cwA:Lizo;

    .line 848
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 851
    :cond_4
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBj()V

    goto :goto_0

    .line 848
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 854
    :pswitch_1
    const-string v0, "entry"

    invoke-static {p2, v0}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    .line 856
    const-string v0, "updated_entry"

    invoke-static {p2, v0}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    move-result-object v0

    .line 858
    const-string v1, "entry_change"

    invoke-static {p2, v1}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    move-result-object v1

    .line 860
    iget-object v2, p0, Lfmy;->cwK:Lfmv;

    iget-object v2, v2, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v2, v0, v1}, Lfnc;->i(Lizj;Lizj;)V

    goto :goto_0

    .line 863
    :pswitch_2
    const-string v0, "entry"

    invoke-static {p2, v0}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    move-result-object v0

    .line 865
    const-string v1, "child_entries"

    invoke-static {p2, v1}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 867
    iget-object v2, p0, Lfmy;->cwK:Lfmv;

    iget-object v2, v2, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v2, v0, v1}, Lfnc;->c(Lizj;Ljava/util/Collection;)V

    goto :goto_0

    .line 870
    :pswitch_3
    const-string v2, "refresh_error_auth"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 872
    if-eqz v1, :cond_6

    .line 873
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    const v1, 0x7f0a0151

    invoke-interface {v0, v1}, Lfnc;->ex(I)V

    .line 890
    :goto_1
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->cwv:Lfne;

    if-eqz v0, :cond_5

    .line 891
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBk()V

    .line 894
    :cond_5
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAX()V

    goto/16 :goto_0

    .line 875
    :cond_6
    if-ne v3, v6, :cond_7

    .line 877
    iget-object v1, p0, Lfmy;->cwK:Lfmv;

    invoke-virtual {v1, v0}, Lfmv;->fs(Z)V

    goto :goto_1

    .line 880
    :cond_7
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v1, v0, Lfmv;->cwx:Ljava/lang/Object;

    monitor-enter v1

    .line 881
    :try_start_1
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-wide v2, v0, Lfmv;->cwy:J

    .line 882
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 884
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    iget-object v1, p0, Lfmy;->cwK:Lfmv;

    iget-object v1, v1, Lfmv;->mActivityHelper:Lfzw;

    iget-object v4, p0, Lfmy;->cwK:Lfmv;

    iget-object v4, v4, Lfmv;->mClock:Lemp;

    invoke-interface {v0, v2, v3, v1, v4}, Lfnc;->a(JLfzw;Lemp;)V

    goto :goto_1

    .line 882
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 897
    :pswitch_4
    const-string v2, "refresh_type"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 902
    if-eq v2, v6, :cond_0

    .line 904
    iget-object v3, p0, Lfmy;->cwK:Lfmv;

    iget-object v3, v3, Lfmv;->cwx:Ljava/lang/Object;

    monitor-enter v3

    .line 905
    :try_start_2
    iget-object v4, p0, Lfmy;->cwK:Lfmv;

    const/4 v5, 0x0

    iput-object v5, v4, Lfmv;->cwA:Lizo;

    .line 906
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 908
    invoke-static {v2}, Lflk;->iW(I)Z

    move-result v3

    if-nez v3, :cond_9

    if-eqz v2, :cond_8

    invoke-static {v2}, Lflk;->iS(I)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    :goto_2
    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAW()V

    goto/16 :goto_0

    .line 906
    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_9
    move v0, v1

    .line 908
    goto :goto_2

    .line 915
    :pswitch_5
    iget-object v0, p0, Lfmy;->cwK:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBk()V

    .line 916
    const-string v0, "disabled_reason"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 918
    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 922
    const-string v0, "com.google.android.velvet.ui.settings.SettingsActivity"

    invoke-direct {p0, v0, v7}, Lfmy;->C(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 927
    :cond_a
    const-string v0, "com.google.android.googlequicksearchbox.SearchActivity"

    invoke-direct {p0, v0, v7}, Lfmy;->C(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 839
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
