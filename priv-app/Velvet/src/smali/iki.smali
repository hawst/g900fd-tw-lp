.class public abstract Liki;
.super Likl;
.source "PG"

# interfaces
.implements Lird;
.implements Ljava/util/SortedSet;


# static fields
.field private static final dDj:Ljava/util/Comparator;

.field private static final dDt:Liki;


# instance fields
.field final transient dDm:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 92
    invoke-static {}, Liqa;->aYi()Liqa;

    move-result-object v0

    sput-object v0, Liki;->dDj:Ljava/util/Comparator;

    .line 95
    new-instance v0, Liip;

    sget-object v1, Liki;->dDj:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Liip;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Liki;->dDt:Liki;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0}, Likl;-><init>()V

    .line 567
    iput-object p1, p0, Liki;->dDm:Ljava/util/Comparator;

    .line 568
    return-void
.end method

.method static a(Ljava/util/Comparator;)Liki;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Liki;->dDj:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    sget-object v0, Liki;->dDt:Liki;

    .line 108
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Liip;

    invoke-direct {v0, p0}, Liip;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Collection;)Liki;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 360
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    invoke-static {p0, p1}, Lire;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Liki;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Liki;

    invoke-virtual {v0}, Liki;->aWH()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Lipn;

    if-eqz v0, :cond_9

    check-cast p1, Lipn;

    invoke-interface {p1}, Lipn;->aWA()Ljava/util/Set;

    move-result-object v0

    :goto_1
    instance-of v1, v0, Ljava/util/Set;

    if-eqz v1, :cond_2

    invoke-static {p0, v0}, Lire;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_1

    check-cast v0, Ljava/util/Set;

    :goto_2
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-virtual {v1}, Lijj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p0}, Liki;->a(Ljava/util/Comparator;)Liki;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_2

    :cond_2
    invoke-static {v0}, Likm;->u(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    invoke-static {p0, v0}, Lire;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    :cond_3
    array-length v0, v1

    if-nez v0, :cond_4

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v2

    :goto_3
    array-length v3, v1

    if-ge v0, v3, :cond_6

    aget-object v3, v1, v0

    add-int/lit8 v4, v2, -0x1

    aget-object v4, v1, v4

    invoke-interface {p0, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-eqz v3, :cond_5

    add-int/lit8 v3, v2, 0x1

    aget-object v4, v1, v0

    aput-object v4, v1, v2

    move v2, v3

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    array-length v0, v1

    if-ge v2, v0, :cond_7

    invoke-static {v1, v2}, Lipz;->c([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    array-length v3, v1

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v5, v0, v5, v2}, Liqb;->a([Ljava/lang/Object;I[Ljava/lang/Object;II)V

    move-object v1, v0

    :cond_7
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    :cond_8
    new-instance v0, Liqn;

    invoke-direct {v0, v1, p0}, Liqn;-><init>(Lijj;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_9
    move-object v0, p1

    goto :goto_1
.end method

.method static synthetic a(Ljava/util/Comparator;Ljava/util/Iterator;)Liki;
    .locals 2

    .prologue
    .line 89
    invoke-static {p0}, Liqs;->c(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-static {v0, p1}, Likr;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-virtual {v1}, Lijj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Liki;->a(Ljava/util/Comparator;)Liki;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Liqn;

    invoke-direct {v0, v1, p0}, Liqn;-><init>(Lijj;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 692
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Liki;
.end method

.method public abstract aWI()Lirv;
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Liki;->dDm:Ljava/util/Comparator;

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 2

    .prologue
    .line 87
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Liki;->l(Ljava/lang/Object;Z)Liki;

    move-result-object v0

    return-object v0
.end method

.method abstract indexOf(Ljava/lang/Object;)I
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Liki;->aWI()Lirv;

    move-result-object v0

    return-object v0
.end method

.method abstract l(Ljava/lang/Object;Z)Liki;
.end method

.method abstract m(Ljava/lang/Object;Z)Liki;
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Liki;->dDm:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    invoke-virtual {p0, p1, v1, p2, v2}, Liki;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Liki;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 2

    .prologue
    .line 87
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Liki;->m(Ljava/lang/Object;Z)Liki;

    move-result-object v0

    return-object v0
.end method

.method final w(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Liki;->dDm:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method writeReplace()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 696
    new-instance v0, Likk;

    iget-object v1, p0, Liki;->dDm:Ljava/util/Comparator;

    invoke-virtual {p0}, Liki;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Likk;-><init>(Ljava/util/Comparator;[Ljava/lang/Object;)V

    return-object v0
.end method
