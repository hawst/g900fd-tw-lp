.class public final Ljhe;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ely:[Ljhe;


# instance fields
.field private aez:I

.field private eiQ:Ljava/lang/String;

.field public elz:[Ljhf;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljhe;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljhe;->eiQ:Ljava/lang/String;

    invoke-static {}, Ljhf;->blB()[Ljhf;

    move-result-object v0

    iput-object v0, p0, Ljhe;->elz:[Ljhf;

    const/4 v0, 0x0

    iput-object v0, p0, Ljhe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhe;->eCz:I

    return-void
.end method

.method public static blz()[Ljhe;
    .locals 2

    sget-object v0, Ljhe;->ely:[Ljhe;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljhe;->ely:[Ljhe;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljhe;

    sput-object v0, Ljhe;->ely:[Ljhe;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljhe;->ely:[Ljhe;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhe;->eiQ:Ljava/lang/String;

    iget v0, p0, Ljhe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhe;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhe;->elz:[Ljhf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhf;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljhe;->elz:[Ljhf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljhf;

    invoke-direct {v3}, Ljhf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljhe;->elz:[Ljhf;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljhf;

    invoke-direct {v3}, Ljhf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhe;->elz:[Ljhf;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget v0, p0, Ljhe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljhe;->eiQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ljhe;->elz:[Ljhf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljhe;->elz:[Ljhf;

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljhe;->elz:[Ljhf;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Ljhe;->elz:[Ljhf;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bjI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhe;->eiQ:Ljava/lang/String;

    return-object v0
.end method

.method public final blA()Z
    .locals 1

    iget v0, p0, Ljhe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljhe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljhe;->eiQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljhe;->elz:[Ljhf;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljhe;->elz:[Ljhf;

    array-length v1, v1

    if-lez v1, :cond_3

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljhe;->elz:[Ljhf;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ljhe;->elz:[Ljhf;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    :cond_3
    return v0
.end method

.method public final uW(Ljava/lang/String;)Ljhe;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhe;->eiQ:Ljava/lang/String;

    iget v0, p0, Ljhe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhe;->aez:I

    return-object p0
.end method
