.class public final Lcmd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final amT:J

.field private final mGsaConfig:Lchk;

.field private final mHttpExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mHttpHelper:Ldkx;

.field private final mSdchManager:Lczz;


# direct methods
.method public constructor <init>(Lchk;Ljava/util/concurrent/ExecutorService;Ldkx;Lczz;J)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcmd;->mGsaConfig:Lchk;

    .line 25
    iput-object p2, p0, Lcmd;->mHttpExecutor:Ljava/util/concurrent/ExecutorService;

    .line 26
    iput-object p3, p0, Lcmd;->mHttpHelper:Ldkx;

    .line 27
    iput-object p4, p0, Lcmd;->mSdchManager:Lczz;

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcmd;->amT:J

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Ldlb;I)Lcma;
    .locals 6

    .prologue
    .line 40
    new-instance v0, Legl;

    iget-wide v2, p0, Lcmd;->amT:J

    sget-object v1, Leoi;->cgG:Leoi;

    invoke-virtual {v1}, Leoi;->auU()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Legl;-><init>(JJ)V

    .line 43
    new-instance v1, Lclu;

    iget-object v2, p0, Lcmd;->mHttpExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lcmd;->mGsaConfig:Lchk;

    iget-object v4, p0, Lcmd;->mSdchManager:Lczz;

    invoke-direct {v1, v2, v3, v4, v0}, Lclu;-><init>(Ljava/util/concurrent/ExecutorService;Lchk;Lczz;Legl;)V

    .line 44
    iget-object v0, p0, Lcmd;->mHttpHelper:Ldkx;

    invoke-virtual {v0, p1, p2, v1}, Ldkx;->a(Ldlb;ILdlg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcma;

    return-object v0
.end method
