.class public abstract Lfet;
.super Lfeu;
.source "PG"


# instance fields
.field private cpU:Ljbp;

.field private mDirectionsLauncher:Lgah;

.field protected final mFrequentPlaceEntry:Ljal;

.field protected final mTravelReport:Lgca;


# direct methods
.method public constructor <init>(Lizj;Ljbp;Lgah;Lemp;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 29
    iput-object p2, p0, Lfet;->cpU:Ljbp;

    .line 30
    invoke-static {p1}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v0

    iput-object v0, p0, Lfet;->mFrequentPlaceEntry:Ljal;

    .line 31
    iget-object v0, p0, Lfet;->mFrequentPlaceEntry:Ljal;

    iget-object v0, v0, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 32
    new-instance v0, Lgca;

    iget-object v1, p0, Lfet;->mFrequentPlaceEntry:Ljal;

    iget-object v1, v1, Ljal;->dMF:[Liyg;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1, p4}, Lgca;-><init>(Liyg;Lemp;)V

    iput-object v0, p0, Lfet;->mTravelReport:Lgca;

    .line 36
    :goto_0
    iput-object p3, p0, Lfet;->mDirectionsLauncher:Lgah;

    .line 37
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfet;->mTravelReport:Lgca;

    goto :goto_0
.end method


# virtual methods
.method public final d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lfet;->mTravelReport:Lgca;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lfet;->mTravelReport:Lgca;

    iget-object v0, v0, Lgca;->mRoute:Liyg;

    .line 43
    iget-object v1, p0, Lfet;->mFrequentPlaceEntry:Ljal;

    iget-object v1, v1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    .line 44
    invoke-static {p1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v2

    .line 46
    new-instance v3, Lffj;

    iget-object v4, p0, Lfet;->mDirectionsLauncher:Lgah;

    iget-object v5, p0, Lfet;->cpU:Ljbp;

    invoke-direct {v3, v2, v4, v1, v0}, Lffj;-><init>(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Lgah;Ljbp;Liyg;)V

    .line 48
    invoke-static {v3}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_0
.end method
