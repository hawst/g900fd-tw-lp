.class public Lckh;
.super Lcfy;
.source "PG"


# instance fields
.field private final aZA:Ljava/lang/String;

.field private final aZB:Ljava/lang/String;

.field private final aZC:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final aZD:Ljava/util/Map;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final aZE:[Ljava/lang/String;

.field private final aZz:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;ZZZ)V
    .locals 8
    .param p8    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 69
    const v6, 0x7f04004e

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lcfy;-><init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;ILjava/util/Map;)V

    .line 70
    iput-object p5, p0, Lckh;->aZz:Ljava/lang/String;

    .line 71
    iput-object p6, p0, Lckh;->aZA:Ljava/lang/String;

    .line 72
    iput-object p7, p0, Lckh;->aZB:Ljava/lang/String;

    .line 73
    move-object/from16 v0, p8

    iput-object v0, p0, Lckh;->aZC:Ljava/lang/String;

    .line 74
    move-object/from16 v0, p9

    iput-object v0, p0, Lckh;->aZD:Ljava/util/Map;

    .line 75
    move-object/from16 v0, p11

    iput-object v0, p0, Lckh;->aZE:[Ljava/lang/String;

    .line 77
    return-void
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)Lckh;
    .locals 15

    .prologue
    .line 159
    new-instance v0, Lckh;

    invoke-static {p0}, Lckh;->hg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v2, 0x0

    new-array v11, v2, [Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v14, 0x0

    move/from16 v2, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v10, p4

    move/from16 v13, p5

    invoke-direct/range {v0 .. v14}, Lckh;-><init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;ZZZ)V

    return-object v0
.end method

.method public static a(Ljja;Ljava/lang/String;)Lckh;
    .locals 15

    .prologue
    .line 34
    invoke-virtual {p0}, Ljja;->OL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p0}, Ljja;->OL()Ljava/lang/String;

    move-result-object v6

    .line 39
    :goto_0
    new-instance v0, Lckh;

    iget-object v1, p0, Ljja;->eog:Ljava/lang/String;

    invoke-static {v1}, Lckh;->hg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljja;->bnB()I

    move-result v2

    iget-object v3, p0, Ljja;->eoh:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Ljja;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Ljja;->eoi:Ljava/lang/String;

    invoke-virtual {p0}, Ljja;->bnC()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Ljja;->bnD()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Ljja;->eon:[Ljjb;

    invoke-static {v9}, Lckh;->a([Ljjb;)Ljava/util/Map;

    move-result-object v9

    iget-object v10, p0, Ljja;->eok:[Ljjb;

    invoke-static {v10}, Lckh;->a([Ljjb;)Ljava/util/Map;

    move-result-object v10

    iget-object v11, p0, Ljja;->eor:[Ljava/lang/String;

    invoke-virtual {p0}, Ljja;->bnE()Z

    move-result v12

    invoke-virtual {p0}, Ljja;->bnF()Z

    move-result v13

    invoke-virtual {p0}, Ljja;->bnK()Z

    move-result v14

    invoke-direct/range {v0 .. v14}, Lckh;-><init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;ZZZ)V

    return-object v0

    :cond_0
    move-object/from16 v6, p1

    .line 37
    goto :goto_0
.end method

.method public static hg(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "web"

    .line 168
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "web."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final OK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lckh;->aZz:Ljava/lang/String;

    return-object v0
.end method

.method public final OL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lckh;->aZA:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Landroid/net/Uri;Z)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 122
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lckh;->aZB:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lckh;->aZC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 142
    :goto_0
    return v0

    .line 126
    :cond_0
    if-eqz p2, :cond_3

    .line 127
    iget-object v0, p0, Lckh;->aZD:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lckh;->aZD:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 129
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v3

    .line 134
    goto :goto_0

    :cond_2
    move v0, v2

    .line 139
    goto :goto_0

    :cond_3
    move v0, v3

    .line 142
    goto :goto_0
.end method

.method public final isEnabled()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 101
    iget-object v2, p0, Lckh;->aZE:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 102
    invoke-static {}, Lesp;->avF()Ljava/lang/String;

    move-result-object v3

    .line 103
    iget-object v4, p0, Lckh;->aZE:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 104
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 111
    :cond_0
    :goto_1
    return v0

    .line 103
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 108
    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WebCorpus["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcfy;->aqV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lckh;->aZz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PATH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lckh;->aZB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", AUTH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lckh;->aZC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PARAMS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lckh;->aZD:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MODELINK:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcfy;->aUJ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
