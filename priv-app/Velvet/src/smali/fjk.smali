.class public final Lfjk;
.super Lfnm;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field private final mPresenter:Lgyi;

.field private final mSpeechLevelSource:Lequ;


# direct methods
.method public constructor <init>(Lgyi;Lequ;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lfnm;-><init>()V

    .line 28
    iput-object p1, p0, Lfjk;->mPresenter:Lgyi;

    .line 29
    iput-object p2, p0, Lfjk;->mSpeechLevelSource:Lequ;

    .line 31
    iget-object v0, p0, Lfjk;->mPresenter:Lgyi;

    invoke-virtual {v0}, Lgyi;->Nf()Ldda;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lfjk;->mPresenter:Lgyi;

    invoke-virtual {v0, p0}, Lgyi;->a(Lddc;)V

    .line 36
    :cond_0
    return-void
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lddb;)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfjk;->fE(I)V

    .line 80
    iget-object v0, p0, Lfjk;->mPresenter:Lgyi;

    invoke-virtual {v0, p0}, Lgyi;->b(Lddc;)V

    .line 87
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 83
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lfjk;->fE(I)V

    goto :goto_0

    .line 85
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfjk;->fE(I)V

    goto :goto_0
.end method

.method public final aAr()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lfjk;->mPresenter:Lgyi;

    invoke-virtual {v0}, Lgyi;->Nf()Ldda;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apf()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 49
    iget-object v0, p0, Lfjk;->mPresenter:Lgyi;

    invoke-virtual {v0, p0}, Lgyi;->a(Lddc;)V

    .line 51
    :cond_0
    return-void
.end method

.method public final aAs()Lequ;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lfjk;->mSpeechLevelSource:Lequ;

    return-object v0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lfjk;->mPresenter:Lgyi;

    invoke-virtual {v0}, Lgyi;->Nf()Ldda;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xb()Z

    .line 68
    :cond_0
    return-void
.end method
