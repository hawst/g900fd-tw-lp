.class public abstract Lfog;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfoh;


# instance fields
.field private final cvM:Ljava/lang/String;

.field private final cxx:Landroid/net/Uri;

.field private final cxy:Landroid/view/View;

.field private final mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lfog;->mActivity:Landroid/app/Activity;

    .line 111
    iput-object v0, p0, Lfog;->cvM:Ljava/lang/String;

    .line 112
    iput-object v0, p0, Lfog;->cxx:Landroid/net/Uri;

    .line 113
    iput-object p2, p0, Lfog;->cxy:Landroid/view/View;

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lfog;->mActivity:Landroid/app/Activity;

    .line 119
    iput-object p2, p0, Lfog;->cvM:Ljava/lang/String;

    .line 120
    iput-object p3, p0, Lfog;->cxx:Landroid/net/Uri;

    .line 121
    iput-object p4, p0, Lfog;->cxy:Landroid/view/View;

    .line 122
    return-void
.end method


# virtual methods
.method public a(Lfom;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lfog;->mActivity:Landroid/app/Activity;

    const-string v1, "com.google.android.googlequicksearchbox.MY_REMINDERS"

    invoke-static {v0, v1}, Lgaw;->m(Landroid/content/Context;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public final aBK()V
    .locals 3

    .prologue
    .line 144
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 145
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.velvet.ui.settings.SettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    iget-object v1, p0, Lfog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 149
    return-void
.end method

.method public b(Lfom;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lfog;->mActivity:Landroid/app/Activity;

    const-string v1, "com.google.android.googlequicksearchbox.TRAINING_CLOSET"

    invoke-static {v0, v1}, Lgaw;->m(Landroid/content/Context;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public c(Lfom;)V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lfog;->mActivity:Landroid/app/Activity;

    const-string v1, "com.google.android.handsfree.ENTER_CAR_MODE_VIA_MENU"

    invoke-static {v0, v1}, Lgaw;->m(Landroid/content/Context;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public d(Lfom;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 154
    iget-object v0, p0, Lfog;->cvM:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfog;->cxx:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 155
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A help context and url must be set for the default help and feedback action to take place"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    iget-object v0, p0, Lfog;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfog;->cvM:Ljava/lang/String;

    iget-object v2, p0, Lfog;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lfnx;->aQ(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lfog;->cxx:Landroid/net/Uri;

    iget-object v4, p0, Lfog;->cxy:Landroid/view/View;

    new-instance v5, Lenz;

    iget-object v7, p0, Lfog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lenz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Lenz;->acJ()V

    const-string v7, "GEL.GSAPrefs.now_enabled"

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lgbk;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V

    .line 162
    return-void
.end method
