.class public final Lglv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglx;


# instance fields
.field private final aWy:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lglv;->aWy:Ljava/util/List;

    .line 35
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 115
    invoke-interface {v0}, Lglx;->CZ()V

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method public final Nr()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 157
    invoke-interface {v0}, Lglx;->Nr()V

    goto :goto_0

    .line 159
    :cond_0
    return-void
.end method

.method public final Ns()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 171
    invoke-interface {v0}, Lglx;->Ns()V

    goto :goto_0

    .line 173
    :cond_0
    return-void
.end method

.method public final Nv()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 150
    invoke-interface {v0}, Lglx;->Nv()V

    goto :goto_0

    .line 152
    :cond_0
    return-void
.end method

.method public final Nw()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 143
    invoke-interface {v0}, Lglx;->Nw()V

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method

.method public final a(Lieb;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 94
    invoke-interface {v0, p1}, Lglx;->a(Lieb;)V

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method public final a(Ljps;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 73
    invoke-interface {v0, p1}, Lglx;->a(Ljps;)V

    goto :goto_0

    .line 75
    :cond_0
    return-void
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 101
    invoke-interface {v0, p1, p2}, Lglx;->a(Ljvv;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method public final a(Ljxb;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 87
    invoke-interface {v0, p1}, Lglx;->a(Ljxb;)V

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public final aEM()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 122
    invoke-interface {v0}, Lglx;->aEM()V

    goto :goto_0

    .line 124
    :cond_0
    return-void
.end method

.method public final au(J)V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 129
    invoke-interface {v0, p1, p2}, Lglx;->au(J)V

    goto :goto_0

    .line 131
    :cond_0
    return-void
.end method

.method public final b(Lglx;)V
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 40
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljwp;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 80
    invoke-interface {v0, p1}, Lglx;->b(Ljwp;)V

    goto :goto_0

    .line 82
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 108
    invoke-interface {v0, p1}, Lglx;->c(Lcom/google/android/shared/speech/HotwordResult;)V

    goto :goto_0

    .line 110
    :cond_0
    return-void
.end method

.method public final c(Leiq;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 59
    invoke-interface {v0, p1}, Lglx;->c(Leiq;)V

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method

.method public final d(Ljyl;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 164
    invoke-interface {v0, p1}, Lglx;->d(Ljyl;)V

    goto :goto_0

    .line 166
    :cond_0
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 136
    invoke-interface {v0}, Lglx;->onEndOfSpeech()V

    goto :goto_0

    .line 138
    :cond_0
    return-void
.end method

.method public final v([B)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lglv;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    .line 66
    invoke-interface {v0, p1}, Lglx;->v([B)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method
