.class public final Lggh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cJv:[Ljava/lang/String;

.field private static final cJw:[Ljava/lang/String;

.field private static final cJx:[Ljava/lang/String;

.field private static final cJy:[Ljava/lang/String;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mRelationshipManager:Lcjg;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "lookup"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    sput-object v0, Lggh;->cJv:[Ljava/lang/String;

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v2

    const-string v1, "lookup"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data3"

    aput-object v1, v0, v6

    sput-object v0, Lggh;->cJw:[Ljava/lang/String;

    .line 51
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v2

    const-string v1, "lookup"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data3"

    aput-object v1, v0, v6

    sput-object v0, Lggh;->cJx:[Ljava/lang/String;

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v2

    const-string v1, "lookup"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data3"

    aput-object v1, v0, v6

    sput-object v0, Lggh;->cJy:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Lcjg;)V
    .locals 0
    .param p1    # Landroid/content/ContentResolver;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lggh;->mContentResolver:Landroid/content/ContentResolver;

    .line 78
    iput-object p2, p0, Lggh;->mRelationshipManager:Lcjg;

    .line 79
    return-void
.end method

.method private a(JLdzb;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p3    # Ldzb;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # [Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 151
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 152
    iget-object v0, p0, Lggh;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    move-object v1, p4

    move-object v2, p5

    move-object v3, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    if-eqz v0, :cond_0

    .line 156
    new-instance v2, Lggi;

    invoke-direct {v2, p3, v1}, Lggi;-><init>(Ldzb;Ljava/util/List;)V

    .line 157
    invoke-static {v2, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    .line 159
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final bz(J)Lcom/google/android/search/shared/contact/Person;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 88
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lggh;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lggh;->cJv:[Ljava/lang/String;

    const-string v3, "_id = ?"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lggj;

    invoke-direct {v1}, Lggj;-><init>()V

    invoke-static {v1, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    iget-object v0, v1, Lggj;->aYL:Lcom/google/android/search/shared/contact/Person;

    .line 89
    :goto_0
    if-nez v0, :cond_1

    .line 90
    const-string v0, "ContactIdLookup"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetchContactInfo() : No Result for ContactId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 98
    :goto_1
    return-object v5

    :cond_0
    move-object v0, v5

    .line 88
    goto :goto_0

    .line 93
    :cond_1
    iget-object v1, p0, Lggh;->mRelationshipManager:Lcjg;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcjg;->f(Ljava/util/Collection;)V

    .line 94
    sget-object v4, Ldzb;->bRq:Ldzb;

    sget-object v5, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lggh;->cJw:[Ljava/lang/String;

    const-string v7, "contact_id = ?"

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v7}, Lggh;->a(JLdzb;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Person;->D(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    .line 95
    sget-object v4, Ldzb;->bRp:Ldzb;

    sget-object v5, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lggh;->cJx:[Ljava/lang/String;

    const-string v7, "contact_id = ?"

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v7}, Lggh;->a(JLdzb;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Person;->E(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    .line 96
    sget-object v4, Ldzb;->bRr:Ldzb;

    sget-object v5, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lggh;->cJy:[Ljava/lang/String;

    const-string v7, "contact_id = ?"

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v7}, Lggh;->a(JLdzb;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Person;->G(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    move-object v5, v0

    .line 98
    goto :goto_1
.end method
