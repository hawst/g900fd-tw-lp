.class public final Lekl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bXk:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final bXl:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final cdr:Landroid/view/View;

.field public final cds:Ljava/util/List;

.field private final cdt:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f110013

    .line 1553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1551
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lekl;->cdt:Landroid/graphics/Rect;

    .line 1554
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1555
    iput-object p1, p0, Lekl;->cdr:Landroid/view/View;

    .line 1556
    iput-object p2, p0, Lekl;->bXk:Landroid/view/View;

    .line 1557
    iput-object p3, p0, Lekl;->bXl:Landroid/view/View;

    .line 1559
    iget-object v0, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0, v2, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1560
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v0

    .line 1561
    iget-object v1, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0, v1}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 1562
    iget-object v1, p0, Lekl;->bXk:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1563
    iget-object v1, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v0, v1}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 1564
    iget-object v1, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v1, v2, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1566
    :cond_0
    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1567
    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v0, v1}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 1568
    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v1, v2, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1570
    :cond_1
    iget-object v0, v0, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lekl;->cds:Ljava/util/List;

    .line 1571
    return-void
.end method

.method public static aQ(Landroid/view/View;)Lekl;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1697
    const v0, 0x7f110013

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekl;

    return-object v0
.end method

.method private static aS(II)I
    .locals 2

    .prologue
    .line 1609
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 1610
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    add-int/2addr v1, p1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p0

    :cond_0
    return p0
.end method


# virtual methods
.method public final aug()Z
    .locals 2

    .prologue
    .line 1669
    iget-object v0, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final fb(Z)V
    .locals 3

    .prologue
    .line 1662
    if-eqz p1, :cond_0

    const/16 v0, 0x8

    move v1, v0

    .line 1663
    :goto_0
    iget-object v0, p0, Lekl;->cds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1664
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1662
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1666
    :cond_1
    return-void
.end method

.method public final getMeasuredHeight()I
    .locals 3

    .prologue
    .line 1623
    iget-object v0, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1625
    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 1627
    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1629
    :cond_0
    return v0
.end method

.method public final getMeasuredWidth()I
    .locals 3

    .prologue
    .line 1616
    iget-object v0, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method public final id(I)V
    .locals 3

    .prologue
    .line 1673
    iget-object v0, p0, Lekl;->cds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1674
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 1676
    :cond_0
    return-void
.end method

.method public final measure(II)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1588
    iget-object v0, p0, Lekl;->cdt:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lekl;->cdt:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1594
    :cond_0
    iget-object v0, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    invoke-static {p1, v0}, Lekl;->aS(II)I

    move-result v0

    .line 1596
    iget-object v1, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    invoke-static {p2, v1}, Lekl;->aS(II)I

    move-result v1

    .line 1599
    iget-object v2, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1600
    iget-object v0, p0, Lekl;->bXk:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 1601
    iget-object v0, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 1603
    :cond_1
    iget-object v0, p0, Lekl;->bXl:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_2

    .line 1604
    iget-object v0, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 1606
    :cond_2
    return-void
.end method

.method public final y(III)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 1633
    iget-object v0, p0, Lekl;->bXk:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_0

    .line 1634
    iget-object v0, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 1636
    iget-object v1, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, p2, v1

    iget v2, v0, Lekm;->cdC:I

    add-int/2addr v1, v2

    .line 1637
    iget-object v2, p0, Lekl;->bXk:Landroid/view/View;

    iget v3, v0, Lekm;->cdB:I

    add-int/2addr v3, p1

    iget v0, v0, Lekm;->cdB:I

    add-int/2addr v0, p3

    iget-object v4, p0, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/view/View;->layout(IIII)V

    .line 1641
    :cond_0
    iget-object v0, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, p2, v0

    .line 1642
    iget-object v1, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 1643
    iget-object v2, p0, Lekl;->cdr:Landroid/view/View;

    iget-object v3, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v3, p1, v3

    iget-object v4, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, p3

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/view/View;->layout(IIII)V

    .line 1649
    iget-object v0, p0, Lekl;->bXl:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v5, :cond_1

    .line 1651
    iget-object v0, p0, Lekl;->cdt:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    .line 1652
    iget-object v1, p0, Lekl;->bXl:Landroid/view/View;

    iget-object v2, p0, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, p1, v0, p3, v2}, Landroid/view/View;->layout(IIII)V

    .line 1655
    :cond_1
    return-void
.end method
