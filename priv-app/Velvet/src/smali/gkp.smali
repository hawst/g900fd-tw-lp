.class public final Lgkp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cNM:Ljup;

.field private static final cNW:[F


# instance fields
.field private final aOx:Ligi;

.field final bhl:Ljava/util/concurrent/Executor;

.field private final cNN:Z

.field private final cNO:Ljava/util/concurrent/Executor;

.field private cNP:I

.field private cNQ:Lcom/google/speech/grammar/pumpkin/ActionFrame;

.field private cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

.field private cNS:Lgkl;

.field private cNT:Lgko;

.field private cNU:Lgkk;

.field private cNV:Lgkn;

.field private mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

.field private final mAppSelectionHelper:Libo;

.field private final mContactLookup:Lghy;

.field private mDisambiguationFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

.field private final mGsaConfigFlags:Lchk;

.field private final mPumpkinLoader:Lgio;

.field private final mRelationshipNameLookup:Leai;

.field private final mServices:Lcfo;

.field private mTagger:Lcom/google/speech/grammar/pumpkin/Tagger;

.field private mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljup;

    invoke-direct {v0}, Ljup;-><init>()V

    sput-object v0, Lgkp;->cNM:Ljup;

    .line 157
    const/16 v0, 0xa

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lgkp;->cNW:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f68f5c3    # 0.91f
        0x3f6b851f    # 0.92f
        0x3f6e147b    # 0.93f
        0x3f70a3d7    # 0.94f
        0x3f733333    # 0.95f
        0x3f75c28f    # 0.96f
        0x3f7851ec    # 0.97f
        0x3f7ae148    # 0.98f
        0x3f7d70a4    # 0.99f
    .end array-data
.end method

.method public constructor <init>(Lcfo;ZLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgio;Lghy;Libo;Lchk;Leai;)V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lgkp;->mServices:Lcfo;

    .line 121
    iput-boolean p2, p0, Lgkp;->cNN:Z

    .line 122
    iput-object p3, p0, Lgkp;->bhl:Ljava/util/concurrent/Executor;

    .line 123
    iput-object p4, p0, Lgkp;->cNO:Ljava/util/concurrent/Executor;

    .line 124
    iput-object p5, p0, Lgkp;->mPumpkinLoader:Lgio;

    .line 125
    iput-object p6, p0, Lgkp;->mContactLookup:Lghy;

    .line 126
    iput-object p7, p0, Lgkp;->mAppSelectionHelper:Libo;

    .line 127
    iput-object p8, p0, Lgkp;->mGsaConfigFlags:Lchk;

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lgkp;->cNP:I

    .line 129
    iput-object p9, p0, Lgkp;->mRelationshipNameLookup:Leai;

    .line 130
    invoke-virtual {p1}, Lcfo;->Eg()Ligi;

    move-result-object v0

    iput-object v0, p0, Lgkp;->aOx:Ligi;

    .line 131
    return-void
.end method

.method private static F(Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 481
    sget-object v0, Lhzi;->dvr:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 482
    if-nez v0, :cond_0

    .line 483
    const-string v0, "PumpkinTagger"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isActionEnabled() - unknown, assuming false: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 488
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lgkp;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lgkp;->cNP:I

    return v0
.end method

.method public static al(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 373
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 374
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 375
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 376
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    .line 379
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 380
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_1
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 384
    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 385
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 389
    :cond_3
    return-object v1
.end method

.method static synthetic b(Lgkp;)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    return-object v0
.end method

.method private b(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;)V
    .locals 6
    .param p1    # Lcom/google/android/search/shared/contact/PersonDisambiguation;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 263
    invoke-static {p1}, Lgkp;->o(Lcom/google/android/search/shared/contact/PersonDisambiguation;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 275
    iget-object v0, p0, Lgkp;->mDisambiguationFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->aGd()[B

    move-result-object v0

    invoke-static {v0}, Lgio;->S([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v0

    iput-object v0, p0, Lgkp;->mDisambiguationFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 276
    :cond_0
    :goto_0
    iget v0, p0, Lgkp;->cNP:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const-string v0, "PumpkinTagger"

    const-string v1, "addUserValidators(): Pumpkin not initialized"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 277
    :goto_1
    iget-object v0, p0, Lgkp;->mDisambiguationFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iput-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    .line 279
    :goto_2
    return-void

    .line 265
    :pswitch_0
    iget-object v0, p0, Lgkp;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iput-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    goto :goto_2

    .line 268
    :pswitch_1
    iget-object v0, p0, Lgkp;->cNQ:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    if-nez v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->aGe()[B

    move-result-object v0

    invoke-static {v0}, Lgio;->S([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v0

    iput-object v0, p0, Lgkp;->cNQ:Lcom/google/speech/grammar/pumpkin/ActionFrame;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 269
    :cond_1
    :goto_3
    iget-object v0, p0, Lgkp;->cNQ:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iput-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    goto :goto_2

    .line 268
    :catch_0
    move-exception v0

    const-string v1, "PumpkinTagger"

    const-string v2, "Couldn\'t load set recipient assets."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_3

    .line 275
    :catch_1
    move-exception v0

    const-string v1, "PumpkinTagger"

    const-string v2, "Couldn\'t load disambiguation assets."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 276
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lgkp;->al(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, p2}, Lgkp;->c(Ljava/util/List;Ldzb;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    monitor-enter v2

    :try_start_2
    iget-object v3, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v4, "SELECT_NAME"

    new-instance v5, Lgkm;

    invoke-direct {v5, v1}, Lgkm;-><init>(Ljava/util/List;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    iget-object v1, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v3, "SELECT_TYPE"

    new-instance v4, Lgkm;

    invoke-direct {v4, v0}, Lgkm;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v3, v4}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static b(Ljup;)Z
    .locals 1

    .prologue
    .line 693
    if-eqz p0, :cond_0

    iget-object v0, p0, Ljup;->eFE:[Ljuo;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lgkp;)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lgkp;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    return-object v0
.end method

.method public static c(Ljava/util/List;Ldzb;)Ljava/util/List;
    .locals 5
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 395
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 396
    if-eqz p1, :cond_0

    sget-object v0, Ldzb;->bRt:Ldzb;

    if-ne p1, v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 416
    :goto_0
    return-object v0

    .line 402
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 403
    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v0

    .line 404
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 405
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 406
    if-eqz v0, :cond_3

    .line 408
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 412
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 416
    goto :goto_0
.end method

.method public static na(Ljava/lang/String;)F
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 170
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 171
    :cond_0
    const/4 v0, 0x0

    .line 175
    :goto_0
    sget-object v1, Lgkp;->cNW:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 176
    sget-object v1, Lgkp;->cNW:[F

    aget v0, v1, v0

    return v0

    .line 173
    :cond_1
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    goto :goto_0
.end method

.method public static o(Lcom/google/android/search/shared/contact/PersonDisambiguation;)I
    .locals 1
    .param p0    # Lcom/google/android/search/shared/contact/PersonDisambiguation;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 291
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    const/4 v0, 0x0

    .line 305
    :goto_0
    return v0

    .line 299
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alA()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 305
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method final a(Lijj;ZILitp;Lcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;
    .locals 3
    .param p1    # Lijj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 742
    const/4 v0, 0x0

    .line 743
    if-lez p3, :cond_2

    if-eqz p1, :cond_2

    .line 745
    const/4 v1, 0x0

    :goto_0
    if-ge v1, p3, :cond_2

    invoke-virtual {p1}, Lijj;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    if-eqz v0, :cond_0

    iget-object v2, v0, Ljup;->eFE:[Ljuo;

    array-length v2, v2

    if-nez v2, :cond_2

    .line 747
    :cond_0
    invoke-virtual {p1, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p5}, Lgkp;->a(Ljava/lang/String;ZLcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v2

    .line 748
    invoke-static {v2}, Lgkp;->b(Ljup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p4, v0}, Litp;->mw(I)Litp;

    .line 746
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 753
    :cond_2
    return-object v0
.end method

.method final a(Ljava/lang/String;Lijj;ZILitp;Lcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;
    .locals 6
    .param p2    # Lijj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 712
    const/4 v0, 0x0

    .line 713
    iget-object v1, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v2, "CONTACT"

    iget-object v3, p0, Lgkp;->cNV:Lgkn;

    invoke-virtual {v1, v2, v3}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 715
    iget-object v1, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v2, "APP"

    iget-object v3, p0, Lgkp;->cNV:Lgkn;

    invoke-virtual {v1, v2, v3}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 717
    invoke-virtual {p0, p1, p3, p6}, Lgkp;->a(Ljava/lang/String;ZLcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v1

    .line 719
    invoke-static {v1}, Lgkp;->b(Ljup;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Ljup;->eFE:[Ljuo;

    aget-object v2, v2, v4

    iget-object v2, v2, Ljuo;->eFB:Ljava/lang/String;

    const-string v3, "CallContact"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, v1, Ljup;->eFE:[Ljuo;

    aget-object v1, v1, v4

    iget-object v1, v1, Ljuo;->eFB:Ljava/lang/String;

    const-string v2, "OpenApp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 725
    invoke-virtual/range {v0 .. v5}, Lgkp;->a(Lijj;ZILitp;Lcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v0

    .line 728
    :cond_1
    iget-object v1, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v2, "CONTACT"

    iget-object v3, p0, Lgkp;->cNS:Lgkl;

    invoke-virtual {v1, v2, v3}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 730
    iget-object v1, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v2, "APP"

    iget-object v3, p0, Lgkp;->cNU:Lgkk;

    invoke-virtual {v1, v2, v3}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 733
    return-object v0
.end method

.method public final a(Ljava/lang/String;ZLcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523
    const-string v2, "play some music"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "play music"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "play songs"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "play song"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "play a song"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "play some songs"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "play some song"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isActionBlacklisted: query = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " matched blacklist"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v2, :cond_3

    .line 526
    sget-object v0, Lgkp;->cNM:Ljup;

    .line 564
    :goto_1
    return-object v0

    :cond_2
    move v2, v1

    .line 523
    goto :goto_0

    .line 529
    :cond_3
    monitor-enter p0

    .line 533
    :try_start_0
    iget v2, p0, Lgkp;->cNP:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    if-nez p3, :cond_5

    .line 534
    :cond_4
    new-instance v0, Ljup;

    invoke-direct {v0}, Ljup;-><init>()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 537
    :cond_5
    :try_start_1
    iget v2, p0, Lgkp;->cNP:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_2
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 538
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540
    const/16 v0, 0x153

    invoke-static {v0}, Lege;->ht(I)V

    .line 544
    iget-object v2, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    monitor-enter v2

    .line 545
    :try_start_2
    iget-object v0, p0, Lgkp;->mTagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    iget-object v3, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    invoke-virtual {v0, p1, p3, v3}, Lcom/google/speech/grammar/pumpkin/Tagger;->a(Ljava/lang/String;Lcom/google/speech/grammar/pumpkin/ActionFrame;Lcom/google/speech/grammar/pumpkin/UserValidators;)Ljup;

    move-result-object v0

    .line 546
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 547
    invoke-static {v0}, Lgkp;->b(Ljup;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, Ljup;->eFE:[Ljuo;

    aget-object v1, v2, v1

    iget-object v1, v1, Ljuo;->eFB:Ljava/lang/String;

    invoke-virtual {p0, v1, p2}, Lgkp;->z(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_6

    .line 553
    sget-object v0, Lgkp;->cNM:Ljup;

    .line 556
    :cond_6
    const/16 v1, 0x154

    invoke-static {v1}, Lege;->ht(I)V

    goto :goto_1

    :cond_7
    move v0, v1

    .line 537
    goto :goto_2

    .line 546
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcky;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 235
    if-nez p1, :cond_0

    move-object v1, v0

    .line 241
    :goto_0
    if-eqz v1, :cond_1

    .line 242
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/CommunicationAction;->afZ()Ldzb;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lgkp;->b(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;)V

    .line 258
    :goto_1
    return-void

    .line 235
    :cond_0
    invoke-virtual {p1}, Lcky;->Pn()Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v1

    goto :goto_0

    .line 248
    :cond_1
    if-nez p1, :cond_2

    move-object v1, v0

    .line 250
    :goto_2
    if-eqz v1, :cond_3

    .line 251
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akB()Ldzb;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lgkp;->b(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;)V

    goto :goto_1

    .line 248
    :cond_2
    invoke-virtual {p1}, Lcky;->Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 257
    :cond_3
    iget-object v0, p0, Lgkp;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iput-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lefk;Lefk;Lefk;)V
    .locals 8

    .prologue
    .line 423
    iget-boolean v0, p0, Lgkp;->cNN:Z

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lgkp;->mServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    .line 430
    invoke-virtual {p0, v0}, Lgkp;->a(Lcky;)V

    .line 432
    :cond_0
    sget-object v0, Lcgg;->aVH:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v4, 0x4

    .line 434
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apR()Lijj;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v3

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lgkp;->a(Ljava/lang/String;Lijj;ZILefk;Lefk;Lefk;)V

    .line 442
    return-void

    .line 432
    :cond_1
    iget-object v0, p0, Lgkp;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FB()I

    move-result v4

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lijj;ZILefk;Lefk;Lefk;)V
    .locals 12
    .param p2    # Lijj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 585
    iget-object v11, p0, Lgkp;->cNO:Ljava/util/concurrent/Executor;

    new-instance v0, Lgks;

    const-string v2, "tagAsync"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p1

    move v5, p3

    move-object v6, p2

    move/from16 v7, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p6

    move-object/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lgks;-><init>(Lgkp;Ljava/lang/String;[ILjava/lang/String;ZLijj;ILefk;Lefk;Lefk;)V

    invoke-interface {v11, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 641
    return-void
.end method

.method final a(Ljup;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 644
    iget-object v0, p0, Lgkp;->aOx:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    .line 645
    iget-object v3, p1, Ljup;->eFE:[Ljuo;

    aget-object v3, v3, v2

    iget-object v4, v3, Ljuo;->eFB:Ljava/lang/String;

    .line 646
    invoke-virtual {v0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v5

    .line 648
    iget-object v3, p1, Ljup;->eFE:[Ljuo;

    aget-object v3, v3, v2

    iget-object v6, v3, Ljuo;->eFC:[Ljun;

    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v8, v6, v3

    .line 649
    iget-object v9, v8, Ljun;->name:Ljava/lang/String;

    const-string v10, "Contact"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 650
    invoke-virtual {v8}, Ljun;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 653
    iget-object v6, p0, Lgkp;->mRelationshipNameLookup:Leai;

    invoke-interface {v6, v3}, Leai;->kH(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lgkp;->mContactLookup:Lghy;

    invoke-virtual {v6, v3}, Lghy;->mH(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lgkp;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->FX()Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 689
    :goto_1
    return v0

    .line 648
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 665
    :cond_1
    if-eqz v5, :cond_2

    invoke-interface {v5}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v3, v1

    .line 667
    :goto_2
    if-eqz v3, :cond_4

    .line 670
    invoke-static {v4}, Licv;->pd(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    .line 671
    goto :goto_1

    :cond_3
    move v3, v2

    .line 665
    goto :goto_2

    .line 676
    :cond_4
    invoke-static {v4}, Licv;->pd(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    move v0, v1

    .line 677
    goto :goto_1

    .line 681
    :cond_5
    const-string v3, "Undo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 682
    invoke-virtual {v0}, Lcky;->Pg()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_1

    .line 684
    :cond_7
    const-string v3, "Redo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 685
    invoke-virtual {v0}, Lcky;->Ph()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    .line 689
    goto :goto_1
.end method

.method final aGV()Z
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lgkp;->mDisambiguationFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iget-object v1, p0, Lgkp;->mDisambiguationFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aGW()Z
    .locals 2

    .prologue
    .line 701
    iget-object v0, p0, Lgkp;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkp;->cNR:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    iget-object v1, p0, Lgkp;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;ZZ)Z
    .locals 1

    .prologue
    .line 470
    if-eqz p2, :cond_0

    .line 471
    iget-object v0, p0, Lgkp;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Jj()I

    move-result v0

    .line 477
    :goto_0
    invoke-static {p1, v0}, Lgkp;->F(Ljava/lang/String;I)Z

    move-result v0

    return v0

    .line 472
    :cond_0
    if-eqz p3, :cond_1

    .line 473
    iget-object v0, p0, Lgkp;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Jk()I

    move-result v0

    goto :goto_0

    .line 475
    :cond_1
    iget-object v0, p0, Lgkp;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Ji()I

    move-result v0

    goto :goto_0
.end method

.method public final d(Lefk;)V
    .locals 4

    .prologue
    .line 135
    monitor-enter p0

    .line 136
    :try_start_0
    iget v0, p0, Lgkp;->cNP:I

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x1

    iput v0, p0, Lgkp;->cNP:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    monitor-exit p0

    .line 143
    iget-object v0, p0, Lgkp;->cNO:Ljava/util/concurrent/Executor;

    new-instance v1, Lgkq;

    const-string v2, "Initializing"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3, p1}, Lgkq;-><init>(Lgkp;Ljava/lang/String;[ILefk;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 155
    :goto_0
    return-void

    .line 139
    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final ed()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181
    monitor-enter p0

    .line 182
    :try_start_0
    iget v2, p0, Lgkp;->cNP:I

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 183
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    const/16 v0, 0x151

    invoke-static {v0}, Lege;->ht(I)V

    .line 189
    :try_start_1
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->aGb()V

    .line 190
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->ed()V

    .line 193
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->aGc()[B

    move-result-object v0

    invoke-static {v0}, Lgio;->S([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v0

    iput-object v0, p0, Lgkp;->mActionFrame:Lcom/google/speech/grammar/pumpkin/ActionFrame;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 203
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->buJ()Lcom/google/speech/grammar/pumpkin/Tagger;

    move-result-object v0

    iput-object v0, p0, Lgkp;->mTagger:Lcom/google/speech/grammar/pumpkin/Tagger;

    .line 204
    iget-object v0, p0, Lgkp;->mPumpkinLoader:Lgio;

    invoke-virtual {v0}, Lgio;->buK()Lcom/google/speech/grammar/pumpkin/UserValidators;

    move-result-object v0

    iput-object v0, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    .line 205
    new-instance v0, Lgkk;

    iget-object v1, p0, Lgkp;->mAppSelectionHelper:Libo;

    invoke-direct {v0, v1}, Lgkk;-><init>(Libo;)V

    iput-object v0, p0, Lgkp;->cNU:Lgkk;

    .line 206
    new-instance v0, Lgkl;

    iget-object v1, p0, Lgkp;->mContactLookup:Lghy;

    invoke-direct {v0, v1}, Lgkl;-><init>(Lghy;)V

    iput-object v0, p0, Lgkp;->cNS:Lgkl;

    .line 207
    new-instance v0, Lgko;

    iget-object v1, p0, Lgkp;->mRelationshipNameLookup:Leai;

    invoke-direct {v0, v1}, Lgko;-><init>(Leai;)V

    iput-object v0, p0, Lgkp;->cNT:Lgko;

    .line 208
    new-instance v0, Lgkn;

    invoke-direct {v0}, Lgkn;-><init>()V

    iput-object v0, p0, Lgkp;->cNV:Lgkn;

    .line 209
    iget-object v0, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v1, "CONTACT"

    iget-object v2, p0, Lgkp;->cNS:Lgkl;

    invoke-virtual {v0, v1, v2}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 212
    iget-object v0, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v1, "RELATIONSHIP"

    iget-object v2, p0, Lgkp;->cNT:Lgko;

    invoke-virtual {v0, v1, v2}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 215
    iget-object v0, p0, Lgkp;->mUserValidators:Lcom/google/speech/grammar/pumpkin/UserValidators;

    const-string v1, "APP"

    iget-object v2, p0, Lgkp;->cNU:Lgkk;

    invoke-virtual {v0, v1, v2}, Lcom/google/speech/grammar/pumpkin/UserValidators;->addValidator(Ljava/lang/String;Ljuq;)V

    .line 219
    const/16 v0, 0x152

    invoke-static {v0}, Lege;->ht(I)V

    .line 222
    monitor-enter p0

    .line 223
    const/4 v0, 0x2

    :try_start_2
    iput v0, p0, Lgkp;->cNP:I

    .line 224
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 182
    goto :goto_0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    monitor-enter p0

    .line 197
    const/4 v2, 0x3

    :try_start_3
    iput v2, p0, Lgkp;->cNP:I

    .line 198
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 199
    const-string v2, "PumpkinTagger"

    const-string v3, "Couldn\'t load configuration assets."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 198
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 224
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final z(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 451
    if-eqz p2, :cond_1

    const-string v0, "AddRelationship"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "RemoveRelationship"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456
    :cond_0
    const/4 v0, 0x0

    .line 458
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lgkp;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FA()I

    move-result v0

    invoke-static {p1, v0}, Lgkp;->F(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method
