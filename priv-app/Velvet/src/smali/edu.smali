.class public final Ledu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final acx:Ljava/lang/String;

.field private bKO:I

.field private bKP:I

.field private final bVV:I

.field public bVW:I

.field public bVX:Ljava/util/List;

.field private bVY:Lcom/google/android/shared/search/SuggestionLogInfo;

.field private bVZ:I

.field private bWa:I

.field private bWb:Ljava/lang/String;

.field private bWc:J

.field private bWd:J

.field private bWe:J

.field private bWf:J

.field private bWg:J

.field private bWh:J

.field private bWi:J

.field private bWj:J

.field private bWk:J

.field private bWl:J

.field private bWm:I

.field private bWn:I

.field private bWo:J

.field public bWp:Z

.field private bWq:I

.field private bWr:I

.field private bWs:I

.field private final beJ:Ljava/lang/String;

.field private bqv:Lcom/google/android/shared/search/Query;

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lcom/google/android/shared/search/SearchBoxStats;Lemp;Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p2, p0, Ledu;->mClock:Lemp;

    .line 107
    iput-object p3, p0, Ledu;->bqv:Lcom/google/android/shared/search/Query;

    .line 108
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->aro()I

    move-result v0

    iput v0, p0, Ledu;->bVV:I

    .line 109
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arp()I

    move-result v0

    iput v0, p0, Ledu;->bVW:I

    .line 110
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arq()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWd:J

    .line 111
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arI()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWc:J

    .line 112
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arr()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWe:J

    .line 113
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->art()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWg:J

    .line 114
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->aru()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWh:J

    .line 115
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arw()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWk:J

    .line 116
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->ary()I

    move-result v0

    iput v0, p0, Ledu;->bWm:I

    .line 117
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->ars()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWf:J

    .line 118
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arz()I

    move-result v0

    iput v0, p0, Ledu;->bWn:I

    .line 119
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arA()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWo:J

    .line 120
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arD()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ledu;->bVX:Ljava/util/List;

    .line 121
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arE()Lcom/google/android/shared/search/SuggestionLogInfo;

    move-result-object v0

    iput-object v0, p0, Ledu;->bVY:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 122
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arG()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledu;->bWb:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arH()I

    move-result v0

    iput v0, p0, Ledu;->bVZ:I

    .line 124
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arB()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arC()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arF()I

    move-result v0

    iput v0, p0, Ledu;->bWa:I

    .line 127
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arv()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWj:J

    .line 128
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arx()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWl:J

    .line 129
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledu;->acx:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->getSource()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledu;->beJ:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arP()I

    move-result v0

    iput v0, p0, Ledu;->bWq:I

    .line 133
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arQ()I

    move-result v0

    iput v0, p0, Ledu;->bWr:I

    .line 134
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arR()I

    move-result v0

    iput v0, p0, Ledu;->bWs:I

    .line 135
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arS()I

    move-result v0

    iput v0, p0, Ledu;->bKO:I

    .line 136
    invoke-virtual {p1}, Lcom/google/android/shared/search/SearchBoxStats;->arT()I

    move-result v0

    iput v0, p0, Ledu;->bKP:I

    .line 137
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lemp;Lcom/google/android/shared/search/Query;I)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iput-object p3, p0, Ledu;->mClock:Lemp;

    .line 88
    iput-object p1, p0, Ledu;->acx:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Ledu;->beJ:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Ledu;->bqv:Lcom/google/android/shared/search/Query;

    .line 91
    iput p5, p0, Ledu;->bVV:I

    .line 93
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Ledu;->bVX:Ljava/util/List;

    .line 94
    sget-object v0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZT:Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, p0, Ledu;->bVY:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Ledu;->bVZ:I

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Ledu;->bWa:I

    .line 98
    const-string v0, ""

    iput-object v0, p0, Ledu;->bWb:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Ledu;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWc:J

    .line 103
    return-void
.end method

.method private anT()J
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Ledu;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Ledu;->bWc:J

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/google/android/shared/search/SuggestionLogInfo;Z)Ledu;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 192
    iput-object p1, p0, Ledu;->bVX:Ljava/util/List;

    .line 193
    iput-object p2, p0, Ledu;->bVY:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 195
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    .line 196
    invoke-virtual {p2}, Lcom/google/android/shared/search/SuggestionLogInfo;->asJ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p3, :cond_3

    .line 199
    :cond_0
    iget-wide v2, p0, Ledu;->bWi:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    iget-wide v2, p0, Ledu;->bWi:J

    iget-wide v4, p0, Ledu;->bWh:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 200
    :cond_1
    iget v2, p0, Ledu;->bWn:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ledu;->bWn:I

    .line 201
    iget-wide v2, p0, Ledu;->bWo:J

    iget-wide v4, p0, Ledu;->bWh:J

    sub-long v4, v0, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Ledu;->bWo:J

    .line 204
    :cond_2
    iput-wide v0, p0, Ledu;->bWi:J

    .line 205
    iget-object v2, p0, Ledu;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Ledu;->bWf:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    .line 206
    iput-wide v0, p0, Ledu;->bWf:J

    .line 207
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Ledu;->bWm:I

    .line 212
    :cond_3
    return-object p0
.end method

.method public final aQ(Lcom/google/android/shared/search/Query;)Ledu;
    .locals 6

    .prologue
    .line 164
    iput-object p1, p0, Ledu;->bqv:Lcom/google/android/shared/search/Query;

    .line 165
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledu;->bWb:Ljava/lang/String;

    .line 167
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    .line 168
    iget-wide v2, p0, Ledu;->bWg:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 169
    iput-wide v0, p0, Ledu;->bWg:J

    .line 171
    :cond_0
    iput-wide v0, p0, Ledu;->bWh:J

    .line 173
    return-object p0
.end method

.method public final anU()Ledu;
    .locals 4

    .prologue
    .line 146
    iget-wide v0, p0, Ledu;->bWd:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 147
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWd:J

    .line 150
    :cond_0
    return-object p0
.end method

.method public final anV()Ledu;
    .locals 4

    .prologue
    .line 155
    iget-wide v0, p0, Ledu;->bWe:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 156
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWe:J

    .line 159
    :cond_0
    return-object p0
.end method

.method public final anW()Ledu;
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Ledu;->bWq:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ledu;->bWq:I

    .line 271
    return-object p0
.end method

.method public final anX()Ledu;
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Ledu;->bKO:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ledu;->bKO:I

    .line 290
    return-object p0
.end method

.method public final anY()Ledu;
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Ledu;->bKP:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ledu;->bKP:I

    .line 297
    return-object p0
.end method

.method public anZ()Lehj;
    .locals 12

    .prologue
    .line 302
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWk:J

    .line 304
    iget-object v0, p0, Ledu;->acx:Ljava/lang/String;

    iget-object v1, p0, Ledu;->beJ:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v1

    iget v0, p0, Ledu;->bVV:I

    iput v0, v1, Lehj;->bVV:I

    iget-wide v2, p0, Ledu;->bWd:J

    iget-wide v4, p0, Ledu;->bWe:J

    iget-wide v6, p0, Ledu;->bWg:J

    iget-wide v8, p0, Ledu;->bWh:J

    iget-wide v10, p0, Ledu;->bWk:J

    invoke-virtual/range {v1 .. v11}, Lehj;->a(JJJJJ)Lehj;

    move-result-object v0

    iget v1, p0, Ledu;->bWm:I

    iget-wide v2, p0, Ledu;->bWf:J

    iget v4, p0, Ledu;->bWn:I

    iget-wide v5, p0, Ledu;->bWo:J

    invoke-virtual/range {v0 .. v6}, Lehj;->a(IJIJ)Lehj;

    move-result-object v1

    iget-object v0, p0, Ledu;->bVY:Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, v1, Lehj;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    iget-object v0, p0, Ledu;->bWb:Ljava/lang/String;

    iput-object v0, v1, Lehj;->bWb:Ljava/lang/String;

    iget v2, p0, Ledu;->bVZ:I

    iget v3, p0, Ledu;->bWa:I

    iget-wide v4, p0, Ledu;->bWj:J

    iget-wide v6, p0, Ledu;->bWl:J

    invoke-virtual/range {v1 .. v7}, Lehj;->b(IIJJ)Lehj;

    move-result-object v0

    iget v1, p0, Ledu;->bVW:I

    iput v1, v0, Lehj;->bVW:I

    iget-boolean v1, p0, Ledu;->bWp:Z

    iget v2, p0, Ledu;->bWq:I

    iget v3, p0, Ledu;->bWr:I

    iget v4, p0, Ledu;->bWs:I

    iget v5, p0, Ledu;->bKO:I

    iget v6, p0, Ledu;->bKP:I

    iput-boolean v1, v0, Lehj;->bZJ:Z

    iput v2, v0, Lehj;->bWq:I

    iput v3, v0, Lehj;->bWr:I

    iput v4, v0, Lehj;->bWs:I

    iput v5, v0, Lehj;->bKO:I

    iput v6, v0, Lehj;->bKP:I

    return-object v0
.end method

.method public b(ILcom/google/android/shared/search/Suggestion;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 216
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 218
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iput p1, p0, Ledu;->bVZ:I

    .line 221
    iget-object v0, p0, Ledu;->bVX:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    iget-object v0, p0, Ledu;->bVX:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Ledu;->bWa:I

    .line 230
    :goto_1
    iget-object v0, p0, Ledu;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledu;->bWb:Ljava/lang/String;

    .line 232
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWj:J

    .line 233
    return-void

    .line 216
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 226
    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Ledu;->bWa:I

    .line 228
    const-string v0, "SearchFormulationLogging"

    const-string v1, "Suggestion interacted with, was not registered as shown."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final eN(Z)Ledu;
    .locals 1

    .prologue
    .line 275
    if-eqz p1, :cond_0

    .line 276
    iget v0, p0, Ledu;->bWs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ledu;->bWs:I

    .line 283
    :goto_0
    return-object p0

    .line 278
    :cond_0
    iget v0, p0, Ledu;->bWr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ledu;->bWr:I

    goto :goto_0
.end method

.method public final n(Lcom/google/android/shared/search/Suggestion;)Ledu;
    .locals 2
    .param p1    # Lcom/google/android/shared/search/Suggestion;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 253
    invoke-direct {p0}, Ledu;->anT()J

    move-result-wide v0

    iput-wide v0, p0, Ledu;->bWl:J

    .line 254
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Ledu;->b(ILcom/google/android/shared/search/Suggestion;)V

    .line 257
    return-object p0
.end method
