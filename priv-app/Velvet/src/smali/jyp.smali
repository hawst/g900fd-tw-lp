.class public final Ljyp;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eLT:J

.field private eLY:J

.field private eLZ:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 87
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Ljyp;->aez:I

    iput-wide v2, p0, Ljyp;->eLY:J

    iput-wide v2, p0, Ljyp;->eLT:J

    iput-wide v2, p0, Ljyp;->eLZ:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljyp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyp;->eCz:I

    .line 89
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljyp;->eLY:J

    iget v0, p0, Ljyp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljyp;->eLT:J

    iget v0, p0, Ljyp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyp;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljyp;->eLZ:J

    iget v0, p0, Ljyp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyp;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 104
    iget v0, p0, Ljyp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iget-wide v2, p0, Ljyp;->eLY:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 107
    :cond_0
    iget v0, p0, Ljyp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 108
    const/4 v0, 0x2

    iget-wide v2, p0, Ljyp;->eLT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 110
    :cond_1
    iget v0, p0, Ljyp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 111
    const/4 v0, 0x3

    iget-wide v2, p0, Ljyp;->eLZ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 113
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 114
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 118
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 119
    iget v1, p0, Ljyp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 120
    const/4 v1, 0x1

    iget-wide v2, p0, Ljyp;->eLY:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_0
    iget v1, p0, Ljyp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 124
    const/4 v1, 0x2

    iget-wide v2, p0, Ljyp;->eLT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_1
    iget v1, p0, Ljyp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 128
    const/4 v1, 0x3

    iget-wide v2, p0, Ljyp;->eLZ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_2
    return v0
.end method
