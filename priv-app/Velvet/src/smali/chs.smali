.class public final Lchs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhgm;


# static fields
.field private static final aXo:Landroid/net/Uri;


# instance fields
.field private final aXk:Lcyg;

.field private final aXp:Ljava/lang/String;

.field private aXq:Lcyh;

.field private aXr:Lcyh;

.field private final mContext:Landroid/content/Context;

.field private final mMainPrefs:Lcyg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "content://com.google.android.voicesearch/prefs"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lchs;->aXo:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcyg;Lcyg;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lchs;->mContext:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lchs;->aXk:Lcyg;

    .line 83
    iput-object p3, p0, Lchs;->mMainPrefs:Lcyg;

    .line 84
    iput-object p4, p0, Lchs;->aXp:Ljava/lang/String;

    .line 85
    return-void
.end method

.method private Kw()V
    .locals 6

    .prologue
    .line 196
    :try_start_0
    iget-object v0, p0, Lchs;->mMainPrefs:Lcyg;

    const-string v1, "settings_upgraded"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "settings_upgraded"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 210
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Lchs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lchs;->aXo:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 206
    invoke-static {p0, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v0

    const-string v0, "Search.GsaPreferenceUpgrader"

    const-string v1, "Error during voice search settings upgrade."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcyg;Lcyg;Ljava/lang/String;II)V
    .locals 8

    .prologue
    .line 75
    new-instance v2, Lchs;

    invoke-direct {v2, p0, p1, p2, p3}, Lchs;-><init>(Landroid/content/Context;Lcyg;Lcyg;Ljava/lang/String;)V

    const/16 v3, 0xa

    if-ge p4, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    const/4 v0, 0x1

    invoke-static {v0}, Lifv;->gX(Z)V

    iget-object v0, v2, Lchs;->aXk:Lcyg;

    invoke-interface {v0}, Lcyg;->EI()V

    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    invoke-interface {v0}, Lcyg;->EI()V

    :try_start_0
    iget-object v0, v2, Lchs;->aXk:Lcyg;

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    iput-object v0, v2, Lchs;->aXq:Lcyh;

    if-gtz p4, :cond_0

    const-string v0, "StartupSettings"

    invoke-direct {v2, v0}, Lchs;->gr(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v2, Lchs;->aXp:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v4, v0, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v4, 0x2

    if-gt v0, v4, :cond_0

    iget-object v4, v2, Lchs;->aXq:Lcyh;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lchu;

    invoke-direct {v5, v2, v4}, Lchu;-><init>(Lchs;Lcyh;)V

    invoke-static {v1, v5}, Lchs;->a(Ljava/util/Map;Lchv;)V

    move p4, v0

    :cond_0
    if-gtz p4, :cond_5

    invoke-direct {v2}, Lchs;->Kw()V

    const-string v0, "AlarmUtils"

    invoke-direct {v2, v0}, Lchs;->gr(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-string v5, "_StartTimeMillis"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    instance-of v5, v0, Ljava/lang/Long;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AlarmStartTimeMillis_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x10

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v5

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v5, v1, v6, v7}, Lcyh;->c(Ljava/lang/String;J)Lcyh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, v2, Lchs;->aXk:Lcyg;

    invoke-interface {v1}, Lcyg;->EJ()V

    iget-object v1, v2, Lchs;->mMainPrefs:Lcyg;

    invoke-interface {v1}, Lcyg;->EJ()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    :try_start_1
    const-string v0, "PredictiveCardsOptInSettings"

    invoke-direct {v2, v0}, Lchs;->gr(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-direct {v2, v0}, Lchs;->d(Ljava/util/Map;)Lchv;

    move-result-object v1

    invoke-static {v0, v1}, Lchs;->a(Ljava/util/Map;Lchv;)V

    :cond_4
    const-string v0, "com.google.android.googlequicksearchbox_preferences"

    invoke-direct {v2, v0}, Lchs;->gr(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-direct {v2, v0}, Lchs;->d(Ljava/util/Map;)Lchv;

    move-result-object v1

    invoke-static {v0, v1}, Lchs;->a(Ljava/util/Map;Lchv;)V

    :cond_5
    const/4 v0, 0x2

    if-ge p4, v0, :cond_6

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "gservices_overrides"

    iget-object v4, v2, Lchs;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lchx;->ah(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    :cond_6
    const/4 v0, 0x3

    if-ge p4, v0, :cond_7

    const-string v0, "SearchSettings"

    invoke-direct {v2, v0}, Lchs;->gr(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-direct {v2, v0}, Lchs;->d(Ljava/util/Map;)Lchv;

    move-result-object v1

    invoke-static {v0, v1}, Lchs;->a(Ljava/util/Map;Lchv;)V

    :cond_7
    iget-object v0, v2, Lchs;->aXq:Lcyh;

    invoke-interface {v0}, Lcyh;->apply()V

    iget-object v0, v2, Lchs;->aXr:Lcyh;

    if-eqz v0, :cond_8

    iget-object v0, v2, Lchs;->aXr:Lcyh;

    invoke-interface {v0}, Lcyh;->apply()V

    :cond_8
    const/4 v0, 0x5

    if-ge p4, v0, :cond_9

    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v1, "personalized_search"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v1

    const-string v4, "personalized_search_bool"

    const-string v5, "0"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_2
    invoke-interface {v1, v4, v0}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    :cond_9
    const/4 v0, 0x6

    if-ge p4, v0, :cond_a

    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v1, "web_corpora_config_url"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "web_corpora_config"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    :cond_a
    const/16 v0, 0x8

    if-ge p4, v0, :cond_b

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "need_source_stats_upgrade"

    const/4 v4, 0x1

    invoke-interface {v0, v1, v4}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    :cond_b
    const/16 v0, 0x9

    if-ge p4, v0, :cond_d

    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v1, "promo_card_dismissed"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    sget-object v1, Lgxd;->cYe:Lgxd;

    invoke-virtual {v1}, Lgxd;->aLm()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v0, v1, v4}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    :cond_c
    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v1, "swipe_card_dismissed"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "card_swiped_for_dismiss"

    const/4 v4, 0x1

    invoke-interface {v0, v1, v4}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    :cond_d
    const/16 v0, 0xa

    if-ge p4, v0, :cond_10

    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v1, "safe_search"

    invoke-interface {v0, v1}, Lcyg;->contains(Ljava/lang/String;)Z

    move-result v0

    iget-object v1, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v4, "safe_search_settings"

    sget-object v5, Lcpn;->bfc:Ljava/lang/String;

    invoke-interface {v1, v4, v5}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcpn;->bfb:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    if-nez v0, :cond_e

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v1

    const-string v4, "safe_search"

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    :cond_e
    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v1

    const-string v4, "safe_search_settings"

    invoke-interface {v1, v4}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    iget-object v1, v2, Lchs;->mMainPrefs:Lcyg;

    const-string v4, "safe_search_bimodal"

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_f

    if-nez v0, :cond_f

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "safe_search"

    const/4 v4, 0x1

    invoke-interface {v0, v1, v4}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    :cond_f
    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "safe_search_bimodal"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    :cond_10
    invoke-virtual {v2}, Lchs;->Kx()Lcyh;

    move-result-object v0

    iget-object v1, v2, Lchs;->aXp:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    iget-object v0, v2, Lchs;->aXq:Lcyh;

    invoke-interface {v0}, Lcyh;->apply()V

    const/4 v0, 0x0

    iput-object v0, v2, Lchs;->aXq:Lcyh;

    iget-object v0, v2, Lchs;->aXr:Lcyh;

    if-eqz v0, :cond_11

    iget-object v0, v2, Lchs;->aXr:Lcyh;

    invoke-interface {v0}, Lcyh;->apply()V

    const/4 v0, 0x0

    iput-object v0, v2, Lchs;->aXr:Lcyh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_11
    iget-object v0, v2, Lchs;->aXk:Lcyg;

    invoke-interface {v0}, Lcyg;->EJ()V

    iget-object v0, v2, Lchs;->mMainPrefs:Lcyg;

    invoke-interface {v0}, Lcyg;->EJ()V

    return-void

    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method private static a(Ljava/util/Map;Lchv;)V
    .locals 6

    .prologue
    .line 378
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 379
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 380
    invoke-interface {p1, v1}, Lchv;->gs(Ljava/lang/String;)Lcyh;

    move-result-object v3

    .line 381
    if-eqz v3, :cond_0

    .line 383
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 387
    instance-of v4, v0, Ljava/lang/Boolean;

    if-eqz v4, :cond_1

    .line 389
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v3, v1, v0}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    goto :goto_0

    .line 390
    :cond_1
    instance-of v4, v0, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 392
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v3, v1, v0}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    goto :goto_0

    .line 393
    :cond_2
    instance-of v4, v0, Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 395
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v3, v1, v4, v5}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    goto :goto_0

    .line 396
    :cond_3
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 398
    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v1, v0}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    goto :goto_0

    .line 399
    :cond_4
    instance-of v4, v0, Ljava/util/Set;

    if-eqz v4, :cond_5

    .line 401
    check-cast v0, Ljava/util/Set;

    invoke-interface {v3, v1, v0}, Lcyh;->b(Ljava/lang/String;Ljava/util/Set;)Lcyh;

    goto :goto_0

    .line 402
    :cond_5
    instance-of v4, v0, Ljava/lang/Float;

    if-eqz v4, :cond_0

    .line 404
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-interface {v3, v1, v0}, Lcyh;->b(Ljava/lang/String;F)Lcyh;

    goto :goto_0

    .line 409
    :cond_6
    return-void
.end method

.method private d(Ljava/util/Map;)Lchv;
    .locals 1

    .prologue
    .line 296
    new-instance v0, Lcht;

    invoke-direct {v0, p0, p1}, Lcht;-><init>(Lchs;Ljava/util/Map;)V

    return-object v0
.end method

.method private gr(Ljava/lang/String;)Ljava/util/Map;
    .locals 4

    .prologue
    .line 412
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lchs;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    const-string v2, "shared_prefs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 414
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".bak"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 415
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 416
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 419
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    :try_start_0
    iget-object v1, p0, Lchs;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 427
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-object v0, v1

    .line 430
    :goto_1
    return-object v0

    .line 427
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    throw v1

    .line 430
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method final Kx()Lcyh;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lchs;->aXq:Lcyh;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    iget-object v0, p0, Lchs;->aXq:Lcyh;

    return-object v0
.end method

.method final Ky()Lcyh;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lchs;->aXr:Lcyh;

    if-nez v0, :cond_0

    .line 367
    iget-object v0, p0, Lchs;->mMainPrefs:Lcyg;

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    iput-object v0, p0, Lchs;->aXr:Lcyh;

    .line 369
    :cond_0
    iget-object v0, p0, Lchs;->aXr:Lcyh;

    return-object v0
.end method

.method public final h(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 435
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 436
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 438
    const-string v2, "profanityFilter"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 440
    invoke-virtual {p0}, Lchs;->Ky()Lcyh;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-interface {v2, v0, v1}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    const-string v2, "actual_language_setting"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 447
    invoke-virtual {p0}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v2, "spoken-language-bcp-47"

    invoke-interface {v0, v2, v1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    goto :goto_0

    .line 448
    :cond_2
    const-string v2, "pref-voice-personalization-status"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 452
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 453
    invoke-virtual {p0}, Lchs;->Ky()Lcyh;

    move-result-object v0

    const-string v1, "personalizedResults"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
