.class public final Ljoq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewK:[Ljoq;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dQT:Z

.field public ewL:[Ljoj;

.field public ewM:Ljop;

.field private ewN:Z

.field private ewO:I

.field private ewP:Ljava/lang/String;

.field private ewQ:Ljava/lang/String;

.field public ewR:Ljpe;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 171
    iput v1, p0, Ljoq;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljoq;->agq:Ljava/lang/String;

    invoke-static {}, Ljoj;->brj()[Ljoj;

    move-result-object v0

    iput-object v0, p0, Ljoq;->ewL:[Ljoj;

    iput-object v2, p0, Ljoq;->ewM:Ljop;

    iput-boolean v1, p0, Ljoq;->dQT:Z

    iput-boolean v1, p0, Ljoq;->ewN:Z

    iput v1, p0, Ljoq;->ewO:I

    const-string v0, ""

    iput-object v0, p0, Ljoq;->ewP:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljoq;->ewQ:Ljava/lang/String;

    iput-object v2, p0, Ljoq;->ewR:Ljpe;

    iput-object v2, p0, Ljoq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljoq;->eCz:I

    .line 172
    return-void
.end method

.method public static brt()[Ljoq;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Ljoq;->ewK:[Ljoq;

    if-nez v0, :cond_1

    .line 26
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 28
    :try_start_0
    sget-object v0, Ljoq;->ewK:[Ljoq;

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Ljoq;

    sput-object v0, Ljoq;->ewK:[Ljoq;

    .line 31
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Ljoq;->ewK:[Ljoq;

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljoq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoq;->agq:Ljava/lang/String;

    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoq;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoq;->ewL:[Ljoj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljoj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljoq;->ewL:[Ljoj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljoj;

    invoke-direct {v3}, Ljoj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljoq;->ewL:[Ljoj;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljoj;

    invoke-direct {v3}, Ljoj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoq;->ewL:[Ljoj;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljoq;->ewM:Ljop;

    if-nez v0, :cond_4

    new-instance v0, Ljop;

    invoke-direct {v0}, Ljop;-><init>()V

    iput-object v0, p0, Ljoq;->ewM:Ljop;

    :cond_4
    iget-object v0, p0, Ljoq;->ewM:Ljop;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljoq;->dQT:Z

    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoq;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljoq;->ewN:Z

    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljoq;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoq;->ewP:Ljava/lang/String;

    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljoq;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoq;->ewQ:Ljava/lang/String;

    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljoq;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljoq;->ewR:Ljpe;

    if-nez v0, :cond_5

    new-instance v0, Ljpe;

    invoke-direct {v0}, Ljpe;-><init>()V

    iput-object v0, p0, Ljoq;->ewR:Ljpe;

    :cond_5
    iget-object v0, p0, Ljoq;->ewR:Ljpe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljoq;->ewO:I

    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljoq;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 193
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 194
    const/4 v0, 0x1

    iget-object v1, p0, Ljoq;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 196
    :cond_0
    iget-object v0, p0, Ljoq;->ewL:[Ljoj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljoq;->ewL:[Ljoj;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 197
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljoq;->ewL:[Ljoj;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 198
    iget-object v1, p0, Ljoq;->ewL:[Ljoj;

    aget-object v1, v1, v0

    .line 199
    if-eqz v1, :cond_1

    .line 200
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 197
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_2
    iget-object v0, p0, Ljoq;->ewM:Ljop;

    if-eqz v0, :cond_3

    .line 205
    const/4 v0, 0x3

    iget-object v1, p0, Ljoq;->ewM:Ljop;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 207
    :cond_3
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 208
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljoq;->dQT:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 210
    :cond_4
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 211
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljoq;->ewN:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 213
    :cond_5
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 214
    const/4 v0, 0x6

    iget-object v1, p0, Ljoq;->ewP:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 216
    :cond_6
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 217
    const/4 v0, 0x7

    iget-object v1, p0, Ljoq;->ewQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 219
    :cond_7
    iget-object v0, p0, Ljoq;->ewR:Ljpe;

    if-eqz v0, :cond_8

    .line 220
    const/16 v0, 0x8

    iget-object v1, p0, Ljoq;->ewR:Ljpe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 222
    :cond_8
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_9

    .line 223
    const/16 v0, 0x9

    iget v1, p0, Ljoq;->ewO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 225
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 226
    return-void
.end method

.method public final bru()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Ljoq;->dQT:Z

    return v0
.end method

.method public final brv()Z
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brw()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Ljoq;->ewN:Z

    return v0
.end method

.method public final brx()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Ljoq;->ewO:I

    return v0
.end method

.method public final bry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Ljoq;->ewP:Ljava/lang/String;

    return-object v0
.end method

.method public final brz()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Ljoq;->ewQ:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ljoq;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final iO(Z)Ljoq;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljoq;->dQT:Z

    .line 73
    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoq;->aez:I

    .line 74
    return-object p0
.end method

.method public final iP(Z)Ljoq;
    .locals 1

    .prologue
    .line 91
    iput-boolean p1, p0, Ljoq;->ewN:Z

    .line 92
    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljoq;->aez:I

    .line 93
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 230
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 231
    iget v1, p0, Ljoq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 232
    const/4 v1, 0x1

    iget-object v2, p0, Ljoq;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_0
    iget-object v1, p0, Ljoq;->ewL:[Ljoj;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljoq;->ewL:[Ljoj;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 236
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljoq;->ewL:[Ljoj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 237
    iget-object v2, p0, Ljoq;->ewL:[Ljoj;

    aget-object v2, v2, v0

    .line 238
    if-eqz v2, :cond_1

    .line 239
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 236
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 244
    :cond_3
    iget-object v1, p0, Ljoq;->ewM:Ljop;

    if-eqz v1, :cond_4

    .line 245
    const/4 v1, 0x3

    iget-object v2, p0, Ljoq;->ewM:Ljop;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_4
    iget v1, p0, Ljoq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 249
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljoq;->dQT:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 252
    :cond_5
    iget v1, p0, Ljoq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 253
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljoq;->ewN:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 256
    :cond_6
    iget v1, p0, Ljoq;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 257
    const/4 v1, 0x6

    iget-object v2, p0, Ljoq;->ewP:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_7
    iget v1, p0, Ljoq;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_8

    .line 261
    const/4 v1, 0x7

    iget-object v2, p0, Ljoq;->ewQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_8
    iget-object v1, p0, Ljoq;->ewR:Ljpe;

    if-eqz v1, :cond_9

    .line 265
    const/16 v1, 0x8

    iget-object v2, p0, Ljoq;->ewR:Ljpe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_9
    iget v1, p0, Ljoq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_a

    .line 269
    const/16 v1, 0x9

    iget v2, p0, Ljoq;->ewO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_a
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Ljoq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ra(I)Ljoq;
    .locals 1

    .prologue
    .line 110
    iput p1, p0, Ljoq;->ewO:I

    .line 111
    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljoq;->aez:I

    .line 112
    return-object p0
.end method

.method public final xN(Ljava/lang/String;)Ljoq;
    .locals 1

    .prologue
    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47
    :cond_0
    iput-object p1, p0, Ljoq;->agq:Ljava/lang/String;

    .line 48
    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoq;->aez:I

    .line 49
    return-object p0
.end method

.method public final xO(Ljava/lang/String;)Ljoq;
    .locals 1

    .prologue
    .line 129
    if-nez p1, :cond_0

    .line 130
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 132
    :cond_0
    iput-object p1, p0, Ljoq;->ewP:Ljava/lang/String;

    .line 133
    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljoq;->aez:I

    .line 134
    return-object p0
.end method

.method public final xP(Ljava/lang/String;)Ljoq;
    .locals 1

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 152
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 154
    :cond_0
    iput-object p1, p0, Ljoq;->ewQ:Ljava/lang/String;

    .line 155
    iget v0, p0, Ljoq;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljoq;->aez:I

    .line 156
    return-object p0
.end method
