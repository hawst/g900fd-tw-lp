.class final Lmf;
.super Lkx;
.source "PG"


# instance fields
.field private lA:I

.field private synthetic lt:Lmb;

.field private final lv:Ljava/lang/String;

.field private lw:Z

.field private lx:I

.field private ly:I

.field private lz:Lmc;


# direct methods
.method public constructor <init>(Lmb;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Lmf;->lt:Lmb;

    invoke-direct {p0}, Lkx;-><init>()V

    .line 298
    const/4 v0, -0x1

    iput v0, p0, Lmf;->lx:I

    .line 305
    iput-object p2, p0, Lmf;->lv:Ljava/lang/String;

    .line 306
    return-void
.end method


# virtual methods
.method public final V(I)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lmf;->lz:Lmc;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lmf;->lz:Lmc;

    iget v1, p0, Lmf;->lA:I

    invoke-virtual {v0, v1, p1}, Lmc;->k(II)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    iput p1, p0, Lmf;->lx:I

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Lmf;->ly:I

    goto :goto_0
.end method

.method public final W(I)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lmf;->lz:Lmc;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lmf;->lz:Lmc;

    iget v1, p0, Lmf;->lA:I

    invoke-virtual {v0, v1, p1}, Lmc;->l(II)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, Lmf;->ly:I

    add-int/2addr v0, p1

    iput v0, p0, Lmf;->ly:I

    goto :goto_0
.end method

.method public final b(Lmc;)V
    .locals 2

    .prologue
    .line 309
    iput-object p1, p0, Lmf;->lz:Lmc;

    .line 310
    iget-object v0, p0, Lmf;->lv:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lmc;->m(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lmf;->lA:I

    .line 311
    iget-boolean v0, p0, Lmf;->lw:Z

    if-eqz v0, :cond_1

    .line 312
    iget v0, p0, Lmf;->lA:I

    invoke-virtual {p1, v0}, Lmc;->aa(I)V

    .line 313
    iget v0, p0, Lmf;->lx:I

    if-ltz v0, :cond_0

    .line 314
    iget v0, p0, Lmf;->lA:I

    iget v1, p0, Lmf;->lx:I

    invoke-virtual {p1, v0, v1}, Lmc;->k(II)V

    .line 315
    const/4 v0, -0x1

    iput v0, p0, Lmf;->lx:I

    .line 317
    :cond_0
    iget v0, p0, Lmf;->ly:I

    if-eqz v0, :cond_1

    .line 318
    iget v0, p0, Lmf;->lA:I

    iget v1, p0, Lmf;->ly:I

    invoke-virtual {p1, v0, v1}, Lmc;->l(II)V

    .line 319
    const/4 v0, 0x0

    iput v0, p0, Lmf;->ly:I

    .line 322
    :cond_1
    return-void
.end method

.method public final bD()V
    .locals 2

    .prologue
    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmf;->lw:Z

    .line 340
    iget-object v0, p0, Lmf;->lz:Lmc;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lmf;->lz:Lmc;

    iget v1, p0, Lmf;->lA:I

    invoke-virtual {v0, v1}, Lmc;->aa(I)V

    .line 343
    :cond_0
    return-void
.end method

.method public final bE()V
    .locals 2

    .prologue
    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmf;->lw:Z

    .line 348
    iget-object v0, p0, Lmf;->lz:Lmc;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lmf;->lz:Lmc;

    iget v1, p0, Lmf;->lA:I

    invoke-virtual {v0, v1}, Lmc;->ab(I)V

    .line 351
    :cond_0
    return-void
.end method

.method public final cb()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lmf;->lz:Lmc;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lmf;->lz:Lmc;

    iget v1, p0, Lmf;->lA:I

    invoke-virtual {v0, v1}, Lmc;->Z(I)V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lmf;->lz:Lmc;

    .line 328
    const/4 v0, 0x0

    iput v0, p0, Lmf;->lA:I

    .line 330
    :cond_0
    return-void
.end method

.method public final onRelease()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lmf;->lt:Lmb;

    invoke-static {v0, p0}, Lmb;->a(Lmb;Lmf;)V

    .line 335
    return-void
.end method
