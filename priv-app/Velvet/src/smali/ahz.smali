.class public Lahz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private YI:Landroid/os/UserHandle;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/os/UserHandle;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lahz;->YI:Landroid/os/UserHandle;

    .line 30
    return-void
.end method

.method static a(Landroid/os/UserHandle;)Lahz;
    .locals 1

    .prologue
    .line 44
    if-nez p0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lahz;

    invoke-direct {v0, p0}, Lahz;-><init>(Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method public static lN()Lahz;
    .locals 2

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 37
    new-instance v0, Lahz;

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    invoke-direct {v0, v1}, Lahz;-><init>(Landroid/os/UserHandle;)V

    .line 39
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lahz;

    invoke-direct {v0}, Lahz;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahz;->YI:Landroid/os/UserHandle;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lahz;->YI:Landroid/os/UserHandle;

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 94
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 66
    instance-of v0, p1, Lahz;

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 69
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 70
    iget-object v0, p0, Lahz;->YI:Landroid/os/UserHandle;

    check-cast p1, Lahz;

    iget-object v1, p1, Lahz;->YI:Landroid/os/UserHandle;

    invoke-virtual {v0, v1}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 72
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final getUser()Landroid/os/UserHandle;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lahz;->YI:Landroid/os/UserHandle;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 78
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 79
    iget-object v0, p0, Lahz;->YI:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->hashCode()I

    move-result v0

    .line 81
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 58
    iget-object v0, p0, Lahz;->YI:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
