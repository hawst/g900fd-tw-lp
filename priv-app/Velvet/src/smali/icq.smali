.class public abstract Licq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

.field mAppSelectionHelper:Libo;


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object p1, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    .line 227
    iput-object p2, p0, Licq;->mAppSelectionHelper:Libo;

    .line 228
    return-void
.end method


# virtual methods
.method protected aUN()Lijj;
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    invoke-virtual {v0}, Ljmh;->bpF()Ljava/lang/String;

    move-result-object v0

    .line 367
    if-nez v0, :cond_0

    .line 368
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 370
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Licq;->mAppSelectionHelper:Libo;

    invoke-virtual {p0, v0}, Licq;->oX(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Libo;->an(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(Ljmh;)I
    .locals 1

    .prologue
    .line 253
    invoke-virtual {p1}, Ljmh;->bpO()I

    move-result v0

    return v0
.end method

.method protected gU(Z)Lcom/google/android/shared/util/App;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 280
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 282
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v6

    .line 286
    iget-object v0, p0, Licq;->mAppSelectionHelper:Libo;

    iget-object v3, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/PlayMediaAction;->getMimeType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v4}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Libo;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/shared/util/App;

    move-result-object v3

    .line 289
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahP()Lcom/google/android/shared/util/App;

    move-result-object v0

    .line 290
    iget-object v4, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v4}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahS()Lcom/google/android/shared/util/App;

    move-result-object v4

    .line 291
    if-eqz v0, :cond_3

    if-ne v3, v0, :cond_3

    move v5, v1

    .line 292
    :goto_1
    if-eqz v4, :cond_4

    if-ne v3, v4, :cond_4

    .line 297
    :goto_2
    invoke-virtual {p0, v6}, Licq;->c(Ljmh;)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 338
    :cond_0
    if-eqz v3, :cond_7

    move-object v0, v3

    .line 349
    :cond_1
    :goto_3
    return-object v0

    :cond_2
    move v0, v2

    .line 280
    goto :goto_0

    :cond_3
    move v5, v2

    .line 291
    goto :goto_1

    :cond_4
    move v1, v2

    .line 292
    goto :goto_2

    .line 299
    :pswitch_0
    invoke-static {v6}, Licp;->b(Ljmh;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    goto :goto_3

    .line 308
    :pswitch_1
    const/16 v1, 0xff

    invoke-static {v1}, Lege;->ht(I)V

    .line 314
    :pswitch_2
    if-eqz v3, :cond_5

    if-eqz v5, :cond_0

    :cond_5
    if-eqz v4, :cond_0

    move-object v0, v4

    .line 319
    goto :goto_3

    .line 326
    :pswitch_3
    invoke-static {v6}, Licp;->b(Ljmh;)Z

    move-result v4

    if-nez v4, :cond_6

    if-eqz v1, :cond_0

    :cond_6
    if-eqz v0, :cond_0

    goto :goto_3

    .line 346
    :cond_7
    if-nez p1, :cond_8

    if-nez v0, :cond_1

    :cond_8
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    goto :goto_3

    .line 297
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract oX(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract oY(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract oZ(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method protected pc(Ljava/lang/String;)Lijj;
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Licq;->mAppSelectionHelper:Libo;

    invoke-virtual {p0, p1}, Licq;->oZ(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Libo;->an(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method
