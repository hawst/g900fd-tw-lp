.class public final Lfws;
.super Lfuz;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 48
    iput-object p4, p0, Lfws;->mPhotoWithAttributionDecorator:Lgbd;

    .line 49
    return-void
.end method

.method private static a(Lizj;Lixx;Z)Lanh;
    .locals 3

    .prologue
    .line 153
    new-instance v0, Laoj;

    invoke-direct {v0}, Laoj;-><init>()V

    invoke-virtual {p1}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    move-result-object v0

    .line 156
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 157
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    .line 158
    iput-object p0, v1, Lanh;->ahu:Lizj;

    .line 159
    iput-object v0, v1, Lanh;->agz:Laoj;

    .line 160
    new-instance v0, Lfsa;

    invoke-virtual {p1}, Lixx;->oY()I

    move-result v2

    invoke-direct {v0, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0201ba

    invoke-virtual {v0, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v1, Lanh;->ahs:Lani;

    .line 163
    invoke-virtual {v1, p2}, Lanh;->aW(Z)Lanh;

    .line 164
    invoke-virtual {v1, p2}, Lanh;->aX(Z)Lanh;

    .line 166
    return-object v1
.end method

.method public static a(Lizj;Ljhh;)Lanh;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p1}, Ljhh;->blK()Ljava/lang/String;

    move-result-object v0

    .line 201
    new-instance v1, Lany;

    invoke-direct {v1}, Lany;-><init>()V

    .line 202
    invoke-virtual {p1}, Ljhh;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 203
    invoke-virtual {v1, v0}, Lany;->bk(Ljava/lang/String;)Lany;

    .line 204
    invoke-virtual {p1}, Ljhh;->oo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lany;->bj(Ljava/lang/String;)Lany;

    .line 206
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 207
    const/16 v2, 0x33

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 208
    iput-object v1, v0, Lanh;->agw:Lany;

    .line 209
    iput-object p0, v0, Lanh;->ahu:Lizj;

    .line 210
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lizj;Z)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 77
    iget-object v0, p1, Lizj;->dTF:Ljhh;

    iget-object v0, v0, Ljhh;->elH:[Ljhi;

    .line 78
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_1

    .line 79
    :cond_0
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    .line 82
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 84
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    :cond_2
    iget-object v0, p1, Lizj;->dTF:Ljhh;

    iget-object v5, v0, Ljhh;->elH:[Ljhi;

    array-length v6, v5

    :goto_2
    if-ge v3, v6, :cond_9

    aget-object v7, v5, v3

    .line 87
    iget-object v0, v7, Ljhi;->elM:Lixx;

    if-nez v0, :cond_5

    move-object v0, v1

    .line 91
    :goto_3
    if-nez v0, :cond_3

    .line 92
    iget-object v0, v7, Ljhi;->elN:Lixx;

    if-nez v0, :cond_8

    move-object v0, v1

    .line 95
    :cond_3
    :goto_4
    if-eqz v0, :cond_4

    .line 96
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v7, 0x2

    if-eq v0, v7, :cond_9

    .line 100
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 87
    :cond_5
    invoke-virtual {v0}, Lixx;->avd()Z

    move-result v8

    if-nez v8, :cond_6

    move-object v0, v1

    goto :goto_3

    :cond_6
    invoke-virtual {v0}, Lixx;->bbe()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    move-object v0, v1

    goto :goto_3

    :cond_7
    invoke-static {p1, v0, p2}, Lfws;->a(Lizj;Lixx;Z)Lanh;

    move-result-object v0

    goto :goto_3

    .line 92
    :cond_8
    invoke-static {p1, v0, p2}, Lfws;->a(Lizj;Lixx;Z)Lanh;

    move-result-object v0

    goto :goto_4

    :cond_9
    move-object v0, v2

    .line 104
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x0

    .line 53
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    .line 55
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 56
    invoke-virtual {p0, p1, v2}, Lfws;->f(Landroid/content/Context;Lizj;)Lanh;

    move-result-object v0

    .line 57
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    invoke-virtual {v0}, Lanh;->oV()Z

    move-result v4

    .line 59
    iget-object v5, v2, Lizj;->dTF:Ljhh;

    invoke-virtual {v5}, Ljhh;->bfL()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-static {v3, v0}, Lfws;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 61
    invoke-static {p1, v2, v4}, Lfws;->a(Landroid/content/Context;Lizj;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 62
    iget-object v0, v2, Lizj;->dTF:Ljhh;

    invoke-static {v2, v0}, Lfws;->a(Lizj;Ljhh;)Lanh;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    iget-object v2, v2, Lizj;->dTF:Ljhh;

    new-instance v0, Laoj;

    invoke-direct {v0}, Laoj;-><init>()V

    const v4, 0x7f0a0152

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    invoke-virtual {v4, v8}, Lanh;->cm(I)Lanh;

    iget-object v5, p0, Lfuz;->mEntry:Lizj;

    iput-object v5, v4, Lanh;->ahu:Lizj;

    iput-object v0, v4, Lanh;->agz:Laoj;

    new-instance v0, Lfsa;

    const/16 v5, 0x14

    invoke-direct {v0, v5}, Lfsa;-><init>(I)V

    const v5, 0x7f0200ed

    invoke-virtual {v0, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljhh;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljhh;->blJ()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Ljhh;->blI()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v1, v1}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v4, Lanh;->ahs:Lani;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 66
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 67
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 68
    return-object v1

    .line 59
    :cond_1
    new-instance v6, Laoj;

    invoke-direct {v6}, Laoj;-><init>()V

    const v0, 0x7f0a035e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    invoke-virtual {v0, v8}, Lanh;->cm(I)Lanh;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    iput-object v6, v0, Lanh;->agz:Laoj;

    new-instance v6, Lfsa;

    const/16 v7, 0x4a

    invoke-direct {v6, v7}, Lfsa;-><init>(I)V

    const v7, 0x7f0201ba

    invoke-virtual {v6, v7}, Lfsa;->jB(I)Lfsa;

    move-result-object v6

    invoke-virtual {v5}, Ljhh;->bfK()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v5

    iput-object v5, v0, Lanh;->ahs:Lani;

    invoke-virtual {v0, v4}, Lanh;->aW(Z)Lanh;

    invoke-virtual {v0, v4}, Lanh;->aX(Z)Lanh;

    goto/16 :goto_0

    .line 63
    :pswitch_0
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0a0429

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0a0428

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f(Landroid/content/Context;Lizj;)Lanh;
    .locals 4

    .prologue
    .line 182
    iget-object v1, p2, Lizj;->dTF:Ljhh;

    .line 183
    iget-object v0, v1, Ljhh;->eeb:[Ljcn;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, v1, Ljhh;->eeb:[Ljcn;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 184
    :goto_0
    new-instance v2, Lapp;

    invoke-direct {v2}, Lapp;-><init>()V

    .line 185
    invoke-virtual {v1}, Ljhh;->ol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lapp;->dy(Ljava/lang/String;)Lapp;

    .line 186
    invoke-virtual {v1}, Ljhh;->ol()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lapp;->dx(Ljava/lang/String;)Lapp;

    .line 187
    invoke-virtual {v1}, Ljhh;->rE()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lapp;->dw(Ljava/lang/String;)Lapp;

    .line 188
    invoke-virtual {v1}, Ljhh;->rG()Z

    move-result v3

    invoke-virtual {v2, v3}, Lapp;->bl(Z)Lapp;

    .line 189
    new-instance v3, Lfrs;

    invoke-virtual {v1}, Ljhh;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, p1, v1, p2}, Lfrs;-><init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V

    iput-object v2, v3, Lfrs;->cBl:Lapp;

    const/4 v1, 0x1

    iput-boolean v1, v3, Lfrs;->cBh:Z

    iget-object v1, p0, Lfws;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-virtual {v3, v0, v1}, Lfrs;->a(Ljcn;Lgbd;)Lfrs;

    move-result-object v0

    invoke-virtual {v0}, Lfrs;->aDh()Lanh;

    move-result-object v0

    return-object v0

    .line 183
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
