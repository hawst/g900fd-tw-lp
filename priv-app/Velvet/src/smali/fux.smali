.class public final Lfux;
.super Lfuz;
.source "PG"


# instance fields
.field private cte:Ljbp;

.field private mCalendarData:Lamk;

.field private mFrequentPlaceEntry:Ljal;

.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 59
    iput-object p4, p0, Lfux;->mTimeToLeaveFactory:Lfyk;

    .line 60
    iput-object p5, p0, Lfux;->mStringEvaluator:Lgbr;

    .line 61
    return-void
.end method

.method private aDw()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSf:Lixr;

    invoke-virtual {v0}, Lixr;->baJ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSf:Lixr;

    invoke-virtual {v0}, Lixr;->baI()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSf:Lixr;

    invoke-virtual {v0}, Lixr;->baE()Z

    move-result v0

    goto :goto_0
.end method

.method private aZ(Landroid/content/Context;)Lanh;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 181
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    .line 182
    iget-object v2, v1, Lizj;->dSf:Lixr;

    .line 183
    iget-object v3, p0, Lfux;->cte:Ljbp;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lixr;->dMF:[Liyg;

    array-length v3, v3

    if-nez v3, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-object v0

    .line 187
    :cond_1
    new-instance v3, Lgca;

    iget-object v2, v2, Lixr;->dMF:[Liyg;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v4, p0, Lfuz;->mClock:Lemp;

    invoke-direct {v3, v2, v4}, Lgca;-><init>(Liyg;Lemp;)V

    .line 188
    invoke-virtual {v3}, Lgca;->aEH()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    new-instance v0, Lfzb;

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfux;->mStringEvaluator:Lgbr;

    invoke-direct {v0, v2, v3, v4}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    .line 193
    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    iget-object v3, p0, Lfux;->cte:Ljbp;

    invoke-virtual {v0, p1, v1, v2, v3}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    .line 82
    invoke-direct {p0}, Lfux;->aDw()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 83
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->g(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dSf:Lixr;

    invoke-virtual {v1}, Lixr;->baD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->lV(Ljava/lang/String;)Lamk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfux;->d(Lamk;)V

    .line 91
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 93
    invoke-direct {p0, p1}, Lfux;->aZ(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_6

    .line 95
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :goto_1
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSf:Lixr;

    iget-object v1, p0, Lfux;->cte:Ljbp;

    if-eqz v1, :cond_9

    iget-object v1, v0, Lixr;->dMF:[Liyg;

    array-length v1, v1

    if-eqz v1, :cond_9

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->no()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfvv;

    iget-object v3, p0, Lfux;->mEntry:Lizj;

    const/16 v4, 0xba

    iget-object v5, p0, Lfux;->cte:Ljbp;

    invoke-direct {v2, v3, v4, v0, v5}, Lfvv;-><init>(Lizj;ILiyg;Ljbp;)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfvv;->mName:Ljava/lang/String;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, v2, Lfvv;->cCH:Z

    invoke-virtual {v2}, Lfvv;->aDh()Lanh;

    move-result-object v0

    move-object v2, v0

    .line 101
    :goto_2
    if-nez v2, :cond_a

    const/4 v0, 0x1

    :goto_3
    new-instance v1, Lfsa;

    const/4 v3, 0x3

    invoke-direct {v1, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f020134

    invoke-virtual {v1, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    invoke-direct {p0}, Lfux;->aDw()Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "content://com.android.calendar/events/%1$s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lfux;->mCalendarData:Lamk;

    iget-object v8, v8, Lamk;->aeQ:Lamo;

    invoke-virtual {v8}, Lamo;->nj()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v1, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "beginTime"

    iget-object v5, p0, Lfux;->mCalendarData:Lamk;

    iget-object v5, v5, Lamk;->aeQ:Lamo;

    invoke-virtual {v5}, Lamo;->nk()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual {v4, v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "endTime"

    iget-object v5, p0, Lfux;->mCalendarData:Lamk;

    iget-object v5, v5, Lamk;->aeQ:Lamo;

    invoke-virtual {v5}, Lamo;->nm()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-virtual {v4, v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v3

    new-instance v4, Lftn;

    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Lftn;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nl()Z

    move-result v5

    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nn()Z

    move-result v7

    const/4 v1, 0x0

    if-eqz v5, :cond_1

    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nk()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    const/4 v1, 0x0

    invoke-static {p1, v8, v9, v1}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v7, :cond_c

    iget-object v5, p0, Lfux;->mCalendarData:Lamk;

    iget-object v5, v5, Lamk;->aeQ:Lamo;

    invoke-virtual {v5}, Lamo;->nm()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    const/4 v5, 0x0

    invoke-static {p1, v8, v9, v5}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v5

    const v7, 0x7f0a02a4

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    const/4 v1, 0x1

    aput-object v5, v8, v1

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_5
    iput-object v1, v4, Lftn;->ceJ:Ljava/lang/String;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {v4, v3, v1}, Lftn;->a(Lani;Lizj;)Lftn;

    move-result-object v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfux;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v0}, Lamo;->no()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lftn;->cBj:Ljava/lang/String;

    :cond_2
    invoke-virtual {v1}, Lftn;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-static {v6, v2}, Lfux;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lfux;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v0}, Lamo;->nr()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lez v0, :cond_3

    invoke-direct {p0}, Lfux;->aDw()Z

    move-result v1

    if-nez v1, :cond_d

    :cond_3
    const/4 v0, 0x0

    :goto_6
    invoke-static {v6, v0}, Lfux;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 105
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 106
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 107
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 108
    return-object v1

    .line 88
    :cond_4
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSf:Lixr;

    new-instance v1, Lamo;

    invoke-direct {v1}, Lamo;-><init>()V

    invoke-virtual {v0}, Lixr;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lamo;->V(Ljava/lang/String;)Lamo;

    move-result-object v1

    invoke-virtual {v0}, Lixr;->nk()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lamo;->t(J)Lamo;

    move-result-object v1

    invoke-virtual {v0}, Lixr;->nm()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lamo;->u(J)Lamo;

    move-result-object v1

    iget-object v2, v0, Lixr;->aeB:Ljbp;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lixr;->aeB:Ljbp;

    invoke-virtual {v2}, Ljbp;->oK()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v0, v0, Lixr;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lamo;->W(Ljava/lang/String;)Lamo;

    :cond_5
    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    iput-object v1, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {p0, v0}, Lfux;->d(Lamk;)V

    goto/16 :goto_0

    .line 97
    :cond_6
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v3, Lizj;->dSf:Lixr;

    iget-object v0, p0, Lfux;->cte:Ljbp;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lixr;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_7

    iget-object v0, v3, Lizj;->dUv:Ljhk;

    if-nez v0, :cond_8

    :cond_7
    const/4 v0, 0x0

    :goto_7
    invoke-static {v6, v0}, Lfux;->a(Ljava/util/List;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lfux;->mTimeToLeaveFactory:Lfyk;

    iget-object v4, v1, Lixr;->dMF:[Liyg;

    iget-object v5, p0, Lfux;->cte:Ljbp;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;)Lanh;

    move-result-object v0

    goto :goto_7

    .line 100
    :cond_9
    const/4 v0, 0x0

    move-object v2, v0

    goto/16 :goto_2

    .line 101
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_b
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dSf:Lixr;

    invoke-virtual {v1}, Lixr;->baH()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    :cond_c
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 103
    :cond_d
    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    iget-object v1, v1, Lamk;->aeQ:Lamo;

    invoke-virtual {v1}, Lamo;->nj()J

    move-result-wide v2

    invoke-static {v2, v3}, Lfzy;->bs(J)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lfsa;

    const/16 v3, 0x2f

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f02012d

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v1

    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100032

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    iput-object v2, v0, Lanh;->agz:Laoj;

    iput-object v1, v0, Lanh;->ahs:Lani;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    goto/16 :goto_6
.end method

.method protected final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 114
    invoke-virtual {p2}, Lanh;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 120
    invoke-super {p0, p1, p2, p3}, Lfuz;->a(Landroid/content/Context;Lanh;I)Lanh;

    move-result-object v0

    :goto_0
    return-object v0

    .line 116
    :pswitch_0
    invoke-direct {p0, p1}, Lfux;->aZ(Landroid/content/Context;)Lanh;

    move-result-object v0

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch
.end method

.method public final aDx()Lamo;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lfux;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lfmt;)V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lfuz;->b(Landroid/content/Context;Lfmt;)V

    .line 76
    iget-object v0, p0, Lfux;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v0}, Lamo;->nh()J

    move-result-wide v0

    .line 77
    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v2

    invoke-virtual {v2}, Lfml;->aBa()Lfor;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2, v0, v1}, Lfor;->bl(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making dismiss calendar entry request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final ba(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 300
    iget-object v0, p0, Lfux;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v0}, Lamo;->nk()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lamk;)V
    .locals 3

    .prologue
    .line 128
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    iput-object v0, p0, Lfux;->mCalendarData:Lamk;

    .line 129
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSf:Lixr;

    .line 130
    iget-object v1, p0, Lfux;->mCalendarData:Lamk;

    invoke-static {v0, v1}, Lfzy;->a(Lixr;Lamk;)Ljbp;

    move-result-object v1

    iput-object v1, p0, Lfux;->cte:Ljbp;

    .line 135
    new-instance v1, Ljak;

    invoke-direct {v1}, Ljak;-><init>()V

    .line 136
    iget-object v2, p0, Lfux;->cte:Ljbp;

    if-eqz v2, :cond_0

    .line 137
    iget-object v2, p0, Lfux;->cte:Ljbp;

    iput-object v2, v1, Ljak;->aeB:Ljbp;

    .line 139
    :cond_0
    new-instance v2, Ljal;

    invoke-direct {v2}, Ljal;-><init>()V

    iput-object v2, p0, Lfux;->mFrequentPlaceEntry:Ljal;

    .line 140
    iget-object v2, p0, Lfux;->mFrequentPlaceEntry:Ljal;

    iput-object v1, v2, Ljal;->dWL:Ljak;

    .line 141
    iget-object v1, v0, Lixr;->dMF:[Liyg;

    array-length v1, v1

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lfux;->mFrequentPlaceEntry:Ljal;

    iget-object v0, v0, Lixr;->dMF:[Liyg;

    iput-object v0, v1, Ljal;->dMF:[Liyg;

    .line 144
    :cond_1
    return-void
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0, p1, p2}, Lfux;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 68
    iget-object v0, v0, Lang;->ags:[Lanh;

    return-object v0
.end method
