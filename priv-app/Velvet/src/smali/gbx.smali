.class public final Lgbx;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lizj;Ljbp;Ljava/lang/Iterable;ZLgbr;)Lanh;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 44
    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 45
    invoke-static {p0, p2, p3, p5}, Lgbx;->a(Landroid/content/Context;Ljbp;Ljava/lang/Iterable;Lgbr;)Lapj;

    move-result-object v1

    iput-object v1, v0, Lanh;->agX:Lapj;

    .line 47
    iput-object p1, v0, Lanh;->ahu:Lizj;

    .line 48
    invoke-virtual {v0, p4}, Lanh;->aV(Z)Lanh;

    .line 49
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljbp;Liyg;Lgbr;)Lapf;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 68
    new-instance v1, Lapf;

    invoke-direct {v1}, Lapf;-><init>()V

    .line 70
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbW()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v2

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbV()J

    move-result-wide v4

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v0

    int-to-long v6, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sub-long/2addr v2, v6

    invoke-static {p0, v2, v3}, Lgbx;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v5}, Lgbx;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a01c8

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v0, v4, v8

    aput-object v2, v4, v9

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapf;->cV(Ljava/lang/String;)Lapf;

    .line 77
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v2

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbV()J

    move-result-wide v4

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v0

    int-to-long v6, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sub-long/2addr v2, v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v2, v4, v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {p0, v0, v9}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapf;->cW(Ljava/lang/String;)Lapf;

    .line 83
    :cond_0
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dPR:Ljbp;

    if-eqz v0, :cond_1

    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dPR:Ljbp;

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbO()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbU()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbN()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0a01d1

    :goto_0
    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v3}, Liyh;->bbT()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lgbx;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p2, Liyg;->dPL:Liyh;

    iget-object v3, v3, Liyh;->dPR:Ljbp;

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-virtual {v1, v0}, Lapf;->cX(Ljava/lang/String;)Lapf;

    .line 94
    :cond_1
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbQ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v0

    invoke-static {p0, v0, v9}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapf;->cZ(Ljava/lang/String;)Lapf;

    .line 98
    :cond_2
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbZ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbY()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v0, v2

    const/16 v2, 0x5a0

    if-lt v0, v2, :cond_7

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lapf;->da(Ljava/lang/String;)Lapf;

    .line 102
    :cond_3
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQg:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 103
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQg:[Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v1, v0}, Lapf;->cY(Ljava/lang/String;)Lapf;

    .line 105
    :cond_4
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQj:Ljhe;

    if-eqz v0, :cond_5

    .line 106
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQj:Ljhe;

    invoke-virtual {p3, p0, v0}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapf;->dc(Ljava/lang/String;)Lapf;

    .line 110
    :cond_5
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQi:[Liyk;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbP()I

    move-result v2

    invoke-static {v0, v2}, Lgbx;->c(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 113
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lapi;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapi;

    iput-object v0, v1, Lapf;->akH:[Lapi;

    .line 116
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQe:[Liyi;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 117
    iget-object v0, p2, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQe:[Liyi;

    invoke-static {p0, v0}, Lgbx;->a(Landroid/content/Context;[Liyi;)Lapg;

    move-result-object v0

    iput-object v0, v1, Lapf;->akM:Lapg;

    .line 120
    :cond_6
    invoke-static {p1, p2, v8}, Lgaz;->a(Ljbp;Liyg;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapf;->db(Ljava/lang/String;)Lapf;

    .line 121
    iget-object v0, p2, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->sI()Z

    move-result v0

    invoke-virtual {v1, v0}, Lapf;->bj(Z)Lapf;

    .line 123
    return-object v1

    .line 87
    :pswitch_0
    const v0, 0x7f0a01cd

    goto/16 :goto_0

    :pswitch_1
    const v0, 0x7f0a01ce

    goto/16 :goto_0

    :pswitch_2
    const v0, 0x7f0a01cf

    goto/16 :goto_0

    :pswitch_3
    const v0, 0x7f0a01d0

    goto/16 :goto_0

    .line 99
    :cond_7
    const/16 v2, 0x3c

    if-lt v0, v2, :cond_9

    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    if-lez v0, :cond_8

    const v3, 0x7f0a01cc

    new-array v4, v10, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f10002b

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f10002a

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;[Liyi;)Lapg;
    .locals 7

    .prologue
    const v6, 0x7f0b00b8

    const v5, 0x7f0b00b7

    const/4 v4, 0x0

    .line 289
    new-instance v1, Lapg;

    invoke-direct {v1}, Lapg;-><init>()V

    .line 290
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lgbx;->ag(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    .line 291
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 293
    const v0, 0x7f0a01a1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->dd(Ljava/lang/String;)Lapg;

    .line 294
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lapg;->cR(I)Lapg;

    .line 328
    :goto_0
    return-object v1

    .line 297
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 298
    packed-switch v0, :pswitch_data_0

    .line 324
    const-string v2, "TransitTripUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown Alert Type:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 300
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lapg;->cR(I)Lapg;

    .line 301
    const v0, 0x7f0a019e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->dd(Ljava/lang/String;)Lapg;

    .line 302
    aget-object v0, p1, v4

    invoke-virtual {v0}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->de(Ljava/lang/String;)Lapg;

    goto :goto_0

    .line 305
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lapg;->cR(I)Lapg;

    .line 306
    const v0, 0x7f0a019d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->dd(Ljava/lang/String;)Lapg;

    .line 307
    aget-object v0, p1, v4

    iget-object v0, v0, Liyi;->dPR:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->de(Ljava/lang/String;)Lapg;

    goto :goto_0

    .line 310
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lapg;->cR(I)Lapg;

    .line 312
    const v0, 0x7f0a01a0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->dd(Ljava/lang/String;)Lapg;

    .line 314
    aget-object v0, p1, v4

    invoke-virtual {v0}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->de(Ljava/lang/String;)Lapg;

    goto/16 :goto_0

    .line 317
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lapg;->cR(I)Lapg;

    .line 319
    const v0, 0x7f0a019f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->dd(Ljava/lang/String;)Lapg;

    .line 321
    aget-object v0, p1, v4

    invoke-virtual {v0}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapg;->de(Ljava/lang/String;)Lapg;

    goto/16 :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljbp;Ljava/lang/Iterable;Lgbr;)Lapj;
    .locals 4

    .prologue
    .line 55
    new-instance v1, Lapj;

    invoke-direct {v1}, Lapj;-><init>()V

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 57
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyg;

    .line 58
    invoke-static {p0, p1, v0, p3}, Lgbx;->a(Landroid/content/Context;Ljbp;Liyg;Lgbr;)Lapf;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lapf;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapf;

    iput-object v0, v1, Lapj;->alm:[Lapf;

    .line 61
    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 215
    .line 216
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 273
    :goto_0
    return-object v0

    .line 219
    :cond_1
    invoke-static {p1}, Lgbx;->ag(Ljava/util/List;)Ljava/util/Set;

    move-result-object v0

    .line 220
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-le v2, v3, :cond_2

    .line 222
    const v0, 0x7f0a018e

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 227
    packed-switch v0, :pswitch_data_0

    .line 270
    const-string v2, "TransitTripUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown Alert Type:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    .line 229
    :pswitch_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 230
    const v1, 0x7f0a0188

    new-array v2, v6, [Ljava/lang/Object;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyi;

    invoke-virtual {v0}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    aput-object p2, v2, v3

    aput-object p3, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_3
    const v0, 0x7f0a0189

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 240
    const v1, 0x7f0a0186

    new-array v2, v6, [Ljava/lang/Object;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyi;

    iget-object v0, v0, Liyi;->dPR:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    aput-object p2, v2, v3

    aput-object p3, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 244
    :cond_4
    const v0, 0x7f0a0187

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 249
    :pswitch_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 250
    const v1, 0x7f0a018a

    new-array v2, v6, [Ljava/lang/Object;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyi;

    invoke-virtual {v0}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    aput-object p2, v2, v3

    aput-object p3, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 254
    :cond_5
    const v0, 0x7f0a018b

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 259
    :pswitch_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_6

    .line 260
    const v1, 0x7f0a018c

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p2, v2, v4

    aput-object p3, v2, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyi;

    invoke-virtual {v0}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 264
    :cond_6
    const v0, 0x7f0a018d

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static ag(Ljava/util/List;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 277
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 278
    if-nez p0, :cond_0

    move-object v0, v1

    .line 284
    :goto_0
    return-object v0

    .line 281
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyi;

    .line 282
    invoke-virtual {v0}, Liyi;->getType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 284
    goto :goto_0
.end method

.method private static c(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 387
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 388
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/List;I)Ljava/util/List;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    const/4 v11, -0x1

    .line 128
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 129
    if-eqz p1, :cond_0

    .line 130
    new-instance v0, Lapi;

    invoke-direct {v0}, Lapi;-><init>()V

    .line 131
    invoke-virtual {v0, v12}, Lapi;->cV(I)Lapi;

    .line 132
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyk;

    .line 135
    iget-object v4, v0, Liyk;->dQq:[Liyj;

    array-length v4, v4

    if-eqz v4, :cond_1

    .line 136
    new-instance v4, Lapi;

    invoke-direct {v4}, Lapi;-><init>()V

    .line 139
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lapi;->cV(I)Lapi;

    .line 140
    iget-object v5, v0, Liyk;->dQq:[Liyj;

    aget-object v5, v5, v1

    .line 141
    invoke-virtual {v5}, Liyj;->sk()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 142
    invoke-virtual {v5}, Liyj;->sj()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lapi;->dg(Ljava/lang/String;)Lapi;

    .line 173
    :goto_1
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_2
    invoke-virtual {v5}, Liyj;->bcf()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 145
    invoke-virtual {v5}, Liyj;->bce()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lapi;->dg(Ljava/lang/String;)Lapi;

    .line 147
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 148
    iget-object v6, v0, Liyk;->dQq:[Liyj;

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_7

    aget-object v8, v6, v0

    .line 149
    new-instance v9, Laph;

    invoke-direct {v9}, Laph;-><init>()V

    .line 150
    invoke-virtual {v8}, Liyj;->oK()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v8}, Liyj;->hasBackgroundColor()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v8}, Liyj;->getBackgroundColor()I

    move-result v10

    if-eq v10, v11, :cond_5

    invoke-virtual {v8}, Liyj;->hasForegroundColor()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v8}, Liyj;->bcg()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 156
    invoke-virtual {v8}, Liyj;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Laph;->df(Ljava/lang/String;)Laph;

    .line 157
    invoke-virtual {v8}, Liyj;->getBackgroundColor()I

    move-result v10

    invoke-virtual {v9, v10}, Laph;->cT(I)Laph;

    .line 158
    invoke-virtual {v8}, Liyj;->getForegroundColor()I

    move-result v8

    invoke-virtual {v9, v8}, Laph;->cS(I)Laph;

    .line 168
    :goto_3
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 159
    :cond_5
    invoke-virtual {v8}, Liyj;->hasBackgroundColor()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v8}, Liyj;->getBackgroundColor()I

    move-result v10

    if-eq v10, v11, :cond_6

    .line 161
    invoke-virtual {v8}, Liyj;->getBackgroundColor()I

    move-result v8

    invoke-virtual {v9, v8}, Laph;->cU(I)Laph;

    goto :goto_3

    .line 162
    :cond_6
    invoke-virtual {v8}, Liyj;->oK()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 164
    invoke-virtual {v8}, Liyj;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Laph;->df(Ljava/lang/String;)Laph;

    goto :goto_3

    .line 170
    :cond_7
    invoke-static {v5}, Lgbx;->m(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 171
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Laph;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laph;

    iput-object v0, v4, Lapi;->all:[Laph;

    goto/16 :goto_1

    .line 175
    :cond_8
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_9

    if-nez p1, :cond_a

    .line 176
    :cond_9
    new-instance v0, Lapi;

    invoke-direct {v0}, Lapi;-><init>()V

    .line 177
    invoke-virtual {v0, v12}, Lapi;->cV(I)Lapi;

    .line 178
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_a
    return-object v2
.end method

.method public static m(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 7

    .prologue
    .line 345
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 346
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 347
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laph;

    .line 348
    invoke-virtual {v0}, Laph;->hasBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Laph;->getBackgroundColor()I

    move-result v1

    const/4 v5, -0x1

    if-ne v1, v5, :cond_1

    :cond_0
    invoke-virtual {v0}, Laph;->hasForegroundColor()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Laph;->getForegroundColor()I

    move-result v1

    const/high16 v5, -0x1000000

    if-eq v1, v5, :cond_2

    :cond_1
    const/4 v1, 0x1

    .line 351
    :goto_1
    invoke-virtual {v0}, Laph;->oK()Z

    move-result v5

    if-eqz v5, :cond_3

    if-nez v1, :cond_3

    .line 353
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 348
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 355
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 356
    new-instance v1, Laph;

    invoke-direct {v1}, Laph;-><init>()V

    .line 357
    const-string v5, " / "

    invoke-static {v5}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v5

    new-instance v6, Lgby;

    invoke-direct {v6}, Lgby;-><init>()V

    invoke-static {v2, v6}, Likm;->b(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v6

    invoke-virtual {v5, v6}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Laph;->df(Ljava/lang/String;)Laph;

    .line 365
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 368
    :cond_4
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 371
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 372
    new-instance v0, Laph;

    invoke-direct {v0}, Laph;-><init>()V

    .line 373
    const-string v1, " / "

    invoke-static {v1}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v1

    new-instance v4, Lgbz;

    invoke-direct {v4}, Lgbz;-><init>()V

    invoke-static {v2, v4}, Likm;->b(Ljava/lang/Iterable;Lifg;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laph;->df(Ljava/lang/String;)Laph;

    .line 381
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_6
    return-object v3
.end method
