.class public final Ljlt;
.super Ljsl;
.source "PG"


# static fields
.field public static final esN:Ljsm;


# instance fields
.field private aez:I

.field private eoc:I

.field private esO:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3181
    const/16 v0, 0xb

    const-class v1, Ljlt;

    const v2, 0x17c60cca

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljlt;->esN:Ljsm;

    .line 3200
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3245
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3246
    iput v0, p0, Ljlt;->aez:I

    iput v0, p0, Ljlt;->eoc:I

    iput v0, p0, Ljlt;->esO:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljlt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlt;->eCz:I

    .line 3247
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3174
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljlt;->eoc:I

    iget v0, p0, Ljlt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljlt;->esO:I

    iget v0, p0, Ljlt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlt;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3261
    iget v0, p0, Ljlt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3262
    const/4 v0, 0x1

    iget v1, p0, Ljlt;->eoc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3264
    :cond_0
    iget v0, p0, Ljlt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3265
    const/4 v0, 0x2

    iget v1, p0, Ljlt;->esO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3267
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3268
    return-void
.end method

.method public final bpu()I
    .locals 1

    .prologue
    .line 3229
    iget v0, p0, Ljlt;->esO:I

    return v0
.end method

.method public final getAction()I
    .locals 1

    .prologue
    .line 3210
    iget v0, p0, Ljlt;->eoc:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3272
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3273
    iget v1, p0, Ljlt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3274
    const/4 v1, 0x1

    iget v2, p0, Ljlt;->eoc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3277
    :cond_0
    iget v1, p0, Ljlt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3278
    const/4 v1, 0x2

    iget v2, p0, Ljlt;->esO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3281
    :cond_1
    return v0
.end method
