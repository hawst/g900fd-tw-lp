.class public abstract Leiq;
.super Ljava/lang/Exception;
.source "PG"

# interfaces
.implements Lefr;


# instance fields
.field final bDv:I

.field private final cam:I


# direct methods
.method protected constructor <init>(II)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GsaErrorCode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", engine: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 36
    iput p1, p0, Leiq;->cam:I

    .line 37
    iput p2, p0, Leiq;->bDv:I

    .line 38
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 23
    iput p2, p0, Leiq;->cam:I

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Leiq;->bDv:I

    .line 25
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 42
    const/4 v0, 0x2

    iput v0, p0, Leiq;->cam:I

    .line 43
    iput p3, p0, Leiq;->bDv:I

    .line 44
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    iput p3, p0, Leiq;->cam:I

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Leiq;->bDv:I

    .line 32
    return-void
.end method

.method protected constructor <init>(Ljava/lang/Throwable;II)V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GsaErrorCode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", engine: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 48
    iput p2, p0, Leiq;->cam:I

    .line 49
    iput p3, p0, Leiq;->bDv:I

    .line 50
    return-void
.end method


# virtual methods
.method public aja()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public final aot()Ljava/lang/Exception;
    .locals 0

    .prologue
    .line 74
    return-object p0
.end method

.method public aou()I
    .locals 1

    .prologue
    .line 69
    const/16 v0, 0xd3

    return v0
.end method

.method public final ata()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Leiq;->cam:I

    return v0
.end method

.method public final getErrorCode()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Leiq;->bDv:I

    return v0
.end method
