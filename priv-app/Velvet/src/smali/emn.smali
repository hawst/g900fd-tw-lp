.class public final Lemn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lesm;


# instance fields
.field private final cfu:Lijj;


# direct methods
.method private constructor <init>(Lijj;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lemn;->cfu:Lijj;

    .line 22
    return-void
.end method

.method public static b(Lijj;)Lemn;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lemn;

    invoke-direct {v0, p0}, Lemn;-><init>(Lijj;)V

    return-object v0
.end method


# virtual methods
.method public final D(Landroid/net/Uri;)Leml;
    .locals 3

    .prologue
    .line 31
    invoke-static {p1}, Lesp;->aQ(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-static {}, Lepr;->avq()Lepr;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 35
    :cond_0
    iget-object v0, p0, Lemn;->cfu:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesm;

    .line 36
    invoke-interface {v0, p1}, Lesm;->u(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    invoke-interface {v0, p1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v0

    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "CascadingUriLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No loader can load "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 42
    invoke-static {}, Lepr;->avq()Lepr;

    move-result-object v0

    goto :goto_0
.end method

.method public final clearCache()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lemn;->cfu:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesm;

    .line 48
    invoke-interface {v0}, Lesm;->clearCache()V

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method
