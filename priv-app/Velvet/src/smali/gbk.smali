.class public final Lgbk;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V
    .locals 8
    .param p4    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 145
    if-nez p4, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 149
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->fi(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->b(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/gms/googlehelp/GoogleHelp;->g(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->l(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 154
    invoke-static {p6}, Lgbk;->ae(Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_0

    .line 156
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->n(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 158
    :cond_0
    invoke-static {p7}, Lgbk;->ae(Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    .line 159
    if-eqz v1, :cond_1

    .line 160
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->o(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 163
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->yY()Landroid/content/Intent;

    move-result-object v0

    .line 164
    new-instance v1, Lbkb;

    invoke-direct {v1, p0}, Lbkb;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v0}, Lbkb;->g(Landroid/content/Intent;)V

    .line 167
    if-eqz p5, :cond_2

    .line 168
    const/16 v0, 0x2b

    invoke-static {v0}, Lgal;->jP(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.SAVE_CALL_LOG"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 173
    :cond_2
    if-nez p4, :cond_3

    .line 174
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 176
    :cond_3
    return-void

    .line 145
    :cond_4
    instance-of v0, p4, Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_8

    move-object v0, p4

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->computeVerticalScrollRange()I

    move-result v2

    invoke-virtual {v0, v1, v1, v4, v2}, Lcom/google/android/shared/ui/CoScrollContainer;->layout(IIII)V

    invoke-virtual {p4, v1}, Landroid/view/View;->buildDrawingCache(Z)V

    invoke-static {v4, v2}, Lgbk;->ba(II)Landroid/graphics/Point;

    move-result-object v5

    if-nez v5, :cond_6

    move v3, v4

    :goto_1
    if-nez v5, :cond_7

    move v1, v2

    :goto_2
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v1, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    if-eqz v5, :cond_5

    iget v6, v5, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    int-to-float v4, v4

    div-float v4, v6, v4

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    int-to-float v2, v2

    div-float v2, v5, v2

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Canvas;->scale(FF)V

    :cond_5
    invoke-virtual {v0, v3}, Lcom/google/android/shared/ui/CoScrollContainer;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p4}, Landroid/view/View;->destroyDrawingCache()V

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->requestLayout()V

    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    iget v1, v5, Landroid/graphics/Point;->x:I

    move v3, v1

    goto :goto_1

    :cond_7
    iget v1, v5, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_8
    invoke-virtual {p4}, Landroid/view/View;->buildDrawingCache()V

    invoke-virtual {p4}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lgbk;->ba(II)Landroid/graphics/Point;

    move-result-object v1

    if-nez v1, :cond_9

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_3
    invoke-virtual {p4}, Landroid/view/View;->destroyDrawingCache()V

    goto/16 :goto_0

    :cond_9
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v2, v1, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V
    .locals 10
    .param p5    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 235
    const v0, 0x7f0a0583

    invoke-interface {p0, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 236
    const v0, 0x7f02015f

    invoke-interface {v9, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 237
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 238
    new-instance v0, Lgbl;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v8}, Lgbl;-><init>(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V

    invoke-interface {v9, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 242
    return-void
.end method

.method public static a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 214
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    move-object v7, v5

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V

    .line 216
    return-void
.end method

.method private static ae(Ljava/util/List;)Landroid/os/Bundle;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 193
    const/4 v0, 0x0

    .line 194
    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 196
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 197
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 200
    :cond_1
    return-object v0
.end method

.method private static ba(II)Landroid/graphics/Point;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    mul-int v0, p0, p1

    const/high16 v1, 0x40000

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    .line 73
    :cond_0
    int-to-double v0, p0

    int-to-double v2, p1

    div-double/2addr v0, v2

    .line 77
    const-wide/high16 v2, 0x4110000000000000L    # 262144.0

    mul-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    sget-object v4, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v4}, Liuo;->a(DLjava/math/RoundingMode;)I

    move-result v2

    .line 79
    int-to-double v4, v2

    div-double v0, v4, v0

    sget-object v3, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Liuo;->a(DLjava/math/RoundingMode;)I

    move-result v1

    .line 81
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method
