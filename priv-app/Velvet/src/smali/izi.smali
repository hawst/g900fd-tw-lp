.class public final Lizi;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dRR:[Lizi;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dRS:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17881
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 17882
    iput v0, p0, Lizi;->aez:I

    iput v0, p0, Lizi;->dRS:I

    const-string v0, ""

    iput-object v0, p0, Lizi;->agq:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lizi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizi;->eCz:I

    .line 17883
    return-void
.end method

.method public static bcN()[Lizi;
    .locals 2

    .prologue
    .line 17827
    sget-object v0, Lizi;->dRR:[Lizi;

    if-nez v0, :cond_1

    .line 17828
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17830
    :try_start_0
    sget-object v0, Lizi;->dRR:[Lizi;

    if-nez v0, :cond_0

    .line 17831
    const/4 v0, 0x0

    new-array v0, v0, [Lizi;

    sput-object v0, Lizi;->dRR:[Lizi;

    .line 17833
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17835
    :cond_1
    sget-object v0, Lizi;->dRR:[Lizi;

    return-object v0

    .line 17833
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 17821
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizi;->dRS:I

    iget v0, p0, Lizi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizi;->agq:Ljava/lang/String;

    iget v0, p0, Lizi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 17897
    iget v0, p0, Lizi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 17898
    const/4 v0, 0x1

    iget v1, p0, Lizi;->dRS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 17900
    :cond_0
    iget v0, p0, Lizi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 17901
    const/4 v0, 0x2

    iget-object v1, p0, Lizi;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 17903
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 17904
    return-void
.end method

.method public final bcO()I
    .locals 1

    .prologue
    .line 17843
    iget v0, p0, Lizi;->dRS:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17862
    iget-object v0, p0, Lizi;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 17908
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 17909
    iget v1, p0, Lizi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 17910
    const/4 v1, 0x1

    iget v2, p0, Lizi;->dRS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17913
    :cond_0
    iget v1, p0, Lizi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 17914
    const/4 v1, 0x2

    iget-object v2, p0, Lizi;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17917
    :cond_1
    return v0
.end method
