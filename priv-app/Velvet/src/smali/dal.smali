.class public final Ldal;
.super Landroid/telephony/PhoneStateListener;
.source "PG"


# instance fields
.field private final bpK:Ldam;

.field private final mContext:Landroid/content/Context;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Ldam;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 48
    iput-object p1, p0, Ldal;->bpK:Ldam;

    .line 49
    const-string v0, "phone"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Ldal;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 50
    iput-object p2, p0, Ldal;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method private fx(I)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Ldal;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 79
    :goto_1
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :cond_1
    iget-object v0, p0, Ldal;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    goto :goto_1
.end method


# virtual methods
.method public final onCallStateChanged(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 89
    packed-switch p1, :pswitch_data_0

    .line 100
    :goto_0
    return-void

    .line 91
    :pswitch_0
    iget-object v0, p0, Ldal;->bpK:Ldam;

    invoke-interface {v0}, Ldam;->UW()V

    goto :goto_0

    .line 94
    :pswitch_1
    iget-object v0, p0, Ldal;->bpK:Ldam;

    invoke-interface {v0, p2}, Ldam;->jp(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :pswitch_2
    iget-object v0, p0, Ldal;->bpK:Ldam;

    invoke-interface {v0}, Ldam;->UV()V

    goto :goto_0

    .line 89
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final startListening()V
    .locals 1

    .prologue
    .line 61
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Ldal;->fx(I)V

    .line 62
    return-void
.end method

.method public final stopListening()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldal;->fx(I)V

    .line 69
    return-void
.end method
