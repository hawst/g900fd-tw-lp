.class public Lcfc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aTV:Lcfh;

.field private final aTW:Ljava/util/concurrent/Executor;

.field final aTX:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mContext:Landroid/content/Context;

.field private final mGsaConfigFlags:Lchk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcfh;

    invoke-direct {v0, p1}, Lcfh;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcfc;-><init>(Landroid/content/Context;Lchk;Ljava/util/concurrent/Executor;Lcfh;)V

    .line 67
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lchk;Ljava/util/concurrent/Executor;Lcfh;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcfc;->aTX:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 72
    iput-object p1, p0, Lcfc;->mContext:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcfc;->mGsaConfigFlags:Lchk;

    .line 74
    iput-object p3, p0, Lcfc;->aTW:Ljava/util/concurrent/Executor;

    .line 75
    iput-object p4, p0, Lcfc;->aTV:Lcfh;

    .line 76
    return-void
.end method

.method static ge(Ljava/lang/String;)Ljava/util/List;
    .locals 3

    .prologue
    .line 169
    const-string v0, "[]"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 189
    :goto_0
    return-object v0

    .line 173
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 175
    new-instance v1, Ljava/io/ByteArrayInputStream;

    sget-object v2, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 176
    new-instance v2, Ldlo;

    invoke-direct {v2, v1}, Ldlo;-><init>(Ljava/io/InputStream;)V

    .line 178
    :try_start_0
    invoke-virtual {v2}, Ldlo;->beginArray()V

    .line 179
    :goto_1
    invoke-virtual {v2}, Ldlo;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    invoke-virtual {v2}, Ldlo;->nextString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 184
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "ChromePrerenderer"

    const-string v1, "Unable to parse the prerender hints"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-static {}, Lijj;->aWW()Lijj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 187
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 182
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ldlo;->endArray()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 187
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
.end method


# virtual methods
.method public final Dv()V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lcfc;->aTX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 83
    iget-object v0, p0, Lcfc;->aTW:Ljava/util/concurrent/Executor;

    new-instance v1, Lcfd;

    const-string v2, "chrome-prerender-attach"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lcfd;-><init>(Lcfc;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 95
    return-void

    .line 83
    nop

    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method Dw()Z
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcfc;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lesp;->ax(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcfc;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GQ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final detach()V
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lcfc;->aTX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 102
    iget-object v0, p0, Lcfc;->aTW:Ljava/util/concurrent/Executor;

    new-instance v1, Lcfe;

    const-string v2, "chrome-prerender-detach"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lcfe;-><init>(Lcfc;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 111
    return-void

    .line 102
    nop

    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method gf(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 231
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 233
    iget-object v1, p0, Lcfc;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v1, :cond_1

    .line 236
    :cond_0
    const/4 v0, 0x0

    .line 238
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_0
.end method

.method public final requestCancelPrerender()V
    .locals 4

    .prologue
    .line 142
    iget-object v0, p0, Lcfc;->aTW:Ljava/util/concurrent/Executor;

    new-instance v1, Lcfg;

    const-string v2, "chrome-prerender-cancel"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lcfg;-><init>(Lcfc;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    return-void

    .line 142
    nop

    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method public final y(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 120
    iget-object v6, p0, Lcfc;->aTW:Ljava/util/concurrent/Executor;

    new-instance v0, Lcff;

    const-string v2, "chrome-prerender-urls"

    const/4 v1, 0x2

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcff;-><init>(Lcfc;Ljava/lang/String;[ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 136
    return-void

    .line 120
    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method
