.class public final Ldfl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lddc;
.implements Leti;


# instance fields
.field final aMD:Leqo;

.field private final aOw:Ljava/util/concurrent/Executor;

.field public anR:Z

.field public final bmB:Landroid/database/DataSetObserver;

.field private final bwf:Ljava/lang/Runnable;

.field private final bwg:Ljava/lang/Runnable;

.field private final bwh:Ljava/lang/Runnable;

.field private final bwi:Ldfu;

.field private final bwj:Ldgm;

.field public final bwk:Ldfe;

.field private final bwl:Ldft;

.field private bwm:Ldfk;

.field private bwn:Ljava/lang/Runnable;

.field private bwo:Z

.field bwp:Ldem;

.field private volatile bwq:I

.field private bwr:Z

.field private bws:Ljava/util/Locale;

.field private bwt:J

.field private bwu:Z

.field private bwv:Lcom/google/android/shared/search/Query;

.field private bww:Lcom/google/android/shared/search/Query;

.field private bwx:Lcom/google/android/shared/search/Query;

.field public bwy:Lcqn;

.field private cq:Z

.field private final mClock:Lemp;

.field public final mCoreSearchServices:Lcfo;

.field private final mEventBus:Ldda;

.field private final mFactory:Lgpu;

.field final mGlobalSearchServices:Lcgh;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mSearchBoxLogging:Lcpd;

.field private final mSearchController:Lcjt;

.field private final mServiceState:Ldcu;

.field private final mShouldQueryStrategy:Ldfz;

.field public mSources:Ldgh;


# direct methods
.method public constructor <init>(Leqo;Ljava/util/concurrent/Executor;Lcfo;Lcgh;Lgpu;Lcpd;Lemp;Ldgm;Ldfz;Ldfe;Lcjt;Ldft;)V
    .locals 2

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Ldfm;

    const-string v1, "Update suggestions"

    invoke-direct {v0, p0, v1}, Ldfm;-><init>(Ldfl;Ljava/lang/String;)V

    iput-object v0, p0, Ldfl;->bwf:Ljava/lang/Runnable;

    .line 93
    new-instance v0, Ldfn;

    const-string v1, "Grey out stale suggestions"

    invoke-direct {v0, p0, v1}, Ldfn;-><init>(Ldfl;Ljava/lang/String;)V

    iput-object v0, p0, Ldfl;->bwg:Ljava/lang/Runnable;

    .line 105
    new-instance v0, Ldfo;

    const-string v1, "Suggestions timeout"

    invoke-direct {v0, p0, v1}, Ldfo;-><init>(Ldfl;Ljava/lang/String;)V

    iput-object v0, p0, Ldfl;->bwh:Ljava/lang/Runnable;

    .line 115
    new-instance v0, Ldfu;

    invoke-direct {v0, p0}, Ldfu;-><init>(Ldfl;)V

    iput-object v0, p0, Ldfl;->bwi:Ldfu;

    .line 136
    new-instance v0, Ldfp;

    invoke-direct {v0, p0}, Ldfp;-><init>(Ldfl;)V

    iput-object v0, p0, Ldfl;->bmB:Landroid/database/DataSetObserver;

    .line 170
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldfl;->bwv:Lcom/google/android/shared/search/Query;

    .line 171
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldfl;->bww:Lcom/google/android/shared/search/Query;

    .line 172
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldfl;->bwx:Lcom/google/android/shared/search/Query;

    .line 182
    iput-object p4, p0, Ldfl;->mGlobalSearchServices:Lcgh;

    .line 183
    iput-object p3, p0, Ldfl;->mCoreSearchServices:Lcfo;

    .line 184
    iput-object p5, p0, Ldfl;->mFactory:Lgpu;

    .line 185
    iput-object p1, p0, Ldfl;->aMD:Leqo;

    .line 186
    iput-object p2, p0, Ldfl;->aOw:Ljava/util/concurrent/Executor;

    .line 187
    iput-object p6, p0, Ldfl;->mSearchBoxLogging:Lcpd;

    .line 188
    iput-object p7, p0, Ldfl;->mClock:Lemp;

    .line 189
    iput-object p8, p0, Ldfl;->bwj:Ldgm;

    .line 190
    iput-object p9, p0, Ldfl;->mShouldQueryStrategy:Ldfz;

    .line 191
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Ldfl;->bws:Ljava/util/Locale;

    .line 192
    iput-object p10, p0, Ldfl;->bwk:Ldfe;

    .line 193
    iput-object p11, p0, Ldfl;->mSearchController:Lcjt;

    .line 194
    iget-object v0, p0, Ldfl;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nf()Ldda;

    move-result-object v0

    iput-object v0, p0, Ldfl;->mEventBus:Ldda;

    .line 195
    iget-object v0, p0, Ldfl;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 196
    iget-object v0, p0, Ldfl;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    iput-object v0, p0, Ldfl;->mServiceState:Ldcu;

    .line 197
    iput-object p12, p0, Ldfl;->bwl:Ldft;

    .line 198
    return-void
.end method

.method private Bm()Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    invoke-interface {v0}, Ldfk;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/search/shared/service/ClientConfig;->bUT:Lcom/google/android/search/shared/service/ClientConfig;

    goto :goto_0
.end method

.method private abr()V
    .locals 1

    .prologue
    .line 449
    invoke-static {}, Lenu;->auR()V

    .line 450
    iget-object v0, p0, Ldfl;->bwp:Ldem;

    if-eqz v0, :cond_0

    .line 451
    invoke-direct {p0}, Ldfl;->abu()V

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Ldfl;->bwp:Ldem;

    .line 455
    iget-object v0, p0, Ldfl;->mGlobalSearchServices:Lcgh;

    invoke-interface {v0}, Lcgh;->ER()Ldet;

    move-result-object v0

    invoke-virtual {v0}, Ldet;->abr()V

    .line 457
    :cond_0
    return-void
.end method

.method private abu()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Ldfl;->bwp:Ldem;

    invoke-virtual {v0}, Ldem;->close()V

    .line 524
    iget-object v0, p0, Ldfl;->aMD:Leqo;

    iget-object v1, p0, Ldfl;->bwh:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 525
    iget-object v0, p0, Ldfl;->aMD:Leqo;

    iget-object v1, p0, Ldfl;->bwg:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 526
    iget-object v0, p0, Ldfl;->bwn:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfl;->aMD:Leqo;

    iget-object v1, p0, Ldfl;->bwn:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 527
    :cond_0
    return-void
.end method

.method private abv()V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 658
    invoke-direct {p0}, Ldfl;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amY()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    sget-object v1, Ldem;->bvr:Ldem;

    invoke-interface {v0, v1}, Ldfk;->a(Ldem;)V

    .line 661
    :cond_0
    return-void

    .line 657
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private abw()Lcqn;
    .locals 6

    .prologue
    .line 668
    iget-object v0, p0, Ldfl;->bwy:Lcqn;

    if-nez v0, :cond_0

    .line 669
    new-instance v0, Lcqn;

    iget-object v1, p0, Ldfl;->mFactory:Lgpu;

    iget-object v2, p0, Ldfl;->mSearchController:Lcjt;

    invoke-virtual {v2}, Lcjt;->MV()Lcmw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgpu;->c(Lcmw;)Lcqe;

    move-result-object v1

    iget-object v2, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    iget-object v3, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    iget-object v4, p0, Ldfl;->mClock:Lemp;

    iget-object v5, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BK()Lcke;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcqn;-><init>(Lcqe;Lcrh;Lchk;Lemp;Lcke;)V

    iput-object v0, p0, Ldfl;->bwy:Lcqn;

    .line 675
    :cond_0
    iget-object v0, p0, Ldfl;->bwy:Lcqn;

    return-object v0
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 610
    const/4 v0, 0x1

    return v0
.end method

.method final a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 414
    iget v0, p0, Ldfl;->bwq:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 415
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Ldfl;->bwl:Ldft;

    invoke-interface {v0, p1, p2}, Ldft;->b(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;)Z

    move-result v0

    .line 418
    iget-object v1, p0, Ldfl;->bwi:Ldfu;

    invoke-virtual {v1, v0, v2}, Ldfu;->n(ZZ)V

    .line 423
    :goto_1
    iget-object v0, p0, Ldfl;->aMD:Leqo;

    iget-object v1, p0, Ldfl;->bwi:Ldfu;

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 424
    return-void

    :cond_0
    move v0, v2

    .line 414
    goto :goto_0

    .line 420
    :cond_1
    invoke-direct {p0}, Ldfl;->abw()Lcqn;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcqn;->b(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v0

    .line 421
    iget-object v2, p0, Ldfl;->bwi:Ldfu;

    invoke-virtual {v2, v0, v1}, Ldfu;->n(ZZ)V

    goto :goto_1
.end method

.method public final a(Lddb;)V
    .locals 5

    .prologue
    .line 615
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 618
    iget-object v0, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Ldfl;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0}, Lcpd;->Rg()V

    .line 623
    :cond_0
    iget-object v0, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 624
    iget-object v0, p0, Ldfl;->bwv:Lcom/google/android/shared/search/Query;

    if-eq v1, v0, :cond_3

    .line 625
    iget-object v0, p0, Ldfl;->mShouldQueryStrategy:Ldfz;

    iget-object v2, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-direct {p0}, Ldfl;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v3

    iget-object v4, p0, Ldfl;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Ldfz;->a(Lcom/google/android/search/core/state/QueryState;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;)Z

    move-result v0

    .line 627
    if-nez v0, :cond_1

    iget-object v0, p0, Ldfl;->bwv:Lcom/google/android/shared/search/Query;

    invoke-static {v0, v1}, Lcom/google/android/shared/search/Query;->k(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 629
    invoke-virtual {p0}, Ldfl;->abt()V

    .line 635
    :cond_2
    iput-object v1, p0, Ldfl;->bwv:Lcom/google/android/shared/search/Query;

    .line 637
    :cond_3
    iget-object v0, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 638
    iget-object v1, p0, Ldfl;->bww:Lcom/google/android/shared/search/Query;

    if-eq v0, v1, :cond_4

    .line 639
    invoke-direct {p0}, Ldfl;->abw()Lcqn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcqn;->v(Lcom/google/android/shared/search/Query;)V

    .line 640
    iput-object v0, p0, Ldfl;->bww:Lcom/google/android/shared/search/Query;

    .line 642
    :cond_4
    iget-object v1, p0, Ldfl;->bwx:Lcom/google/android/shared/search/Query;

    if-eq v1, v0, :cond_5

    iget-object v1, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 643
    invoke-direct {p0}, Ldfl;->abw()Lcqn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcqn;->u(Lcom/google/android/shared/search/Query;)V

    .line 644
    iput-object v0, p0, Ldfl;->bwx:Lcom/google/android/shared/search/Query;

    .line 646
    :cond_5
    return-void

    .line 627
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldfk;)V
    .locals 2

    .prologue
    .line 225
    invoke-static {}, Lenu;->auR()V

    .line 230
    iget-object v0, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Ldfl;->bwp:Ldem;

    .line 232
    iget-object v0, p0, Ldfl;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0}, Lcpd;->Rg()V

    .line 235
    :cond_0
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-ne v0, p1, :cond_1

    .line 279
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-eqz v0, :cond_2

    .line 241
    invoke-direct {p0}, Ldfl;->abv()V

    .line 244
    :cond_2
    iput-object p1, p0, Ldfl;->bwm:Ldfk;

    .line 246
    iget-boolean v0, p0, Ldfl;->cq:Z

    if-nez v0, :cond_3

    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfl;->cq:Z

    .line 251
    iget-object v0, p0, Ldfl;->bwj:Ldgm;

    invoke-virtual {v0}, Ldgm;->abU()V

    .line 254
    :cond_3
    iget-boolean v0, p0, Ldfl;->anR:Z

    if-eqz v0, :cond_4

    .line 256
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 257
    iget-object v1, p0, Ldfl;->bws:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 258
    iput-object v0, p0, Ldfl;->bws:Ljava/util/Locale;

    .line 259
    iget-object v0, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 264
    :cond_4
    iget-boolean v0, p0, Ldfl;->bwo:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Ldfl;->bwp:Ldem;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldfl;->bwp:Ldem;

    invoke-virtual {v0}, Ldem;->isDone()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 267
    iget-object v0, p0, Ldfl;->bwp:Ldem;

    invoke-virtual {v0}, Ldem;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 268
    iget-object v0, p0, Ldfl;->bwp:Ldem;

    invoke-virtual {v0}, Ldem;->abi()Ldem;

    move-result-object v0

    iput-object v0, p0, Ldfl;->bwp:Ldem;

    .line 270
    :cond_5
    iget v0, p0, Ldfl;->bwq:I

    if-nez v0, :cond_6

    iget-boolean v0, p0, Ldfl;->bwr:Z

    if-eqz v0, :cond_6

    .line 271
    iget-object v0, p0, Ldfl;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->No()V

    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfl;->bwr:Z

    .line 274
    :cond_6
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    iget-object v1, p0, Ldfl;->bwp:Ldem;

    invoke-interface {v0, v1}, Ldfk;->a(Ldem;)V

    .line 278
    :goto_1
    iget-object v0, p0, Ldfl;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->c(Lddc;)V

    goto :goto_0

    .line 276
    :cond_7
    invoke-virtual {p0}, Ldfl;->aaV()V

    goto :goto_1
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Ldfl;->bwj:Ldgm;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 681
    return-void
.end method

.method public final aaV()V
    .locals 3

    .prologue
    .line 341
    iget-object v0, p0, Ldfl;->aMD:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    invoke-virtual {p0}, Ldfl;->abt()V

    .line 351
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Ldfl;->aMD:Leqo;

    new-instance v1, Ldfq;

    const-string v2, "Update suggestions buffered"

    invoke-direct {v1, p0, v2}, Ldfq;-><init>(Ldfl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final aaW()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 470
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    iget-object v0, p0, Ldfl;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Ldfl;->bwt:J

    .line 472
    iput-boolean v7, p0, Ldfl;->bwo:Z

    .line 473
    iput-boolean v7, p0, Ldfl;->bwu:Z

    .line 474
    iget-object v0, p0, Ldfl;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 477
    iget-object v0, p0, Ldfl;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0, v2}, Lcpd;->i(Lcom/google/android/shared/search/Query;)V

    .line 478
    iget-object v0, p0, Ldfl;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abA()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Ldfl;->abw()Lcqn;

    move-result-object v0

    .line 482
    :goto_0
    iget-object v3, p0, Ldfl;->mGlobalSearchServices:Lcgh;

    invoke-interface {v3}, Lcgh;->ER()Ldet;

    move-result-object v3

    iget-object v4, p0, Ldfl;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->YQ()Lgnu;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, v4, Lgnu;->cQz:Lgny;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lgnu;->cQz:Lgny;

    invoke-virtual {v5}, Lgny;->aIk()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "chrome"

    iget-object v6, v4, Lgnu;->cQz:Lgny;

    invoke-virtual {v6}, Lgny;->aIl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    invoke-virtual {v5}, Lchk;->Iy()Z

    move-result v5

    if-nez v5, :cond_4

    :cond_0
    :goto_1
    invoke-virtual {v3, v2, v0, v1}, Ldet;->a(Lcom/google/android/shared/search/Query;Lcns;Ljava/lang/String;)Ldem;

    move-result-object v0

    .line 485
    iget-object v1, p0, Ldfl;->bwp:Ldem;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Ldfl;->abu()V

    :cond_1
    iput-object v0, p0, Ldfl;->bwp:Ldem;

    iget-object v1, p0, Ldfl;->aMD:Leqo;

    iget-object v2, p0, Ldfl;->bwh:Ljava/lang/Runnable;

    iget-object v3, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DD()Lcjs;

    move-result-object v3

    invoke-virtual {v3}, Lcjs;->Lo()I

    move-result v3

    int-to-long v4, v3

    invoke-interface {v1, v2, v4, v5}, Leqo;->a(Ljava/lang/Runnable;J)V

    iget-object v1, p0, Ldfl;->aMD:Leqo;

    iget-object v2, p0, Ldfl;->bwg:Ljava/lang/Runnable;

    iget-object v3, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-virtual {v3}, Lchk;->Fy()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Leqo;->a(Ljava/lang/Runnable;J)V

    invoke-virtual {v0}, Ldem;->abf()Ldfd;

    move-result-object v0

    new-instance v1, Ldfs;

    const-string v2, "Refresh suggestions"

    invoke-direct {v1, p0, v2, v0}, Ldfs;-><init>(Ldfl;Ljava/lang/String;Ldfd;)V

    iput-object v1, p0, Ldfl;->bwn:Ljava/lang/Runnable;

    iget-object v1, p0, Ldfl;->aMD:Leqo;

    iget-object v2, p0, Ldfl;->bwn:Ljava/lang/Runnable;

    iget-object v0, v0, Ldfd;->bvR:[J

    aget-wide v4, v0, v7

    invoke-interface {v1, v2, v4, v5}, Leqo;->a(Ljava/lang/Runnable;J)V

    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    iget-object v1, p0, Ldfl;->bwp:Ldem;

    invoke-interface {v0, v1}, Ldfk;->a(Ldem;)V

    .line 486
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    .line 478
    goto/16 :goto_0

    .line 482
    :cond_4
    iget-object v1, v4, Lgnu;->cQz:Lgny;

    invoke-virtual {v1}, Lgny;->aIj()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method final abt()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 362
    iget-boolean v0, p0, Ldfl;->cq:Z

    if-nez v0, :cond_1

    .line 364
    iput-boolean v6, p0, Ldfl;->bwo:Z

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 365
    :cond_1
    iget-object v0, p0, Ldfl;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Ldfl;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0}, Lcpd;->Rh()V

    .line 369
    iget-boolean v0, p0, Ldfl;->bwu:Z

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->Lp()I

    move-result v0

    int-to-long v0, v0

    .line 374
    iget-object v2, p0, Ldfl;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ldfl;->bwt:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 379
    iget-object v2, p0, Ldfl;->aMD:Leqo;

    iget-object v3, p0, Ldfl;->bwf:Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Leqo;->i(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Ldfl;->abr()V

    iput-boolean v6, p0, Ldfl;->bwu:Z

    iget-object v2, p0, Ldfl;->aMD:Leqo;

    iget-object v3, p0, Ldfl;->bwf:Ljava/lang/Runnable;

    invoke-interface {v2, v3, v0, v1}, Leqo;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final b(Ldfk;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 289
    invoke-static {}, Lenu;->auR()V

    .line 291
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-ne p1, v0, :cond_1

    .line 293
    iget-object v0, p0, Ldfl;->aMD:Leqo;

    iget-object v1, p0, Ldfl;->bwf:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 294
    iget-object v0, p0, Ldfl;->bwj:Ldgm;

    invoke-virtual {v0}, Ldgm;->abV()V

    .line 295
    iput-boolean v2, p0, Ldfl;->bwu:Z

    .line 297
    iget-object v0, p0, Ldfl;->bwp:Ldem;

    if-eqz v0, :cond_0

    .line 298
    invoke-direct {p0}, Ldfl;->abu()V

    .line 299
    iget-object v0, p0, Ldfl;->mGlobalSearchServices:Lcgh;

    invoke-interface {v0}, Lcgh;->ER()Ldet;

    move-result-object v0

    invoke-virtual {v0}, Ldet;->abr()V

    .line 300
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-eqz v0, :cond_0

    .line 301
    invoke-direct {p0}, Ldfl;->abv()V

    .line 304
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldfl;->bwm:Ldfk;

    .line 305
    iput-boolean v2, p0, Ldfl;->cq:Z

    .line 306
    iget-object v0, p0, Ldfl;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    .line 308
    :cond_1
    return-void
.end method

.method public final c(Lcom/google/android/shared/search/Suggestion;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 387
    invoke-static {}, Lenu;->auR()V

    .line 388
    iget-boolean v0, p0, Ldfl;->anR:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 389
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 391
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 393
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldfl;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abI()Ljava/lang/String;

    move-result-object v5

    .line 395
    :goto_1
    invoke-direct {p0}, Ldfl;->abr()V

    .line 396
    iget v0, p0, Ldfl;->bwq:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldfl;->bwq:I

    .line 397
    iget-object v6, p0, Ldfl;->aOw:Ljava/util/concurrent/Executor;

    new-instance v0, Ldfr;

    const-string v2, "removeSuggestionFromHistory"

    const/4 v1, 0x2

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Ldfr;-><init>(Ldfl;Ljava/lang/String;[ILcom/google/android/shared/search/Suggestion;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 410
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 389
    goto :goto_0

    .line 393
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    .line 407
    :cond_4
    const-string v0, "SuggestionsPresenter"

    const-string v2, "Attempt to remove non-web suggestion?. Just refresh"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v2, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 408
    invoke-virtual {p0}, Ldfl;->aaV()V

    goto :goto_2

    .line 397
    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public final cV()V
    .locals 4

    .prologue
    .line 206
    invoke-static {}, Lenu;->auR()V

    .line 208
    iget-boolean v0, p0, Ldfl;->anR:Z

    if-nez v0, :cond_2

    .line 209
    iget-object v0, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v0

    iget-object v1, p0, Ldfl;->bmB:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 211
    iget-object v0, p0, Ldfl;->mSources:Ldgh;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldfl;->mGlobalSearchServices:Lcgh;

    invoke-interface {v0}, Lcgh;->EN()Ldgh;

    move-result-object v0

    iput-object v0, p0, Ldfl;->mSources:Ldgh;

    iget-object v0, p0, Ldfl;->mSources:Ldgh;

    iget-object v1, p0, Ldfl;->bmB:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Ldgh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    iget-object v0, p0, Ldfl;->bwk:Ldfe;

    iget-object v1, v0, Ldfe;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    sget-boolean v2, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjA:Z

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcum;->c(Lcjs;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Ldfe;->mAppContext:Landroid/content/Context;

    iget-object v3, v0, Ldfe;->mAppContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->aj(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v2, v0, Ldfe;->mAppContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lctb;->a(Landroid/content/Context;Lcjs;)Lctb;

    move-result-object v1

    iput-object v1, v0, Ldfe;->bvW:Lctb;

    :cond_1
    iget-object v1, v0, Ldfe;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->Em()Lcha;

    move-result-object v1

    new-instance v2, Ldff;

    invoke-direct {v2, v0}, Ldff;-><init>(Ldfe;)V

    invoke-virtual {v1, v2}, Lcha;->b(Lemy;)V

    new-instance v2, Ldfg;

    invoke-direct {v2, v0}, Ldfg;-><init>(Ldfe;)V

    iput-object v2, v0, Ldfe;->bvV:Lchg;

    iget-object v0, v0, Ldfe;->bvV:Lchg;

    invoke-virtual {v1, v0}, Lcha;->a(Lchg;)V

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfl;->anR:Z

    .line 214
    :cond_2
    return-void
.end method

.method final e(IIZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 430
    invoke-static {}, Lenu;->auR()V

    .line 432
    iget v0, p0, Ldfl;->bwq:I

    add-int v2, p1, p2

    if-lt v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 433
    iget v0, p0, Ldfl;->bwq:I

    add-int v2, p1, p2

    sub-int/2addr v0, v2

    iput v0, p0, Ldfl;->bwq:I

    .line 434
    iget-object v0, p0, Ldfl;->bwm:Ldfk;

    if-eqz v0, :cond_2

    .line 435
    if-lez p2, :cond_0

    .line 436
    iget-object v0, p0, Ldfl;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->No()V

    .line 441
    :cond_0
    :goto_1
    if-eqz p3, :cond_3

    .line 442
    iget-object v0, p0, Ldfl;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 446
    :goto_2
    return-void

    .line 432
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 438
    :cond_2
    if-lez p2, :cond_0

    .line 439
    iput-boolean v1, p0, Ldfl;->bwr:Z

    goto :goto_1

    .line 444
    :cond_3
    invoke-virtual {p0}, Ldfl;->aaV()V

    goto :goto_2
.end method
