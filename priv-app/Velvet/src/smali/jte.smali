.class public final Ljte;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eDn:[Ljte;


# instance fields
.field private aez:I

.field public eDl:[Ljava/lang/String;

.field private ewG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Ljte;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljte;->ewG:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljte;->eDl:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljte;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljte;->eCz:I

    .line 57
    return-void
.end method

.method public static buq()[Ljte;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Ljte;->eDn:[Ljte;

    if-nez v0, :cond_1

    .line 18
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    sget-object v0, Ljte;->eDn:[Ljte;

    if-nez v0, :cond_0

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Ljte;

    sput-object v0, Ljte;->eDn:[Ljte;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_1
    sget-object v0, Ljte;->eDn:[Ljte;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljte;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljte;->ewG:Ljava/lang/String;

    iget v0, p0, Ljte;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljte;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljte;->eDl:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljte;->eDl:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljte;->eDl:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljte;->eDl:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 71
    iget v0, p0, Ljte;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x1

    iget-object v1, p0, Ljte;->ewG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 74
    :cond_0
    iget-object v0, p0, Ljte;->eDl:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljte;->eDl:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 75
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljte;->eDl:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 76
    iget-object v1, p0, Ljte;->eDl:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 77
    if-eqz v1, :cond_1

    .line 78
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 75
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 83
    return-void
.end method

.method public final getCanonicalName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ljte;->ewG:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 88
    iget v2, p0, Ljte;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 89
    const/4 v2, 0x1

    iget-object v3, p0, Ljte;->ewG:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    :cond_0
    iget-object v2, p0, Ljte;->eDl:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljte;->eDl:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 95
    :goto_0
    iget-object v4, p0, Ljte;->eDl:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 96
    iget-object v4, p0, Ljte;->eDl:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 97
    if-eqz v4, :cond_1

    .line 98
    add-int/lit8 v3, v3, 0x1

    .line 99
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 95
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_2
    add-int/2addr v0, v2

    .line 104
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 106
    :cond_3
    return v0
.end method

.method public final yO(Ljava/lang/String;)Ljte;
    .locals 1

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Ljte;->ewG:Ljava/lang/String;

    .line 40
    iget v0, p0, Ljte;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljte;->aez:I

    .line 41
    return-object p0
.end method
