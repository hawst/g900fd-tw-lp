.class public abstract Lcnd;
.super Lcnc;
.source "PG"

# interfaces
.implements Lcqe;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfo;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcnc;-><init>(Landroid/content/Context;Lcfo;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/shared/search/Query;Z)Ldfw;
.end method

.method protected final a(Lcom/google/android/shared/search/Query;Lemy;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcnd;->a(Lcom/google/android/shared/search/Query;Z)Ldfw;

    move-result-object v0

    invoke-interface {p2, v0}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public close()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public final hs(Ljava/lang/String;)Ldef;
    .locals 3
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 47
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    new-instance v0, Ldfw;

    invoke-virtual {p0}, Lcnd;->QJ()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ldfw;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 50
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcnd;->a(Lcom/google/android/shared/search/Query;Z)Ldfw;

    move-result-object v0

    goto :goto_0
.end method
