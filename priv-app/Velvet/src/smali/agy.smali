.class public final Lagy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private final XJ:Lags;

.field private final XK:Landroid/view/animation/DecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1967
    new-instance v0, Lags;

    const v1, 0x3eb33333    # 0.35f

    invoke-direct {v0, v1}, Lags;-><init>(F)V

    iput-object v0, p0, Lagy;->XJ:Lags;

    .line 1968
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lagy;->XK:Landroid/view/animation/DecelerateInterpolator;

    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 2

    .prologue
    .line 1971
    iget-object v0, p0, Lagy;->XK:Landroid/view/animation/DecelerateInterpolator;

    iget-object v1, p0, Lagy;->XJ:Lags;

    invoke-virtual {v1, p1}, Lags;->getInterpolation(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method
