.class public final Lclh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field final auh:Lbhi;

.field final baR:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final baS:Lcli;

.field private final baT:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lclh;->baR:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 36
    new-instance v0, Lcli;

    invoke-direct {v0, p0}, Lcli;-><init>(Lclh;)V

    iput-object v0, p0, Lclh;->baS:Lcli;

    .line 49
    iput-object p2, p0, Lclh;->baT:Ljava/util/concurrent/ScheduledExecutorService;

    .line 50
    new-instance v0, Lbhj;

    invoke-direct {v0, p1}, Lbhj;-><init>(Landroid/content/Context;)V

    sget-object v1, Lbbr;->auv:Lbgx;

    invoke-virtual {v0, v1}, Lbhj;->a(Lbgx;)Lbhj;

    move-result-object v0

    invoke-virtual {v0}, Lbhj;->yx()Lbhi;

    move-result-object v0

    iput-object v0, p0, Lclh;->auh:Lbhi;

    .line 53
    return-void
.end method


# virtual methods
.method public final Py()Ljava/util/concurrent/Future;
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lclh;->baT:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lclj;

    invoke-direct {v2, p0, v0}, Lclj;-><init>(Lclh;Livy;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 72
    return-object v0
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    .line 127
    const-string v0, "RecentContextApiClient (pulling context now)"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 129
    :try_start_0
    invoke-virtual {p0}, Lclh;->Py()Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 130
    const-string v2, "UsageInfo"

    invoke-virtual {p1, v2}, Letj;->lt(Ljava/lang/String;)V

    const-string v2, "Timestamp"

    invoke-virtual {p1, v2}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xs()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Letn;->a(Ljava/util/Date;)V

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xr()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "URI"

    invoke-virtual {p1, v2}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xr()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/appdatasearch/DocumentId;->getUri()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    const-string v2, "Pkg"

    invoke-virtual {p1, v2}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xr()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    const-string v0, "[Interrupted]"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 137
    :cond_1
    :goto_1
    return-void

    .line 135
    :catch_1
    move-exception v0

    const-string v0, "[Failed]"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final connect()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lclh;->baR:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 59
    iget-object v0, p0, Lclh;->baT:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lclh;->baS:Lcli;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 60
    return-void
.end method

.method public final disconnect()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lclh;->baR:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 64
    iget-object v0, p0, Lclh;->auh:Lbhi;

    invoke-interface {v0}, Lbhi;->disconnect()V

    .line 65
    return-void
.end method
