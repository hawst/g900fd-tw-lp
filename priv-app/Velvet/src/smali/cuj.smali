.class final Lcuj;
.super Lctt;
.source "PG"

# interfaces
.implements Lcts;


# instance fields
.field private final biL:Lctd;

.field private biO:Z

.field private final bjj:Lcud;

.field private final bjk:I

.field private final bjl:I

.field private final bjm:I

.field private final bjn:I

.field private final bjo:I

.field private bjp:J

.field private bjq:J

.field private final mCursor:Landroid/database/Cursor;

.field private final ro:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Lctd;)V
    .locals 3

    .prologue
    .line 355
    invoke-direct {p0}, Lctt;-><init>()V

    .line 356
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    .line 357
    iput-object p2, p0, Lcuj;->biL:Lctd;

    .line 358
    iget-object v0, p0, Lcuj;->biL:Lctd;

    iget-object v1, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iput-object v1, v0, Lctd;->mCursor:Landroid/database/Cursor;

    const-string v2, "mimetype"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lctd;->bis:I

    .line 359
    new-instance v0, Lcud;

    invoke-direct {v0}, Lcud;-><init>()V

    iput-object v0, p0, Lcuj;->bjj:Lcud;

    .line 360
    const-string v0, "contact_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuj;->bjk:I

    .line 361
    const-string v0, "lookup"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuj;->bjo:I

    .line 362
    const-string v0, "photo_thumb_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuj;->ro:I

    .line 363
    const-string v0, "display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuj;->bjl:I

    .line 364
    const-string v0, "phonetic_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuj;->bjm:I

    .line 365
    const-string v0, "times_contacted"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuj;->bjn:I

    .line 367
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcuj;->bjp:J

    .line 368
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcuj;->biO:Z

    .line 369
    return-void
.end method

.method private j(Landroid/database/Cursor;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 410
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 419
    :goto_0
    return v0

    .line 411
    :catch_0
    move-exception v1

    .line 416
    const-string v2, "IcingCPHelper"

    const-string v3, "moveToNext threw an exception"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 417
    const v1, 0xa700f4

    invoke-static {v1}, Lhwt;->lx(I)V

    .line 418
    iput-boolean v0, p0, Lcuj;->biO:Z

    goto :goto_0
.end method


# virtual methods
.method public final SS()Z
    .locals 1

    .prologue
    .line 430
    iget-boolean v0, p0, Lcuj;->biO:Z

    return v0
.end method

.method protected final synthetic ST()Ljava/lang/Object;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 337
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    iget-object v1, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, v1}, Lcuj;->j(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-wide v2, p0, Lcuj;->bjq:J

    iget-object v1, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    invoke-static {v1}, Lcub;->i(Landroid/database/Cursor;)J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcuj;->bjq:J

    iget-object v1, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iget v2, p0, Lcuj;->bjk:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget-wide v4, p0, Lcuj;->bjp:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    iget-wide v4, p0, Lcuj;->bjp:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcuj;->bjj:Lcud;

    invoke-virtual {v0}, Lcud;->SX()Lctp;

    move-result-object v0

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    invoke-virtual {v1}, Lcud;->clear()V

    :cond_0
    iput-wide v2, p0, Lcuj;->bjp:J

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    iget-wide v2, p0, Lcuj;->bjp:J

    iput-wide v2, v1, Lcud;->biV:J

    iget-object v1, v1, Lcud;->biR:Lcue;

    iput-wide v2, v1, Lcue;->biV:J

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    iget-object v2, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iget v3, p0, Lcuj;->bjo:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcud;->biR:Lcue;

    iput-object v2, v1, Lcue;->bjd:Ljava/lang/String;

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    iget-object v2, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iget v3, p0, Lcuj;->ro:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcud;->biR:Lcue;

    iput-object v2, v1, Lcue;->bje:Ljava/lang/String;

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    iget-object v2, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iget v3, p0, Lcuj;->bjl:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcud;->biR:Lcue;

    iput-object v2, v1, Lcue;->bjf:Ljava/lang/String;

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    iget-object v2, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iget v3, p0, Lcuj;->bjm:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcud;->biR:Lcue;

    iput-object v2, v1, Lcue;->bjg:Ljava/lang/String;

    iget-object v1, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    iget v2, p0, Lcuj;->bjn:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    int-to-long v2, v1

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    iget-object v1, v1, Lcud;->biR:Lcue;

    iput-wide v2, v1, Lcue;->bjh:J

    :cond_1
    move-object v1, v0

    iget-object v2, p0, Lcuj;->biL:Lctd;

    iget-object v3, p0, Lcuj;->bjj:Lcud;

    iget-object v0, v2, Lctd;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    const-string v4, "Cursor must be set"

    invoke-static {v0, v4}, Lifv;->d(ZLjava/lang/Object;)V

    iget-object v0, v2, Lctd;->mCursor:Landroid/database/Cursor;

    iget v4, v2, Lctd;->bis:I

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v2, Lctd;->biq:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctf;

    if-eqz v0, :cond_2

    iget-object v2, v2, Lctd;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v3, v2}, Lctf;->a(Lcud;Landroid/database/Cursor;)V

    :cond_2
    move-object v0, v1

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    if-nez v0, :cond_5

    iget-object v1, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-wide v2, p0, Lcuj;->bjp:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcuj;->bjj:Lcud;

    invoke-virtual {v0}, Lcud;->SX()Lctp;

    move-result-object v0

    iget-object v1, p0, Lcuj;->bjj:Lcud;

    invoke-virtual {v1}, Lcud;->clear()V

    iput-wide v6, p0, Lcuj;->bjp:J

    :cond_5
    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcuj;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 426
    return-void
.end method
