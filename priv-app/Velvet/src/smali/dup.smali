.class public final Ldup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static ay(Landroid/os/Parcel;)Lcom/google/android/search/shared/actions/ParcelableVoiceAction;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    new-instance v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    invoke-direct {v0, v2}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;-><init>(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 66
    :goto_0
    return-object v0

    .line 58
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 59
    const-class v1, Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    invoke-static {v1}, Lifv;->gY(Z)V

    .line 61
    const-string v1, "CREATOR"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 62
    instance-of v1, v0, Landroid/os/Parcelable$Creator;

    invoke-static {v1}, Lifv;->gY(Z)V

    .line 64
    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 65
    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    .line 66
    new-instance v1, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;-><init>(Lcom/google/android/search/shared/actions/VoiceAction;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t unparcel VoiceAction: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-static {p1}, Ldup;->ay(Landroid/os/Parcel;)Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    new-array v0, p1, [Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    return-object v0
.end method
