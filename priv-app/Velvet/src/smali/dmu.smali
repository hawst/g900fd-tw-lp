.class public final Ldmu;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(ZZZZ)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 45
    if-nez p3, :cond_3

    .line 54
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 57
    :goto_0
    if-eqz v0, :cond_2

    .line 59
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apl()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 87
    :goto_1
    return-object v0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :cond_2
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apk()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1

    .line 68
    :cond_3
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 69
    if-eqz p0, :cond_4

    sget-object v0, Lcgg;->aVs:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 71
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apl()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1

    .line 73
    :cond_4
    if-eqz p2, :cond_5

    .line 75
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apl()Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1

    .line 80
    :cond_5
    if-eqz p0, :cond_6

    .line 82
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apk()Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1

    .line 87
    :cond_6
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apk()Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(ZZZZZ)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 181
    if-nez p4, :cond_3

    .line 187
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    if-nez p3, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    move v0, v1

    .line 193
    :cond_2
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/search/Query;->eR(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    .line 198
    :cond_3
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 199
    :cond_4
    if-nez p2, :cond_5

    if-eqz p0, :cond_6

    sget-object v2, Lcgg;->aVs:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_6

    .line 201
    :cond_5
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->eR(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 203
    :cond_6
    if-eqz p3, :cond_7

    .line 205
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->eR(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 209
    :cond_7
    if-nez p1, :cond_8

    .line 211
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->eR(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_8
    if-eqz p0, :cond_9

    .line 216
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/search/Query;->eR(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_9
    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v0}, Lcom/google/android/shared/search/Query;->eR(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(ZZZZZZ)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 122
    if-nez p4, :cond_3

    .line 128
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    if-nez p3, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    move v0, v1

    .line 135
    :cond_2
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    .line 140
    :cond_3
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 141
    :cond_4
    if-nez p2, :cond_5

    if-eqz p0, :cond_6

    if-nez p5, :cond_6

    .line 144
    :cond_5
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 146
    :cond_6
    if-eqz p3, :cond_7

    .line 148
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 152
    :cond_7
    if-nez p1, :cond_8

    .line 154
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_8
    if-eqz p0, :cond_9

    .line 159
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_9
    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v0}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->hz(I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0
.end method
