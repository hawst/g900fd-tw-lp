.class public final Ldru;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic bHA:Lcom/google/android/search/searchplate/RecognizerView;


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/RecognizerView;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Ldru;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 284
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 285
    iget-object v1, p0, Ldru;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-static {v1}, Lcom/google/android/search/searchplate/RecognizerView;->d(Lcom/google/android/search/searchplate/RecognizerView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 286
    iget-object v1, p0, Ldru;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-static {v1}, Lcom/google/android/search/searchplate/RecognizerView;->d(Lcom/google/android/search/searchplate/RecognizerView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 287
    return-void
.end method
