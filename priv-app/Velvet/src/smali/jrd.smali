.class public final Ljrd;
.super Ljsl;
.source "PG"


# static fields
.field public static final eAe:Ljsm;


# instance fields
.field private aez:I

.field private eAf:I

.field private eAg:I

.field public eqD:Lier;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2695
    const/16 v0, 0xb

    const-class v1, Ljrd;

    const v2, 0x1f5fe4e2

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljrd;->eAe:Ljsm;

    .line 2701
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2749
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2750
    iput v0, p0, Ljrd;->aez:I

    iput-object v1, p0, Ljrd;->eqD:Lier;

    iput v0, p0, Ljrd;->eAf:I

    iput v0, p0, Ljrd;->eAg:I

    iput-object v1, p0, Ljrd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrd;->eCz:I

    .line 2751
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2688
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljrd;->eqD:Lier;

    if-nez v0, :cond_1

    new-instance v0, Lier;

    invoke-direct {v0}, Lier;-><init>()V

    iput-object v0, p0, Ljrd;->eqD:Lier;

    :cond_1
    iget-object v0, p0, Ljrd;->eqD:Lier;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrd;->eAf:I

    iget v0, p0, Ljrd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrd;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrd;->eAg:I

    iget v0, p0, Ljrd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrd;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2766
    iget-object v0, p0, Ljrd;->eqD:Lier;

    if-eqz v0, :cond_0

    .line 2767
    const/4 v0, 0x1

    iget-object v1, p0, Ljrd;->eqD:Lier;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2769
    :cond_0
    iget v0, p0, Ljrd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 2770
    const/4 v0, 0x2

    iget v1, p0, Ljrd;->eAf:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2772
    :cond_1
    iget v0, p0, Ljrd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 2773
    const/4 v0, 0x3

    iget v1, p0, Ljrd;->eAg:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2775
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2776
    return-void
.end method

.method public final bsY()I
    .locals 1

    .prologue
    .line 2714
    iget v0, p0, Ljrd;->eAf:I

    return v0
.end method

.method public final bsZ()Z
    .locals 1

    .prologue
    .line 2722
    iget v0, p0, Ljrd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bta()I
    .locals 1

    .prologue
    .line 2733
    iget v0, p0, Ljrd;->eAg:I

    return v0
.end method

.method public final btb()Z
    .locals 1

    .prologue
    .line 2741
    iget v0, p0, Ljrd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2780
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2781
    iget-object v1, p0, Ljrd;->eqD:Lier;

    if-eqz v1, :cond_0

    .line 2782
    const/4 v1, 0x1

    iget-object v2, p0, Ljrd;->eqD:Lier;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2785
    :cond_0
    iget v1, p0, Ljrd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 2786
    const/4 v1, 0x2

    iget v2, p0, Ljrd;->eAf:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2789
    :cond_1
    iget v1, p0, Ljrd;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 2790
    const/4 v1, 0x3

    iget v2, p0, Ljrd;->eAg:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2793
    :cond_2
    return v0
.end method

.method public final rG(I)Ljrd;
    .locals 1

    .prologue
    .line 2717
    iput p1, p0, Ljrd;->eAf:I

    .line 2718
    iget v0, p0, Ljrd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrd;->aez:I

    .line 2719
    return-object p0
.end method

.method public final rH(I)Ljrd;
    .locals 1

    .prologue
    .line 2736
    iput p1, p0, Ljrd;->eAg:I

    .line 2737
    iget v0, p0, Ljrd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrd;->aez:I

    .line 2738
    return-object p0
.end method
