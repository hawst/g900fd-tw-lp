.class public final Lgho;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bcO:[Ljava/lang/String;


# instance fields
.field private cKo:Lghp;

.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_last_updated_timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lgho;->bcO:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lgho;->mContentResolver:Landroid/content/ContentResolver;

    .line 32
    new-instance v0, Lghp;

    invoke-direct {v0}, Lghp;-><init>()V

    iput-object v0, p0, Lgho;->cKo:Lghp;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lghp;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lgho;->cKo:Lghp;

    .line 38
    return-void
.end method

.method public final i(JI)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 73
    const/16 v0, 0x64

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 74
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "limit"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 82
    :try_start_0
    iget-object v0, p0, Lgho;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lgho;->bcO:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "contact_last_updated_timestamp DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lgho;->cKo:Lghp;

    invoke-virtual {v1, v0}, Lghp;->m(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v6

    .line 89
    :goto_1
    iget-object v0, p0, Lgho;->cKo:Lghp;

    iget-object v0, v0, Lghp;->bxW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 90
    iget-object v0, p0, Lgho;->cKo:Lghp;

    iget-object v0, v0, Lghp;->bxW:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghn;

    .line 95
    iget-wide v4, v0, Lghn;->cKn:J

    cmp-long v3, v4, p1

    if-ltz v3, :cond_0

    .line 98
    iget-object v0, v0, Lghn;->mName:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 84
    :catch_0
    move-exception v0

    .line 85
    const-string v1, "FreshContactLookup"

    const-string v2, "Failed to query fresh contacts"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :cond_0
    return-object v2
.end method
