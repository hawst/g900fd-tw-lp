.class public final Lfvk;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 34
    return-void
.end method

.method public static a(Landroid/content/Context;Lixf;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    const v0, 0x7f0a022d

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lixf;->aZF()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lixf;->aZP()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lixf;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 122
    const v0, 0x7f0a022e

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lixf;->aZC()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lixf;->aZD()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lixf;->aZE()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;Lixf;)Ljava/lang/String;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 130
    invoke-virtual {p1}, Lixf;->aZM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const v0, 0x7f0a0202

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lixf;->aZL()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 138
    :goto_0
    return-object v0

    .line 134
    :cond_0
    invoke-virtual {p1}, Lixf;->aZR()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p1}, Lixf;->aZQ()J

    move-result-wide v2

    const v1, 0x7f0a0214

    invoke-static {p0, v2, v3, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {p0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " \u00b7 "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    aput-object v0, v4, v6

    const v0, 0x80016

    invoke-static {p0, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    .line 38
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 40
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSO:Lixe;

    .line 41
    iget-object v3, v0, Lixe;->dMO:Lixf;

    .line 43
    new-instance v1, Lftn;

    invoke-static {p1, v3}, Lfvk;->a(Landroid/content/Context;Lixf;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lftn;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3}, Lfvk;->c(Landroid/content/Context;Lixf;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lftn;->ceJ:Ljava/lang/String;

    invoke-static {p1, v3}, Lfvk;->b(Landroid/content/Context;Lixf;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lftn;->cBj:Ljava/lang/String;

    const v4, 0x7f0200e0

    iput v4, v1, Lftn;->cBL:I

    invoke-virtual {v1}, Lftn;->aDh()Lanh;

    move-result-object v1

    .line 49
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v1, v0, Lixe;->dMM:Ljcn;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lixe;->dMM:Ljcn;

    invoke-virtual {v1}, Ljcn;->qG()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Lfrt;

    iget-object v4, v0, Lixe;->dMM:Ljcn;

    invoke-virtual {v4}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lfrt;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lixf;->aZV()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lfrt;->cBn:Ljava/lang/String;

    invoke-virtual {v3}, Lixf;->ow()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lfrt;->cBo:Ljava/lang/String;

    invoke-virtual {v1}, Lfrt;->aDh()Lanh;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    iget-object v4, v0, Lixe;->dMO:Lixf;

    new-instance v5, Laou;

    invoke-direct {v5}, Laou;-><init>()V

    const v0, 0x7f0a0233

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    new-instance v6, Lfye;

    invoke-direct {v6, p1, v9}, Lfye;-><init>(Landroid/content/Context;I)V

    const v0, 0x7f0a021e

    invoke-virtual {v4}, Lixf;->aZG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1, v8}, Lfye;->a(ILjava/lang/String;I)Lfye;

    invoke-virtual {v4}, Lixf;->aZJ()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v4}, Lixf;->aZK()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {v4}, Lixf;->aZI()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "\u2014"

    :goto_0
    invoke-virtual {v4}, Lixf;->getGroup()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "\u2014"

    :goto_1
    const v7, 0x7f0a0225

    invoke-virtual {v6, v7, v0}, Lfye;->l(ILjava/lang/String;)Lfye;

    move-result-object v0

    const v7, 0x7f0a0226

    invoke-virtual {v0, v7, v1}, Lfye;->l(ILjava/lang/String;)Lfye;

    :cond_2
    const v0, 0x7f0a0222

    invoke-virtual {v4}, Lixf;->aZW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1, v8}, Lfye;->a(ILjava/lang/String;I)Lfye;

    move-result-object v0

    const v1, 0x7f0a0221

    invoke-virtual {v4}, Lixf;->aZU()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v1, v7, v8}, Lfye;->a(ILjava/lang/String;I)Lfye;

    move-result-object v0

    const v1, 0x7f0a0223

    invoke-virtual {v4}, Lixf;->aZS()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v1, v7, v8}, Lfye;->a(ILjava/lang/String;I)Lfye;

    move-result-object v0

    const v1, 0x7f0a022a

    invoke-virtual {v4}, Lixf;->aZT()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v1, v7, v9}, Lfye;->a(ILjava/lang/String;I)Lfye;

    iget-object v0, v6, Lfye;->cDl:Lfyg;

    if-eqz v0, :cond_3

    invoke-virtual {v6}, Lfye;->aDQ()V

    :cond_3
    iget-object v0, v6, Lfye;->cDj:Ljava/util/LinkedList;

    const/4 v1, 0x0

    new-array v1, v1, [Laow;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laow;

    iput-object v0, v5, Laou;->akm:[Laow;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    invoke-virtual {v4}, Lixf;->aZO()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lfsa;

    const/16 v6, 0x2a

    invoke-direct {v1, v6}, Lfsa;-><init>(I)V

    const v6, 0x7f0201e1

    invoke-virtual {v1, v6}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    invoke-virtual {v4}, Lixf;->aZN()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    :cond_4
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    iput-object v5, v0, Lanh;->agN:Laou;

    .line 59
    if-eqz v0, :cond_5

    .line 60
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_5
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v3, Lixf;->dMX:[Ljbg;

    const/16 v3, 0x29

    invoke-static {p1, v0, v1, v3}, Lfsx;->a(Landroid/content/Context;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfvk;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 66
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 67
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 68
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 69
    return-object v1

    .line 58
    :cond_6
    invoke-virtual {v4}, Lixf;->aZI()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v4}, Lixf;->getGroup()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method
