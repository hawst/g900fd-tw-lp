.class final Lfkj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic cjN:Landroid/app/Activity;

.field private synthetic ctI:Lfmt;

.field private synthetic ctJ:Landroid/view/View;

.field private synthetic ctT:Lfkf;

.field private synthetic ctU:Z


# direct methods
.method constructor <init>(Lfkf;Landroid/view/View;Lfmt;Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lfkj;->ctT:Lfkf;

    iput-object p2, p0, Lfkj;->ctJ:Landroid/view/View;

    iput-object p3, p0, Lfkj;->ctI:Lfmt;

    iput-object p4, p0, Lfkj;->cjN:Landroid/app/Activity;

    iput-boolean p5, p0, Lfkj;->ctU:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 289
    iget-object v0, p0, Lfkj;->ctT:Lfkf;

    invoke-virtual {v0}, Lfkf;->aAB()I

    move-result v0

    .line 290
    packed-switch v0, :pswitch_data_0

    .line 303
    const-string v1, "InferredEventEntryAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected step: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :goto_0
    return-void

    .line 292
    :pswitch_0
    iget-object v0, p0, Lfkj;->ctJ:Landroid/view/View;

    const v1, 0x7f110229

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lfkj;->ctT:Lfkf;

    iget-object v1, p0, Lfkj;->ctJ:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lfkf;->x(Landroid/view/View;I)V

    .line 294
    iget-object v0, p0, Lfkj;->ctI:Lfmt;

    iget-object v1, p0, Lfkj;->ctT:Lfkf;

    invoke-virtual {v1}, Lfkf;->getEntry()Lizj;

    move-result-object v1

    const/16 v2, 0xf0

    invoke-interface {v0, v1, v2, v3, v3}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    goto :goto_0

    .line 299
    :pswitch_1
    iget-object v0, p0, Lfkj;->ctT:Lfkf;

    iget-object v1, p0, Lfkj;->cjN:Landroid/app/Activity;

    iget-object v2, p0, Lfkj;->ctI:Lfmt;

    const/16 v3, 0xa7

    iget-boolean v4, p0, Lfkj;->ctU:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lfkf;->a(Landroid/content/Context;Lfmt;IZ)V

    goto :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
