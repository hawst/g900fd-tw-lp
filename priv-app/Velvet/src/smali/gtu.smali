.class public final Lgtu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgod;


# instance fields
.field private cRg:Ljava/lang/String;

.field private final cWg:Lq;

.field private cWh:Lefu;

.field private cWi:Lgop;

.field private cWj:Lgub;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/extradex/ExtraDexHostActivity;Lq;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p2, p0, Lgtu;->cWg:Lq;

    .line 44
    return-void
.end method


# virtual methods
.method public final aIp()Lgoh;
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lgtu;->cWh:Lefu;

    iget-object v1, p0, Lgtu;->cRg:Ljava/lang/String;

    iget-object v2, p0, Lgtu;->cWg:Lq;

    invoke-interface {v0, v1, v2}, Lefu;->a(Ljava/lang/String;Lq;)Lgoh;

    move-result-object v0

    return-object v0
.end method

.method public final aIq()Lgoj;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lgtu;->cWj:Lgub;

    return-object v0
.end method

.method public final load(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 48
    invoke-static {p1}, Lefy;->kO(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 49
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lgtx;->nC(Ljava/lang/String;)Lgor;

    move-result-object v2

    .line 51
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v3

    .line 53
    invoke-virtual {v3, v2}, Lgtx;->d(Lgor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefu;

    iput-object v0, p0, Lgtu;->cWh:Lefu;

    .line 54
    invoke-virtual {v3, v2}, Lgtx;->e(Lgor;)Lgop;

    move-result-object v0

    iput-object v0, p0, Lgtu;->cWi:Lgop;

    .line 55
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgtu;->cRg:Ljava/lang/String;

    .line 56
    new-instance v0, Lgoi;

    iget-object v1, p0, Lgtu;->cWi:Lgop;

    iget-object v1, v1, Lgop;->cRn:Ljava/util/jar/JarFile;

    iget-object v2, p0, Lgtu;->cWi:Lgop;

    iget-object v2, v2, Lgop;->cRe:Ljava/lang/ClassLoader;

    invoke-direct {v0, v1, v2}, Lgoi;-><init>(Ljava/util/jar/JarFile;Ljava/lang/ClassLoader;)V

    .line 57
    new-instance v1, Lgub;

    iget-object v2, p0, Lgtu;->cWg:Lq;

    invoke-direct {v1, v0}, Lgub;-><init>(Lgoi;)V

    iput-object v1, p0, Lgtu;->cWj:Lgub;

    .line 58
    return-void
.end method
