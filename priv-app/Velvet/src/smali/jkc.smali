.class public final Ljkc;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile epN:[Ljkc;


# instance fields
.field private aez:I

.field private agv:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 74
    iput v0, p0, Ljkc;->aez:I

    iput v0, p0, Ljkc;->agv:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljkc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkc;->eCz:I

    .line 75
    return-void
.end method

.method public static boh()[Ljkc;
    .locals 2

    .prologue
    .line 41
    sget-object v0, Ljkc;->epN:[Ljkc;

    if-nez v0, :cond_1

    .line 42
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 44
    :try_start_0
    sget-object v0, Ljkc;->epN:[Ljkc;

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Ljkc;

    sput-object v0, Ljkc;->epN:[Ljkc;

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :cond_1
    sget-object v0, Ljkc;->epN:[Ljkc;

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 28
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljkc;->agv:I

    iget v0, p0, Ljkc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkc;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Ljkc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    iget v1, p0, Ljkc;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 91
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 92
    return-void
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Ljkc;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 96
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 97
    iget v1, p0, Ljkc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 98
    const/4 v1, 0x1

    iget v2, p0, Ljkc;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_0
    return v0
.end method

.method public final qp(I)Ljkc;
    .locals 1

    .prologue
    .line 60
    iput p1, p0, Ljkc;->agv:I

    .line 61
    iget v0, p0, Ljkc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkc;->aez:I

    .line 62
    return-object p0
.end method
