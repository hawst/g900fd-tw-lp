.class public final Lhqv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgco;
.implements Lgcz;


# instance fields
.field private final cFT:Lgda;

.field private final cGq:Lgky;

.field private final dow:Ljava/util/List;


# direct methods
.method public constructor <init>(Lgky;Lgda;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhqv;->dow:Ljava/util/List;

    .line 41
    iput-object p1, p0, Lhqv;->cGq:Lgky;

    .line 42
    iput-object p2, p0, Lhqv;->cFT:Lgda;

    .line 43
    return-void
.end method

.method private a(Ljvr;)V
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p1}, Ljvr;->getEventType()I

    move-result v0

    if-nez v0, :cond_1

    .line 76
    iget-object v0, p0, Lhqv;->cFT:Lgda;

    invoke-interface {v0}, Lgda;->aEK()V

    .line 81
    :cond_0
    :goto_0
    iget-object v0, p0, Lhqv;->cGq:Lgky;

    invoke-interface {v0, p1}, Lgky;->c(Ljvr;)V

    .line 82
    iget-object v0, p0, Lhqv;->cGq:Lgky;

    invoke-interface {v0, p1}, Lgky;->d(Ljvr;)V

    .line 83
    return-void

    .line 77
    :cond_1
    invoke-virtual {p1}, Ljvr;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Ljvr;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 79
    :cond_2
    iget-object v0, p0, Lhqv;->cFT:Lgda;

    invoke-interface {v0}, Lgda;->onEndOfSpeech()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lehn;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 47
    invoke-virtual {p1}, Lehn;->ata()I

    move-result v0

    .line 50
    invoke-virtual {p1}, Lehn;->asO()Ljvr;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Ljvr;->getEventType()I

    move-result v2

    if-ne v0, v4, :cond_3

    if-nez v2, :cond_2

    const/16 v2, 0xb2

    invoke-static {v2}, Lege;->ht(I)V

    .line 53
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljvr;->getEventType()I

    move-result v2

    if-nez v2, :cond_5

    .line 56
    invoke-direct {p0, v1}, Lhqv;->a(Ljvr;)V

    .line 69
    :cond_1
    :goto_1
    return-void

    .line 51
    :cond_2
    if-ne v2, v4, :cond_0

    const/16 v2, 0xb4

    invoke-static {v2}, Lege;->ht(I)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    if-nez v2, :cond_4

    const/16 v2, 0xb3

    invoke-static {v2}, Lege;->ht(I)V

    goto :goto_0

    :cond_4
    if-ne v2, v4, :cond_0

    const/16 v2, 0xb5

    invoke-static {v2}, Lege;->ht(I)V

    goto :goto_0

    .line 60
    :cond_5
    iget-object v2, p0, Lhqv;->cFT:Lgda;

    invoke-interface {v2, v0}, Lgda;->jY(I)I

    move-result v0

    .line 61
    if-nez v0, :cond_6

    .line 63
    invoke-direct {p0, v1}, Lhqv;->a(Ljvr;)V

    goto :goto_1

    .line 64
    :cond_6
    if-ne v0, v4, :cond_1

    .line 66
    iget-object v0, p0, Lhqv;->dow:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lhqv;->cGq:Lgky;

    invoke-interface {v0, v1}, Lgky;->c(Ljvr;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Lehu;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lehn;

    invoke-virtual {p0, p1}, Lhqv;->a(Lehn;)V

    return-void
.end method

.method public final aES()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lhqv;->dow:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehn;

    .line 110
    invoke-virtual {v0}, Lehn;->asO()Ljvr;

    move-result-object v0

    invoke-direct {p0, v0}, Lhqv;->a(Ljvr;)V

    goto :goto_0

    .line 112
    :cond_0
    return-void
.end method
