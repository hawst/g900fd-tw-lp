.class public final Ljxz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eIt:I

.field private eLA:J

.field private eLB:J

.field private eLC:I

.field private eLw:I

.field private eLx:I

.field private eLy:I

.field private eLz:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 179
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 180
    iput v0, p0, Ljxz;->aez:I

    iput v0, p0, Ljxz;->eLw:I

    iput v0, p0, Ljxz;->eLx:I

    iput v0, p0, Ljxz;->eLy:I

    iput v0, p0, Ljxz;->eLz:I

    iput-wide v2, p0, Ljxz;->eLA:J

    iput-wide v2, p0, Ljxz;->eLB:J

    iput v0, p0, Ljxz;->eIt:I

    iput v0, p0, Ljxz;->eLC:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljxz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxz;->eCz:I

    .line 181
    return-void
.end method

.method public static aD([B)Ljxz;
    .locals 1

    .prologue
    .line 327
    new-instance v0, Ljxz;

    invoke-direct {v0}, Ljxz;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljxz;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxz;->eLw:I

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxz;->eLx:I

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxz;->eLy:I

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxz;->eLz:I

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljxz;->eLA:J

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljxz;->eLB:J

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxz;->eIt:I

    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxz;->eLC:I

    iget v0, p0, Ljxz;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljxz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 201
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x1

    iget v1, p0, Ljxz;->eLw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 204
    :cond_0
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 205
    const/4 v0, 0x2

    iget v1, p0, Ljxz;->eLx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 207
    :cond_1
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 208
    const/4 v0, 0x3

    iget v1, p0, Ljxz;->eLy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 210
    :cond_2
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 211
    const/4 v0, 0x4

    iget v1, p0, Ljxz;->eLz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 213
    :cond_3
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 214
    const/4 v0, 0x5

    iget-wide v2, p0, Ljxz;->eLA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 216
    :cond_4
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 217
    const/4 v0, 0x6

    iget-wide v2, p0, Ljxz;->eLB:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 219
    :cond_5
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 220
    const/4 v0, 0x7

    iget v1, p0, Ljxz;->eIt:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 222
    :cond_6
    iget v0, p0, Ljxz;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 223
    const/16 v0, 0x8

    iget v1, p0, Ljxz;->eLC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 225
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 226
    return-void
.end method

.method public final bvR()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Ljxz;->eLw:I

    return v0
.end method

.method public final bvS()Z
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Ljxz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bvT()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Ljxz;->eLC:I

    return v0
.end method

.method public final bvU()Z
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Ljxz;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dO(J)Ljxz;
    .locals 1

    .prologue
    .line 109
    iput-wide p1, p0, Ljxz;->eLA:J

    .line 110
    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljxz;->aez:I

    .line 111
    return-object p0
.end method

.method public final dP(J)Ljxz;
    .locals 1

    .prologue
    .line 128
    iput-wide p1, p0, Ljxz;->eLB:J

    .line 129
    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljxz;->aez:I

    .line 130
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 230
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 231
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 232
    const/4 v1, 0x1

    iget v2, p0, Ljxz;->eLw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_0
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 236
    const/4 v1, 0x2

    iget v2, p0, Ljxz;->eLx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_1
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 240
    const/4 v1, 0x3

    iget v2, p0, Ljxz;->eLy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    :cond_2
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 244
    const/4 v1, 0x4

    iget v2, p0, Ljxz;->eLz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_3
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 248
    const/4 v1, 0x5

    iget-wide v2, p0, Ljxz;->eLA:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    :cond_4
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 252
    const/4 v1, 0x6

    iget-wide v2, p0, Ljxz;->eLB:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_5
    iget v1, p0, Ljxz;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 256
    const/4 v1, 0x7

    iget v2, p0, Ljxz;->eIt:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_6
    iget v1, p0, Ljxz;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 260
    const/16 v1, 0x8

    iget v2, p0, Ljxz;->eLC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_7
    return v0
.end method

.method public final sW(I)Ljxz;
    .locals 1

    .prologue
    .line 33
    iput p1, p0, Ljxz;->eLw:I

    .line 34
    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxz;->aez:I

    .line 35
    return-object p0
.end method

.method public final sX(I)Ljxz;
    .locals 1

    .prologue
    .line 52
    iput p1, p0, Ljxz;->eLx:I

    .line 53
    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxz;->aez:I

    .line 54
    return-object p0
.end method

.method public final sY(I)Ljxz;
    .locals 1

    .prologue
    .line 71
    iput p1, p0, Ljxz;->eLy:I

    .line 72
    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljxz;->aez:I

    .line 73
    return-object p0
.end method

.method public final sZ(I)Ljxz;
    .locals 1

    .prologue
    .line 90
    iput p1, p0, Ljxz;->eLz:I

    .line 91
    iget v0, p0, Ljxz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljxz;->aez:I

    .line 92
    return-object p0
.end method

.method public final ta(I)Ljxz;
    .locals 1

    .prologue
    .line 166
    iput p1, p0, Ljxz;->eLC:I

    .line 167
    iget v0, p0, Ljxz;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljxz;->aez:I

    .line 168
    return-object p0
.end method
