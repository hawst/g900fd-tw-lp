.class public final Laba;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final NJ:Ljava/util/HashMap;

.field private final Od:Ljava/text/Collator;

.field private final Oe:Lahh;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 3719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3720
    invoke-static {p1}, Lahh;->A(Landroid/content/Context;)Lahh;

    move-result-object v0

    iput-object v0, p0, Laba;->Oe:Lahh;

    .line 3721
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Laba;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 3722
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laba;->NJ:Ljava/util/HashMap;

    .line 3723
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Laba;->Od:Ljava/text/Collator;

    .line 3724
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 3727
    iget-object v0, p0, Laba;->NJ:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3728
    iget-object v0, p0, Laba;->NJ:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 3735
    :goto_0
    iget-object v0, p0, Laba;->NJ:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3736
    iget-object v0, p0, Laba;->NJ:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3743
    :goto_1
    iget-object v2, p0, Laba;->Od:Ljava/text/Collator;

    invoke-virtual {v2, v1, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0

    .line 3730
    :cond_0
    instance-of v0, p1, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v0, :cond_1

    iget-object v1, p0, Laba;->Oe:Lahh;

    move-object v0, p1

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v1, v0}, Lahh;->b(Landroid/appwidget/AppWidgetProviderInfo;)Ljava/lang/String;

    move-result-object v0

    .line 3733
    :goto_2
    iget-object v1, p0, Laba;->NJ:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 3730
    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v1, p0, Laba;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 3738
    :cond_2
    instance-of v0, p2, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v0, :cond_3

    iget-object v2, p0, Laba;->Oe:Lahh;

    move-object v0, p2

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v2, v0}, Lahh;->b(Landroid/appwidget/AppWidgetProviderInfo;)Ljava/lang/String;

    move-result-object v0

    .line 3741
    :goto_3
    iget-object v2, p0, Laba;->NJ:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v0, p2

    .line 3738
    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v2, p0, Laba;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
