.class public final Lewi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfcx;


# static fields
.field public static final ckA:Ljeh;

.field public static ckz:I


# instance fields
.field private final bBb:Landroid/net/ConnectivityManager;

.field private final cjR:Levm;

.field private final ckB:Ljava/lang/String;

.field private ckw:Ljeh;

.field private ckx:Z

.field private cky:Z

.field private final mAccessibilityManager:Lelo;

.field private final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcfo;

.field private final mDebugFeatures:Lckw;

.field private final mDeviceCapabilityManager:Lenm;

.field private final mExecutedUserActionStore:Lfcr;

.field private final mGooglePlayServicesHelper:Lcha;

.field private final mGsaConfigFlags:Lchk;

.field private final mGsaPreferenceController:Lchr;

.field private final mHttpHelper:Ldkx;

.field private final mLoginHelper:Lcrh;

.field private final mUserClientIdManager:Lewh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0xa

    sput v0, Lewi;->ckz:I

    .line 85
    new-instance v0, Ljeh;

    invoke-direct {v0}, Ljeh;-><init>()V

    sput-object v0, Lewi;->ckA:Ljeh;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcfo;Lckw;Ljava/lang/String;Ldkx;Lcrh;Levm;Landroid/net/ConnectivityManager;Lfcr;Lcha;Lewh;Lchr;Lenm;Lelo;Lchk;)V
    .locals 2

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v1, 0x0

    iput-object v1, p0, Lewi;->ckw:Ljeh;

    .line 74
    const/4 v1, 0x0

    iput-boolean v1, p0, Lewi;->ckx:Z

    .line 75
    const/4 v1, 0x0

    iput-boolean v1, p0, Lewi;->cky:Z

    .line 119
    iput-object p1, p0, Lewi;->mContext:Landroid/content/Context;

    .line 120
    iput-object p2, p0, Lewi;->mCoreSearchServices:Lcfo;

    .line 121
    iput-object p3, p0, Lewi;->mDebugFeatures:Lckw;

    .line 122
    iput-object p4, p0, Lewi;->ckB:Ljava/lang/String;

    .line 123
    iput-object p5, p0, Lewi;->mHttpHelper:Ldkx;

    .line 124
    iput-object p6, p0, Lewi;->mLoginHelper:Lcrh;

    .line 125
    iput-object p9, p0, Lewi;->mExecutedUserActionStore:Lfcr;

    .line 126
    iput-object p7, p0, Lewi;->cjR:Levm;

    .line 127
    iput-object p8, p0, Lewi;->bBb:Landroid/net/ConnectivityManager;

    .line 128
    iput-object p10, p0, Lewi;->mGooglePlayServicesHelper:Lcha;

    .line 129
    iput-object p11, p0, Lewi;->mUserClientIdManager:Lewh;

    .line 130
    iput-object p12, p0, Lewi;->mGsaPreferenceController:Lchr;

    .line 131
    iput-object p13, p0, Lewi;->mDeviceCapabilityManager:Lenm;

    .line 132
    move-object/from16 v0, p14

    iput-object v0, p0, Lewi;->mAccessibilityManager:Lelo;

    .line 133
    move-object/from16 v0, p15

    iput-object v0, p0, Lewi;->mGsaConfigFlags:Lchk;

    .line 134
    return-void
.end method

.method private M(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lewi;->mExecutedUserActionStore:Lfcr;

    invoke-interface {v0, p1}, Lfcr;->M(Ljava/util/List;)V

    .line 469
    return-void
.end method

.method private a(Ljed;ZLandroid/accounts/Account;)Lfcy;
    .locals 16
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 236
    invoke-static {}, Lenu;->auQ()V

    .line 239
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lewi;->ckx:Z

    if-eqz v2, :cond_1

    .line 240
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 241
    :goto_0
    new-instance v2, Lfcy;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lfcy;-><init>(Ljeh;[B)V

    .line 369
    :cond_0
    :goto_1
    return-object v2

    .line 246
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->ckw:Ljeh;

    if-eqz v2, :cond_4

    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edr:Lizm;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edt:Ljgv;

    if-eqz v2, :cond_3

    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edF:Ljih;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->ckw:Ljeh;

    iget-object v2, v2, Ljeh;->eer:Ljii;

    if-eqz v2, :cond_4

    .line 250
    :cond_3
    new-instance v2, Lfcy;

    move-object/from16 v0, p0

    iget-object v3, v0, Lewi;->ckw:Ljeh;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lfcy;-><init>(Ljeh;[B)V

    goto :goto_1

    .line 253
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lewi;->cky:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edr:Lizm;

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edr:Lizm;

    iget-object v2, v2, Lizm;->dUH:[Ljbj;

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 255
    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edr:Lizm;

    iget-object v2, v2, Lizm;->dUH:[Ljbj;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 256
    iget-object v3, v2, Ljbj;->dYx:[I

    array-length v3, v3

    if-nez v3, :cond_5

    .line 257
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljbj;->hB(Z)Ljbj;

    .line 261
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lewi;->awo()Z

    move-result v2

    if-nez v2, :cond_6

    .line 262
    const-string v2, "Velvet.VelvetNetworkClient"

    const-string v3, "Network connection not availble"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const/4 v2, 0x0

    goto :goto_1

    .line 266
    :cond_6
    if-nez p3, :cond_7

    .line 267
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mLoginHelper:Lcrh;

    invoke-virtual {v2}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object p3

    .line 271
    :cond_7
    if-nez p3, :cond_8

    .line 272
    const-string v2, "Velvet.VelvetNetworkClient"

    const-string v3, "Cannot connect to server without account"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const/4 v2, 0x0

    goto :goto_1

    .line 276
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 278
    move-object/from16 v0, p1

    iget-object v2, v0, Ljed;->edv:Ljej;

    move-object/from16 v0, p0

    iget-object v3, v0, Lewi;->cjR:Levm;

    move/from16 v0, p2

    invoke-virtual {v3, v2, v0}, Levm;->a(Ljej;Z)Ljej;

    move-result-object v2

    move-object/from16 v0, p1

    iput-object v2, v0, Ljed;->edv:Ljej;

    .line 279
    invoke-virtual/range {p0 .. p1}, Lewi;->d(Ljed;)Ljed;

    move-result-object v6

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->Ft()[I

    move-result-object v2

    iput-object v2, v6, Ljed;->edO:[I

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mGsaPreferenceController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    const-string v3, "latency_event_id"

    invoke-interface {v2, v3}, Lcyg;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    sget v3, Lewi;->ckz:I

    int-to-double v10, v3

    mul-double/2addr v8, v10

    double-to-int v3, v8

    sget v7, Lewi;->ckz:I

    rem-int/2addr v3, v7

    if-nez v3, :cond_9

    const-string v3, "latency_event_id"

    const/4 v7, 0x0

    invoke-interface {v2, v3, v7}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v3

    new-instance v7, Liya;

    invoke-direct {v7}, Liya;-><init>()V

    invoke-virtual {v7, v3}, Liya;->ae([B)Liya;

    move-result-object v3

    const-string v7, "total_network_latency"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v3, v7}, Liya;->nw(I)Liya;

    move-result-object v3

    const-string v7, "deserialization_latency"

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v3, v2}, Liya;->nx(I)Liya;

    move-result-object v2

    iget-object v3, v6, Ljed;->edL:[Liya;

    invoke-static {v3, v2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Liya;

    iput-object v2, v6, Ljed;->edL:[Liya;

    .line 285
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mExecutedUserActionStore:Lfcr;

    invoke-interface {v2}, Lfcr;->axy()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, v6, Ljed;->eds:Liwl;

    if-eqz v2, :cond_b

    iget-object v2, v6, Ljed;->eds:Liwl;

    move-object v3, v2

    :goto_2
    iget-object v2, v3, Liwl;->afC:[Lizv;

    invoke-static {v2, v7}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lizv;

    iput-object v2, v3, Liwl;->afC:[Lizv;

    iput-object v3, v6, Ljed;->eds:Liwl;

    .line 288
    :cond_a
    new-instance v8, Ldld;

    const-string v2, "https://android.googleapis.com/tg/fe/request"

    invoke-direct {v8, v2}, Ldld;-><init>(Ljava/lang/String;)V

    .line 290
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v4, v5}, Lewi;->a(Ljed;J)Ljgb;

    move-result-object v2

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v8, v2}, Ldld;->F([B)V

    .line 291
    const-string v2, "Content-Type"

    const-string v3, "application/octet-stream"

    invoke-virtual {v8, v2, v3}, Ldld;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const/4 v3, 0x0

    .line 295
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    const/4 v2, 0x2

    if-ge v4, v2, :cond_14

    .line 297
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mLoginHelper:Lcrh;

    const-string v5, "oauth2:https://www.googleapis.com/auth/googlenow"

    const-wide/16 v10, 0x1388

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v5, v10, v11}, Lcrh;->c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v5

    .line 300
    if-nez v5, :cond_c

    .line 301
    const-string v2, "Velvet.VelvetNetworkClient"

    const-string v4, "Failed to get auth token"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    new-instance v2, Lfcy;

    sget-object v4, Lewi;->ckA:Ljeh;

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lfcy;-><init>(Ljeh;[B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lewi;->cky:Z

    .line 368
    if-nez v3, :cond_0

    .line 369
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lewi;->M(Ljava/util/List;)V

    goto/16 :goto_1

    .line 285
    :cond_b
    new-instance v2, Liwl;

    invoke-direct {v2}, Liwl;-><init>()V

    move-object v3, v2

    goto :goto_2

    .line 304
    :cond_c
    :try_start_2
    const-string v2, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "OAuth "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v2, v6}, Ldld;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v10

    .line 310
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mHttpHelper:Ldkx;

    const/16 v6, 0x8

    invoke-virtual {v2, v8, v6}, Ldkx;->b(Ldld;I)[B
    :try_end_3
    .catch Left; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 311
    if-nez v2, :cond_e

    .line 313
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lewi;->cky:Z

    .line 368
    if-nez v3, :cond_d

    .line 369
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lewi;->M(Ljava/util/List;)V

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 315
    :cond_e
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v10, v12, v10

    .line 316
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 317
    invoke-static {v2}, Ljgc;->al([B)Ljgc;

    move-result-object v6

    .line 318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long v12, v14, v12

    .line 323
    invoke-virtual {v6}, Ljgc;->getStatus()I

    move-result v2

    const/4 v9, 0x2

    if-ne v2, v9, :cond_11

    .line 324
    const-string v2, ""

    .line 325
    invoke-virtual {v6}, Ljgc;->bjO()Z

    move-result v9

    if-eqz v9, :cond_f

    .line 326
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, ": "

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljgc;->getErrorCode()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 328
    :cond_f
    const-string v6, "Velvet.VelvetNetworkClient"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Received ERROR from server"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Left; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 329
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lewi;->cky:Z

    .line 368
    if-nez v3, :cond_10

    .line 369
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lewi;->M(Ljava/util/List;)V

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 335
    :cond_11
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DO()Lgpp;

    move-result-object v2

    invoke-interface {v2}, Lgpp;->aJj()V

    .line 337
    invoke-virtual {v6}, Ljgc;->bdc()[B

    move-result-object v2

    long-to-int v9, v10

    long-to-int v10, v12

    move-object/from16 v0, p0

    iget-object v11, v0, Lewi;->mGsaPreferenceController:Lchr;

    invoke-virtual {v11}, Lchr;->Kt()Lcyg;

    move-result-object v11

    invoke-interface {v11}, Lcyg;->EH()Lcyh;

    move-result-object v11

    const-string v12, "latency_event_id"

    invoke-interface {v11, v12, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    const-string v2, "total_network_latency"

    add-int/2addr v9, v10

    invoke-interface {v11, v2, v9}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    const-string v2, "deserialization_latency"

    invoke-interface {v11, v2, v10}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    invoke-interface {v11}, Lcyh;->apply()V

    .line 341
    const/4 v3, 0x1

    .line 342
    new-instance v2, Lfcy;

    iget-object v9, v6, Ljgc;->ejq:Ljeh;

    invoke-virtual {v6}, Ljgc;->bdc()[B

    move-result-object v6

    invoke-direct {v2, v9, v6}, Lfcy;-><init>(Ljeh;[B)V
    :try_end_5
    .catch Left; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 364
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lewi;->cky:Z

    goto/16 :goto_1

    .line 344
    :catch_0
    move-exception v2

    .line 345
    :try_start_6
    invoke-virtual {v2}, Left;->getErrorCode()I

    move-result v6

    const/16 v9, 0x191

    if-ne v6, v9, :cond_12

    .line 346
    const-string v6, "Velvet.VelvetNetworkClient"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Authorization exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lewi;->mLoginHelper:Lcrh;

    invoke-virtual {v2, v5}, Lcrh;->iC(Ljava/lang/String;)V

    .line 295
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_3

    .line 350
    :cond_12
    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 360
    :catch_1
    move-exception v2

    .line 361
    :try_start_7
    const-string v4, "Velvet.VelvetNetworkClient"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Network error: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 362
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lewi;->cky:Z

    .line 368
    if-nez v3, :cond_13

    .line 369
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lewi;->M(Ljava/util/List;)V

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 354
    :cond_14
    :try_start_8
    const-string v2, "Velvet.VelvetNetworkClient"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Request retries failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ldld;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 359
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lewi;->cky:Z

    .line 368
    if-nez v3, :cond_15

    .line 369
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lewi;->M(Ljava/util/List;)V

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 364
    :catchall_0
    move-exception v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lewi;->cky:Z

    .line 368
    if-nez v3, :cond_16

    .line 369
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lewi;->M(Ljava/util/List;)V

    :cond_16
    throw v2

    :catch_2
    move-exception v2

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljed;)Ljeh;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 138
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, v0}, Lewi;->a(Ljed;ZLandroid/accounts/Account;)Lfcy;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_0

    iget-object v0, v1, Lfcy;->coT:Ljeh;

    :cond_0
    return-object v0
.end method

.method public final a(Ljed;Landroid/accounts/Account;)Ljeh;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lewi;->a(Ljed;ZLandroid/accounts/Account;)Lfcy;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    iget-object v0, v0, Lfcy;->coT:Ljeh;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljed;J)Ljgb;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 507
    new-instance v1, Lizc;

    invoke-direct {v1}, Lizc;-><init>()V

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lizc;->rA(Ljava/lang/String;)Lizc;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lizc;->rB(Ljava/lang/String;)Lizc;

    move-result-object v1

    .line 510
    new-instance v2, Lixz;

    invoke-direct {v2}, Lixz;-><init>()V

    invoke-virtual {v2, v0}, Lixz;->nt(I)Lixz;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lixz;->ri(Ljava/lang/String;)Lixz;

    move-result-object v2

    invoke-virtual {p0}, Lewi;->awn()I

    move-result v3

    invoke-virtual {v2, v3}, Lixz;->nu(I)Lixz;

    move-result-object v2

    iget-object v3, p0, Lewi;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v3}, Lenm;->auH()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Lixz;->nv(I)Lixz;

    move-result-object v0

    .line 515
    iput-object v1, v0, Lixz;->dPd:Lizc;

    .line 517
    iget-object v1, p0, Lewi;->mUserClientIdManager:Lewh;

    invoke-virtual {v1}, Lewh;->awk()Ljava/lang/Long;

    move-result-object v1

    .line 518
    if-eqz v1, :cond_0

    .line 519
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lixz;->cl(J)Lixz;

    .line 522
    :cond_0
    iget-object v1, p0, Lewi;->mAccessibilityManager:Lelo;

    invoke-virtual {v1}, Lelo;->aum()Z

    move-result v1

    invoke-virtual {v0, v1}, Lixz;->hk(Z)Lixz;

    .line 524
    iget-object v1, p0, Lewi;->ckB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lixz;->rj(Ljava/lang/String;)Lixz;

    .line 526
    new-instance v1, Liws;

    invoke-direct {v1}, Liws;-><init>()V

    iget-object v2, p0, Lewi;->mGooglePlayServicesHelper:Lcha;

    invoke-virtual {v2}, Lcha;->Fj()I

    move-result v2

    invoke-virtual {v1, v2}, Liws;->nf(I)Liws;

    move-result-object v1

    iget-object v2, p0, Lewi;->mGooglePlayServicesHelper:Lcha;

    invoke-virtual {v2}, Lcha;->Fk()I

    move-result v2

    invoke-virtual {v1, v2}, Liws;->ng(I)Liws;

    move-result-object v1

    iget-object v2, p0, Lewi;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lhgn;->bI(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Liws;->hf(Z)Liws;

    move-result-object v1

    .line 532
    iput-object v1, v0, Lixz;->dPb:Liws;

    .line 534
    new-instance v1, Ljgb;

    invoke-direct {v1}, Ljgb;-><init>()V

    const-wide/16 v2, 0x3e8

    div-long v2, p2, v2

    invoke-virtual {v1, v2, v3}, Ljgb;->df(J)Ljgb;

    move-result-object v1

    .line 536
    iput-object p1, v1, Ljgb;->ejp:Ljed;

    .line 537
    iput-object v0, v1, Ljgb;->ejo:Lixz;

    .line 539
    return-object v1

    .line 510
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final a(Ljeh;)V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lewi;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    iput-object p1, p0, Lewi;->ckw:Ljeh;

    .line 557
    :cond_0
    return-void
.end method

.method public final awn()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lewi;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 167
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-nez v2, :cond_1

    .line 222
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 170
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 186
    :pswitch_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 202
    :pswitch_2
    const/16 v0, 0xc

    goto :goto_0

    .line 172
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 176
    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    .line 178
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_0

    .line 180
    :pswitch_6
    const/4 v0, 0x2

    goto :goto_0

    .line 188
    :pswitch_7
    const/4 v0, 0x5

    goto :goto_0

    .line 190
    :pswitch_8
    const/4 v0, 0x6

    goto :goto_0

    .line 192
    :pswitch_9
    const/4 v0, 0x7

    goto :goto_0

    .line 194
    :pswitch_a
    const/16 v0, 0x9

    goto :goto_0

    .line 196
    :pswitch_b
    const/16 v0, 0x8

    goto :goto_0

    .line 198
    :pswitch_c
    const/16 v0, 0xa

    goto :goto_0

    .line 200
    :pswitch_d
    const/16 v0, 0xb

    goto :goto_0

    .line 204
    :pswitch_e
    const/16 v0, 0xd

    goto :goto_0

    .line 206
    :pswitch_f
    const/16 v0, 0xe

    goto :goto_0

    .line 208
    :pswitch_10
    const/16 v0, 0xf

    goto :goto_0

    .line 210
    :pswitch_11
    const/16 v0, 0x10

    goto :goto_0

    .line 212
    :pswitch_12
    const/16 v0, 0x11

    goto :goto_0

    .line 214
    :pswitch_13
    const/16 v0, 0x12

    goto :goto_0

    .line 216
    :pswitch_14
    const/16 v0, 0x13

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 186
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_9
        :pswitch_14
        :pswitch_8
        :pswitch_a
        :pswitch_c
        :pswitch_7
        :pswitch_e
        :pswitch_11
        :pswitch_f
        :pswitch_12
        :pswitch_d
        :pswitch_13
        :pswitch_b
        :pswitch_10
    .end packed-switch
.end method

.method public final awo()Z
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lewi;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 545
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    goto :goto_0
.end method

.method public final awp()V
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lewi;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    const/4 v0, 0x1

    iput-boolean v0, p0, Lewi;->cky:Z

    .line 586
    :cond_0
    return-void
.end method

.method public final b(Ljed;)Lfcy;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 145
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lewi;->a(Ljed;ZLandroid/accounts/Account;)Lfcy;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljed;)Ljeh;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 150
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lewi;->a(Ljed;ZLandroid/accounts/Account;)Lfcy;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_0

    iget-object v0, v1, Lfcy;->coT:Ljeh;

    :cond_0
    return-object v0
.end method

.method public final d(Ljed;)Ljed;
    .locals 6

    .prologue
    .line 411
    new-instance v3, Lizw;

    invoke-direct {v3}, Lizw;-><init>()V

    .line 412
    iget-object v0, p0, Lewi;->mGsaPreferenceController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "now_opted_in_experiments"

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyg;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 414
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 415
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 416
    const/4 v0, 0x0

    .line 417
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 418
    add-int/lit8 v2, v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    aput v0, v4, v1

    move v1, v2

    .line 419
    goto :goto_0

    .line 420
    :cond_0
    iput-object v4, v3, Lizw;->dVO:[I

    .line 421
    iput-object v3, p1, Ljed;->edH:Lizw;

    .line 423
    :cond_1
    return-object p1
.end method

.method public final fg(Z)V
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lewi;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    iput-boolean p1, p0, Lewi;->ckx:Z

    .line 573
    :cond_0
    return-void
.end method
