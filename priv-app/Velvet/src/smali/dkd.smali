.class public Ldkd;
.super Ldkb;
.source "PG"


# instance fields
.field private final mLength:I

.field private final rQ:[B
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    new-array v0, v2, [B

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, v2}, Ldkd;-><init>([BII)V

    .line 123
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 126
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Ldkd;-><init>([BII)V

    .line 127
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 3
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    invoke-direct {p0}, Ldkb;-><init>()V

    .line 130
    const/4 v0, -0x1

    if-lt p2, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 131
    array-length v0, p1

    if-gt p2, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 132
    if-ltz p3, :cond_2

    :goto_2
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 133
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Ldkd;->rQ:[B

    .line 134
    iput p2, p0, Ldkd;->mLength:I

    .line 135
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v0, v2

    .line 131
    goto :goto_1

    :cond_2
    move v1, v2

    .line 132
    goto :goto_2
.end method


# virtual methods
.method public final acz()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Ldkd;->rQ:[B

    array-length v0, v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 165
    instance-of v0, p1, Ldkd;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Ldkd;->rQ:[B

    check-cast p1, Ldkd;

    iget-object v1, p1, Ldkd;->rQ:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 146
    iget v0, p0, Ldkd;->mLength:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 147
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Ldkd;->rQ:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 149
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Ldkd;->rQ:[B

    const/4 v2, 0x0

    iget v3, p0, Ldkd;->mLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    goto :goto_0
.end method

.method final toShortString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, "D"

    return-object v0
.end method
