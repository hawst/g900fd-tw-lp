.class public final Lcvt;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final blj:Ligi;

.field private blk:Landroid/preference/Preference;


# direct methods
.method public constructor <init>(Lcke;Ligi;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 26
    iput-object p2, p0, Lcvt;->blj:Ligi;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 36
    iput-object p1, p0, Lcvt;->blk:Landroid/preference/Preference;

    .line 37
    iget-object v0, p0, Lcvt;->blk:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 38
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcvt;->blj:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcvt;->blk:Landroid/preference/Preference;

    invoke-virtual {v0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->bM(Landroid/content/Context;)V

    .line 43
    const/4 v0, 0x1

    return v0
.end method
