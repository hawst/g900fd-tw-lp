.class public final Lewd;
.super Lfkv;
.source "PG"


# instance fields
.field public ckk:Lgyi;

.field private final ckl:Ljava/lang/Object;

.field private final ckm:Z

.field private ckn:Lfjk;

.field private final mSettings:Lcke;

.field private final mSpeechLevelSource:Lequ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfml;Lfnn;Lern;Lequ;Lemp;Lcke;Z)V
    .locals 6

    .prologue
    .line 43
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lfkv;-><init>(Landroid/content/Context;Lfml;Lfnn;Lern;Lemp;)V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lewd;->ckl:Ljava/lang/Object;

    .line 44
    iput-object p5, p0, Lewd;->mSpeechLevelSource:Lequ;

    .line 45
    iput-object p7, p0, Lewd;->mSettings:Lcke;

    .line 46
    iput-boolean p8, p0, Lewd;->ckm:Z

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/sidekick/shared/client/NowSearchOptions;)Z
    .locals 1
    .param p2    # Lcom/google/android/sidekick/shared/client/NowSearchOptions;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 55
    iget-object v0, p0, Lewd;->ckk:Lgyi;

    invoke-virtual {v0, p1, p2}, Lgyi;->a(Ljava/lang/String;Lcom/google/android/sidekick/shared/client/NowSearchOptions;)Z

    move-result v0

    return v0
.end method

.method public final wj()Lekf;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lewd;->ckk:Lgyi;

    invoke-virtual {v0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lewd;->ckk:Lgyi;

    invoke-virtual {v0}, Lgyi;->wj()Lekf;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wq()Lfnm;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 61
    iget-object v1, p0, Lewd;->ckl:Ljava/lang/Object;

    monitor-enter v1

    .line 62
    :try_start_0
    iget-object v0, p0, Lewd;->ckn:Lfjk;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lfjk;

    iget-object v2, p0, Lewd;->ckk:Lgyi;

    iget-object v3, p0, Lewd;->mSpeechLevelSource:Lequ;

    invoke-direct {v0, v2, v3}, Lfjk;-><init>(Lgyi;Lequ;)V

    iput-object v0, p0, Lewd;->ckn:Lfjk;

    .line 66
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    iget-object v0, p0, Lewd;->ckn:Lfjk;

    return-object v0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final wr()Leoj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lewd;->ckk:Lgyi;

    invoke-virtual {v0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lewd;->ckk:Lgyi;

    invoke-virtual {v0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->wr()Leoj;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wu()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lewd;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->wu()Z

    move-result v0

    return v0
.end method

.method public final wv()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lewd;->ckm:Z

    return v0
.end method
