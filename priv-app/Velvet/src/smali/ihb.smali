.class abstract Lihb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liph;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x21f766b1f568c81dL


# instance fields
.field private transient dCc:I

.field private transient dCd:Ljava/util/Set;

.field private transient dCe:Ljava/util/Collection;

.field private transient dCf:Ljava/util/Collection;

.field private transient dCg:Ljava/util/Map;

.field private transient map:Ljava/util/Map;


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 120
    iput-object p1, p0, Lihb;->map:Ljava/util/Map;

    .line 121
    return-void
.end method

.method static synthetic a(Lihb;I)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lihb;->dCc:I

    add-int/2addr v0, p1

    iput v0, p0, Lihb;->dCc:I

    return v0
.end method

.method static synthetic a(Lihb;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lihb;->bm(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lihb;Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lihb;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 364
    instance-of v0, p2, Ljava/util/SortedSet;

    if-eqz v0, :cond_0

    .line 365
    new-instance v0, Liht;

    check-cast p2, Ljava/util/SortedSet;

    invoke-direct {v0, p0, p1, p2, v1}, Liht;-><init>(Lihb;Ljava/lang/Object;Ljava/util/SortedSet;Liho;)V

    .line 371
    :goto_0
    return-object v0

    .line 366
    :cond_0
    instance-of v0, p2, Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 367
    new-instance v0, Lihs;

    check-cast p2, Ljava/util/Set;

    invoke-direct {v0, p0, p1, p2}, Lihs;-><init>(Lihb;Ljava/lang/Object;Ljava/util/Set;)V

    goto :goto_0

    .line 368
    :cond_1
    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_2

    .line 369
    check-cast p2, Ljava/util/List;

    invoke-direct {p0, p1, p2, v1}, Lihb;->a(Ljava/lang/Object;Ljava/util/List;Liho;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 371
    :cond_2
    new-instance v0, Liho;

    invoke-direct {v0, p0, p1, p2, v1}, Liho;-><init>(Lihb;Ljava/lang/Object;Ljava/util/Collection;Liho;)V

    goto :goto_0
.end method

.method static synthetic a(Lihb;Ljava/util/Collection;)Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 88
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lihb;Ljava/lang/Object;Ljava/util/List;Liho;)Ljava/util/List;
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Lihb;->a(Ljava/lang/Object;Ljava/util/List;Liho;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/List;Liho;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Liho;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 377
    instance-of v0, p2, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    new-instance v0, Lihl;

    invoke-direct {v0, p0, p1, p2, p3}, Lihl;-><init>(Lihb;Ljava/lang/Object;Ljava/util/List;Liho;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lihq;

    invoke-direct {v0, p0, p1, p2, p3}, Lihq;-><init>(Lihb;Ljava/lang/Object;Ljava/util/List;Liho;)V

    goto :goto_0
.end method

.method static synthetic a(Lihb;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lihb;)I
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lihb;->dCc:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lihb;->dCc:I

    return v0
.end method

.method static synthetic b(Lihb;I)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lihb;->dCc:I

    sub-int/2addr v0, p1

    iput v0, p0, Lihb;->dCc:I

    return v0
.end method

.method private bm(Ljava/lang/Object;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1012
    :try_start_0
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1020
    if-eqz v0, :cond_0

    .line 1021
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    .line 1022
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1023
    iget v0, p0, Lihb;->dCc:I

    sub-int/2addr v0, v1

    iput v0, p0, Lihb;->dCc:I

    :cond_0
    move v0, v1

    move v1, v0

    .line 1025
    :goto_0
    return v1

    .line 1016
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1014
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Lihb;)I
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lihb;->dCc:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lihb;->dCc:I

    return v0
.end method


# virtual methods
.method abstract aWm()Ljava/util/Collection;
.end method

.method public aWn()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 1068
    iget-object v0, p0, Lihb;->dCf:Ljava/util/Collection;

    .line 1069
    if-nez v0, :cond_0

    instance-of v0, p0, Liqr;

    if-eqz v0, :cond_1

    new-instance v0, Lihd;

    invoke-direct {v0, p0}, Lihd;-><init>(Lihb;)V

    :goto_0
    iput-object v0, p0, Lihb;->dCf:Ljava/util/Collection;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Lihe;

    invoke-direct {v0, p0}, Lihe;-><init>(Lihb;)V

    goto :goto_0
.end method

.method final aWo()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 1104
    new-instance v0, Lihi;

    invoke-direct {v0, p0}, Lihi;-><init>(Lihb;)V

    return-object v0
.end method

.method public aWp()Ljava/util/Map;
    .locals 2

    .prologue
    .line 1157
    iget-object v0, p0, Lihb;->dCg:Ljava/util/Map;

    .line 1158
    if-nez v0, :cond_0

    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_1

    new-instance v1, Lihm;

    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lihm;-><init>(Lihb;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lihb;->dCg:Ljava/util/Map;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Lihf;

    iget-object v1, p0, Lihb;->map:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lihf;-><init>(Lihb;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public bk(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 306
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 307
    invoke-virtual {p0}, Lihb;->aWm()Ljava/util/Collection;

    move-result-object v1

    .line 309
    if-eqz v0, :cond_0

    .line 310
    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 311
    iget v2, p0, Lihb;->dCc:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lihb;->dCc:I

    .line 312
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 315
    :cond_0
    instance-of v0, v1, Ljava/util/SortedSet;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    instance-of v0, v1, Ljava/util/Set;

    if-eqz v0, :cond_2

    check-cast v1, Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0

    :cond_2
    instance-of v0, v1, Ljava/util/List;

    if-eqz v0, :cond_3

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public bl(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 350
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 351
    if-nez v0, :cond_0

    .line 352
    invoke-virtual {p0}, Lihb;->aWm()Ljava/util/Collection;

    move-result-object v0

    .line 354
    :cond_0
    invoke-direct {p0, p1, v0}, Lihb;->a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 335
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    goto :goto_0

    .line 337
    :cond_0
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 338
    const/4 v0, 0x0

    iput v0, p0, Lihb;->dCc:I

    .line 339
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 177
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 182
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 183
    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    .line 188
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1352
    if-ne p1, p0, :cond_0

    .line 1353
    const/4 v0, 0x1

    .line 1359
    :goto_0
    return v0

    .line 1355
    :cond_0
    instance-of v0, p1, Liph;

    if-eqz v0, :cond_1

    .line 1356
    check-cast p1, Liph;

    .line 1357
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {p1}, Liph;->aWp()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1359
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lihb;->dCd:Ljava/util/Set;

    .line 867
    if-nez v0, :cond_0

    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    instance-of v0, v0, Ljava/util/SortedMap;

    if-eqz v0, :cond_1

    new-instance v1, Lihn;

    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, p0, v0}, Lihn;-><init>(Lihb;Ljava/util/SortedMap;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lihb;->dCd:Ljava/util/Set;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Lihj;

    iget-object v1, p0, Lihb;->map:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lihj;-><init>(Lihb;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public p(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 193
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 194
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 201
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lihb;->aWm()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget v0, p0, Lihb;->dCc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lihb;->dCc:I

    .line 205
    const/4 v0, 0x1

    .line 207
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final r(Ljava/util/Map;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 125
    iput-object p1, p0, Lihb;->map:Ljava/util/Map;

    .line 126
    iput v2, p0, Lihb;->dCc:I

    .line 127
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 128
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 129
    iget v1, p0, Lihb;->dCc:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lihb;->dCc:I

    goto :goto_0

    :cond_0
    move v1, v2

    .line 128
    goto :goto_1

    .line 131
    :cond_1
    return-void
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 222
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 223
    if-nez v0, :cond_0

    .line 224
    const/4 v0, 0x0

    .line 234
    :goto_0
    return v0

    .line 227
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 228
    if-eqz v1, :cond_1

    .line 229
    iget v2, p0, Lihb;->dCc:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lihb;->dCc:I

    .line 230
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move v0, v1

    .line 234
    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lihb;->dCc:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1382
    iget-object v0, p0, Lihb;->map:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lihb;->dCe:Ljava/util/Collection;

    .line 1038
    if-nez v0, :cond_0

    .line 1039
    new-instance v0, Lihc;

    invoke-direct {v0, p0}, Lihc;-><init>(Lihb;)V

    iput-object v0, p0, Lihb;->dCe:Ljava/util/Collection;

    .line 1045
    :cond_0
    return-object v0
.end method
