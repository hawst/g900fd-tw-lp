.class public final Lxl;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic FA:Ljava/lang/Runnable;

.field private synthetic KX:Lcom/android/launcher3/Launcher;

.field private synthetic Lj:Landroid/view/View;

.field private synthetic Lk:Z

.field private synthetic Lm:Landroid/view/View;

.field private synthetic Ln:Landroid/view/View;

.field private synthetic Lo:Lcom/android/launcher3/AppsCustomizePagedView;

.field private synthetic Ls:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;Landroid/view/View;ZLandroid/view/View;Ljava/lang/Runnable;Landroid/view/View;Landroid/view/View;Lcom/android/launcher3/AppsCustomizePagedView;)V
    .locals 0

    .prologue
    .line 3631
    iput-object p1, p0, Lxl;->KX:Lcom/android/launcher3/Launcher;

    iput-object p2, p0, Lxl;->Lj:Landroid/view/View;

    iput-boolean p3, p0, Lxl;->Lk:Z

    iput-object p4, p0, Lxl;->Ls:Landroid/view/View;

    iput-object p5, p0, Lxl;->FA:Ljava/lang/Runnable;

    iput-object p6, p0, Lxl;->Lm:Landroid/view/View;

    iput-object p7, p0, Lxl;->Ln:Landroid/view/View;

    iput-object p8, p0, Lxl;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3634
    iget-object v0, p0, Lxl;->Lj:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3635
    iget-object v0, p0, Lxl;->KX:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lxl;->Lj:Landroid/view/View;

    iget-boolean v3, p0, Lxl;->Lk:Z

    invoke-static {v0, v2, v3, v4}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V

    .line 3636
    iget-object v0, p0, Lxl;->KX:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lxl;->Ls:Landroid/view/View;

    iget-boolean v3, p0, Lxl;->Lk:Z

    invoke-static {v0, v2, v3, v4}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V

    .line 3637
    iget-object v0, p0, Lxl;->FA:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 3638
    iget-object v0, p0, Lxl;->FA:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 3641
    :cond_0
    iget-object v0, p0, Lxl;->Lm:Landroid/view/View;

    invoke-virtual {v0, v1, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3642
    iget-object v0, p0, Lxl;->Ln:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 3643
    iget-object v0, p0, Lxl;->Ln:Landroid/view/View;

    invoke-virtual {v0, v1, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3645
    :cond_1
    iget-object v0, p0, Lxl;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/AppsCustomizePagedView;->z(Z)V

    .line 3647
    iget-object v0, p0, Lxl;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildCount()I

    move-result v2

    move v0, v1

    .line 3648
    :goto_0
    if-ge v0, v2, :cond_2

    .line 3649
    iget-object v3, p0, Lxl;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v3, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 3650
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3648
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3654
    :cond_2
    iget-object v0, p0, Lxl;->Ln:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 3655
    iget-object v0, p0, Lxl;->Ln:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 3656
    iget-object v0, p0, Lxl;->Ln:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 3657
    iget-object v0, p0, Lxl;->Ln:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 3659
    :cond_3
    iget-object v0, p0, Lxl;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-object v1, p0, Lxl;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->jg()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->bu(I)V

    .line 3661
    iget-object v0, p0, Lxl;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->o(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/AppsCustomizePagedView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->jh()V

    .line 3662
    return-void
.end method
