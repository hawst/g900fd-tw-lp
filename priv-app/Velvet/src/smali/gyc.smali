.class public final Lgyc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leet;
.implements Lekg;


# instance fields
.field bso:I

.field private final cZa:Lgzz;

.field private cZb:I

.field private cZc:Z

.field private cZd:Lcom/google/android/search/suggest/SuggestionListView;

.field mScrollView:Lekf;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgzz;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lgyc;->cZa:Lgzz;

    .line 39
    return-void
.end method

.method private aAU()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgyc;->mScrollView:Lekf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/suggest/SuggestionListView;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lgyc;->aAU()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 68
    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    if-eq v0, p1, :cond_2

    .line 69
    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leet;)V

    .line 72
    :cond_0
    iput-object p1, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    .line 73
    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p0}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leet;)V

    .line 76
    :cond_1
    invoke-virtual {p0}, Lgyc;->aLC()V

    .line 78
    :cond_2
    return-void
.end method

.method aLC()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 82
    invoke-direct {p0}, Lgyc;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lgyc;->bso:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lgyc;->cZc:Z

    if-nez v0, :cond_0

    iget v0, p0, Lgyc;->cZb:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyc;->cZd:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 88
    :goto_0
    iget-object v3, p0, Lgyc;->cZa:Lgzz;

    if-eqz v0, :cond_1

    :goto_1
    invoke-interface {v3, v1}, Lgzz;->kK(I)V

    .line 91
    return-void

    :cond_0
    move v0, v2

    .line 82
    goto :goto_0

    :cond_1
    move v1, v2

    .line 88
    goto :goto_1
.end method

.method public final aa(II)V
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lgyc;->cZb:I

    if-eq v0, p1, :cond_0

    .line 102
    iput p1, p0, Lgyc;->cZb:I

    .line 103
    invoke-virtual {p0}, Lgyc;->aLC()V

    .line 105
    :cond_0
    return-void
.end method

.method public final aoh()V
    .locals 0

    .prologue
    .line 135
    invoke-virtual {p0}, Lgyc;->aLC()V

    .line 136
    return-void
.end method

.method public final dr(I)V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public final vS()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public final vT()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public final vU()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public final vV()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgyc;->cZc:Z

    .line 110
    invoke-virtual {p0}, Lgyc;->aLC()V

    .line 111
    return-void
.end method

.method public final vW()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyc;->cZc:Z

    .line 116
    invoke-virtual {p0}, Lgyc;->aLC()V

    .line 117
    return-void
.end method
