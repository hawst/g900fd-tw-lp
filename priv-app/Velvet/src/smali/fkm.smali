.class public final Lfkm;
.super Lfjz;
.source "PG"


# instance fields
.field final mRefreshManager:Lfmv;


# direct methods
.method public constructor <init>(Lfzw;Lfmv;)V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-direct {p0, v0, p1}, Lfjz;-><init>(Lizj;Lfzw;)V

    .line 25
    iput-object p2, p0, Lfkm;->mRefreshManager:Lfmv;

    .line 26
    return-void
.end method

.method private static y(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 57
    const v0, 0x7f110171

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    const v0, 0x7f1100bc

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 59
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 60
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 31
    const v0, 0x7f0400b3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 33
    new-instance v1, Lfkn;

    invoke-direct {v1, p0, v0}, Lfkn;-><init>(Lfkm;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-object v0
.end method

.method public final a(Lfmt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public final bk(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f0a0236

    invoke-static {p1, v0}, Lfkm;->y(Landroid/view/View;I)V

    .line 50
    return-void
.end method

.method public final bl(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f0a0150

    invoke-static {p1, v0}, Lfkm;->y(Landroid/view/View;I)V

    .line 54
    return-void
.end method
