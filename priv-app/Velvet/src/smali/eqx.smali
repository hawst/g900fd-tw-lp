.class public final Leqx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aD:Ljava/lang/String;

.field private final bbd:Lenw;

.field private final chI:Liiy;

.field private final chJ:Z

.field private final chK:Z

.field private chL:Ljava/lang/Enum;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Enum;ZLenw;Liiy;Z)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p5, p0, Leqx;->chI:Liiy;

    .line 52
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Leqx;->aD:Ljava/lang/String;

    .line 53
    iput-boolean p3, p0, Leqx;->chJ:Z

    .line 54
    iput-object p4, p0, Leqx;->bbd:Lenw;

    .line 55
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    iput-object v0, p0, Leqx;->chL:Ljava/lang/Enum;

    .line 56
    iput-boolean p6, p0, Leqx;->chK:Z

    .line 57
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/Enum;ZZLiiy;Z)V
    .locals 7

    .prologue
    .line 40
    if-eqz p4, :cond_1

    new-instance v4, Lenw;

    invoke-direct {v4}, Lenw;-><init>()V

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Leqx;-><init>(Ljava/lang/String;Ljava/lang/Enum;ZLenw;Liiy;Z)V

    .line 45
    iget-boolean v0, p0, Leqx;->chK:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leqx;->aD:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Init state "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 46
    :cond_0
    return-void

    .line 40
    :cond_1
    new-instance v4, Lenw;

    invoke-direct {v4}, Lenw;-><init>()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Leqy;

    invoke-direct {v0, p0, p1}, Leqy;-><init>(Ljava/lang/String;Ljava/lang/Enum;)V

    return-object v0
.end method

.method private lm(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 107
    iget-boolean v0, p0, Leqx;->chJ:Z

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Leqx;->aD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Leqx;->aD:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 112
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Leqx;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 61
    iget-object v0, p0, Leqx;->chI:Liiy;

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Liiy;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leqx;->chI:Liiy;

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Liiy;->bn(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Illegal transation "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Leqx;->lm(Ljava/lang/String;)V

    .line 65
    :cond_1
    iget-boolean v0, p0, Leqx;->chK:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Leqx;->aD:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 67
    :cond_2
    iget-object v0, p0, Leqx;->chL:Ljava/lang/Enum;

    .line 68
    iput-object p1, p0, Leqx;->chL:Ljava/lang/Enum;

    .line 69
    return-void
.end method

.method public final b(Ljava/lang/Enum;)Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Leqx;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 85
    iget-object v0, p0, Leqx;->chL:Ljava/lang/Enum;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Enum;)Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Leqx;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 93
    invoke-virtual {p0, p1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Enum;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Leqx;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 101
    iget-object v0, p0, Leqx;->chL:Ljava/lang/Enum;

    if-eq v0, p1, :cond_0

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current state is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", expected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Leqx;->lm(Ljava/lang/String;)V

    .line 104
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current state="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leqx;->chL:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
