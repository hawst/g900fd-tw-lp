.class public final Lcyt;
.super Lcvc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public amK:Landroid/preference/PreferenceGroup;

.field private final ble:Lhxr;

.field private final blj:Ligi;

.field private final bnr:Lchm;

.field final bns:Lcyv;

.field public bnt:Landroid/preference/Preference;

.field private final mActivity:Landroid/app/Activity;

.field final mAsyncServices:Lema;

.field private final mCoreSearchServices:Lcfo;

.field private final mDebugFeatures:Lckw;

.field final mGsaConfigFlags:Lchk;

.field private final mNetworkInformation:Lgno;

.field private final mSearchConfig:Lcjs;

.field private final mSettings:Lcke;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Lcfo;Lhym;Landroid/app/Activity;Ligi;Lhxr;Lchk;Lckw;Lema;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcvc;-><init>()V

    .line 58
    new-instance v0, Lcyu;

    invoke-direct {v0, p0}, Lcyu;-><init>(Lcyt;)V

    iput-object v0, p0, Lcyt;->bnr:Lchm;

    .line 61
    new-instance v0, Lcyv;

    invoke-direct {v0, p0}, Lcyv;-><init>(Lcyt;)V

    iput-object v0, p0, Lcyt;->bns:Lcyv;

    .line 75
    iput-object p1, p0, Lcyt;->mCoreSearchServices:Lcfo;

    .line 76
    invoke-virtual {p1}, Lcfo;->DD()Lcjs;

    move-result-object v0

    iput-object v0, p0, Lcyt;->mSearchConfig:Lcjs;

    .line 77
    invoke-virtual {p1}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lcyt;->mSettings:Lcke;

    .line 78
    invoke-virtual {p1}, Lcfo;->DY()Lgno;

    move-result-object v0

    iput-object v0, p0, Lcyt;->mNetworkInformation:Lgno;

    .line 79
    iput-object p2, p0, Lcyt;->mVoiceSettings:Lhym;

    .line 80
    iput-object p3, p0, Lcyt;->mActivity:Landroid/app/Activity;

    .line 81
    iput-object p4, p0, Lcyt;->blj:Ligi;

    .line 82
    iput-object p5, p0, Lcyt;->ble:Lhxr;

    .line 83
    iput-object p6, p0, Lcyt;->mGsaConfigFlags:Lchk;

    .line 84
    iput-object p7, p0, Lcyt;->mDebugFeatures:Lckw;

    .line 85
    iput-object p8, p0, Lcyt;->mAsyncServices:Lema;

    .line 86
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 96
    const-string v0, "handsFree"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    new-instance v0, Lbyl;

    invoke-direct {v0}, Lbyl;-><init>()V

    .line 98
    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lbyl;->BX()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcyt;->amK:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcyt;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    const-string v0, "hotword"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iput-object p1, p0, Lcyt;->bnt:Landroid/preference/Preference;

    goto :goto_0

    .line 107
    :cond_2
    invoke-super {p0, p1}, Lcvc;->a(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method public final a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcvc;->a(Landroid/preference/PreferenceScreen;)V

    .line 91
    iput-object p1, p0, Lcyt;->amK:Landroid/preference/PreferenceGroup;

    .line 92
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 118
    const-string v1, "personalizedResults"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "managePersonalization"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    :cond_0
    iget-object v0, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Ga()Z

    move-result v0

    .line 134
    :cond_1
    :goto_0
    return v0

    .line 122
    :cond_2
    const-string v1, "audio_history"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "manage_audio_history"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 126
    :cond_3
    iget-object v1, p0, Lcyt;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTE()Z

    move-result v1

    if-nez v1, :cond_1

    .line 130
    iget-object v1, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->Ga()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->HO()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcgg;->aVr:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    :cond_5
    invoke-super {p0, p1}, Lcvc;->d(Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final e(Landroid/preference/Preference;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 144
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 163
    :cond_0
    :goto_0
    return-object v0

    .line 145
    :cond_1
    const-string v1, "debugS3Server"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugS3Logging"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugPersonalization"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugConfigurationDate"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugConfigurationExperiment"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "audioLoggingEnabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugSendLoggedAudio"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "s3SandboxOverride"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugRecognitionEngineRestrict"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugFreshContacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugTopContacts"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debugQuixote"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 157
    :cond_2
    const-string v0, "debug_voice_controller"

    goto :goto_0

    .line 158
    :cond_3
    const-string v1, "profanityFilter"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "downloadLanguagePacks"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_6

    .line 159
    const-string v0, "voice_ime_composite_controller"

    goto :goto_0

    .line 158
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 160
    :cond_6
    invoke-static {v0}, Laqm;->ea(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    const-string v0, "account_settings_controller"

    goto/16 :goto_0
.end method

.method protected final f(Landroid/preference/Preference;)Lcxt;
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 168
    invoke-virtual {p0, p1}, Lcyt;->e(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v0

    .line 169
    const-string v1, "language"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    new-instance v0, Lcwy;

    iget-object v1, p0, Lcyt;->mSettings:Lcke;

    iget-object v2, p0, Lcyt;->mVoiceSettings:Lhym;

    iget-object v3, p0, Lcyt;->mCoreSearchServices:Lcfo;

    iget-object v4, p0, Lcyt;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1, v2, v3, v4}, Lcwy;-><init>(Lcke;Lhym;Lcfo;Landroid/content/Context;)V

    .line 200
    :goto_0
    return-object v0

    .line 172
    :cond_0
    const-string v1, "ttsMode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 173
    new-instance v0, Lcyr;

    iget-object v1, p0, Lcyt;->mSettings:Lcke;

    invoke-direct {v0, v1}, Lcyr;-><init>(Lcke;)V

    goto :goto_0

    .line 174
    :cond_1
    const-string v1, "managePersonalization"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    new-instance v0, Lcxc;

    iget-object v1, p0, Lcyt;->mSettings:Lcke;

    iget-object v2, p0, Lcyt;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcyt;->mVoiceSettings:Lhym;

    iget-object v4, p0, Lcyt;->ble:Lhxr;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxc;-><init>(Lcke;Landroid/app/Activity;Lhym;Lhxr;)V

    goto :goto_0

    .line 177
    :cond_2
    const-string v1, "personalizedResults"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    new-instance v0, Lcxq;

    iget-object v1, p0, Lcyt;->mSettings:Lcke;

    iget-object v2, p0, Lcyt;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcyt;->mVoiceSettings:Lhym;

    iget-object v4, p0, Lcyt;->ble:Lhxr;

    invoke-direct {v0, v1, v2, v3, v4}, Lcxq;-><init>(Lcke;Landroid/app/Activity;Lhym;Lhxr;)V

    goto :goto_0

    .line 180
    :cond_3
    const-string v1, "bluetoothHeadset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 181
    new-instance v0, Lcvb;

    iget-object v1, p0, Lcyt;->mSearchConfig:Lcjs;

    invoke-direct {v0, v1}, Lcvb;-><init>(Lcjs;)V

    goto :goto_0

    .line 182
    :cond_4
    const-string v1, "debug_voice_controller"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 183
    new-instance v0, Lcvq;

    iget-object v1, p0, Lcyt;->mDebugFeatures:Lckw;

    iget-object v2, p0, Lcyt;->mVoiceSettings:Lhym;

    iget-object v3, p0, Lcyt;->ble:Lhxr;

    iget-object v4, p0, Lcyt;->mNetworkInformation:Lgno;

    iget-object v5, p0, Lcyt;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcyt;->mSearchConfig:Lcjs;

    iget-object v7, p0, Lcyt;->mGsaConfigFlags:Lchk;

    invoke-direct/range {v0 .. v7}, Lcvq;-><init>(Lckw;Lhym;Lhxr;Lgno;Landroid/content/Context;Lcjs;Lchk;)V

    goto :goto_0

    .line 186
    :cond_5
    const-string v1, "voice_ime_composite_controller"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 187
    new-instance v0, Lcyw;

    iget-object v1, p0, Lcyt;->mSettings:Lcke;

    iget-object v2, p0, Lcyt;->blj:Ligi;

    invoke-direct {v0, v1, v2}, Lcyw;-><init>(Lcke;Ligi;)V

    goto :goto_0

    .line 188
    :cond_6
    const-string v1, "account_settings_controller"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 189
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v10

    .line 190
    new-instance v0, Laqm;

    iget-object v1, p0, Lcyt;->mSettings:Lcke;

    iget-object v2, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    iget-object v3, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    iget-object v4, p0, Lcyt;->mActivity:Landroid/app/Activity;

    iget-object v5, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DI()Lcpn;

    move-result-object v5

    invoke-virtual {v10}, Lgql;->aJR()Lcgh;

    move-result-object v6

    invoke-interface {v6}, Lcgh;->EU()Lcrr;

    move-result-object v6

    invoke-virtual {v10}, Lgql;->aJq()Lhhq;

    move-result-object v7

    iget-object v8, p0, Lcyt;->mActivity:Landroid/app/Activity;

    invoke-virtual {v10}, Lgql;->aJq()Lhhq;

    move-result-object v10

    iget-object v11, v10, Lhhq;->mSettings:Lhym;

    invoke-virtual {v11}, Lhym;->aFc()Ljava/lang/String;

    iget-object v11, p0, Lcyt;->mActivity:Landroid/app/Activity;

    instance-of v11, v11, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v11, :cond_7

    iget-object v9, p0, Lcyt;->mActivity:Landroid/app/Activity;

    check-cast v9, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v9}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->wr()Leoj;

    move-result-object v9

    :cond_7
    iget-object v11, p0, Lcyt;->mActivity:Landroid/app/Activity;

    iget-object v10, v10, Lhhq;->mSettings:Lhym;

    invoke-static {v11, v10, v9}, Lcss;->a(Landroid/content/Context;Lhym;Leoj;)Lcsq;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Laqm;-><init>(Lcke;Lcrh;Lchk;Landroid/app/Activity;Lcpn;Lcrr;Lhhq;Landroid/content/Context;Lcsq;)V

    goto/16 :goto_0

    :cond_8
    move-object v0, v9

    .line 200
    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 233
    invoke-super {p0}, Lcvc;->onPause()V

    .line 234
    iget-object v0, p0, Lcyt;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lcyt;->bnr:Lchm;

    invoke-virtual {v0, v1}, Lchk;->b(Lchm;)V

    .line 235
    return-void
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    .line 216
    invoke-super {p0}, Lcvc;->onResume()V

    .line 218
    iget-object v0, p0, Lcyt;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lcyt;->bnr:Lchm;

    invoke-virtual {v0, v1}, Lchk;->a(Lchm;)V

    .line 219
    iget-object v0, p0, Lcyt;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Fr()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcyt;->bnt:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 221
    iget-object v0, p0, Lcyt;->bnt:Landroid/preference/Preference;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcyt;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lcyt;->bnr:Lchm;

    invoke-virtual {v0, v1}, Lchk;->b(Lchm;)V

    .line 229
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lcyt;->bnt:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 225
    iget-object v0, p0, Lcyt;->bnt:Landroid/preference/Preference;

    const v1, 0x7f0a059e

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 226
    iget-object v0, p0, Lcyt;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "send_gsa_home_request"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    goto :goto_0
.end method
