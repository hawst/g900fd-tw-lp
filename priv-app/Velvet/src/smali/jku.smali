.class public final Ljku;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ahB:I

.field private eqp:I

.field private eqq:[B

.field private eqr:Z

.field private eqs:I

.field private eqt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 425
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 426
    iput v0, p0, Ljku;->aez:I

    iput v0, p0, Ljku;->ahB:I

    iput v0, p0, Ljku;->eqp:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljku;->eqq:[B

    iput-boolean v1, p0, Ljku;->eqr:Z

    iput v1, p0, Ljku;->eqs:I

    const-string v0, ""

    iput-object v0, p0, Ljku;->eqt:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljku;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljku;->eCz:I

    .line 427
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljku;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljku;->ahB:I

    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljku;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljku;->eqp:I

    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljku;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljku;->eqq:[B

    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljku;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljku;->eqr:Z

    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljku;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Ljku;->eqs:I

    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljku;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljku;->eqt:Ljava/lang/String;

    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljku;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 445
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 446
    const/4 v0, 0x1

    iget v1, p0, Ljku;->ahB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 448
    :cond_0
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 449
    const/4 v0, 0x2

    iget v1, p0, Ljku;->eqp:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 451
    :cond_1
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 452
    const/4 v0, 0x3

    iget-object v1, p0, Ljku;->eqq:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 454
    :cond_2
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 455
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljku;->eqr:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 457
    :cond_3
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 458
    const/4 v0, 0x5

    iget v1, p0, Ljku;->eqs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 460
    :cond_4
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 461
    const/4 v0, 0x6

    iget-object v1, p0, Ljku;->eqt:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 463
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 464
    return-void
.end method

.method public final aIW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Ljku;->eqt:Ljava/lang/String;

    return-object v0
.end method

.method public final as([B)Ljku;
    .locals 1

    .prologue
    .line 349
    if-nez p1, :cond_0

    .line 350
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 352
    :cond_0
    iput-object p1, p0, Ljku;->eqq:[B

    .line 353
    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljku;->aez:I

    .line 354
    return-object p0
.end method

.method public final bbg()Z
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boA()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Ljku;->eqr:Z

    return v0
.end method

.method public final boB()Z
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bow()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Ljku;->eqp:I

    return v0
.end method

.method public final box()Z
    .locals 1

    .prologue
    .line 335
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boy()[B
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Ljku;->eqq:[B

    return-object v0
.end method

.method public final boz()Z
    .locals 1

    .prologue
    .line 357
    iget v0, p0, Ljku;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iu(Z)Ljku;
    .locals 1

    .prologue
    .line 371
    iput-boolean p1, p0, Ljku;->eqr:Z

    .line 372
    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljku;->aez:I

    .line 373
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 468
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 469
    iget v1, p0, Ljku;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 470
    const/4 v1, 0x1

    iget v2, p0, Ljku;->ahB:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 473
    :cond_0
    iget v1, p0, Ljku;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 474
    const/4 v1, 0x2

    iget v2, p0, Ljku;->eqp:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 477
    :cond_1
    iget v1, p0, Ljku;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 478
    const/4 v1, 0x3

    iget-object v2, p0, Ljku;->eqq:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    :cond_2
    iget v1, p0, Ljku;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 482
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljku;->eqr:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 485
    :cond_3
    iget v1, p0, Ljku;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 486
    const/4 v1, 0x5

    iget v2, p0, Ljku;->eqs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    :cond_4
    iget v1, p0, Ljku;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 490
    const/4 v1, 0x6

    iget-object v2, p0, Ljku;->eqt:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 493
    :cond_5
    return v0
.end method

.method public final oY()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Ljku;->ahB:I

    return v0
.end method

.method public final qx(I)Ljku;
    .locals 1

    .prologue
    .line 311
    iput p1, p0, Ljku;->ahB:I

    .line 312
    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljku;->aez:I

    .line 313
    return-object p0
.end method

.method public final qy(I)Ljku;
    .locals 1

    .prologue
    .line 330
    iput p1, p0, Ljku;->eqp:I

    .line 331
    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljku;->aez:I

    .line 332
    return-object p0
.end method

.method public final ws(Ljava/lang/String;)Ljku;
    .locals 1

    .prologue
    .line 409
    if-nez p1, :cond_0

    .line 410
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 412
    :cond_0
    iput-object p1, p0, Ljku;->eqt:Ljava/lang/String;

    .line 413
    iget v0, p0, Ljku;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljku;->aez:I

    .line 414
    return-object p0
.end method
