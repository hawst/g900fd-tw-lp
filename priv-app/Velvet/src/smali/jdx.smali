.class public final Ljdx;
.super Ljsl;
.source "PG"


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private afX:Ljava/lang/String;

.field public aiX:Ljcn;

.field private dNn:I

.field public dOp:Ljck;

.field private dQU:Liym;

.field public ebd:Ljbr;

.field private ecQ:Ljava/lang/String;

.field private ecR:Ljava/lang/String;

.field private ecY:J

.field private ecZ:Ljava/lang/String;

.field private eda:J

.field private edb:I

.field private edc:I

.field public edd:Ljdy;

.field private ede:Liyn;

.field private edf:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v3, p0, Ljdx;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljdx;->ecQ:Ljava/lang/String;

    iput-wide v4, p0, Ljdx;->ecY:J

    const-string v0, ""

    iput-object v0, p0, Ljdx;->ecR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdx;->afX:Ljava/lang/String;

    iput v2, p0, Ljdx;->dNn:I

    const-string v0, ""

    iput-object v0, p0, Ljdx;->ecZ:Ljava/lang/String;

    iput-wide v4, p0, Ljdx;->eda:J

    iput v2, p0, Ljdx;->edb:I

    iput v2, p0, Ljdx;->edc:I

    iput-object v1, p0, Ljdx;->aiX:Ljcn;

    iput-object v1, p0, Ljdx;->aeB:Ljbp;

    iput-object v1, p0, Ljdx;->edd:Ljdy;

    iput-object v1, p0, Ljdx;->ede:Liyn;

    iput-object v1, p0, Ljdx;->dQU:Liym;

    iput-boolean v3, p0, Ljdx;->edf:Z

    iput-object v1, p0, Ljdx;->ebd:Ljbr;

    iput-object v1, p0, Ljdx;->dOp:Ljck;

    iput-object v1, p0, Ljdx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdx;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdx;->ecQ:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdx;->ecR:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdx;->ecZ:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljdx;->eda:J

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdx;->edb:I

    iget v0, p0, Ljdx;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljdx;->edc:I

    iget v0, p0, Ljdx;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Ljdx;->dNn:I

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdx;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdx;->afX:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdx;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljdx;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljdx;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Ljdx;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljdx;->aeB:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljdx;->aeB:Ljbp;

    :cond_2
    iget-object v0, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljdx;->edd:Ljdy;

    if-nez v0, :cond_3

    new-instance v0, Ljdy;

    invoke-direct {v0}, Ljdy;-><init>()V

    iput-object v0, p0, Ljdx;->edd:Ljdy;

    :cond_3
    iget-object v0, p0, Ljdx;->edd:Ljdy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ljdx;->ede:Liyn;

    if-nez v0, :cond_4

    new-instance v0, Liyn;

    invoke-direct {v0}, Liyn;-><init>()V

    iput-object v0, p0, Ljdx;->ede:Liyn;

    :cond_4
    iget-object v0, p0, Ljdx;->ede:Liyn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ljdx;->dQU:Liym;

    if-nez v0, :cond_5

    new-instance v0, Liym;

    invoke-direct {v0}, Liym;-><init>()V

    iput-object v0, p0, Ljdx;->dQU:Liym;

    :cond_5
    iget-object v0, p0, Ljdx;->dQU:Liym;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljdx;->edf:Z

    iget v0, p0, Ljdx;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljdx;->aez:I

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Ljdx;->ebd:Ljbr;

    if-nez v0, :cond_6

    new-instance v0, Ljbr;

    invoke-direct {v0}, Ljbr;-><init>()V

    iput-object v0, p0, Ljdx;->ebd:Ljbr;

    :cond_6
    iget-object v0, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljdx;->ecY:J

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdx;->aez:I

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Ljdx;->dOp:Ljck;

    if-nez v0, :cond_7

    new-instance v0, Ljck;

    invoke-direct {v0}, Ljck;-><init>()V

    iput-object v0, p0, Ljdx;->dOp:Ljck;

    :cond_7
    iget-object v0, p0, Ljdx;->dOp:Ljck;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljdx;->ecQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljdx;->ecR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljdx;->ecZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-wide v2, p0, Ljdx;->eda:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_3
    iget v0, p0, Ljdx;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Ljdx;->edb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_4
    iget v0, p0, Ljdx;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Ljdx;->edc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_5
    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Ljdx;->dNn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_6
    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Ljdx;->afX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Ljdx;->aiX:Ljcn;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Ljdx;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_8
    iget-object v0, p0, Ljdx;->aeB:Ljbp;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_9
    iget-object v0, p0, Ljdx;->edd:Ljdy;

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Ljdx;->edd:Ljdy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_a
    iget-object v0, p0, Ljdx;->ede:Liyn;

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Ljdx;->ede:Liyn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_b
    iget-object v0, p0, Ljdx;->dQU:Liym;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Ljdx;->dQU:Liym;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_c
    iget v0, p0, Ljdx;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    iget-boolean v1, p0, Ljdx;->edf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_d
    iget-object v0, p0, Ljdx;->ebd:Ljbr;

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    iget-object v1, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_e
    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_f

    const/16 v0, 0x10

    iget-wide v2, p0, Ljdx;->ecY:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_f
    iget-object v0, p0, Ljdx;->dOp:Ljck;

    if-eqz v0, :cond_10

    const/16 v0, 0x11

    iget-object v1, p0, Ljdx;->dOp:Ljck;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhU()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdx;->ecQ:Ljava/lang/String;

    return-object v0
.end method

.method public final bhV()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhW()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdx;->ecR:Ljava/lang/String;

    return-object v0
.end method

.method public final bhX()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdx;->ecZ:Ljava/lang/String;

    return-object v0
.end method

.method public final bhZ()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bia()J
    .locals 2

    iget-wide v0, p0, Ljdx;->eda:J

    return-wide v0
.end method

.method public final bib()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bic()I
    .locals 1

    iget v0, p0, Ljdx;->edb:I

    return v0
.end method

.method public final bid()I
    .locals 1

    iget v0, p0, Ljdx;->edc:I

    return v0
.end method

.method public final bie()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bif()Z
    .locals 1

    iget-boolean v0, p0, Ljdx;->edf:Z

    return v0
.end method

.method public final cW(J)Ljdx;
    .locals 1

    iput-wide p1, p0, Ljdx;->eda:J

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final getState()I
    .locals 1

    iget v0, p0, Ljdx;->dNn:I

    return v0
.end method

.method public final hI(Z)Ljdx;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdx;->edf:Z

    iget v0, p0, Ljdx;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljdx;->ecQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljdx;->ecR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljdx;->ecZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Ljdx;->eda:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Ljdx;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Ljdx;->edb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljdx;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Ljdx;->edc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Ljdx;->dNn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Ljdx;->afX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Ljdx;->aiX:Ljcn;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Ljdx;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Ljdx;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Ljdx;->edd:Ljdy;

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Ljdx;->edd:Ljdy;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Ljdx;->ede:Liyn;

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Ljdx;->ede:Liyn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Ljdx;->dQU:Liym;

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    iget-object v2, p0, Ljdx;->dQU:Liym;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Ljdx;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    iget-boolean v2, p0, Ljdx;->edf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Ljdx;->ebd:Ljbr;

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    iget-object v2, p0, Ljdx;->ebd:Ljbr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Ljdx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    iget-wide v2, p0, Ljdx;->ecY:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Ljdx;->dOp:Ljck;

    if-eqz v1, :cond_10

    const/16 v1, 0x11

    iget-object v2, p0, Ljdx;->dOp:Ljck;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    return v0
.end method

.method public final oY(I)Ljdx;
    .locals 1

    iput p1, p0, Ljdx;->dNn:I

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final oZ(I)Ljdx;
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Ljdx;->edb:I

    iget v0, p0, Ljdx;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final ol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdx;->afX:Ljava/lang/String;

    return-object v0
.end method

.method public final om()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tQ(Ljava/lang/String;)Ljdx;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdx;->ecQ:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final tR(Ljava/lang/String;)Ljdx;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdx;->ecR:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final tS(Ljava/lang/String;)Ljdx;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdx;->afX:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final tT(Ljava/lang/String;)Ljdx;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdx;->ecZ:Ljava/lang/String;

    iget v0, p0, Ljdx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdx;->aez:I

    return-object p0
.end method

.method public final yj()Z
    .locals 1

    iget v0, p0, Ljdx;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
