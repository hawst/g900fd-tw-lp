.class abstract Lipl;
.super Ljava/util/AbstractCollection;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 2007
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method abstract aWq()Liph;
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 2037
    invoke-virtual {p0}, Lipl;->aWq()Liph;

    move-result-object v0

    invoke-interface {v0}, Liph;->clear()V

    .line 2038
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2033
    invoke-virtual {p0}, Lipl;->aWq()Liph;

    move-result-object v0

    invoke-interface {v0, p1}, Liph;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 2011
    invoke-virtual {p0}, Lipl;->aWq()Liph;

    move-result-object v0

    invoke-interface {v0}, Liph;->aWn()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2013
    new-instance v1, Lipm;

    invoke-direct {v1, p0, v0}, Lipm;-><init>(Lipl;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 2029
    invoke-virtual {p0}, Lipl;->aWq()Liph;

    move-result-object v0

    invoke-interface {v0}, Liph;->size()I

    move-result v0

    return v0
.end method
