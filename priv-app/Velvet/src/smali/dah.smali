.class public Ldah;
.super Lech;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private aOR:Landroid/os/Bundle;

.field private final aPk:Lcom/google/android/search/shared/service/ClientConfig;

.field final bpb:Landroid/os/IBinder$DeathRecipient;

.field private final bpc:J

.field private final bpd:Lecg;

.field private final bpe:Ldaj;

.field private bpf:Ldas;

.field private bpg:J

.field private bph:Landroid/os/Bundle;

.field private bpi:Z

.field private bpj:I

.field private bpk:Z

.field private bpl:Ldex;

.field private bpm:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bpn:Z

.field private bpo:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cq:Z

.field final mService:Lcom/google/android/search/core/service/SearchService;

.field private mSuggestionsController:Ldep;


# direct methods
.method public constructor <init>(JLcom/google/android/search/core/service/SearchService;Lecg;Lecm;Lcom/google/android/search/shared/service/ClientConfig;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Lech;-><init>()V

    .line 66
    new-instance v0, Ldai;

    invoke-direct {v0, p0}, Ldai;-><init>(Ldah;)V

    iput-object v0, p0, Ldah;->bpb:Landroid/os/IBinder$DeathRecipient;

    .line 113
    iput-wide p1, p0, Ldah;->bpc:J

    .line 114
    iput-object p3, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    .line 115
    iput-object p4, p0, Ldah;->bpd:Lecg;

    .line 116
    iput-object p6, p0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    .line 117
    new-instance v0, Ldaj;

    invoke-direct {v0, p0, p5}, Ldaj;-><init>(Ldah;Lecm;)V

    iput-object v0, p0, Ldah;->bpe:Ldaj;

    .line 118
    return-void
.end method

.method private UI()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 527
    iput-object v0, p0, Ldah;->bpo:Lcom/google/android/shared/search/Query;

    .line 528
    iput-object v0, p0, Ldah;->bpm:Lcom/google/android/shared/search/Query;

    .line 529
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldah;->bpn:Z

    .line 530
    return-void
.end method

.method static synthetic b(Ldah;)Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    return-object v0
.end method


# virtual methods
.method public final Bm()Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    return-object v0
.end method

.method public final Bq()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0}, Lecg;->Bq()V

    .line 411
    :cond_0
    return-void
.end method

.method public final CV()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0}, Ldaj;->CV()V

    .line 306
    return-void
.end method

.method public final Ne()Ldyl;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    return-object v0
.end method

.method public final UA()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Ldah;->bpj:I

    return v0
.end method

.method public final UB()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Ldah;->bph:Landroid/os/Bundle;

    return-object v0
.end method

.method public final UC()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0}, Ldaj;->pingBinder()Z

    move-result v0

    return v0
.end method

.method public final UD()Ldfk;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    return-object v0
.end method

.method public final UE()Leqv;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    return-object v0
.end method

.method public final UF()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0}, Lecg;->UF()V

    .line 348
    :goto_0
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldah;->bpn:Z

    .line 346
    const/4 v0, 0x0

    iput-object v0, p0, Ldah;->bpo:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method public final UG()Ldep;
    .locals 17

    .prologue
    .line 425
    invoke-static {}, Lenu;->auR()V

    .line 426
    move-object/from16 v0, p0

    iget-object v1, v0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->anc()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Ldah;->mSuggestionsController:Ldep;

    if-nez v1, :cond_2

    .line 427
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJU()Lgpu;

    invoke-static {}, Lgpu;->aJv()Ldep;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Ldah;->mSuggestionsController:Ldep;

    .line 429
    move-object/from16 v0, p0

    iget-object v1, v0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->and()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 430
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v15

    invoke-virtual {v15}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v5

    invoke-virtual {v15}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v13

    invoke-virtual {v15}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DQ()Lcpd;

    move-result-object v16

    new-instance v8, Ldfy;

    invoke-direct {v8}, Ldfy;-><init>()V

    sget-object v1, Lcgg;->aVi:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_0
    new-instance v1, Ldel;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ldel;-><init>(I)V

    sget-object v2, Lcgg;->aVi:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcgg;->aVj:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    move-object v10, v1

    :goto_1
    new-instance v1, Ldaf;

    invoke-virtual {v5}, Lcjs;->Lj()I

    move-result v2

    invoke-virtual {v5}, Lcjs;->Li()I

    move-result v3

    invoke-virtual {v13}, Lchk;->HG()I

    move-result v4

    invoke-virtual {v5}, Lcjs;->MP()I

    move-result v5

    invoke-virtual {v13}, Lchk;->HD()I

    move-result v6

    sget-object v7, Lcgg;->aVC:Lcgg;

    invoke-virtual {v7}, Lcgg;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_5

    new-instance v7, Lddy;

    new-instance v9, Ldfb;

    invoke-direct {v9, v13}, Ldfb;-><init>(Lchk;)V

    iget-object v11, v15, Lgql;->mClock:Lemp;

    invoke-direct {v7, v9, v11}, Lddy;-><init>(Ldeb;Lemp;)V

    :goto_2
    new-instance v9, Ldds;

    invoke-direct {v9}, Ldds;-><init>()V

    new-instance v11, Ldel;

    const/4 v12, 0x3

    invoke-direct {v11, v12}, Ldel;-><init>(I)V

    new-instance v12, Ldel;

    const/4 v14, 0x4

    invoke-direct {v12, v14}, Ldel;-><init>(I)V

    invoke-virtual {v15}, Lgql;->aJR()Lcgh;

    move-result-object v14

    invoke-interface {v14}, Lcgh;->EP()Ldfz;

    move-result-object v14

    invoke-direct/range {v1 .. v14}, Ldaf;-><init>(IIIIILdeb;Ldeb;Ldds;Ldeb;Ldeb;Ldeb;Lchk;Ldfz;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldah;->bpe:Ldaj;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcpd;->a(Ldex;)Ldex;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ldah;->bpl:Ldex;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldah;->mSuggestionsController:Ldep;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldah;->bpl:Ldex;

    new-instance v4, Lddn;

    iget-object v5, v15, Lgql;->mClock:Lemp;

    invoke-direct {v4, v1, v13, v5}, Lddn;-><init>(Ldes;Lchk;Lemp;)V

    invoke-virtual {v2, v3, v4}, Ldep;->a(Ldex;Lddn;)V

    .line 432
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Ldah;->mSuggestionsController:Ldep;

    invoke-virtual {v1}, Ldep;->start()V

    .line 434
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Ldah;->mSuggestionsController:Ldep;

    return-object v1

    .line 430
    :cond_3
    new-instance v1, Lddt;

    invoke-direct {v1, v8}, Lddt;-><init>(Ldeb;)V

    move-object v8, v1

    goto/16 :goto_0

    :cond_4
    new-instance v10, Lddt;

    invoke-direct {v10, v1}, Lddt;-><init>(Ldeb;)V

    goto :goto_1

    :cond_5
    new-instance v7, Ldfb;

    invoke-direct {v7, v13}, Ldfb;-><init>(Lchk;)V

    goto :goto_2
.end method

.method final UH()Lcjt;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0}, Lcom/google/android/search/core/service/SearchService;->UH()Lcjt;

    move-result-object v0

    return-object v0
.end method

.method public final Uu()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldah;->cq:Z

    .line 201
    return-void
.end method

.method public final Uv()V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldah;->bpi:Z

    .line 205
    return-void
.end method

.method public final Uw()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Ldah;->bpk:Z

    return v0
.end method

.method public final Ux()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Ldah;->bpi:Z

    return v0
.end method

.method public final Uy()J
    .locals 2

    .prologue
    .line 224
    iget-wide v0, p0, Ldah;->bpg:J

    return-wide v0
.end method

.method public final Uz()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Ldah;->aOR:Landroid/os/Bundle;

    return-object v0
.end method

.method public final a(JLandroid/os/Bundle;Landroid/os/Bundle;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 140
    iget-boolean v0, p0, Ldah;->bpi:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 141
    iput-boolean v1, p0, Ldah;->cq:Z

    .line 142
    iget-object v0, p0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 147
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Ldah;->bpg:J

    .line 154
    :goto_1
    iput-object p3, p0, Ldah;->aOR:Landroid/os/Bundle;

    .line 155
    iput-object p4, p0, Ldah;->bph:Landroid/os/Bundle;

    .line 156
    iput p5, p0, Ldah;->bpj:I

    .line 157
    return-void

    .line 140
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_1
    iput-wide p1, p0, Ldah;->bpg:J

    goto :goto_1

    .line 152
    :cond_2
    iput-wide p1, p0, Ldah;->bpg:J

    goto :goto_1
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;I)V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1, p2}, Lecg;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;I)V

    .line 419
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0, p1, p2, p3}, Ldaj;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    .line 290
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0, p1, p2, p3}, Ldaj;->a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V

    .line 298
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1, p2}, Lecg;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    .line 372
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1, p2, p3}, Lecg;->a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V

    .line 388
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0, p1}, Ldaj;->a(Lcom/google/android/shared/speech/HotwordResult;)V

    .line 302
    return-void
.end method

.method final a(Ldas;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Ldah;->bpf:Ldas;

    .line 161
    return-void
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 802
    const-string v0, "AttachedClient"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 803
    const-string v0, "client ID"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Ldah;->bpc:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 804
    const-string v0, "handing over"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldah;->bpi:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 805
    iget-object v0, p0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 806
    return-void
.end method

.method public final a(ZZZLecg;)V
    .locals 3

    .prologue
    .line 184
    if-eqz p1, :cond_0

    if-nez p2, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 185
    iget-object v0, p0, Ldah;->bpf:Ldas;

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Ldah;->bpf:Ldas;

    invoke-virtual {v0, p1, p2, p3}, Ldas;->a(ZZZ)V

    .line 188
    :cond_1
    if-eqz p1, :cond_6

    .line 189
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    :try_start_0
    iget-object v0, p0, Ldah;->bpm:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldah;->bpm:Lcom/google/android/shared/search/Query;

    invoke-interface {p4, v0}, Lecg;->y(Lcom/google/android/shared/search/Query;)V

    :cond_2
    iget-boolean v0, p0, Ldah;->bpn:Z

    if-eqz v0, :cond_3

    invoke-interface {p4}, Lecg;->UF()V

    :cond_3
    iget-object v0, p0, Ldah;->bpo:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldah;->bpo:Lcom/google/android/shared/search/Query;

    invoke-interface {p4, v0}, Lecg;->x(Lcom/google/android/shared/search/Query;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_1
    invoke-direct {p0}, Ldah;->UI()V

    .line 193
    :goto_2
    return-void

    .line 184
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    const-string v1, "AttachedClient"

    const-string v2, "Flush pending items failed"

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 191
    :cond_6
    invoke-direct {p0}, Ldah;->UI()V

    goto :goto_2
.end method

.method public final varargs a([Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0, p1}, Ldaj;->a([Landroid/content/Intent;)V

    .line 282
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1, p2}, Lecg;->b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    .line 380
    :cond_0
    return-void
.end method

.method public final bq(Z)V
    .locals 1

    .prologue
    .line 400
    iput-boolean p1, p0, Ldah;->bpk:Z

    .line 401
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1}, Lecg;->bq(Z)V

    .line 404
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1}, Lecg;->c(Lcom/google/android/shared/search/Suggestion;)V

    .line 396
    :cond_0
    return-void
.end method

.method public final c(Ljyl;)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0, p1}, Ldaj;->c(Ljyl;)V

    .line 294
    return-void
.end method

.method public final ca()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 248
    invoke-static {}, Lenu;->auR()V

    .line 249
    iget-object v0, p0, Ldah;->mSuggestionsController:Ldep;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Ldah;->bpl:Ldex;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Ldah;->mSuggestionsController:Ldep;

    iget-object v1, p0, Ldah;->bpl:Ldex;

    invoke-virtual {v0, v1}, Ldep;->b(Ldex;)V

    .line 253
    :cond_0
    iput-object v2, p0, Ldah;->bpl:Ldex;

    .line 254
    iget-object v0, p0, Ldah;->mSuggestionsController:Ldep;

    invoke-virtual {v0}, Ldep;->stop()V

    .line 255
    iput-object v2, p0, Ldah;->mSuggestionsController:Ldep;

    .line 257
    :cond_1
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0}, Ldaj;->UJ()V

    .line 258
    return-void
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0}, Lecg;->cancel()V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-direct {p0}, Ldah;->UI()V

    goto :goto_0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 244
    iget-wide v0, p0, Ldah;->bpc:J

    return-wide v0
.end method

.method final isActive()Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    return v0
.end method

.method public final isStarted()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Ldah;->cq:Z

    return v0
.end method

.method public final stopListening()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0}, Lecg;->stopListening()V

    .line 364
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 795
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AttachedClient["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Ldah;->bpc:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Ldah;->bpi:Z

    if-eqz v0, :cond_0

    const-string v0, " (HANDING OVER)"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldah;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final ua()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0}, Ldaj;->ua()V

    .line 310
    return-void
.end method

.method public final w(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Ldah;->bpe:Ldaj;

    invoke-virtual {v0, p1}, Ldaj;->w(Lcom/google/android/shared/search/Query;)V

    .line 286
    return-void
.end method

.method public final x(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1}, Lecg;->x(Lcom/google/android/shared/search/Query;)V

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    iput-object p1, p0, Ldah;->bpo:Lcom/google/android/shared/search/Query;

    .line 322
    iget-boolean v0, p0, Ldah;->bpn:Z

    if-nez v0, :cond_0

    .line 323
    const/4 v0, 0x0

    iput-object v0, p0, Ldah;->bpm:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method public final y(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/service/SearchService;->e(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Ldah;->bpd:Lecg;

    invoke-interface {v0, p1}, Lecg;->y(Lcom/google/android/shared/search/Query;)V

    .line 338
    :goto_0
    return-void

    .line 333
    :cond_0
    iput-object p1, p0, Ldah;->bpm:Lcom/google/android/shared/search/Query;

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Ldah;->bpo:Lcom/google/android/shared/search/Query;

    .line 336
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldah;->bpn:Z

    goto :goto_0
.end method
