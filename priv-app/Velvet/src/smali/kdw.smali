.class final Lkdw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkde;


# instance fields
.field private final eTB:Ljava/io/DataOutputStream;

.field private final eTC:Z

.field private final eTU:Ljava/io/ByteArrayOutputStream;

.field private final eTV:Ljava/io/DataOutputStream;


# direct methods
.method constructor <init>(Ljava/io/OutputStream;Z)V
    .locals 5

    .prologue
    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    .line 295
    iput-boolean p2, p0, Lkdw;->eTC:Z

    .line 297
    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0}, Ljava/util/zip/Deflater;-><init>()V

    .line 298
    sget-object v1, Lkdu;->eTS:[B

    invoke-virtual {v0, v1}, Ljava/util/zip/Deflater;->setDictionary([B)V

    .line 299
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lkdw;->eTU:Ljava/io/ByteArrayOutputStream;

    .line 300
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-static {}, Lkbl;->byd()Lkbl;

    move-result-object v2

    iget-object v3, p0, Lkdw;->eTU:Ljava/io/ByteArrayOutputStream;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lkbl;->a(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lkdw;->eTV:Ljava/io/DataOutputStream;

    .line 302
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(ILkdb;)V
    .locals 2

    .prologue
    .line 360
    monitor-enter p0

    :try_start_0
    iget v0, p2, Lkdb;->eTj:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 361
    :cond_0
    :try_start_1
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v1, -0x7ffcfffd

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 365
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 366
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v1, 0x7fffffff

    and-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 367
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    iget v1, p2, Lkdb;->eTj:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 368
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 369
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lkdt;)V
    .locals 5

    .prologue
    const v4, 0xffffff

    .line 396
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lkdt;->eTO:I

    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v0

    .line 399
    mul-int/lit8 v1, v0, 0x8

    add-int/lit8 v1, v1, 0x4

    .line 400
    iget-object v2, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v3, -0x7ffcfffc

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 401
    iget-object v2, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    and-int/2addr v1, v4

    or-int/lit8 v1, v1, 0x0

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 402
    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 403
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-gt v0, v1, :cond_1

    .line 404
    invoke-virtual {p1, v0}, Lkdt;->isSet(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {p1, v0}, Lkdt;->tR(I)I

    move-result v1

    .line 406
    iget-object v2, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    and-int v3, v0, v4

    or-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 407
    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    iget-object v2, p1, Lkdt;->eTR:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 403
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 409
    :cond_1
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    monitor-exit p0

    return-void

    .line 396
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZI[BII)V
    .locals 3

    .prologue
    .line 378
    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 379
    :goto_0
    :try_start_0
    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v2, 0x7fffffff

    and-int/2addr v2, p2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 380
    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const v2, 0xffffff

    and-int/2addr v2, p5

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 381
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p3, p4, p5}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    monitor-exit p0

    return-void

    .line 378
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZZIIIILjava/util/List;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 315
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkdw;->eTU:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-interface {p7}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v2, p0, Lkdw;->eTV:Ljava/io/DataOutputStream;

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lkdw;->eTV:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lkdw;->eTV:Ljava/io/DataOutputStream;

    const-string v4, "UTF-8"

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lkdw;->eTV:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 316
    iget-object v0, p0, Lkdw;->eTU:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    add-int/lit8 v3, v0, 0xa

    .line 317
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    move v2, v0

    :goto_1
    if-eqz p2, :cond_2

    const/4 v0, 0x2

    :goto_2
    or-int/2addr v0, v2

    .line 320
    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v2, -0x7ffcffff

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 322
    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    const v2, 0xffffff

    and-int/2addr v2, v3

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 323
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v1, 0x7fffffff

    and-int/2addr v1, p3

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 324
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 325
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 326
    iget-object v0, p0, Lkdw;->eTU:Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 327
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    .line 317
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final declared-synchronized b(ILkdb;)V
    .locals 2

    .prologue
    .line 436
    monitor-enter p0

    :try_start_0
    iget v0, p2, Lkdb;->eTk:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 437
    :cond_0
    :try_start_1
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v1, -0x7ffcfff9

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 441
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 442
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 443
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    iget v1, p2, Lkdb;->eTk:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 444
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized bB(II)V
    .locals 2

    .prologue
    .line 449
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v1, -0x7ffcfff7

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 453
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 454
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 455
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 456
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    monitor-exit p0

    return-void

    .line 449
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized bzf()V
    .locals 0

    .prologue
    .line 306
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    iget-object v1, p0, Lkdw;->eTV:Ljava/io/DataOutputStream;

    invoke-static {v0, v1}, Lkbt;->a(Ljava/io/Closeable;Ljava/io/Closeable;)V

    .line 461
    return-void
.end method

.method public final declared-synchronized flush()V
    .locals 1

    .prologue
    .line 309
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    monitor-exit p0

    return-void

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i(ZI)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 423
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lkdw;->eTC:Z

    rem-int/lit8 v2, p2, 0x2

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eq v3, v2, :cond_1

    .line 424
    :goto_1
    if-eq p1, v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "payload != reply"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 425
    :cond_2
    :try_start_1
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const v1, -0x7ffcfffa

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 429
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 430
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 431
    iget-object v0, p0, Lkdw;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    monitor-exit p0

    return-void
.end method
