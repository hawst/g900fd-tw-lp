.class public final Lbxa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field GR:Landroid/widget/ScrollView;

.field private final aNo:Z

.field final aNp:Lbxg;

.field private aNq:Lcse;

.field final mActivity:Landroid/app/Activity;

.field private mPrefs:Landroid/content/SharedPreferences;

.field public mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ZLbxg;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lbxa;->mActivity:Landroid/app/Activity;

    .line 60
    iput-boolean p2, p0, Lbxa;->aNo:Z

    .line 61
    iput-object p3, p0, Lbxa;->aNp:Lbxg;

    .line 62
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    iput-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    .line 63
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lbxa;->mVoiceSettings:Lhym;

    .line 64
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    .line 65
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iput-object v0, p0, Lbxa;->aNq:Lcse;

    .line 67
    return-void
.end method


# virtual methods
.method public final Bg()V
    .locals 8

    .prologue
    const v7, 0x7f110164

    const v4, 0x7f110162

    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v3, 0x7f040062

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setContentView(I)V

    .line 75
    iget-object v0, p0, Lbxa;->aNq:Lcse;

    iget-object v3, p0, Lbxa;->mVoiceSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v0}, Lgcq;->aEU()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lbxa;->aNq:Lcse;

    iget-object v3, p0, Lbxa;->mVoiceSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v0}, Lgcq;->getPrompt()Ljava/lang/String;

    move-result-object v3

    .line 81
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 83
    iget-object v4, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a069f

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :goto_1
    iget-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "contact_upload_for_communication_actions_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v1, 0x7f110163

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 99
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 100
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 118
    :goto_2
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v1, 0x7f110161

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    .line 121
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v1, 0x7f110166

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 122
    new-instance v1, Lbxc;

    invoke-direct {v1, p0}, Lbxc;-><init>(Lbxa;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v1, 0x7f110167

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 129
    new-instance v1, Lbxd;

    invoke-direct {v1, p0}, Lbxd;-><init>(Lbxa;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lbxe;

    invoke-direct {v1, p0}, Lbxe;-><init>(Lbxa;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 154
    iget-object v0, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lbxf;

    invoke-direct {v1, p0}, Lbxf;-><init>(Lbxa;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 161
    return-void

    :cond_0
    move v0, v2

    .line 75
    goto/16 :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    iget-object v1, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a06a0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 103
    :cond_2
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 105
    new-instance v1, Lbxb;

    invoke-direct {v1, p0}, Lbxb;-><init>(Lbxa;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method final Bh()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 164
    iget-object v1, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 166
    iget-object v2, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lbxa;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v2, v0}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method final Bi()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v1, 0x7f110167

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 172
    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p0}, Lbxa;->Bh()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    const v1, 0x7f0a06a8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const v1, 0x7f0a044e

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public final Bj()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 191
    iget-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hands_free_auto_driving_car_mode_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 192
    iget-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hands_free_auto_nav_car_mode_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 195
    iget-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hands_free_read_notifications_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 198
    iget-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "contact_upload_for_communication_actions_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 202
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 203
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const-class v3, Lbwz;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 209
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v1, 0x7f110165

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 210
    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const-class v2, Lbwz;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 214
    const-string v1, "entry-point"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 217
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 218
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 219
    const-string v0, "android.intent.extra.shortcut.NAME"

    iget-object v2, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0a09bc

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    const-string v0, "android.intent.extra.shortcut.ICON_RESOURCE"

    iget-object v2, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const v3, 0x7f020022

    invoke-static {v2, v3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 224
    const-string v0, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lbxa;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hands_free_enabled"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 232
    iget-boolean v0, p0, Lbxa;->aNo:Z

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 244
    :goto_0
    return-void

    .line 240
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.ENTER_MANUAL_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 241
    iget-object v1, p0, Lbxa;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 242
    iget-object v1, p0, Lbxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
