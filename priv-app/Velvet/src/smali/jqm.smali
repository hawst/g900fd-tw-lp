.class public final Ljqm;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eyY:I

.field private eyZ:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7467
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7468
    iput v0, p0, Ljqm;->aez:I

    iput v0, p0, Ljqm;->eyY:I

    iput v0, p0, Ljqm;->eyZ:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqm;->eCz:I

    .line 7469
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 7397
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqm;->eyY:I

    iget v0, p0, Ljqm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljqm;->eyZ:I

    iget v0, p0, Ljqm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqm;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 7483
    iget v0, p0, Ljqm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7484
    const/4 v0, 0x1

    iget v1, p0, Ljqm;->eyY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 7486
    :cond_0
    iget v0, p0, Ljqm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7487
    const/4 v0, 0x2

    iget v1, p0, Ljqm;->eyZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 7489
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7490
    return-void
.end method

.method public final bsv()I
    .locals 1

    .prologue
    .line 7432
    iget v0, p0, Ljqm;->eyY:I

    return v0
.end method

.method public final getFormat()I
    .locals 1

    .prologue
    .line 7451
    iget v0, p0, Ljqm;->eyZ:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 7494
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7495
    iget v1, p0, Ljqm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7496
    const/4 v1, 0x1

    iget v2, p0, Ljqm;->eyY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7499
    :cond_0
    iget v1, p0, Ljqm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7500
    const/4 v1, 0x2

    iget v2, p0, Ljqm;->eyZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7503
    :cond_1
    return v0
.end method

.method public final ru(I)Ljqm;
    .locals 1

    .prologue
    .line 7435
    iput p1, p0, Ljqm;->eyY:I

    .line 7436
    iget v0, p0, Ljqm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqm;->aez:I

    .line 7437
    return-object p0
.end method

.method public final rv(I)Ljqm;
    .locals 1

    .prologue
    .line 7454
    iput p1, p0, Ljqm;->eyZ:I

    .line 7455
    iget v0, p0, Ljqm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqm;->aez:I

    .line 7456
    return-object p0
.end method
