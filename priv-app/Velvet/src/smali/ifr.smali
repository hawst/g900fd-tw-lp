.class final Lifr;
.super Lifq;
.source "PG"


# static fields
.field private static final dBp:Lifr;

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 256
    new-instance v0, Lifr;

    invoke-direct {v0}, Lifr;-><init>()V

    sput-object v0, Lifr;->dBp:Lifr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lifq;-><init>(B)V

    return-void
.end method

.method static synthetic aVY()Lifr;
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lifr;->dBp:Lifr;

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lifr;->dBp:Lifr;

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 289
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 263
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "value is absent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 293
    const v0, 0x598df91c

    return v0
.end method

.method public final isPresent()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    const-string v0, "Optional.absent()"

    return-object v0
.end method
