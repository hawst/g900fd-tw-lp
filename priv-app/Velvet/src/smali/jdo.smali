.class public final Ljdo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ecz:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljdo;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljdo;->ecz:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljdo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdo;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljdo;->ecz:J

    iget v0, p0, Ljdo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdo;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljdo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v2, p0, Ljdo;->ecz:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhJ()J
    .locals 2

    iget-wide v0, p0, Ljdo;->ecz:J

    return-wide v0
.end method

.method public final cV(J)Ljdo;
    .locals 1

    iput-wide p1, p0, Ljdo;->ecz:J

    iget v0, p0, Ljdo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdo;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Ljdo;->ecz:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method
