.class public final enum Lgqv;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cTG:Lgqv;

.field public static final enum cTH:Lgqv;

.field public static final enum cTI:Lgqv;

.field public static final enum cTJ:Lgqv;

.field public static final enum cTK:Lgqv;

.field public static final enum cTL:Lgqv;

.field private static final synthetic cTN:[Lgqv;


# instance fields
.field public final cTM:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 605
    new-instance v0, Lgqv;

    const-string v1, "CALL"

    const v2, 0x7f0a0811

    invoke-direct {v0, v1, v4, v2}, Lgqv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgqv;->cTG:Lgqv;

    .line 606
    new-instance v0, Lgqv;

    const-string v1, "CONTACT_INFO"

    const v2, 0x7f0a0813

    invoke-direct {v0, v1, v5, v2}, Lgqv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgqv;->cTH:Lgqv;

    .line 607
    new-instance v0, Lgqv;

    const-string v1, "EMAIL"

    const v2, 0x7f0a0815

    invoke-direct {v0, v1, v6, v2}, Lgqv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgqv;->cTI:Lgqv;

    .line 608
    new-instance v0, Lgqv;

    const-string v1, "REMINDER"

    const v2, 0x7f0a0817

    invoke-direct {v0, v1, v7, v2}, Lgqv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgqv;->cTJ:Lgqv;

    .line 609
    new-instance v0, Lgqv;

    const-string v1, "SET_RELATIONSHIP"

    const v2, 0x7f0a0819

    invoke-direct {v0, v1, v8, v2}, Lgqv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgqv;->cTK:Lgqv;

    .line 610
    new-instance v0, Lgqv;

    const-string v1, "SMS"

    const/4 v2, 0x5

    const v3, 0x7f0a081b

    invoke-direct {v0, v1, v2, v3}, Lgqv;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lgqv;->cTL:Lgqv;

    .line 603
    const/4 v0, 0x6

    new-array v0, v0, [Lgqv;

    sget-object v1, Lgqv;->cTG:Lgqv;

    aput-object v1, v0, v4

    sget-object v1, Lgqv;->cTH:Lgqv;

    aput-object v1, v0, v5

    sget-object v1, Lgqv;->cTI:Lgqv;

    aput-object v1, v0, v6

    sget-object v1, Lgqv;->cTJ:Lgqv;

    aput-object v1, v0, v7

    sget-object v1, Lgqv;->cTK:Lgqv;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgqv;->cTL:Lgqv;

    aput-object v2, v0, v1

    sput-object v0, Lgqv;->cTN:[Lgqv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 614
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 615
    iput p3, p0, Lgqv;->cTM:I

    .line 616
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgqv;
    .locals 1

    .prologue
    .line 603
    const-class v0, Lgqv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgqv;

    return-object v0
.end method

.method public static values()[Lgqv;
    .locals 1

    .prologue
    .line 603
    sget-object v0, Lgqv;->cTN:[Lgqv;

    invoke-virtual {v0}, [Lgqv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgqv;

    return-object v0
.end method
