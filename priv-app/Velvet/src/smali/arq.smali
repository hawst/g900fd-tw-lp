.class public final Larq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final mGsaConfigFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field private final mSearchHistoryHelper:Lcrr;

.field private final mVoiceSearchServices:Lhhq;


# direct methods
.method public constructor <init>(Lcrr;Lhhq;Lcrh;Lchk;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Larq;->mSearchHistoryHelper:Lcrr;

    .line 31
    iput-object p2, p0, Larq;->mVoiceSearchServices:Lhhq;

    .line 32
    iput-object p3, p0, Larq;->mLoginHelper:Lcrh;

    .line 33
    iput-object p4, p0, Larq;->mGsaConfigFlags:Lchk;

    .line 34
    return-void
.end method

.method static synthetic a(Larq;)Lhhq;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Larq;->mVoiceSearchServices:Lhhq;

    return-object v0
.end method

.method static synthetic b(Larq;)Lcrh;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Larq;->mLoginHelper:Lcrh;

    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Larq;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final ub()Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 38
    iget-object v0, p0, Larq;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Kl()Z

    move-result v0

    .line 40
    if-nez v0, :cond_0

    .line 68
    :goto_0
    return-object v4

    .line 44
    :cond_0
    iget-object v0, p0, Larq;->mSearchHistoryHelper:Lcrr;

    iget-object v1, p0, Larq;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Larr;

    invoke-direct {v3, p0}, Larr;-><init>(Larq;)V

    invoke-virtual {v0, v1, v2, v3}, Lcrr;->a(Landroid/accounts/Account;ZLemy;)V

    goto :goto_0
.end method
