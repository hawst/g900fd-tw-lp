.class public abstract Lhcd;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final dcn:Ljava/lang/String;

.field private final dco:I

.field private dcp:Landroid/widget/TextView;

.field private dcq:Z

.field private dcr:Lhck;

.field protected dcs:I

.field protected dct:I

.field protected dcu:I

.field private mCardsPrefs:Lcxs;

.field protected mLoginHelper:Lcrh;

.field private mNetworkClient:Lfcx;

.field private mNowOptInHelper:Leux;

.field mNowOptInSettings:Lcin;

.field protected mTaskRunner:Lerk;

.field private mUserClientIdManager:Lewh;

.field private mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lhcd;->dcs:I

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Lhcd;->dct:I

    .line 88
    const/4 v0, 0x2

    iput v0, p0, Lhcd;->dcu:I

    .line 96
    iput-object p1, p0, Lhcd;->dcn:Ljava/lang/String;

    .line 97
    iput p2, p0, Lhcd;->dco:I

    .line 98
    return-void
.end method

.method static synthetic a(Lhcd;Z)V
    .locals 1

    .prologue
    .line 65
    if-nez p1, :cond_0

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lhcd;->kL(I)V

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lhcd;->kM(I)V

    return-void

    :cond_1
    iget v0, p0, Lhcd;->dct:I

    goto :goto_0
.end method

.method private static b(Landroid/text/Spanned;I)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 242
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 244
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v3, Landroid/text/style/StyleSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/StyleSpan;

    .line 246
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    .line 247
    invoke-virtual {v4}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_0

    invoke-virtual {v4}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    .line 249
    :cond_0
    invoke-interface {v2, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 250
    invoke-interface {v2, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    .line 251
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v7, 0x12

    invoke-interface {v2, v6, v5, v4, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 246
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    :cond_2
    return-object v2
.end method

.method private nN(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lhcd;->mUserInteractionLogger:Lcpx;

    const-string v1, "BUTTON_PRESS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lhcd;->dcn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public static z(III)V
    .locals 2

    .prologue
    .line 188
    const/16 v0, 0xa3

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 190
    new-instance v1, Ljix;

    invoke-direct {v1}, Ljix;-><init>()V

    invoke-virtual {v1, p0}, Ljix;->qj(I)Ljix;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljix;->qi(I)Ljix;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljix;->qk(I)Ljix;

    move-result-object v1

    iput-object v1, v0, Litu;->dIT:Ljix;

    .line 194
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 195
    return-void
.end method


# virtual methods
.method protected final K(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 338
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    invoke-virtual {p0}, Lhcd;->awo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 342
    iget v0, p0, Lhcd;->dct:I

    invoke-virtual {p0, v0}, Lhcd;->kM(I)V

    .line 352
    :goto_0
    return-void

    .line 346
    :cond_0
    invoke-virtual {p0}, Lhcd;->aAW()V

    .line 349
    iget-object v0, p0, Lhcd;->mUserClientIdManager:Lewh;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lewh;->ckt:Z

    .line 351
    iget-object v0, p0, Lhcd;->dcr:Lhck;

    iget-object v1, p0, Lhcd;->mTaskRunner:Lerk;

    iget-object v2, p0, Lhcd;->mNowOptInHelper:Leux;

    invoke-virtual {v0, v1, v2, p1}, Lhck;->a(Lerk;Leux;Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method public final a(Lcin;Lcrh;Lcpx;Lcxs;Lewh;Lfcx;Leux;Lerk;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lhcd;->mNowOptInSettings:Lcin;

    .line 129
    iput-object p2, p0, Lhcd;->mLoginHelper:Lcrh;

    .line 130
    iput-object p3, p0, Lhcd;->mUserInteractionLogger:Lcpx;

    .line 131
    iput-object p4, p0, Lhcd;->mCardsPrefs:Lcxs;

    .line 132
    iput-object p5, p0, Lhcd;->mUserClientIdManager:Lewh;

    .line 133
    iput-object p6, p0, Lhcd;->mNetworkClient:Lfcx;

    .line 134
    iput-object p7, p0, Lhcd;->mNowOptInHelper:Leux;

    .line 135
    iput-object p8, p0, Lhcd;->mTaskRunner:Lerk;

    .line 136
    return-void
.end method

.method protected abstract aAW()V
.end method

.method public final aNA()V
    .locals 1

    .prologue
    .line 315
    const-string v0, "ACCEPT_OPT_IN"

    invoke-direct {p0, v0}, Lhcd;->nN(Ljava/lang/String;)V

    .line 316
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lhcd;->kL(I)V

    .line 319
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhcd;->gi(Z)V

    .line 321
    invoke-virtual {p0}, Lhcd;->aNB()V

    .line 322
    return-void
.end method

.method protected abstract aNB()V
.end method

.method protected final aNC()V
    .locals 2

    .prologue
    .line 359
    const-string v0, "DECLINE_OPT_IN"

    invoke-direct {p0, v0}, Lhcd;->nN(Ljava/lang/String;)V

    .line 360
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lhcd;->kL(I)V

    .line 362
    iget-object v0, p0, Lhcd;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_0

    .line 364
    iget-object v1, p0, Lhcd;->mNowOptInSettings:Lcin;

    invoke-interface {v1, v0}, Lcin;->g(Landroid/accounts/Account;)V

    .line 367
    :cond_0
    iget-object v0, p0, Lhcd;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KB()V

    .line 368
    iget-object v0, p0, Lhcd;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TK()V

    .line 370
    iget v0, p0, Lhcd;->dcu:I

    invoke-virtual {p0, v0}, Lhcd;->kM(I)V

    .line 371
    return-void
.end method

.method protected abstract aNx()I
.end method

.method protected final aNy()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 201
    new-array v0, v5, [I

    const v1, 0x1010036

    aput v1, v0, v4

    invoke-virtual {p0, v0}, Lhcd;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 202
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 204
    const v0, 0x7f1103f0

    invoke-virtual {p0, v0}, Lhcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 205
    invoke-virtual {p0}, Lhcd;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0485

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    const v0, 0x7f1103f1

    invoke-virtual {p0, v0}, Lhcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 208
    invoke-virtual {p0}, Lhcd;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    const v0, 0x7f1103f2

    invoke-virtual {p0, v0}, Lhcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 211
    invoke-virtual {p0}, Lhcd;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-static {v2, v1}, Lhcd;->b(Landroid/text/Spanned;I)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    const v0, 0x7f1103f3

    invoke-virtual {p0, v0}, Lhcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 216
    invoke-virtual {p0}, Lhcd;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-static {v2, v1}, Lhcd;->b(Landroid/text/Spanned;I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-boolean v0, p0, Lhcd;->dcq:Z

    if-eqz v0, :cond_0

    .line 223
    const v0, 0x7f1103f4

    invoke-virtual {p0, v0}, Lhcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 224
    const v1, 0x7f0a010c

    invoke-virtual {p0, v1}, Lhcd;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 226
    const v2, 0x7f0a02ee

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lhcd;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    :cond_0
    const v0, 0x7f1101da

    invoke-virtual {p0, v0}, Lhcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhcd;->dcp:Landroid/widget/TextView;

    .line 231
    iget-object v0, p0, Lhcd;->dcp:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    iget-object v0, p0, Lhcd;->dcr:Lhck;

    invoke-virtual {v0}, Lhck;->aND()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    invoke-virtual {p0, v4}, Lhcd;->gi(Z)V

    .line 237
    invoke-virtual {p0}, Lhcd;->aAW()V

    .line 239
    :cond_1
    return-void
.end method

.method public aNz()Ljava/lang/String;
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lhcd;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lhcd;->dcq:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a02ed

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0a02eb

    goto :goto_0
.end method

.method public awa()V
    .locals 9

    .prologue
    .line 108
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DF()Lcin;

    move-result-object v1

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DR()Lcpx;

    move-result-object v3

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DE()Lcxs;

    move-result-object v4

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v5

    invoke-virtual {v5}, Lfdb;->aya()Lewh;

    move-result-object v5

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v6

    invoke-virtual {v6}, Lfdb;->axI()Lfcx;

    move-result-object v6

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v7

    invoke-virtual {v7}, Lfdb;->ayh()Leux;

    move-result-object v7

    iget-object v8, v0, Lgql;->mAsyncServices:Lema;

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lhcd;->a(Lcin;Lcrh;Lcpx;Lcxs;Lewh;Lfcx;Leux;Lerk;)V

    .line 117
    return-void
.end method

.method protected final awo()Z
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lhcd;->mNetworkClient:Lfcx;

    invoke-interface {v0}, Lfcx;->awo()Z

    move-result v0

    return v0
.end method

.method protected abstract gi(Z)V
.end method

.method protected final kL(I)V
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Lhcd;->dco:I

    invoke-virtual {p0}, Lhcd;->aNx()I

    move-result v1

    invoke-static {v0, p1, v1}, Lhcd;->z(III)V

    .line 179
    return-void
.end method

.method protected abstract kM(I)V
.end method

.method protected final nM(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 161
    if-eqz p1, :cond_0

    .line 162
    iget-object v0, p0, Lhcd;->mUserInteractionLogger:Lcpx;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lhcd;->dcn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpx;->io(Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lhcd;->mUserInteractionLogger:Lcpx;

    iget-object v1, p0, Lhcd;->dcn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcpx;->io(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 305
    iget-object v0, p0, Lhcd;->dcp:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 306
    const-string v0, "LEARN_MORE"

    invoke-direct {p0, v0}, Lhcd;->nN(Ljava/lang/String;)V

    .line 307
    new-instance v0, Lhcf;

    invoke-direct {v0}, Lhcf;-><init>()V

    invoke-virtual {p0}, Lhcd;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lhcf;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 309
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lhcd;->mNowOptInSettings:Lcin;

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lhcd;->awa()V

    .line 143
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 145
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lhcd;->dcq:Z

    .line 147
    invoke-virtual {p0}, Lhcd;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "optin_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lhck;

    iput-object v0, p0, Lhcd;->dcr:Lhck;

    .line 148
    iget-object v0, p0, Lhcd;->dcr:Lhck;

    if-nez v0, :cond_1

    .line 149
    new-instance v0, Lhck;

    invoke-direct {v0}, Lhck;-><init>()V

    iput-object v0, p0, Lhcd;->dcr:Lhck;

    .line 150
    invoke-virtual {p0}, Lhcd;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lhcd;->dcr:Lhck;

    const-string v2, "optin_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 152
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhcd;->nM(Ljava/lang/String;)V

    .line 158
    return-void
.end method
