.class public final Lcuw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final bkD:Ljava/lang/String;

.field private final mConfig:Lcjs;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcjs;Landroid/content/ContentResolver;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    iput-object p1, p0, Lcuw;->mConfig:Lcjs;

    .line 324
    iput-object p2, p0, Lcuw;->mContentResolver:Landroid/content/ContentResolver;

    .line 325
    iput-object p3, p0, Lcuw;->mPreferences:Landroid/content/SharedPreferences;

    .line 326
    iput-object p4, p0, Lcuw;->bkD:Ljava/lang/String;

    .line 327
    return-void
.end method


# virtual methods
.method public final Tg()Ljava/lang/Boolean;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 331
    iget-object v0, p0, Lcuw;->mConfig:Lcjs;

    invoke-static {v0}, Lcum;->b(Lcjs;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    const-string v0, "UpdateIcingCorporaService"

    const-string v1, "Ignoring applications task because apps corpus disabled."

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 333
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 376
    :goto_0
    return-object v0

    .line 336
    :cond_0
    iget-object v0, p0, Lcuw;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v0

    .line 337
    iget-object v3, p0, Lcuw;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->MD()I

    move-result v3

    .line 339
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcuw;->mPreferences:Landroid/content/SharedPreferences;

    const-string v7, "KEY_LAST_APPLICATIONS_UPDATE"

    invoke-interface {v6, v7, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcuw;->mPreferences:Landroid/content/SharedPreferences;

    const-string v9, "applications_last_scores_update_timestamp"

    invoke-interface {v8, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 343
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    .line 348
    const-string v6, "NONE"

    iget-object v7, p0, Lcuw;->bkD:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 349
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 350
    :cond_1
    const-string v6, "FORCE_ALL"

    iget-object v7, p0, Lcuw;->bkD:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "MAYBE"

    iget-object v7, p0, Lcuw;->bkD:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 353
    :cond_2
    iget-object v3, p0, Lcuw;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjC:Landroid/net/Uri;

    invoke-virtual {v3, v4, v10, v10, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v1

    .line 356
    :goto_1
    if-eqz v0, :cond_4

    .line 357
    iget-object v0, p0, Lcuw;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "KEY_LAST_APPLICATIONS_UPDATE"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 365
    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 353
    goto :goto_1

    .line 362
    :cond_4
    const-string v0, "UpdateIcingCorporaService"

    const-string v3, "Full apps update failed"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 366
    :cond_5
    iget-object v3, p0, Lcuw;->bkD:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 367
    iget-object v3, p0, Lcuw;->bkD:Ljava/lang/String;

    .line 368
    iget-object v4, p0, Lcuw;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjC:Landroid/net/Uri;

    invoke-virtual {v4, v5, v10, v3, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_7

    move v0, v1

    .line 371
    :goto_3
    if-nez v0, :cond_6

    .line 372
    const-string v0, "UpdateIcingCorporaService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Apps update for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 374
    :cond_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 368
    goto :goto_3

    .line 376
    :cond_8
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0}, Lcuw;->Tg()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
