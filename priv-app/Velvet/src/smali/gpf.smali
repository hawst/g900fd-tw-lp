.class public final Lgpf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cSe:Ljava/lang/ThreadLocal;


# instance fields
.field private volatile anR:Z

.field private final cSf:Ljava/lang/Object;

.field private final cSg:Ljava/util/concurrent/ExecutorService;

.field private final cSh:Ljava/util/Set;

.field private final cSi:Ljava/util/concurrent/locks/Lock;

.field private cSj:Landroid/webkit/CookieSyncManager;

.field cSk:Landroid/webkit/CookieManager;

.field private cSl:Ljava/lang/String;

.field volatile cSm:J

.field private volatile cSn:Z

.field final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcfo;

.field final mGsaConfig:Lchk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lgpf;->cSe:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lema;Lchk;Lcfo;)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgpf;->cSf:Ljava/lang/Object;

    .line 81
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lgpf;->cSi:Ljava/util/concurrent/locks/Lock;

    .line 146
    iput-object p1, p0, Lgpf;->mContext:Landroid/content/Context;

    .line 150
    const-string v0, "Get Cookies"

    invoke-virtual {p2, v0}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lgpf;->cSg:Ljava/util/concurrent/ExecutorService;

    .line 151
    iput-object p3, p0, Lgpf;->mGsaConfig:Lchk;

    .line 152
    iget-object v0, p0, Lgpf;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->FF()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lgpf;->cSh:Ljava/util/Set;

    .line 153
    iput-object p4, p0, Lgpf;->mCoreSearchServices:Lcfo;

    .line 154
    return-void
.end method

.method public static a(Landroid/content/Context;Lema;Lchk;Lcfo;)Lgpf;
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lgpf;

    invoke-direct {v0, p0, p1, p2, p3}, Lgpf;-><init>(Landroid/content/Context;Lema;Lchk;Lcfo;)V

    return-object v0
.end method

.method public static aIY()V
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lgpf;->cSe:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 168
    return-void
.end method

.method private aIZ()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 172
    invoke-static {}, Lenu;->auQ()V

    .line 174
    :try_start_0
    iget-object v2, p0, Lgpf;->cSi:Ljava/util/concurrent/locks/Lock;

    const-wide/16 v4, 0xa

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Ljava/util/concurrent/locks/Lock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    .line 176
    :try_start_1
    iget-object v2, p0, Lgpf;->cSj:Landroid/webkit/CookieSyncManager;

    if-nez v2, :cond_0

    .line 177
    iget-object v2, p0, Lgpf;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 178
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v2

    iput-object v2, p0, Lgpf;->cSj:Landroid/webkit/CookieSyncManager;

    .line 179
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    iput-object v2, p0, Lgpf;->cSk:Landroid/webkit/CookieManager;

    .line 180
    const/4 v2, 0x1

    iput-boolean v2, p0, Lgpf;->anR:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    :cond_0
    :try_start_2
    iget-object v2, p0, Lgpf;->cSi:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 195
    :goto_0
    return v0

    .line 184
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lgpf;->cSi:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 194
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move v0, v1

    .line 195
    goto :goto_0

    .line 188
    :cond_1
    const v0, 0x9e206a

    :try_start_3
    invoke-static {v0}, Lhwt;->lx(I)V

    .line 189
    const-string v0, "Cookies"

    const-string v2, "Omitting cookies because initialize lock timed out"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    move v0, v1

    .line 190
    goto :goto_0
.end method

.method private nu(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const v5, 0xb5baea

    const/4 v4, 0x0

    .line 279
    sget-object v0, Lgpf;->cSe:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "Cookies"

    const-string v1, "getCookie must not be called on WebView thread"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 282
    :cond_0
    invoke-direct {p0}, Lgpf;->aIZ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 288
    iget-wide v0, p0, Lgpf;->cSm:J

    .line 289
    cmp-long v2, v0, v6

    if-lez v2, :cond_1

    .line 290
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 291
    iget-object v2, p0, Lgpf;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->Jt()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpf;->cSn:Z

    .line 294
    iput-wide v6, p0, Lgpf;->cSm:J

    .line 296
    const v0, 0xd24433

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 297
    const-string v0, "Cookies"

    const-string v1, "Deadlock detected. Future getCookie called will be abandoned"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 298
    iget-object v0, p0, Lgpf;->cSg:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 303
    :cond_1
    iget-boolean v0, p0, Lgpf;->cSn:Z

    if-nez v0, :cond_3

    .line 305
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lgpg;

    invoke-direct {v1, p0, p1}, Lgpg;-><init>(Lgpf;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 306
    iget-object v1, p0, Lgpf;->cSg:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 307
    iget-object v1, p0, Lgpf;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Js()I

    move-result v1

    .line 309
    int-to-long v2, v1

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 325
    :goto_0
    return-object v0

    .line 310
    :catch_0
    move-exception v0

    .line 311
    const-string v1, "Cookies"

    const-string v2, "getCookie timeout"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 312
    invoke-static {v5}, Lhwt;->lx(I)V

    .line 325
    :cond_2
    :goto_1
    const-string v0, ""

    goto :goto_0

    .line 313
    :catch_1
    move-exception v0

    .line 314
    const-string v1, "Cookies"

    const-string v2, "getCookie interrupted"

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 315
    invoke-static {v5}, Lhwt;->lx(I)V

    goto :goto_1

    .line 316
    :catch_2
    move-exception v0

    .line 317
    const-string v1, "Cookies"

    const-string v2, "getCookie exception"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 321
    :cond_3
    const v0, 0xd24495

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 322
    const-string v0, "Cookies"

    const-string v1, "getCookie timed out"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ldyj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 336
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v0, Lgpf;->cSe:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 339
    const-string v0, "Cookies"

    const-string v1, "setCookiesFromHeaders must not be called on WebView thread"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 342
    :cond_0
    iget-boolean v0, p0, Lgpf;->anR:Z

    if-eqz v0, :cond_1

    .line 343
    const-string v0, "Set-Cookie"

    invoke-virtual {p2, v0}, Ldyj;->aF(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_2

    .line 347
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 348
    iget-object v2, p0, Lgpf;->cSk:Landroid/webkit/CookieManager;

    invoke-virtual {v2, p1, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 352
    :cond_1
    const-string v0, "Cookies"

    const-string v1, "Could not set cookies because CookieManager not initialised"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 354
    :cond_2
    return-void
.end method

.method public final aJa()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 357
    invoke-direct {p0}, Lgpf;->aIZ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lgpf;->cSk:Landroid/webkit/CookieManager;

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 359
    iget-object v1, p0, Lgpf;->cSf:Ljava/lang/Object;

    monitor-enter v1

    .line 360
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lgpf;->cSl:Ljava/lang/String;

    .line 361
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    invoke-static {v2}, Lege;->kP(Ljava/lang/String;)V

    .line 364
    :cond_0
    return-void

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aR(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 247
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lgpf;->hH(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v1, p0, Lgpf;->cSf:Ljava/lang/Object;

    monitor-enter v1

    .line 251
    :try_start_0
    iget-object v0, p0, Lgpf;->cSl:Ljava/lang/String;

    invoke-static {v0, p2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 254
    :cond_2
    :try_start_1
    iput-object p2, p0, Lgpf;->cSl:Ljava/lang/String;

    .line 255
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    invoke-virtual {p0, p2}, Lgpf;->nt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 261
    invoke-static {v0}, Lege;->kP(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final getCookie(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lgpf;->nu(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 274
    invoke-virtual {p0, p1, v0}, Lgpf;->aR(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    return-object v0
.end method

.method public final hH(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 268
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpn;->o(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nt(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    .line 221
    :cond_0
    const-string v0, "; "

    invoke-static {p1, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 224
    invoke-static {}, Lior;->aYc()Ljava/util/TreeMap;

    move-result-object v3

    .line 225
    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v2, v0

    .line 226
    const/16 v6, 0x3d

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 227
    if-ltz v6, :cond_1

    .line 228
    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 231
    iget-object v7, p0, Lgpf;->cSh:Ljava/util/Set;

    invoke-interface {v7, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 232
    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 235
    :cond_2
    const-string v0, "; "

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final sync()V
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Lgpf;->aIZ()Z

    .line 206
    iget-object v0, p0, Lgpf;->cSj:Landroid/webkit/CookieSyncManager;

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 207
    return-void
.end method
