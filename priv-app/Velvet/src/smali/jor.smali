.class public final Ljor;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field private dQW:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2075
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2076
    const/4 v0, 0x0

    iput v0, p0, Ljor;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljor;->agv:I

    const-string v0, ""

    iput-object v0, p0, Ljor;->dQW:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljor;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljor;->eCz:I

    .line 2077
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2008
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljor;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljor;->agv:I

    iget v0, p0, Ljor;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljor;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljor;->dQW:Ljava/lang/String;

    iget v0, p0, Ljor;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljor;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2091
    iget v0, p0, Ljor;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2092
    const/4 v0, 0x1

    iget v1, p0, Ljor;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2094
    :cond_0
    iget v0, p0, Ljor;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2095
    const/4 v0, 0x2

    iget-object v1, p0, Ljor;->dQW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2097
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2098
    return-void
.end method

.method public final brA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2056
    iget-object v0, p0, Ljor;->dQW:Ljava/lang/String;

    return-object v0
.end method

.method public final brB()Z
    .locals 1

    .prologue
    .line 2067
    iget v0, p0, Ljor;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 2037
    iget v0, p0, Ljor;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2102
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2103
    iget v1, p0, Ljor;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2104
    const/4 v1, 0x1

    iget v2, p0, Ljor;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2107
    :cond_0
    iget v1, p0, Ljor;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2108
    const/4 v1, 0x2

    iget-object v2, p0, Ljor;->dQW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2111
    :cond_1
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 2045
    iget v0, p0, Ljor;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rb(I)Ljor;
    .locals 1

    .prologue
    .line 2040
    iput p1, p0, Ljor;->agv:I

    .line 2041
    iget v0, p0, Ljor;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljor;->aez:I

    .line 2042
    return-object p0
.end method

.method public final xQ(Ljava/lang/String;)Ljor;
    .locals 1

    .prologue
    .line 2059
    if-nez p1, :cond_0

    .line 2060
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2062
    :cond_0
    iput-object p1, p0, Ljor;->dQW:Ljava/lang/String;

    .line 2063
    iget v0, p0, Ljor;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljor;->aez:I

    .line 2064
    return-object p0
.end method
