.class public final Lkmr;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile faL:[Lkmr;


# instance fields
.field private aez:I

.field private dzl:F

.field private eHY:Ljtn;

.field private faM:Ljava/lang/String;

.field private faN:[Lkmr;

.field private faO:[Lkmo;

.field private faP:Ljava/lang/String;

.field private faQ:[Lkmo;

.field private name:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lkmr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lkmr;->name:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lkmr;->value:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lkmr;->faM:Ljava/lang/String;

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lkmr;->dzl:F

    invoke-static {}, Lkmr;->bCo()[Lkmr;

    move-result-object v0

    iput-object v0, p0, Lkmr;->faN:[Lkmr;

    invoke-static {}, Lkmo;->bCm()[Lkmo;

    move-result-object v0

    iput-object v0, p0, Lkmr;->faO:[Lkmo;

    const-string v0, ""

    iput-object v0, p0, Lkmr;->faP:Ljava/lang/String;

    invoke-static {}, Lkmo;->bCm()[Lkmo;

    move-result-object v0

    iput-object v0, p0, Lkmr;->faQ:[Lkmo;

    iput-object v1, p0, Lkmr;->eHY:Ljtn;

    iput-object v1, p0, Lkmr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkmr;->eCz:I

    .line 110
    return-void
.end method

.method public static bCo()[Lkmr;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lkmr;->faL:[Lkmr;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lkmr;->faL:[Lkmr;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lkmr;

    sput-object v0, Lkmr;->faL:[Lkmr;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lkmr;->faL:[Lkmr;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkmr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkmr;->name:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkmr;->value:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lkmr;->dzl:F

    iget v0, p0, Lkmr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkmr;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkmr;->faM:Ljava/lang/String;

    iget v0, p0, Lkmr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkmr;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lkmr;->faN:[Lkmr;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lkmr;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lkmr;->faN:[Lkmr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lkmr;

    invoke-direct {v3}, Lkmr;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lkmr;->faN:[Lkmr;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lkmr;

    invoke-direct {v3}, Lkmr;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lkmr;->faN:[Lkmr;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lkmr;->faO:[Lkmo;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lkmo;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lkmr;->faO:[Lkmo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lkmo;

    invoke-direct {v3}, Lkmo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lkmr;->faO:[Lkmo;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lkmo;

    invoke-direct {v3}, Lkmo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lkmr;->faO:[Lkmo;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkmr;->faP:Ljava/lang/String;

    iget v0, p0, Lkmr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkmr;->aez:I

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lkmr;->faQ:[Lkmo;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lkmo;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lkmr;->faQ:[Lkmo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lkmo;

    invoke-direct {v3}, Lkmo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lkmr;->faQ:[Lkmo;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lkmo;

    invoke-direct {v3}, Lkmo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lkmr;->faQ:[Lkmo;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lkmr;->eHY:Ljtn;

    if-nez v0, :cond_a

    new-instance v0, Ljtn;

    invoke-direct {v0}, Ljtn;-><init>()V

    iput-object v0, p0, Lkmr;->eHY:Ljtn;

    :cond_a
    iget-object v0, p0, Lkmr;->eHY:Ljtn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x92 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 131
    const/4 v0, 0x1

    iget-object v2, p0, Lkmr;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 132
    const/4 v0, 0x2

    iget-object v2, p0, Lkmr;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 133
    iget v0, p0, Lkmr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x3

    iget v2, p0, Lkmr;->dzl:F

    invoke-virtual {p1, v0, v2}, Ljsj;->a(IF)V

    .line 136
    :cond_0
    iget v0, p0, Lkmr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 137
    const/4 v0, 0x4

    iget-object v2, p0, Lkmr;->faM:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 139
    :cond_1
    iget-object v0, p0, Lkmr;->faN:[Lkmr;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkmr;->faN:[Lkmr;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 140
    :goto_0
    iget-object v2, p0, Lkmr;->faN:[Lkmr;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 141
    iget-object v2, p0, Lkmr;->faN:[Lkmr;

    aget-object v2, v2, v0

    .line 142
    if-eqz v2, :cond_2

    .line 143
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 140
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_3
    iget-object v0, p0, Lkmr;->faO:[Lkmo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lkmr;->faO:[Lkmo;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 148
    :goto_1
    iget-object v2, p0, Lkmr;->faO:[Lkmo;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 149
    iget-object v2, p0, Lkmr;->faO:[Lkmo;

    aget-object v2, v2, v0

    .line 150
    if-eqz v2, :cond_4

    .line 151
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 148
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 155
    :cond_5
    iget v0, p0, Lkmr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 156
    const/16 v0, 0x8

    iget-object v2, p0, Lkmr;->faP:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 158
    :cond_6
    iget-object v0, p0, Lkmr;->faQ:[Lkmo;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lkmr;->faQ:[Lkmo;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 159
    :goto_2
    iget-object v0, p0, Lkmr;->faQ:[Lkmo;

    array-length v0, v0

    if-ge v1, v0, :cond_8

    .line 160
    iget-object v0, p0, Lkmr;->faQ:[Lkmo;

    aget-object v0, v0, v1

    .line 161
    if-eqz v0, :cond_7

    .line 162
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 159
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 166
    :cond_8
    iget-object v0, p0, Lkmr;->eHY:Ljtn;

    if-eqz v0, :cond_9

    .line 167
    const/16 v0, 0x12

    iget-object v1, p0, Lkmr;->eHY:Ljtn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 169
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 170
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 174
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 175
    const/4 v2, 0x1

    iget-object v3, p0, Lkmr;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 177
    const/4 v2, 0x2

    iget-object v3, p0, Lkmr;->value:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    iget v2, p0, Lkmr;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    .line 180
    const/4 v2, 0x3

    iget v3, p0, Lkmr;->dzl:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 183
    :cond_0
    iget v2, p0, Lkmr;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 184
    const/4 v2, 0x4

    iget-object v3, p0, Lkmr;->faM:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    :cond_1
    iget-object v2, p0, Lkmr;->faN:[Lkmr;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lkmr;->faN:[Lkmr;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 188
    :goto_0
    iget-object v3, p0, Lkmr;->faN:[Lkmr;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 189
    iget-object v3, p0, Lkmr;->faN:[Lkmr;

    aget-object v3, v3, v0

    .line 190
    if-eqz v3, :cond_2

    .line 191
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 188
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 196
    :cond_4
    iget-object v2, p0, Lkmr;->faO:[Lkmo;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lkmr;->faO:[Lkmo;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v0

    move v0, v1

    .line 197
    :goto_1
    iget-object v3, p0, Lkmr;->faO:[Lkmo;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    .line 198
    iget-object v3, p0, Lkmr;->faO:[Lkmo;

    aget-object v3, v3, v0

    .line 199
    if-eqz v3, :cond_5

    .line 200
    const/4 v4, 0x7

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 197
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    move v0, v2

    .line 205
    :cond_7
    iget v2, p0, Lkmr;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_8

    .line 206
    const/16 v2, 0x8

    iget-object v3, p0, Lkmr;->faP:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 209
    :cond_8
    iget-object v2, p0, Lkmr;->faQ:[Lkmo;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lkmr;->faQ:[Lkmo;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 210
    :goto_2
    iget-object v2, p0, Lkmr;->faQ:[Lkmo;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 211
    iget-object v2, p0, Lkmr;->faQ:[Lkmo;

    aget-object v2, v2, v1

    .line 212
    if-eqz v2, :cond_9

    .line 213
    const/16 v3, 0x9

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 210
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 218
    :cond_a
    iget-object v1, p0, Lkmr;->eHY:Ljtn;

    if-eqz v1, :cond_b

    .line 219
    const/16 v1, 0x12

    iget-object v2, p0, Lkmr;->eHY:Ljtn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :cond_b
    return v0
.end method
