.class public final Ljzb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eMU:Z

.field private eMV:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5091
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5092
    iput v0, p0, Ljzb;->aez:I

    iput-boolean v0, p0, Ljzb;->eMU:Z

    iput-boolean v0, p0, Ljzb;->eMV:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljzb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzb;->eCz:I

    .line 5093
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5034
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzb;->eMU:Z

    iget v0, p0, Ljzb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzb;->eMV:Z

    iget v0, p0, Ljzb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5107
    iget v0, p0, Ljzb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5108
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljzb;->eMU:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5110
    :cond_0
    iget v0, p0, Ljzb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5111
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljzb;->eMV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5113
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5114
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5118
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5119
    iget v1, p0, Ljzb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5120
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljzb;->eMU:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5123
    :cond_0
    iget v1, p0, Ljzb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5124
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljzb;->eMV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5127
    :cond_1
    return v0
.end method
