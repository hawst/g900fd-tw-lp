.class public final Ljhx;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile emx:[Ljhx;


# instance fields
.field private aez:I

.field private dQo:Ljava/lang/String;

.field public emy:[Ljhy;

.field private emz:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41314
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 41315
    const/4 v0, 0x0

    iput v0, p0, Ljhx;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljhx;->dQo:Ljava/lang/String;

    invoke-static {}, Ljhy;->bmq()[Ljhy;

    move-result-object v0

    iput-object v0, p0, Ljhx;->emy:[Ljhy;

    const-string v0, ""

    iput-object v0, p0, Ljhx;->emz:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljhx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhx;->eCz:I

    .line 41316
    return-void
.end method

.method public static bmn()[Ljhx;
    .locals 2

    .prologue
    .line 41254
    sget-object v0, Ljhx;->emx:[Ljhx;

    if-nez v0, :cond_1

    .line 41255
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 41257
    :try_start_0
    sget-object v0, Ljhx;->emx:[Ljhx;

    if-nez v0, :cond_0

    .line 41258
    const/4 v0, 0x0

    new-array v0, v0, [Ljhx;

    sput-object v0, Ljhx;->emx:[Ljhx;

    .line 41260
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41262
    :cond_1
    sget-object v0, Ljhx;->emx:[Ljhx;

    return-object v0

    .line 41260
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 41047
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhx;->dQo:Ljava/lang/String;

    iget v0, p0, Ljhx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhx;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljhx;->emy:[Ljhy;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhy;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljhx;->emy:[Ljhy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljhy;

    invoke-direct {v3}, Ljhy;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljhx;->emy:[Ljhy;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljhy;

    invoke-direct {v3}, Ljhy;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljhx;->emy:[Ljhy;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhx;->emz:Ljava/lang/String;

    iget v0, p0, Ljhx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhx;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 41331
    iget v0, p0, Ljhx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 41332
    const/4 v0, 0x1

    iget-object v1, p0, Ljhx;->dQo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41334
    :cond_0
    iget-object v0, p0, Ljhx;->emy:[Ljhy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljhx;->emy:[Ljhy;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 41335
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljhx;->emy:[Ljhy;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 41336
    iget-object v1, p0, Ljhx;->emy:[Ljhy;

    aget-object v1, v1, v0

    .line 41337
    if-eqz v1, :cond_1

    .line 41338
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 41335
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41342
    :cond_2
    iget v0, p0, Ljhx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 41343
    const/4 v0, 0x3

    iget-object v1, p0, Ljhx;->emz:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 41345
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 41346
    return-void
.end method

.method public final bch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41270
    iget-object v0, p0, Ljhx;->dQo:Ljava/lang/String;

    return-object v0
.end method

.method public final bmo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41295
    iget-object v0, p0, Ljhx;->emz:Ljava/lang/String;

    return-object v0
.end method

.method public final bmp()Z
    .locals 1

    .prologue
    .line 41306
    iget v0, p0, Ljhx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 41350
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 41351
    iget v1, p0, Ljhx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 41352
    const/4 v1, 0x1

    iget-object v2, p0, Ljhx;->dQo:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41355
    :cond_0
    iget-object v1, p0, Ljhx;->emy:[Ljhy;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljhx;->emy:[Ljhy;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 41356
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljhx;->emy:[Ljhy;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 41357
    iget-object v2, p0, Ljhx;->emy:[Ljhy;

    aget-object v2, v2, v0

    .line 41358
    if-eqz v2, :cond_1

    .line 41359
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 41356
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 41364
    :cond_3
    iget v1, p0, Ljhx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 41365
    const/4 v1, 0x3

    iget-object v2, p0, Ljhx;->emz:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41368
    :cond_4
    return v0
.end method

.method public final vk(Ljava/lang/String;)Ljhx;
    .locals 1

    .prologue
    .line 41273
    if-nez p1, :cond_0

    .line 41274
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41276
    :cond_0
    iput-object p1, p0, Ljhx;->dQo:Ljava/lang/String;

    .line 41277
    iget v0, p0, Ljhx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhx;->aez:I

    .line 41278
    return-object p0
.end method

.method public final vl(Ljava/lang/String;)Ljhx;
    .locals 1

    .prologue
    .line 41298
    if-nez p1, :cond_0

    .line 41299
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41301
    :cond_0
    iput-object p1, p0, Ljhx;->emz:Ljava/lang/String;

    .line 41302
    iget v0, p0, Ljhx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhx;->aez:I

    .line 41303
    return-object p0
.end method
