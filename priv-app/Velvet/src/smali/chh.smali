.class public final Lchh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final aWH:Landroid/util/SparseArray;

.field private static final aWI:Landroid/util/SparseArray;

.field private static final aWJ:Ljava/lang/Boolean;

.field private static final aWK:Ljava/lang/Integer;

.field private static final aWL:[Ljava/lang/Integer;

.field private static final aWM:[[Ljava/lang/Integer;

.field private static final aWN:[Ljava/lang/Long;

.field private static final aWO:Ljava/lang/String;

.field private static final aWP:[Ljava/lang/String;

.field private static aWQ:Landroid/util/SparseArray;

.field private static aWR:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lchh;->aWH:Landroid/util/SparseArray;

    .line 15
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lchh;->aWI:Landroid/util/SparseArray;

    .line 22
    invoke-static {v5, v8}, Lchh;->ag(II)V

    .line 28
    const-string v0, "%1$s://%2$s/search?redir_esc="

    invoke-static {v6, v0}, Lchh;->f(ILjava/lang/String;)V

    .line 32
    invoke-static {v7, v7}, Lchh;->ag(II)V

    .line 35
    const/4 v0, 0x4

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "en-US"

    aput-object v1, v0, v4

    const-string v1, "en-GB"

    aput-object v1, v0, v5

    const-string v1, "en-001"

    aput-object v1, v0, v6

    const-string v1, "en-AU"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "en-CA"

    aput-object v2, v0, v1

    const-string v1, "en-IN"

    aput-object v1, v0, v8

    const/4 v1, 0x6

    const-string v2, "en-NZ"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "en-ZA"

    aput-object v2, v0, v1

    invoke-static {v8, v0}, Lchh;->c(I[Ljava/lang/String;)V

    .line 50
    const/4 v0, 0x6

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 59
    const/4 v0, 0x7

    const/16 v1, 0x1dff

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 63
    const/16 v0, 0x8

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "en-US"

    aput-object v2, v1, v4

    const-string v2, "en-GB"

    aput-object v2, v1, v5

    const-string v2, "en-001"

    aput-object v2, v1, v6

    const-string v2, "en-AU"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "en-CA"

    aput-object v3, v1, v2

    const-string v2, "en-IN"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "en-NZ"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "en-ZA"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 75
    const/16 v0, 0x9

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 79
    const/16 v0, 0xa

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 83
    const/16 v0, 0xb

    const/16 v1, 0x18

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 87
    const/16 v0, 0xc

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 91
    const/16 v0, 0xd

    const v1, 0x493e0

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 95
    const/16 v0, 0xe

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 99
    const/16 v0, 0xf

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 102
    const/16 v0, 0x10

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 106
    const/16 v0, 0x11

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 110
    const/16 v0, 0x12

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 113
    const/16 v0, 0x13

    const/16 v1, 0x18

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 116
    const/16 v0, 0x14

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 119
    const/16 v0, 0x15

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 122
    const/16 v0, 0x16

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 125
    const/16 v0, 0x17

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 129
    const/16 v0, 0x18

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 133
    const/16 v0, 0xf8

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 136
    const/16 v0, 0x19

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 139
    const/16 v0, 0x1a

    const-string v1, "https://www.googleapis.com/plus/v2whitelisted/people"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 142
    const/16 v0, 0x1b

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 149
    const/16 v0, 0x1c

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 152
    const/16 v0, 0x1d

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 155
    const/16 v0, 0x1e

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 157
    const/16 v0, 0x1f

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "content://applications/search_suggest_query"

    aput-object v2, v1, v4

    const-string v2, "content://browser/bookmarks/search_suggest_query"

    aput-object v2, v1, v5

    const-string v2, "content://com.android.chrome.browser/search_suggest_query"

    aput-object v2, v1, v6

    const-string v2, "content://com.android.contacts/search_suggest_query"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "content://com.google.android.videos/search_suggest_query"

    aput-object v3, v1, v2

    const-string v2, "content://com.google.android.music.MusicContent/search/search_suggest_query"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "content://com.google.android.apps.books/search_suggest_query"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 166
    const/16 v0, 0xfa

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 170
    const/16 v0, 0x20

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 173
    const/16 v0, 0xf9

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 178
    const/16 v0, 0xf7

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 181
    const/16 v0, 0x22

    const-string v1, "[          { \"package\" : \"com.google.android.googlequicksearchbox\", \"corpus\" : \"applications_uri\", \"weight\" : 3 },          { \"package\" : \"com.google.android.googlequicksearchbox\", \"corpus\" : \"contacts_contact_id\", \"weight\" : 2 }        ]"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 187
    const/16 v0, 0x23

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 189
    const/16 v0, 0x24

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 191
    const/16 v0, 0x25

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 193
    const/16 v0, 0x26

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 196
    const/16 v0, 0x27

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 198
    const/16 v0, 0x28

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 200
    const/16 v0, 0x29

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 202
    const/16 v0, 0x2a

    invoke-static {v0, v6}, Lchh;->ag(II)V

    .line 204
    const/16 v0, 0x2b

    const/16 v1, 0xf

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 206
    const/16 v0, 0x2c

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 208
    const/16 v0, 0x2d

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 210
    const/16 v0, 0x2e

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 212
    const/16 v0, 0x2f

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 214
    const/16 v0, 0x30

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 216
    const/16 v0, 0x31

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 218
    const/16 v0, 0x32

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 220
    const/16 v0, 0x33

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 222
    const/16 v0, 0x34

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 224
    const/16 v0, 0x35

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 226
    const/16 v0, 0x36

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 228
    const/16 v0, 0x37

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 230
    const/16 v0, 0x38

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 232
    const/16 v0, 0x39

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 234
    const/16 v0, 0x3a

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 236
    const/16 v0, 0x3b

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 238
    const/16 v0, 0x3c

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 240
    const/16 v0, 0x3d

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 242
    const/16 v0, 0x3e

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 244
    const/16 v0, 0x3f

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 246
    const/16 v0, 0x40

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 249
    const/16 v0, 0x41

    const-string v1, ""

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 252
    const/16 v0, 0x42

    const-string v1, "/ajax/pi/ar"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 255
    const/16 v0, 0x43

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 258
    const/16 v0, 0x44

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 261
    const/16 v0, 0x45

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 264
    const/16 v0, 0x46

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 268
    const/16 v0, 0x47

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 273
    const/16 v0, 0x48

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 277
    const/16 v0, 0x49

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 280
    const/16 v0, 0x4a

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 283
    const/16 v0, 0x4b

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 287
    const/16 v0, 0x4c

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 292
    const/16 v0, 0x4d

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "https://accounts.google.com/Login"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 296
    const/16 v0, 0x4e

    const-string v1, "https://%2$s/history/edit"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 299
    const/16 v0, 0x4f

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 303
    const/16 v0, 0x50

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 308
    const/16 v0, 0x51

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 313
    const/16 v0, 0x52

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 318
    const/16 v0, 0x53

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 321
    const/16 v0, 0x54

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 324
    const/16 v0, 0x56

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 327
    const/16 v0, 0x57

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 330
    const/16 v0, 0x58

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 334
    const/16 v0, 0x59

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 337
    const/16 v0, 0x5a

    const-string v1, "https://%1$s/gen_204?ct=android_gsa_url_query&q=%2$s&url=%3$s"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 340
    const/16 v0, 0x5b

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 347
    const/16 v0, 0x5c

    const-string v1, "%1$s://%2$s/search"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 350
    const/16 v0, 0x5d

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 354
    const/16 v0, 0x5e

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 358
    const/16 v0, 0x5f

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 361
    const/16 v0, 0x60

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 367
    const/16 v0, 0x61

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 370
    const/16 v0, 0x62

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 373
    const/16 v0, 0x63

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 376
    const/16 v0, 0x64

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 379
    const/16 v0, 0x65

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 382
    const/16 v0, 0x66

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 385
    const/16 v0, 0x67

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 388
    const/16 v0, 0x68

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 391
    const/16 v0, 0x69

    const/16 v1, 0x18

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 394
    const/16 v0, 0x6a

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 398
    const/16 v0, 0x6b

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 401
    const/16 v0, 0x6c

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 405
    const/16 v0, 0x6d

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 408
    const/16 v0, 0x6f

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 412
    const/16 v0, 0x70

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 417
    const/16 v0, 0x71

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 422
    const/16 v0, 0x72

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 425
    const/16 v0, 0x73

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 429
    const/16 v0, 0x74

    const-string v1, ""

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 431
    const/16 v0, 0x75

    const-string v1, "%1$s://%2$s"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 435
    const/16 v0, 0x76

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 438
    const/16 v0, 0x77

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 439
    const/16 v0, 0x12d

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 443
    const/16 v0, 0x12f

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 447
    const/16 v0, 0x130

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 451
    const/16 v0, 0x12e

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 455
    const/16 v0, 0x12c

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 459
    const/16 v0, 0x109

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 462
    const/16 v0, 0xf1

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 465
    const/16 v0, 0xf2

    const-string v1, "/"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 468
    const/16 v0, 0x78

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 471
    const/16 v0, 0x79

    const-string v1, "GEL"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 475
    const/16 v0, 0x7a

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 478
    const/16 v0, 0x7b

    const/16 v1, 0x4000

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 481
    const/16 v0, 0x7c

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 484
    const/16 v0, 0x7d

    const-string v1, ""

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 489
    const/16 v0, 0x7e

    const-string v1, ""

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 493
    const/16 v0, 0x7f

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "en-US"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 497
    const/16 v0, 0x80

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 500
    const/16 v0, 0x81

    const v1, 0x15180

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 505
    const/16 v0, 0x82

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 508
    const/16 v0, 0x83

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 511
    const/16 v0, 0x84

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 515
    const/16 v0, 0x85

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 518
    const/16 v0, 0x86

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "text/html"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 522
    const/16 v0, 0x87

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "application/json"

    aput-object v2, v1, v4

    const-string v2, "x-application/json"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 527
    const/16 v0, 0x88

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "application/x-protobuffer"

    aput-object v2, v1, v4

    const-string v2, "application/protobuf"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 534
    const/16 v0, 0x89

    const/high16 v1, 0x400000

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 538
    const/16 v0, 0x8a

    const-string v1, ""

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 541
    const/16 v0, 0x8b

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 545
    const/16 v0, 0x8c

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 548
    const/16 v0, 0x8d

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 551
    const/16 v0, 0x8e

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 566
    const/16 v0, 0x8f

    const/16 v1, 0x14

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "refresh_search_history"

    aput-object v2, v1, v4

    const-string v2, "1h"

    aput-object v2, v1, v5

    const-string v2, "flush_analytics"

    aput-object v2, v1, v6

    const-string v2, "15m"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "refresh_search_domain_and_cookies"

    aput-object v3, v1, v2

    const-string v2, "1d"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "update_gservices_config"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "1d"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "update_icing_corpora"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "12h"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "update_language_packs"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "1d"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "refresh_auth_tokens"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "570m"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "log_contact_accounts_to_clearcut"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "1d"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "update_hotword_models"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "2h"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "refresh_audio_history_preference"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "7d"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 592
    const/16 v0, 0x90

    const/16 v1, 0x78

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 597
    const/16 v0, 0x91

    invoke-static {v0, v5}, Lchh;->ag(II)V

    .line 601
    const/16 v0, 0x92

    const/16 v1, 0xe

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 607
    const/16 v0, 0x93

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 613
    const/16 v0, 0x94

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 619
    const/16 v0, 0x95

    const/16 v1, 0xc8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 622
    const/16 v0, 0x96

    const-string v1, "hist"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 625
    const/16 v0, 0x97

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 628
    const/16 v0, 0x98

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 631
    const/16 v0, 0x9b

    const-string v1, "oauth2:https://www.googleapis.com/auth/googlenow"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 634
    const/16 v0, 0x9c

    const-string v1, "oauth2:https://www.googleapis.com/auth/googlenow"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 638
    const/16 v0, 0x9d

    const-string v1, "mobilepersonalfeeds"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 641
    const/16 v0, 0x9e

    const-string v1, "GoogleLogin auth="

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 644
    const/16 v0, 0x9f

    const/16 v1, 0x12c

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 648
    const/16 v0, 0xa0

    invoke-static {v0, v6}, Lchh;->ag(II)V

    .line 653
    const/16 v0, 0xa1

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 657
    const/16 v0, 0xa2

    const/16 v1, 0x190

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 659
    const/16 v0, 0xa3

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 661
    const/16 v0, 0xa4

    const/16 v1, 0xc8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 665
    const/16 v0, 0xa5

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 667
    const/16 v0, 0xa6

    const-string v1, "com.android.chrome"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 669
    const/16 v0, 0xa7

    const-string v1, "omnibox"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 673
    const/16 v0, 0x113

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 676
    const/16 v0, 0xa8

    const/16 v1, 0xc8

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 679
    const/16 v0, 0xa9

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 683
    const/16 v0, 0xaa

    const/16 v1, 0x7530

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 686
    const/16 v0, 0xab

    const-string v1, "android.clients.google.com"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 689
    const/16 v0, 0xac

    const-string v1, "/proxy/gsasuggest/deleteitems"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 693
    const/16 v0, 0xad

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 696
    const/16 v0, 0xae

    const-string v1, "gs_pcr"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 699
    const/16 v0, 0xaf

    const-string v1, "t"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 702
    const/16 v0, 0xb0

    const-string v1, "qsb-android"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 705
    const/16 v0, 0xb1

    const-string v1, "qsb-android-external"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 708
    const/16 v0, 0xb2

    const-string v1, "qsb-android-ssb"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 712
    const/16 v0, 0xb3

    const v1, 0x493e0

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 715
    const/16 v0, 0xb4

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 721
    const/16 v0, 0xb5

    const-string v1, "/s"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 724
    const/16 v0, 0xb6

    invoke-static {v0, v7}, Lchh;->ag(II)V

    .line 727
    const/16 v0, 0xb7

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 729
    const/16 v0, 0xb8

    const-string v1, "https://%2$s/searchdomaincheck?format=gsasearchparameters"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 733
    const/16 v0, 0xb9

    const-string v1, "%1$s://%2$s/complete/search"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 736
    const/16 v0, 0xf3

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 739
    const/16 v0, 0xf4

    const/16 v1, 0xe10

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 743
    const/16 v0, 0xba

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 746
    const/16 v0, 0xbb

    const/16 v1, 0x9c4

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 750
    const/16 v0, 0xbc

    invoke-static {v0, v8}, Lchh;->ag(II)V

    .line 759
    const/16 v0, 0xbd

    const/16 v1, 0x1b

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "en-US"

    aput-object v2, v1, v4

    const-string v2, "en-US/hotword.data"

    aput-object v2, v1, v5

    const-string v2, "Ok Google"

    aput-object v2, v1, v6

    const-string v2, "en-GB"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "en-GB/hotword.data"

    aput-object v3, v1, v2

    const-string v2, "Ok Google"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "en-CA"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "en-US/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "en-AU"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "en-GB/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "en-001"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "en-US/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "fr-FR"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "fr-FR/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "de-DE"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "de-DE/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "de-AT"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "de-DE/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "ru-RU"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "ru-RU/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "Ok Google"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 789
    const/16 v0, 0xbe

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "de-AT"

    aput-object v2, v1, v4

    const-string v2, "de-DE"

    aput-object v2, v1, v5

    const-string v2, "fr-FR"

    aput-object v2, v1, v6

    const-string v2, "en-AU"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "en-CA"

    aput-object v3, v1, v2

    const-string v2, "en-GB"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "en-US"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "en-001"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "ru-RU"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 801
    const/16 v0, 0xbf

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "en-US"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 805
    const/16 v0, 0xc0

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "de-AT"

    aput-object v2, v1, v4

    const-string v2, "de-DE/hotword.data"

    aput-object v2, v1, v5

    const-string v2, "de-DE"

    aput-object v2, v1, v6

    const-string v2, "de-DE/hotword.data"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "fr-FR"

    aput-object v3, v1, v2

    const-string v2, "fr-FR/hotword.data"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "en-AU"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "en-GB/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "en-CA"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "en-US/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "en-GB"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "en-GB/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "en-US"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "en-US/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "en-001"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "en-US/hotword.data"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "ru-RU"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "ru-RU/hotword.data"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 827
    const/16 v0, 0xc1

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "XT1049"

    aput-object v2, v1, v4

    const-string v2, "XT1050"

    aput-object v2, v1, v5

    const-string v2, "XT1052"

    aput-object v2, v1, v6

    const-string v2, "XT1053"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "XT1055"

    aput-object v3, v1, v2

    const-string v2, "XT1056"

    aput-object v2, v1, v8

    const/4 v2, 0x6

    const-string v3, "XT1058"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "XT1060"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "XT912A"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 839
    const/16 v0, 0xc2

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 842
    const/16 v0, 0xfc

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 845
    const/16 v0, 0x10f

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 848
    const/16 v0, 0x110

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 852
    const/16 v0, 0xc3

    invoke-static {v0, v6}, Lchh;->ag(II)V

    .line 856
    const/16 v0, 0xc4

    const v1, 0x5f5e100

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 859
    const/16 v0, 0xc5

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 862
    const/16 v0, 0xc6

    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 865
    const/16 v0, 0xc7

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 868
    const/16 v0, 0xc9

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 871
    const/16 v0, 0xca

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 874
    const/16 v0, 0xcb

    const-string v1, "https://%2$s/history/audio"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 877
    const/16 v0, 0xcc

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 883
    const/16 v0, 0xcd

    const/16 v1, 0x49

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 891
    const/16 v0, 0xce

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 899
    const/16 v0, 0xcf

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 907
    const/16 v0, 0xd0

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 910
    const/16 v0, 0xd1

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 913
    const/16 v0, 0xfb

    invoke-static {v0, v7}, Lchh;->ag(II)V

    .line 918
    const/16 v0, 0xd2

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 922
    const/16 v0, 0xd3

    const v1, 0x249f0

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 927
    const/16 v0, 0xd4

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 931
    const/16 v0, 0xd5

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 935
    const/16 v0, 0xd6

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 939
    const/16 v0, 0xd7

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 954
    const/16 v0, 0xd8

    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 959
    const/16 v0, 0xd9

    const/16 v1, 0x46

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 964
    const/16 v0, 0xda

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 969
    const/16 v0, 0xdb

    const/16 v1, 0x5a

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 973
    const/16 v0, 0xdc

    invoke-static {v0, v6}, Lchh;->ag(II)V

    .line 977
    const/16 v0, 0xfe

    invoke-static {v0, v6}, Lchh;->ag(II)V

    .line 981
    const/16 v0, 0xff

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 987
    const/16 v0, 0xdd

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 992
    const/16 v0, 0xde

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 997
    const/16 v0, 0xdf

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1002
    const/16 v0, 0xe0

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1007
    const/16 v0, 0xe1

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1010
    const/16 v0, 0xe2

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "NID"

    aput-object v2, v1, v4

    const-string v2, "PREF"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lchh;->c(I[Ljava/lang/String;)V

    .line 1015
    const/16 v0, 0xe3

    const-string v1, ""

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 1018
    const/16 v0, 0xe4

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 1023
    const/16 v0, 0xe5

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 1026
    const/16 v0, 0xe6

    const-string v1, ")]}\'\n"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 1029
    const/16 v0, 0xe8

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1032
    const/16 v0, 0x102

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1035
    const/16 v0, 0x106

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1038
    const/16 v0, 0x107

    invoke-static {v0, v4}, Lchh;->ag(II)V

    .line 1041
    const/16 v0, 0x108

    const-string v1, "dummy_header"

    invoke-static {v0, v1}, Lchh;->f(ILjava/lang/String;)V

    .line 1044
    const/16 v0, 0xe9

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1047
    const/16 v0, 0xf6

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1049
    const/16 v0, 0xf5

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1053
    const/16 v0, 0x100

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1058
    const/16 v0, 0x14e

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1063
    const/16 v0, 0x14f

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 1066
    const/16 v0, 0x104

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1069
    const/16 v0, 0x105

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1072
    const/16 v0, 0x112

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 1075
    const/16 v0, 0x111

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 1079
    const/16 v0, 0x103

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1082
    const/16 v0, 0x118

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 1085
    const/16 v0, 0x11d

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1088
    const/16 v0, 0x11f

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 1091
    const/16 v0, 0x11a

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1094
    const/16 v0, 0x120

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Long;

    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const-wide/16 v2, 0xbb8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Long;)V

    .line 1098
    const/16 v0, 0x121

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 1101
    const/16 v0, 0x122

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Long;

    const-wide/16 v2, 0x578

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Long;)V

    .line 1105
    const/4 v0, 0x4

    new-array v0, v0, [[Ljava/lang/Integer;

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    invoke-static {}, Lchh;->Fq()Landroid/util/SparseArray;

    move-result-object v1

    const/16 v2, 0x123

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1109
    const/16 v0, 0x125

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1112
    const/16 v0, 0x126

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1115
    const/16 v0, 0x127

    new-array v1, v7, [Ljava/lang/Integer;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x1388

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 1118
    const/16 v0, 0x128

    new-array v1, v7, [Ljava/lang/Integer;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x1388

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 1121
    const/16 v0, 0x129

    new-array v1, v7, [Ljava/lang/Integer;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x1388

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 1124
    const/16 v0, 0x12a

    new-array v1, v7, [Ljava/lang/Integer;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x1388

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 1127
    const/16 v0, 0x12b

    new-array v1, v7, [Ljava/lang/Integer;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/16 v2, 0x1388

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lchh;->a(I[Ljava/lang/Integer;)V

    .line 1130
    const/16 v0, 0x131

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1133
    const/16 v0, 0x13b

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Lchh;->ag(II)V

    .line 1138
    const/16 v0, 0x146

    invoke-static {v0, v4}, Lchh;->p(IZ)V

    .line 1141
    const/16 v0, 0x161

    invoke-static {v0, v5}, Lchh;->p(IZ)V

    .line 1158
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v5}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lchh;->aWJ:Ljava/lang/Boolean;

    .line 1159
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, v5}, Ljava/lang/Integer;-><init>(I)V

    sput-object v0, Lchh;->aWK:Ljava/lang/Integer;

    .line 1160
    new-array v0, v4, [Ljava/lang/Integer;

    sput-object v0, Lchh;->aWL:[Ljava/lang/Integer;

    .line 1161
    filled-new-array {v4, v4}, [I

    move-result-object v0

    const-class v1, Ljava/lang/Integer;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/Integer;

    sput-object v0, Lchh;->aWM:[[Ljava/lang/Integer;

    .line 1162
    new-array v0, v4, [Ljava/lang/Long;

    sput-object v0, Lchh;->aWN:[Ljava/lang/Long;

    .line 1163
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, Lchh;->aWO:Ljava/lang/String;

    .line 1164
    new-array v0, v4, [Ljava/lang/String;

    sput-object v0, Lchh;->aWP:[Ljava/lang/String;

    return-void
.end method

.method public static Fo()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1177
    sput-object v0, Lchh;->aWQ:Landroid/util/SparseArray;

    .line 1178
    sput-object v0, Lchh;->aWR:Landroid/util/SparseArray;

    .line 1179
    return-void
.end method

.method private static Fp()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 1182
    sget-object v0, Lchh;->aWQ:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    sget-object v0, Lchh;->aWQ:Landroid/util/SparseArray;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lchh;->aWH:Landroid/util/SparseArray;

    goto :goto_0
.end method

.method private static Fq()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 1186
    sget-object v0, Lchh;->aWR:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    sget-object v0, Lchh;->aWR:Landroid/util/SparseArray;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lchh;->aWI:Landroid/util/SparseArray;

    goto :goto_0
.end method

.method private static a(I[Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 1198
    invoke-static {}, Lchh;->Fq()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1199
    return-void
.end method

.method private static a(I[Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 1206
    invoke-static {}, Lchh;->Fq()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1207
    return-void
.end method

.method private static ag(II)V
    .locals 2

    .prologue
    .line 1194
    invoke-static {}, Lchh;->Fp()Landroid/util/SparseArray;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1195
    return-void
.end method

.method private static c(I[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1214
    invoke-static {}, Lchh;->Fq()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1215
    return-void
.end method

.method public static c(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 1171
    sput-object p0, Lchh;->aWQ:Landroid/util/SparseArray;

    .line 1172
    sput-object p1, Lchh;->aWR:Landroid/util/SparseArray;

    .line 1173
    return-void
.end method

.method private static eC(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1218
    invoke-static {}, Lchh;->Fp()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1219
    if-nez v0, :cond_0

    .line 1220
    new-instance v0, Lchi;

    invoke-direct {v0, p0}, Lchi;-><init>(I)V

    throw v0

    .line 1222
    :cond_0
    return-object v0
.end method

.method private static eD(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1226
    invoke-static {}, Lchh;->Fq()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 1227
    if-nez v0, :cond_0

    .line 1228
    new-instance v0, Lchi;

    invoke-direct {v0, p0}, Lchi;-><init>(I)V

    throw v0

    .line 1230
    :cond_0
    return-object v0
.end method

.method public static eE(I)Z
    .locals 2

    .prologue
    .line 1234
    invoke-static {p0}, Lchh;->eC(I)Ljava/lang/Object;

    move-result-object v0

    .line 1235
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/Boolean;

    if-nez v1, :cond_1

    .line 1236
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getBool()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1238
    :cond_1
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static eF(I)[[I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1262
    invoke-static {p0}, Lchh;->eD(I)[Ljava/lang/Object;

    move-result-object v4

    .line 1263
    if-eqz v4, :cond_0

    instance-of v0, v4, [[Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1264
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getIntArray()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1266
    :cond_1
    array-length v0, v4

    new-array v5, v0, [[I

    move v1, v2

    .line 1267
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_3

    .line 1268
    aget-object v0, v4, v1

    check-cast v0, [Ljava/lang/Integer;

    check-cast v0, [Ljava/lang/Integer;

    .line 1269
    array-length v3, v0

    new-array v3, v3, [I

    aput-object v3, v5, v1

    move v3, v2

    .line 1270
    :goto_1
    array-length v6, v0

    if-ge v3, v6, :cond_2

    .line 1271
    aget-object v6, v5, v1

    aget-object v7, v0, v3

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v6, v3

    .line 1270
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1267
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1274
    :cond_3
    return-object v5
.end method

.method public static eG(I)[J
    .locals 6

    .prologue
    .line 1278
    invoke-static {p0}, Lchh;->eD(I)[Ljava/lang/Object;

    move-result-object v2

    .line 1279
    if-eqz v2, :cond_0

    instance-of v0, v2, [Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 1280
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getLongArray()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1282
    :cond_1
    array-length v0, v2

    new-array v3, v0, [J

    .line 1283
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_2

    .line 1284
    aget-object v0, v2, v1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 1283
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1286
    :cond_2
    return-object v3
.end method

.method public static eH(I)I
    .locals 2

    .prologue
    .line 1318
    invoke-static {}, Lchh;->Fp()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 1319
    :goto_0
    if-eqz v0, :cond_8

    .line 1320
    sget-object v1, Lchh;->aWJ:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1321
    const/4 v0, 0x0

    .line 1336
    :goto_1
    return v0

    .line 1318
    :cond_0
    invoke-static {}, Lchh;->Fq()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1322
    :cond_2
    sget-object v1, Lchh;->aWK:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1323
    const/4 v0, 0x1

    goto :goto_1

    .line 1324
    :cond_3
    sget-object v1, Lchh;->aWL:[Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1325
    const/4 v0, 0x2

    goto :goto_1

    .line 1326
    :cond_4
    sget-object v1, Lchh;->aWM:[[Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1327
    const/4 v0, 0x6

    goto :goto_1

    .line 1328
    :cond_5
    sget-object v1, Lchh;->aWN:[Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1329
    const/4 v0, 0x5

    goto :goto_1

    .line 1330
    :cond_6
    sget-object v1, Lchh;->aWO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1331
    const/4 v0, 0x3

    goto :goto_1

    .line 1332
    :cond_7
    sget-object v1, Lchh;->aWP:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1333
    const/4 v0, 0x4

    goto :goto_1

    .line 1336
    :cond_8
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static f(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 1210
    invoke-static {}, Lchh;->Fp()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1211
    return-void
.end method

.method public static getInt(I)I
    .locals 2

    .prologue
    .line 1242
    invoke-static {p0}, Lchh;->eC(I)Ljava/lang/Object;

    move-result-object v0

    .line 1243
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/Integer;

    if-nez v1, :cond_1

    .line 1244
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getInt()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1246
    :cond_1
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static getIntArray(I)[I
    .locals 4

    .prologue
    .line 1250
    invoke-static {p0}, Lchh;->eD(I)[Ljava/lang/Object;

    move-result-object v2

    .line 1251
    if-eqz v2, :cond_0

    instance-of v0, v2, [Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1252
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getIntArray()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1254
    :cond_1
    array-length v0, v2

    new-array v3, v0, [I

    .line 1255
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_2

    .line 1256
    aget-object v0, v2, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 1255
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1258
    :cond_2
    return-object v3
.end method

.method public static getString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1290
    invoke-static {p0}, Lchh;->eC(I)Ljava/lang/Object;

    move-result-object v0

    .line 1291
    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1292
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getString()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1294
    :cond_1
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getStringArray(I)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1298
    invoke-static {p0}, Lchh;->eD(I)[Ljava/lang/Object;

    move-result-object v0

    .line 1299
    if-eqz v0, :cond_0

    instance-of v1, v0, [Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1300
    :cond_0
    new-instance v0, Lchj;

    const-string v1, "getStringArray()"

    invoke-direct {v0, p0, v1}, Lchj;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1302
    :cond_1
    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static p(IZ)V
    .locals 2

    .prologue
    .line 1190
    invoke-static {}, Lchh;->Fp()Landroid/util/SparseArray;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1191
    return-void
.end method
