.class final Lect;
.super Lecn;
.source "PG"


# instance fields
.field private volatile bUX:Lecm;

.field private volatile bUY:Lecm;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 606
    invoke-direct {p0}, Lecn;-><init>()V

    .line 607
    return-void
.end method


# virtual methods
.method public final CV()V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0}, Lecm;->CV()V

    .line 706
    return-void
.end method

.method public final UK()V
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0}, Lecm;->UK()V

    .line 711
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V

    .line 673
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2, p3}, Lecm;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    .line 685
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljava/util/List;ZLcom/google/android/shared/search/SuggestionLogInfo;)V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2, p3, p4}, Lecm;->a(Lcom/google/android/shared/search/Query;Ljava/util/List;ZLcom/google/android/shared/search/SuggestionLogInfo;)V

    .line 619
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2, p3}, Lecm;->a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V

    .line 696
    return-void
.end method

.method public final a(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->a(Lcom/google/android/shared/speech/HotwordResult;)V

    .line 701
    return-void
.end method

.method final a(Lecm;Lecm;)V
    .locals 0

    .prologue
    .line 611
    iput-object p1, p0, Lect;->bUX:Lecm;

    .line 612
    iput-object p2, p0, Lect;->bUY:Lecm;

    .line 613
    return-void
.end method

.method public final a([Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->a([Landroid/content/Intent;)V

    .line 624
    return-void
.end method

.method public final c(ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2}, Lecm;->c(ILandroid/os/Bundle;)V

    .line 721
    return-void
.end method

.method public final d(IIZ)V
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2, p3}, Lecm;->d(IIZ)V

    .line 630
    return-void
.end method

.method public final e(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2}, Lecm;->e(ILjava/lang/String;)V

    .line 640
    return-void
.end method

.method public final en(I)V
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->en(I)V

    .line 650
    return-void
.end method

.method public final eo(I)V
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lect;->bUY:Lecm;

    invoke-interface {v0, p1}, Lecm;->eo(I)V

    .line 657
    return-void
.end method

.method public final fv(I)V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->fv(I)V

    .line 645
    return-void
.end method

.method public final gb(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->gb(Ljava/lang/String;)V

    .line 668
    return-void
.end method

.method public final j([B)V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->j([B)V

    .line 690
    return-void
.end method

.method public final ua()V
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0}, Lecm;->ua()V

    .line 716
    return-void
.end method

.method public final w(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->w(Lcom/google/android/shared/search/Query;)V

    .line 678
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1, p2}, Lecm;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    return-void
.end method

.method public final z(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lect;->bUX:Lecm;

    invoke-interface {v0, p1}, Lecm;->z(Lcom/google/android/shared/search/Query;)V

    .line 635
    return-void
.end method
