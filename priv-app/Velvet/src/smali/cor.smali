.class public final Lcor;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final mGsaConfig:Lchk;

.field private final mLoginHelper:Lcrh;


# direct methods
.method public constructor <init>(Lcrh;Lchk;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcor;->mLoginHelper:Lcrh;

    .line 30
    iput-object p2, p0, Lcor;->mGsaConfig:Lchk;

    .line 31
    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcor;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final ub()Ljava/lang/Void;
    .locals 6

    .prologue
    const-wide/16 v4, 0x1388

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcor;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->IW()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "oauth2:https://www.googleapis.com/auth/googlenow"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcor;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->JO()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mobilepersonalfeeds"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 41
    iget-object v2, p0, Lcor;->mLoginHelper:Lcrh;

    iget-object v3, p0, Lcor;->mLoginHelper:Lcrh;

    invoke-virtual {v3, v0, v4, v5}, Lcrh;->l(Ljava/lang/String;J)Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcrh;->g(Ljava/util/Collection;)V

    .line 45
    iget-object v2, p0, Lcor;->mLoginHelper:Lcrh;

    invoke-virtual {v2, v0, v4, v5}, Lcrh;->l(Ljava/lang/String;J)Ljava/util/Collection;

    goto :goto_0

    .line 48
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method
