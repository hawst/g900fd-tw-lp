.class public final Lezr;
.super Lfbs;
.source "PG"


# instance fields
.field private final aUh:Lcxs;

.field private final mClock:Lemp;

.field private final mNotificationStore:Lffp;


# direct methods
.method public constructor <init>(Ligi;Lfbp;Lcxs;Lffp;Lemp;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p2}, Lfbs;-><init>(Lfbp;)V

    .line 37
    iput-object p3, p0, Lezr;->aUh:Lcxs;

    .line 39
    iput-object p4, p0, Lezr;->mNotificationStore:Lffp;

    .line 40
    iput-object p5, p0, Lezr;->mClock:Lemp;

    .line 41
    return-void
.end method


# virtual methods
.method protected final synthetic a(ILizj;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 24
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Lezd;

    iget-object v1, p2, Lizj;->dSH:Lixj;

    invoke-direct {v0, v1}, Lezd;-><init>(Lixj;)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Lezf;

    iget-object v1, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v2, p0, Lezr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lezf;-><init>(Lizj;Lffp;Lemp;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p2, Lizj;->dTk:Lixs;

    invoke-static {v0}, Lgaa;->a(Lixs;)Ljbp;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lizj;->dTk:Lixs;

    iget-object v0, v0, Lixs;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p2, Lizj;->dTk:Lixs;

    iget-object v0, v0, Lixs;->dMF:[Liyg;

    aget-object v1, v0, v2

    :cond_1
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p2, Lizj;->dTl:Ljbv;

    iget-object v0, v0, Ljbv;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_2

    iget-object v0, p2, Lizj;->dTl:Ljbv;

    iget-object v0, v0, Ljbv;->dMF:[Liyg;

    aget-object v1, v0, v2

    :cond_2
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p2, Lizj;->dSK:Lizr;

    invoke-virtual {v0}, Lizr;->getType()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    new-instance v0, Lezl;

    invoke-direct {v0}, Lezl;-><init>()V

    goto :goto_0

    :cond_3
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lezj;

    iget-object v1, p2, Lizj;->dSi:Ljae;

    invoke-direct {v0, v1}, Lezj;-><init>(Ljae;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p2, Lizj;->dSJ:Ljbz;

    iget-object v0, v0, Ljbz;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v0, p2, Lizj;->dSJ:Ljbz;

    iget-object v0, v0, Ljbz;->dMF:[Liyg;

    aget-object v1, v0, v2

    :cond_4
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lezq;

    iget-object v1, p2, Lizj;->dSB:Ljci;

    invoke-direct {v0, v1}, Lezq;-><init>(Ljci;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lezv;

    invoke-direct {v0, p2}, Lezv;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_5
        0xe -> :sswitch_1
        0x17 -> :sswitch_8
        0x1b -> :sswitch_0
        0x1c -> :sswitch_4
        0x2a -> :sswitch_7
        0x35 -> :sswitch_9
        0x38 -> :sswitch_6
        0x40 -> :sswitch_2
        0x42 -> :sswitch_3
        0x5e -> :sswitch_a
    .end sparse-switch
.end method

.method protected final synthetic a(ILizq;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Lezs;

    invoke-direct {v0, p0}, Lezs;-><init>(Lezr;)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Lezo;

    invoke-direct {v0, p2}, Lezo;-><init>(Lizq;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lezz;

    invoke-direct {v0, p2}, Lezz;-><init>(Lizq;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lfaa;

    invoke-direct {v0, p2}, Lfaa;-><init>(Lizq;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lezy;

    invoke-direct {v0}, Lezy;-><init>()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_0
        0x26 -> :sswitch_1
        0x6c -> :sswitch_2
        0x7a -> :sswitch_3
        0x7b -> :sswitch_4
    .end sparse-switch
.end method

.method protected final synthetic a(Lizj;Ljal;I)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 24
    iget-object v1, p2, Ljal;->dMF:[Liyg;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, p2, Ljal;->dMF:[Liyg;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    :goto_0
    packed-switch p3, :pswitch_data_0

    :goto_1
    return-object v0

    :cond_0
    move-object v1, v0

    goto :goto_0

    :pswitch_0
    new-instance v0, Lezm;

    iget-object v2, p0, Lezr;->aUh:Lcxs;

    iget-object v4, p0, Lezr;->mNotificationStore:Lffp;

    iget-object v5, p0, Lezr;->mClock:Lemp;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lezm;-><init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
