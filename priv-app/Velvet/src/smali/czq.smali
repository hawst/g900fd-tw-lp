.class public abstract Lczq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bom:Ljava/lang/String;

.field protected final bon:Ljava/util/concurrent/ConcurrentHashMap;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lczq;->bon:Ljava/util/concurrent/ConcurrentHashMap;

    .line 48
    iput-object p1, p0, Lczq;->bom:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lczq;->mContext:Landroid/content/Context;

    .line 50
    return-void
.end method


# virtual methods
.method public Ul()Ljava/io/File;
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lczq;->bom:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lczq;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lczq;->bom:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final Um()[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 100
    invoke-virtual {p0}, Lczq;->Ul()Ljava/io/File;

    move-result-object v2

    .line 101
    if-nez v2, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 104
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v1, v4

    .line 105
    if-lez v1, :cond_0

    .line 108
    new-array v1, v1, [B

    .line 110
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 111
    invoke-virtual {v3, v1}, Ljava/io/FileInputStream;->read([B)I

    .line 112
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 117
    goto :goto_0

    .line 115
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public eA()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lczq;->bon:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 67
    return-void
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 57
    iget-object v0, p0, Lczq;->bon:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lczq;->bon:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method
