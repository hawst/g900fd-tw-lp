.class public final Lgfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lger;


# instance fields
.field private final anL:I

.field private final cII:Lgfn;

.field private final cIJ:Z

.field private final cIO:Z


# direct methods
.method public constructor <init>(IZLgfr;Lhhu;Z)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lgfo;->anL:I

    .line 38
    iput-boolean p2, p0, Lgfo;->cIO:Z

    .line 39
    new-instance v0, Lgfh;

    invoke-direct {v0, p4, p3}, Lgfh;-><init>(Lhhu;Lgfr;)V

    iput-object v0, p0, Lgfo;->cII:Lgfn;

    .line 40
    iput-boolean p5, p0, Lgfo;->cIJ:Z

    .line 41
    return-void
.end method

.method private aFG()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lgfo;->anL:I

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x8

    return v0
.end method


# virtual methods
.method public final df()Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 45
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    new-instance v0, Lgfi;

    invoke-direct {v0}, Lgfi;-><init>()V

    .line 51
    :goto_0
    return-object v0

    .line 47
    :cond_0
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 48
    new-instance v0, Lgfk;

    iget v1, p0, Lgfo;->anL:I

    invoke-direct {p0}, Lgfo;->aFG()I

    move-result v2

    iget-boolean v3, p0, Lgfo;->cIO:Z

    iget-object v4, p0, Lgfo;->cII:Lgfn;

    iget-boolean v5, p0, Lgfo;->cIJ:Z

    invoke-direct/range {v0 .. v5}, Lgfk;-><init>(IIZLgfn;Z)V

    goto :goto_0

    .line 51
    :cond_1
    new-instance v0, Lgfm;

    iget v1, p0, Lgfo;->anL:I

    invoke-direct {p0}, Lgfo;->aFG()I

    move-result v2

    iget-boolean v3, p0, Lgfo;->cIO:Z

    iget-object v4, p0, Lgfo;->cII:Lgfn;

    iget-boolean v5, p0, Lgfo;->cIJ:Z

    invoke-direct/range {v0 .. v5}, Lgfm;-><init>(IIZLgfn;Z)V

    goto :goto_0
.end method
