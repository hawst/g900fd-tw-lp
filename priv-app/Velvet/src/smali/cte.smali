.class abstract Lcte;
.super Lctf;
.source "PG"


# instance fields
.field private final bit:Ljava/lang/String;

.field private final biu:Ljava/lang/String;

.field private final biv:Ljava/lang/String;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lctf;-><init>()V

    .line 142
    iput-object p1, p0, Lcte;->mResources:Landroid/content/res/Resources;

    .line 143
    iput-object p2, p0, Lcte;->bit:Ljava/lang/String;

    .line 144
    iput-object p3, p0, Lcte;->biu:Ljava/lang/String;

    .line 145
    iput-object p4, p0, Lcte;->biv:Ljava/lang/String;

    .line 146
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;
.end method

.method protected abstract a(Lcud;JLjava/lang/String;ILjava/lang/String;I)V
.end method

.method public final a(Lcud;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Lcte;->bit:Ljava/lang/String;

    invoke-static {p2, v0}, Lcte;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 161
    iget-object v0, p0, Lcte;->bit:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 162
    iget-object v0, p0, Lcte;->biu:Ljava/lang/String;

    invoke-static {p2, v0}, Lcte;->c(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    .line 163
    iget-object v0, p0, Lcte;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcte;->biv:Ljava/lang/String;

    invoke-static {p2, v2}, Lcte;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v5, v2}, Lcte;->a(Landroid/content/res/Resources;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 166
    const-string v0, "is_super_primary"

    invoke-static {p2, v0}, Lcte;->c(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    move v2, v7

    .line 167
    :goto_0
    if-nez v2, :cond_0

    const-string v0, "is_primary"

    invoke-static {p2, v0}, Lcte;->c(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v7

    .line 168
    :goto_1
    if-eqz v2, :cond_5

    const/4 v7, 0x3

    .line 170
    :cond_1
    :goto_2
    const-string v0, "_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_6

    const-string v2, "Icing.ContactsDataHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v8, "Requested column "

    invoke-direct {v3, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " didn\'t exist in the cursor."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const-wide/16 v2, 0x0

    :goto_3
    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcte;->a(Lcud;JLjava/lang/String;ILjava/lang/String;I)V

    .line 173
    :cond_2
    return-void

    :cond_3
    move v2, v1

    .line 166
    goto :goto_0

    :cond_4
    move v0, v1

    .line 167
    goto :goto_1

    .line 168
    :cond_5
    if-eqz v0, :cond_1

    const/4 v7, 0x2

    goto :goto_2

    .line 170
    :cond_6
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    goto :goto_3
.end method

.method public final h(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 150
    const-string v0, "_id"

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v0, "is_primary"

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v0, "is_super_primary"

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcte;->bit:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcte;->biu:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcte;->biv:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 156
    return-void
.end method
