.class public final Lcfm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcfm;->mContentResolver:Landroid/content/ContentResolver;

    .line 25
    return-void
.end method

.method private a(JLjava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 28
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 29
    const-string v3, "contact_id = ? AND account_type= \'com.google\' AND account_name= ? AND data_set is null"

    .line 33
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    aput-object p3, v4, v2

    .line 34
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "sourceid"

    aput-object v0, v2, v5

    .line 45
    :try_start_0
    iget-object v0, p0, Lcfm;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    const-string v1, "ContactRawSourceIdLookup"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception querying content provider: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v6

    goto :goto_0
.end method


# virtual methods
.method public final b(JLjava/lang/String;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcfm;->a(JLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 55
    new-instance v2, Lcfn;

    invoke-direct {v2, p0, p3, v0}, Lcfn;-><init>(Lcfm;Ljava/lang/String;Ljava/util/Set;)V

    .line 68
    if-eqz v1, :cond_0

    .line 69
    invoke-static {v2, v1}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    .line 71
    :cond_0
    return-object v0
.end method
