.class public final Ledi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldta;


# instance fields
.field private final bVw:Landroid/text/style/ForegroundColorSpan;

.field private bVx:Lcom/google/android/shared/util/CorrectionSpan;

.field private bVy:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Ledi;->bVw:Landroid/text/style/ForegroundColorSpan;

    .line 28
    return-void
.end method

.method private static b(Landroid/view/MotionEvent;Landroid/widget/EditText;)Lcom/google/android/shared/util/CorrectionSpan;
    .locals 4

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 67
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 68
    invoke-virtual {p1, v0, v1}, Landroid/widget/EditText;->getOffsetForPosition(FF)I

    move-result v0

    .line 69
    const/4 v1, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 72
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    const-class v3, Lcom/google/android/shared/util/CorrectionSpan;

    invoke-interface {v2, v0, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/shared/util/CorrectionSpan;

    .line 74
    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 75
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 78
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/text/Editable;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 83
    .line 84
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 87
    check-cast p1, Landroid/text/Spanned;

    invoke-static {p1}, Leqt;->c(Landroid/text/Spanned;)Landroid/text/Spannable;

    move-result-object p1

    .line 89
    :cond_0
    return-object p1
.end method

.method public final a(IILandroid/text/Editable;)V
    .locals 4

    .prologue
    .line 149
    const-class v0, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p3, v0}, Leqt;->a(Landroid/text/Spannable;Ljava/lang/Class;)V

    .line 152
    if-ne p1, p2, :cond_0

    iget-boolean v0, p0, Ledi;->bVy:Z

    if-eqz v0, :cond_0

    .line 153
    const-class v0, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-static {p3, p1, v0}, Leqt;->a(Landroid/text/Spanned;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Ledi;->bVw:Landroid/text/style/ForegroundColorSpan;

    invoke-interface {p3, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {p3, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    const/16 v3, 0x11

    invoke-interface {p3, v1, v2, v0, v3}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 161
    :cond_0
    return-void
.end method

.method public final a(Landroid/text/Spanned;Landroid/text/Editable;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v1, Lcom/google/android/shared/util/CorrectionSpan;

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/shared/util/CorrectionSpan;

    .line 97
    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v3, Lcom/google/android/shared/util/CorrectionSpan;

    invoke-interface {p2, v2, v1, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/shared/util/CorrectionSpan;

    array-length v4, v1

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v1, v3

    .line 98
    invoke-interface {p2, v5}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 97
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 100
    :cond_0
    array-length v3, v0

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v2, v0, v1

    .line 101
    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 102
    invoke-interface {p1, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    .line 103
    invoke-interface {p1, v4, v5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 106
    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result v7

    if-lt v7, v5, :cond_1

    invoke-interface {p2, v4, v5}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 108
    const/16 v6, 0x21

    invoke-interface {p2, v2, v4, v5, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 100
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 111
    :cond_2
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/widget/EditText;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 40
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    :goto_0
    :pswitch_0
    return-void

    .line 43
    :pswitch_1
    invoke-static {p1, p2}, Ledi;->b(Landroid/view/MotionEvent;Landroid/widget/EditText;)Lcom/google/android/shared/util/CorrectionSpan;

    move-result-object v0

    iput-object v0, p0, Ledi;->bVx:Lcom/google/android/shared/util/CorrectionSpan;

    goto :goto_0

    .line 49
    :pswitch_2
    invoke-static {p1, p2}, Ledi;->b(Landroid/view/MotionEvent;Landroid/widget/EditText;)Lcom/google/android/shared/util/CorrectionSpan;

    move-result-object v6

    .line 50
    if-eqz v6, :cond_0

    iget-object v0, p0, Ledi;->bVx:Lcom/google/android/shared/util/CorrectionSpan;

    if-ne v0, v6, :cond_0

    .line 51
    invoke-virtual {v6}, Lcom/google/android/shared/util/CorrectionSpan;->auG()Ljava/lang/String;

    move-result-object v3

    .line 52
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 53
    invoke-interface {v0, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v6}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 55
    invoke-interface {v0, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 57
    :cond_0
    iput-object v7, p0, Ledi;->bVx:Lcom/google/android/shared/util/CorrectionSpan;

    goto :goto_0

    .line 60
    :pswitch_3
    iput-object v7, p0, Ledi;->bVx:Lcom/google/android/shared/util/CorrectionSpan;

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ldto;Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 165
    iget v0, p1, Ldto;->cZ:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ledi;->bVy:Z

    .line 166
    iget-object v0, p1, Ldto;->bJs:Ljava/lang/CharSequence;

    .line 167
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, v0, Landroid/text/Spanned;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ledi;->bVy:Z

    if-eqz v1, :cond_0

    .line 175
    check-cast v0, Landroid/text/Spanned;

    const-class v1, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-static {v0, p2, v1}, Leqt;->a(Landroid/text/Spanned;Landroid/text/Spannable;Ljava/lang/Class;)V

    .line 178
    :cond_0
    return-void

    .line 165
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZLjava/lang/CharSequence;Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 116
    if-eqz p1, :cond_0

    .line 117
    const-class v0, Landroid/text/style/ForegroundColorSpan;

    invoke-static {p3, v0}, Leqt;->a(Landroid/text/Spannable;Ljava/lang/Class;)V

    .line 124
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    instance-of v0, p2, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 129
    check-cast p2, Landroid/text/Spannable;

    invoke-static {p2}, Leqt;->c(Landroid/text/Spanned;)Landroid/text/Spannable;

    .line 132
    :cond_1
    return-void
.end method
