.class public Lhdn;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private dcU:Lbwu;

.field final ddA:Lbwu;

.field private final ddu:Lesk;

.field private final ddv:Lesk;

.field private final ddw:Lesk;

.field private final ddx:Lesk;

.field private final ddy:Lbwu;

.field private final ddz:Lbwu;

.field mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    const v5, 0x7f020062

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    new-instance v0, Lhdo;

    const-string v1, "Lockscreen opt in"

    invoke-direct {v0, p0, v1}, Lhdo;-><init>(Lhdn;Ljava/lang/String;)V

    iput-object v0, p0, Lhdn;->ddu:Lesk;

    .line 66
    new-instance v0, Lhdp;

    const-string v1, "Start intent picker"

    invoke-direct {v0, p0, v1}, Lhdp;-><init>(Lhdn;Ljava/lang/String;)V

    iput-object v0, p0, Lhdn;->ddv:Lesk;

    .line 77
    new-instance v0, Lhdq;

    const-string v1, "Lockscreen opt out"

    invoke-direct {v0, p0, v1}, Lhdq;-><init>(Lhdn;Ljava/lang/String;)V

    iput-object v0, p0, Lhdn;->ddw:Lesk;

    .line 103
    new-instance v0, Lhdr;

    const-string v1, "Kill activity"

    invoke-direct {v0, p0, v1}, Lhdr;-><init>(Lhdn;Ljava/lang/String;)V

    iput-object v0, p0, Lhdn;->ddx:Lesk;

    .line 114
    new-instance v0, Lhds;

    const v3, 0x7f0b0094

    const v4, 0x7f0a06a9

    const v6, 0x7f0a06ac

    iget-object v7, p0, Lhdn;->ddu:Lesk;

    const v8, 0x7f0a06ad

    iget-object v9, p0, Lhdn;->ddw:Lesk;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhds;-><init>(Lhdn;Landroid/app/Activity;IIIILesk;ILesk;)V

    iput-object v0, p0, Lhdn;->ddy:Lbwu;

    .line 149
    new-instance v0, Lhdt;

    const v3, 0x7f0b0095

    const v4, 0x7f0a06ae

    const v6, 0x7f0a06b0

    iget-object v7, p0, Lhdn;->ddv:Lesk;

    const v8, 0x7f0a06b1

    iget-object v9, p0, Lhdn;->ddx:Lesk;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhdt;-><init>(Lhdn;Landroid/app/Activity;IIIILesk;ILesk;)V

    iput-object v0, p0, Lhdn;->ddz:Lbwu;

    .line 172
    new-instance v0, Lhdu;

    const v3, 0x7f0b0096

    const v4, 0x7f0a06b2

    const v5, 0x7f02005a

    const v6, 0x7f0a06b5

    iget-object v7, p0, Lhdn;->ddx:Lesk;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v9}, Lhdu;-><init>(Lhdn;Landroid/app/Activity;IIIILesk;ILesk;)V

    iput-object v0, p0, Lhdn;->ddA:Lbwu;

    return-void
.end method


# virtual methods
.method final a(Lbwu;)V
    .locals 1

    .prologue
    .line 232
    if-eqz p1, :cond_0

    .line 233
    iput-object p1, p0, Lhdn;->dcU:Lbwu;

    .line 234
    iget-object v0, p0, Lhdn;->dcU:Lbwu;

    invoke-virtual {v0}, Lbwu;->getLayoutId()I

    move-result v0

    invoke-virtual {p0, v0}, Lhdn;->setContentView(I)V

    .line 235
    invoke-virtual {p1}, Lbwu;->Be()V

    .line 237
    :cond_0
    return-void
.end method

.method final aNK()Z
    .locals 1

    .prologue
    .line 224
    const-string v0, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-static {p0, v0}, Lhsh;->w(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method final aNL()V
    .locals 3

    .prologue
    .line 244
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 247
    const-string v1, "com.google.android.voicesearch.PICKER_START"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhdn;->startActivityForResult(Landroid/content/Intent;I)V

    .line 249
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 211
    if-nez p1, :cond_0

    .line 212
    const/16 v0, 0x2a

    if-ne p2, v0, :cond_0

    .line 213
    const-string v0, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-static {p0, v0}, Lhsh;->w(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lhdn;->ddA:Lbwu;

    invoke-virtual {p0, v0}, Lhdn;->a(Lbwu;)V

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lhdn;->ddz:Lbwu;

    invoke-virtual {p0, v0}, Lhdn;->a(Lbwu;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lhdn;->dcU:Lbwu;

    iget-object v1, p0, Lhdn;->ddy:Lbwu;

    if-ne v0, v1, :cond_1

    .line 93
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 94
    invoke-virtual {p0}, Lhdn;->finish()V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lhdn;->dcU:Lbwu;

    iget-object v1, p0, Lhdn;->ddz:Lbwu;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lhdn;->dcU:Lbwu;

    iget-object v1, p0, Lhdn;->ddA:Lbwu;

    if-ne v0, v1, :cond_0

    .line 96
    :cond_2
    iget-object v0, p0, Lhdn;->ddy:Lbwu;

    invoke-virtual {p0, v0}, Lhdn;->a(Lbwu;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 201
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 204
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    iput-object v0, p0, Lhdn;->mPrefs:Landroid/content/SharedPreferences;

    .line 206
    iget-object v0, p0, Lhdn;->ddy:Lbwu;

    invoke-virtual {p0, v0}, Lhdn;->a(Lbwu;)V

    .line 207
    return-void
.end method
