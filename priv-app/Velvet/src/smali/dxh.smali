.class public final Ldxh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 217
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    const-class v0, Ljqg;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljqg;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    invoke-direct {v2, v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;)V

    return-object v2
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 217
    new-array v0, p1, [Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    return-object v0
.end method
