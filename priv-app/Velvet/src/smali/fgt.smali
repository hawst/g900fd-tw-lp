.class public final Lfgt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cpa:Lfao;

.field private final cpc:Lezt;

.field private final cqX:Lcin;

.field private final cre:Lgxc;

.field private final mClock:Lemp;

.field private final mEntryProvider:Lfaq;

.field private final mEntryTreePruner:Lfbh;

.field private final mExecutedUserActionStore:Lfcr;

.field private final mGooglePlayServicesHelper:Lcha;

.field private final mLocationOracle:Lfdr;

.field private final mNetworkClient:Lfcx;

.field private final mSearchUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lfaq;Lfao;Lfdr;Lcin;Lcha;Lezt;Lcpn;Lfbh;Lgxc;Lemp;Lfcx;Lfcr;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lfgt;->mEntryProvider:Lfaq;

    .line 81
    iput-object p2, p0, Lfgt;->cpa:Lfao;

    .line 82
    iput-object p3, p0, Lfgt;->mLocationOracle:Lfdr;

    .line 83
    iput-object p4, p0, Lfgt;->cqX:Lcin;

    .line 84
    iput-object p5, p0, Lfgt;->mGooglePlayServicesHelper:Lcha;

    .line 85
    iput-object p6, p0, Lfgt;->cpc:Lezt;

    .line 86
    iput-object p7, p0, Lfgt;->mSearchUrlHelper:Lcpn;

    .line 87
    iput-object p8, p0, Lfgt;->mEntryTreePruner:Lfbh;

    .line 88
    iput-object p9, p0, Lfgt;->cre:Lgxc;

    .line 89
    iput-object p10, p0, Lfgt;->mClock:Lemp;

    .line 90
    iput-object p11, p0, Lfgt;->mNetworkClient:Lfcx;

    .line 91
    iput-object p12, p0, Lfgt;->mExecutedUserActionStore:Lfcr;

    .line 92
    return-void
.end method

.method private static h(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 231
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 232
    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    const-string v1, "com.google.android.apps.sidekick.TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    const-string v1, "com.google.android.apps.sidekick.TRACE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 237
    return-void
.end method


# virtual methods
.method public final aO(Landroid/content/Context;)Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;
    .locals 10

    .prologue
    .line 95
    new-instance v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-direct {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;-><init>()V

    .line 96
    new-instance v3, Lizn;

    invoke-direct {v3}, Lizn;-><init>()V

    .line 98
    iget-object v0, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->axa()J

    move-result-wide v4

    .line 100
    iget-object v0, p0, Lfgt;->mGooglePlayServicesHelper:Lcha;

    invoke-virtual {v0}, Lcha;->Fj()I

    move-result v1

    .line 103
    iget-object v0, p0, Lfgt;->cqX:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-nez v0, :cond_3

    .line 105
    const/4 v0, 0x5

    iput v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    .line 192
    :cond_0
    :goto_0
    iget-object v0, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->axe()Ljava/util/List;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 194
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcq;

    .line 195
    iget-object v1, v0, Lfcq;->coM:Ljcn;

    iput-object v1, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxO:Ljcn;

    .line 196
    iget-object v1, v0, Lfcq;->coN:Lixc;

    if-eqz v1, :cond_f

    iget-object v1, v0, Lfcq;->coN:Lixc;

    invoke-virtual {v1}, Lixc;->getSource()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_f

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_11

    iget-object v0, v0, Lfcq;->coM:Ljcn;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljcn;->bgL()Z

    move-result v1

    if-nez v1, :cond_10

    :cond_1
    const/4 v0, 0x0

    :goto_2
    iput-object v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxP:Ljava/lang/String;

    .line 199
    :cond_2
    iput-object v3, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxE:Lizn;

    .line 200
    return-object v2

    .line 106
    :cond_3
    if-eqz v1, :cond_4

    .line 107
    const-string v0, "RemoteServiceHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Google Play Services not available: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lbgt;->dD(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const/4 v0, 0x4

    iput v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v0, 0x7f0a043c

    packed-switch v1, :pswitch_data_0

    :goto_3
    :pswitch_0
    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxL:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    packed-switch v1, :pswitch_data_1

    const/4 v0, 0x0

    :goto_4
    iput-object v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxM:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lfgt;->mGooglePlayServicesHelper:Lcha;

    invoke-virtual {v0, v1}, Lcha;->eB(I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxN:Landroid/content/Intent;

    .line 121
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-nez v0, :cond_0

    .line 124
    const/16 v0, 0x2e

    invoke-static {p1, v0}, Lfgt;->h(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 111
    :pswitch_1
    const v0, 0x7f0a0434

    goto :goto_3

    :pswitch_2
    const v0, 0x7f0a0436

    goto :goto_3

    :pswitch_3
    const v0, 0x7f0a0435

    goto :goto_3

    :pswitch_4
    const v0, 0x7f0a0437

    goto :goto_3

    .line 114
    :pswitch_5
    const v6, 0x7f0a0438

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :pswitch_6
    const v6, 0x7f0a0439

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :pswitch_7
    const v6, 0x7f0a043a

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 126
    :cond_4
    iget-object v0, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->awV()Z

    move-result v0

    if-nez v0, :cond_5

    .line 129
    const/4 v0, 0x2

    iput v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    goto/16 :goto_0

    .line 130
    :cond_5
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-nez v0, :cond_6

    .line 135
    const/4 v0, 0x2

    iput v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    .line 136
    const/16 v0, 0x2f

    invoke-static {p1, v0}, Lfgt;->h(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 137
    :cond_6
    iget-object v0, p0, Lfgt;->cpa:Lfao;

    invoke-interface {v0}, Lfao;->awR()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 139
    const/4 v0, 0x3

    iput v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    .line 140
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lfgt;->h(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 143
    :cond_7
    const/4 v0, 0x1

    iput v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    .line 145
    iget-object v0, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->awW()Lizo;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_e

    .line 149
    iget-object v1, p0, Lfgt;->mEntryTreePruner:Lfbh;

    invoke-virtual {v1, v0}, Lfbh;->g(Lizo;)Lizo;

    move-result-object v1

    .line 151
    iget-object v0, p0, Lfgt;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v0

    .line 152
    iget-object v6, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v6}, Lfaq;->axg()Ljbp;

    move-result-object v6

    .line 153
    invoke-static {v6}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v6

    iput-object v6, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxB:Landroid/location/Location;

    .line 155
    iput-wide v4, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxF:J

    .line 156
    iget-object v6, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v6}, Lfaq;->axb()J

    move-result-wide v6

    iput-wide v6, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxG:J

    .line 157
    iget-object v6, p0, Lfgt;->mEntryProvider:Lfaq;

    invoke-virtual {v6}, Lfaq;->awY()Z

    move-result v6

    iput-boolean v6, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxH:Z

    .line 159
    new-instance v6, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iget-object v7, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxB:Landroid/location/Location;

    invoke-direct {v6, v0, v7}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>(Landroid/location/Location;Landroid/location/Location;)V

    .line 161
    iget-object v0, p0, Lfgt;->cpc:Lezt;

    invoke-virtual {v0, v6, v1}, Lezt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizo;)V

    .line 162
    iput-object v6, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 166
    new-instance v0, Lfbg;

    invoke-direct {v0, v6}, Lfbg;-><init>(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    .line 168
    invoke-virtual {v0, v1}, Lfbg;->f(Lizo;)V

    .line 170
    iget-object v0, v3, Lizn;->dUI:[Lizo;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizo;

    iput-object v0, v3, Lizn;->dUI:[Lizo;

    .line 172
    iget-object v0, p0, Lfgt;->cre:Lgxc;

    sget-object v6, Lgxd;->cYe:Lgxd;

    iget-object v7, v0, Lgxc;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {v6}, Lgxd;->aLm()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_9

    const/4 v0, 0x0

    :goto_5
    iput-boolean v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxI:Z

    .line 174
    iget-object v6, p0, Lfgt;->cre:Lgxc;

    iget-object v0, v1, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_b

    iget-object v0, v1, Lizo;->dUQ:Lizq;

    iget-object v0, v0, Lizq;->dUW:[Lizq;

    array-length v0, v0

    :goto_6
    iget-object v1, v6, Lgxc;->mPrefs:Landroid/content/SharedPreferences;

    const-string v7, "card_swiped_for_dismiss"

    const/4 v8, 0x0

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_c

    const/4 v1, 0x3

    if-lt v0, v1, :cond_c

    iget-object v0, v6, Lgxc;->mPrefs:Landroid/content/SharedPreferences;

    sget-object v1, Lgxd;->cYe:Lgxd;

    invoke-virtual {v1}, Lgxd;->aLn()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v6, 0x0

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxJ:Z

    .line 177
    iget-object v0, p0, Lfgt;->mNetworkClient:Lfcx;

    invoke-interface {v0}, Lfcx;->awo()Z

    move-result v0

    if-nez v0, :cond_8

    .line 178
    iget-object v0, p0, Lfgt;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v4

    const-wide/32 v4, 0x2bf20

    cmp-long v0, v0, v4

    if-lez v0, :cond_d

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxK:Z

    .line 183
    :cond_8
    iget-object v0, p0, Lfgt;->mExecutedUserActionStore:Lfcr;

    iget-boolean v1, v2, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxK:Z

    invoke-interface {v0, v1}, Lfcr;->fk(Z)V

    goto/16 :goto_0

    .line 172
    :cond_9
    invoke-virtual {v6}, Lgxd;->aLn()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lgxc;->mPrefs:Landroid/content/SharedPreferences;

    const-wide/16 v8, 0x0

    invoke-interface {v7, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-eqz v8, :cond_a

    iget-object v0, v0, Lgxc;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    const-wide/16 v8, 0x1388

    cmp-long v0, v6, v8

    if-ltz v0, :cond_a

    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    const/4 v0, 0x1

    goto :goto_5

    .line 174
    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    goto :goto_7

    .line 178
    :cond_d
    const/4 v0, 0x0

    goto :goto_8

    .line 186
    :cond_e
    const-string v0, "RemoteServiceHelper"

    const-string v1, "Expected to have entries, but entry tree was null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 196
    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_10
    invoke-virtual {v0}, Ljcn;->bgK()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lfgt;->mSearchUrlHelper:Lcpn;

    sget-object v4, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v4, v0}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 114
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method
