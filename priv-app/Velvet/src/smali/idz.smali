.class public final Lidz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dzk:[Lidz;


# instance fields
.field private aez:I

.field private dzl:F

.field private dzm:Ljava/lang/String;

.field private dzn:J

.field private dzo:J

.field private dzp:J

.field private dzq:J

.field private dzr:Liea;

.field private dzs:Liea;

.field private dzt:[Ljava/lang/String;

.field private dzu:Ljava/lang/String;

.field public dzv:Liee;

.field public dzw:Lieh;

.field private dzx:Lied;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1583
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1584
    const/4 v0, 0x0

    iput v0, p0, Lidz;->aez:I

    const/4 v0, 0x0

    iput v0, p0, Lidz;->dzl:F

    const-string v0, ""

    iput-object v0, p0, Lidz;->dzm:Ljava/lang/String;

    iput-wide v2, p0, Lidz;->dzn:J

    iput-wide v2, p0, Lidz;->dzo:J

    iput-wide v2, p0, Lidz;->dzp:J

    iput-wide v2, p0, Lidz;->dzq:J

    iput-object v1, p0, Lidz;->dzr:Liea;

    iput-object v1, p0, Lidz;->dzs:Liea;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lidz;->dzt:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lidz;->dzu:Ljava/lang/String;

    iput-object v1, p0, Lidz;->dzv:Liee;

    iput-object v1, p0, Lidz;->dzw:Lieh;

    iput-object v1, p0, Lidz;->dzx:Lied;

    iput-object v1, p0, Lidz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lidz;->eCz:I

    .line 1585
    return-void
.end method

.method public static aVe()[Lidz;
    .locals 2

    .prologue
    .line 1413
    sget-object v0, Lidz;->dzk:[Lidz;

    if-nez v0, :cond_1

    .line 1414
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1416
    :try_start_0
    sget-object v0, Lidz;->dzk:[Lidz;

    if-nez v0, :cond_0

    .line 1417
    const/4 v0, 0x0

    new-array v0, v0, [Lidz;

    sput-object v0, Lidz;->dzk:[Lidz;

    .line 1419
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1421
    :cond_1
    sget-object v0, Lidz;->dzk:[Lidz;

    return-object v0

    .line 1419
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1270
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lidz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lidz;->dzl:F

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lidz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidz;->dzm:Ljava/lang/String;

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lidz;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lidz;->dzv:Liee;

    if-nez v0, :cond_1

    new-instance v0, Liee;

    invoke-direct {v0}, Liee;-><init>()V

    iput-object v0, p0, Lidz;->dzv:Liee;

    :cond_1
    iget-object v0, p0, Lidz;->dzv:Liee;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lidz;->dzw:Lieh;

    if-nez v0, :cond_2

    new-instance v0, Lieh;

    invoke-direct {v0}, Lieh;-><init>()V

    iput-object v0, p0, Lidz;->dzw:Lieh;

    :cond_2
    iget-object v0, p0, Lidz;->dzw:Lieh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lidz;->dzn:J

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lidz;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lidz;->dzp:J

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lidz;->aez:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lidz;->dzr:Liea;

    if-nez v0, :cond_3

    new-instance v0, Liea;

    invoke-direct {v0}, Liea;-><init>()V

    iput-object v0, p0, Lidz;->dzr:Liea;

    :cond_3
    iget-object v0, p0, Lidz;->dzr:Liea;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lidz;->dzs:Liea;

    if-nez v0, :cond_4

    new-instance v0, Liea;

    invoke-direct {v0}, Liea;-><init>()V

    iput-object v0, p0, Lidz;->dzs:Liea;

    :cond_4
    iget-object v0, p0, Lidz;->dzs:Liea;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lidz;->dzx:Lied;

    if-nez v0, :cond_5

    new-instance v0, Lied;

    invoke-direct {v0}, Lied;-><init>()V

    iput-object v0, p0, Lidz;->dzx:Lied;

    :cond_5
    iget-object v0, p0, Lidz;->dzx:Lied;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lidz;->dzt:[Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lidz;->dzt:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lidz;->dzt:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lidz;->dzt:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lidz;->dzu:Ljava/lang/String;

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lidz;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lidz;->dzq:J

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lidz;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lidz;->dzo:J

    iget v0, p0, Lidz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lidz;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1610
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1611
    const/4 v0, 0x1

    iget v1, p0, Lidz;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 1613
    :cond_0
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1614
    const/4 v0, 0x2

    iget-object v1, p0, Lidz;->dzm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1616
    :cond_1
    iget-object v0, p0, Lidz;->dzv:Liee;

    if-eqz v0, :cond_2

    .line 1617
    const/4 v0, 0x3

    iget-object v1, p0, Lidz;->dzv:Liee;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1619
    :cond_2
    iget-object v0, p0, Lidz;->dzw:Lieh;

    if-eqz v0, :cond_3

    .line 1620
    const/4 v0, 0x4

    iget-object v1, p0, Lidz;->dzw:Lieh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1622
    :cond_3
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 1623
    const/4 v0, 0x5

    iget-wide v2, p0, Lidz;->dzn:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1625
    :cond_4
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 1626
    const/4 v0, 0x6

    iget-wide v2, p0, Lidz;->dzp:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->g(IJ)V

    .line 1628
    :cond_5
    iget-object v0, p0, Lidz;->dzr:Liea;

    if-eqz v0, :cond_6

    .line 1629
    const/4 v0, 0x7

    iget-object v1, p0, Lidz;->dzr:Liea;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1631
    :cond_6
    iget-object v0, p0, Lidz;->dzs:Liea;

    if-eqz v0, :cond_7

    .line 1632
    const/16 v0, 0x8

    iget-object v1, p0, Lidz;->dzs:Liea;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1634
    :cond_7
    iget-object v0, p0, Lidz;->dzx:Lied;

    if-eqz v0, :cond_8

    .line 1635
    const/16 v0, 0x9

    iget-object v1, p0, Lidz;->dzx:Lied;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1637
    :cond_8
    iget-object v0, p0, Lidz;->dzt:[Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lidz;->dzt:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 1638
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lidz;->dzt:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 1639
    iget-object v1, p0, Lidz;->dzt:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1640
    if-eqz v1, :cond_9

    .line 1641
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1638
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1645
    :cond_a
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_b

    .line 1646
    const/16 v0, 0xb

    iget-object v1, p0, Lidz;->dzu:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1648
    :cond_b
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_c

    .line 1649
    const/16 v0, 0xc

    iget-wide v2, p0, Lidz;->dzq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->g(IJ)V

    .line 1651
    :cond_c
    iget v0, p0, Lidz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_d

    .line 1652
    const/16 v0, 0xd

    iget-wide v2, p0, Lidz;->dzo:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1654
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1655
    return-void
.end method

.method public final aVf()J
    .locals 2

    .prologue
    .line 1508
    iget-wide v0, p0, Lidz;->dzp:J

    return-wide v0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1659
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1660
    iget v2, p0, Lidz;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 1661
    const/4 v2, 0x1

    iget v3, p0, Lidz;->dzl:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 1664
    :cond_0
    iget v2, p0, Lidz;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 1665
    const/4 v2, 0x2

    iget-object v3, p0, Lidz;->dzm:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1668
    :cond_1
    iget-object v2, p0, Lidz;->dzv:Liee;

    if-eqz v2, :cond_2

    .line 1669
    const/4 v2, 0x3

    iget-object v3, p0, Lidz;->dzv:Liee;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1672
    :cond_2
    iget-object v2, p0, Lidz;->dzw:Lieh;

    if-eqz v2, :cond_3

    .line 1673
    const/4 v2, 0x4

    iget-object v3, p0, Lidz;->dzw:Lieh;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1676
    :cond_3
    iget v2, p0, Lidz;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_4

    .line 1677
    const/4 v2, 0x5

    iget-wide v4, p0, Lidz;->dzn:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1680
    :cond_4
    iget v2, p0, Lidz;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_5

    .line 1681
    const/4 v2, 0x6

    iget-wide v4, p0, Lidz;->dzp:J

    invoke-static {v2, v4, v5}, Ljsj;->j(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1684
    :cond_5
    iget-object v2, p0, Lidz;->dzr:Liea;

    if-eqz v2, :cond_6

    .line 1685
    const/4 v2, 0x7

    iget-object v3, p0, Lidz;->dzr:Liea;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1688
    :cond_6
    iget-object v2, p0, Lidz;->dzs:Liea;

    if-eqz v2, :cond_7

    .line 1689
    const/16 v2, 0x8

    iget-object v3, p0, Lidz;->dzs:Liea;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1692
    :cond_7
    iget-object v2, p0, Lidz;->dzx:Lied;

    if-eqz v2, :cond_8

    .line 1693
    const/16 v2, 0x9

    iget-object v3, p0, Lidz;->dzx:Lied;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1696
    :cond_8
    iget-object v2, p0, Lidz;->dzt:[Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lidz;->dzt:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_b

    move v2, v1

    move v3, v1

    .line 1699
    :goto_0
    iget-object v4, p0, Lidz;->dzt:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_a

    .line 1700
    iget-object v4, p0, Lidz;->dzt:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1701
    if-eqz v4, :cond_9

    .line 1702
    add-int/lit8 v3, v3, 0x1

    .line 1703
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1699
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1707
    :cond_a
    add-int/2addr v0, v2

    .line 1708
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1710
    :cond_b
    iget v1, p0, Lidz;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_c

    .line 1711
    const/16 v1, 0xb

    iget-object v2, p0, Lidz;->dzu:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1714
    :cond_c
    iget v1, p0, Lidz;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_d

    .line 1715
    const/16 v1, 0xc

    iget-wide v2, p0, Lidz;->dzq:J

    invoke-static {v1, v2, v3}, Ljsj;->j(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1718
    :cond_d
    iget v1, p0, Lidz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_e

    .line 1719
    const/16 v1, 0xd

    iget-wide v2, p0, Lidz;->dzo:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1722
    :cond_e
    return v0
.end method
