.class public final Ljhk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public elQ:Ljhj;

.field private elR:J

.field private elS:Z

.field private elT:Z

.field private elU:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 27711
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 27712
    iput v2, p0, Ljhk;->aez:I

    iput-object v3, p0, Ljhk;->elQ:Ljhj;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhk;->elR:J

    iput-boolean v2, p0, Ljhk;->elS:Z

    iput-boolean v2, p0, Ljhk;->elT:Z

    const-string v0, ""

    iput-object v0, p0, Ljhk;->elU:Ljava/lang/String;

    iput-object v3, p0, Ljhk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhk;->eCz:I

    .line 27713
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 27610
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljhk;->elQ:Ljhj;

    if-nez v0, :cond_1

    new-instance v0, Ljhj;

    invoke-direct {v0}, Ljhj;-><init>()V

    iput-object v0, p0, Ljhk;->elQ:Ljhj;

    :cond_1
    iget-object v0, p0, Ljhk;->elQ:Ljhj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljhk;->elR:J

    iget v0, p0, Ljhk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljhk;->elS:Z

    iget v0, p0, Ljhk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhk;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljhk;->elT:Z

    iget v0, p0, Ljhk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhk;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhk;->elU:Ljava/lang/String;

    iget v0, p0, Ljhk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 27730
    iget-object v0, p0, Ljhk;->elQ:Ljhj;

    if-eqz v0, :cond_0

    .line 27731
    const/4 v0, 0x1

    iget-object v1, p0, Ljhk;->elQ:Ljhj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 27733
    :cond_0
    iget v0, p0, Ljhk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 27734
    const/4 v0, 0x2

    iget-wide v2, p0, Ljhk;->elR:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 27736
    :cond_1
    iget v0, p0, Ljhk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 27737
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljhk;->elS:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 27739
    :cond_2
    iget v0, p0, Ljhk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 27740
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljhk;->elT:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 27742
    :cond_3
    iget v0, p0, Ljhk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 27743
    const/4 v0, 0x5

    iget-object v1, p0, Ljhk;->elU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 27745
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 27746
    return-void
.end method

.method public final blO()J
    .locals 2

    .prologue
    .line 27635
    iget-wide v0, p0, Ljhk;->elR:J

    return-wide v0
.end method

.method public final blP()Z
    .locals 1

    .prologue
    .line 27654
    iget-boolean v0, p0, Ljhk;->elS:Z

    return v0
.end method

.method public final blQ()Z
    .locals 1

    .prologue
    .line 27673
    iget-boolean v0, p0, Ljhk;->elT:Z

    return v0
.end method

.method public final blR()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27692
    iget-object v0, p0, Ljhk;->elU:Ljava/lang/String;

    return-object v0
.end method

.method public final dj(J)Ljhk;
    .locals 2

    .prologue
    .line 27638
    const-wide/32 v0, 0xf7e0

    iput-wide v0, p0, Ljhk;->elR:J

    .line 27639
    iget v0, p0, Ljhk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhk;->aez:I

    .line 27640
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 27750
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 27751
    iget-object v1, p0, Ljhk;->elQ:Ljhj;

    if-eqz v1, :cond_0

    .line 27752
    const/4 v1, 0x1

    iget-object v2, p0, Ljhk;->elQ:Ljhj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27755
    :cond_0
    iget v1, p0, Ljhk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 27756
    const/4 v1, 0x2

    iget-wide v2, p0, Ljhk;->elR:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27759
    :cond_1
    iget v1, p0, Ljhk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 27760
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljhk;->elS:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 27763
    :cond_2
    iget v1, p0, Ljhk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 27764
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljhk;->elT:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 27767
    :cond_3
    iget v1, p0, Ljhk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 27768
    const/4 v1, 0x5

    iget-object v2, p0, Ljhk;->elU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27771
    :cond_4
    return v0
.end method
