.class public final Ljpu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private exY:Z

.field private exZ:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 723
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 724
    iput v0, p0, Ljpu;->aez:I

    iput-boolean v0, p0, Ljpu;->exY:Z

    iput v0, p0, Ljpu;->exZ:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljpu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpu;->eCz:I

    .line 725
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljpu;->exY:Z

    iget v0, p0, Ljpu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljpu;->exZ:I

    iget v0, p0, Ljpu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 739
    iget v0, p0, Ljpu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 740
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljpu;->exY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 742
    :cond_0
    iget v0, p0, Ljpu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 743
    const/4 v0, 0x2

    iget v1, p0, Ljpu;->exZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 745
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 746
    return-void
.end method

.method public final brX()Z
    .locals 1

    .prologue
    .line 688
    iget-boolean v0, p0, Ljpu;->exY:Z

    return v0
.end method

.method public final iR(Z)Ljpu;
    .locals 1

    .prologue
    .line 691
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpu;->exY:Z

    .line 692
    iget v0, p0, Ljpu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpu;->aez:I

    .line 693
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 750
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 751
    iget v1, p0, Ljpu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 752
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljpu;->exY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 755
    :cond_0
    iget v1, p0, Ljpu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 756
    const/4 v1, 0x2

    iget v2, p0, Ljpu;->exZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 759
    :cond_1
    return v0
.end method
