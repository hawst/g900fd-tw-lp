.class public abstract Lfrk;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 28
    return-void
.end method

.method private a(Landroid/widget/EditText;Z)Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lfrm;

    invoke-direct {v0, p0, p1, p2}, Lfrm;-><init>(Lfrk;Landroid/widget/EditText;Z)V

    return-object v0
.end method

.method private a(Landroid/widget/EditText;Landroid/text/TextWatcher;Landroid/widget/EditText;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 88
    new-instance v0, Lfrl;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lfrl;-><init>(Lfrk;Ljava/lang/String;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/text/TextWatcher;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 102
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/String;Landroid/widget/EditText;)V
.end method

.method protected aCZ()V
    .locals 8

    .prologue
    const/16 v7, 0x2002

    const/4 v6, 0x1

    .line 37
    iget-object v2, p0, Lfro;->mView:Landroid/view/View;

    .line 38
    invoke-virtual {p0}, Lfrk;->aDa()Lank;

    move-result-object v3

    .line 40
    const v0, 0x7f1102f7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 41
    const v1, 0x7f1102f5

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 44
    const v4, 0x7f1102f6

    invoke-virtual {v3}, Lank;->pi()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 45
    const v4, 0x7f1102f4

    invoke-virtual {v3}, Lank;->pg()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 48
    invoke-virtual {v3}, Lank;->pk()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 49
    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 51
    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 56
    :cond_0
    invoke-virtual {v3}, Lank;->pj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {v3}, Lank;->ph()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 58
    invoke-virtual {v3}, Lank;->ph()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 68
    :goto_0
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lfrk;->a(Landroid/widget/EditText;Z)Landroid/text/TextWatcher;

    move-result-object v2

    invoke-virtual {v3}, Lank;->pj()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v2, v1, v4}, Lfrk;->a(Landroid/widget/EditText;Landroid/text/TextWatcher;Landroid/widget/EditText;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, v0, v6}, Lfrk;->a(Landroid/widget/EditText;Z)Landroid/text/TextWatcher;

    move-result-object v2

    invoke-virtual {v3}, Lank;->ph()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lfrk;->a(Landroid/widget/EditText;Landroid/text/TextWatcher;Landroid/widget/EditText;Ljava/lang/String;)V

    .line 74
    return-void

    .line 61
    :cond_1
    :try_start_0
    invoke-virtual {v3}, Lank;->pj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lfrk;->a(Ljava/lang/String;Landroid/widget/EditText;)V
    :try_end_0
    .catch Lfrn; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected abstract aDa()Lank;
.end method

.method protected b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 32
    const v0, 0x7f04010c

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(Ljava/lang/String;Landroid/widget/EditText;)V
.end method
