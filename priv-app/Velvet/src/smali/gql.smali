.class public final Lgql;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field private static cSZ:Lgql;


# instance fields
.field private aYZ:Lciu;

.field private bWQ:Lesm;

.field private final cSS:Ljava/lang/Object;

.field final cST:Ljava/lang/Object;

.field volatile cSU:Z

.field private cSV:Z

.field private cSW:Lfdt;

.field private cSX:Leak;

.field private cSY:Lgif;

.field private cqY:Lesm;

.field private mAccessibilityManager:Lelo;

.field public final mAppContext:Landroid/content/Context;

.field public final mAsyncServices:Lema;

.field public final mClock:Lemp;

.field private mCoreServices:Lcfo;

.field private mFactory:Lgpu;

.field private mGlobalSearchServices:Lcgh;

.field private mPersonShortcutManager:Lciy;

.field private mPrefController:Lchr;

.field private mRelationshipManager:Lcjg;

.field private mSidekickInjector:Lfdb;

.field private mVoiceSearchServices:Lhhq;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgql;->cSS:Ljava/lang/Object;

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgql;->cST:Ljava/lang/Object;

    .line 128
    iput-object p1, p0, Lgql;->mAppContext:Landroid/content/Context;

    .line 129
    invoke-static {}, Lema;->aur()Lema;

    move-result-object v0

    iput-object v0, p0, Lgql;->mAsyncServices:Lema;

    .line 130
    new-instance v0, Lere;

    invoke-direct {v0, p1}, Lere;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgql;->mClock:Lemp;

    .line 131
    return-void
.end method

.method private a(Lerd;)Lesm;
    .locals 3

    .prologue
    .line 285
    new-instance v0, Leme;

    iget-object v1, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Leme;-><init>(Ljava/util/concurrent/Executor;Lerd;)V

    .line 287
    new-instance v1, Leqf;

    iget-object v2, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Leqf;-><init>(Ljava/util/concurrent/Executor;Lesm;)V

    return-object v1
.end method

.method public static declared-synchronized aJN()Lgql;
    .locals 3

    .prologue
    .line 113
    const-class v1, Lgql;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lgql;->cSZ:Lgql;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lgql;

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJe()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    invoke-direct {v0, v2}, Lgql;-><init>(Landroid/content/Context;)V

    sput-object v0, Lgql;->cSZ:Lgql;

    .line 122
    :goto_0
    sget-object v0, Lgql;->cSZ:Lgql;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 119
    :cond_0
    :try_start_1
    sget-object v0, Lgql;->cSZ:Lgql;

    iget-object v0, v0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJe()Lcom/google/android/velvet/VelvetApplication;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized kw(I)V
    .locals 2

    .prologue
    .line 426
    const-class v1, Lgql;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lgql;->cSZ:Lgql;

    if-eqz v0, :cond_1

    .line 427
    invoke-static {p0}, Ldmn;->gc(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    sget-object v0, Lgql;->cSZ:Lgql;

    iget-object v0, v0, Lgql;->cSX:Leak;

    if-eqz v0, :cond_0

    .line 429
    sget-object v0, Lgql;->cSZ:Lgql;

    iget-object v0, v0, Lgql;->cSX:Leak;

    iget-object v0, v0, Leak;->bSz:Lean;

    invoke-virtual {v0}, Lean;->evictAll()V

    .line 431
    :cond_0
    sget-object v0, Lgql;->cSZ:Lgql;

    iget-object v0, v0, Lgql;->bWQ:Lesm;

    if-eqz v0, :cond_1

    .line 432
    sget-object v0, Lgql;->cSZ:Lgql;

    iget-object v0, v0, Lgql;->bWQ:Lesm;

    invoke-interface {v0}, Lesm;->clearCache()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    :cond_1
    monitor-exit v1

    return-void

    .line 426
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized El()Lfdr;
    .locals 1

    .prologue
    .line 415
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v0, v0, Lfdb;->mLocationOracle:Lfdr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized SC()Lcfo;
    .locals 2

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mCoreServices:Lcfo;

    if-nez v0, :cond_0

    .line 312
    new-instance v0, Lcfo;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Lcfo;-><init>(Landroid/content/Context;Lgql;Ljava/lang/Object;)V

    iput-object v0, p0, Lgql;->mCoreServices:Lcfo;

    .line 314
    :cond_0
    iget-object v0, p0, Lgql;->mCoreServices:Lcfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Letj;)V
    .locals 5

    .prologue
    .line 440
    const-string v0, "VelvetServices state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 447
    const-string v0, "PROD"

    .line 449
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJh()Ljava/lang/String;

    move-result-object v1

    .line 450
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " BUILD("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 452
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v1

    .line 453
    const-string v0, "Features"

    invoke-virtual {v1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 454
    invoke-static {}, Lcgg;->values()[Lcgg;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 455
    invoke-virtual {v4}, Lcgg;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Letj;->lv(Ljava/lang/String;)V

    .line 454
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 457
    :cond_0
    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 458
    invoke-virtual {p0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 459
    iget-object v0, p0, Lgql;->mGlobalSearchServices:Lcgh;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 460
    iget-object v0, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 461
    return-void
.end method

.method public final aJO()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 139
    iget-object v1, p0, Lgql;->cSS:Ljava/lang/Object;

    monitor-enter v1

    .line 142
    :try_start_0
    iget-boolean v2, p0, Lgql;->cSV:Z

    if-eqz v2, :cond_0

    .line 143
    monitor-exit v1

    .line 170
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DF()Lcin;

    move-result-object v2

    invoke-interface {v2}, Lcin;->Kz()Z

    move-result v2

    if-nez v2, :cond_1

    .line 148
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 155
    :cond_1
    :try_start_1
    iget-object v2, p0, Lgql;->cSW:Lfdt;

    if-nez v2, :cond_3

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 156
    invoke-virtual {p0}, Lgql;->El()Lfdr;

    move-result-object v0

    const-string v2, "velvetservices"

    invoke-interface {v0, v2}, Lfdr;->lK(Ljava/lang/String;)Lfdt;

    move-result-object v0

    iput-object v0, p0, Lgql;->cSW:Lfdt;

    .line 157
    iget-object v0, p0, Lgql;->cSW:Lfdt;

    invoke-interface {v0}, Lfdt;->acquire()V

    .line 159
    invoke-virtual {p0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lfdb;->axM()Leyj;

    move-result-object v2

    invoke-virtual {p0}, Lgql;->aJr()Lfdb;

    move-result-object v3

    invoke-interface {v2, v3}, Leyj;->a(Lfdb;)V

    .line 162
    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DD()Lcjs;

    move-result-object v2

    invoke-virtual {v2}, Lcjs;->Mr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 163
    iget-object v2, v0, Lfdb;->coW:Lgue;

    const-wide/32 v4, 0x2bf20

    invoke-virtual {v2, v4, v5}, Lgue;->bG(J)Lcgs;

    .line 167
    :cond_2
    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    invoke-virtual {v0}, Lfaq;->awS()V

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgql;->cSV:Z

    .line 170
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 155
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final aJP()V
    .locals 4

    .prologue
    .line 174
    iget-boolean v0, p0, Lgql;->cSU:Z

    if-eqz v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lgqm;

    const-string v2, "Register sidekick alarms"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lgqm;-><init>(Lgql;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final aJQ()V
    .locals 7

    .prologue
    .line 234
    iget-object v1, p0, Lgql;->cSS:Ljava/lang/Object;

    monitor-enter v1

    .line 235
    :try_start_0
    iget-boolean v0, p0, Lgql;->cSV:Z

    if-nez v0, :cond_0

    .line 236
    monitor-exit v1

    .line 256
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Lgql;->mAppContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.sidekick.TrafficIntentService.SHUTDOWN_ACTION"

    const/4 v4, 0x0

    iget-object v5, p0, Lgql;->mAppContext:Landroid/content/Context;

    const-class v6, Lcom/google/android/sidekick/main/TrafficIntentService;

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 246
    iget-object v0, p0, Lgql;->mAppContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    const/4 v4, 0x0

    iget-object v5, p0, Lgql;->mAppContext:Landroid/content/Context;

    const-class v6, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 250
    iget-object v0, p0, Lgql;->cSW:Lfdt;

    invoke-interface {v0}, Lfdt;->release()V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lgql;->cSW:Lfdt;

    .line 253
    iget-object v2, p0, Lgql;->cST:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v0, p0, Lgql;->cSU:Z

    if-nez v0, :cond_1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 255
    :goto_1
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lgql;->cSV:Z

    .line 256
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 253
    :cond_1
    :try_start_3
    iget-object v0, p0, Lgql;->mSidekickInjector:Lfdb;

    iget-object v0, v0, Lfdb;->coW:Lgue;

    invoke-virtual {v0}, Lgue;->aKw()Lcgs;

    iget-object v0, p0, Lgql;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axM()Leyj;

    move-result-object v0

    invoke-virtual {p0}, Lgql;->aJr()Lfdb;

    move-result-object v3

    invoke-interface {v0, v3}, Leyj;->b(Lfdb;)V

    iget-object v0, p0, Lgql;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axZ()Lfak;

    move-result-object v0

    invoke-virtual {v0}, Lfak;->awQ()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgql;->cSU:Z

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public final declared-synchronized aJR()Lcgh;
    .locals 4

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mGlobalSearchServices:Lcgh;

    if-nez v0, :cond_0

    .line 352
    new-instance v0, Lcgi;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v2

    iget-object v3, p0, Lgql;->mAsyncServices:Lema;

    invoke-direct {v0, v1, v2, v3, p0}, Lcgi;-><init>(Landroid/content/Context;Lcfo;Lema;Ljava/lang/Object;)V

    iput-object v0, p0, Lgql;->mGlobalSearchServices:Lcgh;

    .line 355
    :cond_0
    iget-object v0, p0, Lgql;->mGlobalSearchServices:Lcgh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJS()Lesm;
    .locals 5

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->cqY:Lesm;

    if-nez v0, :cond_0

    .line 370
    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DG()Ldkx;

    move-result-object v0

    new-instance v1, Lcva;

    iget-object v2, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcva;-><init>(Ldkx;Landroid/content/res/Resources;)V

    invoke-direct {p0, v1}, Lgql;->a(Lerd;)Lesm;

    move-result-object v0

    new-instance v1, Lcuz;

    iget-object v2, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcuz;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {p0, v1}, Lgql;->a(Lerd;)Lesm;

    move-result-object v1

    new-instance v2, Leao;

    iget-object v3, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Leao;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lgql;->a(Lerd;)Lesm;

    move-result-object v2

    new-instance v3, Leaq;

    iget-object v4, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Leaq;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1, v2, v3}, Lijj;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lemn;->b(Lijj;)Lemn;

    move-result-object v0

    iput-object v0, p0, Lgql;->cqY:Lesm;

    .line 372
    :cond_0
    iget-object v0, p0, Lgql;->cqY:Lesm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJT()Lesm;
    .locals 3

    .prologue
    .line 385
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->bWQ:Lesm;

    if-nez v0, :cond_0

    .line 386
    iget-object v0, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d009b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    new-instance v1, Leao;

    iget-object v2, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Leao;-><init>(Landroid/content/Context;)V

    new-instance v2, Leap;

    invoke-direct {v2, v0, v0, v1}, Leap;-><init>(IILerd;)V

    invoke-direct {p0, v2}, Lgql;->a(Lerd;)Lesm;

    move-result-object v0

    new-instance v1, Leak;

    invoke-direct {v1, v0}, Leak;-><init>(Lesm;)V

    new-instance v0, Leaq;

    iget-object v2, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Leaq;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v0}, Lijj;->r(Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lemn;->b(Lijj;)Lemn;

    move-result-object v0

    iput-object v0, p0, Lgql;->bWQ:Lesm;

    .line 388
    :cond_0
    iget-object v0, p0, Lgql;->bWQ:Lesm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJU()Lgpu;
    .locals 2

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mFactory:Lgpu;

    if-nez v0, :cond_0

    .line 420
    new-instance v0, Lgpu;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lgpu;-><init>(Lgql;Landroid/content/Context;)V

    iput-object v0, p0, Lgql;->mFactory:Lgpu;

    .line 422
    :cond_0
    iget-object v0, p0, Lgql;->mFactory:Lgpu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJV()Lcjg;
    .locals 8

    .prologue
    .line 468
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mRelationshipManager:Lcjg;

    if-nez v0, :cond_1

    .line 469
    const/4 v5, 0x0

    .line 470
    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->FY()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    new-instance v0, Lcjl;

    invoke-virtual {p0}, Lgql;->aJW()Leai;

    move-result-object v1

    iget-object v2, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DL()Lcrh;

    move-result-object v4

    new-instance v5, Lcit;

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v6

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DG()Ldkx;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcit;-><init>(Lchk;Ldkx;)V

    new-instance v6, Lcfm;

    iget-object v7, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct {v6, v7}, Lcfm;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct/range {v0 .. v6}, Lcjl;-><init>(Leai;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcrh;Lcis;Lcfm;)V

    move-object v5, v0

    .line 481
    :cond_0
    new-instance v0, Lcjg;

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-virtual {p0}, Lgql;->aJW()Leai;

    move-result-object v2

    iget-object v3, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    iget-object v4, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcjg;-><init>(Lcke;Leai;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcjl;)V

    iput-object v0, p0, Lgql;->mRelationshipManager:Lcjg;

    .line 488
    :cond_1
    iget-object v0, p0, Lgql;->mRelationshipManager:Lcjg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJW()Leai;
    .locals 1

    .prologue
    .line 492
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lgql;->aJY()Lgif;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJX()Lggq;
    .locals 3

    .prologue
    .line 496
    monitor-enter p0

    :try_start_0
    new-instance v0, Lggq;

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-interface {v2}, Lcke;->Oo()Ljta;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lggq;-><init>(Lcke;Ljta;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJY()Lgif;
    .locals 3

    .prologue
    .line 501
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->cSY:Lgif;

    if-nez v0, :cond_0

    .line 502
    new-instance v0, Lgic;

    invoke-virtual {p0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    iget-object v1, v1, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lgic;-><init>(Ljava/lang/String;)V

    .line 504
    new-instance v1, Lgif;

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lgif;-><init>(Lcke;Lgic;)V

    iput-object v1, p0, Lgql;->cSY:Lgif;

    .line 507
    :cond_0
    iget-object v0, p0, Lgql;->cSY:Lgif;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 501
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJZ()Lciu;
    .locals 3

    .prologue
    .line 511
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->aYZ:Lciu;

    if-nez v0, :cond_0

    .line 512
    new-instance v0, Lciu;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgql;->mAsyncServices:Lema;

    invoke-direct {v0, v1, v2}, Lciu;-><init>(Landroid/content/Context;Lema;)V

    iput-object v0, p0, Lgql;->aYZ:Lciu;

    .line 514
    :cond_0
    iget-object v0, p0, Lgql;->aYZ:Lciu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJq()Lhhq;
    .locals 6

    .prologue
    .line 359
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mVoiceSearchServices:Lhhq;

    if-nez v0, :cond_0

    .line 360
    new-instance v0, Lhhq;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {p0}, Lgql;->aJs()Lchr;

    move-result-object v3

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lhhq;-><init>(Landroid/content/Context;Lema;Lchr;Lcfo;Ljava/lang/Object;)V

    iput-object v0, p0, Lgql;->mVoiceSearchServices:Lhhq;

    .line 362
    iget-object v0, p0, Lgql;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTz()V

    .line 364
    :cond_0
    iget-object v0, p0, Lgql;->mVoiceSearchServices:Lhhq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJr()Lfdb;
    .locals 7

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mSidekickInjector:Lfdb;

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Lfdb;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lgql;->aJs()Lchr;

    move-result-object v2

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v3

    iget-object v4, p0, Lgql;->mAsyncServices:Lema;

    iget-object v5, p0, Lgql;->mAppContext:Landroid/content/Context;

    check-cast v5, Lcom/google/android/velvet/VelvetApplication;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lfdb;-><init>(Landroid/content/Context;Lchr;Lcfo;Lema;Lgpd;Ljava/lang/Object;)V

    iput-object v0, p0, Lgql;->mSidekickInjector:Lfdb;

    .line 326
    iget-object v0, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lgqn;

    const-string v2, "Start Now services"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lgqn;-><init>(Lgql;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lgql;->mSidekickInjector:Lfdb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aJs()Lchr;
    .locals 3

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mPrefController:Lchr;

    if-nez v0, :cond_0

    .line 296
    new-instance v0, Lchr;

    iget-object v1, p0, Lgql;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgql;->mAsyncServices:Lema;

    invoke-direct {v0, v1, v2}, Lchr;-><init>(Landroid/content/Context;Lema;)V

    iput-object v0, p0, Lgql;->mPrefController:Lchr;

    .line 307
    :cond_0
    iget-object v0, p0, Lgql;->mPrefController:Lchr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aKa()Lciy;
    .locals 4

    .prologue
    .line 518
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->mPersonShortcutManager:Lciy;

    if-nez v0, :cond_0

    .line 519
    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->FZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 520
    new-instance v0, Lciz;

    invoke-virtual {p0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    iget-object v2, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, p0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lciz;-><init>(Lcke;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lgql;->mPersonShortcutManager:Lciy;

    .line 528
    :cond_0
    :goto_0
    iget-object v0, p0, Lgql;->mPersonShortcutManager:Lciy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 525
    :cond_1
    :try_start_1
    new-instance v0, Lcjc;

    invoke-direct {v0}, Lcjc;-><init>()V

    iput-object v0, p0, Lgql;->mPersonShortcutManager:Lciy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 518
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aKb()Lelo;
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lgql;->mAccessibilityManager:Lelo;

    if-nez v0, :cond_0

    .line 541
    iget-object v0, p0, Lgql;->mAppContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    new-instance v1, Lelo;

    invoke-direct {v1, v0}, Lelo;-><init>(Landroid/view/accessibility/AccessibilityManager;)V

    iput-object v1, p0, Lgql;->mAccessibilityManager:Lelo;

    .line 544
    :cond_0
    iget-object v0, p0, Lgql;->mAccessibilityManager:Lelo;

    return-object v0
.end method

.method public final declared-synchronized anM()Lesm;
    .locals 2

    .prologue
    .line 377
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgql;->cSX:Leak;

    if-nez v0, :cond_0

    .line 378
    new-instance v0, Leak;

    invoke-virtual {p0}, Lgql;->aJS()Lesm;

    move-result-object v1

    invoke-direct {v0, v1}, Leak;-><init>(Lesm;)V

    iput-object v0, p0, Lgql;->cSX:Leak;

    .line 380
    :cond_0
    iget-object v0, p0, Lgql;->cSX:Leak;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 377
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
