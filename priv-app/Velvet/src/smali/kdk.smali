.class final Lkdk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdc;


# instance fields
.field private final eTA:Lkdh;

.field private final eTt:Ljava/io/DataInputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Z)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    .line 70
    new-instance v0, Lkdh;

    iget-object v1, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-direct {v0, v1, p2}, Lkdh;-><init>(Ljava/io/DataInputStream;Z)V

    iput-object v0, p0, Lkdk;->eTA:Lkdh;

    .line 72
    return-void
.end method

.method private a(Lkdd;II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 201
    rem-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_0

    const-string v0, "TYPE_SETTINGS length %% 8 != 0: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 202
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 203
    :cond_1
    new-instance v2, Lkdt;

    invoke-direct {v2}, Lkdt;-><init>()V

    move v0, v1

    .line 204
    :goto_0
    if-ge v0, p2, :cond_2

    .line 205
    iget-object v3, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 206
    iget-object v4, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 208
    const v5, 0xffffff

    and-int/2addr v3, v5

    .line 209
    invoke-virtual {v2, v3, v1, v4}, Lkdt;->set(III)V

    .line 204
    add-int/lit8 v0, v0, 0x8

    goto :goto_0

    .line 211
    :cond_2
    invoke-interface {p1, v1, v2}, Lkdd;->a(ZLkdt;)V

    .line 212
    return-void
.end method

.method private static varargs f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 253
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lkdd;)Z
    .locals 14

    .prologue
    const/4 v5, 0x4

    const/high16 v13, -0x10000

    const v12, 0x7fffffff

    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 87
    :try_start_0
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 91
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 93
    and-int v0, v2, v13

    shr-int/lit8 v0, v0, 0x10

    .line 94
    const v4, 0xff00

    and-int/2addr v4, v2

    shr-int/lit8 v4, v4, 0x8

    .line 95
    and-int/lit16 v2, v2, 0xff

    .line 97
    and-int/2addr v3, v12

    .line 99
    packed-switch v4, :pswitch_data_0

    .line 137
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TODO"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :catch_0
    move-exception v0

    move v7, v1

    .line 134
    :goto_0
    :pswitch_1
    return v7

    .line 101
    :pswitch_2
    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    move v1, v7

    :cond_0
    iget-object v2, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-interface {p1, v1, v3, v2, v0}, Lkdd;->a(ZILjava/io/InputStream;I)V

    goto :goto_0

    .line 105
    :pswitch_3
    if-nez v3, :cond_1

    const-string v0, "TYPE_HEADERS streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v4, p0, Lkdk;->eTA:Lkdh;

    iget-wide v8, v4, Lkdh;->eTy:J

    int-to-long v10, v0

    add-long/2addr v8, v10

    iput-wide v8, v4, Lkdh;->eTy:J

    :goto_1
    iget-wide v8, v4, Lkdh;->eTy:J

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-lez v0, :cond_a

    invoke-virtual {v4}, Lkdh;->bzl()I

    move-result v0

    and-int/lit16 v5, v0, 0x80

    if-eqz v5, :cond_3

    const/16 v5, 0x7f

    invoke-virtual {v4, v0, v5}, Lkdh;->bC(II)I

    move-result v0

    iget-object v5, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v5, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v5, v0}, Ljava/util/BitSet;->clear(I)V

    goto :goto_1

    :cond_2
    iget-object v5, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v5, v0}, Ljava/util/BitSet;->set(I)V

    iget-object v5, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-virtual {v4, v0}, Lkdh;->getName(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-virtual {v4, v0}, Lkdh;->getValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/16 v5, 0x60

    if-ne v0, v5, :cond_4

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    and-int/lit16 v5, v0, 0xe0

    const/16 v6, 0x60

    if-ne v5, v6, :cond_5

    const/16 v5, 0x1f

    invoke-virtual {v4, v0, v5}, Lkdh;->bC(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Lkdh;->getName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    const/16 v5, 0x40

    if-ne v0, v5, :cond_6

    iget-object v0, v4, Lkdh;->eTv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lkdh;->bm(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v5, v0}, Ljava/util/BitSet;->set(I)V

    goto/16 :goto_1

    :cond_6
    and-int/lit16 v5, v0, 0xe0

    const/16 v6, 0x40

    if-ne v5, v6, :cond_7

    const/16 v5, 0x1f

    invoke-virtual {v4, v0, v5}, Lkdh;->bC(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v5, v4, Lkdh;->eTv:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v0}, Lkdh;->getName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, Lkdh;->bm(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    goto/16 :goto_1

    :cond_7
    if-nez v0, :cond_8

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lkdh;->bzl()I

    move-result v5

    const/16 v6, 0xff

    invoke-virtual {v4, v5, v6}, Lkdh;->bC(II)I

    move-result v5

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v0, v6}, Lkdh;->e(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    goto/16 :goto_1

    :cond_8
    and-int/lit16 v5, v0, 0xc0

    if-nez v5, :cond_9

    const/16 v5, 0x3f

    invoke-virtual {v4, v0, v5}, Lkdh;->bC(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4}, Lkdh;->bzl()I

    move-result v5

    const/16 v6, 0xff

    invoke-virtual {v4, v5, v6}, Lkdh;->bC(II)I

    move-result v5

    invoke-virtual {v4, v0}, Lkdh;->getName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lkdh;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v0, v6}, Lkdh;->e(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v8, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v4, Lkdh;->eTu:Ljava/util/BitSet;

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    goto/16 :goto_1

    :cond_9
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    and-int/lit8 v0, v2, 0x4

    if-eqz v0, :cond_c

    iget-object v0, p0, Lkdk;->eTA:Lkdh;

    invoke-virtual {v0}, Lkdh;->bzk()V

    iget-object v0, p0, Lkdk;->eTA:Lkdh;

    new-instance v5, Ljava/util/ArrayList;

    iget-object v4, v0, Lkdh;->eTw:Ljava/util/List;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, v0, Lkdh;->eTw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    and-int/lit8 v0, v2, 0x1

    if-eqz v0, :cond_b

    move v2, v7

    :goto_2
    const/4 v4, -0x1

    sget-object v6, Lkdf;->eTp:Lkdf;

    move-object v0, p1

    invoke-interface/range {v0 .. v6}, Lkdd;->a(ZZIILjava/util/List;Lkdf;)V

    goto/16 :goto_0

    :cond_b
    move v2, v1

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    and-int v0, v2, v13

    shr-int/lit8 v0, v0, 0x10

    const v5, 0xff00

    and-int/2addr v5, v2

    shr-int/lit8 v5, v5, 0x8

    and-int/lit16 v2, v2, 0xff

    and-int/2addr v4, v12

    if-eq v5, v7, :cond_d

    const-string v0, "TYPE_HEADERS didn\'t have FLAG_END_HEADERS"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_d
    if-eq v4, v3, :cond_1

    const-string v0, "TYPE_HEADERS streamId changed"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 109
    :pswitch_4
    if-eq v0, v5, :cond_e

    const-string v2, "TYPE_PRIORITY length: %d != 4"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_e
    if-nez v3, :cond_f

    const-string v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_f
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    goto/16 :goto_0

    .line 113
    :pswitch_5
    if-eq v0, v5, :cond_10

    const-string v2, "TYPE_RST_STREAM length: %d != 4"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_10
    if-nez v3, :cond_11

    const-string v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_11
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    invoke-static {v0}, Lkdb;->tN(I)Lkdb;

    move-result-object v2

    if-nez v2, :cond_12

    const-string v2, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_12
    invoke-interface {p1, v3, v2}, Lkdd;->a(ILkdb;)V

    goto/16 :goto_0

    .line 117
    :pswitch_6
    invoke-direct {p0, p1, v0, v3}, Lkdk;->a(Lkdd;II)V

    goto/16 :goto_0

    .line 125
    :pswitch_7
    const/16 v4, 0x8

    if-eq v0, v4, :cond_13

    const-string v2, "TYPE_PING length != 8: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_13
    if-eqz v3, :cond_14

    const-string v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_14
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iget-object v3, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_15

    move v1, v7

    :cond_15
    invoke-interface {p1, v1, v0, v3}, Lkdd;->c(ZII)V

    goto/16 :goto_0

    .line 129
    :pswitch_8
    const/16 v2, 0x8

    if-ge v0, v2, :cond_16

    const-string v2, "TYPE_GOAWAY length < 8: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_16
    iget-object v2, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iget-object v3, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    add-int/lit8 v0, v0, -0x8

    invoke-static {v3}, Lkdb;->tN(I)Lkdb;

    move-result-object v4

    if-nez v4, :cond_17

    const-string v0, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdk;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_17
    iget-object v1, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    int-to-long v4, v0

    invoke-static {v1, v4, v5}, Lkbt;->b(Ljava/io/InputStream;J)J

    move-result-wide v4

    int-to-long v0, v0

    cmp-long v0, v4, v0

    if-eqz v0, :cond_18

    new-instance v0, Ljava/io/IOException;

    const-string v1, "TYPE_GOAWAY opaque data was truncated"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    invoke-interface {p1, v2}, Lkdd;->tP(I)V

    goto/16 :goto_0

    .line 133
    :pswitch_9
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    and-int/2addr v0, v12

    invoke-interface {p1, v3, v0}, Lkdd;->bA(II)V

    goto/16 :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lkdk;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 258
    return-void
.end method
