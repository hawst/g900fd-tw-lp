.class public final Ljio;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dVT:Ljava/lang/String;

.field private dWL:Ljak;

.field private enA:Z

.field public enz:Ljhm;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v0, p0, Ljio;->aez:I

    iput-object v1, p0, Ljio;->dWL:Ljak;

    iput-object v1, p0, Ljio;->enz:Ljhm;

    iput-boolean v0, p0, Ljio;->enA:Z

    const-string v0, ""

    iput-object v0, p0, Ljio;->dVT:Ljava/lang/String;

    iput-object v1, p0, Ljio;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljio;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljio;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljio;->dWL:Ljak;

    if-nez v0, :cond_1

    new-instance v0, Ljak;

    invoke-direct {v0}, Ljak;-><init>()V

    iput-object v0, p0, Ljio;->dWL:Ljak;

    :cond_1
    iget-object v0, p0, Ljio;->dWL:Ljak;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljio;->enz:Ljhm;

    if-nez v0, :cond_2

    new-instance v0, Ljhm;

    invoke-direct {v0}, Ljhm;-><init>()V

    iput-object v0, p0, Ljio;->enz:Ljhm;

    :cond_2
    iget-object v0, p0, Ljio;->enz:Ljhm;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljio;->enA:Z

    iget v0, p0, Ljio;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljio;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljio;->dVT:Ljava/lang/String;

    iget v0, p0, Ljio;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljio;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget-object v0, p0, Ljio;->dWL:Ljak;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljio;->dWL:Ljak;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_0
    iget-object v0, p0, Ljio;->enz:Ljhm;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljio;->enz:Ljhm;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    iget v0, p0, Ljio;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Ljio;->enA:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_2
    iget v0, p0, Ljio;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljio;->dVT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bnj()Z
    .locals 1

    iget-boolean v0, p0, Ljio;->enA:Z

    return v0
.end method

.method public final bnk()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljio;->dVT:Ljava/lang/String;

    return-object v0
.end method

.method public final bnl()Z
    .locals 1

    iget v0, p0, Ljio;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final im(Z)Ljio;
    .locals 1

    iput-boolean p1, p0, Ljio;->enA:Z

    iget v0, p0, Ljio;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljio;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget-object v1, p0, Ljio;->dWL:Ljak;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljio;->dWL:Ljak;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljio;->enz:Ljhm;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljio;->enz:Ljhm;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljio;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Ljio;->enA:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljio;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljio;->dVT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    return v0
.end method
