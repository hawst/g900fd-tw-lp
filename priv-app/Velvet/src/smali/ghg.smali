.class public final Lghg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final cJY:Lchk;

.field private final cJZ:Ljava/util/List;

.field private final mDiscourseContextSupplier:Ligi;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Ligi;Lchk;Lcke;)V
    .locals 1
    .param p1    # Ligi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lghg;->mDiscourseContextSupplier:Ligi;

    .line 43
    iput-object p2, p0, Lghg;->cJY:Lchk;

    .line 44
    iput-object p3, p0, Lghg;->mSearchSettings:Lcke;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    .line 46
    return-void
.end method

.method public static a(Ljava/util/List;Ldzb;)Ljava/util/Set;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 148
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 149
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 158
    :goto_0
    return-object v0

    .line 152
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 153
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 154
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->rn()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 155
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 158
    goto :goto_0
.end method

.method public static ai(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 110
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 111
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    .line 114
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 115
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 116
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 117
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 122
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 124
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 125
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 126
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 130
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 131
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v4, :cond_5

    aget-object v5, v3, v0

    .line 132
    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 133
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move-object v0, v1

    .line 137
    goto :goto_0
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 5

    .prologue
    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 51
    iget-object v0, p0, Lghg;->cJY:Lchk;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lghg;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->CS()Z

    move-result v0

    .line 54
    iget-object v2, p0, Lghg;->cJY:Lchk;

    invoke-virtual {v2}, Lchk;->FI()I

    move-result v2

    .line 56
    iget-object v3, p0, Lghg;->cJY:Lchk;

    invoke-virtual {v3}, Lchk;->FT()Z

    move-result v3

    if-eqz v3, :cond_0

    if-lez v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 72
    :goto_0
    return-object v0

    .line 63
    :cond_1
    iget-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lghg;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v2

    if-nez v2, :cond_5

    :cond_2
    iget-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    .line 64
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    .line 65
    iget-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    move-object v0, v1

    .line 72
    goto :goto_0

    .line 63
    :cond_4
    invoke-virtual {v0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agy()Ldxl;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    goto :goto_2

    :cond_6
    invoke-virtual {v0}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v2

    if-nez v2, :cond_8

    :cond_7
    iget-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    goto :goto_2

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lghg;->cJZ:Ljava/util/List;

    invoke-static {v2}, Lghg;->ai(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v3, p0, Lghg;->cJZ:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->akB()Ldzb;

    move-result-object v0

    invoke-static {v2, v0}, Lghg;->a(Ljava/util/List;Ldzb;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lghg;->cJZ:Ljava/util/List;

    goto :goto_2
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lghg;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
