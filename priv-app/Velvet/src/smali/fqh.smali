.class public final Lfqh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final GS:Landroid/view/LayoutInflater;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lfqh;->mContext:Landroid/content/Context;

    .line 32
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfqh;->GS:Landroid/view/LayoutInflater;

    .line 33
    return-void
.end method

.method public static jj(I)Z
    .locals 1

    .prologue
    .line 67
    packed-switch p0, :pswitch_data_0

    .line 77
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 75
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 67
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Landroid/view/ViewGroup;Lizj;Lfqc;)Landroid/view/View;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 42
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v5

    .line 43
    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 60
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Don\'t know how to create view for question type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :pswitch_1
    iget-object v0, p0, Lfqh;->GS:Landroid/view/LayoutInflater;

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v1

    invoke-static {v1, v3}, Lfqk;->F(IZ)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfqb;

    invoke-interface {v0, p3}, Lfqb;->z(Lizj;)V

    invoke-interface {v0, v5}, Lfqb;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V

    invoke-interface {v0, p4}, Lfqb;->a(Lfqc;)V

    check-cast v0, Landroid/view/View;

    .line 55
    :goto_0
    return-object v0

    .line 51
    :pswitch_2
    iget-object v0, p0, Lfqh;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f04019b

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    invoke-static {v4, v5, p3}, Lfqk;->a(Landroid/view/View;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;)V

    const v0, 0x7f11046a

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCs()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfoz;

    iget-object v2, p0, Lfqh;->GS:Landroid/view/LayoutInflater;

    const v7, 0x7f04019c

    invoke-virtual {v2, v7, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iget-object v7, v1, Lfoz;->ckR:Liwk;

    invoke-virtual {v7}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget v7, v1, Lfoz;->cyi:I

    invoke-static {v7}, Lfqk;->jk(I)I

    move-result v7

    if-eqz v7, :cond_1

    iget-object v8, p0, Lfqh;->mContext:Landroid/content/Context;

    invoke-static {v8, v7}, Lfqk;->i(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-static {v2, v9, v7, v9, v9}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v7, Lfqi;

    invoke-direct {v7, p0, p4, v5, v1}, Lfqi;-><init>(Lfqh;Lfqc;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfoz;)V

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    move-object v0, v4

    goto :goto_0

    .line 55
    :pswitch_3
    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCw()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    const-string v1, "Question does not have fulfill action"

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    iget-object v0, p0, Lfqh;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f04019d

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f1100f5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v2, p0, Lfqh;->mContext:Landroid/content/Context;

    const v3, 0x7f0201bc

    invoke-static {v2, v3}, Lfqk;->i(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v1, v9, v2, v9, v9}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :goto_3
    new-instance v2, Lfqj;

    invoke-direct {v2, p0, v5, p4}, Lfqj;-><init>(Lfqh;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfqc;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto :goto_2

    :pswitch_4
    iget-object v2, p0, Lfqh;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0455

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :pswitch_5
    iget-object v2, p0, Lfqh;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0456

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 43
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 55
    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
