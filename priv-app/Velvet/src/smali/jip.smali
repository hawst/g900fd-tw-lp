.class public final Ljip;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aiX:Ljcn;

.field private ajj:Ljava/lang/String;

.field private dMi:J

.field private dMk:Ljava/lang/String;

.field private enB:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56931
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 56932
    const/4 v0, 0x0

    iput v0, p0, Ljip;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljip;->afh:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljip;->enB:[Ljava/lang/String;

    iput-object v2, p0, Ljip;->aiX:Ljcn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljip;->dMi:J

    const-string v0, ""

    iput-object v0, p0, Ljip;->ajj:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljip;->dMk:Ljava/lang/String;

    iput-object v2, p0, Ljip;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljip;->eCz:I

    .line 56933
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 56821
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljip;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljip;->afh:Ljava/lang/String;

    iget v0, p0, Ljip;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljip;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljip;->enB:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljip;->enB:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljip;->enB:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljip;->enB:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljip;->aiX:Ljcn;

    if-nez v0, :cond_4

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljip;->aiX:Ljcn;

    :cond_4
    iget-object v0, p0, Ljip;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljip;->dMi:J

    iget v0, p0, Ljip;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljip;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljip;->dMk:Ljava/lang/String;

    iget v0, p0, Ljip;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljip;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljip;->ajj:Ljava/lang/String;

    iget v0, p0, Ljip;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljip;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 56951
    iget v0, p0, Ljip;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 56952
    const/4 v0, 0x1

    iget-object v1, p0, Ljip;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56954
    :cond_0
    iget-object v0, p0, Ljip;->enB:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljip;->enB:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 56955
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljip;->enB:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 56956
    iget-object v1, p0, Ljip;->enB:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 56957
    if-eqz v1, :cond_1

    .line 56958
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56955
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56962
    :cond_2
    iget-object v0, p0, Ljip;->aiX:Ljcn;

    if-eqz v0, :cond_3

    .line 56963
    const/4 v0, 0x3

    iget-object v1, p0, Ljip;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 56965
    :cond_3
    iget v0, p0, Ljip;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 56966
    const/4 v0, 0x4

    iget-wide v2, p0, Ljip;->dMi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 56968
    :cond_4
    iget v0, p0, Ljip;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 56969
    const/4 v0, 0x5

    iget-object v1, p0, Ljip;->dMk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56971
    :cond_5
    iget v0, p0, Ljip;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 56972
    const/4 v0, 0x6

    iget-object v1, p0, Ljip;->ajj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56974
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 56975
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 56979
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 56980
    iget v2, p0, Ljip;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 56981
    const/4 v2, 0x1

    iget-object v3, p0, Ljip;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 56984
    :cond_0
    iget-object v2, p0, Ljip;->enB:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljip;->enB:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 56987
    :goto_0
    iget-object v4, p0, Ljip;->enB:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 56988
    iget-object v4, p0, Ljip;->enB:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 56989
    if-eqz v4, :cond_1

    .line 56990
    add-int/lit8 v3, v3, 0x1

    .line 56991
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 56987
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56995
    :cond_2
    add-int/2addr v0, v2

    .line 56996
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 56998
    :cond_3
    iget-object v1, p0, Ljip;->aiX:Ljcn;

    if-eqz v1, :cond_4

    .line 56999
    const/4 v1, 0x3

    iget-object v2, p0, Ljip;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57002
    :cond_4
    iget v1, p0, Ljip;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 57003
    const/4 v1, 0x4

    iget-wide v2, p0, Ljip;->dMi:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57006
    :cond_5
    iget v1, p0, Ljip;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 57007
    const/4 v1, 0x5

    iget-object v2, p0, Ljip;->dMk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57010
    :cond_6
    iget v1, p0, Ljip;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 57011
    const/4 v1, 0x6

    iget-object v2, p0, Ljip;->ajj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57014
    :cond_7
    return v0
.end method
