.class public final Ljwl;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJf:Ljsm;


# instance fields
.field private aez:I

.field private dIG:I

.field private eJg:I

.field private eJh:I

.field private eJi:I

.field private eJj:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljwl;

    const v2, 0xd04a4b2

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwl;->eJf:Ljsm;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 146
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 147
    iput v0, p0, Ljwl;->aez:I

    iput v0, p0, Ljwl;->eJg:I

    iput v0, p0, Ljwl;->eJh:I

    iput v0, p0, Ljwl;->eJi:I

    iput v0, p0, Ljwl;->eJj:I

    iput v0, p0, Ljwl;->dIG:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljwl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwl;->eCz:I

    .line 148
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwl;->eJg:I

    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwl;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwl;->eJh:I

    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwl;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwl;->eJi:I

    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwl;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwl;->eJj:I

    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwl;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwl;->dIG:I

    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwl;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 165
    iget v0, p0, Ljwl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x1

    iget v1, p0, Ljwl;->eJg:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 168
    :cond_0
    iget v0, p0, Ljwl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 169
    const/4 v0, 0x2

    iget v1, p0, Ljwl;->eJh:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 171
    :cond_1
    iget v0, p0, Ljwl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 172
    const/4 v0, 0x3

    iget v1, p0, Ljwl;->eJi:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 174
    :cond_2
    iget v0, p0, Ljwl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 175
    const/4 v0, 0x4

    iget v1, p0, Ljwl;->eJj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 177
    :cond_3
    iget v0, p0, Ljwl;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 178
    const/4 v0, 0x5

    iget v1, p0, Ljwl;->dIG:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 180
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 181
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 185
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 186
    iget v1, p0, Ljwl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 187
    const/4 v1, 0x1

    iget v2, p0, Ljwl;->eJg:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_0
    iget v1, p0, Ljwl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 191
    const/4 v1, 0x2

    iget v2, p0, Ljwl;->eJh:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_1
    iget v1, p0, Ljwl;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 195
    const/4 v1, 0x3

    iget v2, p0, Ljwl;->eJi:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_2
    iget v1, p0, Ljwl;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 199
    const/4 v1, 0x4

    iget v2, p0, Ljwl;->eJj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_3
    iget v1, p0, Ljwl;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 203
    const/4 v1, 0x5

    iget v2, p0, Ljwl;->dIG:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    :cond_4
    return v0
.end method

.method public final sG(I)Ljwl;
    .locals 1

    .prologue
    .line 57
    iput p1, p0, Ljwl;->eJg:I

    .line 58
    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwl;->aez:I

    .line 59
    return-object p0
.end method

.method public final sH(I)Ljwl;
    .locals 1

    .prologue
    .line 76
    iput p1, p0, Ljwl;->eJh:I

    .line 77
    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwl;->aez:I

    .line 78
    return-object p0
.end method

.method public final sI(I)Ljwl;
    .locals 1

    .prologue
    .line 95
    iput p1, p0, Ljwl;->eJi:I

    .line 96
    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwl;->aez:I

    .line 97
    return-object p0
.end method

.method public final sJ(I)Ljwl;
    .locals 1

    .prologue
    .line 114
    iput p1, p0, Ljwl;->eJj:I

    .line 115
    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwl;->aez:I

    .line 116
    return-object p0
.end method

.method public final sK(I)Ljwl;
    .locals 1

    .prologue
    .line 133
    iput p1, p0, Ljwl;->dIG:I

    .line 134
    iget v0, p0, Ljwl;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwl;->aez:I

    .line 135
    return-object p0
.end method
