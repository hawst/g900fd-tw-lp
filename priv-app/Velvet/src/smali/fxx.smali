.class public final Lfxx;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 41
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfxx;->fA(Z)V

    .line 48
    iget-object v0, v1, Lanh;->agx:Laor;

    invoke-virtual {v0}, Laor;->rk()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 50
    const v2, 0x7f110320

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 51
    iget-object v2, v1, Lanh;->agx:Laor;

    invoke-virtual {v2}, Laor;->rj()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 52
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 57
    :cond_0
    :goto_0
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v2, 0x7f110098

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 60
    iget-object v2, v1, Lanh;->agx:Laor;

    invoke-virtual {v2}, Laor;->pa()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 61
    iget-object v2, v1, Lanh;->agx:Laor;

    invoke-virtual {v2}, Laor;->oZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/ui/WebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 66
    :goto_1
    new-instance v2, Lfox;

    invoke-direct {v2}, Lfox;-><init>()V

    .line 67
    iget-object v3, v1, Lanh;->agx:Laor;

    iget-object v3, v3, Laor;->ajY:Ljbp;

    iput-object v3, v2, Lfox;->mSource:Ljbp;

    iget-object v3, v1, Lanh;->agx:Laor;

    iget-object v3, v3, Laor;->ajZ:Ljal;

    iput-object v3, v2, Lfox;->mFrequentPlaceEntry:Ljal;

    iget-object v3, v1, Lanh;->agx:Laor;

    invoke-virtual {v3}, Laor;->rg()Z

    move-result v3

    iput-boolean v3, v2, Lfox;->cxZ:Z

    iget-object v3, v1, Lanh;->agx:Laor;

    invoke-virtual {v3}, Laor;->rh()Z

    move-result v3

    iput-boolean v3, v2, Lfox;->cya:Z

    iget-object v3, v1, Lanh;->agx:Laor;

    invoke-virtual {v3}, Laor;->ri()Z

    move-result v3

    iput-boolean v3, v2, Lfox;->cyb:Z

    iget-object v1, v1, Lanh;->agx:Laor;

    invoke-virtual {v1}, Laor;->rk()Z

    move-result v1

    iput-boolean v1, v2, Lfox;->cyc:Z

    .line 74
    iget-object v1, p0, Lfxx;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v1, v1, Lfml;->cwa:Lgbo;

    invoke-virtual {v2}, Lfox;->aCh()Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lgbo;->a(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;Landroid/widget/ImageView;)V

    .line 75
    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lfxx;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020296

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 82
    :cond_1
    return-void

    .line 53
    :cond_2
    iget-object v0, v1, Lanh;->agx:Laor;

    invoke-virtual {v0}, Laor;->rm()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 55
    const v2, 0x7f110336

    iget-object v3, v1, Lanh;->agx:Laor;

    invoke-virtual {v3}, Laor;->rl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lfxx;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    goto/16 :goto_0

    .line 63
    :cond_3
    iget-object v2, p0, Lfxx;->mContext:Landroid/content/Context;

    const v3, 0x7f0a015c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/ui/WebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 29
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 30
    const v0, 0x7f04011a

    .line 32
    iget-object v2, v1, Lanh;->agx:Laor;

    invoke-virtual {v2}, Laor;->rk()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, v1, Lanh;->agx:Laor;

    invoke-virtual {v1}, Laor;->rm()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    :cond_0
    const v0, 0x7f040128

    .line 36
    :cond_1
    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
