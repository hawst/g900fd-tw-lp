.class public final Ljvt;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eHZ:[Ljvt;


# instance fields
.field private aez:I

.field private aiC:Ljava/lang/String;

.field private eHS:Ljvy;

.field private eIa:D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1956
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1957
    const/4 v0, 0x0

    iput v0, p0, Ljvt;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljvt;->aiC:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljvt;->eIa:D

    iput-object v2, p0, Ljvt;->eHS:Ljvy;

    iput-object v2, p0, Ljvt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvt;->eCz:I

    .line 1958
    return-void
.end method

.method public static bve()[Ljvt;
    .locals 2

    .prologue
    .line 1899
    sget-object v0, Ljvt;->eHZ:[Ljvt;

    if-nez v0, :cond_1

    .line 1900
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1902
    :try_start_0
    sget-object v0, Ljvt;->eHZ:[Ljvt;

    if-nez v0, :cond_0

    .line 1903
    const/4 v0, 0x0

    new-array v0, v0, [Ljvt;

    sput-object v0, Ljvt;->eHZ:[Ljvt;

    .line 1905
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1907
    :cond_1
    sget-object v0, Ljvt;->eHZ:[Ljvt;

    return-object v0

    .line 1905
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1893
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvt;->aiC:Ljava/lang/String;

    iget v0, p0, Ljvt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljvt;->eIa:D

    iget v0, p0, Ljvt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvt;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljvt;->eHS:Ljvy;

    if-nez v0, :cond_1

    new-instance v0, Ljvy;

    invoke-direct {v0}, Ljvy;-><init>()V

    iput-object v0, p0, Ljvt;->eHS:Ljvy;

    :cond_1
    iget-object v0, p0, Ljvt;->eHS:Ljvy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1973
    iget v0, p0, Ljvt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1974
    const/4 v0, 0x1

    iget-object v1, p0, Ljvt;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1976
    :cond_0
    iget v0, p0, Ljvt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1977
    const/4 v0, 0x2

    iget-wide v2, p0, Ljvt;->eIa:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1979
    :cond_1
    iget-object v0, p0, Ljvt;->eHS:Ljvy;

    if-eqz v0, :cond_2

    .line 1980
    const/4 v0, 0x3

    iget-object v1, p0, Ljvt;->eHS:Ljvy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1982
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1983
    return-void
.end method

.method public final bvf()D
    .locals 2

    .prologue
    .line 1937
    iget-wide v0, p0, Ljvt;->eIa:D

    return-wide v0
.end method

.method public final bvg()Z
    .locals 1

    .prologue
    .line 1945
    iget v0, p0, Ljvt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1915
    iget-object v0, p0, Ljvt;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method public final hasText()Z
    .locals 1

    .prologue
    .line 1926
    iget v0, p0, Ljvt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1987
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1988
    iget v1, p0, Ljvt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1989
    const/4 v1, 0x1

    iget-object v2, p0, Ljvt;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1992
    :cond_0
    iget v1, p0, Ljvt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1993
    const/4 v1, 0x2

    iget-wide v2, p0, Ljvt;->eIa:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1996
    :cond_1
    iget-object v1, p0, Ljvt;->eHS:Ljvy;

    if-eqz v1, :cond_2

    .line 1997
    const/4 v1, 0x3

    iget-object v2, p0, Ljvt;->eHS:Ljvy;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2000
    :cond_2
    return v0
.end method

.method public final w(D)Ljvt;
    .locals 1

    .prologue
    .line 1940
    iput-wide p1, p0, Ljvt;->eIa:D

    .line 1941
    iget v0, p0, Ljvt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvt;->aez:I

    .line 1942
    return-object p0
.end method

.method public final zp(Ljava/lang/String;)Ljvt;
    .locals 1

    .prologue
    .line 1918
    if-nez p1, :cond_0

    .line 1919
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1921
    :cond_0
    iput-object p1, p0, Ljvt;->aiC:Ljava/lang/String;

    .line 1922
    iget v0, p0, Ljvt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvt;->aez:I

    .line 1923
    return-object p0
.end method
