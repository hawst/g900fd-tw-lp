.class public final Ldi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static em:Ldq;

.field private static final en:Ljava/lang/String;

.field private static final eo:Ljava/lang/String;

.field private static final ep:Ldi;

.field private static final eq:Ldi;


# instance fields
.field private final cZ:I

.field private final er:Z

.field private final es:Ldq;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 83
    sget-object v0, Ldr;->eE:Ldq;

    sput-object v0, Ldi;->em:Ldq;

    .line 113
    const/16 v0, 0x200e

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldi;->en:Ljava/lang/String;

    .line 118
    const/16 v0, 0x200f

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldi;->eo:Ljava/lang/String;

    .line 215
    new-instance v0, Ldi;

    const/4 v1, 0x0

    sget-object v2, Ldi;->em:Ldq;

    invoke-direct {v0, v1, v3, v2}, Ldi;-><init>(ZILdq;)V

    sput-object v0, Ldi;->ep:Ldi;

    .line 220
    new-instance v0, Ldi;

    const/4 v1, 0x1

    sget-object v2, Ldi;->em:Ldq;

    invoke-direct {v0, v1, v3, v2}, Ldi;-><init>(ZILdq;)V

    sput-object v0, Ldi;->eq:Ldi;

    return-void
.end method

.method private constructor <init>(ZILdq;)V
    .locals 0

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-boolean p1, p0, Ldi;->er:Z

    .line 262
    iput p2, p0, Ldi;->cZ:I

    .line 263
    iput-object p3, p0, Ldi;->es:Ldq;

    .line 264
    return-void
.end method

.method synthetic constructor <init>(ZILdq;B)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Ldi;-><init>(ZILdq;)V

    return-void
.end method

.method public static a(Ljava/util/Locale;)Ldi;
    .locals 1

    .prologue
    .line 252
    new-instance v0, Ldj;

    invoke-direct {v0, p0}, Ldj;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v0}, Ldj;->ao()Ldi;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ldq;Z)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 374
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {p2, p1, v2, v0}, Ldq;->isRtl(Ljava/lang/CharSequence;II)Z

    move-result v3

    .line 375
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 376
    iget v0, p0, Ldi;->cZ:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 377
    if-eqz v3, :cond_4

    sget-object v0, Ldr;->eD:Ldq;

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-interface {v0, p1, v2, v5}, Ldq;->isRtl(Ljava/lang/CharSequence;II)Z

    move-result v0

    iget-boolean v5, p0, Ldi;->er:Z

    if-nez v5, :cond_5

    if-nez v0, :cond_0

    invoke-static {p1}, Ldi;->d(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v1, :cond_5

    :cond_0
    sget-object v0, Ldi;->en:Ljava/lang/String;

    :goto_2
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    :cond_1
    iget-boolean v0, p0, Ldi;->er:Z

    if-eq v3, v0, :cond_9

    .line 381
    if-eqz v3, :cond_8

    const/16 v0, 0x202b

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 382
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    const/16 v0, 0x202c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 387
    :goto_4
    if-eqz v3, :cond_a

    sget-object v0, Ldr;->eD:Ldq;

    :goto_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v0, p1, v2, v3}, Ldq;->isRtl(Ljava/lang/CharSequence;II)Z

    move-result v0

    iget-boolean v2, p0, Ldi;->er:Z

    if-nez v2, :cond_b

    if-nez v0, :cond_2

    invoke-static {p1}, Ldi;->c(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v1, :cond_b

    :cond_2
    sget-object v0, Ldi;->en:Ljava/lang/String;

    :goto_6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v2

    .line 376
    goto :goto_0

    .line 377
    :cond_4
    sget-object v0, Ldr;->eC:Ldq;

    goto :goto_1

    :cond_5
    iget-boolean v5, p0, Ldi;->er:Z

    if-eqz v5, :cond_7

    if-eqz v0, :cond_6

    invoke-static {p1}, Ldi;->d(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_7

    :cond_6
    sget-object v0, Ldi;->eo:Ljava/lang/String;

    goto :goto_2

    :cond_7
    const-string v0, ""

    goto :goto_2

    .line 381
    :cond_8
    const/16 v0, 0x202a

    goto :goto_3

    .line 385
    :cond_9
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 387
    :cond_a
    sget-object v0, Ldr;->eC:Ldq;

    goto :goto_5

    :cond_b
    iget-boolean v1, p0, Ldi;->er:Z

    if-eqz v1, :cond_d

    if-eqz v0, :cond_c

    invoke-static {p1}, Ldi;->c(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_d

    :cond_c
    sget-object v0, Ldi;->eo:Ljava/lang/String;

    goto :goto_6

    :cond_d
    const-string v0, ""

    goto :goto_6
.end method

.method public static ak()Ldi;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Ldj;

    invoke-direct {v0}, Ldj;-><init>()V

    invoke-virtual {v0}, Ldj;->ao()Ldi;

    move-result-object v0

    return-object v0
.end method

.method static synthetic al()Ldq;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Ldi;->em:Ldq;

    return-object v0
.end method

.method static synthetic am()Ldi;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Ldi;->eq:Ldi;

    return-object v0
.end method

.method static synthetic an()Ldi;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Ldi;->ep:Ldi;

    return-object v0
.end method

.method static synthetic b(Ljava/util/Locale;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 78
    invoke-static {p0}, Ldy;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 462
    new-instance v0, Ldk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldk;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ldk;->aq()I

    move-result v0

    return v0
.end method

.method private static d(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 479
    new-instance v0, Ldk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldk;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ldk;->ap()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ldq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ldi;->a(Ljava/lang/String;Ldq;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unicodeWrap(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Ldi;->es:Ldq;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ldi;->a(Ljava/lang/String;Ldq;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
