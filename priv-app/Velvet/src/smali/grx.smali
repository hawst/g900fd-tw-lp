.class public final Lgrx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cUG:[Ljava/lang/Class;


# instance fields
.field private final aOx:Ligi;

.field private final cUD:J

.field private final cUE:Lgru;

.field private final cUF:Lhhb;

.field private final mContext:Landroid/content/Context;

.field private final mGsaConfigFlags:Lchk;

.field private final mNetworkInfo:Lgno;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 363
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/search/shared/actions/EmailAction;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/google/android/search/shared/actions/PhoneCallAction;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/google/android/search/shared/actions/SetReminderAction;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/google/android/search/shared/actions/SmsAction;

    aput-object v2, v0, v1

    sput-object v0, Lgrx;->cUG:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lchk;Ligi;JLhhb;Lgno;Lgru;)V
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lgrx;->mContext:Landroid/content/Context;

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgrx;->mResources:Landroid/content/res/Resources;

    .line 116
    iput-object p2, p0, Lgrx;->mGsaConfigFlags:Lchk;

    .line 117
    iput-object p3, p0, Lgrx;->aOx:Ligi;

    .line 118
    iput-wide p4, p0, Lgrx;->cUD:J

    .line 119
    iput-object p6, p0, Lgrx;->cUF:Lhhb;

    .line 120
    iput-object p7, p0, Lgrx;->mNetworkInfo:Lgno;

    .line 121
    iput-object p8, p0, Lgrx;->cUE:Lgru;

    .line 122
    return-void
.end method

.method static synthetic a(Lgrx;)Lhhb;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrx;->cUF:Lhhb;

    return-object v0
.end method

.method static synthetic b(Lgrx;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrx;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic c(Lgrx;)Lgru;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrx;->cUE:Lgru;

    return-object v0
.end method

.method static synthetic d(Lgrx;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrx;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lgrx;)Lgno;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrx;->mNetworkInfo:Lgno;

    return-object v0
.end method

.method static synthetic f(Lgrx;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lgrx;->cUD:J

    return-wide v0
.end method

.method static synthetic g(Lgrx;)Lchk;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lgrx;->mGsaConfigFlags:Lchk;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/service/ClientConfig;ZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 16
    .param p3    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/search/shared/service/ClientConfig;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 140
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->Pq()I

    move-result v5

    .line 141
    if-nez v5, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIA()I

    move-result v2

    if-nez v2, :cond_0

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lgrx;->aOx:Ligi;

    invoke-interface {v2}, Ligi;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcky;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcky;->h(Lcom/google/android/search/shared/actions/VoiceAction;)I

    move-result v5

    .line 156
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 158
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIM()J

    move-result-wide v10

    .line 165
    :goto_0
    new-instance v2, Lgrz;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->alg()Z

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIA()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIN()Z

    move-result v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->getPeanut()Ljrp;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v8, v3, Ljrp;->eBt:[Ljkt;

    array-length v8, v8

    if-nez v8, :cond_13

    :cond_1
    const/4 v12, 0x0

    :goto_1
    move-object/from16 v3, p0

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v13, p5

    invoke-direct/range {v2 .. v13}, Lgrz;-><init>(Lgrx;ZIIZLcom/google/android/shared/search/Query;Lcom/google/android/search/shared/service/ClientConfig;JIZ)V

    .line 170
    const/4 v3, 0x0

    .line 172
    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agw()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 173
    sget-object v3, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 177
    :cond_2
    if-nez v3, :cond_3

    .line 178
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIA()I

    move-result v3

    if-gtz v3, :cond_16

    const/4 v3, 0x0

    .line 182
    :cond_3
    :goto_2
    if-nez v3, :cond_6

    .line 183
    const/4 v3, 0x0

    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v4

    if-eqz v4, :cond_1c

    if-eqz p3, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    if-eqz p6, :cond_1c

    :cond_5
    sget-object v3, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 187
    :cond_6
    :goto_3
    if-nez v3, :cond_7

    .line 188
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 192
    :cond_7
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alj()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 195
    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v4}, Lcom/google/android/search/shared/actions/utils/CardDecision;->all()I

    move-result v15

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v4

    if-eqz v4, :cond_2a

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v4

    iget-object v5, v4, Ljkt;->eqo:Ljlm;

    if-eqz v5, :cond_2a

    iget-object v4, v4, Ljkt;->eqo:Ljlm;

    invoke-virtual {v4}, Ljlm;->bpi()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v4}, Ljlm;->alk()I

    move-result v3

    :cond_8
    invoke-virtual {v4}, Ljlm;->alm()Z

    move-result v5

    if-eqz v5, :cond_2a

    invoke-virtual {v4}, Ljlm;->all()I

    move-result v15

    move v14, v3

    :goto_4
    new-instance v3, Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIQ()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lgrx;->b(Lcom/google/android/search/shared/service/ClientConfig;)Z

    move-result v7

    if-eqz v7, :cond_22

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->Zv()Z

    move-result v7

    if-eqz v7, :cond_22

    const/4 v7, 0x1

    :goto_5
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIO()Z

    move-result v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v9

    if-nez v9, :cond_23

    const/4 v9, 0x1

    :goto_6
    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v10

    if-eqz v10, :cond_24

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v10

    :goto_7
    if-eqz v10, :cond_25

    sget-object v11, Ljll;->erS:Ljsm;

    invoke-virtual {v10, v11}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_9

    sget-object v11, Ljrc;->ezW:Ljsm;

    invoke-virtual {v10, v11}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_25

    :cond_9
    const/4 v10, 0x1

    :goto_8
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->alg()Z

    move-result v11

    if-eqz v11, :cond_26

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v11

    if-eqz v11, :cond_a

    if-eqz v10, :cond_26

    :cond_a
    const/4 v10, 0x1

    :goto_9
    const/4 v11, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIM()J

    move-result-wide v12

    invoke-direct/range {v3 .. v15}, Lcom/google/android/search/shared/actions/utils/CardDecision;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZZZJII)V

    .line 199
    :cond_b
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alj()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 201
    if-eqz p3, :cond_c

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->aqw()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v4

    if-nez v4, :cond_27

    :cond_c
    const/4 v4, 0x1

    :goto_a
    invoke-static {v4}, Lifv;->gY(Z)V

    .line 213
    :cond_d
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alf()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ale()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ale()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_f

    :cond_e
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v4

    if-nez v4, :cond_28

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ale()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_28

    :cond_f
    const/4 v4, 0x1

    .line 218
    :goto_b
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v5

    if-eqz v5, :cond_10

    if-eqz v4, :cond_10

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alj()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 221
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x32

    if-gt v4, v5, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a063c

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_c
    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lgrz;->a(Lgrz;Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    .line 226
    :cond_10
    return-object v3

    .line 159
    :cond_11
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 161
    const-wide/16 v10, 0x0

    goto/16 :goto_0

    .line 163
    :cond_12
    move-object/from16 v0, p0

    iget-wide v10, v0, Lgrx;->cUD:J

    goto/16 :goto_0

    .line 165
    :cond_13
    iget-object v3, v3, Ljrp;->eBt:[Ljkt;

    const/4 v8, 0x0

    aget-object v3, v3, v8

    iget-object v8, v3, Ljkt;->eqo:Ljlm;

    if-eqz v8, :cond_14

    iget-object v8, v3, Ljkt;->eqo:Ljlm;

    invoke-virtual {v8}, Ljlm;->bpi()Z

    move-result v8

    if-nez v8, :cond_15

    :cond_14
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_15
    iget-object v3, v3, Ljkt;->eqo:Ljlm;

    invoke-virtual {v3}, Ljlm;->alk()I

    move-result v12

    goto/16 :goto_1

    .line 178
    :cond_16
    sget-object v4, Lgrx;->cUG:[Ljava/lang/Class;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_d
    if-ge v3, v5, :cond_1a

    aget-object v6, v4, v3

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    const/4 v3, 0x1

    :goto_e
    if-eqz v3, :cond_17

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-eqz v3, :cond_17

    sget-object v4, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-ne v3, v4, :cond_3

    :cond_17
    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agb()Ldtw;

    move-result-object v3

    if-eqz v3, :cond_18

    sget-object v4, Ldtw;->bLj:Ldtw;

    if-ne v3, v4, :cond_1b

    :cond_18
    const v3, 0x7f0a0722

    :goto_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a0723

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a0931

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const/4 v5, 0x0

    invoke-static {v4, v3, v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v3

    invoke-virtual {v3}, Ldya;->alr()Ldya;

    move-result-object v3

    const/4 v4, 0x1

    iput-boolean v4, v3, Ldya;->bQq:Z

    invoke-virtual {v3}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    goto/16 :goto_2

    :cond_19
    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_1a
    const/4 v3, 0x0

    goto :goto_e

    :cond_1b
    iget v3, v3, Ldtw;->bLt:I

    goto :goto_f

    .line 183
    :cond_1c
    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agb()Ldtw;

    move-result-object v5

    if-nez v5, :cond_1d

    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_1d
    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v4

    if-eqz v4, :cond_1f

    iget v4, v5, Ldtw;->bLs:I

    iget v3, v5, Ldtw;->bLt:I

    :goto_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->t(Ljava/lang/String;I)Ldya;

    move-result-object v4

    if-eqz v3, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1e

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Ldya;->u(Ljava/lang/String;I)Ldya;

    :cond_1e
    const/4 v3, 0x1

    iput-boolean v3, v4, Ldya;->bQq:Z

    invoke-virtual {v4}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    goto/16 :goto_3

    :cond_1f
    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v4

    if-eqz v4, :cond_20

    iget v4, v5, Ldtw;->bLu:I

    goto :goto_10

    :cond_20
    invoke-interface/range {p1 .. p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agr()Z

    move-result v4

    if-eqz v4, :cond_21

    const v4, 0x7f0a0650

    goto :goto_10

    :cond_21
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 195
    :cond_22
    const/4 v7, 0x0

    goto/16 :goto_5

    :cond_23
    const/4 v9, 0x0

    goto/16 :goto_6

    :cond_24
    const/4 v10, 0x0

    goto/16 :goto_7

    :cond_25
    const/4 v10, 0x0

    goto/16 :goto_8

    :cond_26
    const/4 v10, 0x0

    goto/16 :goto_9

    .line 201
    :cond_27
    const/4 v4, 0x0

    goto/16 :goto_a

    .line 213
    :cond_28
    const/4 v4, 0x0

    goto/16 :goto_b

    .line 221
    :cond_29
    move-object/from16 v0, p0

    iget-object v3, v0, Lgrx;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a063b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_c

    :cond_2a
    move v14, v3

    goto/16 :goto_4
.end method

.method final b(Lcom/google/android/search/shared/service/ClientConfig;)Z
    .locals 1
    .param p1    # Lcom/google/android/search/shared/service/ClientConfig;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 423
    iget-object v0, p0, Lgrx;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GU()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/service/ClientConfig;->anh()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgrx;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GT()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
