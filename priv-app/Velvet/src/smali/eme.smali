.class public final Leme;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lesm;


# instance fields
.field final cfe:Lerd;

.field private final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lerd;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Leme;->mExecutor:Ljava/util/concurrent/Executor;

    .line 16
    iput-object p2, p0, Leme;->cfe:Lerd;

    .line 17
    return-void
.end method


# virtual methods
.method public final D(Landroid/net/Uri;)Leml;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lemi;

    invoke-direct {v0}, Lemi;-><init>()V

    .line 22
    iget-object v1, p0, Leme;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lemf;

    invoke-direct {v2, p0, v0, p1}, Lemf;-><init>(Leme;Lemi;Landroid/net/Uri;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 28
    return-object v0
.end method

.method public final clearCache()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Leme;->cfe:Lerd;

    invoke-virtual {v0}, Lerd;->clearCache()V

    .line 34
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Leme;->cfe:Lerd;

    invoke-virtual {v0, p1}, Lerd;->u(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
