.class public final Ljqs;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ezm:[Ljqs;


# instance fields
.field private aez:I

.field private aib:Ljava/lang/String;

.field private eoL:I

.field private eyF:I

.field public ezn:Ljqv;

.field private ezo:I

.field private ezp:I

.field private ezq:Z

.field public ezr:[Ljng;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 4877
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4878
    iput v1, p0, Ljqs;->aez:I

    iput v1, p0, Ljqs;->eoL:I

    iput-object v2, p0, Ljqs;->ezn:Ljqv;

    iput v1, p0, Ljqs;->ezo:I

    const-string v0, ""

    iput-object v0, p0, Ljqs;->aib:Ljava/lang/String;

    iput v1, p0, Ljqs;->ezp:I

    iput v1, p0, Ljqs;->eyF:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljqs;->ezq:Z

    invoke-static {}, Ljng;->bqB()[Ljng;

    move-result-object v0

    iput-object v0, p0, Ljqs;->ezr:[Ljng;

    iput-object v2, p0, Ljqs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqs;->eCz:I

    .line 4879
    return-void
.end method

.method public static bsx()[Ljqs;
    .locals 2

    .prologue
    .line 4741
    sget-object v0, Ljqs;->ezm:[Ljqs;

    if-nez v0, :cond_1

    .line 4742
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 4744
    :try_start_0
    sget-object v0, Ljqs;->ezm:[Ljqs;

    if-nez v0, :cond_0

    .line 4745
    const/4 v0, 0x0

    new-array v0, v0, [Ljqs;

    sput-object v0, Ljqs;->ezm:[Ljqs;

    .line 4747
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4749
    :cond_1
    sget-object v0, Ljqs;->ezm:[Ljqs;

    return-object v0

    .line 4747
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4735
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljqs;->ezn:Ljqv;

    if-nez v0, :cond_1

    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iput-object v0, p0, Ljqs;->ezn:Ljqv;

    :cond_1
    iget-object v0, p0, Ljqs;->ezn:Ljqv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqs;->ezo:I

    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqs;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqs;->aib:Ljava/lang/String;

    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqs;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqs;->ezp:I

    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqs;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqs;->eyF:I

    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqs;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqs;->ezq:Z

    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqs;->aez:I

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqs;->ezr:[Ljng;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljng;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljqs;->ezr:[Ljng;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljng;

    invoke-direct {v3}, Ljng;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljqs;->ezr:[Ljng;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljng;

    invoke-direct {v3}, Ljng;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqs;->ezr:[Ljng;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqs;->eoL:I

    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqs;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 4899
    iget-object v0, p0, Ljqs;->ezn:Ljqv;

    if-eqz v0, :cond_0

    .line 4900
    const/4 v0, 0x1

    iget-object v1, p0, Ljqs;->ezn:Ljqv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4902
    :cond_0
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4903
    const/4 v0, 0x2

    iget v1, p0, Ljqs;->ezo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4905
    :cond_1
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 4906
    const/4 v0, 0x5

    iget-object v1, p0, Ljqs;->aib:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4908
    :cond_2
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 4909
    const/4 v0, 0x6

    iget v1, p0, Ljqs;->ezp:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4911
    :cond_3
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 4912
    const/4 v0, 0x7

    iget v1, p0, Ljqs;->eyF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4914
    :cond_4
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 4915
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljqs;->ezq:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4917
    :cond_5
    iget-object v0, p0, Ljqs;->ezr:[Ljng;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljqs;->ezr:[Ljng;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 4918
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljqs;->ezr:[Ljng;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 4919
    iget-object v1, p0, Ljqs;->ezr:[Ljng;

    aget-object v1, v1, v0

    .line 4920
    if-eqz v1, :cond_6

    .line 4921
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 4918
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4925
    :cond_7
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    .line 4926
    const/16 v0, 0xa

    iget v1, p0, Ljqs;->eoL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4928
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4929
    return-void
.end method

.method public final bsA()Z
    .locals 1

    .prologue
    .line 4847
    iget v0, p0, Ljqs;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsB()Z
    .locals 1

    .prologue
    .line 4858
    iget-boolean v0, p0, Ljqs;->ezq:Z

    return v0
.end method

.method public final bss()I
    .locals 1

    .prologue
    .line 4839
    iget v0, p0, Ljqs;->eyF:I

    return v0
.end method

.method public final bsy()I
    .locals 1

    .prologue
    .line 4779
    iget v0, p0, Ljqs;->ezo:I

    return v0
.end method

.method public final bsz()I
    .locals 1

    .prologue
    .line 4820
    iget v0, p0, Ljqs;->ezp:I

    return v0
.end method

.method public final iW(Z)Ljqs;
    .locals 1

    .prologue
    .line 4861
    iput-boolean p1, p0, Ljqs;->ezq:Z

    .line 4862
    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqs;->aez:I

    .line 4863
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 4933
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4934
    iget-object v1, p0, Ljqs;->ezn:Ljqv;

    if-eqz v1, :cond_0

    .line 4935
    const/4 v1, 0x1

    iget-object v2, p0, Ljqs;->ezn:Ljqv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4938
    :cond_0
    iget v1, p0, Ljqs;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4939
    const/4 v1, 0x2

    iget v2, p0, Ljqs;->ezo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4942
    :cond_1
    iget v1, p0, Ljqs;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 4943
    const/4 v1, 0x5

    iget-object v2, p0, Ljqs;->aib:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4946
    :cond_2
    iget v1, p0, Ljqs;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 4947
    const/4 v1, 0x6

    iget v2, p0, Ljqs;->ezp:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4950
    :cond_3
    iget v1, p0, Ljqs;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 4951
    const/4 v1, 0x7

    iget v2, p0, Ljqs;->eyF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4954
    :cond_4
    iget v1, p0, Ljqs;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 4955
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljqs;->ezq:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4958
    :cond_5
    iget-object v1, p0, Ljqs;->ezr:[Ljng;

    if-eqz v1, :cond_8

    iget-object v1, p0, Ljqs;->ezr:[Ljng;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 4959
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljqs;->ezr:[Ljng;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 4960
    iget-object v2, p0, Ljqs;->ezr:[Ljng;

    aget-object v2, v2, v0

    .line 4961
    if-eqz v2, :cond_6

    .line 4962
    const/16 v3, 0x9

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4959
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    move v0, v1

    .line 4967
    :cond_8
    iget v1, p0, Ljqs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_9

    .line 4968
    const/16 v1, 0xa

    iget v2, p0, Ljqs;->eoL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4971
    :cond_9
    return v0
.end method

.method public final rA(I)Ljqs;
    .locals 1

    .prologue
    .line 4842
    iput p1, p0, Ljqs;->eyF:I

    .line 4843
    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqs;->aez:I

    .line 4844
    return-object p0
.end method

.method public final ry(I)Ljqs;
    .locals 1

    .prologue
    .line 4782
    iput p1, p0, Ljqs;->ezo:I

    .line 4783
    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqs;->aez:I

    .line 4784
    return-object p0
.end method

.method public final rz(I)Ljqs;
    .locals 1

    .prologue
    .line 4823
    iput p1, p0, Ljqs;->ezp:I

    .line 4824
    iget v0, p0, Ljqs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqs;->aez:I

    .line 4825
    return-object p0
.end method
