.class public final Lfzb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private cDA:Liyk;

.field private cDB:Liyk;

.field private cDz:Liyk;

.field private final mClock:Lemp;

.field private final mStringEvaluator:Lgbr;

.field private final mTravelReport:Lgca;


# direct methods
.method public constructor <init>(Lemp;Lgca;Lgbr;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lfzb;->mClock:Lemp;

    .line 57
    iput-object p2, p0, Lfzb;->mTravelReport:Lgca;

    .line 58
    iput-object p3, p0, Lfzb;->mStringEvaluator:Lgbr;

    .line 59
    invoke-virtual {p0}, Lfzb;->aDT()V

    .line 60
    return-void
.end method

.method private static a(Liyg;Ljbp;)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 523
    invoke-virtual {p0}, Liyg;->aEz()I

    move-result v0

    invoke-static {v0}, Lgba;->jQ(I)Lgba;

    move-result-object v0

    .line 524
    invoke-static {p0}, Lgaz;->c(Liyg;)Ljava/lang/String;

    move-result-object v1

    .line 525
    invoke-virtual {p0}, Liyg;->bbL()Ljava/lang/String;

    move-result-object v2

    .line 526
    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v3, v2}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lapc;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 439
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v0, v0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    iget-object v0, v0, Liyh;->dQi:[Liyk;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    move v1, v2

    .line 442
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 443
    iget-object v4, p0, Lfzb;->mTravelReport:Lgca;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyk;

    invoke-virtual {v4, v0}, Lgca;->c(Liyk;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 450
    :goto_1
    if-nez v1, :cond_0

    iget-object v0, p0, Lfzb;->cDA:Liyk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v0, v0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v2

    .line 452
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v3, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v2}, Lgbx;->c(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 455
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lapi;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapi;

    iput-object v0, p2, Lapc;->akH:[Lapi;

    .line 457
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    iget-object v0, v0, Liyh;->dQj:Ljhe;

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lfzb;->mStringEvaluator:Lgbr;

    iget-object v1, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v1}, Lgca;->aDF()Liyh;

    move-result-object v1

    iget-object v1, v1, Liyh;->dQj:Ljhe;

    invoke-virtual {v0, p1, v1}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lapc;->cO(Ljava/lang/String;)Lapc;

    .line 461
    :cond_1
    return-void

    .line 442
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private b(Landroid/content/Context;Lapc;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 469
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 470
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->sk()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->sj()Ljava/lang/String;

    move-result-object v0

    .line 472
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 473
    invoke-virtual {p2, v0}, Lapc;->cL(Ljava/lang/String;)Lapc;

    .line 477
    :cond_0
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->oK()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQv:Ljbp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQv:Ljbp;

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->bci()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 481
    :cond_2
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 482
    const-string v0, ""

    .line 483
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    iget-object v2, v2, Liyk;->dQq:[Liyj;

    aget-object v2, v2, v8

    invoke-virtual {v2}, Liyj;->bci()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 484
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->bch()Ljava/lang/String;

    move-result-object v0

    .line 488
    :cond_3
    :goto_1
    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 489
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    iget-object v2, v2, Liyk;->dQt:[J

    array-length v2, v2

    if-lez v2, :cond_8

    .line 490
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    iget-object v2, v2, Liyk;->dQt:[J

    aget-wide v2, v2, v8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lfzb;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 492
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {p1, v2, v9}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    .line 494
    const v3, 0x7f0a01db

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v8

    aput-object v0, v4, v9

    aput-object v2, v4, v10

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lapc;->cM(Ljava/lang/String;)Lapc;

    .line 504
    :cond_4
    :goto_2
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQr:Ljbp;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQr:Ljbp;

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 506
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQr:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    iget-object v1, p0, Lfzb;->cDB:Liyk;

    invoke-virtual {v1}, Liyk;->bcl()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 509
    const v1, 0x7f0a01c1

    new-array v2, v10, [Ljava/lang/Object;

    aput-object v0, v2, v8

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    invoke-virtual {v0}, Liyk;->bck()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lapc;->cN(Ljava/lang/String;)Lapc;

    .line 519
    :cond_5
    :goto_3
    return-void

    .line 470
    :cond_6
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->bce()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 485
    :cond_7
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    iget-object v2, v2, Liyk;->dQv:Ljbp;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lfzb;->cDB:Liyk;

    iget-object v2, v2, Liyk;->dQv:Ljbp;

    invoke-virtual {v2}, Ljbp;->oK()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 486
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQv:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 498
    :cond_8
    const v2, 0x7f0a01da

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v1, v3, v8

    aput-object v0, v3, v9

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lapc;->cM(Ljava/lang/String;)Lapc;

    goto :goto_2

    .line 514
    :cond_9
    const v1, 0x7f0a01dc

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v0, v2, v8

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lapc;->cN(Ljava/lang/String;)Lapc;

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 332
    invoke-interface {p3}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v1

    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v2, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 431
    :goto_0
    return-object v0

    .line 335
    :cond_0
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 336
    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    .line 337
    iput-object p2, v1, Lanh;->ahu:Lizj;

    .line 338
    invoke-virtual {v1, v6}, Lanh;->aV(Z)Lanh;

    .line 340
    new-instance v2, Lapb;

    invoke-direct {v2}, Lapb;-><init>()V

    .line 341
    iput-object v2, v1, Lanh;->ahn:Lapb;

    .line 342
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00ba

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lanh;->cn(I)Lanh;

    .line 344
    iget-object v3, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v3, v3, Lgca;->mRoute:Liyg;

    invoke-static {v3, p4}, Lfzb;->a(Liyg;Ljbp;)Ljava/lang/String;

    move-result-object v3

    .line 345
    if-eqz v3, :cond_1

    .line 346
    new-instance v4, Lfsa;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Lfsa;-><init>(I)V

    const v5, 0x7f02011b

    invoke-virtual {v4, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v4

    invoke-virtual {v4, v3, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v4

    iput-object v4, v1, Lanh;->ahs:Lani;

    .line 350
    :cond_1
    new-instance v4, Lfsa;

    const/16 v5, 0x97

    invoke-direct {v4, v5}, Lfsa;-><init>(I)V

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lfsa;->jC(I)Lani;

    move-result-object v4

    iput-object v4, v1, Lanh;->aht:Lani;

    .line 354
    invoke-virtual {p0, p2, p3}, Lfzb;->a(Lizj;Lfmt;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 355
    new-array v4, v6, [Laoo;

    invoke-virtual {p0, p1}, Lfzb;->bm(Landroid/content/Context;)Laoo;

    move-result-object v5

    aput-object v5, v4, v7

    iput-object v4, v2, Lapb;->akE:[Laoo;

    .line 356
    invoke-virtual {p0, p1, p2, p3}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lapb;->cI(Ljava/lang/String;)Lapb;

    .line 357
    invoke-virtual {p0, p2, p3}, Lfzb;->c(Lizj;Lfmt;)I

    move-result v4

    invoke-virtual {v2, v4}, Lapb;->cO(I)Lapb;

    .line 358
    invoke-virtual {p0, p2, p3}, Lfzb;->b(Lizj;Lfmt;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 359
    const v4, 0x7f0a01d5

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lapb;->cJ(Ljava/lang/String;)Lapb;

    .line 367
    :cond_2
    :goto_1
    invoke-virtual {p0, p1, p3, v6}, Lfzb;->b(Landroid/content/Context;Lfmt;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lapb;->cG(Ljava/lang/String;)Lapb;

    .line 368
    invoke-virtual {p0, p1, p3}, Lfzb;->g(Landroid/content/Context;Lfmt;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lapb;->cH(Ljava/lang/String;)Lapb;

    .line 370
    iget-object v4, p0, Lfzb;->cDz:Liyk;

    if-nez v4, :cond_4

    .line 373
    invoke-virtual {v2}, Lapb;->rS()Lapb;

    .line 374
    invoke-virtual {v2}, Lapb;->rV()Lapb;

    .line 375
    iput-object v0, v2, Lapb;->akE:[Laoo;

    .line 376
    iput-object v0, v2, Lapb;->akG:Lapc;

    move-object v0, v1

    .line 377
    goto/16 :goto_0

    .line 362
    :cond_3
    invoke-virtual {p0, p1}, Lfzb;->bn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lapb;->cJ(Ljava/lang/String;)Lapb;

    goto :goto_1

    .line 380
    :cond_4
    new-instance v0, Lapc;

    invoke-direct {v0}, Lapc;-><init>()V

    .line 381
    iput-object v0, v2, Lapb;->akG:Lapc;

    .line 382
    invoke-direct {p0, p1, v0}, Lfzb;->a(Landroid/content/Context;Lapc;)V

    .line 383
    if-eqz v3, :cond_5

    .line 384
    invoke-virtual {v0, v3}, Lapc;->cP(Ljava/lang/String;)Lapc;

    .line 388
    :cond_5
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbW()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 389
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbV()J

    move-result-wide v2

    .line 391
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {p1, v2, v3, v7}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v2

    .line 393
    const v3, 0x7f0a01c7

    new-array v4, v6, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lapc;->cK(Ljava/lang/String;)Lapc;

    .line 399
    :cond_6
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    if-eqz v2, :cond_9

    .line 401
    invoke-direct {p0, p1, v0}, Lfzb;->b(Landroid/content/Context;Lapc;)V

    .line 425
    :cond_7
    :goto_2
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    iget-object v2, v2, Liyh;->dQe:[Liyi;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 427
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    iget-object v2, v2, Liyh;->dQe:[Liyi;

    invoke-static {p1, v2}, Lgbx;->a(Landroid/content/Context;[Liyi;)Lapg;

    move-result-object v2

    iput-object v2, v0, Lapc;->akM:Lapg;

    :cond_8
    move-object v0, v1

    .line 431
    goto/16 :goto_0

    .line 402
    :cond_9
    iget-object v2, p0, Lfzb;->cDA:Liyk;

    if-eqz v2, :cond_7

    .line 406
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbS()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 408
    const v2, 0x7f0a01de

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lapc;->cM(Ljava/lang/String;)Lapc;

    .line 410
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbR()I

    move-result v2

    invoke-static {p1, v2, v6}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    .line 414
    iget-object v3, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v3, v3, Lgca;->mRoute:Liyg;

    iget-object v3, v3, Liyg;->dPL:Liyh;

    iget-object v3, v3, Liyh;->dQi:[Liyk;

    .line 415
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    .line 416
    iget-object v4, v3, Liyk;->dQv:Ljbp;

    if-eqz v4, :cond_7

    iget-object v4, v3, Liyk;->dQv:Ljbp;

    invoke-virtual {v4}, Ljbp;->oK()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 418
    const v4, 0x7f0a01df

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v7

    iget-object v2, v3, Liyk;->dQv:Ljbp;

    invoke-virtual {v2}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lapc;->cN(Ljava/lang/String;)Lapc;

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Lizj;Ljava/lang/String;Ljbp;)Lanh;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 276
    iget-object v0, p0, Lfzb;->cDz:Liyk;

    if-nez v0, :cond_1

    .line 277
    const/4 v0, 0x0

    .line 326
    :cond_0
    :goto_0
    return-object v0

    .line 280
    :cond_1
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 281
    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 282
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 283
    invoke-virtual {v0, v8}, Lanh;->aV(Z)Lanh;

    .line 285
    new-instance v1, Lapc;

    invoke-direct {v1}, Lapc;-><init>()V

    .line 286
    iput-object v1, v0, Lanh;->ahi:Lapc;

    .line 287
    invoke-direct {p0, p1, v1}, Lfzb;->a(Landroid/content/Context;Lapc;)V

    .line 288
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    invoke-static {v2, p4}, Lfzb;->a(Liyg;Ljbp;)Ljava/lang/String;

    move-result-object v2

    .line 289
    if-eqz v2, :cond_2

    .line 290
    invoke-virtual {v1, v2}, Lapc;->cP(Ljava/lang/String;)Lapc;

    .line 294
    :cond_2
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbW()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 295
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbV()J

    move-result-wide v2

    .line 297
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {p1, v2, v3, v7}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v2

    .line 299
    const v3, 0x7f0a01c9

    new-array v4, v8, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lapc;->cK(Ljava/lang/String;)Lapc;

    .line 304
    :cond_3
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    if-eqz v2, :cond_5

    .line 306
    invoke-direct {p0, p1, v1}, Lfzb;->b(Landroid/content/Context;Lapc;)V

    .line 320
    :cond_4
    :goto_1
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    iget-object v2, v2, Liyh;->dQe:[Liyi;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 322
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    iget-object v2, v2, Liyh;->dQe:[Liyi;

    invoke-static {p1, v2}, Lgbx;->a(Landroid/content/Context;[Liyi;)Lapg;

    move-result-object v2

    iput-object v2, v1, Lapc;->akM:Lapg;

    goto :goto_0

    .line 307
    :cond_5
    iget-object v2, p0, Lfzb;->cDA:Liyk;

    if-eqz v2, :cond_4

    .line 311
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbS()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 312
    iget-object v2, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v2, v2, Lgca;->mRoute:Liyg;

    iget-object v2, v2, Liyg;->dPL:Liyh;

    invoke-virtual {v2}, Liyh;->bbR()I

    move-result v2

    .line 314
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100021

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object p3, v5, v8

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lapc;->cM(Ljava/lang/String;)Lapc;

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lizj;Lfmt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lfzb;->cDz:Liyk;

    if-nez v0, :cond_0

    .line 217
    const-string v0, ""

    .line 222
    :goto_0
    return-object v0

    .line 219
    :cond_0
    invoke-virtual {p0, p2, p3}, Lfzb;->b(Lizj;Lfmt;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 220
    invoke-virtual {p0, p1}, Lfzb;->bn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 222
    :cond_1
    const v0, 0x7f0a01d7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lizj;Lfmt;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 193
    invoke-virtual {p0, p1, p2}, Lfzb;->b(Lizj;Lfmt;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    iget-object v2, p0, Lfzb;->cDz:Liyk;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lfzb;->cDz:Liyk;

    iget-object v2, v2, Liyk;->dQx:Ljcg;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lfzb;->cDz:Liyk;

    iget-object v2, v2, Liyk;->dQx:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lfzb;->cDz:Liyk;

    iget-object v2, v2, Liyk;->dQx:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    invoke-virtual {v2}, Ljie;->bkv()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 200
    iget-object v2, p0, Lfzb;->cDz:Liyk;

    iget-object v2, v2, Liyk;->dQx:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    invoke-virtual {v2}, Ljie;->bmV()J

    move-result-wide v2

    .line 202
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lfzb;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    .line 204
    sub-long/2addr v2, v4

    .line 206
    const-wide/16 v4, 0xb4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 209
    goto :goto_0
.end method

.method public final aDT()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aEE()Liyk;

    move-result-object v0

    iput-object v0, p0, Lfzb;->cDB:Liyk;

    .line 77
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aED()Liyk;

    move-result-object v0

    iput-object v0, p0, Lfzb;->cDA:Liyk;

    .line 78
    iget-object v0, p0, Lfzb;->cDA:Liyk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzb;->cDB:Liyk;

    :goto_0
    iput-object v0, p0, Lfzb;->cDz:Liyk;

    .line 79
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lfzb;->cDA:Liyk;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lfmt;Z)Ljava/lang/String;
    .locals 11
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 100
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfzb;->cDA:Liyk;

    if-nez v0, :cond_1

    .line 101
    if-eqz p3, :cond_0

    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const v0, 0x7f0a01de

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, Lfzb;->cDA:Liyk;

    if-nez v0, :cond_5

    .line 113
    const-string v0, ""

    .line 114
    iget-object v1, p0, Lfzb;->cDB:Liyk;

    iget-object v1, v1, Liyk;->dQq:[Liyj;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Liyj;->bci()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 115
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Liyj;->bch()Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_2
    :goto_1
    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lfzb;->cDB:Liyk;

    iget-object v1, v1, Liyk;->dQq:[Liyj;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Liyj;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    iget-object v2, p0, Lfzb;->cDB:Liyk;

    invoke-static {v2}, Lgca;->b(Liyk;)J

    move-result-wide v2

    .line 123
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lfzb;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    .line 124
    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long/2addr v2, v4

    invoke-virtual {v6, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 126
    if-lez v2, :cond_4

    .line 127
    invoke-static {p1, v2, v9}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    .line 128
    const v3, 0x7f0a01db

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v8

    aput-object v0, v4, v9

    aput-object v2, v4, v10

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 116
    :cond_3
    iget-object v1, p0, Lfzb;->cDB:Liyk;

    iget-object v1, v1, Liyk;->dQv:Ljbp;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfzb;->cDB:Liyk;

    iget-object v1, v1, Liyk;->dQv:Ljbp;

    invoke-virtual {v1}, Ljbp;->oK()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 117
    iget-object v0, p0, Lfzb;->cDB:Liyk;

    iget-object v0, v0, Liyk;->dQv:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 131
    :cond_4
    const v2, 0x7f0a01da

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v1, v3, v8

    aput-object v0, v3, v9

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 137
    :cond_5
    iget-object v0, p0, Lfzb;->cDA:Liyk;

    iget-object v0, v0, Liyk;->dQv:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lfzb;->cDA:Liyk;

    invoke-static {v1}, Lgca;->a(Liyk;)J

    move-result-wide v2

    .line 140
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {p1, v2, v3, v8}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    .line 142
    const v2, 0x7f0a01d3

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v0, v3, v8

    aput-object v1, v3, v9

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final b(Lizj;Lfmt;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-static {p1}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 229
    iget-object v2, p0, Lfzb;->cDz:Liyk;

    if-nez v2, :cond_0

    move v0, v1

    .line 235
    :goto_0
    return v0

    .line 232
    :cond_0
    iget-object v2, p0, Lfzb;->cDz:Liyk;

    iget-object v2, v2, Liyk;->dQx:Ljcg;

    iput-object v2, v0, Lizj;->dUr:Ljcg;

    .line 233
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->n(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    move-result-object v2

    .line 235
    if-eqz v2, :cond_1

    invoke-virtual {v2, v0}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->y(Lizj;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bm(Landroid/content/Context;)Laoo;
    .locals 4

    .prologue
    .line 247
    new-instance v0, Laoo;

    invoke-direct {v0}, Laoo;-><init>()V

    .line 248
    iget-object v1, p0, Lfzb;->cDz:Liyk;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfzb;->cDz:Liyk;

    iget-object v1, v1, Liyk;->dQx:Ljcg;

    if-eqz v1, :cond_0

    .line 249
    const/4 v1, 0x1

    new-array v1, v1, [Ljcg;

    const/4 v2, 0x0

    iget-object v3, p0, Lfzb;->cDz:Liyk;

    iget-object v3, v3, Liyk;->dQx:Ljcg;

    aput-object v3, v1, v2

    iput-object v1, v0, Laoo;->ajM:[Ljcg;

    .line 251
    invoke-virtual {p0, p1}, Lfzb;->bn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoo;->cc(Ljava/lang/String;)Laoo;

    .line 252
    const v1, 0x7f0a01d5

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoo;->cd(Ljava/lang/String;)Laoo;

    .line 255
    :cond_0
    return-object v0
.end method

.method public final bn(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 260
    iget-object v0, p0, Lfzb;->cDz:Liyk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzb;->cDz:Liyk;

    iget-object v0, v0, Liyk;->dQx:Ljcg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzb;->cDz:Liyk;

    iget-object v0, v0, Liyk;->dQx:Ljcg;

    iget-object v0, v0, Ljcg;->dRY:Ljie;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzb;->cDz:Liyk;

    iget-object v0, v0, Liyk;->dQx:Ljcg;

    iget-object v0, v0, Ljcg;->dRY:Ljie;

    invoke-virtual {v0}, Ljie;->bkv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lfzb;->cDz:Liyk;

    iget-object v0, v0, Liyk;->dQx:Ljcg;

    iget-object v0, v0, Ljcg;->dRY:Ljie;

    invoke-virtual {v0}, Ljie;->bmV()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 266
    const v2, 0x7f0a01d4

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1, v4}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final c(Lizj;Lfmt;)I
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0, p1, p2}, Lfzb;->b(Lizj;Lfmt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const v0, 0x7f0200e8

    .line 242
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0200e7

    goto :goto_0
.end method

.method public final g(Landroid/content/Context;Lfmt;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 162
    iget-object v0, p0, Lfzb;->cDz:Liyk;

    if-nez v0, :cond_3

    .line 163
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 187
    :goto_0
    return-object v0

    .line 167
    :cond_0
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v0, v0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbR()I

    move-result v0

    invoke-static {p1, v0, v6}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v1, v1, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    iget-object v1, v1, Liyh;->dQi:[Liyk;

    .line 171
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    .line 172
    iget-object v2, v1, Liyk;->dQv:Ljbp;

    if-eqz v2, :cond_1

    iget-object v2, v1, Liyk;->dQv:Ljbp;

    invoke-virtual {v2}, Ljbp;->oK()Z

    move-result v2

    if-nez v2, :cond_2

    .line 173
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 177
    :cond_2
    const v2, 0x7f0a01df

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v7

    iget-object v0, v1, Liyk;->dQv:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 180
    :cond_3
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    iget-object v0, v0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    invoke-virtual {v0}, Liyh;->bbV()J

    move-result-wide v0

    .line 182
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lfzb;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 183
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long/2addr v0, v2

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 185
    invoke-static {p1, v0, v6}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 187
    const v1, 0x7f0a01ca

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 148
    iget-object v0, p0, Lfzb;->cDz:Liyk;

    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    .line 154
    :cond_0
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aEC()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0, v4}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 156
    const v1, 0x7f0a0183

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    aput-object p2, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final m(Landroid/location/Location;)Z
    .locals 1
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    iget-object v0, p0, Lfzb;->mTravelReport:Lgca;

    invoke-virtual {v0, p1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfzb;->cDz:Liyk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
