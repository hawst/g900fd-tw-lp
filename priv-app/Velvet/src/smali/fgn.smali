.class public final Lfgn;
.super Lfeu;
.source "PG"


# instance fields
.field private final cqT:Ljho;

.field private final cqU:Ljhn;


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 24
    iget-object v0, p1, Lizj;->dTS:Ljho;

    iput-object v0, p0, Lfgn;->cqT:Ljho;

    .line 25
    iget-object v0, p0, Lfgn;->cqT:Ljho;

    iget-object v0, v0, Ljho;->dPO:[Ljhn;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lfgn;->cqU:Ljhn;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lfgn;->cqU:Ljhn;

    invoke-virtual {v0}, Ljhn;->blZ()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lfgn;->cqU:Ljhn;

    invoke-virtual {v0}, Ljhn;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "event"

    return-object v0
.end method

.method public final ayY()I
    .locals 3

    .prologue
    const v0, 0x7f0202dc

    .line 42
    iget-object v1, p0, Lfgn;->cqU:Ljhn;

    invoke-virtual {v1}, Ljhn;->oP()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljhn;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    const v0, 0x7f0202da

    goto :goto_0

    :sswitch_1
    const v0, 0x7f0202cf

    goto :goto_0

    :sswitch_2
    const v0, 0x7f0202d3

    goto :goto_0

    :sswitch_3
    const v0, 0x7f02016f

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lfgb;->cqJ:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfgn;->cqU:Ljhn;

    invoke-virtual {v0}, Ljhn;->blZ()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getVisibility()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method
