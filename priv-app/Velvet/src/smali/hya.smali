.class public Lhya;
.super Lgly;
.source "PG"


# instance fields
.field private final bEC:Lgnp;

.field private bED:Z

.field private final bbd:Lenw;

.field private final dun:Z

.field private final duq:Z

.field private final dux:Landroid/speech/RecognitionService$Callback;

.field private final duy:Z

.field private final mOnDoneListener:Lhyb;


# direct methods
.method public constructor <init>(Landroid/speech/RecognitionService$Callback;Lhyb;ZZZ)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lgly;-><init>()V

    .line 59
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/speech/RecognitionService$Callback;

    iput-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    .line 60
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyb;

    iput-object v0, p0, Lhya;->mOnDoneListener:Lhyb;

    .line 61
    iput-boolean p3, p0, Lhya;->dun:Z

    .line 62
    iput-boolean p4, p0, Lhya;->duy:Z

    .line 63
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lhya;->bbd:Lenw;

    .line 64
    new-instance v0, Lgnp;

    invoke-direct {v0}, Lgnp;-><init>()V

    iput-object v0, p0, Lhya;->bEC:Lgnp;

    .line 65
    iput-boolean p5, p0, Lhya;->duq:Z

    .line 66
    return-void
.end method

.method private b(Ljvv;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhya;->bED:Z

    .line 231
    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    iget-object v1, v0, Ljvw;->eIk:[Ljvs;

    .line 233
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 234
    array-length v0, v1

    new-array v3, v0, [F

    .line 236
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 237
    aget-object v4, v1, v0

    .line 238
    invoke-virtual {v4}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    invoke-virtual {v4}, Ljvs;->bty()F

    move-result v4

    aput v4, v3, v0

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 243
    const-string v1, "results_recognition"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 244
    const-string v1, "confidence_scores"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 245
    if-eqz p2, :cond_1

    .line 246
    const-string v1, "results_language"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_1
    :try_start_0
    iget-object v1, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1, v0}, Landroid/speech/RecognitionService$Callback;->results(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_1
    return-void

    .line 250
    :catch_0
    move-exception v0

    .line 251
    const-string v1, "ListenerAdapter"

    const-string v2, "#result remote callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final CZ()V
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 260
    iget-boolean v0, p0, Lhya;->bED:Z

    if-nez v0, :cond_0

    .line 262
    :try_start_0
    iget-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :cond_0
    :goto_0
    iget-object v0, p0, Lhya;->mOnDoneListener:Lhyb;

    invoke-interface {v0}, Lhyb;->CZ()V

    .line 268
    return-void

    .line 263
    :catch_0
    move-exception v0

    .line 264
    const-string v1, "ListenerAdapter"

    const-string v2, "#error remote callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final Nr()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 73
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 74
    iget-object v1, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1, v0}, Landroid/speech/RecognitionService$Callback;->readyForSpeech(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    const-string v1, "ListenerAdapter"

    const-string v2, "readyForSpeech callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final Nw()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 107
    iget-boolean v0, p0, Lhya;->bED:Z

    if-nez v0, :cond_0

    .line 109
    :try_start_0
    iget-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_0
    :goto_0
    iget-object v0, p0, Lhya;->mOnDoneListener:Lhyb;

    invoke-interface {v0}, Lhyb;->CZ()V

    .line 115
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const-string v1, "ListenerAdapter"

    const-string v2, "#error remote callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final O(F)V
    .locals 3

    .prologue
    .line 142
    :try_start_0
    iget-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v0, p1}, Landroid/speech/RecognitionService$Callback;->rmsChanged(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 144
    const-string v1, "ListenerAdapter"

    const-string v2, "rmsChanged callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 151
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 154
    iget-boolean v0, p0, Lhya;->duq:Z

    if-eqz v0, :cond_0

    .line 156
    :goto_0
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-nez v0, :cond_3

    .line 157
    iget-boolean v0, p0, Lhya;->dun:Z

    if-eqz v0, :cond_5

    .line 158
    iget-object v0, p1, Ljvv;->eIm:Ljvw;

    if-eqz v0, :cond_3

    iget-object v1, p1, Ljvv;->eIm:Ljvw;

    iget-object v0, v1, Ljvw;->eIk:[Ljvs;

    array-length v2, v0

    if-lez v2, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-array v4, v2, [F

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    iget-object v5, v1, Ljvw;->eIk:[Ljvs;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Ljvs;->bty()F

    move-result v5

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 154
    :cond_0
    const/4 p2, 0x0

    goto :goto_0

    .line 158
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "results_recognition"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "confidence_scores"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    if-eqz p2, :cond_2

    const-string v1, "results_language"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :try_start_0
    iget-object v1, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1, v0}, Landroid/speech/RecognitionService$Callback;->partialResults(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :cond_3
    :goto_2
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v7, :cond_4

    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    if-eqz v0, :cond_4

    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    iget-object v0, v0, Ljvw;->eIk:[Ljvs;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 169
    invoke-direct {p0, p1, p2}, Lhya;->b(Ljvv;Ljava/lang/String;)V

    .line 171
    :cond_4
    return-void

    .line 158
    :catch_0
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "#partialResults remote callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 159
    :cond_5
    iget-boolean v0, p0, Lhya;->duy:Z

    if-eqz v0, :cond_3

    .line 160
    iget-object v0, p0, Lhya;->bEC:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->g(Ljvv;)Landroid/util/Pair;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "results_recognition"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "android.speech.extra.UNSTABLE_TEXT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p2, :cond_6

    const-string v1, "results_language"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :try_start_1
    iget-object v1, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v1, v0}, Landroid/speech/RecognitionService$Callback;->partialResults(Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v1, "ListenerAdapter"

    const-string v2, "#partialResults remote callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public final au(J)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 85
    :try_start_0
    iget-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v0}, Landroid/speech/RecognitionService$Callback;->beginningOfSpeech()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    const-string v1, "ListenerAdapter"

    const-string v2, "beginningOfSpeech callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final c(Leiq;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 121
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    const-string v0, "ListenerAdapter"

    const-string v1, "onError"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 125
    :try_start_0
    iget-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-static {p1}, Leno;->h(Leiq;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/speech/RecognitionService$Callback;->error(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    iget-object v0, p0, Lhya;->mOnDoneListener:Lhyb;

    invoke-interface {v0}, Lhyb;->CZ()V

    .line 130
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    const-string v1, "ListenerAdapter"

    const-string v2, "error callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final onEndOfSpeech()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lhya;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 96
    :try_start_0
    iget-object v0, p0, Lhya;->dux:Landroid/speech/RecognitionService$Callback;

    invoke-virtual {v0}, Landroid/speech/RecognitionService$Callback;->endOfSpeech()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    const-string v1, "ListenerAdapter"

    const-string v2, "endOfSpeech callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
