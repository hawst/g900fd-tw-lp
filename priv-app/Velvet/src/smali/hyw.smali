.class final Lhyw;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic aSa:Lcom/google/android/shared/search/Query;

.field private synthetic aSc:Lcom/google/android/search/shared/actions/VoiceAction;

.field private synthetic duV:Lcom/google/android/velvet/ActionData;

.field private synthetic duW:Lhyq;

.field private synthetic duY:Ljava/util/List;


# direct methods
.method constructor <init>(Lhyq;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lhyw;->duW:Lhyq;

    iput-object p2, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    iput-object p3, p0, Lhyw;->aSa:Lcom/google/android/shared/search/Query;

    iput-object p4, p0, Lhyw;->duV:Lcom/google/android/velvet/ActionData;

    iput-object p5, p0, Lhyw;->duY:Ljava/util/List;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 861
    iget-object v0, p0, Lhyw;->duW:Lhyq;

    iget-object v0, v0, Lhyq;->aRU:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhll;

    iget-object v1, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v0, v1}, Lhll;->I(Lcom/google/android/search/shared/actions/VoiceAction;)Lhlk;

    move-result-object v0

    iget-object v1, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Lhlk;->G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v1

    iget-object v2, p0, Lhyw;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v2}, Lhlk;->H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/shared/util/MatchingAppInfo;->lk(Ljava/lang/String;)V

    :cond_0
    return-object v1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 861
    check-cast p1, Lcom/google/android/shared/util/MatchingAppInfo;

    iget-object v0, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, p1}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Lcom/google/android/shared/util/MatchingAppInfo;)V

    invoke-virtual {p1}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    iget-object v0, p0, Lhyw;->duV:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIR()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ActionProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action suppressed due to no matching apps: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhyw;->aSc:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iget-object v0, p0, Lhyw;->duW:Lhyq;

    iget-object v1, p0, Lhyw;->duV:Lcom/google/android/velvet/ActionData;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lhyw;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2, v3}, Lhyq;->b(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhyw;->duW:Lhyq;

    iget-object v1, p0, Lhyw;->duV:Lcom/google/android/velvet/ActionData;

    iget-object v2, p0, Lhyw;->duY:Ljava/util/List;

    iget-object v3, p0, Lhyw;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2, v3}, Lhyq;->b(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method
