.class public final Ldii;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgh;


# instance fields
.field public final aVS:Ldhu;

.field private final byN:Ldjk;

.field public final byO:Ldij;

.field private byP:Lijm;

.field private final mObservable:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>(Ldjk;Ldhu;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjk;

    iput-object v0, p0, Ldii;->byN:Ldjk;

    .line 45
    iput-object p2, p0, Ldii;->aVS:Ldhu;

    .line 47
    new-instance v0, Ldij;

    invoke-direct {v0}, Ldij;-><init>()V

    iput-object v0, p0, Ldii;->byO:Ldij;

    .line 48
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Ldii;->mObservable:Landroid/database/DataSetObservable;

    .line 49
    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v0

    iput-object v0, p0, Ldii;->byP:Lijm;

    .line 51
    new-instance v0, Ldik;

    invoke-direct {v0, p0}, Ldik;-><init>(Ldii;)V

    .line 52
    iget-object v1, p0, Ldii;->byN:Ldjk;

    invoke-virtual {v1, v0}, Ldjk;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 53
    iget-object v1, p0, Ldii;->aVS:Ldhu;

    invoke-interface {v1, v0}, Ldhu;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 55
    invoke-virtual {p0}, Ldii;->acf()V

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 132
    const-string v0, "CompositeSources"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Ldii;->aVS:Ldhu;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 134
    iget-object v0, p0, Ldii;->byN:Ldjk;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 135
    iget-object v0, p0, Ldii;->byO:Ldij;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 136
    return-void
.end method

.method public final declared-synchronized abS()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldii;->byP:Lijm;

    invoke-virtual {v0}, Lijm;->aWM()Lijd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final acf()V
    .locals 6

    .prologue
    .line 85
    iget-object v0, p0, Ldii;->aVS:Ldhu;

    invoke-interface {v0}, Ldhu;->abS()Ljava/util/Collection;

    move-result-object v1

    .line 87
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 88
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 90
    invoke-virtual {v0}, Ldht;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 96
    iget-object v0, p0, Ldii;->byN:Ldjk;

    if-eqz v0, :cond_3

    .line 97
    iget-object v0, p0, Ldii;->byN:Ldjk;

    invoke-virtual {v0}, Ldjk;->abS()Ljava/util/Collection;

    move-result-object v0

    .line 99
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldiq;

    .line 100
    invoke-interface {v0}, Ldiq;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    .line 101
    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 103
    invoke-interface {v3, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 106
    :cond_2
    iget-object v0, p0, Ldii;->byO:Ldij;

    invoke-virtual {v0, v3}, Ldij;->i(Ljava/util/Collection;)V

    .line 109
    :cond_3
    invoke-static {}, Lijm;->aWZ()Lijn;

    move-result-object v2

    .line 110
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgb;

    .line 111
    invoke-interface {v0}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    goto :goto_2

    .line 113
    :cond_4
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgb;

    .line 114
    invoke-interface {v0}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    goto :goto_3

    .line 116
    :cond_5
    monitor-enter p0

    .line 117
    const/4 v0, 0x0

    .line 118
    :try_start_0
    invoke-virtual {v2}, Lijn;->aXa()Lijm;

    move-result-object v1

    .line 119
    iget-object v2, p0, Ldii;->byP:Lijm;

    invoke-virtual {v2, v1}, Lijm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 120
    iput-object v1, p0, Ldii;->byP:Lijm;

    .line 121
    const/4 v0, 0x1

    .line 123
    :cond_6
    if-eqz v0, :cond_7

    .line 125
    iget-object v0, p0, Ldii;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 127
    :cond_7
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ldii;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ldii;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 76
    return-void
.end method
