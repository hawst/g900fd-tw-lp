.class public final Lcem;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final aTy:Ljava/io/DataInputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcem;->aTy:Ljava/io/DataInputStream;

    .line 35
    return-void
.end method


# virtual methods
.method public final Dq()Ljww;
    .locals 4

    .prologue
    .line 45
    :cond_0
    iget-object v0, p0, Lcem;->aTy:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    if-lez v0, :cond_1

    const/high16 v1, 0x400000

    if-le v0, v1, :cond_2

    .line 51
    :cond_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong len "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 56
    :cond_2
    new-array v0, v0, [B

    .line 57
    iget-object v1, p0, Lcem;->aTy:Ljava/io/DataInputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 58
    new-instance v1, Ljww;

    invoke-direct {v1}, Ljww;-><init>()V

    .line 59
    invoke-static {v1, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 65
    return-object v1
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcem;->aTy:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 71
    return-void
.end method
