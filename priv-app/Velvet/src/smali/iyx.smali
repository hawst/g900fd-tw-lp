.class public final Liyx;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field private ajk:Ljava/lang/String;

.field private dRo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46133
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 46134
    const/4 v0, 0x0

    iput v0, p0, Liyx;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Liyx;->agv:I

    const-string v0, ""

    iput-object v0, p0, Liyx;->dRo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liyx;->ajk:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Liyx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liyx;->eCz:I

    .line 46135
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 46047
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liyx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Liyx;->agv:I

    iget v0, p0, Liyx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liyx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyx;->dRo:Ljava/lang/String;

    iget v0, p0, Liyx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liyx;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyx;->ajk:Ljava/lang/String;

    iget v0, p0, Liyx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liyx;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 46150
    iget v0, p0, Liyx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 46151
    const/4 v0, 0x1

    iget v1, p0, Liyx;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 46153
    :cond_0
    iget v0, p0, Liyx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 46154
    const/4 v0, 0x2

    iget-object v1, p0, Liyx;->dRo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46156
    :cond_1
    iget v0, p0, Liyx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 46157
    const/4 v0, 0x3

    iget-object v1, p0, Liyx;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46159
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 46160
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 46164
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 46165
    iget v1, p0, Liyx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 46166
    const/4 v1, 0x1

    iget v2, p0, Liyx;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 46169
    :cond_0
    iget v1, p0, Liyx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 46170
    const/4 v1, 0x2

    iget-object v2, p0, Liyx;->dRo:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46173
    :cond_1
    iget v1, p0, Liyx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 46174
    const/4 v1, 0x3

    iget-object v2, p0, Liyx;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46177
    :cond_2
    return v0
.end method
