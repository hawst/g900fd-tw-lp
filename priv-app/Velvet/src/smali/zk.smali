.class final Lzk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic Ng:Landroid/content/ContentResolver;

.field private synthetic Nh:Z

.field private synthetic Ni:Landroid/content/ContentValues;

.field private synthetic Nj:Lwq;

.field private synthetic Nk:[Ljava/lang/StackTraceElement;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;ZLandroid/content/ContentValues;Lwq;[Ljava/lang/StackTraceElement;)V
    .locals 0

    .prologue
    .line 1033
    iput-object p1, p0, Lzk;->Ng:Landroid/content/ContentResolver;

    iput-boolean p2, p0, Lzk;->Nh:Z

    iput-object p3, p0, Lzk;->Ni:Landroid/content/ContentValues;

    iput-object p4, p0, Lzk;->Nj:Lwq;

    iput-object p5, p0, Lzk;->Nk:[Ljava/lang/StackTraceElement;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1035
    iget-object v1, p0, Lzk;->Ng:Landroid/content/ContentResolver;

    iget-boolean v0, p0, Lzk;->Nh:Z

    if-eqz v0, :cond_1

    sget-object v0, Labn;->CONTENT_URI:Landroid/net/Uri;

    :goto_0
    iget-object v2, p0, Lzk;->Ni:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1039
    sget-object v1, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v1

    .line 1040
    :try_start_0
    iget-object v0, p0, Lzk;->Nj:Lwq;

    iget-wide v2, v0, Lwq;->id:J

    iget-object v0, p0, Lzk;->Nj:Lwq;

    iget-object v4, p0, Lzk;->Nk:[Ljava/lang/StackTraceElement;

    invoke-static {v2, v3, v0, v4}, Lzi;->a(JLwq;[Ljava/lang/StackTraceElement;)V

    .line 1041
    sget-object v0, Lzi;->MW:Ljava/util/HashMap;

    iget-object v2, p0, Lzk;->Nj:Lwq;

    iget-wide v2, v2, Lwq;->id:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lzk;->Nj:Lwq;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1042
    iget-object v0, p0, Lzk;->Nj:Lwq;

    iget v0, v0, Lwq;->Jz:I

    packed-switch v0, :pswitch_data_0

    .line 1064
    :cond_0
    :goto_1
    :pswitch_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 1035
    :cond_1
    sget-object v0, Labn;->OT:Landroid/net/Uri;

    goto :goto_0

    .line 1044
    :pswitch_1
    :try_start_1
    sget-object v2, Lzi;->MZ:Ljava/util/HashMap;

    iget-object v0, p0, Lzk;->Nj:Lwq;

    iget-wide v4, v0, Lwq;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v0, p0, Lzk;->Nj:Lwq;

    check-cast v0, Lvy;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1048
    :pswitch_2
    iget-object v0, p0, Lzk;->Nj:Lwq;

    iget-wide v2, v0, Lwq;->JA:J

    const-wide/16 v4, -0x64

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    iget-object v0, p0, Lzk;->Nj:Lwq;

    iget-wide v2, v0, Lwq;->JA:J

    const-wide/16 v4, -0x65

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    .line 1050
    :cond_2
    sget-object v0, Lzi;->MX:Ljava/util/ArrayList;

    iget-object v2, p0, Lzk;->Nj:Lwq;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1064
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1052
    :cond_3
    :try_start_2
    sget-object v0, Lzi;->MZ:Ljava/util/HashMap;

    iget-object v2, p0, Lzk;->Nj:Lwq;

    iget-wide v2, v2, Lwq;->JA:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "adding item: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lzk;->Nj:Lwq;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to a folder that  doesn\'t exist"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1056
    const-string v2, "Launcher.Model"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1061
    :pswitch_3
    sget-object v2, Lzi;->MY:Ljava/util/ArrayList;

    iget-object v0, p0, Lzk;->Nj:Lwq;

    check-cast v0, Lyy;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1042
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
