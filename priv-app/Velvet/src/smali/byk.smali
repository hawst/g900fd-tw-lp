.class public abstract Lbyk;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Lecr;


# instance fields
.field protected aOO:Lecq;

.field private aOP:Z

.field private aOQ:Z

.field private aOR:Landroid/os/Bundle;

.field private aOS:J

.field private as:Z

.field private cq:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    return-void
.end method

.method private BI()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 142
    iget-boolean v0, p0, Lbyk;->aOP:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbyk;->cq:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbyk;->as:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbyk;->aOQ:Z

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lbyk;->aOR:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    iget-object v1, p0, Lbyk;->aOR:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lecq;->C(Landroid/os/Bundle;)V

    .line 153
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyk;->aOP:Z

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lbyk;->aOR:Landroid/os/Bundle;

    .line 155
    iput-wide v4, p0, Lbyk;->aOS:J

    .line 156
    :cond_0
    return-void

    .line 146
    :cond_1
    iget-wide v0, p0, Lbyk;->aOS:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    iget-wide v2, p0, Lbyk;->aOS:J

    invoke-virtual {v0, v2, v3}, Lecq;->aD(J)V

    goto :goto_0

    .line 151
    :cond_2
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    goto :goto_0
.end method


# virtual methods
.method protected abstract Bm()Lcom/google/android/search/shared/service/ClientConfig;
.end method

.method protected abstract Bn()Lecm;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    iput-object p1, p0, Lbyk;->aOR:Landroid/os/Bundle;

    .line 47
    new-instance v0, Lecq;

    invoke-virtual {p0}, Lbyk;->Bn()Lecm;

    move-result-object v3

    invoke-virtual {p0}, Lbyk;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v4

    move-object v1, p0

    move-object v2, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lbyk;->aOO:Lecq;

    .line 50
    if-nez p1, :cond_0

    .line 51
    invoke-virtual {p0}, Lbyk;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "handover-session-id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lbyk;->aOS:J

    .line 54
    :cond_0
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 55
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 121
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 60
    const-string v0, "handover-session-id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lbyk;->aOS:J

    .line 66
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbyk;->as:Z

    .line 99
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyk;->as:Z

    .line 81
    invoke-direct {p0}, Lbyk;->BI()V

    .line 82
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 113
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lecq;->a(Landroid/os/Bundle;Z)V

    .line 114
    return-void
.end method

.method public onServiceConnected()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbyk;->cq:Z

    .line 73
    invoke-direct {p0}, Lbyk;->BI()V

    .line 74
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    iput-boolean v1, p0, Lbyk;->cq:Z

    .line 105
    invoke-virtual {p0}, Lbyk;->isChangingConfigurations()Z

    iget-boolean v0, p0, Lbyk;->aOP:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbyk;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbyk;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->anz()V

    :goto_0
    iput-boolean v1, p0, Lbyk;->aOP:Z

    .line 106
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 107
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lbyk;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 88
    iput-boolean p1, p0, Lbyk;->aOQ:Z

    .line 89
    if-eqz p1, :cond_0

    .line 90
    invoke-direct {p0}, Lbyk;->BI()V

    .line 92
    :cond_0
    return-void
.end method

.method public tY()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method
