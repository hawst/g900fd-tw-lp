.class public final Lftr;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 14

    .prologue
    const/high16 v11, 0x3f000000    # 0.5f

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    const/4 v10, 0x0

    const v9, 0x7f1101a8

    const/4 v8, 0x0

    .line 40
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 41
    iget-object v2, p0, Lfro;->cBc:Lanh;

    .line 42
    iget-object v3, v2, Lanh;->aho:Lanc;

    .line 44
    invoke-virtual {v3}, Lanc;->ot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 46
    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 47
    iget-object v4, p0, Lftr;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d016d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 49
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setMaxHeight(I)V

    .line 50
    invoke-virtual {v0}, Lcom/google/android/search/shared/ui/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 51
    const/4 v5, -0x2

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 52
    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    :cond_0
    const v0, 0x7f1101a9

    invoke-virtual {v3}, Lanc;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 56
    iget-object v0, v3, Lanc;->agd:Lapp;

    if-eqz v0, :cond_3

    .line 58
    iget-object v0, v3, Lanc;->agd:Lapp;

    .line 59
    const v4, 0x7f11036e

    invoke-virtual {v0}, Lapp;->th()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 60
    const v4, 0x7f11036a

    invoke-virtual {v0}, Lapp;->te()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 61
    const v4, 0x7f11036b

    invoke-virtual {v0}, Lapp;->tf()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v0}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 68
    :goto_0
    const v0, 0x7f1100b0

    invoke-virtual {v3}, Lanc;->oo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 72
    invoke-virtual {v3}, Lanc;->op()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    const v0, 0x7f1100b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 77
    iget-object v4, p0, Lftr;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0201cd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 78
    const/16 v5, 0xa8

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 79
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 80
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    int-to-double v6, v5

    mul-double/2addr v6, v12

    double-to-int v5, v6

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v12

    double-to-int v6, v6

    invoke-virtual {v4, v8, v8, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 82
    new-instance v5, Landroid/graphics/drawable/ScaleDrawable;

    invoke-direct {v5, v4, v8, v11, v11}, Landroid/graphics/drawable/ScaleDrawable;-><init>(Landroid/graphics/drawable/Drawable;IFF)V

    .line 83
    invoke-static {v0, v5, v10, v10, v10}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 87
    :cond_1
    iget-object v0, v2, Lanh;->aho:Lanc;

    invoke-virtual {v0}, Lanc;->os()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    invoke-virtual {p0, v9}, Lftr;->jy(I)V

    .line 90
    :cond_2
    invoke-virtual {v3}, Lanc;->oj()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v9, v0}, Lftr;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lftr;->fA(Z)V

    .line 92
    return-void

    .line 62
    :cond_3
    invoke-virtual {v3}, Lanc;->om()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 63
    const v0, 0x7f110080

    invoke-virtual {v3}, Lanc;->ol()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 64
    const v0, 0x7f1102f0

    invoke-virtual {v3}, Lanc;->on()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_4
    const v0, 0x7f11036e

    invoke-virtual {v3}, Lanc;->on()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lftr;->b(Landroid/view/View;ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 35
    const v0, 0x7f040143

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
