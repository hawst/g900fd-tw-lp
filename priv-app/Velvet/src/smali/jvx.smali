.class public final Ljvx;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eIs:Ljvs;

.field private eIt:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 543
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 544
    iput v0, p0, Ljvx;->aez:I

    iput-object v1, p0, Ljvx;->eIs:Ljvs;

    iput v0, p0, Ljvx;->eIt:I

    iput-object v1, p0, Ljvx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvx;->eCz:I

    .line 545
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 502
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljvx;->eIs:Ljvs;

    if-nez v0, :cond_1

    new-instance v0, Ljvs;

    invoke-direct {v0}, Ljvs;-><init>()V

    iput-object v0, p0, Ljvx;->eIs:Ljvs;

    :cond_1
    iget-object v0, p0, Ljvx;->eIs:Ljvs;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljvx;->eIt:I

    iget v0, p0, Ljvx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvx;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 559
    iget-object v0, p0, Ljvx;->eIs:Ljvs;

    if-eqz v0, :cond_0

    .line 560
    const/4 v0, 0x1

    iget-object v1, p0, Ljvx;->eIs:Ljvs;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 562
    :cond_0
    iget v0, p0, Ljvx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 563
    const/4 v0, 0x2

    iget v1, p0, Ljvx;->eIt:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 565
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 566
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 570
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 571
    iget-object v1, p0, Ljvx;->eIs:Ljvs;

    if-eqz v1, :cond_0

    .line 572
    const/4 v1, 0x1

    iget-object v2, p0, Ljvx;->eIs:Ljvs;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_0
    iget v1, p0, Ljvx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 576
    const/4 v1, 0x2

    iget v2, p0, Ljvx;->eIt:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_1
    return v0
.end method
