.class public final Ljmf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ett:[Ljmf;


# instance fields
.field private aez:I

.field private dHr:Ljava/lang/String;

.field private etu:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5299
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5300
    const/4 v0, 0x0

    iput v0, p0, Ljmf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmf;->etu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmf;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljmf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmf;->eCz:I

    .line 5301
    return-void
.end method

.method public static bpE()[Ljmf;
    .locals 2

    .prologue
    .line 5242
    sget-object v0, Ljmf;->ett:[Ljmf;

    if-nez v0, :cond_1

    .line 5243
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 5245
    :try_start_0
    sget-object v0, Ljmf;->ett:[Ljmf;

    if-nez v0, :cond_0

    .line 5246
    const/4 v0, 0x0

    new-array v0, v0, [Ljmf;

    sput-object v0, Ljmf;->ett:[Ljmf;

    .line 5248
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5250
    :cond_1
    sget-object v0, Ljmf;->ett:[Ljmf;

    return-object v0

    .line 5248
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmf;->etu:Ljava/lang/String;

    iget v0, p0, Ljmf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmf;->dHr:Ljava/lang/String;

    iget v0, p0, Ljmf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5315
    iget v0, p0, Ljmf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5316
    const/4 v0, 0x1

    iget-object v1, p0, Ljmf;->etu:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5318
    :cond_0
    iget v0, p0, Ljmf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5319
    const/4 v0, 0x2

    iget-object v1, p0, Ljmf;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5321
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5322
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5326
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5327
    iget v1, p0, Ljmf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5328
    const/4 v1, 0x1

    iget-object v2, p0, Ljmf;->etu:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5331
    :cond_0
    iget v1, p0, Ljmf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5332
    const/4 v1, 0x2

    iget-object v2, p0, Ljmf;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5335
    :cond_1
    return v0
.end method
