.class public final Lhhk;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic dhf:Lcom/google/android/voicesearch/SendSmsActivity;

.field private dhh:I


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/SendSmsActivity;I)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lhhk;->dhf:Lcom/google/android/voicesearch/SendSmsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 82
    iput p2, p0, Lhhk;->dhh:I

    .line 83
    return-void
.end method

.method private declared-synchronized j(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhhk;->dhh:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhhk;->dhh:I

    .line 101
    iget-object v0, p0, Lhhk;->dhf:Lcom/google/android/voicesearch/SendSmsActivity;

    iget-object v0, v0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lidv;

    invoke-virtual {v0}, Lidv;->aSw()V

    .line 102
    iget v0, p0, Lhhk;->dhh:I

    if-gtz v0, :cond_0

    .line 106
    const-string v0, "com.google.android.voicesearch.extras.SMS_RECIPIENTS"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string v1, "com.google.android.voicesearch.extras.SMS_MESSAGE"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-static {p1, v1, v0}, Lhhm;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lhhk;->dhf:Lcom/google/android/voicesearch/SendSmsActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/SendSmsActivity;->aOP()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    monitor-exit p0

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 88
    const-string v0, "com.google.android.voicesearch.action.SMS_STATUS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {p0}, Lhhk;->getResultCode()I

    move-result v0

    .line 91
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 92
    iget-object v1, p0, Lhhk;->dhf:Lcom/google/android/voicesearch/SendSmsActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->a(Ljava/lang/Exception;I)V

    goto :goto_0

    .line 94
    :cond_1
    invoke-direct {p0, p1, p2}, Lhhk;->j(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
