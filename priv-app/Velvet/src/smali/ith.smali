.class public final Lith;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dHD:Ljsw;

.field public dHE:[Liue;

.field private dHF:Ljava/lang/String;

.field private dHG:Liuh;

.field private dHH:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5849
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5850
    iput v2, p0, Lith;->aez:I

    iput-object v1, p0, Lith;->dHD:Ljsw;

    invoke-static {}, Liue;->aZc()[Liue;

    move-result-object v0

    iput-object v0, p0, Lith;->dHE:[Liue;

    const-string v0, ""

    iput-object v0, p0, Lith;->dHF:Ljava/lang/String;

    iput-object v1, p0, Lith;->dHG:Liuh;

    iput v2, p0, Lith;->dHH:I

    iput-object v1, p0, Lith;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lith;->eCz:I

    .line 5851
    return-void
.end method

.method public static ac([B)Lith;
    .locals 1

    .prologue
    .line 5987
    new-instance v0, Lith;

    invoke-direct {v0}, Lith;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lith;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5780
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lith;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lith;->dHD:Ljsw;

    if-nez v0, :cond_1

    new-instance v0, Ljsw;

    invoke-direct {v0}, Ljsw;-><init>()V

    iput-object v0, p0, Lith;->dHD:Ljsw;

    :cond_1
    iget-object v0, p0, Lith;->dHD:Ljsw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lith;->dHE:[Liue;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liue;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lith;->dHE:[Liue;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Liue;

    invoke-direct {v3}, Liue;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lith;->dHE:[Liue;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Liue;

    invoke-direct {v3}, Liue;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lith;->dHE:[Liue;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lith;->dHF:Ljava/lang/String;

    iget v0, p0, Lith;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lith;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lith;->dHG:Liuh;

    if-nez v0, :cond_5

    new-instance v0, Liuh;

    invoke-direct {v0}, Liuh;-><init>()V

    iput-object v0, p0, Lith;->dHG:Liuh;

    :cond_5
    iget-object v0, p0, Lith;->dHG:Liuh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lith;->dHH:I

    iget v0, p0, Lith;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lith;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 5868
    iget-object v0, p0, Lith;->dHD:Ljsw;

    if-eqz v0, :cond_0

    .line 5869
    const/4 v0, 0x1

    iget-object v1, p0, Lith;->dHD:Ljsw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5871
    :cond_0
    iget-object v0, p0, Lith;->dHE:[Liue;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lith;->dHE:[Liue;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 5872
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lith;->dHE:[Liue;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 5873
    iget-object v1, p0, Lith;->dHE:[Liue;

    aget-object v1, v1, v0

    .line 5874
    if-eqz v1, :cond_1

    .line 5875
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 5872
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5879
    :cond_2
    iget v0, p0, Lith;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 5880
    const/4 v0, 0x3

    iget-object v1, p0, Lith;->dHF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5882
    :cond_3
    iget-object v0, p0, Lith;->dHG:Liuh;

    if-eqz v0, :cond_4

    .line 5883
    const/4 v0, 0x4

    iget-object v1, p0, Lith;->dHG:Liuh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5885
    :cond_4
    iget v0, p0, Lith;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 5886
    const/4 v0, 0x5

    iget v1, p0, Lith;->dHH:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5888
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5889
    return-void
.end method

.method public final aYE()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5808
    iget-object v0, p0, Lith;->dHF:Ljava/lang/String;

    return-object v0
.end method

.method public final aYF()I
    .locals 1

    .prologue
    .line 5833
    iget v0, p0, Lith;->dHH:I

    return v0
.end method

.method public final aYG()Z
    .locals 1

    .prologue
    .line 5841
    iget v0, p0, Lith;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 5893
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5894
    iget-object v1, p0, Lith;->dHD:Ljsw;

    if-eqz v1, :cond_0

    .line 5895
    const/4 v1, 0x1

    iget-object v2, p0, Lith;->dHD:Ljsw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5898
    :cond_0
    iget-object v1, p0, Lith;->dHE:[Liue;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lith;->dHE:[Liue;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 5899
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lith;->dHE:[Liue;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 5900
    iget-object v2, p0, Lith;->dHE:[Liue;

    aget-object v2, v2, v0

    .line 5901
    if-eqz v2, :cond_1

    .line 5902
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 5899
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 5907
    :cond_3
    iget v1, p0, Lith;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    .line 5908
    const/4 v1, 0x3

    iget-object v2, p0, Lith;->dHF:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5911
    :cond_4
    iget-object v1, p0, Lith;->dHG:Liuh;

    if-eqz v1, :cond_5

    .line 5912
    const/4 v1, 0x4

    iget-object v2, p0, Lith;->dHG:Liuh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5915
    :cond_5
    iget v1, p0, Lith;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 5916
    const/4 v1, 0x5

    iget v2, p0, Lith;->dHH:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5919
    :cond_6
    return v0
.end method

.method public final mr(I)Lith;
    .locals 1

    .prologue
    .line 5836
    iput p1, p0, Lith;->dHH:I

    .line 5837
    iget v0, p0, Lith;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lith;->aez:I

    .line 5838
    return-object p0
.end method

.method public final pH(Ljava/lang/String;)Lith;
    .locals 1

    .prologue
    .line 5811
    if-nez p1, :cond_0

    .line 5812
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5814
    :cond_0
    iput-object p1, p0, Lith;->dHF:Ljava/lang/String;

    .line 5815
    iget v0, p0, Lith;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lith;->aez:I

    .line 5816
    return-object p0
.end method
