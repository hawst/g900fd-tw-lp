.class public final Ljbj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dYs:[Ljbj;


# instance fields
.field private aez:I

.field private afz:I

.field private dLZ:[B

.field private dYA:I

.field private dYB:Z

.field public dYC:Ljbk;

.field public dYD:[J

.field private dYt:I

.field private dYu:I

.field private dYv:I

.field private dYw:[Ljbl;

.field public dYx:[I

.field private dYy:Z

.field private dYz:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33104
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 33105
    iput v1, p0, Ljbj;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljbj;->afz:I

    iput v1, p0, Ljbj;->dYt:I

    iput v1, p0, Ljbj;->dYu:I

    iput v1, p0, Ljbj;->dYv:I

    invoke-static {}, Ljbl;->beX()[Ljbl;

    move-result-object v0

    iput-object v0, p0, Ljbj;->dYw:[Ljbl;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljbj;->dYx:[I

    iput-boolean v1, p0, Ljbj;->dYy:Z

    iput-boolean v1, p0, Ljbj;->dYz:Z

    iput v1, p0, Ljbj;->dYA:I

    iput-boolean v1, p0, Ljbj;->dYB:Z

    iput-object v2, p0, Ljbj;->dYC:Ljbk;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljbj;->dLZ:[B

    sget-object v0, Ljsu;->eCB:[J

    iput-object v0, p0, Ljbj;->dYD:[J

    iput-object v2, p0, Ljbj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbj;->eCz:I

    .line 33106
    return-void
.end method

.method public static beV()[Ljbj;
    .locals 2

    .prologue
    .line 32905
    sget-object v0, Ljbj;->dYs:[Ljbj;

    if-nez v0, :cond_1

    .line 32906
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 32908
    :try_start_0
    sget-object v0, Ljbj;->dYs:[Ljbj;

    if-nez v0, :cond_0

    .line 32909
    const/4 v0, 0x0

    new-array v0, v0, [Ljbj;

    sput-object v0, Ljbj;->dYs:[Ljbj;

    .line 32911
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32913
    :cond_1
    sget-object v0, Ljbj;->dYs:[Ljbj;

    return-object v0

    .line 32911
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 32548
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljbj;->afz:I

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbj;->dYt:I

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbj;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbj;->dYu:I

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbj;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbj;->dYv:I

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbj;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbj;->dYw:[Ljbl;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbl;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljbj;->dYw:[Ljbl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljbl;

    invoke-direct {v3}, Ljbl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljbj;->dYw:[Ljbl;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljbl;

    invoke-direct {v3}, Ljbl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbj;->dYw:[Ljbl;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_5

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    :pswitch_2
    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_3
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_5
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljbj;->dYx:[I

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    if-nez v0, :cond_7

    array-length v3, v5

    if-ne v2, v3, :cond_7

    iput-object v5, p0, Ljbj;->dYx:[I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Ljbj;->dYx:[I

    array-length v0, v0

    goto :goto_5

    :cond_7
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_8

    iget-object v4, p0, Ljbj;->dYx:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljbj;->dYx:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    if-eqz v0, :cond_d

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljbj;->dYx:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_a

    iget-object v0, p0, Ljbj;->dYx:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_3

    :pswitch_6
    goto :goto_8

    :pswitch_7
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_b
    iget-object v2, p0, Ljbj;->dYx:[I

    array-length v2, v2

    goto :goto_7

    :cond_c
    iput-object v4, p0, Ljbj;->dYx:[I

    :cond_d
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbj;->dYA:I

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljbj;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbj;->dYy:Z

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbj;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbj;->dYB:Z

    iget v0, p0, Ljbj;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbj;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljbj;->dLZ:[B

    iget v0, p0, Ljbj;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljbj;->aez:I

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ljbj;->dYC:Ljbk;

    if-nez v0, :cond_e

    new-instance v0, Ljbk;

    invoke-direct {v0}, Ljbk;-><init>()V

    iput-object v0, p0, Ljbj;->dYC:Ljbk;

    :cond_e
    iget-object v0, p0, Ljbj;->dYC:Ljbk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbj;->dYz:Z

    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbj;->aez:I

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x71

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbj;->dYD:[J

    if-nez v0, :cond_10

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_f

    iget-object v3, p0, Ljbj;->dYD:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_11

    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_10
    iget-object v0, p0, Ljbj;->dYD:[J

    array-length v0, v0

    goto :goto_9

    :cond_11
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Ljbj;->dYD:[J

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Ljbj;->dYD:[J

    if-nez v0, :cond_13

    move v0, v1

    :goto_b
    add-int/2addr v3, v0

    new-array v3, v3, [J

    if-eqz v0, :cond_12

    iget-object v4, p0, Ljbj;->dYD:[J

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    :goto_c
    array-length v4, v3

    if-ge v0, v4, :cond_14

    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_13
    iget-object v0, p0, Ljbj;->dYD:[J

    array-length v0, v0

    goto :goto_b

    :cond_14
    iput-object v3, p0, Ljbj;->dYD:[J

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
        0x38 -> :sswitch_8
        0x40 -> :sswitch_9
        0x48 -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x60 -> :sswitch_d
        0x71 -> :sswitch_e
        0x72 -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 33131
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 33132
    const/4 v0, 0x1

    iget v2, p0, Ljbj;->afz:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 33134
    :cond_0
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 33135
    const/4 v0, 0x2

    iget v2, p0, Ljbj;->dYt:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 33137
    :cond_1
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 33138
    const/4 v0, 0x3

    iget v2, p0, Ljbj;->dYu:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 33140
    :cond_2
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 33141
    const/4 v0, 0x4

    iget v2, p0, Ljbj;->dYv:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 33143
    :cond_3
    iget-object v0, p0, Ljbj;->dYw:[Ljbl;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljbj;->dYw:[Ljbl;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 33144
    :goto_0
    iget-object v2, p0, Ljbj;->dYw:[Ljbl;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 33145
    iget-object v2, p0, Ljbj;->dYw:[Ljbl;

    aget-object v2, v2, v0

    .line 33146
    if-eqz v2, :cond_4

    .line 33147
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 33144
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33151
    :cond_5
    iget-object v0, p0, Ljbj;->dYx:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljbj;->dYx:[I

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 33152
    :goto_1
    iget-object v2, p0, Ljbj;->dYx:[I

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 33153
    const/4 v2, 0x6

    iget-object v3, p0, Ljbj;->dYx:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 33152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 33156
    :cond_6
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 33157
    const/4 v0, 0x7

    iget v2, p0, Ljbj;->dYA:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 33159
    :cond_7
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    .line 33160
    const/16 v0, 0x8

    iget-boolean v2, p0, Ljbj;->dYy:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 33162
    :cond_8
    iget v0, p0, Ljbj;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_9

    .line 33163
    const/16 v0, 0x9

    iget-boolean v2, p0, Ljbj;->dYB:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 33165
    :cond_9
    iget v0, p0, Ljbj;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_a

    .line 33166
    const/16 v0, 0xa

    iget-object v2, p0, Ljbj;->dLZ:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    .line 33168
    :cond_a
    iget-object v0, p0, Ljbj;->dYC:Ljbk;

    if-eqz v0, :cond_b

    .line 33169
    const/16 v0, 0xb

    iget-object v2, p0, Ljbj;->dYC:Ljbk;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 33171
    :cond_b
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_c

    .line 33172
    const/16 v0, 0xc

    iget-boolean v2, p0, Ljbj;->dYz:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 33174
    :cond_c
    iget-object v0, p0, Ljbj;->dYD:[J

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljbj;->dYD:[J

    array-length v0, v0

    if-lez v0, :cond_d

    .line 33175
    :goto_2
    iget-object v0, p0, Ljbj;->dYD:[J

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 33176
    const/16 v0, 0xe

    iget-object v2, p0, Ljbj;->dYD:[J

    aget-wide v2, v2, v1

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 33175
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 33179
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 33180
    return-void
.end method

.method public final hB(Z)Ljbj;
    .locals 1

    .prologue
    .line 33025
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbj;->dYz:Z

    .line 33026
    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbj;->aez:I

    .line 33027
    return-object p0
.end method

.method public final hC(Z)Ljbj;
    .locals 1

    .prologue
    .line 33063
    iput-boolean p1, p0, Ljbj;->dYB:Z

    .line 33064
    iget v0, p0, Ljbj;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbj;->aez:I

    .line 33065
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 33184
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 33185
    iget v2, p0, Ljbj;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 33186
    const/4 v2, 0x1

    iget v3, p0, Ljbj;->afz:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 33189
    :cond_0
    iget v2, p0, Ljbj;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 33190
    const/4 v2, 0x2

    iget v3, p0, Ljbj;->dYt:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 33193
    :cond_1
    iget v2, p0, Ljbj;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 33194
    const/4 v2, 0x3

    iget v3, p0, Ljbj;->dYu:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 33197
    :cond_2
    iget v2, p0, Ljbj;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 33198
    const/4 v2, 0x4

    iget v3, p0, Ljbj;->dYv:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 33201
    :cond_3
    iget-object v2, p0, Ljbj;->dYw:[Ljbl;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljbj;->dYw:[Ljbl;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 33202
    :goto_0
    iget-object v3, p0, Ljbj;->dYw:[Ljbl;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 33203
    iget-object v3, p0, Ljbj;->dYw:[Ljbl;

    aget-object v3, v3, v0

    .line 33204
    if-eqz v3, :cond_4

    .line 33205
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 33202
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 33210
    :cond_6
    iget-object v2, p0, Ljbj;->dYx:[I

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljbj;->dYx:[I

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    .line 33212
    :goto_1
    iget-object v3, p0, Ljbj;->dYx:[I

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 33213
    iget-object v3, p0, Ljbj;->dYx:[I

    aget v3, v3, v1

    .line 33214
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 33212
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 33217
    :cond_7
    add-int/2addr v0, v2

    .line 33218
    iget-object v1, p0, Ljbj;->dYx:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33220
    :cond_8
    iget v1, p0, Ljbj;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 33221
    const/4 v1, 0x7

    iget v2, p0, Ljbj;->dYA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 33224
    :cond_9
    iget v1, p0, Ljbj;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 33225
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljbj;->dYy:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33228
    :cond_a
    iget v1, p0, Ljbj;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_b

    .line 33229
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljbj;->dYB:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33232
    :cond_b
    iget v1, p0, Ljbj;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_c

    .line 33233
    const/16 v1, 0xa

    iget-object v2, p0, Ljbj;->dLZ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 33236
    :cond_c
    iget-object v1, p0, Ljbj;->dYC:Ljbk;

    if-eqz v1, :cond_d

    .line 33237
    const/16 v1, 0xb

    iget-object v2, p0, Ljbj;->dYC:Ljbk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33240
    :cond_d
    iget v1, p0, Ljbj;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_e

    .line 33241
    const/16 v1, 0xc

    iget-boolean v2, p0, Ljbj;->dYz:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33244
    :cond_e
    iget-object v1, p0, Ljbj;->dYD:[J

    if-eqz v1, :cond_f

    iget-object v1, p0, Ljbj;->dYD:[J

    array-length v1, v1

    if-lez v1, :cond_f

    .line 33245
    iget-object v1, p0, Ljbj;->dYD:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 33246
    add-int/2addr v0, v1

    .line 33247
    iget-object v1, p0, Ljbj;->dYD:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 33249
    :cond_f
    return v0
.end method

.method public final nK()I
    .locals 1

    .prologue
    .line 32921
    iget v0, p0, Ljbj;->afz:I

    return v0
.end method

.method public final nL()Z
    .locals 1

    .prologue
    .line 32929
    iget v0, p0, Ljbj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final om(I)Ljbj;
    .locals 1

    .prologue
    .line 32924
    iput p1, p0, Ljbj;->afz:I

    .line 32925
    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbj;->aez:I

    .line 32926
    return-object p0
.end method

.method public final on(I)Ljbj;
    .locals 1

    .prologue
    .line 33044
    const/4 v0, 0x1

    iput v0, p0, Ljbj;->dYA:I

    .line 33045
    iget v0, p0, Ljbj;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljbj;->aez:I

    .line 33046
    return-object p0
.end method
