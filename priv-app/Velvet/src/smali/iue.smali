.class public final Liue;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dJP:[Liue;


# instance fields
.field private aez:I

.field private dJQ:Ljava/lang/String;

.field private dJR:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6060
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6061
    const/4 v0, 0x0

    iput v0, p0, Liue;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liue;->dJQ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liue;->dJR:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Liue;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liue;->eCz:I

    .line 6062
    return-void
.end method

.method public static aZc()[Liue;
    .locals 2

    .prologue
    .line 6003
    sget-object v0, Liue;->dJP:[Liue;

    if-nez v0, :cond_1

    .line 6004
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 6006
    :try_start_0
    sget-object v0, Liue;->dJP:[Liue;

    if-nez v0, :cond_0

    .line 6007
    const/4 v0, 0x0

    new-array v0, v0, [Liue;

    sput-object v0, Liue;->dJP:[Liue;

    .line 6009
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6011
    :cond_1
    sget-object v0, Liue;->dJP:[Liue;

    return-object v0

    .line 6009
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5997
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liue;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liue;->dJQ:Ljava/lang/String;

    iget v0, p0, Liue;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liue;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liue;->dJR:Ljava/lang/String;

    iget v0, p0, Liue;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liue;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6076
    iget v0, p0, Liue;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6077
    const/4 v0, 0x1

    iget-object v1, p0, Liue;->dJQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6079
    :cond_0
    iget v0, p0, Liue;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6080
    const/4 v0, 0x2

    iget-object v1, p0, Liue;->dJR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6082
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6083
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6087
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6088
    iget v1, p0, Liue;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6089
    const/4 v1, 0x1

    iget-object v2, p0, Liue;->dJQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6092
    :cond_0
    iget v1, p0, Liue;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6093
    const/4 v1, 0x2

    iget-object v2, p0, Liue;->dJR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6096
    :cond_1
    return v0
.end method

.method public final pU(Ljava/lang/String;)Liue;
    .locals 1

    .prologue
    .line 6022
    if-nez p1, :cond_0

    .line 6023
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6025
    :cond_0
    iput-object p1, p0, Liue;->dJQ:Ljava/lang/String;

    .line 6026
    iget v0, p0, Liue;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liue;->aez:I

    .line 6027
    return-object p0
.end method

.method public final pV(Ljava/lang/String;)Liue;
    .locals 1

    .prologue
    .line 6044
    if-nez p1, :cond_0

    .line 6045
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6047
    :cond_0
    iput-object p1, p0, Liue;->dJR:Ljava/lang/String;

    .line 6048
    iget v0, p0, Liue;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liue;->aez:I

    .line 6049
    return-object p0
.end method
