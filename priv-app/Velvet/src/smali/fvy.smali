.class public final Lfvy;
.super Lfuz;
.source "PG"


# instance fields
.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 46
    iput-object p4, p0, Lfvy;->mTimeToLeaveFactory:Lfyk;

    .line 47
    iput-object p5, p0, Lfvy;->mStringEvaluator:Lgbr;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x0

    const/4 v7, 0x1

    .line 52
    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    .line 53
    iget-object v8, v3, Lizj;->dSJ:Ljbz;

    .line 55
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 58
    iget-object v0, v8, Ljbz;->dMF:[Liyg;

    array-length v0, v0

    if-ne v0, v7, :cond_e

    .line 59
    new-instance v0, Lgca;

    iget-object v1, v8, Ljbz;->dMF:[Liyg;

    aget-object v1, v1, v12

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    invoke-direct {v0, v1, v2}, Lgca;-><init>(Liyg;Lemp;)V

    .line 60
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 62
    new-instance v1, Lfzb;

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfvy;->mStringEvaluator:Lgbr;

    invoke-direct {v1, v2, v0, v4}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    .line 64
    invoke-virtual {p0}, Lfvy;->aAy()Lizj;

    move-result-object v0

    iget-object v2, v8, Ljbz;->dZO:Ljbp;

    invoke-virtual {v1, p1, v0, p2, v2}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;

    move-result-object v0

    .line 66
    invoke-static {v9, v0}, Lfvy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 69
    :goto_0
    iget-object v1, v8, Ljbz;->dMF:[Liyg;

    array-length v1, v1

    if-eqz v1, :cond_0

    iget-object v1, v3, Lizj;->dUv:Ljhk;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lfvy;->mTimeToLeaveFactory:Lfyk;

    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v4, v8, Ljbz;->dMF:[Liyg;

    iget-object v5, v8, Ljbz;->dZO:Ljbp;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;)Lanh;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_0
    new-instance v1, Laob;

    invoke-direct {v1}, Laob;-><init>()V

    iget-object v2, v8, Ljbz;->dZN:Ljbw;

    invoke-virtual {v2}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laob;->bo(Ljava/lang/String;)Laob;

    iget-object v0, v2, Ljbw;->aiX:Ljcn;

    iput-object v0, v1, Laob;->aiX:Ljcn;

    invoke-virtual {v8}, Ljbz;->bfS()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Ljbz;->bfR()J

    move-result-wide v4

    const-wide/16 v10, 0x3e8

    mul-long/2addr v4, v10

    const v0, 0x7f0a0363

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p1, v4, v5, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v12

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, " \u00b7 "

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/CharSequence;

    aput-object v0, v10, v12

    const v0, 0x8001a

    invoke-static {p1, v4, v5, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v7

    invoke-static {v3, v10}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laob;->br(Ljava/lang/String;)Laob;

    :cond_1
    iget-object v0, v8, Ljbz;->dZN:Ljbw;

    const/4 v3, 0x3

    invoke-static {v3}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0}, Ljbw;->bfI()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Ljbw;->bfI()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v0}, Ljbw;->bfJ()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v0}, Ljbw;->bfJ()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_c

    move-object v0, v6

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v1, v0}, Laob;->bp(Ljava/lang/String;)Laob;

    :cond_4
    invoke-virtual {v8}, Ljbz;->bfD()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v8}, Ljbz;->bfC()I

    move-result v0

    :goto_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100035

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v12

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laob;->bq(Ljava/lang/String;)Laob;

    invoke-virtual {v2}, Ljbw;->bfL()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lfsa;

    const/16 v3, 0x4a

    invoke-direct {v0, v3}, Lfsa;-><init>(I)V

    invoke-virtual {v2}, Ljbw;->bfK()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v7}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v0

    iput-object v0, v1, Laob;->aiY:Lani;

    :cond_5
    iget-object v0, v2, Ljbw;->dZG:[Ljdq;

    if-eqz v0, :cond_6

    iget-object v0, v2, Ljbw;->dZG:[Ljdq;

    array-length v0, v0

    if-lez v0, :cond_6

    iget-object v0, v2, Ljbw;->dZG:[Ljdq;

    aget-object v0, v0, v12

    const/16 v3, 0x89

    invoke-static {p1, v0, v3}, Lgbh;->a(Landroid/content/Context;Ljdq;I)Laok;

    move-result-object v0

    iput-object v0, v1, Laob;->ahN:Laok;

    :cond_6
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v3, 0x31

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    iput-object v1, v0, Lanh;->ahm:Laob;

    iget-object v1, p0, Lfvy;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    invoke-virtual {v2}, Ljbw;->bfN()Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lfsa;

    const/16 v3, 0x8a

    invoke-direct {v1, v3}, Lfsa;-><init>(I)V

    invoke-virtual {v2}, Ljbw;->bfM()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v7}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    :cond_7
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, v8, Ljbz;->dMM:Ljcn;

    if-eqz v0, :cond_8

    iget-object v0, v8, Ljbz;->dMM:Ljcn;

    invoke-virtual {v0}, Ljcn;->qG()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 79
    new-instance v0, Lfrt;

    iget-object v1, v8, Ljbz;->dMM:Ljcn;

    invoke-virtual {v1}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lfrt;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljbz;->baL()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lfrt;->cBo:Ljava/lang/String;

    invoke-virtual {v0}, Lfrt;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_8
    iget-object v0, v8, Ljbz;->dZO:Ljbp;

    if-eqz v0, :cond_a

    .line 85
    iget-object v0, v8, Ljbz;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_9

    iget-object v0, v8, Ljbz;->dMF:[Liyg;

    aget-object v6, v0, v12

    .line 86
    :cond_9
    new-instance v0, Lfvv;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    const/16 v2, 0xba

    iget-object v3, v8, Ljbz;->dZO:Ljbp;

    invoke-direct {v0, v1, v2, v6, v3}, Lfvv;-><init>(Lizj;ILiyg;Ljbp;)V

    iput-boolean v12, v0, Lfvv;->cCH:Z

    invoke-virtual {v0}, Lfvv;->aDh()Lanh;

    move-result-object v0

    .line 91
    invoke-static {v9, v0}, Lfvy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 94
    :cond_a
    iget-object v0, v8, Ljbz;->dOz:Ljbg;

    if-eqz v0, :cond_b

    iget-object v0, v8, Ljbz;->dOz:Ljbg;

    invoke-virtual {v0}, Ljbg;->beR()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 95
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v8, Ljbz;->dOz:Ljbg;

    const/16 v2, 0x48

    invoke-static {p1, v0, v1, v2}, Lfsx;->a(Landroid/content/Context;Lizj;Ljbg;I)Lanh;

    move-result-object v0

    .line 97
    invoke-static {v9, v0}, Lfvy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 100
    :cond_b
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 101
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 102
    invoke-virtual {v1, v7}, Lang;->aS(Z)Lang;

    .line 103
    return-object v1

    .line 75
    :cond_c
    const-string v0, " \u00b7 "

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_d
    move v0, v7

    goto/16 :goto_2

    :cond_e
    move-object v0, v6

    goto/16 :goto_0
.end method
