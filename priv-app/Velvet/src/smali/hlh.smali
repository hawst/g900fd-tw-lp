.class final Lhlh;
.super Lgly;
.source "PG"


# instance fields
.field private aZw:Lhli;

.field private final bEC:Lgnp;

.field private synthetic djV:Lhlf;

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Lhlf;Lhli;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 432
    iput-object p1, p0, Lhlh;->djV:Lhlf;

    invoke-direct {p0}, Lgly;-><init>()V

    .line 426
    new-instance v0, Lgnp;

    invoke-direct {v0}, Lgnp;-><init>()V

    iput-object v0, p0, Lhlh;->bEC:Lgnp;

    .line 433
    iput-object p2, p0, Lhlh;->aZw:Lhli;

    .line 434
    iput-object p3, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    .line 435
    return-void
.end method

.method private a(Leil;)V
    .locals 4

    .prologue
    .line 714
    const-string v0, "no_match"

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 715
    iget-object v0, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lege;->a(Lefr;JJ)V

    .line 716
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    iget-object v1, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lhli;->a(Leil;Ljava/lang/String;)V

    .line 717
    return-void
.end method

.method private b(Ljvw;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 594
    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->a(Ljvw;)Lijj;

    move-result-object v4

    .line 608
    invoke-virtual {v4}, Lijj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v4, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-virtual {v0}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 614
    :goto_0
    iget-object v3, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lcgg;->aVu:Lcgg;

    invoke-virtual {v3}, Lcgg;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v2

    .line 616
    :goto_1
    if-nez v0, :cond_2

    iget-object v5, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v5}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v3

    if-nez v3, :cond_2

    .line 620
    iget-object v3, p0, Lhlh;->djV:Lhlf;

    iget-object v3, v3, Lhlf;->mVss:Lhhq;

    invoke-virtual {v3}, Lhhq;->aOZ()Lhik;

    move-result-object v3

    invoke-virtual {v3}, Lhik;->aPD()V

    .line 627
    :cond_2
    if-eqz v0, :cond_6

    .line 628
    const-string v0, "VoiceSearchController"

    const-string v3, "Empty combined result"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 632
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v3, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    :cond_3
    invoke-virtual {v0, v1, v2}, Lhlf;->J(ZZ)V

    .line 633
    new-instance v0, Lein;

    invoke-direct {v0}, Lein;-><init>()V

    invoke-direct {p0, v0}, Lhlh;->a(Leil;)V

    .line 670
    :goto_2
    return-void

    :cond_4
    move v0, v1

    .line 608
    goto :goto_0

    :cond_5
    move v3, v1

    .line 614
    goto :goto_1

    .line 635
    :cond_6
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HE()Z

    move-result v3

    .line 636
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPf()Lgel;

    move-result-object v5

    .line 640
    if-eqz v3, :cond_7

    invoke-virtual {v4, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-virtual {v0}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannedString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannedString;

    move-result-object v0

    .line 644
    :goto_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 645
    invoke-static {v0, p2}, Leqt;->b(Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/text/SpannedString;

    move-result-object v0

    move-object v7, v0

    .line 648
    :goto_4
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0, v7}, Lhli;->n(Ljava/lang/CharSequence;)V

    .line 650
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v1

    .line 652
    :goto_5
    invoke-virtual {v4}, Lijj;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 653
    invoke-virtual {v4, v2}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    .line 654
    invoke-virtual {v0}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 652
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 640
    :cond_7
    iget-object v0, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-virtual {v5, v6, v0}, Lgel;->a(Ljava/lang/String;Lcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannedString;

    move-result-object v0

    goto :goto_3

    .line 656
    :cond_8
    iget-object v0, v1, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    .line 659
    iget-object v1, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 660
    iget-object v1, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v7, v0, v3}, Lcom/google/android/shared/search/Query;->a(Ljava/lang/CharSequence;Lijj;Z)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 662
    iget-object v2, p0, Lhlh;->djV:Lhlf;

    iget-object v3, p0, Lhlh;->djV:Lhlf;

    invoke-virtual {v3, v1}, Lhlf;->bb(Lcom/google/android/shared/search/Query;)Lcmk;

    move-result-object v3

    iput-object v3, v2, Lhlf;->djc:Lcmk;

    .line 663
    iget-object v2, p0, Lhlh;->djV:Lhlf;

    iget-object v2, v2, Lhlf;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lhlh;->djV:Lhlf;

    iget-object v4, v4, Lhlf;->djc:Lcmk;

    iget-object v5, p0, Lhlh;->djV:Lhlf;

    iget-object v5, v5, Lhlf;->mVss:Lhhq;

    iget-object v5, v5, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->aus()Leqo;

    move-result-object v5

    iget-object v6, p0, Lhlh;->djV:Lhlf;

    iget-object v6, v6, Lhlf;->mGsaConfigFlags:Lchk;

    invoke-virtual {v6}, Lchk;->HS()I

    move-result v6

    invoke-static/range {v1 .. v6}, Lcmq;->a(Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)Lcmq;

    move-result-object v1

    .line 666
    invoke-virtual {v1}, Lcmq;->Qq()V

    .line 668
    iget-object v2, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v2, v7, v0, v1}, Lhli;->a(Ljava/lang/CharSequence;Lijj;Lcmq;)V

    goto/16 :goto_2

    :cond_9
    move-object v7, v0

    goto :goto_4
.end method


# virtual methods
.method public final CZ()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 519
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 521
    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    new-instance v0, Leio;

    invoke-direct {v0}, Leio;-><init>()V

    invoke-direct {p0, v0}, Lhlh;->a(Leil;)V

    .line 526
    :cond_0
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    invoke-virtual {v0}, Lcmk;->Qe()Z

    move-result v0

    if-nez v0, :cond_1

    .line 527
    const-string v0, "VoiceSearchController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Incomplete proxy task: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhlh;->djV:Lhlf;

    iget-object v3, v3, Lhlf;->djc:Lcmk;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 528
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    invoke-virtual {v0}, Lcmk;->cancel()V

    .line 529
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    const/4 v2, 0x0

    iput-object v2, v0, Lhlf;->djc:Lcmk;

    .line 531
    :cond_1
    const-string v0, "VOICE_SEARCH_COMPLETE"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 532
    iget-object v2, p0, Lhlh;->djV:Lhlf;

    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Lhlf;->J(ZZ)V

    .line 533
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->CZ()V

    .line 534
    return-void

    :cond_2
    move v0, v1

    .line 532
    goto :goto_0
.end method

.method public final Nr()V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 442
    const-string v0, "SPEAK_NOW"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 443
    const/4 v0, 0x5

    invoke-static {v0}, Lege;->ht(I)V

    .line 445
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->Nr()V

    .line 446
    return-void
.end method

.method public final Ns()V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 461
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->Ns()V

    .line 462
    return-void
.end method

.method public final Nv()V
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 489
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->Nv()V

    .line 490
    return-void
.end method

.method public final Nw()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 475
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 479
    iget-object v2, p0, Lhlh;->djV:Lhlf;

    iget-object v0, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0, v1}, Lhlf;->J(ZZ)V

    .line 480
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->Nw()V

    .line 481
    const-string v0, "VOICE_SEARCH_COMPLETE"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 482
    return-void

    .line 479
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljps;)V
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 543
    invoke-static {p1}, Lico;->b(Ljps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 546
    const-string v0, "VoiceSearchController"

    const-string v1, "Unexpected majel response in stream."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 548
    :cond_0
    return-void
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 562
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 564
    invoke-static {p1}, Lgnm;->f(Ljvv;)V

    .line 566
    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 567
    const-string v0, "VoiceSearchController"

    const-string v1, "Result after completed recognition."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 571
    :cond_1
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-nez v0, :cond_2

    .line 577
    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->g(Ljvv;)Landroid/util/Pair;

    move-result-object v1

    .line 579
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 580
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 583
    iget-object v2, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v2, v0, v1}, Lhli;->x(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 584
    :cond_2
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 586
    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    invoke-direct {p0, v0, p2}, Lhlh;->b(Ljvw;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljxb;)V
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 692
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    iget-object v1, p1, Ljxb;->eKb:Ljrp;

    invoke-interface {v0, v1}, Lhli;->a(Ljrp;)V

    .line 693
    return-void
.end method

.method public final aEM()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 698
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 700
    iget-object v2, p0, Lhlh;->djV:Lhlf;

    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0, v1}, Lhlf;->J(ZZ)V

    .line 701
    return-void

    .line 700
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final au(J)V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 453
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->Nu()V

    .line 454
    return-void
.end method

.method public final b(Ljwp;)V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 677
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Iz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p1, Ljwp;->eJs:Ljvw;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lhlh;->b(Ljvw;Ljava/lang/String;)V

    .line 683
    :cond_0
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    if-eqz v0, :cond_1

    .line 684
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    invoke-virtual {v0, p1}, Lcmk;->a(Ljwp;)V

    .line 686
    :cond_1
    return-void
.end method

.method public final c(Leiq;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 495
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 497
    instance-of v0, p1, Lehz;

    if-eqz v0, :cond_3

    const-string v0, "VoiceSearchController"

    const-string v1, "No recognizers available."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 498
    :goto_0
    invoke-virtual {p1}, Leiq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 501
    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    invoke-virtual {v0}, Lhik;->aPH()V

    .line 504
    :cond_0
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    invoke-virtual {v0, p1}, Lcmk;->d(Leiq;)V

    .line 508
    :cond_1
    iget-object v0, p0, Lhlh;->bEC:Lgnp;

    iget-object v0, v0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 509
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 510
    const-string v1, "VoiceSearchController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got error after recognizing ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 512
    :cond_2
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    invoke-virtual {v0, v4, v4}, Lhlf;->J(ZZ)V

    .line 513
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    iget-object v1, p0, Lhlh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lhli;->a(Leiq;Ljava/lang/String;)V

    .line 514
    return-void

    .line 497
    :cond_3
    const-string v0, "VoiceSearchController"

    const-string v1, "onError"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d(Ljyl;)V
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 708
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->djc:Lcmk;

    invoke-virtual {v0, p1}, Lcmk;->a(Ljyl;)V

    .line 711
    :cond_0
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 469
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0}, Lhli;->Nt()V

    .line 470
    return-void
.end method

.method public final v([B)V
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lhlh;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 555
    iget-object v0, p0, Lhlh;->aZw:Lhli;

    invoke-interface {v0, p1}, Lhli;->p([B)V

    .line 556
    return-void
.end method
