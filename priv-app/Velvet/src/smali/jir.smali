.class public final Ljir;
.super Ljsl;
.source "PG"


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private ejU:Lixb;

.field private enK:I

.field private enL:Ljava/lang/String;

.field public enM:Ljis;

.field public enN:[Ljis;

.field private enO:[Ljis;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42510
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 42511
    const/4 v0, 0x0

    iput v0, p0, Ljir;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljir;->enK:I

    iput-object v1, p0, Ljir;->aeB:Ljbp;

    const-string v0, ""

    iput-object v0, p0, Ljir;->enL:Ljava/lang/String;

    iput-object v1, p0, Ljir;->enM:Ljis;

    invoke-static {}, Ljis;->bnr()[Ljis;

    move-result-object v0

    iput-object v0, p0, Ljir;->enN:[Ljis;

    invoke-static {}, Ljis;->bnr()[Ljis;

    move-result-object v0

    iput-object v0, p0, Ljir;->enO:[Ljis;

    iput-object v1, p0, Ljir;->ejU:Lixb;

    iput-object v1, p0, Ljir;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljir;->eCz:I

    .line 42512
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 42002
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljir;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljir;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljir;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljir;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljir;->enN:[Ljis;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljis;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljir;->enN:[Ljis;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljis;

    invoke-direct {v3}, Ljis;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljir;->enN:[Ljis;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljis;

    invoke-direct {v3}, Ljis;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljir;->enN:[Ljis;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljir;->enK:I

    iget v0, p0, Ljir;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljir;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljir;->enL:Ljava/lang/String;

    iget v0, p0, Ljir;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljir;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljir;->ejU:Lixb;

    if-nez v0, :cond_5

    new-instance v0, Lixb;

    invoke-direct {v0}, Lixb;-><init>()V

    iput-object v0, p0, Ljir;->ejU:Lixb;

    :cond_5
    iget-object v0, p0, Ljir;->ejU:Lixb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljir;->enO:[Ljis;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljis;

    if-eqz v0, :cond_6

    iget-object v3, p0, Ljir;->enO:[Ljis;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Ljis;

    invoke-direct {v3}, Ljis;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ljir;->enO:[Ljis;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Ljis;

    invoke-direct {v3}, Ljis;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljir;->enO:[Ljis;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljir;->enM:Ljis;

    if-nez v0, :cond_9

    new-instance v0, Ljis;

    invoke-direct {v0}, Ljis;-><init>()V

    iput-object v0, p0, Ljir;->enM:Ljis;

    :cond_9
    iget-object v0, p0, Ljir;->enM:Ljis;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 42531
    iget-object v0, p0, Ljir;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 42532
    const/4 v0, 0x1

    iget-object v2, p0, Ljir;->aeB:Ljbp;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 42534
    :cond_0
    iget-object v0, p0, Ljir;->enN:[Ljis;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljir;->enN:[Ljis;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 42535
    :goto_0
    iget-object v2, p0, Ljir;->enN:[Ljis;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 42536
    iget-object v2, p0, Ljir;->enN:[Ljis;

    aget-object v2, v2, v0

    .line 42537
    if-eqz v2, :cond_1

    .line 42538
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 42535
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42542
    :cond_2
    iget v0, p0, Ljir;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 42543
    const/4 v0, 0x3

    iget v2, p0, Ljir;->enK:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 42545
    :cond_3
    iget v0, p0, Ljir;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 42546
    const/4 v0, 0x4

    iget-object v2, p0, Ljir;->enL:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 42548
    :cond_4
    iget-object v0, p0, Ljir;->ejU:Lixb;

    if-eqz v0, :cond_5

    .line 42549
    const/4 v0, 0x5

    iget-object v2, p0, Ljir;->ejU:Lixb;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 42551
    :cond_5
    iget-object v0, p0, Ljir;->enO:[Ljis;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljir;->enO:[Ljis;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 42552
    :goto_1
    iget-object v0, p0, Ljir;->enO:[Ljis;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 42553
    iget-object v0, p0, Ljir;->enO:[Ljis;

    aget-object v0, v0, v1

    .line 42554
    if-eqz v0, :cond_6

    .line 42555
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 42552
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 42559
    :cond_7
    iget-object v0, p0, Ljir;->enM:Ljis;

    if-eqz v0, :cond_8

    .line 42560
    const/4 v0, 0x7

    iget-object v1, p0, Ljir;->enM:Ljis;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 42562
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 42563
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 42567
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 42568
    iget-object v2, p0, Ljir;->aeB:Ljbp;

    if-eqz v2, :cond_0

    .line 42569
    const/4 v2, 0x1

    iget-object v3, p0, Ljir;->aeB:Ljbp;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42572
    :cond_0
    iget-object v2, p0, Ljir;->enN:[Ljis;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljir;->enN:[Ljis;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 42573
    :goto_0
    iget-object v3, p0, Ljir;->enN:[Ljis;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 42574
    iget-object v3, p0, Ljir;->enN:[Ljis;

    aget-object v3, v3, v0

    .line 42575
    if-eqz v3, :cond_1

    .line 42576
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 42573
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 42581
    :cond_3
    iget v2, p0, Ljir;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    .line 42582
    const/4 v2, 0x3

    iget v3, p0, Ljir;->enK:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 42585
    :cond_4
    iget v2, p0, Ljir;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_5

    .line 42586
    const/4 v2, 0x4

    iget-object v3, p0, Ljir;->enL:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42589
    :cond_5
    iget-object v2, p0, Ljir;->ejU:Lixb;

    if-eqz v2, :cond_6

    .line 42590
    const/4 v2, 0x5

    iget-object v3, p0, Ljir;->ejU:Lixb;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42593
    :cond_6
    iget-object v2, p0, Ljir;->enO:[Ljis;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljir;->enO:[Ljis;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 42594
    :goto_1
    iget-object v2, p0, Ljir;->enO:[Ljis;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 42595
    iget-object v2, p0, Ljir;->enO:[Ljis;

    aget-object v2, v2, v1

    .line 42596
    if-eqz v2, :cond_7

    .line 42597
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 42594
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 42602
    :cond_8
    iget-object v1, p0, Ljir;->enM:Ljis;

    if-eqz v1, :cond_9

    .line 42603
    const/4 v1, 0x7

    iget-object v2, p0, Ljir;->enM:Ljis;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 42606
    :cond_9
    return v0
.end method

.method public final vY(Ljava/lang/String;)Ljir;
    .locals 1

    .prologue
    .line 42482
    if-nez p1, :cond_0

    .line 42483
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42485
    :cond_0
    iput-object p1, p0, Ljir;->enL:Ljava/lang/String;

    .line 42486
    iget v0, p0, Ljir;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljir;->aez:I

    .line 42487
    return-object p0
.end method
