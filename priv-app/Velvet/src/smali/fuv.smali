.class public final Lfuv;
.super Lfuz;
.source "PG"


# instance fields
.field private final cug:Lfss;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfss;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 31
    iput-object p4, p0, Lfuv;->cug:Lfss;

    .line 32
    return-void
.end method

.method private a(Ljef;ILiwk;IZ)Lani;
    .locals 4

    .prologue
    .line 88
    iget-object v0, p1, Ljef;->ajE:Ljei;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljef;->ajE:Ljei;

    invoke-virtual {v0}, Ljei;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    new-instance v1, Lfsa;

    invoke-direct {v1, p2}, Lfsa;-><init>(I)V

    invoke-virtual {v1, p4}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iget-object v3, p1, Ljef;->ajE:Ljei;

    invoke-virtual {v1, v2, p3, v3}, Lfsa;->a(Lizj;Liwk;Ljei;)Lani;

    move-result-object v1

    .line 94
    iget-object v2, v1, Lani;->ahH:Laom;

    invoke-virtual {v2, v0}, Laom;->bY(Ljava/lang/String;)Laom;

    .line 95
    if-eqz p5, :cond_0

    .line 96
    iget-object v0, v1, Lani;->ahH:Laom;

    invoke-virtual {p1}, Ljef;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Laom;->bZ(Ljava/lang/String;)Laom;

    .line 97
    iget-object v0, v1, Lani;->ahH:Laom;

    invoke-virtual {p1}, Ljef;->bcs()Z

    move-result v2

    invoke-virtual {v0, v2}, Laom;->bd(Z)Laom;

    .line 99
    :cond_0
    return-object v1

    .line 88
    :cond_1
    invoke-virtual {p3}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 36
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTu:Ljef;

    .line 38
    invoke-virtual {p0, p1, v0}, Lfuv;->a(Landroid/content/Context;Ljef;)Lanh;

    move-result-object v1

    .line 39
    invoke-virtual {p0, p1, v0}, Lfuv;->b(Landroid/content/Context;Ljef;)Lanh;

    move-result-object v0

    .line 41
    new-instance v2, Lang;

    invoke-direct {v2}, Lang;-><init>()V

    .line 42
    if-eqz v0, :cond_0

    .line 43
    const/4 v3, 0x2

    new-array v3, v3, [Lanh;

    aput-object v0, v3, v5

    aput-object v1, v3, v4

    iput-object v3, v2, Lang;->ags:[Lanh;

    .line 47
    :goto_0
    invoke-virtual {v2, v4}, Lang;->aS(Z)Lang;

    .line 48
    return-object v2

    .line 45
    :cond_0
    new-array v0, v4, [Lanh;

    aput-object v1, v0, v5

    iput-object v0, v2, Lang;->ags:[Lanh;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljef;)Lanh;
    .locals 7

    .prologue
    .line 53
    iget-object v0, p0, Lfuv;->cug:Lfss;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {v0, p1, v1, p2}, Lfss;->a(Landroid/content/Context;Lizj;Ljef;)Lanh;

    move-result-object v6

    .line 56
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    const/16 v1, 0xa0

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-static {v0, v1, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v3

    .line 58
    if-eqz v3, :cond_0

    invoke-virtual {p2}, Ljef;->qG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v2, 0x3

    const v4, 0x7f0201ab

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfuv;->a(Ljef;ILiwk;IZ)Lani;

    move-result-object v0

    iput-object v0, v6, Lanh;->ahs:Lani;

    .line 64
    :cond_0
    return-object v6
.end method

.method public final b(Landroid/content/Context;Ljef;)Lanh;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v2, 0xa0

    const/4 v5, 0x0

    .line 70
    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    new-array v3, v5, [I

    invoke-static {v1, v2, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v3

    .line 73
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Liwk;->aZp()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    const v4, 0x7f0200ed

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfuv;->a(Ljef;ILiwk;IZ)Lani;

    move-result-object v0

    .line 77
    new-instance v1, Lftn;

    invoke-virtual {v3}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lftn;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {v1, v0, v2}, Lftn;->a(Lani;Lizj;)Lftn;

    move-result-object v0

    invoke-virtual {v0}, Lftn;->aDh()Lanh;

    move-result-object v0

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lanh;->cn(I)Lanh;

    .line 82
    :cond_0
    return-object v0
.end method
