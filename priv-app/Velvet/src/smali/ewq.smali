.class public final Lewq;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field ckK:Lewu;

.field mEntry:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 33
    return-void
.end method

.method public static a(Landroid/app/Fragment;Lizj;)Lewq;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 42
    const-string v1, "ENTRY_KEY"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 44
    new-instance v1, Lewq;

    invoke-direct {v1}, Lewq;-><init>()V

    .line 45
    invoke-virtual {v1, v0}, Lewq;->setArguments(Landroid/os/Bundle;)V

    .line 46
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Lewq;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 47
    return-object v1
.end method

.method static synthetic a(Lewq;Lexl;Z)V
    .locals 2

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lexl;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    const/4 v1, -0x2

    invoke-virtual {p1, v1}, Lexl;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 58
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v4

    .line 60
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v5

    .line 61
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axX()Leuc;

    move-result-object v6

    .line 64
    if-eqz p1, :cond_1

    .line 65
    const-string v0, "ENTRY_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    iput-object v0, p0, Lewq;->mEntry:Lizj;

    .line 70
    :goto_0
    invoke-virtual {p0}, Lewq;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 71
    instance-of v1, v0, Lewu;

    if-eqz v1, :cond_0

    .line 72
    check-cast v0, Lewu;

    iput-object v0, p0, Lewq;->ckK:Lewu;

    .line 75
    :cond_0
    invoke-virtual {p0}, Lewq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 76
    new-instance v2, Lexl;

    invoke-virtual {p0}, Lewq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lewq;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v7, 0x7f0a03f6

    invoke-direct {v2, v0, v1, v7}, Lexl;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;I)V

    .line 78
    const v7, 0x1040013

    new-instance v0, Lewr;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lewr;-><init>(Lewq;Lexl;Landroid/content/Context;Lfcx;Lemp;Leuc;)V

    invoke-virtual {v2, v7, v0}, Lexl;->a(ILandroid/view/View$OnClickListener;)V

    .line 111
    const v0, 0x1040009

    new-instance v1, Lewt;

    invoke-direct {v1, p0, v2}, Lewt;-><init>(Lewq;Lexl;)V

    invoke-virtual {v2, v0, v1}, Lexl;->b(ILandroid/view/View$OnClickListener;)V

    .line 118
    return-object v2

    .line 67
    :cond_1
    invoke-virtual {p0}, Lewq;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ENTRY_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    iput-object v0, p0, Lewq;->mEntry:Lizj;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 53
    const-string v0, "ENTRY_KEY"

    iget-object v1, p0, Lewq;->mEntry:Lizj;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 54
    return-void
.end method
