.class public final Lfgo;
.super Lfet;
.source "PG"


# instance fields
.field private final mEntry:Lizj;


# direct methods
.method public constructor <init>(Lizj;Ljbp;Lgah;Lemp;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lfet;-><init>(Lizj;Ljbp;Lgah;Lemp;)V

    .line 37
    iput-object p1, p0, Lfgo;->mEntry:Lizj;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0, p1, p2}, Lfgo;->b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lfgo;->mTravelReport:Lgca;

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 124
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lfgo;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aEH()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lfgo;->mTravelReport:Lgca;

    invoke-virtual {v0, p1}, Lgca;->bt(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lfgo;->mTravelReport:Lgca;

    invoke-virtual {v0, p1}, Lgca;->bs(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aN(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 94
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 95
    iget-object v0, p0, Lfgo;->mFrequentPlaceEntry:Ljal;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lfgo;->mFrequentPlaceEntry:Ljal;

    iget-object v2, v0, Ljal;->dMF:[Liyg;

    .line 97
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 98
    iget-object v4, v4, Liyg;->dPL:Liyh;

    .line 99
    if-eqz v4, :cond_0

    .line 100
    iget-object v4, v4, Liyh;->dQe:[Liyi;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 97
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_1
    const-string v0, "TrafficNotification"

    const-string v2, "No FrequentPlaceEntry in Entry"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_2
    const-string v0, ""

    const-string v2, ""

    invoke-static {p1, v1, v0, v2}, Lgbx;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "event"

    return-object v0
.end method

.method public final ayY()I
    .locals 2

    .prologue
    const v0, 0x7f0202db

    .line 42
    iget-object v1, p0, Lfgo;->mTravelReport:Lgca;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lfgo;->mTravelReport:Lgca;

    invoke-virtual {v1}, Lgca;->aEz()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 51
    iget-object v1, p0, Lfgo;->mTravelReport:Lgca;

    invoke-virtual {v1}, Lgca;->aEA()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 45
    :pswitch_0
    const v0, 0x7f0202d7

    goto :goto_0

    .line 47
    :pswitch_1
    const v0, 0x7f0202d1

    goto :goto_0

    .line 49
    :pswitch_2
    const v0, 0x7f0202e0

    goto :goto_0

    .line 51
    :pswitch_3
    const v0, 0x7f0202de

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0202df

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0202dd

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 51
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lfgo;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfgb;->cqz:Lfgb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfgb;->cqx:Lfgb;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lfgo;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfgo;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->bge()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lfgo;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->bgd()Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_0
    :goto_0
    return-object v0

    .line 82
    :cond_1
    invoke-virtual {p0, p1}, Lfgo;->aN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    .line 84
    const v0, 0x7f0a0173

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfgo;->mFrequentPlaceEntry:Ljal;

    iget-object v3, v3, Ljal;->dWL:Ljak;

    invoke-static {p1, v3}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
