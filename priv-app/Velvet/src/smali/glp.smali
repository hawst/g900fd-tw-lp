.class public final Lglp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cON:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lglp;->cON:Ljava/util/List;

    .line 41
    return-void
.end method

.method private aHj()Ljvv;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 68
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 71
    iget-object v1, p0, Lglp;->cON:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvv;

    .line 72
    iget-object v4, v0, Ljvv;->eIm:Ljvw;

    if-eqz v4, :cond_2

    .line 73
    iget-object v0, v0, Ljvv;->eIm:Ljvw;

    .line 75
    iget-object v4, v0, Ljvw;->eIk:[Ljvs;

    array-length v4, v4

    if-lez v4, :cond_2

    .line 76
    iget-object v0, v0, Ljvw;->eIk:[Ljvs;

    aget-object v0, v0, v6

    .line 77
    invoke-virtual {v0}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v0}, Ljvs;->bty()F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    move v0, v1

    :goto_1
    move v1, v0

    .line 81
    goto :goto_0

    .line 83
    :cond_0
    iget-object v0, p0, Lglp;->cON:Ljava/util/List;

    iget-object v2, p0, Lglp;->cON:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvv;

    .line 84
    new-instance v2, Ljvv;

    invoke-direct {v2}, Ljvv;-><init>()V

    .line 86
    :try_start_0
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v4

    invoke-static {v2, v4}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    new-instance v0, Ljvw;

    invoke-direct {v0}, Ljvw;-><init>()V

    .line 97
    const/4 v4, 0x1

    new-array v4, v4, [Ljvs;

    new-instance v5, Ljvs;

    invoke-direct {v5}, Ljvs;-><init>()V

    invoke-virtual {v5, v1}, Ljvs;->aa(F)Ljvs;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljvs;->zo(Ljava/lang/String;)Ljvs;

    move-result-object v1

    aput-object v1, v4, v6

    iput-object v4, v0, Ljvw;->eIk:[Ljvs;

    .line 100
    iput-object v0, v2, Ljvv;->eIo:Ljvw;

    :cond_1
    move-object v0, v2

    .line 103
    :goto_2
    return-object v0

    .line 88
    :catch_0
    move-exception v1

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final aHi()Ljvv;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lglp;->cON:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const/4 v0, 0x0

    .line 64
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    iget-object v0, p0, Lglp;->cON:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 60
    iget-object v0, p0, Lglp;->cON:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvv;

    iget-object v1, v0, Ljvv;->eIm:Ljvw;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ljvv;->eIm:Ljvw;

    iput-object v1, v0, Ljvv;->eIo:Ljvw;

    goto :goto_0

    .line 64
    :cond_2
    invoke-direct {p0}, Lglp;->aHj()Ljvv;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Ljvv;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lglp;->cON:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method
