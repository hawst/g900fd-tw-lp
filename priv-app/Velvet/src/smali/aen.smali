.class public Laen;
.super Laeu;
.source "PG"


# instance fields
.field private mFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Laeu;-><init>()V

    .line 204
    iput-object p1, p0, Laen;->mFile:Ljava/io/File;

    .line 205
    iput-object p2, p0, Laen;->UB:Landroid/graphics/drawable/Drawable;

    .line 206
    return-void
.end method


# virtual methods
.method public final a(Ladz;)V
    .locals 4

    .prologue
    .line 209
    new-instance v0, Laii;

    iget-object v1, p0, Laen;->mFile:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const/16 v2, 0x400

    invoke-direct {v0, p1, v1, v2}, Laii;-><init>(Landroid/content/Context;Landroid/net/Uri;I)V

    .line 211
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Ladz;->a(Laif;ZZLjava/lang/Runnable;)V

    .line 212
    return-void
.end method

.method public final isSelectable()Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method public final kC()Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    return v0
.end method

.method public final n(Ladz;)V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Laen;->mFile:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ladz;->a(Landroid/net/Uri;Z)V

    .line 216
    return-void
.end method
