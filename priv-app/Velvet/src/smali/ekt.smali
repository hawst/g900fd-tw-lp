.class public final Lekt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cdX:Landroid/view/animation/LinearInterpolator;

.field private static cec:F


# instance fields
.field private AC:Z

.field private cdY:F

.field private cdZ:I

.field private cea:I

.field private ceb:I

.field private ced:F

.field private cee:F

.field cef:Leky;

.field private ceg:I

.field private ceh:F

.field private cei:Landroid/view/View;

.field private cej:Z

.field private cek:F

.field public cel:Z

.field public cem:Z

.field private cen:Z

.field private fW:Landroid/view/VelocityTracker;

.field private hO:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lekt;->cdX:Landroid/view/animation/LinearInterpolator;

    .line 41
    const v0, 0x3e19999a    # 0.15f

    sput v0, Lekt;->cec:F

    return-void
.end method

.method public constructor <init>(ILeky;FF)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lekt;->cdY:F

    .line 36
    const/16 v0, 0x4b

    iput v0, p0, Lekt;->cdZ:I

    .line 37
    const/16 v0, 0x96

    iput v0, p0, Lekt;->cea:I

    .line 38
    const/16 v0, 0x7d0

    iput v0, p0, Lekt;->ceb:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lekt;->ced:F

    .line 59
    iput-boolean v1, p0, Lekt;->cel:Z

    .line 60
    iput-boolean v1, p0, Lekt;->cem:Z

    .line 61
    iput-boolean v1, p0, Lekt;->hO:Z

    .line 66
    iput-object p2, p0, Lekt;->cef:Leky;

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lekt;->ceg:I

    .line 68
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lekt;->fW:Landroid/view/VelocityTracker;

    .line 69
    iput p3, p0, Lekt;->cek:F

    .line 70
    iput p4, p0, Lekt;->cee:F

    .line 71
    return-void
.end method

.method private M(F)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 320
    iget v0, p0, Lekt;->ceg:I

    if-nez v0, :cond_3

    .line 321
    iget-boolean v0, p0, Lekt;->cen:Z

    if-eqz v0, :cond_1

    .line 322
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lekt;->cem:Z

    .line 329
    :goto_0
    return v0

    .line 322
    :cond_0
    iget-boolean v0, p0, Lekt;->cel:Z

    goto :goto_0

    .line 324
    :cond_1
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_2

    iget-boolean v0, p0, Lekt;->cel:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lekt;->cem:Z

    goto :goto_0

    .line 329
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private aW(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lekt;->ceg:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method private aX(Landroid/view/View;)F
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 135
    iget v1, p0, Lekt;->ceg:I

    if-nez v1, :cond_0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    goto :goto_0
.end method

.method private g(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 3

    .prologue
    .line 115
    iget v0, p0, Lekt;->ceg:I

    if-nez v0, :cond_0

    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p2, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 117
    return-object v0

    .line 115
    :cond_0
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    goto :goto_0
.end method

.method private h(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lekt;->ceg:I

    if-nez v0, :cond_0

    .line 127
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method private q(Landroid/view/MotionEvent;)F
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lekt;->ceg:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final aV(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lekt;->h(Landroid/view/View;F)V

    .line 99
    return-void
.end method

.method final aY(Landroid/view/View;)F
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 143
    invoke-direct {p0, p1}, Lekt;->aX(Landroid/view/View;)F

    move-result v0

    .line 144
    const v2, 0x3f266666    # 0.65f

    mul-float/2addr v2, v0

    .line 146
    invoke-direct {p0, p1}, Lekt;->aW(Landroid/view/View;)F

    move-result v3

    .line 147
    sget v4, Lekt;->cec:F

    mul-float/2addr v4, v0

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    .line 148
    sget v4, Lekt;->cec:F

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    div-float/2addr v0, v2

    sub-float v0, v1, v0

    .line 152
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 153
    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 154
    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0

    .line 149
    :cond_0
    sget v4, Lekt;->cec:F

    sub-float v4, v1, v4

    mul-float/2addr v4, v0

    cmpg-float v4, v3, v4

    if-gez v4, :cond_1

    .line 150
    sget v4, Lekt;->cec:F

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final aui()V
    .locals 2

    .prologue
    .line 86
    iget-boolean v0, p0, Lekt;->AC:Z

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lekt;->cef:Leky;

    .line 89
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lekt;->h(Landroid/view/View;F)V

    .line 90
    iget-object v0, p0, Lekt;->cef:Leky;

    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    invoke-interface {v0, v1}, Leky;->aU(Landroid/view/View;)V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lekt;->cei:Landroid/view/View;

    .line 93
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lekt;->AC:Z

    .line 95
    :cond_1
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 158
    iget-boolean v1, p0, Lekt;->hO:Z

    if-nez v1, :cond_0

    .line 193
    :goto_0
    return v0

    .line 159
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 161
    packed-switch v1, :pswitch_data_0

    .line 193
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lekt;->AC:Z

    goto :goto_0

    .line 163
    :pswitch_0
    iput-boolean v0, p0, Lekt;->AC:Z

    .line 164
    iget-object v1, p0, Lekt;->cef:Leky;

    invoke-interface {v1, p1}, Leky;->p(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lekt;->cei:Landroid/view/View;

    .line 165
    iget-object v1, p0, Lekt;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    .line 166
    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 167
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    invoke-static {v0}, Leot;->ay(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lekt;->cen:Z

    .line 168
    iget-object v0, p0, Lekt;->cef:Leky;

    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    invoke-interface {v0, v1}, Leky;->aR(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lekt;->cej:Z

    .line 169
    iget-object v0, p0, Lekt;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 170
    invoke-direct {p0, p1}, Lekt;->q(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lekt;->ceh:F

    goto :goto_1

    .line 172
    :cond_2
    iput-boolean v0, p0, Lekt;->cej:Z

    goto :goto_1

    .line 176
    :pswitch_1
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lekt;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 178
    invoke-direct {p0, p1}, Lekt;->q(Landroid/view/MotionEvent;)F

    move-result v0

    .line 179
    iget v1, p0, Lekt;->ceh:F

    sub-float/2addr v0, v1

    .line 180
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lekt;->cee:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 181
    iget-object v0, p0, Lekt;->cef:Leky;

    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    invoke-interface {v0, v1}, Leky;->aS(Landroid/view/View;)V

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekt;->AC:Z

    .line 183
    invoke-direct {p0, p1}, Lekt;->q(Landroid/view/MotionEvent;)F

    move-result v0

    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    invoke-direct {p0, v1}, Lekt;->aW(Landroid/view/View;)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lekt;->ceh:F

    goto :goto_1

    .line 189
    :pswitch_2
    iput-boolean v0, p0, Lekt;->AC:Z

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lekt;->cei:Landroid/view/View;

    goto :goto_1

    .line 161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 271
    iget-boolean v0, p0, Lekt;->hO:Z

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return v4

    .line 272
    :cond_0
    iget-boolean v0, p0, Lekt;->AC:Z

    if-nez v0, :cond_1

    .line 273
    invoke-virtual {p0, p1}, Lekt;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 274
    iget-boolean v4, p0, Lekt;->cej:Z

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Lekt;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 279
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 280
    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    move v4, v3

    .line 295
    goto :goto_0

    .line 283
    :pswitch_0
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 284
    invoke-direct {p0, p1}, Lekt;->q(Landroid/view/MotionEvent;)F

    move-result v0

    iget v1, p0, Lekt;->ceh:F

    sub-float v1, v0, v1

    .line 285
    invoke-direct {p0, v1}, Lekt;->M(F)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lekt;->cef:Leky;

    iget-object v2, p0, Lekt;->cei:Landroid/view/View;

    invoke-interface {v0, v2}, Leky;->aR(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_14

    :cond_3
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    invoke-direct {p0, v0}, Lekt;->aX(Landroid/view/View;)F

    move-result v2

    const v0, 0x3e19999a    # 0.15f

    mul-float/2addr v0, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v2

    if-ltz v4, :cond_5

    cmpl-float v1, v1, v5

    if-lez v1, :cond_4

    :goto_2
    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lekt;->h(Landroid/view/View;F)V

    iget-object v0, p0, Lekt;->cef:Leky;

    invoke-interface {v0}, Leky;->auh()V

    iget-boolean v0, p0, Lekt;->cej:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    invoke-virtual {p0, v0}, Lekt;->aY(Landroid/view/View;)F

    move-result v0

    iget-object v1, p0, Lekt;->cei:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    :cond_4
    neg-float v0, v0

    goto :goto_2

    :cond_5
    div-float/2addr v1, v2

    float-to-double v4, v1

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v1, v4

    mul-float/2addr v0, v1

    goto :goto_2

    .line 290
    :pswitch_1
    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 291
    iget-object v1, p0, Lekt;->fW:Landroid/view/VelocityTracker;

    iget v0, p0, Lekt;->ceb:I

    int-to-float v0, v0

    iget v2, p0, Lekt;->cek:F

    mul-float/2addr v0, v2

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2, v0}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget v0, p0, Lekt;->ceg:I

    if-nez v0, :cond_a

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_3
    iget v2, p0, Lekt;->ceg:I

    if-nez v2, :cond_b

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    :goto_4
    iget v2, p0, Lekt;->cdY:F

    iget v6, p0, Lekt;->cek:F

    mul-float/2addr v6, v2

    iget-object v2, p0, Lekt;->cei:Landroid/view/View;

    invoke-direct {p0, v2}, Lekt;->aW(Landroid/view/View;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v8, v2

    const-wide v10, 0x3fe3333333333333L    # 0.6

    iget-object v2, p0, Lekt;->cei:Landroid/view/View;

    invoke-direct {p0, v2}, Lekt;->aX(Landroid/view/View;)F

    move-result v2

    float-to-double v12, v2

    mul-double/2addr v10, v12

    cmpl-double v2, v8, v10

    if-lez v2, :cond_c

    move v2, v3

    :goto_5
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpl-float v6, v8, v6

    if-lez v6, :cond_f

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v6, v1

    if-lez v1, :cond_f

    cmpl-float v1, v0, v5

    if-lez v1, :cond_d

    move v6, v3

    :goto_6
    cmpl-float v1, v7, v5

    if-lez v1, :cond_e

    move v1, v3

    :goto_7
    if-ne v6, v1, :cond_f

    move v1, v3

    :goto_8
    iget-object v6, p0, Lekt;->cef:Leky;

    iget-object v8, p0, Lekt;->cei:Landroid/view/View;

    invoke-interface {v6, v8}, Leky;->aR(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-direct {p0, v7}, Lekt;->M(F)Z

    move-result v6

    if-eqz v6, :cond_7

    if-nez v1, :cond_6

    if-eqz v2, :cond_7

    :cond_6
    move v4, v3

    :cond_7
    if-eqz v4, :cond_13

    iget-object v2, p0, Lekt;->cei:Landroid/view/View;

    if-eqz v1, :cond_10

    :goto_9
    iget-object v1, p0, Lekt;->cef:Leky;

    invoke-interface {v1, v2}, Leky;->aR(Landroid/view/View;)Z

    move-result v4

    cmpg-float v1, v0, v5

    if-ltz v1, :cond_9

    cmpl-float v1, v0, v5

    if-nez v1, :cond_8

    invoke-direct {p0, v2}, Lekt;->aW(Landroid/view/View;)F

    move-result v1

    cmpg-float v1, v1, v5

    if-ltz v1, :cond_9

    :cond_8
    cmpl-float v1, v0, v5

    if-nez v1, :cond_11

    invoke-direct {p0, v2}, Lekt;->aW(Landroid/view/View;)F

    move-result v1

    cmpl-float v1, v1, v5

    if-nez v1, :cond_11

    iget v1, p0, Lekt;->ceg:I

    if-ne v1, v3, :cond_11

    :cond_9
    invoke-direct {p0, v2}, Lekt;->aX(Landroid/view/View;)F

    move-result v1

    neg-float v1, v1

    :goto_a
    iget v6, p0, Lekt;->cea:I

    cmpl-float v5, v0, v5

    if-eqz v5, :cond_12

    invoke-direct {p0, v2}, Lekt;->aW(Landroid/view/View;)F

    move-result v5

    sub-float v5, v1, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v7, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float v0, v5, v0

    float-to-int v0, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_b
    invoke-direct {p0, v2, v1}, Lekt;->g(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    sget-object v5, Lekt;->cdX:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v1, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v6, v0

    invoke-virtual {v1, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v0, Leku;

    invoke-direct {v0, p0, v2, v4}, Leku;-><init>(Lekt;Landroid/view/View;Z)V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Lekv;

    invoke-direct {v0, p0, v4, v2}, Lekv;-><init>(Lekt;ZLandroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto/16 :goto_3

    :cond_b
    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    goto/16 :goto_4

    :cond_c
    move v2, v4

    goto/16 :goto_5

    :cond_d
    move v6, v4

    goto/16 :goto_6

    :cond_e
    move v1, v4

    goto/16 :goto_7

    :cond_f
    move v1, v4

    goto/16 :goto_8

    :cond_10
    move v0, v5

    goto/16 :goto_9

    :cond_11
    invoke-direct {p0, v2}, Lekt;->aX(Landroid/view/View;)F

    move-result v1

    goto :goto_a

    :cond_12
    iget v0, p0, Lekt;->cdZ:I

    goto :goto_b

    :cond_13
    iget-object v0, p0, Lekt;->cef:Leky;

    iget-object v0, p0, Lekt;->cei:Landroid/view/View;

    iget-object v1, p0, Lekt;->cef:Leky;

    invoke-interface {v1, v0}, Leky;->aR(Landroid/view/View;)Z

    move-result v1

    invoke-direct {p0, v0, v5}, Lekt;->g(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0x96

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v4, Lekw;

    invoke-direct {v4, p0, v1, v0}, Lekw;-><init>(Lekt;ZLandroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v4, Lekx;

    invoke-direct {v4, p0, v1, v0}, Lekx;-><init>(Lekt;ZLandroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_1

    :cond_14
    move v0, v1

    goto/16 :goto_2

    .line 280
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final setEnabled(Z)V
    .locals 0

    .prologue
    .line 82
    iput-boolean p1, p0, Lekt;->hO:Z

    .line 83
    return-void
.end method
