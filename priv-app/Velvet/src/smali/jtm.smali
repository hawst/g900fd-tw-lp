.class public final Ljtm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eDy:[Ljtm;


# instance fields
.field public aLI:I

.field private aez:I

.field private dzl:F

.field public eDz:[Ljtk;

.field public length:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 385
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 386
    iput v0, p0, Ljtm;->aez:I

    iput v0, p0, Ljtm;->aLI:I

    iput v0, p0, Ljtm;->length:I

    invoke-static {}, Ljtk;->buu()[Ljtk;

    move-result-object v0

    iput-object v0, p0, Ljtm;->eDz:[Ljtk;

    const/4 v0, 0x0

    iput v0, p0, Ljtm;->dzl:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljtm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljtm;->eCz:I

    .line 387
    return-void
.end method

.method public static bux()[Ljtm;
    .locals 2

    .prologue
    .line 344
    sget-object v0, Ljtm;->eDy:[Ljtm;

    if-nez v0, :cond_1

    .line 345
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 347
    :try_start_0
    sget-object v0, Ljtm;->eDy:[Ljtm;

    if-nez v0, :cond_0

    .line 348
    const/4 v0, 0x0

    new-array v0, v0, [Ljtm;

    sput-object v0, Ljtm;->eDy:[Ljtm;

    .line 350
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    :cond_1
    sget-object v0, Ljtm;->eDy:[Ljtm;

    return-object v0

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 338
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljtm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtm;->aLI:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtm;->length:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljtm;->eDz:[Ljtk;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljtk;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljtm;->eDz:[Ljtk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljtk;

    invoke-direct {v3}, Ljtk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljtm;->eDz:[Ljtk;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljtk;

    invoke-direct {v3}, Ljtk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljtm;->eDz:[Ljtk;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljtm;->dzl:F

    iget v0, p0, Ljtm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtm;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 403
    const/4 v0, 0x1

    iget v1, p0, Ljtm;->aLI:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 404
    const/4 v0, 0x2

    iget v1, p0, Ljtm;->length:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 405
    iget-object v0, p0, Ljtm;->eDz:[Ljtk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljtm;->eDz:[Ljtk;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 406
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljtm;->eDz:[Ljtk;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 407
    iget-object v1, p0, Ljtm;->eDz:[Ljtk;

    aget-object v1, v1, v0

    .line 408
    if-eqz v1, :cond_0

    .line 409
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 406
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_1
    iget v0, p0, Ljtm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 414
    const/4 v0, 0x4

    iget v1, p0, Ljtm;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 416
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 417
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 421
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 422
    const/4 v1, 0x1

    iget v2, p0, Ljtm;->aLI:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 424
    const/4 v1, 0x2

    iget v2, p0, Ljtm;->length:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v1, v0

    .line 426
    iget-object v0, p0, Ljtm;->eDz:[Ljtk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljtm;->eDz:[Ljtk;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 427
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljtm;->eDz:[Ljtk;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 428
    iget-object v2, p0, Ljtm;->eDz:[Ljtk;

    aget-object v2, v2, v0

    .line 429
    if-eqz v2, :cond_0

    .line 430
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 427
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 435
    :cond_1
    iget v0, p0, Ljtm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 436
    const/4 v0, 0x4

    iget v2, p0, Ljtm;->dzl:F

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    .line 439
    :cond_2
    return v1
.end method
