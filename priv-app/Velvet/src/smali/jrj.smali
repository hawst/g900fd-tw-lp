.class public final Ljrj;
.super Ljsl;
.source "PG"


# static fields
.field public static final eAA:Ljsm;


# instance fields
.field private aez:I

.field public eAB:Ljrk;

.field public eAC:[Ljrk;

.field public eAD:Ljpz;

.field private eAf:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2344
    const/16 v0, 0xb

    const-class v1, Ljrj;

    const v2, 0x1ef2616a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljrj;->eAA:Ljsm;

    .line 2518
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2553
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2554
    iput v2, p0, Ljrj;->aez:I

    iput-object v1, p0, Ljrj;->eAB:Ljrk;

    invoke-static {}, Ljrk;->btn()[Ljrk;

    move-result-object v0

    iput-object v0, p0, Ljrj;->eAC:[Ljrk;

    iput v2, p0, Ljrj;->eAf:I

    iput-object v1, p0, Ljrj;->eAD:Ljpz;

    iput-object v1, p0, Ljrj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrj;->eCz:I

    .line 2555
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2337
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljrj;->eAB:Ljrk;

    if-nez v0, :cond_1

    new-instance v0, Ljrk;

    invoke-direct {v0}, Ljrk;-><init>()V

    iput-object v0, p0, Ljrj;->eAB:Ljrk;

    :cond_1
    iget-object v0, p0, Ljrj;->eAB:Ljrk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrj;->eAC:[Ljrk;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljrk;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljrj;->eAC:[Ljrk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljrk;

    invoke-direct {v3}, Ljrk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljrj;->eAC:[Ljrk;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljrk;

    invoke-direct {v3}, Ljrk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrj;->eAC:[Ljrk;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrj;->eAf:I

    iget v0, p0, Ljrj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrj;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljrj;->eAD:Ljpz;

    if-nez v0, :cond_5

    new-instance v0, Ljpz;

    invoke-direct {v0}, Ljpz;-><init>()V

    iput-object v0, p0, Ljrj;->eAD:Ljpz;

    :cond_5
    iget-object v0, p0, Ljrj;->eAD:Ljpz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2571
    iget-object v0, p0, Ljrj;->eAB:Ljrk;

    if-eqz v0, :cond_0

    .line 2572
    const/4 v0, 0x1

    iget-object v1, p0, Ljrj;->eAB:Ljrk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2574
    :cond_0
    iget-object v0, p0, Ljrj;->eAC:[Ljrk;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljrj;->eAC:[Ljrk;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2575
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljrj;->eAC:[Ljrk;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 2576
    iget-object v1, p0, Ljrj;->eAC:[Ljrk;

    aget-object v1, v1, v0

    .line 2577
    if-eqz v1, :cond_1

    .line 2578
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 2575
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2582
    :cond_2
    iget v0, p0, Ljrj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 2583
    const/4 v0, 0x3

    iget v1, p0, Ljrj;->eAf:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2585
    :cond_3
    iget-object v0, p0, Ljrj;->eAD:Ljpz;

    if-eqz v0, :cond_4

    .line 2586
    const/4 v0, 0x4

    iget-object v1, p0, Ljrj;->eAD:Ljpz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2588
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2589
    return-void
.end method

.method public final bsY()I
    .locals 1

    .prologue
    .line 2534
    iget v0, p0, Ljrj;->eAf:I

    return v0
.end method

.method public final bsZ()Z
    .locals 1

    .prologue
    .line 2542
    iget v0, p0, Ljrj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 2593
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2594
    iget-object v1, p0, Ljrj;->eAB:Ljrk;

    if-eqz v1, :cond_0

    .line 2595
    const/4 v1, 0x1

    iget-object v2, p0, Ljrj;->eAB:Ljrk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2598
    :cond_0
    iget-object v1, p0, Ljrj;->eAC:[Ljrk;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljrj;->eAC:[Ljrk;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 2599
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljrj;->eAC:[Ljrk;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2600
    iget-object v2, p0, Ljrj;->eAC:[Ljrk;

    aget-object v2, v2, v0

    .line 2601
    if-eqz v2, :cond_1

    .line 2602
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2599
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2607
    :cond_3
    iget v1, p0, Ljrj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    .line 2608
    const/4 v1, 0x3

    iget v2, p0, Ljrj;->eAf:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2611
    :cond_4
    iget-object v1, p0, Ljrj;->eAD:Ljpz;

    if-eqz v1, :cond_5

    .line 2612
    const/4 v1, 0x4

    iget-object v2, p0, Ljrj;->eAD:Ljpz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2615
    :cond_5
    return v0
.end method

.method public final rQ(I)Ljrj;
    .locals 1

    .prologue
    .line 2537
    iput p1, p0, Ljrj;->eAf:I

    .line 2538
    iget v0, p0, Ljrj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrj;->aez:I

    .line 2539
    return-object p0
.end method
