.class public final Lkee;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static synthetic $assertionsDisabled:Z


# instance fields
.field private final eUp:Lkdx;

.field private eUq:J

.field private eUr:I

.field private eUs:Ljava/util/List;

.field private final eUt:Lkef;

.field private final eUu:Lkeg;

.field private eUv:Lkdb;

.field private final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lkee;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkee;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(ILkdx;ZZILjava/util/List;Lkdt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkee;->eUq:J

    .line 56
    new-instance v0, Lkef;

    invoke-direct {v0, p0, v2}, Lkef;-><init>(Lkee;B)V

    iput-object v0, p0, Lkee;->eUt:Lkef;

    .line 57
    new-instance v0, Lkeg;

    invoke-direct {v0, p0, v2}, Lkeg;-><init>(Lkee;B)V

    iput-object v0, p0, Lkee;->eUu:Lkeg;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lkee;->eUv:Lkdb;

    .line 68
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    if-nez p6, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_1
    iput p1, p0, Lkee;->id:I

    .line 71
    iput-object p2, p0, Lkee;->eUp:Lkdx;

    .line 72
    iget-object v0, p0, Lkee;->eUt:Lkef;

    invoke-static {v0, p4}, Lkef;->a(Lkef;Z)Z

    .line 73
    iget-object v0, p0, Lkee;->eUu:Lkeg;

    invoke-static {v0, p3}, Lkeg;->a(Lkeg;Z)Z

    .line 74
    invoke-direct {p0, p7}, Lkee;->b(Lkdt;)V

    .line 78
    return-void
.end method

.method static synthetic b(Lkee;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lkee;->id:I

    return v0
.end method

.method private b(Lkdt;)V
    .locals 2

    .prologue
    const/high16 v0, 0x10000

    .line 300
    sget-boolean v1, Lkee;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lkee;->eUp:Lkdx;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 301
    :cond_0
    if-eqz p1, :cond_1

    iget v1, p1, Lkdt;->eTO:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_1

    iget-object v0, p1, Lkdt;->eTR:[I

    const/4 v1, 0x7

    aget v0, v0, v1

    :cond_1
    iput v0, p0, Lkee;->eUr:I

    .line 304
    return-void
.end method

.method static synthetic c(Lkee;)Lkdx;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkee;->eUp:Lkdx;

    return-object v0
.end method

.method private c(Lkdb;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 226
    sget-boolean v1, Lkee;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 227
    :cond_0
    monitor-enter p0

    .line 228
    :try_start_0
    iget-object v1, p0, Lkee;->eUv:Lkdb;

    if-eqz v1, :cond_1

    .line 229
    monitor-exit p0

    .line 238
    :goto_0
    return v0

    .line 231
    :cond_1
    iget-object v1, p0, Lkee;->eUt:Lkef;

    invoke-static {v1}, Lkef;->a(Lkef;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lkee;->eUu:Lkeg;

    invoke-static {v1}, Lkeg;->a(Lkeg;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 232
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 234
    :cond_2
    :try_start_1
    iput-object p1, p0, Lkee;->eUv:Lkdb;

    .line 235
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 236
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    iget-object v0, p0, Lkee;->eUp:Lkdx;

    iget v1, p0, Lkee;->id:I

    invoke-virtual {v0, v1}, Lkdx;->tT(I)Lkee;

    .line 238
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic d(Lkee;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lkee;->eUq:J

    return-wide v0
.end method

.method static synthetic e(Lkee;)Lkdb;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkee;->eUv:Lkdb;

    return-object v0
.end method

.method static synthetic f(Lkee;)V
    .locals 2

    .prologue
    .line 31
    sget-boolean v0, Lkee;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkee;->eUt:Lkef;

    invoke-static {v0}, Lkef;->a(Lkef;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lkee;->eUt:Lkef;

    invoke-static {v0}, Lkef;->b(Lkef;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkee;->eUu:Lkeg;

    invoke-static {v0}, Lkeg;->a(Lkeg;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lkee;->eUu:Lkeg;

    invoke-static {v0}, Lkeg;->b(Lkeg;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lkee;->isOpen()Z

    move-result v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    sget-object v0, Lkdb;->eTf:Lkdb;

    invoke-virtual {p0, v0}, Lkee;->a(Lkdb;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    if-nez v1, :cond_2

    iget-object v0, p0, Lkee;->eUp:Lkdx;

    iget v1, p0, Lkee;->id:I

    invoke-virtual {v0, v1}, Lkdx;->tT(I)Lkee;

    goto :goto_1
.end method

.method static synthetic g(Lkee;)Lkeg;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lkee;->eUu:Lkeg;

    return-object v0
.end method

.method static synthetic h(Lkee;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lkee;->eUr:I

    return v0
.end method


# virtual methods
.method final a(Ljava/util/List;Lkdf;)V
    .locals 4

    .prologue
    .line 242
    sget-boolean v0, Lkee;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 243
    :cond_0
    const/4 v1, 0x0

    .line 244
    const/4 v0, 0x1

    .line 245
    monitor-enter p0

    .line 246
    :try_start_0
    iget-object v2, p0, Lkee;->eUs:Ljava/util/List;

    if-nez v2, :cond_3

    .line 247
    invoke-virtual {p2}, Lkdf;->bzi()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 248
    sget-object v1, Lkdb;->eSV:Lkdb;

    .line 264
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    if-eqz v1, :cond_5

    .line 266
    invoke-virtual {p0, v1}, Lkee;->b(Lkdb;)V

    .line 270
    :cond_1
    :goto_1
    return-void

    .line 250
    :cond_2
    :try_start_1
    iput-object p1, p0, Lkee;->eUs:Ljava/util/List;

    .line 251
    invoke-virtual {p0}, Lkee;->isOpen()Z

    move-result v0

    .line 252
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 255
    :cond_3
    :try_start_2
    invoke-virtual {p2}, Lkdf;->bzj()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 256
    sget-object v1, Lkdb;->eSY:Lkdb;

    goto :goto_0

    .line 258
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 259
    iget-object v3, p0, Lkee;->eUs:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 260
    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 261
    iput-object v2, p0, Lkee;->eUs:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 267
    :cond_5
    if-nez v0, :cond_1

    .line 268
    iget-object v0, p0, Lkee;->eUp:Lkdx;

    iget v1, p0, Lkee;->id:I

    invoke-virtual {v0, v1}, Lkdx;->tT(I)Lkee;

    goto :goto_1
.end method

.method public final a(Lkdb;)V
    .locals 2

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lkee;->c(Lkdb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lkee;->eUp:Lkdx;

    iget v1, p0, Lkee;->id:I

    invoke-virtual {v0, v1, p1}, Lkdx;->d(ILkdb;)V

    goto :goto_0
.end method

.method public final b(Lkdb;)V
    .locals 2

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lkee;->c(Lkdb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lkee;->eUp:Lkdx;

    iget v1, p0, Lkee;->id:I

    invoke-virtual {v0, v1, p1}, Lkdx;->c(ILkdb;)V

    goto :goto_0
.end method

.method public final bzp()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 102
    iget v0, p0, Lkee;->id:I

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 103
    :goto_0
    iget-object v3, p0, Lkee;->eUp:Lkdx;

    iget-boolean v3, v3, Lkdx;->eTC:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 103
    goto :goto_1
.end method

.method public final declared-synchronized bzq()Ljava/util/List;
    .locals 3

    .prologue
    .line 120
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lkee;->eUs:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkee;->eUv:Lkdb;

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    :try_start_1
    new-instance v1, Ljava/io/InterruptedIOException;

    invoke-direct {v1}, Ljava/io/InterruptedIOException;-><init>()V

    .line 129
    invoke-virtual {v1, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 130
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 123
    :cond_0
    :try_start_2
    iget-object v0, p0, Lkee;->eUs:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lkee;->eUs:Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 126
    :cond_1
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stream was reset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lkee;->eUv:Lkdb;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method final bzr()V
    .locals 2

    .prologue
    .line 278
    sget-boolean v0, Lkee;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 280
    :cond_0
    monitor-enter p0

    .line 281
    :try_start_0
    iget-object v0, p0, Lkee;->eUt:Lkef;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lkef;->a(Lkef;Z)Z

    .line 282
    invoke-virtual {p0}, Lkee;->isOpen()Z

    move-result v0

    .line 283
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 284
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    if-nez v0, :cond_1

    .line 286
    iget-object v0, p0, Lkee;->eUp:Lkdx;

    iget v1, p0, Lkee;->id:I

    invoke-virtual {v0, v1}, Lkdx;->tT(I)Lkee;

    .line 288
    :cond_1
    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final c(Ljava/io/InputStream;I)V
    .locals 1

    .prologue
    .line 273
    sget-boolean v0, Lkee;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 274
    :cond_0
    iget-object v0, p0, Lkee;->eUt:Lkef;

    invoke-virtual {v0, p1, p2}, Lkef;->d(Ljava/io/InputStream;I)V

    .line 275
    return-void
.end method

.method final c(Lkdt;)V
    .locals 1

    .prologue
    .line 307
    sget-boolean v0, Lkee;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 308
    :cond_0
    invoke-direct {p0, p1}, Lkee;->b(Lkdt;)V

    .line 309
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 310
    return-void
.end method

.method final declared-synchronized d(Lkdb;)V
    .locals 1

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkee;->eUv:Lkdb;

    if-nez v0, :cond_0

    .line 292
    iput-object p1, p0, Lkee;->eUv:Lkdb;

    .line 293
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :cond_0
    monitor-exit p0

    return-void

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final dV(J)V
    .locals 1

    .prologue
    .line 175
    iput-wide p1, p0, Lkee;->eUq:J

    .line 176
    return-void
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lkee;->eUt:Lkef;

    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 194
    monitor-enter p0

    .line 195
    :try_start_0
    iget-object v0, p0, Lkee;->eUs:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lkee;->bzp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reply before requesting the output stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    iget-object v0, p0, Lkee;->eUu:Lkeg;

    return-object v0
.end method

.method public final declared-synchronized isOpen()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lkee;->eUv:Lkdb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 97
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 94
    :cond_1
    :try_start_1
    iget-object v1, p0, Lkee;->eUt:Lkef;

    invoke-static {v1}, Lkef;->a(Lkef;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lkee;->eUt:Lkef;

    invoke-static {v1}, Lkef;->b(Lkef;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lkee;->eUu:Lkeg;

    invoke-static {v1}, Lkeg;->a(Lkeg;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lkee;->eUu:Lkeg;

    invoke-static {v1}, Lkeg;->b(Lkeg;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lkee;->eUs:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 97
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized tV(I)V
    .locals 1

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkee;->eUu:Lkeg;

    invoke-static {v0, p1}, Lkeg;->a(Lkeg;I)I

    .line 314
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    monitor-exit p0

    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
