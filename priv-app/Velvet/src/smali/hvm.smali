.class final Lhvm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhwe;


# instance fields
.field private synthetic dsg:Lhvi;


# direct methods
.method constructor <init>(Lhvi;)V
    .locals 0

    .prologue
    .line 575
    iput-object p1, p0, Lhvm;->dsg:Lhvi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljzh;)V
    .locals 2

    .prologue
    .line 619
    const/16 v0, 0x43

    invoke-static {v0}, Lege;->ht(I)V

    .line 621
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->dsa:Lhvd;

    invoke-virtual {p1}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhvd;->aY(Ljava/lang/Object;)V

    .line 622
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSj()V

    .line 623
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->dsc:Lhvd;

    invoke-virtual {v0}, Lhvd;->aSw()V

    .line 624
    return-void
.end method

.method public final aAr()V
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drY:Lhuy;

    const/16 v0, 0x40

    invoke-static {v0}, Lege;->ht(I)V

    .line 587
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSj()V

    .line 588
    return-void
.end method

.method public final aSO()V
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->stopListening()V

    .line 579
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drY:Lhuy;

    const/16 v0, 0x3f

    invoke-static {v0}, Lege;->ht(I)V

    .line 581
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSN()V

    .line 582
    return-void
.end method

.method public final aSP()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSM()V

    .line 593
    return-void
.end method

.method public final aSQ()V
    .locals 3

    .prologue
    .line 608
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSL()V

    .line 609
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSH()V

    .line 610
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drX:Lhvz;

    invoke-virtual {v0}, Lhvz;->aSY()V

    .line 611
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-nez v0, :cond_0

    .line 612
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v1, v0, Lhvi;->dho:Lhvf;

    iget-object v0, v0, Lhvi;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lhvf;->a(Ljze;Z)V

    .line 614
    :cond_0
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->mVoiceRecognitionHandler:Lhvs;

    invoke-virtual {v0}, Lhvs;->aSW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lhvi;->dsf:Z

    .line 599
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    iget-object v0, v0, Lhvi;->drX:Lhvz;

    invoke-virtual {v0}, Lhvz;->aTc()V

    .line 603
    :goto_0
    return-void

    .line 601
    :cond_0
    iget-object v0, p0, Lhvm;->dsg:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSM()V

    goto :goto_0
.end method
