.class public final Ldhl;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic bxI:Ldhd;

.field private synthetic bxJ:Ldhu;

.field private synthetic bxK:Ldhv;

.field private synthetic bxO:Lemy;


# direct methods
.method public constructor <init>(Ldhd;Ldhu;Ldhv;Lemy;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Ldhl;->bxI:Ldhd;

    iput-object p2, p0, Ldhl;->bxJ:Ldhu;

    iput-object p3, p0, Ldhl;->bxK:Ldhv;

    iput-object p4, p0, Ldhl;->bxO:Lemy;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 163
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 164
    const-string v1, "com.google.android.gms.icing.GlobalSearchAppRegistered3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    iget-object v3, p0, Ldhl;->bxJ:Ldhu;

    iget-object v4, p0, Ldhl;->bxK:Ldhv;

    invoke-static {p2}, Lbbz;->e(Landroid/content/Intent;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-interface {v3, v0}, Ldhu;->j(Ljava/util/Collection;)V

    .line 167
    iget-object v0, p0, Ldhl;->bxI:Ldhd;

    iget-object v0, v0, Ldhd;->bxH:Ldgm;

    iget-object v1, p0, Ldhl;->bxO:Lemy;

    invoke-virtual {v0, v1}, Ldgm;->d(Lemy;)V

    .line 176
    :cond_0
    :goto_1
    return-void

    .line 166
    :cond_1
    new-instance v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    invoke-direct {v6}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;-><init>()V

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->packageName:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->labelId:I

    iput v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->labelId:I

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avi:I

    iput v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avi:I

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->iconId:I

    iput v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->iconId:I

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avj:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avj:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avk:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avk:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->avl:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->avl:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->xa()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    iput-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->xa()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    :goto_2
    iget-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    iget-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    new-instance v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    invoke-direct {v8}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;-><init>()V

    aput-object v8, v0, v1

    iget-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    aget-object v8, v0, v1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->auC:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    aget-object v0, v0, v1

    iget-object v8, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->aLu:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;

    aget-object v8, v8, v1

    iget-object v8, v8, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->auC:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->eJ(Ljava/lang/String;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v8

    iput-object v8, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->avn:[Lcom/google/android/gms/appdatasearch/Feature;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    iput-boolean v9, v6, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;->enabled:Z

    new-array v0, v9, [Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    aput-object v6, v0, v2

    invoke-virtual {v4, v0}, Ldhv;->a([Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;)Ljava/util/Collection;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    :cond_3
    const-string v1, "com.google.android.gms.icing.GlobalSearchableAppUnRegistered"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "AppPackageName"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    iget-object v1, p0, Ldhl;->bxJ:Ldhu;

    invoke-interface {v1, v0}, Ldhu;->jF(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
