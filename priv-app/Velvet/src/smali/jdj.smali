.class public final Ljdj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ecb:[Ljdj;


# instance fields
.field public ame:Ljde;

.field public ecc:[Ljdj;

.field public ecd:[Ljdf;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput-object v1, p0, Ljdj;->ame:Ljde;

    invoke-static {}, Ljdj;->bhp()[Ljdj;

    move-result-object v0

    iput-object v0, p0, Ljdj;->ecc:[Ljdj;

    invoke-static {}, Ljdf;->bhg()[Ljdf;

    move-result-object v0

    iput-object v0, p0, Ljdj;->ecd:[Ljdf;

    iput-object v1, p0, Ljdj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdj;->eCz:I

    return-void
.end method

.method public static bhp()[Ljdj;
    .locals 2

    sget-object v0, Ljdj;->ecb:[Ljdj;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdj;->ecb:[Ljdj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljdj;

    sput-object v0, Ljdj;->ecb:[Ljdj;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljdj;->ecb:[Ljdj;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljdj;->ame:Ljde;

    if-nez v0, :cond_1

    new-instance v0, Ljde;

    invoke-direct {v0}, Ljde;-><init>()V

    iput-object v0, p0, Ljdj;->ame:Ljde;

    :cond_1
    iget-object v0, p0, Ljdj;->ame:Ljde;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdj;->ecc:[Ljdj;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdj;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljdj;->ecc:[Ljdj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljdj;

    invoke-direct {v3}, Ljdj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljdj;->ecc:[Ljdj;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljdj;

    invoke-direct {v3}, Ljdj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdj;->ecc:[Ljdj;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdj;->ecd:[Ljdf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdf;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljdj;->ecd:[Ljdf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljdf;

    invoke-direct {v3}, Ljdf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljdj;->ecd:[Ljdf;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljdf;

    invoke-direct {v3}, Ljdf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdj;->ecd:[Ljdf;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Ljdj;->ame:Ljde;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v2, p0, Ljdj;->ame:Ljde;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_0
    iget-object v0, p0, Ljdj;->ecc:[Ljdj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljdj;->ecc:[Ljdj;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljdj;->ecc:[Ljdj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ljdj;->ecc:[Ljdj;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljdj;->ecd:[Ljdf;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljdj;->ecd:[Ljdf;

    array-length v0, v0

    if-lez v0, :cond_4

    :goto_1
    iget-object v0, p0, Ljdj;->ecd:[Ljdf;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Ljdj;->ecd:[Ljdf;

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget-object v2, p0, Ljdj;->ame:Ljde;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljdj;->ame:Ljde;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Ljdj;->ecc:[Ljdj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljdj;->ecc:[Ljdj;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ljdj;->ecc:[Ljdj;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Ljdj;->ecc:[Ljdj;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    :cond_3
    iget-object v2, p0, Ljdj;->ecd:[Ljdf;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljdj;->ecd:[Ljdf;

    array-length v2, v2

    if-lez v2, :cond_5

    :goto_1
    iget-object v2, p0, Ljdj;->ecd:[Ljdf;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    iget-object v2, p0, Ljdj;->ecd:[Ljdf;

    aget-object v2, v2, v1

    if-eqz v2, :cond_4

    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    return v0
.end method
