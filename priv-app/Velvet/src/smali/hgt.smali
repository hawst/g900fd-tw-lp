.class public final Lhgt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;


# direct methods
.method public constructor <init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lhgt;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 297
    iget-object v0, p0, Lhgt;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Z)Z

    .line 298
    iget-object v0, p0, Lhgt;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {p2}, Lbzf;->C(Landroid/os/IBinder;)Lbze;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Lbze;)Lbze;

    .line 300
    :try_start_0
    iget-object v0, p0, Lhgt;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    iget-object v1, p0, Lhgt;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->j(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Lbze;

    move-result-object v1

    invoke-interface {v1}, Lbze;->Cc()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 302
    const-string v1, "GsaVoiceInteractionSrv"

    const-string v2, "Remote Exception %s"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lhgt;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Z)Z

    .line 292
    return-void
.end method
