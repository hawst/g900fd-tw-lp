.class final Licf;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "PG"


# instance fields
.field private synthetic dxX:Lice;


# direct methods
.method constructor <init>(Lice;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Licf;->dxX:Lice;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDone(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Licf;->dxX:Lice;

    invoke-virtual {v0, p1}, Lice;->onUtteranceCompleted(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public final onError(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 137
    const/4 v1, 0x0

    .line 139
    iget-object v0, p0, Licf;->dxX:Lice;

    iget-object v2, v0, Lice;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 140
    :try_start_0
    iget-object v0, p0, Licf;->dxX:Lice;

    iget-object v0, v0, Lice;->dxO:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lick;

    .line 141
    if-eqz v0, :cond_1

    iget v3, v0, Lick;->dyi:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 142
    iget-object v1, p0, Licf;->dxX:Lice;

    iget-object v1, v1, Lice;->dxO:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v5, v0

    .line 145
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    if-eqz v5, :cond_0

    .line 149
    iget-object v0, p0, Licf;->dxX:Lice;

    iget-object v1, v5, Lick;->dyf:Ljava/lang/String;

    iget v2, v5, Lick;->dyg:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v5, Lick;->dyh:Lesk;

    const/4 v4, 0x3

    iget-object v5, v5, Lick;->dyj:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lice;->a(Ljava/lang/String;Ljava/lang/Integer;Lesk;ILjava/lang/String;)V

    .line 154
    :goto_1
    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 152
    :cond_0
    iget-object v0, p0, Licf;->dxX:Lice;

    invoke-virtual {v0, p1}, Lice;->onUtteranceCompleted(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v5, v1

    goto :goto_0
.end method

.method public final onStart(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Licf;->dxX:Lice;

    iput-boolean v1, v0, Lice;->dxT:Z

    .line 161
    iget-object v0, p0, Licf;->dxX:Lice;

    iget-boolean v0, v0, Lice;->dxU:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Licf;->dxX:Lice;

    invoke-virtual {v0}, Lice;->stop()V

    .line 164
    iget-object v0, p0, Licf;->dxX:Lice;

    iput-boolean v1, v0, Lice;->dxU:Z

    .line 166
    :cond_0
    return-void
.end method
