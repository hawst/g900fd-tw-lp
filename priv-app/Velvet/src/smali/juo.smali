.class public final Ljuo;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFz:[Ljuo;


# instance fields
.field private aez:I

.field private dMy:F

.field private eFA:Ljava/lang/String;

.field public eFB:Ljava/lang/String;

.field public eFC:[Ljun;

.field private eFD:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 389
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 390
    const/4 v0, 0x0

    iput v0, p0, Ljuo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljuo;->eFA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuo;->eFB:Ljava/lang/String;

    invoke-static {}, Ljun;->buL()[Ljun;

    move-result-object v0

    iput-object v0, p0, Ljuo;->eFC:[Ljun;

    const/4 v0, 0x0

    iput v0, p0, Ljuo;->dMy:F

    const-string v0, ""

    iput-object v0, p0, Ljuo;->eFD:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljuo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuo;->eCz:I

    .line 391
    return-void
.end method

.method public static buN()[Ljuo;
    .locals 2

    .prologue
    .line 307
    sget-object v0, Ljuo;->eFz:[Ljuo;

    if-nez v0, :cond_1

    .line 308
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 310
    :try_start_0
    sget-object v0, Ljuo;->eFz:[Ljuo;

    if-nez v0, :cond_0

    .line 311
    const/4 v0, 0x0

    new-array v0, v0, [Ljuo;

    sput-object v0, Ljuo;->eFz:[Ljuo;

    .line 313
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :cond_1
    sget-object v0, Ljuo;->eFz:[Ljuo;

    return-object v0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 301
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuo;->eFA:Ljava/lang/String;

    iget v0, p0, Ljuo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuo;->eFB:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljuo;->eFC:[Ljun;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljun;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljuo;->eFC:[Ljun;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljun;

    invoke-direct {v3}, Ljun;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljuo;->eFC:[Ljun;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljun;

    invoke-direct {v3}, Ljun;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljuo;->eFC:[Ljun;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuo;->dMy:F

    iget v0, p0, Ljuo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuo;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuo;->eFD:Ljava/lang/String;

    iget v0, p0, Ljuo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljuo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 408
    iget v0, p0, Ljuo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 409
    const/4 v0, 0x1

    iget-object v1, p0, Ljuo;->eFA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 411
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Ljuo;->eFB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 412
    iget-object v0, p0, Ljuo;->eFC:[Ljun;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljuo;->eFC:[Ljun;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 413
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljuo;->eFC:[Ljun;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 414
    iget-object v1, p0, Ljuo;->eFC:[Ljun;

    aget-object v1, v1, v0

    .line 415
    if-eqz v1, :cond_1

    .line 416
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 413
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 420
    :cond_2
    iget v0, p0, Ljuo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 421
    const/4 v0, 0x4

    iget v1, p0, Ljuo;->dMy:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 423
    :cond_3
    iget v0, p0, Ljuo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 424
    const/4 v0, 0x5

    iget-object v1, p0, Ljuo;->eFD:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 426
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 427
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 431
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 432
    iget v1, p0, Ljuo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 433
    const/4 v1, 0x1

    iget-object v2, p0, Ljuo;->eFA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 436
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Ljuo;->eFB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v1, v0

    .line 438
    iget-object v0, p0, Ljuo;->eFC:[Ljun;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljuo;->eFC:[Ljun;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 439
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljuo;->eFC:[Ljun;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 440
    iget-object v2, p0, Ljuo;->eFC:[Ljun;

    aget-object v2, v2, v0

    .line 441
    if-eqz v2, :cond_1

    .line 442
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 439
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 447
    :cond_2
    iget v0, p0, Ljuo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 448
    const/4 v0, 0x4

    iget v2, p0, Ljuo;->dMy:F

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    .line 451
    :cond_3
    iget v0, p0, Ljuo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 452
    const/4 v0, 0x5

    iget-object v2, p0, Ljuo;->eFD:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 455
    :cond_4
    return v1
.end method
