.class public final Ldqv;
.super Landroid/graphics/drawable/LayerDrawable;
.source "PG"


# instance fields
.field private Gr:I

.field private bGv:F

.field private bGw:Z

.field private bGx:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 34
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object p1, v2, v1

    aput-object p2, v2, v0

    invoke-direct {p0, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 30
    iput-boolean v0, p0, Ldqv;->bGw:Z

    .line 31
    iput-boolean v0, p0, Ldqv;->bGx:Z

    .line 38
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    if-eq v2, v3, :cond_0

    :goto_0
    const-string v1, "Use drawables with different constant state or mutate them first."

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 40
    const/16 v0, 0xff

    iput v0, p0, Ldqv;->Gr:I

    .line 41
    invoke-direct {p0}, Ldqv;->aeS()V

    .line 42
    return-void

    :cond_0
    move v0, v1

    .line 38
    goto :goto_0
.end method

.method private aeS()V
    .locals 6

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldqv;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 98
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ldqv;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 100
    iget v2, p0, Ldqv;->Gr:I

    int-to-float v2, v2

    iget v3, p0, Ldqv;->bGv:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 105
    iget v3, p0, Ldqv;->Gr:I

    sub-int/2addr v3, v2

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 106
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 108
    return-void
.end method


# virtual methods
.method public final G(F)V
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Ldqv;->bGv:F

    .line 49
    invoke-direct {p0}, Ldqv;->aeS()V

    .line 50
    invoke-virtual {p0}, Ldqv;->invalidateSelf()V

    .line 51
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldqv;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 71
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ldqv;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 73
    iget-boolean v2, p0, Ldqv;->bGx:Z

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 91
    :cond_0
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 93
    return-void
.end method

.method protected final onStateChange([I)Z
    .locals 3

    .prologue
    .line 139
    iget-boolean v0, p0, Ldqv;->bGw:Z

    if-nez v0, :cond_1

    .line 140
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget v1, p1, v0

    const v2, 0x10100a7

    if-ne v1, v2, :cond_2

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget v2, p1, v2

    aput v2, v1, v0

    :cond_0
    move-object p1, v1

    .line 142
    :cond_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->onStateChange([I)Z

    move-result v0

    return v0

    .line 140
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Ldqv;->Gr:I

    .line 65
    invoke-direct {p0}, Ldqv;->aeS()V

    .line 66
    return-void
.end method
