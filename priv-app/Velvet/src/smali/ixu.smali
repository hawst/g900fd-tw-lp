.class public final Lixu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field public dOG:Lixv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28648
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 28649
    const/4 v0, 0x0

    iput v0, p0, Lixu;->aez:I

    iput-object v1, p0, Lixu;->dOG:Lixv;

    const-string v0, ""

    iput-object v0, p0, Lixu;->afb:Ljava/lang/String;

    iput-object v1, p0, Lixu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixu;->eCz:I

    .line 28650
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 28604
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lixu;->dOG:Lixv;

    if-nez v0, :cond_1

    new-instance v0, Lixv;

    invoke-direct {v0}, Lixv;-><init>()V

    iput-object v0, p0, Lixu;->dOG:Lixv;

    :cond_1
    iget-object v0, p0, Lixu;->dOG:Lixv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixu;->afb:Ljava/lang/String;

    iget v0, p0, Lixu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 28664
    iget-object v0, p0, Lixu;->dOG:Lixv;

    if-eqz v0, :cond_0

    .line 28665
    const/4 v0, 0x1

    iget-object v1, p0, Lixu;->dOG:Lixv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 28667
    :cond_0
    iget v0, p0, Lixu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 28668
    const/4 v0, 0x2

    iget-object v1, p0, Lixu;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28670
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 28671
    return-void
.end method

.method public final baV()Z
    .locals 1

    .prologue
    .line 28640
    iget v0, p0, Lixu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28629
    iget-object v0, p0, Lixu;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 28675
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 28676
    iget-object v1, p0, Lixu;->dOG:Lixv;

    if-eqz v1, :cond_0

    .line 28677
    const/4 v1, 0x1

    iget-object v2, p0, Lixu;->dOG:Lixv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28680
    :cond_0
    iget v1, p0, Lixu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 28681
    const/4 v1, 0x2

    iget-object v2, p0, Lixu;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28684
    :cond_1
    return v0
.end method

.method public final ra(Ljava/lang/String;)Lixu;
    .locals 1

    .prologue
    .line 28632
    if-nez p1, :cond_0

    .line 28633
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28635
    :cond_0
    iput-object p1, p0, Lixu;->afb:Ljava/lang/String;

    .line 28636
    iget v0, p0, Lixu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixu;->aez:I

    .line 28637
    return-object p0
.end method
