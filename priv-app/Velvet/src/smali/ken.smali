.class public final Lken;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private bab:Ljava/lang/String;

.field private eUI:Z

.field private eUV:[B

.field private eUW:I

.field private eUX:Ljava/lang/String;

.field public eUY:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 735
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 736
    iput v1, p0, Lken;->aez:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lken;->eUV:[B

    const-string v0, ""

    iput-object v0, p0, Lken;->bab:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lken;->eUW:I

    const-string v0, ""

    iput-object v0, p0, Lken;->ajB:Ljava/lang/String;

    iput-boolean v1, p0, Lken;->eUI:Z

    const-string v0, ""

    iput-object v0, p0, Lken;->eUX:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lken;->eUY:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lken;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lken;->eCz:I

    .line 737
    return-void
.end method


# virtual methods
.method public final AH(Ljava/lang/String;)Lken;
    .locals 1

    .prologue
    .line 675
    if-nez p1, :cond_0

    .line 676
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 678
    :cond_0
    iput-object p1, p0, Lken;->ajB:Ljava/lang/String;

    .line 679
    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lken;->aez:I

    .line 680
    return-object p0
.end method

.method public final AI(Ljava/lang/String;)Lken;
    .locals 1

    .prologue
    .line 716
    if-nez p1, :cond_0

    .line 717
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 719
    :cond_0
    iput-object p1, p0, Lken;->eUX:Ljava/lang/String;

    .line 720
    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lken;->aez:I

    .line 721
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 583
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lken;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lken;->eUV:[B

    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lken;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lken;->bab:Ljava/lang/String;

    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lken;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lken;->eUW:I

    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lken;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lken;->ajB:Ljava/lang/String;

    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lken;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lken;->eUI:Z

    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lken;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lken;->eUX:Ljava/lang/String;

    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lken;->aez:I

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lken;->eUY:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lken;->eUY:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lken;->eUY:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lken;->eUY:[Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 756
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 757
    const/4 v0, 0x1

    iget-object v1, p0, Lken;->eUV:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 759
    :cond_0
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 760
    const/4 v0, 0x2

    iget-object v1, p0, Lken;->bab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 762
    :cond_1
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 763
    const/4 v0, 0x3

    iget v1, p0, Lken;->eUW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 765
    :cond_2
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 766
    const/4 v0, 0x4

    iget-object v1, p0, Lken;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 768
    :cond_3
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 769
    const/4 v0, 0x5

    iget-boolean v1, p0, Lken;->eUI:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 771
    :cond_4
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 772
    const/4 v0, 0x6

    iget-object v1, p0, Lken;->eUX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 774
    :cond_5
    iget-object v0, p0, Lken;->eUY:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lken;->eUY:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 775
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lken;->eUY:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 776
    iget-object v1, p0, Lken;->eUY:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 777
    if-eqz v1, :cond_6

    .line 778
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 775
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 782
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 783
    return-void
.end method

.method public final aI([B)Lken;
    .locals 1

    .prologue
    .line 612
    if-nez p1, :cond_0

    .line 613
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 615
    :cond_0
    iput-object p1, p0, Lken;->eUV:[B

    .line 616
    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lken;->aez:I

    .line 617
    return-object p0
.end method

.method public final acS()[B
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lken;->eUV:[B

    return-object v0
.end method

.method public final bhT()Z
    .locals 1

    .prologue
    .line 683
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhf()Z
    .locals 1

    .prologue
    .line 724
    iget v0, p0, Lken;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lken;->eUX:Ljava/lang/String;

    return-object v0
.end method

.method public final bzy()Z
    .locals 1

    .prologue
    .line 694
    iget-boolean v0, p0, Lken;->eUI:Z

    return v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lken;->ajB:Ljava/lang/String;

    return-object v0
.end method

.method public final getStreamType()I
    .locals 1

    .prologue
    .line 653
    iget v0, p0, Lken;->eUW:I

    return v0
.end method

.method public final jR(Z)Lken;
    .locals 1

    .prologue
    .line 697
    iput-boolean p1, p0, Lken;->eUI:Z

    .line 698
    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lken;->aez:I

    .line 699
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 787
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 788
    iget v2, p0, Lken;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 789
    const/4 v2, 0x1

    iget-object v3, p0, Lken;->eUV:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 792
    :cond_0
    iget v2, p0, Lken;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 793
    const/4 v2, 0x2

    iget-object v3, p0, Lken;->bab:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 796
    :cond_1
    iget v2, p0, Lken;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 797
    const/4 v2, 0x3

    iget v3, p0, Lken;->eUW:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 800
    :cond_2
    iget v2, p0, Lken;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 801
    const/4 v2, 0x4

    iget-object v3, p0, Lken;->ajB:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 804
    :cond_3
    iget v2, p0, Lken;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_4

    .line 805
    const/4 v2, 0x5

    iget-boolean v3, p0, Lken;->eUI:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 808
    :cond_4
    iget v2, p0, Lken;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_5

    .line 809
    const/4 v2, 0x6

    iget-object v3, p0, Lken;->eUX:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 812
    :cond_5
    iget-object v2, p0, Lken;->eUY:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lken;->eUY:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    move v3, v1

    .line 815
    :goto_0
    iget-object v4, p0, Lken;->eUY:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_7

    .line 816
    iget-object v4, p0, Lken;->eUY:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 817
    if-eqz v4, :cond_6

    .line 818
    add-int/lit8 v3, v3, 0x1

    .line 819
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 815
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 823
    :cond_7
    add-int/2addr v0, v2

    .line 824
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 826
    :cond_8
    return v0
.end method

.method public final tZ(I)Lken;
    .locals 1

    .prologue
    .line 656
    const/4 v0, -0x1

    iput v0, p0, Lken;->eUW:I

    .line 657
    iget v0, p0, Lken;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lken;->aez:I

    .line 658
    return-object p0
.end method
