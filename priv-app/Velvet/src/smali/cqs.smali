.class public abstract Lcqs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected final mGsaConfig:Lchk;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field protected final mHttpHelper:Ldkx;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final mLoginHelper:Lcrh;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lchk;Ldkx;Lcrh;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lcqs;->mGsaConfig:Lchk;

    .line 77
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkx;

    iput-object v0, p0, Lcqs;->mHttpHelper:Ldkx;

    .line 78
    iput-object p3, p0, Lcqs;->mLoginHelper:Lcrh;

    .line 79
    return-void
.end method


# virtual methods
.method public abstract b(Lcom/google/android/shared/search/Query;Z)Lcqt;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final iv(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 96
    iget-object v1, p0, Lcqs;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->IH()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "https"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcqs;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->IG()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "client"

    iget-object v3, p0, Lcqs;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->IT()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 97
    const-string v1, "delq"

    invoke-virtual {v2, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 98
    const-string v1, "callback"

    const-string v3, "google.sbox.d0"

    invoke-virtual {v2, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 99
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v3

    .line 100
    const-string v1, "mobilepersonalfeeds"

    iget-object v4, p0, Lcqs;->mLoginHelper:Lcrh;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcqs;->mLoginHelper:Lcrh;

    iget-object v5, p0, Lcqs;->mLoginHelper:Lcrh;

    invoke-virtual {v5}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v1, v6, v7}, Lcrh;->b(Landroid/accounts/Account;Ljava/lang/String;J)Lcrm;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    if-eqz v1, :cond_0

    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "GoogleLogin auth="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Lcrm;->bhj:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-nez v1, :cond_2

    .line 119
    :goto_1
    return v0

    .line 100
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 106
    :cond_2
    :try_start_0
    new-instance v1, Ldlb;

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 108
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldlb;->setUseCaches(Z)V

    .line 109
    iget-object v2, p0, Lcqs;->mHttpHelper:Ldkx;

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, Ldkx;->b(Ldlb;I)Ljava/lang/String;
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    .line 118
    :catch_1
    move-exception v1

    goto :goto_1
.end method
