.class public final Lgth;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/search/shared/actions/CommunicationAction;Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 132
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 133
    :cond_0
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    .line 137
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_2

    .line 139
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Lcom/google/android/search/shared/contact/RelationshipStatus;)V

    .line 141
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;->i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcky;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 42
    if-nez p0, :cond_1

    move-object v0, v7

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcky;->Pn()Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v6

    .line 48
    if-eqz v6, :cond_3

    .line 49
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/CommunicationAction;->afZ()Ldzb;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/CommunicationAction;->agc()I

    move-result v5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lgth;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    invoke-static {v6, v0}, Lgth;->a(Lcom/google/android/search/shared/actions/CommunicationAction;Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_3
    invoke-virtual {p0}, Lcky;->Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v8

    .line 53
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 54
    invoke-virtual {p0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 55
    instance-of v1, v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-nez v1, :cond_4

    .line 56
    const-string v1, "DisambiguationUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not a modular action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 57
    goto :goto_0

    :cond_4
    move-object v6, v0

    .line 59
    check-cast v6, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v8}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v8}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akB()Ldzb;

    move-result-object v1

    const/4 v5, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lgth;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v7

    goto :goto_0

    :cond_5
    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-direct {v1, v8, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    invoke-virtual {v6, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->e(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Lcom/google/android/search/shared/actions/modular/ModularAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aje()Ljqb;

    move-result-object v2

    invoke-virtual {v1}, Ljrl;->btq()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v1, v2, Ljqb;->eyg:Ljrg;

    if-eqz v1, :cond_0

    iget-object v1, v2, Ljqb;->eyg:Ljrg;

    iput-object v7, v1, Ljrg;->eAo:Ljqv;

    goto/16 :goto_0

    :cond_6
    move-object v0, v7

    .line 63
    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 207
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    .line 208
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 209
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 211
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alS()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 216
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 217
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    :cond_1
    new-instance v2, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v2, p1, p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Ldzb;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 234
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_3

    .line 235
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Landroid/os/Parcelable;Z)V

    .line 239
    :goto_1
    return-object v2

    .line 222
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alS()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 223
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 237
    :cond_3
    invoke-virtual {v2, v1, v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->c(Ljava/util/List;Z)V

    goto :goto_1
.end method

.method private static a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 123
    :goto_0
    return-object v0

    .line 118
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lgth;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lgti;

    move-result-object v0

    .line 120
    iget v1, v0, Lgti;->cUX:I

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, p5}, Litu;->mC(I)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 123
    iget-object v0, v0, Lgti;->cUW:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 245
    new-instance v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v1, p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 271
    :goto_0
    return-object v0

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v0

    .line 252
    if-nez v0, :cond_1

    move-object v0, v1

    .line 254
    goto :goto_0

    .line 257
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 259
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 260
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v4

    .line 261
    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 262
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 266
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 267
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->b(Landroid/os/Parcelable;Z)V

    :goto_2
    move-object v0, v1

    .line 271
    goto :goto_0

    .line 269
    :cond_4
    invoke-virtual {v1, v2, v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->d(Ljava/util/List;Z)V

    goto :goto_2
.end method

.method public static a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lgti;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 162
    const/4 v1, 0x0

    .line 165
    if-eqz p2, :cond_1

    .line 166
    invoke-static {p0, p1, p2}, Lgth;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    .line 167
    const/16 v0, 0x86

    .line 173
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v2, 0x1

    move v3, v2

    .line 177
    :goto_1
    if-eqz p3, :cond_5

    .line 178
    if-eqz v1, :cond_3

    .line 179
    invoke-static {v1, p3}, Lgth;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 185
    :goto_2
    const/16 v1, 0x88

    move-object v2, v0

    .line 189
    :goto_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-nez v0, :cond_4

    .line 194
    :cond_0
    new-instance v0, Lgti;

    invoke-direct {v0, v2, v1}, Lgti;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;I)V

    .line 201
    :goto_4
    return-object v0

    .line 168
    :cond_1
    if-eqz p4, :cond_6

    .line 169
    invoke-static {p0, p4}, Lgth;->b(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    .line 170
    const/16 v0, 0x87

    goto :goto_0

    :cond_2
    move v3, v2

    .line 173
    goto :goto_1

    .line 181
    :cond_3
    invoke-static {p0, p3}, Lgth;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    goto :goto_2

    .line 198
    :cond_4
    new-instance v0, Lgti;

    const/16 v1, 0x89

    invoke-direct {v0, p0, v1}, Lgti;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;I)V

    goto :goto_4

    :cond_5
    move-object v2, v1

    move v1, v0

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public static b(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 279
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 284
    if-gtz v0, :cond_0

    .line 285
    const-string v1, "DisambiguationUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Selection index is less than 1: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :goto_0
    return-object p0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    const-string v1, "DisambiguationUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Selection string cannot be parsed as an integer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 289
    :cond_0
    new-instance v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v1, p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 294
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v2

    .line 295
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v0, v3, :cond_1

    .line 296
    const-string v1, "DisambiguationUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Selection index exceeds the number of contacts: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_1
    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->b(Landroid/os/Parcelable;Z)V

    :goto_1
    move-object p0, v1

    .line 314
    goto :goto_0

    .line 304
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v2

    .line 305
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v0, v3, :cond_3

    .line 306
    const-string v1, "DisambiguationUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Selection index exceeds the number of persons: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 311
    :cond_3
    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Landroid/os/Parcelable;Z)V

    goto :goto_1
.end method
