.class public final Ljpl;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private air:Ljava/lang/String;

.field private dWc:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 72
    iput v1, p0, Ljpl;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljpl;->air:Ljava/lang/String;

    iput-boolean v1, p0, Ljpl;->dWc:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljpl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpl;->eCz:I

    .line 73
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpl;->air:Ljava/lang/String;

    iget v0, p0, Ljpl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpl;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljpl;->dWc:Z

    iget v0, p0, Ljpl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpl;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Ljpl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x1

    iget-object v1, p0, Ljpl;->air:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 90
    :cond_0
    iget v0, p0, Ljpl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 91
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljpl;->dWc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 93
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 94
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 98
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 99
    iget v1, p0, Ljpl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x1

    iget-object v2, p0, Ljpl;->air:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_0
    iget v1, p0, Ljpl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 104
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljpl;->dWc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 107
    :cond_1
    return v0
.end method
