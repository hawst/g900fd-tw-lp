.class public final Litm;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHP:D

.field private dHQ:Z

.field private dHR:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4086
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4087
    iput v2, p0, Litm;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Litm;->dHP:D

    iput-boolean v2, p0, Litm;->dHQ:Z

    iput v2, p0, Litm;->dHR:I

    const/4 v0, 0x0

    iput-object v0, p0, Litm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litm;->eCz:I

    .line 4088
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 4005
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Litm;->dHP:D

    iget v0, p0, Litm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Litm;->dHQ:Z

    iget v0, p0, Litm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litm;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litm;->dHR:I

    iget v0, p0, Litm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litm;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 4103
    iget v0, p0, Litm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4104
    const/4 v0, 0x1

    iget-wide v2, p0, Litm;->dHP:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 4106
    :cond_0
    iget v0, p0, Litm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4107
    const/4 v0, 0x2

    iget-boolean v1, p0, Litm;->dHQ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4109
    :cond_1
    iget v0, p0, Litm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 4110
    const/4 v0, 0x3

    iget v1, p0, Litm;->dHR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4112
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4113
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 4117
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4118
    iget v1, p0, Litm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4119
    const/4 v1, 0x1

    iget-wide v2, p0, Litm;->dHP:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 4122
    :cond_0
    iget v1, p0, Litm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4123
    const/4 v1, 0x2

    iget-boolean v2, p0, Litm;->dHQ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4126
    :cond_1
    iget v1, p0, Litm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 4127
    const/4 v1, 0x3

    iget v2, p0, Litm;->dHR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4130
    :cond_2
    return v0
.end method
