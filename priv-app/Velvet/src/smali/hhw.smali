.class public final Lhhw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhhu;
.implements Lhje;


# instance fields
.field private final dK:Ljava/lang/Object;

.field final dhD:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final dhE:Lenw;

.field private final dhF:Lenw;

.field private dhG:J

.field dhH:Z

.field private dhI:I

.field private dhJ:I

.field private dhK:Z

.field private dhL:Z

.field dhM:Lhhv;

.field private dhN:I

.field private dhO:Ljava/lang/String;

.field private dhP:J

.field private dhQ:Lhiw;

.field private dhR:Z

.field private final dhS:Lhiv;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final jv:Llb;

.field private final kv:Lle;

.field final mAudioManager:Landroid/media/AudioManager;

.field final mBluetoothController:Lhjb;

.field private final mClock:Lemp;

.field final mExecutor:Ljava/util/concurrent/Executor;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lemp;Lhym;Landroid/media/AudioManager;Ljava/util/concurrent/Executor;Lenw;Lenw;Lhjb;Lhiv;)V
    .locals 4
    .param p8    # Lhiv;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lhhx;

    invoke-direct {v0, p0}, Lhhx;-><init>(Lhhw;)V

    iput-object v0, p0, Lhhw;->dhD:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 102
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhhw;->dhG:J

    .line 108
    iput-boolean v2, p0, Lhhw;->dhH:Z

    .line 116
    const/4 v0, 0x3

    iput v0, p0, Lhhw;->dhI:I

    .line 126
    const/4 v0, 0x1

    iput v0, p0, Lhhw;->dhJ:I

    .line 132
    iput-boolean v2, p0, Lhhw;->dhK:Z

    .line 138
    iput-boolean v2, p0, Lhhw;->dhL:Z

    .line 141
    iput-object v3, p0, Lhhw;->dhM:Lhhv;

    .line 146
    const/16 v0, 0xc

    iput v0, p0, Lhhw;->dhN:I

    .line 164
    iput-object v3, p0, Lhhw;->dhQ:Lhiw;

    .line 166
    iput-boolean v2, p0, Lhhw;->dhR:Z

    .line 190
    new-instance v0, Lhhz;

    invoke-direct {v0, p0}, Lhhz;-><init>(Lhhw;)V

    iput-object v0, p0, Lhhw;->kv:Lle;

    .line 192
    new-instance v0, Llc;

    invoke-direct {v0}, Llc;-><init>()V

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Llc;->h(Ljava/lang/String;)Llc;

    move-result-object v0

    invoke-virtual {v0}, Llc;->bK()Llb;

    move-result-object v0

    iput-object v0, p0, Lhhw;->jv:Llb;

    .line 175
    iput-object p1, p0, Lhhw;->mClock:Lemp;

    .line 176
    iput-object p2, p0, Lhhw;->mSettings:Lhym;

    .line 177
    iput-object p3, p0, Lhhw;->mAudioManager:Landroid/media/AudioManager;

    .line 178
    iput-object p7, p0, Lhhw;->mBluetoothController:Lhjb;

    .line 179
    iput-object p4, p0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    .line 180
    iput-object p5, p0, Lhhw;->dhE:Lenw;

    .line 181
    iput-object p6, p0, Lhhw;->dhF:Lenw;

    .line 182
    iput-object p8, p0, Lhhw;->dhS:Lhiv;

    .line 183
    return-void
.end method

.method private aPA()V
    .locals 5

    .prologue
    .line 747
    iget-object v0, p0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhid;

    const-string v2, "AudioRouter"

    const-string v3, "maybeAbandonAudioFocus"

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-direct {v1, p0, v2, v3, v4}, Lhid;-><init>(Lhhw;Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 753
    return-void
.end method

.method private aPt()Z
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    .line 333
    iget v0, p0, Lhhw;->dhI:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aPw()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, -0x1

    .line 407
    iget-object v1, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v1}, Lhjb;->aPQ()I

    move-result v1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    iget-object v1, p0, Lhhw;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lhhw;->dhP:J

    sub-long/2addr v2, v4

    .line 417
    iget v1, p0, Lhhw;->dhJ:I

    if-eq v1, v6, :cond_2

    iget v1, p0, Lhhw;->dhJ:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_3

    .line 420
    :cond_2
    iget-object v1, p0, Lhhw;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v1

    .line 421
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 422
    iget-object v4, p0, Lhhw;->mSettings:Lhym;

    invoke-virtual {v4, v1}, Lhym;->oL(Ljava/lang/String;)I

    move-result v1

    .line 426
    :goto_1
    if-ne v1, v0, :cond_5

    .line 427
    iget v0, p0, Lhhw;->dhJ:I

    if-ne v0, v6, :cond_4

    const/16 v0, 0x96

    .line 433
    :cond_3
    :goto_2
    int-to-long v0, v0

    sub-long/2addr v0, v2

    .line 436
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 440
    :try_start_0
    iget-object v2, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 441
    :catch_0
    move-exception v0

    .line 442
    const-string v1, "AudioRouter"

    const-string v2, "Thread was interrupted, aborting await"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 427
    :cond_4
    const/16 v0, 0x7d0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    move v1, v0

    goto :goto_1
.end method

.method private aPx()Z
    .locals 9

    .prologue
    const/16 v8, 0xb

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 539
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    .line 540
    iget v0, p0, Lhhw;->dhI:I

    if-nez v0, :cond_0

    const-wide/16 v0, 0x3e8

    .line 542
    :goto_0
    iget-object v4, p0, Lhhw;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->uptimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 544
    :goto_1
    iget-object v6, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v6}, Lhjb;->aPO()I

    move-result v6

    if-nez v6, :cond_1

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_1

    iget v6, p0, Lhhw;->dhN:I

    if-eq v6, v8, :cond_1

    .line 547
    :try_start_0
    iget-object v6, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v6, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    iget-object v0, p0, Lhhw;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    sub-long v0, v4, v0

    goto :goto_1

    .line 540
    :cond_0
    const-wide/16 v0, 0xc8

    goto :goto_0

    .line 548
    :catch_0
    move-exception v0

    .line 549
    const-string v1, "AudioRouter"

    const-string v3, "Thread was interrupted, aborting await"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 574
    :goto_2
    return v0

    .line 555
    :cond_1
    iget v0, p0, Lhhw;->dhN:I

    if-ne v0, v8, :cond_2

    move v0, v2

    .line 557
    goto :goto_2

    .line 560
    :cond_2
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPO()I

    move-result v0

    .line 561
    iget-object v1, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v1}, Lhjb;->aPP()Lhji;

    move-result-object v1

    .line 562
    if-nez v0, :cond_3

    .line 563
    const-string v0, "AudioRouter"

    const-string v1, "Timed out waiting for BT device state"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    iput-boolean v3, p0, Lhhw;->dhK:Z

    move v0, v2

    .line 565
    goto :goto_2

    .line 566
    :cond_3
    const/4 v4, 0x2

    if-eq v0, v4, :cond_4

    if-nez v1, :cond_5

    :cond_4
    move v0, v2

    .line 568
    goto :goto_2

    .line 572
    :cond_5
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPP()Lhji;

    move v0, v3

    .line 574
    goto :goto_2
.end method

.method private aPy()Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0xb

    const/4 v2, 0x0

    .line 583
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    .line 584
    iget-object v0, p0, Lhhw;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNq:Ljzd;

    invoke-virtual {v0}, Ljzd;->bwN()I

    move-result v0

    int-to-long v0, v0

    .line 585
    iget-object v4, p0, Lhhw;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->uptimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 587
    :goto_0
    iget-object v6, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v6}, Lhjb;->aPQ()I

    move-result v6

    if-eq v6, v8, :cond_0

    iget-boolean v6, p0, Lhhw;->dhL:Z

    if-eqz v6, :cond_1

    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-lez v6, :cond_1

    iget v6, p0, Lhhw;->dhN:I

    if-eq v6, v8, :cond_1

    .line 590
    :try_start_0
    iget-object v6, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v6, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    iget-object v0, p0, Lhhw;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    sub-long v0, v4, v0

    goto :goto_0

    .line 591
    :catch_0
    move-exception v0

    .line 592
    const-string v1, "AudioRouter"

    const-string v3, "Thread was interrupted, aborting await"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 619
    :goto_1
    return v0

    .line 598
    :cond_1
    iget v0, p0, Lhhw;->dhN:I

    if-ne v0, v8, :cond_2

    move v0, v2

    .line 600
    goto :goto_1

    .line 603
    :cond_2
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPQ()I

    move-result v0

    .line 604
    if-ne v0, v8, :cond_3

    .line 605
    const-string v0, "AudioRouter"

    const-string v1, "SCO connection timed out"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iput-boolean v3, p0, Lhhw;->dhK:Z

    .line 607
    iget-object v0, p0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhib;

    const-string v3, "AudioRouter"

    const-string v4, "awaitBluetoothScoConnectionLocked: stopSco"

    new-array v5, v2, [I

    invoke-direct {v1, p0, v3, v4, v5}, Lhib;-><init>(Lhhw;Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    move v0, v2

    .line 613
    goto :goto_1

    .line 614
    :cond_3
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 615
    const-string v0, "AudioRouter"

    const-string v1, "SCO connection attempt failed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 616
    goto :goto_1

    :cond_4
    move v0, v3

    .line 619
    goto :goto_1
.end method

.method private aPz()V
    .locals 5

    .prologue
    .line 718
    iget-object v0, p0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhic;

    const-string v2, "AudioRouter"

    const-string v3, "maybeRequestAudioFocus"

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-direct {v1, p0, v2, v3, v4}, Lhic;-><init>(Lhhw;Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 743
    return-void
.end method

.method private static kU(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 342
    if-eq p0, v0, :cond_0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private nZ(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v0, 0x1

    const/16 v5, 0xc

    .line 497
    iget-object v2, p0, Lhhw;->dK:Ljava/lang/Object;

    .line 501
    :try_start_0
    iget v2, p0, Lhhw;->dhN:I

    if-ne v2, v5, :cond_0

    move v2, v0

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "awaitBluetoothRoutingLocked: mAwaitState="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lhhw;->dhN:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Was expecting AWAIT_STATE_NONE(12"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "). Other states are AWAITING(10"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") and CANCELLED(11"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lifv;->d(ZLjava/lang/Object;)V

    .line 508
    const/16 v2, 0xa

    iput v2, p0, Lhhw;->dhN:I

    .line 509
    iput-object p1, p0, Lhhw;->dhO:Ljava/lang/String;

    .line 512
    iget-object v2, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v2}, Lhjb;->aPQ()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-ne v2, v5, :cond_1

    .line 514
    iput v5, p0, Lhhw;->dhN:I

    .line 533
    iput-object v6, p0, Lhhw;->dhO:Ljava/lang/String;

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 501
    goto :goto_0

    .line 515
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lhhw;->dhK:Z

    if-eqz v2, :cond_3

    .line 516
    const-string v2, "AudioRouter"

    const-string v3, "SCO connection has failed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget v2, p0, Lhhw;->dhI:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    .line 532
    :goto_2
    iput v5, p0, Lhhw;->dhN:I

    .line 533
    iput-object v6, p0, Lhhw;->dhO:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move v0, v1

    .line 517
    goto :goto_2

    .line 521
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lhhw;->aPx()Z

    move-result v2

    if-nez v2, :cond_5

    .line 522
    iget v2, p0, Lhhw;->dhI:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_4

    .line 532
    :goto_3
    iput v5, p0, Lhhw;->dhN:I

    .line 533
    iput-object v6, p0, Lhhw;->dhO:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move v0, v1

    .line 522
    goto :goto_3

    .line 526
    :cond_5
    :try_start_3
    invoke-direct {p0}, Lhhw;->aPy()Z

    move-result v2

    if-nez v2, :cond_7

    .line 527
    iget v2, p0, Lhhw;->dhI:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_6

    .line 532
    :goto_4
    iput v5, p0, Lhhw;->dhN:I

    .line 533
    iput-object v6, p0, Lhhw;->dhO:Ljava/lang/String;

    goto :goto_1

    :cond_6
    move v0, v1

    .line 527
    goto :goto_4

    .line 529
    :cond_7
    iput v5, p0, Lhhw;->dhN:I

    .line 533
    iput-object v6, p0, Lhhw;->dhO:Ljava/lang/String;

    goto :goto_1

    .line 532
    :catchall_0
    move-exception v0

    iput v5, p0, Lhhw;->dhN:I

    .line 533
    iput-object v6, p0, Lhhw;->dhO:Ljava/lang/String;

    throw v0
.end method


# virtual methods
.method public final a(IILhhv;Z)V
    .locals 8
    .param p3    # Lhhv;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/4 v7, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 207
    if-eqz p4, :cond_0

    iget-object v3, p0, Lhhw;->dhS:Lhiv;

    if-eqz v3, :cond_0

    .line 214
    iget-object v3, p0, Lhhw;->dhS:Lhiv;

    iget-object v4, p0, Lhhw;->jv:Llb;

    iget-object v5, p0, Lhhw;->kv:Lle;

    iget-object v3, v3, Lhiv;->atY:Lld;

    const/4 v6, 0x4

    invoke-virtual {v3, v4, v5, v6}, Lld;->a(Llb;Lle;I)V

    .line 218
    :cond_0
    invoke-static {p1}, Lhhw;->kU(I)Z

    move-result v3

    if-eqz v3, :cond_1

    if-ne p2, v1, :cond_1

    .line 219
    const-string v3, "AudioRouter"

    const-string v4, "CONNECTION_TYPE_NONE for BT route, forcing BT off."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v0

    .line 223
    :cond_1
    iget-object v3, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v3

    .line 224
    if-eq p1, v7, :cond_7

    .line 225
    :try_start_0
    invoke-direct {p0}, Lhhw;->aPz()V

    .line 229
    :goto_0
    iput-object p3, p0, Lhhw;->dhM:Lhhv;

    .line 232
    iget-object v4, p0, Lhhw;->dhS:Lhiv;

    if-eqz v4, :cond_d

    .line 235
    if-eqz p4, :cond_2

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lhhw;->dhQ:Lhiw;

    if-nez v0, :cond_2

    .line 240
    iget-object v0, p0, Lhhw;->dhS:Lhiv;

    invoke-virtual {v0}, Lhiv;->aPK()Lhiw;

    move-result-object v0

    .line 241
    iget-object v4, p0, Lhhw;->dhS:Lhiv;

    invoke-virtual {v4}, Lhiv;->aPJ()Lhiw;

    move-result-object v4

    .line 243
    iget-object v5, v0, Lhiw;->mb:Llm;

    iget-object v5, v5, Llm;->kN:Ljava/lang/String;

    iget-object v6, v4, Lhiw;->mb:Llm;

    iget-object v6, v6, Llm;->kN:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 245
    iget-object v2, p0, Lhhw;->dhS:Lhiv;

    invoke-virtual {v2, v4}, Lhiv;->a(Lhiw;)V

    .line 246
    iput-object v0, p0, Lhhw;->dhQ:Lhiw;

    move v2, v1

    .line 250
    :cond_2
    if-eq p1, v7, :cond_3

    if-nez p1, :cond_d

    :cond_3
    iget-object v0, p0, Lhhw;->dhQ:Lhiw;

    if-eqz v0, :cond_d

    .line 254
    iget-object v0, p0, Lhhw;->dhQ:Lhiw;

    iget-object v0, v0, Lhiw;->mb:Llm;

    iget-object v4, v0, Llm;->kN:Ljava/lang/String;

    .line 255
    iget-object v0, p0, Lhhw;->dhS:Lhiv;

    iget-object v0, v0, Lhiv;->atY:Lld;

    invoke-static {}, Lld;->getRoutes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llm;

    .line 256
    iget-object v0, v0, Llm;->kN:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 262
    iget-object v0, p0, Lhhw;->dhS:Lhiv;

    iget-object v2, p0, Lhhw;->dhQ:Lhiw;

    invoke-virtual {v0, v2}, Lhiv;->a(Lhiw;)V

    move v0, v1

    .line 267
    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lhhw;->dhQ:Lhiw;

    .line 273
    :goto_2
    if-nez p2, :cond_5

    move p2, v1

    .line 275
    :cond_5
    iget v1, p0, Lhhw;->dhI:I

    if-ne p1, v1, :cond_6

    iget v1, p0, Lhhw;->dhJ:I

    if-eq p2, v1, :cond_b

    .line 278
    :cond_6
    const-string v1, "AudioRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Route changed: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lhhw;->dhB:[Ljava/lang/String;

    iget v5, p0, Lhhw;->dhI:I

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "->"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lhhw;->dhB:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lhhw;->dhC:[Ljava/lang/String;

    iget v5, p0, Lhhw;->dhJ:I

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "->"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lhhw;->dhC:[Ljava/lang/String;

    aget-object v4, v4, p2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget v1, p0, Lhhw;->dhI:I

    .line 283
    iput p1, p0, Lhhw;->dhI:I

    .line 284
    iput p2, p0, Lhhw;->dhJ:I

    .line 286
    iget-object v2, p0, Lhhw;->dhS:Lhiv;

    if-eqz v2, :cond_9

    if-eqz v0, :cond_9

    .line 290
    iget-object v0, p0, Lhhw;->dhS:Lhiv;

    iget-object v1, p0, Lhhw;->kv:Lle;

    iget-object v0, v0, Lhiv;->atY:Lld;

    invoke-virtual {v0, v1}, Lld;->a(Lle;)V

    .line 291
    iget v0, p0, Lhhw;->dhI:I

    if-ne v0, v7, :cond_8

    .line 292
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhhw;->dhG:J

    .line 299
    :goto_3
    monitor-exit v3

    .line 313
    :goto_4
    return-void

    .line 227
    :cond_7
    invoke-direct {p0}, Lhhw;->aPA()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 294
    :cond_8
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lhhw;->dhR:Z

    goto :goto_3

    .line 301
    :cond_9
    invoke-static {v1}, Lhhw;->kU(I)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-static {p1}, Lhhw;->kU(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhhw;->dhK:Z

    .line 305
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhhw;->dhL:Z

    .line 306
    iget-object v0, p0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhia;

    const-string v2, "AudioRouter"

    const-string v4, "updateRoute: synchronizeBluetoothState"

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-direct {v1, p0, v2, v4, v5}, Lhia;-><init>(Lhhw;Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 313
    :cond_b
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :cond_c
    move v0, v2

    goto/16 :goto_1

    :cond_d
    move v0, v2

    goto/16 :goto_2
.end method

.method final aPB()V
    .locals 2

    .prologue
    .line 760
    iget-boolean v0, p0, Lhhw;->dhH:Z

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lhhw;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lhhw;->dhD:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 763
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 765
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhhw;->dhH:Z

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 767
    :cond_1
    const-string v0, "AudioRouter"

    const-string v1, "Unable to release audio focus"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final aPC()V
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lhhw;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 786
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 787
    :try_start_0
    invoke-virtual {p0}, Lhhw;->aPv()V

    .line 788
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 789
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPo()V
    .locals 2

    .prologue
    .line 668
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 669
    :try_start_0
    invoke-direct {p0}, Lhhw;->aPt()Z

    move-result v0

    if-nez v0, :cond_0

    .line 670
    invoke-direct {p0}, Lhhw;->aPz()V

    .line 672
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPp()V
    .locals 2

    .prologue
    .line 678
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 679
    :try_start_0
    invoke-direct {p0}, Lhhw;->aPt()Z

    move-result v0

    if-nez v0, :cond_0

    .line 680
    invoke-direct {p0}, Lhhw;->aPA()V

    .line 682
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPq()I
    .locals 3

    .prologue
    .line 687
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 688
    :try_start_0
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPQ()I

    move-result v0

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    .line 690
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 695
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    monitor-exit v1

    goto :goto_0

    .line 697
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPr()I
    .locals 3

    .prologue
    .line 702
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 703
    :try_start_0
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPQ()I

    move-result v0

    const/16 v2, 0xc

    if-ne v0, v2, :cond_0

    .line 705
    const/4 v0, 0x3

    monitor-exit v1

    .line 711
    :goto_0
    return v0

    .line 706
    :cond_0
    iget-object v0, p0, Lhhw;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 708
    const/4 v0, 0x2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 713
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 711
    :cond_1
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0
.end method

.method public final aPs()Z
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p0}, Lhhw;->aPu()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhhw;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aPu()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    .line 338
    iget v0, p0, Lhhw;->dhI:I

    invoke-static {v0}, Lhhw;->kU(I)Z

    move-result v0

    return v0
.end method

.method final aPv()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 351
    iget-object v0, p0, Lhhw;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 352
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 353
    :try_start_0
    iget-boolean v0, p0, Lhhw;->dhL:Z

    if-eqz v0, :cond_0

    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhhw;->dhL:Z

    .line 355
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 357
    :cond_0
    invoke-virtual {p0}, Lhhw;->aPu()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 359
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPN()V

    .line 360
    iget-boolean v0, p0, Lhhw;->dhK:Z

    if-eqz v0, :cond_1

    .line 362
    monitor-exit v1

    .line 396
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPQ()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPO()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPP()Lhji;

    .line 367
    const-string v0, "AudioRouter"

    const-string v2, "BT required, starting SCO"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    iget v2, p0, Lhhw;->dhJ:I

    invoke-virtual {v0, v2}, Lhjb;->kZ(I)V

    .line 396
    :cond_2
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 384
    :cond_3
    :try_start_1
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPQ()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 386
    const-string v0, "AudioRouter"

    const-string v2, "BT not required, stopping SCO"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v0, p0, Lhhw;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPR()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final bg(II)V
    .locals 10

    .prologue
    const/16 v9, 0xc

    const/16 v8, 0xb

    .line 794
    iget-object v0, p0, Lhhw;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 800
    iget-object v6, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v6

    .line 801
    :try_start_0
    invoke-virtual {p0}, Lhhw;->aPu()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 802
    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lhhw;->dhK:Z

    if-nez v0, :cond_0

    .line 803
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhhw;->dhK:Z

    .line 804
    if-ne p1, v9, :cond_2

    .line 805
    const-string v0, "AudioRouter"

    const-string v1, "BT route lost"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    iget-object v0, p0, Lhhw;->dhM:Lhhv;

    if-eqz v0, :cond_0

    .line 807
    iget-object v5, p0, Lhhw;->dhM:Lhhv;

    .line 808
    iget-object v7, p0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lhie;

    const-string v2, "AudioRouter"

    const-string v3, "onScoStateChanged: onRouteLost"

    const/4 v1, 0x0

    new-array v4, v1, [I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lhie;-><init>(Lhhw;Ljava/lang/String;Ljava/lang/String;[ILhhv;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 820
    :cond_0
    :goto_0
    if-ne p2, v9, :cond_1

    if-ne p1, v8, :cond_1

    .line 821
    iget-object v0, p0, Lhhw;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhhw;->dhP:J

    .line 826
    :cond_1
    invoke-virtual {p0}, Lhhw;->aPv()V

    .line 827
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 828
    monitor-exit v6

    return-void

    .line 816
    :cond_2
    if-ne p1, v8, :cond_0

    .line 817
    const-string v0, "AudioRouter"

    const-string v1, "BT connection failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 828
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final gr(Z)V
    .locals 2

    .prologue
    .line 648
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 649
    if-eqz p1, :cond_0

    :try_start_0
    invoke-direct {p0}, Lhhw;->aPt()Z

    move-result v0

    if-nez v0, :cond_0

    .line 650
    invoke-direct {p0}, Lhhw;->aPz()V

    .line 652
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final gs(Z)V
    .locals 2

    .prologue
    .line 658
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 659
    if-eqz p1, :cond_0

    :try_start_0
    invoke-direct {p0}, Lhhw;->aPt()Z

    move-result v0

    if-nez v0, :cond_0

    .line 660
    invoke-direct {p0}, Lhhw;->aPA()V

    .line 662
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final mB(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 626
    iget-object v1, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 627
    :try_start_0
    iget v0, p0, Lhhw;->dhN:I

    const/16 v2, 0xa

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lhhw;->dhO:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhhw;->dhO:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    const/16 v0, 0xb

    iput v0, p0, Lhhw;->dhN:I

    .line 631
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 635
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final nY(Ljava/lang/String;)Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x9c4

    const-wide/16 v8, 0x7d0

    .line 451
    iget-object v0, p0, Lhhw;->dhF:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 452
    invoke-static {}, Lenu;->auQ()V

    .line 453
    new-instance v0, Lerc;

    invoke-direct {v0}, Lerc;-><init>()V

    invoke-virtual {v0}, Lerc;->avy()Lerc;

    move-result-object v1

    .line 454
    iget-object v2, p0, Lhhw;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 456
    :try_start_0
    invoke-virtual {p0}, Lhhw;->aPu()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 457
    invoke-direct {p0, p1}, Lhhw;->nZ(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 478
    :try_start_1
    invoke-direct {p0}, Lhhw;->aPw()V

    .line 479
    invoke-virtual {v1}, Lerc;->avz()I

    move-result v1

    .line 480
    int-to-long v4, v1

    cmp-long v3, v4, v8

    if-lez v3, :cond_0

    .line 481
    const-string v3, "AudioRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "awaitRouting took "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "ms"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    return v0

    .line 459
    :cond_1
    :try_start_2
    iget-boolean v0, p0, Lhhw;->dhR:Z

    if-eqz v0, :cond_2

    .line 463
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhhw;->dhR:Z

    .line 464
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lhhw;->dhG:J

    sub-long/2addr v4, v6

    .line 466
    const-wide/16 v6, 0x9c4

    sub-long v4, v10, v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v4

    .line 469
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    .line 471
    :try_start_3
    iget-object v0, p0, Lhhw;->dK:Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 475
    :cond_2
    :goto_1
    :try_start_4
    invoke-direct {p0}, Lhhw;->aPw()V

    .line 479
    invoke-virtual {v1}, Lerc;->avz()I

    move-result v0

    .line 480
    int-to-long v4, v0

    cmp-long v1, v4, v8

    if-lez v1, :cond_3

    .line 481
    const-string v1, "AudioRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "awaitRouting took "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "ms"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_3
    monitor-exit v2

    const/4 v0, 0x1

    goto :goto_0

    .line 478
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lhhw;->aPw()V

    .line 479
    invoke-virtual {v1}, Lerc;->avz()I

    move-result v1

    .line 480
    int-to-long v4, v1

    cmp-long v3, v4, v8

    if-lez v3, :cond_4

    .line 481
    const-string v3, "AudioRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "awaitRouting took "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "ms"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_4
    throw v0

    .line 486
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
