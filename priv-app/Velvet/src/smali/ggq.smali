.class public final Lggq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcox;


# static fields
.field private static final cJG:Ljava/util/Locale;


# instance fields
.field private cJH:Ljta;

.field private final mSearchSettings:Lcke;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lggq;->cJG:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>(Lcke;Ljta;)V
    .locals 0
    .param p1    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljta;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lggq;->mSearchSettings:Lcke;

    .line 32
    iput-object p2, p0, Lggq;->cJH:Ljta;

    .line 33
    return-void
.end method

.method private a(Ljta;Z)V
    .locals 1

    .prologue
    .line 138
    if-nez p2, :cond_0

    .line 139
    iput-object p1, p0, Lggq;->cJH:Ljta;

    .line 141
    :cond_0
    iget-object v0, p0, Lggq;->mSearchSettings:Lcke;

    invoke-interface {v0, p1}, Lcke;->a(Ljta;)V

    .line 142
    return-void
.end method


# virtual methods
.method public final BK()Lcke;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lggq;->mSearchSettings:Lcke;

    return-object v0
.end method

.method public final Ez()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x5

    return v0
.end method

.method public final a(Ljje;Z)V
    .locals 1
    .param p1    # Ljje;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 124
    iget-object v0, p1, Ljje;->eoF:Ljta;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p1, Ljje;->eoF:Ljta;

    invoke-direct {p0, v0, p2}, Lggq;->a(Ljta;Z)V

    .line 127
    :cond_0
    return-void
.end method

.method public final ci(Z)V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lggq;->a(Ljta;Z)V

    .line 132
    return-void
.end method

.method public final mD(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    if-eqz p1, :cond_0

    iget-object v2, p0, Lggq;->cJH:Ljta;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 54
    :cond_1
    :goto_0
    return v0

    .line 42
    :cond_2
    sget-object v2, Lggq;->cJG:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 44
    iget-object v2, p0, Lggq;->cJH:Ljta;

    iget-object v5, v2, Ljta;->eDj:[Ljtb;

    array-length v6, v5

    move v3, v1

    :goto_1
    if-ge v3, v6, :cond_4

    aget-object v2, v5, v3

    .line 45
    invoke-virtual {v2}, Ljtb;->getCanonicalName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 48
    iget-object v7, v2, Ljtb;->eDl:[Ljava/lang/String;

    array-length v8, v7

    move v2, v1

    :goto_2
    if-ge v2, v8, :cond_3

    aget-object v9, v7, v2

    .line 49
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 48
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 44
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_4
    move v0, v1

    .line 54
    goto :goto_0
.end method

.method public final mE(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 62
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 63
    if-nez p1, :cond_0

    move-object v0, v2

    .line 86
    :goto_0
    return-object v0

    .line 67
    :cond_0
    sget-object v0, Lggq;->cJG:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 69
    iget-object v0, p0, Lggq;->cJH:Ljta;

    iget-object v7, v0, Ljta;->eDj:[Ljtb;

    array-length v8, v7

    move v5, v4

    move v0, v4

    :goto_1
    if-ge v5, v8, :cond_2

    aget-object v9, v7, v5

    .line 70
    invoke-virtual {v9}, Ljtb;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 80
    :cond_1
    if-eqz v0, :cond_5

    .line 81
    invoke-virtual {v9}, Ljtb;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, v9, Ljtb;->eDl:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    move-object v0, v2

    .line 86
    goto :goto_0

    .line 74
    :cond_3
    iget-object v10, v9, Ljtb;->eDl:[Ljava/lang/String;

    array-length v11, v10

    move v3, v4

    :goto_2
    if-ge v3, v11, :cond_1

    aget-object v12, v10, v3

    .line 75
    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    move v0, v1

    .line 74
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 69
    :cond_5
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1
.end method

.method public final mF(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 95
    if-nez p1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-object v0

    .line 99
    :cond_1
    iget-object v1, p0, Lggq;->cJH:Ljta;

    iget-object v4, v1, Ljta;->eDj:[Ljtb;

    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    .line 100
    invoke-virtual {v6}, Ljtb;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    invoke-virtual {v6}, Ljtb;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_2
    iget-object v7, v6, Ljtb;->eDl:[Ljava/lang/String;

    array-length v8, v7

    move v1, v2

    :goto_2
    if-ge v1, v8, :cond_4

    aget-object v9, v7, v1

    .line 104
    invoke-virtual {v9, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 105
    invoke-virtual {v6}, Ljtb;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 99
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1
.end method
