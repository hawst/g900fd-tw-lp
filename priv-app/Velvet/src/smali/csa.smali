.class public final Lcsa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bhu:I

.field private bhv:I

.field private bhw:I

.field private bhx:Lcsb;

.field private bhy:I

.field private bhz:I

.field public rV:[B


# direct methods
.method public constructor <init>(IIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 29
    if-lez p2, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 30
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 31
    if-lez p4, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 32
    if-lt p1, p4, :cond_3

    :goto_3
    const-string v0, "The requested audio buffer size must be at least the read size."

    invoke-static {v1, v0}, Lifv;->c(ZLjava/lang/Object;)V

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcsa;->bhu:I

    .line 35
    mul-int v0, p4, p2

    mul-int/lit8 v0, v0, 0x2

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcsa;->bhv:I

    .line 36
    mul-int v0, p1, p2

    mul-int/lit8 v0, v0, 0x2

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcsa;->bhw:I

    .line 41
    iget v0, p0, Lcsa;->bhv:I

    iget v1, p0, Lcsa;->bhu:I

    sub-int/2addr v0, v1

    .line 42
    iget v1, p0, Lcsa;->bhw:I

    add-int/2addr v0, v1

    new-array v0, v0, [B

    iput-object v0, p0, Lcsa;->rV:[B

    .line 43
    return-void

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0

    :cond_1
    move v0, v2

    .line 29
    goto :goto_1

    :cond_2
    move v0, v2

    .line 31
    goto :goto_2

    :cond_3
    move v1, v2

    .line 32
    goto :goto_3
.end method

.method private al(II)Lcsb;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcsa;->bhx:Lcsb;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcsb;

    invoke-direct {v0, p1, p2}, Lcsb;-><init>(II)V

    iput-object v0, p0, Lcsa;->bhx:Lcsb;

    .line 109
    :goto_0
    iget-object v0, p0, Lcsa;->bhx:Lcsb;

    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcsa;->bhx:Lcsb;

    iput p1, v0, Lcsb;->offset:I

    .line 107
    iget-object v0, p0, Lcsa;->bhx:Lcsb;

    iput p2, v0, Lcsb;->size:I

    goto :goto_0
.end method


# virtual methods
.method public final SB()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 133
    iget v0, p0, Lcsa;->bhz:I

    if-nez v0, :cond_1

    .line 135
    iget v0, p0, Lcsa;->bhy:I

    new-array v0, v0, [B

    .line 136
    iget-object v1, p0, Lcsa;->rV:[B

    iget v2, p0, Lcsa;->bhy:I

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    :cond_0
    :goto_0
    return-object v0

    .line 139
    :cond_1
    iget v0, p0, Lcsa;->bhw:I

    new-array v0, v0, [B

    .line 142
    iget v1, p0, Lcsa;->bhw:I

    iget v2, p0, Lcsa;->bhy:I

    sub-int/2addr v1, v2

    .line 143
    iget-object v2, p0, Lcsa;->rV:[B

    iget v3, p0, Lcsa;->bhz:I

    sub-int/2addr v3, v1

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    iget v2, p0, Lcsa;->bhy:I

    if-lez v2, :cond_0

    .line 145
    iget-object v2, p0, Lcsa;->rV:[B

    iget v3, p0, Lcsa;->bhy:I

    invoke-static {v2, v4, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final g(Ljava/io/InputStream;)Lcsb;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget v2, p0, Lcsa;->bhy:I

    move v0, v1

    .line 64
    :goto_0
    iget v3, p0, Lcsa;->bhv:I

    if-ge v0, v3, :cond_3

    .line 65
    iget-object v3, p0, Lcsa;->rV:[B

    iget v4, p0, Lcsa;->bhy:I

    iget v5, p0, Lcsa;->bhv:I

    sub-int/2addr v5, v0

    invoke-virtual {p1, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 66
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 67
    iget v1, p0, Lcsa;->bhu:I

    rem-int v1, v0, v1

    if-eqz v1, :cond_0

    .line 71
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Audio stream not multiple of sample size."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    invoke-direct {p0, v2, v0}, Lcsa;->al(II)Lcsb;

    move-result-object v0

    .line 78
    :goto_1
    return-object v0

    .line 75
    :cond_1
    iget v4, p0, Lcsa;->bhy:I

    add-int/2addr v4, v3

    iput v4, p0, Lcsa;->bhy:I

    iget v4, p0, Lcsa;->bhy:I

    iget v5, p0, Lcsa;->bhv:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcsa;->rV:[B

    array-length v5, v5

    if-le v4, v5, :cond_2

    iget v4, p0, Lcsa;->bhy:I

    iput v4, p0, Lcsa;->bhz:I

    iput v1, p0, Lcsa;->bhy:I

    .line 76
    :cond_2
    add-int/2addr v0, v3

    .line 77
    goto :goto_0

    .line 78
    :cond_3
    iget v0, p0, Lcsa;->bhv:I

    invoke-direct {p0, v2, v0}, Lcsa;->al(II)Lcsb;

    move-result-object v0

    goto :goto_1
.end method
