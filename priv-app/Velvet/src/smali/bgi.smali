.class public final Lbgi;
.super Ljava/lang/Object;


# instance fields
.field aAA:I

.field aAB:I

.field aAC:I

.field aAD:I

.field aAE:I

.field aAF:Ljava/lang/String;

.field aAG:I

.field aAH:I

.field aAx:F

.field aAy:I

.field aAz:I

.field azS:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lbgi;->clear()V

    return-void
.end method

.method static fc(Ljava/lang/String;)I
    .locals 6

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x5

    const/4 v4, 0x7

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x7

    const/16 v5, 0x9

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method clear()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbgi;->aAx:F

    iput v1, p0, Lbgi;->aAy:I

    iput v1, p0, Lbgi;->aAz:I

    iput v2, p0, Lbgi;->aAA:I

    iput v1, p0, Lbgi;->aAB:I

    iput v2, p0, Lbgi;->aAC:I

    iput v1, p0, Lbgi;->aAD:I

    iput v1, p0, Lbgi;->aAE:I

    iput-object v3, p0, Lbgi;->aAF:Ljava/lang/String;

    iput v2, p0, Lbgi;->aAG:I

    iput v2, p0, Lbgi;->aAH:I

    iput-object v3, p0, Lbgi;->azS:Lorg/json/JSONObject;

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v0, p1, Lbgi;

    if-eqz v0, :cond_0

    check-cast p1, Lbgi;

    iget-object v0, p0, Lbgi;->azS:Lorg/json/JSONObject;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p1, Lbgi;->azS:Lorg/json/JSONObject;

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lbgi;->azS:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lbgi;->azS:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbgi;->azS:Lorg/json/JSONObject;

    iget-object v3, p1, Lbgi;->azS:Lorg/json/JSONObject;

    invoke-static {v0, v3}, Lbjx;->g(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    iget v0, p0, Lbgi;->aAx:F

    iget v3, p1, Lbgi;->aAx:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget v0, p0, Lbgi;->aAy:I

    iget v3, p1, Lbgi;->aAy:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lbgi;->aAz:I

    iget v3, p1, Lbgi;->aAz:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lbgi;->aAA:I

    iget v3, p1, Lbgi;->aAA:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lbgi;->aAB:I

    iget v3, p1, Lbgi;->aAB:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lbgi;->aAC:I

    iget v3, p1, Lbgi;->aAC:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lbgi;->aAE:I

    iget v3, p1, Lbgi;->aAE:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lbgi;->aAF:Ljava/lang/String;

    iget-object v3, p1, Lbgi;->aAF:Ljava/lang/String;

    invoke-static {v0, v3}, Lblo;->h(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbgi;->aAG:I

    iget v3, p1, Lbgi;->aAG:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lbgi;->aAH:I

    iget v3, p1, Lbgi;->aAH:I

    if-ne v0, v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lbgi;->aAx:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lbgi;->aAy:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lbgi;->aAz:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lbgi;->aAA:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lbgi;->aAB:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lbgi;->aAC:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lbgi;->aAD:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lbgi;->aAE:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lbgi;->aAF:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lbgi;->aAG:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lbgi;->aAH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lbgi;->azS:Lorg/json/JSONObject;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
