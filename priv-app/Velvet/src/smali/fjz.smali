.class public abstract Lfjz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfkd;


# instance fields
.field final mActivityHelper:Lfzw;

.field volatile mEntry:Lizj;

.field private final mEntryTreeNode:Lizq;


# direct methods
.method public constructor <init>(Lizj;Lfzw;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lfjz;->mEntryTreeNode:Lizq;

    .line 57
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lfjz;->mEntry:Lizj;

    .line 58
    iput-object p2, p0, Lfjz;->mActivityHelper:Lfzw;

    .line 59
    return-void
.end method

.method public static a(Landroid/content/Context;Lfmt;Lixx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 295
    invoke-static {p1, p2, v1}, Lgac;->a(Lfmt;Lixx;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    invoke-virtual {p2}, Lixx;->baY()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0a05df

    .line 298
    :goto_0
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 300
    :cond_0
    return-void

    .line 296
    :cond_1
    const v0, 0x7f0a0656

    goto :goto_0
.end method


# virtual methods
.method public final T(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public final a(Landroid/view/View;Lizj;)Landroid/view/View;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 317
    const v0, 0x7f110004

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 319
    if-eqz v0, :cond_1

    .line 320
    invoke-static {p2}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v2

    .line 321
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 322
    const v1, 0x7f110002

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizj;

    .line 323
    if-eqz v1, :cond_0

    .line 325
    invoke-static {v1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v1

    invoke-virtual {v2, v1}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Lfmt;)Lfry;
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lfmt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public final a(Landroid/view/View;Lgbr;)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dQj:Ljhe;

    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfjz;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dQj:Ljhe;

    invoke-virtual {p2, v0, v1}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    .line 205
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 206
    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 209
    :cond_0
    return-void
.end method

.method public a(Lfmt;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 179
    const/4 v3, 0x0

    .line 180
    if-nez v3, :cond_0

    .line 181
    iget-object v3, p0, Lfjz;->mEntry:Lizj;

    .line 183
    :cond_0
    new-instance v0, Lfka;

    const/4 v4, 0x3

    move-object v1, p0

    move-object v2, p1

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lfka;-><init>(Lfjz;Lfmt;Lizj;ILfmt;Landroid/view/View;)V

    .line 192
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    return-void
.end method

.method public final aAA()Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 409
    const/4 v0, 0x0

    return-object v0
.end method

.method public final aAv()Lizq;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public final aAw()Lizj;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method

.method public final aAx()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public final aAy()Lizj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    return-object v0
.end method

.method public final aAz()Landroid/os/Bundle;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 400
    const/4 v0, 0x0

    return-object v0
.end method

.method public final aP(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public final ayU()Ljava/lang/String;
    .locals 3

    .prologue
    .line 247
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 248
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 249
    const-string v1, "Adapter"

    invoke-static {v0, v1}, Lesp;->aI(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 250
    const-string v1, "Entry"

    invoke-static {v0, v1}, Lesp;->aI(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lfjz;->mEntry:Lizj;

    invoke-static {v1}, Lgbm;->M(Lizj;)Ljava/lang/String;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    :cond_0
    return-object v0
.end method

.method public final b(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lftp;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lfmt;)V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    invoke-interface {p2, v0}, Lfmt;->t(Lizj;)V

    .line 122
    const-string v0, "DISMISS_CARD"

    invoke-virtual {p0}, Lfjz;->ayU()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lfmt;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public c(Landroid/content/Context;Lfmt;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUp:Lixx;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUp:Lixx;

    invoke-static {p1, p2, v0}, Lfjz;->a(Landroid/content/Context;Lfmt;Lixx;)V

    .line 172
    :cond_0
    return-void
.end method

.method public final f(Lizj;Lizj;)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public final getEntry()Lizj;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    return-object v0
.end method

.method public final s(Lizj;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lfjz;->mEntry:Lizj;

    .line 75
    return-void
.end method
