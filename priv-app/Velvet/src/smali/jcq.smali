.class public final Ljcq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eaT:[Ljcq;


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private aiC:Ljava/lang/String;

.field private dOS:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4519
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4520
    const/4 v0, 0x0

    iput v0, p0, Ljcq;->aez:I

    iput-object v1, p0, Ljcq;->aeB:Ljbp;

    const/16 v0, 0x1e

    iput v0, p0, Ljcq;->dOS:I

    const-string v0, ""

    iput-object v0, p0, Ljcq;->aiC:Ljava/lang/String;

    iput-object v1, p0, Ljcq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcq;->eCz:I

    .line 4521
    return-void
.end method

.method public static bgO()[Ljcq;
    .locals 2

    .prologue
    .line 4462
    sget-object v0, Ljcq;->eaT:[Ljcq;

    if-nez v0, :cond_1

    .line 4463
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 4465
    :try_start_0
    sget-object v0, Ljcq;->eaT:[Ljcq;

    if-nez v0, :cond_0

    .line 4466
    const/4 v0, 0x0

    new-array v0, v0, [Ljcq;

    sput-object v0, Ljcq;->eaT:[Ljcq;

    .line 4468
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4470
    :cond_1
    sget-object v0, Ljcq;->eaT:[Ljcq;

    return-object v0

    .line 4468
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4438
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljcq;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljcq;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljcq;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljcq;->dOS:I

    iget v0, p0, Ljcq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcq;->aiC:Ljava/lang/String;

    iget v0, p0, Ljcq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcq;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x16
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4536
    iget-object v0, p0, Ljcq;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 4537
    const/4 v0, 0x1

    iget-object v1, p0, Ljcq;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4539
    :cond_0
    iget v0, p0, Ljcq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 4540
    const/4 v0, 0x2

    iget v1, p0, Ljcq;->dOS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4542
    :cond_1
    iget v0, p0, Ljcq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 4543
    const/4 v0, 0x3

    iget-object v1, p0, Ljcq;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4545
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4546
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4550
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4551
    iget-object v1, p0, Ljcq;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 4552
    const/4 v1, 0x1

    iget-object v2, p0, Ljcq;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4555
    :cond_0
    iget v1, p0, Ljcq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 4556
    const/4 v1, 0x2

    iget v2, p0, Ljcq;->dOS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4559
    :cond_1
    iget v1, p0, Ljcq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 4560
    const/4 v1, 0x3

    iget-object v2, p0, Ljcq;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4563
    :cond_2
    return v0
.end method

.method public final oJ(I)Ljcq;
    .locals 1

    .prologue
    .line 4484
    const/16 v0, 0x16

    iput v0, p0, Ljcq;->dOS:I

    .line 4485
    iget v0, p0, Ljcq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcq;->aez:I

    .line 4486
    return-object p0
.end method
