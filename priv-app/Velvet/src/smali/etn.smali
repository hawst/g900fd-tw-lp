.class public Letn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bng:Ljava/lang/String;

.field private final ciH:Letj;

.field private final ciN:Z


# direct methods
.method private constructor <init>(Letj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Letn;-><init>(Letj;Ljava/lang/String;Z)V

    .line 404
    return-void
.end method

.method synthetic constructor <init>(Letj;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0, p1, p2}, Letn;-><init>(Letj;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Letj;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    iput-object p1, p0, Letn;->ciH:Letj;

    .line 408
    iput-object p2, p0, Letn;->bng:Ljava/lang/String;

    .line 409
    iput-boolean p3, p0, Letn;->ciN:Z

    .line 410
    return-void
.end method

.method synthetic constructor <init>(Letj;Ljava/lang/String;ZB)V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0, p1, p2, p3}, Letn;-><init>(Letj;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Date;)V
    .locals 2

    .prologue
    .line 481
    invoke-static {}, Letj;->avQ()Ljava/text/SimpleDateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 482
    return-void
.end method

.method public final avR()V
    .locals 2

    .prologue
    .line 485
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 486
    return-void
.end method

.method public avS()Letn;
    .locals 4

    .prologue
    .line 520
    new-instance v0, Leto;

    iget-object v1, p0, Letn;->ciH:Letj;

    iget-object v2, p0, Letn;->bng:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Leto;-><init>(Letn;Letj;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final b(Leti;)V
    .locals 4
    .param p1    # Leti;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 489
    if-nez p1, :cond_0

    .line 490
    invoke-virtual {p0}, Letn;->avR()V

    .line 495
    :goto_0
    return-void

    .line 493
    :cond_0
    iget-object v0, p0, Letn;->ciH:Letj;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Letn;->bng:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v0, v1, v2, v3, v3}, Letj;->a(Letj;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 494
    iget-object v0, p0, Letn;->ciH:Letj;

    invoke-virtual {v0}, Letj;->avJ()Letj;

    move-result-object v0

    invoke-virtual {v0, p1}, Letj;->b(Leti;)V

    goto :goto_0
.end method

.method public final bd(J)V
    .locals 3

    .prologue
    .line 453
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 454
    return-void
.end method

.method public final be(J)V
    .locals 3

    .prologue
    .line 457
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 458
    return-void
.end method

.method public d(Ljava/lang/CharSequence;Z)V
    .locals 4

    .prologue
    .line 435
    if-nez p1, :cond_1

    const-string v0, "null"

    .line 436
    :goto_0
    iget-object v1, p0, Letn;->ciH:Letj;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Letn;->bng:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, p2, v3}, Letj;->a(Letj;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 437
    iget-boolean v1, p0, Letn;->ciN:Z

    if-eqz v1, :cond_0

    .line 438
    iget-object v1, p0, Letn;->ciH:Letj;

    invoke-static {v1}, Letj;->e(Letj;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Letn;->bng:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    :cond_0
    return-void

    .line 435
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Ljava/lang/Enum;)V
    .locals 2
    .param p1    # Ljava/lang/Enum;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 443
    if-nez p1, :cond_0

    const-string v0, "null"

    .line 444
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 445
    return-void

    .line 443
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Ljava/lang/Enum;)V
    .locals 2
    .param p1    # Ljava/lang/Enum;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 448
    if-nez p1, :cond_0

    const-string v0, "null"

    .line 449
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 450
    return-void

    .line 448
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ff(Z)V
    .locals 2

    .prologue
    .line 473
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 474
    return-void
.end method

.method public final io(I)V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Letn;->ciH:Letj;

    invoke-static {v0}, Letj;->f(Letj;)Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 511
    return-void
.end method
