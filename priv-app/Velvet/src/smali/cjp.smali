.class final Lcjp;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic aYJ:Lcjl;


# direct methods
.method public constructor <init>(Lcjl;)V
    .locals 4

    .prologue
    .line 266
    iput-object p1, p0, Lcjp;->aYJ:Lcjl;

    .line 267
    const-string v0, "PeopleApiSaverTask"

    iget-object v1, p1, Lcjl;->mUiExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p1, Lcjl;->mBgExecutor:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {p0, v0, v1, v2, v3}, Lenp;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    .line 268
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 265
    check-cast p1, [Lcjn;

    iget-object v1, p0, Lcjp;->aYJ:Lcjl;

    const/4 v0, 0x0

    aget-object v2, p1, v0

    iget-object v0, v1, Lcjl;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "RelationshipManagerForPeopleClient"

    const-string v1, "Active account is null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v3, v1, Lcjl;->aYF:Lcfm;

    iget-object v4, v2, Lcjn;->aYL:Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v4

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Lcfm;->b(JLjava/lang/String;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcjl;->mLoginHelper:Lcrh;

    const-string v5, "oauth2:https://www.googleapis.com/auth/googlenow https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    const-wide/16 v6, 0x1388

    invoke-virtual {v4, v0, v5, v6, v7}, Lcrh;->c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v1, "RelationshipManagerForPeopleClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get Auth Token for account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Lcjl;->G(Ljava/lang/String;Ljava/lang/String;)Lcjm;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v4, v0, v2}, Lcjl;->a(Ljava/lang/String;Lcjm;Lcjn;)V

    goto :goto_0
.end method
