.class public final Lftm;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 13

    .prologue
    const v4, 0x7f1101a8

    const/4 v3, 0x0

    .line 34
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 35
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v5, v1, Lanh;->ahp:Lanw;

    .line 36
    invoke-virtual {v5}, Lanw;->oK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    const v1, 0x7f1101a9

    invoke-virtual {v5}, Lanw;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lftm;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 39
    :cond_0
    invoke-virtual {v5}, Lanw;->om()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    const v1, 0x7f110080

    invoke-virtual {v5}, Lanw;->ol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lftm;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 42
    :cond_1
    invoke-virtual {v5}, Lanw;->ok()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 43
    invoke-virtual {v5}, Lanw;->oj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v4, v1}, Lftm;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 44
    invoke-virtual {p0, v4}, Lftm;->jy(I)V

    .line 46
    :cond_2
    invoke-virtual {v5}, Lanw;->pX()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v5, Lanw;->aiM:[Lanx;

    if-eqz v1, :cond_4

    .line 47
    :cond_3
    const v1, 0x7f110272

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 49
    :cond_4
    invoke-virtual {v5}, Lanw;->pX()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 50
    const v1, 0x7f1101aa

    invoke-virtual {v5}, Lanw;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lftm;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 52
    :cond_5
    iget-object v1, v5, Lanw;->aiM:[Lanx;

    if-eqz v1, :cond_c

    .line 53
    invoke-virtual {p0}, Lftm;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    .line 54
    const v1, 0x7f110302

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 55
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 56
    const/4 v2, 0x1

    .line 57
    iget-object v7, v5, Lanw;->aiM:[Lanx;

    array-length v8, v7

    move v4, v3

    :goto_0
    if-ge v4, v8, :cond_c

    aget-object v9, v7, v4

    .line 58
    const v10, 0x7f040140

    invoke-virtual {v6, v10, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 60
    if-eqz v2, :cond_7

    .line 62
    invoke-virtual {v5}, Lanw;->pZ()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 63
    const v2, 0x7f110365

    invoke-virtual {v5}, Lanw;->pY()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v2, v11}, Lftm;->b(Landroid/view/View;ILjava/lang/String;)V

    :cond_6
    move v2, v3

    .line 66
    :cond_7
    invoke-virtual {v9}, Lanx;->oK()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 67
    const v11, 0x7f110366

    invoke-virtual {v9}, Lanx;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lftm;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 69
    :cond_8
    invoke-virtual {v9}, Lanx;->qc()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 70
    const v11, 0x7f110367

    invoke-virtual {v9}, Lanx;->qb()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lftm;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 73
    :cond_9
    invoke-virtual {v9}, Lanx;->qe()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 74
    const v11, 0x7f110212

    invoke-virtual {v9}, Lanx;->qd()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v10, v11, v12}, Lftm;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 76
    :cond_a
    iget-object v11, v9, Lanx;->aiQ:Lani;

    if-eqz v11, :cond_b

    .line 77
    iget-object v9, v9, Lanx;->aiQ:Lani;

    invoke-virtual {p0, v10, v9}, Lftm;->a(Landroid/view/View;Lani;)V

    .line 79
    :cond_b
    invoke-virtual {v1, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 57
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 82
    :cond_c
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 29
    const v0, 0x7f040112

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
