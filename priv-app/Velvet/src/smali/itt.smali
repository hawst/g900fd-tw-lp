.class public final Litt;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dIf:I

.field private dIg:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5110
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5111
    iput v0, p0, Litt;->aez:I

    iput v0, p0, Litt;->dIf:I

    iput v0, p0, Litt;->dIg:I

    const/4 v0, 0x0

    iput-object v0, p0, Litt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litt;->eCz:I

    .line 5112
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5053
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litt;->dIf:I

    iget v0, p0, Litt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litt;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litt;->dIg:I

    iget v0, p0, Litt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litt;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5126
    iget v0, p0, Litt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5127
    const/4 v0, 0x1

    iget v1, p0, Litt;->dIf:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5129
    :cond_0
    iget v0, p0, Litt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5130
    const/4 v0, 0x2

    iget v1, p0, Litt;->dIg:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5132
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5133
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5137
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5138
    iget v1, p0, Litt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5139
    const/4 v1, 0x1

    iget v2, p0, Litt;->dIf:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5142
    :cond_0
    iget v1, p0, Litt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5143
    const/4 v1, 0x2

    iget v2, p0, Litt;->dIg:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5146
    :cond_1
    return v0
.end method

.method public final mA(I)Litt;
    .locals 1

    .prologue
    .line 5097
    iput p1, p0, Litt;->dIg:I

    .line 5098
    iget v0, p0, Litt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litt;->aez:I

    .line 5099
    return-object p0
.end method

.method public final mz(I)Litt;
    .locals 1

    .prologue
    .line 5078
    iput p1, p0, Litt;->dIf:I

    .line 5079
    iget v0, p0, Litt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litt;->aez:I

    .line 5080
    return-object p0
.end method
