.class public final Ljbn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dYL:I

.field private dYM:I

.field private dYN:F

.field private dYO:I

.field private dYP:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30080
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 30081
    iput v1, p0, Ljbn;->aez:I

    iput v1, p0, Ljbn;->dYL:I

    iput v1, p0, Ljbn;->dYM:I

    const/4 v0, 0x0

    iput v0, p0, Ljbn;->dYN:F

    iput v1, p0, Ljbn;->dYO:I

    iput v1, p0, Ljbn;->dYP:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljbn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbn;->eCz:I

    .line 30082
    return-void
.end method


# virtual methods
.method public final U(F)Ljbn;
    .locals 1

    .prologue
    .line 30029
    iput p1, p0, Ljbn;->dYN:F

    .line 30030
    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbn;->aez:I

    .line 30031
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 29966
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbn;->dYL:I

    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbn;->dYM:I

    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbn;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljbn;->dYN:F

    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbn;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbn;->dYO:I

    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbn;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbn;->dYP:I

    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbn;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 30099
    iget v0, p0, Ljbn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 30100
    const/4 v0, 0x1

    iget v1, p0, Ljbn;->dYL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 30102
    :cond_0
    iget v0, p0, Ljbn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 30103
    const/4 v0, 0x2

    iget v1, p0, Ljbn;->dYM:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 30105
    :cond_1
    iget v0, p0, Ljbn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 30106
    const/4 v0, 0x3

    iget v1, p0, Ljbn;->dYN:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 30108
    :cond_2
    iget v0, p0, Ljbn;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 30109
    const/4 v0, 0x4

    iget v1, p0, Ljbn;->dYO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 30111
    :cond_3
    iget v0, p0, Ljbn;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 30112
    const/4 v0, 0x5

    iget v1, p0, Ljbn;->dYP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 30114
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 30115
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 30119
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 30120
    iget v1, p0, Ljbn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 30121
    const/4 v1, 0x1

    iget v2, p0, Ljbn;->dYL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30124
    :cond_0
    iget v1, p0, Ljbn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 30125
    const/4 v1, 0x2

    iget v2, p0, Ljbn;->dYM:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30128
    :cond_1
    iget v1, p0, Ljbn;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 30129
    const/4 v1, 0x3

    iget v2, p0, Ljbn;->dYN:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 30132
    :cond_2
    iget v1, p0, Ljbn;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 30133
    const/4 v1, 0x4

    iget v2, p0, Ljbn;->dYO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30136
    :cond_3
    iget v1, p0, Ljbn;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 30137
    const/4 v1, 0x5

    iget v2, p0, Ljbn;->dYP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30140
    :cond_4
    return v0
.end method

.method public final oo(I)Ljbn;
    .locals 1

    .prologue
    .line 29991
    iput p1, p0, Ljbn;->dYL:I

    .line 29992
    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbn;->aez:I

    .line 29993
    return-object p0
.end method

.method public final op(I)Ljbn;
    .locals 1

    .prologue
    .line 30010
    iput p1, p0, Ljbn;->dYM:I

    .line 30011
    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbn;->aez:I

    .line 30012
    return-object p0
.end method

.method public final oq(I)Ljbn;
    .locals 1

    .prologue
    .line 30048
    iput p1, p0, Ljbn;->dYO:I

    .line 30049
    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbn;->aez:I

    .line 30050
    return-object p0
.end method

.method public final or(I)Ljbn;
    .locals 1

    .prologue
    .line 30067
    iput p1, p0, Ljbn;->dYP:I

    .line 30068
    iget v0, p0, Ljbn;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbn;->aez:I

    .line 30069
    return-object p0
.end method
