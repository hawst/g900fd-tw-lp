.class public abstract Lfos;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Lfor;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 29
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p0, p0, v0}, Lfos;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public static I(Landroid/os/IBinder;)Lfor;
    .locals 2

    .prologue
    .line 37
    if-nez p0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    .line 40
    :cond_0
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_1

    instance-of v1, v0, Lfor;

    if-eqz v1, :cond_1

    .line 42
    check-cast v0, Lfor;

    goto :goto_0

    .line 44
    :cond_1
    new-instance v0, Lfot;

    invoke-direct {v0, p0}, Lfot;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 48
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 52
    sparse-switch p1, :sswitch_data_0

    .line 760
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 56
    :sswitch_0
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :sswitch_1
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lfos;->getVersion()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 64
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :sswitch_2
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-virtual {p0, v0}, Lfos;->lL(Ljava/lang/String;)Z

    move-result v0

    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    if-eqz v0, :cond_0

    move v0, v9

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    move v0, v10

    goto :goto_1

    .line 79
    :sswitch_3
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-virtual {p0, v0}, Lfos;->lM(Ljava/lang/String;)Z

    move-result v0

    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    if-eqz v0, :cond_1

    move v10, v9

    :cond_1
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 89
    :sswitch_4
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lfos;->Kz()Z

    move-result v0

    .line 91
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    if-eqz v0, :cond_2

    move v10, v9

    :cond_2
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 97
    :sswitch_5
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0}, Lfos;->azF()V

    .line 99
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 104
    :sswitch_6
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lfos;->azG()Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    move-result-object v0

    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v0, :cond_3

    .line 108
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    invoke-virtual {v0, p3, v9}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 112
    :cond_3
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 118
    :sswitch_7
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-object v1, v0

    .line 127
    :cond_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 131
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v5, v9

    :goto_2
    move-object v0, p0

    .line 132
    invoke-virtual/range {v0 .. v5}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;JIZ)V

    goto/16 :goto_0

    :cond_5
    move v5, v10

    .line 131
    goto :goto_2

    .line 137
    :sswitch_8
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 140
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 146
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 147
    invoke-virtual {p0, v0, v1}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    .line 143
    goto :goto_3

    .line 152
    :sswitch_9
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 155
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    .line 160
    :goto_4
    invoke-virtual {p0, v0}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    .line 158
    goto :goto_4

    .line 165
    :sswitch_a
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 168
    invoke-virtual {p0, v0, v1}, Lfos;->bl(J)V

    goto/16 :goto_0

    .line 173
    :sswitch_b
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 176
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 182
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_8

    move v10, v9

    .line 183
    :cond_8
    invoke-virtual {p0, v0, v10}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V

    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    .line 179
    goto :goto_5

    .line 188
    :sswitch_c
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 191
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-object v2, v0

    .line 197
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 198
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 203
    :goto_7
    invoke-virtual {p0, v2, v0}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V

    goto/16 :goto_0

    :cond_a
    move-object v2, v1

    .line 194
    goto :goto_6

    :cond_b
    move-object v0, v1

    .line 201
    goto :goto_7

    .line 208
    :sswitch_d
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 211
    sget-object v0, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    move-object v2, v0

    .line 217
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 218
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 224
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_e

    move v1, v9

    .line 225
    :goto_a
    invoke-virtual {p0, v2, v0, v1}, Lfos;->b(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 226
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 227
    if-eqz v0, :cond_f

    .line 228
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 229
    invoke-virtual {v0, p3, v9}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_c
    move-object v2, v1

    .line 214
    goto :goto_8

    :cond_d
    move-object v0, v1

    .line 221
    goto :goto_9

    :cond_e
    move v1, v10

    .line 224
    goto :goto_a

    .line 232
    :cond_f
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 238
    :sswitch_e
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 240
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    .line 241
    sget-object v0, Landroid/location/Location;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    move-object v2, v0

    .line 247
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 248
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 254
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_12

    move v1, v9

    .line 255
    :goto_d
    invoke-virtual {p0, v2, v0, v1}, Lfos;->a(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 256
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 257
    if-eqz v0, :cond_13

    .line 258
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 259
    invoke-virtual {v0, p3, v9}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_10
    move-object v2, v1

    .line 244
    goto :goto_b

    :cond_11
    move-object v0, v1

    .line 251
    goto :goto_c

    :cond_12
    move v1, v10

    .line 254
    goto :goto_d

    .line 262
    :cond_13
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 268
    :sswitch_f
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 270
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    .line 271
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    .line 276
    :goto_e
    invoke-virtual {p0, v0}, Lfos;->b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 278
    if-eqz v0, :cond_15

    .line 279
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    invoke-virtual {v0, p3, v9}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_14
    move-object v0, v1

    .line 274
    goto :goto_e

    .line 283
    :cond_15
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 289
    :sswitch_10
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0}, Lfos;->ayr()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 291
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 292
    if-eqz v0, :cond_16

    .line 293
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 294
    invoke-virtual {v0, p3, v9}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 297
    :cond_16
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 303
    :sswitch_11
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 305
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 306
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 311
    :goto_f
    invoke-virtual {p0, v0}, Lfos;->P(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_17
    move-object v0, v1

    .line 309
    goto :goto_f

    .line 316
    :sswitch_12
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 320
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_18

    move v10, v9

    .line 321
    :cond_18
    invoke-virtual {p0, v0, v1, v10}, Lfos;->b(JZ)V

    goto/16 :goto_0

    .line 326
    :sswitch_13
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 328
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 329
    invoke-virtual {p0, v0}, Lfos;->U(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 330
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 331
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 336
    :sswitch_14
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 339
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-object v2, v0

    .line 345
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1a

    .line 346
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-object v3, v0

    .line 352
    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1b

    .line 353
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 358
    :goto_12
    invoke-virtual {p0, v2, v3, v0}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V

    goto/16 :goto_0

    :cond_19
    move-object v2, v1

    .line 342
    goto :goto_10

    :cond_1a
    move-object v3, v1

    .line 349
    goto :goto_11

    :cond_1b
    move-object v0, v1

    .line 356
    goto :goto_12

    .line 363
    :sswitch_15
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0}, Lfos;->azH()V

    goto/16 :goto_0

    .line 369
    :sswitch_16
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 371
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1c

    .line 372
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-object v2, v0

    .line 378
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1d

    .line 379
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-object v3, v0

    .line 385
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1e

    .line 386
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 391
    :goto_15
    invoke-virtual {p0, v2, v3, v0}, Lfos;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V

    goto/16 :goto_0

    :cond_1c
    move-object v2, v1

    .line 375
    goto :goto_13

    :cond_1d
    move-object v3, v1

    .line 382
    goto :goto_14

    :cond_1e
    move-object v0, v1

    .line 389
    goto :goto_15

    .line 396
    :sswitch_17
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p0}, Lfos;->azI()V

    goto/16 :goto_0

    .line 402
    :sswitch_18
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 404
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    .line 405
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 410
    :goto_16
    invoke-virtual {p0, v0}, Lfos;->K(Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_1f
    move-object v0, v1

    .line 408
    goto :goto_16

    .line 415
    :sswitch_19
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 417
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_20

    .line 418
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 424
    :goto_17
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_21

    move v1, v9

    .line 425
    :goto_18
    invoke-virtual {p0, v0, v1}, Lfos;->e(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 426
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 427
    if-eqz v0, :cond_22

    .line 428
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 429
    invoke-virtual {v0, p3, v9}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_20
    move-object v0, v1

    .line 421
    goto :goto_17

    :cond_21
    move v1, v10

    .line 424
    goto :goto_18

    .line 432
    :cond_22
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 438
    :sswitch_1a
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 440
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 442
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 443
    invoke-virtual {p0, v0, v1}, Lfos;->b(Ljava/util/List;I)Landroid/content/Intent;

    move-result-object v0

    .line 444
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 445
    if-eqz v0, :cond_23

    .line 446
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 447
    invoke-virtual {v0, p3, v9}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 450
    :cond_23
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 456
    :sswitch_1b
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 458
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_25

    .line 459
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 464
    :goto_19
    invoke-virtual {p0, v0}, Lfos;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Z

    move-result v0

    .line 465
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 466
    if-eqz v0, :cond_24

    move v10, v9

    :cond_24
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_25
    move-object v0, v1

    .line 462
    goto :goto_19

    .line 471
    :sswitch_1c
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 473
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_26

    .line 474
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 479
    :goto_1a
    invoke-virtual {p0, v0}, Lfos;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V

    goto/16 :goto_0

    :cond_26
    move-object v0, v1

    .line 477
    goto :goto_1a

    .line 484
    :sswitch_1d
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 486
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 487
    invoke-virtual {p0, v0}, Lfos;->iH(I)Z

    move-result v0

    .line 488
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 489
    if-eqz v0, :cond_27

    move v10, v9

    :cond_27
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 494
    :sswitch_1e
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 495
    invoke-virtual {p0}, Lfos;->wt()V

    goto/16 :goto_0

    .line 500
    :sswitch_1f
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 502
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 504
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 506
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 507
    invoke-virtual {p0, v0, v1, v2}, Lfos;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 509
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 514
    :sswitch_20
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 517
    invoke-virtual {p0, v0}, Lfos;->iJ(I)V

    goto/16 :goto_0

    .line 522
    :sswitch_21
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 524
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 525
    invoke-virtual {p0, v0}, Lfos;->iK(I)V

    goto/16 :goto_0

    .line 530
    :sswitch_22
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 531
    invoke-virtual {p0}, Lfos;->azK()V

    goto/16 :goto_0

    .line 536
    :sswitch_23
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 537
    invoke-virtual {p0}, Lfos;->azL()V

    goto/16 :goto_0

    .line 542
    :sswitch_24
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 543
    invoke-virtual {p0}, Lfos;->azM()Z

    move-result v0

    .line 544
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 545
    if-eqz v0, :cond_28

    move v10, v9

    :cond_28
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 550
    :sswitch_25
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 551
    invoke-virtual {p0}, Lfos;->azN()Landroid/os/Bundle;

    move-result-object v0

    .line 552
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 553
    if-eqz v0, :cond_29

    .line 554
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 555
    invoke-virtual {v0, p3, v9}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 558
    :cond_29
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 564
    :sswitch_26
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 566
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2a

    .line 567
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 572
    :goto_1b
    invoke-virtual {p0, v0}, Lfos;->c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V

    goto/16 :goto_0

    :cond_2a
    move-object v0, v1

    .line 570
    goto :goto_1b

    .line 577
    :sswitch_27
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 578
    invoke-virtual {p0}, Lfos;->ayp()V

    goto/16 :goto_0

    .line 583
    :sswitch_28
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 585
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    .line 586
    invoke-virtual {p0, v0}, Lfos;->V(Ljava/util/List;)V

    goto/16 :goto_0

    .line 591
    :sswitch_29
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 592
    invoke-virtual {p0}, Lfos;->awf()V

    goto/16 :goto_0

    .line 597
    :sswitch_2a
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 599
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 600
    invoke-virtual {p0, v0}, Lfos;->iI(I)V

    goto/16 :goto_0

    .line 605
    :sswitch_2b
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 607
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2c

    .line 608
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 614
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2b

    move v10, v9

    .line 615
    :cond_2b
    invoke-virtual {p0, v0, v10}, Lfos;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V

    goto/16 :goto_0

    :cond_2c
    move-object v0, v1

    .line 611
    goto :goto_1c

    .line 620
    :sswitch_2c
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 622
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 623
    invoke-virtual {p0, v0}, Lfos;->lN(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 624
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 625
    if-eqz v0, :cond_2d

    .line 626
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 627
    invoke-virtual {v0, p3, v9}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 630
    :cond_2d
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 636
    :sswitch_2d
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 638
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2e

    .line 639
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 645
    :goto_1d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 646
    invoke-virtual {p0, v0, v1}, Lfos;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V

    goto/16 :goto_0

    :cond_2e
    move-object v0, v1

    .line 642
    goto :goto_1d

    .line 651
    :sswitch_2e
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 653
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_30

    .line 654
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 660
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2f

    move v10, v9

    .line 661
    :cond_2f
    invoke-virtual {p0, v0, v10}, Lfos;->c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V

    goto/16 :goto_0

    :cond_30
    move-object v0, v1

    .line 657
    goto :goto_1e

    .line 666
    :sswitch_2f
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 668
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_31

    move v10, v9

    .line 669
    :cond_31
    invoke-virtual {p0, v10}, Lfos;->fl(Z)V

    goto/16 :goto_0

    .line 674
    :sswitch_30
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p0}, Lfos;->azO()V

    goto/16 :goto_0

    .line 680
    :sswitch_31
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 682
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    .line 684
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 686
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 688
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 690
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    .line 691
    invoke-virtual/range {v0 .. v8}, Lfos;->b(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 692
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 693
    if-eqz v0, :cond_32

    .line 694
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 695
    invoke-virtual {v0, p3, v9}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 698
    :cond_32
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 704
    :sswitch_32
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 706
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_33

    .line 707
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 712
    :goto_1f
    invoke-virtual {p0, v0}, Lfos;->d(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    .line 713
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 714
    if-eqz v0, :cond_34

    .line 715
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 716
    invoke-virtual {v0, p3, v9}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_33
    move-object v0, v1

    .line 710
    goto :goto_1f

    .line 719
    :cond_34
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 725
    :sswitch_33
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 727
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 728
    invoke-virtual {p0, v0}, Lfos;->iL(I)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    .line 729
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 730
    if-eqz v0, :cond_35

    .line 731
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 732
    invoke-virtual {v0, p3, v9}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 735
    :cond_35
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 741
    :sswitch_34
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 742
    invoke-virtual {p0}, Lfos;->azJ()V

    goto/16 :goto_0

    .line 747
    :sswitch_35
    const-string v0, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 748
    invoke-virtual {p0}, Lfos;->azP()Landroid/accounts/Account;

    move-result-object v0

    .line 749
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 750
    if-eqz v0, :cond_36

    .line 751
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 752
    invoke-virtual {v0, p3, v9}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 755
    :cond_36
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 52
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_10
        0xf -> :sswitch_11
        0x10 -> :sswitch_12
        0x11 -> :sswitch_13
        0x12 -> :sswitch_14
        0x13 -> :sswitch_15
        0x14 -> :sswitch_16
        0x15 -> :sswitch_17
        0x17 -> :sswitch_18
        0x18 -> :sswitch_19
        0x19 -> :sswitch_1a
        0x1a -> :sswitch_1b
        0x1b -> :sswitch_1c
        0x1c -> :sswitch_1d
        0x1d -> :sswitch_1e
        0x1e -> :sswitch_1f
        0x1f -> :sswitch_20
        0x20 -> :sswitch_21
        0x21 -> :sswitch_22
        0x22 -> :sswitch_23
        0x23 -> :sswitch_24
        0x24 -> :sswitch_25
        0x25 -> :sswitch_26
        0x26 -> :sswitch_27
        0x27 -> :sswitch_28
        0x28 -> :sswitch_29
        0x29 -> :sswitch_2c
        0x2a -> :sswitch_2d
        0x2b -> :sswitch_2e
        0x2c -> :sswitch_2b
        0x2d -> :sswitch_2f
        0x2e -> :sswitch_30
        0x2f -> :sswitch_e
        0x30 -> :sswitch_f
        0x31 -> :sswitch_31
        0x32 -> :sswitch_2a
        0x33 -> :sswitch_32
        0x34 -> :sswitch_33
        0x35 -> :sswitch_34
        0x36 -> :sswitch_35
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
