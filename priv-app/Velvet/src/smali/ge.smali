.class public final Lge;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static ge:Lgo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1151
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1152
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1153
    new-instance v0, Lgf;

    invoke-direct {v0}, Lgf;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    .line 1171
    :goto_0
    return-void

    .line 1154
    :cond_0
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 1155
    new-instance v0, Lgn;

    invoke-direct {v0}, Lgn;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1156
    :cond_1
    const/16 v1, 0x11

    if-lt v0, v1, :cond_2

    .line 1157
    new-instance v0, Lgm;

    invoke-direct {v0}, Lgm;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1158
    :cond_2
    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 1159
    new-instance v0, Lgl;

    invoke-direct {v0}, Lgl;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1160
    :cond_3
    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 1161
    new-instance v0, Lgk;

    invoke-direct {v0}, Lgk;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1162
    :cond_4
    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 1163
    new-instance v0, Lgj;

    invoke-direct {v0}, Lgj;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1164
    :cond_5
    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 1165
    new-instance v0, Lgi;

    invoke-direct {v0}, Lgi;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1166
    :cond_6
    const/4 v1, 0x7

    if-lt v0, v1, :cond_7

    .line 1167
    new-instance v0, Lgh;

    invoke-direct {v0}, Lgh;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0

    .line 1169
    :cond_7
    new-instance v0, Lgg;

    invoke-direct {v0}, Lgg;-><init>()V

    sput-object v0, Lge;->ge:Lgo;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 1579
    sget-object v0, Lge;->ge:Lgo;

    const/4 v1, 0x0

    invoke-interface {v0, p0, p1, v1}, Lgo;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1580
    return-void
.end method

.method public static a(Landroid/view/View;Lep;)V
    .locals 1

    .prologue
    .line 1343
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1}, Lgo;->a(Landroid/view/View;Lep;)V

    .line 1344
    return-void
.end method

.method public static a(Landroid/view/View;Lhz;)V
    .locals 1

    .prologue
    .line 1328
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1}, Lgo;->a(Landroid/view/View;Lhz;)V

    .line 1329
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1421
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1}, Lgo;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1422
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 1438
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1, p2, p3}, Lgo;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 1439
    return-void
.end method

.method public static b(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 1181
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1}, Lgo;->b(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1480
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1}, Lgo;->c(Landroid/view/View;I)V

    .line 1481
    return-void
.end method

.method public static f(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1206
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->f(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1389
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->g(Landroid/view/View;)V

    .line 1390
    return-void
.end method

.method public static h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1456
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->h(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static i(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 1536
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->i(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static j(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1671
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->j(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static k(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    .prologue
    .line 1703
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->k(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public static l(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2242
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0}, Lgo;->l(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1292
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1}, Lgo;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1293
    return-void
.end method

.method public static performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 1498
    sget-object v0, Lge;->ge:Lgo;

    invoke-interface {v0, p0, p1, p2}, Lgo;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
