.class public final Lizl;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dUD:[Lizl;


# instance fields
.field private aez:I

.field private dUE:[J

.field private dUF:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37731
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 37732
    iput v1, p0, Lizl;->aez:I

    sget-object v0, Ljsu;->eCB:[J

    iput-object v0, p0, Lizl;->dUE:[J

    iput v1, p0, Lizl;->dUF:I

    const/4 v0, 0x0

    iput-object v0, p0, Lizl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizl;->eCz:I

    .line 37733
    return-void
.end method

.method public static bdi()[Lizl;
    .locals 2

    .prologue
    .line 37696
    sget-object v0, Lizl;->dUD:[Lizl;

    if-nez v0, :cond_1

    .line 37697
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 37699
    :try_start_0
    sget-object v0, Lizl;->dUD:[Lizl;

    if-nez v0, :cond_0

    .line 37700
    const/4 v0, 0x0

    new-array v0, v0, [Lizl;

    sput-object v0, Lizl;->dUD:[Lizl;

    .line 37702
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37704
    :cond_1
    sget-object v0, Lizl;->dUD:[Lizl;

    return-object v0

    .line 37702
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 37690
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizl;->dUE:[J

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_1

    iget-object v3, p0, Lizl;->dUE:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lizl;->dUE:[J

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lizl;->dUE:[J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btR()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Lizl;->dUE:[J

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_5

    iget-object v4, p0, Lizl;->dUE:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lizl;->dUE:[J

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lizl;->dUE:[J

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizl;->dUF:I

    iget v0, p0, Lizl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizl;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 37747
    iget-object v0, p0, Lizl;->dUE:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lizl;->dUE:[J

    array-length v0, v0

    if-lez v0, :cond_0

    .line 37748
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lizl;->dUE:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 37749
    const/4 v1, 0x1

    iget-object v2, p0, Lizl;->dUE:[J

    aget-wide v2, v2, v0

    invoke-virtual {p1, v1, v2, v3}, Ljsj;->h(IJ)V

    .line 37748
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37752
    :cond_0
    iget v0, p0, Lizl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 37753
    const/4 v0, 0x2

    iget v1, p0, Lizl;->dUF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 37755
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 37756
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 37760
    invoke-super {p0}, Ljsl;->lF()I

    move-result v2

    .line 37761
    iget-object v1, p0, Lizl;->dUE:[J

    if-eqz v1, :cond_2

    iget-object v1, p0, Lizl;->dUE:[J

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v0

    .line 37763
    :goto_0
    iget-object v3, p0, Lizl;->dUE:[J

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 37764
    iget-object v3, p0, Lizl;->dUE:[J

    aget-wide v4, v3, v0

    .line 37765
    invoke-static {v4, v5}, Ljsj;->dJ(J)I

    move-result v3

    add-int/2addr v1, v3

    .line 37763
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37768
    :cond_0
    add-int v0, v2, v1

    .line 37769
    iget-object v1, p0, Lizl;->dUE:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 37771
    :goto_1
    iget v1, p0, Lizl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 37772
    const/4 v1, 0x2

    iget v2, p0, Lizl;->dUF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 37775
    :cond_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
