.class public final Lfry;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bLS:Ljava/lang/CharSequence;

.field public final cBp:Ljava/lang/CharSequence;

.field public final cBq:Ljava/lang/CharSequence;

.field public final cBr:Lfrz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final cBs:Lfrz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final cBt:I

.field public final cBu:I

.field public cBv:Z

.field public final mCardContainer:Lfmt;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mEntry:Lizj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;)V
    .locals 10
    .param p4    # Lfrz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lfrz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v8, -0x1

    .line 30
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    move v9, v8

    invoke-direct/range {v0 .. v9}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;Lfmt;Lizj;II)V

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;Lfmt;Lizj;II)V
    .locals 1
    .param p4    # Lfrz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lfrz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lfmt;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfry;->cBv:Z

    .line 51
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lfry;->bLS:Ljava/lang/CharSequence;

    .line 55
    iput-object p2, p0, Lfry;->cBp:Ljava/lang/CharSequence;

    .line 56
    iput-object p3, p0, Lfry;->cBq:Ljava/lang/CharSequence;

    .line 57
    iput-object p4, p0, Lfry;->cBr:Lfrz;

    .line 58
    iput-object p5, p0, Lfry;->cBs:Lfrz;

    .line 59
    iput-object p6, p0, Lfry;->mCardContainer:Lfmt;

    .line 60
    iput-object p7, p0, Lfry;->mEntry:Lizj;

    .line 61
    iput p8, p0, Lfry;->cBt:I

    .line 62
    iput p9, p0, Lfry;->cBu:I

    .line 63
    return-void
.end method
