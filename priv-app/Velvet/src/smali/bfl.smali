.class public final Lbfl;
.super Ljava/lang/Object;


# instance fields
.field azx:Lcom/google/android/gms/cast/CastDevice;

.field azy:Lbfm;

.field private azz:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/CastDevice;Lbfm;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CastDevice parameter cannot be null"

    invoke-static {p1, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "CastListener parameter cannot be null"

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lbfl;->azx:Lcom/google/android/gms/cast/CastDevice;

    iput-object p2, p0, Lbfl;->azy:Lbfm;

    const/4 v0, 0x0

    iput v0, p0, Lbfl;->azz:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/cast/CastDevice;Lbfm;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbfl;-><init>(Lcom/google/android/gms/cast/CastDevice;Lbfm;)V

    return-void
.end method
