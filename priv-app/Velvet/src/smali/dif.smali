.class public final Ldif;
.super Lepm;
.source "PG"


# instance fields
.field private final byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;[Lcom/google/android/gms/appdatasearch/UsageInfo;)V
    .locals 2

    .prologue
    .line 31
    const-string v0, "IcingUsageReport"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 32
    iput-object p1, p0, Ldif;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 34
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/16 v7, 0x96

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 40
    new-instance v0, Lbhj;

    iget-object v2, p0, Ldif;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lbhj;-><init>(Landroid/content/Context;)V

    sget-object v2, Lbbr;->auv:Lbgx;

    invoke-virtual {v0, v2}, Lbhj;->a(Lbgx;)Lbhj;

    move-result-object v0

    invoke-virtual {v0}, Lbhj;->yx()Lbhi;

    move-result-object v2

    .line 43
    const-wide/16 v4, 0x2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v0}, Lbhi;->b(JLjava/util/concurrent/TimeUnit;)Lbgm;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lbgm;->yk()Z

    move-result v3

    if-nez v3, :cond_0

    .line 45
    const-string v2, "IcingUsageReportRunnable"

    const-string v3, "Failed to connect when reporting usage: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 71
    :goto_0
    return-void

    .line 49
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 50
    iget-object v3, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    array-length v3, v3

    if-le v3, v7, :cond_2

    .line 52
    iget-object v0, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    array-length v0, v0

    div-int/lit16 v3, v0, 0x96

    .line 53
    iget-object v0, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    array-length v0, v0

    rem-int/lit16 v4, v0, 0x96

    .line 54
    const/16 v0, 0x96

    new-array v5, v0, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    move v0, v1

    .line 55
    :goto_1
    if-ge v0, v3, :cond_1

    .line 56
    iget-object v1, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    array-length v6, v5

    mul-int/2addr v6, v0

    const/4 v7, 0x0

    array-length v8, v5

    invoke-static {v1, v6, v5, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    sget-object v1, Lbbr;->auw:Lbcs;

    invoke-interface {v1, v2, v5}, Lbcs;->a(Lbhi;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Lbhm;

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59
    :cond_1
    new-array v0, v4, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 60
    iget-object v1, p0, Ldif;->byH:[Lcom/google/android/gms/appdatasearch/UsageInfo;

    mul-int/lit16 v3, v3, 0x96

    const/4 v4, 0x0

    array-length v5, v0

    invoke-static {v1, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    :cond_2
    sget-object v1, Lbbr;->auw:Lbcs;

    invoke-interface {v1, v2, v0}, Lbcs;->a(Lbhi;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Lbhm;

    move-result-object v0

    invoke-interface {v0}, Lbhm;->yq()Lbho;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 64
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->yk()Z

    move-result v1

    if-nez v1, :cond_3

    .line 65
    const-string v1, "IcingUsageReportRunnable"

    const-string v3, "Failed to report usage: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :cond_3
    invoke-interface {v2}, Lbhi;->disconnect()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lbhi;->disconnect()V

    throw v0
.end method
