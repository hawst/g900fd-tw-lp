.class public final Lexd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfbn;


# instance fields
.field private final cla:Lgbg;

.field private final clb:Ljbp;


# direct methods
.method public constructor <init>(Lizj;Ljbp;)V
    .locals 1
    .param p2    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iput-object v0, p0, Lexd;->cla:Lgbg;

    .line 24
    iput-object p2, p0, Lexd;->clb:Ljbp;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lgbg;)Lizj;
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lexd;->cla:Lgbg;

    invoke-virtual {v0, p1}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 30
    invoke-virtual {p1}, Lgbg;->aEf()Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 31
    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Lizj;

    .line 32
    iget-object v2, p0, Lexd;->clb:Ljbp;

    if-eqz v2, :cond_1

    .line 33
    iget-object v2, v0, Lizj;->dSc:Ljal;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lizj;->dSc:Ljal;

    iget-object v2, v2, Ljal;->dWL:Ljak;

    if-eqz v2, :cond_1

    .line 35
    iget-object v2, v0, Lizj;->dSc:Ljal;

    iget-object v2, v2, Ljal;->dWL:Ljak;

    .line 36
    iget-object v3, v2, Ljak;->aeB:Ljbp;

    if-eqz v3, :cond_2

    .line 37
    iget-object v3, p0, Lexd;->clb:Ljbp;

    invoke-virtual {v3}, Ljbp;->oK()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 38
    iget-object v3, v2, Ljak;->aeB:Ljbp;

    iget-object v4, p0, Lexd;->clb:Ljbp;

    invoke-virtual {v4}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    .line 40
    :cond_0
    iget-object v3, p0, Lexd;->clb:Ljbp;

    invoke-virtual {v3}, Ljbp;->bfb()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 41
    iget-object v2, v2, Ljak;->aeB:Ljbp;

    iget-object v3, p0, Lexd;->clb:Ljbp;

    invoke-virtual {v3}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    .line 49
    :cond_1
    :goto_0
    const/16 v2, 0x10

    invoke-static {v0, v2}, Lgbm;->j(Lizj;I)V

    .line 52
    :goto_1
    return-object v1

    .line 44
    :cond_2
    iget-object v3, p0, Lexd;->clb:Ljbp;

    iput-object v3, v2, Ljak;->aeB:Ljbp;

    goto :goto_0

    .line 52
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final synthetic as(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lgbg;

    invoke-virtual {p0, p1}, Lexd;->a(Lgbg;)Lizj;

    move-result-object v0

    return-object v0
.end method
