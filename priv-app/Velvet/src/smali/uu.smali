.class public final Luu;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private final ob:Landroid/graphics/Paint;

.field private rI:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Luu;->ob:Landroid/graphics/Paint;

    .line 32
    return-void
.end method


# virtual methods
.method public final f(Landroid/graphics/Bitmap;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    iget-object v1, p0, Luu;->rI:Landroid/graphics/Bitmap;

    if-eq p1, v1, :cond_2

    .line 40
    iget-object v1, p0, Luu;->rI:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Luu;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Luu;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v0, v1, v2}, Luu;->invalidate(IIII)V

    .line 43
    :cond_0
    iput-object p1, p0, Luu;->rI:Landroid/graphics/Bitmap;

    .line 44
    iget-object v1, p0, Luu;->rI:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 45
    iget-object v1, p0, Luu;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Luu;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0, v0, v0, v1, v2}, Luu;->invalidate(IIII)V

    .line 47
    :cond_1
    const/4 v0, 0x1

    .line 49
    :cond_2
    return v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Luu;->rI:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Luu;->rI:Landroid/graphics/Bitmap;

    iget-object v1, p0, Luu;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 57
    :cond_0
    return-void
.end method
