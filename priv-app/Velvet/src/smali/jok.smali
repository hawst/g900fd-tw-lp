.class public final Ljok;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewx:[Ljok;


# instance fields
.field private aez:I

.field private dHr:Ljava/lang/String;

.field public ewy:Ljor;

.field private ewz:Ljos;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1154
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1155
    const/4 v0, 0x0

    iput v0, p0, Ljok;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljok;->dHr:Ljava/lang/String;

    iput-object v1, p0, Ljok;->ewy:Ljor;

    iput-object v1, p0, Ljok;->ewz:Ljos;

    iput-object v1, p0, Ljok;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljok;->eCz:I

    .line 1156
    return-void
.end method

.method public static brn()[Ljok;
    .locals 2

    .prologue
    .line 1113
    sget-object v0, Ljok;->ewx:[Ljok;

    if-nez v0, :cond_1

    .line 1114
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1116
    :try_start_0
    sget-object v0, Ljok;->ewx:[Ljok;

    if-nez v0, :cond_0

    .line 1117
    const/4 v0, 0x0

    new-array v0, v0, [Ljok;

    sput-object v0, Ljok;->ewx:[Ljok;

    .line 1119
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1121
    :cond_1
    sget-object v0, Ljok;->ewx:[Ljok;

    return-object v0

    .line 1119
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1107
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljok;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljok;->dHr:Ljava/lang/String;

    iget v0, p0, Ljok;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljok;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljok;->ewy:Ljor;

    if-nez v0, :cond_1

    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    iput-object v0, p0, Ljok;->ewy:Ljor;

    :cond_1
    iget-object v0, p0, Ljok;->ewy:Ljor;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljok;->ewz:Ljos;

    if-nez v0, :cond_2

    new-instance v0, Ljos;

    invoke-direct {v0}, Ljos;-><init>()V

    iput-object v0, p0, Ljok;->ewz:Ljos;

    :cond_2
    iget-object v0, p0, Ljok;->ewz:Ljos;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1171
    iget v0, p0, Ljok;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1172
    const/4 v0, 0x1

    iget-object v1, p0, Ljok;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1174
    :cond_0
    iget-object v0, p0, Ljok;->ewy:Ljor;

    if-eqz v0, :cond_1

    .line 1175
    const/4 v0, 0x2

    iget-object v1, p0, Ljok;->ewy:Ljor;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1177
    :cond_1
    iget-object v0, p0, Ljok;->ewz:Ljos;

    if-eqz v0, :cond_2

    .line 1178
    const/4 v0, 0x3

    iget-object v1, p0, Ljok;->ewz:Ljos;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1180
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1181
    return-void
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Ljok;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1185
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1186
    iget v1, p0, Ljok;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1187
    const/4 v1, 0x1

    iget-object v2, p0, Ljok;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1190
    :cond_0
    iget-object v1, p0, Ljok;->ewy:Ljor;

    if-eqz v1, :cond_1

    .line 1191
    const/4 v1, 0x2

    iget-object v2, p0, Ljok;->ewy:Ljor;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1194
    :cond_1
    iget-object v1, p0, Ljok;->ewz:Ljos;

    if-eqz v1, :cond_2

    .line 1195
    const/4 v1, 0x3

    iget-object v2, p0, Ljok;->ewz:Ljos;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1198
    :cond_2
    return v0
.end method

.method public final xH(Ljava/lang/String;)Ljok;
    .locals 1

    .prologue
    .line 1132
    if-nez p1, :cond_0

    .line 1133
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1135
    :cond_0
    iput-object p1, p0, Ljok;->dHr:Ljava/lang/String;

    .line 1136
    iget v0, p0, Ljok;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljok;->aez:I

    .line 1137
    return-object p0
.end method
