.class public final Lite;
.super Ljsl;
.source "PG"


# instance fields
.field private dHu:J

.field private dHv:I

.field private dHw:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lite;->dHu:J

    iput v2, p0, Lite;->dHv:I

    iput v2, p0, Lite;->dHw:I

    const/4 v0, 0x0

    iput-object v0, p0, Lite;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lite;->eCz:I

    .line 36
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lite;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lite;->dHu:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Lite;->dHv:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Lite;->dHw:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 50
    const/4 v0, 0x1

    iget-wide v2, p0, Lite;->dHu:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 51
    const/4 v0, 0x2

    iget v1, p0, Lite;->dHv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 52
    const/4 v0, 0x3

    iget v1, p0, Lite;->dHw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 53
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 54
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 59
    const/4 v1, 0x1

    iget-wide v2, p0, Lite;->dHu:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    const/4 v1, 0x2

    iget v2, p0, Lite;->dHv:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 63
    const/4 v1, 0x3

    iget v2, p0, Lite;->dHw:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 65
    return v0
.end method
