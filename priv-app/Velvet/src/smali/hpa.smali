.class public final Lhpa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhpd;


# instance fields
.field private synthetic dne:Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lhpa;->dne:Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(JJJ)V
    .locals 7

    .prologue
    .line 25
    iget-object v0, p0, Lhpa;->dne:Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;->ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p3, p4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p5, p6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->setValue(Ljava/lang/Object;)V

    .line 27
    iget-object v1, p0, Lhpa;->dne:Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;

    iget-object v0, p0, Lhpa;->dne:Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;->ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)V

    .line 28
    return-void
.end method
