.class public final Lctb;
.super Landroid/database/ContentObserver;
.source "PG"


# instance fields
.field private final bil:J

.field private final bim:Landroid/os/Handler;

.field private final bin:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final bio:Ljava/lang/Runnable;

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 59
    iput-object p1, p0, Lctb;->mContext:Landroid/content/Context;

    .line 60
    iput-wide p2, p0, Lctb;->bil:J

    .line 62
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lctb;->bin:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lctb;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lctb;->bim:Landroid/os/Handler;

    .line 64
    new-instance v0, Lctc;

    invoke-direct {v0, p0}, Lctc;-><init>(Lctb;)V

    iput-object v0, p0, Lctb;->bio:Ljava/lang/Runnable;

    .line 72
    return-void
.end method

.method static synthetic a(Lctb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lctb;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcjs;)Lctb;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 34
    sget-boolean v1, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjA:Z

    if-nez v1, :cond_0

    .line 35
    const-string v1, "ContactsContentObserver"

    const-string v2, "ContactsContentObserver registered without delta api supported."

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 49
    :goto_0
    return-object v0

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcjs;->MI()I

    move-result v1

    int-to-long v2, v1

    .line 41
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 42
    const-string v1, "ContactsContentObserver"

    const-string v2, "Contacts content observer disabled."

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 46
    :cond_1
    new-instance v0, Lctb;

    invoke-direct {v0, p0, v2, v3}, Lctb;-><init>(Landroid/content/Context;J)V

    .line 47
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v6, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method static synthetic b(Lctb;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lctb;->bin:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method


# virtual methods
.method public final onChange(ZLandroid/net/Uri;)V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lctb;->bin:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lctb;->bim:Landroid/os/Handler;

    iget-object v1, p0, Lctb;->bio:Ljava/lang/Runnable;

    iget-wide v2, p0, Lctb;->bil:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    :cond_0
    return-void
.end method

.method public final unregister()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lctb;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 55
    return-void
.end method
