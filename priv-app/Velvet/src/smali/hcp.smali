.class public final Lhcp;
.super Lenp;
.source "PG"


# instance fields
.field private final bhb:[Landroid/accounts/Account;

.field private synthetic dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/FirstRunActivity;[Landroid/accounts/Account;Lerk;)V
    .locals 2

    .prologue
    .line 460
    iput-object p1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    .line 461
    const-string v0, "UpdateAccountConfigurations"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, p3, v1}, Lenp;-><init>(Ljava/lang/String;Lerk;[I)V

    .line 463
    iput-object p2, p0, Lhcp;->bhb:[Landroid/accounts/Account;

    .line 464
    return-void

    .line 461
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAccountConfigurationHelper:Lhcb;

    iget-object v1, p0, Lhcp;->bhb:[Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lhcb;->b([Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 457
    check-cast p1, Ljava/util/List;

    iget-object v2, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iput-object p1, v2, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    iget-object v2, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-static {v2}, Lcom/google/android/velvet/tg/FirstRunActivity;->a(Lcom/google/android/velvet/tg/FirstRunActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNI()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNA()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNH()V

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dcu:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kM(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    const v3, 0x7f0a0240

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dcu:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kM(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v2, v2, Lcom/google/android/velvet/tg/FirstRunActivity;->dcB:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dcB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->nO(Ljava/lang/String;)Lhcc;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "FirstRunActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to retrieve account configuration for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v2, v2, Lcom/google/android/velvet/tg/FirstRunActivity;->dcB:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dct:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kM(I)V

    goto :goto_0

    :cond_3
    iget-boolean v0, v0, Lhcc;->aPI:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dcu:I

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kM(I)V

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lhcp;->bhb:[Landroid/accounts/Account;

    array-length v5, v4

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v5, :cond_7

    aget-object v6, v4, v3

    iget-object v7, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v8, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/velvet/tg/FirstRunActivity;->nO(Ljava/lang/String;)Lhcc;

    move-result-object v7

    if-nez v7, :cond_6

    const-string v0, "FirstRunActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to retrieve account configuration for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_5
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    iget-boolean v6, v7, Lhcc;->aPI:Z

    if-eqz v6, :cond_5

    move v2, v1

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lhcp;->bhb:[Landroid/accounts/Account;

    array-length v1, v1

    if-nez v1, :cond_9

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunActivity;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KB()V

    :cond_8
    :goto_3
    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-static {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->b(Lcom/google/android/velvet/tg/FirstRunActivity;)V

    goto/16 :goto_0

    :cond_9
    if-nez v2, :cond_c

    iget-object v2, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    if-eqz v0, :cond_a

    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dct:I

    :goto_4
    invoke-virtual {v2, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kM(I)V

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kL(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v1, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget v1, v1, Lcom/google/android/velvet/tg/FirstRunActivity;->dcu:I

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    iget-object v0, v0, Lcom/google/android/velvet/tg/FirstRunActivity;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KB()V

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->kL(I)V

    goto/16 :goto_0

    :cond_c
    if-eqz v0, :cond_8

    iget-object v0, p0, Lhcp;->dcJ:Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNH()V

    goto :goto_3
.end method
