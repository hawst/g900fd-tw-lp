.class public final Lclm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglc;


# static fields
.field private static final baW:Lehs;


# instance fields
.field baX:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lehs;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lehs;-><init>(I)V

    sput-object v0, Lclm;->baW:Lehs;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lgdo;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lclm;->baX:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 50
    iput-object p1, p0, Lclm;->mExecutor:Ljava/util/concurrent/Executor;

    .line 51
    iput-object p2, p0, Lclm;->mSpeechSettings:Lgdo;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lger;Lggg;Lgnj;)V
    .locals 8

    .prologue
    .line 58
    iget-object v7, p0, Lclm;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcln;

    const-string v2, "startRecognition"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcln;-><init>(Lclm;Ljava/lang/String;[ILger;Lggg;Lgnj;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 82
    return-void
.end method

.method final a(Ljava/io/InputStream;Lggg;Lgnj;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 112
    iget-object v0, p0, Lclm;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNu:Ljzz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclm;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNu:Ljzz;

    invoke-virtual {v0}, Ljzz;->bxy()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 113
    :goto_0
    invoke-virtual {p3}, Lgnj;->getMode()I

    move-result v1

    if-ne v1, v5, :cond_3

    const/16 v1, 0x1f40

    .line 115
    :goto_1
    new-array v4, v1, [B

    move v1, v2

    .line 119
    :cond_1
    :goto_2
    :try_start_0
    invoke-static {p1, v4}, Leoo;->a(Ljava/io/InputStream;[B)V

    .line 120
    iget-object v3, p0, Lclm;->baX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    .line 121
    :goto_3
    if-nez v3, :cond_5

    .line 145
    invoke-static {p1}, Leoo;->i(Ljava/io/InputStream;)V

    :goto_4
    return-void

    .line 112
    :cond_2
    iget-object v0, p0, Lclm;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNu:Ljzz;

    invoke-virtual {v0}, Ljzz;->bxx()F

    move-result v0

    goto :goto_0

    .line 113
    :cond_3
    const/16 v1, 0xfa0

    goto :goto_1

    .line 120
    :cond_4
    :try_start_1
    array-length v3, v4

    invoke-static {v4, v3}, Lcom/google/audio/ears/MusicDetector;->process([BI)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    goto :goto_3

    .line 123
    :cond_5
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v3, v3, v0

    if-ltz v3, :cond_7

    .line 124
    add-int/lit8 v1, v1, 0x1

    .line 125
    invoke-virtual {p3}, Lgnj;->getMode()I

    move-result v3

    if-ne v3, v5, :cond_6

    .line 126
    const/4 v3, 0x3

    if-lt v1, v3, :cond_1

    .line 127
    sget-object v3, Lclm;->baW:Lehs;

    invoke-interface {p2, v3}, Lggg;->a(Lehu;)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 145
    :catch_0
    move-exception v0

    invoke-static {p1}, Leoo;->i(Ljava/io/InputStream;)V

    goto :goto_4

    .line 130
    :cond_6
    :try_start_2
    sget-object v0, Lclm;->baW:Lehs;

    invoke-interface {p2, v0}, Lggg;->a(Lehu;)V
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 145
    invoke-static {p1}, Leoo;->i(Ljava/io/InputStream;)V

    goto :goto_4

    :cond_7
    move v1, v2

    .line 134
    goto :goto_2

    .line 140
    :catch_1
    move-exception v0

    .line 141
    :try_start_3
    new-instance v1, Leid;

    const-string v2, "Error reading from input stream."

    invoke-direct {v1, v2, v0}, Leid;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2, v1}, Lggg;->b(Leiq;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 145
    invoke-static {p1}, Leoo;->i(Ljava/io/InputStream;)V

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-static {p1}, Leoo;->i(Ljava/io/InputStream;)V

    throw v0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lclm;->baX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lclm;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lclo;

    const-string v2, "close"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lclo;-><init>(Lclm;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final fd(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 86
    iget-object v1, p0, Lclm;->baX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    :goto_0
    return v0

    .line 90
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/google/audio/ears/MusicDetector;->init(I)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    const/4 v0, 0x1

    goto :goto_0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    const-string v2, "MusicDetectorRecognitionEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception on native init(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v2, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
