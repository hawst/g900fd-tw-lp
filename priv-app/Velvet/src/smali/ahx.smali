.class public final Lahx;
.super Laht;
.source "PG"


# instance fields
.field private final YC:Landroid/util/SparseArray;

.field private final YD:Ljava/util/HashSet;

.field private final YE:Landroid/content/pm/PackageInstaller;

.field private final YF:Lwi;

.field private final YG:Landroid/content/pm/PackageInstaller$SessionCallback;

.field private as:Z

.field private li:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Laht;-><init>()V

    .line 37
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lahx;->YC:Landroid/util/SparseArray;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lahx;->YD:Ljava/util/HashSet;

    .line 151
    new-instance v0, Lahy;

    invoke-direct {v0, p0}, Lahy;-><init>(Lahx;)V

    iput-object v0, p0, Lahx;->YG:Landroid/content/pm/PackageInstaller$SessionCallback;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v0

    iput-object v0, p0, Lahx;->YE:Landroid/content/pm/PackageInstaller;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lyu;->n(Landroid/content/Context;)V

    .line 48
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v0, v0, Lyu;->xn:Lwi;

    iput-object v0, p0, Lahx;->YF:Lwi;

    .line 50
    iput-boolean v1, p0, Lahx;->as:Z

    .line 51
    iput-boolean v1, p0, Lahx;->li:Z

    .line 53
    iget-object v0, p0, Lahx;->YE:Landroid/content/pm/PackageInstaller;

    iget-object v1, p0, Lahx;->YG:Landroid/content/pm/PackageInstaller$SessionCallback;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageInstaller;->registerSessionCallback(Landroid/content/pm/PackageInstaller$SessionCallback;)V

    .line 56
    iget-object v0, p0, Lahx;->YE:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller;->getAllSessions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 57
    iget-object v2, p0, Lahx;->YC:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method

.method static synthetic a(Lahx;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lahx;->YC:Landroid/util/SparseArray;

    return-object v0
.end method

.method private a(Lahv;)V
    .locals 8

    .prologue
    .line 110
    iget-boolean v0, p0, Lahx;->as:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lahx;->li:Z

    if-nez v0, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lahx;->YC:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    iget-object v0, p0, Lahx;->YD:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    :cond_2
    sget-object v2, Lyu;->Mj:Lyu;

    .line 120
    if-eqz v2, :cond_0

    .line 126
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 127
    if-eqz p1, :cond_3

    iget v0, p1, Lahv;->state:I

    if-eqz v0, :cond_3

    .line 128
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_3
    iget-object v0, p0, Lahx;->YC:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_5

    .line 131
    iget-object v0, p0, Lahx;->YC:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 132
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 133
    new-instance v4, Lahv;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getProgress()F

    move-result v0

    const/high16 v7, 0x42c80000    # 100.0f

    mul-float/2addr v0, v7

    float-to-int v0, v0

    invoke-direct {v4, v5, v6, v0}, Lahv;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 138
    :cond_5
    iget-object v0, p0, Lahx;->YC:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 139
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 140
    invoke-virtual {v2, v3}, Lyu;->p(Ljava/util/ArrayList;)V

    .line 143
    :cond_6
    iget-object v0, p0, Lahx;->YD:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lahx;->YD:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    iget-object v3, v2, Lyu;->Kr:Lzi;

    invoke-virtual {v3, v0}, Lzi;->s(Ljava/lang/String;)V

    goto :goto_2

    .line 147
    :cond_7
    iget-object v0, p0, Lahx;->YD:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lahx;Lahv;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lahx;->a(Lahv;)V

    return-void
.end method

.method static synthetic a(Lahx;Landroid/content/pm/PackageInstaller$SessionInfo;Lahz;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lahx;->a(Landroid/content/pm/PackageInstaller$SessionInfo;Lahz;)V

    return-void
.end method

.method private a(Landroid/content/pm/PackageInstaller$SessionInfo;Lahz;)V
    .locals 5

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_1

    .line 77
    iget-object v1, p0, Lahx;->YF:Lwi;

    invoke-virtual {p1}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppIcon()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppLabel()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v0, p2}, Lwi;->a(Ljava/lang/String;Lahz;)V

    invoke-virtual {v1, v0, p2}, Lwi;->b(Ljava/lang/String;Lahz;)Lwj;

    move-result-object v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iput-object v3, v0, Lwj;->title:Ljava/lang/CharSequence;

    :cond_0
    if-eqz v2, :cond_1

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, v1, Lwi;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v1, v1, Lwi;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    .line 80
    :cond_1
    return-void
.end method

.method static synthetic b(Lahx;)Landroid/content/pm/PackageInstaller;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lahx;->YE:Landroid/content/pm/PackageInstaller;

    return-object v0
.end method

.method static synthetic c(Lahx;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lahx;->YD:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final lK()Ljava/util/HashSet;
    .locals 5

    .prologue
    .line 63
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 64
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v2

    .line 65
    iget-object v0, p0, Lahx;->YE:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller;->getAllSessions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 66
    invoke-direct {p0, v0, v2}, Lahx;->a(Landroid/content/pm/PackageInstaller$SessionInfo;Lahz;)V

    .line 67
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 68
    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_1
    return-object v1
.end method

.method public final lL()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahx;->li:Z

    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lahx;->a(Lahv;)V

    .line 90
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lahx;->as:Z

    .line 95
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahx;->as:Z

    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lahx;->a(Lahv;)V

    .line 101
    return-void
.end method

.method public final onStop()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
