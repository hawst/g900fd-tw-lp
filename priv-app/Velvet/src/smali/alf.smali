.class public Lalf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lamd;


# static fields
.field private static adQ:Lalf;


# instance fields
.field private volatile acx:Ljava/lang/String;

.field private adM:Lamb;

.field private adN:Lajw;

.field private volatile adO:Ljava/lang/Boolean;

.field private final adP:Ljava/util/Map;

.field private mContext:Landroid/content/Context;

.field private mThread:Lakf;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lalf;->adP:Ljava/util/Map;

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    invoke-static {p1}, Laky;->F(Landroid/content/Context;)Laky;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lalf;-><init>(Landroid/content/Context;Lakf;)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lakf;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lalf;->adP:Ljava/util/Map;

    .line 60
    if-nez p1, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lalf;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lalf;->mThread:Lakf;

    .line 65
    new-instance v0, Lajw;

    invoke-direct {v0}, Lajw;-><init>()V

    iput-object v0, p0, Lalf;->adN:Lajw;

    .line 66
    iget-object v0, p0, Lalf;->mThread:Lakf;

    new-instance v1, Lalg;

    invoke-direct {v1, p0}, Lalg;-><init>(Lalf;)V

    invoke-interface {v0, v1}, Lakf;->a(Lali;)V

    .line 74
    iget-object v0, p0, Lalf;->mThread:Lakf;

    new-instance v1, Lalh;

    invoke-direct {v1, p0}, Lalh;-><init>(Lalf;)V

    invoke-interface {v0, v1}, Lakf;->a(Lakg;)V

    .line 82
    return-void
.end method

.method public static H(Landroid/content/Context;)Lalf;
    .locals 2

    .prologue
    .line 90
    const-class v1, Lalf;

    monitor-enter v1

    .line 91
    :try_start_0
    sget-object v0, Lalf;->adQ:Lalf;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lalf;

    invoke-direct {v0, p0}, Lalf;-><init>(Landroid/content/Context;)V

    sput-object v0, Lalf;->adQ:Lalf;

    .line 94
    :cond_0
    sget-object v0, Lalf;->adQ:Lalf;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lalf;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lalf;->adO:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lalf;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lalf;->acx:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public final C(Ljava/lang/String;)Lamb;
    .locals 3

    .prologue
    .line 166
    monitor-enter p0

    .line 167
    if-nez p1, :cond_0

    .line 168
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170
    :cond_0
    :try_start_1
    iget-object v0, p0, Lalf;->adP:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamb;

    .line 172
    if-nez v0, :cond_1

    .line 173
    new-instance v0, Lamb;

    invoke-direct {v0, p1, p0}, Lamb;-><init>(Ljava/lang/String;Lamd;)V

    .line 174
    iget-object v1, p0, Lalf;->adP:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v1, p0, Lalf;->adM:Lamb;

    if-nez v1, :cond_1

    .line 176
    iput-object v0, p0, Lalf;->adM:Lamb;

    .line 179
    :cond_1
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v1

    sget-object v2, Lale;->adw:Lale;

    invoke-virtual {v1, v2}, Lald;->a(Lale;)V

    .line 180
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 217
    monitor-enter p0

    .line 218
    if-nez p1, :cond_0

    .line 219
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hit cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 221
    :cond_0
    :try_start_1
    const-string v0, "language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lame;->c(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    const-string v1, "adSenseAdMobHitId"

    iget-object v0, p0, Lalf;->adN:Lajw;

    iget-boolean v0, v0, Lajw;->abw:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    const-string v0, "screenResolution"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lalf;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lalf;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    const-string v0, "usage"

    invoke-static {}, Lald;->mD()Lald;

    move-result-object v1

    invoke-virtual {v1}, Lald;->mF()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    invoke-virtual {v0}, Lald;->mE()Ljava/lang/String;

    .line 230
    iget-object v0, p0, Lalf;->mThread:Lakf;

    invoke-interface {v0, p1}, Lakf;->a(Ljava/util/Map;)V

    .line 232
    const-string v0, "trackingId"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    monitor-exit p0

    return-void

    .line 222
    :cond_1
    invoke-static {}, Lajx;->ml()Lajx;

    move-result-object v0

    invoke-virtual {v0}, Lajx;->mm()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0
.end method
