.class public final Lbrj;
.super Ljava/lang/Object;

# interfaces
.implements Lbgo;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final aHw:Lbmo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbgp;Lbgq;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lbmo;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lbmo;-><init>(Landroid/content/Context;Lbgp;Lbgq;Ljava/lang/String;)V

    iput-object v0, p0, Lbrj;->aHw:Lbmo;

    return-void
.end method

.method public static j(Landroid/content/Intent;)Ljava/util/List;
    .locals 3

    const-string v0, "com.google.android.location.intent.extra.geofence_list"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/android/gms/internal/nl;->g([B)Lcom/google/android/gms/internal/nl;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/LocationRequest;Lbrm;Landroid/os/Looper;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lbrj;->aHw:Lbmo;

    invoke-static {p1}, Lcom/google/android/gms/internal/nj;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/internal/nj;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lbmo;->a(Lcom/google/android/gms/internal/nj;Lbrm;Landroid/os/Looper;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lbrj;->aHw:Lbmo;

    invoke-static {p1}, Lcom/google/android/gms/internal/nj;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/internal/nj;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lbmo;->b(Lcom/google/android/gms/internal/nj;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final connect()V
    .locals 1

    iget-object v0, p0, Lbrj;->aHw:Lbmo;

    invoke-virtual {v0}, Lbmo;->connect()V

    return-void
.end method

.method public final d(Landroid/app/PendingIntent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lbrj;->aHw:Lbmo;

    invoke-virtual {v0, p1}, Lbmo;->d(Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final disconnect()V
    .locals 1

    iget-object v0, p0, Lbrj;->aHw:Lbmo;

    invoke-virtual {v0}, Lbmo;->disconnect()V

    return-void
.end method
