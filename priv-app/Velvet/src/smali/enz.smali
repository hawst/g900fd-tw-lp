.class public Lenz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lenz;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method


# virtual methods
.method public acJ()V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lenz;->mContext:Landroid/content/Context;

    const-string v1, "GEL.GSAPrefs"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 54
    return-void
.end method

.method public final contains(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lenz;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final getBoolean(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lenz;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected final declared-synchronized getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lenz;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lenz;->mContext:Landroid/content/Context;

    const-string v1, "GEL.GSAPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lenz;->mPrefs:Landroid/content/SharedPreferences;

    .line 96
    :cond_0
    iget-object v0, p0, Lenz;->mPrefs:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lenz;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lenz;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
