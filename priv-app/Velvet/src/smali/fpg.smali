.class public Lfpg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cyp:Ljava/lang/String;


# instance fields
.field private final mBundle:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lfpg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfpg;->cyp:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lfpg;->mBundle:Landroid/os/Bundle;

    .line 20
    return-void
.end method

.method public static i(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpg;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lfpg;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 33
    new-instance v1, Lfpg;

    invoke-direct {v1, v0}, Lfpg;-><init>(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final lW(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lfpg;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method
