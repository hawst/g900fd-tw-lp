.class final Lfxh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private cBx:Ljava/lang/String;

.field private cCX:J

.field private cCY:I

.field private cCZ:Ljava/lang/String;

.field private cDa:Ljava/lang/String;

.field private cDb:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(JILjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1
    .param p6    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-wide p1, p0, Lfxh;->cCX:J

    .line 195
    iput p3, p0, Lfxh;->cCY:I

    .line 196
    iput-object p4, p0, Lfxh;->cCZ:Ljava/lang/String;

    .line 197
    iput-object p5, p0, Lfxh;->cDa:Ljava/lang/String;

    .line 198
    iput-object p6, p0, Lfxh;->cDb:Ljava/lang/Integer;

    .line 199
    iput-object p7, p0, Lfxh;->cBx:Ljava/lang/String;

    .line 200
    return-void
.end method


# virtual methods
.method public final aDP()J
    .locals 6

    .prologue
    .line 220
    iget-wide v0, p0, Lfxh;->cCX:J

    const-wide/16 v2, 0x3e8

    iget v4, p0, Lfxh;->cCY:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final bl(Landroid/content/Context;)Lapd;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 229
    new-instance v1, Lapd;

    invoke-direct {v1}, Lapd;-><init>()V

    .line 230
    iget-object v0, p0, Lfxh;->cCZ:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lapd;->cR(Ljava/lang/String;)Lapd;

    .line 231
    iget-object v0, p0, Lfxh;->cDa:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lapd;->cS(Ljava/lang/String;)Lapd;

    .line 232
    iget-object v0, p0, Lfxh;->cDb:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lfxh;->cDb:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lapd;->cP(I)Lapd;

    .line 235
    :cond_0
    iget-object v0, p0, Lfxh;->cBx:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lfxh;->cBx:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lapd;->cQ(Ljava/lang/String;)Lapd;

    .line 238
    :cond_1
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    new-instance v2, Ljava/util/Date;

    invoke-virtual {p0}, Lfxh;->aDP()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapd;->cT(Ljava/lang/String;)Lapd;

    .line 239
    iget v0, p0, Lfxh;->cCY:I

    div-int/lit8 v0, v0, 0x3c

    if-lez v0, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100026

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lapd;->cU(Ljava/lang/String;)Lapd;

    .line 240
    iget v0, p0, Lfxh;->cCY:I

    if-lez v0, :cond_2

    .line 241
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lapd;->cQ(I)Lapd;

    .line 243
    :cond_2
    return-object v1

    .line 239
    :cond_3
    if-gez v0, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100027

    neg-int v4, v0

    new-array v5, v7, [Ljava/lang/Object;

    neg-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 184
    check-cast p1, Lfxh;

    invoke-virtual {p0}, Lfxh;->aDP()J

    move-result-wide v0

    invoke-virtual {p1}, Lfxh;->aDP()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
