.class public final Lkew;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eVh:F

.field private eVi:F

.field private eVj:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 670
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 671
    iput v1, p0, Lkew;->aez:I

    iput v0, p0, Lkew;->eVh:F

    iput v0, p0, Lkew;->eVi:F

    iput v1, p0, Lkew;->eVj:I

    const/4 v0, 0x0

    iput-object v0, p0, Lkew;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkew;->eCz:I

    .line 672
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 594
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkew;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lkew;->eVh:F

    iget v0, p0, Lkew;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkew;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lkew;->eVi:F

    iget v0, p0, Lkew;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkew;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkew;->eVj:I

    iget v0, p0, Lkew;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkew;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 687
    iget v0, p0, Lkew;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 688
    const/4 v0, 0x1

    iget v1, p0, Lkew;->eVh:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 690
    :cond_0
    iget v0, p0, Lkew;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 691
    const/4 v0, 0x2

    iget v1, p0, Lkew;->eVi:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 693
    :cond_1
    iget v0, p0, Lkew;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 694
    const/4 v0, 0x3

    iget v1, p0, Lkew;->eVj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 696
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 697
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 701
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 702
    iget v1, p0, Lkew;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 703
    const/4 v1, 0x1

    iget v2, p0, Lkew;->eVh:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 706
    :cond_0
    iget v1, p0, Lkew;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 707
    const/4 v1, 0x2

    iget v2, p0, Lkew;->eVi:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 710
    :cond_1
    iget v1, p0, Lkew;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 711
    const/4 v1, 0x3

    iget v2, p0, Lkew;->eVj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 714
    :cond_2
    return v0
.end method
