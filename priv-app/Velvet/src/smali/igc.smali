.class abstract enum Ligc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lifw;


# static fields
.field private static final synthetic dBA:[Ligc;

.field private static enum dBw:Ligc;

.field private static enum dBx:Ligc;

.field public static final enum dBy:Ligc;

.field private static enum dBz:Ligc;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 272
    new-instance v0, Ligd;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1, v2}, Ligd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligc;->dBw:Ligc;

    .line 277
    new-instance v0, Lige;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1, v3}, Lige;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligc;->dBx:Ligc;

    .line 282
    new-instance v0, Ligf;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1, v4}, Ligf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligc;->dBy:Ligc;

    .line 287
    new-instance v0, Ligg;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1, v5}, Ligg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligc;->dBz:Ligc;

    .line 271
    const/4 v0, 0x4

    new-array v0, v0, [Ligc;

    sget-object v1, Ligc;->dBw:Ligc;

    aput-object v1, v0, v2

    sget-object v1, Ligc;->dBx:Ligc;

    aput-object v1, v0, v3

    sget-object v1, Ligc;->dBy:Ligc;

    aput-object v1, v0, v4

    sget-object v1, Ligc;->dBz:Ligc;

    aput-object v1, v0, v5

    sput-object v0, Ligc;->dBA:[Ligc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0, p1, p2}, Ligc;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ligc;
    .locals 1

    .prologue
    .line 271
    const-class v0, Ligc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ligc;

    return-object v0
.end method

.method public static values()[Ligc;
    .locals 1

    .prologue
    .line 271
    sget-object v0, Ligc;->dBA:[Ligc;

    invoke-virtual {v0}, [Ligc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ligc;

    return-object v0
.end method


# virtual methods
.method final aWb()Lifw;
    .locals 0

    .prologue
    .line 295
    return-object p0
.end method
