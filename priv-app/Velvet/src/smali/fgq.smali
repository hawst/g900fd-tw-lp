.class public final Lfgq;
.super Lfos;
.source "PG"


# instance fields
.field private final aSd:Ldmf;

.field private synthetic crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;Ldmf;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-direct {p0}, Lfos;-><init>()V

    .line 210
    iput-object p2, p0, Lfgq;->aSd:Ldmf;

    .line 211
    return-void
.end method

.method private static a(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lfox;
    .locals 2

    .prologue
    .line 761
    invoke-static {}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCb()Lfox;

    move-result-object v1

    const-class v0, Ljal;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljal;

    iput-object v0, v1, Lfox;->mFrequentPlaceEntry:Ljal;

    invoke-static {p0}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v0

    iput-object v0, v1, Lfox;->mSource:Ljbp;

    return-object v1
.end method

.method private static h(Lizj;I)Liwk;
    .locals 5

    .prologue
    .line 768
    iget-object v2, p0, Lizj;->dUo:[Liwk;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 769
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 773
    :goto_1
    return-object v0

    .line 768
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 773
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final K(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 445
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mImageLoader:Lesm;

    invoke-interface {v0, p1}, Lesm;->D(Landroid/net/Uri;)Leml;

    .line 446
    return-void
.end method

.method public final Kz()Z
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 266
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    return v0
.end method

.method public final P(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 377
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 379
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TI()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    const-string v3, "SharedPreferencesContext"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Illegal key: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "b"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_1
    const-string v5, "s"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_2
    const-string v5, "i"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_3
    const-string v5, "l"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_4
    const-string v5, "f"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_5
    const-string v0, "r"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    :cond_6
    const-string v0, "SharedPreferencesContext"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unrecognized prefix: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 381
    return-void
.end method

.method public final U(Ljava/util/List;)Ljava/util/List;
    .locals 4

    .prologue
    .line 395
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 397
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 398
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 399
    const-class v3, Ljdj;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 401
    :cond_0
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v0, v1}, Lfdn;->t(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final V(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 683
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 685
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 687
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 688
    const-class v3, Lizv;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 690
    :cond_0
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExecutedUserActionStore:Lfcr;

    invoke-interface {v0, v1}, Lfcr;->P(Ljava/util/List;)V

    .line 691
    return-void
.end method

.method public final a(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)Landroid/graphics/Bitmap;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 744
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 745
    invoke-static {p1, p2}, Lfgq;->a(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lfox;

    move-result-object v0

    iput-boolean p3, v0, Lfox;->cxZ:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfox;->cya:Z

    invoke-virtual {v0}, Lfox;->aCh()Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfgq;->b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V
    .locals 8

    .prologue
    .line 316
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 318
    iget v0, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 339
    const-string v0, "GoogleNowRemoteService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown logging request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 320
    :pswitch_0
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    iget-object v1, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 324
    :pswitch_1
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    iget-object v1, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->mEntry:Lizj;

    iget v2, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxR:I

    iget-object v3, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxS:Lixx;

    invoke-virtual {v0, v1, v2, v3}, Lcpx;->a(Lizj;ILixx;)V

    goto :goto_0

    .line 328
    :pswitch_2
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    iget-object v1, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcpx;->X(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 332
    :pswitch_3
    iget-object v0, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 333
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v3, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    iget-object v4, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v6, v1

    invoke-virtual {v3, v4, v0, v6, v7}, Lcpx;->d(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1

    .line 318
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V
    .locals 3

    .prologue
    .line 305
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 307
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 308
    invoke-static {v0, p2}, Lfgq;->h(Lizj;I)Liwk;

    move-result-object v1

    .line 309
    if-eqz v1, :cond_0

    .line 310
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExecutedUserActionStore:Lfcr;

    invoke-interface {v2, v0, v1}, Lfcr;->e(Lizj;Liwk;)V

    .line 312
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;JIZ)V
    .locals 8

    .prologue
    .line 291
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 293
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v2

    check-cast v2, Lizj;

    .line 294
    const/16 v0, 0x15

    invoke-static {v2, v0}, Lfgq;->h(Lizj;I)Liwk;

    move-result-object v3

    .line 295
    if-eqz v3, :cond_0

    .line 296
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExecutedUserActionStore:Lfcr;

    move-wide v4, p2

    move v6, p4

    move v7, p5

    invoke-interface/range {v1 .. v7}, Lfcr;->a(Lizj;Liwk;JIZ)V

    .line 299
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    .locals 5

    .prologue
    .line 366
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 368
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 369
    const-class v1, Lizj;

    invoke-virtual {p2, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Lizj;

    .line 371
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcpx;->a(Lizj;ILixx;)V

    .line 372
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    invoke-static {v1}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lfaq;->a(Lizj;Ljava/util/Collection;)V

    .line 373
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    .locals 4

    .prologue
    .line 407
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 409
    const-class v0, Ljde;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljde;

    .line 410
    const-class v1, Ljdf;

    invoke-virtual {p2, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Ljdf;

    .line 411
    const-class v2, Lizj;

    invoke-virtual {p3, v2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v2

    check-cast v2, Lizj;

    .line 413
    iget-object v3, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v3, v3, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v3, v0, v1, v2}, Lfdn;->a(Ljde;Ljdf;Lizj;)V

    .line 414
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
    .locals 4

    .prologue
    .line 354
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 356
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 357
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    invoke-static {v0}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v2

    invoke-virtual {v1, v2}, Lfaq;->s(Ljava/util/Collection;)V

    .line 358
    if-eqz p2, :cond_0

    .line 359
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcpx;->a(Lizj;ILixx;)V

    .line 361
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Z
    .locals 3

    .prologue
    .line 517
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 519
    const-class v0, Ljkw;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljkw;

    .line 521
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mGsaConfigFlags:Lchk;

    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v2}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    invoke-virtual {v1}, Lchk;->GL()Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ljkw;Lcom/google/android/search/shared/actions/RemindersConfigFlags;)Lcom/google/android/search/shared/actions/SetReminderAction;

    move-result-object v0

    .line 524
    new-instance v1, Lefl;

    invoke-direct {v1}, Lefl;-><init>()V

    .line 525
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cra:Lhqs;

    invoke-virtual {v2, v0, v1}, Lhqs;->a(Lcom/google/android/search/shared/actions/SetReminderAction;Lefk;)V

    .line 527
    invoke-static {v1}, Livg;->c(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final awf()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 562
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lfgq;->iI(I)V

    .line 563
    return-void
.end method

.method public final ayp()V
    .locals 2

    .prologue
    .line 676
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 678
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cnj:Lfdg;

    invoke-virtual {v0}, Lfdg;->ayp()V

    .line 679
    return-void
.end method

.method public final ayr()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 785
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 786
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cpd:Lfdi;

    invoke-interface {v0}, Lfdi;->ayr()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final azF()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 273
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-interface {v0}, Lcin;->KD()V

    .line 274
    return-void
.end method

.method public final azG()Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 282
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.READ_CALENDAR"

    const-string v2, "READ_CALENDAR is required to get Now cards"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqW:Lfgt;

    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v1}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfgt;->aO(Landroid/content/Context;)Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final azH()V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 420
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v0}, Lfdn;->ays()V

    .line 421
    return-void
.end method

.method public final azI()V
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 438
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayI()V

    .line 439
    return-void
.end method

.method public final azJ()V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 488
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExtraDexReg:Lgtx;

    sget-object v1, Lguc;->ctv:Lgor;

    invoke-virtual {v0, v1}, Lgtx;->b(Lgor;)V

    .line 489
    return-void
.end method

.method public final azK()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 624
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 625
    return-void
.end method

.method public final azL()V
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 631
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->coV:Lgxc;

    invoke-virtual {v0}, Lgxc;->azL()V

    .line 632
    return-void
.end method

.method public final azM()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 636
    iget-object v1, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ldmf;->gb(I)V

    .line 638
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    .line 639
    if-nez v1, :cond_0

    .line 640
    const-string v1, "GoogleNowRemoteService"

    const-string v2, "No active account"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    :goto_0
    return v0

    .line 645
    :cond_0
    :try_start_0
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSearchHistoryHelper:Lcrr;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcrr;->b(Landroid/accounts/Account;ZZ)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 646
    :catch_0
    move-exception v1

    .line 647
    const-string v2, "GoogleNowRemoteService"

    const-string v3, "Error enabling search history"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final azN()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 654
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 656
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 658
    const-string v1, "CONFIGURATION_REMINDERS_ENABLED"

    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->ayl()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 661
    return-object v0
.end method

.method public final azO()V
    .locals 2

    .prologue
    .line 734
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 736
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcxi;->cM(Z)V

    .line 737
    return-void
.end method

.method public final azP()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 822
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 823
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/List;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 497
    :try_start_0
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExtraDexReg:Lgtx;

    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqZ:Livq;

    invoke-static {v0}, Lgtx;->c(Livq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgud;

    .line 498
    invoke-interface {v0, p1}, Lgud;->j(Ljava/util/List;)V

    .line 499
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lgud;->eE(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Lgos; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 510
    :goto_0
    return-object v0

    .line 501
    :catch_0
    move-exception v0

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 503
    iget-object v1, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    new-instance v2, Lfgs;

    invoke-direct {v2, p0, v0}, Lfgs;-><init>(Lfgq;Lgql;)V

    invoke-interface {v1, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 510
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)Landroid/graphics/Bitmap;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 754
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 755
    invoke-static {p1, p2}, Lfgq;->a(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lfox;

    move-result-object v0

    iput-boolean p3, v0, Lfox;->cxZ:Z

    invoke-virtual {v0}, Lfox;->aCh()Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfgq;->b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 779
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 780
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cpd:Lfdi;

    invoke-interface {v0, p1}, Lfdi;->a(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final b(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;
    .locals 10

    .prologue
    .line 792
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 793
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCalendarDataProvider:Leym;

    move-wide v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Leym;->a(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(JZ)V
    .locals 3

    .prologue
    .line 386
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 388
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcxi;->b(JZ)V

    .line 390
    return-void
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 532
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 534
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v3

    check-cast v3, Lizj;

    .line 535
    const/16 v0, 0x22

    new-array v1, v7, [I

    invoke-static {v3, v0, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v4

    .line 536
    if-eqz v4, :cond_0

    .line 537
    new-instance v0, Leye;

    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNetworkClient:Lfcx;

    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v2}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v5, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v5, v5, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mClock:Lemp;

    iget-object v6, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v6, v6, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->ckD:Leuc;

    invoke-direct/range {v0 .. v6}, Leye;-><init>(Lfcx;Landroid/content/Context;Lizj;Liwk;Lemp;Leuc;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leye;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 541
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 703
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 705
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 706
    invoke-static {v0, p2, v2, v2}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->b(Lizj;ILixx;Ljava/lang/Integer;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v1

    .line 708
    invoke-virtual {p0, v1}, Lfgq;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 709
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    new-instance v2, Lexp;

    invoke-direct {v2, v0}, Lexp;-><init>(Lizj;)V

    invoke-virtual {v1, v2}, Lfaq;->a(Lfbn;)V

    .line 710
    return-void
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    .locals 4

    .prologue
    .line 426
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 428
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 429
    const-class v1, Ljde;

    invoke-virtual {p2, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Ljde;

    .line 430
    const-class v2, Liwk;

    invoke-virtual {p3, v2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v2

    check-cast v2, Liwk;

    .line 431
    iget-object v3, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v3, v3, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v3, v0, v1, v2}, Lfdn;->a(Lizj;Ljde;Liwk;)Landroid/os/AsyncTask;

    .line 432
    return-void
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
    .locals 3

    .prologue
    .line 574
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 576
    const-class v0, Ljbj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljbj;

    .line 577
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mFreshenRequestManager:Lfil;

    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v2}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p2}, Lfil;->a(Landroid/content/Context;Ljbj;Z)V

    .line 578
    return-void
.end method

.method public final bl(J)V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 348
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCalendarDataProvider:Leym;

    invoke-interface {v0, p1, p2}, Leym;->bh(J)Z

    .line 349
    return-void
.end method

.method public final c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    .locals 2

    .prologue
    .line 666
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 668
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 669
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v1}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 670
    invoke-static {v0}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->b(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 672
    return-void
.end method

.method public final c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 715
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 717
    const-class v0, Lizj;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 718
    if-eqz p2, :cond_0

    .line 719
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    invoke-virtual {v1, v0, v2}, Lcpx;->c(Lizj;I)Z

    .line 723
    :goto_0
    return-void

    .line 721
    :cond_0
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    invoke-virtual {v1, v0, v2}, Lcpx;->b(Lizj;I)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;
    .locals 2

    .prologue
    .line 800
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 802
    const/16 v0, 0x33

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljed;->hK(Z)Ljed;

    move-result-object v1

    .line 804
    const-class v0, Lixp;

    invoke-virtual {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lixp;

    iput-object v0, v1, Ljed;->edB:Lixp;

    .line 805
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNetworkClient:Lfcx;

    invoke-interface {v0, v1}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v0

    .line 806
    if-eqz v0, :cond_0

    iget-object v1, v0, Ljeh;->een:Lixq;

    if-nez v1, :cond_1

    .line 807
    :cond_0
    const/4 v0, 0x0

    .line 809
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Ljeh;->een:Lixq;

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public final e(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 450
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ldmf;->gb(I)V

    .line 455
    if-eqz p2, :cond_1

    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mImageLoader:Lesm;

    .line 457
    :goto_0
    invoke-interface {v0, p1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v0

    .line 459
    invoke-interface {v0}, Leps;->auB()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 460
    invoke-interface {v0}, Leps;->auC()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 479
    :goto_1
    instance-of v2, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 480
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 482
    :cond_0
    return-object v1

    .line 455
    :cond_1
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqY:Lesm;

    goto :goto_0

    .line 462
    :cond_2
    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v2

    .line 463
    new-instance v3, Lfgr;

    invoke-direct {v3, p0, v2}, Lfgr;-><init>(Lfgq;Livy;)V

    invoke-interface {v0, v3}, Leps;->e(Lemy;)V

    .line 471
    :try_start_0
    invoke-virtual {v2}, Livy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 472
    :catch_0
    move-exception v0

    .line 473
    const-string v2, "GoogleNowRemoteService"

    const-string v3, "Failed to get Drawable"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 476
    goto :goto_1

    .line 474
    :catch_1
    move-exception v0

    .line 475
    const-string v2, "GoogleNowRemoteService"

    const-string v3, "Failed to get Drawable"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_1
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 583
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 585
    new-instance v0, Ljia;

    invoke-direct {v0}, Ljia;-><init>()V

    invoke-virtual {v0, p1}, Ljia;->vs(Ljava/lang/String;)Ljia;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljia;->vt(Ljava/lang/String;)Ljia;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljia;->vu(Ljava/lang/String;)Ljia;

    move-result-object v0

    .line 589
    const/4 v1, 0x4

    invoke-static {v1}, Lfjw;->iQ(I)Ljed;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljed;->hK(Z)Ljed;

    move-result-object v1

    .line 591
    iput-object v0, v1, Ljed;->edE:Ljia;

    .line 592
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNetworkClient:Lfcx;

    invoke-interface {v0, v1}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v0

    .line 593
    if-eqz v0, :cond_0

    iget-object v1, v0, Ljeh;->eeq:Ljib;

    if-nez v1, :cond_1

    .line 594
    :cond_0
    const/4 v0, 0x0

    .line 596
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Ljeh;->eeq:Ljib;

    invoke-virtual {v0}, Ljib;->bmC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final fl(Z)V
    .locals 2

    .prologue
    .line 727
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 729
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcxi;->cL(Z)V

    .line 730
    return-void
.end method

.method public final getVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 217
    const-string v0, "0.1"

    return-object v0
.end method

.method public final iH(I)Z
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 547
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mReminderSmartActionUtil:Leqm;

    invoke-virtual {v0, p1}, Leqm;->il(I)Z

    move-result v0

    return v0
.end method

.method public final iI(I)V
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 569
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    invoke-virtual {v0, p1}, Lfaq;->iv(I)V

    .line 570
    return-void
.end method

.method public final iJ(I)V
    .locals 4

    .prologue
    .line 602
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 604
    invoke-static {p1}, Lgxd;->kG(I)Lgxd;

    move-result-object v0

    .line 605
    if-eqz v0, :cond_0

    .line 606
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->coV:Lgxc;

    invoke-virtual {v0}, Lgxd;->aLn()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lgxc;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, v1, Lgxc;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 608
    :cond_0
    return-void
.end method

.method public final iK(I)V
    .locals 4

    .prologue
    .line 613
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 615
    invoke-static {p1}, Lgxd;->kG(I)Lgxd;

    move-result-object v0

    .line 616
    if-eqz v0, :cond_0

    .line 617
    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v1, v1, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->coV:Lgxc;

    iget-object v2, v1, Lgxc;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v0}, Lgxd;->aLm()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {v1}, Lgxc;->azL()V

    .line 619
    :cond_0
    return-void
.end method

.method public final iL(I)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;
    .locals 2

    .prologue
    .line 814
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 816
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNotificationStore:Lffp;

    invoke-virtual {v0, p1}, Lffp;->iG(I)Lizj;

    move-result-object v0

    .line 817
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public final lL(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 222
    iget-object v2, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ldmf;->gb(I)V

    .line 225
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLoginHelper:Lcrh;

    invoke-virtual {v2, p1}, Lcrh;->iy(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 226
    if-nez v2, :cond_1

    .line 227
    const-string v1, "GoogleNowRemoteService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    iget-object v3, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v3, v3, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-interface {v3, v2}, Lcin;->j(Landroid/accounts/Account;)V

    .line 234
    iget-object v3, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v3, v3, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-interface {v3, v2}, Lcin;->i(Landroid/accounts/Account;)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final lM(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 239
    iget-object v2, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ldmf;->gb(I)V

    .line 242
    iget-object v2, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v2, v2, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLoginHelper:Lcrh;

    invoke-virtual {v2, p1}, Lcrh;->iy(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    .line 243
    if-nez v2, :cond_1

    .line 244
    const-string v1, "GoogleNowRemoteService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_0
    :goto_0
    return v0

    .line 250
    :cond_1
    iget-object v3, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v3, v3, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-interface {v3, v2}, Lcin;->i(Landroid/accounts/Account;)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 255
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-interface {v0, v2}, Lcin;->f(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 256
    goto :goto_0

    .line 259
    :cond_2
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNowOptInHelper:Leux;

    invoke-virtual {v0, v2}, Leux;->D(Landroid/accounts/Account;)Z

    move-result v0

    goto :goto_0
.end method

.method public final lN(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 695
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 697
    new-instance v0, Lgpk;

    iget-object v1, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    invoke-virtual {v1}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgpk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final wt()V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lfgq;->aSd:Ldmf;

    invoke-static {}, Lfgq;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ldmf;->gb(I)V

    .line 554
    iget-object v0, p0, Lfgq;->crb:Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;

    iget-object v0, v0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->invalidate()V

    .line 555
    return-void
.end method
