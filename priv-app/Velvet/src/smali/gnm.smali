.class public final Lgnm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cQo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lgnm;->cQo:Z

    return-void
.end method

.method public static f(Ljvv;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 93
    sget-boolean v0, Lgnm;->cQo:Z

    if-eqz v0, :cond_1

    .line 94
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 95
    const-string v0, "RESULTS: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    invoke-virtual {p0}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v9, :cond_1

    .line 98
    iget-object v0, p0, Ljvv;->eIo:Ljvw;

    .line 99
    if-eqz v0, :cond_1

    iget-object v3, v0, Ljvw;->eIk:[Ljvs;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 100
    iget-object v3, v0, Ljvw;->eIk:[Ljvs;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 101
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "result:\"%s\","

    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v1

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 108
    :cond_1
    return-void
.end method

.method public static nj(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 62
    sget-boolean v0, Lgnm;->cQo:Z

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "TestPlatformLog"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TEST_PLATFORM: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 65
    :cond_0
    return-void
.end method

.method public static nk(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ERROR: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public static setEnabled(Z)V
    .locals 0

    .prologue
    .line 53
    sput-boolean p0, Lgnm;->cQo:Z

    .line 54
    return-void
.end method
