.class public final Lewk;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final bLG:I

.field private final ckC:Ljava/util/Collection;

.field private ckD:Leuc;

.field private final mClock:Lemp;

.field private mEntryProvider:Lfaq;

.field private final mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Lfcx;Ljava/util/Collection;ILemp;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 39
    iput-object p1, p0, Lewk;->mNetworkClient:Lfcx;

    .line 40
    iput-object p2, p0, Lewk;->ckC:Ljava/util/Collection;

    .line 41
    iput p3, p0, Lewk;->bLG:I

    .line 42
    iput-object p4, p0, Lewk;->mClock:Lemp;

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Lfaq;Leuc;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfaq;

    iput-object v0, p0, Lewk;->mEntryProvider:Lfaq;

    .line 51
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuc;

    iput-object v0, p0, Lewk;->ckD:Leuc;

    .line 52
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 27
    iget-object v0, p0, Lewk;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    const/16 v0, 0x19

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v3

    new-instance v6, Liwl;

    invoke-direct {v6}, Liwl;-><init>()V

    iget-object v0, p0, Lewk;->ckC:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iget-object v8, v0, Lizj;->dUo:[Liwk;

    array-length v9, v8

    move v1, v2

    :goto_1
    if-ge v1, v9, :cond_0

    aget-object v10, v8, v1

    invoke-virtual {v10}, Liwk;->getType()I

    move-result v11

    iget v12, p0, Lewk;->bLG:I

    if-ne v11, v12, :cond_1

    new-instance v1, Lgam;

    invoke-direct {v1, v0, v10, v4, v5}, Lgam;-><init>(Lizj;Liwk;J)V

    const/4 v0, 0x1

    new-array v0, v0, [Lizv;

    invoke-virtual {v1}, Lgam;->aDZ()Lizv;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, v6, Liwl;->afC:[Lizv;

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, v6, Liwl;->afC:[Lizv;

    array-length v0, v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :cond_3
    :goto_2
    return-object v0

    :cond_4
    iput-object v6, v3, Ljed;->eds:Liwl;

    iget-object v0, p0, Lewk;->mNetworkClient:Lfcx;

    invoke-interface {v0, v3}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v1, "BatchRecordActionTask"

    const-string v2, "Error sending request to the server"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 27
    check-cast p1, Ljeh;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lewk;->mEntryProvider:Lfaq;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljeh;->eeg:Liwm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lewk;->ckD:Leuc;

    iget-object v1, p1, Ljeh;->eeg:Liwm;

    iget-object v1, v1, Liwm;->dMd:[Ljbt;

    invoke-virtual {v0, v1}, Leuc;->a([Ljbt;)V

    :cond_0
    iget-object v0, p0, Lewk;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->invalidate()V

    :cond_1
    return-void
.end method
