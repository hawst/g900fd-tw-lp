.class public final Lfzo;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 32
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 33
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->aha:Lapo;

    .line 34
    invoke-virtual {v1}, Lapo;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    const v2, 0x7f1101a9

    invoke-virtual {v1}, Lapo;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 37
    :cond_0
    invoke-virtual {v1}, Lapo;->om()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 38
    const v2, 0x7f110080

    invoke-virtual {v1}, Lapo;->ol()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 40
    :cond_1
    invoke-virtual {v1}, Lapo;->ok()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 41
    const v2, 0x7f1101a8

    invoke-virtual {v1}, Lapo;->oj()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v2, v4}, Lfzo;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 43
    :cond_2
    invoke-virtual {v1}, Lapo;->pM()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 44
    const v2, 0x7f1102f2

    invoke-virtual {v1}, Lapo;->pL()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 46
    :cond_3
    invoke-virtual {v1}, Lapo;->td()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 47
    const v2, 0x7f110368

    invoke-virtual {v1}, Lapo;->tc()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 49
    :cond_4
    invoke-virtual {v1}, Lapo;->pf()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 50
    const v2, 0x7f1100b0

    invoke-virtual {v1}, Lapo;->oo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 52
    :cond_5
    const v2, 0x7f110369

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 53
    invoke-virtual {p0}, Lfzo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .line 54
    iget-object v5, v1, Lapo;->aly:[Ljbm;

    array-length v6, v5

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_6

    aget-object v7, v5, v2

    .line 55
    const v1, 0x7f040142

    invoke-virtual {v4, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 57
    const v8, 0x7f1102ea

    invoke-virtual {v7}, Ljbm;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v8, v9}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 58
    const v8, 0x7f1102ec

    invoke-virtual {v7}, Ljbm;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v8, v7}, Lfzo;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 59
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 54
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 61
    :cond_6
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f040141

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
