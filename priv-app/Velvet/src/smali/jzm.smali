.class public final Ljzm;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajk:Ljava/lang/String;

.field private ako:Ljava/lang/String;

.field private eNP:Z

.field public eNQ:[Ljava/lang/String;

.field public eNR:[Ljava/lang/String;

.field private eNS:I

.field private eNT:I

.field private eNU:I

.field private eNV:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2170
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2171
    iput v1, p0, Ljzm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzm;->ajk:Ljava/lang/String;

    iput-boolean v1, p0, Ljzm;->eNP:Z

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljzm;->eNQ:[Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljzm;->ako:Ljava/lang/String;

    iput v1, p0, Ljzm;->eNS:I

    iput v1, p0, Ljzm;->eNT:I

    iput v1, p0, Ljzm;->eNU:I

    iput-boolean v1, p0, Ljzm;->eNV:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljzm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzm;->eCz:I

    .line 2172
    return-void
.end method


# virtual methods
.method public final Ag(Ljava/lang/String;)Ljzm;
    .locals 1

    .prologue
    .line 2031
    if-nez p1, :cond_0

    .line 2032
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2034
    :cond_0
    iput-object p1, p0, Ljzm;->ajk:Ljava/lang/String;

    .line 2035
    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzm;->aez:I

    .line 2036
    return-object p0
.end method

.method public final Ah(Ljava/lang/String;)Ljzm;
    .locals 1

    .prologue
    .line 2078
    if-nez p1, :cond_0

    .line 2079
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2081
    :cond_0
    iput-object p1, p0, Ljzm;->ako:Ljava/lang/String;

    .line 2082
    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzm;->aez:I

    .line 2083
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2006
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzm;->ajk:Ljava/lang/String;

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzm;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzm;->eNQ:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzm;->eNQ:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzm;->eNQ:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljzm;->eNQ:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljzm;->eNR:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljzm;->eNR:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzm;->ako:Ljava/lang/String;

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzm;->aez:I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzm;->eNS:I

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzm;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzm;->eNT:I

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzm;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzm;->eNU:I

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljzm;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzm;->eNV:Z

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljzm;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzm;->eNP:Z

    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzm;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2193
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2194
    const/4 v0, 0x1

    iget-object v2, p0, Ljzm;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 2196
    :cond_0
    iget-object v0, p0, Ljzm;->eNQ:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljzm;->eNQ:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 2197
    :goto_0
    iget-object v2, p0, Ljzm;->eNQ:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2198
    iget-object v2, p0, Ljzm;->eNQ:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 2199
    if-eqz v2, :cond_1

    .line 2200
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 2197
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2204
    :cond_2
    iget-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 2205
    :goto_1
    iget-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 2206
    iget-object v0, p0, Ljzm;->eNR:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 2207
    if-eqz v0, :cond_3

    .line 2208
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 2205
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2212
    :cond_4
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 2213
    const/4 v0, 0x4

    iget-object v1, p0, Ljzm;->ako:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2215
    :cond_5
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 2216
    const/4 v0, 0x5

    iget v1, p0, Ljzm;->eNS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2218
    :cond_6
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    .line 2219
    const/4 v0, 0x6

    iget v1, p0, Ljzm;->eNT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2221
    :cond_7
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 2222
    const/4 v0, 0x7

    iget v1, p0, Ljzm;->eNU:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2224
    :cond_8
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 2225
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljzm;->eNV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2227
    :cond_9
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    .line 2228
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljzm;->eNP:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2230
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2231
    return-void
.end method

.method public final bxc()Z
    .locals 1

    .prologue
    .line 2050
    iget-boolean v0, p0, Ljzm;->eNP:Z

    return v0
.end method

.method public final bxd()I
    .locals 1

    .prologue
    .line 2097
    iget v0, p0, Ljzm;->eNS:I

    return v0
.end method

.method public final bxe()I
    .locals 1

    .prologue
    .line 2135
    iget v0, p0, Ljzm;->eNU:I

    return v0
.end method

.method public final bxf()Z
    .locals 1

    .prologue
    .line 2143
    iget v0, p0, Ljzm;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bxg()Z
    .locals 1

    .prologue
    .line 2154
    iget-boolean v0, p0, Ljzm;->eNV:Z

    return v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2028
    iget-object v0, p0, Ljzm;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method public final jF(Z)Ljzm;
    .locals 1

    .prologue
    .line 2053
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljzm;->eNP:Z

    .line 2054
    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzm;->aez:I

    .line 2055
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2235
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2236
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2237
    const/4 v1, 0x1

    iget-object v3, p0, Ljzm;->ajk:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2240
    :cond_0
    iget-object v1, p0, Ljzm;->eNQ:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljzm;->eNQ:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 2243
    :goto_0
    iget-object v5, p0, Ljzm;->eNQ:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 2244
    iget-object v5, p0, Ljzm;->eNQ:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 2245
    if-eqz v5, :cond_1

    .line 2246
    add-int/lit8 v4, v4, 0x1

    .line 2247
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 2243
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2251
    :cond_2
    add-int/2addr v0, v3

    .line 2252
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 2254
    :cond_3
    iget-object v1, p0, Ljzm;->eNR:[Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljzm;->eNR:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    .line 2257
    :goto_1
    iget-object v4, p0, Ljzm;->eNR:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_5

    .line 2258
    iget-object v4, p0, Ljzm;->eNR:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 2259
    if-eqz v4, :cond_4

    .line 2260
    add-int/lit8 v3, v3, 0x1

    .line 2261
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 2257
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2265
    :cond_5
    add-int/2addr v0, v1

    .line 2266
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 2268
    :cond_6
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 2269
    const/4 v1, 0x4

    iget-object v2, p0, Ljzm;->ako:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2272
    :cond_7
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    .line 2273
    const/4 v1, 0x5

    iget v2, p0, Ljzm;->eNS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2276
    :cond_8
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_9

    .line 2277
    const/4 v1, 0x6

    iget v2, p0, Ljzm;->eNT:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2280
    :cond_9
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_a

    .line 2281
    const/4 v1, 0x7

    iget v2, p0, Ljzm;->eNU:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2284
    :cond_a
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_b

    .line 2285
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljzm;->eNV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2288
    :cond_b
    iget v1, p0, Ljzm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_c

    .line 2289
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljzm;->eNP:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2292
    :cond_c
    return v0
.end method

.method public final rA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2075
    iget-object v0, p0, Ljzm;->ako:Ljava/lang/String;

    return-object v0
.end method

.method public final tw(I)Ljzm;
    .locals 1

    .prologue
    .line 2100
    const/16 v0, 0x1000

    iput v0, p0, Ljzm;->eNS:I

    .line 2101
    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzm;->aez:I

    .line 2102
    return-object p0
.end method

.method public final tx(I)Ljzm;
    .locals 1

    .prologue
    .line 2138
    const/16 v0, 0x800

    iput v0, p0, Ljzm;->eNU:I

    .line 2139
    iget v0, p0, Ljzm;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljzm;->aez:I

    .line 2140
    return-object p0
.end method
