.class public final Ljfo;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eiH:[Ljfo;


# instance fields
.field private aeB:Ljbp;

.field private aez:I

.field private agq:Ljava/lang/String;

.field private dXN:Ljcn;

.field private ehX:Z

.field private eiI:J

.field private eiJ:J

.field private eiK:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 15039
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15040
    iput v1, p0, Ljfo;->aez:I

    iput-wide v4, p0, Ljfo;->eiI:J

    iput-boolean v1, p0, Ljfo;->ehX:Z

    const-string v0, ""

    iput-object v0, p0, Ljfo;->agq:Ljava/lang/String;

    iput-object v2, p0, Ljfo;->dXN:Ljcn;

    iput-object v2, p0, Ljfo;->aeB:Ljbp;

    iput-wide v4, p0, Ljfo;->eiJ:J

    iput v1, p0, Ljfo;->eiK:I

    iput-object v2, p0, Ljfo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfo;->eCz:I

    .line 15041
    return-void
.end method

.method public static bjD()[Ljfo;
    .locals 2

    .prologue
    .line 14922
    sget-object v0, Ljfo;->eiH:[Ljfo;

    if-nez v0, :cond_1

    .line 14923
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 14925
    :try_start_0
    sget-object v0, Ljfo;->eiH:[Ljfo;

    if-nez v0, :cond_0

    .line 14926
    const/4 v0, 0x0

    new-array v0, v0, [Ljfo;

    sput-object v0, Ljfo;->eiH:[Ljfo;

    .line 14928
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14930
    :cond_1
    sget-object v0, Ljfo;->eiH:[Ljfo;

    return-object v0

    .line 14928
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 14916
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljfo;->eiI:J

    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfo;->ehX:Z

    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfo;->agq:Ljava/lang/String;

    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfo;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljfo;->dXN:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljfo;->dXN:Ljcn;

    :cond_1
    iget-object v0, p0, Ljfo;->dXN:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljfo;->aeB:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljfo;->aeB:Ljbp;

    :cond_2
    iget-object v0, p0, Ljfo;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljfo;->eiJ:J

    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfo;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfo;->eiK:I

    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfo;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 15060
    iget v0, p0, Ljfo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15061
    const/4 v0, 0x1

    iget-wide v2, p0, Ljfo;->eiI:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 15063
    :cond_0
    iget v0, p0, Ljfo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15064
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfo;->ehX:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15066
    :cond_1
    iget v0, p0, Ljfo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 15067
    const/4 v0, 0x3

    iget-object v1, p0, Ljfo;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15069
    :cond_2
    iget-object v0, p0, Ljfo;->dXN:Ljcn;

    if-eqz v0, :cond_3

    .line 15070
    const/4 v0, 0x4

    iget-object v1, p0, Ljfo;->dXN:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15072
    :cond_3
    iget-object v0, p0, Ljfo;->aeB:Ljbp;

    if-eqz v0, :cond_4

    .line 15073
    const/4 v0, 0x5

    iget-object v1, p0, Ljfo;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15075
    :cond_4
    iget v0, p0, Ljfo;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 15076
    const/4 v0, 0x6

    iget-wide v2, p0, Ljfo;->eiJ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 15078
    :cond_5
    iget v0, p0, Ljfo;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 15079
    const/4 v0, 0x7

    iget v1, p0, Ljfo;->eiK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 15081
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15082
    return-void
.end method

.method public final bjE()J
    .locals 2

    .prologue
    .line 14938
    iget-wide v0, p0, Ljfo;->eiI:J

    return-wide v0
.end method

.method public final bjF()Z
    .locals 1

    .prologue
    .line 14957
    iget-boolean v0, p0, Ljfo;->ehX:Z

    return v0
.end method

.method public final bjG()Z
    .locals 1

    .prologue
    .line 14965
    iget v0, p0, Ljfo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final de(J)Ljfo;
    .locals 1

    .prologue
    .line 14941
    iput-wide p1, p0, Ljfo;->eiI:J

    .line 14942
    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfo;->aez:I

    .line 14943
    return-object p0
.end method

.method public final if(Z)Ljfo;
    .locals 1

    .prologue
    .line 14960
    iput-boolean p1, p0, Ljfo;->ehX:Z

    .line 14961
    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfo;->aez:I

    .line 14962
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 15086
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 15087
    iget v1, p0, Ljfo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15088
    const/4 v1, 0x1

    iget-wide v2, p0, Ljfo;->eiI:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 15091
    :cond_0
    iget v1, p0, Ljfo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15092
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfo;->ehX:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 15095
    :cond_1
    iget v1, p0, Ljfo;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 15096
    const/4 v1, 0x3

    iget-object v2, p0, Ljfo;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15099
    :cond_2
    iget-object v1, p0, Ljfo;->dXN:Ljcn;

    if-eqz v1, :cond_3

    .line 15100
    const/4 v1, 0x4

    iget-object v2, p0, Ljfo;->dXN:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15103
    :cond_3
    iget-object v1, p0, Ljfo;->aeB:Ljbp;

    if-eqz v1, :cond_4

    .line 15104
    const/4 v1, 0x5

    iget-object v2, p0, Ljfo;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15107
    :cond_4
    iget v1, p0, Ljfo;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 15108
    const/4 v1, 0x6

    iget-wide v2, p0, Ljfo;->eiJ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 15111
    :cond_5
    iget v1, p0, Ljfo;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 15112
    const/4 v1, 0x7

    iget v2, p0, Ljfo;->eiK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15115
    :cond_6
    return v0
.end method

.method public final us(Ljava/lang/String;)Ljfo;
    .locals 1

    .prologue
    .line 14979
    if-nez p1, :cond_0

    .line 14980
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14982
    :cond_0
    iput-object p1, p0, Ljfo;->agq:Ljava/lang/String;

    .line 14983
    iget v0, p0, Ljfo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfo;->aez:I

    .line 14984
    return-object p0
.end method
