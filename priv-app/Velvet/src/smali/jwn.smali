.class public final Ljwn;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eJk:[Ljwn;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 294
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 295
    const/4 v0, 0x0

    iput v0, p0, Ljwn;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljwn;->bmI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwn;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljwn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwn;->eCz:I

    .line 296
    return-void
.end method

.method public static bvm()[Ljwn;
    .locals 2

    .prologue
    .line 237
    sget-object v0, Ljwn;->eJk:[Ljwn;

    if-nez v0, :cond_1

    .line 238
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 240
    :try_start_0
    sget-object v0, Ljwn;->eJk:[Ljwn;

    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    new-array v0, v0, [Ljwn;

    sput-object v0, Ljwn;->eJk:[Ljwn;

    .line 243
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :cond_1
    sget-object v0, Ljwn;->eJk:[Ljwn;

    return-object v0

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwn;->bmI:Ljava/lang/String;

    iget v0, p0, Ljwn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwn;->dHr:Ljava/lang/String;

    iget v0, p0, Ljwn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwn;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 310
    iget v0, p0, Ljwn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 311
    const/4 v0, 0x1

    iget-object v1, p0, Ljwn;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 313
    :cond_0
    iget v0, p0, Ljwn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 314
    const/4 v0, 0x2

    iget-object v1, p0, Ljwn;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 316
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 317
    return-void
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Ljwn;->bmI:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Ljwn;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 321
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 322
    iget v1, p0, Ljwn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 323
    const/4 v1, 0x1

    iget-object v2, p0, Ljwn;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_0
    iget v1, p0, Ljwn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 327
    const/4 v1, 0x2

    iget-object v2, p0, Ljwn;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_1
    return v0
.end method

.method public final zq(Ljava/lang/String;)Ljwn;
    .locals 1

    .prologue
    .line 256
    if-nez p1, :cond_0

    .line 257
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 259
    :cond_0
    iput-object p1, p0, Ljwn;->bmI:Ljava/lang/String;

    .line 260
    iget v0, p0, Ljwn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwn;->aez:I

    .line 261
    return-object p0
.end method

.method public final zr(Ljava/lang/String;)Ljwn;
    .locals 1

    .prologue
    .line 278
    if-nez p1, :cond_0

    .line 279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 281
    :cond_0
    iput-object p1, p0, Ljwn;->dHr:Ljava/lang/String;

    .line 282
    iget v0, p0, Ljwn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwn;->aez:I

    .line 283
    return-object p0
.end method
