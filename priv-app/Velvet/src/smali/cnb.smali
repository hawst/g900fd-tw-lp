.class public final Lcnb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field public final amT:J

.field public final bcK:J

.field public final bcL:J

.field public final bcM:Z


# direct methods
.method public constructor <init>(JJJZ)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-wide p1, p0, Lcnb;->amT:J

    .line 51
    iput-wide p3, p0, Lcnb;->bcK:J

    .line 52
    iput-wide p5, p0, Lcnb;->bcL:J

    .line 53
    iput-boolean p7, p0, Lcnb;->bcM:Z

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 78
    const-string v0, "SearchResultMetadata"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 79
    const-string v0, "requestId"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcnb;->amT:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 80
    const-string v0, "editRequestId"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcnb;->bcK:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 81
    const-string v0, "fetchTimeMillis"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcnb;->bcL:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 82
    const-string v0, "srpWasPrefetched"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcnb;->bcM:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 83
    return-void
.end method
