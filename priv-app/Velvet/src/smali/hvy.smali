.class public final Lhvy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final dsA:I

.field final dsB:I

.field final dsy:I

.field final dsz:I


# direct methods
.method public constructor <init>(Landroid/util/DisplayMetrics;)V
    .locals 7

    .prologue
    const/4 v5, -0x2

    const v4, 0x7f0400a9

    const v3, 0x7f0400a4

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iget v0, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 47
    iget v1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v2, p1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 49
    if-le v0, v1, :cond_4

    .line 55
    :goto_0
    const/16 v2, 0x2ee

    if-lt v1, v2, :cond_0

    const/16 v2, 0x4b0

    if-lt v0, v2, :cond_0

    .line 57
    const v0, 0x43a38000    # 327.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsy:I

    .line 58
    const/high16 v0, 0x43c10000    # 386.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsz:I

    .line 59
    iput v3, p0, Lhvy;->dsA:I

    .line 60
    const v0, 0x7f0400a7

    iput v0, p0, Lhvy;->dsB:I

    .line 86
    :goto_1
    return-void

    .line 61
    :cond_0
    const/16 v2, 0x226

    if-lt v1, v2, :cond_1

    const/16 v2, 0x384

    if-lt v0, v2, :cond_1

    .line 63
    const/high16 v0, 0x43ad0000    # 346.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsy:I

    .line 64
    const/high16 v0, 0x43950000    # 298.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsz:I

    .line 65
    iput v3, p0, Lhvy;->dsA:I

    .line 66
    iput v3, p0, Lhvy;->dsB:I

    goto :goto_1

    .line 67
    :cond_1
    const/16 v2, 0x168

    if-lt v1, v2, :cond_2

    const/16 v2, 0x24e

    if-lt v0, v2, :cond_2

    .line 69
    const/high16 v0, 0x43830000    # 262.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsy:I

    .line 70
    const/high16 v0, 0x43490000    # 201.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsz:I

    .line 71
    iput v3, p0, Lhvy;->dsA:I

    .line 72
    iput v4, p0, Lhvy;->dsB:I

    goto :goto_1

    .line 73
    :cond_2
    const/16 v2, 0x140

    if-lt v1, v2, :cond_3

    const/16 v1, 0x212

    if-lt v0, v1, :cond_3

    .line 75
    const/high16 v0, 0x43700000    # 240.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsy:I

    .line 76
    const/high16 v0, 0x43340000    # 180.0f

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lhvy;->dsz:I

    .line 77
    iput v3, p0, Lhvy;->dsA:I

    .line 78
    iput v4, p0, Lhvy;->dsB:I

    goto :goto_1

    .line 81
    :cond_3
    iput v5, p0, Lhvy;->dsy:I

    .line 82
    iput v5, p0, Lhvy;->dsz:I

    .line 83
    const v0, 0x7f0400a5

    iput v0, p0, Lhvy;->dsA:I

    .line 84
    iput v4, p0, Lhvy;->dsB:I

    goto :goto_1

    :cond_4
    move v6, v1

    move v1, v0

    move v0, v6

    goto/16 :goto_0
.end method
