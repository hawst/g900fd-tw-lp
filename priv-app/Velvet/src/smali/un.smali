.class public final Lun;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laiq;


# instance fields
.field private FW:I

.field private FX:I

.field private FY:Lra;

.field private sd:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V
    .locals 2

    .prologue
    const/16 v1, 0x400

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Laim;->E(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lun;->FW:I

    .line 42
    iput-object p2, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    .line 43
    invoke-static {v1, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lun;->FX:I

    .line 44
    return-void
.end method


# virtual methods
.method public final a(IIILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 90
    iget v0, p0, Lun;->FW:I

    .line 91
    if-nez p4, :cond_0

    .line 92
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 94
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 95
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 96
    neg-int v2, p2

    neg-int v3, p3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 97
    iget-object v2, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 98
    iget-object v1, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 99
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 100
    return-object p4
.end method

.method public final getRotation()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public final gh()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lun;->FW:I

    return v0
.end method

.method public final gi()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public final gj()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public final gk()Lqz;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/high16 v5, 0x44800000    # 1024.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 68
    iget v1, p0, Lun;->FX:I

    if-nez v1, :cond_0

    .line 85
    :goto_0
    return-object v0

    .line 71
    :cond_0
    iget-object v1, p0, Lun;->FY:Lra;

    if-nez v1, :cond_3

    .line 72
    iget-object v1, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v2, v1

    .line 73
    iget-object v1, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    .line 74
    :goto_1
    cmpl-float v3, v2, v5

    if-gtz v3, :cond_1

    cmpl-float v3, v1, v5

    if-lez v3, :cond_2

    .line 75
    :cond_1
    div-float/2addr v2, v4

    .line 76
    div-float/2addr v1, v4

    goto :goto_1

    .line 78
    :cond_2
    float-to-int v3, v2

    float-to-int v4, v1

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 79
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 80
    iget-object v5, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    new-instance v6, Landroid/graphics/Rect;

    float-to-int v2, v2

    float-to-int v1, v1

    invoke-direct {v6, v7, v7, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 81
    iget-object v1, p0, Lun;->sd:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 82
    invoke-virtual {v4, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    new-instance v0, Lra;

    invoke-direct {v0, v3}, Lra;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lun;->FY:Lra;

    .line 85
    :cond_3
    iget-object v0, p0, Lun;->FY:Lra;

    goto :goto_0
.end method
