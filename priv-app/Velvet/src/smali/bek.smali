.class public Lbek;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected axN:Ljava/lang/String;

.field protected axP:Ljava/lang/String;

.field protected axQ:Ljava/lang/String;

.field protected axR:Ljava/lang/String;

.field protected axU:Z

.field protected final axV:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbek;->axV:Ljava/util/Map;

    .line 128
    return-void
.end method


# virtual methods
.method public b(Ljava/lang/String;Z)Lbek;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lbek;->axP:Ljava/lang/String;

    .line 187
    iput-boolean p2, p0, Lbek;->axU:Z

    .line 188
    return-object p0
.end method

.method public final eT(Ljava/lang/String;)Lbek;
    .locals 2

    .prologue
    .line 135
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A valid tableName must be supplied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    iput-object p1, p0, Lbek;->axN:Ljava/lang/String;

    .line 139
    return-object p0
.end method

.method public eU(Ljava/lang/String;)Lbek;
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lbek;->b(Ljava/lang/String;Z)Lbek;

    move-result-object v0

    return-object v0
.end method

.method public eV(Ljava/lang/String;)Lbek;
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lbek;->axQ:Ljava/lang/String;

    .line 193
    return-object p0
.end method

.method public eW(Ljava/lang/String;)Lbek;
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lbek;->axR:Ljava/lang/String;

    .line 198
    return-object p0
.end method

.method public final n(Ljava/lang/String;Ljava/lang/String;)Lbek;
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lbek;->axV:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Corpus map already contains mapping for section "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :cond_0
    iget-object v0, p0, Lbek;->axV:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-object p0
.end method
