.class public final Lhqn;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final dok:Ljava/lang/String;

.field private synthetic dol:Lhqm;


# direct methods
.method public constructor <init>(Lhqm;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lhqn;->dol:Lhqm;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 118
    iput-object p2, p0, Lhqn;->dok:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public final a([Ljcy;)Ljava/util/List;
    .locals 13

    .prologue
    .line 170
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 171
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 172
    array-length v10, p1

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v10, :cond_4

    aget-object v11, p1, v7

    .line 173
    iget-object v4, v11, Ljcy;->ebc:Ljbq;

    .line 175
    iget-object v0, p0, Lhqn;->dol:Lhqm;

    iget-boolean v0, v0, Lhqm;->doj:Z

    if-eqz v0, :cond_2

    iget-object v0, v11, Ljcy;->dYZ:Lixu;

    if-eqz v0, :cond_2

    iget-object v0, v11, Ljcy;->dYZ:Lixu;

    iget-object v0, v0, Lixu;->dOG:Lixv;

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, v11, Ljcy;->dYZ:Lixu;

    iget-object v0, v0, Lixu;->dOG:Lixv;

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lixv;->dMA:Ljbc;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lixv;->dMA:Ljbc;

    invoke-virtual {v0}, Ljbc;->beI()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljbc;->beH()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljbc;->beK()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljbc;->beJ()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 179
    invoke-interface {v8, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 181
    iget-object v0, v11, Ljcy;->dYZ:Lixu;

    invoke-virtual {v0}, Lixu;->baV()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v11, Ljcy;->dYZ:Lixu;

    invoke-virtual {v0}, Lixu;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 184
    :goto_1
    new-instance v0, Lhqq;

    iget-object v2, p0, Lhqn;->dol:Lhqm;

    iget-object v2, v2, Lhqm;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0588

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v5, v11, Ljcy;->dYZ:Lixu;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lhqq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljcx;Ljbq;Lixu;Ljbr;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-interface {v8, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_2
    new-instance v0, Lhqq;

    invoke-virtual {v11}, Ljcy;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11}, Ljcy;->aRx()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v11, Ljcy;->eaZ:Ljcx;

    const/4 v5, 0x0

    iget-object v6, v11, Ljcy;->ebd:Ljbr;

    invoke-direct/range {v0 .. v6}, Lhqq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljcx;Ljbq;Lixu;Ljbr;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 181
    :cond_3
    invoke-virtual {v11}, Ljcy;->getDescription()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 203
    :cond_4
    return-object v9
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 114
    const/16 v0, 0x10

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljed;->hK(Z)Ljed;

    move-result-object v0

    new-instance v1, Ljcr;

    invoke-direct {v1}, Ljcr;-><init>()V

    iget-object v2, p0, Lhqn;->dok:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljcr;->to(Ljava/lang/String;)Ljcr;

    move-result-object v1

    iget-object v2, p0, Lhqn;->dol:Lhqm;

    iget v2, v2, Lhqm;->doi:I

    invoke-virtual {v1, v2}, Ljcr;->oK(I)Ljcr;

    move-result-object v1

    iput-object v1, v0, Ljed;->edz:Ljcr;

    iget-object v1, p0, Lhqn;->dol:Lhqm;

    iget-object v1, v1, Lhqm;->mNetworkClient:Lfcx;

    invoke-interface {v1, v0}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "PlacesApiFetcher"

    const-string v1, "Error fetching place suggestions"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Ljeh;->eel:Ljcs;

    iget-object v0, v0, Ljcs;->eaU:[Ljcy;

    invoke-virtual {p0, v0}, Lhqn;->a([Ljcy;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 114
    check-cast p1, Ljava/util/List;

    iget-object v1, p0, Lhqn;->dol:Lhqm;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lhqm;->bnA:Z

    iget-object v0, v1, Lhqm;->doh:Lhqr;

    invoke-virtual {v0}, Lhqr;->clear()V

    iget-boolean v0, v1, Lhqm;->bnA:Z

    if-nez v0, :cond_0

    iget-object v0, v1, Lhqm;->doh:Lhqr;

    invoke-virtual {v0, p1}, Lhqr;->addAll(Ljava/util/Collection;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
