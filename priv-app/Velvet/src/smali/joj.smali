.class public final Ljoj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewn:[Ljoj;


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field private dQM:Ljava/lang/String;

.field public ewo:[Ljoo;

.field public ewp:[Ljava/lang/String;

.field private ewq:Ljava/lang/String;

.field private ewr:Ljava/lang/String;

.field public ews:[Ljon;

.field public ewt:[Ljok;

.field public ewu:[Ljom;

.field public ewv:[Ljol;

.field private eww:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1659
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1660
    iput v1, p0, Ljoj;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljoj;->afb:Ljava/lang/String;

    invoke-static {}, Ljoo;->brs()[Ljoo;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ewo:[Ljoo;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljoj;->ewq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljoj;->ewr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljoj;->dQM:Ljava/lang/String;

    invoke-static {}, Ljon;->brr()[Ljon;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ews:[Ljon;

    invoke-static {}, Ljok;->brn()[Ljok;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ewt:[Ljok;

    invoke-static {}, Ljom;->brq()[Ljom;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ewu:[Ljom;

    invoke-static {}, Ljol;->bro()[Ljol;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ewv:[Ljol;

    iput-boolean v1, p0, Ljoj;->eww:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljoj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljoj;->eCz:I

    .line 1661
    return-void
.end method

.method public static brj()[Ljoj;
    .locals 2

    .prologue
    .line 1521
    sget-object v0, Ljoj;->ewn:[Ljoj;

    if-nez v0, :cond_1

    .line 1522
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1524
    :try_start_0
    sget-object v0, Ljoj;->ewn:[Ljoj;

    if-nez v0, :cond_0

    .line 1525
    const/4 v0, 0x0

    new-array v0, v0, [Ljoj;

    sput-object v0, Ljoj;->ewn:[Ljoj;

    .line 1527
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1529
    :cond_1
    sget-object v0, Ljoj;->ewn:[Ljoj;

    return-object v0

    .line 1527
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 817
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljoj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoj;->afb:Ljava/lang/String;

    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ewr:Ljava/lang/String;

    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljoj;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoj;->dQM:Ljava/lang/String;

    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljoj;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoj;->ews:[Ljon;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljon;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljoj;->ews:[Ljon;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljon;

    invoke-direct {v3}, Ljon;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljoj;->ews:[Ljon;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljon;

    invoke-direct {v3}, Ljon;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoj;->ews:[Ljon;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoj;->ewt:[Ljok;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljok;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljoj;->ewt:[Ljok;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljok;

    invoke-direct {v3}, Ljok;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljoj;->ewt:[Ljok;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljok;

    invoke-direct {v3}, Ljok;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoj;->ewt:[Ljok;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoj;->ewu:[Ljom;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljom;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljoj;->ewu:[Ljom;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Ljom;

    invoke-direct {v3}, Ljom;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljoj;->ewu:[Ljom;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Ljom;

    invoke-direct {v3}, Ljom;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoj;->ewu:[Ljom;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljoj;->eww:Z

    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljoj;->aez:I

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoj;->ewv:[Ljol;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ljol;

    if-eqz v0, :cond_a

    iget-object v3, p0, Ljoj;->ewv:[Ljol;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    new-instance v3, Ljol;

    invoke-direct {v3}, Ljol;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Ljoj;->ewv:[Ljol;

    array-length v0, v0

    goto :goto_7

    :cond_c
    new-instance v3, Ljol;

    invoke-direct {v3}, Ljol;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoj;->ewv:[Ljol;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoj;->ewq:Ljava/lang/String;

    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoj;->aez:I

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoj;->ewo:[Ljoo;

    if-nez v0, :cond_e

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Ljoo;

    if-eqz v0, :cond_d

    iget-object v3, p0, Ljoj;->ewo:[Ljoo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    new-instance v3, Ljoo;

    invoke-direct {v3}, Ljoo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_e
    iget-object v0, p0, Ljoj;->ewo:[Ljoo;

    array-length v0, v0

    goto :goto_9

    :cond_f
    new-instance v3, Ljoo;

    invoke-direct {v3}, Ljoo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljoj;->ewo:[Ljoo;

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x62

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    if-nez v0, :cond_11

    move v0, v1

    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v3, p0, Ljoj;->ewp:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_12

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_11
    iget-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_b

    :cond_12
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljoj;->ewp:[Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1684
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1685
    const/4 v0, 0x1

    iget-object v2, p0, Ljoj;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1687
    :cond_0
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 1688
    const/4 v0, 0x2

    iget-object v2, p0, Ljoj;->ewr:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1690
    :cond_1
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 1691
    const/4 v0, 0x3

    iget-object v2, p0, Ljoj;->dQM:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1693
    :cond_2
    iget-object v0, p0, Ljoj;->ews:[Ljon;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljoj;->ews:[Ljon;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 1694
    :goto_0
    iget-object v2, p0, Ljoj;->ews:[Ljon;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1695
    iget-object v2, p0, Ljoj;->ews:[Ljon;

    aget-object v2, v2, v0

    .line 1696
    if-eqz v2, :cond_3

    .line 1697
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1694
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1701
    :cond_4
    iget-object v0, p0, Ljoj;->ewt:[Ljok;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljoj;->ewt:[Ljok;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 1702
    :goto_1
    iget-object v2, p0, Ljoj;->ewt:[Ljok;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 1703
    iget-object v2, p0, Ljoj;->ewt:[Ljok;

    aget-object v2, v2, v0

    .line 1704
    if-eqz v2, :cond_5

    .line 1705
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1702
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1709
    :cond_6
    iget-object v0, p0, Ljoj;->ewu:[Ljom;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljoj;->ewu:[Ljom;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 1710
    :goto_2
    iget-object v2, p0, Ljoj;->ewu:[Ljom;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 1711
    iget-object v2, p0, Ljoj;->ewu:[Ljom;

    aget-object v2, v2, v0

    .line 1712
    if-eqz v2, :cond_7

    .line 1713
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1710
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1717
    :cond_8
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 1718
    const/4 v0, 0x7

    iget-boolean v2, p0, Ljoj;->eww:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 1720
    :cond_9
    iget-object v0, p0, Ljoj;->ewv:[Ljol;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljoj;->ewv:[Ljol;

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    .line 1721
    :goto_3
    iget-object v2, p0, Ljoj;->ewv:[Ljol;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 1722
    iget-object v2, p0, Ljoj;->ewv:[Ljol;

    aget-object v2, v2, v0

    .line 1723
    if-eqz v2, :cond_a

    .line 1724
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1721
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1728
    :cond_b
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c

    .line 1729
    const/16 v0, 0xa

    iget-object v2, p0, Ljoj;->ewq:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1731
    :cond_c
    iget-object v0, p0, Ljoj;->ewo:[Ljoo;

    if-eqz v0, :cond_e

    iget-object v0, p0, Ljoj;->ewo:[Ljoo;

    array-length v0, v0

    if-lez v0, :cond_e

    move v0, v1

    .line 1732
    :goto_4
    iget-object v2, p0, Ljoj;->ewo:[Ljoo;

    array-length v2, v2

    if-ge v0, v2, :cond_e

    .line 1733
    iget-object v2, p0, Ljoj;->ewo:[Ljoo;

    aget-object v2, v2, v0

    .line 1734
    if-eqz v2, :cond_d

    .line 1735
    const/16 v3, 0xb

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1732
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1739
    :cond_e
    iget-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_10

    .line 1740
    :goto_5
    iget-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_10

    .line 1741
    iget-object v0, p0, Ljoj;->ewp:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 1742
    if-eqz v0, :cond_f

    .line 1743
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 1740
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1747
    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1748
    return-void
.end method

.method public final alO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1565
    iget-object v0, p0, Ljoj;->ewq:Ljava/lang/String;

    return-object v0
.end method

.method public final baV()Z
    .locals 1

    .prologue
    .line 1548
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bob()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1609
    iget-object v0, p0, Ljoj;->dQM:Ljava/lang/String;

    return-object v0
.end method

.method public final brk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Ljoj;->ewr:Ljava/lang/String;

    return-object v0
.end method

.method public final brl()Z
    .locals 1

    .prologue
    .line 1620
    iget v0, p0, Ljoj;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brm()Z
    .locals 1

    .prologue
    .line 1643
    iget-boolean v0, p0, Ljoj;->eww:Z

    return v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1537
    iget-object v0, p0, Ljoj;->afb:Ljava/lang/String;

    return-object v0
.end method

.method public final iN(Z)Ljoj;
    .locals 1

    .prologue
    .line 1646
    iput-boolean p1, p0, Ljoj;->eww:Z

    .line 1647
    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljoj;->aez:I

    .line 1648
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1752
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1753
    iget v2, p0, Ljoj;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 1754
    const/4 v2, 0x1

    iget-object v3, p0, Ljoj;->afb:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1757
    :cond_0
    iget v2, p0, Ljoj;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 1758
    const/4 v2, 0x2

    iget-object v3, p0, Ljoj;->ewr:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1761
    :cond_1
    iget v2, p0, Ljoj;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    .line 1762
    const/4 v2, 0x3

    iget-object v3, p0, Ljoj;->dQM:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1765
    :cond_2
    iget-object v2, p0, Ljoj;->ews:[Ljon;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljoj;->ews:[Ljon;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 1766
    :goto_0
    iget-object v3, p0, Ljoj;->ews:[Ljon;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 1767
    iget-object v3, p0, Ljoj;->ews:[Ljon;

    aget-object v3, v3, v0

    .line 1768
    if-eqz v3, :cond_3

    .line 1769
    const/4 v4, 0x4

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1766
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1774
    :cond_5
    iget-object v2, p0, Ljoj;->ewt:[Ljok;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljoj;->ewt:[Ljok;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 1775
    :goto_1
    iget-object v3, p0, Ljoj;->ewt:[Ljok;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 1776
    iget-object v3, p0, Ljoj;->ewt:[Ljok;

    aget-object v3, v3, v0

    .line 1777
    if-eqz v3, :cond_6

    .line 1778
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1775
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v2

    .line 1783
    :cond_8
    iget-object v2, p0, Ljoj;->ewu:[Ljom;

    if-eqz v2, :cond_b

    iget-object v2, p0, Ljoj;->ewu:[Ljom;

    array-length v2, v2

    if-lez v2, :cond_b

    move v2, v0

    move v0, v1

    .line 1784
    :goto_2
    iget-object v3, p0, Ljoj;->ewu:[Ljom;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 1785
    iget-object v3, p0, Ljoj;->ewu:[Ljom;

    aget-object v3, v3, v0

    .line 1786
    if-eqz v3, :cond_9

    .line 1787
    const/4 v4, 0x6

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1784
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    move v0, v2

    .line 1792
    :cond_b
    iget v2, p0, Ljoj;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_c

    .line 1793
    const/4 v2, 0x7

    iget-boolean v3, p0, Ljoj;->eww:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1796
    :cond_c
    iget-object v2, p0, Ljoj;->ewv:[Ljol;

    if-eqz v2, :cond_f

    iget-object v2, p0, Ljoj;->ewv:[Ljol;

    array-length v2, v2

    if-lez v2, :cond_f

    move v2, v0

    move v0, v1

    .line 1797
    :goto_3
    iget-object v3, p0, Ljoj;->ewv:[Ljol;

    array-length v3, v3

    if-ge v0, v3, :cond_e

    .line 1798
    iget-object v3, p0, Ljoj;->ewv:[Ljol;

    aget-object v3, v3, v0

    .line 1799
    if-eqz v3, :cond_d

    .line 1800
    const/16 v4, 0x9

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1797
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_e
    move v0, v2

    .line 1805
    :cond_f
    iget v2, p0, Ljoj;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_10

    .line 1806
    const/16 v2, 0xa

    iget-object v3, p0, Ljoj;->ewq:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1809
    :cond_10
    iget-object v2, p0, Ljoj;->ewo:[Ljoo;

    if-eqz v2, :cond_13

    iget-object v2, p0, Ljoj;->ewo:[Ljoo;

    array-length v2, v2

    if-lez v2, :cond_13

    move v2, v0

    move v0, v1

    .line 1810
    :goto_4
    iget-object v3, p0, Ljoj;->ewo:[Ljoo;

    array-length v3, v3

    if-ge v0, v3, :cond_12

    .line 1811
    iget-object v3, p0, Ljoj;->ewo:[Ljoo;

    aget-object v3, v3, v0

    .line 1812
    if-eqz v3, :cond_11

    .line 1813
    const/16 v4, 0xb

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1810
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_12
    move v0, v2

    .line 1818
    :cond_13
    iget-object v2, p0, Ljoj;->ewp:[Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Ljoj;->ewp:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_16

    move v2, v1

    move v3, v1

    .line 1821
    :goto_5
    iget-object v4, p0, Ljoj;->ewp:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_15

    .line 1822
    iget-object v4, p0, Ljoj;->ewp:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1823
    if-eqz v4, :cond_14

    .line 1824
    add-int/lit8 v3, v3, 0x1

    .line 1825
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1821
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1829
    :cond_15
    add-int/2addr v0, v2

    .line 1830
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1832
    :cond_16
    return v0
.end method

.method public final xD(Ljava/lang/String;)Ljoj;
    .locals 1

    .prologue
    .line 1540
    if-nez p1, :cond_0

    .line 1541
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1543
    :cond_0
    iput-object p1, p0, Ljoj;->afb:Ljava/lang/String;

    .line 1544
    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoj;->aez:I

    .line 1545
    return-object p0
.end method

.method public final xE(Ljava/lang/String;)Ljoj;
    .locals 1

    .prologue
    .line 1568
    if-nez p1, :cond_0

    .line 1569
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1571
    :cond_0
    iput-object p1, p0, Ljoj;->ewq:Ljava/lang/String;

    .line 1572
    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoj;->aez:I

    .line 1573
    return-object p0
.end method

.method public final xF(Ljava/lang/String;)Ljoj;
    .locals 1

    .prologue
    .line 1590
    if-nez p1, :cond_0

    .line 1591
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1593
    :cond_0
    iput-object p1, p0, Ljoj;->ewr:Ljava/lang/String;

    .line 1594
    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljoj;->aez:I

    .line 1595
    return-object p0
.end method

.method public final xG(Ljava/lang/String;)Ljoj;
    .locals 1

    .prologue
    .line 1612
    if-nez p1, :cond_0

    .line 1613
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1615
    :cond_0
    iput-object p1, p0, Ljoj;->dQM:Ljava/lang/String;

    .line 1616
    iget v0, p0, Ljoj;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljoj;->aez:I

    .line 1617
    return-object p0
.end method
