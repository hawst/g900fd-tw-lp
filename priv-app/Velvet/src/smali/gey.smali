.class final Lgey;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field private final cHI:I

.field private final cIi:Lgfs;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cIm:Lglw;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cIn:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(ILgfs;Lglw;)V
    .locals 1
    .param p2    # Lgfs;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lglw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171
    const-string v0, "MicrophoneReader"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 172
    iput p1, p0, Lgey;->cHI:I

    .line 173
    iput-object p2, p0, Lgey;->cIi:Lgfs;

    .line 174
    iput-object p3, p0, Lgey;->cIm:Lglw;

    .line 175
    return-void
.end method


# virtual methods
.method public final aFw()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lgey;->cIn:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 183
    return-void
.end method

.method public final run()V
    .locals 12

    .prologue
    .line 187
    iget-object v0, p0, Lgey;->cIn:Ljava/io/InputStream;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget v0, p0, Lgey;->cHI:I

    new-array v6, v0, [B

    .line 190
    const/4 v5, 0x1

    move v0, v5

    .line 192
    :goto_0
    :try_start_0
    iget-object v1, p0, Lgey;->cIn:Ljava/io/InputStream;

    invoke-virtual {v1, v6}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v1, -0x1

    if-eq v4, v1, :cond_6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_6

    .line 194
    if-eqz v0, :cond_8

    .line 195
    iget-object v0, p0, Lgey;->cIm:Lglw;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lgey;->cIm:Lglw;

    invoke-interface {v0}, Lglw;->Nr()V

    .line 198
    :cond_0
    const/4 v0, 0x0

    move v5, v0

    .line 201
    :goto_1
    iget-object v0, p0, Lgey;->cIi:Lgfs;

    if-eqz v0, :cond_7

    .line 202
    iget-object v7, p0, Lgey;->cIi:Lgfs;

    const/4 v8, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    div-int/lit8 v9, v4, 0x2

    add-int/2addr v4, v8

    :goto_2
    const/4 v8, 0x2

    if-lt v4, v8, :cond_1

    add-int/lit8 v8, v4, -0x1

    aget-byte v8, v6, v8

    shl-int/lit8 v8, v8, 0x8

    add-int/lit8 v10, v4, -0x2

    aget-byte v10, v6, v10

    and-int/lit16 v10, v10, 0xff

    add-int/2addr v8, v10

    int-to-short v8, v8

    int-to-long v10, v8

    add-long/2addr v2, v10

    mul-int/2addr v8, v8

    int-to-long v10, v8

    add-long/2addr v0, v10

    add-int/lit8 v4, v4, -0x2

    goto :goto_2

    :cond_1
    int-to-long v10, v9

    mul-long/2addr v0, v10

    mul-long/2addr v2, v2

    sub-long/2addr v0, v2

    mul-int v2, v9, v9

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v1, v0

    iget-boolean v0, v7, Lgfs;->cIT:Z

    if-nez v0, :cond_2

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_2

    const-string v0, "SpeechLevelGenerator"

    const-string v2, "Really low audio levels detected. The audio input may have issues."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, v7, Lgfs;->cIT:Z

    :cond_2
    iget v0, v7, Lgfs;->cIS:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    const v0, 0x3f7fbe77    # 0.999f

    iget v2, v7, Lgfs;->cIS:F

    mul-float/2addr v0, v2

    const v2, 0x3a83126f    # 0.001f

    mul-float/2addr v2, v1

    add-float/2addr v0, v2

    iput v0, v7, Lgfs;->cIS:F

    :goto_3
    const/high16 v0, -0x3d100000    # -120.0f

    iget v2, v7, Lgfs;->cIS:F

    float-to-double v2, v2

    const-wide/16 v8, 0x0

    cmpl-double v2, v2, v8

    if-lez v2, :cond_3

    iget v2, v7, Lgfs;->cIS:F

    div-float v2, v1, v2

    float-to-double v2, v2

    const-wide v8, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v2, v2, v8

    if-lez v2, :cond_3

    const/high16 v0, 0x41200000    # 10.0f

    iget v2, v7, Lgfs;->cIS:F

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    :cond_3
    iget-object v1, v7, Lgfs;->mSpeechLevelSource:Lequ;

    const/high16 v2, -0x40000000    # -2.0f

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v2, -0x40000000    # -2.0f

    sub-float/2addr v0, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v0, v2

    const/high16 v2, 0x41400000    # 12.0f

    div-float/2addr v0, v2

    float-to-int v0, v0

    const/16 v2, 0x1e

    if-ge v0, v2, :cond_5

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v1, v0}, Lequ;->gp(I)V

    move v0, v5

    goto/16 :goto_0

    :cond_4
    const v0, 0x3f733333    # 0.95f

    iget v2, v7, Lgfs;->cIS:F

    mul-float/2addr v0, v2

    const v2, 0x3d4ccccd    # 0.05f

    mul-float/2addr v2, v1

    add-float/2addr v0, v2

    iput v0, v7, Lgfs;->cIS:F
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 209
    :catch_0
    move-exception v0

    iget-object v0, p0, Lgey;->cIn:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 210
    :goto_5
    return-void

    .line 202
    :cond_5
    :try_start_1
    div-int/lit8 v0, v0, 0xa
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    mul-int/lit8 v0, v0, 0xa

    goto :goto_4

    .line 209
    :cond_6
    iget-object v0, p0, Lgey;->cIn:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_5

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lgey;->cIn:Ljava/io/InputStream;

    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :cond_7
    move v0, v5

    goto/16 :goto_0

    :cond_8
    move v5, v0

    goto/16 :goto_1
.end method

.method public final setInputStream(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lgey;->cIn:Ljava/io/InputStream;

    .line 179
    return-void
.end method
