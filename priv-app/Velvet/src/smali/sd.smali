.class public final Lsd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic yG:Lcom/android/launcher3/AppsCustomizePagedView;

.field private synthetic yL:I

.field private synthetic yM:I

.field private synthetic yN:Lacr;

.field private synthetic yO:Z

.field private synthetic yP:I

.field private synthetic yQ:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/AppsCustomizePagedView;IILacr;ZILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1181
    iput-object p1, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    iput p2, p0, Lsd;->yL:I

    iput p3, p0, Lsd;->yM:I

    iput-object p4, p0, Lsd;->yN:Lacr;

    iput-boolean p5, p0, Lsd;->yO:Z

    iput p6, p0, Lsd;->yP:I

    iput-object p7, p0, Lsd;->yQ:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1184
    iget v3, p0, Lsd;->yL:I

    .line 1185
    iget v4, p0, Lsd;->yM:I

    .line 1186
    iget-object v0, p0, Lsd;->yN:Lacr;

    invoke-virtual {v0}, Lacr;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1187
    iget-object v0, p0, Lsd;->yN:Lacr;

    invoke-virtual {v0, v1}, Lacr;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PagedViewWidget;

    .line 1188
    invoke-virtual {v0}, Lcom/android/launcher3/PagedViewWidget;->jN()[I

    move-result-object v0

    .line 1189
    aget v3, v0, v1

    .line 1190
    const/4 v1, 0x1

    aget v4, v0, v1

    .line 1193
    :cond_0
    iget-object v0, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v0

    iget-object v1, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->b(Lcom/android/launcher3/AppsCustomizePagedView;)Laco;

    move-result-object v1

    invoke-virtual {v0, v3, v4, v1}, Lafb;->a(IILaco;)V

    .line 1195
    iget-boolean v0, p0, Lsd;->yO:Z

    if-eqz v0, :cond_1

    .line 1196
    new-instance v0, Lsg;

    iget v1, p0, Lsd;->yP:I

    iget-object v2, p0, Lsd;->yQ:Ljava/util/ArrayList;

    iget-object v6, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v6}, Lcom/android/launcher3/AppsCustomizePagedView;->ee()Lafb;

    move-result-object v7

    move-object v6, v5

    invoke-direct/range {v0 .. v7}, Lsg;-><init>(ILjava/util/ArrayList;IILsf;Lsf;Lafb;)V

    .line 1198
    iget-object v1, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v1, v5, v0}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/AppsCustomizePagedView;Lrv;Lsg;)V

    .line 1199
    iget-object v1, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    iget-boolean v2, p0, Lsd;->yO:Z

    invoke-static {v1, v0, v2}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/AppsCustomizePagedView;Lsg;Z)V

    .line 1208
    :goto_0
    iget-object v0, p0, Lsd;->yN:Lacr;

    invoke-virtual {v0, v5}, Lacr;->f(Ljava/lang/Runnable;)V

    .line 1209
    return-void

    .line 1201
    :cond_1
    iget-object v0, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->c(Lcom/android/launcher3/AppsCustomizePagedView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1202
    iget-object v0, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->d(Lcom/android/launcher3/AppsCustomizePagedView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1204
    :cond_2
    iget-object v6, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    iget v7, p0, Lsd;->yP:I

    iget-object v8, p0, Lsd;->yQ:Ljava/util/ArrayList;

    iget-object v0, p0, Lsd;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->e(Lcom/android/launcher3/AppsCustomizePagedView;)I

    move-result v11

    move v9, v3

    move v10, v4

    invoke-static/range {v6 .. v11}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/AppsCustomizePagedView;ILjava/util/ArrayList;III)V

    goto :goto_0
.end method
