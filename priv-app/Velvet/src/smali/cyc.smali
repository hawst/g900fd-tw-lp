.class public abstract Lcyc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcxt;


# instance fields
.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcyc;-><init>(Lcke;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Lcke;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcyc;->mSettings:Lcke;

    .line 39
    return-void
.end method


# virtual methods
.method protected final TL()Lcke;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcyc;->mSettings:Lcke;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 43
    iget-object v0, p0, Lcyc;->mSettings:Lcke;

    return-object v0

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public i(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method
