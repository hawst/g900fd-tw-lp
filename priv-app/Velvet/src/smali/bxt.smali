.class final Lbxt;
.super Leea;
.source "PG"


# instance fields
.field private synthetic aOl:Lbxr;


# direct methods
.method constructor <init>(Lbxr;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lbxt;->aOl:Lbxr;

    invoke-direct {p0}, Leea;-><init>()V

    return-void
.end method


# virtual methods
.method public final A(F)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 131
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-boolean v0, v0, Lbxr;->aOi:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-boolean v0, v0, Lbxr;->aOh:Z

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 132
    :cond_0
    const/high16 v0, 0x43960000    # 300.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-boolean v0, v0, Lbxr;->aOh:Z

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-object v1, p0, Lbxt;->aOl:Lbxr;

    iget-object v1, v1, Lbxr;->aOf:[F

    aget v1, v1, v3

    invoke-static {v0, v4, v3, v1}, Lbxr;->a(Lbxr;IIF)V

    .line 138
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    const/16 v1, 0x58

    invoke-virtual {v0, v1}, Lbxr;->ep(I)V

    goto :goto_0

    .line 139
    :cond_1
    const/high16 v0, -0x3c6a0000    # -300.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-boolean v0, v0, Lbxr;->aOi:Z

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-object v1, p0, Lbxt;->aOl:Lbxr;

    iget-object v1, v1, Lbxr;->aOf:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    invoke-static {v0, v3, v4, v1}, Lbxr;->a(Lbxr;IIF)V

    .line 145
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    const/16 v1, 0x57

    invoke-virtual {v0, v1}, Lbxr;->ep(I)V

    goto :goto_0

    .line 147
    :cond_2
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    invoke-virtual {v0}, Lbxr;->Bt()V

    goto :goto_0
.end method

.method public final onClick()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-object v0, v0, Lbxr;->mState:Lbyi;

    invoke-virtual {v0}, Lbyi;->BH()V

    .line 102
    return-void
.end method

.method public final z(F)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 109
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-object v0, v0, Lbxr;->aOf:[F

    if-nez v0, :cond_0

    .line 110
    iget-object v1, p0, Lbxt;->aOl:Lbxr;

    iget-object v0, v1, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v0, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v2

    iget-object v0, v1, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v0, v7}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v3

    iget-object v0, v1, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v0, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    sub-float v4, v0, v3

    const/4 v5, 0x5

    new-array v5, v5, [F

    sub-float v6, v2, v4

    aput v6, v5, v9

    aput v2, v5, v7

    aput v3, v5, v8

    const/4 v2, 0x3

    aput v0, v5, v2

    const/4 v2, 0x4

    add-float/2addr v0, v4

    aput v0, v5, v2

    iput-object v5, v1, Lbxr;->aOf:[F

    .line 112
    :cond_0
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-object v0, v0, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v0, v7}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    add-float/2addr v0, p1

    .line 113
    iget-object v1, p0, Lbxt;->aOl:Lbxr;

    iget-object v1, v1, Lbxr;->aOf:[F

    aget v1, v1, v8

    .line 118
    iget-object v2, p0, Lbxt;->aOl:Lbxr;

    iget-boolean v2, v2, Lbxr;->aOi:Z

    if-nez v2, :cond_2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_2

    .line 126
    :cond_1
    return-void

    .line 123
    :cond_2
    iget-object v2, p0, Lbxt;->aOl:Lbxr;

    iget-boolean v2, v2, Lbxr;->aOh:Z

    if-nez v2, :cond_3

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 125
    :cond_3
    iget-object v0, p0, Lbxt;->aOl:Lbxr;

    iget-object v1, v0, Lbxr;->aOk:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, v0, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setX(F)V

    goto :goto_0
.end method
