.class public Ldmv;
.super Ldni;
.source "PG"


# instance fields
.field final aXe:Ljava/util/concurrent/Executor;

.field final bCF:Ljava/util/List;

.field final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;Lcjs;Lcke;Ljava/util/List;Lemp;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p2}, Lcjs;->LD()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GsaCommunicationJsHelper"

    invoke-direct {p0, p1, v0, v1}, Ldni;-><init>(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput-object p2, p0, Ldmv;->mConfig:Lcjs;

    .line 49
    iput-object p3, p0, Ldmv;->mSettings:Lcke;

    .line 50
    iput-object p4, p0, Ldmv;->bCF:Ljava/util/List;

    .line 51
    iput-object p5, p0, Ldmv;->mClock:Lemp;

    .line 52
    iput-object p6, p0, Ldmv;->aXe:Ljava/util/concurrent/Executor;

    .line 53
    return-void
.end method


# virtual methods
.method protected a(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ldmw;

    invoke-direct {v0, p0}, Ldmw;-><init>(Ldmv;)V

    invoke-virtual {p1, v0, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public final adB()V
    .locals 5

    .prologue
    .line 67
    iget-object v0, p0, Ldmv;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldmv;->kc(Ljava/lang/String;)V

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Ldmv;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NF()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Ldmv;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LB()Ljava/lang/String;

    move-result-object v0

    .line 71
    iget-object v1, p0, Ldmv;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->NG()Ljava/lang/String;

    move-result-object v1

    .line 74
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldmv;->kc(Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method
