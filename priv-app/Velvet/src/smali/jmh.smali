.class public final Ljmh;
.super Ljsl;
.source "PG"


# static fields
.field public static final ety:Ljsm;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private esQ:[B

.field private esR:Ljava/lang/String;

.field public etA:Ljml;

.field public etB:Ljmk;

.field public etC:Ljmj;

.field public etD:Ljmi;

.field private etE:Ljava/lang/String;

.field private etF:Ljava/lang/String;

.field private etG:Ljava/lang/String;

.field private etH:I

.field private etI:J

.field public etJ:[Ljmm;

.field private etK:Ljava/lang/String;

.field private etL:Ljava/lang/String;

.field private etM:Z

.field private etN:Ljava/lang/String;

.field public etO:[I

.field private etP:Z

.field private etQ:[Ljmf;

.field public etR:[Ljava/lang/String;

.field private ete:D

.field private etf:I

.field private etz:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5386
    const/16 v0, 0xb

    const-class v1, Ljmh;

    const v2, 0xc20beea

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmh;->ety:Ljsm;

    .line 6726
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 7110
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7111
    iput v3, p0, Ljmh;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmh;->agq:Ljava/lang/String;

    iput v3, p0, Ljmh;->etz:I

    iput-object v2, p0, Ljmh;->etA:Ljml;

    iput-object v2, p0, Ljmh;->etB:Ljmk;

    iput-object v2, p0, Ljmh;->etC:Ljmj;

    iput-object v2, p0, Ljmh;->etD:Ljmi;

    const-string v0, ""

    iput-object v0, p0, Ljmh;->etE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmh;->etF:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmh;->ajk:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljmh;->esQ:[B

    const-string v0, ""

    iput-object v0, p0, Ljmh;->esR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmh;->etG:Ljava/lang/String;

    iput v4, p0, Ljmh;->etH:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljmh;->etI:J

    invoke-static {}, Ljmm;->bql()[Ljmm;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etJ:[Ljmm;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljmh;->ete:D

    iput v3, p0, Ljmh;->etf:I

    const-string v0, ""

    iput-object v0, p0, Ljmh;->etK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmh;->etL:Ljava/lang/String;

    iput-boolean v3, p0, Ljmh;->etM:Z

    const-string v0, ""

    iput-object v0, p0, Ljmh;->etN:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljmh;->etO:[I

    iput-boolean v4, p0, Ljmh;->etP:Z

    invoke-static {}, Ljmf;->bpE()[Ljmf;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etQ:[Ljmf;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    iput-object v2, p0, Ljmh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmh;->eCz:I

    .line 7112
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 5379
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->agq:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmh;->etz:I

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmh;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->ajk:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljmh;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etN:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    const v2, 0x8000

    or-int/2addr v0, v2

    iput v0, p0, Ljmh;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljmh;->etA:Ljml;

    if-nez v0, :cond_1

    new-instance v0, Ljml;

    invoke-direct {v0}, Ljml;-><init>()V

    iput-object v0, p0, Ljmh;->etA:Ljml;

    :cond_1
    iget-object v0, p0, Ljmh;->etA:Ljml;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljmh;->etB:Ljmk;

    if-nez v0, :cond_2

    new-instance v0, Ljmk;

    invoke-direct {v0}, Ljmk;-><init>()V

    iput-object v0, p0, Ljmh;->etB:Ljmk;

    :cond_2
    iget-object v0, p0, Ljmh;->etB:Ljmk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljmh;->etC:Ljmj;

    if-nez v0, :cond_3

    new-instance v0, Ljmj;

    invoke-direct {v0}, Ljmj;-><init>()V

    iput-object v0, p0, Ljmh;->etC:Ljmj;

    :cond_3
    iget-object v0, p0, Ljmh;->etC:Ljmj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etE:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljmh;->esQ:[B

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->esR:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etG:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljmh;->etI:J

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljmh;->etJ:[Ljmm;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljmm;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljmh;->etJ:[Ljmm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljmm;

    invoke-direct {v3}, Ljmm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ljmh;->etJ:[Ljmm;

    array-length v0, v0

    goto :goto_1

    :cond_6
    new-instance v3, Ljmm;

    invoke-direct {v3}, Ljmm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljmh;->etJ:[Ljmm;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    iput-wide v2, p0, Ljmh;->ete:D

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmh;->etf:I

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etK:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etL:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmh;->etM:Z

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Ljmh;->etD:Ljmi;

    if-nez v0, :cond_7

    new-instance v0, Ljmi;

    invoke-direct {v0}, Ljmi;-><init>()V

    iput-object v0, p0, Ljmh;->etD:Ljmi;

    :cond_7
    iget-object v0, p0, Ljmh;->etD:Ljmi;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_14
    const/16 v0, 0xb0

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_9

    if-eqz v3, :cond_8

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_9
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljmh;->etO:[I

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    if-nez v0, :cond_b

    array-length v3, v5

    if-ne v2, v3, :cond_b

    iput-object v5, p0, Ljmh;->etO:[I

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Ljmh;->etO:[I

    array-length v0, v0

    goto :goto_5

    :cond_b
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_c

    iget-object v4, p0, Ljmh;->etO:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljmh;->etO:[I

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_d

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_6

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    if-eqz v0, :cond_11

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljmh;->etO:[I

    if-nez v2, :cond_f

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_e

    iget-object v0, p0, Ljmh;->etO:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_10

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_8

    :pswitch_2
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_f
    iget-object v2, p0, Ljmh;->etO:[I

    array-length v2, v2

    goto :goto_7

    :cond_10
    iput-object v4, p0, Ljmh;->etO:[I

    :cond_11
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmh;->etP:Z

    iget v0, p0, Ljmh;->aez:I

    const/high16 v2, 0x10000

    or-int/2addr v0, v2

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_17
    const/16 v0, 0xc2

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljmh;->etQ:[Ljmf;

    if-nez v0, :cond_13

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Ljmf;

    if-eqz v0, :cond_12

    iget-object v3, p0, Ljmh;->etQ:[Ljmf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_14

    new-instance v3, Ljmf;

    invoke-direct {v3}, Ljmf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_13
    iget-object v0, p0, Ljmh;->etQ:[Ljmf;

    array-length v0, v0

    goto :goto_9

    :cond_14
    new-instance v3, Ljmf;

    invoke-direct {v3}, Ljmf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljmh;->etQ:[Ljmf;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Ljmh;->etH:I

    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmh;->etF:Ljava/lang/String;

    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljmh;->aez:I

    goto/16 :goto_0

    :sswitch_1a
    const/16 v0, 0xda

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    if-nez v0, :cond_16

    move v0, v1

    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_15

    iget-object v3, p0, Ljmh;->etR:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_15
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_17

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_16
    iget-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_b

    :cond_17
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljmh;->etR:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x70 -> :sswitch_c
        0x7a -> :sswitch_d
        0x81 -> :sswitch_e
        0x88 -> :sswitch_f
        0x92 -> :sswitch_10
        0x9a -> :sswitch_11
        0xa0 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb0 -> :sswitch_14
        0xb2 -> :sswitch_15
        0xb8 -> :sswitch_16
        0xc2 -> :sswitch_17
        0xc8 -> :sswitch_18
        0xd2 -> :sswitch_19
        0xda -> :sswitch_1a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7149
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7150
    const/4 v0, 0x1

    iget-object v2, p0, Ljmh;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7152
    :cond_0
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7153
    const/4 v0, 0x3

    iget v2, p0, Ljmh;->etz:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 7155
    :cond_1
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_2

    .line 7156
    const/4 v0, 0x4

    iget-object v2, p0, Ljmh;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7158
    :cond_2
    iget v0, p0, Ljmh;->aez:I

    const v2, 0x8000

    and-int/2addr v0, v2

    if-eqz v0, :cond_3

    .line 7159
    const/4 v0, 0x5

    iget-object v2, p0, Ljmh;->etN:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7161
    :cond_3
    iget-object v0, p0, Ljmh;->etA:Ljml;

    if-eqz v0, :cond_4

    .line 7162
    const/4 v0, 0x6

    iget-object v2, p0, Ljmh;->etA:Ljml;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 7164
    :cond_4
    iget-object v0, p0, Ljmh;->etB:Ljmk;

    if-eqz v0, :cond_5

    .line 7165
    const/4 v0, 0x7

    iget-object v2, p0, Ljmh;->etB:Ljmk;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 7167
    :cond_5
    iget-object v0, p0, Ljmh;->etC:Ljmj;

    if-eqz v0, :cond_6

    .line 7168
    const/16 v0, 0x8

    iget-object v2, p0, Ljmh;->etC:Ljmj;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 7170
    :cond_6
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    .line 7171
    const/16 v0, 0x9

    iget-object v2, p0, Ljmh;->etE:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7173
    :cond_7
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 7174
    const/16 v0, 0xa

    iget-object v2, p0, Ljmh;->esQ:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    .line 7176
    :cond_8
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 7177
    const/16 v0, 0xb

    iget-object v2, p0, Ljmh;->esR:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7179
    :cond_9
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_a

    .line 7180
    const/16 v0, 0xc

    iget-object v2, p0, Ljmh;->etG:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7182
    :cond_a
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_b

    .line 7183
    const/16 v0, 0xe

    iget-wide v2, p0, Ljmh;->etI:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 7185
    :cond_b
    iget-object v0, p0, Ljmh;->etJ:[Ljmm;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljmh;->etJ:[Ljmm;

    array-length v0, v0

    if-lez v0, :cond_d

    move v0, v1

    .line 7186
    :goto_0
    iget-object v2, p0, Ljmh;->etJ:[Ljmm;

    array-length v2, v2

    if-ge v0, v2, :cond_d

    .line 7187
    iget-object v2, p0, Ljmh;->etJ:[Ljmm;

    aget-object v2, v2, v0

    .line 7188
    if-eqz v2, :cond_c

    .line 7189
    const/16 v3, 0xf

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 7186
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7193
    :cond_d
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_e

    .line 7194
    const/16 v0, 0x10

    iget-wide v2, p0, Ljmh;->ete:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 7196
    :cond_e
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_f

    .line 7197
    const/16 v0, 0x11

    iget v2, p0, Ljmh;->etf:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 7199
    :cond_f
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_10

    .line 7200
    const/16 v0, 0x12

    iget-object v2, p0, Ljmh;->etK:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7202
    :cond_10
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_11

    .line 7203
    const/16 v0, 0x13

    iget-object v2, p0, Ljmh;->etL:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7205
    :cond_11
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_12

    .line 7206
    const/16 v0, 0x14

    iget-boolean v2, p0, Ljmh;->etM:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 7208
    :cond_12
    iget-object v0, p0, Ljmh;->etD:Ljmi;

    if-eqz v0, :cond_13

    .line 7209
    const/16 v0, 0x15

    iget-object v2, p0, Ljmh;->etD:Ljmi;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 7211
    :cond_13
    iget-object v0, p0, Ljmh;->etO:[I

    if-eqz v0, :cond_14

    iget-object v0, p0, Ljmh;->etO:[I

    array-length v0, v0

    if-lez v0, :cond_14

    move v0, v1

    .line 7212
    :goto_1
    iget-object v2, p0, Ljmh;->etO:[I

    array-length v2, v2

    if-ge v0, v2, :cond_14

    .line 7213
    const/16 v2, 0x16

    iget-object v3, p0, Ljmh;->etO:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 7212
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7216
    :cond_14
    iget v0, p0, Ljmh;->aez:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    if-eqz v0, :cond_15

    .line 7217
    const/16 v0, 0x17

    iget-boolean v2, p0, Ljmh;->etP:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 7219
    :cond_15
    iget-object v0, p0, Ljmh;->etQ:[Ljmf;

    if-eqz v0, :cond_17

    iget-object v0, p0, Ljmh;->etQ:[Ljmf;

    array-length v0, v0

    if-lez v0, :cond_17

    move v0, v1

    .line 7220
    :goto_2
    iget-object v2, p0, Ljmh;->etQ:[Ljmf;

    array-length v2, v2

    if-ge v0, v2, :cond_17

    .line 7221
    iget-object v2, p0, Ljmh;->etQ:[Ljmf;

    aget-object v2, v2, v0

    .line 7222
    if-eqz v2, :cond_16

    .line 7223
    const/16 v3, 0x18

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 7220
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7227
    :cond_17
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_18

    .line 7228
    const/16 v0, 0x19

    iget v2, p0, Ljmh;->etH:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 7230
    :cond_18
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_19

    .line 7231
    const/16 v0, 0x1a

    iget-object v2, p0, Ljmh;->etF:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 7233
    :cond_19
    iget-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1b

    .line 7234
    :goto_3
    iget-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_1b

    .line 7235
    iget-object v0, p0, Ljmh;->etR:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 7236
    if-eqz v0, :cond_1a

    .line 7237
    const/16 v2, 0x1b

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 7234
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 7241
    :cond_1b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7242
    return-void
.end method

.method public final bpF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6789
    iget-object v0, p0, Ljmh;->etE:Ljava/lang/String;

    return-object v0
.end method

.method public final bpG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6811
    iget-object v0, p0, Ljmh;->etF:Ljava/lang/String;

    return-object v0
.end method

.method public final bpH()Z
    .locals 1

    .prologue
    .line 6822
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpI()[B
    .locals 1

    .prologue
    .line 6855
    iget-object v0, p0, Ljmh;->esQ:[B

    return-object v0
.end method

.method public final bpJ()Z
    .locals 1

    .prologue
    .line 6866
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6877
    iget-object v0, p0, Ljmh;->esR:Ljava/lang/String;

    return-object v0
.end method

.method public final bpL()Z
    .locals 1

    .prologue
    .line 6888
    iget v0, p0, Ljmh;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6899
    iget-object v0, p0, Ljmh;->etG:Ljava/lang/String;

    return-object v0
.end method

.method public final bpN()Z
    .locals 1

    .prologue
    .line 6910
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpO()I
    .locals 1

    .prologue
    .line 6921
    iget v0, p0, Ljmh;->etH:I

    return v0
.end method

.method public final bpP()J
    .locals 2

    .prologue
    .line 6940
    iget-wide v0, p0, Ljmh;->etI:J

    return-wide v0
.end method

.method public final bpQ()D
    .locals 2

    .prologue
    .line 6962
    iget-wide v0, p0, Ljmh;->ete:D

    return-wide v0
.end method

.method public final bpR()Z
    .locals 1

    .prologue
    .line 6970
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7000
    iget-object v0, p0, Ljmh;->etK:Ljava/lang/String;

    return-object v0
.end method

.method public final bpT()Z
    .locals 1

    .prologue
    .line 7011
    iget v0, p0, Ljmh;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bpU()Z
    .locals 1

    .prologue
    .line 7044
    iget-boolean v0, p0, Ljmh;->etM:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6736
    iget-object v0, p0, Ljmh;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6833
    iget-object v0, p0, Ljmh;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method public final iC(Z)Ljmh;
    .locals 1

    .prologue
    .line 7047
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljmh;->etM:Z

    .line 7048
    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljmh;->aez:I

    .line 7049
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 7246
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7247
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 7248
    const/4 v2, 0x1

    iget-object v3, p0, Ljmh;->agq:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7251
    :cond_0
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 7252
    const/4 v2, 0x3

    iget v3, p0, Ljmh;->etz:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7255
    :cond_1
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_2

    .line 7256
    const/4 v2, 0x4

    iget-object v3, p0, Ljmh;->ajk:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7259
    :cond_2
    iget v2, p0, Ljmh;->aez:I

    const v3, 0x8000

    and-int/2addr v2, v3

    if-eqz v2, :cond_3

    .line 7260
    const/4 v2, 0x5

    iget-object v3, p0, Ljmh;->etN:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7263
    :cond_3
    iget-object v2, p0, Ljmh;->etA:Ljml;

    if-eqz v2, :cond_4

    .line 7264
    const/4 v2, 0x6

    iget-object v3, p0, Ljmh;->etA:Ljml;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7267
    :cond_4
    iget-object v2, p0, Ljmh;->etB:Ljmk;

    if-eqz v2, :cond_5

    .line 7268
    const/4 v2, 0x7

    iget-object v3, p0, Ljmh;->etB:Ljmk;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7271
    :cond_5
    iget-object v2, p0, Ljmh;->etC:Ljmj;

    if-eqz v2, :cond_6

    .line 7272
    const/16 v2, 0x8

    iget-object v3, p0, Ljmh;->etC:Ljmj;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7275
    :cond_6
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_7

    .line 7276
    const/16 v2, 0x9

    iget-object v3, p0, Ljmh;->etE:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7279
    :cond_7
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_8

    .line 7280
    const/16 v2, 0xa

    iget-object v3, p0, Ljmh;->esQ:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 7283
    :cond_8
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_9

    .line 7284
    const/16 v2, 0xb

    iget-object v3, p0, Ljmh;->esR:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7287
    :cond_9
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_a

    .line 7288
    const/16 v2, 0xc

    iget-object v3, p0, Ljmh;->etG:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7291
    :cond_a
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_b

    .line 7292
    const/16 v2, 0xe

    iget-wide v4, p0, Ljmh;->etI:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 7295
    :cond_b
    iget-object v2, p0, Ljmh;->etJ:[Ljmm;

    if-eqz v2, :cond_e

    iget-object v2, p0, Ljmh;->etJ:[Ljmm;

    array-length v2, v2

    if-lez v2, :cond_e

    move v2, v0

    move v0, v1

    .line 7296
    :goto_0
    iget-object v3, p0, Ljmh;->etJ:[Ljmm;

    array-length v3, v3

    if-ge v0, v3, :cond_d

    .line 7297
    iget-object v3, p0, Ljmh;->etJ:[Ljmm;

    aget-object v3, v3, v0

    .line 7298
    if-eqz v3, :cond_c

    .line 7299
    const/16 v4, 0xf

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7296
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_d
    move v0, v2

    .line 7304
    :cond_e
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_f

    .line 7305
    const/16 v2, 0x10

    iget-wide v4, p0, Ljmh;->ete:D

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 7308
    :cond_f
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_10

    .line 7309
    const/16 v2, 0x11

    iget v3, p0, Ljmh;->etf:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7312
    :cond_10
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_11

    .line 7313
    const/16 v2, 0x12

    iget-object v3, p0, Ljmh;->etK:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7316
    :cond_11
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_12

    .line 7317
    const/16 v2, 0x13

    iget-object v3, p0, Ljmh;->etL:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7320
    :cond_12
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_13

    .line 7321
    const/16 v2, 0x14

    iget-boolean v3, p0, Ljmh;->etM:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 7324
    :cond_13
    iget-object v2, p0, Ljmh;->etD:Ljmi;

    if-eqz v2, :cond_14

    .line 7325
    const/16 v2, 0x15

    iget-object v3, p0, Ljmh;->etD:Ljmi;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7328
    :cond_14
    iget-object v2, p0, Ljmh;->etO:[I

    if-eqz v2, :cond_16

    iget-object v2, p0, Ljmh;->etO:[I

    array-length v2, v2

    if-lez v2, :cond_16

    move v2, v1

    move v3, v1

    .line 7330
    :goto_1
    iget-object v4, p0, Ljmh;->etO:[I

    array-length v4, v4

    if-ge v2, v4, :cond_15

    .line 7331
    iget-object v4, p0, Ljmh;->etO:[I

    aget v4, v4, v2

    .line 7332
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 7330
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7335
    :cond_15
    add-int/2addr v0, v3

    .line 7336
    iget-object v2, p0, Ljmh;->etO:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 7338
    :cond_16
    iget v2, p0, Ljmh;->aez:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_17

    .line 7339
    const/16 v2, 0x17

    iget-boolean v3, p0, Ljmh;->etP:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 7342
    :cond_17
    iget-object v2, p0, Ljmh;->etQ:[Ljmf;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Ljmh;->etQ:[Ljmf;

    array-length v2, v2

    if-lez v2, :cond_1a

    move v2, v0

    move v0, v1

    .line 7343
    :goto_2
    iget-object v3, p0, Ljmh;->etQ:[Ljmf;

    array-length v3, v3

    if-ge v0, v3, :cond_19

    .line 7344
    iget-object v3, p0, Ljmh;->etQ:[Ljmf;

    aget-object v3, v3, v0

    .line 7345
    if-eqz v3, :cond_18

    .line 7346
    const/16 v4, 0x18

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7343
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_19
    move v0, v2

    .line 7351
    :cond_1a
    iget v2, p0, Ljmh;->aez:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_1b

    .line 7352
    const/16 v2, 0x19

    iget v3, p0, Ljmh;->etH:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7355
    :cond_1b
    iget v2, p0, Ljmh;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_1c

    .line 7356
    const/16 v2, 0x1a

    iget-object v3, p0, Ljmh;->etF:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7359
    :cond_1c
    iget-object v2, p0, Ljmh;->etR:[Ljava/lang/String;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Ljmh;->etR:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1f

    move v2, v1

    move v3, v1

    .line 7362
    :goto_3
    iget-object v4, p0, Ljmh;->etR:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_1e

    .line 7363
    iget-object v4, p0, Ljmh;->etR:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 7364
    if-eqz v4, :cond_1d

    .line 7365
    add-int/lit8 v3, v3, 0x1

    .line 7366
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7362
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 7370
    :cond_1e
    add-int/2addr v0, v2

    .line 7371
    mul-int/lit8 v1, v3, 0x2

    add-int/2addr v0, v1

    .line 7373
    :cond_1f
    return v0
.end method

.method public final qM(I)Ljmh;
    .locals 1

    .prologue
    .line 6761
    const/4 v0, 0x3

    iput v0, p0, Ljmh;->etz:I

    .line 6762
    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmh;->aez:I

    .line 6763
    return-object p0
.end method

.method public final qN(I)Ljmh;
    .locals 1

    .prologue
    .line 6924
    iput p1, p0, Ljmh;->etH:I

    .line 6925
    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljmh;->aez:I

    .line 6926
    return-object p0
.end method

.method public final wQ(Ljava/lang/String;)Ljmh;
    .locals 1

    .prologue
    .line 6739
    if-nez p1, :cond_0

    .line 6740
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6742
    :cond_0
    iput-object p1, p0, Ljmh;->agq:Ljava/lang/String;

    .line 6743
    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmh;->aez:I

    .line 6744
    return-object p0
.end method

.method public final wR(Ljava/lang/String;)Ljmh;
    .locals 1

    .prologue
    .line 6792
    if-nez p1, :cond_0

    .line 6793
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6795
    :cond_0
    iput-object p1, p0, Ljmh;->etE:Ljava/lang/String;

    .line 6796
    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmh;->aez:I

    .line 6797
    return-object p0
.end method

.method public final wS(Ljava/lang/String;)Ljmh;
    .locals 1

    .prologue
    .line 6836
    if-nez p1, :cond_0

    .line 6837
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6839
    :cond_0
    iput-object p1, p0, Ljmh;->ajk:Ljava/lang/String;

    .line 6840
    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljmh;->aez:I

    .line 6841
    return-object p0
.end method

.method public final wT(Ljava/lang/String;)Ljmh;
    .locals 1

    .prologue
    .line 6880
    if-nez p1, :cond_0

    .line 6881
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6883
    :cond_0
    iput-object p1, p0, Ljmh;->esR:Ljava/lang/String;

    .line 6884
    iget v0, p0, Ljmh;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljmh;->aez:I

    .line 6885
    return-object p0
.end method

.method public final wU(Ljava/lang/String;)Ljmh;
    .locals 1

    .prologue
    .line 6902
    if-nez p1, :cond_0

    .line 6903
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6905
    :cond_0
    iput-object p1, p0, Ljmh;->etG:Ljava/lang/String;

    .line 6906
    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljmh;->aez:I

    .line 6907
    return-object p0
.end method

.method public final wV(Ljava/lang/String;)Ljmh;
    .locals 1

    .prologue
    .line 7003
    if-nez p1, :cond_0

    .line 7004
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7006
    :cond_0
    iput-object p1, p0, Ljmh;->etK:Ljava/lang/String;

    .line 7007
    iget v0, p0, Ljmh;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljmh;->aez:I

    .line 7008
    return-object p0
.end method
