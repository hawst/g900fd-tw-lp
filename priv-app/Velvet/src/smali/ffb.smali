.class public final Lffb;
.super Lfeu;
.source "PG"


# instance fields
.field private final cmF:Ljae;

.field private final cpY:Lgbi;


# direct methods
.method public constructor <init>(Lizj;Lgbi;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 29
    iget-object v0, p1, Lizj;->dSi:Ljae;

    iput-object v0, p0, Lffb;->cmF:Ljae;

    .line 30
    iput-object p2, p0, Lffb;->cpY:Lgbi;

    .line 31
    return-void
.end method

.method private aze()I
    .locals 6

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 72
    iget-object v1, p0, Lffb;->cpY:Lgbi;

    iget v1, v1, Lgbi;->crh:I

    packed-switch v1, :pswitch_data_0

    .line 80
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljaj;->bdY()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljaj;->bea()Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    :cond_0
    const/4 v0, 0x0

    .line 91
    :goto_1
    return v0

    .line 74
    :pswitch_0
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    iget-object v0, v0, Lgbi;->cEK:Ljag;

    iget-object v0, v0, Ljag;->dWg:Ljaj;

    goto :goto_0

    .line 79
    :pswitch_1
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    iget-object v0, v0, Lgbi;->cEK:Ljag;

    iget-object v0, v0, Ljag;->dWk:Ljaj;

    goto :goto_0

    .line 91
    :cond_1
    const-wide/16 v2, 0x0

    invoke-virtual {v0}, Ljaj;->bdZ()J

    move-result-wide v4

    invoke-virtual {v0}, Ljaj;->bdX()J

    move-result-wide v0

    sub-long v0, v4, v0

    const-wide/16 v4, 0x3c

    div-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_1

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 36
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    iget-object v0, v0, Lgbi;->cEK:Ljag;

    .line 38
    invoke-virtual {v0}, Ljag;->getStatusCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 63
    :cond_0
    iget-object v0, p0, Lffb;->cmF:Ljae;

    iget-object v0, v0, Ljae;->dWa:[Ljag;

    aget-object v0, v0, v5

    .line 64
    const v1, 0x7f0a0205

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 40
    :pswitch_0
    const v1, 0x7f0a0206

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 45
    :pswitch_1
    invoke-direct {p0}, Lffb;->aze()I

    move-result v1

    .line 46
    if-lez v1, :cond_1

    .line 47
    invoke-static {p1, v1, v6}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v1

    .line 49
    const v2, 0x7f0a0208

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_1
    const v1, 0x7f0a020a

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 130
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    iget-object v1, v0, Lgbi;->cEK:Ljag;

    .line 132
    invoke-virtual {v1}, Ljag;->getStatusCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 166
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    new-instance v0, Lffz;

    const v2, 0x7f0a0207

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lffz;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 141
    :pswitch_1
    invoke-direct {p0}, Lffb;->aze()I

    move-result v0

    .line 142
    if-lez v0, :cond_2

    .line 144
    invoke-static {p1, v0, v5}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0209

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v1}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v1}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 157
    :goto_1
    new-instance v2, Lffz;

    invoke-direct {v2, v0}, Lffz;-><init>(Ljava/lang/String;)V

    .line 158
    iget-object v0, v1, Ljag;->dWf:Ljaf;

    if-eqz v0, :cond_1

    iget-object v0, v1, Ljag;->dWf:Ljaf;

    iget-object v0, v0, Ljaf;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, v1, Ljag;->dWf:Ljaf;

    iget-object v0, v0, Ljaf;->aeB:Ljbp;

    invoke-virtual {v2, v0, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    .line 162
    :cond_1
    invoke-virtual {v2}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 152
    :cond_2
    const v0, 0x7f0a020b

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 132
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lffb;->cpY:Lgbi;

    invoke-virtual {v0, p1}, Lgbi;->bo(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    const-string v0, "event"

    return-object v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 105
    const v0, 0x7f0202d4

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lffb;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfgb;->cqz:Lfgb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfgb;->cqH:Lfgb;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lffb;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lffb;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
