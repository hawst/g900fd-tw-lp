.class public final Ldoi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bgq:Lchk;

.field private final mActionState:Ldbd;

.field private final mContext:Landroid/content/Context;

.field private final mCorpora:Lcfu;

.field private final mIntentStarter:Leqp;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mSearchBoxLogging:Lcpd;

.field private final mSearchUrlHelper:Lcpn;

.field private final mService:Ldao;

.field private final mServiceState:Ldcu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Lcpn;Lcpd;Ldda;Lcfu;Ldao;Leqp;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Ldoi;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Ldoi;->bgq:Lchk;

    .line 55
    iput-object p3, p0, Ldoi;->mSearchUrlHelper:Lcpn;

    .line 56
    iput-object p4, p0, Ldoi;->mSearchBoxLogging:Lcpd;

    .line 57
    iput-object p6, p0, Ldoi;->mCorpora:Lcfu;

    .line 58
    invoke-virtual {p5}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 59
    invoke-virtual {p5}, Ldda;->aao()Ldcu;

    move-result-object v0

    iput-object v0, p0, Ldoi;->mServiceState:Ldcu;

    .line 60
    invoke-virtual {p5}, Ldda;->aaj()Ldbd;

    move-result-object v0

    iput-object v0, p0, Ldoi;->mActionState:Ldbd;

    .line 61
    iput-object p7, p0, Ldoi;->mService:Ldao;

    .line 62
    iput-object p8, p0, Ldoi;->mIntentStarter:Leqp;

    .line 63
    return-void
.end method


# virtual methods
.method public final aee()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 66
    iget-object v0, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldoi;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EB()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldoi;->mCorpora:Lcfu;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apU()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcfu;->gi(Ljava/lang/String;)Lcfy;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v3, v0, Lcic;

    if-eqz v3, :cond_4

    check-cast v0, Lcic;

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_1

    iget-object v0, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xx()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v3, Lcfy;->aqV:Ljava/lang/String;

    const-string v6, "map"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x8c

    :goto_1
    if-eqz v0, :cond_0

    invoke-static {v0}, Lege;->ht(I)V

    :cond_0
    invoke-virtual {v3, v5}, Lcic;->gw(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v6, p0, Ldoi;->mService:Ldao;

    new-array v7, v8, [Landroid/content/Intent;

    aput-object v0, v7, v2

    invoke-virtual {v6, v7}, Ldao;->c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v6

    if-nez v6, :cond_6

    :goto_2
    iget-object v3, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-static {v0}, Lhgo;->ag(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/shared/search/Query;->F(Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iget-object v3, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    .line 67
    :cond_1
    iget-object v0, p0, Ldoi;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hf()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 68
    :cond_2
    :goto_3
    iget-object v0, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xy()Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Ldoi;->mService:Ldao;

    new-array v4, v8, [Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_d

    const-string v1, "com.google.android.googlequicksearchbox.EXTERNAL_ACTIVITY_LAUNCH_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    :goto_4
    aput-object v0, v4, v2

    invoke-virtual {v3, v4}, Ldao;->b([Landroid/content/Intent;)Z

    .line 69
    :cond_3
    return-void

    :cond_4
    move-object v3, v1

    .line 66
    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    invoke-virtual {v3, v5}, Lcic;->gv(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_2

    .line 67
    :cond_7
    iget-object v4, p0, Ldoi;->mActionState:Ldbd;

    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Ldoi;->mActionState:Ldbd;

    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_8
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Ldoi;->bgq:Lchk;

    invoke-virtual {v4}, Lchk;->Hg()Z

    move-result v4

    if-nez v4, :cond_9

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->arB()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_9
    iget-object v0, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XS()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {v4}, Lcet;->i(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Ldoi;->mIntentStarter:Leqp;

    invoke-static {v4, v0}, Lcet;->a(Landroid/net/Uri;Leqp;)Landroid/content/Intent;

    move-result-object v0

    :goto_5
    const-string v5, "com.google.android.shared.util.SimpleIntentStarter.USE_FAST_TRANSITION"

    invoke-virtual {v0, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Ldoi;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Ldoi;->mIntentStarter:Leqp;

    new-array v5, v8, [Landroid/content/Intent;

    aput-object v0, v5, v2

    invoke-interface {v3, v5}, Leqp;->c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_a
    iget-object v3, p0, Ldoi;->mContext:Landroid/content/Context;

    iget-object v5, p0, Ldoi;->mServiceState:Ldcu;

    invoke-virtual {v5}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v0, v2, v8, v5}, Lhgn;->a(Landroid/content/Context;Landroid/content/Intent;ZZLjava/lang/String;)V

    iget-object v3, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-static {v0}, Lhgo;->ag(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/shared/search/Query;->F(Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iget-object v3, p0, Ldoi;->mSearchBoxLogging:Lcpd;

    iget-object v5, p0, Ldoi;->mSearchUrlHelper:Lcpn;

    iget-object v6, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v6}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v6, v4}, Lcpd;->a(Lcpn;Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Ldoi;->mSearchBoxLogging:Lcpd;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v3

    invoke-static {v3}, Lcpd;->b(Lcom/google/android/shared/search/SearchBoxStats;)V

    :cond_b
    iget-object v3, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v4, p0, Ldoi;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    goto/16 :goto_3

    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v0, v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_5

    :cond_d
    move-object v0, v1

    .line 68
    goto/16 :goto_4
.end method
