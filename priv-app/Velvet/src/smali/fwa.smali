.class public final Lfwa;
.super Lfus;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;

.field private final mPlaceDataHelper:Lgbe;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgan;Lgbe;Lgbr;)V
    .locals 6

    .prologue
    .line 41
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 43
    iput-object p4, p0, Lfwa;->mFifeImageUrlUtil:Lgan;

    .line 44
    iput-object p5, p0, Lfwa;->mPlaceDataHelper:Lgbe;

    .line 45
    return-void
.end method

.method private static B(Lizj;)Ljak;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Lizj;->dSt:Ljal;

    if-nez v1, :cond_0

    .line 138
    const-string v1, "QpNearbyPlacesEntryAdapter"

    const-string v2, "Unexpected Entry without nearby place entry."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_0
    return-object v0

    .line 141
    :cond_0
    iget-object v1, p0, Lizj;->dSt:Ljal;

    .line 143
    iget-object v2, v1, Ljal;->dWL:Ljak;

    if-nez v2, :cond_1

    .line 144
    const-string v1, "QpNearbyPlacesEntryAdapter"

    const-string v2, "Unexpected nearby place entry without frequent place."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, v1, Ljal;->dWL:Ljak;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 49
    new-instance v10, Lanh;

    invoke-direct {v10}, Lanh;-><init>()V

    .line 50
    const/16 v0, 0xa

    invoke-virtual {v10, v0}, Lanh;->cm(I)Lanh;

    .line 51
    iput-object p2, v10, Lanh;->ahu:Lizj;

    .line 53
    invoke-static {p2}, Lfwa;->B(Lizj;)Ljak;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v6, v7

    .line 54
    :goto_0
    iget-object v3, v6, Ljcu;->dME:Lixn;

    .line 56
    invoke-static {p2}, Lfwa;->B(Lizj;)Ljak;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Ljak;->pf()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 59
    invoke-virtual {v0}, Ljak;->oo()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    .line 64
    :goto_1
    iget-object v0, p2, Lizj;->dSt:Ljal;

    if-eqz v0, :cond_5

    iget-object v0, p2, Lizj;->dSt:Ljal;

    invoke-virtual {v0}, Ljal;->ber()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lizj;->dSt:Ljal;

    invoke-virtual {v0}, Ljal;->beq()I

    move-result v0

    move v9, v0

    .line 68
    :goto_2
    new-instance v0, Lfud;

    invoke-virtual {p0}, Lfwa;->aDy()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v4, p0, Lfwa;->mPlaceDataHelper:Lgbe;

    iget-object v5, p0, Lfwa;->mFifeImageUrlUtil:Lgan;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfud;-><init>(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lixn;Lgbe;Lgan;)V

    invoke-virtual {v6}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lfud;->bLI:Ljava/lang/String;

    iput v9, v0, Lfud;->cCd:I

    iput-object v8, v0, Lfud;->kO:Ljava/lang/String;

    invoke-virtual {v0}, Lfud;->aDt()Laoc;

    move-result-object v0

    iput-object v0, v10, Lanh;->agH:Laoc;

    .line 75
    invoke-virtual {v3}, Lixn;->bar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v3}, Lixn;->baq()J

    move-result-wide v0

    invoke-static {v0, v1}, Lgbe;->bt(J)Ljava/lang/String;

    move-result-object v0

    .line 77
    new-instance v1, Lfsa;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    invoke-virtual {v1, v0, v7}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 78
    iput-object v0, v10, Lanh;->ahs:Lani;

    .line 81
    :cond_0
    return-object v10

    .line 53
    :cond_1
    iget-object v1, v0, Ljak;->clz:Ljcu;

    if-nez v1, :cond_2

    const-string v0, "QpNearbyPlacesEntryAdapter"

    const-string v1, "Unexpected frequent place without place data."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    goto :goto_0

    :cond_2
    iget-object v0, v0, Ljak;->clz:Ljcu;

    iget-object v1, v0, Ljcu;->dME:Lixn;

    if-nez v1, :cond_3

    const-string v0, "QpNearbyPlacesEntryAdapter"

    const-string v1, "Unexpected place data without business data."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    goto :goto_0

    :cond_3
    move-object v6, v0

    goto :goto_0

    .line 60
    :cond_4
    iget-object v0, v3, Lixn;->dNH:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 61
    const-string v0, " \u00b7 "

    iget-object v1, v3, Lixn;->dNH:[Ljava/lang/String;

    invoke-static {v0, v1}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v8, v0

    goto :goto_1

    .line 64
    :cond_5
    const/4 v0, 0x0

    move v9, v0

    goto :goto_2

    :cond_6
    move-object v8, v7

    goto/16 :goto_1
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 4

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v1, v1, Lizq;->dUZ:Lizj;

    .line 94
    iget-object v2, v1, Lizj;->dSS:Ljca;

    if-eqz v2, :cond_0

    .line 95
    iget-object v1, v1, Lizj;->dSS:Ljca;

    .line 96
    iget-object v2, v1, Ljca;->dZR:Lixx;

    if-eqz v2, :cond_0

    .line 97
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 98
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 99
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 100
    new-instance v2, Lfsa;

    const/16 v3, 0x8b

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0200ed

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    iget-object v3, v1, Ljca;->dZR:Lixx;

    invoke-virtual {v2, v3}, Lfsa;->b(Lixx;)Lani;

    move-result-object v2

    iput-object v2, v0, Lanh;->ahs:Lani;

    .line 104
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    .line 105
    iget-object v1, v1, Ljca;->dZR:Lixx;

    invoke-virtual {v1}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 106
    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 110
    :cond_0
    return-object v0
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSS:Ljca;

    invoke-virtual {v0}, Ljca;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
