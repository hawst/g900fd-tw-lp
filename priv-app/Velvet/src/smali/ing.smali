.class final Ling;
.super Ljava/util/AbstractSet;
.source "PG"


# instance fields
.field private synthetic dFd:Limn;


# direct methods
.method constructor <init>(Limn;)V
    .locals 0

    .prologue
    .line 3885
    iput-object p1, p0, Ling;->dFd:Limn;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 3929
    iget-object v0, p0, Ling;->dFd:Limn;

    invoke-virtual {v0}, Limn;->clear()V

    .line 3930
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3894
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 3904
    :cond_0
    :goto_0
    return v0

    .line 3897
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 3898
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3899
    if-eqz v1, :cond_0

    .line 3902
    iget-object v2, p0, Ling;->dFd:Limn;

    invoke-virtual {v2, v1}, Limn;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3904
    if-eqz v1, :cond_0

    iget-object v2, p0, Ling;->dFd:Limn;

    iget-object v2, v2, Limn;->dEx:Lifc;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 3924
    iget-object v0, p0, Ling;->dFd:Limn;

    invoke-virtual {v0}, Limn;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 3889
    new-instance v0, Linf;

    iget-object v1, p0, Ling;->dFd:Limn;

    invoke-direct {v0, v1}, Linf;-><init>(Limn;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3909
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 3914
    :cond_0
    :goto_0
    return v0

    .line 3912
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 3913
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3914
    if-eqz v1, :cond_0

    iget-object v2, p0, Ling;->dFd:Limn;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Limn;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 3919
    iget-object v0, p0, Ling;->dFd:Limn;

    invoke-virtual {v0}, Limn;->size()I

    move-result v0

    return v0
.end method
