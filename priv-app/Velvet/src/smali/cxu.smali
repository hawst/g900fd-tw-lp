.class public final Lcxu;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcxv;

    invoke-direct {v0, p0, p1, p2}, Lcxv;-><init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcxv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 53
    return-void
.end method

.method private static a(Landroid/preference/PreferenceGroup;)V
    .locals 7

    .prologue
    const v6, 0x7f0a014f

    const/4 v3, 0x0

    .line 153
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 156
    invoke-static {p0}, Lcxu;->n(Landroid/preference/Preference;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 167
    invoke-virtual {p0, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/preference/Preference;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    instance-of v1, v0, Landroid/preference/PreferenceCategory;

    if-eqz v1, :cond_b

    :cond_2
    instance-of v1, v0, Landroid/preference/ListPreference;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    .line 169
    :goto_2
    if-eqz v0, :cond_3

    .line 170
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 168
    :cond_4
    instance-of v1, v0, Landroid/preference/PreferenceGroup;

    if-eqz v1, :cond_6

    move-object v1, v0

    check-cast v1, Landroid/preference/PreferenceGroup;

    invoke-static {v1}, Lcxu;->a(Landroid/preference/PreferenceGroup;)V

    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v3

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    instance-of v1, v0, Landroid/preference/TwoStatePreference;

    if-eqz v1, :cond_a

    check-cast v0, Landroid/preference/TwoStatePreference;

    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->getSummaryOn()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->getSummaryOn()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    :cond_8
    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->getSummaryOff()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->getSummaryOff()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    :cond_9
    move-object v0, v3

    goto :goto_2

    :cond_a
    instance-of v1, v0, Landroid/preference/RingtonePreference;

    if-eqz v1, :cond_b

    invoke-static {v0}, Lcxu;->m(Landroid/preference/Preference;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0, v1}, Lcxu;->a(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V

    :cond_b
    move-object v0, v3

    goto :goto_2

    .line 174
    :cond_c
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 175
    const-string v0, ", "

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    invoke-virtual {v0, v4}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 177
    :cond_d
    invoke-virtual {p0, v6}, Landroid/preference/PreferenceGroup;->setSummary(I)V

    goto/16 :goto_0
.end method

.method public static l(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 37
    invoke-static {p0}, Lcxu;->o(Landroid/preference/Preference;)V

    .line 38
    instance-of v0, p0, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    .line 39
    check-cast p0, Landroid/preference/PreferenceGroup;

    invoke-static {p0}, Lcxu;->a(Landroid/preference/PreferenceGroup;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-static {p0}, Lcxu;->n(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static m(Landroid/preference/Preference;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    .line 96
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static n(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 187
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 188
    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 190
    :try_start_0
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 191
    if-nez v1, :cond_2

    .line 195
    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a014e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    invoke-virtual {p0}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "configured_summary"

    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 201
    const v1, 0x7f0a014e

    invoke-virtual {p0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 203
    :cond_0
    const/4 v0, 0x0

    .line 215
    :cond_1
    :goto_0
    return v0

    .line 205
    :cond_2
    invoke-virtual {p0}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "configured_summary"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 207
    if-eqz v1, :cond_1

    .line 208
    invoke-virtual {p0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static o(Landroid/preference/Preference;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 226
    invoke-virtual {p0}, Landroid/preference/Preference;->isPersistent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    invoke-virtual {p0}, Landroid/preference/Preference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 231
    instance-of v0, p0, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_2

    .line 232
    check-cast p0, Landroid/preference/PreferenceGroup;

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-static {v1}, Lcxu;->o(Landroid/preference/Preference;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 234
    :cond_2
    instance-of v0, p0, Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    if-eqz v0, :cond_4

    move-object v0, p0

    .line 235
    check-cast v0, Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    .line 237
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v2, :cond_3

    move v1, v2

    .line 240
    :cond_3
    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/NotificationGroupPreference;->setChecked(Z)V

    goto :goto_0

    .line 241
    :cond_4
    instance-of v0, p0, Landroid/preference/TwoStatePreference;

    if-eqz v0, :cond_5

    move-object v0, p0

    .line 242
    check-cast v0, Landroid/preference/TwoStatePreference;

    .line 243
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 244
    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    .line 245
    :cond_5
    instance-of v0, p0, Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 246
    check-cast v0, Landroid/preference/ListPreference;

    .line 247
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    .line 248
    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v4

    .line 249
    invoke-virtual {p0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 250
    if-eqz v3, :cond_0

    .line 251
    :goto_2
    array-length v5, v2

    if-ge v1, v5, :cond_0

    .line 252
    aget-object v5, v4, v1

    invoke-virtual {v5, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 253
    aget-object v5, v2, v1

    .line 254
    invoke-virtual {v0, v5}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 251
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
