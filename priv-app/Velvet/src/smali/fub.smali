.class public final Lfub;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 8

    .prologue
    const v7, 0x7f110379

    const v4, 0x7f1102ef

    const/4 v6, 0x0

    .line 37
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v1, v0, Lanh;->ahj:Laoa;

    .line 38
    iget-object v2, p0, Lfro;->mView:Landroid/view/View;

    .line 41
    const v0, 0x7f1101a9

    invoke-virtual {v1}, Laoa;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 44
    iget-object v0, v1, Laoa;->ahN:Laok;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lfub;->mContext:Landroid/content/Context;

    iget-object v3, v1, Laoa;->ahN:Laok;

    invoke-static {v0, v3}, Lfxt;->a(Landroid/content/Context;Laok;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 47
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 48
    invoke-static {v2, v4, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 49
    iget-object v0, v1, Laoa;->ahN:Laok;

    iget-object v0, v0, Laok;->ahs:Lani;

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 51
    iget-object v3, v1, Laoa;->ahN:Laok;

    iget-object v3, v3, Laok;->ahs:Lani;

    invoke-virtual {p0, v0, v3}, Lfub;->a(Landroid/view/View;Lani;)V

    .line 56
    :cond_0
    const v0, 0x7f110304

    invoke-virtual {v1}, Laoa;->getInfo()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 58
    invoke-virtual {v1}, Laoa;->qi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lfub;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b00b9

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 60
    invoke-virtual {v1}, Laoa;->qh()Ljava/lang/String;

    move-result-object v4

    .line 61
    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 62
    :goto_0
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 63
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v3, 0x11

    invoke-virtual {v5, v4, v6, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 64
    const v0, 0x7f110277

    invoke-static {v2, v0, v5}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 68
    :cond_1
    iget-object v0, v1, Laoa;->aiX:Ljcn;

    if-eqz v0, :cond_2

    iget-object v0, v1, Laoa;->aiX:Ljcn;

    invoke-virtual {v0}, Ljcn;->qG()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    const v0, 0x7f110303

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 70
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget-object v0, v1, Laoa;->aiX:Ljcn;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v7, v0}, Lfub;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 72
    iget-object v0, v1, Laoa;->aiY:Lani;

    if-eqz v0, :cond_2

    .line 73
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 74
    iget-object v1, v1, Laoa;->aiY:Lani;

    invoke-virtual {p0, v0, v1}, Lfub;->a(Landroid/view/View;Lani;)V

    .line 77
    :cond_2
    return-void

    .line 61
    :cond_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 32
    const v0, 0x7f040114

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
