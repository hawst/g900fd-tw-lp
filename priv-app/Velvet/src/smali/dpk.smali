.class public final Ldpk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field public aXc:Ldnb;

.field final bDo:Ljava/lang/Object;

.field private final bEL:Ljava/lang/Runnable;

.field bEM:Lceu;

.field bEN:Lcki;

.field bEO:Z

.field private bEP:Lcid;

.field private bEQ:Z

.field private bER:Z

.field public bES:Ldyo;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mBasePageContentFetcher:Lckm;

.field final mCoreServices:Lcfo;

.field final mEventBus:Ldda;

.field private final mFactory:Lgpu;

.field private final mGsaConfigFlags:Lchk;

.field private final mIntentStarter:Leoj;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field public final mSearchController:Lcjt;

.field final mUiThread:Leqo;

.field final mUrlHelper:Lcpn;

.field private mWebAppController:Ldmz;

.field public mWebView:Landroid/webkit/WebView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcfo;Lcjt;Ldda;Lchk;Lcpn;Leoj;Lgpu;Leqo;)V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ldpl;

    invoke-direct {v0, p0}, Ldpl;-><init>(Ldpk;)V

    iput-object v0, p0, Ldpk;->bEL:Ljava/lang/Runnable;

    .line 91
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldpk;->bDo:Ljava/lang/Object;

    .line 111
    iput-object p1, p0, Ldpk;->mCoreServices:Lcfo;

    .line 112
    iput-object p2, p0, Ldpk;->mSearchController:Lcjt;

    .line 113
    iput-object p3, p0, Ldpk;->mEventBus:Ldda;

    .line 114
    invoke-virtual {p3}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 115
    iput-object p4, p0, Ldpk;->mGsaConfigFlags:Lchk;

    .line 116
    iput-object p5, p0, Ldpk;->mUrlHelper:Lcpn;

    .line 117
    iput-object p6, p0, Ldpk;->mIntentStarter:Leoj;

    .line 118
    iput-object p7, p0, Ldpk;->mFactory:Lgpu;

    .line 119
    iput-object p8, p0, Ldpk;->mUiThread:Leqo;

    .line 120
    return-void
.end method


# virtual methods
.method public final a(Lddb;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 123
    invoke-virtual {p0, v0}, Ldpk;->dI(Z)V

    .line 126
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldpk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Ie()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XW()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xj()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xg()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ldpk;->aep()Ldnb;

    move-result-object v0

    invoke-virtual {v0}, Ldnb;->adI()V

    iget-object v0, p0, Ldpk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Id()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Ldpk;->mBasePageContentFetcher:Lckm;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldpk;->mFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJG()Lckm;

    move-result-object v0

    iput-object v0, p0, Ldpk;->mBasePageContentFetcher:Lckm;

    :cond_0
    iget-object v1, p0, Ldpk;->bDo:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ldpk;->bEN:Lcki;

    if-nez v0, :cond_a

    iget-boolean v0, p0, Ldpk;->bEO:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldpk;->bEO:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ldpk;->bEN:Lcki;

    iget-object v0, p0, Ldpk;->mBasePageContentFetcher:Lckm;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Ldpk;->aep()Ldnb;

    move-result-object v0

    invoke-virtual {v0}, Ldnb;->If()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ldpm;

    invoke-direct {v3, p0}, Ldpm;-><init>(Ldpk;)V

    iget-object v4, p0, Ldpk;->mBasePageContentFetcher:Lckm;

    invoke-virtual {v4, v0, v3}, Lckm;->a(Ljava/lang/String;Lemy;)V

    :cond_1
    move-object v0, v2

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ldpk;->aep()Ldnb;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldnb;->a(Lcki;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xi()V

    .line 131
    :cond_2
    :goto_1
    iget-object v0, p0, Ldpk;->mWebAppController:Ldmz;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 132
    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XE()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_3

    .line 134
    iget-object v1, p0, Ldpk;->mWebAppController:Ldmz;

    invoke-virtual {v1, v0}, Ldmz;->aE(Lcom/google/android/shared/search/Query;)V

    .line 137
    :cond_3
    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XF()Landroid/util/Pair;

    move-result-object v3

    .line 138
    if-eqz v3, :cond_5

    .line 144
    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amU()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 146
    iget-object v0, p0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->MS()Lcfc;

    move-result-object v0

    invoke-virtual {v0}, Lcfc;->Dv()V

    .line 148
    :cond_4
    invoke-virtual {p0}, Ldpk;->aep()Ldnb;

    move-result-object v4

    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->getWebViewQuery()Lcom/google/android/shared/search/Query;

    move-result-object v5

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldyo;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcnb;

    invoke-virtual {v4, v5, v0, v1}, Ldnb;->a(Lcom/google/android/shared/search/Query;Ldyo;Lcnb;)V

    .line 150
    iget-object v0, p0, Ldpk;->bEP:Lcid;

    iget-object v1, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->getWebViewQuery()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcid;->setWebViewQuery(Lcom/google/android/shared/search/Query;)V

    .line 151
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ldyo;

    iput-object v0, p0, Ldpk;->bES:Ldyo;

    .line 154
    :cond_5
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 155
    :cond_6
    iget-object v0, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 157
    iget-object v0, p0, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {p0}, Ldpk;->aep()Ldnb;

    move-result-object v1

    invoke-virtual {v1}, Ldnb;->adN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcky;->hl(Ljava/lang/String;)V

    .line 164
    :cond_7
    :goto_2
    iget-object v0, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    .line 165
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Ldpk;->mWebAppController:Ldmz;

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Ldcy;->ZV()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 167
    iget-object v1, p0, Ldpk;->mWebAppController:Ldmz;

    invoke-virtual {v0}, Ldcy;->ZX()Z

    move-result v0

    invoke-virtual {v1, v0}, Ldmz;->dF(Z)V

    .line 171
    :cond_8
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xq()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 173
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    invoke-virtual {v0}, Ldnb;->adM()V

    .line 175
    :cond_9
    return-void

    .line 128
    :cond_a
    :try_start_1
    iget-object v0, p0, Ldpk;->bEN:Lcki;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_b
    invoke-virtual {p0}, Ldpk;->aep()Ldnb;

    move-result-object v0

    invoke-virtual {v0}, Ldnb;->adG()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xi()V

    goto/16 :goto_1

    .line 161
    :cond_c
    iget-object v0, p0, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0, v2}, Lcky;->hl(Ljava/lang/String;)V

    goto :goto_2

    :cond_d
    move-object v0, v2

    goto/16 :goto_0
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 341
    const-string v0, "WebViewWorker"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 343
    iget-object v0, p0, Ldpk;->mWebAppController:Ldmz;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 348
    return-void
.end method

.method public aep()Ldnb;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 286
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    if-nez v0, :cond_0

    .line 287
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Ldpk;->mFactory:Lgpu;

    new-instance v2, Ldpn;

    invoke-direct {v2, p0}, Ldpn;-><init>(Ldpk;)V

    invoke-virtual {v0, v2}, Lgpu;->a(Lcex;)Lceu;

    move-result-object v0

    iput-object v0, p0, Ldpk;->bEM:Lceu;

    iget-object v0, p0, Ldpk;->mFactory:Lgpu;

    new-instance v2, Ldpo;

    invoke-direct {v2, p0}, Ldpo;-><init>(Ldpk;)V

    iget-object v3, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0, v2, v3}, Lgpu;->a(Ldnj;Ldda;)Ldnb;

    move-result-object v0

    iput-object v0, p0, Ldpk;->aXc:Ldnb;

    iget-object v0, p0, Ldpk;->mFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJC()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    iget-object v0, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    const-string v2, "RESULTS"

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setTag(Ljava/lang/Object;)V

    iput-boolean v1, p0, Ldpk;->bEQ:Z

    iget-object v0, p0, Ldpk;->mFactory:Lgpu;

    iget-object v1, p0, Ldpk;->aXc:Ldnb;

    iget-object v2, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0, v1, v2}, Lgpu;->a(Ldnb;Ldda;)Ldmz;

    move-result-object v0

    iput-object v0, p0, Ldpk;->mWebAppController:Ldmz;

    iget-object v0, p0, Ldpk;->mFactory:Lgpu;

    iget-object v1, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Ldpk;->mEventBus:Ldda;

    iget-object v4, p0, Ldpk;->aXc:Ldnb;

    iget-object v5, p0, Ldpk;->mWebAppController:Ldmz;

    iget-object v6, p0, Ldpk;->mIntentStarter:Leoj;

    move-object v3, p0

    invoke-virtual/range {v0 .. v6}, Lgpu;->a(Landroid/webkit/WebView;Ldda;Ldpk;Ldnb;Ldmz;Leoj;)Ldmv;

    move-result-object v0

    iget-object v1, p0, Ldpk;->mFactory:Lgpu;

    iget-object v2, p0, Ldpk;->mIntentStarter:Leoj;

    iget-object v3, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v1, v2, v3}, Lgpu;->a(Leoj;Ldda;)Lcid;

    move-result-object v1

    iput-object v1, p0, Ldpk;->bEP:Lcid;

    iget-object v1, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Ldpk;->bEP:Lcid;

    const-string v3, "agsa_ext"

    invoke-virtual {v1, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Ldpk;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DS()Lckg;

    move-result-object v1

    iget-object v2, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Lckg;->a(Landroid/webkit/WebView;)V

    iget-object v1, p0, Ldpk;->aXc:Ldnb;

    iget-object v2, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Ldpk;->mWebAppController:Ldmz;

    invoke-virtual {v1, v2, v0, v3}, Ldnb;->a(Landroid/webkit/WebView;Ldmv;Ldmz;)V

    .line 289
    :cond_0
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    return-object v0

    :cond_1
    move v0, v1

    .line 287
    goto :goto_0
.end method

.method public final b(Lcki;)V
    .locals 2

    .prologue
    .line 204
    iget-object v1, p0, Ldpk;->bDo:Ljava/lang/Object;

    monitor-enter v1

    .line 205
    :try_start_0
    iput-object p1, p0, Ldpk;->bEN:Lcki;

    .line 206
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ca()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Ldpk;->aXc:Ldnb;

    invoke-virtual {v0}, Ldnb;->ca()V

    .line 319
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldpk;->bER:Z

    .line 320
    return-void
.end method

.method dI(Z)V
    .locals 4

    .prologue
    .line 178
    iget-object v0, p0, Ldpk;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Yz()Z

    move-result v0

    .line 179
    iget-object v1, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ldpk;->bEQ:Z

    if-eq v0, v1, :cond_0

    iget-boolean v1, p0, Ldpk;->bER:Z

    if-nez v1, :cond_0

    .line 180
    if-eqz v0, :cond_2

    .line 181
    if-eqz p1, :cond_1

    .line 182
    iget-object v0, p0, Ldpk;->mUiThread:Leqo;

    iget-object v1, p0, Ldpk;->bEL:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldpk;->bEQ:Z

    goto :goto_0

    .line 188
    :cond_2
    iget-object v0, p0, Ldpk;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldpk;->bEQ:Z

    goto :goto_0
.end method

.method public final kd(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->MS()Lcfc;

    move-result-object v0

    iget-object v1, p0, Ldpk;->aXc:Ldnb;

    invoke-virtual {v1}, Ldnb;->adD()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcfc;->y(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void
.end method
