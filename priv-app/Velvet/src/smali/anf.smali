.class public final Lanf;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private agr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7127
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7128
    const/4 v0, 0x0

    iput v0, p0, Lanf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lanf;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lanf;->agr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lanf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lanf;->eCz:I

    .line 7129
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 7064
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lanf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanf;->agq:Ljava/lang/String;

    iget v0, p0, Lanf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanf;->agr:Ljava/lang/String;

    iget v0, p0, Lanf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 7143
    iget v0, p0, Lanf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7144
    const/4 v0, 0x1

    iget-object v1, p0, Lanf;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7146
    :cond_0
    iget v0, p0, Lanf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7147
    const/4 v0, 0x2

    iget-object v1, p0, Lanf;->agr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7149
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7150
    return-void
.end method

.method public final al(Ljava/lang/String;)Lanf;
    .locals 1

    .prologue
    .line 7089
    if-nez p1, :cond_0

    .line 7090
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7092
    :cond_0
    iput-object p1, p0, Lanf;->agq:Ljava/lang/String;

    .line 7093
    iget v0, p0, Lanf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lanf;->aez:I

    .line 7094
    return-object p0
.end method

.method public final am(Ljava/lang/String;)Lanf;
    .locals 1

    .prologue
    .line 7111
    if-nez p1, :cond_0

    .line 7112
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7114
    :cond_0
    iput-object p1, p0, Lanf;->agr:Ljava/lang/String;

    .line 7115
    iget v0, p0, Lanf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lanf;->aez:I

    .line 7116
    return-object p0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7086
    iget-object v0, p0, Lanf;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 7154
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7155
    iget v1, p0, Lanf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7156
    const/4 v1, 0x1

    iget-object v2, p0, Lanf;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7159
    :cond_0
    iget v1, p0, Lanf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7160
    const/4 v1, 0x2

    iget-object v2, p0, Lanf;->agr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7163
    :cond_1
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 7097
    iget v0, p0, Lanf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7108
    iget-object v0, p0, Lanf;->agr:Ljava/lang/String;

    return-object v0
.end method

.method public final oM()Z
    .locals 1

    .prologue
    .line 7119
    iget v0, p0, Lanf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
