.class public final Lcxg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static Tx()Lizm;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 30
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    .line 31
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljbj;->om(I)Ljbj;

    .line 32
    new-array v1, v2, [I

    aput v2, v1, v3

    iput-object v1, v0, Ljbj;->dYx:[I

    .line 34
    new-instance v1, Lizm;

    invoke-direct {v1}, Lizm;-><init>()V

    .line 35
    new-array v2, v2, [Ljbj;

    aput-object v0, v2, v3

    iput-object v2, v1, Lizm;->dUH:[Ljbj;

    .line 36
    return-object v1
.end method

.method public static a(Lizn;)Landroid/util/Pair;
    .locals 11
    .param p0    # Lizn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v10, 0x12

    const/16 v9, 0x11

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 46
    if-eqz p0, :cond_0

    iget-object v1, p0, Lizn;->dUI:[Lizo;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-object v0

    .line 50
    :cond_1
    iget-object v1, p0, Lizn;->dUI:[Lizo;

    aget-object v1, v1, v4

    .line 51
    iget-object v2, v1, Lizo;->dUQ:Lizq;

    if-eqz v2, :cond_0

    .line 57
    iget-object v1, v1, Lizo;->dUQ:Lizq;

    .line 58
    iget-object v5, v1, Lizq;->dUX:[Lizj;

    array-length v6, v5

    move v3, v4

    move-object v2, v0

    :goto_1
    if-ge v3, v6, :cond_3

    aget-object v1, v5, v3

    .line 59
    iget-object v7, v1, Lizj;->dSc:Ljal;

    if-eqz v7, :cond_4

    .line 60
    const/4 v7, 0x1

    new-array v7, v7, [I

    aput v10, v7, v4

    invoke-static {v1, v9, v7}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v7

    .line 61
    if-eqz v7, :cond_4

    .line 62
    invoke-virtual {v7}, Liwk;->getType()I

    move-result v8

    if-ne v8, v9, :cond_2

    .line 58
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_1

    .line 65
    :cond_2
    invoke-virtual {v7}, Liwk;->getType()I

    move-result v7

    if-ne v7, v10, :cond_4

    move-object v0, v1

    move-object v1, v2

    .line 66
    goto :goto_2

    .line 72
    :cond_3
    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_2
.end method
