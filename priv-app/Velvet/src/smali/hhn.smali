.class public final Lhhn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdn;


# instance fields
.field private final aSZ:Ljava/util/concurrent/ExecutorService;

.field private final aSp:Lcdz;

.field private final cGs:Ljava/util/concurrent/ExecutorService;

.field private final cGt:Ljava/util/concurrent/ExecutorService;

.field private final dhm:Lgni;

.field private final mAsyncServices:Lema;

.field private final mClock:Lemp;

.field private final mGsaConfigFlags:Lchk;

.field private final mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mNetworkInformation:Lgno;

.field private final mRecognitionEngineParams:Lgnb;

.field private final mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Lgno;Lgni;Lgnb;Lgdo;Lchk;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lema;)V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lhhn;->mNetworkInformation:Lgno;

    .line 97
    iput-object p2, p0, Lhhn;->dhm:Lgni;

    .line 98
    iput-object p3, p0, Lhhn;->mRecognitionEngineParams:Lgnb;

    .line 99
    iput-object p5, p0, Lhhn;->mGsaConfigFlags:Lchk;

    .line 100
    iput-object p4, p0, Lhhn;->mSpeechSettings:Lgdo;

    .line 101
    iput-object p6, p0, Lhhn;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 102
    iput-object p7, p0, Lhhn;->mClock:Lemp;

    .line 103
    iput-object p8, p0, Lhhn;->mAsyncServices:Lema;

    .line 104
    new-instance v0, Lgct;

    invoke-direct {v0, p5, p1}, Lgct;-><init>(Lchk;Lgno;)V

    iput-object v0, p0, Lhhn;->aSp:Lcdz;

    .line 108
    iget-object v0, p0, Lhhn;->mAsyncServices:Lema;

    const-string v1, "LocalEngine"

    invoke-virtual {v0, v1}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lhhn;->mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 110
    iget-object v0, p0, Lhhn;->mAsyncServices:Lema;

    const-string v1, "NetworkEngine"

    invoke-virtual {v0, v1}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lhhn;->aSZ:Ljava/util/concurrent/ExecutorService;

    .line 112
    iget-object v0, p0, Lhhn;->mAsyncServices:Lema;

    const-string v1, "MusicDetector"

    invoke-virtual {v0, v1}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lhhn;->cGs:Ljava/util/concurrent/ExecutorService;

    .line 114
    iget-object v0, p0, Lhhn;->mAsyncServices:Lema;

    const-string v1, "HotwordDetector"

    invoke-virtual {v0, v1}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lhhn;->cGt:Ljava/util/concurrent/ExecutorService;

    .line 116
    return-void
.end method

.method private aOQ()Z
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lhhn;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    .line 238
    iget-object v1, v0, Ljze;->eNu:Ljzz;

    if-nez v1, :cond_0

    .line 239
    const/4 v0, 0x1

    .line 241
    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Ljze;->eNu:Ljzz;

    invoke-virtual {v0}, Ljzz;->bxz()Z

    move-result v0

    goto :goto_0
.end method

.method private d(Lgnj;)Z
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p1}, Lgnj;->getMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhhn;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Ht()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgnj;)Lgcl;
    .locals 3

    .prologue
    .line 225
    new-instance v0, Lgcm;

    iget-object v1, p0, Lhhn;->mSpeechSettings:Lgdo;

    iget-object v2, p0, Lhhn;->mNetworkInformation:Lgno;

    invoke-virtual {v2}, Lgno;->isConnected()Z

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lgcm;-><init>(Lgnj;Lgdo;Z)V

    return-object v0
.end method

.method public final a(Lgnj;Lgil;Lgcl;Lgcd;Lglx;Ljava/util/concurrent/ExecutorService;)Lgim;
    .locals 16

    .prologue
    .line 126
    new-instance v14, Lgck;

    invoke-virtual/range {p1 .. p1}, Lgnj;->aHF()Z

    move-result v2

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-direct {v14, v0, v1, v2}, Lgck;-><init>(Lglx;Lgcd;Z)V

    .line 128
    new-instance v15, Lgkw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhhn;->mSpeechSettings:Lgdo;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lgnj;->c(Lgdo;)Ljzk;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v15, v14, v2, v3}, Lgkw;-><init>(Lglx;Ljzk;Z)V

    .line 133
    invoke-interface/range {p3 .. p3}, Lgcl;->aEO()I

    move-result v4

    .line 134
    invoke-interface/range {p3 .. p3}, Lgcl;->aEP()I

    move-result v5

    .line 135
    invoke-virtual/range {p1 .. p1}, Lgnj;->Hq()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    if-ne v4, v2, :cond_3

    const/4 v2, 0x1

    .line 137
    :goto_0
    invoke-direct/range {p0 .. p1}, Lhhn;->d(Lgnj;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 138
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 139
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 140
    if-eqz v4, :cond_0

    .line 141
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_0
    if-eqz v5, :cond_1

    .line 144
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_1
    if-eqz v2, :cond_2

    .line 147
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_2
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    new-instance v3, Lgcu;

    invoke-direct/range {p0 .. p0}, Lhhn;->aOQ()Z

    move-result v2

    move-object/from16 v0, p2

    invoke-direct {v3, v6, v7, v0, v2}, Lgcu;-><init>(Ljava/util/List;Ljava/util/List;Lgil;Z)V

    move-object v7, v3

    .line 179
    :goto_1
    new-instance v8, Lgcn;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-direct {v8, v0, v1, v14, v7}, Lgcn;-><init>(Lgcd;Lgil;Lglx;Lgda;)V

    .line 185
    invoke-interface {v7, v8}, Lgda;->a(Lggg;)V

    .line 187
    new-instance v9, Lhqv;

    invoke-direct {v9, v15, v7}, Lhqv;-><init>(Lgky;Lgda;)V

    .line 192
    new-instance v10, Lgcw;

    invoke-direct {v10, v15, v7}, Lgcw;-><init>(Lgky;Lgda;)V

    .line 197
    new-instance v2, Lhqx;

    new-instance v5, Lgcp;

    invoke-direct {v5}, Lgcp;-><init>()V

    move-object/from16 v3, p4

    move-object v4, v15

    move-object v6, v14

    invoke-direct/range {v2 .. v7}, Lhqx;-><init>(Lgcd;Lgky;Lgdi;Lglx;Lgda;)V

    .line 205
    new-instance v3, Lgcs;

    invoke-direct {v3, v14}, Lgcs;-><init>(Lglx;)V

    .line 208
    new-instance v4, Lgcr;

    invoke-direct {v4, v14}, Lgcr;-><init>(Lglx;)V

    .line 211
    invoke-virtual {v8, v2}, Lgcn;->b(Lgco;)V

    .line 212
    invoke-interface {v7, v9}, Lgda;->a(Lgco;)V

    .line 214
    new-instance v5, Lgdh;

    invoke-direct {v5, v8, v7}, Lgdh;-><init>(Lggg;Lgda;)V

    .line 215
    const-class v6, Lehv;

    invoke-virtual {v5, v6, v2}, Lgdh;->a(Ljava/lang/Class;Lgcz;)V

    .line 216
    const-class v2, Leho;

    invoke-virtual {v5, v2, v4}, Lgdh;->a(Ljava/lang/Class;Lgcz;)V

    .line 217
    const-class v2, Lehn;

    invoke-virtual {v5, v2, v9}, Lgdh;->a(Ljava/lang/Class;Lgcz;)V

    .line 218
    const-class v2, Lehs;

    invoke-virtual {v5, v2, v3}, Lgdh;->a(Ljava/lang/Class;Lgcz;)V

    .line 219
    const-class v2, Leht;

    invoke-virtual {v5, v2, v10}, Lgdh;->a(Ljava/lang/Class;Lgcz;)V

    .line 220
    return-object v5

    .line 135
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 155
    :cond_4
    if-eqz v2, :cond_5

    .line 156
    new-instance v3, Lgeg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhhn;->mSpeechSettings:Lgdo;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lgnj;->b(Lgdo;)I

    move-result v2

    int-to-long v6, v2

    invoke-direct/range {p0 .. p0}, Lhhn;->aOQ()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lhhn;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, p0

    iget-object v12, v0, Lhhn;->mClock:Lemp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lhhn;->mSpeechSettings:Lgdo;

    move-object/from16 v9, p2

    move-object/from16 v10, p6

    invoke-direct/range {v3 .. v13}, Lgeg;-><init>(IIJZLgil;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lgdo;)V

    move-object v7, v3

    goto :goto_1

    .line 167
    :cond_5
    new-instance v3, Lgce;

    invoke-interface/range {p3 .. p3}, Lgcl;->aEO()I

    move-result v4

    invoke-interface/range {p3 .. p3}, Lgcl;->aEP()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhhn;->mSpeechSettings:Lgdo;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lgnj;->b(Lgdo;)I

    move-result v2

    int-to-long v6, v2

    invoke-direct/range {p0 .. p0}, Lhhn;->aOQ()Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lhhn;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    move-object/from16 v0, p0

    iget-object v12, v0, Lhhn;->mClock:Lemp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lhhn;->mSpeechSettings:Lgdo;

    move-object/from16 v9, p2

    move-object/from16 v10, p6

    invoke-direct/range {v3 .. v13}, Lgce;-><init>(IIJZLgil;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lgdo;)V

    move-object v7, v3

    goto/16 :goto_1
.end method

.method public final a(Lger;Lgnj;)Lgmj;
    .locals 3

    .prologue
    .line 254
    invoke-virtual {p2}, Lgnj;->getMode()I

    move-result v0

    invoke-static {v0}, Lgnj;->kr(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    new-instance v0, Lgmm;

    iget-object v1, p0, Lhhn;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iget-object v2, p0, Lhhn;->dhm:Lgni;

    invoke-direct {v0, v1, p1, v2, p2}, Lgmm;-><init>(Ljava/util/concurrent/ExecutorService;Lger;Lgni;Lgnj;)V

    .line 258
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lgmp;

    iget-object v1, p0, Lhhn;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iget-object v2, p0, Lhhn;->dhm:Lgni;

    invoke-direct {v0, v1, p1, v2, p2}, Lgmp;-><init>(Ljava/util/concurrent/ExecutorService;Lger;Lgni;Lgnj;)V

    goto :goto_0
.end method

.method public final aEZ()Lgcx;
    .locals 7

    .prologue
    .line 231
    new-instance v0, Lgcy;

    iget-object v1, p0, Lhhn;->mRecognitionEngineParams:Lgnb;

    iget-object v3, p0, Lhhn;->mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

    iget-object v4, p0, Lhhn;->aSZ:Ljava/util/concurrent/ExecutorService;

    iget-object v5, p0, Lhhn;->cGs:Ljava/util/concurrent/ExecutorService;

    iget-object v6, p0, Lhhn;->cGt:Ljava/util/concurrent/ExecutorService;

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lgcy;-><init>(Lgnb;Lgdn;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method public final b(Lgnj;)Lcdp;
    .locals 3

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lhhn;->d(Lgnj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    new-instance v0, Lgdl;

    invoke-direct {v0}, Lgdl;-><init>()V

    .line 275
    :goto_0
    return-object v0

    .line 268
    :cond_0
    new-instance v1, Lhho;

    invoke-direct {v1, p0}, Lhho;-><init>(Lhhn;)V

    .line 275
    new-instance v0, Lcdb;

    iget-object v2, p0, Lhhn;->mClock:Lemp;

    invoke-direct {v0, v1, v2}, Lcdb;-><init>(Ligi;Lemp;)V

    goto :goto_0
.end method

.method public final c(Lgnj;)Lcdz;
    .locals 1

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lhhn;->d(Lgnj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lhhn;->aSp:Lcdz;

    .line 283
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
