.class public final Ljht;
.super Ljsl;
.source "PG"


# instance fields
.field public amf:Ljha;

.field public amg:Ljdn;

.field public emj:Liwt;

.field public emk:Ljel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5445
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5446
    iput-object v0, p0, Ljht;->amf:Ljha;

    iput-object v0, p0, Ljht;->amg:Ljdn;

    iput-object v0, p0, Ljht;->emj:Liwt;

    iput-object v0, p0, Ljht;->emk:Ljel;

    iput-object v0, p0, Ljht;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljht;->eCz:I

    .line 5447
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5416
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljht;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljht;->amf:Ljha;

    if-nez v0, :cond_1

    new-instance v0, Ljha;

    invoke-direct {v0}, Ljha;-><init>()V

    iput-object v0, p0, Ljht;->amf:Ljha;

    :cond_1
    iget-object v0, p0, Ljht;->amf:Ljha;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljht;->amg:Ljdn;

    if-nez v0, :cond_2

    new-instance v0, Ljdn;

    invoke-direct {v0}, Ljdn;-><init>()V

    iput-object v0, p0, Ljht;->amg:Ljdn;

    :cond_2
    iget-object v0, p0, Ljht;->amg:Ljdn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljht;->emj:Liwt;

    if-nez v0, :cond_3

    new-instance v0, Liwt;

    invoke-direct {v0}, Liwt;-><init>()V

    iput-object v0, p0, Ljht;->emj:Liwt;

    :cond_3
    iget-object v0, p0, Ljht;->emj:Liwt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljht;->emk:Ljel;

    if-nez v0, :cond_4

    new-instance v0, Ljel;

    invoke-direct {v0}, Ljel;-><init>()V

    iput-object v0, p0, Ljht;->emk:Ljel;

    :cond_4
    iget-object v0, p0, Ljht;->emk:Ljel;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5462
    iget-object v0, p0, Ljht;->amf:Ljha;

    if-eqz v0, :cond_0

    .line 5463
    const/4 v0, 0x1

    iget-object v1, p0, Ljht;->amf:Ljha;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5465
    :cond_0
    iget-object v0, p0, Ljht;->amg:Ljdn;

    if-eqz v0, :cond_1

    .line 5466
    const/4 v0, 0x2

    iget-object v1, p0, Ljht;->amg:Ljdn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5468
    :cond_1
    iget-object v0, p0, Ljht;->emj:Liwt;

    if-eqz v0, :cond_2

    .line 5469
    const/4 v0, 0x3

    iget-object v1, p0, Ljht;->emj:Liwt;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5471
    :cond_2
    iget-object v0, p0, Ljht;->emk:Ljel;

    if-eqz v0, :cond_3

    .line 5472
    const/4 v0, 0x4

    iget-object v1, p0, Ljht;->emk:Ljel;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5474
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5475
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5479
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5480
    iget-object v1, p0, Ljht;->amf:Ljha;

    if-eqz v1, :cond_0

    .line 5481
    const/4 v1, 0x1

    iget-object v2, p0, Ljht;->amf:Ljha;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5484
    :cond_0
    iget-object v1, p0, Ljht;->amg:Ljdn;

    if-eqz v1, :cond_1

    .line 5485
    const/4 v1, 0x2

    iget-object v2, p0, Ljht;->amg:Ljdn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5488
    :cond_1
    iget-object v1, p0, Ljht;->emj:Liwt;

    if-eqz v1, :cond_2

    .line 5489
    const/4 v1, 0x3

    iget-object v2, p0, Ljht;->emj:Liwt;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5492
    :cond_2
    iget-object v1, p0, Ljht;->emk:Ljel;

    if-eqz v1, :cond_3

    .line 5493
    const/4 v1, 0x4

    iget-object v2, p0, Ljht;->emk:Ljel;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5496
    :cond_3
    return v0
.end method
