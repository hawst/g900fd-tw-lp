.class public Lblw;
.super Lblf;


# static fields
.field private static final NAMESPACE:Ljava/lang/String;

.field private static final aEW:J

.field private static final aEX:J

.field private static final aEY:J

.field private static final aEZ:J


# instance fields
.field private aFa:J

.field private aFb:Lbfv;

.field private final aFc:Lbma;

.field private final aFd:Lbma;

.field private final aFe:Lbma;

.field private final aFf:Lbma;

.field private final aFg:Lbma;

.field private final aFh:Lbma;

.field private final aFi:Lbma;

.field private final aFj:Lbma;

.field private final aFk:Lbma;

.field private final aFl:Lbma;

.field private final aFm:Ljava/util/List;

.field private final aFn:Ljava/lang/Runnable;

.field private aFo:Z

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-wide/16 v4, 0x18

    const-string v0, "com.google.cast.media"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "urn:x-cast:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lblw;->NAMESPACE:Ljava/lang/String;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lblw;->aEW:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lblw;->aEX:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lblw;->aEY:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lblw;->aEZ:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lblw;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lblw;->NAMESPACE:Ljava/lang/String;

    const-string v1, "MediaControlChannel"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lblf;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lblw;->mHandler:Landroid/os/Handler;

    new-instance v0, Lblx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lblx;-><init>(Lblw;B)V

    iput-object v0, p0, Lblw;->aFn:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lblw;->aFm:Ljava/util/List;

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEX:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFc:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFc:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFd:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFd:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFe:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFe:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFf:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFf:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEY:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFg:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFg:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFh:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFh:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFi:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFi:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFj:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFj:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFk:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFk:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbma;

    sget-wide v2, Lblw;->aEW:J

    invoke-direct {v0, v2, v3}, Lbma;-><init>(J)V

    iput-object v0, p0, Lblw;->aFl:Lbma;

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    iget-object v1, p0, Lblw;->aFl:Lbma;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lblw;->zB()V

    return-void
.end method

.method static synthetic a(Lblw;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    return-object v0
.end method

.method private a(JLorg/json/JSONObject;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lblw;->aFc:Lbma;

    invoke-virtual {v0, p1, p2}, Lbma;->I(J)Z

    move-result v3

    iget-object v0, p0, Lblw;->aFg:Lbma;

    invoke-virtual {v0}, Lbma;->zD()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lblw;->aFg:Lbma;

    invoke-virtual {v0, p1, p2}, Lbma;->I(J)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iget-object v4, p0, Lblw;->aFh:Lbma;

    invoke-virtual {v4}, Lbma;->zD()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lblw;->aFh:Lbma;

    invoke-virtual {v4, p1, p2}, Lbma;->I(J)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lblw;->aFi:Lbma;

    invoke-virtual {v4}, Lbma;->zD()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lblw;->aFi:Lbma;

    invoke-virtual {v4, p1, p2}, Lbma;->I(J)Z

    move-result v4

    if-nez v4, :cond_8

    :cond_1
    :goto_1
    if-eqz v0, :cond_b

    const/4 v0, 0x2

    :goto_2
    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_2
    if-nez v3, :cond_3

    iget-object v1, p0, Lblw;->aFb:Lbfv;

    if-nez v1, :cond_9

    :cond_3
    new-instance v0, Lbfv;

    invoke-direct {v0, p3}, Lbfv;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lblw;->aFb:Lbfv;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lblw;->aFa:J

    const/4 v0, 0x7

    :goto_3
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lblw;->aFa:J

    invoke-virtual {p0}, Lblw;->yg()V

    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lblw;->aFa:J

    invoke-virtual {p0}, Lblw;->yg()V

    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lblw;->wK()V

    :cond_6
    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    invoke-virtual {v0, p1, p2, v2}, Lbma;->e(JI)Z

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    iget-object v1, p0, Lblw;->aFb:Lbfv;

    invoke-virtual {v1, p3, v0}, Lbfv;->a(Lorg/json/JSONObject;I)I

    move-result v0

    goto :goto_3

    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method static synthetic a(Lblw;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lblw;->aFo:Z

    return v0
.end method

.method static synthetic b(Lblw;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lblw;->bH(Z)V

    return-void
.end method

.method private bH(Z)V
    .locals 4

    iget-boolean v0, p0, Lblw;->aFo:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lblw;->aFo:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lblw;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lblw;->aFn:Ljava/lang/Runnable;

    sget-wide v2, Lblw;->aEZ:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lblw;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lblw;->aFn:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private zB()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lblw;->bH(Z)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lblw;->aFa:J

    const/4 v0, 0x0

    iput-object v0, p0, Lblw;->aFb:Lbfv;

    iget-object v0, p0, Lblw;->aFc:Lbma;

    invoke-virtual {v0}, Lbma;->clear()V

    iget-object v0, p0, Lblw;->aFg:Lbma;

    invoke-virtual {v0}, Lbma;->clear()V

    iget-object v0, p0, Lblw;->aFh:Lbma;

    invoke-virtual {v0}, Lbma;->clear()V

    return-void
.end method


# virtual methods
.method public final a(Lblz;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iget-object v1, p0, Lblf;->aEn:Lbly;

    invoke-interface {v1}, Lbly;->yh()J

    move-result-wide v2

    iget-object v1, p0, Lblw;->aFj:Lbma;

    invoke-virtual {v1, v2, v3, p1}, Lbma;->a(JLblz;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lblw;->bH(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "GET_STATUS"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Lblw;->aFb:Lbfv;

    if-eqz v1, :cond_0

    const-string v1, "mediaSessionId"

    iget-object v4, p0, Lblw;->aFb:Lbfv;

    iget-wide v4, v4, Lbfv;->azZ:J

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lblw;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b(JI)V
    .locals 3

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    invoke-virtual {v0, p1, p2, p3}, Lbma;->e(JI)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final fr(Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lblw;->aEl:Lblv;

    const-string v1, "message received: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "requestId"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "MEDIA_STATUS"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Lblw;->a(JLorg/json/JSONObject;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lblw;->aFb:Lbfv;

    invoke-virtual {p0}, Lblw;->yg()V

    invoke-virtual {p0}, Lblw;->wK()V

    iget-object v0, p0, Lblw;->aFj:Lbma;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lbma;->e(JI)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lblw;->aEl:Lblv;

    const-string v2, "Message is malformed (%s); ignoring: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    aput-object p1, v3, v7

    invoke-virtual {v1, v2, v3}, Lblv;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v4, "INVALID_PLAYER_STATE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v1, p0, Lblw;->aEl:Lblv;

    const-string v4, "received unexpected error: Invalid Player State."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lblv;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Lbma;->a(JILorg/json/JSONObject;)Z

    goto :goto_1

    :cond_3
    const-string v4, "LOAD_FAILED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lblw;->aFc:Lbma;

    const/16 v4, 0x834

    invoke-virtual {v1, v2, v3, v4, v0}, Lbma;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    :cond_4
    const-string v4, "LOAD_CANCELLED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lblw;->aFc:Lbma;

    const/16 v4, 0x835

    invoke-virtual {v1, v2, v3, v4, v0}, Lbma;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    :cond_5
    const-string v4, "INVALID_REQUEST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lblw;->aEl:Lblv;

    const-string v4, "received unexpected error: Invalid Request."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lblv;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v0, p0, Lblw;->aFm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Lbma;->a(JILorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method protected wK()V
    .locals 0

    return-void
.end method

.method public final ye()J
    .locals 12

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lblw;->yf()Lbfs;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    iget-wide v0, p0, Lblw;->aFa:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblw;->aFb:Lbfv;

    iget-wide v10, v0, Lbfv;->aAb:D

    iget-object v0, p0, Lblw;->aFb:Lbfv;

    iget-wide v4, v0, Lbfv;->aAe:J

    iget-object v0, p0, Lblw;->aFb:Lbfv;

    iget v0, v0, Lbfv;->aAc:I

    const-wide/16 v6, 0x0

    cmpl-double v1, v10, v6

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    :cond_2
    move-wide v2, v4

    goto :goto_0

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Lblw;->aFa:J

    sub-long/2addr v0, v6

    cmp-long v6, v0, v2

    if-gez v6, :cond_7

    move-wide v6, v2

    :goto_1
    cmp-long v0, v6, v2

    if-nez v0, :cond_4

    move-wide v2, v4

    goto :goto_0

    :cond_4
    iget-wide v0, v8, Lbfs;->azQ:J

    long-to-double v6, v6

    mul-double/2addr v6, v10

    double-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v6, v0, v2

    if-lez v6, :cond_5

    cmp-long v6, v4, v0

    if-lez v6, :cond_5

    :goto_2
    move-wide v2, v0

    goto :goto_0

    :cond_5
    cmp-long v0, v4, v2

    if-gez v0, :cond_6

    move-wide v0, v2

    goto :goto_2

    :cond_6
    move-wide v0, v4

    goto :goto_2

    :cond_7
    move-wide v6, v0

    goto :goto_1
.end method

.method public final yf()Lbfs;
    .locals 1

    iget-object v0, p0, Lblw;->aFb:Lbfv;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lblw;->aFb:Lbfv;

    iget-object v0, v0, Lbfv;->aAa:Lbfs;

    goto :goto_0
.end method

.method protected yg()V
    .locals 0

    return-void
.end method

.method public final zk()V
    .locals 0

    invoke-direct {p0}, Lblw;->zB()V

    return-void
.end method
