.class public final Ljim;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile env:[Ljim;


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private afi:J

.field private afj:J

.field private afo:I

.field private dOg:Ljava/lang/String;

.field private ebr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31208
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 31209
    iput v1, p0, Ljim;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljim;->dOg:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljim;->ebr:Ljava/lang/String;

    iput-object v2, p0, Ljim;->aeB:Ljbp;

    iput-wide v4, p0, Ljim;->afi:J

    iput-wide v4, p0, Ljim;->afj:J

    iput v1, p0, Ljim;->afo:I

    iput-object v2, p0, Ljim;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljim;->eCz:I

    .line 31210
    return-void
.end method

.method public static bni()[Ljim;
    .locals 2

    .prologue
    .line 31091
    sget-object v0, Ljim;->env:[Ljim;

    if-nez v0, :cond_1

    .line 31092
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 31094
    :try_start_0
    sget-object v0, Ljim;->env:[Ljim;

    if-nez v0, :cond_0

    .line 31095
    const/4 v0, 0x0

    new-array v0, v0, [Ljim;

    sput-object v0, Ljim;->env:[Ljim;

    .line 31097
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31099
    :cond_1
    sget-object v0, Ljim;->env:[Ljim;

    return-object v0

    .line 31097
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 31078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljim;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljim;->dOg:Ljava/lang/String;

    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljim;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljim;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljim;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljim;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljim;->afi:J

    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljim;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljim;->afj:J

    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljim;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljim;->afo:I

    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljim;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljim;->ebr:Ljava/lang/String;

    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljim;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 31228
    iget v0, p0, Ljim;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 31229
    const/4 v0, 0x1

    iget-object v1, p0, Ljim;->dOg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31231
    :cond_0
    iget-object v0, p0, Ljim;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 31232
    const/4 v0, 0x2

    iget-object v1, p0, Ljim;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 31234
    :cond_1
    iget v0, p0, Ljim;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 31235
    const/4 v0, 0x3

    iget-wide v2, p0, Ljim;->afi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 31237
    :cond_2
    iget v0, p0, Ljim;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 31238
    const/4 v0, 0x4

    iget-wide v2, p0, Ljim;->afj:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 31240
    :cond_3
    iget v0, p0, Ljim;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 31241
    const/4 v0, 0x5

    iget v1, p0, Ljim;->afo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 31243
    :cond_4
    iget v0, p0, Ljim;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 31244
    const/4 v0, 0x6

    iget-object v1, p0, Ljim;->ebr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31246
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 31247
    return-void
.end method

.method public final dr(J)Ljim;
    .locals 1

    .prologue
    .line 31157
    iput-wide p1, p0, Ljim;->afi:J

    .line 31158
    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljim;->aez:I

    .line 31159
    return-object p0
.end method

.method public final ds(J)Ljim;
    .locals 1

    .prologue
    .line 31176
    iput-wide p1, p0, Ljim;->afj:J

    .line 31177
    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljim;->aez:I

    .line 31178
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 31251
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 31252
    iget v1, p0, Ljim;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 31253
    const/4 v1, 0x1

    iget-object v2, p0, Ljim;->dOg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31256
    :cond_0
    iget-object v1, p0, Ljim;->aeB:Ljbp;

    if-eqz v1, :cond_1

    .line 31257
    const/4 v1, 0x2

    iget-object v2, p0, Ljim;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31260
    :cond_1
    iget v1, p0, Ljim;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 31261
    const/4 v1, 0x3

    iget-wide v2, p0, Ljim;->afi:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 31264
    :cond_2
    iget v1, p0, Ljim;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 31265
    const/4 v1, 0x4

    iget-wide v2, p0, Ljim;->afj:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 31268
    :cond_3
    iget v1, p0, Ljim;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 31269
    const/4 v1, 0x5

    iget v2, p0, Ljim;->afo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 31272
    :cond_4
    iget v1, p0, Ljim;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 31273
    const/4 v1, 0x6

    iget-object v2, p0, Ljim;->ebr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31276
    :cond_5
    return v0
.end method

.method public final qc(I)Ljim;
    .locals 1

    .prologue
    .line 31195
    iput p1, p0, Ljim;->afo:I

    .line 31196
    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljim;->aez:I

    .line 31197
    return-object p0
.end method

.method public final vS(Ljava/lang/String;)Ljim;
    .locals 1

    .prologue
    .line 31110
    if-nez p1, :cond_0

    .line 31111
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31113
    :cond_0
    iput-object p1, p0, Ljim;->dOg:Ljava/lang/String;

    .line 31114
    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljim;->aez:I

    .line 31115
    return-object p0
.end method

.method public final vT(Ljava/lang/String;)Ljim;
    .locals 1

    .prologue
    .line 31132
    if-nez p1, :cond_0

    .line 31133
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31135
    :cond_0
    iput-object p1, p0, Ljim;->ebr:Ljava/lang/String;

    .line 31136
    iget v0, p0, Ljim;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljim;->aez:I

    .line 31137
    return-object p0
.end method
