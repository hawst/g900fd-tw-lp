.class public Lgpu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cSK:Leey;

.field final mAppContext:Landroid/content/Context;

.field private final mVelvetServices:Lgql;


# direct methods
.method public constructor <init>(Lgql;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    new-instance v0, Leey;

    invoke-direct {v0}, Leey;-><init>()V

    iput-object v0, p0, Lgpu;->cSK:Leey;

    .line 221
    iput-object p2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    .line 222
    iput-object p1, p0, Lgpu;->mVelvetServices:Lgql;

    .line 223
    return-void
.end method

.method private DD()Lcjs;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lgxe;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 417
    invoke-virtual {p0}, Lgxe;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 418
    const v1, 0x7f04004f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lgxe;Landroid/view/ViewGroup;Lcfy;)Landroid/view/View;
    .locals 3

    .prologue
    .line 412
    invoke-virtual {p0}, Lgxe;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 413
    iget v1, p2, Lcfy;->aUM:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lhad;)Landroid/view/View;
    .locals 4

    .prologue
    .line 731
    invoke-virtual {p0}, Lhad;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 732
    const v1, 0x7f0401bc

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lhad;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 356
    invoke-virtual {p0}, Lhad;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 357
    const v1, 0x7f0401bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lhah;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 350
    invoke-virtual {p0}, Lhah;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 351
    const v1, 0x7f0401be

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lgpu;)Lciu;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJZ()Lciu;

    move-result-object v0

    return-object v0
.end method

.method private a(Lgyy;Landroid/view/ViewGroup;I)Lcom/google/android/search/suggest/SuggestionListView;
    .locals 6

    .prologue
    .line 322
    invoke-virtual {p1}, Lgyy;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 323
    const/4 v1, 0x0

    invoke-virtual {v0, p3, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/suggest/SuggestionListView;

    .line 324
    iget-object v1, p0, Lgpu;->cSK:Leey;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJR()Lcgh;

    move-result-object v2

    invoke-interface {v2}, Lcgh;->EO()Leeo;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJT()Lesm;

    move-result-object v3

    invoke-virtual {p1}, Lgyy;->aLH()Lgyz;

    move-result-object v4

    invoke-virtual {v4}, Lgyz;->aLZ()Lela;

    move-result-object v4

    new-instance v5, Leli;

    invoke-direct {v5}, Leli;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leey;Leeo;Lesm;Lela;Leld;)V

    .line 328
    invoke-virtual {p1}, Lgyy;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->aLY()Leen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leen;)V

    .line 329
    return-object v0
.end method

.method public static a(Ldda;Lhym;)Ldcw;
    .locals 1

    .prologue
    .line 1283
    new-instance v0, Ldcw;

    invoke-direct {v0, p0, p1}, Ldcw;-><init>(Ldda;Lhym;)V

    return-object v0
.end method

.method public static a(Lgxb;)Lgxa;
    .locals 1

    .prologue
    .line 723
    new-instance v0, Lgxa;

    invoke-direct {v0, p0}, Lgxa;-><init>(Lgxb;)V

    return-object v0
.end method

.method public static a(Lgzz;)Lgyd;
    .locals 1

    .prologue
    .line 651
    new-instance v0, Lgyd;

    invoke-direct {v0, p0}, Lgyd;-><init>(Lgzz;)V

    return-object v0
.end method

.method private a(Lifg;)Ligi;
    .locals 1
    .param p1    # Lifg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 498
    new-instance v0, Lgqd;

    invoke-direct {v0, p0, p1}, Lgqd;-><init>(Lgpu;Lifg;)V

    return-object v0
.end method

.method private aJA()Lhhb;
    .locals 4

    .prologue
    .line 680
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 681
    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    .line 682
    iget-object v2, v1, Lhhq;->mSettings:Lhym;

    .line 684
    new-instance v3, Lhhb;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lchk;->gn(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1}, Lhhq;->aAq()Lgno;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lhhb;-><init>(ZLgno;)V

    return-object v3
.end method

.method public static aJJ()Ldcf;
    .locals 1

    .prologue
    .line 1327
    new-instance v0, Ldcf;

    invoke-direct {v0}, Ldcf;-><init>()V

    return-object v0
.end method

.method private aJo()Lerk;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    return-object v0
.end method

.method private aJp()Lema;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    return-object v0
.end method

.method public static aJv()Ldep;
    .locals 1

    .prologue
    .line 397
    new-instance v0, Ldep;

    invoke-direct {v0}, Ldep;-><init>()V

    return-object v0
.end method

.method public static d(Lgyy;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 333
    invoke-virtual {p0}, Lgyy;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 334
    const v1, 0x7f0401c9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcmw;)Lcqe;
    .locals 8
    .param p1    # Lcmw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1367
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v7

    .line 1370
    new-instance v0, Lcqj;

    invoke-virtual {v7}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v7}, Lcfo;->DG()Ldkx;

    move-result-object v2

    invoke-virtual {v7}, Lcfo;->DI()Lcpn;

    move-result-object v3

    invoke-virtual {v7}, Lcfo;->DL()Lcrh;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DY()Lgno;

    move-result-object v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcqj;-><init>(Lchk;Ldkx;Lcpn;Lcrh;Lcmw;Lgno;)V

    .line 1373
    new-instance v1, Lcql;

    invoke-virtual {v7}, Lcfo;->DD()Lcjs;

    move-result-object v2

    invoke-virtual {v7}, Lcfo;->DC()Lemp;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v7}, Lcfo;->DI()Lcpn;

    move-result-object v5

    invoke-virtual {v7}, Lcfo;->BK()Lcke;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcql;-><init>(Lcjs;Lemp;Landroid/content/Context;Lcpn;Lcke;)V

    .line 1375
    new-instance v2, Lcqh;

    iget-object v3, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, v0, v1, v3, v7}, Lcqh;-><init>(Lcqs;Lcqu;Landroid/content/Context;Lcfo;)V

    .line 1376
    return-object v2
.end method

.method public static e(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/search/core/ui/ErrorView;
    .locals 3

    .prologue
    .line 338
    invoke-virtual {p0}, Lgyy;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 339
    const v1, 0x7f040080

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/ui/ErrorView;

    return-object v0
.end method

.method public static f(Lgyy;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 344
    invoke-virtual {p0}, Lgyy;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 345
    const v1, 0x7f0400d8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private fO(Z)Lcnq;
    .locals 7

    .prologue
    .line 741
    new-instance v0, Lcnq;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DI()Lcpn;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DG()Ldkx;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DU()Lcfu;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->aJR()Lcgh;

    move-result-object v5

    invoke-interface {v5}, Lcgh;->EV()Lddl;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v5, Lcgg;->aVl:Lcgg;

    invoke-virtual {v5}, Lcgg;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->aJY()Lgif;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->aJX()Lggq;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcox;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcox;

    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v6

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lcnq;-><init>(Lcke;Lcpn;Ldkx;Z[Lcox;Lckw;)V

    return-object v0
.end method

.method public static g(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/GetGoogleNowView;
    .locals 3

    .prologue
    .line 718
    invoke-virtual {p0}, Lgyy;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 719
    const v1, 0x7f040092

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/GetGoogleNowView;

    return-object v0
.end method

.method public static n(Ldda;)Ldcv;
    .locals 1

    .prologue
    .line 1302
    new-instance v0, Ldcv;

    invoke-direct {v0, p0}, Ldcv;-><init>(Ldda;)V

    return-object v0
.end method

.method public static o(Ldda;)Ldbi;
    .locals 1

    .prologue
    .line 1306
    new-instance v0, Ldbi;

    invoke-direct {v0, p0}, Ldbi;-><init>(Ldda;)V

    return-object v0
.end method

.method public static s(Ldda;)Ldbh;
    .locals 1

    .prologue
    .line 1331
    new-instance v0, Ldbh;

    invoke-direct {v0, p0}, Ldbh;-><init>(Ldda;)V

    return-object v0
.end method


# virtual methods
.method public final A(Ljava/lang/String;Z)Ljava/util/concurrent/Callable;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 763
    const-string v1, "refresh_search_history"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 764
    new-instance v0, Lgqf;

    invoke-direct {v0, p0}, Lgqf;-><init>(Lgpu;)V

    .line 946
    :cond_0
    :goto_0
    return-object v0

    .line 774
    :cond_1
    const-string v1, "flush_analytics"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 775
    new-instance v0, Lgqg;

    invoke-direct {v0, p0}, Lgqg;-><init>(Lgpu;)V

    goto :goto_0

    .line 782
    :cond_2
    const-string v1, "refresh_search_domain_and_cookies"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 783
    new-instance v0, Lcot;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DL()Lcrh;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DI()Lcpn;

    move-result-object v5

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->DG()Ldkx;

    move-result-object v6

    iget-object v7, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DT()Lgpf;

    move-result-object v7

    iget-object v8, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v8}, Lgql;->SC()Lcfo;

    move-result-object v8

    invoke-virtual {v8}, Lcfo;->DK()Ldkq;

    move-result-object v8

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v9

    invoke-virtual {v9}, Lema;->aus()Leqo;

    move-result-object v9

    iget-object v10, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v10}, Lgql;->SC()Lcfo;

    move-result-object v10

    invoke-virtual {v10}, Lcfo;->DS()Lckg;

    move-result-object v10

    move-object v11, p0

    move v12, p2

    invoke-direct/range {v0 .. v12}, Lcot;-><init>(Lemp;Lchk;Lcke;Lcrh;Lcpn;Ldkx;Lgpf;Ldkq;Ljava/util/concurrent/Executor;Lckg;Lgpu;Z)V

    goto :goto_0

    .line 795
    :cond_3
    const-string v1, "update_gservices_config"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 796
    new-instance v0, Lchx;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJq()Lhhq;

    move-result-object v3

    iget-object v3, v3, Lhhq;->mSettings:Lhym;

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJs()Lchr;

    move-result-object v4

    iget-object v4, v4, Lchr;->mGelStartupPrefs:Ldku;

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lchx;-><init>(Landroid/content/Context;Lcfo;Lhym;Ldku;I)V

    goto/16 :goto_0

    .line 799
    :cond_4
    const-string v1, "update_icing_corpora"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 800
    const-string v0, "Velvet.VelvetFactory"

    const-string v1, "refreshing internal icing corpora"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    new-instance v0, Lgqh;

    invoke-direct {v0, p0}, Lgqh;-><init>(Lgpu;)V

    goto/16 :goto_0

    .line 809
    :cond_5
    const-string v1, "send_gsa_home_request"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 811
    invoke-direct {p0, v3}, Lgpu;->fO(Z)Lcnq;

    move-result-object v0

    goto/16 :goto_0

    .line 812
    :cond_6
    const-string v1, "send_gsa_home_request_then_crash"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 814
    invoke-direct {p0, v4}, Lgpu;->fO(Z)Lcnq;

    move-result-object v0

    goto/16 :goto_0

    .line 815
    :cond_7
    const-string v1, "delete_local_search_history"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 817
    new-instance v0, Lgqi;

    invoke-direct {v0, p0}, Lgqi;-><init>(Lgpu;)V

    goto/16 :goto_0

    .line 824
    :cond_8
    const-string v1, "update_language_packs"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 825
    const-string v0, "Velvet.VelvetFactory"

    const-string v1, "checking for language pack updates"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    new-instance v0, Lhrw;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    invoke-virtual {v1}, Lhhq;->aOX()Lhrk;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DY()Lgno;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJq()Lhhq;

    move-result-object v3

    iget-object v3, v3, Lhhq;->mSettings:Lhym;

    invoke-direct {v0, v1, v2, v3}, Lhrw;-><init>(Lhrk;Lgno;Lhym;)V

    goto/16 :goto_0

    .line 830
    :cond_9
    const-string v1, "refresh_auth_tokens"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 832
    new-instance v0, Lcor;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v1

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcor;-><init>(Lcrh;Lchk;)V

    goto/16 :goto_0

    .line 834
    :cond_a
    const-string v1, "send_training_answers"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 835
    new-instance v0, Lgqj;

    invoke-direct {v0, p0}, Lgqj;-><init>(Lgpu;)V

    goto/16 :goto_0

    .line 842
    :cond_b
    const-string v1, "clear_training_data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 843
    new-instance v0, Lgqk;

    invoke-direct {v0, p0}, Lgqk;-><init>(Lgpu;)V

    goto/16 :goto_0

    .line 850
    :cond_c
    const-string v1, "sync_gel_prefs"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 851
    new-instance v0, Lgpw;

    invoke-direct {v0, p0}, Lgpw;-><init>(Lgpu;)V

    goto/16 :goto_0

    .line 898
    :cond_d
    const-string v1, "log_contacts_to_clearcut_unconditionally"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "log_contacts_to_clearcut_after_hash_check"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 904
    :cond_e
    const-string v0, "log_contacts_to_clearcut_after_hash_check"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 906
    new-instance v1, Lcnm;

    invoke-direct {v1}, Lcnm;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcnm;->ab(J)Lcnm;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcnm;->cv(Z)Lcnm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcnm;->cw(Z)Lcnm;

    move-result-object v1

    .line 910
    new-instance v0, Lcnn;

    iget-object v2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJs()Lchr;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4, v1}, Lcnn;-><init>(Landroid/content/Context;Lchr;Lcfo;Lcnm;)V

    goto/16 :goto_0

    .line 912
    :cond_f
    const-string v1, "log_contact_accounts_to_clearcut"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 913
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 914
    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJs()Lchr;

    move-result-object v1

    .line 915
    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    .line 916
    invoke-static {v1, v4, v2, v3}, Lcnj;->a(Lchr;Lcfo;J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 918
    new-instance v0, Lcnj;

    iget-object v2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v2, v1, v4}, Lcnj;-><init>(Landroid/content/Context;Lchr;Lcfo;)V

    goto/16 :goto_0

    .line 923
    :cond_10
    const-string v1, "update_hotword_models"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 924
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    invoke-virtual {v0}, Lhhq;->aOW()Lcsd;

    move-result-object v0

    goto/16 :goto_0

    .line 925
    :cond_11
    const-string v1, "refresh_audio_history_preference"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 926
    new-instance v0, Larq;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJR()Lcgh;

    move-result-object v1

    invoke-interface {v1}, Lcgh;->EU()Lcrr;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v3

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Larq;-><init>(Lcrr;Lhhq;Lcrh;Lchk;)V

    goto/16 :goto_0

    .line 931
    :cond_12
    const-string v1, "silent_speakerid_enrollment"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 932
    new-instance v0, Lgpx;

    invoke-direct {v0, p0}, Lgpx;-><init>(Lgpu;)V

    goto/16 :goto_0

    .line 940
    :cond_13
    const-string v1, "migrate_relationships"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 941
    new-instance v0, Lcjr;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    iget-object v2, v1, Lhhq;->mContactLookup:Lghy;

    if-nez v2, :cond_14

    iget-object v2, v1, Lhhq;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v2

    iput-object v2, v1, Lhhq;->mContactLookup:Lghy;

    :cond_14
    iget-object v1, v1, Lhhq;->mContactLookup:Lghy;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJZ()Lciu;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJV()Lcjg;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcjr;-><init>(Lghy;Lciu;Lcjg;)V

    goto/16 :goto_0

    .line 945
    :cond_15
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t create task for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lifv;->c(ZLjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method final BL()Lchk;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    return-object v0
.end method

.method final SC()Lcfo;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcex;)Lceu;
    .locals 4

    .prologue
    .line 617
    new-instance v0, Lceu;

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v1

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DG()Ldkx;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Lceu;-><init>(Leqo;Ljava/util/concurrent/Executor;Ldkx;Lcex;)V

    return-object v0
.end method

.method public final a(Leoj;Lcil;Lcik;)Lcey;
    .locals 8

    .prologue
    .line 1248
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v3

    .line 1249
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v5

    .line 1250
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DY()Lgno;

    move-result-object v6

    .line 1251
    new-instance v0, Lcid;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    move-object v2, p1

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcid;-><init>(Landroid/content/Context;Leoj;Lcpn;Lcil;Lcke;Lgno;Lcik;)V

    return-object v0
.end method

.method public final a(Leoj;Ldda;)Lcid;
    .locals 9

    .prologue
    .line 1231
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v3

    .line 1232
    invoke-static {v3}, Lcid;->searchResultsTrustPolicy(Lcpn;)Lcil;

    move-result-object v4

    .line 1234
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v5

    .line 1235
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DY()Lgno;

    move-result-object v6

    .line 1236
    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v0

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v8

    .line 1237
    new-instance v0, Lcid;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    move-object v2, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcid;-><init>(Landroid/content/Context;Leoj;Lcpn;Lcil;Lcke;Lgno;Ldda;Lemm;)V

    return-object v0
.end method

.method public final a(Ldao;Ldaz;Leqp;)Lcjt;
    .locals 11

    .prologue
    .line 532
    new-instance v0, Lcjt;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v3

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v4

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v6

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    new-instance v10, Lbwo;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DC()Lemp;

    move-result-object v5

    invoke-direct {v10, v2, v5}, Lbwo;-><init>(Lhym;Lemp;)V

    move-object v2, p1

    move-object v5, p0

    move-object v8, p2

    move-object v9, p3

    invoke-direct/range {v0 .. v10}, Lcjt;-><init>(Landroid/content/Context;Ldao;Lcfo;Lhhq;Lgpu;Leqo;Ljava/util/concurrent/Executor;Ldaz;Leqp;Lbwo;)V

    return-object v0
.end method

.method public final a(Lcmt;)Lcmw;
    .locals 12

    .prologue
    .line 970
    new-instance v0, Lcmw;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DI()Lcpn;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DG()Ldkx;

    move-result-object v5

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v6

    invoke-virtual {v6}, Lema;->aus()Leqo;

    move-result-object v7

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v6

    invoke-virtual {v6}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->DZ()Lczz;

    move-result-object v9

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->DT()Lgpf;

    move-result-object v10

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->DW()Lcpq;

    move-result-object v11

    move-object v6, p1

    invoke-direct/range {v0 .. v11}, Lcmw;-><init>(Lchk;Lemp;Lcrh;Lcpn;Ldkx;Lcmt;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lczz;Lgpf;Lcpq;)V

    return-object v0
.end method

.method public final a(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/search/suggest/SuggestionListView;
    .locals 1

    .prologue
    .line 307
    const v0, 0x7f0401ca

    invoke-direct {p0, p1, p2, v0}, Lgpu;->a(Lgyy;Landroid/view/ViewGroup;I)Lcom/google/android/search/suggest/SuggestionListView;

    move-result-object v0

    return-object v0
.end method

.method public final a(Leqp;Lcom/google/android/search/core/state/QueryState;Ldcu;)Ldee;
    .locals 8

    .prologue
    .line 402
    new-instance v0, Ldee;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJR()Lcgh;

    move-result-object v3

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v4

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Ldee;-><init>(Landroid/content/Context;Lchk;Lcgh;Lema;Leqp;Lcom/google/android/search/core/state/QueryState;Ldcu;)V

    return-object v0
.end method

.method public final a(Landroid/webkit/WebView;Ldda;Ldpk;Ldnb;Ldmz;Leoj;)Ldmv;
    .locals 10

    .prologue
    .line 1190
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1191
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1192
    new-instance v0, Lcho;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v2

    invoke-virtual {v2}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v5

    iget-object v2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v8

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v8}, Lcho;-><init>(Lcfo;Ldda;Ldpk;Ldnb;Lfaq;Leqp;Ljava/lang/String;Ljava/util/concurrent/Executor;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1202
    invoke-interface {v9, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1204
    new-instance v0, Ldmv;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v2

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v3

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v5

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v1

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v6

    move-object v1, p1

    move-object v4, v9

    invoke-direct/range {v0 .. v6}, Ldmv;-><init>(Landroid/webkit/WebView;Lcjs;Lcke;Ljava/util/List;Lemp;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public final a(Ldnb;Ldda;)Ldmz;
    .locals 6

    .prologue
    .line 1215
    new-instance v0, Ldmz;

    invoke-virtual {p2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v2

    invoke-virtual {p2}, Ldda;->aar()Lddk;

    move-result-object v3

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DI()Lcpn;

    move-result-object v4

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldmz;-><init>(Ldnb;Lcom/google/android/search/core/state/QueryState;Lddk;Lcpn;Lemp;)V

    return-object v0
.end method

.method public final a(Ldnj;Ldda;)Ldnb;
    .locals 13

    .prologue
    .line 604
    new-instance v0, Ldnb;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-direct {p0}, Lgpu;->DD()Lcjs;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DC()Lemp;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DI()Lcpn;

    move-result-object v5

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v6

    invoke-virtual {v6}, Lema;->aus()Leqo;

    move-result-object v6

    invoke-virtual {p2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v8

    invoke-virtual {p2}, Ldda;->aar()Lddk;

    move-result-object v9

    iget-object v7, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DP()Lcob;

    move-result-object v10

    iget-object v7, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DS()Lckg;

    move-result-object v11

    iget-object v12, p0, Lgpu;->mAppContext:Landroid/content/Context;

    move-object v7, p1

    invoke-direct/range {v0 .. v12}, Ldnb;-><init>(Lcke;Lcjs;Lchk;Lemp;Lcpn;Leqo;Ldnj;Lcom/google/android/search/core/state/QueryState;Lddk;Lcob;Ligi;Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Ldda;Leoj;)Ldnm;
    .locals 12

    .prologue
    .line 1032
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    .line 1033
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    iget-object v2, v0, Lgql;->mAsyncServices:Lema;

    .line 1034
    new-instance v0, Ldnm;

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJq()Lhhq;

    move-result-object v3

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v4

    new-instance v5, Lgpz;

    invoke-direct {v5, p0, p2}, Lgpz;-><init>(Lgpu;Leoj;)V

    invoke-static {v5}, Ligj;->b(Ligi;)Ligi;

    move-result-object v5

    invoke-virtual {v1}, Lcfo;->Eg()Ligi;

    move-result-object v6

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v8

    iget-object v9, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v10

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aKa()Lciy;

    move-result-object v11

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v11}, Ldnm;-><init>(Ldda;Lgpu;Lhhq;Lcrh;Ligi;Ligi;Ljava/util/concurrent/Executor;Leqo;Landroid/content/Context;Lchk;Lciy;)V

    return-object v0
.end method

.method public final a(Ldda;Ldao;Leqp;)Ldoi;
    .locals 9

    .prologue
    .line 1098
    new-instance v0, Ldoi;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DI()Lcpn;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DQ()Lcpd;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DU()Lcfu;

    move-result-object v6

    move-object v5, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Ldoi;-><init>(Landroid/content/Context;Lchk;Lcpn;Lcpd;Ldda;Lcfu;Ldao;Leqp;)V

    return-object v0
.end method

.method public final a(Ldda;Ldnz;Ldlu;Ldmh;)Ldoj;
    .locals 15

    .prologue
    .line 1071
    new-instance v0, Ldoj;

    invoke-virtual/range {p1 .. p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Ldda;->aal()Ldby;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Ldda;->aao()Ldcu;

    move-result-object v3

    iget-object v5, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJq()Lhhq;

    move-result-object v6

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v4

    invoke-virtual {v4}, Lema;->aus()Leqo;

    move-result-object v7

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v4

    invoke-virtual {v4}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v9

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->Eo()Lckx;

    move-result-object v11

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJq()Lhhq;

    move-result-object v4

    iget-object v13, v4, Lhhq;->mSettings:Lhym;

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->BK()Lcke;

    move-result-object v14

    move-object/from16 v4, p2

    move-object/from16 v10, p3

    move-object/from16 v12, p4

    invoke-direct/range {v0 .. v14}, Ldoj;-><init>(Lcom/google/android/search/core/state/QueryState;Ldby;Ldcu;Ldnz;Landroid/content/Context;Lhhq;Leqo;Ljava/util/concurrent/ScheduledExecutorService;Lchk;Ldlu;Lckx;Ldmh;Lhym;Lcke;)V

    return-object v0
.end method

.method public final a(Ldda;Ldmh;Ldaz;Leqp;)Ldpb;
    .locals 12

    .prologue
    .line 1089
    new-instance v0, Ldpb;

    invoke-virtual {p1}, Ldda;->aao()Ldcu;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v6

    invoke-virtual {p1}, Ldda;->aaj()Ldbd;

    move-result-object v7

    invoke-virtual {p1}, Ldda;->aau()Ldbh;

    move-result-object v8

    iget-object v4, p0, Lgpu;->mAppContext:Landroid/content/Context;

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/telephony/TelephonyManager;

    iget-object v4, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v11

    move-object v4, p3

    move-object v5, p2

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Ldpb;-><init>(Ldcu;Landroid/content/Context;Lgql;Ldaz;Ldmh;Lcom/google/android/search/core/state/QueryState;Ldbd;Ldbh;Leqp;Landroid/telephony/TelephonyManager;Lcn;)V

    return-object v0
.end method

.method public final a(Lcjt;Ldda;Leoj;)Ldpk;
    .locals 9

    .prologue
    .line 1105
    new-instance v0, Ldpk;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DI()Lcpn;

    move-result-object v5

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v8

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Ldpk;-><init>(Lcfo;Lcjt;Ldda;Lchk;Lcpn;Leoj;Lgpu;Leqo;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/velvet/ui/MainContentView;)Lguz;
    .locals 4

    .prologue
    .line 524
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Lcom/google/android/velvet/ui/MainContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090162

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 526
    new-instance v1, Lguz;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v3

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    invoke-direct {v1, p1, v0, v2, v3}, Lguz;-><init>(Lcom/google/android/velvet/ui/MainContentView;Landroid/content/Context;Lcfo;Leqo;)V

    return-object v1
.end method

.method public final a(Lgxh;)Lgxe;
    .locals 2

    .prologue
    .line 407
    new-instance v0, Lgxe;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DU()Lcfu;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lgxe;-><init>(Lcfu;Lgxh;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)Lgxi;
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 628
    const-string v0, "results"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/velvet/ui/MainContentView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v6

    new-instance v0, Lgpv;

    invoke-direct {v0, p0, v4, v6}, Lgpv;-><init>(Lgpu;Landroid/content/Context;Lchk;)V

    invoke-direct {p0, v0}, Lgpu;->a(Lifg;)Ligi;

    move-result-object v7

    new-instance v0, Lgxw;

    invoke-virtual {v1}, Lhhq;->aPm()Lhix;

    move-result-object v2

    invoke-virtual {v1}, Lhhq;->aPj()Lice;

    move-result-object v3

    new-instance v5, Lhgz;

    invoke-direct {v5, v4, v6, v7}, Lhgz;-><init>(Landroid/content/Context;Lchk;Ligi;)V

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v6

    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v7

    move-object v1, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lgxw;-><init>(Lcom/google/android/velvet/ui/MainContentView;Lhix;Lice;Lgpu;Lhgz;Lchk;Legs;)V

    .line 647
    :goto_0
    return-object v0

    .line 630
    :cond_0
    const-string v0, "suggest"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 631
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v1, "traditional_view_time_recording"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ldku;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    new-instance v0, Lfns;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJr()Lfdb;

    move-result-object v3

    invoke-virtual {v3}, Lfdb;->Sh()Lfcr;

    move-result-object v3

    invoke-direct {p0}, Lgpu;->aJo()Lerk;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lfns;-><init>(Landroid/content/Context;Lemp;Lfjv;Lern;Z)V

    new-instance v1, Lfmv;

    iget-object v2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {p0}, Lgpu;->aJo()Lerk;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJr()Lfdb;

    move-result-object v4

    invoke-virtual {v4}, Lfdb;->axP()Lfjs;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->aJr()Lfdb;

    move-result-object v5

    invoke-virtual {v5}, Lfdb;->axW()Lfzw;

    move-result-object v5

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->aJr()Lfdb;

    move-result-object v6

    invoke-virtual {v6}, Lfdb;->ayg()Lfml;

    move-result-object v6

    new-instance v8, Lgan;

    invoke-direct {v8}, Lgan;-><init>()V

    iget-object v7, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->anM()Lesm;

    move-result-object v9

    iget-object v7, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DC()Lemp;

    move-result-object v10

    move-object v7, v0

    invoke-direct/range {v1 .. v10}, Lfmv;-><init>(Landroid/content/Context;Lerk;Lfjs;Lfzw;Lfml;Lfns;Lgan;Lesm;Lemp;)V

    new-instance v2, Lewd;

    iget-object v3, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayg()Lfml;

    move-result-object v4

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v5, v0, Lfdb;->mUndoDismissManager:Lfnn;

    invoke-direct {p0}, Lgpu;->aJo()Lerk;

    move-result-object v6

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    invoke-virtual {v0}, Lhhq;->aAs()Lequ;

    move-result-object v7

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v8

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v9

    move v10, v11

    invoke-direct/range {v2 .. v10}, Lewd;-><init>(Landroid/content/Context;Lfml;Lfnn;Lern;Lequ;Lemp;Lcke;Z)V

    new-instance v3, Lgyf;

    invoke-direct {p0}, Lgpu;->aJo()Lerk;

    move-result-object v4

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v5

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DQ()Lcpd;

    move-result-object v6

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v9

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayg()Lfml;

    move-result-object v11

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v12, v0, Lfdb;->mUndoDismissManager:Lfnn;

    move-object v7, p2

    move-object v8, v1

    move-object v10, v2

    invoke-direct/range {v3 .. v12}, Lgyf;-><init>(Lerp;Lcjs;Lcpd;Lcom/google/android/velvet/ui/MainContentView;Lfmv;Lcin;Lewd;Lfml;Lfnn;)V

    iput-object v3, v2, Lewd;->ckk:Lgyi;

    invoke-virtual {v1, v3}, Lfmv;->a(Lfnc;)V

    move-object v0, v3

    goto/16 :goto_0

    .line 632
    :cond_1
    const-string v0, "summons"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 633
    new-instance v0, Lgyg;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJR()Lcgh;

    move-result-object v1

    invoke-interface {v1}, Lcgh;->EQ()Ldgd;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v2}, Lchk;->IR()I

    move-result v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-virtual {v3}, Lchk;->IS()I

    move-result v3

    invoke-direct {v0, p2, v1, v2, v3}, Lgyg;-><init>(Lcom/google/android/velvet/ui/MainContentView;Ldgd;II)V

    goto/16 :goto_0

    .line 634
    :cond_2
    const-string v0, "error"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 635
    new-instance v0, Lgwy;

    invoke-direct {v0, p2}, Lgwy;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    goto/16 :goto_0

    .line 636
    :cond_3
    const-string v0, "voicesearch"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 637
    new-instance v0, Lhag;

    invoke-direct {v0, p2}, Lhag;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    goto/16 :goto_0

    .line 638
    :cond_4
    const-string v0, "actiondiscovery"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 639
    invoke-virtual {p0, p2}, Lgpu;->a(Lcom/google/android/velvet/ui/MainContentView;)Lguz;

    move-result-object v0

    goto/16 :goto_0

    .line 640
    :cond_5
    const-string v0, "voicesearchlang"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 641
    new-instance v0, Lhah;

    invoke-direct {v0, p2}, Lhah;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    goto/16 :goto_0

    .line 642
    :cond_6
    const-string v0, "voicecorrection"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 643
    new-instance v0, Lhad;

    invoke-direct {v0, p2}, Lhad;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    goto/16 :goto_0

    .line 645
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Don\'t know how to create presenter "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lifv;->c(ZLjava/lang/Object;)V

    .line 647
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lhac;Lcom/google/android/search/shared/service/ClientConfig;)Lgyz;
    .locals 9

    .prologue
    .line 298
    new-instance v0, Lgyz;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v5

    iget-object v7, p0, Lgpu;->mAppContext:Landroid/content/Context;

    check-cast v7, Lcom/google/android/velvet/VelvetApplication;

    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v8

    move-object v2, p1

    move-object v3, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lgyz;-><init>(Landroid/content/Context;Lhac;Lcom/google/android/search/shared/service/ClientConfig;Lgql;Lema;Lgpu;Lgpe;Lckw;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/velvet/ui/InAppWebPageActivity;Leoj;)Lhax;
    .locals 21

    .prologue
    .line 710
    new-instance v10, Lhaw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lgpu;->aJp()Lema;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2, v3}, Lhaw;-><init>(Lgpu;Lcfo;Lema;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lgpu;->mAppContext:Landroid/content/Context;

    new-instance v2, Lhax;

    iget-object v3, v10, Lhaw;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v13

    iget-object v3, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DG()Ldkx;

    move-result-object v14

    iget-object v3, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DI()Lcpn;

    move-result-object v15

    new-instance v16, Lhbt;

    iget-object v3, v10, Lhaw;->mVelvetFactory:Lgpu;

    iget-object v4, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DI()Lcpn;

    move-result-object v4

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1, v4}, Lhbt;-><init>(Lgpu;Leoj;Lcpn;)V

    new-instance v17, Lhap;

    iget-object v3, v10, Lhaw;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v11

    iget-object v3, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DT()Lgpf;

    move-result-object v18

    iget-object v3, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v19

    iget-object v3, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DI()Lcpn;

    move-result-object v20

    new-instance v3, Lcmd;

    iget-object v4, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v5, v10, Lhaw;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    iget-object v6, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DG()Ldkx;

    move-result-object v6

    iget-object v7, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->DZ()Lczz;

    move-result-object v7

    const-wide/16 v8, 0x0

    invoke-direct/range {v3 .. v9}, Lcmd;-><init>(Lchk;Ljava/util/concurrent/ExecutorService;Ldkx;Lczz;J)V

    move-object/from16 v4, v17

    move-object v5, v11

    move-object/from16 v6, v18

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, Lhap;-><init>(Ljava/util/concurrent/Executor;Lgpf;Lchk;Lcpn;Lcmd;)V

    new-instance v9, Lhav;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Lhav;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    iget-object v3, v10, Lhaw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v11

    move-object v3, v13

    move-object v4, v14

    move-object v5, v15

    move-object v6, v12

    move-object/from16 v7, v16

    move-object/from16 v8, v17

    move-object/from16 v10, p1

    invoke-direct/range {v2 .. v11}, Lhax;-><init>(Leqo;Ldkx;Lcpn;Landroid/content/Context;Lhbt;Lhap;Lhav;Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcrh;)V

    return-object v2
.end method

.method public final a(Lgxi;Ldda;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Leoj;)Lhhe;
    .locals 15

    .prologue
    .line 657
    new-instance v0, Lhhe;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->Eg()Ligi;

    move-result-object v5

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJV()Lcjg;

    move-result-object v9

    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v10

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v12

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJZ()Lciu;

    move-result-object v13

    invoke-direct {p0}, Lgpu;->aJA()Lhhb;

    move-result-object v14

    move-object/from16 v4, p2

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v11, p5

    invoke-direct/range {v0 .. v14}, Lhhe;-><init>(Landroid/content/Context;Lemp;Lema;Ldda;Ligi;Lgxi;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Lcjg;Legs;Leoj;Lchk;Lciu;Lhhb;)V

    return-object v0
.end method

.method public final a(Leoj;)Lhkb;
    .locals 7

    .prologue
    .line 1001
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 1002
    invoke-virtual {v0}, Lcfo;->DX()Lenm;

    move-result-object v2

    .line 1003
    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v5

    .line 1004
    invoke-virtual {p0, p1}, Lgpu;->b(Leoj;)Lhll;

    move-result-object v3

    .line 1005
    invoke-virtual {p0}, Lgpu;->aJB()Lhla;

    move-result-object v4

    .line 1007
    new-instance v0, Lgpy;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lgpy;-><init>(Lgpu;Lenm;Lhll;Lhla;Lchk;)V

    invoke-direct {p0, v0}, Lgpu;->a(Lifg;)Ligi;

    move-result-object v6

    .line 1021
    new-instance v0, Lhkb;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lhkb;-><init>(Landroid/content/Context;Lenm;Lhll;Lhla;Lchk;Ligi;)V

    return-object v0
.end method

.method public final a(Ldda;Ligi;Ligi;)Lhyq;
    .locals 31

    .prologue
    .line 1117
    move-object/from16 v0, p0

    iget-object v2, v0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v10

    .line 1118
    move-object/from16 v0, p0

    iget-object v2, v0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v15

    .line 1119
    invoke-virtual {v10}, Lcfo;->BL()Lchk;

    move-result-object v16

    .line 1120
    new-instance v2, Lhyq;

    invoke-virtual/range {p1 .. p1}, Ldda;->aaj()Ldbd;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ldda;->aao()Ldcu;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lgpu;->mAppContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v6

    invoke-virtual {v10}, Lcfo;->Em()Lcha;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v8}, Lgql;->aKa()Lciy;

    move-result-object v8

    invoke-virtual {v10}, Lcfo;->BK()Lcke;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-static {v11}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-virtual {v10}, Lcfo;->DX()Lenm;

    move-result-object v13

    invoke-virtual {v10}, Lcfo;->BL()Lchk;

    move-result-object v14

    invoke-virtual {v14}, Lchk;->IA()Z

    move-result v14

    iget-object v15, v15, Lhhq;->mSettings:Lhym;

    invoke-virtual {v15}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lchk;->gn(Ljava/lang/String;)Z

    move-result v15

    invoke-virtual {v10}, Lcfo;->Eg()Ligi;

    move-result-object v17

    invoke-virtual {v10}, Lcfo;->DL()Lcrh;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lgpu;->aJz()Lgrx;

    move-result-object v19

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcfo;->a(Ldda;)Lbxy;

    move-result-object v20

    new-instance v21, Libv;

    new-instance v22, Lghd;

    move-object/from16 v0, p0

    iget-object v0, v0, Lgpu;->mAppContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lghd;-><init>(Landroid/content/ContentResolver;)V

    const/16 v23, 0x1

    invoke-direct/range {v21 .. v23}, Libv;-><init>(Lghd;Z)V

    invoke-virtual {v10}, Lcfo;->Ef()Lgpc;

    move-result-object v22

    new-instance v23, Licp;

    invoke-virtual {v10}, Lcfo;->Es()Libo;

    move-result-object v24

    invoke-virtual {v10}, Lcfo;->DL()Lcrh;

    move-result-object v25

    invoke-direct/range {v23 .. v25}, Licp;-><init>(Libo;Lglm;)V

    invoke-virtual {v10}, Lcfo;->DI()Lcpn;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lgpu;->mVelvetServices:Lgql;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lgql;->aJV()Lcjg;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lgpu;->mVelvetServices:Lgql;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lgql;->aJZ()Lciu;

    move-result-object v27

    invoke-virtual {v10}, Lcfo;->DF()Lcin;

    move-result-object v28

    new-instance v29, Lbwo;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v10}, Lgql;->aJq()Lhhq;

    move-result-object v10

    iget-object v10, v10, Lhhq;->mSettings:Lhym;

    move-object/from16 v0, p0

    iget-object v0, v0, Lgpu;->mVelvetServices:Lgql;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lgql;->SC()Lcfo;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcfo;->DC()Lemp;

    move-result-object v25

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-direct {v0, v10, v1}, Lbwo;-><init>(Lhym;Lemp;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v10}, Lgql;->aJR()Lcgh;

    move-result-object v10

    invoke-interface {v10}, Lcgh;->ET()Ldgm;

    move-result-object v30

    move-object/from16 v10, p2

    move-object/from16 v25, p3

    invoke-direct/range {v2 .. v30}, Lhyq;-><init>(Ldbd;Ldcu;Landroid/content/Context;Ldyv;Lcha;Lciy;Lcke;Ligi;Lghy;Landroid/content/ContentResolver;Lenm;ZZLchk;Ligi;Lcrh;Lgrx;Lbxy;Libv;Lgpc;Licp;Lcpn;Ligi;Lcjg;Lciu;Lcin;Lbwo;Ldgm;)V

    return-object v2
.end method

.method public final aJB()Lhla;
    .locals 5

    .prologue
    .line 690
    new-instance v0, Lhla;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v2

    invoke-virtual {v2}, Lfdb;->axI()Lfcx;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJr()Lfdb;

    move-result-object v3

    invoke-virtual {v3}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DC()Lemp;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lhla;-><init>(Landroid/content/Context;Lfcx;Lfaq;Lemp;)V

    return-object v0
.end method

.method public final aJC()Landroid/webkit/WebView;
    .locals 5

    .prologue
    .line 695
    new-instance v1, Lemc;

    iget-object v0, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lemc;-><init>(Landroid/content/Context;)V

    .line 696
    iget-object v0, p0, Lgpu;->mAppContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040156

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 699
    invoke-virtual {v1, v0}, Lemc;->setView(Landroid/view/View;)V

    .line 700
    invoke-static {}, Letc;->avI()V

    .line 701
    return-object v0
.end method

.method public final aJD()Landroid/webkit/WebView;
    .locals 2

    .prologue
    .line 953
    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 954
    invoke-static {}, Letc;->avI()V

    .line 955
    return-object v0
.end method

.method public final aJE()Lhlf;
    .locals 8

    .prologue
    .line 959
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v2

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v3

    new-instance v0, Lhlf;

    iget-object v4, v1, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v5, v1, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DZ()Lczz;

    move-result-object v5

    iget-object v6, v1, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DT()Lgpf;

    move-result-object v6

    iget-object v7, v1, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->DW()Lcpq;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lhlf;-><init>(Lhhq;Lemp;Lcpn;Lchk;Lczz;Lgpf;Lcpq;)V

    return-object v0
.end method

.method public final aJF()Lcmt;
    .locals 3

    .prologue
    .line 985
    new-instance v0, Lcmt;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DI()Lcpn;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcmt;-><init>(Lcjs;Lcpn;)V

    return-object v0
.end method

.method public final aJG()Lckm;
    .locals 7

    .prologue
    .line 991
    new-instance v0, Lckm;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DG()Ldkx;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DI()Lcpn;

    move-result-object v4

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v5

    invoke-virtual {v5}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->DC()Lemp;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lckm;-><init>(Landroid/content/Context;Lchk;Ldkx;Lcpn;Ljava/util/concurrent/ScheduledExecutorService;Lemp;)V

    return-object v0
.end method

.method public final aJH()Lhlr;
    .locals 7

    .prologue
    .line 1256
    new-instance v0, Lhlr;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DG()Ldkx;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DI()Lcpn;

    move-result-object v2

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJr()Lfdb;

    move-result-object v4

    invoke-virtual {v4}, Lfdb;->axX()Leuc;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DL()Lcrh;

    move-result-object v5

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lhlr;-><init>(Ldkx;Lcpn;Lerk;Leuc;Lcrh;Lchk;)V

    return-object v0
.end method

.method public final aJI()Licx;
    .locals 8

    .prologue
    .line 1266
    new-instance v0, Licx;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-static {v3}, Libo;->bU(Landroid/content/Context;)Libo;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DX()Lenm;

    move-result-object v5

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x13

    if-lt v6, v7, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-direct/range {v0 .. v6}, Licx;-><init>(Landroid/content/Context;Lchk;Libo;Ldyv;Lenm;Z)V

    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final aJK()Lcqe;
    .locals 8

    .prologue
    .line 1339
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v6

    .line 1340
    new-instance v7, Lcqj;

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v6}, Lcfo;->DG()Ldkx;

    move-result-object v1

    invoke-virtual {v6}, Lcfo;->DI()Lcpn;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DY()Lgno;

    move-result-object v3

    invoke-direct {v7, v0, v1, v2, v3}, Lcqj;-><init>(Lchk;Ldkx;Lcpn;Lgno;)V

    .line 1342
    new-instance v0, Lcql;

    invoke-virtual {v6}, Lcfo;->DD()Lcjs;

    move-result-object v1

    invoke-virtual {v6}, Lcfo;->DC()Lemp;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Lcfo;->DI()Lcpn;

    move-result-object v4

    invoke-virtual {v6}, Lcfo;->BK()Lcke;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcql;-><init>(Lcjs;Lemp;Landroid/content/Context;Lcpn;Lcke;)V

    .line 1344
    new-instance v1, Lcqh;

    iget-object v2, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v7, v0, v2, v6}, Lcqh;-><init>(Lcqs;Lcqu;Landroid/content/Context;Lcfo;)V

    return-object v1
.end method

.method public final aJL()Lcqe;
    .locals 1

    .prologue
    .line 1355
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgpu;->d(Lcmw;)Lcqe;

    move-result-object v0

    return-object v0
.end method

.method public final aJM()Ldmh;
    .locals 2

    .prologue
    .line 1387
    new-instance v0, Ldmh;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Ldmh;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method final aJq()Lhhq;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    return-object v0
.end method

.method final aJr()Lfdb;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    return-object v0
.end method

.method final aJs()Lchr;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    return-object v0
.end method

.method public final aJt()Lddn;
    .locals 8

    .prologue
    .line 361
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v1

    .line 362
    invoke-virtual {v1}, Lchk;->Fz()I

    move-result v2

    .line 363
    invoke-direct {p0}, Lgpu;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->Lg()I

    move-result v3

    .line 364
    new-instance v4, Lddn;

    new-instance v5, Ldfa;

    sget-object v0, Lcgg;->aVC:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lddy;

    new-instance v6, Ldfb;

    invoke-direct {v6, v1}, Ldfb;-><init>(Lchk;)V

    iget-object v7, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DC()Lemp;

    move-result-object v7

    invoke-direct {v0, v6, v7}, Lddy;-><init>(Ldeb;Lemp;)V

    :goto_0
    new-instance v6, Ldfy;

    invoke-direct {v6}, Ldfy;-><init>()V

    invoke-direct {v5, v0, v6, v2, v3}, Ldfa;-><init>(Ldeb;Ldeb;II)V

    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    invoke-direct {v4, v5, v1, v0}, Lddn;-><init>(Ldes;Lchk;Lemp;)V

    return-object v4

    :cond_0
    new-instance v0, Ldfb;

    invoke-direct {v0, v1}, Ldfb;-><init>(Lchk;)V

    goto :goto_0
.end method

.method public final aJu()Lddn;
    .locals 5

    .prologue
    .line 377
    new-instance v0, Lddn;

    new-instance v1, Ldec;

    const/4 v2, 0x0

    new-instance v3, Ldds;

    invoke-direct {v3}, Ldds;-><init>()V

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v4

    invoke-virtual {v4}, Lchk;->Fz()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Ldec;-><init>(ILdeb;I)V

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lddn;-><init>(Ldes;Lchk;Lemp;)V

    return-object v0
.end method

.method public final aJw()Livq;
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v0

    sget-object v1, Lhjt;->ctv:Lgor;

    invoke-virtual {v0, v1}, Lgtx;->c(Lgor;)Livq;

    move-result-object v0

    return-object v0
.end method

.method public final aJx()Ldda;
    .locals 3

    .prologue
    .line 576
    new-instance v0, Ldda;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-direct {v0, p0, v1, v2}, Ldda;-><init>(Lgpu;Lemp;Lhym;)V

    .line 581
    return-object v0
.end method

.method public final aJy()Lela;
    .locals 3

    .prologue
    .line 598
    new-instance v0, Lela;

    iget-object v1, p0, Lgpu;->cSK:Leey;

    const/4 v1, 0x2

    invoke-direct {p0}, Lgpu;->DD()Lcjs;

    move-result-object v2

    invoke-virtual {v2}, Lcjs;->LC()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lela;-><init>(II)V

    return-object v0
.end method

.method public final aJz()Lgrx;
    .locals 11

    .prologue
    .line 665
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v9

    .line 666
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v7

    .line 667
    iget-object v4, v7, Lhhq;->mSettings:Lhym;

    .line 669
    new-instance v0, Lgrx;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v9}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v9}, Lcfo;->Eg()Ligi;

    move-result-object v3

    invoke-virtual {v4}, Lhym;->aUn()J

    move-result-wide v4

    invoke-direct {p0}, Lgpu;->aJA()Lhhb;

    move-result-object v6

    invoke-virtual {v7}, Lhhq;->aAq()Lgno;

    move-result-object v7

    new-instance v8, Lgru;

    iget-object v10, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v9}, Lcfo;->DC()Lemp;

    move-result-object v9

    invoke-direct {v8, v10, v9}, Lgru;-><init>(Landroid/content/Context;Lemp;)V

    invoke-direct/range {v0 .. v8}, Lgrx;-><init>(Landroid/content/Context;Lchk;Ligi;JLhhb;Lgno;Lgru;)V

    return-object v0
.end method

.method public final b(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/search/suggest/SuggestionListView;
    .locals 1

    .prologue
    .line 312
    const v0, 0x7f040180

    invoke-direct {p0, p1, p2, v0}, Lgpu;->a(Lgyy;Landroid/view/ViewGroup;I)Lcom/google/android/search/suggest/SuggestionListView;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ldda;Lhym;)Ldby;
    .locals 3

    .prologue
    .line 1287
    new-instance v0, Ldby;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    invoke-direct {v0, p1, v1, p2, v2}, Ldby;-><init>(Ldda;Lchk;Lhym;Lemp;)V

    return-object v0
.end method

.method public final b(Leoj;)Lhll;
    .locals 9

    .prologue
    .line 1156
    new-instance v0, Lhll;

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v3, v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v6, Lgqa;

    invoke-direct {v6, p0}, Lgqa;-><init>(Lgpu;)V

    new-instance v7, Lgqb;

    invoke-direct {v7, p0}, Lgqb;-><init>(Lgpu;)V

    new-instance v8, Lgqc;

    invoke-direct {v8, p0}, Lgqc;-><init>(Lgpu;)V

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lhll;-><init>(Landroid/content/Context;Lcfo;Leoj;Landroid/content/pm/PackageManager;ZLigi;Ligi;Ligi;)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final c(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/search/suggest/SuggestionListView;
    .locals 1

    .prologue
    .line 317
    const v0, 0x7f040181

    invoke-direct {p0, p1, p2, v0}, Lgpu;->a(Lgyy;Landroid/view/ViewGroup;I)Lcom/google/android/search/suggest/SuggestionListView;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcmw;)Lcqe;
    .locals 3

    .prologue
    .line 1359
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmw;

    invoke-direct {p0, v0}, Lgpu;->d(Lcmw;)Lcqe;

    move-result-object v0

    .line 1362
    new-instance v1, Lcno;

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->Eu()Lddv;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcno;-><init>(Lcqe;Lddv;)V

    return-object v1
.end method

.method public final c(Ldda;Lhym;)Ldcu;
    .locals 9

    .prologue
    .line 1295
    new-instance v0, Ldcu;

    new-instance v2, Lbyl;

    invoke-direct {v2}, Lbyl;-><init>()V

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v4

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v5

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Leoh;->at(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {p0}, Lgpu;->aJM()Ldmh;

    move-result-object v7

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DY()Lgno;

    move-result-object v8

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Ldcu;-><init>(Ldda;Lbyl;Lhym;Lcke;Lchk;ZLdmh;Lgno;)V

    return-object v0
.end method

.method public final c(Lcjt;)Ldfl;
    .locals 13

    .prologue
    .line 540
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v3

    .line 541
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJR()Lcgh;

    move-result-object v4

    .line 542
    new-instance v10, Ldfe;

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v0

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iget-object v1, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct {v10, v3, v0, v1}, Ldfe;-><init>(Lcfo;Ljava/util/concurrent/Executor;Landroid/content/Context;)V

    .line 544
    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v0

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    .line 545
    new-instance v0, Ldfl;

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v1

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-virtual {v3}, Lcfo;->DQ()Lcpd;

    move-result-object v6

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v7

    invoke-interface {v4}, Lcgh;->ET()Ldgm;

    move-result-object v8

    invoke-interface {v4}, Lcgh;->EP()Ldfz;

    move-result-object v9

    new-instance v12, Lgqe;

    invoke-direct {v12, p0}, Lgqe;-><init>(Lgpu;)V

    move-object v5, p0

    move-object v11, p1

    invoke-direct/range {v0 .. v12}, Ldfl;-><init>(Leqo;Ljava/util/concurrent/Executor;Lcfo;Lcgh;Lgpu;Lcpd;Lemp;Ldgm;Ldfz;Ldfe;Lcjt;Ldft;)V

    return-object v0
.end method

.method public final d(Lcjt;)Ldpe;
    .locals 3

    .prologue
    .line 965
    new-instance v0, Ldpe;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v2

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Ldpe;-><init>(Lhhq;Ljava/util/concurrent/Executor;Lcjt;)V

    return-object v0
.end method

.method public final f(Ldda;)Lcom/google/android/search/core/state/QueryState;
    .locals 4

    .prologue
    .line 585
    new-instance v0, Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DI()Lcpn;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DQ()Lcpd;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/search/core/state/QueryState;-><init>(Ldda;Lcpn;Lcpd;Lchk;)V

    return-object v0
.end method

.method public final g(Ldda;)Ldbj;
    .locals 4

    .prologue
    .line 593
    new-instance v0, Ldbj;

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v1

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Ldbj;-><init>(Ldda;Leqo;Lchk;Lcke;)V

    return-object v0
.end method

.method public final h(Ldda;)Ldpg;
    .locals 8

    .prologue
    .line 1051
    new-instance v0, Ldpg;

    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {p1}, Ldda;->aak()Ldcw;

    move-result-object v2

    invoke-virtual {p1}, Ldda;->aaj()Ldbd;

    move-result-object v3

    invoke-virtual {p1}, Ldda;->aao()Ldcu;

    move-result-object v4

    iget-object v5, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->aJq()Lhhq;

    move-result-object v5

    iget-object v6, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v6}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcfo;->a(Ldda;)Lbxy;

    move-result-object v6

    iget-object v7, p0, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-direct/range {v0 .. v7}, Ldpg;-><init>(Lcom/google/android/search/core/state/QueryState;Ldcw;Ldbd;Ldcu;Lhhq;Lbxy;Landroid/content/Context;)V

    return-object v0
.end method

.method public final i(Ldda;)Ldnz;
    .locals 10

    .prologue
    .line 1057
    new-instance v0, Ldnz;

    invoke-virtual {p1}, Ldda;->aau()Ldbh;

    move-result-object v1

    invoke-virtual {p1}, Ldda;->aak()Ldcw;

    move-result-object v2

    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    iget-object v4, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->aJq()Lhhq;

    move-result-object v4

    invoke-direct {p0}, Lgpu;->aJp()Lema;

    move-result-object v5

    invoke-virtual {v5}, Lema;->aus()Leqo;

    move-result-object v5

    iget-object v6, p0, Lgpu;->mAppContext:Landroid/content/Context;

    iget-object v7, p0, Lgpu;->mAppContext:Landroid/content/Context;

    const-string v8, "audio"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/AudioManager;

    iget-object v8, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v8}, Lgql;->aJq()Lhhq;

    move-result-object v8

    iget-object v8, v8, Lhhq;->mSettings:Lhym;

    invoke-virtual {p1}, Ldda;->aao()Ldcu;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Ldnz;-><init>(Ldbh;Ldcw;Lcom/google/android/search/core/state/QueryState;Lhhq;Leqo;Landroid/content/Context;Landroid/media/AudioManager;Lhym;Ldcu;)V

    return-object v0
.end method

.method public final j(Ldgb;)Lddn;
    .locals 5

    .prologue
    .line 387
    new-instance v0, Lddn;

    new-instance v1, Ldec;

    const/4 v2, 0x2

    new-instance v3, Ldga;

    invoke-direct {v3, p1}, Ldga;-><init>(Ldgb;)V

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v4

    invoke-virtual {v4}, Lchk;->Ir()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Ldec;-><init>(ILdeb;I)V

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lddn;-><init>(Ldes;Lchk;Lemp;)V

    return-object v0
.end method

.method public final j(Ldda;)Ldou;
    .locals 4

    .prologue
    .line 1065
    new-instance v0, Ldou;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    invoke-virtual {p1}, Ldda;->aam()Ldcg;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p0}, Ldou;-><init>(Lcfo;Lhhq;Ldcg;Lgpu;)V

    return-object v0
.end method

.method public final k(Ldda;)Ldor;
    .locals 6

    .prologue
    .line 1081
    new-instance v0, Ldor;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->El()Lfdr;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DP()Lcob;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DD()Lcjs;

    move-result-object v3

    invoke-virtual {p1}, Ldda;->aau()Ldbh;

    move-result-object v4

    invoke-virtual {p1}, Ldda;->aao()Ldcu;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ldor;-><init>(Lfdr;Lcob;Lcjs;Ldbh;Ldcu;)V

    return-object v0
.end method

.method public final l(Ldda;)Ldbd;
    .locals 4

    .prologue
    .line 1274
    new-instance v0, Ldbd;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v1

    iget-object v2, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aKa()Lciy;

    move-result-object v2

    iget-object v3, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Ldbd;-><init>(Ldda;Lchk;Lciy;Lemp;)V

    return-object v0
.end method

.method public final m(Ldda;)Ldct;
    .locals 2

    .prologue
    .line 1279
    new-instance v0, Ldct;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ldct;-><init>(Ldda;Lchk;)V

    return-object v0
.end method

.method public final p(Ldda;)Ldcg;
    .locals 2

    .prologue
    .line 1310
    new-instance v0, Ldcg;

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    iget-object v1, v1, Lhhq;->mSettings:Lhym;

    invoke-direct {v0, p1, v1}, Ldcg;-><init>(Ldda;Lhym;)V

    return-object v0
.end method

.method public final q(Ldda;)Lddk;
    .locals 2

    .prologue
    .line 1314
    new-instance v0, Lddk;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lddk;-><init>(Ldda;Lchk;)V

    return-object v0
.end method

.method public final r(Ldda;)Ldcy;
    .locals 6

    .prologue
    .line 1318
    new-instance v0, Ldcy;

    invoke-virtual {p0}, Lgpu;->BL()Lchk;

    move-result-object v2

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v3

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v4

    iget-object v1, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldcy;-><init>(Ldda;Lchk;Lcke;Lfcx;Lemp;)V

    return-object v0
.end method

.method public final r(Landroid/content/Context;I)Lhgj;
    .locals 6

    .prologue
    .line 1380
    iget-object v0, p0, Lgpu;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    .line 1381
    iget-object v4, v1, Lhhq;->mSettings:Lhym;

    .line 1382
    new-instance v0, Lhgj;

    invoke-virtual {v1}, Lhhq;->aPb()Lhhu;

    move-result-object v2

    iget-object v1, v1, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    move-object v1, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lhgj;-><init>(Landroid/content/Context;Lhhu;Ljava/util/concurrent/Executor;Lhym;I)V

    return-object v0
.end method
