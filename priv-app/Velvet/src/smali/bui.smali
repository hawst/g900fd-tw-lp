.class public final Lbui;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbul;


# instance fields
.field private synthetic aKC:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 0

    .prologue
    .line 1115
    iput-object p1, p0, Lbui;->aKC:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;B)V
    .locals 0

    .prologue
    .line 1115
    invoke-direct {p0, p1}, Lbui;-><init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    return-void
.end method


# virtual methods
.method public final ap(Landroid/view/View;)Lbuk;
    .locals 3

    .prologue
    .line 1118
    new-instance v1, Lbuk;

    invoke-direct {v1}, Lbuk;-><init>()V

    .line 1119
    iput-object p1, v1, Lbuk;->aKE:Landroid/view/View;

    .line 1120
    const v0, 0x7f1103df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKF:Landroid/view/View;

    .line 1121
    const v0, 0x7f110053

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKH:Landroid/view/View;

    .line 1122
    iget-object v0, v1, Lbuk;->aKH:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKN:Landroid/widget/ImageView;

    .line 1123
    const v0, 0x7f1103e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbuk;->aKI:Landroid/widget/TextView;

    .line 1125
    const v0, 0x7f110054

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbuk;->aKJ:Landroid/widget/TextView;

    .line 1126
    const v0, 0x7f1103dd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKM:Landroid/widget/ImageView;

    .line 1127
    const v0, 0x7f1103e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, v1, Lbuk;->aKG:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    .line 1128
    const v0, 0x7f1103de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKD:Landroid/view/View;

    .line 1129
    iget-object v0, p0, Lbui;->aKC:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    const v2, 0x7f1103db

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->findViewById(I)Landroid/view/View;

    .line 1130
    iget-object v0, p0, Lbui;->aKC:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->g(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131
    const/high16 v0, 0x7f110000

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKK:Landroid/view/View;

    .line 1132
    iget-object v0, v1, Lbuk;->aKK:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKO:Landroid/widget/ImageView;

    .line 1133
    const v0, 0x7f1100bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKL:Landroid/view/View;

    .line 1134
    iget-object v0, v1, Lbuk;->aKL:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKP:Landroid/widget/ImageView;

    .line 1135
    const v0, 0x7f1100b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKT:Landroid/view/View;

    .line 1136
    iget-object v0, v1, Lbuk;->aKT:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKX:Landroid/widget/ImageView;

    .line 1137
    const v0, 0x7f1103dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKU:Landroid/widget/ImageView;

    .line 1139
    const v0, 0x7f1103e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKQ:Landroid/view/View;

    .line 1140
    const v0, 0x7f1103e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbuk;->aKR:Landroid/widget/TextView;

    .line 1142
    const v0, 0x7f1103e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbuk;->aKS:Landroid/widget/TextView;

    .line 1144
    const v0, 0x7f1100b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKV:Landroid/view/View;

    .line 1145
    iget-object v0, v1, Lbuk;->aKV:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKY:Landroid/widget/ImageView;

    .line 1146
    const v0, 0x7f1100ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbuk;->aKW:Landroid/view/View;

    .line 1147
    iget-object v0, v1, Lbuk;->aKW:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbuk;->aKZ:Landroid/widget/ImageView;

    .line 1150
    :cond_0
    return-object v1
.end method
