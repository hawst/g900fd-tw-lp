.class public Ldmz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcim;
.implements Leti;


# instance fields
.field private final aXc:Ldnb;

.field private final bCK:Ljava/util/List;

.field private final mClock:Lemp;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mUrlHelper:Lcpn;

.field private final mWebAppState:Lddk;


# direct methods
.method public constructor <init>(Ldnb;Lcom/google/android/search/core/state/QueryState;Lddk;Lcpn;Lemp;)V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Ldmz;->aXc:Ldnb;

    .line 103
    iput-object p2, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 104
    iput-object p3, p0, Ldmz;->mWebAppState:Lddk;

    .line 105
    iput-object p4, p0, Ldmz;->mUrlHelper:Lcpn;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldmz;->bCK:Ljava/util/List;

    .line 107
    iput-object p5, p0, Ldmz;->mClock:Lemp;

    .line 108
    return-void
.end method

.method private a(Ljava/lang/String;Lehm;)V
    .locals 4

    .prologue
    .line 323
    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0}, Lddk;->aaR()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 324
    invoke-static {p1, v0}, Ldmz;->d(Ljava/lang/String;Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lehm;)V

    .line 333
    :goto_0
    return-void

    .line 329
    :cond_0
    const/16 v1, 0x177

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 332
    const-string v0, "GWAC"

    const-string v1, "Discarding obsolete contextId: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 127
    if-nez p1, :cond_0

    .line 128
    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    invoke-static {v0, v1}, Lesp;->bb(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method private jS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 120
    const-string v0, "%1$s://%2$s"

    .line 121
    iget-object v1, p0, Ldmz;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->hE(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 5

    .prologue
    .line 569
    const-string v0, "GsaWebAppController"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 570
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    .line 571
    const-string v0, "mWebAppMessages"

    invoke-virtual {v2, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 572
    iget-object v0, p0, Ldmz;->bCK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 573
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 576
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 140
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    :try_start_0
    const-string v2, "ts"

    iget-object v3, p0, Ldmz;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p2, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_0
    iget-object v2, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v2}, Lddk;->aaL()Ljava/lang/String;

    move-result-object v2

    .line 156
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 162
    :goto_1
    return v0

    .line 160
    :cond_0
    iget-object v3, p0, Ldmz;->aXc:Ldnb;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%1$s(%2$s,%3$s);"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v0

    invoke-static {p1}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v0, 0x2

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldnb;->jV(Ljava/lang/String;)V

    move v0, v1

    .line 162
    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final aE(Lcom/google/android/shared/search/Query;)V
    .locals 6

    .prologue
    .line 518
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 521
    :try_start_0
    const-string v1, "nav"

    .line 522
    const-string v2, "gci"

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-static {v4, v5}, Lesp;->bb(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 523
    iget-object v2, p0, Ldmz;->mUrlHelper:Lcpn;

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/google/android/shared/search/Query;->a(Lehm;)Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcpn;->j(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 525
    const-string v3, "du"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 526
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 527
    const-string v2, "ss"

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 529
    :cond_0
    iget-object v2, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v2}, Lddk;->aaN()Z

    move-result v2

    .line 530
    const-string v3, "sc"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 531
    const-string v3, "hn"

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 532
    const-string v3, "ns"

    iget-object v4, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v4, p1}, Lddk;->aB(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 533
    invoke-virtual {p0, v1, v0}, Ldmz;->a(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534
    if-eqz v2, :cond_1

    .line 535
    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0, p1}, Lddk;->aA(Lcom/google/android/shared/search/Query;)V

    .line 539
    :goto_0
    const/16 v0, 0x162

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 549
    :goto_1
    return-void

    .line 537
    :cond_1
    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0, p1}, Lddk;->av(Lcom/google/android/shared/search/Query;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 546
    :catch_0
    move-exception v0

    .line 547
    const-string v1, "GWAC"

    const-string v2, "Exception creating JSON object for WebApp message - commitQuery."

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 543
    :cond_2
    :try_start_1
    iget-object v0, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    new-instance v1, Lcom/google/android/search/shared/actions/errors/SearchError;

    const v2, 0x90001

    invoke-direct {v1, p1, v2}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;I)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public final c(Lorg/json/JSONObject;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 178
    const-string v0, "SU"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 180
    const-string v0, "agsav2-init"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 181
    iget-object v0, p0, Ldmz;->aXc:Ldnb;

    invoke-virtual {v0}, Ldnb;->adC()Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v3, p0, Ldmz;->aXc:Ldnb;

    invoke-virtual {v3}, Ldnb;->adC()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldmz;->aXc:Ldnb;

    invoke-virtual {v0, v1}, Ldnb;->jY(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 183
    :cond_0
    :goto_0
    const-string v0, "agsav2-navus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 184
    const-string v1, "gci"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v1}, Lddk;->aaR()Lcom/google/android/shared/search/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/search/shared/actions/errors/SearchError;

    const v3, 0x90002

    invoke-direct {v2, v1, v3}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;I)V

    invoke-static {v0, v1}, Ldmz;->d(Ljava/lang/String;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 186
    :cond_1
    const-string v0, "agsav2-nq"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 187
    invoke-virtual {p0, v0}, Ldmz;->f(Lorg/json/JSONObject;)V

    .line 189
    :cond_2
    const-string v0, "agsav2-rndrc"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 190
    :try_start_0
    const-string v0, "gci"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v2}, Lddk;->aaS()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-static {v0, v2}, Ldmz;->d(Ljava/lang/String;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x173

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    iget-object v0, p0, Ldmz;->aXc:Ldnb;

    invoke-virtual {v0}, Ldnb;->adE()V

    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0, v2}, Lddk;->az(Lcom/google/android/shared/search/Query;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 192
    :cond_3
    :goto_1
    const-string v0, "agsav2-rndrf"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 193
    :try_start_1
    const-string v0, "gci"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "du"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v3}, Lddk;->aaS()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-static {v0, v3}, Ldmz;->d(Ljava/lang/String;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldmz;->mUrlHelper:Lcpn;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v2, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/search/core/state/QueryState;->e(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 195
    :cond_4
    :goto_2
    const-string v0, "agsav2-ssc"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 196
    :try_start_2
    const-string v0, "du"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "ss"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "ts"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0}, Lddk;->aaS()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v0, p0, Ldmz;->mUrlHelper:Lcpn;

    invoke-virtual/range {v0 .. v5}, Lcpn;->a(Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v2, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/search/core/state/QueryState;->h(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    .line 198
    :cond_5
    :goto_3
    const-string v0, "agsav2-ssr"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 199
    :try_start_3
    const-string v0, "du"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "oss"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "ss"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "ts"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0}, Lddk;->aaS()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v0, p0, Ldmz;->mUrlHelper:Lcpn;

    invoke-virtual/range {v0 .. v5}, Lcpn;->a(Lcom/google/android/shared/search/Query;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v3, p0, Ldmz;->mUrlHelper:Lcpn;

    invoke-virtual {v3, v1, v0}, Lcpn;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v2, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/search/core/state/QueryState;->f(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4

    .line 201
    :cond_6
    :goto_4
    const-string v0, "agsav2-ssp"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 202
    :try_start_4
    const-string v0, "du"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "ss"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ts"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v3, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v3}, Lddk;->aaS()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v6, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v6}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v6

    iget-object v7, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v7, v3}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-virtual {v6}, Lcom/google/android/shared/search/Query;->arb()J

    move-result-wide v8

    cmp-long v4, v4, v8

    if-lez v4, :cond_e

    invoke-virtual {v6}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v0, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xb()Z
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_5

    .line 204
    :cond_7
    :goto_5
    const-string v0, "agsav2-uq"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 205
    invoke-virtual {p0, v0}, Ldmz;->g(Lorg/json/JSONObject;)V

    .line 207
    :cond_8
    const-string v0, "agsav2-uqrc"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 208
    :try_start_5
    const-string v0, "gci"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "rid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ru"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Ldmz;->jS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v4}, Lddk;->aaK()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lehm;->e(Ljava/lang/String;Ljava/lang/String;J)Lehm;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Ldmz;->a(Ljava/lang/String;Lehm;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_6

    .line 210
    :cond_9
    :goto_6
    return-void

    .line 181
    :cond_a
    :try_start_6
    const-string v0, "af"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "rps"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v4, 0x16d

    invoke-static {v4}, Lege;->hs(I)Litu;

    move-result-object v4

    invoke-static {v4}, Lege;->a(Litu;)V

    iget-object v4, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v4, v0, v3}, Lddk;->al(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ackinit"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0, v0, v3}, Ldmz;->a(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v3, "GWAC"

    const-string v4, "handleWebAppInitialized(%s, %s)"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v10

    aput-object v2, v5, v11

    invoke-static {v3, v0, v4, v5}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 190
    :catch_1
    move-exception v0

    const-string v2, "GWAC"

    const-string v3, "handleRenderComplete(%s)"

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 193
    :cond_b
    :try_start_7
    iget-object v0, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/search/core/state/QueryState;->e(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    const-string v0, "GWAC"

    const-string v2, "Can\'t construct the query object from directUrl."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v0

    const-string v2, "GWAC"

    const-string v3, "handleRenderFailed(%s)"

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 196
    :cond_c
    :try_start_8
    const-string v0, "GWAC"

    const-string v1, "Can\'t construct the query object from directUrl: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_3

    :catch_3
    move-exception v0

    const-string v1, "GWAC"

    const-string v2, "handleSubstateCreated(%s)"

    new-array v3, v11, [Ljava/lang/Object;

    aput-object v6, v3, v10

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 199
    :cond_d
    :try_start_9
    const-string v0, "GWAC"

    const-string v1, "Can\'t construct the query object from directUrl: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v1, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_4

    :catch_4
    move-exception v0

    const-string v1, "GWAC"

    const-string v2, "handleSubstateReplaced(%s)"

    new-array v3, v11, [Ljava/lang/Object;

    aput-object v6, v3, v10

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 202
    :cond_e
    :try_start_a
    const-string v3, "GWAC"

    const-string v4, "Can\'t pop the query: directurl,substate: %s, %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_5

    :catch_5
    move-exception v0

    const-string v2, "GWAC"

    const-string v3, "handleSubstatePopped(%s)"

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 208
    :catch_6
    move-exception v0

    const-string v2, "GWAC"

    const-string v3, "handleUpdateQueryAndRequestContent(%s)"

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v1, v4, v10

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_6
.end method

.method public final dF(Z)V
    .locals 3

    .prologue
    .line 557
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 559
    :try_start_0
    const-string v1, "vis"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 560
    const-string v1, "chgvis"

    invoke-virtual {p0, v1, v0}, Ldmz;->a(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 565
    :goto_0
    return-void

    .line 561
    :catch_0
    move-exception v0

    .line 562
    const-string v1, "GWAC"

    const-string v2, "Exception creating JSON object for WebApp message - changeWebViewVisibility."

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final f(Lorg/json/JSONObject;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 264
    :try_start_0
    const-string v0, "du"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ldmz;->jS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 265
    const-string v0, "ts"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 266
    iget-object v0, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v0}, Lddk;->aaK()J

    move-result-wide v4

    .line 267
    const-string v0, "rid"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 268
    const-string v6, "ru"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Ldmz;->jS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 270
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v4, v5}, Lehm;->aJ(J)Lehm;

    move-result-object v0

    .line 273
    :goto_0
    iget-object v4, p0, Ldmz;->mUrlHelper:Lcpn;

    invoke-virtual {v4, v1, v2, v3, v0}, Lcpn;->a(Ljava/lang/String;JLehm;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_1

    .line 276
    iget-object v1, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v2, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v2}, Lddk;->aaS()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/search/core/state/QueryState;->h(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    .line 286
    :goto_1
    return-void

    .line 270
    :cond_0
    invoke-static {v0, v6, v4, v5}, Lehm;->e(Ljava/lang/String;Ljava/lang/String;J)Lehm;

    move-result-object v0

    goto :goto_0

    .line 278
    :cond_1
    const/16 v0, 0x178

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 281
    const-string v0, "GWAC"

    const-string v1, "Can\'t construct the query object from directUrl."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 283
    :catch_0
    move-exception v0

    .line 284
    const-string v1, "GWAC"

    const-string v2, "handleNewQuery(%s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v8

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final g(Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 313
    :try_start_0
    const-string v0, "gci"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 314
    iget-object v1, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v1}, Lddk;->aaK()J

    move-result-wide v2

    invoke-static {v2, v3}, Lehm;->aJ(J)Lehm;

    move-result-object v1

    .line 316
    invoke-direct {p0, v0, v1}, Ldmz;->a(Ljava/lang/String;Lehm;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :goto_0
    return-void

    .line 317
    :catch_0
    move-exception v0

    .line 318
    const-string v1, "GWAC"

    const-string v2, "handleUpdateQuery(%s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final jT(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 466
    iget-object v1, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v1}, Lddk;->aaR()Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 467
    iget-object v1, p0, Ldmz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v2}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v1

    invoke-virtual {v1}, Lehm;->asM()J

    move-result-wide v4

    iget-object v1, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v1}, Lddk;->aaK()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v0

    .line 480
    :cond_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 481
    const-string v1, "rndr"

    .line 483
    :try_start_0
    const-string v4, "gci"

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    invoke-static {v6, v7}, Lesp;->bb(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 484
    iget-object v4, p0, Ldmz;->mUrlHelper:Lcpn;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/google/android/shared/search/Query;->a(Lehm;)Lcom/google/android/shared/search/Query;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcpn;->j(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 486
    const-string v5, "du"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 487
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 488
    const-string v4, "ss"

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 498
    :cond_2
    :goto_1
    const/16 v4, 0x172

    invoke-static {v4}, Lege;->hs(I)Litu;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v4

    invoke-static {v4}, Lege;->a(Litu;)V

    .line 501
    invoke-virtual {p0, v1, v3}, Ldmz;->a(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 502
    iget-object v1, p0, Ldmz;->mWebAppState:Lddk;

    invoke-virtual {v1, v2}, Lddk;->ay(Lcom/google/android/shared/search/Query;)Z

    .line 503
    const/4 v0, 0x1

    goto :goto_0

    .line 490
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v4

    .line 491
    invoke-static {v4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v5

    invoke-virtual {v5}, Lehm;->asN()Z

    move-result v5

    if-nez v5, :cond_2

    .line 493
    const-string v1, "fcrndr"

    .line 494
    const-string v5, "rid"

    invoke-virtual {v4}, Lehm;->zK()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 495
    const-string v4, "ru"

    invoke-virtual {v3, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 507
    :catch_0
    move-exception v1

    .line 508
    const-string v2, "GWAC"

    const-string v3, "loadSearchResults:Exception creating JSON object for WebApp message"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 505
    :cond_4
    :try_start_1
    const-string v1, "GWAC"

    const-string v2, "loadSearchResults:Failed to send WebApp message"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
