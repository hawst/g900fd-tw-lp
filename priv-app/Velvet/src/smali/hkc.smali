.class final Lhkc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldvn;


# instance fields
.field private synthetic diS:Lhkb;

.field private final mCardController:Ledr;


# direct methods
.method constructor <init>(Lhkb;Ledr;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lhkc;->diS:Lhkb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p2, p0, Lhkc;->mCardController:Ledr;

    .line 112
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 105
    new-instance v0, Lhop;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    new-instance v2, Ldxd;

    iget-object v3, p0, Lhkc;->diS:Lhkb;

    iget-object v3, v3, Lhkb;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lhkc;->diS:Lhkb;

    iget-object v4, v4, Lhkb;->mResources:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v4}, Ldxd;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iget-object v3, p0, Lhkc;->diS:Lhkb;

    iget-object v3, v3, Lhkb;->aoG:Lhla;

    invoke-direct {v0, v1, v2, v3}, Lhop;-><init>(Ledr;Ldxd;Lhla;)V

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lhkc;->diS:Lhkb;

    iget-object v0, v0, Lhkb;->dgQ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjv;

    iget-object v1, p0, Lhkc;->mCardController:Ledr;

    invoke-interface {v0, p1, v1}, Lhjv;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v0

    return-object v0
.end method
