.class public final Ljhr;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eme:[Ljhr;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public ecf:[I

.field public emf:[Ljdj;

.field public emg:[Ljhr;

.field private emh:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5697
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5698
    iput v1, p0, Ljhr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljhr;->afh:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljhr;->ecf:[I

    invoke-static {}, Ljdj;->bhp()[Ljdj;

    move-result-object v0

    iput-object v0, p0, Ljhr;->emf:[Ljdj;

    invoke-static {}, Ljhr;->bmd()[Ljhr;

    move-result-object v0

    iput-object v0, p0, Ljhr;->emg:[Ljhr;

    iput v1, p0, Ljhr;->emh:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljhr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhr;->eCz:I

    .line 5699
    return-void
.end method

.method public static bmd()[Ljhr;
    .locals 2

    .prologue
    .line 5634
    sget-object v0, Ljhr;->eme:[Ljhr;

    if-nez v0, :cond_1

    .line 5635
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 5637
    :try_start_0
    sget-object v0, Ljhr;->eme:[Ljhr;

    if-nez v0, :cond_0

    .line 5638
    const/4 v0, 0x0

    new-array v0, v0, [Ljhr;

    sput-object v0, Ljhr;->eme:[Ljhr;

    .line 5640
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5642
    :cond_1
    sget-object v0, Ljhr;->eme:[Ljhr;

    return-object v0

    .line 5640
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 5619
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhr;->afh:Ljava/lang/String;

    iget v0, p0, Ljhr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhr;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljhr;->ecf:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljhr;->ecf:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljhr;->ecf:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljhr;->ecf:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljhr;->ecf:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Ljhr;->ecf:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljhr;->ecf:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljhr;->ecf:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljhr;->ecf:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljhr;->emf:[Ljdj;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljdj;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljhr;->emf:[Ljdj;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljdj;

    invoke-direct {v3}, Ljdj;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljhr;->emf:[Ljdj;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Ljdj;

    invoke-direct {v3}, Ljdj;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v1, p0, Ljhr;->emf:[Ljdj;

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    if-nez v0, :cond_f

    move v0, v2

    :goto_9
    add-int/2addr v1, v0

    new-array v1, v1, [Ljhr;

    if-eqz v0, :cond_e

    iget-object v3, p0, Ljhr;->emg:[Ljhr;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_a
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    new-instance v3, Ljhr;

    invoke-direct {v3}, Ljhr;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    array-length v0, v0

    goto :goto_9

    :cond_10
    new-instance v3, Ljhr;

    invoke-direct {v3}, Ljhr;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v1, p0, Ljhr;->emg:[Ljhr;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Ljhr;->emh:I

    iget v0, p0, Ljhr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhr;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5716
    iget v0, p0, Ljhr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5717
    const/4 v0, 0x1

    iget-object v2, p0, Ljhr;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 5719
    :cond_0
    iget-object v0, p0, Ljhr;->ecf:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljhr;->ecf:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 5720
    :goto_0
    iget-object v2, p0, Ljhr;->ecf:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 5721
    const/4 v2, 0x2

    iget-object v3, p0, Ljhr;->ecf:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 5720
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5724
    :cond_1
    iget-object v0, p0, Ljhr;->emf:[Ljdj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljhr;->emf:[Ljdj;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 5725
    :goto_1
    iget-object v2, p0, Ljhr;->emf:[Ljdj;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 5726
    iget-object v2, p0, Ljhr;->emf:[Ljdj;

    aget-object v2, v2, v0

    .line 5727
    if-eqz v2, :cond_2

    .line 5728
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 5725
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5732
    :cond_3
    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 5733
    :goto_2
    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 5734
    iget-object v0, p0, Ljhr;->emg:[Ljhr;

    aget-object v0, v0, v1

    .line 5735
    if-eqz v0, :cond_4

    .line 5736
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 5733
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5740
    :cond_5
    iget v0, p0, Ljhr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 5741
    const/4 v0, 0x6

    iget v1, p0, Ljhr;->emh:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5743
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5744
    return-void
.end method

.method public final bme()I
    .locals 1

    .prologue
    .line 5681
    iget v0, p0, Ljhr;->emh:I

    return v0
.end method

.method public final bmf()Z
    .locals 1

    .prologue
    .line 5689
    iget v0, p0, Ljhr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5650
    iget-object v0, p0, Ljhr;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 5748
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5749
    iget v1, p0, Ljhr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5750
    const/4 v1, 0x1

    iget-object v3, p0, Ljhr;->afh:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5753
    :cond_0
    iget-object v1, p0, Ljhr;->ecf:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljhr;->ecf:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 5755
    :goto_0
    iget-object v4, p0, Ljhr;->ecf:[I

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 5756
    iget-object v4, p0, Ljhr;->ecf:[I

    aget v4, v4, v1

    .line 5757
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 5755
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5760
    :cond_1
    add-int/2addr v0, v3

    .line 5761
    iget-object v1, p0, Ljhr;->ecf:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5763
    :cond_2
    iget-object v1, p0, Ljhr;->emf:[Ljdj;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljhr;->emf:[Ljdj;

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v0

    move v0, v2

    .line 5764
    :goto_1
    iget-object v3, p0, Ljhr;->emf:[Ljdj;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 5765
    iget-object v3, p0, Ljhr;->emf:[Ljdj;

    aget-object v3, v3, v0

    .line 5766
    if-eqz v3, :cond_3

    .line 5767
    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v1, v3

    .line 5764
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    .line 5772
    :cond_5
    iget-object v1, p0, Ljhr;->emg:[Ljhr;

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljhr;->emg:[Ljhr;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 5773
    :goto_2
    iget-object v1, p0, Ljhr;->emg:[Ljhr;

    array-length v1, v1

    if-ge v2, v1, :cond_7

    .line 5774
    iget-object v1, p0, Ljhr;->emg:[Ljhr;

    aget-object v1, v1, v2

    .line 5775
    if-eqz v1, :cond_6

    .line 5776
    const/4 v3, 0x4

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5773
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 5781
    :cond_7
    iget v1, p0, Ljhr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_8

    .line 5782
    const/4 v1, 0x6

    iget v2, p0, Ljhr;->emh:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5785
    :cond_8
    return v0
.end method
