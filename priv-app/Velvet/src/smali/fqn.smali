.class public final Lfqn;
.super Lbtf;
.source "PG"

# interfaces
.implements Lfom;


# instance fields
.field private czE:Landroid/view/ViewGroup;

.field private czF:Landroid/widget/LinearLayout;

.field private czG:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

.field private czH:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

.field private czI:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

.field private czJ:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

.field private czK:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

.field private czL:I

.field private czM:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

.field private czN:Landroid/view/View;

.field private mPresenter:Lfnx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-direct {p0, p1}, Lbtf;-><init>(Landroid/content/Context;)V

    .line 38
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0400d5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_0
    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object v0, p0, Lbtf;->aJl:Landroid/view/View;

    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    iget-object v0, p0, Lbtf;->aJl:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-static {}, Lbtf;->AK()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbtf;->aJl:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setNestedScrollingEnabled(Z)V

    invoke-virtual {p0, v3}, Lbtf;->setNestedScrollingEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110283

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lfqn;->czF:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110287

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfqn;->czN:Landroid/view/View;

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110052

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iput-object v0, p0, Lfqn;->czG:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iget-object v0, p0, Lfqn;->czG:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    new-instance v1, Lfqo;

    invoke-direct {v1, p0}, Lfqo;-><init>(Lfqn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110050

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iput-object v0, p0, Lfqn;->czH:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iget-object v0, p0, Lfqn;->czH:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    new-instance v1, Lfqp;

    invoke-direct {v1, p0}, Lfqp;-><init>(Lfqn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110284

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iput-object v0, p0, Lfqn;->czI:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iget-object v0, p0, Lfqn;->czI:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    new-instance v1, Lfqq;

    invoke-direct {v1, p0}, Lfqq;-><init>(Lfqn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110285

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iput-object v0, p0, Lfqn;->czJ:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iget-object v0, p0, Lfqn;->czJ:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    new-instance v1, Lfqr;

    invoke-direct {v1, p0}, Lfqr;-><init>(Lfqn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    const v1, 0x7f110286

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iput-object v0, p0, Lfqn;->czK:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iget-object v0, p0, Lfqn;->czK:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    new-instance v1, Lfqs;

    invoke-direct {v1, p0}, Lfqs;-><init>(Lfqn;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, -0x1

    iput v0, p0, Lfqn;->czL:I

    iget v0, p0, Lfqn;->czL:I

    invoke-virtual {p0, v0}, Lfqn;->jn(I)V

    .line 39
    return-void
.end method

.method static synthetic a(Lfqn;)Lfnx;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->mPresenter:Lfnx;

    return-object v0
.end method

.method static synthetic b(Lfqn;)Lcom/google/android/sidekick/shared/ui/DrawerEntry;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->czG:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    return-object v0
.end method

.method static synthetic c(Lfqn;)Lcom/google/android/sidekick/shared/ui/DrawerEntry;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->czM:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    return-object v0
.end method

.method static synthetic d(Lfqn;)Lcom/google/android/sidekick/shared/ui/DrawerEntry;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->czH:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    return-object v0
.end method

.method static synthetic e(Lfqn;)Lcom/google/android/sidekick/shared/ui/DrawerEntry;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->czI:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    return-object v0
.end method

.method static synthetic f(Lfqn;)Lcom/google/android/sidekick/shared/ui/DrawerEntry;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->czJ:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    return-object v0
.end method

.method static synthetic g(Lfqn;)Lcom/google/android/sidekick/shared/ui/DrawerEntry;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lfqn;->czK:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Lfok;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lfqn;->czN:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    new-instance v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    invoke-virtual {p0}, Lfqn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;-><init>(Landroid/content/Context;)V

    .line 207
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 209
    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setText(Ljava/lang/CharSequence;)V

    .line 211
    new-instance v1, Lfqt;

    invoke-direct {v1, p0, p2, v0}, Lfqt;-><init>(Lfqn;Lfok;Lcom/google/android/sidekick/shared/ui/DrawerEntry;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v1, p0, Lfqn;->czF:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 220
    return-void
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 224
    iget-object v0, p0, Lfqn;->czE:Landroid/view/ViewGroup;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    const/4 v2, 0x0

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 225
    return-void
.end method

.method public final ft(Z)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lfqn;->czG:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setEnabled(Z)V

    .line 156
    return-void
.end method

.method public final fu(Z)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lfqn;->czH:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setEnabled(Z)V

    .line 161
    return-void
.end method

.method public final g(Lfnx;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lfqn;->mPresenter:Lfnx;

    .line 136
    return-void
.end method

.method public final iY(I)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lfqn;->czI:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setVisibility(I)V

    .line 141
    return-void
.end method

.method public final jl(I)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lfqn;->czG:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setVisibility(I)V

    .line 146
    return-void
.end method

.method public final jm(I)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lfqn;->czH:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setVisibility(I)V

    .line 151
    return-void
.end method

.method public final jn(I)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 170
    iput p1, p0, Lfqn;->czL:I

    .line 172
    const/4 v0, -0x1

    .line 176
    packed-switch p1, :pswitch_data_0

    move v2, v4

    move v5, v0

    .line 188
    :goto_0
    iget-object v0, p0, Lfqn;->czF:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v7

    move v6, v4

    .line 189
    :goto_1
    if-ge v6, v7, :cond_1

    .line 190
    iget-object v0, p0, Lfqn;->czF:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 192
    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 194
    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v5, :cond_0

    move-object v0, v1

    .line 195
    check-cast v0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    iput-object v0, p0, Lfqn;->czM:Lcom/google/android/sidekick/shared/ui/DrawerEntry;

    .line 196
    invoke-virtual {v1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 189
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 178
    :pswitch_0
    const v0, 0x7f110052

    move v2, v3

    move v5, v0

    .line 179
    goto :goto_0

    .line 181
    :pswitch_1
    const v0, 0x7f110050

    move v2, v3

    move v5, v0

    .line 182
    goto :goto_0

    .line 199
    :cond_1
    return-void

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
