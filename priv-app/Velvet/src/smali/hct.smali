.class public final Lhct;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;Lerk;)V
    .locals 2

    .prologue
    .line 230
    iput-object p1, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    .line 231
    const-string v0, "LoadAccount"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, p2, v1}, Lenp;-><init>(Ljava/lang/String;Lerk;[I)V

    .line 232
    return-void

    .line 231
    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 229
    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    iget-object v0, v0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    iget-object v1, v1, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccountConfigurationHelper:Lhcb;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/accounts/Account;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lhcb;->b([Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 229
    check-cast p1, Lhcc;

    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    invoke-static {v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->b(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;)Lhct;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v0, p1, Lhcc;->aPI:Z

    if-nez v0, :cond_5

    :cond_2
    if-nez p1, :cond_4

    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->kL(I)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->kO(I)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p1, Lhcc;->aPI:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->kL(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    iget-object v1, p1, Lhcc;->mAccount:Landroid/accounts/Account;

    iput-object v1, v0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    iget-object v1, p0, Lhct;->dcS:Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;

    iget-object v1, v1, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->L(Landroid/accounts/Account;)V

    goto :goto_0
.end method
