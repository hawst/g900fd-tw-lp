.class final Lgrz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldvn;


# instance fields
.field private final baC:I

.field private final cRX:I

.field private final cUH:Z

.field private final cUI:Z

.field private final cUJ:Z

.field private final cUK:I

.field private final cUL:Z

.field private final cUM:I

.field private synthetic cUN:Lgrx;

.field private final mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mQuery:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lgrx;ZIIZLcom/google/android/shared/search/Query;Lcom/google/android/search/shared/service/ClientConfig;JIZ)V
    .locals 2
    .param p6    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/search/shared/service/ClientConfig;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 455
    iput-object p1, p0, Lgrz;->cUN:Lgrx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    iput-boolean p2, p0, Lgrz;->cUH:Z

    .line 458
    iput p3, p0, Lgrz;->baC:I

    .line 459
    iput p4, p0, Lgrz;->cRX:I

    .line 460
    iput-object p6, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    .line 461
    invoke-static {p1}, Lgrx;->a(Lgrx;)Lhhb;

    move-result-object v0

    invoke-virtual {v0, p6}, Lhhb;->ba(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    iput-boolean v0, p0, Lgrz;->cUJ:Z

    .line 462
    iput-boolean p5, p0, Lgrz;->cUI:Z

    .line 463
    iput-object p7, p0, Lgrz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    .line 464
    long-to-int v0, p8

    iput v0, p0, Lgrz;->cUK:I

    .line 465
    iput p10, p0, Lgrz;->cUM:I

    .line 466
    iput-boolean p11, p0, Lgrz;->cUL:Z

    .line 467
    return-void
.end method

.method private static B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 4

    .prologue
    .line 1561
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldya;->bQq:Z

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    .line 1564
    if-eqz p1, :cond_0

    .line 1565
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ldya;->aB(J)Ldya;

    .line 1569
    :goto_0
    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0

    .line 1567
    :cond_0
    invoke-virtual {v0}, Ldya;->alr()Ldya;

    goto :goto_0
.end method

.method private a(Lcom/google/android/search/shared/actions/AbstractRelationshipAction;II)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1468
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 1470
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1475
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Relationship;->ams()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Relationship;->amr()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    aput-object v0, v3, v5

    invoke-virtual {v1, p3, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1480
    :goto_0
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    .line 1492
    :goto_1
    return-object v0

    .line 1475
    :cond_0
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    aput-object v0, v3, v5

    invoke-virtual {v1, p2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1481
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1486
    sget-object v1, Lgqu;->cTl:Lgrl;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lgrz;->ap(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->g(Lgrx;)Lchk;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lgrl;->a(Ljava/lang/String;Ljava/util/List;Lchk;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_1

    .line 1491
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 1492
    invoke-static {}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alo()Ldya;

    move-result-object v0

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/google/android/search/shared/actions/CommunicationAction;Ldzb;Lgra;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1309
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 1311
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1312
    invoke-direct {p0, v0, p2, p3}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Lgra;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lgrz;->a(Lgtl;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    .line 1338
    :goto_0
    return-object v0

    .line 1315
    :cond_0
    sget-object v1, Ldzb;->bRq:Ldzb;

    if-ne p2, v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isInteractive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1317
    invoke-static {}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alo()Ldya;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->f(Lgrx;)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lgrz;->a(Ldya;J)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    .line 1318
    :cond_1
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1319
    sget-object v1, Ldzb;->bRq:Ldzb;

    if-eq p2, v1, :cond_2

    sget-object v1, Ldzb;->bRq:Ldzb;

    if-ne p2, v1, :cond_3

    .line 1321
    :cond_2
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05f4

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/search/shared/actions/utils/CardDecision;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    iput-boolean v5, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    .line 1327
    :cond_3
    sget-object v1, Ldzb;->bRp:Ldzb;

    if-ne p2, v1, :cond_4

    .line 1328
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05f7

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/search/shared/actions/utils/CardDecision;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    iput-boolean v5, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    .line 1335
    :cond_4
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_0

    .line 1338
    :cond_5
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/actions/CommunicationAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 2

    .prologue
    .line 581
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 583
    const/16 v0, 0x9a

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/CommunicationAction;->agc()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 586
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v1

    .line 589
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Relationship;->amr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    sget-object v0, Lgqu;->cTB:Lgrn;

    .line 595
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Relationship;->ams()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgrn;->nx(Ljava/lang/String;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    .line 598
    :goto_1
    return-object v0

    .line 592
    :cond_0
    sget-object v0, Lgqu;->cTA:Lgrn;

    goto :goto_0

    .line 598
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ldya;J)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 2

    .prologue
    .line 1516
    invoke-direct {p0}, Lgrz;->aKh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517
    invoke-virtual {p1, p2, p3}, Ldya;->aB(J)Ldya;

    .line 1519
    :cond_0
    invoke-virtual {p1}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method private a(Ldya;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 1

    .prologue
    .line 1503
    if-eqz p2, :cond_1

    .line 1504
    invoke-virtual {p1}, Ldya;->alq()Ldya;

    .line 1508
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p1, Ldya;->bQq:Z

    invoke-virtual {p1}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0

    .line 1505
    :cond_1
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1506
    invoke-virtual {p1}, Ldya;->alr()Ldya;

    goto :goto_0
.end method

.method static synthetic a(Lgrz;Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method private a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 1

    .prologue
    .line 1406
    iget-boolean v0, p0, Lgrz;->cUJ:Z

    invoke-direct {p0, p1, v0}, Lgrz;->a(Lgtl;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method private a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 3

    .prologue
    .line 1450
    invoke-direct {p0, p1}, Lgrz;->b(Lgtl;)Lgqr;

    move-result-object v1

    .line 1451
    if-nez v1, :cond_0

    .line 1453
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 1463
    :goto_0
    return-object v0

    .line 1456
    :cond_0
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1457
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    invoke-virtual {v1}, Lgqr;->Bb()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    .line 1463
    :goto_1
    iget-boolean v1, v1, Lgqr;->cTg:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-direct {p0, v0, v1}, Lgrz;->a(Ldya;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    .line 1460
    :cond_1
    invoke-virtual {v1}, Lgqr;->aKc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lgqr;->Bb()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    goto :goto_1

    .line 1463
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private a(Lgtl;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1414
    if-nez p2, :cond_1

    .line 1415
    invoke-direct {p0, p1}, Lgrz;->b(Lgtl;)Lgqr;

    move-result-object v0

    .line 1416
    if-nez v0, :cond_0

    .line 1418
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 1425
    :goto_0
    return-object v0

    .line 1420
    :cond_0
    invoke-virtual {v0}, Lgqr;->aKc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    .line 1425
    :cond_1
    invoke-direct {p0, p1, v1}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Lgra;)Lgtl;
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1362
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 1363
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1365
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1366
    iget-object v0, p3, Lgra;->cTY:Lgrl;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lgrz;->ap(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->g(Lgrx;)Lchk;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lgrl;->a(Ljava/lang/String;Ljava/util/List;Lchk;)Lgtl;

    move-result-object v0

    .line 1396
    :goto_0
    return-object v0

    .line 1371
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v2

    .line 1372
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1374
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1375
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 1377
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 1378
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->rn()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1379
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 1380
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1381
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1372
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1386
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 1387
    if-gt v0, v1, :cond_5

    .line 1388
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 1390
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 1391
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1393
    :cond_4
    iget-object v0, p3, Lgra;->cUa:Lgrc;

    iget-object v2, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v2}, Lgrx;->g(Lgrx;)Lchk;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgrc;->b(Ljava/util/List;Lchk;)Lgtl;

    move-result-object v0

    goto :goto_0

    .line 1396
    :cond_5
    iget-object v0, p3, Lgra;->cTZ:Lgre;

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->g(Lgrx;)Lchk;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lgre;->b(Ljava/util/List;Lchk;)Lgtl;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static a(Lgqy;Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Ldzb;)Lgtl;
    .locals 5

    .prologue
    .line 1260
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 1261
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1263
    if-nez v1, :cond_1

    .line 1264
    const/4 v2, 0x0

    .line 1265
    sget-object v3, Lgry;->bRb:[I

    invoke-virtual {p3}, Ldzb;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    move-object v0, v2

    .line 1271
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1279
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v0

    .line 1280
    sget-object v1, Ldzb;->bRq:Ldzb;

    if-ne p3, v1, :cond_0

    .line 1281
    invoke-static {v0}, Lerr;->lo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1290
    :cond_0
    :goto_1
    invoke-virtual {p0, v0, p2}, Lgqy;->aS(Ljava/lang/String;Ljava/lang/String;)Lgtl;

    move-result-object v0

    return-object v0

    .line 1267
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1270
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 1265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aKh()Z
    .locals 1

    .prologue
    .line 1524
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqw()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static ap(Ljava/util/List;)Ljava/util/List;
    .locals 3

    .prologue
    .line 1346
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 1347
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 1348
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1350
    :cond_0
    return-object v1
.end method

.method private b(Lgtl;)Lgqr;
    .locals 6

    .prologue
    .line 1532
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v2

    iget v3, p0, Lgrz;->baC:I

    iget v4, p0, Lgrz;->cRX:I

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lgqu;->a(Landroid/content/res/Resources;Landroid/content/res/Resources;ZIILgtl;)Lgqr;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 2

    .prologue
    .line 1544
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    .line 1545
    iget-object v1, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1546
    :cond_0
    const/4 v0, 0x0

    .line 1552
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0
.end method

.method private p(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 3

    .prologue
    .line 552
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    .line 556
    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Relationship;->amr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 557
    sget-object v0, Lgqu;->cTD:Lgqw;

    move-object v1, v0

    .line 561
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Relationship;->ams()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lgqw;->aS(Ljava/lang/String;Ljava/lang/String;)Lgtl;

    move-result-object v0

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    .line 566
    :goto_1
    return-object v0

    .line 559
    :cond_0
    sget-object v0, Lgqu;->cTC:Lgqw;

    move-object v1, v0

    goto :goto_0

    .line 566
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 428
    invoke-direct {p0, p1, v0, v0}, Lgrz;->a(Lcom/google/android/search/shared/actions/AbstractRelationshipAction;II)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->ahd()Ljnm;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->ahb()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a067a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CardDecisionFactory"

    const-string v2, "Unexpected empty TTS string from AgendaAction"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    new-instance v1, Ldya;

    invoke-direct {v1}, Ldya;-><init>()V

    invoke-virtual {v1, v0, v4}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    iput-boolean v5, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->ahb()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->c(Lgrx;)Lgru;

    move-result-object v1

    iget-object v2, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lgru;->a(Ljnm;Z)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agS()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agV()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v5, :cond_4

    :cond_3
    invoke-virtual {v0}, Ljnm;->bqK()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/AgendaAction;->agX()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->c(Lgrx;)Lgru;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->d(Lgrx;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lgru;->a(Lcom/google/android/search/shared/actions/AgendaAction;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Ljnm;->bqL()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 428
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a08d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x4

    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-direct {p0, v1}, Lgrz;->p(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, v1, p1}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/actions/CommunicationAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgrz;->cUJ:Z

    if-nez v0, :cond_2

    sget-object v0, Ldzb;->bRp:Ldzb;

    sget-object v1, Lgqu;->cTk:Lgra;

    invoke-direct {p0, p1, v0, v1}, Lgrz;->a(Lcom/google/android/search/shared/actions/CommunicationAction;Ldzb;Lgra;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    sget-object v0, Lgqu;->cTn:Lgtl;

    invoke-direct {p0, v0, v2}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v2, Lgqu;->cTF:Lgrj;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgrj;->nx(Ljava/lang/String;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lgqu;->cTn:Lgtl;

    invoke-direct {p0, v0, v2}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Ldzb;->bRp:Ldzb;

    sget-object v2, Lgqu;->cTk:Lgra;

    invoke-direct {p0, v1, v0, v2}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Lgra;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lgrz;->cRX:I

    if-lez v0, :cond_7

    sget-object v0, Lgqu;->cTr:Lgtl;

    invoke-direct {p0, v0, v3}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a061e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a061f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lgrz;->a(Ldya;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    iget-boolean v0, p0, Lgrz;->cUH:Z

    if-nez v0, :cond_b

    iget v0, p0, Lgrz;->cUM:I

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "CardDecisionFactory"

    const-string v1, "Multi-modal edit message prompts are not implemented"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lgqu;->cTw:Lgrb;

    invoke-direct {p0, v0, v3}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    sget-object v0, Lgqu;->cTy:Lgqy;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getBody()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ldzb;->bRp:Ldzb;

    invoke-static {v0, v1, v2, v3}, Lgrz;->a(Lgqy;Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Ldzb;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0620

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0621

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    iput-boolean v4, v0, Ldya;->bQq:Z

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lgrz;->a(Ldya;J)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahw()Ljpe;

    move-result-object v1

    iget-object v2, v1, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    if-ne v2, v0, :cond_1

    invoke-virtual {v1}, Ljpe;->oY()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :goto_0
    if-eqz v0, :cond_3

    const v0, 0x7f0a0611

    invoke-direct {p0, p1, v0}, Lgrz;->c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->canExecute()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_1

    :cond_3
    const v0, 0x7f0a0612

    invoke-direct {p0, p1, v0}, Lgrz;->c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_1
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 428
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/MessageSearchAction;->ahD()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldya;->bQq:Z

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/MessageSearchAction;->ahE()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldya;->alq()Ldya;

    :cond_0
    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
    .locals 9

    .prologue
    const v8, 0x7f0a05fe

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 428
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->e(Lgrx;)Lgno;

    invoke-static {}, Lgno;->aHY()I

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PhoneCallAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v2

    invoke-direct {p0, v2}, Lgrz;->p(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-direct {p0, v2, p1}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/actions/CommunicationAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lgrz;->cUJ:Z

    if-nez v0, :cond_3

    sget-object v0, Ldzb;->bRq:Ldzb;

    sget-object v1, Lgqu;->cTj:Lgra;

    invoke-direct {p0, p1, v0, v1}, Lgrz;->a(Lcom/google/android/search/shared/actions/CommunicationAction;Ldzb;Lgra;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v1, Lgqu;->cTE:Lgrj;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgrj;->nx(Ljava/lang/String;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    sget-object v0, Lgqu;->cTp:Lgtl;

    invoke-direct {p0, v0, v5}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_6
    sget-object v0, Ldzb;->bRq:Ldzb;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-nez v1, :cond_7

    sget-object v1, Lgqu;->cTj:Lgra;

    invoke-direct {p0, v2, v0, v1}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Lgra;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_7
    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a05ff

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const v1, 0x7f0a0611

    invoke-direct {p0, p1, v1}, Lgrz;->c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    if-eqz v1, :cond_9

    move-object v0, v1

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0600

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lerr;->lo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_9
    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isInteractive()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/search/shared/actions/utils/CardDecision;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    :goto_2
    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isInteractive()Z

    move-result v1

    if-nez v1, :cond_a

    sget-object v1, Lcgg;->aVk:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_a
    const-wide/16 v2, 0x0

    :goto_3
    iput-boolean v7, v0, Ldya;->bQq:Z

    invoke-direct {p0, v0, v2, v3}, Lgrz;->a(Ldya;J)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, v6}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    goto :goto_2

    :cond_c
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->f(Lgrx;)J

    move-result-wide v2

    goto :goto_3
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 9

    .prologue
    const v5, 0x7f0a060e

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    invoke-virtual {v0}, Ljmh;->bpU()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahW()Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahP()Lcom/google/android/shared/util/App;

    move-result-object v4

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    iget-object v4, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahZ()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahT()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0610

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahU()Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v4

    iget-object v4, v4, Ljmh;->etA:Ljml;

    if-eqz v4, :cond_e

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    iget-object v6, v0, Ljmh;->etA:Ljml;

    invoke-virtual {v6}, Ljml;->bpZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v3

    :goto_2
    invoke-virtual {v6}, Ljml;->aVp()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v4, v3

    :goto_3
    invoke-virtual {v6}, Ljml;->aVj()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    move-object v5, v3

    :goto_4
    invoke-virtual {v6}, Ljml;->te()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    :goto_5
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahW()Lcom/google/android/shared/util/App;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/shared/util/App;->getLabel()Ljava/lang/String;

    move-result-object v6

    if-nez v0, :cond_3

    if-eqz v4, :cond_a

    :cond_3
    if-eqz v0, :cond_8

    :goto_6
    if-eqz v5, :cond_9

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0618

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v2

    aput-object v5, v7, v1

    aput-object v6, v7, v8

    invoke-virtual {v3, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-static {v0, v1}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {v6}, Ljml;->bpZ()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    invoke-virtual {v6}, Ljml;->aVp()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, Ljml;->aVj()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    :cond_7
    invoke-virtual {v6}, Ljml;->te()Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    :cond_8
    move-object v0, v4

    goto :goto_6

    :cond_9
    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0617

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v2

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_a
    if-nez v5, :cond_b

    if-eqz v3, :cond_d

    :cond_b
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0a0617

    new-array v7, v8, [Ljava/lang/Object;

    if-eqz v5, :cond_c

    :goto_8
    aput-object v5, v7, v2

    aput-object v6, v7, v1

    invoke-virtual {v0, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_c
    move-object v5, v3

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0616

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v6, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_e
    if-nez v0, :cond_13

    :cond_f
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aia()Z

    move-result v0

    if-nez v0, :cond_10

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_1

    :cond_10
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->g(Lgrx;)Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->HA()Z

    move-result v0

    if-eqz v0, :cond_12

    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    iget-object v2, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v2}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0856

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Ldya;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ldya;->aB(J)Ldya;

    move-result-object v0

    iget-object v2, p0, Lgrz;->cUN:Lgrx;

    iget-object v3, p0, Lgrz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v2, v3}, Lgrx;->b(Lcom/google/android/search/shared/service/ClientConfig;)Z

    move-result v2

    if-eqz v2, :cond_11

    iput-boolean v1, v0, Ldya;->bQl:Z

    :cond_11
    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_1

    :cond_12
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    iget-object v2, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v2}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->f(Lgrx;)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lgrz;->a(Ldya;J)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_1

    :cond_13
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_1
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 428
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PuntAction;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "COMMUNICATION"

    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldya;->bQq:Z

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PuntAction;->aig()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 428
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ReadNotificationAction;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 428
    const v0, 0x7f0a056e

    const v1, 0x7f0a056f

    invoke-direct {p0, p1, v0, v1}, Lgrz;->a(Lcom/google/android/search/shared/actions/AbstractRelationshipAction;II)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 428
    const v0, 0x7f0a0615

    invoke-direct {p0, p1, v0}, Lgrz;->c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v1, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->aik()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a061b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->avf()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->avg()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "com.google.android.gm"

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "Gmail"

    :cond_2
    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0619

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->aik()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 428
    const v0, 0x7f0a0613

    invoke-direct {p0, p1, v0}, Lgrz;->c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetAlarmAction;->sZ()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a061c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 428
    iget v1, p0, Lgrz;->cRX:I

    if-lez v1, :cond_1

    iget v1, p0, Lgrz;->cUM:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    iget v0, p0, Lgrz;->cUM:I

    invoke-direct {p0, v1, v0}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :pswitch_1
    sget-object v1, Lgqu;->cTt:Lgtl;

    goto :goto_0

    :pswitch_2
    sget-object v1, Lgqu;->cTu:Lgtl;

    goto :goto_0

    :pswitch_3
    sget-object v1, Lgqu;->cTv:Lgtl;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 428
    const v0, 0x7f0a0614

    invoke-direct {p0, p1, v0}, Lgrz;->c(Lcom/google/android/search/shared/actions/VoiceAction;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetTimerAction;->aiD()I

    move-result v1

    if-gtz v1, :cond_1

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a061d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgrz;->B(Ljava/lang/String;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-direct {p0, v1}, Lgrz;->p(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, v1, p1}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/actions/CommunicationAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgrz;->cUJ:Z

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lgqu;->cTm:Lgrl;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lgrz;->ap(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->g(Lgrx;)Lchk;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Lgrl;->a(Ljava/lang/String;Ljava/util/List;Lchk;)Lgtl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgrz;->a(Lgtl;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    sget-object v0, Lgqu;->cTq:Lgtl;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Ldzb;->bRt:Ldzb;

    sget-object v2, Lgqu;->cTj:Lgra;

    invoke-direct {p0, v1, v0, v2}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Lgra;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x4

    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-direct {p0, v1}, Lgrz;->p(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, v1, p1}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/actions/CommunicationAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgrz;->cUJ:Z

    if-nez v0, :cond_2

    sget-object v0, Ldzb;->bRq:Ldzb;

    sget-object v1, Lgqu;->cTi:Lgra;

    invoke-direct {p0, p1, v0, v1}, Lgrz;->a(Lcom/google/android/search/shared/actions/CommunicationAction;Ldzb;Lgra;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    sget-object v0, Lgqu;->cTo:Lgtl;

    invoke-direct {p0, v0, v2}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v2, Lgqu;->cTE:Lgrj;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgrj;->nx(Ljava/lang/String;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lgqu;->cTo:Lgtl;

    invoke-direct {p0, v0, v2}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Ldzb;->bRq:Ldzb;

    sget-object v2, Lgqu;->cTi:Lgra;

    invoke-direct {p0, v1, v0, v2}, Lgrz;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ldzb;Lgra;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0}, Lgrz;->a(Lgtl;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->aiL()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lgrz;->cUH:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a060a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a060b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    iput-boolean v4, v0, Ldya;->bQq:Z

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lgrz;->a(Ldya;J)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    iget v0, p0, Lgrz;->cUM:I

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "CardDecisionFactory"

    const-string v1, "Multi-modal edit message prompts are not implemented"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lgqu;->cTx:Lgrb;

    invoke-direct {p0, v0, v3}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lgqu;->cTz:Lgqy;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->getBody()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ldzb;->bRq:Ldzb;

    invoke-static {v0, v1, v2, v3}, Lgrz;->a(Lgqy;Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Ldzb;)Lgtl;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    iget v0, p0, Lgrz;->cRX:I

    if-lez v0, :cond_b

    sget-object v0, Lgqu;->cTs:Lgtl;

    invoke-direct {p0, v0, v3}, Lgrz;->a(Lgtl;I)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0602

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v1}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0609

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->b(Ljava/lang/String;Ljava/lang/String;I)Ldya;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lgrz;->a(Ldya;Z)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SocialUpdateAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 428
    iget-boolean v0, p0, Lgrz;->cUI:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agz()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lgrz;->cUH:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajf()Ljqs;

    move-result-object v1

    invoke-static {v0, v1}, Ldvy;->a(Lcom/google/android/shared/util/MatchingAppInfo;Ljqs;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lgrz;->cUI:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agz()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgrz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    :cond_2
    new-instance v1, Ldvy;

    new-instance v2, Ldxd;

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->d(Lgrx;)Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v3}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ldxd;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iget-object v0, p0, Lgrz;->cUN:Lgrx;

    invoke-static {v0}, Lgrx;->b(Lgrx;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {p0}, Lgrz;->aKh()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lgrz;->cUL:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iget-object v4, p0, Lgrz;->cUN:Lgrx;

    iget-object v5, p0, Lgrz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v4, v5}, Lgrx;->b(Lcom/google/android/search/shared/service/ClientConfig;)Z

    move-result v4

    invoke-direct {v1, v2, v3, v0, v4}, Ldvy;-><init>(Ldxd;Landroid/content/res/Resources;ZZ)V

    iget-boolean v0, p0, Lgrz;->cUH:Z

    iget v2, p0, Lgrz;->baC:I

    iget v3, p0, Lgrz;->cUK:I

    invoke-virtual {v1, p1, v0, v2, v3}, Ldvy;->a(Ldvu;ZII)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    return-object v0
.end method
