.class public final Lgmk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private OF:Z

.field private final cPk:Ljava/util/List;


# direct methods
.method public varargs constructor <init>([Lceq;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgmk;->cPk:Ljava/util/List;

    .line 30
    return-void
.end method


# virtual methods
.method public final aHo()Ljwv;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lgmk;->cPk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 44
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceq;

    .line 46
    invoke-interface {v0}, Lceq;->yo()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    invoke-interface {v0}, Lceq;->Dr()Ljwv;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_1

    move-object v0, v1

    .line 66
    :goto_1
    return-object v0

    .line 55
    :cond_1
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 56
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 61
    :cond_2
    iget-boolean v0, p0, Lgmk;->OF:Z

    if-nez v0, :cond_3

    .line 63
    invoke-virtual {p0}, Lgmk;->aHp()V

    .line 64
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljwv;->jl(Z)Ljwv;

    move-result-object v0

    goto :goto_1

    .line 66
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final aHp()V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmk;->OF:Z

    .line 72
    iget-object v0, p0, Lgmk;->cPk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lceq;

    .line 73
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, p0, Lgmk;->cPk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 76
    return-void
.end method
