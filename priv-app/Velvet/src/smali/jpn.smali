.class public final Ljpn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dWx:J

.field private dWy:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 412
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 413
    iput v2, p0, Ljpn;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljpn;->dWx:J

    iput v2, p0, Ljpn;->dWy:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljpn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpn;->eCz:I

    .line 414
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 355
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljpn;->dWx:J

    iget v0, p0, Ljpn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljpn;->dWy:I

    iget v0, p0, Ljpn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpn;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 428
    iget v0, p0, Ljpn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 429
    const/4 v0, 0x1

    iget-wide v2, p0, Ljpn;->dWx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 431
    :cond_0
    iget v0, p0, Ljpn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 432
    const/4 v0, 0x2

    iget v1, p0, Ljpn;->dWy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 434
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 435
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 439
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 440
    iget v1, p0, Ljpn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 441
    const/4 v1, 0x1

    iget-wide v2, p0, Ljpn;->dWx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    :cond_0
    iget v1, p0, Ljpn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 445
    const/4 v1, 0x2

    iget v2, p0, Ljpn;->dWy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 448
    :cond_1
    return v0
.end method
