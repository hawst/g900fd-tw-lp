.class public final Lgxw;
.super Lguv;
.source "PG"


# instance fields
.field private final Oq:I

.field private cYM:Z

.field cYN:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field cYO:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field cYP:Lejd;

.field cYQ:Lejd;

.field cYR:Z

.field cYS:Z

.field private cYT:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mGsaConfigFlags:Lchk;

.field private final mLocalTtsManager:Lice;

.field private final mTtsAudioPlayer:Lhix;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentView;Lhix;Lice;Lgpu;Lhgz;Lchk;Legs;)V
    .locals 7

    .prologue
    .line 88
    const-string v1, "results"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lguv;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;Lgpu;Lhgz;Lchk;Legs;)V

    .line 89
    iput-object p2, p0, Lgxw;->mTtsAudioPlayer:Lhix;

    .line 90
    iput-object p3, p0, Lgxw;->mLocalTtsManager:Lice;

    .line 91
    iput-object p6, p0, Lgxw;->mGsaConfigFlags:Lchk;

    .line 92
    sget-object v0, Lcgg;->aVn:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aLi()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lgxw;->Oq:I

    .line 98
    :goto_1
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aLi()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lgxw;->Oq:I

    goto :goto_1
.end method

.method static synthetic a(Lgxw;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lgxw;->Oq:I

    return v0
.end method

.method private aLy()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lgxz;

    invoke-direct {v0, p0}, Lgxz;-><init>(Lgxw;)V

    invoke-virtual {p0, v0}, Lgxw;->a(Lesj;)V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgxw;->cYR:Z

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lgxw;->cYT:Landroid/view/View;

    .line 196
    iget-object v0, p0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 197
    return-void
.end method

.method private gb(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 263
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 266
    :cond_0
    if-eqz p1, :cond_1

    .line 267
    iget-object v0, p0, Lgxw;->cYP:Lejd;

    iget-object v1, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/MainContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lgxw;->mGsaConfigFlags:Lchk;

    invoke-static {v1, v2, v5, v4}, Lcpn;->a(Landroid/content/res/Resources;Lchk;ZZ)I

    move-result v1

    iget-object v2, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/MainContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lgxw;->mGsaConfigFlags:Lchk;

    invoke-static {v2, v3, v4, v4}, Lcpn;->a(Landroid/content/res/Resources;Lchk;ZZ)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lejd;->aO(II)V

    .line 270
    iput-boolean v5, p0, Lgxw;->cYM:Z

    goto :goto_0

    .line 272
    :cond_1
    iget-object v0, p0, Lgxw;->cYP:Lejd;

    invoke-virtual {v0, v4, v4}, Lejd;->aO(II)V

    .line 273
    iput-boolean v4, p0, Lgxw;->cYM:Z

    goto :goto_0
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 514
    const/16 v0, 0x1fff

    return v0
.end method

.method protected final X(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 224
    new-instance v1, Lgxl;

    const-wide/16 v2, 0x0

    invoke-direct {v1, p0, v0, v2, v3}, Lgxl;-><init>(Lgxi;IJ)V

    invoke-virtual {p0, v1}, Lgxi;->a(Lesj;)V

    .line 226
    invoke-virtual {p0}, Lgxw;->aLH()Lgyz;

    move-result-object v1

    iget-object v1, v1, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    invoke-virtual {v1}, Lcjs;->LG()Z

    move-result v1

    invoke-virtual {p0, v1}, Lgxw;->fY(Z)V

    .line 228
    iget v1, p0, Lgxw;->Oq:I

    if-nez v1, :cond_0

    .line 229
    iget-object v1, p0, Lgyy;->mPresenter:Lgyz;

    iget-object v1, v1, Lgyz;->mVelvetFactory:Lgpu;

    invoke-virtual {p0}, Lgxw;->aLt()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-static {p0, v1}, Lgpu;->d(Lgyy;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lgxw;->cYO:Landroid/view/View;

    .line 230
    iget-object v1, p0, Lgxw;->cYO:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget v1, p0, Lgxw;->Oq:I

    if-nez v1, :cond_1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    new-instance v0, Lgxx;

    invoke-direct {v0, p0}, Lgxx;-><init>(Lgxw;)V

    invoke-virtual {p0, v0}, Lgxw;->a(Lesj;)V

    .line 233
    :cond_0
    return-void

    .line 231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lddb;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 112
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    .line 115
    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {v1}, Ldda;->aak()Ldcw;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Ldcw;->Zo()I

    move-result v7

    .line 121
    if-eqz v7, :cond_1

    .line 125
    const/4 v4, 0x2

    if-ne v7, v4, :cond_7

    .line 126
    iget-object v4, p0, Lgxw;->mTtsAudioPlayer:Lhix;

    invoke-virtual {v0}, Ldcw;->Zp()[B

    move-result-object v0

    invoke-virtual {v4, v0}, Lhix;->X([B)V

    move-object v0, v2

    :cond_0
    move-object v6, v2

    move-object v5, v0

    .line 139
    :goto_0
    new-instance v0, Lgya;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    iget-object v3, p0, Lgxw;->mTtsAudioPlayer:Lhix;

    iget-object v4, p0, Lgxw;->mLocalTtsManager:Lice;

    const/4 v10, 0x3

    if-ne v7, v10, :cond_8

    move v7, v8

    :goto_1
    invoke-direct/range {v0 .. v7}, Lgya;-><init>(Ldda;Lcom/google/android/shared/search/Query;Lhix;Lice;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lgxw;->a(Lesj;)V

    .line 145
    :cond_1
    invoke-virtual {v1}, Ldda;->aan()Ldcy;

    move-result-object v3

    .line 149
    invoke-virtual {p0}, Lgxw;->aKE()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 150
    invoke-virtual {v3}, Ldcy;->ZB()Z

    move-result v1

    .line 151
    invoke-virtual {v3}, Ldcy;->ZN()Z

    move-result v0

    .line 153
    :goto_2
    iget v2, p0, Lgxw;->Oq:I

    if-eq v2, v8, :cond_9

    move v2, v8

    :goto_3
    if-eqz v2, :cond_a

    .line 154
    invoke-virtual {v3}, Ldcy;->ZA()Z

    move-result v2

    .line 162
    :goto_4
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v4

    if-eqz v4, :cond_3

    if-nez v2, :cond_3

    .line 163
    if-nez v0, :cond_2

    if-nez v1, :cond_b

    iget-boolean v0, p0, Lgxw;->cYR:Z

    if-eqz v0, :cond_b

    .line 164
    :cond_2
    invoke-direct {p0}, Lgxw;->aLy()V

    .line 172
    :cond_3
    :goto_5
    invoke-super {p0, p1}, Lguv;->a(Lddb;)V

    .line 175
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 176
    :cond_4
    if-eqz v2, :cond_6

    .line 177
    iget-boolean v0, p0, Lgxw;->cYR:Z

    if-nez v0, :cond_c

    .line 178
    iget v0, p0, Lgxw;->Oq:I

    if-eq v0, v8, :cond_5

    move v9, v8

    :cond_5
    invoke-static {v9}, Lifv;->gY(Z)V

    iput-boolean v8, p0, Lgxw;->cYR:Z

    new-instance v0, Lgyb;

    invoke-direct {v0, p0}, Lgyb;-><init>(Lgxw;)V

    invoke-virtual {p0, v0}, Lgxw;->a(Lesj;)V

    .line 187
    :cond_6
    :goto_6
    return-void

    .line 130
    :cond_7
    invoke-virtual {v0}, Ldcw;->Zm()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v4

    .line 132
    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v5

    invoke-virtual {v4, v5}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 133
    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aIW()Ljava/lang/String;

    move-result-object v6

    move-object v5, v0

    goto/16 :goto_0

    :cond_8
    move v7, v9

    .line 139
    goto :goto_1

    :cond_9
    move v2, v9

    .line 153
    goto :goto_3

    .line 158
    :cond_a
    invoke-virtual {v3}, Ldcy;->ZA()Z

    move-result v2

    invoke-virtual {p0, v2}, Lgxw;->fX(Z)V

    move v2, v9

    goto :goto_4

    .line 165
    :cond_b
    iget-boolean v0, p0, Lgxw;->cYR:Z

    if-eqz v0, :cond_3

    .line 166
    new-instance v0, Lgxz;

    invoke-direct {v0, p0, v9}, Lgxz;-><init>(Lgxw;Z)V

    invoke-virtual {p0, v0}, Lgxw;->a(Lesj;)V

    .line 167
    iput-boolean v9, p0, Lgxw;->cYR:Z

    goto :goto_5

    .line 179
    :cond_c
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 180
    invoke-virtual {v3}, Ldcy;->ZH()Z

    move-result v0

    .line 181
    iget-boolean v1, p0, Lgxw;->cYM:Z

    if-eq v1, v0, :cond_6

    .line 182
    invoke-direct {p0, v0}, Lgxw;->gb(Z)V

    goto :goto_6

    :cond_d
    move v0, v9

    move v1, v9

    goto/16 :goto_2
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 503
    invoke-super {p0, p1}, Lguv;->a(Letj;)V

    .line 504
    const-string v0, "ResultsPresenter"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 505
    const-string v0, "Mode"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lgxw;->Oq:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 506
    const-string v0, "WebViewShown"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lgxw;->cYR:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 507
    const-string v0, "WebViewVisible"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lgxw;->cYS:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 508
    const-string v0, "LastVisibleCard"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgxw;->cYT:Landroid/view/View;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 509
    return-void
.end method

.method protected final aKE()Z
    .locals 2

    .prologue
    .line 102
    iget v0, p0, Lgxw;->Oq:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aKF()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 237
    invoke-super {p0}, Lguv;->aKF()V

    .line 239
    iget v0, p0, Lgxw;->Oq:I

    if-nez v0, :cond_0

    .line 240
    iget v0, p0, Lgxw;->Oq:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lgxw;->cYO:Landroid/view/View;

    new-instance v2, Lgxy;

    invoke-direct {v2, p0, v0}, Lgxy;-><init>(Lgxw;Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lgxw;->a(Lesj;)V

    .line 242
    :cond_0
    invoke-virtual {p0, v1}, Lgxw;->fY(Z)V

    .line 246
    invoke-direct {p0}, Lgxw;->aLy()V

    .line 247
    return-void

    :cond_1
    move v0, v1

    .line 240
    goto :goto_0
.end method

.method final aLA()V
    .locals 8

    .prologue
    const v4, 0x7f0d00ae

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 341
    iget-object v1, p0, Lgxw;->cYN:Landroid/view/View;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    iget-boolean v1, p0, Lgxw;->cYS:Z

    invoke-static {v1}, Lifv;->gY(Z)V

    .line 344
    iget v1, p0, Lgxw;->Oq:I

    if-nez v1, :cond_3

    .line 345
    iget-object v1, p0, Lgxw;->cYT:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 347
    invoke-virtual {p0, v4}, Lgxw;->getDimensionPixelSize(I)I

    move-result v1

    .line 350
    invoke-virtual {p0}, Lgxw;->Nf()Ldda;

    move-result-object v2

    invoke-virtual {v2}, Ldda;->aan()Ldcy;

    move-result-object v2

    invoke-virtual {v2}, Ldcy;->ZM()Lhha;

    move-result-object v2

    invoke-virtual {v2}, Lhha;->aOI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    iget-object v2, p0, Lgxw;->cYT:Landroid/view/View;

    iget-object v3, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v3}, Lgyz;->wj()Lekf;

    move-result-object v3

    invoke-interface {v3}, Lekf;->getScrollY()I

    move-result v3

    if-lez v3, :cond_1

    .line 353
    :goto_0
    iget-object v2, p0, Lgxw;->cYQ:Lejd;

    iget-object v3, p0, Lgxw;->cYT:Landroid/view/View;

    invoke-virtual {v2, v3, v1, v0}, Lejd;->e(Landroid/view/View;II)V

    .line 355
    iget-object v2, p0, Lgxw;->cYO:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 358
    :cond_0
    iget-object v2, p0, Lgxw;->cYP:Lejd;

    iget-object v3, p0, Lgxw;->cYT:Landroid/view/View;

    invoke-virtual {v2, v3, v1, v0}, Lejd;->e(Landroid/view/View;II)V

    .line 366
    :goto_1
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 367
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 368
    return-void

    .line 351
    :cond_1
    invoke-virtual {p0, v4}, Lgxw;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v4}, Lgyz;->wj()Lekf;

    move-result-object v4

    invoke-interface {v4}, Lekf;->ate()I

    move-result v4

    iget-object v5, p0, Lgxw;->cYO:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    const v6, 0x7f0d00c2

    invoke-virtual {p0, v6}, Lgxw;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    sub-int v2, v4, v2

    sub-int/2addr v2, v5

    sub-int/2addr v2, v6

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 360
    :cond_2
    iget-object v0, p0, Lgxw;->cYP:Lejd;

    invoke-virtual {v0, v7}, Lejd;->hM(I)V

    .line 361
    iget-object v0, p0, Lgxw;->cYO:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 364
    :cond_3
    iget-object v0, p0, Lgxw;->cYP:Lejd;

    invoke-virtual {v0, v7}, Lejd;->hM(I)V

    goto :goto_1
.end method

.method final aLz()Z
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    if-nez v0, :cond_1

    .line 252
    invoke-virtual {p0}, Lgxw;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aMl()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    .line 253
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    if-nez v0, :cond_0

    .line 254
    const/4 v0, 0x0

    .line 259
    :goto_0
    return v0

    .line 256
    :cond_0
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    iput-object v0, p0, Lgxw;->cYP:Lejd;

    .line 257
    invoke-virtual {p0}, Lgxw;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZH()Z

    move-result v0

    invoke-direct {p0, v0}, Lgxw;->gb(Z)V

    .line 259
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final bB(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 328
    iget-object v0, p0, Lgxw;->cYT:Landroid/view/View;

    if-eq v0, p1, :cond_0

    .line 329
    iput-object p1, p0, Lgxw;->cYT:Landroid/view/View;

    .line 330
    iget-boolean v0, p0, Lgxw;->cYS:Z

    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {p0}, Lgxw;->aLA()V

    .line 334
    :cond_0
    return-void
.end method

.method public final fT(Z)V
    .locals 4

    .prologue
    .line 302
    invoke-super {p0, p1}, Lguv;->fT(Z)V

    .line 306
    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgxw;->cYP:Lejd;

    invoke-virtual {v0}, Lejd;->atq()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgxw;->cYN:Landroid/view/View;

    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v1

    invoke-static {v1}, Lifv;->gY(Z)V

    iget-object v1, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/MainContentView;->bI(Landroid/view/View;)F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lgxw;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZJ()V

    .line 309
    :cond_0
    return-void

    .line 306
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Lguv;->onStart()V

    .line 211
    invoke-virtual {p0, p0}, Lgxw;->a(Lddc;)V

    .line 212
    return-void
.end method

.method public final onStop()V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0}, Lguv;->onStop()V

    .line 217
    invoke-virtual {p0, p0}, Lgxw;->b(Lddc;)V

    .line 218
    return-void
.end method
