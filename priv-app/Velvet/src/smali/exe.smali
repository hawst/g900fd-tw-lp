.class public final Lexe;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Lefk;


# instance fields
.field private ckI:Z

.field private clc:Leyc;

.field private mEntryProvider:Lfaq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method public static E(Landroid/content/Intent;)Lexe;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 74
    const-string v0, "entry"

    const-class v3, Lizj;

    invoke-static {v1, v0, v3}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 75
    const-string v3, "action"

    const-class v4, Liwk;

    invoke-static {v1, v3, v4}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Liwk;

    .line 76
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 79
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0, v1, v2, v2, v2}, Lexe;->a(Lizj;Liwk;Ljbp;Ljava/lang/String;Ljava/lang/String;)Lexe;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lizj;Liwk;Ljbp;Ljava/lang/String;Ljava/lang/String;)Lexe;
    .locals 4
    .param p2    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59
    new-instance v0, Lexe;

    invoke-direct {v0}, Lexe;-><init>()V

    .line 61
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry_key"

    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v2, "action_key"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    if-eqz p2, :cond_0

    const-string v2, "edited_location_key"

    invoke-static {p2}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    const-string v2, "old_name_key"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "old_address_key"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v0, v1}, Lexe;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v0
.end method


# virtual methods
.method public final synthetic ar(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 33
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0}, Lexe;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v1

    const-string v2, "action_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lgbm;->R([B)Liwk;

    const-string v2, "old_name_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "old_address_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "edited_location_key"

    const-class v3, Ljbp;

    invoke-static {v0, v2, v3}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljbp;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lexe;->mEntryProvider:Lfaq;

    new-instance v3, Lexd;

    invoke-direct {v3, v1, v0}, Lexd;-><init>(Lizj;Ljbp;)V

    invoke-virtual {v2, v3}, Lfaq;->a(Lfbn;)V

    :cond_0
    invoke-virtual {p0}, Lexe;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Leyf;->a(Landroid/app/FragmentManager;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lexe;->clc:Leyc;

    iget-boolean v0, p0, Lexe;->ckI:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lexe;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "editPlaceWorkerFragment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 99
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v2

    iput-object v2, p0, Lexe;->mEntryProvider:Lfaq;

    .line 102
    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v5

    .line 103
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v8

    .line 105
    invoke-virtual {p0}, Lexe;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 106
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v2

    .line 107
    const-string v1, "action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->R([B)Liwk;

    move-result-object v3

    .line 108
    const-string v1, "edited_location_key"

    const-class v4, Ljbp;

    invoke-static {v0, v1, v4}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v4

    check-cast v4, Ljbp;

    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lexe;->setRetainInstance(Z)V

    .line 115
    invoke-virtual {p0}, Lexe;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Leyf;->a(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    .line 118
    new-instance v0, Leyc;

    invoke-virtual {p0}, Lexe;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v6, 0x0

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Leyc;-><init>(Landroid/content/Context;Lizj;Liwk;Ljbp;Lfcx;Lfaq;Lefk;Lemp;)V

    iput-object v0, p0, Lexe;->clc:Leyc;

    .line 120
    iget-object v0, p0, Lexe;->clc:Leyc;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leyc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 121
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lexe;->clc:Leyc;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lexe;->clc:Leyc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leyc;->cancel(Z)Z

    .line 128
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lexe;->clc:Leyc;

    .line 129
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 130
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lexe;->ckI:Z

    .line 135
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 136
    return-void
.end method
