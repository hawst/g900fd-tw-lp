.class public final Lgny;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private cQO:Ljava/lang/String;

.field private cQP:Ljava/lang/String;

.field private cQQ:Z

.field public cQR:[Lgnx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 451
    const/4 v0, 0x0

    iput v0, p0, Lgny;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lgny;->cQO:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lgny;->cQP:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgny;->cQQ:Z

    invoke-static {}, Lgnx;->aIi()[Lgnx;

    move-result-object v0

    iput-object v0, p0, Lgny;->cQR:[Lgnx;

    const/4 v0, 0x0

    iput-object v0, p0, Lgny;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lgny;->eCz:I

    .line 452
    return-void
.end method

.method public static V([B)Lgny;
    .locals 1

    .prologue
    .line 571
    new-instance v0, Lgny;

    invoke-direct {v0}, Lgny;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lgny;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 365
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lgny;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgny;->cQO:Ljava/lang/String;

    iget v0, p0, Lgny;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgny;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgny;->cQP:Ljava/lang/String;

    iget v0, p0, Lgny;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgny;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lgny;->cQQ:Z

    iget v0, p0, Lgny;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lgny;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lgny;->cQR:[Lgnx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgnx;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lgny;->cQR:[Lgnx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lgnx;

    invoke-direct {v3}, Lgnx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lgny;->cQR:[Lgnx;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lgnx;

    invoke-direct {v3}, Lgnx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lgny;->cQR:[Lgnx;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 468
    iget v0, p0, Lgny;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 469
    const/4 v0, 0x1

    iget-object v1, p0, Lgny;->cQO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 471
    :cond_0
    iget v0, p0, Lgny;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 472
    const/4 v0, 0x2

    iget-object v1, p0, Lgny;->cQP:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 474
    :cond_1
    iget v0, p0, Lgny;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 475
    const/4 v0, 0x3

    iget-boolean v1, p0, Lgny;->cQQ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 477
    :cond_2
    iget-object v0, p0, Lgny;->cQR:[Lgnx;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgny;->cQR:[Lgnx;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 478
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lgny;->cQR:[Lgnx;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 479
    iget-object v1, p0, Lgny;->cQR:[Lgnx;

    aget-object v1, v1, v0

    .line 480
    if-eqz v1, :cond_3

    .line 481
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 478
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 486
    return-void
.end method

.method public final aIj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lgny;->cQO:Ljava/lang/String;

    return-object v0
.end method

.method public final aIk()Z
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lgny;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lgny;->cQP:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 490
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 491
    iget v1, p0, Lgny;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 492
    const/4 v1, 0x1

    iget-object v2, p0, Lgny;->cQO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 495
    :cond_0
    iget v1, p0, Lgny;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 496
    const/4 v1, 0x2

    iget-object v2, p0, Lgny;->cQP:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 499
    :cond_1
    iget v1, p0, Lgny;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 500
    const/4 v1, 0x3

    iget-boolean v2, p0, Lgny;->cQQ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 503
    :cond_2
    iget-object v1, p0, Lgny;->cQR:[Lgnx;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lgny;->cQR:[Lgnx;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 504
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lgny;->cQR:[Lgnx;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 505
    iget-object v2, p0, Lgny;->cQR:[Lgnx;

    aget-object v2, v2, v0

    .line 506
    if-eqz v2, :cond_3

    .line 507
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 504
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 512
    :cond_5
    return v0
.end method
