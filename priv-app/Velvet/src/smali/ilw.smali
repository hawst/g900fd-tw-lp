.class public final Lilw;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/List;Lifg;)Ljava/util/List;
    .locals 1

    .prologue
    .line 345
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_0

    new-instance v0, Lima;

    invoke-direct {v0, p0, p1}, Lima;-><init>(Ljava/util/List;Lifg;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Limb;

    invoke-direct {v0, p0, p1}, Limb;-><init>(Ljava/util/List;Lifg;)V

    goto :goto_0
.end method

.method public static aD(Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 729
    instance-of v0, p0, Lily;

    if-eqz v0, :cond_0

    .line 730
    check-cast p0, Lily;

    invoke-virtual {p0}, Lily;->aXo()Ljava/util/List;

    move-result-object v0

    .line 734
    :goto_0
    return-object v0

    .line 731
    :cond_0
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_1

    .line 732
    new-instance v0, Lilx;

    invoke-direct {v0, p0}, Lilx;-><init>(Ljava/util/List;)V

    goto :goto_0

    .line 734
    :cond_1
    new-instance v0, Lily;

    invoke-direct {v0, p0}, Lily;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static g(Ljava/util/Iterator;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 136
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 138
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    :cond_0
    return-object v0
.end method

.method public static varargs k([Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 90
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    array-length v0, p0

    invoke-static {v0}, Lilw;->md(I)I

    move-result v0

    .line 93
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 94
    invoke-static {v1, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 95
    return-object v1
.end method

.method private static md(I)I
    .locals 4

    .prologue
    .line 99
    if-ltz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 102
    const-wide/16 v0, 0x5

    int-to-long v2, p0

    add-long/2addr v0, v2

    div-int/lit8 v2, p0, 0xa

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lius;->bZ(J)I

    move-result v0

    return v0

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static me(I)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 167
    if-ltz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0

    .line 167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mf(I)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0}, Lilw;->md(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public static x(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 117
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0}, Liia;->o(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public static y(Ljava/lang/Iterable;)Ljava/util/LinkedList;
    .locals 3

    .prologue
    .line 216
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 217
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 218
    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 220
    :cond_0
    return-object v0
.end method
