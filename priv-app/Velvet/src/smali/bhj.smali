.class public final Lbhj;
.super Ljava/lang/Object;


# instance fields
.field private final aBg:Ljava/util/Set;

.field private aBh:Ljava/lang/String;

.field private final aBi:Ljava/util/Map;

.field private aBj:I

.field private aBk:Landroid/os/Looper;

.field private final aBl:Ljava/util/Set;

.field private final aBm:Ljava/util/Set;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbhj;->aBg:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbhj;->aBi:Ljava/util/Map;

    const/4 v0, -0x1

    iput v0, p0, Lbhj;->aBj:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbhj;->aBl:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbhj;->aBm:Ljava/util/Set;

    iput-object p1, p0, Lbhj;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lbhj;->aBk:Landroid/os/Looper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbhj;->aBh:Ljava/lang/String;

    return-void
.end method

.method private yw()Lcom/google/android/gms/common/internal/ClientSettings;
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v2, p0, Lbhj;->aBg:Ljava/util/Set;

    const/4 v3, 0x0

    iget-object v5, p0, Lbhj;->aBh:Ljava/lang/String;

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lbgx;)Lbhj;
    .locals 5

    iget-object v0, p0, Lbhj;->aBi:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lbgx;->aAU:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lbhj;->aBg:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhq;

    iget-object v0, v0, Lbhq;->aBn:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public final a(Lbgx;Lbgz;)Lbhj;
    .locals 5

    const-string v0, "Null options are not permitted for this Api"

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbhj;->aBi:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lbgx;->aAU:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lbhj;->aBg:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhq;

    iget-object v0, v0, Lbhq;->aBn:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public final c(Lbhk;)Lbhj;
    .locals 1

    iget-object v0, p0, Lbhj;->aBl:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final c(Lbhl;)Lbhj;
    .locals 1

    iget-object v0, p0, Lbhj;->aBm:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final yx()Lbhi;
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lbhj;->aBi:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must call addApi() to add at least one API"

    invoke-static {v0, v1}, Lbjr;->b(ZLjava/lang/Object;)V

    iget v0, p0, Lbhj;->aBj:I

    if-ltz v0, :cond_2

    invoke-static {v9}, Lbib;->b(Lq;)Lbib;

    move-result-object v8

    iget v0, p0, Lbhj;->aBj:I

    invoke-virtual {v8, v0}, Lbib;->dH(I)Lbhi;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lbhs;

    iget-object v1, p0, Lbhj;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbhj;->aBk:Landroid/os/Looper;

    invoke-direct {p0}, Lbhj;->yw()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lbhj;->aBi:Ljava/util/Map;

    iget-object v5, p0, Lbhj;->aBl:Ljava/util/Set;

    iget-object v6, p0, Lbhj;->aBm:Ljava/util/Set;

    iget v7, p0, Lbhj;->aBj:I

    invoke-direct/range {v0 .. v7}, Lbhs;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    :cond_0
    iget v1, p0, Lbhj;->aBj:I

    invoke-virtual {v8, v1, v0, v9}, Lbib;->a(ILbhi;Lbhl;)V

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Lbhs;

    iget-object v1, p0, Lbhj;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lbhj;->aBk:Landroid/os/Looper;

    invoke-direct {p0}, Lbhj;->yw()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lbhj;->aBi:Ljava/util/Map;

    iget-object v5, p0, Lbhj;->aBl:Ljava/util/Set;

    iget-object v6, p0, Lbhj;->aBm:Ljava/util/Set;

    const/4 v7, -0x1

    invoke-direct/range {v0 .. v7}, Lbhs;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    goto :goto_1
.end method
