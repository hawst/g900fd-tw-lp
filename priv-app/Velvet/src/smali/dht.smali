.class public Ldht;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldgb;
.implements Leti;


# instance fields
.field private final RO:Ljava/lang/String;

.field private final buX:Z

.field private final byg:I

.field private final byh:I

.field private final byi:I

.field private final byj:Landroid/content/pm/ApplicationInfo;

.field private final byk:Ljava/lang/String;

.field private final byl:Ljava/lang/String;

.field private final bym:Ljava/lang/String;

.field private final byn:Ljava/lang/String;

.field private final byo:Ljava/lang/String;

.field private final byp:Ljava/util/Set;

.field private hO:Z

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/Set;Z)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Ldht;->mName:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Ldht;->RO:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Ldht;->byj:Landroid/content/pm/ApplicationInfo;

    .line 39
    iput p4, p0, Ldht;->byg:I

    .line 40
    iput p5, p0, Ldht;->byi:I

    .line 41
    iput p6, p0, Ldht;->byh:I

    .line 42
    iput-object p7, p0, Ldht;->byk:Ljava/lang/String;

    .line 43
    iput-object p8, p0, Ldht;->byl:Ljava/lang/String;

    .line 44
    iput-object p9, p0, Ldht;->bym:Ljava/lang/String;

    .line 45
    iput-boolean p10, p0, Ldht;->buX:Z

    .line 46
    iput-object p11, p0, Ldht;->byn:Ljava/lang/String;

    .line 47
    iput-object p12, p0, Ldht;->byo:Ljava/lang/String;

    .line 48
    iput-object p13, p0, Ldht;->byp:Ljava/util/Set;

    .line 49
    iput-boolean p14, p0, Ldht;->hO:Z

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Icing source ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldht;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 144
    const-string v0, "Canonical name"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldht;->byn:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 145
    const-string v0, "Enabled"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldht;->hO:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 146
    return-void
.end method

.method public final abJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldht;->byk:Ljava/lang/String;

    return-object v0
.end method

.method public final abK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldht;->byl:Ljava/lang/String;

    return-object v0
.end method

.method public final abL()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Ldht;->byg:I

    return v0
.end method

.method public final abM()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Ldht;->byi:I

    return v0
.end method

.method public final abN()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Ldht;->byh:I

    return v0
.end method

.method public final abO()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Ldht;->buX:Z

    return v0
.end method

.method public final abP()Z
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public final abQ()Z
    .locals 2

    .prologue
    .line 93
    const-string v0, "contacts"

    iget-object v1, p0, Ldht;->byn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aca()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldht;->bym:Ljava/lang/String;

    return-object v0
.end method

.method public final acb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Ldht;->byo:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 151
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Ldht;->byj:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public final getCanonicalName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Ldht;->byn:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ldht;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ldht;->RO:Ljava/lang/String;

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Ldht;->hO:Z

    return v0
.end method

.method public final jE(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Ldht;->byp:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final setEnabled(Z)V
    .locals 0

    .prologue
    .line 123
    iput-boolean p1, p0, Ldht;->hO:Z

    .line 124
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IcingSource[name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldht;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canonicalName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldht;->byn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ldht;->hO:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
