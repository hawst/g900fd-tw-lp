.class public final Ljby;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dZI:[Ljbw;

.field private dZJ:Ljava/lang/String;

.field private dZK:Ljava/lang/String;

.field private dZL:J

.field private dZM:Ljbp;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55230
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 55231
    const/4 v0, 0x0

    iput v0, p0, Ljby;->aez:I

    invoke-static {}, Ljbw;->bfH()[Ljbw;

    move-result-object v0

    iput-object v0, p0, Ljby;->dZI:[Ljbw;

    const-string v0, ""

    iput-object v0, p0, Ljby;->dZJ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljby;->dZK:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljby;->dZL:J

    iput-object v2, p0, Ljby;->dZM:Ljbp;

    iput-object v2, p0, Ljby;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljby;->eCz:I

    .line 55232
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 55142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljby;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljby;->dZI:[Ljbw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljby;->dZI:[Ljbw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljbw;

    invoke-direct {v3}, Ljbw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljby;->dZI:[Ljbw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljbw;

    invoke-direct {v3}, Ljbw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljby;->dZI:[Ljbw;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljby;->dZJ:Ljava/lang/String;

    iget v0, p0, Ljby;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljby;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljby;->dZK:Ljava/lang/String;

    iget v0, p0, Ljby;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljby;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljby;->dZL:J

    iget v0, p0, Ljby;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljby;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljby;->dZM:Ljbp;

    if-nez v0, :cond_4

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljby;->dZM:Ljbp;

    :cond_4
    iget-object v0, p0, Ljby;->dZM:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 55249
    iget-object v0, p0, Ljby;->dZI:[Ljbw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljby;->dZI:[Ljbw;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 55250
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljby;->dZI:[Ljbw;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 55251
    iget-object v1, p0, Ljby;->dZI:[Ljbw;

    aget-object v1, v1, v0

    .line 55252
    if-eqz v1, :cond_0

    .line 55253
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 55250
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55257
    :cond_1
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 55258
    const/4 v0, 0x2

    iget-object v1, p0, Ljby;->dZJ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 55260
    :cond_2
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 55261
    const/4 v0, 0x3

    iget-object v1, p0, Ljby;->dZK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 55263
    :cond_3
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 55264
    const/4 v0, 0x4

    iget-wide v2, p0, Ljby;->dZL:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->g(IJ)V

    .line 55266
    :cond_4
    iget-object v0, p0, Ljby;->dZM:Ljbp;

    if-eqz v0, :cond_5

    .line 55267
    const/4 v0, 0x5

    iget-object v1, p0, Ljby;->dZM:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 55269
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 55270
    return-void
.end method

.method public final bfO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55167
    iget-object v0, p0, Ljby;->dZJ:Ljava/lang/String;

    return-object v0
.end method

.method public final bfP()Z
    .locals 1

    .prologue
    .line 55178
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55189
    iget-object v0, p0, Ljby;->dZK:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 55274
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 55275
    iget-object v0, p0, Ljby;->dZI:[Ljbw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljby;->dZI:[Ljbw;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 55276
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljby;->dZI:[Ljbw;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 55277
    iget-object v2, p0, Ljby;->dZI:[Ljbw;

    aget-object v2, v2, v0

    .line 55278
    if-eqz v2, :cond_0

    .line 55279
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 55276
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55284
    :cond_1
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 55285
    const/4 v0, 0x2

    iget-object v2, p0, Ljby;->dZJ:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 55288
    :cond_2
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 55289
    const/4 v0, 0x3

    iget-object v2, p0, Ljby;->dZK:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 55292
    :cond_3
    iget v0, p0, Ljby;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 55293
    const/4 v0, 0x4

    iget-wide v2, p0, Ljby;->dZL:J

    invoke-static {v0, v2, v3}, Ljsj;->j(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 55296
    :cond_4
    iget-object v0, p0, Ljby;->dZM:Ljbp;

    if-eqz v0, :cond_5

    .line 55297
    const/4 v0, 0x5

    iget-object v2, p0, Ljby;->dZM:Ljbp;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 55300
    :cond_5
    return v1
.end method

.method public final sK(Ljava/lang/String;)Ljby;
    .locals 1

    .prologue
    .line 55170
    if-nez p1, :cond_0

    .line 55171
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55173
    :cond_0
    iput-object p1, p0, Ljby;->dZJ:Ljava/lang/String;

    .line 55174
    iget v0, p0, Ljby;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljby;->aez:I

    .line 55175
    return-object p0
.end method

.method public final sL(Ljava/lang/String;)Ljby;
    .locals 1

    .prologue
    .line 55192
    if-nez p1, :cond_0

    .line 55193
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55195
    :cond_0
    iput-object p1, p0, Ljby;->dZK:Ljava/lang/String;

    .line 55196
    iget v0, p0, Ljby;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljby;->aez:I

    .line 55197
    return-object p0
.end method
