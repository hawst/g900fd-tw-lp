.class public final Lhsm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final dpW:Z

.field final mAudioRouter:Lhhu;

.field final mBluetoothController:Lhjb;

.field private final mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mSoundManager:Lhik;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lhhu;Lhjb;ZLhik;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p3, p0, Lhsm;->mAudioRouter:Lhhu;

    .line 48
    iput-object p4, p0, Lhsm;->mBluetoothController:Lhjb;

    .line 49
    iput-object p2, p0, Lhsm;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 50
    iput-object p1, p0, Lhsm;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 51
    iput-boolean p5, p0, Lhsm;->dpW:Z

    .line 52
    iput-object p6, p0, Lhsm;->mSoundManager:Lhik;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lhsp;)V
    .locals 7

    .prologue
    .line 56
    invoke-static {}, Lenu;->auR()V

    .line 58
    iget-object v0, p0, Lhsm;->mUiExecutor:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhsp;

    .line 60
    new-instance v4, Lhsn;

    invoke-direct {v4, p0, v5}, Lhsn;-><init>(Lhsm;Lhsp;)V

    .line 69
    iget-object v0, p0, Lhsm;->mSoundManager:Lhik;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhik;->gt(Z)V

    .line 70
    iget-object v6, p0, Lhsm;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lhso;

    const-string v2, "Establish route"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lhso;-><init>(Lhsm;Ljava/lang/String;[ILhhv;Lhsp;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 100
    return-void
.end method

.method public final aRP()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 103
    iget-object v0, p0, Lhsm;->mAudioRouter:Lhhu;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v2, v3}, Lhhu;->a(IILhhv;Z)V

    .line 105
    iget-object v0, p0, Lhsm;->mSoundManager:Lhik;

    invoke-virtual {v0, v3}, Lhik;->gt(Z)V

    .line 106
    return-void
.end method
