.class final Lgye;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldsj;


# instance fields
.field private synthetic cZh:Lgyd;


# direct methods
.method constructor <init>(Lgyd;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lgye;->cZh:Lgyd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;IZ)V
    .locals 1

    .prologue
    .line 273
    if-eqz p3, :cond_0

    .line 274
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->UF()V

    .line 276
    :cond_0
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lgyz;->e(Ljava/lang/CharSequence;I)V

    .line 279
    :cond_1
    return-void
.end method

.method public final afc()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 168
    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->N(Lcom/google/android/shared/search/Query;)V

    .line 170
    :cond_0
    return-void
.end method

.method public final afd()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XT()V

    .line 176
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->Xb()Z

    .line 178
    :cond_0
    return-void
.end method

.method public final afe()V
    .locals 5

    .prologue
    .line 184
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Ldcw;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    invoke-virtual {v0}, Ldcw;->Zq()V

    goto :goto_0

    .line 195
    :cond_1
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v1}, Lgyd;->Nf()Ldda;

    move-result-object v1

    invoke-virtual {v1}, Ldda;->aap()Ldct;

    move-result-object v1

    invoke-virtual {v1}, Ldct;->Yw()I

    move-result v1

    .line 197
    packed-switch v1, :pswitch_data_0

    .line 205
    :pswitch_0
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aph()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0

    .line 201
    :pswitch_1
    const-string v2, "SearchPlatePresenter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "onStartVoiceSearchClicked not supported in mode: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "unknown mode: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    packed-switch v1, :pswitch_data_1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "MODE_CLEAR"

    goto :goto_1

    :pswitch_3
    const-string v0, "MODE_TEXT_INPUT"

    goto :goto_1

    :pswitch_4
    const-string v0, "MODE_VOICE_INPUT"

    goto :goto_1

    :pswitch_5
    const-string v0, "MODE_MUSIC_INPUT"

    goto :goto_1

    :pswitch_6
    const-string v0, "MODE_TV_INPUT"

    goto :goto_1

    :pswitch_7
    const-string v0, "MODE_TEXT_RESULT"

    goto :goto_1

    :pswitch_8
    const-string v0, "MODE_VOICE_RESULT"

    goto :goto_1

    :pswitch_9
    const-string v0, "MODE_FOLLOW_ON"

    goto :goto_1

    :pswitch_a
    const-string v0, "MODE_AUDIO_ERROR"

    goto :goto_1

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 201
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final afl()V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 235
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apc()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    .line 237
    return-void
.end method

.method public final afm()V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->afm()V

    .line 244
    :cond_0
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-static {v0}, Lgyd;->a(Lgyd;)Lgzz;

    move-result-object v0

    invoke-interface {v0}, Lgzz;->UK()V

    .line 245
    return-void
.end method

.method public final afn()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->afn()V

    .line 252
    :cond_0
    return-void
.end method

.method public final afo()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lgyz;->kJ(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public final afp()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aMj()V

    .line 215
    :cond_0
    return-void
.end method

.method public final afq()V
    .locals 0

    .prologue
    .line 284
    invoke-virtual {p0}, Lgye;->afm()V

    .line 285
    return-void
.end method

.method public final afr()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0}, Ledu;->anW()Ledu;

    .line 295
    :cond_0
    return-void
.end method

.method public final afs()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0}, Ledu;->anX()Ledu;

    .line 313
    :cond_0
    return-void
.end method

.method public final aft()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0}, Ledu;->anY()Ledu;

    .line 322
    :cond_0
    return-void
.end method

.method public final afu()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->Xb()Z

    .line 343
    :cond_0
    return-void
.end method

.method public final afv()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->Cq()V

    .line 336
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;II)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lgyz;->g(Ljava/lang/CharSequence;II)V

    .line 222
    :cond_0
    return-void
.end method

.method public final dY(Z)V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    iput-boolean p1, v0, Ledu;->bWp:Z

    .line 331
    :cond_0
    return-void
.end method

.method public final dZ(Z)V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0, p1}, Ledu;->eN(Z)Ledu;

    .line 305
    :cond_0
    return-void
.end method

.method public final gm(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 258
    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lgye;->cZh:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0, v1}, Lgyz;->kJ(I)V

    .line 261
    const/4 v0, 0x1

    .line 263
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
