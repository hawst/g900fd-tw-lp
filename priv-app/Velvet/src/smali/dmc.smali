.class public final Ldmc;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static final b(Ljava/io/IOException;I)V
    .locals 4

    .prologue
    const v0, 0x50002

    const/4 v1, 0x0

    .line 106
    const-string v2, "NetworkUtils"

    const-string v3, "OkHttp exception"

    invoke-static {v2, v3, p0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "Unexpected response code for CONNECT: "

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v3, 0x26

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 108
    :goto_0
    if-eqz v2, :cond_1

    .line 109
    new-instance v0, Left;

    const-string v1, "OkHttp"

    invoke-direct {v0, v2, v1}, Left;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    .line 107
    goto :goto_0

    .line 111
    :cond_1
    if-eqz p0, :cond_1d

    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1d

    instance-of v3, p0, Ljava/net/ProtocolException;

    if-eqz v3, :cond_7

    const-string v3, "Received HTTP_PROXY_AUTH"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v0, 0x50001

    .line 112
    :cond_2
    :goto_1
    if-nez v0, :cond_1e

    .line 115
    :goto_2
    new-instance v0, Lefs;

    invoke-direct {v0, p1}, Lefs;-><init>(I)V

    throw v0

    .line 111
    :cond_3
    const-string v3, "expected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "bytes but received"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    const v0, 0x50016

    goto :goto_1

    :cond_4
    const-string v3, "unexpected end of stream"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v0, "version != 3"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x50003

    goto :goto_1

    :cond_5
    const-string v0, "content-length promised"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x50004

    goto :goto_1

    :cond_6
    const-string v0, "exceeded content-length limit"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const v0, 0x50005

    goto :goto_1

    :cond_7
    const-string v3, "Hostname"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "was not verified"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const v0, 0x50006

    goto :goto_1

    :cond_8
    const-string v3, "Unexpected NPN transport"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const v0, 0x50007

    goto :goto_1

    :cond_9
    const-string v3, "unexpected journal header"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const v0, 0x50008

    goto :goto_1

    :cond_a
    const-string v3, "unexpected journal line"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const v0, 0x50019

    goto :goto_1

    :cond_b
    const-string v3, "Cannot buffer entire body for content length"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const v0, 0x50009

    goto/16 :goto_1

    :cond_c
    const-string v3, "Content-Length and stream disagree"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    const v0, 0x5000a

    goto/16 :goto_1

    :cond_d
    const-string v3, "not a readable directory"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    const v0, 0x5000b

    goto/16 :goto_1

    :cond_e
    const-string v3, "not a readable directory"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    const v0, 0x5001b

    goto/16 :goto_1

    :cond_f
    const-string v3, "failed to delete file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    const v0, 0x5000c

    goto/16 :goto_1

    :cond_10
    const-string v3, "Unexpected frame"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    const v0, 0x5000d

    goto/16 :goto_1

    :cond_11
    const-string v3, "shutdown"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const v0, 0x5000e

    goto/16 :goto_1

    :cond_12
    const-string v3, "stream closed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    const v0, 0x5000f

    goto/16 :goto_1

    :cond_13
    const-string v3, "stream finished"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    const v0, 0x50010

    goto/16 :goto_1

    :cond_14
    const-string v3, "stream was reset"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    const v0, 0x50011

    goto/16 :goto_1

    :cond_15
    const-string v3, "TYPE_GOAWAY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    const v0, 0x50012

    goto/16 :goto_1

    :cond_16
    const-string v3, "numberOfPairs < 0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    const v0, 0x50013

    goto/16 :goto_1

    :cond_17
    const-string v3, "numberOfPairs < 1024"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_18

    const v0, 0x50014

    goto/16 :goto_1

    :cond_18
    const-string v3, "name.length == 0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    const v0, 0x50015

    goto/16 :goto_1

    :cond_19
    const-string v3, "TLS tunnel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    const v0, 0x50017

    goto/16 :goto_1

    :cond_1a
    const-string v3, "Failed to authenticate with proxy"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1b

    const v0, 0x50018

    goto/16 :goto_1

    :cond_1b
    const-string v3, "unexpected end of stream"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v0, "Exception in connect"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const v0, 0x5001c

    goto/16 :goto_1

    :cond_1c
    const-string v0, " != "

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const v0, 0x5001d

    goto/16 :goto_1

    :cond_1d
    move v0, v1

    goto/16 :goto_1

    :cond_1e
    move p1, v0

    goto/16 :goto_2
.end method

.method public static d(Lbzh;)V
    .locals 3

    .prologue
    .line 38
    :try_start_0
    invoke-interface {p0}, Lbzh;->connect()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 46
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    new-instance v1, Lefs;

    const v2, 0x40003

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 41
    :catch_1
    move-exception v0

    .line 42
    const v1, 0x4000e

    invoke-static {v0, v1}, Ldmc;->b(Ljava/io/IOException;I)V

    goto :goto_0

    .line 43
    :catch_2
    move-exception v0

    .line 44
    const v1, 0x40002

    invoke-static {v0, v1}, Ldmc;->b(Ljava/io/IOException;I)V

    goto :goto_0
.end method

.method public static e(Lbzh;)Ljava/io/InputStream;
    .locals 3

    .prologue
    const v2, 0x4000d

    .line 59
    :try_start_0
    invoke-interface {p0}, Lbzh;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 65
    :try_start_1
    invoke-interface {p0}, Lbzh;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 71
    new-instance v1, Left;

    const-string v2, "HttpURLConnection.getInputStream() failed"

    invoke-direct {v1, v0, v2}, Left;-><init>(ILjava/lang/String;)V

    throw v1

    .line 69
    :catch_1
    move-exception v1

    new-instance v1, Lefs;

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 72
    :catch_2
    move-exception v0

    .line 73
    new-instance v1, Lefs;

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method
