.class final Laio;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field private synthetic ZG:Laim;


# direct methods
.method private constructor <init>(Laim;)V
    .locals 0

    .prologue
    .line 789
    iput-object p1, p0, Laio;->ZG:Laim;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laim;B)V
    .locals 0

    .prologue
    .line 789
    invoke-direct {p0, p1}, Laio;-><init>(Laim;)V

    return-void
.end method

.method private lX()Lain;
    .locals 2

    .prologue
    .line 801
    iget-object v0, p0, Laio;->ZG:Laim;

    invoke-static {v0}, Laim;->d(Laim;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 803
    :goto_0
    :try_start_0
    iget-object v0, p0, Laio;->ZG:Laim;

    invoke-static {v0}, Laim;->e(Laim;)Laip;

    move-result-object v0

    invoke-virtual {v0}, Laip;->lY()Lain;

    move-result-object v0

    .line 804
    if-eqz v0, :cond_0

    .line 805
    monitor-exit v1

    return-object v0

    .line 807
    :cond_0
    iget-object v0, p0, Laio;->ZG:Laim;

    invoke-static {v0}, Laim;->d(Laim;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 809
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final lW()V
    .locals 2

    .prologue
    .line 792
    invoke-virtual {p0}, Laio;->interrupt()V

    .line 794
    :try_start_0
    invoke-virtual {p0}, Laio;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 798
    :goto_0
    return-void

    .line 796
    :catch_0
    move-exception v0

    const-string v0, "TiledImageRenderer"

    const-string v1, "Interrupted while waiting for TileDecoder thread to finish!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 815
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Laio;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 816
    invoke-direct {p0}, Laio;->lX()Lain;

    move-result-object v0

    .line 817
    iget-object v1, p0, Laio;->ZG:Laim;

    invoke-static {v1, v0}, Laim;->a(Laim;Lain;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 822
    :catch_0
    move-exception v0

    :cond_0
    return-void
.end method
