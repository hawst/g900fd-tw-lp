.class public final enum Ldtk;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum bKA:Ldtk;

.field public static final enum bKB:Ldtk;

.field public static final enum bKC:Ldtk;

.field public static final enum bKD:Ldtk;

.field public static final enum bKE:Ldtk;

.field private static final synthetic bKF:[Ldtk;

.field public static final enum bKz:Ldtk;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 373
    new-instance v0, Ldtk;

    const-string v1, "NOT_KNOWN"

    invoke-direct {v0, v1, v3}, Ldtk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtk;->bKz:Ldtk;

    .line 376
    new-instance v0, Ldtk;

    const-string v1, "TYPING"

    invoke-direct {v0, v1, v4}, Ldtk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtk;->bKA:Ldtk;

    .line 379
    new-instance v0, Ldtk;

    const-string v1, "GESTURE"

    invoke-direct {v0, v1, v5}, Ldtk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtk;->bKB:Ldtk;

    .line 387
    new-instance v0, Ldtk;

    const-string v1, "PREDICTION_OR_AUTOCOMMIT"

    invoke-direct {v0, v1, v6}, Ldtk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtk;->bKC:Ldtk;

    .line 390
    new-instance v0, Ldtk;

    const-string v1, "RECORRECTION"

    invoke-direct {v0, v1, v7}, Ldtk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtk;->bKD:Ldtk;

    .line 396
    new-instance v0, Ldtk;

    const-string v1, "RECAPITALIZATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldtk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtk;->bKE:Ldtk;

    .line 371
    const/4 v0, 0x6

    new-array v0, v0, [Ldtk;

    sget-object v1, Ldtk;->bKz:Ldtk;

    aput-object v1, v0, v3

    sget-object v1, Ldtk;->bKA:Ldtk;

    aput-object v1, v0, v4

    sget-object v1, Ldtk;->bKB:Ldtk;

    aput-object v1, v0, v5

    sget-object v1, Ldtk;->bKC:Ldtk;

    aput-object v1, v0, v6

    sget-object v1, Ldtk;->bKD:Ldtk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldtk;->bKE:Ldtk;

    aput-object v2, v0, v1

    sput-object v0, Ldtk;->bKF:[Ldtk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldtk;
    .locals 1

    .prologue
    .line 371
    const-class v0, Ldtk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldtk;

    return-object v0
.end method

.method public static values()[Ldtk;
    .locals 1

    .prologue
    .line 371
    sget-object v0, Ldtk;->bKF:[Ldtk;

    invoke-virtual {v0}, [Ldtk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldtk;

    return-object v0
.end method


# virtual methods
.method public final afV()Z
    .locals 1

    .prologue
    .line 399
    sget-object v0, Ldtk;->bKB:Ldtk;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
