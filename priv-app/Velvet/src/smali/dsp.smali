.class public final Ldsp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldsk;


# instance fields
.field private Oq:I

.field private final bIM:Landroid/animation/ValueAnimator;

.field private final bIN:Landroid/animation/ValueAnimator;

.field public final bIO:Ldss;

.field bIP:Landroid/view/View;

.field bIQ:F

.field bIR:F

.field bIS:Lcom/google/android/search/searchplate/SearchPlate;

.field private bIT:Landroid/view/View;

.field private final bIU:Z


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/View;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x15e

    const/4 v3, 0x2

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Ldsp;->Oq:I

    .line 37
    invoke-virtual {p1}, Lcom/google/android/search/searchplate/SearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Ldsp;->bIU:Z

    .line 42
    new-instance v1, Ldss;

    iget-boolean v2, p0, Ldsp;->bIU:Z

    invoke-direct {v1, v0, v2}, Ldss;-><init>(Landroid/content/res/Resources;Z)V

    iput-object v1, p0, Ldsp;->bIO:Ldss;

    .line 43
    iput-object p2, p0, Ldsp;->bIT:Landroid/view/View;

    .line 44
    iget-object v0, p0, Ldsp;->bIT:Landroid/view/View;

    iget-object v1, p0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 46
    iput-object p1, p0, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    .line 47
    iget-object v0, p0, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-boolean v1, p0, Ldsp;->bIU:Z

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dU(Z)V

    .line 48
    iget-object v0, p0, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p0}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldsk;)V

    .line 50
    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Ldsp;->bIM:Landroid/animation/ValueAnimator;

    .line 51
    iget-object v0, p0, Ldsp;->bIM:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 52
    iget-object v0, p0, Ldsp;->bIM:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 53
    iget-object v0, p0, Ldsp;->bIM:Landroid/animation/ValueAnimator;

    new-instance v1, Ldsq;

    invoke-direct {v1, p0}, Ldsq;-><init>(Ldsp;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 73
    new-array v0, v3, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Ldsp;->bIN:Landroid/animation/ValueAnimator;

    .line 74
    iget-object v0, p0, Ldsp;->bIN:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 75
    iget-object v0, p0, Ldsp;->bIN:Landroid/animation/ValueAnimator;

    new-instance v1, Ldsr;

    invoke-direct {v1, p0}, Ldsr;-><init>(Ldsp;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 81
    return-void

    .line 50
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 73
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private go(I)Z
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Ldsp;->Oq:I

    invoke-static {v0}, Ldtd;->gu(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Ldsp;->Oq:I

    invoke-static {v0}, Ldtd;->gv(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p1}, Ldtd;->gr(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final afw()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public final w(IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 118
    iget-boolean v0, p0, Ldsp;->bIU:Z

    if-eqz v0, :cond_c

    .line 119
    iget-object v3, p0, Ldsp;->bIO:Ldss;

    iget v0, p0, Ldsp;->Oq:I

    invoke-static {v0}, Ldtd;->gv(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Ldsp;->go(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Ldss;->eb(Z)V

    .line 120
    invoke-direct {p0, p1}, Ldsp;->go(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    if-eqz p2, :cond_3

    .line 122
    iget-object v0, p0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, v5, v5}, Ldss;->r(FF)V

    .line 153
    :cond_1
    :goto_1
    iput p1, p0, Ldsp;->Oq:I

    .line 157
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 119
    goto :goto_0

    .line 124
    :cond_3
    iput v4, p0, Ldsp;->bIQ:F

    .line 125
    iput v4, p0, Ldsp;->bIR:F

    .line 126
    iget-object v0, p0, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const v1, 0x7f11022f

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldsp;->bIP:Landroid/view/View;

    .line 127
    iget-object v0, p0, Ldsp;->bIM:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_1

    .line 129
    :cond_4
    iget v0, p0, Ldsp;->Oq:I

    invoke-static {v0}, Ldtd;->gr(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Ldtd;->gu(I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_7

    .line 130
    if-eqz p2, :cond_6

    .line 131
    iget-object v0, p0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, v4, v5}, Ldss;->r(FF)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 129
    goto :goto_3

    .line 133
    :cond_6
    iput v4, p0, Ldsp;->bIQ:F

    .line 134
    iput v4, p0, Ldsp;->bIR:F

    .line 135
    iget-object v0, p0, Ldsp;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const v1, 0x7f1103bc

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldsp;->bIP:Landroid/view/View;

    .line 138
    iget-object v0, p0, Ldsp;->bIM:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_1

    .line 140
    :cond_7
    invoke-static {p1}, Ldtd;->gt(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 141
    if-eqz p2, :cond_8

    .line 142
    iget-object v0, p0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, v5, v5}, Ldss;->r(FF)V

    goto :goto_1

    .line 143
    :cond_8
    iget v0, p0, Ldsp;->Oq:I

    invoke-static {v0}, Ldtd;->gu(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Ldsp;->bIN:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_1

    .line 146
    :cond_9
    iget v0, p0, Ldsp;->Oq:I

    invoke-static {v0}, Ldtd;->gt(I)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p1}, Ldtd;->gu(I)Z

    move-result v0

    if-eqz v0, :cond_a

    :goto_4
    if-eqz v2, :cond_1

    .line 147
    if-eqz p2, :cond_b

    .line 148
    iget-object v0, p0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, v4, v5}, Ldss;->r(FF)V

    goto :goto_1

    :cond_a
    move v2, v1

    .line 146
    goto :goto_4

    .line 150
    :cond_b
    iget-object v0, p0, Ldsp;->bIN:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_1

    .line 155
    :cond_c
    iget-object v0, p0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, v2}, Ldss;->eb(Z)V

    goto/16 :goto_2
.end method
