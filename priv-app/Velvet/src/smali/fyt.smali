.class final Lfyt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private synthetic cDu:Lfym;

.field private synthetic cDw:Landroid/view/View;

.field private synthetic cxa:Landroid/widget/PopupWindow;


# direct methods
.method constructor <init>(Lfym;Landroid/view/View;Landroid/widget/PopupWindow;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lfyt;->cDu:Lfym;

    iput-object p2, p0, Lfyt;->cDw:Landroid/view/View;

    iput-object p3, p0, Lfyt;->cxa:Landroid/widget/PopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 450
    if-eqz p3, :cond_0

    .line 453
    mul-int/lit8 v0, p2, 0x5

    .line 454
    iget-object v1, p0, Lfyt;->cDu:Lfym;

    iget-object v2, p0, Lfyt;->cDw:Landroid/view/View;

    invoke-static {v1, v2, v0}, Lfym;->a(Lfym;Landroid/view/View;I)V

    .line 456
    :cond_0
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 446
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    .prologue
    .line 437
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    .line 438
    iget-object v1, p0, Lfyt;->cDu:Lfym;

    iget-object v2, p0, Lfyt;->cDw:Landroid/view/View;

    invoke-static {v1, v2, v0}, Lfym;->a(Lfym;Landroid/view/View;I)V

    .line 439
    iget-object v1, p0, Lfyt;->cDu:Lfym;

    invoke-virtual {v1, v0}, Lfym;->jN(I)V

    .line 440
    iget-object v0, p0, Lfyt;->cxa:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 441
    iget-object v0, p0, Lfyt;->cDu:Lfym;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lfym;->a(Lfym;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;

    .line 442
    return-void
.end method
