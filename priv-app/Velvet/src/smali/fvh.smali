.class public final Lfvh;
.super Lfuz;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 52
    iput-object p4, p0, Lfvh;->mPhotoWithAttributionDecorator:Lgbd;

    .line 53
    return-void
.end method

.method private bc(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 234
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSK:Lizr;

    .line 235
    invoke-virtual {v0}, Lizr;->nk()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    .line 236
    const/16 v6, 0x12

    move-object v1, p1

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private oo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 226
    iget-object v1, v0, Lizj;->dUu:Ljdv;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v1}, Ljdv;->bhO()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhN()Ljava/lang/String;

    move-result-object v0

    .line 229
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lizj;->dSK:Lizr;

    invoke-virtual {v0}, Lizr;->bdu()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 11

    .prologue
    .line 71
    new-instance v3, Lang;

    invoke-direct {v3}, Lang;-><init>()V

    .line 72
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSK:Lizr;

    iget-object v1, v0, Lizr;->aiX:Ljcn;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lizr;->aiX:Ljcn;

    invoke-virtual {v1}, Ljcn;->qG()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_0
    const/4 v0, 0x1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_a

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v0, Lizj;->dSK:Lizr;

    const/4 v0, 0x0

    iget-object v5, v1, Lizr;->aeB:Ljbp;

    if-eqz v5, :cond_1

    iget-object v0, v1, Lizr;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-direct {p0, p1}, Lfvh;->bc(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    new-instance v6, Lano;

    invoke-direct {v6}, Lano;-><init>()V

    new-instance v7, Lanh;

    invoke-direct {v7}, Lanh;-><init>()V

    const/16 v8, 0x25

    invoke-virtual {v7, v8}, Lanh;->cm(I)Lanh;

    iput-object v6, v7, Lanh;->ahb:Lano;

    iget-object v8, p0, Lfvh;->mEntry:Lizj;

    iput-object v8, v7, Lanh;->ahu:Lizj;

    invoke-virtual {v1}, Lizr;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lano;->aF(Ljava/lang/String;)Lano;

    const-string v8, " \u00b7 "

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/CharSequence;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v0, 0x1

    aput-object v5, v9, v0

    invoke-static {v8, v9}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v6, v0}, Lano;->aG(Ljava/lang/String;)Lano;

    :cond_2
    invoke-direct {p0}, Lfvh;->oo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v6, v0}, Lano;->aH(Ljava/lang/String;)Lano;

    :cond_3
    iget-object v0, p0, Lfvh;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v1, v1, Lizr;->aiX:Ljcn;

    const v5, 0x7f0d012b

    const v8, 0x7f0d012c

    invoke-virtual {v0, p1, v1, v5, v8}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v0

    iput-object v0, v6, Lano;->ahM:Laoi;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    if-nez v2, :cond_c

    const/4 v0, 0x1

    :goto_2
    iget-object v5, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v5, Lizj;->dSK:Lizr;

    iget-object v6, v1, Lizr;->aeB:Ljbp;

    if-nez v6, :cond_d

    const/4 v0, 0x0

    :goto_3
    invoke-static {v4, v0}, Lfvh;->a(Ljava/util/List;Ljava/lang/Object;)V

    if-nez v2, :cond_10

    const/4 v0, 0x1

    :goto_4
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v2, Lizj;->dSK:Lizr;

    iget-object v5, v1, Lizr;->dNE:Lixx;

    if-eqz v5, :cond_4

    iget-object v5, v1, Lizr;->dNE:Lixx;

    invoke-virtual {v5}, Lixx;->baX()Z

    move-result v5

    if-nez v5, :cond_11

    :cond_4
    const/4 v0, 0x0

    :goto_5
    invoke-static {v4, v0}, Lfvh;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v3, Lang;->ags:[Lanh;

    .line 73
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lang;->aS(Z)Lang;

    .line 74
    return-object v3

    .line 72
    :cond_5
    iget-object v0, v0, Lizr;->aiX:Ljcn;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ic_concert-generic.png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_6
    const-string v1, "ic_album-generic.png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_7
    const-string v1, "ic_movie-generic.png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_8
    const-string v1, "ic_tv-generic.png"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSK:Lizr;

    iget-object v1, p0, Lfvh;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v0, v0, Lizr;->aiX:Ljcn;

    const v5, 0x7f0d0172

    const v6, 0x7f0d016d

    invoke-virtual {v1, p1, v0, v5, v6}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v0

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    iget-object v5, p0, Lfvh;->mEntry:Lizj;

    iput-object v5, v1, Lanh;->ahu:Lizj;

    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Lanh;->cm(I)Lanh;

    new-instance v5, Lanz;

    invoke-direct {v5}, Lanz;-><init>()V

    iput-object v5, v1, Lanh;->agy:Lanz;

    iget-object v5, v1, Lanh;->agy:Lanz;

    iput-object v0, v5, Lanz;->aiU:Laoi;

    iget-object v0, v1, Lanh;->agy:Lanz;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lanz;->ba(Z)Lanz;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v5, v1, Lizj;->dSK:Lizr;

    new-instance v6, Lftn;

    invoke-virtual {v5}, Lizr;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Lftn;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iget-object v7, v5, Lizr;->aeB:Ljbp;

    if-eqz v7, :cond_b

    iget-object v0, v5, Lizr;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_b
    invoke-direct {p0, p1}, Lfvh;->bc(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    const-string v7, " \u00b7 "

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/CharSequence;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    aput-object v5, v8, v0

    invoke-static {v7, v8}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lftn;->ceJ:Ljava/lang/String;

    invoke-direct {p0}, Lfvh;->oo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lftn;->cBi:Ljava/lang/String;

    invoke-virtual {v6}, Lftn;->aDh()Lanh;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lanh;->aW(Z)Lanh;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_d
    iget-object v6, v1, Lizr;->aeB:Ljbp;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static {v6, v7, v8, v9, v10}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lfsa;

    const/16 v8, 0x38

    invoke-direct {v7, v8}, Lfsa;-><init>(I)V

    const v8, 0x7f0201b8

    invoke-virtual {v7, v8}, Lfsa;->jB(I)Lfsa;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v6, v8}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v6

    new-instance v7, Laon;

    invoke-direct {v7}, Laon;-><init>()V

    iget-object v8, v1, Lizr;->aeB:Ljbp;

    invoke-virtual {v8}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_f

    iget-object v9, v1, Lizr;->aeB:Ljbp;

    invoke-virtual {v9}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_e

    invoke-virtual {v7, v8}, Laon;->ca(Ljava/lang/String;)Laon;

    iget-object v1, v1, Lizr;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Laon;->cb(Ljava/lang/String;)Laon;

    :goto_6
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/16 v8, 0xc

    invoke-virtual {v1, v8}, Lanh;->cm(I)Lanh;

    iput-object v7, v1, Lanh;->agA:Laon;

    iput-object v6, v1, Lanh;->ahs:Lani;

    iput-object v5, v1, Lanh;->ahu:Lizj;

    invoke-virtual {v1, v0}, Lanh;->aW(Z)Lanh;

    invoke-virtual {v1, v0}, Lanh;->aX(Z)Lanh;

    move-object v0, v1

    goto/16 :goto_3

    :cond_e
    invoke-virtual {v7, v8}, Laon;->cb(Ljava/lang/String;)Laon;

    goto :goto_6

    :cond_f
    iget-object v1, v1, Lizr;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Laon;->cb(Ljava/lang/String;)Laon;

    goto :goto_6

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_11
    new-instance v5, Laoj;

    invoke-direct {v5}, Laoj;-><init>()V

    const v6, 0x7f0a0360

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v6, Lfsa;

    const/4 v7, 0x3

    invoke-direct {v6, v7}, Lfsa;-><init>(I)V

    const v7, 0x7f0200ed

    invoke-virtual {v6, v7}, Lfsa;->jB(I)Lfsa;

    move-result-object v6

    iget-object v1, v1, Lizr;->dNE:Lixx;

    invoke-virtual {v1}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v6, v1, v7}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v6

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/4 v7, 0x3

    invoke-virtual {v1, v7}, Lanh;->cm(I)Lanh;

    iput-object v5, v1, Lanh;->agz:Laoj;

    iput-object v6, v1, Lanh;->ahs:Lani;

    iput-object v2, v1, Lanh;->ahu:Lizj;

    invoke-virtual {v1, v0}, Lanh;->aW(Z)Lanh;

    invoke-virtual {v1, v0}, Lanh;->aX(Z)Lanh;

    move-object v0, v1

    goto/16 :goto_5
.end method

.method public final bb(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSK:Lizr;

    iget-object v0, v0, Lizr;->aiX:Ljcn;

    .line 62
    if-eqz v0, :cond_0

    .line 63
    iget-object v1, p0, Lfvh;->mPhotoWithAttributionDecorator:Lgbd;

    const v2, 0x7f0d0220

    const v3, 0x7f0d0214

    invoke-virtual {v1, p1, v0, v2, v3}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
