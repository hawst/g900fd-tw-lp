.class public final Ljkr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eqc:J

.field private eqd:I

.field private eqe:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8478
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8479
    iput v2, p0, Ljkr;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljkr;->eqc:J

    iput v2, p0, Ljkr;->eqd:I

    iput-boolean v2, p0, Ljkr;->eqe:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljkr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkr;->eCz:I

    .line 8480
    return-void
.end method


# virtual methods
.method public final Pu()J
    .locals 2

    .prologue
    .line 8424
    iget-wide v0, p0, Ljkr;->eqc:J

    return-wide v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljkr;->eqc:J

    iget v0, p0, Ljkr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljkr;->eqd:I

    iget v0, p0, Ljkr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkr;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljkr;->eqe:Z

    iget v0, p0, Ljkr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkr;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 8495
    iget v0, p0, Ljkr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8496
    const/4 v0, 0x1

    iget-wide v2, p0, Ljkr;->eqc:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 8498
    :cond_0
    iget v0, p0, Ljkr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 8499
    const/4 v0, 0x2

    iget v1, p0, Ljkr;->eqd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 8501
    :cond_1
    iget v0, p0, Ljkr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 8502
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljkr;->eqe:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 8504
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8505
    return-void
.end method

.method public final bom()Z
    .locals 1

    .prologue
    .line 8432
    iget v0, p0, Ljkr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bon()I
    .locals 1

    .prologue
    .line 8443
    iget v0, p0, Ljkr;->eqd:I

    return v0
.end method

.method public final boo()Z
    .locals 1

    .prologue
    .line 8451
    iget v0, p0, Ljkr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bop()Z
    .locals 1

    .prologue
    .line 8462
    iget-boolean v0, p0, Ljkr;->eqe:Z

    return v0
.end method

.method public final dz(J)Ljkr;
    .locals 1

    .prologue
    .line 8427
    iput-wide p1, p0, Ljkr;->eqc:J

    .line 8428
    iget v0, p0, Ljkr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkr;->aez:I

    .line 8429
    return-object p0
.end method

.method public final ir(Z)Ljkr;
    .locals 1

    .prologue
    .line 8465
    iput-boolean p1, p0, Ljkr;->eqe:Z

    .line 8466
    iget v0, p0, Ljkr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkr;->aez:I

    .line 8467
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 8509
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8510
    iget v1, p0, Ljkr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8511
    const/4 v1, 0x1

    iget-wide v2, p0, Ljkr;->eqc:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8514
    :cond_0
    iget v1, p0, Ljkr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 8515
    const/4 v1, 0x2

    iget v2, p0, Ljkr;->eqd:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8518
    :cond_1
    iget v1, p0, Ljkr;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 8519
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljkr;->eqe:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8522
    :cond_2
    return v0
.end method

.method public final qw(I)Ljkr;
    .locals 1

    .prologue
    .line 8446
    iput p1, p0, Ljkr;->eqd:I

    .line 8447
    iget v0, p0, Ljkr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkr;->aez:I

    .line 8448
    return-object p0
.end method
