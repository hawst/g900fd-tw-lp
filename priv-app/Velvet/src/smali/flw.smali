.class public Lflw;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Leko;
.implements Lekq;
.implements Lfnc;
.implements Lfnl;


# instance fields
.field private Fd:Landroid/view/View;

.field private bFk:Lfmq;

.field private cvA:I

.field private cvB:Z

.field private cvC:Z

.field private cvD:Z

.field cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field cvp:Lcom/google/android/shared/ui/CoScrollContainer;

.field private cvq:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

.field private cvr:Landroid/view/View;

.field private cvs:Lfrg;

.field private cvt:Lcom/google/android/search/shared/ui/WebImageView;

.field cvu:Z

.field private cvv:Landroid/view/animation/Interpolator;

.field public cvw:Lflr;

.field private cvx:Lfkv;

.field private cvy:Z

.field private cvz:I

.field mActivity:Landroid/app/Activity;

.field private mNowRemoteClient:Lfml;

.field private mRefreshManager:Lfmv;

.field private mUndoDismissManager:Lfnn;

.field private yW:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 128
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 115
    iput-boolean v0, p0, Lflw;->cvC:Z

    .line 116
    iput-boolean v0, p0, Lflw;->cvD:Z

    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 124
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 115
    iput-boolean v0, p0, Lflw;->cvC:Z

    .line 116
    iput-boolean v0, p0, Lflw;->cvD:Z

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 120
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    iput-boolean v0, p0, Lflw;->cvC:Z

    .line 116
    iput-boolean v0, p0, Lflw;->cvD:Z

    .line 121
    return-void
.end method


# virtual methods
.method public final Cr()V
    .locals 2

    .prologue
    .line 749
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0}, Lcom/google/android/search/shared/ui/WebImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v1}, Lcom/google/android/search/shared/ui/WebImageView;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 750
    return-void
.end method

.method public final Cs()V
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflw;->a(Livq;)V

    .line 755
    return-void
.end method

.method public final D(F)V
    .locals 6

    .prologue
    const/16 v1, 0x8

    .line 436
    const v0, 0x358637bd    # 1.0E-6f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 438
    invoke-virtual {p0, v1}, Lflw;->setVisibility(I)V

    .line 452
    :goto_0
    return-void

    .line 442
    :cond_0
    invoke-virtual {p0}, Lflw;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 444
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflw;->setVisibility(I)V

    .line 446
    :cond_1
    iget-object v0, p0, Lflw;->cvv:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 448
    iget-object v1, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setAlpha(F)V

    .line 449
    iget-object v1, p0, Lflw;->cvq:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->setAlpha(F)V

    .line 451
    invoke-virtual {p0}, Lflw;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v0, v2

    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v0, v2

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public final a(JLfzw;Lemp;)V
    .locals 3

    .prologue
    .line 729
    iget-object v0, p0, Lflw;->cvw:Lflr;

    new-instance v1, Lfmc;

    invoke-direct {v1, p0}, Lfmc;-><init>(Lflw;)V

    invoke-virtual {v0, v1}, Lflr;->b(Lifw;)Lfkd;

    move-result-object v0

    .line 738
    if-nez v0, :cond_0

    .line 739
    new-instance v0, Lfkq;

    invoke-direct {v0, p3, p4, p1, p2}, Lfkq;-><init>(Lfzw;Lemp;J)V

    .line 740
    iget-object v1, p0, Lflw;->cvw:Lflr;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lflr;->a(Lfkd;I)V

    .line 742
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Leoj;Lfmv;Lflr;Lfml;Lfnn;Lfkv;)V
    .locals 4

    .prologue
    .line 278
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lflw;->mActivity:Landroid/app/Activity;

    .line 279
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmv;

    iput-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    .line 281
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflr;

    iput-object v0, p0, Lflw;->cvw:Lflr;

    .line 282
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfml;

    iput-object v0, p0, Lflw;->mNowRemoteClient:Lfml;

    .line 283
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnn;

    iput-object v0, p0, Lflw;->mUndoDismissManager:Lfnn;

    .line 284
    iput-object p7, p0, Lflw;->cvx:Lfkv;

    .line 289
    iget-object v0, p0, Lflw;->cvx:Lfkv;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lfkv;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-static {p1}, Leot;->av(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Lflw;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Lflr;->cvk:I

    .line 295
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;ZLjava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 615
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 616
    if-eqz p3, :cond_0

    .line 617
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    new-instance v1, Lflz;

    invoke-direct {v1, p0, p3}, Lflz;-><init>(Lflw;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 623
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setClickable(Z)V

    .line 625
    :cond_0
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 628
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/search/shared/ui/WebImageView;->setFocusableInTouchMode(Z)V

    .line 629
    if-eqz p2, :cond_1

    .line 630
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {p0}, Lflw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a064e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 635
    :goto_0
    return-void

    .line 633
    :cond_1
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Lejw;)V
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->a(Lejw;)V

    .line 640
    return-void
.end method

.method public final a(Lfkd;I)V
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1, p2}, Lflr;->a(Lfkd;I)V

    .line 590
    return-void
.end method

.method public final a(Livq;)V
    .locals 2
    .param p1    # Livq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 760
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0}, Lcom/google/android/search/shared/ui/WebImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 761
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 518
    iget-object v0, p0, Lflw;->cvw:Lflr;

    const/4 v4, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move v7, v3

    move v8, v3

    invoke-virtual/range {v0 .. v8}, Lflr;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    .line 526
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    iput-object v5, v0, Lfmv;->cwG:Lizj;

    .line 527
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;Z)V
    .locals 9
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 536
    iget-object v0, p0, Lflw;->cvw:Lflr;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move-object v6, p3

    move v7, p4

    move v8, v3

    invoke-virtual/range {v0 .. v8}, Lflr;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    .line 544
    return-void
.end method

.method public final aAR()Z
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lflw;->cvB:Z

    return v0
.end method

.method public final aAS()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 380
    iget-boolean v0, p0, Lflw;->cvu:Z

    if-eqz v0, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflw;->cvu:Z

    .line 386
    invoke-virtual {p0}, Lflw;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 388
    invoke-virtual {p0, v2}, Lflw;->setVisibility(I)V

    .line 391
    :cond_2
    iget-object v0, p0, Lflw;->bFk:Lfmq;

    if-nez v0, :cond_3

    .line 392
    iget-object v0, p0, Lflw;->mNowRemoteClient:Lfml;

    const-string v1, "NowClientCardsView"

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Lflw;->bFk:Lfmq;

    .line 393
    iget-object v0, p0, Lflw;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 396
    :cond_3
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBn()V

    .line 397
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBp()V

    .line 398
    iget-object v0, p0, Lflw;->cvx:Lfkv;

    invoke-virtual {v0, v2}, Lfkv;->setVisibility(I)V

    .line 400
    iget-boolean v0, p0, Lflw;->cvD:Z

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    iget-object v1, p0, Lflw;->cvs:Lfrg;

    invoke-virtual {v0, v1}, Lfmv;->a(Lfrg;)V

    goto :goto_0
.end method

.method public final aAT()V
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeAllViews()V

    .line 549
    invoke-virtual {p0}, Lflw;->aAX()V

    .line 550
    return-void
.end method

.method public final aAU()Z
    .locals 1

    .prologue
    .line 579
    const/4 v0, 0x1

    return v0
.end method

.method public final aAV()Z
    .locals 1

    .prologue
    .line 609
    const/4 v0, 0x1

    return v0
.end method

.method public final aAW()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lflw;->cvs:Lfrg;

    invoke-interface {v0}, Lfrg;->start()V

    .line 645
    return-void
.end method

.method public final aAX()V
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lflw;->cvs:Lfrg;

    invoke-interface {v0}, Lfrg;->stop()V

    .line 650
    return-void
.end method

.method public final aAY()V
    .locals 4

    .prologue
    .line 660
    invoke-virtual {p0}, Lflw;->aAT()V

    .line 662
    invoke-virtual {p0}, Lflw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e1

    iget-object v2, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 665
    const v0, 0x7f1101f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 666
    new-instance v2, Lfma;

    invoke-direct {v2, p0}, Lfma;-><init>(Lflw;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 681
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-static {v1}, Lflr;->bm(Landroid/view/View;)V

    .line 682
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->addView(Landroid/view/View;)V

    .line 683
    return-void
.end method

.method final aAZ()V
    .locals 2

    .prologue
    .line 690
    invoke-virtual {p0}, Lflw;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 691
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflw;->setVisibility(I)V

    .line 692
    invoke-virtual {p0}, Lflw;->requestLayout()V

    .line 693
    new-instance v0, Lfmb;

    invoke-direct {v0, p0}, Lfmb;-><init>(Lflw;)V

    invoke-virtual {p0, v0}, Lflw;->post(Ljava/lang/Runnable;)Z

    .line 702
    :cond_0
    return-void
.end method

.method public final ak(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->ak(Landroid/view/View;)V

    .line 491
    return-void
.end method

.method protected final atL()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 705
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 706
    iget-object v1, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v1, v0}, Lflr;->U(Landroid/os/Bundle;)V

    .line 707
    return-object v0
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 200
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lflw;->yW:Landroid/graphics/Rect;

    .line 201
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->ib(I)V

    .line 202
    iget-object v0, p0, Lflw;->mUndoDismissManager:Lfnn;

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1}, Lfnn;->ib(I)V

    .line 203
    return-void
.end method

.method public final bo(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 774
    iget-object v0, p0, Lflw;->Fd:Landroid/view/View;

    if-eq v0, p1, :cond_0

    .line 775
    iput-object p1, p0, Lflw;->Fd:Landroid/view/View;

    .line 776
    invoke-virtual {p0}, Lflw;->invalidate()V

    .line 778
    :cond_0
    return-void
.end method

.method public final c(Lizj;Ljava/util/Collection;)V
    .locals 1
    .param p2    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 599
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1, p2}, Lflr;->c(Lizj;Ljava/util/Collection;)V

    .line 600
    return-void
.end method

.method public final cN()Z
    .locals 3

    .prologue
    .line 460
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0}, Lflr;->aAQ()Ljava/lang/String;

    move-result-object v0

    .line 461
    if-eqz v0, :cond_0

    .line 462
    iget-object v1, p0, Lflw;->mNowRemoteClient:Lfml;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->aJ(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 463
    const/4 v0, 0x1

    .line 465
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lfkd;)V
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->d(Lfkd;)V

    .line 605
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 782
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 785
    iget-object v0, p0, Lflw;->Fd:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lflw;->Fd:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 788
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lflw;->mUndoDismissManager:Lfnn;

    invoke-virtual {v0, p1}, Lfnn;->l(Landroid/view/MotionEvent;)V

    .line 655
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final ex(I)V
    .locals 4

    .prologue
    .line 554
    invoke-virtual {p0}, Lflw;->aAT()V

    .line 556
    invoke-virtual {p0}, Lflw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040080

    iget-object v2, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 558
    const v0, 0x7f1101b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 560
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 564
    const v0, 0x7f1101b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 565
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 567
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-static {v1}, Lflr;->bm(Landroid/view/View;)V

    .line 568
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->addView(Landroid/view/View;)V

    .line 572
    invoke-virtual {p0}, Lflw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 573
    return-void
.end method

.method final fp(Z)V
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lflw;->cvy:Z

    if-ne p1, v0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    iput-boolean p1, p0, Lflw;->cvy:Z

    .line 235
    if-nez p1, :cond_0

    .line 236
    invoke-virtual {p0}, Lflw;->requestLayout()V

    .line 237
    invoke-virtual {p0}, Lflw;->invalidate()V

    goto :goto_0
.end method

.method public final fq(Z)V
    .locals 2

    .prologue
    .line 302
    iget-boolean v0, p0, Lflw;->cvB:Z

    if-ne v0, p1, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iput-boolean p1, p0, Lflw;->cvB:Z

    .line 306
    iget-boolean v0, p0, Lflw;->cvB:Z

    if-eqz v0, :cond_3

    .line 307
    invoke-virtual {p0}, Lflw;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflw;->fp(Z)V

    :cond_2
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {p0}, Lflw;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lflx;

    invoke-direct {v1, p0}, Lflx;-><init>(Lflw;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lflw;->cvq:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-static {v0}, Lelv;->ba(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-static {v0}, Lelv;->ba(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lflw;->cvw:Lflr;

    iget-object v1, v0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v1}, Lfkz;->aAH()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v0}, Lfkz;->aAG()Z

    goto :goto_0

    .line 309
    :cond_3
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lfly;

    invoke-direct {v1, p0}, Lfly;-><init>(Lflw;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lflw;->cvq:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-static {v0}, Lelv;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-static {v0}, Lelv;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0
.end method

.method public final fr(Z)V
    .locals 2

    .prologue
    .line 406
    iget-boolean v0, p0, Lflw;->cvu:Z

    if-nez v0, :cond_1

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflw;->cvu:Z

    .line 412
    if-nez p1, :cond_2

    .line 413
    iget-object v0, p0, Lflw;->cvw:Lflr;

    iget-object v0, v0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v0}, Lfkz;->aAG()Z

    .line 415
    :cond_2
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBo()V

    .line 416
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBq()V

    .line 417
    iget-object v0, p0, Lflw;->cvx:Lfkv;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lfkv;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lflw;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBh()V

    .line 420
    iget-object v0, p0, Lflw;->bFk:Lfmq;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lflw;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, Lflw;->bFk:Lfmq;

    goto :goto_0
.end method

.method protected final g(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 711
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 712
    check-cast p1, Landroid/os/Bundle;

    .line 713
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->V(Landroid/os/Bundle;)V

    .line 717
    :goto_0
    return-void

    .line 715
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final i(Lizj;Lizj;)V
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lflw;->cvw:Lflr;

    invoke-virtual {v0, p1, p2}, Lflr;->g(Lizj;Lizj;)V

    .line 585
    return-void
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lflw;->cvu:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 133
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 135
    const v0, 0x7f11025e

    invoke-virtual {p0, v0}, Lflw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SuggestionGridLayout;

    iput-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 136
    const v0, 0x7f11025c

    invoke-virtual {p0, v0}, Lflw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    iput-object v0, p0, Lflw;->cvp:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 137
    const v0, 0x7f11025f

    invoke-virtual {p0, v0}, Lflw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    iput-object v0, p0, Lflw;->cvq:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    .line 138
    const v0, 0x7f110243

    invoke-virtual {p0, v0}, Lflw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflw;->cvr:Landroid/view/View;

    .line 139
    const v0, 0x7f110297

    invoke-virtual {p0, v0}, Lflw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    .line 143
    iget-object v0, p0, Lflw;->cvr:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflw;->cvr:Landroid/view/View;

    check-cast v0, Lfrg;

    :goto_0
    iput-object v0, p0, Lflw;->cvs:Lfrg;

    .line 147
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Lekq;)V

    .line 148
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Leko;)V

    .line 152
    invoke-virtual {p0}, Lflw;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Leot;->av(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 153
    iget-object v1, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v1}, Lcom/google/android/search/shared/ui/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 154
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v2, v0, :cond_0

    .line 155
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 156
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    iget-object v0, p0, Lflw;->cvt:Lcom/google/android/search/shared/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 160
    :cond_0
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflw;->cvv:Landroid/view/animation/Interpolator;

    .line 164
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    iget-boolean v0, p0, Lflw;->cvC:Z

    if-eqz v0, :cond_1

    .line 165
    invoke-virtual {p0}, Lflw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 167
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lflw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 168
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 170
    const/16 v3, 0x50

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 171
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    const v2, 0x7f02025b

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 173
    invoke-virtual {p0, v1}, Lflw;->addView(Landroid/view/View;)V

    .line 175
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lflw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 176
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 178
    const/16 v0, 0x30

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 179
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    const v0, 0x7f020302

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 181
    invoke-virtual {p0, v1}, Lflw;->addView(Landroid/view/View;)V

    .line 183
    :cond_1
    return-void

    .line 143
    :cond_2
    iget-object v0, p0, Lflw;->cvq:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lflw;->cvy:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 243
    iget-boolean v0, p0, Lflw;->cvy:Z

    if-eqz v0, :cond_1

    .line 244
    iget v0, p0, Lflw;->cvz:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lflw;->cvA:I

    if-ne p2, v0, :cond_0

    .line 248
    invoke-virtual {p0}, Lflw;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lflw;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflw;->setMeasuredDimension(II)V

    .line 258
    :goto_0
    return-void

    .line 251
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflw;->cvy:Z

    .line 253
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 256
    iput p1, p0, Lflw;->cvz:I

    .line 257
    iput p2, p0, Lflw;->cvA:I

    goto :goto_0
.end method

.method public final w(Lizj;)V
    .locals 2

    .prologue
    .line 722
    iget-object v1, p0, Lflw;->cvw:Lflr;

    iget-object v0, p0, Lflw;->yW:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p1, v0}, Lflr;->i(Lizj;I)V

    .line 723
    return-void

    .line 722
    :cond_0
    iget-object v0, p0, Lflw;->yW:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method

.method public final w(ZZ)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->w(ZZ)V

    .line 227
    return-void
.end method

.method public final wi()Lcom/google/android/shared/ui/SuggestionGridLayout;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lflw;->cvo:Lcom/google/android/shared/ui/SuggestionGridLayout;

    return-object v0
.end method

.method public final wj()Lekf;
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lflw;->cvp:Lcom/google/android/shared/ui/CoScrollContainer;

    return-object v0
.end method

.method public final wk()Z
    .locals 1

    .prologue
    .line 485
    const/4 v0, 0x1

    return v0
.end method
