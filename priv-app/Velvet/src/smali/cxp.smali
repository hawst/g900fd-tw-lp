.class public final Lcxp;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final bmv:Z

.field private synthetic bmw:Lcxo;

.field private final mAppContext:Landroid/content/Context;

.field private final mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Lcxo;Landroid/content/Context;ZLfcx;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcxp;->bmw:Lcxo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 122
    iput-object p2, p0, Lcxp;->mAppContext:Landroid/content/Context;

    .line 123
    iput-boolean p3, p0, Lcxp;->bmv:Z

    .line 124
    iput-object p4, p0, Lcxp;->mNetworkClient:Lfcx;

    .line 125
    return-void
.end method

.method private varargs TF()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 131
    :try_start_0
    new-instance v0, Liyl;

    invoke-direct {v0}, Liyl;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Liyl;->hq(Z)Liyl;

    move-result-object v0

    .line 134
    iget-boolean v1, p0, Lcxp;->bmv:Z

    if-eqz v1, :cond_0

    .line 136
    new-instance v1, Ljcz;

    invoke-direct {v1}, Ljcz;-><init>()V

    .line 138
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljcz;->hF(Z)Ljcz;

    .line 139
    iput-object v1, v0, Liyl;->dQC:Ljcz;

    .line 142
    :cond_0
    const/16 v1, 0xc

    invoke-static {v1}, Lfjw;->iQ(I)Ljed;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljed;->hK(Z)Ljed;

    move-result-object v1

    .line 145
    iput-object v0, v1, Ljed;->edq:Liyl;

    .line 147
    iget-object v0, p0, Lcxp;->mNetworkClient:Lfcx;

    invoke-interface {v0, v1}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v0

    if-nez v0, :cond_1

    .line 150
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    .line 152
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    const-string v1, "OptOutWorkerFragment"

    const-string v2, "Exception while attempting to opt out"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 156
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Lcxp;->TF()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 111
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcxp;->bmw:Lcxo;

    invoke-virtual {v0}, Lcxo;->TE()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcxp;->mAppContext:Landroid/content/Context;

    const v1, 0x7f0a02f2

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcxp;->mAppContext:Landroid/content/Context;

    const v1, 0x7f0a02f3

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v1

    const-string v2, "OPT_OUT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    iget-object v1, p0, Lcxp;->bmw:Lcxo;

    iget-object v1, v1, Lcxo;->mAccount:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcin;->m(Landroid/accounts/Account;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
