.class public final Lemv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cfB:Lenk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lenk;

    invoke-direct {v0}, Lenk;-><init>()V

    sput-object v0, Lemv;->cfB:Lenk;

    return-void
.end method

.method static a(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZ)Lemw;
    .locals 7

    .prologue
    .line 81
    new-instance v0, Lemw;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v6}, Lemw;-><init>(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZZ)V

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/util/concurrent/ThreadFactory;Z)Lemw;
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 96
    new-instance v0, Lemw;

    const v2, 0x7fffffff

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p1

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lemw;-><init>(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZZ)V

    return-object v0
.end method

.method static b(ZLjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 869
    if-eqz p0, :cond_0

    .line 870
    const-string v0, "ConcurrentUtils"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 874
    :goto_0
    return-void

    .line 872
    :cond_0
    const-string v0, "ConcurrentUtils"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x5

    invoke-static {v2, v0, p1, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static d(Letj;)V
    .locals 1

    .prologue
    .line 858
    const-string v0, "All UI-thread tasks: logging disabled"

    invoke-virtual {p0, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 863
    return-void
.end method

.method public static lg(Ljava/lang/String;)Lemw;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 137
    invoke-static {p0}, Lemv;->lh(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v3

    .line 138
    new-instance v0, Lemw;

    const/4 v2, 0x1

    move-object v1, p0

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lemw;-><init>(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZZ)V

    return-object v0
.end method

.method private static lh(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 144
    new-instance v0, Livz;

    invoke-direct {v0}, Livz;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-%d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    iput-object v1, v0, Livz;->dLB:Ljava/lang/String;

    iget-object v2, v0, Livz;->dLB:Ljava/lang/String;

    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v1

    if-eqz v2, :cond_0

    new-instance v3, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v6, 0x0

    invoke-direct {v3, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    :goto_0
    new-instance v0, Liwa;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Liwa;-><init>(Ljava/util/concurrent/ThreadFactory;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicLong;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-object v0

    :cond_0
    move-object v3, v4

    goto :goto_0
.end method

.method public static q(Ljava/lang/String;Z)Lemw;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 118
    invoke-static {p0}, Lemv;->lh(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v3

    .line 119
    new-instance v0, Lemw;

    const/4 v4, 0x0

    move-object v1, p0

    move v5, p1

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lemw;-><init>(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZZ)V

    return-object v0
.end method
