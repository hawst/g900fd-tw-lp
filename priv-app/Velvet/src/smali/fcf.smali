.class public final Lfcf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private anR:Z

.field private final coB:Z

.field private final coC:Ligi;

.field private coD:Ljsr;

.field private final coz:Ljava/lang/String;

.field private final dK:Ljava/lang/Object;

.field private final mFileBytesReader:Lfcj;

.field private final mFileBytesWriter:Lfck;


# direct methods
.method public constructor <init>(Ligi;Ljava/lang/String;Lfcj;Lfck;Z)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfcf;->dK:Ljava/lang/Object;

    .line 49
    iput-object p1, p0, Lfcf;->coC:Ligi;

    .line 50
    iput-object p2, p0, Lfcf;->coz:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lfcf;->mFileBytesReader:Lfcj;

    .line 52
    iput-object p4, p0, Lfcf;->mFileBytesWriter:Lfck;

    .line 53
    iput-boolean p5, p0, Lfcf;->coB:Z

    .line 54
    return-void
.end method

.method private axr()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 119
    iget-boolean v0, p0, Lfcf;->anR:Z

    if-nez v0, :cond_2

    .line 121
    :try_start_0
    iget-object v0, p0, Lfcf;->coC:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    iget-boolean v1, p0, Lfcf;->coB:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfcf;->mFileBytesReader:Lfcj;

    iget-object v2, p0, Lfcf;->coz:Ljava/lang/String;

    const/high16 v3, 0x80000

    invoke-virtual {v1, v2, v3}, Lfcj;->B(Ljava/lang/String;I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    :try_start_1
    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_1
    :try_start_2
    iput-object v0, p0, Lfcf;->coD:Ljsr;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Lfcf;->coD:Ljsr;

    if-nez v0, :cond_1

    const-string v0, "FileBackedProto"

    const-string v1, "Failed to restore state from file. Reverting to empty proto"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lfcf;->coC:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    iput-object v0, p0, Lfcf;->coD:Ljsr;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 123
    :cond_1
    iput-boolean v5, p0, Lfcf;->anR:Z

    .line 126
    :cond_2
    return-void

    .line 121
    :cond_3
    :try_start_4
    iget-object v1, p0, Lfcf;->mFileBytesReader:Lfcj;

    iget-object v2, p0, Lfcf;->coz:Ljava/lang/String;

    const/high16 v3, 0x80000

    invoke-virtual {v1, v2, v3}, Lfcj;->A(Ljava/lang/String;I)[B

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "FileBackedProto"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading data from disk. Deleting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lfcf;->coz:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lfcf;->mFileBytesWriter:Lfck;

    iget-object v2, p0, Lfcf;->coz:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lfck;->lH(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v0

    :try_start_5
    iget-object v0, p0, Lfcf;->coD:Ljsr;

    if-nez v0, :cond_4

    const-string v0, "FileBackedProto"

    const-string v2, "Failed to restore state from file. Reverting to empty proto"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lfcf;->coC:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    iput-object v0, p0, Lfcf;->coD:Ljsr;

    :cond_4
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 123
    :catchall_1
    move-exception v0

    iput-boolean v5, p0, Lfcf;->anR:Z

    throw v0
.end method


# virtual methods
.method public final a(Lfcg;)V
    .locals 5

    .prologue
    .line 80
    invoke-static {}, Lenu;->auQ()V

    .line 82
    iget-object v1, p0, Lfcf;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    invoke-direct {p0}, Lfcf;->axr()V

    .line 85
    iget-object v2, p0, Lfcf;->coD:Ljsr;

    iget-object v0, p0, Lfcf;->coC:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v2, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    .line 86
    invoke-interface {p1, v0}, Lfcg;->aS(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 87
    if-eqz v0, :cond_0

    .line 88
    iput-object v0, p0, Lfcf;->coD:Ljsr;

    .line 89
    iget-boolean v0, p0, Lfcf;->coB:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfcf;->mFileBytesWriter:Lfck;

    iget-object v2, p0, Lfcf;->coz:Ljava/lang/String;

    iget-object v3, p0, Lfcf;->coD:Ljsr;

    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    const/high16 v4, 0x80000

    invoke-virtual {v0, v2, v3, v4}, Lfck;->a(Ljava/lang/String;[BI)Z

    .line 91
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lfcf;->mFileBytesWriter:Lfck;

    iget-object v2, p0, Lfcf;->coz:Ljava/lang/String;

    iget-object v3, p0, Lfcf;->coD:Ljsr;

    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfck;->h(Ljava/lang/String;[B)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axq()Ljsr;
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lenu;->auQ()V

    .line 67
    iget-object v1, p0, Lfcf;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    :try_start_0
    invoke-direct {p0}, Lfcf;->axr()V

    .line 69
    iget-object v0, p0, Lfcf;->coD:Ljsr;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axs()V
    .locals 3

    .prologue
    .line 172
    invoke-static {}, Lenu;->auQ()V

    .line 173
    iget-object v1, p0, Lfcf;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    iget-object v0, p0, Lfcf;->mFileBytesWriter:Lfck;

    iget-object v2, p0, Lfcf;->coz:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lfck;->lH(Ljava/lang/String;)V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lfcf;->coD:Ljsr;

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfcf;->anR:Z

    .line 177
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
