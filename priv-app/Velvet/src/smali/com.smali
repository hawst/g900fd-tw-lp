.class public final Lcom;
.super Lcol;
.source "PG"


# static fields
.field private static final bdK:[B

.field private static final bdL:[B

.field private static final bdM:[B

.field private static final bdN:[B

.field private static final bdO:[B

.field private static final bdP:[B

.field private static final bdQ:[B

.field private static final bdR:[B

.field private static final bdS:Ljava/nio/charset/Charset;


# instance fields
.field private final bdT:Ljava/io/InputStream;

.field private bdU:Ldlo;

.field private final bdV:Ljava/lang/String;

.field private final bdx:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x5

    .line 58
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom;->bdK:[B

    .line 59
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom;->bdL:[B

    .line 60
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom;->bdM:[B

    .line 61
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom;->bdN:[B

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom;->bdO:[B

    .line 63
    new-array v0, v2, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom;->bdP:[B

    .line 64
    new-array v0, v1, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom;->bdQ:[B

    .line 65
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom;->bdR:[B

    .line 70
    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    sput-object v0, Lcom;->bdS:Ljava/nio/charset/Charset;

    return-void

    .line 58
    nop

    :array_0
    .array-data 1
        0x65t
        0x63t
        0x74t
        0x63t
        0x6dt
    .end array-data

    .line 59
    nop

    :array_1
    .array-data 1
        0x65t
        0x63t
        0x74t
        0x61t
        0x69t
        0x73t
    .end array-data

    .line 60
    nop

    :array_2
    .array-data 1
        0x73t
        0x61t
        0x6ft
    .end array-data

    .line 61
    :array_3
    .array-data 1
        0x6dt
        0x6ft
        0x64t
        0x65t
        0x73t
    .end array-data

    .line 62
    nop

    :array_4
    .array-data 1
        0x66t
        0x70t
    .end array-data

    .line 63
    nop

    :array_5
    .array-data 1
        0x69t
        0x6ct
        0x70t
    .end array-data

    .line 64
    :array_6
    .array-data 1
        0x73t
        0x63t
        0x70t
        0x61t
        0x64t
    .end array-data

    .line 65
    nop

    :array_7
    .array-data 1
        0x73t
        0x65t
        0x61t
        0x72t
        0x63t
        0x68t
        0x5ft
        0x74t
        0x6ft
        0x6ft
        0x6ct
        0x73t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 124
    invoke-direct {p0}, Lcol;-><init>()V

    .line 125
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iput-object p1, p0, Lcom;->bdT:Ljava/io/InputStream;

    .line 131
    iput-object p2, p0, Lcom;->bdV:Ljava/lang/String;

    .line 132
    iput-object p3, p0, Lcom;->bdx:Ljava/lang/String;

    .line 133
    return-void
.end method

.method private QZ()Lcog;
    .locals 20

    .prologue
    .line 209
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    if-nez v1, :cond_0

    new-instance v1, Ldlo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom;->bdT:Ljava/io/InputStream;

    invoke-direct {v1, v2}, Ldlo;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom;->bdU:Ldlo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ldlo;->setLenient(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdV:Ljava/lang/String;

    if-eqz v1, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom;->bdV:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ldlo;->jP(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :cond_0
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 212
    const/4 v10, 0x0

    .line 213
    const/4 v9, 0x0

    .line 214
    const/4 v8, 0x0

    .line 215
    const/4 v7, 0x0

    .line 216
    const/4 v6, -0x1

    .line 217
    const/4 v5, 0x0

    .line 218
    const/4 v4, 0x0

    .line 219
    const/4 v3, 0x0

    .line 220
    const/4 v2, 0x0

    .line 223
    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->peek()Landroid/util/JsonToken;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v14, Landroid/util/JsonToken;->END_DOCUMENT:Landroid/util/JsonToken;
    :try_end_1
    .catch Landroid/util/MalformedJsonException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    if-ne v1, v14, :cond_2

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_4

    .line 224
    const/4 v1, 0x0

    .line 333
    :goto_2
    return-object v1

    .line 209
    :catch_0
    move-exception v1

    new-instance v2, Lefs;

    const v3, 0x30010

    invoke-direct {v2, v1, v3}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v2

    .line 223
    :cond_2
    :try_start_2
    sget-object v14, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    if-ne v1, v14, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    const-string v14, "PelletParserJson"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Unexpected JSON token. Expected pellet, but got "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x5

    move/from16 v0, v16

    invoke-static {v0, v14, v1, v15}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->skipValue()V
    :try_end_2
    .catch Landroid/util/MalformedJsonException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_0

    .line 323
    :catch_1
    move-exception v1

    new-instance v1, Lefs;

    const v2, 0x30006

    invoke-direct {v1, v2}, Lefs;-><init>(I)V

    throw v1

    .line 229
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->beginObject()V
    :try_end_3
    .catch Landroid/util/MalformedJsonException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move/from16 v17, v6

    move v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v11

    move-object v11, v10

    move/from16 v10, v17

    .line 230
    :goto_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 231
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->acV()Ldlq;

    move-result-object v1

    .line 232
    const/16 v14, 0x65

    invoke-virtual {v1, v14}, Ldlq;->b(B)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 233
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextString()Ljava/lang/String;

    move-result-object v13

    goto :goto_3

    .line 235
    :cond_5
    const/16 v14, 0x63

    invoke-virtual {v1, v14}, Ldlq;->b(B)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 236
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextInt()I

    move-result v10

    goto :goto_3

    .line 238
    :cond_6
    const/16 v14, 0x75

    invoke-virtual {v1, v14}, Ldlq;->b(B)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 239
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextString()Ljava/lang/String;

    move-result-object v12

    goto :goto_3

    .line 241
    :cond_7
    const/16 v14, 0x64

    invoke-virtual {v1, v14}, Ldlq;->b(B)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 242
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->acX()[B

    move-result-object v11

    goto :goto_3

    .line 243
    :cond_8
    const/16 v14, 0x61

    invoke-virtual {v1, v14}, Ldlq;->b(B)Z
    :try_end_4
    .catch Landroid/util/MalformedJsonException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v14

    if-eqz v14, :cond_17

    .line 248
    :try_start_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->beginObject()V

    .line 249
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 250
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->acV()Ldlq;

    move-result-object v1

    .line 252
    sget-object v14, Lcom;->bdK:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 254
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->acX()[B

    move-result-object v8

    goto :goto_4

    .line 255
    :cond_a
    sget-object v14, Lcom;->bdL:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 256
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextInt()I

    move-result v6

    goto :goto_4

    .line 258
    :cond_b
    sget-object v14, Lcom;->bdM:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 259
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextInt()I

    move-result v1

    if-lez v1, :cond_c

    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_4

    :cond_c
    const/4 v1, 0x0

    goto :goto_5

    .line 261
    :cond_d
    sget-object v14, Lcom;->bdN:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_f

    .line 262
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 263
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->beginArray()V

    .line 264
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 265
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Landroid/util/MalformedJsonException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_6

    .line 308
    :catch_2
    move-exception v1

    .line 309
    :try_start_6
    const-string v14, "PelletParserJson"

    const-string v15, "Could not parse pellet \'a\' field"

    invoke-static {v14, v15, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catch Landroid/util/MalformedJsonException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_3

    .line 324
    :catch_3
    move-exception v1

    move/from16 v17, v10

    move-object v10, v11

    move-object v11, v9

    move-object v9, v8

    move-object v8, v7

    move v7, v6

    move/from16 v6, v17

    .line 325
    :goto_7
    const v14, 0x3000e

    invoke-static {v1, v14}, Ldmc;->b(Ljava/io/IOException;I)V

    move-object/from16 v17, v2

    move-object v2, v13

    move-object v13, v3

    move v3, v6

    move-object v6, v9

    move-object v9, v11

    move-object v11, v5

    move-object v5, v10

    move-object v10, v4

    move-object v4, v12

    move-object/from16 v12, v17

    .line 329
    :goto_8
    if-eqz v2, :cond_19

    const/4 v1, -0x1

    if-eq v3, v1, :cond_19

    if-eqz v4, :cond_19

    if-eqz v5, :cond_19

    move-object/from16 v1, p0

    .line 330
    invoke-virtual/range {v1 .. v13}, Lcom;->a(Ljava/lang/String;ILjava/lang/String;[B[BI[ILjava/lang/String;[Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;)Lcog;

    move-result-object v1

    goto/16 :goto_2

    .line 267
    :cond_e
    :try_start_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->endArray()V

    .line 268
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    new-array v7, v1, [I

    .line 269
    const/4 v1, 0x0

    move v14, v1

    :goto_9
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-ge v14, v1, :cond_9

    .line 270
    invoke-interface {v15, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v7, v14

    .line 269
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_9

    .line 273
    :cond_f
    sget-object v14, Lcom;->bdO:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 274
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    .line 276
    :cond_10
    sget-object v14, Lcom;->bdP:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_12

    .line 277
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 278
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->beginArray()V

    .line 279
    :goto_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 280
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 282
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->endArray()V

    .line 283
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    new-array v4, v1, [Ljava/lang/String;

    .line 284
    const/4 v1, 0x0

    move v14, v1

    :goto_b
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-ge v14, v1, :cond_9

    .line 285
    invoke-interface {v15, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v4, v14

    .line 284
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_b

    .line 288
    :cond_12
    sget-object v14, Lcom;->bdQ:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v14

    if-eqz v14, :cond_13

    .line 289
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextBoolean()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto/16 :goto_4

    .line 291
    :cond_13
    sget-object v14, Lcom;->bdR:[B

    invoke-virtual {v1, v14}, Ldlq;->H([B)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 292
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->nextString()Ljava/lang/String;

    move-result-object v2

    .line 296
    const-string v1, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 297
    const/4 v2, 0x0

    .line 299
    :cond_14
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v1, v14

    goto/16 :goto_4

    .line 301
    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->skipValue()V

    goto/16 :goto_4

    .line 306
    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->endObject()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Landroid/util/MalformedJsonException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_3

    .line 311
    :cond_17
    const/16 v14, 0x70

    :try_start_8
    invoke-virtual {v1, v14}, Ldlq;->b(B)Z

    .line 314
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->skipValue()V

    goto/16 :goto_3

    .line 321
    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom;->bdU:Ldlo;

    invoke-virtual {v1}, Ldlo;->endObject()V
    :try_end_8
    .catch Landroid/util/MalformedJsonException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    move-object/from16 v17, v2

    move-object v2, v13

    move-object v13, v3

    move v3, v10

    move-object v10, v4

    move-object v4, v12

    move-object/from16 v12, v17

    move-object/from16 v18, v7

    move v7, v6

    move-object v6, v8

    move-object/from16 v8, v18

    move-object/from16 v19, v5

    move-object v5, v11

    move-object/from16 v11, v19

    .line 326
    goto/16 :goto_8

    .line 333
    :cond_19
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 324
    :catch_4
    move-exception v1

    goto/16 :goto_7
.end method

.method private static f(Ljava/lang/String;[B)Ljrp;
    .locals 5

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 472
    :try_start_0
    const-string v1, "act"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    new-instance v0, Ljrp;

    invoke-direct {v0}, Ljrp;-><init>()V

    invoke-static {v0, p1}, Leqh;->a(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljrp;

    .line 484
    :goto_0
    return-object v0

    .line 475
    :cond_0
    const-string v1, "ans"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 476
    const-string v1, "PelletParserJson"

    const-string v2, "Native answers are no longer supported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 480
    :catch_0
    move-exception v0

    .line 481
    const-string v1, "PelletParserJson"

    const-string v2, "Could not make card"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 482
    new-instance v0, Lefs;

    const v1, 0x30016

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0

    .line 478
    :cond_1
    :try_start_1
    const-string v1, "PelletParserJson"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown pellet type. Not an action or an answer. cardId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static hw(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 451
    const/4 v1, 0x0

    .line 452
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 454
    :cond_0
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 455
    const/16 v3, 0x30

    if-lt v2, v3, :cond_1

    const/16 v3, 0x39

    if-gt v2, v3, :cond_1

    .line 456
    add-int/lit8 v0, v0, -0x1

    .line 459
    if-gtz v0, :cond_0

    .line 460
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 461
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 463
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final QY()Lcog;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 190
    :try_start_0
    invoke-direct {p0}, Lcom;->QZ()Lcog;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 191
    :catch_0
    move-exception v0

    .line 192
    const v1, 0x825da6

    invoke-static {v1}, Lhwt;->lx(I)V

    .line 200
    new-instance v1, Lefs;

    const v2, 0x3000c

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;[B[BI[ILjava/lang/String;[Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;)Lcog;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [I
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # [Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Boolean;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Boolean;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 348
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 349
    const-string v1, "ect"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 350
    if-nez p2, :cond_5

    const/4 v1, 0x1

    .line 353
    :goto_0
    new-instance v5, Lkem;

    invoke-direct {v5}, Lkem;-><init>()V

    .line 354
    invoke-virtual {v5, p1}, Lkem;->AF(Ljava/lang/String;)Lkem;

    .line 356
    if-eqz p12, :cond_0

    .line 357
    invoke-virtual/range {p12 .. p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v5, v2}, Lkem;->jP(Z)Lkem;

    .line 359
    :cond_0
    if-eqz p11, :cond_1

    .line 360
    move-object/from16 v0, p11

    invoke-virtual {v5, v0}, Lkem;->AG(Ljava/lang/String;)Lkem;

    .line 362
    :cond_1
    if-eqz p5, :cond_2

    .line 363
    new-instance v2, Lkel;

    invoke-direct {v2}, Lkel;-><init>()V

    iput-object v2, v5, Lkem;->eUQ:Lkel;

    .line 364
    iget-object v2, v5, Lkem;->eUQ:Lkel;

    new-instance v6, Ljyw;

    invoke-direct {v6}, Ljyw;-><init>()V

    iput-object v6, v2, Lkel;->eUJ:Ljyw;

    .line 366
    :try_start_0
    iget-object v2, v5, Lkem;->eUQ:Lkel;

    iget-object v2, v2, Lkel;->eUJ:Ljyw;

    invoke-static {v2, p5}, Leqh;->a(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :cond_2
    if-eqz p10, :cond_3

    .line 374
    invoke-virtual/range {p10 .. p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v5, v2}, Lkem;->tY(I)Lkem;

    .line 376
    :cond_3
    if-lez p6, :cond_7

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v5, v2}, Lkem;->jQ(Z)Lkem;

    .line 377
    iput-object p7, v5, Lkem;->eUN:[I

    .line 380
    iget-object v2, p0, Lcom;->bdx:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 383
    new-instance v1, Lkeo;

    invoke-direct {v1}, Lkeo;-><init>()V

    iput-object v1, v5, Lkem;->eUS:Lkeo;

    .line 384
    iget-object v1, v5, Lkem;->eUS:Lkeo;

    new-instance v2, Ljava/lang/String;

    sget-object v3, Lcom;->bdS:Ljava/nio/charset/Charset;

    invoke-direct {v2, p4, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v1, v2}, Lkeo;->AJ(Ljava/lang/String;)Lkeo;

    .line 423
    :cond_4
    :goto_3
    array-length v1, p4

    .line 425
    new-instance v2, Lcog;

    invoke-direct {v2, p3, v5, v1}, Lcog;-><init>(Ljava/lang/String;Lkem;I)V

    return-object v2

    .line 350
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 367
    :catch_0
    move-exception v1

    .line 368
    const-string v2, "PelletParserJson"

    const-string v3, "Error making CardMetadata protobuf. data=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 369
    new-instance v2, Lefs;

    const v3, 0x30017

    invoke-direct {v2, v1, v3}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v2

    .line 374
    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    .line 376
    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 385
    :cond_8
    if-nez v4, :cond_c

    .line 387
    new-instance v2, Lken;

    invoke-direct {v2}, Lken;-><init>()V

    iput-object v2, v5, Lkem;->eUR:Lken;

    .line 388
    iget-object v2, v5, Lkem;->eUR:Lken;

    invoke-virtual {v2, p4}, Lken;->aI([B)Lken;

    .line 389
    const-string v2, "q"

    invoke-virtual {v3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 390
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 391
    iget-object v3, v5, Lkem;->eUR:Lken;

    invoke-virtual {v3, v2}, Lken;->AH(Ljava/lang/String;)Lken;

    .line 393
    :cond_9
    iget-object v2, v5, Lkem;->eUR:Lken;

    invoke-virtual {v2, v1}, Lken;->jR(Z)Lken;

    .line 394
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 395
    iget-object v1, v5, Lkem;->eUR:Lken;

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lken;->AI(Ljava/lang/String;)Lken;

    .line 397
    :cond_a
    if-eqz p9, :cond_b

    .line 398
    iget-object v1, v5, Lkem;->eUR:Lken;

    move-object/from16 v0, p9

    iput-object v0, v1, Lken;->eUY:[Ljava/lang/String;

    .line 402
    :cond_b
    iget-object v1, v5, Lkem;->eUR:Lken;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lken;->tZ(I)Lken;

    goto :goto_3

    .line 403
    :cond_c
    const-string v2, "eoc"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 404
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Lkem;->jO(Z)Lkem;

    goto :goto_3

    .line 405
    :cond_d
    const-string v2, "mdp"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 407
    const-string v2, "tts"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 412
    iget-object v2, v5, Lkem;->eUQ:Lkel;

    if-nez v2, :cond_e

    .line 413
    const-string v2, "PelletParserJson"

    const-string v3, "Pellet contains an action but no CardMetadata"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v2, v3, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 414
    new-instance v2, Lkel;

    invoke-direct {v2}, Lkel;-><init>()V

    iput-object v2, v5, Lkem;->eUQ:Lkel;

    .line 416
    :cond_e
    iget-object v2, v5, Lkem;->eUQ:Lkel;

    invoke-static {v4}, Lcom;->hw(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lkel;->tX(I)Lkel;

    .line 417
    iget-object v2, v5, Lkem;->eUQ:Lkel;

    invoke-virtual {v2, v1}, Lkel;->jN(Z)Lkel;

    .line 418
    iget-object v1, v5, Lkem;->eUQ:Lkel;

    invoke-static {v4, p4}, Lcom;->f(Ljava/lang/String;[B)Ljrp;

    move-result-object v2

    iput-object v2, v1, Lkel;->eUG:Ljrp;

    goto/16 :goto_3
.end method
