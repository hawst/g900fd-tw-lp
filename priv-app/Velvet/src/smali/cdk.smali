.class public final enum Lcdk;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum aSN:Lcdk;

.field public static final enum aSO:Lcdk;

.field public static final enum aSP:Lcdk;

.field public static final enum aSQ:Lcdk;

.field private static final synthetic aSR:[Lcdk;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, Lcdk;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcdk;->aSN:Lcdk;

    .line 92
    new-instance v0, Lcdk;

    const-string v1, "UP"

    invoke-direct {v0, v1, v3}, Lcdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcdk;->aSO:Lcdk;

    .line 95
    new-instance v0, Lcdk;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v4}, Lcdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcdk;->aSP:Lcdk;

    .line 98
    new-instance v0, Lcdk;

    const-string v1, "DOWN_ERROR"

    invoke-direct {v0, v1, v5}, Lcdk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcdk;->aSQ:Lcdk;

    .line 86
    const/4 v0, 0x4

    new-array v0, v0, [Lcdk;

    sget-object v1, Lcdk;->aSN:Lcdk;

    aput-object v1, v0, v2

    sget-object v1, Lcdk;->aSO:Lcdk;

    aput-object v1, v0, v3

    sget-object v1, Lcdk;->aSP:Lcdk;

    aput-object v1, v0, v4

    sget-object v1, Lcdk;->aSQ:Lcdk;

    aput-object v1, v0, v5

    sput-object v0, Lcdk;->aSR:[Lcdk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdk;
    .locals 1

    .prologue
    .line 86
    const-class v0, Lcdk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdk;

    return-object v0
.end method

.method public static values()[Lcdk;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcdk;->aSR:[Lcdk;

    invoke-virtual {v0}, [Lcdk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdk;

    return-object v0
.end method
