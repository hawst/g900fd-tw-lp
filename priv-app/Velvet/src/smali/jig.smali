.class public final Ljig;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public ahD:Lixx;

.field public aiS:Ljcn;

.field private aix:Ljava/lang/String;

.field private alx:Ljava/lang/String;

.field private dXG:Liyx;

.field private dXz:Ljava/lang/String;

.field public enl:Lixx;

.field private enm:Liyx;

.field public enn:[Ljbm;

.field private eno:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46795
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 46796
    const/4 v0, 0x0

    iput v0, p0, Ljig;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljig;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljig;->dXz:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljig;->aix:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljig;->alx:Ljava/lang/String;

    iput-object v1, p0, Ljig;->enl:Lixx;

    iput-object v1, p0, Ljig;->enm:Liyx;

    invoke-static {}, Ljbm;->beY()[Ljbm;

    move-result-object v0

    iput-object v0, p0, Ljig;->enn:[Ljbm;

    const-string v0, ""

    iput-object v0, p0, Ljig;->eno:Ljava/lang/String;

    iput-object v1, p0, Ljig;->aiS:Ljcn;

    iput-object v1, p0, Ljig;->ahD:Lixx;

    iput-object v1, p0, Ljig;->dXG:Liyx;

    iput-object v1, p0, Ljig;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljig;->eCz:I

    .line 46797
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 46648
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljig;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljig;->afh:Ljava/lang/String;

    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljig;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljig;->dXz:Ljava/lang/String;

    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljig;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljig;->aix:Ljava/lang/String;

    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljig;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljig;->alx:Ljava/lang/String;

    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljig;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljig;->enm:Liyx;

    if-nez v0, :cond_1

    new-instance v0, Liyx;

    invoke-direct {v0}, Liyx;-><init>()V

    iput-object v0, p0, Ljig;->enm:Liyx;

    :cond_1
    iget-object v0, p0, Ljig;->enm:Liyx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljig;->enn:[Ljbm;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbm;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljig;->enn:[Ljbm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljbm;

    invoke-direct {v3}, Ljbm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljig;->enn:[Ljbm;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljbm;

    invoke-direct {v3}, Ljbm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljig;->enn:[Ljbm;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljig;->eno:Ljava/lang/String;

    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljig;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljig;->aiS:Ljcn;

    if-nez v0, :cond_5

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljig;->aiS:Ljcn;

    :cond_5
    iget-object v0, p0, Ljig;->aiS:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljig;->dXG:Liyx;

    if-nez v0, :cond_6

    new-instance v0, Liyx;

    invoke-direct {v0}, Liyx;-><init>()V

    iput-object v0, p0, Ljig;->dXG:Liyx;

    :cond_6
    iget-object v0, p0, Ljig;->dXG:Liyx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljig;->enl:Lixx;

    if-nez v0, :cond_7

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Ljig;->enl:Lixx;

    :cond_7
    iget-object v0, p0, Ljig;->enl:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljig;->ahD:Lixx;

    if-nez v0, :cond_8

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Ljig;->ahD:Lixx;

    :cond_8
    iget-object v0, p0, Ljig;->ahD:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 46820
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 46821
    const/4 v0, 0x1

    iget-object v1, p0, Ljig;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46823
    :cond_0
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 46824
    const/4 v0, 0x2

    iget-object v1, p0, Ljig;->dXz:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46826
    :cond_1
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 46827
    const/4 v0, 0x3

    iget-object v1, p0, Ljig;->aix:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46829
    :cond_2
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 46830
    const/4 v0, 0x4

    iget-object v1, p0, Ljig;->alx:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46832
    :cond_3
    iget-object v0, p0, Ljig;->enm:Liyx;

    if-eqz v0, :cond_4

    .line 46833
    const/4 v0, 0x5

    iget-object v1, p0, Ljig;->enm:Liyx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46835
    :cond_4
    iget-object v0, p0, Ljig;->enn:[Ljbm;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljig;->enn:[Ljbm;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 46836
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljig;->enn:[Ljbm;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 46837
    iget-object v1, p0, Ljig;->enn:[Ljbm;

    aget-object v1, v1, v0

    .line 46838
    if-eqz v1, :cond_5

    .line 46839
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 46836
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46843
    :cond_6
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    .line 46844
    const/4 v0, 0x7

    iget-object v1, p0, Ljig;->eno:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 46846
    :cond_7
    iget-object v0, p0, Ljig;->aiS:Ljcn;

    if-eqz v0, :cond_8

    .line 46847
    const/16 v0, 0x8

    iget-object v1, p0, Ljig;->aiS:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46849
    :cond_8
    iget-object v0, p0, Ljig;->dXG:Liyx;

    if-eqz v0, :cond_9

    .line 46850
    const/16 v0, 0x9

    iget-object v1, p0, Ljig;->dXG:Liyx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46852
    :cond_9
    iget-object v0, p0, Ljig;->enl:Lixx;

    if-eqz v0, :cond_a

    .line 46853
    const/16 v0, 0xa

    iget-object v1, p0, Ljig;->enl:Lixx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46855
    :cond_a
    iget-object v0, p0, Ljig;->ahD:Lixx;

    if-eqz v0, :cond_b

    .line 46856
    const/16 v0, 0xb

    iget-object v1, p0, Ljig;->ahD:Lixx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 46858
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 46859
    return-void
.end method

.method public final bfA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46692
    iget-object v0, p0, Ljig;->dXz:Ljava/lang/String;

    return-object v0
.end method

.method public final bnb()Z
    .locals 1

    .prologue
    .line 46703
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bnc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46767
    iget-object v0, p0, Ljig;->eno:Ljava/lang/String;

    return-object v0
.end method

.method public final bnd()Z
    .locals 1

    .prologue
    .line 46778
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46670
    iget-object v0, p0, Ljig;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 46863
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 46864
    iget v1, p0, Ljig;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 46865
    const/4 v1, 0x1

    iget-object v2, p0, Ljig;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46868
    :cond_0
    iget v1, p0, Ljig;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 46869
    const/4 v1, 0x2

    iget-object v2, p0, Ljig;->dXz:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46872
    :cond_1
    iget v1, p0, Ljig;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 46873
    const/4 v1, 0x3

    iget-object v2, p0, Ljig;->aix:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46876
    :cond_2
    iget v1, p0, Ljig;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 46877
    const/4 v1, 0x4

    iget-object v2, p0, Ljig;->alx:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46880
    :cond_3
    iget-object v1, p0, Ljig;->enm:Liyx;

    if-eqz v1, :cond_4

    .line 46881
    const/4 v1, 0x5

    iget-object v2, p0, Ljig;->enm:Liyx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46884
    :cond_4
    iget-object v1, p0, Ljig;->enn:[Ljbm;

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljig;->enn:[Ljbm;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 46885
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljig;->enn:[Ljbm;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 46886
    iget-object v2, p0, Ljig;->enn:[Ljbm;

    aget-object v2, v2, v0

    .line 46887
    if-eqz v2, :cond_5

    .line 46888
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 46885
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 46893
    :cond_7
    iget v1, p0, Ljig;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_8

    .line 46894
    const/4 v1, 0x7

    iget-object v2, p0, Ljig;->eno:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46897
    :cond_8
    iget-object v1, p0, Ljig;->aiS:Ljcn;

    if-eqz v1, :cond_9

    .line 46898
    const/16 v1, 0x8

    iget-object v2, p0, Ljig;->aiS:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46901
    :cond_9
    iget-object v1, p0, Ljig;->dXG:Liyx;

    if-eqz v1, :cond_a

    .line 46902
    const/16 v1, 0x9

    iget-object v2, p0, Ljig;->dXG:Liyx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46905
    :cond_a
    iget-object v1, p0, Ljig;->enl:Lixx;

    if-eqz v1, :cond_b

    .line 46906
    const/16 v1, 0xa

    iget-object v2, p0, Ljig;->enl:Lixx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46909
    :cond_b
    iget-object v1, p0, Ljig;->ahD:Lixx;

    if-eqz v1, :cond_c

    .line 46910
    const/16 v1, 0xb

    iget-object v2, p0, Ljig;->ahD:Lixx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46913
    :cond_c
    return v0
.end method

.method public final pL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46714
    iget-object v0, p0, Ljig;->aix:Ljava/lang/String;

    return-object v0
.end method

.method public final pM()Z
    .locals 1

    .prologue
    .line 46725
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 46681
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46736
    iget-object v0, p0, Ljig;->alx:Ljava/lang/String;

    return-object v0
.end method

.method public final td()Z
    .locals 1

    .prologue
    .line 46747
    iget v0, p0, Ljig;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final vK(Ljava/lang/String;)Ljig;
    .locals 1

    .prologue
    .line 46673
    if-nez p1, :cond_0

    .line 46674
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46676
    :cond_0
    iput-object p1, p0, Ljig;->afh:Ljava/lang/String;

    .line 46677
    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljig;->aez:I

    .line 46678
    return-object p0
.end method

.method public final vL(Ljava/lang/String;)Ljig;
    .locals 1

    .prologue
    .line 46695
    if-nez p1, :cond_0

    .line 46696
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46698
    :cond_0
    iput-object p1, p0, Ljig;->dXz:Ljava/lang/String;

    .line 46699
    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljig;->aez:I

    .line 46700
    return-object p0
.end method

.method public final vM(Ljava/lang/String;)Ljig;
    .locals 1

    .prologue
    .line 46717
    if-nez p1, :cond_0

    .line 46718
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46720
    :cond_0
    iput-object p1, p0, Ljig;->aix:Ljava/lang/String;

    .line 46721
    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljig;->aez:I

    .line 46722
    return-object p0
.end method

.method public final vN(Ljava/lang/String;)Ljig;
    .locals 1

    .prologue
    .line 46739
    if-nez p1, :cond_0

    .line 46740
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46742
    :cond_0
    iput-object p1, p0, Ljig;->alx:Ljava/lang/String;

    .line 46743
    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljig;->aez:I

    .line 46744
    return-object p0
.end method

.method public final vO(Ljava/lang/String;)Ljig;
    .locals 1

    .prologue
    .line 46770
    if-nez p1, :cond_0

    .line 46771
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46773
    :cond_0
    iput-object p1, p0, Ljig;->eno:Ljava/lang/String;

    .line 46774
    iget v0, p0, Ljig;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljig;->aez:I

    .line 46775
    return-object p0
.end method
