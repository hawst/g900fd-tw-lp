.class final Lhyv;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic aSa:Lcom/google/android/shared/search/Query;

.field private synthetic duV:Lcom/google/android/velvet/ActionData;

.field private synthetic duW:Lhyq;

.field private synthetic duX:Lcom/google/android/speech/embedded/TaggerResult;


# direct methods
.method constructor <init>(Lhyq;Lcom/google/android/shared/search/Query;Lcom/google/android/speech/embedded/TaggerResult;Lcom/google/android/velvet/ActionData;)V
    .locals 0

    .prologue
    .line 415
    iput-object p1, p0, Lhyv;->duW:Lhyq;

    iput-object p2, p0, Lhyv;->aSa:Lcom/google/android/shared/search/Query;

    iput-object p3, p0, Lhyv;->duX:Lcom/google/android/speech/embedded/TaggerResult;

    iput-object p4, p0, Lhyv;->duV:Lcom/google/android/velvet/ActionData;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private e(Lcom/google/android/search/shared/actions/VoiceAction;I)V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lhyv;->duW:Lhyq;

    iget-object v1, p0, Lhyv;->duV:Lcom/google/android/velvet/ActionData;

    iget-object v2, p0, Lhyv;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, p1, v2}, Lhyq;->a(Lcom/google/android/velvet/ActionData;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/search/Query;)V

    .line 468
    const/16 v0, 0x86

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v0, p2}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 471
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 415
    iget-object v0, p0, Lhyv;->aSa:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lhyv;->duW:Lhyq;

    iget-object v1, v1, Lhyq;->mContactLabelConverter:Ldyv;

    iget-object v2, p0, Lhyv;->duW:Lhyq;

    iget-object v2, v2, Lhyq;->mRelationshipManager:Lcjg;

    iget-object v3, p0, Lhyv;->duW:Lhyq;

    iget-object v3, v3, Lhyq;->mPersonShortcutManager:Lciy;

    iget-object v4, p0, Lhyv;->duW:Lhyq;

    iget-object v4, v4, Lhyq;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lhyv;->duX:Lcom/google/android/speech/embedded/TaggerResult;

    iget-object v6, p0, Lhyv;->duW:Lhyq;

    iget-object v6, v6, Lhyq;->mContactLookup:Lghy;

    iget-object v7, p0, Lhyv;->duW:Lhyq;

    iget-object v7, v7, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v7}, Ligi;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcky;

    invoke-virtual {v7}, Lcky;->Pn()Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/google/android/search/shared/actions/CommunicationAction;->afZ()Ldzb;

    move-result-object v7

    :goto_0
    iget-object v8, p0, Lhyv;->duW:Lhyq;

    iget-object v8, v8, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-static/range {v0 .. v9}, Licv;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Lghy;Ldzb;Ligi;Lifw;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v7}, Lcky;->Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akB()Ldzb;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object v7, v8

    goto :goto_0

    :cond_1
    invoke-virtual {v7}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v7

    instance-of v8, v7, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-eqz v8, :cond_2

    check-cast v7, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-static {v7, v10}, Ldxv;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)Ldzb;

    move-result-object v7

    goto :goto_0

    :cond_2
    move-object v7, v9

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 415
    check-cast p1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iget-object v0, p0, Lhyv;->duW:Lhyq;

    iget-object v0, v0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Pn()Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lhyv;->duW:Lhyq;

    iget-object v0, v0, Lhyq;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v3

    if-nez v3, :cond_3

    move v0, v1

    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lhyv;->duW:Lhyq;

    iget-object v1, p0, Lhyv;->duV:Lcom/google/android/velvet/ActionData;

    iget-object v2, p0, Lhyv;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2}, Lhyq;->c(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V

    :cond_0
    return-void

    :cond_1
    invoke-static {v0, p1}, Lgth;->a(Lcom/google/android/search/shared/actions/CommunicationAction;Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v3

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/CommunicationAction;->agc()I

    move-result v0

    invoke-direct {p0, v3, v0}, Lhyv;->e(Lcom/google/android/search/shared/actions/VoiceAction;I)V

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-nez v4, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-direct {v1, v3, p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->e(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Lcom/google/android/search/shared/actions/modular/ModularAction;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agc()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lhyv;->e(Lcom/google/android/search/shared/actions/VoiceAction;I)V

    move v0, v2

    goto :goto_1
.end method
