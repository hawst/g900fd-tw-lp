.class final Lcuc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcts;


# instance fields
.field private final biK:Ljava/util/List;

.field private final biL:Lctd;

.field private final biM:Ljava/lang/String;

.field private biN:Lcts;

.field private biO:Z

.field private biP:I

.field private biQ:I

.field private final dA:[Ljava/lang/String;

.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    const/4 v0, 0x0

    iput v0, p0, Lcuc;->biQ:I

    .line 206
    iput-object p1, p0, Lcuc;->mContentResolver:Landroid/content/ContentResolver;

    .line 213
    iput-object p3, p0, Lcuc;->biK:Ljava/util/List;

    .line 214
    new-instance v0, Lctd;

    invoke-direct {v0, p2}, Lctd;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcuc;->biL:Lctd;

    .line 215
    new-instance v0, Lcui;

    invoke-direct {v0}, Lcui;-><init>()V

    iput-object v0, p0, Lcuc;->biN:Lcts;

    .line 217
    iget-object v0, p0, Lcuc;->biL:Lctd;

    iget-object v0, v0, Lctd;->bir:Ljava/util/Set;

    .line 218
    const-string v1, "contact_id"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v1, "lookup"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    const-string v1, "photo_thumb_uri"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 221
    const-string v1, "display_name"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 222
    const-string v1, "phonetic_name"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 223
    const-string v1, "times_contacted"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 224
    const-string v1, "raw_contact_id"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcuc;->dA:[Ljava/lang/String;

    .line 227
    const-string v0, "contact_id,is_super_primary DESC,is_primary DESC,raw_contact_id"

    iput-object v0, p0, Lcuc;->biM:Ljava/lang/String;

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcuc;->biO:Z

    .line 235
    return-void
.end method

.method private SV()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 239
    iget-object v0, p0, Lcuc;->biN:Lcts;

    invoke-interface {v0}, Lcts;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcuc;->biP:I

    iget-object v1, p0, Lcuc;->biK:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 250
    iget-object v0, p0, Lcuc;->biN:Lcts;

    invoke-interface {v0}, Lcts;->close()V

    .line 251
    iget v0, p0, Lcuc;->biP:I

    add-int/lit8 v0, v0, 0x19

    iget-object v1, p0, Lcuc;->biK:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 254
    iget-object v1, p0, Lcuc;->biK:Ljava/util/List;

    iget v2, p0, Lcuc;->biP:I

    invoke-interface {v1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 255
    const-string v2, "contact_id IN (%s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, ","

    invoke-static {v4, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 257
    iput v0, p0, Lcuc;->biP:I

    .line 262
    :try_start_0
    iget-object v0, p0, Lcuc;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcuc;->dA:[Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcuc;->biM:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 278
    :goto_0
    iget v1, p0, Lcuc;->biQ:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcuc;->biQ:I

    .line 279
    if-nez v0, :cond_1

    .line 280
    const-string v0, "IcingCPHelper"

    const-string v1, "Could not query ContactsProvider; disabled? Give up."

    new-array v2, v7, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 281
    new-instance v0, Lcui;

    invoke-direct {v0}, Lcui;-><init>()V

    iput-object v0, p0, Lcuc;->biN:Lcts;

    .line 282
    iget-object v0, p0, Lcuc;->biK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcuc;->biP:I

    .line 287
    :cond_0
    :goto_1
    return-void

    .line 268
    :catch_0
    move-exception v0

    .line 272
    const-string v1, "IcingCPHelper"

    const-string v2, "ContentResolver.query threw an exception"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 273
    const v0, 0xc74bbd

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 275
    iput-boolean v7, p0, Lcuc;->biO:Z

    move-object v0, v6

    goto :goto_0

    .line 284
    :cond_1
    new-instance v1, Lcuj;

    iget-object v2, p0, Lcuc;->biL:Lctd;

    invoke-direct {v1, v0, v2}, Lcuj;-><init>(Landroid/database/Cursor;Lctd;)V

    iput-object v1, p0, Lcuc;->biN:Lcts;

    goto :goto_1
.end method

.method private SW()Lctp;
    .locals 1

    .prologue
    .line 297
    invoke-direct {p0}, Lcuc;->SV()V

    .line 298
    iget-object v0, p0, Lcuc;->biN:Lcts;

    invoke-interface {v0}, Lcts;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctp;

    return-object v0
.end method


# virtual methods
.method public final SS()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcuc;->biO:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcuc;->biN:Lcts;

    invoke-interface {v0}, Lcts;->SS()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcuc;->biN:Lcts;

    invoke-interface {v0}, Lcts;->close()V

    .line 327
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 291
    invoke-direct {p0}, Lcuc;->SV()V

    .line 292
    iget-object v0, p0, Lcuc;->biN:Lcts;

    invoke-interface {v0}, Lcts;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Lcuc;->SW()Lctp;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 303
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
