.class public final Lgbn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cFc:Lgbg;

.field public final cFd:Lgbg;

.field public final cFe:Z

.field public final cxZ:Z

.field public final cyb:Z

.field public final cyc:Z

.field public final cyd:Ljava/lang/Integer;

.field public final cye:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljbp;Ljal;ZZZZLjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iput-object v0, p0, Lgbn;->cFc:Lgbg;

    .line 33
    invoke-static {p2}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iput-object v0, p0, Lgbn;->cFd:Lgbg;

    .line 34
    iput-boolean p3, p0, Lgbn;->cxZ:Z

    .line 35
    iput-boolean p4, p0, Lgbn;->cFe:Z

    .line 36
    iput-boolean p5, p0, Lgbn;->cyb:Z

    .line 37
    iput-boolean p6, p0, Lgbn;->cyc:Z

    .line 38
    iput-object p7, p0, Lgbn;->cyd:Ljava/lang/Integer;

    .line 39
    iput-object p8, p0, Lgbn;->cye:Ljava/lang/Integer;

    .line 40
    return-void
.end method

.method public static c(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Lgbn;
    .locals 9

    .prologue
    .line 124
    new-instance v0, Lgbn;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCc()Ljbp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->getFrequentPlace()Ljal;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->rg()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCd()Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->ri()Z

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCe()Z

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCf()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->aCg()Ljava/lang/Integer;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lgbn;-><init>(Ljbp;Ljal;ZZZZLjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    if-ne p1, p0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    instance-of v2, p1, Lgbn;

    if-nez v2, :cond_2

    move v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_2
    check-cast p1, Lgbn;

    .line 87
    iget-boolean v2, p0, Lgbn;->cxZ:Z

    iget-boolean v3, p1, Lgbn;->cxZ:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgbn;->cFc:Lgbg;

    iget-object v3, p1, Lgbn;->cFc:Lgbg;

    invoke-virtual {v2, v3}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgbn;->cFd:Lgbg;

    iget-object v3, p1, Lgbn;->cFd:Lgbg;

    invoke-virtual {v2, v3}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lgbn;->cFe:Z

    iget-boolean v3, p1, Lgbn;->cFe:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgbn;->cyb:Z

    iget-boolean v3, p1, Lgbn;->cyb:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgbn;->cyc:Z

    iget-boolean v3, p1, Lgbn;->cyc:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgbn;->cyd:Ljava/lang/Integer;

    iget-object v3, p1, Lgbn;->cyd:Ljava/lang/Integer;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgbn;->cye:Ljava/lang/Integer;

    iget-object v3, p1, Lgbn;->cye:Ljava/lang/Integer;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 99
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lgbn;->cFc:Lgbg;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lgbn;->cFd:Lgbg;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lgbn;->cxZ:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lgbn;->cFe:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lgbn;->cyb:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lgbn;->cyc:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lgbn;->cyd:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lgbn;->cye:Ljava/lang/Integer;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
