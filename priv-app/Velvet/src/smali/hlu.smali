.class public abstract Lhlu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlk;


# instance fields
.field protected final mIntentStarter:Leoj;


# direct methods
.method protected constructor <init>(Leoj;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lhlu;->mIntentStarter:Leoj;

    .line 29
    return-void
.end method


# virtual methods
.method public G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lhlu;->mIntentStarter:Leoj;

    invoke-virtual {p0, p1}, Lhlu;->J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Leoj;->c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 102
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method protected K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lhlu;->L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected abstract L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
.end method

.method protected abstract M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
.end method

.method public d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 39
    packed-switch p2, :pswitch_data_0

    move v0, v1

    .line 66
    :goto_0
    return v0

    .line 41
    :pswitch_0
    invoke-virtual {p0, p1}, Lhlu;->M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 52
    :goto_1
    array-length v2, v0

    if-nez v2, :cond_0

    move v0, v1

    .line 53
    goto :goto_0

    .line 44
    :pswitch_1
    invoke-virtual {p0, p1}, Lhlu;->L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    goto :goto_1

    .line 47
    :pswitch_2
    invoke-virtual {p0, p1}, Lhlu;->K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    goto :goto_1

    .line 55
    :cond_0
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v3

    .line 56
    invoke-virtual {v3}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 57
    array-length v4, v0

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    aget-object v5, v0, v2

    .line 58
    invoke-virtual {v5}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;

    move-result-object v6

    if-nez v6, :cond_1

    .line 59
    invoke-virtual {v3}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    iget-object v6, p0, Lhlu;->mIntentStarter:Leoj;

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/content/Intent;

    aput-object v5, v7, v1

    invoke-interface {v6, v7}, Leoj;->c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 61
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 66
    :cond_2
    iget-object v1, p0, Lhlu;->mIntentStarter:Leoj;

    invoke-interface {v1, v0}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
