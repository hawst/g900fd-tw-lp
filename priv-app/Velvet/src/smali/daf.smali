.class public final Ldaf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldes;


# instance fields
.field private final boO:I

.field private final boP:I

.field private final boQ:I

.field private final boR:I

.field private final boS:I

.field private final boT:Ldeb;

.field private final boU:Ldeb;

.field private final boV:Ldeb;

.field private final boW:Ldeb;

.field private final boX:Ldeb;

.field private final boY:Ldeb;

.field private final boZ:[Ljava/lang/String;

.field private final mGsaConfigFlags:Lchk;

.field private final mShouldQueryStrategy:Ldfz;


# direct methods
.method public constructor <init>(IIIIILdeb;Ldeb;Ldds;Ldeb;Ldeb;Ldeb;Lchk;Ldfz;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Ldaf;->boO:I

    .line 75
    iput p2, p0, Ldaf;->boP:I

    .line 76
    iput p3, p0, Ldaf;->boQ:I

    .line 77
    iput p4, p0, Ldaf;->boR:I

    .line 78
    iput p5, p0, Ldaf;->boS:I

    .line 79
    iput-object p6, p0, Ldaf;->boT:Ldeb;

    .line 80
    iput-object p7, p0, Ldaf;->boU:Ldeb;

    .line 81
    iput-object p8, p0, Ldaf;->boV:Ldeb;

    .line 82
    iput-object p9, p0, Ldaf;->boW:Ldeb;

    .line 83
    iput-object p10, p0, Ldaf;->boX:Ldeb;

    .line 84
    iput-object p11, p0, Ldaf;->boY:Ldeb;

    .line 85
    iput-object p12, p0, Ldaf;->mGsaConfigFlags:Lchk;

    .line 86
    iput-object p13, p0, Ldaf;->mShouldQueryStrategy:Ldfz;

    .line 87
    sub-int v0, p1, p2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Ldaf;->boZ:[Ljava/lang/String;

    .line 88
    return-void
.end method


# virtual methods
.method public final a(Ldem;Landroid/util/SparseArray;Landroid/util/SparseArray;)Z
    .locals 10

    .prologue
    .line 94
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 95
    invoke-virtual {p1}, Ldem;->abj()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 96
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v4

    .line 102
    new-instance v1, Lddx;

    const-string v0, "action-discovery"

    invoke-direct {v1, v0, v3}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 107
    iget-object v2, p0, Ldaf;->boY:Ldeb;

    iget v5, p0, Ldaf;->boS:I

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v2, p1, v5, v0, v1}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 110
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 111
    invoke-interface {v1}, Lddw;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 112
    invoke-interface {v1}, Lddw;->abc()Z

    move-result v0

    .line 180
    :goto_1
    return v0

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_1
    new-instance v5, Lddx;

    const-string v0, "corrections"

    invoke-direct {v5, v0, v3}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 117
    iget-object v1, p0, Ldaf;->boV:Ldeb;

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v1, p1, v2, v0, v5}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 121
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Ldaf;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GO()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x2

    move v1, v0

    .line 126
    :goto_2
    new-instance v6, Ldej;

    const-string v0, "promoted-summons"

    invoke-direct {v6, v0, v3}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 128
    iget-object v2, p0, Ldaf;->boX:Ldeb;

    iget v7, p0, Ldaf;->boR:I

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v2, p1, v7, v0, v6}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 131
    const/4 v0, 0x0

    .line 132
    invoke-interface {v6}, Lddw;->getCount()I

    move-result v2

    if-lez v2, :cond_c

    .line 134
    const/4 v0, 0x2

    move v2, v0

    .line 137
    :goto_3
    iget-object v0, p0, Ldaf;->boZ:[Ljava/lang/String;

    array-length v0, v0

    iget v7, p0, Ldaf;->boO:I

    iget v8, p0, Ldaf;->boP:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v2

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 140
    new-instance v8, Ldej;

    const-string v0, "summons"

    invoke-direct {v8, v0, v3}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 141
    iget-object v9, p0, Ldaf;->boU:Ldeb;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v9, p1, v7, v0, v8}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 144
    invoke-interface {v8}, Lddw;->getCount()I

    move-result v0

    add-int/2addr v0, v2

    .line 145
    iget-object v2, p0, Ldaf;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v2, v3}, Ldfz;->aD(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v8}, Lddw;->abc()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v6}, Lddw;->abc()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v6}, Lddw;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    :cond_2
    iget-object v2, p0, Ldaf;->boZ:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 153
    iget-object v1, p0, Ldaf;->boZ:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 154
    if-eqz v1, :cond_3

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 156
    :cond_3
    iget-object v1, p0, Ldaf;->boZ:[Ljava/lang/String;

    aput-object v4, v1, v0

    :cond_4
    move v1, v0

    .line 161
    :cond_5
    new-instance v2, Ldej;

    const-string v0, "summons"

    invoke-direct {v2, v0, v3}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 163
    iget-object v4, p0, Ldaf;->boW:Ldeb;

    const/4 v7, 0x1

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v4, p1, v7, v0, v2}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 165
    invoke-interface {v2}, Lddw;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 167
    add-int/lit8 v1, v1, 0x1

    .line 170
    :cond_6
    new-instance v4, Ldej;

    const-string v0, "web"

    invoke-direct {v4, v0, v3}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 171
    iget v0, p0, Ldaf;->boP:I

    iget v3, p0, Ldaf;->boQ:I

    sub-int v1, v3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 172
    iget-object v3, p0, Ldaf;->boT:Ldeb;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v3, p1, v1, v0, v4}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 175
    const/4 v0, 0x0

    invoke-virtual {p3, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 176
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 177
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 178
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 179
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 180
    invoke-interface {v4}, Lddw;->abc()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v8}, Lddw;->abc()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Lddw;->abc()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    goto/16 :goto_1

    .line 121
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    :cond_8
    const/4 v0, 0x0

    :goto_4
    iget-object v1, p0, Ldaf;->boZ:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    iget-object v1, p0, Ldaf;->boZ:[Ljava/lang/String;

    aget-object v1, v1, v0

    if-eqz v1, :cond_9

    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v0

    goto/16 :goto_2

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_a
    iget-object v0, p0, Ldaf;->boZ:[Ljava/lang/String;

    array-length v0, v0

    move v1, v0

    goto/16 :goto_2

    .line 180
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_c
    move v2, v0

    goto/16 :goto_3
.end method
