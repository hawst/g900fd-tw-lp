.class public final Ljfl;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eik:[Ljfl;


# instance fields
.field private aez:I

.field private aiK:Ljava/lang/String;

.field private eil:Ljava/lang/String;

.field private eim:Ljava/lang/String;

.field private ein:J

.field private eio:Ljava/lang/String;

.field private eip:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13127
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13128
    iput v2, p0, Ljfl;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljfl;->eil:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljfl;->eim:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljfl;->aiK:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljfl;->ein:J

    const-string v0, ""

    iput-object v0, p0, Ljfl;->eio:Ljava/lang/String;

    iput-boolean v2, p0, Ljfl;->eip:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfl;->eCz:I

    .line 13129
    return-void
.end method

.method public static bjr()[Ljfl;
    .locals 2

    .prologue
    .line 12988
    sget-object v0, Ljfl;->eik:[Ljfl;

    if-nez v0, :cond_1

    .line 12989
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 12991
    :try_start_0
    sget-object v0, Ljfl;->eik:[Ljfl;

    if-nez v0, :cond_0

    .line 12992
    const/4 v0, 0x0

    new-array v0, v0, [Ljfl;

    sput-object v0, Ljfl;->eik:[Ljfl;

    .line 12994
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12996
    :cond_1
    sget-object v0, Ljfl;->eik:[Ljfl;

    return-object v0

    .line 12994
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 12982
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfl;->eil:Ljava/lang/String;

    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfl;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfl;->eim:Ljava/lang/String;

    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfl;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfl;->aiK:Ljava/lang/String;

    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfl;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljfl;->ein:J

    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfl;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfl;->eip:Z

    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljfl;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljfl;->eio:Ljava/lang/String;

    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfl;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 13147
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13148
    const/4 v0, 0x1

    iget-object v1, p0, Ljfl;->eil:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13150
    :cond_0
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13151
    const/4 v0, 0x2

    iget-object v1, p0, Ljfl;->eim:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13153
    :cond_1
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 13154
    const/4 v0, 0x3

    iget-object v1, p0, Ljfl;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13156
    :cond_2
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 13157
    const/4 v0, 0x4

    iget-wide v2, p0, Ljfl;->ein:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->g(IJ)V

    .line 13159
    :cond_3
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 13160
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljfl;->eip:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13162
    :cond_4
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 13163
    const/4 v0, 0x6

    iget-object v1, p0, Ljfl;->eio:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13165
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13166
    return-void
.end method

.method public final bjs()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13004
    iget-object v0, p0, Ljfl;->eil:Ljava/lang/String;

    return-object v0
.end method

.method public final bjt()Z
    .locals 1

    .prologue
    .line 13015
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bju()Z
    .locals 1

    .prologue
    .line 13037
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bjv()J
    .locals 2

    .prologue
    .line 13070
    iget-wide v0, p0, Ljfl;->ein:J

    return-wide v0
.end method

.method public final bjw()Z
    .locals 1

    .prologue
    .line 13078
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dd(J)Ljfl;
    .locals 1

    .prologue
    .line 13073
    iput-wide p1, p0, Ljfl;->ein:J

    .line 13074
    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfl;->aez:I

    .line 13075
    return-object p0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13048
    iget-object v0, p0, Ljfl;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getSymbol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13026
    iget-object v0, p0, Ljfl;->eim:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 13170
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13171
    iget v1, p0, Ljfl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13172
    const/4 v1, 0x1

    iget-object v2, p0, Ljfl;->eil:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13175
    :cond_0
    iget v1, p0, Ljfl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13176
    const/4 v1, 0x2

    iget-object v2, p0, Ljfl;->eim:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13179
    :cond_1
    iget v1, p0, Ljfl;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 13180
    const/4 v1, 0x3

    iget-object v2, p0, Ljfl;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13183
    :cond_2
    iget v1, p0, Ljfl;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 13184
    const/4 v1, 0x4

    iget-wide v2, p0, Ljfl;->ein:J

    invoke-static {v1, v2, v3}, Ljsj;->j(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13187
    :cond_3
    iget v1, p0, Ljfl;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 13188
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljfl;->eip:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13191
    :cond_4
    iget v1, p0, Ljfl;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 13192
    const/4 v1, 0x6

    iget-object v2, p0, Ljfl;->eio:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13195
    :cond_5
    return v0
.end method

.method public final pX()Z
    .locals 1

    .prologue
    .line 13059
    iget v0, p0, Ljfl;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final up(Ljava/lang/String;)Ljfl;
    .locals 1

    .prologue
    .line 13007
    if-nez p1, :cond_0

    .line 13008
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13010
    :cond_0
    iput-object p1, p0, Ljfl;->eil:Ljava/lang/String;

    .line 13011
    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfl;->aez:I

    .line 13012
    return-object p0
.end method

.method public final uq(Ljava/lang/String;)Ljfl;
    .locals 1

    .prologue
    .line 13029
    if-nez p1, :cond_0

    .line 13030
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13032
    :cond_0
    iput-object p1, p0, Ljfl;->eim:Ljava/lang/String;

    .line 13033
    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfl;->aez:I

    .line 13034
    return-object p0
.end method

.method public final ur(Ljava/lang/String;)Ljfl;
    .locals 1

    .prologue
    .line 13051
    if-nez p1, :cond_0

    .line 13052
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13054
    :cond_0
    iput-object p1, p0, Ljfl;->aiK:Ljava/lang/String;

    .line 13055
    iget v0, p0, Ljfl;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfl;->aez:I

    .line 13056
    return-object p0
.end method
