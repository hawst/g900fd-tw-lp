.class public final Lght;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cJB:Ljava/lang/Object;

.field private cKu:Lghv;

.field private cKv:Ljava/lang/Boolean;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lght;->mContentResolver:Landroid/content/ContentResolver;

    .line 52
    iput-object p2, p0, Lght;->mExecutor:Ljava/util/concurrent/Executor;

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lght;->cJB:Ljava/lang/Object;

    .line 54
    return-void
.end method


# virtual methods
.method public final aFM()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 59
    iget-object v1, p0, Lght;->cJB:Ljava/lang/Object;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-object v0, p0, Lght;->cKv:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lght;->cKu:Lghv;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lghv;

    iget-object v2, p0, Lght;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {v0, p0, v2}, Lghv;-><init>(Lght;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lght;->cKu:Lghv;

    .line 65
    iget-object v0, p0, Lght;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lght;->cKu:Lghv;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lght;->cKv:Ljava/lang/Boolean;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final b(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    iget-object v1, p0, Lght;->cJB:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    :try_start_0
    iput-object p1, p0, Lght;->cKv:Ljava/lang/Boolean;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lght;->cKu:Lghv;

    .line 76
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
