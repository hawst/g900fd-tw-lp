.class public final Ljgc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ail:I

.field private dIb:I

.field private dPh:[B

.field public ejq:Ljeh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 969
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 970
    const/4 v0, 0x0

    iput v0, p0, Ljgc;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljgc;->ail:I

    const/16 v0, 0xc

    iput v0, p0, Ljgc;->dIb:I

    iput-object v1, p0, Ljgc;->ejq:Ljeh;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljgc;->dPh:[B

    iput-object v1, p0, Ljgc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgc;->eCz:I

    .line 971
    return-void
.end method

.method public static al([B)Ljgc;
    .locals 1

    .prologue
    .line 1093
    new-instance v0, Ljgc;

    invoke-direct {v0}, Ljgc;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljgc;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 865
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljgc;->ail:I

    iget v0, p0, Ljgc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljgc;->dIb:I

    iget v0, p0, Ljgc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgc;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljgc;->ejq:Ljeh;

    if-nez v0, :cond_1

    new-instance v0, Ljeh;

    invoke-direct {v0}, Ljeh;-><init>()V

    iput-object v0, p0, Ljgc;->ejq:Ljeh;

    :cond_1
    iget-object v0, p0, Ljgc;->ejq:Ljeh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljgc;->dPh:[B

    iget v0, p0, Ljgc;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 987
    iget v0, p0, Ljgc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 988
    const/4 v0, 0x1

    iget v1, p0, Ljgc;->ail:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 990
    :cond_0
    iget v0, p0, Ljgc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 991
    const/4 v0, 0x2

    iget v1, p0, Ljgc;->dIb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 993
    :cond_1
    iget-object v0, p0, Ljgc;->ejq:Ljeh;

    if-eqz v0, :cond_2

    .line 994
    const/4 v0, 0x3

    iget-object v1, p0, Ljgc;->ejq:Ljeh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 996
    :cond_2
    iget v0, p0, Ljgc;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 997
    const/4 v0, 0x4

    iget-object v1, p0, Ljgc;->dPh:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 999
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1000
    return-void
.end method

.method public final bdc()[B
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Ljgc;->dPh:[B

    return-object v0
.end method

.method public final bjO()Z
    .locals 1

    .prologue
    .line 936
    iget v0, p0, Ljgc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getErrorCode()I
    .locals 1

    .prologue
    .line 928
    iget v0, p0, Ljgc;->dIb:I

    return v0
.end method

.method public final getStatus()I
    .locals 1

    .prologue
    .line 909
    iget v0, p0, Ljgc;->ail:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1004
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1005
    iget v1, p0, Ljgc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1006
    const/4 v1, 0x1

    iget v2, p0, Ljgc;->ail:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1009
    :cond_0
    iget v1, p0, Ljgc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1010
    const/4 v1, 0x2

    iget v2, p0, Ljgc;->dIb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1013
    :cond_1
    iget-object v1, p0, Ljgc;->ejq:Ljeh;

    if-eqz v1, :cond_2

    .line 1014
    const/4 v1, 0x3

    iget-object v2, p0, Ljgc;->ejq:Ljeh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1017
    :cond_2
    iget v1, p0, Ljgc;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 1018
    const/4 v1, 0x4

    iget-object v2, p0, Ljgc;->dPh:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1021
    :cond_3
    return v0
.end method

.method public final pv(I)Ljgc;
    .locals 1

    .prologue
    .line 912
    const/4 v0, 0x1

    iput v0, p0, Ljgc;->ail:I

    .line 913
    iget v0, p0, Ljgc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgc;->aez:I

    .line 914
    return-object p0
.end method
