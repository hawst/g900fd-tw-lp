.class public abstract Lejw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cco:Lijj;

.field public ccp:Z

.field public ccq:Z

.field private final mObservers:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 43
    invoke-static {p1}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-direct {p0, v0}, Lejw;-><init>(Lijj;)V

    .line 44
    return-void
.end method

.method private constructor <init>(Lijj;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v0, p0, Lejw;->ccp:Z

    .line 19
    iput-boolean v0, p0, Lejw;->ccq:Z

    .line 54
    iput-object p1, p0, Lejw;->cco:Lijj;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lejw;->mObservers:Ljava/util/ArrayList;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {p1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-direct {p0, v0}, Lejw;-><init>(Lijj;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lejx;)V
    .locals 1

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lejw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract atR()V
.end method

.method protected abstract atS()V
.end method

.method public final declared-synchronized commit()V
    .locals 2

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lejw;->ccp:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 103
    :cond_0
    monitor-exit p0

    return-void

    .line 98
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lejw;->atR()V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejw;->ccp:Z

    .line 100
    iget-object v0, p0, Lejw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejx;

    .line 101
    invoke-interface {v0, p0}, Lejx;->c(Lejw;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized restore()V
    .locals 2

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lejw;->ccp:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 123
    :cond_0
    monitor-exit p0

    return-void

    .line 118
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lejw;->atS()V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejw;->ccp:Z

    .line 120
    iget-object v0, p0, Lejw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejx;

    .line 121
    invoke-interface {v0, p0}, Lejx;->d(Lejw;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
