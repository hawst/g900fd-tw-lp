.class public final Lhlp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlk;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcfo;

.field private final mIntentStarter:Leoj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leoj;Lcfo;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lhlp;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lhlp;->mIntentStarter:Leoj;

    .line 34
    iput-object p3, p0, Lhlp;->mCoreSearchServices:Lcfo;

    .line 35
    return-void
.end method

.method private a(Lcom/google/android/search/shared/actions/ContactOptInAction;I)Z
    .locals 6

    .prologue
    const v5, 0x7f0a0126

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 45
    if-ne p2, v0, :cond_1

    .line 46
    iget-object v1, p0, Lhlp;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-interface {v1, v0}, Lcke;->cd(Z)V

    .line 48
    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/ContactOptInAction;->el(Z)V

    .line 49
    iget-object v1, p0, Lhlp;->mCoreSearchServices:Lcfo;

    invoke-static {v1}, Lcnj;->a(Lcfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lhlp;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DO()Lgpp;

    move-result-object v1

    const-string v2, "log_contact_accounts_to_clearcut"

    invoke-interface {v1, v2}, Lgpp;->nw(Ljava/lang/String;)V

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    const/4 v2, 0x2

    if-ne p2, v2, :cond_2

    .line 57
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    :try_start_0
    iget-object v3, p0, Lhlp;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0126

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 66
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 67
    iget-object v3, p0, Lhlp;->mIntentStarter:Leoj;

    new-array v0, v0, [Landroid/content/Intent;

    aput-object v2, v0, v1

    invoke-interface {v3, v0}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    const-string v2, "ContactOptInActionExecutor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "execute() : Failed to parse \"Learn more\" URL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lhlp;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 64
    goto :goto_0

    :cond_2
    move v0, v1

    .line 69
    goto :goto_0
.end method


# virtual methods
.method public final synthetic G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/search/shared/actions/ContactOptInAction;

    invoke-direct {p0, p1, p2}, Lhlp;->a(Lcom/google/android/search/shared/actions/ContactOptInAction;I)Z

    move-result v0

    return v0
.end method
