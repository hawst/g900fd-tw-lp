.class public final Lgmt;
.super Lgmq;
.source "PG"


# instance fields
.field private final bYA:Ljava/lang/String;

.field private final cPv:Ljava/lang/String;

.field private final mDeviceParams:Lgmz;

.field private final mLocationHelper:Lglo;

.field private final mSpeechContext:Lgij;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Lglo;Lgij;Lgdo;Ljava/lang/String;Lgmz;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lglo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lgij;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lgdo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lgmz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    const-string v0, "S3ClientInfoBuilderTask"

    invoke-direct {p0, v0}, Lgmq;-><init>(Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lgmt;->mLocationHelper:Lglo;

    .line 48
    iput-object p2, p0, Lgmt;->mSpeechContext:Lgij;

    .line 49
    iput-object p3, p0, Lgmt;->mSpeechSettings:Lgdo;

    .line 50
    iput-object p4, p0, Lgmt;->cPv:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Lgmt;->mDeviceParams:Lgmz;

    .line 52
    iput-object p6, p0, Lgmt;->bYA:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method protected final synthetic aHr()Ljava/lang/Object;
    .locals 14

    .prologue
    const-wide v12, 0x416312d000000000L    # 1.0E7

    .line 27
    new-instance v0, Ljwu;

    invoke-direct {v0}, Ljwu;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljwu;->zz(Ljava/lang/String;)Ljwu;

    move-result-object v0

    const-string v1, "Android"

    invoke-virtual {v0, v1}, Ljwu;->zB(Ljava/lang/String;)Ljwu;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljwu;->zC(Ljava/lang/String;)Ljwu;

    move-result-object v0

    iget-object v1, p0, Lgmt;->cPv:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljwu;->zD(Ljava/lang/String;)Ljwu;

    move-result-object v0

    iget-object v1, p0, Lgmt;->mDeviceParams:Lgmz;

    invoke-interface {v1}, Lgmz;->Dm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljwu;->zE(Ljava/lang/String;)Ljwu;

    move-result-object v0

    iget-object v1, p0, Lgmt;->mDeviceParams:Lgmz;

    invoke-interface {v1}, Lgmz;->OJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljwu;->zA(Ljava/lang/String;)Ljwu;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljwu;->zF(Ljava/lang/String;)Ljwu;

    move-result-object v2

    iget-object v0, p0, Lgmt;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFe()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, Lgmt;->mLocationHelper:Lglo;

    invoke-interface {v0}, Lglo;->aHf()Landroid/location/Location;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lgmt;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aFZ()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgng;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    mul-double/2addr v6, v12

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    mul-double/2addr v8, v12

    iget-object v1, v0, Lgng;->cPT:Lkey;

    iget-object v1, v1, Lkey;->eVn:Lkex;

    invoke-virtual {v1}, Lkex;->bzK()I

    move-result v1

    int-to-double v10, v1

    cmpg-double v1, v6, v10

    if-gtz v1, :cond_1

    iget-object v1, v0, Lgng;->cPT:Lkey;

    iget-object v1, v1, Lkey;->eVn:Lkex;

    invoke-virtual {v1}, Lkex;->bzL()I

    move-result v1

    int-to-double v10, v1

    cmpg-double v1, v8, v10

    if-gtz v1, :cond_1

    iget-object v1, v0, Lgng;->cPT:Lkey;

    iget-object v1, v1, Lkey;->eVm:Lkex;

    invoke-virtual {v1}, Lkex;->bzK()I

    move-result v1

    int-to-double v10, v1

    cmpl-double v1, v6, v10

    if-ltz v1, :cond_1

    iget-object v1, v0, Lgng;->cPT:Lkey;

    iget-object v1, v1, Lkey;->eVm:Lkex;

    invoke-virtual {v1}, Lkex;->bzL()I

    move-result v1

    int-to-double v6, v1

    cmpl-double v1, v8, v6

    if-ltz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    iget-object v0, v0, Lgng;->cPU:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lgmt;->mDeviceParams:Lgmz;

    invoke-interface {v0}, Lgmz;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2, v1}, Ljwu;->sN(I)Ljwu;

    move-result-object v1

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v4}, Ljwu;->sO(I)Ljwu;

    move-result-object v1

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Ljwu;->sP(I)Ljwu;

    :cond_3
    iget-object v0, p0, Lgmt;->bYA:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgmt;->bYA:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljwu;->zG(Ljava/lang/String;)Ljwu;

    :cond_4
    const-class v0, Ljava/lang/String;

    invoke-static {v3, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v2, Ljwu;->eHq:[Ljava/lang/String;

    return-object v2
.end method
