.class public final Lfxz;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 6

    .prologue
    const v5, 0x7f11033b

    const v4, 0x7f11033a

    .line 31
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 32
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agD:Laos;

    .line 34
    invoke-virtual {v1}, Laos;->rn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    const v2, 0x7f110198

    invoke-virtual {v1}, Laos;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 38
    :cond_0
    invoke-virtual {v1}, Laos;->rp()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 39
    const v2, 0x7f11033d

    invoke-virtual {v1}, Laos;->ro()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 42
    :cond_1
    invoke-virtual {v1}, Laos;->rr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 43
    invoke-virtual {v1}, Laos;->rq()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v4, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 46
    :cond_2
    invoke-virtual {v1}, Laos;->rt()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 47
    invoke-virtual {v1}, Laos;->rs()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v5, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 50
    :cond_3
    invoke-virtual {v1}, Laos;->rv()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 51
    invoke-virtual {v1}, Laos;->ru()I

    move-result v2

    invoke-static {v0, v4, v2}, Lgab;->i(Landroid/view/View;II)Landroid/widget/TextView;

    .line 52
    invoke-virtual {v1}, Laos;->ru()I

    move-result v1

    invoke-static {v0, v5, v1}, Lgab;->i(Landroid/view/View;II)Landroid/widget/TextView;

    .line 54
    :cond_4
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 25
    const v0, 0x7f04012a

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
