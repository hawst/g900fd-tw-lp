.class final Lfat;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lifg;


# instance fields
.field private synthetic ckM:Lemp;

.field private synthetic cnz:Lfaq;


# direct methods
.method constructor <init>(Lfaq;Lemp;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lfat;->cnz:Lfaq;

    iput-object p2, p0, Lfat;->ckM:Lemp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private L([B)Ljava/lang/Void;
    .locals 14

    .prologue
    const/16 v13, 0x1e

    const/4 v0, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 256
    .line 258
    if-nez p1, :cond_2

    move v0, v11

    .line 299
    :cond_0
    :goto_0
    iget-object v1, p0, Lfat;->cnz:Lfaq;

    iget-object v1, v1, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 301
    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lfat;->cnz:Lfaq;

    invoke-virtual {v0, v13}, Lfaq;->iu(I)V

    .line 306
    :cond_1
    :goto_1
    return-object v12

    .line 262
    :cond_2
    :try_start_0
    new-instance v10, Lamf;

    invoke-direct {v10}, Lamf;-><init>()V

    .line 263
    invoke-static {v10, p1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 264
    iget-object v1, p0, Lfat;->ckM:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v10}, Lamf;->mM()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 265
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_3

    iget-object v1, p0, Lfat;->cnz:Lfaq;

    invoke-virtual {v1}, Lfaq;->axj()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    :cond_3
    move v0, v11

    .line 267
    goto :goto_0

    .line 272
    :cond_4
    invoke-virtual {v10}, Lamf;->mO()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 273
    invoke-virtual {v10}, Lamf;->getLocale()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lepb;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v7

    .line 276
    :goto_2
    iget-object v1, p0, Lfat;->cnz:Lfaq;

    iget-object v1, v1, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 277
    invoke-virtual {v10}, Lamf;->mN()Z

    move-result v6

    .line 279
    iget-object v1, v10, Lamf;->aeC:Lizn;

    if-eqz v1, :cond_0

    iget-object v1, v10, Lamf;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lfat;->cnz:Lfaq;

    iget-object v2, v10, Lamf;->aeC:Lizn;

    iget-object v3, v10, Lamf;->aeB:Ljbp;

    invoke-virtual {v10}, Lamf;->mM()J

    move-result-wide v4

    if-eqz v6, :cond_5

    const/4 v6, 0x3

    :goto_3
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v10}, Lamf;->mP()Z

    move-result v10

    invoke-virtual/range {v1 .. v10}, Lfaq;->a(Lizn;Ljbp;JILjava/util/Locale;Z[BZ)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "EntryProvider"

    const-string v1, "File storage contained invalid data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    iget-object v0, p0, Lfat;->cnz:Lfaq;

    iget-object v0, v0, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 301
    iget-object v0, p0, Lfat;->cnz:Lfaq;

    invoke-virtual {v0, v13}, Lfaq;->iu(I)V

    goto :goto_1

    .line 281
    :cond_5
    const/4 v6, 0x2

    goto :goto_3

    .line 299
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lfat;->cnz:Lfaq;

    iget-object v1, v1, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 302
    throw v0

    :cond_6
    move-object v7, v12

    goto :goto_2
.end method


# virtual methods
.method public final synthetic as(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 250
    check-cast p1, [B

    invoke-direct {p0, p1}, Lfat;->L([B)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
