.class public final Lgig;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# static fields
.field private static final cLb:Lijj;


# instance fields
.field private final bCq:Ljava/util/Locale;

.field private final cJY:Lchk;

.field private final mDiscourseContext:Lcky;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    const-string v0, "send it"

    const-string v1, "yes"

    const-string v2, "no"

    const-string v3, "cancel"

    invoke-static {v0, v1, v2, v3}, Lijj;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    sput-object v0, Lgig;->cLb:Lijj;

    return-void
.end method

.method public constructor <init>(Lchk;Ljava/util/Locale;Ligi;)V
    .locals 1
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Locale;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ligi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lgig;->cJY:Lchk;

    .line 35
    iput-object p2, p0, Lgig;->bCq:Ljava/util/Locale;

    .line 36
    invoke-interface {p3}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    iput-object v0, p0, Lgig;->mDiscourseContext:Lcky;

    .line 37
    return-void
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    iget-object v1, p0, Lgig;->mDiscourseContext:Lcky;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgig;->mDiscourseContext:Lcky;

    invoke-virtual {v1}, Lcky;->Pp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    if-nez v1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-object v0

    .line 44
    :cond_1
    iget-object v1, p0, Lgig;->cJY:Lchk;

    invoke-virtual {v1}, Lchk;->Gj()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgig;->bCq:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lgig;->mDiscourseContext:Lcky;

    invoke-virtual {v1}, Lcky;->Pp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alk()I

    move-result v1

    .line 51
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    sget-object v0, Lgig;->cLb:Lijj;

    goto :goto_0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lgig;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
