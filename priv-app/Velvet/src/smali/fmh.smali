.class final Lfmh;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic cvO:Lfmd;


# direct methods
.method constructor <init>(Lfmd;)V
    .locals 0

    .prologue
    .line 878
    iput-object p1, p0, Lfmh;->cvO:Lfmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 878
    iget-object v0, p0, Lfmh;->cvO:Lfmd;

    iget-object v0, v0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NowOverlay"

    const-string v1, "Can\'t launch help before the remote client is connected"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfmh;->cvO:Lfmd;

    iget-object v0, v0, Lfmd;->mNowRemoteClient:Lfml;

    iget-object v1, p0, Lfmh;->cvO:Lfmd;

    iget-object v1, v1, Lfmd;->cvM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfml;->lN(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lfmh;->cvO:Lfmd;

    iget-object v0, v0, Lfmd;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->azP()Landroid/accounts/Account;

    move-result-object v2

    new-instance v0, Landroid/util/Pair;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 878
    check-cast p1, Landroid/util/Pair;

    if-eqz p1, :cond_0

    iget-object v4, p0, Lfmh;->cvO:Lfmd;

    iget-object v0, p0, Lfmh;->cvO:Lfmd;

    iget-object v0, v0, Lfmd;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfmh;->cvO:Lfmd;

    iget-object v1, v1, Lfmd;->cvM:Ljava/lang/String;

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/accounts/Account;

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/net/Uri;

    iget-object v5, v4, Lfmd;->cvI:Lflw;

    iget-object v5, v5, Lflw;->cvw:Lflr;

    iget-object v5, v5, Lflr;->cuJ:Lfkz;

    invoke-virtual {v5}, Lfkz;->aAG()Z

    new-instance v5, Landroid/util/Pair;

    const-string v6, "GEL"

    const-string v7, "true"

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v5}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v6

    iget-object v4, v4, Lfmd;->cvI:Lflw;

    iget-object v4, v4, Lflw;->cvp:Lcom/google/android/shared/ui/CoScrollContainer;

    const/4 v5, 0x1

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lgbk;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V

    :cond_0
    return-void
.end method
