.class public final Ljdk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ece:[Ljdk;


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private agv:I

.field private cQx:J

.field public ecf:[I

.field private ecg:Ljava/lang/String;

.field private ech:Ljava/lang/String;

.field private eci:Ljava/lang/String;

.field private ecj:Ljava/lang/String;

.field public eck:Ljdm;

.field private ecl:I

.field public ecm:[Ljdl;

.field public ecn:[Ljdl;

.field public eco:[Ljdl;

.field public ecp:[Ljdl;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljdk;->aez:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljdk;->ecf:[I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljdk;->cQx:J

    iput v2, p0, Ljdk;->agv:I

    const-string v0, ""

    iput-object v0, p0, Ljdk;->ecg:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdk;->ech:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdk;->eci:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdk;->ecj:Ljava/lang/String;

    iput-object v3, p0, Ljdk;->eck:Ljdm;

    iput v2, p0, Ljdk;->ecl:I

    invoke-static {}, Ljdl;->bhz()[Ljdl;

    move-result-object v0

    iput-object v0, p0, Ljdk;->ecm:[Ljdl;

    invoke-static {}, Ljdl;->bhz()[Ljdl;

    move-result-object v0

    iput-object v0, p0, Ljdk;->ecn:[Ljdl;

    invoke-static {}, Ljdl;->bhz()[Ljdl;

    move-result-object v0

    iput-object v0, p0, Ljdk;->eco:[Ljdl;

    invoke-static {}, Ljdl;->bhz()[Ljdl;

    move-result-object v0

    iput-object v0, p0, Ljdk;->ecp:[Ljdl;

    const-string v0, ""

    iput-object v0, p0, Ljdk;->aeE:Ljava/lang/String;

    iput-object v3, p0, Ljdk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdk;->eCz:I

    return-void
.end method

.method public static bhq()[Ljdk;
    .locals 2

    sget-object v0, Ljdk;->ece:[Ljdk;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdk;->ece:[Ljdk;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljdk;

    sput-object v0, Ljdk;->ece:[Ljdk;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljdk;->ece:[Ljdk;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljdk;->cQx:J

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdk;->agv:I

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdk;->ecg:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdk;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdk;->ech:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdk;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljdk;->eck:Ljdm;

    if-nez v0, :cond_1

    new-instance v0, Ljdm;

    invoke-direct {v0}, Ljdm;-><init>()V

    iput-object v0, p0, Ljdk;->eck:Ljdm;

    :cond_1
    iget-object v0, p0, Ljdk;->eck:Ljdm;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdk;->ecm:[Ljdl;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdl;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljdk;->ecm:[Ljdl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljdk;->ecm:[Ljdl;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdk;->ecm:[Ljdl;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x40

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_6

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_1
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_6
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljdk;->ecf:[I

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    if-nez v0, :cond_8

    array-length v3, v5

    if-ne v2, v3, :cond_8

    iput-object v5, p0, Ljdk;->ecf:[I

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Ljdk;->ecf:[I

    array-length v0, v0

    goto :goto_5

    :cond_8
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_9

    iget-object v4, p0, Ljdk;->ecf:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljdk;->ecf:[I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    if-eqz v0, :cond_e

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljdk;->ecf:[I

    if-nez v2, :cond_c

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_b

    iget-object v0, p0, Ljdk;->ecf:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_d

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_3

    goto :goto_8

    :pswitch_3
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_c
    iget-object v2, p0, Ljdk;->ecf:[I

    array-length v2, v2

    goto :goto_7

    :cond_d
    iput-object v4, p0, Ljdk;->ecf:[I

    :cond_e
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    iput v0, p0, Ljdk;->ecl:I

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljdk;->aez:I

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdk;->eco:[Ljdl;

    if-nez v0, :cond_10

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdl;

    if-eqz v0, :cond_f

    iget-object v3, p0, Ljdk;->eco:[Ljdl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_11

    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_10
    iget-object v0, p0, Ljdk;->eco:[Ljdl;

    array-length v0, v0

    goto :goto_9

    :cond_11
    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdk;->eco:[Ljdl;

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdk;->ecn:[Ljdl;

    if-nez v0, :cond_13

    move v0, v1

    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdl;

    if-eqz v0, :cond_12

    iget-object v3, p0, Ljdk;->ecn:[Ljdl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    :goto_c
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_14

    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_13
    iget-object v0, p0, Ljdk;->ecn:[Ljdl;

    array-length v0, v0

    goto :goto_b

    :cond_14
    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdk;->ecn:[Ljdl;

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdk;->ecp:[Ljdl;

    if-nez v0, :cond_16

    move v0, v1

    :goto_d
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdl;

    if-eqz v0, :cond_15

    iget-object v3, p0, Ljdk;->ecp:[Ljdl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_15
    :goto_e
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_17

    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_16
    iget-object v0, p0, Ljdk;->ecp:[Ljdl;

    array-length v0, v0

    goto :goto_d

    :cond_17
    new-instance v3, Ljdl;

    invoke-direct {v3}, Ljdl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdk;->ecp:[Ljdl;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdk;->ecj:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdk;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdk;->aeE:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljdk;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdk;->eci:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdk;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x40 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v2, p0, Ljdk;->cQx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_0
    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v2, p0, Ljdk;->agv:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    :cond_1
    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v2, p0, Ljdk;->ecg:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v2, p0, Ljdk;->ech:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljdk;->eck:Ljdm;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v2, p0, Ljdk;->eck:Ljdm;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_4
    iget-object v0, p0, Ljdk;->ecm:[Ljdl;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljdk;->ecm:[Ljdl;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljdk;->ecm:[Ljdl;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Ljdk;->ecm:[Ljdl;

    aget-object v2, v2, v0

    if-eqz v2, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Ljdk;->ecf:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljdk;->ecf:[I

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljdk;->ecf:[I

    array-length v2, v2

    if-ge v0, v2, :cond_7

    const/16 v2, 0x8

    iget-object v3, p0, Ljdk;->ecf:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget v2, p0, Ljdk;->ecl:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    :cond_8
    iget-object v0, p0, Ljdk;->eco:[Ljdl;

    if-eqz v0, :cond_a

    iget-object v0, p0, Ljdk;->eco:[Ljdl;

    array-length v0, v0

    if-lez v0, :cond_a

    move v0, v1

    :goto_2
    iget-object v2, p0, Ljdk;->eco:[Ljdl;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Ljdk;->eco:[Ljdl;

    aget-object v2, v2, v0

    if-eqz v2, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    iget-object v0, p0, Ljdk;->ecn:[Ljdl;

    if-eqz v0, :cond_c

    iget-object v0, p0, Ljdk;->ecn:[Ljdl;

    array-length v0, v0

    if-lez v0, :cond_c

    move v0, v1

    :goto_3
    iget-object v2, p0, Ljdk;->ecn:[Ljdl;

    array-length v2, v2

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Ljdk;->ecn:[Ljdl;

    aget-object v2, v2, v0

    if-eqz v2, :cond_b

    const/16 v3, 0xb

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_c
    iget-object v0, p0, Ljdk;->ecp:[Ljdl;

    if-eqz v0, :cond_e

    iget-object v0, p0, Ljdk;->ecp:[Ljdl;

    array-length v0, v0

    if-lez v0, :cond_e

    :goto_4
    iget-object v0, p0, Ljdk;->ecp:[Ljdl;

    array-length v0, v0

    if-ge v1, v0, :cond_e

    iget-object v0, p0, Ljdk;->ecp:[Ljdl;

    aget-object v0, v0, v1

    if-eqz v0, :cond_d

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_e
    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_f

    const/16 v0, 0xd

    iget-object v1, p0, Ljdk;->ecj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_f
    iget v0, p0, Ljdk;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_10

    const/16 v0, 0xe

    iget-object v1, p0, Ljdk;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_10
    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_11

    const/16 v0, 0xf

    iget-object v1, p0, Ljdk;->eci:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_11
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdk;->ecg:Ljava/lang/String;

    return-object v0
.end method

.method public final bhs()Z
    .locals 1

    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bht()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdk;->ech:Ljava/lang/String;

    return-object v0
.end method

.method public final bhu()Z
    .locals 1

    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhv()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdk;->ecj:Ljava/lang/String;

    return-object v0
.end method

.method public final bhw()Z
    .locals 1

    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhx()I
    .locals 1

    iget v0, p0, Ljdk;->ecl:I

    return v0
.end method

.method public final bhy()Z
    .locals 1

    iget v0, p0, Ljdk;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cU(J)Ljdk;
    .locals 1

    iput-wide p1, p0, Ljdk;->cQx:J

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdk;->aez:I

    return-object p0
.end method

.method public final getId()J
    .locals 2

    iget-wide v0, p0, Ljdk;->cQx:J

    return-wide v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Ljdk;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Ljdk;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-wide v4, p0, Ljdk;->cQx:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Ljdk;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget v3, p0, Ljdk;->agv:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget v2, p0, Ljdk;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Ljdk;->ecg:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Ljdk;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget-object v3, p0, Ljdk;->ech:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Ljdk;->eck:Ljdm;

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    iget-object v3, p0, Ljdk;->eck:Ljdm;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Ljdk;->ecm:[Ljdl;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljdk;->ecm:[Ljdl;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ljdk;->ecm:[Ljdl;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    iget-object v3, p0, Ljdk;->ecm:[Ljdl;

    aget-object v3, v3, v0

    if-eqz v3, :cond_5

    const/4 v4, 0x6

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    move v0, v2

    :cond_7
    iget-object v2, p0, Ljdk;->ecf:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljdk;->ecf:[I

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    move v3, v1

    :goto_1
    iget-object v4, p0, Ljdk;->ecf:[I

    array-length v4, v4

    if-ge v2, v4, :cond_8

    iget-object v4, p0, Ljdk;->ecf:[I

    aget v4, v4, v2

    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    add-int/2addr v0, v3

    iget-object v2, p0, Ljdk;->ecf:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_9
    iget v2, p0, Ljdk;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_a

    const/16 v2, 0x9

    iget v3, p0, Ljdk;->ecl:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Ljdk;->eco:[Ljdl;

    if-eqz v2, :cond_d

    iget-object v2, p0, Ljdk;->eco:[Ljdl;

    array-length v2, v2

    if-lez v2, :cond_d

    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Ljdk;->eco:[Ljdl;

    array-length v3, v3

    if-ge v0, v3, :cond_c

    iget-object v3, p0, Ljdk;->eco:[Ljdl;

    aget-object v3, v3, v0

    if-eqz v3, :cond_b

    const/16 v4, 0xa

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    move v0, v2

    :cond_d
    iget-object v2, p0, Ljdk;->ecn:[Ljdl;

    if-eqz v2, :cond_10

    iget-object v2, p0, Ljdk;->ecn:[Ljdl;

    array-length v2, v2

    if-lez v2, :cond_10

    move v2, v0

    move v0, v1

    :goto_3
    iget-object v3, p0, Ljdk;->ecn:[Ljdl;

    array-length v3, v3

    if-ge v0, v3, :cond_f

    iget-object v3, p0, Ljdk;->ecn:[Ljdl;

    aget-object v3, v3, v0

    if-eqz v3, :cond_e

    const/16 v4, 0xb

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_f
    move v0, v2

    :cond_10
    iget-object v2, p0, Ljdk;->ecp:[Ljdl;

    if-eqz v2, :cond_12

    iget-object v2, p0, Ljdk;->ecp:[Ljdl;

    array-length v2, v2

    if-lez v2, :cond_12

    :goto_4
    iget-object v2, p0, Ljdk;->ecp:[Ljdl;

    array-length v2, v2

    if-ge v1, v2, :cond_12

    iget-object v2, p0, Ljdk;->ecp:[Ljdl;

    aget-object v2, v2, v1

    if-eqz v2, :cond_11

    const/16 v3, 0xc

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_12
    iget v1, p0, Ljdk;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_13

    const/16 v1, 0xd

    iget-object v2, p0, Ljdk;->ecj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget v1, p0, Ljdk;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_14

    const/16 v1, 0xe

    iget-object v2, p0, Ljdk;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget v1, p0, Ljdk;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_15

    const/16 v1, 0xf

    iget-object v2, p0, Ljdk;->eci:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    return v0
.end method

.method public final oN(I)Ljdk;
    .locals 1

    iput p1, p0, Ljdk;->agv:I

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdk;->aez:I

    return-object p0
.end method

.method public final oO(I)Ljdk;
    .locals 1

    iput p1, p0, Ljdk;->ecl:I

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljdk;->aez:I

    return-object p0
.end method

.method public final tE(Ljava/lang/String;)Ljdk;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdk;->ecg:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdk;->aez:I

    return-object p0
.end method

.method public final tF(Ljava/lang/String;)Ljdk;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdk;->ech:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdk;->aez:I

    return-object p0
.end method

.method public final tG(Ljava/lang/String;)Ljdk;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdk;->ecj:Ljava/lang/String;

    iget v0, p0, Ljdk;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdk;->aez:I

    return-object p0
.end method
