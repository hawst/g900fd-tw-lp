.class abstract enum Limf;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum dEA:Limf;

.field public static final enum dEB:Limf;

.field public static final enum dEC:Limf;

.field public static final enum dED:Limf;

.field private static final synthetic dEE:[Limf;

.field public static final enum dEz:Limf;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 777
    new-instance v0, Limg;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Limg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Limf;->dEz:Limf;

    .line 790
    new-instance v0, Limh;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Limh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Limf;->dEA:Limf;

    .line 802
    new-instance v0, Limi;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Limi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Limf;->dEB:Limf;

    .line 813
    new-instance v0, Limj;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Limj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Limf;->dEC:Limf;

    .line 824
    new-instance v0, Limk;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Limk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Limf;->dED:Limf;

    .line 772
    const/4 v0, 0x5

    new-array v0, v0, [Limf;

    sget-object v1, Limf;->dEz:Limf;

    aput-object v1, v0, v2

    sget-object v1, Limf;->dEA:Limf;

    aput-object v1, v0, v3

    sget-object v1, Limf;->dEB:Limf;

    aput-object v1, v0, v4

    sget-object v1, Limf;->dEC:Limf;

    aput-object v1, v0, v5

    sget-object v1, Limf;->dED:Limf;

    aput-object v1, v0, v6

    sput-object v0, Limf;->dEE:[Limf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 772
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 772
    invoke-direct {p0, p1, p2}, Limf;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Limf;
    .locals 1

    .prologue
    .line 772
    const-class v0, Limf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Limf;

    return-object v0
.end method

.method public static values()[Limf;
    .locals 1

    .prologue
    .line 772
    sget-object v0, Limf;->dEE:[Limf;

    invoke-virtual {v0}, [Limf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Limf;

    return-object v0
.end method
