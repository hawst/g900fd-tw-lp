.class public final Lfhp;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field crR:Lfhq;

.field crS:Z

.field crT:Ljhq;

.field public crU:I

.field mNetworkClient:Lfcx;

.field mTrainingQuestionManager:Lfdn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfhp;->setRetainInstance(Z)V

    .line 64
    return-void
.end method

.method static e(Lizn;)I
    .locals 3

    .prologue
    .line 126
    const/4 v1, 0x0

    .line 127
    invoke-static {p0}, Lcxg;->a(Lizn;)Landroid/util/Pair;

    move-result-object v2

    .line 128
    if-eqz v2, :cond_1

    .line 129
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lizj;

    invoke-static {v0}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x1

    move v1, v0

    .line 133
    :cond_0
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lizj;

    invoke-static {v0}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    add-int/lit8 v1, v1, 0x1

    .line 138
    :cond_1
    return v1
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 70
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v1

    iput-object v1, p0, Lfhp;->mNetworkClient:Lfcx;

    .line 72
    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v0

    iput-object v0, p0, Lfhp;->mTrainingQuestionManager:Lfdn;

    .line 73
    iget-object v0, p0, Lfhp;->crT:Ljhq;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfhp;->crR:Lfhq;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lfhq;

    invoke-direct {v0, p0}, Lfhq;-><init>(Lfhp;)V

    iput-object v0, p0, Lfhp;->crR:Lfhq;

    .line 75
    iget-object v0, p0, Lfhp;->crR:Lfhq;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfhq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 77
    :cond_0
    return-void
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lfhp;->crR:Lfhq;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lfhp;->crR:Lfhq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfhq;->cancel(Z)Z

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lfhp;->crR:Lfhq;

    .line 86
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 87
    return-void
.end method
