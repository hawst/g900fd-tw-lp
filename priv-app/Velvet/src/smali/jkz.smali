.class public final Ljkz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eqT:[Ljkz;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private bmK:F

.field private bmL:I

.field private bmN:Ljava/lang/String;

.field private eqU:Ljava/lang/String;

.field private eqV:Ljlq;

.field private eqW:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 13412
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13413
    iput v1, p0, Ljkz;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljkz;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljkz;->bmN:Ljava/lang/String;

    iput v1, p0, Ljkz;->bmL:I

    const/4 v0, 0x0

    iput v0, p0, Ljkz;->bmK:F

    const-string v0, ""

    iput-object v0, p0, Ljkz;->eqU:Ljava/lang/String;

    iput-object v2, p0, Ljkz;->eqV:Ljlq;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljkz;->eqW:J

    iput-object v2, p0, Ljkz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkz;->eCz:I

    .line 13414
    return-void
.end method

.method public static boI()[Ljkz;
    .locals 2

    .prologue
    .line 13273
    sget-object v0, Ljkz;->eqT:[Ljkz;

    if-nez v0, :cond_1

    .line 13274
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 13276
    :try_start_0
    sget-object v0, Ljkz;->eqT:[Ljkz;

    if-nez v0, :cond_0

    .line 13277
    const/4 v0, 0x0

    new-array v0, v0, [Ljkz;

    sput-object v0, Ljkz;->eqT:[Ljkz;

    .line 13279
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13281
    :cond_1
    sget-object v0, Ljkz;->eqT:[Ljkz;

    return-object v0

    .line 13279
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 13267
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkz;->agq:Ljava/lang/String;

    iget v0, p0, Ljkz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkz;->bmN:Ljava/lang/String;

    iget v0, p0, Ljkz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkz;->bmL:I

    iget v0, p0, Ljkz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkz;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljkz;->bmK:F

    iget v0, p0, Ljkz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljkz;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkz;->eqU:Ljava/lang/String;

    iget v0, p0, Ljkz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljkz;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljkz;->eqV:Ljlq;

    if-nez v0, :cond_1

    new-instance v0, Ljlq;

    invoke-direct {v0}, Ljlq;-><init>()V

    iput-object v0, p0, Ljkz;->eqV:Ljlq;

    :cond_1
    iget-object v0, p0, Ljkz;->eqV:Ljlq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljkz;->eqW:J

    iget v0, p0, Ljkz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljkz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 13433
    iget v0, p0, Ljkz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13434
    const/4 v0, 0x1

    iget-object v1, p0, Ljkz;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13436
    :cond_0
    iget v0, p0, Ljkz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13437
    const/4 v0, 0x2

    iget-object v1, p0, Ljkz;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13439
    :cond_1
    iget v0, p0, Ljkz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 13440
    const/4 v0, 0x3

    iget v1, p0, Ljkz;->bmL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 13442
    :cond_2
    iget v0, p0, Ljkz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 13443
    const/4 v0, 0x4

    iget v1, p0, Ljkz;->bmK:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 13445
    :cond_3
    iget v0, p0, Ljkz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 13446
    const/4 v0, 0x5

    iget-object v1, p0, Ljkz;->eqU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13448
    :cond_4
    iget-object v0, p0, Ljkz;->eqV:Ljlq;

    if-eqz v0, :cond_5

    .line 13449
    const/4 v0, 0x6

    iget-object v1, p0, Ljkz;->eqV:Ljlq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 13451
    :cond_5
    iget v0, p0, Ljkz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 13452
    const/4 v0, 0x7

    iget-wide v2, p0, Ljkz;->eqW:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->g(IJ)V

    .line 13454
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13455
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 13459
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13460
    iget v1, p0, Ljkz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13461
    const/4 v1, 0x1

    iget-object v2, p0, Ljkz;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13464
    :cond_0
    iget v1, p0, Ljkz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13465
    const/4 v1, 0x2

    iget-object v2, p0, Ljkz;->bmN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13468
    :cond_1
    iget v1, p0, Ljkz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 13469
    const/4 v1, 0x3

    iget v2, p0, Ljkz;->bmL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13472
    :cond_2
    iget v1, p0, Ljkz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 13473
    const/4 v1, 0x4

    iget v2, p0, Ljkz;->bmK:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 13476
    :cond_3
    iget v1, p0, Ljkz;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 13477
    const/4 v1, 0x5

    iget-object v2, p0, Ljkz;->eqU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13480
    :cond_4
    iget-object v1, p0, Ljkz;->eqV:Ljlq;

    if-eqz v1, :cond_5

    .line 13481
    const/4 v1, 0x6

    iget-object v2, p0, Ljkz;->eqV:Ljlq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13484
    :cond_5
    iget v1, p0, Ljkz;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 13485
    const/4 v1, 0x7

    iget-wide v2, p0, Ljkz;->eqW:J

    invoke-static {v1, v2, v3}, Ljsj;->j(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13488
    :cond_6
    return v0
.end method
