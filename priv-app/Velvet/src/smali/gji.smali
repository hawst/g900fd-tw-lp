.class final Lgji;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic cGA:Ljava/lang/String;

.field private synthetic cMn:Lgjx;

.field private synthetic cMo:Lgju;

.field private synthetic cMp:Ljwb;

.field private synthetic cMq:Ljzp;


# direct methods
.method constructor <init>(Lgjh;Lgjx;Ljava/lang/String;Lgju;Ljwb;Ljzp;)V
    .locals 0

    .prologue
    .line 140
    iput-object p2, p0, Lgji;->cMn:Lgjx;

    iput-object p3, p0, Lgji;->cGA:Ljava/lang/String;

    iput-object p4, p0, Lgji;->cMo:Lgju;

    iput-object p5, p0, Lgji;->cMp:Ljwb;

    iput-object p6, p0, Lgji;->cMq:Ljzp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 140
    iget-object v0, p0, Lgji;->cMn:Lgjx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgji;->cMn:Lgjx;

    iget-object v1, p0, Lgji;->cGA:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgjx;->mW(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lgji;->cMo:Lgju;

    iget-object v1, p0, Lgji;->cMp:Ljwb;

    invoke-virtual {v0, v1}, Lgju;->run(Ljwb;)Ljvn;

    move-result-object v0

    invoke-virtual {v0}, Ljvn;->getStatus()I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const-string v2, "Greco3EngineManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error running recognition: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_1
    iget-object v1, p0, Lgji;->cMn:Lgjx;

    if-eqz v1, :cond_2

    iget-object v0, v0, Ljvn;->eHL:Ljvf;

    iget-object v1, p0, Lgji;->cMq:Ljzp;

    invoke-static {v1}, Lgjh;->a(Ljzp;)Ljux;

    move-result-object v1

    iput-object v1, v0, Ljvf;->eGP:Ljux;

    iget-object v1, p0, Lgji;->cMq:Ljzp;

    invoke-virtual {v1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljvf;->zd(Ljava/lang/String;)Ljvf;

    iget-object v1, p0, Lgji;->cMn:Lgjx;

    iget-object v2, p0, Lgji;->cGA:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lgjx;->a(Ljava/lang/String;Ljvf;)V

    :cond_2
    iget-object v0, p0, Lgji;->cMo:Lgju;

    return-object v0
.end method
