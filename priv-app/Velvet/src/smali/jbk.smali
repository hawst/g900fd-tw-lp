.class public final Ljbk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dYE:Ljava/lang/String;

.field private dYF:Ljava/lang/String;

.field private dYG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32809
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 32810
    const/4 v0, 0x0

    iput v0, p0, Ljbk;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbk;->dYE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbk;->dYF:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbk;->dYG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljbk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbk;->eCz:I

    .line 32811
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 32724
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbk;->dYE:Ljava/lang/String;

    iget v0, p0, Ljbk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbk;->dYF:Ljava/lang/String;

    iget v0, p0, Ljbk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbk;->dYG:Ljava/lang/String;

    iget v0, p0, Ljbk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 32826
    iget v0, p0, Ljbk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 32827
    const/4 v0, 0x1

    iget-object v1, p0, Ljbk;->dYE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 32829
    :cond_0
    iget v0, p0, Ljbk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 32830
    const/4 v0, 0x2

    iget-object v1, p0, Ljbk;->dYF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 32832
    :cond_1
    iget v0, p0, Ljbk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 32833
    const/4 v0, 0x3

    iget-object v1, p0, Ljbk;->dYG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 32835
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 32836
    return-void
.end method

.method public final beW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32746
    iget-object v0, p0, Ljbk;->dYE:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 32840
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 32841
    iget v1, p0, Ljbk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 32842
    const/4 v1, 0x1

    iget-object v2, p0, Ljbk;->dYE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32845
    :cond_0
    iget v1, p0, Ljbk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 32846
    const/4 v1, 0x2

    iget-object v2, p0, Ljbk;->dYF:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32849
    :cond_1
    iget v1, p0, Ljbk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 32850
    const/4 v1, 0x3

    iget-object v2, p0, Ljbk;->dYG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32853
    :cond_2
    return v0
.end method

.method public final sq(Ljava/lang/String;)Ljbk;
    .locals 1

    .prologue
    .line 32749
    if-nez p1, :cond_0

    .line 32750
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32752
    :cond_0
    iput-object p1, p0, Ljbk;->dYE:Ljava/lang/String;

    .line 32753
    iget v0, p0, Ljbk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbk;->aez:I

    .line 32754
    return-object p0
.end method
