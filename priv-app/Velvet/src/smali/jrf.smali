.class public final Ljrf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eAk:[Ljrf;


# instance fields
.field private aez:I

.field private agv:I

.field public eAl:Ljrg;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 391
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 392
    iput v0, p0, Ljrf;->aez:I

    iput v0, p0, Ljrf;->agv:I

    iput-object v1, p0, Ljrf;->eAl:Ljrg;

    iput-object v1, p0, Ljrf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrf;->eCz:I

    .line 393
    return-void
.end method

.method public static btf()[Ljrf;
    .locals 2

    .prologue
    .line 356
    sget-object v0, Ljrf;->eAk:[Ljrf;

    if-nez v0, :cond_1

    .line 357
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 359
    :try_start_0
    sget-object v0, Ljrf;->eAk:[Ljrf;

    if-nez v0, :cond_0

    .line 360
    const/4 v0, 0x0

    new-array v0, v0, [Ljrf;

    sput-object v0, Ljrf;->eAk:[Ljrf;

    .line 362
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    :cond_1
    sget-object v0, Ljrf;->eAk:[Ljrf;

    return-object v0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 345
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljrf;->agv:I

    iget v0, p0, Ljrf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrf;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljrf;->eAl:Ljrg;

    if-nez v0, :cond_1

    new-instance v0, Ljrg;

    invoke-direct {v0}, Ljrg;-><init>()V

    iput-object v0, p0, Ljrf;->eAl:Ljrg;

    :cond_1
    iget-object v0, p0, Ljrf;->eAl:Ljrg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 407
    iget v0, p0, Ljrf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 408
    const/4 v0, 0x1

    iget v1, p0, Ljrf;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 410
    :cond_0
    iget-object v0, p0, Ljrf;->eAl:Ljrg;

    if-eqz v0, :cond_1

    .line 411
    const/4 v0, 0x2

    iget-object v1, p0, Ljrf;->eAl:Ljrg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 413
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 414
    return-void
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Ljrf;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 418
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 419
    iget v1, p0, Ljrf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 420
    const/4 v1, 0x1

    iget v2, p0, Ljrf;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 423
    :cond_0
    iget-object v1, p0, Ljrf;->eAl:Ljrg;

    if-eqz v1, :cond_1

    .line 424
    const/4 v1, 0x2

    iget-object v2, p0, Ljrf;->eAl:Ljrg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 427
    :cond_1
    return v0
.end method

.method public final rK(I)Ljrf;
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x1

    iput v0, p0, Ljrf;->agv:I

    .line 376
    iget v0, p0, Ljrf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrf;->aez:I

    .line 377
    return-object p0
.end method
