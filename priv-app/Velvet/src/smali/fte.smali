.class public final Lfte;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 30
    return-void
.end method

.method private a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 167
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-static {p1, p2, p4}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 168
    return-void

    :cond_0
    move-object p4, p3

    .line 167
    goto :goto_0
.end method


# virtual methods
.method protected final aCZ()V
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/16 v10, 0x11

    const/4 v3, 0x0

    .line 39
    iget-object v5, p0, Lfro;->mView:Landroid/view/View;

    .line 40
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 41
    iget-object v6, v1, Lanh;->ahd:Lanq;

    .line 43
    invoke-virtual {v6}, Lanq;->pb()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const v1, 0x7f1101a9

    invoke-virtual {v6}, Lanq;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v1, v2}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 47
    :cond_0
    invoke-virtual {v6}, Lanq;->om()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48
    const v1, 0x7f110080

    invoke-virtual {v6}, Lanq;->ol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v1, v2}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 52
    :cond_1
    const/4 v1, 0x4

    .line 53
    invoke-virtual {v6}, Lanq;->pA()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 54
    invoke-virtual {v6}, Lanq;->pD()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {v6}, Lanq;->getStatus()I

    move-result v2

    .line 56
    packed-switch v2, :pswitch_data_0

    .line 73
    const v0, 0x7f0b00ba

    move-object v13, v1

    move v1, v0

    move-object v0, v13

    .line 77
    :goto_0
    invoke-virtual {v6}, Lanq;->pC()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 78
    const-string v4, " \u00b7 "

    new-array v7, v12, [Ljava/lang/CharSequence;

    invoke-virtual {v6}, Lanq;->pB()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    aput-object v0, v7, v11

    invoke-static {v4, v7}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 81
    const v0, 0x7f11007f

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;

    .line 83
    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setBackgroundColor(I)V

    .line 84
    invoke-virtual {v0, v4}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setVisibility(I)V

    :cond_2
    move v13, v2

    move v2, v1

    move v1, v13

    .line 89
    :goto_1
    invoke-virtual {v6}, Lanq;->pF()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v6}, Lanq;->getProgress()F

    move-result v0

    .line 91
    :goto_2
    const/4 v4, 0x3

    if-eq v1, v4, :cond_8

    .line 92
    const v4, 0x7f110310

    invoke-virtual {v6}, Lanq;->pE()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v4, v7}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    move v4, v0

    .line 96
    :goto_3
    const v0, 0x7f1100b0

    invoke-virtual {v6}, Lanq;->oo()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v0, v7}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 97
    const v0, 0x7f11030e

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;

    .line 99
    invoke-virtual {v0, v4}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->N(F)V

    .line 101
    iget-object v4, p0, Lfte;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0b00af

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 103
    if-eqz v2, :cond_9

    .line 104
    iget-object v7, p0, Lfte;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 107
    :goto_4
    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->jo(I)V

    .line 109
    iget-object v0, v6, Lanq;->aij:Lanr;

    if-eqz v0, :cond_4

    .line 110
    iget-object v0, v6, Lanq;->aij:Lanr;

    .line 111
    const v1, 0x7f1101db

    invoke-virtual {v0}, Lanr;->pG()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v1, v7}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 112
    invoke-virtual {v0}, Lanr;->getName()Ljava/lang/String;

    move-result-object v1

    .line 114
    iget-object v7, p0, Lfte;->mContext:Landroid/content/Context;

    const v8, 0x7f0a0210

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 115
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 116
    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v9, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v8, v9, v3, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 118
    const-string v7, " \u00b7 "

    new-array v9, v12, [Ljava/lang/CharSequence;

    aput-object v8, v9, v3

    aput-object v1, v9, v11

    invoke-static {v7, v9}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 120
    const v7, 0x7f110312

    invoke-static {v5, v7, v1}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 122
    invoke-virtual {v0}, Lanr;->pH()Ljava/lang/String;

    move-result-object v1

    .line 123
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 124
    if-eqz v2, :cond_3

    .line 125
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v7, v8, v3, v1, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 128
    :cond_3
    const v1, 0x7f110314

    invoke-static {v5, v1, v7}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 130
    const v1, 0x7f110313

    invoke-virtual {v0}, Lanr;->pl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v1, v7}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 131
    const v1, 0x7f110315

    invoke-virtual {v0}, Lanr;->pI()Ljava/lang/String;

    move-result-object v7

    const-string v8, " - "

    invoke-direct {p0, v5, v1, v7, v8}, Lfte;->a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 133
    const v1, 0x7f110316

    invoke-virtual {v0}, Lanr;->pJ()Ljava/lang/String;

    move-result-object v7

    const-string v8, " - "

    invoke-direct {p0, v5, v1, v7, v8}, Lfte;->a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 135
    const v1, 0x7f110317

    invoke-virtual {v0}, Lanr;->pK()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v1, v0}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 138
    :cond_4
    iget-object v0, v6, Lanq;->aik:Lanr;

    if-eqz v0, :cond_6

    .line 139
    iget-object v0, v6, Lanq;->aik:Lanr;

    .line 140
    const v1, 0x7f1101df

    invoke-virtual {v0}, Lanr;->pG()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v1, v6}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 141
    invoke-virtual {v0}, Lanr;->getName()Ljava/lang/String;

    move-result-object v1

    .line 143
    iget-object v6, p0, Lfte;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0212

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 144
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 145
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v7, v8, v3, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 147
    const-string v4, " \u00b7 "

    new-array v6, v12, [Ljava/lang/CharSequence;

    aput-object v7, v6, v3

    aput-object v1, v6, v11

    invoke-static {v4, v6}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 148
    const v4, 0x7f110318

    invoke-static {v5, v4, v1}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 150
    invoke-virtual {v0}, Lanr;->pH()Ljava/lang/String;

    move-result-object v1

    .line 151
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 152
    if-eqz v2, :cond_5

    .line 153
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v4, v6, v3, v1, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 156
    :cond_5
    const v1, 0x7f11031a

    invoke-static {v5, v1, v4}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 158
    const v1, 0x7f110319

    invoke-virtual {v0}, Lanr;->pl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v1, v2}, Lfte;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 159
    const v1, 0x7f11031b

    invoke-virtual {v0}, Lanr;->pI()Ljava/lang/String;

    move-result-object v2

    const-string v3, " - "

    invoke-direct {p0, v5, v1, v2, v3}, Lfte;->a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 161
    const v1, 0x7f11031c

    invoke-virtual {v0}, Lanr;->pJ()Ljava/lang/String;

    move-result-object v0

    const-string v2, " - "

    invoke-direct {p0, v5, v1, v0, v2}, Lfte;->a(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_6
    return-void

    .line 58
    :pswitch_0
    const v1, 0x7f0b00b9

    .line 60
    goto/16 :goto_0

    .line 62
    :pswitch_1
    const v0, 0x7f0b00b7

    move-object v13, v1

    move v1, v0

    move-object v0, v13

    .line 63
    goto/16 :goto_0

    .line 65
    :pswitch_2
    const v1, 0x7f0b00ba

    .line 67
    goto/16 :goto_0

    .line 69
    :pswitch_3
    const v1, 0x7f0b00b7

    .line 71
    goto/16 :goto_0

    .line 89
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 94
    :cond_8
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_3

    :cond_9
    move v2, v3

    goto/16 :goto_4

    :cond_a
    move v2, v3

    goto/16 :goto_1

    .line 56
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 34
    const v0, 0x7f04011c

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
