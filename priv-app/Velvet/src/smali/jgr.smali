.class public final Ljgr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ekR:Ljava/lang/String;

.field private ekS:D

.field private ekT:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51135
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 51136
    iput v2, p0, Ljgr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljgr;->ekR:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljgr;->ekS:D

    iput v2, p0, Ljgr;->ekT:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljgr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgr;->eCz:I

    .line 51137
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 51056
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgr;->ekR:Ljava/lang/String;

    iget v0, p0, Ljgr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljgr;->ekS:D

    iget v0, p0, Ljgr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgr;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljgr;->ekT:I

    iget v0, p0, Ljgr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgr;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 51152
    iget v0, p0, Ljgr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 51153
    const/4 v0, 0x1

    iget-object v1, p0, Ljgr;->ekR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 51155
    :cond_0
    iget v0, p0, Ljgr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 51156
    const/4 v0, 0x2

    iget-wide v2, p0, Ljgr;->ekS:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 51158
    :cond_1
    iget v0, p0, Ljgr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 51159
    const/4 v0, 0x3

    iget v1, p0, Ljgr;->ekT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 51161
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 51162
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 51166
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 51167
    iget v1, p0, Ljgr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 51168
    const/4 v1, 0x1

    iget-object v2, p0, Ljgr;->ekR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51171
    :cond_0
    iget v1, p0, Ljgr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 51172
    const/4 v1, 0x2

    iget-wide v2, p0, Ljgr;->ekS:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 51175
    :cond_1
    iget v1, p0, Ljgr;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 51176
    const/4 v1, 0x3

    iget v2, p0, Ljgr;->ekT:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 51179
    :cond_2
    return v0
.end method
