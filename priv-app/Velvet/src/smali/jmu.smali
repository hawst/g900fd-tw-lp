.class public final Ljmu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiC:Ljava/lang/String;

.field private euy:I

.field private euz:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9848
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 9849
    iput v1, p0, Ljmu;->aez:I

    const/16 v0, 0x36

    iput v0, p0, Ljmu;->euy:I

    iput v1, p0, Ljmu;->euz:I

    const-string v0, ""

    iput-object v0, p0, Ljmu;->aiC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljmu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmu;->eCz:I

    .line 9850
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9769
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmu;->euy:I

    iget v0, p0, Ljmu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmu;->euz:I

    iget v0, p0, Ljmu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmu;->aiC:Ljava/lang/String;

    iget v0, p0, Ljmu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmu;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 9865
    iget v0, p0, Ljmu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9866
    const/4 v0, 0x1

    iget v1, p0, Ljmu;->euy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 9868
    :cond_0
    iget v0, p0, Ljmu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9869
    const/4 v0, 0x2

    iget v1, p0, Ljmu;->euz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 9871
    :cond_1
    iget v0, p0, Ljmu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 9872
    const/4 v0, 0x3

    iget-object v1, p0, Ljmu;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9874
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9875
    return-void
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9829
    iget-object v0, p0, Ljmu;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 9879
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9880
    iget v1, p0, Ljmu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9881
    const/4 v1, 0x1

    iget v2, p0, Ljmu;->euy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9884
    :cond_0
    iget v1, p0, Ljmu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9885
    const/4 v1, 0x2

    iget v2, p0, Ljmu;->euz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9888
    :cond_1
    iget v1, p0, Ljmu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9889
    const/4 v1, 0x3

    iget-object v2, p0, Ljmu;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9892
    :cond_2
    return v0
.end method

.method public final xj(Ljava/lang/String;)Ljmu;
    .locals 1

    .prologue
    .line 9832
    if-nez p1, :cond_0

    .line 9833
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9835
    :cond_0
    iput-object p1, p0, Ljmu;->aiC:Ljava/lang/String;

    .line 9836
    iget v0, p0, Ljmu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmu;->aez:I

    .line 9837
    return-object p0
.end method
