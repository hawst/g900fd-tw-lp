.class public Lahw;
.super Laht;
.source "PG"


# instance fields
.field private YA:Z

.field private YB:Z

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Laht;-><init>()V

    .line 52
    const-string v0, "com.android.launcher3.compat.PackageInstallerCompatV16.queue"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lahw;->mPrefs:Landroid/content/SharedPreferences;

    .line 53
    return-void
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)Lahv;
    .locals 4

    .prologue
    .line 145
    new-instance v1, Lahv;

    invoke-direct {v1, p0}, Lahv;-><init>(Ljava/lang/String;)V

    .line 147
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-direct {v0, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 148
    const-string v2, "state"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lahv;->state:I

    .line 149
    const-string v2, "progress"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lahv;->progress:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-object v1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    const-string v2, "PackageInstallerCompatV16"

    const-string v3, "failed to deserialize app state update"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private lM()V
    .locals 6

    .prologue
    .line 82
    sget-object v1, Lyu;->Mj:Lyu;

    .line 83
    if-nez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 90
    iget-object v0, p0, Lahw;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91
    iget-object v4, p0, Lahw;->mPrefs:Landroid/content/SharedPreferences;

    const/4 v5, 0x0

    invoke-interface {v4, v0, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 93
    invoke-static {v0, v4}, Lahw;->c(Ljava/lang/String;Ljava/lang/String;)Lahv;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 96
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Lyu;->p(Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public final lK()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 173
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0
.end method

.method public final lL()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahw;->YB:Z

    .line 72
    iget-boolean v0, p0, Lahw;->YA:Z

    if-nez v0, :cond_0

    .line 73
    invoke-direct {p0}, Lahw;->lM()V

    .line 75
    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lahw;->YA:Z

    .line 59
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lahw;->YA:Z

    .line 64
    iget-boolean v0, p0, Lahw;->YB:Z

    if-eqz v0, :cond_0

    .line 65
    invoke-direct {p0}, Lahw;->lM()V

    .line 67
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method
