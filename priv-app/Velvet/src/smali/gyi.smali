.class public abstract Lgyi;
.super Lgxi;
.source "PG"

# interfaces
.implements Lfnc;
.implements Lfnl;


# instance fields
.field private Kl:Z

.field private Oq:I

.field private bFk:Lfmq;

.field private cZr:Z

.field private cZs:Landroid/view/View;

.field cvw:Lflr;

.field private final mCardContainer:Lewd;

.field private final mNowOptInSettings:Lcin;

.field private final mNowRemoteClient:Lfml;

.field private final mRefreshManager:Lfmv;

.field private final mRunner:Lerp;

.field private final mUndoDismissManager:Lfnn;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lerp;Lcom/google/android/velvet/ui/MainContentView;Lfmv;Lcin;Lewd;Lfml;Lfnn;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-direct {p0, p1, p3}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 89
    iput v0, p0, Lgyi;->Oq:I

    .line 95
    iput-boolean v0, p0, Lgyi;->cZr:Z

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgyi;->Kl:Z

    .line 117
    iput-object p2, p0, Lgyi;->mRunner:Lerp;

    .line 118
    iput-object p4, p0, Lgyi;->mRefreshManager:Lfmv;

    .line 119
    iput-object p5, p0, Lgyi;->mNowOptInSettings:Lcin;

    .line 120
    iput-object p6, p0, Lgyi;->mCardContainer:Lewd;

    .line 121
    iput-object p7, p0, Lgyi;->mNowRemoteClient:Lfml;

    .line 122
    iput-object p8, p0, Lgyi;->mUndoDismissManager:Lfnn;

    .line 123
    return-void
.end method

.method static synthetic a(Lgyi;)Lfnn;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgyi;->mUndoDismissManager:Lfnn;

    return-object v0
.end method

.method private aLF()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 397
    iget v0, p0, Lgyi;->Oq:I

    if-ne v0, v1, :cond_1

    .line 401
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBg()V

    .line 406
    :goto_0
    iget v0, p0, Lgyi;->Oq:I

    if-ne v0, v1, :cond_0

    .line 409
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBj()V

    .line 411
    :cond_0
    return-void

    .line 403
    :cond_1
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBi()V

    goto :goto_0
.end method

.method private aLG()V
    .locals 2

    .prologue
    .line 593
    new-instance v0, Lgyo;

    const-string v1, "removePredictiveCards"

    invoke-direct {v0, p0, v1}, Lgyo;-><init>(Lgyi;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lgyi;->a(Lesj;)V

    .line 606
    return-void
.end method


# virtual methods
.method protected X(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 129
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    .line 131
    new-instance v3, Lgyj;

    invoke-direct {v3, p0, v0}, Lgyj;-><init>(Lgyi;Lfmv;)V

    .line 138
    new-instance v8, Lgyk;

    invoke-direct {v8, p0}, Lgyk;-><init>(Lgyi;)V

    .line 149
    new-instance v0, Lflr;

    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lgyi;->mRunner:Lerp;

    invoke-virtual {p0}, Lgyi;->aLt()Landroid/view/ViewGroup;

    move-result-object v4

    check-cast v4, Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v5, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v5}, Lgyz;->wj()Lekf;

    move-result-object v5

    iget-object v6, p0, Lgyi;->mCardContainer:Lewd;

    new-instance v7, Lgbr;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v10

    iget-object v10, v10, Lgql;->mClock:Lemp;

    invoke-direct {v7, v10}, Lgbr;-><init>(Lemp;)V

    invoke-direct/range {v0 .. v8}, Lflr;-><init>(Landroid/app/Activity;Lerp;Lflv;Lcom/google/android/shared/ui/SuggestionGridLayout;Lekf;Lfmt;Lgbr;Lflb;)V

    iput-object v0, p0, Lgyi;->cvw:Lflr;

    .line 162
    if-eqz p1, :cond_0

    .line 165
    const-string v0, "predictive_mode"

    invoke-virtual {p1, v0, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 167
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lgyi;->cZr:Z

    .line 168
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->V(Landroid/os/Bundle;)V

    .line 173
    :cond_0
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    new-instance v1, Lgyl;

    invoke-direct {v1, p0}, Lgyl;-><init>(Lgyi;)V

    invoke-virtual {v0, v1}, Lgyz;->d(Landroid/view/View$OnTouchListener;)V

    .line 180
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->reset()V

    .line 181
    return-void

    :cond_1
    move v0, v9

    .line 167
    goto :goto_0
.end method

.method public final Y(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 188
    const-string v0, "predictive_mode"

    iget v1, p0, Lgyi;->Oq:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 189
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->U(Landroid/os/Bundle;)V

    .line 190
    return-void
.end method

.method public a(JLfzw;Lemp;)V
    .locals 3

    .prologue
    .line 711
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-nez v0, :cond_1

    .line 726
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    new-instance v1, Lgyr;

    invoke-direct {v1, p0}, Lgyr;-><init>(Lgyi;)V

    invoke-virtual {v0, v1}, Lflr;->b(Lifw;)Lfkd;

    move-result-object v0

    .line 722
    if-nez v0, :cond_0

    .line 723
    new-instance v0, Lfkq;

    invoke-direct {v0, p3, p4, p1, p2}, Lfkq;-><init>(Lfzw;Lemp;J)V

    .line 724
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgyi;->a(Lfkd;I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;ZLjava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 665
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    if-nez p3, :cond_1

    .line 668
    const/4 v0, 0x0

    .line 677
    :goto_0
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1, p1, p2, v0}, Lgyz;->a(Landroid/graphics/drawable/Drawable;ZLandroid/view/View$OnClickListener;)V

    .line 679
    :cond_0
    return-void

    .line 670
    :cond_1
    new-instance v0, Lgyq;

    invoke-direct {v0, p0, p3}, Lgyq;-><init>(Lgyi;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lejw;)V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->a(Lejw;)V

    .line 494
    return-void
.end method

.method public final a(Lfkd;I)V
    .locals 1

    .prologue
    .line 481
    new-instance v0, Lgym;

    invoke-direct {v0, p0, p1, p2}, Lgym;-><init>(Lgyi;Lfkd;I)V

    invoke-virtual {p0, v0}, Lgyi;->a(Lesj;)V

    .line 489
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 501
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 507
    :goto_0
    return-void

    .line 503
    :cond_0
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move v7, v4

    move v8, v3

    invoke-virtual/range {v0 .. v8}, Lflr;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;Z)V
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 447
    iget v0, p0, Lgyi;->Oq:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 468
    iget-boolean v0, p0, Lgyi;->cZr:Z

    if-nez v0, :cond_2

    if-eqz p4, :cond_4

    :cond_2
    move v5, v3

    :goto_1
    iget-boolean v0, p0, Lgyi;->cZr:Z

    if-nez v0, :cond_5

    move v6, v3

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, Lgyi;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZLcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    .line 476
    :cond_3
    iput-boolean v7, p0, Lgyi;->cZr:Z

    goto :goto_0

    :cond_4
    move v5, v7

    .line 468
    goto :goto_1

    :cond_5
    move v6, v7

    goto :goto_2
.end method

.method protected a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZLcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V
    .locals 9
    .param p4    # Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 572
    iget-object v1, p0, Lgyi;->cZs:Landroid/view/View;

    if-nez v1, :cond_1

    move-object v5, v0

    .line 574
    :goto_0
    iput-object v0, p0, Lgyi;->cZs:Landroid/view/View;

    .line 576
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move-object v6, p4

    move v7, p5

    move v8, p6

    invoke-virtual/range {v0 .. v8}, Lflr;->b(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)Lesj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgyi;->a(Lesj;)V

    .line 587
    :cond_0
    return-void

    .line 572
    :cond_1
    iget-object v1, p0, Lgyi;->cZs:Landroid/view/View;

    invoke-static {v1}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/sidekick/shared/client/NowSearchOptions;)Z
    .locals 5
    .param p2    # Lcom/google/android/sidekick/shared/client/NowSearchOptions;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 682
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 683
    invoke-virtual {p0}, Lgyi;->Nf()Ldda;

    move-result-object v2

    .line 684
    if-eqz v2, :cond_3

    .line 685
    invoke-virtual {v2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    .line 686
    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v4

    if-nez p2, :cond_0

    move-object v2, v1

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-nez p2, :cond_2

    :goto_2
    invoke-virtual {v4, p1, v2, v1, v0}, Lcom/google/android/shared/search/Query;->a(Ljava/lang/CharSequence;Landroid/location/Location;Ljava/lang/String;Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 690
    const/4 v0, 0x1

    .line 697
    :goto_3
    return v0

    .line 686
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->aqW()Landroid/location/Location;

    move-result-object v2

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->aqX()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->aqr()Z

    move-result v0

    goto :goto_2

    .line 692
    :cond_3
    const-string v1, "Velvet.TgPresenter"

    const-string v2, "Tried to start predictive web search but event bus was null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 695
    :cond_4
    const-string v1, "Velvet.TgPresenter"

    const-string v2, "Tried to start predictive web search but TgPresenter was not attached"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public final aAT()V
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    invoke-direct {p0}, Lgyi;->aLG()V

    .line 438
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyi;->cZr:Z

    .line 439
    invoke-virtual {p0}, Lgyi;->aAX()V

    .line 440
    return-void
.end method

.method public final aAV()Z
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aMp()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aAW()V
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {p0}, Lgyi;->aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->start()V

    .line 307
    :cond_0
    return-void
.end method

.method public final aAX()V
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {p0}, Lgyi;->aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->stop()V

    .line 317
    :cond_0
    return-void
.end method

.method public final aAY()V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method protected aKF()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 231
    iget-object v0, p0, Lgyi;->cZs:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lgyi;->cZs:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgyi;->b([Landroid/view/View;)V

    .line 233
    iput-object v3, p0, Lgyi;->cZs:Landroid/view/View;

    .line 235
    :cond_0
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    if-eqz v0, :cond_1

    .line 236
    iput-object v3, p0, Lgyi;->cvw:Lflr;

    .line 238
    :cond_1
    invoke-virtual {p0}, Lgyi;->aAX()V

    .line 239
    return-void
.end method

.method public ak(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->ak(Landroid/view/View;)V

    .line 324
    :cond_0
    return-void
.end method

.method protected bD(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 609
    const v0, 0x7f11000a

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lizj;Ljava/util/Collection;)V
    .locals 1
    .param p2    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 528
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    :goto_0
    return-void

    .line 530
    :cond_0
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    invoke-virtual {v0, p1, p2}, Lflr;->c(Lizj;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final cN()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 625
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lgyi;->Oq:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    :cond_0
    move v0, v1

    .line 647
    :cond_1
    :goto_0
    return v0

    .line 628
    :cond_2
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    iget-object v0, v0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v0}, Lfkz;->aAH()Z

    move-result v0

    .line 631
    if-eqz v0, :cond_1

    .line 632
    new-instance v2, Lgyp;

    invoke-direct {v2, p0}, Lgyp;-><init>(Lgyi;)V

    invoke-virtual {p0, v2}, Lgyi;->a(Lesj;)V

    .line 645
    invoke-virtual {p0, v1}, Lgyi;->fZ(Z)V

    goto :goto_0
.end method

.method public final d(Lfkd;)V
    .locals 1

    .prologue
    .line 652
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 655
    :goto_0
    return-void

    .line 654
    :cond_0
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    invoke-virtual {v0, p1}, Lflr;->d(Lfkd;)V

    goto :goto_0
.end method

.method public final ex(I)V
    .locals 1

    .prologue
    .line 416
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {p0}, Lgyi;->aAX()V

    .line 418
    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/MainContentView;->cZ(I)V

    .line 420
    :cond_0
    return-void
.end method

.method public final i(Lizj;Lizj;)V
    .locals 1

    .prologue
    .line 513
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 523
    :goto_0
    return-void

    .line 515
    :cond_0
    new-instance v0, Lgyn;

    invoke-direct {v0, p0, p1, p2}, Lgyn;-><init>(Lgyi;Lizj;Lizj;)V

    invoke-virtual {p0, v0}, Lgyi;->a(Lesj;)V

    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    return v0
.end method

.method public final kH(I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 329
    iget-object v3, p0, Lgyi;->mCardContainer:Lewd;

    invoke-virtual {p0}, Lgyi;->wk()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lewd;->setVisibility(I)V

    .line 334
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aLP()Z

    move-result v0

    if-nez v0, :cond_2

    .line 336
    iput v2, p0, Lgyi;->Oq:I

    .line 337
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBi()V

    .line 338
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    invoke-direct {p0}, Lgyi;->aLG()V

    .line 340
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgyz;->a(Lgxi;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyi;->cZs:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->Sv()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyi;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KC()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->aJU()Lgpu;

    invoke-virtual {p0}, Lgyi;->aLt()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {p0, v0}, Lgpu;->g(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/velvet/ui/GetGoogleNowView;

    move-result-object v0

    iput-object v0, p0, Lgyi;->cZs:Landroid/view/View;

    .line 349
    new-array v0, v2, [Landroid/view/View;

    iget-object v2, p0, Lgyi;->cZs:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgyi;->a([Landroid/view/View;)V

    .line 393
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 329
    goto :goto_0

    .line 356
    :cond_2
    iget-object v0, p0, Lgyi;->cZs:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 357
    new-array v0, v2, [Landroid/view/View;

    iget-object v2, p0, Lgyi;->cZs:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgyi;->b([Landroid/view/View;)V

    .line 358
    const/4 v0, 0x0

    iput-object v0, p0, Lgyi;->cZs:Landroid/view/View;

    .line 361
    :cond_3
    iget v0, p0, Lgyi;->Oq:I

    if-eq p1, v0, :cond_0

    .line 362
    iget v0, p0, Lgyi;->Oq:I

    .line 363
    iput p1, p0, Lgyi;->Oq:I

    .line 366
    if-ne p1, v4, :cond_4

    .line 367
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 368
    invoke-virtual {p0}, Lgyi;->aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    move-result-object v1

    .line 369
    if-eqz v1, :cond_4

    .line 370
    iget-object v2, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v2, v1}, Lfmv;->a(Lfrg;)V

    .line 375
    :cond_4
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    if-ne v0, v4, :cond_5

    .line 379
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    iget-object v0, v0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v0}, Lfkz;->aAG()Z

    .line 380
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->reset()V

    .line 381
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBh()V

    .line 385
    :cond_5
    invoke-direct {p0}, Lgyi;->aLG()V

    .line 388
    iget-boolean v0, p0, Lgyi;->Kl:Z

    if-nez v0, :cond_0

    .line 389
    invoke-direct {p0}, Lgyi;->aLF()V

    goto :goto_1
.end method

.method public final kI(I)V
    .locals 1

    .prologue
    .line 614
    const/4 v0, 0x1

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 615
    const/4 v0, 0x2

    iput v0, p0, Lgyi;->Oq:I

    .line 616
    return-void
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgyi;->Kl:Z

    .line 245
    invoke-super {p0}, Lgxi;->onPause()V

    .line 247
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBq()V

    .line 248
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBo()V

    .line 249
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBh()V

    .line 250
    iget-object v0, p0, Lgyi;->mCardContainer:Lewd;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lewd;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lgyi;->bFk:Lfmq;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lgyi;->mUndoDismissManager:Lfnn;

    invoke-virtual {v0}, Lfnn;->aBm()V

    .line 256
    iget-object v0, p0, Lgyi;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBc()V

    .line 257
    iget-object v0, p0, Lgyi;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 258
    const/4 v0, 0x0

    iput-object v0, p0, Lgyi;->bFk:Lfmq;

    .line 260
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 195
    invoke-super {p0}, Lgxi;->onResume()V

    .line 197
    iget-object v2, p0, Lgyi;->bFk:Lfmq;

    if-nez v2, :cond_0

    .line 198
    iget-object v2, p0, Lgyi;->mNowRemoteClient:Lfml;

    const-string v3, "Velvet.TgPresenter"

    invoke-virtual {v2, v3}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v2

    iput-object v2, p0, Lgyi;->bFk:Lfmq;

    .line 199
    iget-object v2, p0, Lgyi;->bFk:Lfmq;

    invoke-virtual {v2}, Lfmq;->aBf()Z

    .line 200
    iget-object v2, p0, Lgyi;->mNowRemoteClient:Lfml;

    invoke-virtual {v2}, Lfml;->aBd()V

    .line 203
    :cond_0
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 204
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v2

    invoke-virtual {v2}, Lgyz;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 205
    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 206
    iget-object v3, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v3, v2, v0}, Lfmv;->c(Landroid/os/Bundle;Z)V

    .line 209
    :cond_1
    invoke-direct {p0}, Lgyi;->aLF()V

    .line 211
    invoke-virtual {p0}, Lgyi;->wk()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 212
    iget-object v2, p0, Lgyi;->mRefreshManager:Lfmv;

    iget-object v2, v2, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v2}, Lfns;->aBp()V

    .line 214
    :cond_2
    iget-object v2, p0, Lgyi;->mRefreshManager:Lfmv;

    iget-object v2, v2, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v2}, Lfns;->aBn()V

    .line 215
    iget-object v2, p0, Lgyi;->mCardContainer:Lewd;

    invoke-virtual {p0}, Lgyi;->wk()Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {v2, v0}, Lewd;->setVisibility(I)V

    .line 219
    iget v0, p0, Lgyi;->Oq:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 220
    invoke-virtual {p0}, Lgyi;->aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_4

    .line 222
    iget-object v2, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v2, v0}, Lfmv;->a(Lfrg;)V

    .line 226
    :cond_4
    iput-boolean v1, p0, Lgyi;->Kl:Z

    .line 227
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 265
    invoke-super {p0}, Lgxi;->onStop()V

    .line 269
    iget-object v0, p0, Lgyi;->mRefreshManager:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBi()V

    .line 270
    return-void
.end method

.method public final w(Lizj;)V
    .locals 2

    .prologue
    .line 702
    iget v0, p0, Lgyi;->Oq:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lgyi;->cvw:Lflr;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lflr;->i(Lizj;I)V

    .line 705
    :cond_0
    return-void
.end method

.method public wi()Lcom/google/android/shared/ui/SuggestionGridLayout;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lgyi;->aLt()Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SuggestionGridLayout;

    return-object v0
.end method

.method public final wk()Z
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lgyi;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 286
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lgyi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->wk()Z

    move-result v0

    goto :goto_0
.end method
