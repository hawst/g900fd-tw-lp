.class final Lbei;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic axK:Lbej;

.field private synthetic axL:Lbeg;


# direct methods
.method constructor <init>(Lbeg;Lbej;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lbei;->axL:Lbeg;

    iput-object p2, p0, Lbei;->axK:Lbej;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 238
    iget-object v2, p0, Lbei;->axL:Lbeg;

    iget-object v2, v2, Lbeg;->axJ:Lbbt;

    iget-object v3, p0, Lbei;->axK:Lbej;

    invoke-virtual {v3}, Lbej;->xF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbbt;->eI(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v1, ".AppDataSearchProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t fetch status for corpus "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lbei;->axK:Lbej;

    invoke-virtual {v3}, Lbej;->xF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->wR()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lbei;->axL:Lbeg;

    invoke-virtual {v3}, Lbeg;->xD()Lbdx;

    move-result-object v3

    iget-object v4, p0, Lbei;->axK:Lbej;

    invoke-interface {v3, v4}, Lbdx;->b(Lbej;)J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->wS()J

    move-result-wide v2

    cmp-long v6, v4, v8

    if-nez v6, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    cmp-long v6, v4, v2

    if-gez v6, :cond_3

    const-string v1, ".AppDataSearchProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Local highest seqno="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " less than lastIndexedSeqno="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    cmp-long v0, v4, v2

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    cmp-long v0, v2, v8

    if-nez v0, :cond_5

    const/4 v0, 0x2

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method
