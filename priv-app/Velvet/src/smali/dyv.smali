.class public final Ldyv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static bRh:Ldyv;


# instance fields
.field private final bRi:Ljava/lang/String;

.field private final bRj:Ljava/lang/String;

.field private final bRk:Ljava/lang/String;

.field private final bRl:Ljava/lang/String;

.field private final bRm:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # Landroid/content/res/Resources;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    if-nez p1, :cond_0

    .line 31
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p1

    .line 33
    :cond_0
    const/4 v0, 0x1

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyv;->bRi:Ljava/lang/String;

    .line 34
    const/16 v0, 0xc

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyv;->bRj:Ljava/lang/String;

    .line 35
    const/4 v0, 0x2

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyv;->bRk:Ljava/lang/String;

    .line 36
    const/4 v0, 0x7

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyv;->bRl:Ljava/lang/String;

    .line 37
    const/4 v0, 0x3

    invoke-static {p1, v0, v1}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyv;->bRm:Ljava/lang/String;

    .line 38
    sput-object p0, Ldyv;->bRh:Ldyv;

    .line 39
    return-void
.end method

.method public static f(Landroid/content/res/Resources;)Ldyv;
    .locals 1
    .param p0    # Landroid/content/res/Resources;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 17
    sget-object v0, Ldyv;->bRh:Ldyv;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Ldyv;

    invoke-direct {v0, p0}, Ldyv;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Ldyv;->bRh:Ldyv;

    .line 20
    :cond_0
    sget-object v0, Ldyv;->bRh:Ldyv;

    return-object v0
.end method


# virtual methods
.method public final a(Ljor;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljor;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 46
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljor;->oP()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljor;->brB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    :cond_0
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    .line 49
    :cond_1
    invoke-virtual {p1}, Ljor;->brB()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    invoke-virtual {p1}, Ljor;->brA()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_2
    invoke-virtual {p1}, Ljor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 62
    iget-object v0, p0, Ldyv;->bRl:Ljava/lang/String;

    goto :goto_0

    .line 54
    :pswitch_0
    iget-object v0, p0, Ldyv;->bRi:Ljava/lang/String;

    goto :goto_0

    .line 56
    :pswitch_1
    iget-object v0, p0, Ldyv;->bRj:Ljava/lang/String;

    goto :goto_0

    .line 58
    :pswitch_2
    iget-object v0, p0, Ldyv;->bRk:Ljava/lang/String;

    goto :goto_0

    .line 60
    :pswitch_3
    iget-object v0, p0, Ldyv;->bRm:Ljava/lang/String;

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final kw(Ljava/lang/String;)Ljor;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    .line 74
    :cond_1
    iget-object v0, p0, Ldyv;->bRi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljor;->rb(I)Ljor;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_2
    iget-object v0, p0, Ldyv;->bRj:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljor;->rb(I)Ljor;

    move-result-object v0

    goto :goto_0

    .line 78
    :cond_3
    iget-object v0, p0, Ldyv;->bRk:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljor;->rb(I)Ljor;

    move-result-object v0

    goto :goto_0

    .line 80
    :cond_4
    iget-object v0, p0, Ldyv;->bRm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 81
    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljor;->rb(I)Ljor;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_5
    new-instance v0, Ljor;

    invoke-direct {v0}, Ljor;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljor;->rb(I)Ljor;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljor;->xQ(Ljava/lang/String;)Ljor;

    move-result-object v0

    goto :goto_0
.end method
