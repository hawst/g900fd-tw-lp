.class public final Lfwp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cCN:Ljava/text/DecimalFormat;

.field private static cCO:Ljava/text/DecimalFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1114
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfwp;->cCN:Ljava/text/DecimalFormat;

    .line 1116
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, ".000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfwp;->cCO:Ljava/text/DecimalFormat;

    return-void
.end method

.method public static a(ILjgn;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1105
    packed-switch p0, :pswitch_data_0

    .line 1109
    iget-object v0, p1, Ljgn;->ekt:Ljgo;

    invoke-static {p0, v0, p2}, Lfwp;->a(ILjgo;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1107
    :pswitch_0
    invoke-virtual {p1}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1105
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method

.method public static a(ILjgo;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1015
    packed-switch p0, :pswitch_data_0

    .line 1099
    :cond_0
    :goto_0
    :pswitch_0
    return-object v0

    .line 1017
    :pswitch_1
    iget-object v1, p1, Ljgo;->ekv:Ljgp;

    if-eqz v1, :cond_0

    .line 1020
    iget-object v0, p1, Ljgo;->ekv:Ljgp;

    .line 1021
    const v1, 0x7f0a0263

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljgp;->blb()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljgp;->bld()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1024
    :pswitch_2
    invoke-virtual {p1}, Ljgo;->bkR()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljgo;->bkQ()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 1027
    sget-object v0, Lfwp;->cCN:Ljava/text/DecimalFormat;

    invoke-virtual {p1}, Ljgo;->bkQ()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1029
    :pswitch_3
    invoke-virtual {p1}, Ljgo;->bkP()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1032
    invoke-virtual {p1}, Ljgo;->bkO()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1034
    :pswitch_4
    invoke-virtual {p1}, Ljgo;->bkL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1037
    invoke-virtual {p1}, Ljgo;->bkK()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1039
    :pswitch_5
    iget-object v1, p1, Ljgo;->eku:Ljgp;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v1}, Ljgp;->ble()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    iget-object v0, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v0}, Ljgp;->bld()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1045
    :pswitch_6
    iget-object v1, p1, Ljgo;->eku:Ljgp;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v1}, Ljgp;->bli()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1049
    iget-object v0, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v0}, Ljgp;->blh()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1051
    :pswitch_7
    invoke-virtual {p1}, Ljgo;->bkZ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1054
    invoke-virtual {p1}, Ljgo;->bkY()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1056
    :pswitch_8
    invoke-virtual {p1}, Ljgo;->bkX()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1059
    invoke-virtual {p1}, Ljgo;->bkW()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1061
    :pswitch_9
    iget-object v1, p1, Ljgo;->eku:Ljgp;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v1}, Ljgp;->bla()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1065
    iget-object v0, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v0}, Ljgp;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1068
    :pswitch_a
    invoke-virtual {p1}, Ljgo;->bkT()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1069
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0a0286

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljgo;->bkS()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1072
    :cond_1
    invoke-virtual {p1}, Ljgo;->bkV()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1073
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0a027f

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljgo;->bkU()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1078
    :pswitch_b
    invoke-virtual {p1}, Ljgo;->bkN()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1081
    invoke-virtual {p1}, Ljgo;->bkM()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1083
    :pswitch_c
    iget-object v1, p1, Ljgo;->eku:Ljgp;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v1}, Ljgp;->blg()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1086
    iget-object v0, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v0}, Ljgp;->blf()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1088
    :pswitch_d
    invoke-virtual {p1}, Ljgo;->bkJ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1091
    sget-object v0, Lfwp;->cCO:Ljava/text/DecimalFormat;

    invoke-virtual {p1}, Ljgo;->bkI()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1093
    :pswitch_e
    iget-object v1, p1, Ljgo;->eku:Ljgp;

    if-eqz v1, :cond_0

    iget-object v1, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v1}, Ljgp;->blc()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1096
    iget-object v0, p1, Ljgo;->eku:Ljgp;

    invoke-virtual {v0}, Ljgp;->blb()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1015
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static b(ILandroid/content/Context;)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v0, 0x7f0a0282

    const/4 v1, -0x1

    .line 1120
    packed-switch p0, :pswitch_data_0

    const-string v0, "QpSportScoreEntryAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized team stat for header: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 1121
    :goto_0
    :pswitch_0
    if-eq v0, v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1120
    :pswitch_1
    const v0, 0x7f0a0288

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a027c

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a027d

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a027e

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a027f

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0a0280

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0a0283

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0a0289

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0a0281

    goto :goto_0

    :pswitch_a
    const v0, 0x7f0a0287

    goto :goto_0

    :pswitch_b
    const v0, 0x7f0a0284

    goto :goto_0

    :pswitch_c
    const v0, 0x7f0a0285

    goto :goto_0

    :pswitch_d
    const v0, 0x7f0a0286

    goto :goto_0

    .line 1121
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
