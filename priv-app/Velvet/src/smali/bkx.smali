.class public final Lbkx;
.super Lbir;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbir;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Lbjm;Lbiv;)V
    .locals 2

    const v0, 0x5d3f18

    iget-object v1, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lbjm;->b(Lbjj;ILjava/lang/String;)V

    return-void
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lbkr;->i(Landroid/os/IBinder;)Lbkq;

    move-result-object v0

    return-object v0
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.icing.LIGHTWEIGHT_INDEX_SERVICE"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.appdatasearch.internal.ILightweightAppDataSearch"

    return-object v0
.end method

.method public final zi()Lbkq;
    .locals 1

    invoke-virtual {p0}, Lbkx;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbkq;

    return-object v0
.end method
