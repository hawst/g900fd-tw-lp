.class public final Lcqn;
.super Lcqf;
.source "PG"


# instance fields
.field private final bgp:Landroid/database/DataSetObserver;

.field private final bgq:Lchk;

.field private bgr:Ljava/lang/String;

.field private bgs:J

.field private bgt:Ljava/lang/String;

.field private bgu:J

.field private bgv:Ljava/lang/String;

.field private final mClock:Lemp;

.field private final mLoginHelper:Lcrh;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lcqe;Lcrh;Lchk;Lemp;Lcke;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcqf;-><init>(Lcqe;)V

    .line 43
    iput-object p2, p0, Lcqn;->mLoginHelper:Lcrh;

    .line 44
    new-instance v0, Lcqo;

    invoke-direct {v0, p0}, Lcqo;-><init>(Lcqn;)V

    iput-object v0, p0, Lcqn;->bgp:Landroid/database/DataSetObserver;

    .line 56
    iget-object v0, p0, Lcqn;->mLoginHelper:Lcrh;

    iget-object v1, p0, Lcqn;->bgp:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 57
    iput-object p3, p0, Lcqn;->bgq:Lchk;

    .line 58
    iput-object p4, p0, Lcqn;->mClock:Lemp;

    .line 59
    iput-object p5, p0, Lcqn;->mSettings:Lcke;

    .line 60
    iget-object v0, p0, Lcqn;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Or()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcqn;->bgr:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcqn;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Os()J

    move-result-wide v0

    iput-wide v0, p0, Lcqn;->bgs:J

    .line 62
    iget-object v0, p0, Lcqn;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ot()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcqn;->bgt:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcqn;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ou()J

    move-result-wide v0

    iput-wide v0, p0, Lcqn;->bgu:J

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcqn;->bgv:Ljava/lang/String;

    .line 65
    return-void
.end method

.method private a(Ldef;Ljava/lang/String;IJ)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 90
    :goto_0
    return v0

    .line 77
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcqn;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 80
    int-to-long v4, p3

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_1
    invoke-interface {p1}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/shared/search/Query;->ay(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 87
    goto :goto_0

    .line 90
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/android/shared/search/Query;Lddw;)Lddw;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 94
    if-nez p2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-object v6

    .line 100
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 101
    iget-object v0, p0, Lcqn;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hm()I

    move-result v7

    .line 102
    iget-object v0, p0, Lcqn;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hn()I

    move-result v3

    .line 104
    iget-object v2, p0, Lcqn;->bgt:Ljava/lang/String;

    iget-wide v4, p0, Lcqn;->bgu:J

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcqn;->a(Ldef;Ljava/lang/String;IJ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    new-instance v6, Lddx;

    invoke-direct {v6, p2}, Lddx;-><init>(Ldef;)V

    .line 107
    iget-object v0, p0, Lcqn;->bgt:Ljava/lang/String;

    new-instance v1, Lded;

    invoke-direct {v1}, Lded;-><init>()V

    iput-object v0, v1, Lded;->bux:Ljava/lang/CharSequence;

    const-string v2, "android.intent.action.WEB_SEARCH"

    iput-object v2, v1, Lded;->buE:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lded;->buJ:Ljava/lang/String;

    const/16 v0, 0x4d

    invoke-virtual {v1, v0}, Lded;->fI(I)Lded;

    move-result-object v0

    iput-boolean v8, v0, Lded;->buN:Z

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {v6, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    .line 119
    :cond_2
    :goto_1
    if-nez v6, :cond_0

    move-object v6, p2

    goto :goto_0

    .line 108
    :cond_3
    iget-object v2, p0, Lcqn;->bgr:Ljava/lang/String;

    iget-wide v4, p0, Lcqn;->bgs:J

    move-object v0, p0

    move-object v1, p2

    move v3, v7

    invoke-direct/range {v0 .. v5}, Lcqn;->a(Ldef;Ljava/lang/String;IJ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 109
    new-instance v0, Lddx;

    invoke-direct {v0, p2}, Lddx;-><init>(Ldef;)V

    .line 110
    iget-object v1, p0, Lcqn;->bgr:Ljava/lang/String;

    invoke-static {v1}, Ldfx;->o(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    invoke-interface {v0, v1}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    :goto_2
    move-object v6, v0

    .line 112
    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcqn;->bgv:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcqn;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Ho()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcqn;->bgr:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/shared/search/Query;->ay(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    new-instance v6, Lddx;

    invoke-direct {v6, p2}, Lddx;-><init>(Ldef;)V

    .line 116
    iget-object v0, p0, Lcqn;->bgv:Ljava/lang/String;

    new-instance v1, Lded;

    invoke-direct {v1}, Lded;-><init>()V

    iput-object v0, v1, Lded;->bux:Ljava/lang/CharSequence;

    const-string v2, "android.intent.action.WEB_SEARCH"

    iput-object v2, v1, Lded;->buE:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lded;->buJ:Ljava/lang/String;

    const/16 v0, 0x4e

    invoke-virtual {v1, v0}, Lded;->fI(I)Lded;

    move-result-object v0

    iput-boolean v8, v0, Lded;->buN:Z

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {v6, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_1

    :cond_5
    move-object v0, v6

    goto :goto_2
.end method

.method public final b(Lcom/google/android/shared/search/Query;Lemy;)V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcqp;

    invoke-direct {v0, p0, p1, p2}, Lcqp;-><init>(Lcqn;Lcom/google/android/shared/search/Query;Lemy;)V

    invoke-super {p0, p1, v0}, Lcqf;->b(Lcom/google/android/shared/search/Query;Lemy;)V

    .line 131
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcqn;->bgr:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqn;->bgr:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/shared/search/Query;->ay(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcqn;->ir(Ljava/lang/String;)V

    .line 139
    :cond_0
    iget-object v0, p0, Lcqn;->bgt:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcqn;->bgt:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/shared/search/Query;->ay(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcqn;->is(Ljava/lang/String;)V

    .line 143
    :cond_1
    invoke-super {p0, p1}, Lcqf;->b(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcqn;->mLoginHelper:Lcrh;

    iget-object v1, p0, Lcqn;->bgp:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 183
    invoke-super {p0}, Lcqf;->close()V

    .line 184
    return-void
.end method

.method public final h(Lcom/google/android/shared/search/Query;)Lddw;
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcqf;->h(Lcom/google/android/shared/search/Query;)Lddw;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcqn;->a(Lcom/google/android/shared/search/Query;Lddw;)Lddw;

    move-result-object v0

    return-object v0
.end method

.method final ir(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 147
    iput-object p1, p0, Lcqn;->bgr:Ljava/lang/String;

    .line 148
    iget-object v0, p0, Lcqn;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcqn;->bgs:J

    .line 149
    iget-object v0, p0, Lcqn;->mSettings:Lcke;

    iget-object v1, p0, Lcqn;->bgr:Ljava/lang/String;

    iget-wide v2, p0, Lcqn;->bgs:J

    invoke-interface {v0, v1, v2, v3}, Lcke;->g(Ljava/lang/String;J)V

    .line 150
    return-void
.end method

.method final is(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 153
    iput-object p1, p0, Lcqn;->bgt:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lcqn;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcqn;->bgu:J

    .line 155
    iget-object v0, p0, Lcqn;->mSettings:Lcke;

    iget-object v1, p0, Lcqn;->bgt:Ljava/lang/String;

    iget-wide v2, p0, Lcqn;->bgu:J

    invoke-interface {v0, v1, v2, v3}, Lcke;->h(Ljava/lang/String;J)V

    .line 156
    return-void
.end method

.method public final u(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcqn;->is(Ljava/lang/String;)V

    .line 162
    :cond_0
    return-void
.end method

.method public final v(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcqn;->bgr:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->afX()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    iget-object v1, p0, Lcqn;->bgr:Ljava/lang/String;

    iput-object v1, p0, Lcqn;->bgv:Ljava/lang/String;

    .line 174
    :goto_0
    invoke-virtual {p0, v0}, Lcqn;->ir(Ljava/lang/String;)V

    .line 176
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcqn;->is(Ljava/lang/String;)V

    .line 178
    :cond_0
    return-void

    .line 172
    :cond_1
    const-string v1, ""

    iput-object v1, p0, Lcqn;->bgv:Ljava/lang/String;

    goto :goto_0
.end method
