.class public final Lcdn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdf;


# instance fields
.field private final aSB:Lcdf;

.field private final aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final aSs:Lcdo;

.field private final mRetryPolicy:Lcdp;


# direct methods
.method public constructor <init>(Lcdf;Lcdp;Lcdo;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcdn;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 43
    iput-object p1, p0, Lcdn;->aSB:Lcdf;

    .line 44
    iput-object p2, p0, Lcdn;->mRetryPolicy:Lcdp;

    .line 45
    iput-object p3, p0, Lcdn;->aSs:Lcdo;

    .line 46
    return-void
.end method


# virtual methods
.method public final b(Leie;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcdn;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcdn;->aSB:Lcdf;

    invoke-interface {v0, p1}, Lcdf;->b(Leie;)V

    goto :goto_0
.end method

.method public final b(Ljww;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcdn;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcdn;->mRetryPolicy:Lcdp;

    invoke-interface {v0, p1}, Lcdp;->a(Ljww;)Leie;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p0, v0}, Lcdn;->c(Leie;)V

    goto :goto_0

    .line 61
    :cond_1
    iget-object v0, p0, Lcdn;->aSB:Lcdf;

    invoke-interface {v0, p1}, Lcdf;->b(Ljww;)V

    goto :goto_0
.end method

.method public final c(Leie;)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcdn;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcdn;->mRetryPolicy:Lcdp;

    invoke-interface {v0, p1}, Lcdp;->a(Leie;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcdn;->aSB:Lcdf;

    new-instance v1, Leih;

    invoke-direct {v1, p1}, Leih;-><init>(Leie;)V

    invoke-interface {v0, v1}, Lcdf;->b(Leie;)V

    .line 84
    iget-object v0, p0, Lcdn;->aSs:Lcdo;

    invoke-virtual {p1}, Leie;->aja()Z

    move-result v1

    invoke-interface {v0, v1}, Lcdo;->cf(Z)V

    goto :goto_0

    .line 86
    :cond_1
    const/16 v0, 0x9

    invoke-static {v0}, Lege;->ht(I)V

    .line 90
    iget-object v0, p0, Lcdn;->aSB:Lcdf;

    invoke-interface {v0, p1}, Lcdf;->c(Leie;)V

    goto :goto_0
.end method

.method public final invalidate()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcdn;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 96
    return-void
.end method
