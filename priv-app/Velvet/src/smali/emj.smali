.class public abstract Lemj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leml;


# instance fields
.field private cfq:Z

.field private cfr:Z

.field private cfs:Ljava/util/List;

.field private final dK:Ljava/lang/Object;

.field private wi:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lemj;->dK:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected final aH(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lemj;->k(Ljava/lang/Object;Z)V

    .line 82
    return-void
.end method

.method public final auB()Z
    .locals 2

    .prologue
    .line 193
    iget-object v1, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 194
    :try_start_0
    iget-boolean v0, p0, Lemj;->cfr:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final declared-synchronized auC()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 201
    :try_start_1
    invoke-virtual {p0}, Lemj;->auB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "getNow() called when haveNow() is false"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 200
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 204
    :cond_0
    :try_start_3
    iget-object v0, p0, Lemj;->wi:Ljava/lang/Object;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final clear()V
    .locals 2

    .prologue
    .line 184
    iget-object v1, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 185
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lemj;->wi:Ljava/lang/Object;

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lemj;->cfr:Z

    .line 187
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract create()V
.end method

.method public final e(Lemy;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 134
    iget-object v1, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    iget-boolean v2, p0, Lemj;->cfr:Z

    .line 136
    iget-object v3, p0, Lemj;->wi:Ljava/lang/Object;

    .line 137
    if-nez v2, :cond_1

    if-eqz p1, :cond_1

    .line 138
    iget-object v4, p0, Lemj;->cfs:Ljava/util/List;

    if-nez v4, :cond_0

    .line 139
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lemj;->cfs:Ljava/util/List;

    .line 141
    :cond_0
    iget-object v4, p0, Lemj;->cfs:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    if-eqz v2, :cond_3

    .line 145
    if-eqz p1, :cond_2

    .line 147
    invoke-interface {p1, v3}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 164
    :cond_2
    :goto_0
    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 150
    :cond_3
    const/4 v1, 0x0

    .line 151
    iget-object v2, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 152
    :try_start_1
    iget-boolean v3, p0, Lemj;->cfq:Z

    if-nez v3, :cond_4

    .line 153
    const/4 v1, 0x1

    iput-boolean v1, p0, Lemj;->cfq:Z

    .line 156
    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 157
    if-eqz v0, :cond_2

    .line 159
    invoke-virtual {p0}, Lemj;->create()V

    goto :goto_0

    .line 156
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final f(Lemy;)V
    .locals 2

    .prologue
    .line 168
    iget-object v1, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 169
    :try_start_0
    iget-object v0, p0, Lemj;->cfs:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lemj;->cfs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lemj;->cfs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 172
    :cond_0
    iget-object v0, p0, Lemj;->cfs:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lemj;->cfs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lemj;->cfq:Z

    .line 174
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public k(Ljava/lang/Object;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 98
    iget-object v1, p0, Lemj;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    if-nez p2, :cond_0

    .line 100
    :try_start_0
    iput-object p1, p0, Lemj;->wi:Ljava/lang/Object;

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lemj;->cfr:Z

    .line 103
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lemj;->cfq:Z

    .line 104
    iget-object v0, p0, Lemj;->cfs:Ljava/util/List;

    .line 105
    const/4 v3, 0x0

    iput-object v3, p0, Lemj;->cfs:Ljava/util/List;

    .line 106
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    if-eqz v0, :cond_2

    .line 108
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemy;

    .line 110
    if-eqz p2, :cond_1

    move-object v1, v2

    :goto_1
    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    move-object v1, p1

    .line 110
    goto :goto_1

    .line 113
    :cond_2
    return-void
.end method
