.class public final Lccr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lefk;


# instance fields
.field private synthetic aRY:Lcom/google/android/remotesearch/RemoteSearchService;

.field private synthetic aRZ:Ljkt;

.field private synthetic aSa:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Lcom/google/android/remotesearch/RemoteSearchService;Ljkt;Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lccr;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object p2, p0, Lccr;->aRZ:Ljkt;

    iput-object p3, p0, Lccr;->aSa:Lcom/google/android/shared/search/Query;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic ar(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 161
    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lccr;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRV:Lefk;

    if-ne p0, v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/actions/VoiceAction;

    instance-of v0, v2, Lcom/google/android/search/shared/actions/ContactOptInAction;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lccr;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    sget-object v1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-static {v0, v2, v1}, Lcom/google/android/remotesearch/RemoteSearchService;->a(Lcom/google/android/remotesearch/RemoteSearchService;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "RemoteSearchService"

    const-string v2, "Can\'t send punt action back"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lccr;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    iget-object v1, p0, Lccr;->aRZ:Ljkt;

    const-string v5, ""

    iget-object v4, p0, Lccr;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Leqt;->H(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    move-object v4, v3

    invoke-virtual/range {v0 .. v7}, Lcky;->a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;Liwg;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljava/lang/String;ZLjava/lang/String;)V

    iget-object v0, p0, Lccr;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    iget-object v1, p0, Lccr;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method
