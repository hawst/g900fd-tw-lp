.class final Lfic;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lifw;


# instance fields
.field final synthetic csf:Lfhs;

.field private csl:Lijj;

.field private csm:Ljava/util/List;


# direct methods
.method public constructor <init>(Lfhs;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 1711
    iput-object p1, p0, Lfic;->csf:Lfhs;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1712
    iput-object p2, p0, Lfic;->csm:Ljava/util/List;

    .line 1713
    iget-object v0, p0, Lfic;->csm:Ljava/util/List;

    invoke-static {v0, p0}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lijj;->q(Ljava/lang/Iterable;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lfic;->csl:Lijj;

    .line 1714
    return-void
.end method

.method private a(Landroid/view/View;ILjava/lang/Integer;)Landroid/view/View;
    .locals 3
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1934
    if-nez p1, :cond_0

    .line 1935
    iget-object v0, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040194

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 1938
    :cond_0
    const v0, 0x7f1101a9

    iget-object v1, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v1}, Lfhs;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 1940
    const-string v0, ""

    .line 1943
    const v1, 0x7f110080

    invoke-static {p1, v1, v0}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 1944
    return-object p1
.end method

.method private a(Landroid/widget/ImageView;I)V
    .locals 3

    .prologue
    .line 2007
    invoke-static {p2}, Lfwq;->jJ(I)I

    move-result v0

    .line 2008
    if-eqz v0, :cond_0

    .line 2009
    iget-object v1, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v1}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 2010
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2011
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2016
    :goto_0
    return-void

    .line 2013
    :cond_0
    const-string v0, "TrainingClosetFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Icon missing for sport: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2014
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;Lfie;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1831
    if-nez p1, :cond_1

    .line 1832
    iget-object v0, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040199

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1836
    :goto_0
    invoke-direct {p0, v1, p2}, Lfic;->c(Landroid/view/View;Lfie;)V

    .line 1838
    iget-object v0, p2, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    .line 1839
    iget-object v2, p0, Lfic;->csf:Lfhs;

    iget-object v2, v2, Lfhs;->csc:Ljava/util/Set;

    new-instance v3, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840
    iget-object v2, p0, Lfic;->csf:Lfhs;

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, p2}, Lfhs;->a(Landroid/view/ViewGroup;Lfie;)V

    .line 1845
    :goto_1
    return-object v1

    .line 1842
    :cond_0
    invoke-static {v1}, Lfhs;->bi(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    move-object v1, p1

    goto :goto_0
.end method

.method private c(Landroid/view/View;Lfie;)V
    .locals 5

    .prologue
    .line 1853
    const v0, 0x7f110257

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1854
    const v1, 0x7f110469

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1855
    iget-object v2, p2, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    .line 1857
    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1858
    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859
    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1863
    const-string v0, ""

    .line 1864
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v3

    .line 1865
    if-eqz v3, :cond_3

    .line 1866
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1898
    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1899
    return-void

    .line 1868
    :pswitch_1
    invoke-virtual {v3}, Ljdf;->bhh()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v0}, Lfhs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0453

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v0}, Lfhs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0454

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1873
    :pswitch_2
    invoke-virtual {v3}, Ljdf;->bhj()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->jb(I)Lfpa;

    move-result-object v0

    .line 1875
    iget-object v0, v0, Lfpa;->cyk:Ljava/lang/String;

    goto :goto_0

    .line 1878
    :pswitch_3
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCm()Ljava/lang/String;

    move-result-object v0

    .line 1879
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1890
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCv()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1884
    :pswitch_4
    iget-object v0, v3, Ljdf;->ebH:Ljdg;

    invoke-virtual {v0}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1887
    :pswitch_5
    iget-object v0, v3, Ljdf;->ebI:Ljdg;

    if-eqz v0, :cond_2

    .line 1888
    iget-object v0, v3, Ljdf;->ebI:Ljdg;

    invoke-virtual {v0}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1894
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCv()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1895
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCv()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1866
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final X(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 1717
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lfic;->e(Ljava/util/List;Z)V

    .line 1718
    return-void
.end method

.method public final a(Landroid/view/View;Lfie;Ljdf;)V
    .locals 3

    .prologue
    .line 1739
    new-instance v0, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v1, p2, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 1742
    iget-object v1, p2, Lfie;->cso:Lfif;

    sget-object v2, Lfif;->csB:Lfif;

    if-ne v1, v2, :cond_1

    .line 1744
    invoke-static {p1}, Lfhs;->bi(Landroid/view/View;)V

    .line 1745
    iget-object v1, p0, Lfic;->csf:Lfhs;

    iget-object v1, v1, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1749
    invoke-virtual {p3}, Ljdf;->bhi()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Ljdf;->bhh()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1750
    iget-object v0, p0, Lfic;->csm:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1751
    iget-object v0, p0, Lfic;->csm:Ljava/util/List;

    invoke-static {v0, p0}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lijj;->q(Ljava/lang/Iterable;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lfic;->csl:Lijj;

    .line 1752
    invoke-virtual {p0}, Lfic;->notifyDataSetChanged()V

    .line 1764
    :cond_0
    :goto_0
    return-void

    .line 1754
    :cond_1
    iget-object v1, p2, Lfie;->cso:Lfif;

    sget-object v2, Lfif;->csu:Lfif;

    if-eq v1, v2, :cond_2

    iget-object v1, p2, Lfie;->cso:Lfif;

    sget-object v2, Lfif;->cst:Lfif;

    if-eq v1, v2, :cond_2

    iget-object v1, p2, Lfie;->cso:Lfif;

    sget-object v2, Lfif;->csE:Lfif;

    if-ne v1, v2, :cond_0

    .line 1759
    :cond_2
    const v1, 0x7f110466

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1760
    invoke-direct {p0, v1, p2}, Lfic;->c(Landroid/view/View;Lfie;)V

    .line 1761
    invoke-static {p1}, Lfhs;->bi(Landroid/view/View;)V

    .line 1762
    iget-object v1, p0, Lfic;->csf:Lfhs;

    iget-object v1, v1, Lfhs;->csc:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final aAe()Ljava/util/List;
    .locals 1

    .prologue
    .line 1729
    iget-object v0, p0, Lfic;->csm:Ljava/util/List;

    return-object v0
.end method

.method public final synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1704
    check-cast p1, Lfie;

    invoke-virtual {p1}, Lfie;->aAf()Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/util/List;Z)V
    .locals 1

    .prologue
    .line 1721
    iput-object p1, p0, Lfic;->csm:Ljava/util/List;

    .line 1722
    iget-object v0, p0, Lfic;->csm:Ljava/util/List;

    invoke-static {v0, p0}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lijj;->q(Ljava/lang/Iterable;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lfic;->csl:Lijj;

    .line 1723
    if-eqz p2, :cond_0

    .line 1724
    invoke-virtual {p0}, Lfic;->notifyDataSetChanged()V

    .line 1726
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1768
    iget-object v0, p0, Lfic;->csl:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1773
    iget-object v0, p0, Lfic;->csl:Lijj;

    invoke-virtual {v0, p1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1779
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1784
    iget-object v0, p0, Lfic;->csl:Lijj;

    invoke-virtual {v0, p1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    iget-object v0, v0, Lfie;->cso:Lfif;

    invoke-virtual {v0}, Lfif;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const v4, 0x1020016

    const/4 v2, 0x0

    .line 1799
    invoke-virtual {p0, p1}, Lfic;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    .line 1801
    sget-object v1, Lfib;->csk:[I

    iget-object v3, v0, Lfie;->cso:Lfif;

    invoke-virtual {v3}, Lfif;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move-object p2, v2

    .line 1826
    :goto_0
    return-object p2

    .line 1807
    :pswitch_0
    invoke-direct {p0, p2, v0}, Lfic;->b(Landroid/view/View;Lfie;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 1810
    :pswitch_1
    if-nez p2, :cond_0

    iget-object v1, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v1}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f040195

    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v2, v0, Lfie;->csr:Ljhr;

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v2}, Ljhr;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f110463

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, v0, Lfie;->css:I

    if-lez v2, :cond_1

    iget v0, v0, Lfie;->css:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1814
    :pswitch_2
    iget-object v1, v0, Lfie;->bOP:Ljava/lang/String;

    if-nez p2, :cond_2

    iget-object v0, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v0}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04019a

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_2
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1818
    :pswitch_3
    const v0, 0x7f0a033a

    invoke-direct {p0, p2, v0, v2}, Lfic;->a(Landroid/view/View;ILjava/lang/Integer;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 1820
    :pswitch_4
    const v0, 0x7f0a0458

    invoke-direct {p0, p2, v0, v2}, Lfic;->a(Landroid/view/View;ILjava/lang/Integer;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 1822
    :pswitch_5
    if-nez p2, :cond_8

    new-instance v2, Lfhf;

    iget-object v1, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v1}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v2, v1}, Lfhf;-><init>(Landroid/content/Context;)V

    :goto_1
    move-object v1, v2

    check-cast v1, Lfhf;

    new-instance v3, Lfid;

    invoke-direct {v3, p0}, Lfid;-><init>(Lfic;)V

    invoke-virtual {v1, v3}, Lfhf;->a(Lfhi;)V

    iget-object v0, v0, Lfie;->csq:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v1, v0}, Lfhf;->a(Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;)V

    move-object p2, v2

    goto/16 :goto_0

    .line 1824
    :pswitch_6
    iget-object v1, v0, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCn()Ljdg;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v1, "TrainingClosetFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "INTERESTED_IN question without entity: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v3

    invoke-virtual {v3}, Ljde;->bhe()[B

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p2, v0}, Lfic;->b(Landroid/view/View;Lfie;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    :cond_3
    if-nez p2, :cond_7

    iget-object v1, p0, Lfic;->csf:Lfhs;

    invoke-virtual {v1}, Lfhs;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v5, 0x7f040198

    invoke-virtual {v1, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    :goto_2
    const v1, 0x7f1102ea

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v4}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x1020006

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v5, v4, Ljdg;->ebK:Ljfh;

    if-eqz v5, :cond_4

    iget-object v4, v4, Ljdg;->ebK:Ljfh;

    invoke-virtual {v4}, Ljfh;->bew()I

    move-result v4

    invoke-direct {p0, v1, v4}, Lfic;->a(Landroid/widget/ImageView;I)V

    :goto_3
    iget-object v1, p0, Lfic;->csf:Lfhs;

    iget-object v1, v1, Lfhs;->csc:Ljava/util/Set;

    new-instance v4, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v3

    invoke-direct {v4, v3}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v3, p0, Lfic;->csf:Lfhs;

    move-object v1, v2

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v3, v1, v0}, Lfhs;->a(Landroid/view/ViewGroup;Lfie;)V

    :goto_4
    move-object p2, v2

    goto/16 :goto_0

    :cond_4
    iget-object v5, v4, Ljdg;->ebL:Ljfi;

    if-eqz v5, :cond_5

    iget-object v4, v4, Ljdg;->ebL:Ljfi;

    invoke-virtual {v4}, Ljfi;->bew()I

    move-result v4

    invoke-direct {p0, v1, v4}, Lfic;->a(Landroid/widget/ImageView;I)V

    goto :goto_3

    :cond_5
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_6
    invoke-static {v2}, Lfhs;->bi(Landroid/view/View;)V

    goto :goto_4

    :cond_7
    move-object v2, p2

    goto :goto_2

    :cond_8
    move-object v2, p2

    goto/16 :goto_1

    .line 1801
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1789
    invoke-static {}, Lfif;->values()[Lfif;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 3

    .prologue
    .line 1794
    invoke-virtual {p0, p1}, Lfic;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfie;

    iget-object v1, v0, Lfie;->cso:Lfif;

    sget-object v2, Lfif;->csx:Lfif;

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lfie;->cso:Lfif;

    sget-object v1, Lfif;->csw:Lfif;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
