.class public abstract Lhfa;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field protected cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

.field private cvK:Lfnx;

.field protected deE:Ljava/lang/CharSequence;

.field private mDrawerListener:Lfoh;

.field protected mVelvetServices:Lgql;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lhfa;->wd()I

    move-result v0

    invoke-virtual {p0, v0}, Lhfa;->setContentView(I)V

    .line 33
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iput-object v0, p0, Lhfa;->mVelvetServices:Lgql;

    .line 35
    const v0, 0x7f110295

    invoke-virtual {p0, v0}, Lhfa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    iput-object v0, p0, Lhfa;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    .line 37
    iget-object v0, p0, Lhfa;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v1, Lhfb;

    invoke-direct {v1, p0}, Lhfb;-><init>(Lhfa;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->a(Lejr;)V

    .line 60
    new-instance v0, Lfnx;

    iget-object v1, p0, Lhfa;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->aCL()Lfqn;

    move-result-object v1

    iget-object v2, p0, Lhfa;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v3, Lgyx;

    iget-object v4, p0, Lhfa;->mVelvetServices:Lgql;

    invoke-direct {v3, v4}, Lgyx;-><init>(Lgql;)V

    invoke-direct {v0, p0, v1, v2, v3}, Lfnx;-><init>(Landroid/app/Activity;Lfom;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfon;)V

    iput-object v0, p0, Lhfa;->cvK:Lfnx;

    .line 64
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    iget-object v1, p0, Lhfa;->mDrawerListener:Lfoh;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lhfa;->we()Lfoh;

    move-result-object v1

    iput-object v1, p0, Lhfa;->mDrawerListener:Lfoh;

    :cond_0
    iget-object v1, p0, Lhfa;->mDrawerListener:Lfoh;

    invoke-virtual {v0, v1}, Lfnx;->a(Lfoh;)V

    .line 66
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onPostCreate(Landroid/os/Bundle;)V

    .line 67
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onDestroy()V

    .line 72
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 73
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lhfa;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cN()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onResume()V

    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 79
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 97
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStart()V

    .line 84
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 85
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lhfa;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStop()V

    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 91
    return-void
.end method

.method protected abstract wd()I
.end method

.method protected abstract we()Lfoh;
.end method
