.class public final Ljpf;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dXL:J

.field private dXM:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 5998
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5999
    const/4 v0, 0x0

    iput v0, p0, Ljpf;->aez:I

    iput-wide v2, p0, Ljpf;->dXL:J

    iput-wide v2, p0, Ljpf;->dXM:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljpf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpf;->eCz:I

    .line 6000
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 5941
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljpf;->dXL:J

    iget v0, p0, Ljpf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljpf;->dXM:J

    iget v0, p0, Ljpf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 6014
    iget v0, p0, Ljpf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6015
    const/4 v0, 0x1

    iget-wide v2, p0, Ljpf;->dXL:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 6017
    :cond_0
    iget v0, p0, Ljpf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6018
    const/4 v0, 0x2

    iget-wide v2, p0, Ljpf;->dXM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 6020
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6021
    return-void
.end method

.method public final beH()J
    .locals 2

    .prologue
    .line 5963
    iget-wide v0, p0, Ljpf;->dXL:J

    return-wide v0
.end method

.method public final beJ()J
    .locals 2

    .prologue
    .line 5982
    iget-wide v0, p0, Ljpf;->dXM:J

    return-wide v0
.end method

.method public final dE(J)Ljpf;
    .locals 1

    .prologue
    .line 5966
    iput-wide p1, p0, Ljpf;->dXL:J

    .line 5967
    iget v0, p0, Ljpf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpf;->aez:I

    .line 5968
    return-object p0
.end method

.method public final dF(J)Ljpf;
    .locals 1

    .prologue
    .line 5985
    iput-wide p1, p0, Ljpf;->dXM:J

    .line 5986
    iget v0, p0, Ljpf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpf;->aez:I

    .line 5987
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 6025
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6026
    iget v1, p0, Ljpf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6027
    const/4 v1, 0x1

    iget-wide v2, p0, Ljpf;->dXL:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6030
    :cond_0
    iget v1, p0, Ljpf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6031
    const/4 v1, 0x2

    iget-wide v2, p0, Ljpf;->dXM:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6034
    :cond_1
    return v0
.end method
