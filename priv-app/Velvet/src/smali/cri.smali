.class final Lcri;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field private synthetic bhc:Lefk;

.field private synthetic bhd:Lcrh;


# direct methods
.method constructor <init>(Lcrh;Lefk;)V
    .locals 0

    .prologue
    .line 347
    iput-object p1, p0, Lcri;->bhd:Lcrh;

    iput-object p2, p0, Lcri;->bhc:Lefk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 351
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/accounts/Account;

    .line 352
    if-eqz v0, :cond_2

    array-length v2, v0

    if-lez v2, :cond_2

    .line 354
    iget-object v2, p0, Lcri;->bhd:Lcrh;

    invoke-virtual {v2}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v2

    .line 355
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 356
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 357
    iget-object v0, p0, Lcri;->bhc:Lefk;

    invoke-interface {v0, v2}, Lefk;->ar(Ljava/lang/Object;)V

    .line 373
    :goto_1
    return-void

    .line 355
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 362
    :cond_1
    iget-object v1, p0, Lcri;->bhc:Lefk;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v0}, Lefk;->ar(Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 365
    :catch_0
    move-exception v0

    .line 366
    const-string v1, "Search.LoginHelper"

    const-string v2, "Retrieving Google accounts failed"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 372
    :cond_2
    :goto_2
    iget-object v0, p0, Lcri;->bhc:Lefk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lefk;->ar(Ljava/lang/Object;)V

    goto :goto_1

    .line 367
    :catch_1
    move-exception v0

    .line 368
    const-string v1, "Search.LoginHelper"

    const-string v2, "Retrieving Google accounts failed"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 369
    :catch_2
    move-exception v0

    .line 370
    const-string v1, "Search.LoginHelper"

    const-string v2, "Retrieving Google accounts failed"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
