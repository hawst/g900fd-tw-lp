.class public final Ljsu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final eCA:[I

.field public static final eCB:[J

.field public static final eCC:[F

.field public static final eCD:[Z

.field public static final eCE:[Ljava/lang/String;

.field public static final eCF:[[B

.field public static final eCG:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    new-array v0, v1, [I

    sput-object v0, Ljsu;->eCA:[I

    .line 76
    new-array v0, v1, [J

    sput-object v0, Ljsu;->eCB:[J

    .line 77
    new-array v0, v1, [F

    sput-object v0, Ljsu;->eCC:[F

    .line 78
    new-array v0, v1, [Z

    sput-object v0, Ljsu;->eCD:[Z

    .line 80
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Ljsu;->eCE:[Ljava/lang/String;

    .line 81
    new-array v0, v1, [[B

    sput-object v0, Ljsu;->eCF:[[B

    .line 82
    new-array v0, v1, [B

    sput-object v0, Ljsu;->eCG:[B

    return-void
.end method

.method public static b(Ljsi;I)Z
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Ljsi;->rU(I)Z

    move-result v0

    return v0
.end method

.method static bz(II)I
    .locals 1

    .prologue
    .line 72
    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static final c(Ljsi;I)I
    .locals 3

    .prologue
    .line 113
    const/4 v0, 0x1

    .line 114
    invoke-virtual {p0}, Ljsi;->getPosition()I

    move-result v1

    .line 115
    invoke-virtual {p0, p1}, Ljsi;->rU(I)Z

    .line 116
    :goto_0
    invoke-virtual {p0}, Ljsi;->btV()I

    move-result v2

    if-lez v2, :cond_0

    .line 117
    invoke-virtual {p0}, Ljsi;->btN()I

    move-result v2

    .line 118
    if-ne v2, p1, :cond_0

    .line 119
    invoke-virtual {p0, p1}, Ljsi;->rU(I)Z

    .line 122
    add-int/lit8 v0, v0, 0x1

    .line 123
    goto :goto_0

    .line 124
    :cond_0
    invoke-virtual {p0, v1}, Ljsi;->rX(I)V

    .line 125
    return v0
.end method

.method static sk(I)I
    .locals 1

    .prologue
    .line 62
    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method public static sl(I)I
    .locals 1

    .prologue
    .line 67
    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method
