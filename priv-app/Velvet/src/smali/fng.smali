.class final Lfng;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic cwK:Lfmv;

.field private dz:Landroid/net/Uri;

.field private final ny:I

.field private final wo:I


# direct methods
.method constructor <init>(Lfmv;II)V
    .locals 1

    .prologue
    .line 1042
    iput-object p1, p0, Lfng;->cwK:Lfmv;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1040
    const/4 v0, 0x0

    iput-object v0, p0, Lfng;->dz:Landroid/net/Uri;

    .line 1043
    iput p2, p0, Lfng;->ny:I

    .line 1044
    iput p3, p0, Lfng;->wo:I

    .line 1045
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    const/16 v4, 0x14

    const/16 v3, 0x8

    const/4 v12, 0x0

    .line 1029
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const v0, 0x7f020053

    const/4 v2, 0x6

    if-lt v1, v2, :cond_1

    if-ge v1, v3, :cond_1

    const v0, 0x7f020050

    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lfng;->dz:Landroid/net/Uri;

    iget-object v1, p0, Lfng;->dz:Landroid/net/Uri;

    iget-object v2, p0, Lfng;->cwK:Lfmv;

    iget-object v2, v2, Lfmv;->cwC:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    if-lt v1, v3, :cond_2

    if-ge v1, v4, :cond_2

    const v0, 0x7f020051

    goto :goto_0

    :cond_2
    if-lt v1, v4, :cond_0

    const/16 v2, 0x16

    if-ge v1, v2, :cond_0

    const v0, 0x7f020052

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lfng;->cwK:Lfmv;

    iget-object v1, v1, Lfmv;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lfng;->cwK:Lfmv;

    iget-object v2, v2, Lfmv;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lfng;->ny:I

    iget v4, p0, Lfng;->wo:I

    int-to-double v6, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-double v8, v5

    div-double/2addr v6, v8

    int-to-double v8, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v10, v5

    div-double/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-double v8, v5

    mul-double/2addr v8, v6

    double-to-int v5, v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-double v8, v8

    mul-double/2addr v6, v8

    double-to-int v6, v6

    if-ne v5, v3, :cond_4

    if-ne v6, v4, :cond_4

    :goto_2
    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    goto :goto_1

    :cond_4
    const/4 v7, 0x1

    invoke-static {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    sub-int v7, v5, v3

    div-int/lit8 v7, v7, 0x2

    sub-int v8, v6, v4

    div-int/lit8 v8, v8, 0x2

    invoke-static {v12, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-static {v12, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v0, v7, v8, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1029
    check-cast p1, Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lfng;->cwK:Lfmv;

    iget-object v1, p0, Lfng;->dz:Landroid/net/Uri;

    iput-object v1, v0, Lfmv;->cwC:Landroid/net/Uri;

    iget-object v0, p0, Lfng;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lfnc;->a(Landroid/graphics/drawable/Drawable;ZLjava/lang/String;)V

    :cond_0
    return-void
.end method
