.class public final Lapt;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public alM:Lapv;

.field private alN:Ljava/lang/String;

.field private alO:Ljava/lang/String;

.field public alP:[Lapu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2664
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2665
    const/4 v0, 0x0

    iput v0, p0, Lapt;->aez:I

    iput-object v1, p0, Lapt;->alM:Lapv;

    const-string v0, ""

    iput-object v0, p0, Lapt;->alN:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapt;->alO:Ljava/lang/String;

    invoke-static {}, Lapu;->ts()[Lapu;

    move-result-object v0

    iput-object v0, p0, Lapt;->alP:[Lapu;

    iput-object v1, p0, Lapt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapt;->eCz:I

    .line 2666
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2382
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lapt;->alM:Lapv;

    if-nez v0, :cond_1

    new-instance v0, Lapv;

    invoke-direct {v0}, Lapv;-><init>()V

    iput-object v0, p0, Lapt;->alM:Lapv;

    :cond_1
    iget-object v0, p0, Lapt;->alM:Lapv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapt;->alN:Ljava/lang/String;

    iget v0, p0, Lapt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapt;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lapt;->alP:[Lapu;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lapu;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lapt;->alP:[Lapu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lapu;

    invoke-direct {v3}, Lapu;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lapt;->alP:[Lapu;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lapu;

    invoke-direct {v3}, Lapu;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lapt;->alP:[Lapu;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapt;->alO:Ljava/lang/String;

    iget v0, p0, Lapt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapt;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2682
    iget-object v0, p0, Lapt;->alM:Lapv;

    if-eqz v0, :cond_0

    .line 2683
    const/4 v0, 0x1

    iget-object v1, p0, Lapt;->alM:Lapv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2685
    :cond_0
    iget v0, p0, Lapt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 2686
    const/4 v0, 0x2

    iget-object v1, p0, Lapt;->alN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2688
    :cond_1
    iget-object v0, p0, Lapt;->alP:[Lapu;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lapt;->alP:[Lapu;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2689
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lapt;->alP:[Lapu;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 2690
    iget-object v1, p0, Lapt;->alP:[Lapu;

    aget-object v1, v1, v0

    .line 2691
    if-eqz v1, :cond_2

    .line 2692
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 2689
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2696
    :cond_3
    iget v0, p0, Lapt;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 2697
    const/4 v0, 0x4

    iget-object v1, p0, Lapt;->alO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2699
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2700
    return-void
.end method

.method public final dI(Ljava/lang/String;)Lapt;
    .locals 1

    .prologue
    .line 2623
    if-nez p1, :cond_0

    .line 2624
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2626
    :cond_0
    iput-object p1, p0, Lapt;->alN:Ljava/lang/String;

    .line 2627
    iget v0, p0, Lapt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapt;->aez:I

    .line 2628
    return-object p0
.end method

.method public final dJ(Ljava/lang/String;)Lapt;
    .locals 1

    .prologue
    .line 2645
    if-nez p1, :cond_0

    .line 2646
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2648
    :cond_0
    iput-object p1, p0, Lapt;->alO:Ljava/lang/String;

    .line 2649
    iget v0, p0, Lapt;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapt;->aez:I

    .line 2650
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 2704
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2705
    iget-object v1, p0, Lapt;->alM:Lapv;

    if-eqz v1, :cond_0

    .line 2706
    const/4 v1, 0x1

    iget-object v2, p0, Lapt;->alM:Lapv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2709
    :cond_0
    iget v1, p0, Lapt;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 2710
    const/4 v1, 0x2

    iget-object v2, p0, Lapt;->alN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2713
    :cond_1
    iget-object v1, p0, Lapt;->alP:[Lapu;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lapt;->alP:[Lapu;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 2714
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lapt;->alP:[Lapu;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2715
    iget-object v2, p0, Lapt;->alP:[Lapu;

    aget-object v2, v2, v0

    .line 2716
    if-eqz v2, :cond_2

    .line 2717
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2714
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2722
    :cond_4
    iget v1, p0, Lapt;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 2723
    const/4 v1, 0x4

    iget-object v2, p0, Lapt;->alO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2726
    :cond_5
    return v0
.end method

.method public final tq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2620
    iget-object v0, p0, Lapt;->alN:Ljava/lang/String;

    return-object v0
.end method

.method public final tr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2642
    iget-object v0, p0, Lapt;->alO:Ljava/lang/String;

    return-object v0
.end method
