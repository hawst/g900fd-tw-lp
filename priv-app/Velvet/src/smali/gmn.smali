.class public final Lgmn;
.super Lcen;
.source "PG"


# instance fields
.field private final aTr:Ljava/lang/String;

.field private final cPo:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cPp:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cPq:Ljxh;

.field private final cPr:Ljxa;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljwe;Ljwe;Ljxh;Ljxa;Ljava/lang/String;Ljava/lang/String;Lgdo;)V
    .locals 9
    .param p1    # Ljava/util/concurrent/Future;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Future;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljwe;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 50
    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    invoke-direct/range {v1 .. v8}, Lcen;-><init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljwe;Ljwe;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iput-object p1, p0, Lgmn;->cPo:Ljava/util/concurrent/Future;

    .line 53
    iput-object p2, p0, Lgmn;->cPp:Ljava/util/concurrent/Future;

    .line 54
    move-object/from16 v0, p8

    iput-object v0, p0, Lgmn;->cPq:Ljxh;

    .line 55
    move-object/from16 v0, p12

    iput-object v0, p0, Lgmn;->mSpeechSettings:Lgdo;

    .line 56
    move-object/from16 v0, p11

    iput-object v0, p0, Lgmn;->aTr:Ljava/lang/String;

    .line 57
    move-object/from16 v0, p9

    iput-object v0, p0, Lgmn;->cPr:Ljxa;

    .line 58
    return-void
.end method


# virtual methods
.method public final Dn()Ljwv;
    .locals 4

    .prologue
    .line 63
    invoke-super {p0}, Lcen;->Dn()Ljwv;

    move-result-object v2

    .line 65
    iget-object v0, p0, Lgmn;->cPo:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 67
    :try_start_0
    sget-object v0, Ljwq;->eJt:Ljsm;

    iget-object v1, p0, Lgmn;->aTE:Lces;

    iget-object v3, p0, Lgmn;->cPo:Ljava/util/concurrent/Future;

    invoke-virtual {v1, v3}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    iget-object v0, p0, Lgmn;->cPp:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 75
    :try_start_1
    sget-object v0, Ljwj;->eJb:Ljsm;

    iget-object v1, p0, Lgmn;->aTE:Lces;

    iget-object v3, p0, Lgmn;->cPp:Ljava/util/concurrent/Future;

    invoke-virtual {v1, v3}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 81
    :cond_1
    const-string v0, "voicesearch"

    iget-object v1, p0, Lgmn;->aTr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ljwu;->eJE:Ljsm;

    invoke-virtual {v2, v0}, Ljwv;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwu;

    invoke-virtual {v0}, Ljwu;->bvz()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ljxd;->eKc:Ljsm;

    new-instance v1, Ljxd;

    invoke-direct {v1}, Ljxd;-><init>()V

    invoke-virtual {v2, v0, v1}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    sget-object v0, Ljxd;->eKc:Ljsm;

    invoke-virtual {v2, v0}, Ljwv;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxd;

    new-instance v3, Ljnz;

    invoke-direct {v3}, Ljnz;-><init>()V

    sget-object v1, Ljwu;->eJE:Ljsm;

    invoke-virtual {v2, v1}, Ljwv;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljwu;

    invoke-virtual {v1}, Ljwu;->OJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljnz;->xw(Ljava/lang/String;)Ljnz;

    move-result-object v1

    iput-object v1, v0, Ljxd;->eKg:Ljnz;

    .line 82
    :cond_2
    iget-object v0, p0, Lgmn;->cPq:Ljxh;

    if-eqz v0, :cond_3

    .line 83
    sget-object v0, Ljxh;->eKv:Ljsm;

    iget-object v1, p0, Lgmn;->cPq:Ljxh;

    invoke-virtual {v2, v0, v1}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 85
    :cond_3
    iget-object v0, p0, Lgmn;->cPr:Ljxa;

    if-eqz v0, :cond_4

    .line 86
    sget-object v0, Ljxa;->eJX:Ljsm;

    iget-object v1, p0, Lgmn;->cPr:Ljxa;

    invoke-virtual {v2, v0, v1}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 88
    :cond_4
    iget-object v0, p0, Lgmn;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFh()Z

    move-result v0

    invoke-virtual {v2, v0}, Ljwv;->jk(Z)Ljwv;

    .line 90
    return-object v2

    .line 69
    :catch_0
    move-exception v0

    .line 70
    new-instance v1, Leie;

    const v2, 0x20001

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 77
    :catch_1
    move-exception v0

    .line 78
    new-instance v1, Leie;

    const v2, 0x20007

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method
