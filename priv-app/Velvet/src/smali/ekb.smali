.class public Lekb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private AC:Z

.field public final caO:I

.field public ccA:I

.field public ccB:Z

.field public ccC:Landroid/widget/EdgeEffect;

.field private final ccD:Lekd;

.field public ccE:I

.field public ccF:Z

.field public ccG:Z

.field private ccH:Z

.field public final ccI:Ljava/lang/Runnable;

.field private final ccr:Landroid/view/ViewConfiguration;

.field public final ccs:Landroid/widget/OverScroller;

.field private cct:F

.field private ccu:I

.field private ccv:I

.field private ccw:I

.field public ccx:I

.field public ccy:Z

.field public ccz:Z

.field public fW:Landroid/view/VelocityTracker;

.field private gM:I

.field private final mContext:Landroid/content/Context;

.field public mScrollConsumer:Leka;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mScrollViewControl:Lekf;

.field public final mView:Landroid/view/View;

.field public oN:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lekf;Landroid/view/View;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput v0, p0, Lekb;->ccE:I

    .line 106
    iput v0, p0, Lekb;->gM:I

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekb;->oN:Z

    .line 120
    new-instance v0, Lekc;

    invoke-direct {v0, p0}, Lekc;-><init>(Lekb;)V

    iput-object v0, p0, Lekb;->ccI:Ljava/lang/Runnable;

    .line 138
    iput-object p1, p0, Lekb;->mContext:Landroid/content/Context;

    .line 139
    iput-object p2, p0, Lekb;->mScrollViewControl:Lekf;

    .line 140
    iput-object p3, p0, Lekb;->mView:Landroid/view/View;

    .line 141
    iput p4, p0, Lekb;->caO:I

    .line 142
    new-instance v0, Lekd;

    invoke-direct {v0}, Lekd;-><init>()V

    iput-object v0, p0, Lekb;->ccD:Lekd;

    .line 143
    invoke-virtual {p0}, Lekb;->atY()Landroid/widget/OverScroller;

    move-result-object v0

    iput-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    .line 144
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object v0, p0, Lekb;->ccr:Landroid/view/ViewConfiguration;

    .line 145
    return-void
.end method

.method private atW()Z
    .locals 2

    .prologue
    .line 612
    iget v0, p0, Lekb;->ccw:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lekb;->ccr:Landroid/view/ViewConfiguration;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private atX()V
    .locals 2

    .prologue
    .line 617
    iget v0, p0, Lekb;->caO:I

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lekb;->ccu:I

    if-eqz v0, :cond_1

    .line 618
    iget-object v0, p0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    if-nez v0, :cond_0

    .line 619
    new-instance v0, Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lekb;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    .line 620
    iget-object v0, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->willNotDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const-string v0, "Velvet.ScrollHelper"

    const-string v1, "Can\'t draw overscroll effects if the view doesn\'t draw"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekb;->ccB:Z

    .line 628
    :cond_1
    iget-boolean v0, p0, Lekb;->ccB:Z

    if-eqz v0, :cond_3

    .line 629
    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_4

    .line 630
    iget v0, p0, Lekb;->ccu:I

    int-to-float v0, v0

    .line 631
    iget-boolean v1, p0, Lekb;->ccz:Z

    if-eqz v1, :cond_2

    .line 632
    neg-float v0, v0

    .line 634
    :cond_2
    iget-object v1, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 635
    iget-object v1, p0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, v0}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 636
    iget-object v0, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 643
    :cond_3
    :goto_0
    return-void

    .line 638
    :cond_4
    iget-object v0, p0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 639
    iget-object v0, p0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    .line 640
    const/4 v0, 0x0

    iput-boolean v0, p0, Lekb;->ccB:Z

    goto :goto_0
.end method

.method private eX(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 698
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lekb;->AC:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lekb;->ccG:Z

    if-eqz v0, :cond_1

    .line 699
    :cond_0
    iget v0, p0, Lekb;->gM:I

    invoke-virtual {p0, v0}, Lekb;->hX(I)V

    .line 701
    :cond_1
    invoke-virtual {p0}, Lekb;->atf()Z

    move-result v0

    if-nez v0, :cond_2

    .line 702
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->ath()V

    .line 704
    :cond_2
    iput-boolean v1, p0, Lekb;->AC:Z

    .line 705
    const/4 v0, -0x1

    iput v0, p0, Lekb;->gM:I

    .line 706
    iput-boolean v1, p0, Lekb;->ccH:Z

    .line 708
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lekb;->ccF:Z

    if-nez v0, :cond_3

    .line 709
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 710
    const/4 v0, 0x0

    iput-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    .line 713
    :cond_3
    iget-boolean v0, p0, Lekb;->ccB:Z

    if-eqz v0, :cond_4

    .line 714
    iget-object v0, p0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 716
    :cond_4
    iput-boolean v1, p0, Lekb;->ccy:Z

    .line 717
    iput v1, p0, Lekb;->ccx:I

    .line 719
    iget-boolean v0, p0, Lekb;->ccG:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lekb;->atV()Z

    move-result v0

    if-nez v0, :cond_5

    .line 720
    iget-object v0, p0, Lekb;->mScrollConsumer:Leka;

    invoke-interface {v0}, Leka;->atu()V

    .line 721
    iput-boolean v1, p0, Lekb;->ccG:Z

    .line 723
    :cond_5
    return-void
.end method

.method private hV(I)I
    .locals 2

    .prologue
    .line 306
    if-lez p1, :cond_1

    .line 307
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->getMaxScrollY()I

    move-result v0

    iget-object v1, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v1}, Lekf;->getScrollY()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 312
    :cond_0
    :goto_0
    return p1

    .line 309
    :cond_1
    if-gez p1, :cond_0

    .line 310
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    neg-int v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0
.end method

.method private m(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 513
    invoke-virtual {p0}, Lekb;->atf()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 516
    iput-boolean v0, p0, Lekb;->AC:Z

    .line 517
    const/4 v2, -0x1

    iput v2, p0, Lekb;->ccE:I

    .line 518
    iget-object v2, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->abortAnimation()V

    .line 520
    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iput v2, p0, Lekb;->gM:I

    .line 521
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lekb;->cct:F

    .line 522
    iput-boolean v1, p0, Lekb;->ccy:Z

    .line 523
    iput v1, p0, Lekb;->ccw:I

    .line 527
    iget-object v2, p0, Lekb;->mScrollConsumer:Leka;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lekb;->ccG:Z

    if-nez v2, :cond_1

    .line 528
    iget-object v2, p0, Lekb;->mScrollConsumer:Leka;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-interface {v2, v3, v4}, Leka;->t(FF)I

    move-result v2

    .line 529
    if-nez v2, :cond_2

    .line 530
    iput-boolean v0, p0, Lekb;->ccG:Z

    .line 535
    :cond_1
    :goto_0
    return-void

    .line 532
    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    :goto_1
    iput-boolean v0, p0, Lekb;->ccH:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private n(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 539
    iget v0, p0, Lekb;->gM:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget v0, p0, Lekb;->gM:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 543
    if-ltz v0, :cond_0

    .line 546
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    .line 547
    iget v0, p0, Lekb;->cct:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_3

    .line 548
    iget v0, p0, Lekb;->cct:F

    sub-float v2, v0, v1

    .line 549
    float-to-int v0, v2

    .line 551
    int-to-float v3, v0

    sub-float v2, v3, v2

    sub-float/2addr v1, v2

    iput v1, p0, Lekb;->cct:F

    .line 552
    iget-boolean v1, p0, Lekb;->ccG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lekb;->mScrollConsumer:Leka;

    invoke-interface {v1, v0}, Leka;->hO(I)I

    move-result v0

    :cond_2
    iput v0, p0, Lekb;->ccu:I

    .line 555
    iget v0, p0, Lekb;->ccw:I

    iget v1, p0, Lekb;->ccu:I

    add-int/2addr v0, v1

    iput v0, p0, Lekb;->ccw:I

    .line 557
    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_0

    .line 558
    iget v0, p0, Lekb;->ccx:I

    iget v1, p0, Lekb;->ccu:I

    add-int/2addr v0, v1

    iput v0, p0, Lekb;->ccx:I

    goto :goto_0

    .line 561
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lekb;->ccu:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Leka;)V
    .locals 2
    .param p1    # Leka;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 763
    iput-object p1, p0, Lekb;->mScrollConsumer:Leka;

    .line 764
    const/4 v0, 0x0

    iput-boolean v0, p0, Lekb;->ccG:Z

    .line 765
    iget-object v0, p0, Lekb;->mView:Landroid/view/View;

    iget-object v1, p0, Lekb;->ccI:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 766
    return-void
.end method

.method public final a(ILandroid/animation/TimeInterpolator;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 425
    invoke-virtual {p0}, Lekb;->atf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v0, v6}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 428
    :cond_0
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v2

    .line 429
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->getMaxScrollY()I

    move-result v0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 430
    if-eq v3, v2, :cond_1

    .line 431
    iput v3, p0, Lekb;->ccE:I

    .line 432
    iget-object v0, p0, Lekb;->ccD:Lekd;

    invoke-virtual {v0, p2}, Lekd;->a(Landroid/animation/TimeInterpolator;)V

    .line 433
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    .line 434
    iget-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    sub-int/2addr v3, v2

    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/widget/OverScroller;->startScroll(IIII)V

    .line 438
    :goto_0
    iget-object v0, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    move v1, v6

    .line 441
    :cond_1
    return v1

    .line 436
    :cond_2
    iget-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    sub-int v4, v3, v2

    move v3, v1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_0
.end method

.method protected atT()V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_0

    .line 347
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 349
    :cond_0
    return-void
.end method

.method public final atU()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 365
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2}, Lekf;->getScrollY()I

    move-result v2

    .line 366
    iget-object v3, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v3}, Lekf;->getMaxScrollY()I

    move-result v3

    .line 367
    iget v4, p0, Lekb;->ccE:I

    packed-switch v4, :pswitch_data_0

    .line 376
    iget v2, p0, Lekb;->ccE:I

    if-le v2, v3, :cond_3

    .line 379
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 382
    invoke-virtual {p0}, Lekb;->atf()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    .line 388
    iput v5, p0, Lekb;->ccE:I

    .line 390
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0, v5}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    .line 392
    :cond_2
    return-void

    .line 370
    :pswitch_0
    iget-object v2, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v2

    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 373
    :pswitch_1
    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 376
    goto :goto_0

    .line 367
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final atV()Z
    .locals 2

    .prologue
    .line 399
    iget v0, p0, Lekb;->ccE:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected atY()Landroid/widget/OverScroller;
    .locals 3

    .prologue
    .line 785
    new-instance v0, Landroid/widget/OverScroller;

    iget-object v1, p0, Lekb;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lekb;->ccD:Lekd;

    invoke-direct {v0, v1, v2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method

.method public final atf()Z
    .locals 2

    .prologue
    .line 395
    iget v0, p0, Lekb;->ccE:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final eY(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 733
    iget-boolean v0, p0, Lekb;->oN:Z

    if-ne v0, p1, :cond_1

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 735
    :cond_1
    iput-boolean p1, p0, Lekb;->oN:Z

    .line 737
    if-nez p1, :cond_0

    .line 739
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    .line 740
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 741
    const/4 v0, 0x0

    iput-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    .line 743
    :cond_2
    iput-boolean v1, p0, Lekb;->ccF:Z

    .line 746
    iput-boolean v1, p0, Lekb;->AC:Z

    .line 747
    const/4 v0, -0x1

    iput v0, p0, Lekb;->gM:I

    .line 748
    invoke-virtual {p0}, Lekb;->atf()Z

    move-result v0

    if-nez v0, :cond_3

    .line 749
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->ath()V

    .line 753
    :cond_3
    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_0

    .line 754
    iput-boolean v1, p0, Lekb;->ccy:Z

    .line 755
    invoke-direct {p0}, Lekb;->atX()V

    .line 756
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->atn()V

    goto :goto_0
.end method

.method public final hW(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const v4, 0x3e4ccccd    # 0.2f

    const/4 v1, 0x0

    .line 320
    iget-boolean v2, p0, Lekb;->oN:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lekb;->mScrollConsumer:Leka;

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 340
    :goto_0
    return v0

    .line 322
    :cond_1
    const/16 v2, 0x14

    if-ne p1, v2, :cond_2

    .line 323
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2}, Lekf;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v3}, Lekf;->getMaxScrollY()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 324
    iget-object v1, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v1}, Lekf;->getScrollY()I

    move-result v1

    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2}, Lekf;->getMaxScrollY()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v1, v2

    .line 327
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2}, Lekf;->getMaxScrollY()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 328
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2, v1}, Lekf;->setScrollY(I)V

    goto :goto_0

    .line 331
    :cond_2
    const/16 v2, 0x13

    if-ne p1, v2, :cond_3

    .line 332
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2}, Lekf;->getScrollY()I

    move-result v2

    if-lez v2, :cond_3

    .line 333
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2}, Lekf;->getScrollY()I

    move-result v2

    iget-object v3, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v3}, Lekf;->getMaxScrollY()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-int/2addr v2, v3

    .line 335
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 336
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v2, v1}, Lekf;->setScrollY(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 340
    goto :goto_0
.end method

.method public hX(I)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 647
    iget-object v0, p0, Lekb;->ccr:Landroid/view/ViewConfiguration;

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    .line 648
    iget-object v1, p0, Lekb;->ccr:Landroid/view/ViewConfiguration;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    .line 650
    iget-object v2, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    invoke-virtual {v2, v4, v0}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 651
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    float-to-int v0, v0

    .line 653
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v2, v1

    if-lez v1, :cond_1

    move v4, v0

    .line 654
    :goto_0
    if-eqz v4, :cond_0

    .line 655
    iget-object v0, p0, Lekb;->ccD:Lekd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lekd;->a(Landroid/animation/TimeInterpolator;)V

    .line 656
    iget-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    iget-object v1, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v2

    neg-int v4, v4

    iget-object v5, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v5}, Lekf;->getMaxScrollY()I

    move-result v8

    move v5, v3

    move v6, v3

    move v7, v3

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 659
    iget-object v0, p0, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getStartY()I

    move-result v0

    iput v0, p0, Lekb;->ccA:I

    .line 660
    const/4 v0, -0x2

    iput v0, p0, Lekb;->ccE:I

    .line 661
    iget-object v0, p0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    .line 663
    :cond_0
    return-void

    :cond_1
    move v4, v3

    .line 653
    goto :goto_0
.end method

.method public o(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 668
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    .line 670
    :cond_0
    iget-object v0, p0, Lekb;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 671
    return-void
.end method

.method public final onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 286
    iget-boolean v1, p0, Lekb;->oN:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lekb;->mScrollConsumer:Leka;

    if-nez v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    and-int/lit16 v1, v1, 0x2002

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 291
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    iget v2, p0, Lekb;->ccv:I

    if-nez v2, :cond_1

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    iget-object v3, p0, Lekb;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x101004d

    invoke-virtual {v3, v4, v2, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected theme to define listPreferredItemHeight."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v3, p0, Lekb;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lekb;->ccv:I

    :cond_1
    iget v2, p0, Lekb;->ccv:I

    neg-int v2, v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 293
    float-to-int v1, v1

    invoke-direct {p0, v1}, Lekb;->hV(I)I

    move-result v1

    .line 294
    if-eqz v1, :cond_2

    .line 295
    iget-object v2, p0, Lekb;->mScrollViewControl:Lekf;

    iget-object v3, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v3}, Lekf;->getScrollY()I

    move-result v3

    add-int/2addr v1, v3

    invoke-interface {v2, v1}, Lekf;->setScrollY(I)V

    .line 299
    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 172
    iget-boolean v2, p0, Lekb;->oN:Z

    if-nez v2, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 184
    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    if-ne v2, v1, :cond_3

    .line 185
    :cond_2
    invoke-direct {p0, v0}, Lekb;->eX(Z)V

    goto :goto_0

    .line 190
    :cond_3
    iget-boolean v3, p0, Lekb;->ccH:Z

    if-nez v3, :cond_0

    .line 196
    if-eqz v2, :cond_4

    iget-boolean v3, p0, Lekb;->AC:Z

    if-eqz v3, :cond_4

    .line 198
    iget-boolean v0, p0, Lekb;->AC:Z

    goto :goto_0

    .line 201
    :cond_4
    packed-switch v2, :pswitch_data_0

    .line 223
    :cond_5
    :goto_1
    :pswitch_0
    iget-boolean v2, p0, Lekb;->AC:Z

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lekb;->ccG:Z

    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    .line 203
    :pswitch_1
    invoke-direct {p0, p1}, Lekb;->m(Landroid/view/MotionEvent;)V

    .line 204
    invoke-virtual {p0, p1}, Lekb;->o(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 208
    :pswitch_2
    invoke-direct {p0, p1}, Lekb;->n(Landroid/view/MotionEvent;)V

    .line 209
    iget-boolean v2, p0, Lekb;->AC:Z

    if-nez v2, :cond_5

    .line 210
    invoke-direct {p0}, Lekb;->atW()Z

    move-result v2

    iput-boolean v2, p0, Lekb;->AC:Z

    .line 211
    iget-boolean v2, p0, Lekb;->AC:Z

    if-eqz v2, :cond_5

    .line 212
    invoke-virtual {p0}, Lekb;->atT()V

    .line 213
    invoke-virtual {p0, p1}, Lekb;->o(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    iget-boolean v0, p0, Lekb;->oN:Z

    if-nez v0, :cond_1

    .line 282
    :cond_0
    :goto_0
    return v2

    .line 236
    :cond_1
    iget-boolean v3, p0, Lekb;->ccy:Z

    .line 237
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 238
    invoke-virtual {p0, p1}, Lekb;->o(Landroid/view/MotionEvent;)V

    .line 240
    packed-switch v0, :pswitch_data_0

    .line 274
    :cond_2
    :goto_1
    if-eqz v3, :cond_14

    iget-boolean v0, p0, Lekb;->ccy:Z

    if-nez v0, :cond_14

    .line 275
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->atn()V

    :cond_3
    :goto_2
    move v2, v1

    .line 282
    goto :goto_0

    .line 242
    :pswitch_0
    invoke-direct {p0, p1}, Lekb;->m(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 247
    :pswitch_1
    iget-boolean v0, p0, Lekb;->ccH:Z

    if-nez v0, :cond_0

    .line 250
    invoke-direct {p0, p1}, Lekb;->n(Landroid/view/MotionEvent;)V

    .line 251
    iget-boolean v0, p0, Lekb;->AC:Z

    if-nez v0, :cond_4

    .line 252
    invoke-direct {p0}, Lekb;->atW()Z

    move-result v0

    iput-boolean v0, p0, Lekb;->AC:Z

    .line 253
    iget-boolean v0, p0, Lekb;->AC:Z

    if-eqz v0, :cond_4

    .line 254
    invoke-virtual {p0}, Lekb;->atT()V

    .line 257
    :cond_4
    iget-boolean v0, p0, Lekb;->AC:Z

    if-eqz v0, :cond_2

    .line 258
    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lekb;->ccu:I

    if-eqz v0, :cond_d

    iget v0, p0, Lekb;->ccu:I

    if-lez v0, :cond_c

    move v0, v1

    :goto_3
    iget-boolean v4, p0, Lekb;->ccz:Z

    if-ne v0, v4, :cond_d

    iput v2, p0, Lekb;->ccw:I

    :cond_5
    :goto_4
    iget-boolean v0, p0, Lekb;->ccy:Z

    if-nez v0, :cond_a

    iget v0, p0, Lekb;->ccu:I

    invoke-direct {p0, v0}, Lekb;->hV(I)I

    move-result v0

    iget v4, p0, Lekb;->ccu:I

    sub-int/2addr v4, v0

    iput v4, p0, Lekb;->ccw:I

    iget-boolean v4, p0, Lekb;->AC:Z

    if-eqz v4, :cond_6

    iget v4, p0, Lekb;->ccw:I

    if-nez v4, :cond_7

    :cond_6
    invoke-direct {p0}, Lekb;->atW()Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_7
    iput-boolean v1, p0, Lekb;->ccy:Z

    iget v4, p0, Lekb;->ccw:I

    iput v4, p0, Lekb;->ccu:I

    iget v4, p0, Lekb;->ccw:I

    iput v4, p0, Lekb;->ccx:I

    iget v4, p0, Lekb;->ccx:I

    if-lez v4, :cond_8

    move v2, v1

    :cond_8
    iput-boolean v2, p0, Lekb;->ccz:Z

    :cond_9
    move v2, v0

    .line 259
    :cond_a
    if-eqz v2, :cond_b

    .line 260
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    iget-object v4, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v4}, Lekf;->getScrollY()I

    move-result v4

    add-int/2addr v2, v4

    invoke-interface {v0, v2}, Lekf;->setScrollY(I)V

    .line 262
    :cond_b
    invoke-direct {p0}, Lekb;->atX()V

    goto :goto_1

    :cond_c
    move v0, v2

    .line 258
    goto :goto_3

    :cond_d
    invoke-direct {p0}, Lekb;->atW()Z

    move-result v0

    if-eqz v0, :cond_12

    iget v0, p0, Lekb;->ccw:I

    if-lez v0, :cond_e

    move v0, v1

    :goto_5
    if-eqz v0, :cond_10

    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    iget-object v4, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v4}, Lekf;->getMaxScrollY()I

    move-result v4

    if-ge v0, v4, :cond_f

    move v0, v1

    :goto_6
    if-eqz v0, :cond_12

    iput-boolean v2, p0, Lekb;->ccy:Z

    iput v2, p0, Lekb;->ccx:I

    iget v0, p0, Lekb;->ccw:I

    iput v0, p0, Lekb;->ccu:I

    goto :goto_4

    :cond_e
    move v0, v2

    goto :goto_5

    :cond_f
    move v0, v2

    goto :goto_6

    :cond_10
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    if-lez v0, :cond_11

    move v0, v1

    goto :goto_6

    :cond_11
    move v0, v2

    goto :goto_6

    :cond_12
    iget v0, p0, Lekb;->ccx:I

    if-lez v0, :cond_13

    move v0, v1

    :goto_7
    iget-boolean v4, p0, Lekb;->ccz:Z

    if-eq v0, v4, :cond_5

    iput-boolean v2, p0, Lekb;->ccy:Z

    iget v0, p0, Lekb;->ccx:I

    iput v0, p0, Lekb;->ccu:I

    iget v0, p0, Lekb;->ccu:I

    iput v0, p0, Lekb;->ccw:I

    goto/16 :goto_4

    :cond_13
    move v0, v2

    goto :goto_7

    .line 266
    :pswitch_2
    invoke-direct {p0, v1}, Lekb;->eX(Z)V

    goto/16 :goto_1

    .line 269
    :pswitch_3
    invoke-direct {p0, v2}, Lekb;->eX(Z)V

    goto/16 :goto_1

    .line 276
    :cond_14
    if-nez v3, :cond_15

    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_15

    .line 277
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->atm()V

    goto/16 :goto_2

    .line 278
    :cond_15
    iget-boolean v0, p0, Lekb;->ccy:Z

    if-eqz v0, :cond_3

    .line 279
    iget-object v0, p0, Lekb;->mScrollViewControl:Lekf;

    iget v2, p0, Lekb;->ccx:I

    invoke-interface {v0, v2}, Lekf;->hJ(I)V

    goto/16 :goto_2

    .line 240
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
