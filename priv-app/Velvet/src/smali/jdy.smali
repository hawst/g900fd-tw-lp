.class public final Ljdy;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAD:I

.field private dAE:I

.field private dAT:J

.field private dAY:J

.field private edg:Ljava/lang/String;

.field private edh:I

.field public edi:Ljdz;

.field public edj:Ljeb;

.field public edk:Ljea;

.field public edl:Ljec;


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v2, p0, Ljdy;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljdy;->edg:Ljava/lang/String;

    iput v2, p0, Ljdy;->dAD:I

    const/4 v0, 0x1

    iput v0, p0, Ljdy;->dAE:I

    iput-wide v4, p0, Ljdy;->dAY:J

    iput-wide v4, p0, Ljdy;->dAT:J

    iput v2, p0, Ljdy;->edh:I

    iput-object v1, p0, Ljdy;->edi:Ljdz;

    iput-object v1, p0, Ljdy;->edj:Ljeb;

    iput-object v1, p0, Ljdy;->edk:Ljea;

    iput-object v1, p0, Ljdy;->edl:Ljec;

    iput-object v1, p0, Ljdy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdy;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdy;->edg:Ljava/lang/String;

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdy;->dAD:I

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdy;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdy;->dAE:I

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdy;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljdy;->dAY:J

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdy;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljdy;->dAT:J

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdy;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdy;->edh:I

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdy;->aez:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljdy;->edi:Ljdz;

    if-nez v0, :cond_1

    new-instance v0, Ljdz;

    invoke-direct {v0}, Ljdz;-><init>()V

    iput-object v0, p0, Ljdy;->edi:Ljdz;

    :cond_1
    iget-object v0, p0, Ljdy;->edi:Ljdz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljdy;->edj:Ljeb;

    if-nez v0, :cond_2

    new-instance v0, Ljeb;

    invoke-direct {v0}, Ljeb;-><init>()V

    iput-object v0, p0, Ljdy;->edj:Ljeb;

    :cond_2
    iget-object v0, p0, Ljdy;->edj:Ljeb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljdy;->edk:Ljea;

    if-nez v0, :cond_3

    new-instance v0, Ljea;

    invoke-direct {v0}, Ljea;-><init>()V

    iput-object v0, p0, Ljdy;->edk:Ljea;

    :cond_3
    iget-object v0, p0, Ljdy;->edk:Ljea;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljdy;->edl:Ljec;

    if-nez v0, :cond_4

    new-instance v0, Ljec;

    invoke-direct {v0}, Ljec;-><init>()V

    iput-object v0, p0, Ljdy;->edl:Ljec;

    :cond_4
    iget-object v0, p0, Ljdy;->edl:Ljec;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljdy;->edg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljdy;->dAD:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljdy;->dAE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_2
    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-wide v2, p0, Ljdy;->dAY:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_3
    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v2, p0, Ljdy;->dAT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_4
    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Ljdy;->edh:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_5
    iget-object v0, p0, Ljdy;->edi:Ljdz;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Ljdy;->edi:Ljdz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_6
    iget-object v0, p0, Ljdy;->edj:Ljeb;

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Ljdy;->edj:Ljeb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_7
    iget-object v0, p0, Ljdy;->edk:Ljea;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Ljdy;->edk:Ljea;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_8
    iget-object v0, p0, Ljdy;->edl:Ljec;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Ljdy;->edl:Ljec;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final aVJ()I
    .locals 1

    iget v0, p0, Ljdy;->dAE:I

    return v0
.end method

.method public final aVK()Z
    .locals 1

    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aVT()J
    .locals 2

    iget-wide v0, p0, Ljdy;->dAY:J

    return-wide v0
.end method

.method public final aVU()Z
    .locals 1

    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ait()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdy;->edg:Ljava/lang/String;

    return-object v0
.end method

.method public final big()Z
    .locals 1

    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bih()J
    .locals 2

    iget-wide v0, p0, Ljdy;->dAT:J

    return-wide v0
.end method

.method public final bii()Z
    .locals 1

    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bij()I
    .locals 1

    iget v0, p0, Ljdy;->edh:I

    return v0
.end method

.method public final bik()Z
    .locals 1

    iget v0, p0, Ljdy;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cX(J)Ljdy;
    .locals 2

    const-wide/16 v0, 0x4d2

    iput-wide v0, p0, Ljdy;->dAY:J

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdy;->aez:I

    return-object p0
.end method

.method public final cY(J)Ljdy;
    .locals 2

    const-wide v0, 0x142be8d4a80L

    iput-wide v0, p0, Ljdy;->dAT:J

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdy;->aez:I

    return-object p0
.end method

.method public final getFrequency()I
    .locals 1

    iget v0, p0, Ljdy;->dAD:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljdy;->edg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljdy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljdy;->dAD:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljdy;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljdy;->dAE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljdy;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Ljdy;->dAY:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Ljdy;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Ljdy;->dAT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljdy;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Ljdy;->edh:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Ljdy;->edi:Ljdz;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Ljdy;->edi:Ljdz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Ljdy;->edj:Ljeb;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Ljdy;->edj:Ljeb;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Ljdy;->edk:Ljea;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Ljdy;->edk:Ljea;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Ljdy;->edl:Ljec;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Ljdy;->edl:Ljec;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    return v0
.end method

.method public final pa(I)Ljdy;
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Ljdy;->dAE:I

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdy;->aez:I

    return-object p0
.end method

.method public final pb(I)Ljdy;
    .locals 1

    const/16 v0, 0xc

    iput v0, p0, Ljdy;->edh:I

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdy;->aez:I

    return-object p0
.end method

.method public final tU(Ljava/lang/String;)Ljdy;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdy;->edg:Ljava/lang/String;

    iget v0, p0, Ljdy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdy;->aez:I

    return-object p0
.end method
