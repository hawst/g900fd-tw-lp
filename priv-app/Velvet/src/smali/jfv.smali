.class public final Ljfv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eiX:Z

.field private eiY:Z

.field private eiZ:J

.field private eja:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14224
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14225
    iput v2, p0, Ljfv;->aez:I

    iput-boolean v2, p0, Ljfv;->eiX:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljfv;->eiY:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljfv;->eiZ:J

    iput-boolean v2, p0, Ljfv;->eja:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfv;->eCz:I

    .line 14226
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 14129
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfv;->eiX:Z

    iget v0, p0, Ljfv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfv;->eiY:Z

    iget v0, p0, Ljfv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljfv;->eiZ:J

    iget v0, p0, Ljfv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfv;->eja:Z

    iget v0, p0, Ljfv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfv;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 14242
    iget v0, p0, Ljfv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14243
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljfv;->eiX:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14245
    :cond_0
    iget v0, p0, Ljfv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14246
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfv;->eiY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14248
    :cond_1
    iget v0, p0, Ljfv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14249
    const/4 v0, 0x3

    iget-wide v2, p0, Ljfv;->eiZ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 14251
    :cond_2
    iget v0, p0, Ljfv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 14252
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfv;->eja:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14254
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14255
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 14259
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14260
    iget v1, p0, Ljfv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14261
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljfv;->eiX:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14264
    :cond_0
    iget v1, p0, Ljfv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14265
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfv;->eiY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14268
    :cond_1
    iget v1, p0, Ljfv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14269
    const/4 v1, 0x3

    iget-wide v2, p0, Ljfv;->eiZ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 14272
    :cond_2
    iget v1, p0, Ljfv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 14273
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfv;->eja:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14276
    :cond_3
    return v0
.end method
