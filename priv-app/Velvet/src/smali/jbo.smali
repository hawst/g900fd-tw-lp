.class public final Ljbo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dYQ:Z

.field private dYR:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20904
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 20905
    iput v1, p0, Ljbo;->aez:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbo;->dYQ:Z

    iput-boolean v1, p0, Ljbo;->dYR:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljbo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbo;->eCz:I

    .line 20906
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 20847
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbo;->dYQ:Z

    iget v0, p0, Ljbo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbo;->dYR:Z

    iget v0, p0, Ljbo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 20920
    iget v0, p0, Ljbo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 20921
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljbo;->dYQ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 20923
    :cond_0
    iget v0, p0, Ljbo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 20924
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljbo;->dYR:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 20926
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 20927
    return-void
.end method

.method public final getEnabled()Z
    .locals 1

    .prologue
    .line 20869
    iget-boolean v0, p0, Ljbo;->dYQ:Z

    return v0
.end method

.method public final hD(Z)Ljbo;
    .locals 1

    .prologue
    .line 20872
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljbo;->dYQ:Z

    .line 20873
    iget v0, p0, Ljbo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbo;->aez:I

    .line 20874
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 20931
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 20932
    iget v1, p0, Ljbo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 20933
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljbo;->dYQ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20936
    :cond_0
    iget v1, p0, Ljbo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 20937
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljbo;->dYR:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20940
    :cond_1
    return v0
.end method
