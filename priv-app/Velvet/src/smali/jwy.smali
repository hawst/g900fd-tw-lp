.class public final Ljwy;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJP:Ljsm;


# instance fields
.field private aez:I

.field private eHd:Ljava/lang/String;

.field public eJQ:[Ljws;

.field public eJR:Ljwt;

.field public eJS:[Ljwt;

.field public eJT:Ljwt;

.field private eJU:Ljava/lang/String;

.field private eJV:Ljava/lang/String;

.field private eJW:Ljava/lang/String;

.field private evV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 863
    const/16 v0, 0xb

    const-class v1, Ljwy;

    const v2, 0x23c442

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwy;->eJP:Ljsm;

    .line 869
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 995
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 996
    const/4 v0, 0x0

    iput v0, p0, Ljwy;->aez:I

    invoke-static {}, Ljws;->bvv()[Ljws;

    move-result-object v0

    iput-object v0, p0, Ljwy;->eJQ:[Ljws;

    iput-object v1, p0, Ljwy;->eJR:Ljwt;

    invoke-static {}, Ljwt;->bvy()[Ljwt;

    move-result-object v0

    iput-object v0, p0, Ljwy;->eJS:[Ljwt;

    iput-object v1, p0, Ljwy;->eJT:Ljwt;

    const-string v0, ""

    iput-object v0, p0, Ljwy;->eHd:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwy;->eJU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwy;->eJV:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwy;->eJW:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwy;->evV:Z

    iput-object v1, p0, Ljwy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwy;->eCz:I

    .line 997
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 856
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljwy;->eJR:Ljwt;

    if-nez v0, :cond_1

    new-instance v0, Ljwt;

    invoke-direct {v0}, Ljwt;-><init>()V

    iput-object v0, p0, Ljwy;->eJR:Ljwt;

    :cond_1
    iget-object v0, p0, Ljwy;->eJR:Ljwt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljwy;->eJT:Ljwt;

    if-nez v0, :cond_2

    new-instance v0, Ljwt;

    invoke-direct {v0}, Ljwt;-><init>()V

    iput-object v0, p0, Ljwy;->eJT:Ljwt;

    :cond_2
    iget-object v0, p0, Ljwy;->eJT:Ljwt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwy;->eJS:[Ljwt;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljwt;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljwy;->eJS:[Ljwt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljwt;

    invoke-direct {v3}, Ljwt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljwy;->eJS:[Ljwt;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljwt;

    invoke-direct {v3}, Ljwt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwy;->eJS:[Ljwt;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwy;->eHd:Ljava/lang/String;

    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwy;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwy;->eJV:Ljava/lang/String;

    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwy;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwy;->eJW:Ljava/lang/String;

    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwy;->aez:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwy;->eJQ:[Ljws;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljws;

    if-eqz v0, :cond_6

    iget-object v3, p0, Ljwy;->eJQ:[Ljws;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Ljws;

    invoke-direct {v3}, Ljws;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ljwy;->eJQ:[Ljws;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Ljws;

    invoke-direct {v3}, Ljws;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwy;->eJQ:[Ljws;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwy;->eJU:Ljava/lang/String;

    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwy;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwy;->evV:Z

    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwy;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x62 -> :sswitch_8
        0x68 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1018
    iget-object v0, p0, Ljwy;->eJR:Ljwt;

    if-eqz v0, :cond_0

    .line 1019
    const/4 v0, 0x2

    iget-object v2, p0, Ljwy;->eJR:Ljwt;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 1021
    :cond_0
    iget-object v0, p0, Ljwy;->eJT:Ljwt;

    if-eqz v0, :cond_1

    .line 1022
    const/4 v0, 0x3

    iget-object v2, p0, Ljwy;->eJT:Ljwt;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 1024
    :cond_1
    iget-object v0, p0, Ljwy;->eJS:[Ljwt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljwy;->eJS:[Ljwt;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 1025
    :goto_0
    iget-object v2, p0, Ljwy;->eJS:[Ljwt;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1026
    iget-object v2, p0, Ljwy;->eJS:[Ljwt;

    aget-object v2, v2, v0

    .line 1027
    if-eqz v2, :cond_2

    .line 1028
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1025
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1032
    :cond_3
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 1033
    const/4 v0, 0x5

    iget-object v2, p0, Ljwy;->eHd:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1035
    :cond_4
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 1036
    const/4 v0, 0x6

    iget-object v2, p0, Ljwy;->eJV:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1038
    :cond_5
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 1039
    const/16 v0, 0x8

    iget-object v2, p0, Ljwy;->eJW:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1041
    :cond_6
    iget-object v0, p0, Ljwy;->eJQ:[Ljws;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljwy;->eJQ:[Ljws;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 1042
    :goto_1
    iget-object v0, p0, Ljwy;->eJQ:[Ljws;

    array-length v0, v0

    if-ge v1, v0, :cond_8

    .line 1043
    iget-object v0, p0, Ljwy;->eJQ:[Ljws;

    aget-object v0, v0, v1

    .line 1044
    if-eqz v0, :cond_7

    .line 1045
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 1042
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1049
    :cond_8
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_9

    .line 1050
    const/16 v0, 0xc

    iget-object v1, p0, Ljwy;->eJU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1052
    :cond_9
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_a

    .line 1053
    const/16 v0, 0xd

    iget-boolean v1, p0, Ljwy;->evV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1055
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1056
    return-void
.end method

.method public final aFb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Ljwy;->eHd:Ljava/lang/String;

    return-object v0
.end method

.method public final aHg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Ljwy;->eJW:Ljava/lang/String;

    return-object v0
.end method

.method public final bvD()Z
    .locals 1

    .prologue
    .line 968
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bvE()Z
    .locals 1

    .prologue
    .line 979
    iget-boolean v0, p0, Ljwy;->evV:Z

    return v0
.end method

.method public final bvF()Z
    .locals 1

    .prologue
    .line 987
    iget v0, p0, Ljwy;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jm(Z)Ljwy;
    .locals 1

    .prologue
    .line 982
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljwy;->evV:Z

    .line 983
    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwy;->aez:I

    .line 984
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1060
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1061
    iget-object v2, p0, Ljwy;->eJR:Ljwt;

    if-eqz v2, :cond_0

    .line 1062
    const/4 v2, 0x2

    iget-object v3, p0, Ljwy;->eJR:Ljwt;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1065
    :cond_0
    iget-object v2, p0, Ljwy;->eJT:Ljwt;

    if-eqz v2, :cond_1

    .line 1066
    const/4 v2, 0x3

    iget-object v3, p0, Ljwy;->eJT:Ljwt;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1069
    :cond_1
    iget-object v2, p0, Ljwy;->eJS:[Ljwt;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljwy;->eJS:[Ljwt;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 1070
    :goto_0
    iget-object v3, p0, Ljwy;->eJS:[Ljwt;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 1071
    iget-object v3, p0, Ljwy;->eJS:[Ljwt;

    aget-object v3, v3, v0

    .line 1072
    if-eqz v3, :cond_2

    .line 1073
    const/4 v4, 0x4

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1070
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1078
    :cond_4
    iget v2, p0, Ljwy;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    .line 1079
    const/4 v2, 0x5

    iget-object v3, p0, Ljwy;->eHd:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1082
    :cond_5
    iget v2, p0, Ljwy;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_6

    .line 1083
    const/4 v2, 0x6

    iget-object v3, p0, Ljwy;->eJV:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1086
    :cond_6
    iget v2, p0, Ljwy;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_7

    .line 1087
    const/16 v2, 0x8

    iget-object v3, p0, Ljwy;->eJW:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1090
    :cond_7
    iget-object v2, p0, Ljwy;->eJQ:[Ljws;

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljwy;->eJQ:[Ljws;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 1091
    :goto_1
    iget-object v2, p0, Ljwy;->eJQ:[Ljws;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 1092
    iget-object v2, p0, Ljwy;->eJQ:[Ljws;

    aget-object v2, v2, v1

    .line 1093
    if-eqz v2, :cond_8

    .line 1094
    const/16 v3, 0x9

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1091
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1099
    :cond_9
    iget v1, p0, Ljwy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_a

    .line 1100
    const/16 v1, 0xc

    iget-object v2, p0, Ljwy;->eJU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1103
    :cond_a
    iget v1, p0, Ljwy;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_b

    .line 1104
    const/16 v1, 0xd

    iget-boolean v2, p0, Ljwy;->evV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1107
    :cond_b
    return v0
.end method

.method public final zJ(Ljava/lang/String;)Ljwy;
    .locals 1

    .prologue
    .line 894
    if-nez p1, :cond_0

    .line 895
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 897
    :cond_0
    iput-object p1, p0, Ljwy;->eHd:Ljava/lang/String;

    .line 898
    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwy;->aez:I

    .line 899
    return-object p0
.end method

.method public final zK(Ljava/lang/String;)Ljwy;
    .locals 1

    .prologue
    .line 960
    if-nez p1, :cond_0

    .line 961
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 963
    :cond_0
    iput-object p1, p0, Ljwy;->eJW:Ljava/lang/String;

    .line 964
    iget v0, p0, Ljwy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwy;->aez:I

    .line 965
    return-object p0
.end method
