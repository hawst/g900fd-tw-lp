.class public final Lacv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static RM:Z

.field private static RN:Lacv;


# instance fields
.field private final RO:Ljava/lang/String;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-boolean v0, Lacv;->RM:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lacv;->RO:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lacv;->mResources:Landroid/content/res/Resources;

    .line 79
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/pm/PackageManager;)Lacv;
    .locals 4

    .prologue
    .line 63
    const-class v2, Lacv;

    monitor-enter v2

    :try_start_0
    sget-boolean v0, Lacv;->RM:Z

    if-nez v0, :cond_1

    .line 64
    const-string v0, "com.android.launcher3.action.PARTNER_CUSTOMIZATION"

    invoke-static {v0, p0}, Ladp;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Landroid/util/Pair;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_0

    .line 66
    new-instance v3, Lacv;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1}, Lacv;-><init>(Ljava/lang/String;Landroid/content/res/Resources;)V

    sput-object v3, Lacv;->RN:Lacv;

    .line 68
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lacv;->RM:Z

    .line 70
    :cond_1
    sget-object v0, Lacv;->RN:Lacv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final a(Landroid/util/DisplayMetrics;)Ltu;
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v0, 0x1

    .line 120
    const/4 v1, 0x0

    .line 122
    new-instance v2, Ltu;

    invoke-direct {v2}, Ltu;-><init>()V

    .line 125
    iput v3, v2, Ltu;->Dg:F

    .line 126
    iput v3, v2, Ltu;->Dh:F

    .line 127
    iput v4, v2, Ltu;->Ei:I

    .line 128
    iput v4, v2, Ltu;->Ej:I

    .line 131
    :try_start_0
    iget-object v3, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v4, "grid_num_rows"

    const-string v5, "integer"

    iget-object v6, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 133
    if-lez v3, :cond_0

    .line 135
    :try_start_1
    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v2, Ltu;->Dg:F
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move v1, v0

    .line 138
    :cond_0
    :try_start_2
    iget-object v3, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v4, "grid_num_columns"

    const-string v5, "integer"

    iget-object v6, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v3

    .line 140
    if-lez v3, :cond_1

    .line 142
    :try_start_3
    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v2, Ltu;->Dh:F
    :try_end_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    move v1, v0

    .line 145
    :cond_1
    :try_start_4
    iget-object v3, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v4, "grid_aa_short_edge_count"

    const-string v5, "integer"

    iget-object v6, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v3

    .line 147
    if-lez v3, :cond_2

    .line 149
    :try_start_5
    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, v2, Ltu;->Ei:I
    :try_end_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_1

    move v1, v0

    .line 152
    :cond_2
    :try_start_6
    iget-object v3, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v4, "grid_aa_long_edge_count"

    const-string v5, "integer"

    iget-object v6, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6 .. :try_end_6} :catch_0

    move-result v3

    .line 154
    if-lez v3, :cond_3

    .line 156
    :try_start_7
    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, v2, Ltu;->Ej:I
    :try_end_7
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7 .. :try_end_7} :catch_1

    move v1, v0

    .line 159
    :cond_3
    :try_start_8
    iget-object v3, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v4, "grid_icon_size_dp"

    const-string v5, "dimen"

    iget-object v6, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_8 .. :try_end_8} :catch_0

    move-result v3

    .line 161
    if-lez v3, :cond_4

    .line 163
    :try_start_9
    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 164
    invoke-static {v1, p1}, Lur;->a(ILandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, v2, Ltu;->Dj:F
    :try_end_9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_9 .. :try_end_9} :catch_1

    .line 169
    :goto_0
    if-eqz v0, :cond_5

    move-object v0, v2

    :goto_1
    return-object v0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    :goto_2
    const-string v3, "Launcher.Partner"

    const-string v4, "Invalid Partner grid resource!"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    move v0, v1

    goto :goto_0

    .line 169
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 166
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_2
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lacv;->RO:Ljava/lang/String;

    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lacv;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public final jR()Z
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v1, "partner_default_layout"

    const-string v2, "xml"

    iget-object v3, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 92
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jS()Z
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v1, "default_wallpapper_hidden"

    const-string v2, "bool"

    iget-object v3, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 104
    if-eqz v0, :cond_0

    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jT()Ljava/io/File;
    .locals 4

    .prologue
    .line 108
    iget-object v0, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v1, "system_wallpaper_directory"

    const-string v2, "string"

    iget-object v3, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 110
    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jU()Z
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lacv;->mResources:Landroid/content/res/Resources;

    const-string v1, "requires_first_run_flow"

    const-string v2, "bool"

    iget-object v3, p0, Lacv;->RO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 116
    if-eqz v0, :cond_0

    iget-object v1, p0, Lacv;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
