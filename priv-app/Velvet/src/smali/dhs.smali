.class public final Ldhs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bya:Ljava/util/Set;

.field private final byb:Ljava/util/Set;

.field private byc:I

.field byd:Z

.field private bye:I

.field private mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method private constructor <init>(Lcom/google/android/shared/search/Query;II)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldhs;->bya:Ljava/util/Set;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldhs;->byb:Ljava/util/Set;

    .line 76
    iput-object p1, p0, Ldhs;->mQuery:Lcom/google/android/shared/search/Query;

    .line 77
    iput p2, p0, Ldhs;->byc:I

    .line 78
    iput p3, p0, Ldhs;->bye:I

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldhs;->byd:Z

    .line 80
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/shared/search/Query;IIB)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Ldhs;-><init>(Lcom/google/android/shared/search/Query;II)V

    return-void
.end method


# virtual methods
.method public final abZ()Ldhr;
    .locals 8

    .prologue
    .line 110
    new-instance v0, Ldhr;

    iget-object v1, p0, Ldhs;->mQuery:Lcom/google/android/shared/search/Query;

    iget v2, p0, Ldhs;->byc:I

    iget-boolean v3, p0, Ldhs;->byd:Z

    iget v4, p0, Ldhs;->bye:I

    iget-object v5, p0, Ldhs;->bya:Ljava/util/Set;

    iget-object v6, p0, Ldhs;->byb:Ljava/util/Set;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Ldhr;-><init>(Lcom/google/android/shared/search/Query;IZILjava/util/Set;Ljava/util/Set;B)V

    return-object v0
.end method

.method public final ap(Ljava/lang/String;Ljava/lang/String;)Ldhs;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Ldhs;->bya:Ljava/util/Set;

    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    return-object p0
.end method

.method public final aq(Ljava/lang/String;Ljava/lang/String;)Ldhs;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Ldhs;->byb:Ljava/util/Set;

    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    return-object p0
.end method
