.class public Lcdc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final aSm:Lcdf;

.field private final aSn:Lcdr;

.field private final aSo:Lgmj;

.field private final aSp:Lcdz;

.field private final aSq:Z

.field private final aSr:Ljava/util/concurrent/ScheduledExecutorService;

.field private final aSs:Lcdo;

.field private aSt:Lcdn;

.field private aSu:Lcdt;

.field private cq:Z

.field private final mClock:Lemp;

.field private final mRetryPolicy:Lcdp;


# direct methods
.method public constructor <init>(Lcdf;Lcdr;Lgmj;Lcdp;Lcdz;Lemp;Z)V
    .locals 3
    .param p4    # Lcdp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcdz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lemp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcdd;

    invoke-direct {v0, p0}, Lcdd;-><init>(Lcdc;)V

    iput-object v0, p0, Lcdc;->aSs:Lcdo;

    .line 102
    if-eqz p5, :cond_0

    if-eqz p6, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Clock must not be null if TimeoutPolicy.Factory is provided"

    invoke-static {v0, v2}, Lifv;->c(ZLjava/lang/Object;)V

    .line 104
    iput-object p1, p0, Lcdc;->aSm:Lcdf;

    .line 105
    iput-object p2, p0, Lcdc;->aSn:Lcdr;

    .line 106
    iput-object p3, p0, Lcdc;->aSo:Lgmj;

    .line 107
    if-nez p4, :cond_1

    new-instance p4, Lcdg;

    invoke-direct {p4}, Lcdg;-><init>()V

    :cond_1
    iput-object p4, p0, Lcdc;->mRetryPolicy:Lcdp;

    .line 108
    iput-object p5, p0, Lcdc;->aSp:Lcdz;

    .line 109
    iput-object p6, p0, Lcdc;->mClock:Lemp;

    .line 110
    iput-boolean p7, p0, Lcdc;->aSq:Z

    .line 111
    const-string v0, "NetworkRunnerTimeout"

    invoke-static {v0, v1}, Lemv;->q(Ljava/lang/String;Z)Lemw;

    move-result-object v0

    iput-object v0, p0, Lcdc;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    .line 113
    return-void

    :cond_2
    move v0, v1

    .line 102
    goto :goto_0
.end method

.method private Db()V
    .locals 7

    .prologue
    .line 163
    new-instance v0, Lcdn;

    iget-object v1, p0, Lcdc;->aSm:Lcdf;

    iget-object v2, p0, Lcdc;->mRetryPolicy:Lcdp;

    iget-object v3, p0, Lcdc;->aSs:Lcdo;

    invoke-direct {v0, v1, v2, v3}, Lcdn;-><init>(Lcdf;Lcdp;Lcdo;)V

    iput-object v0, p0, Lcdc;->aSt:Lcdn;

    .line 164
    iget-object v0, p0, Lcdc;->aSp:Lcdz;

    if-eqz v0, :cond_0

    .line 166
    new-instance v4, Lcea;

    iget-object v0, p0, Lcdc;->mClock:Lemp;

    iget-object v1, p0, Lcdc;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v2, p0, Lcdc;->aSt:Lcdn;

    iget-object v3, p0, Lcdc;->aSp:Lcdz;

    invoke-interface {v3}, Lcdz;->Dj()Lcdy;

    move-result-object v3

    invoke-direct {v4, v0, v1, v2, v3}, Lcea;-><init>(Lemp;Ljava/util/concurrent/ScheduledExecutorService;Lcdf;Lcdy;)V

    .line 172
    :goto_0
    new-instance v0, Lcdt;

    iget-object v1, p0, Lcdc;->aSt:Lcdn;

    iget-object v2, p0, Lcdc;->aSo:Lgmj;

    iget-object v3, p0, Lcdc;->aSn:Lcdr;

    iget-boolean v5, p0, Lcdc;->aSq:Z

    invoke-virtual {p0}, Lcdc;->Da()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcdt;-><init>(Lcdf;Lgmj;Lcdr;Lcdw;ZLjava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcdc;->aSu:Lcdt;

    .line 175
    iget-object v0, p0, Lcdc;->aSu:Lcdt;

    invoke-virtual {v0}, Lcdt;->start()V

    .line 176
    return-void

    .line 169
    :cond_0
    new-instance v4, Lcda;

    invoke-direct {v4}, Lcda;-><init>()V

    goto :goto_0
.end method

.method private Dc()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lcdc;->aSt:Lcdn;

    invoke-virtual {v0}, Lcdn;->invalidate()V

    .line 181
    iput-object v1, p0, Lcdc;->aSt:Lcdn;

    .line 182
    iget-object v0, p0, Lcdc;->aSu:Lcdt;

    invoke-virtual {v0}, Lcdt;->close()V

    .line 183
    iput-object v1, p0, Lcdc;->aSu:Lcdt;

    .line 184
    return-void
.end method

.method static synthetic a(Lcdc;Z)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcdc;->ce(Z)V

    return-void
.end method

.method private declared-synchronized ce(Z)V
    .locals 4

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcdc;->cq:Z

    if-nez v0, :cond_0

    .line 151
    const-string v0, "NetworkRecognitionRnr"

    const-string v1, "Can\'t retry, session already closed."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 154
    :cond_0
    invoke-direct {p0}, Lcdc;->Dc()V

    .line 155
    if-eqz p1, :cond_1

    .line 156
    iget-object v0, p0, Lcdc;->aSo:Lgmj;

    invoke-interface {v0}, Lgmj;->refresh()V

    .line 158
    :cond_1
    invoke-direct {p0}, Lcdc;->Db()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    monitor-exit p0

    return-void

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected Da()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 118
    const-string v0, "NetworkRunner"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lemv;->q(Ljava/lang/String;Z)Lemw;

    move-result-object v0

    return-object v0
.end method

.method public final Dd()Lcdf;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcdc;->aSt:Lcdn;

    return-object v0
.end method

.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcdc;->cq:Z

    const-string v1, "Call to close without start."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdc;->cq:Z

    .line 139
    invoke-direct {p0}, Lcdc;->Dc()V

    .line 140
    iget-object v0, p0, Lcdc;->aSo:Lgmj;

    invoke-interface {v0}, Lgmj;->close()V

    .line 141
    iget-object v0, p0, Lcdc;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    monitor-exit p0

    return-void

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized start()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 126
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcdc;->cq:Z

    if-nez v1, :cond_0

    :goto_0
    const-string v1, "Duplicate call to start."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcdc;->cq:Z

    .line 128
    invoke-direct {p0}, Lcdc;->Db()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    monitor-exit p0

    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
