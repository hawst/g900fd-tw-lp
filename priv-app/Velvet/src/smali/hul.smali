.class public final Lhul;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lhul;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lhul;->mViewGroup:Landroid/view/ViewGroup;

    .line 25
    return-void
.end method

.method private addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lhul;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 75
    iget-object v0, p0, Lhul;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 76
    return-void
.end method


# virtual methods
.method public final aSc()Lhtv;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lhtw;

    iget-object v1, p0, Lhul;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhtw;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-direct {p0, v0}, Lhul;->addView(Landroid/view/View;)V

    .line 32
    return-object v0
.end method

.method public final aSd()Lhto;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lhtp;

    iget-object v1, p0, Lhul;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhtp;-><init>(Landroid/content/Context;)V

    .line 39
    invoke-direct {p0, v0}, Lhul;->addView(Landroid/view/View;)V

    .line 40
    return-object v0
.end method

.method public final aSe()Lhto;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lhtz;

    iget-object v1, p0, Lhul;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhtz;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-direct {p0, v0}, Lhul;->addView(Landroid/view/View;)V

    .line 48
    return-object v0
.end method

.method public final aSf()Lhui;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lhuj;

    iget-object v1, p0, Lhul;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhuj;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-direct {p0, v0}, Lhul;->addView(Landroid/view/View;)V

    .line 56
    return-object v0
.end method

.method public final aSg()Lhss;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lhst;

    iget-object v1, p0, Lhul;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhst;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-direct {p0, v0}, Lhul;->addView(Landroid/view/View;)V

    .line 63
    return-object v0
.end method

.method public final aSh()Lhtf;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lhtg;

    iget-object v1, p0, Lhul;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhtg;-><init>(Landroid/content/Context;)V

    .line 69
    invoke-direct {p0, v0}, Lhul;->addView(Landroid/view/View;)V

    .line 70
    return-object v0
.end method
