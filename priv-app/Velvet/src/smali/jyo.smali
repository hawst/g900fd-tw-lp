.class public final Ljyo;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eLP:[Ljyo;


# instance fields
.field private aez:I

.field public eLQ:Ljyq;

.field private eLR:J

.field private eLS:J

.field private eLT:J

.field private eLU:[Liun;

.field public eLV:[Ljyc;

.field public eLW:Ljyb;

.field public eLX:Ljyp;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 268
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 269
    const/4 v0, 0x0

    iput v0, p0, Ljyo;->aez:I

    iput-object v1, p0, Ljyo;->eLQ:Ljyq;

    iput-wide v2, p0, Ljyo;->eLR:J

    iput-wide v2, p0, Ljyo;->eLS:J

    iput-wide v2, p0, Ljyo;->eLT:J

    invoke-static {}, Liun;->aZe()[Liun;

    move-result-object v0

    iput-object v0, p0, Ljyo;->eLU:[Liun;

    invoke-static {}, Ljyc;->bwb()[Ljyc;

    move-result-object v0

    iput-object v0, p0, Ljyo;->eLV:[Ljyc;

    iput-object v1, p0, Ljyo;->eLW:Ljyb;

    iput-object v1, p0, Ljyo;->eLX:Ljyp;

    iput-object v1, p0, Ljyo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyo;->eCz:I

    .line 270
    return-void
.end method

.method public static bwi()[Ljyo;
    .locals 2

    .prologue
    .line 183
    sget-object v0, Ljyo;->eLP:[Ljyo;

    if-nez v0, :cond_1

    .line 184
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 186
    :try_start_0
    sget-object v0, Ljyo;->eLP:[Ljyo;

    if-nez v0, :cond_0

    .line 187
    const/4 v0, 0x0

    new-array v0, v0, [Ljyo;

    sput-object v0, Ljyo;->eLP:[Ljyo;

    .line 189
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :cond_1
    sget-object v0, Ljyo;->eLP:[Ljyo;

    return-object v0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljyo;->eLQ:Ljyq;

    if-nez v0, :cond_1

    new-instance v0, Ljyq;

    invoke-direct {v0}, Ljyq;-><init>()V

    iput-object v0, p0, Ljyo;->eLQ:Ljyq;

    :cond_1
    iget-object v0, p0, Ljyo;->eLQ:Ljyq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljyo;->eLR:J

    iget v0, p0, Ljyo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljyo;->eLS:J

    iget v0, p0, Ljyo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyo;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyo;->eLU:[Liun;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liun;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljyo;->eLU:[Liun;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Liun;

    invoke-direct {v3}, Liun;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljyo;->eLU:[Liun;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Liun;

    invoke-direct {v3}, Liun;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljyo;->eLU:[Liun;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljyo;->eLX:Ljyp;

    if-nez v0, :cond_5

    new-instance v0, Ljyp;

    invoke-direct {v0}, Ljyp;-><init>()V

    iput-object v0, p0, Ljyo;->eLX:Ljyp;

    :cond_5
    iget-object v0, p0, Ljyo;->eLX:Ljyp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyo;->eLV:[Ljyc;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljyc;

    if-eqz v0, :cond_6

    iget-object v3, p0, Ljyo;->eLV:[Ljyc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Ljyc;

    invoke-direct {v3}, Ljyc;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ljyo;->eLV:[Ljyc;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Ljyc;

    invoke-direct {v3}, Ljyc;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljyo;->eLV:[Ljyc;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljyo;->eLT:J

    iget v0, p0, Ljyo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyo;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljyo;->eLW:Ljyb;

    if-nez v0, :cond_9

    new-instance v0, Ljyb;

    invoke-direct {v0}, Ljyb;-><init>()V

    iput-object v0, p0, Ljyo;->eLW:Ljyb;

    :cond_9
    iget-object v0, p0, Ljyo;->eLW:Ljyb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 290
    iget-object v0, p0, Ljyo;->eLQ:Ljyq;

    if-eqz v0, :cond_0

    .line 291
    const/4 v0, 0x1

    iget-object v2, p0, Ljyo;->eLQ:Ljyq;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 293
    :cond_0
    iget v0, p0, Ljyo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 294
    const/4 v0, 0x2

    iget-wide v2, p0, Ljyo;->eLR:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 296
    :cond_1
    iget v0, p0, Ljyo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 297
    const/4 v0, 0x3

    iget-wide v2, p0, Ljyo;->eLS:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 299
    :cond_2
    iget-object v0, p0, Ljyo;->eLU:[Liun;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljyo;->eLU:[Liun;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 300
    :goto_0
    iget-object v2, p0, Ljyo;->eLU:[Liun;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 301
    iget-object v2, p0, Ljyo;->eLU:[Liun;

    aget-object v2, v2, v0

    .line 302
    if-eqz v2, :cond_3

    .line 303
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 300
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_4
    iget-object v0, p0, Ljyo;->eLX:Ljyp;

    if-eqz v0, :cond_5

    .line 308
    const/4 v0, 0x5

    iget-object v2, p0, Ljyo;->eLX:Ljyp;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 310
    :cond_5
    iget-object v0, p0, Ljyo;->eLV:[Ljyc;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljyo;->eLV:[Ljyc;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 311
    :goto_1
    iget-object v0, p0, Ljyo;->eLV:[Ljyc;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 312
    iget-object v0, p0, Ljyo;->eLV:[Ljyc;

    aget-object v0, v0, v1

    .line 313
    if-eqz v0, :cond_6

    .line 314
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 311
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 318
    :cond_7
    iget v0, p0, Ljyo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 319
    const/4 v0, 0x7

    iget-wide v2, p0, Ljyo;->eLT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 321
    :cond_8
    iget-object v0, p0, Ljyo;->eLW:Ljyb;

    if-eqz v0, :cond_9

    .line 322
    const/16 v0, 0x8

    iget-object v1, p0, Ljyo;->eLW:Ljyb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 324
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 325
    return-void
.end method

.method public final bwj()J
    .locals 2

    .prologue
    .line 202
    iget-wide v0, p0, Ljyo;->eLR:J

    return-wide v0
.end method

.method public final bwk()J
    .locals 2

    .prologue
    .line 221
    iget-wide v0, p0, Ljyo;->eLS:J

    return-wide v0
.end method

.method public final bwl()J
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Ljyo;->eLT:J

    return-wide v0
.end method

.method public final bwm()Z
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Ljyo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dQ(J)Ljyo;
    .locals 1

    .prologue
    .line 205
    iput-wide p1, p0, Ljyo;->eLR:J

    .line 206
    iget v0, p0, Ljyo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyo;->aez:I

    .line 207
    return-object p0
.end method

.method public final dR(J)Ljyo;
    .locals 1

    .prologue
    .line 224
    iput-wide p1, p0, Ljyo;->eLS:J

    .line 225
    iget v0, p0, Ljyo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyo;->aez:I

    .line 226
    return-object p0
.end method

.method public final dS(J)Ljyo;
    .locals 1

    .prologue
    .line 243
    iput-wide p1, p0, Ljyo;->eLT:J

    .line 244
    iget v0, p0, Ljyo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyo;->aez:I

    .line 245
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 330
    iget-object v2, p0, Ljyo;->eLQ:Ljyq;

    if-eqz v2, :cond_0

    .line 331
    const/4 v2, 0x1

    iget-object v3, p0, Ljyo;->eLQ:Ljyq;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 334
    :cond_0
    iget v2, p0, Ljyo;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 335
    const/4 v2, 0x2

    iget-wide v4, p0, Ljyo;->eLR:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 338
    :cond_1
    iget v2, p0, Ljyo;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    .line 339
    const/4 v2, 0x3

    iget-wide v4, p0, Ljyo;->eLS:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 342
    :cond_2
    iget-object v2, p0, Ljyo;->eLU:[Liun;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljyo;->eLU:[Liun;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 343
    :goto_0
    iget-object v3, p0, Ljyo;->eLU:[Liun;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 344
    iget-object v3, p0, Ljyo;->eLU:[Liun;

    aget-object v3, v3, v0

    .line 345
    if-eqz v3, :cond_3

    .line 346
    const/4 v4, 0x4

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 343
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 351
    :cond_5
    iget-object v2, p0, Ljyo;->eLX:Ljyp;

    if-eqz v2, :cond_6

    .line 352
    const/4 v2, 0x5

    iget-object v3, p0, Ljyo;->eLX:Ljyp;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 355
    :cond_6
    iget-object v2, p0, Ljyo;->eLV:[Ljyc;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljyo;->eLV:[Ljyc;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 356
    :goto_1
    iget-object v2, p0, Ljyo;->eLV:[Ljyc;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 357
    iget-object v2, p0, Ljyo;->eLV:[Ljyc;

    aget-object v2, v2, v1

    .line 358
    if-eqz v2, :cond_7

    .line 359
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 356
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 364
    :cond_8
    iget v1, p0, Ljyo;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_9

    .line 365
    const/4 v1, 0x7

    iget-wide v2, p0, Ljyo;->eLT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 368
    :cond_9
    iget-object v1, p0, Ljyo;->eLW:Ljyb;

    if-eqz v1, :cond_a

    .line 369
    const/16 v1, 0x8

    iget-object v2, p0, Ljyo;->eLW:Ljyb;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    :cond_a
    return v0
.end method
