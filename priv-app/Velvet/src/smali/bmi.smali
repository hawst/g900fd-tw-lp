.class public interface abstract Lbmi;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;Lbmf;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Landroid/location/Location;I)V
.end method

.method public abstract a(Lbmf;Ljava/lang/String;)V
.end method

.method public abstract a(Lbro;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nj;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nj;Lbro;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nv;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lbro;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lbro;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/internal/nv;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/internal/nv;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/internal/nv;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;Lbmf;Ljava/lang/String;)V
.end method

.method public abstract a([Ljava/lang/String;Lbmf;Ljava/lang/String;)V
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/nv;Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/gms/internal/nv;Lbmw;)V
.end method

.method public abstract bI(Z)V
.end method

.method public abstract c(Landroid/app/PendingIntent;)V
.end method

.method public abstract fB(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract fC(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
.end method

.method public abstract zE()Landroid/location/Location;
.end method

.method public abstract zF()Landroid/os/IBinder;
.end method

.method public abstract zG()Landroid/os/IBinder;
.end method
