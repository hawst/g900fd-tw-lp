.class public final Lcea;
.super Lepm;
.source "PG"

# interfaces
.implements Lcdw;


# instance fields
.field private final aSB:Lcdf;

.field private final aSr:Ljava/util/concurrent/ScheduledExecutorService;

.field private final aTg:Lcdy;

.field private aTh:J

.field private aTi:J

.field private aTj:J

.field private aTk:Z

.field private aTl:Z

.field private aTm:Z

.field private aTn:Z

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lemp;Ljava/util/concurrent/ScheduledExecutorService;Lcdf;Lcdy;)V
    .locals 2

    .prologue
    .line 46
    const-string v0, "Connection timeout"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 47
    iput-object p1, p0, Lcea;->mClock:Lemp;

    .line 48
    iput-object p2, p0, Lcea;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    .line 49
    iput-object p3, p0, Lcea;->aSB:Lcdf;

    .line 50
    iput-object p4, p0, Lcea;->aTg:Lcdy;

    .line 51
    return-void

    .line 46
    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method


# virtual methods
.method public final declared-synchronized CW()V
    .locals 2

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcea;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcea;->aTh:J

    .line 56
    iget-wide v0, p0, Lcea;->aTh:J

    iput-wide v0, p0, Lcea;->aTi:J

    .line 57
    iget-object v0, p0, Lcea;->aTg:Lcdy;

    invoke-interface {v0}, Lcdy;->Dg()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcea;->aTj:J

    .line 58
    iget-object v0, p0, Lcea;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized CX()V
    .locals 2

    .prologue
    .line 63
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcea;->aTk:Z

    .line 64
    iget-object v0, p0, Lcea;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcea;->aTi:J

    .line 65
    iget-object v0, p0, Lcea;->aTg:Lcdy;

    invoke-interface {v0}, Lcdy;->Dh()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcea;->aTj:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized CY()V
    .locals 2

    .prologue
    .line 70
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcea;->aTl:Z

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcea;->aTm:Z

    .line 72
    iget-object v0, p0, Lcea;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcea;->aTi:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized CZ()V
    .locals 1

    .prologue
    .line 77
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcea;->aTn:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized run()V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 82
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcea;->aTn:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 119
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 87
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcea;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v6

    .line 90
    iget-boolean v0, p0, Lcea;->aTm:Z

    if-nez v0, :cond_7

    .line 92
    iget-boolean v0, p0, Lcea;->aTl:Z

    if-eqz v0, :cond_4

    iget-wide v0, p0, Lcea;->aTi:J

    :goto_1
    sub-long v0, v6, v0

    .line 94
    iget-object v2, p0, Lcea;->aTg:Lcdy;

    invoke-interface {v2}, Lcdy;->Di()I

    move-result v2

    int-to-long v2, v2

    sub-long v0, v2, v0

    .line 95
    cmp-long v2, v0, v4

    if-gtz v2, :cond_2

    .line 96
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcea;->aTm:Z

    .line 97
    iget-object v3, p0, Lcea;->aSB:Lcdf;

    new-instance v8, Leij;

    iget-boolean v2, p0, Lcea;->aTk:Z

    if-eqz v2, :cond_5

    const v2, 0x1001f

    :goto_2
    iget-object v9, p0, Lcea;->aTg:Lcdy;

    invoke-interface {v9}, Lcdy;->Di()I

    move-result v9

    int-to-long v10, v9

    invoke-direct {v8, v2, v10, v11}, Leij;-><init>(IJ)V

    invoke-interface {v3, v8}, Lcdf;->b(Leie;)V

    .line 103
    :cond_2
    :goto_3
    iget-boolean v2, p0, Lcea;->aTm:Z

    if-eqz v2, :cond_3

    .line 105
    iget-wide v0, p0, Lcea;->aTi:J

    sub-long v0, v6, v0

    .line 106
    iget-wide v2, p0, Lcea;->aTj:J

    sub-long v0, v2, v0

    .line 107
    cmp-long v2, v0, v4

    if-gtz v2, :cond_3

    .line 109
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcea;->aTn:Z

    .line 110
    iget-object v3, p0, Lcea;->aSB:Lcdf;

    new-instance v6, Leie;

    iget-boolean v2, p0, Lcea;->aTk:Z

    if-eqz v2, :cond_6

    const v2, 0x1001d

    :goto_4
    invoke-direct {v6, v2}, Leie;-><init>(I)V

    invoke-interface {v3, v6}, Lcdf;->c(Leie;)V

    .line 115
    :cond_3
    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 117
    iget-object v2, p0, Lcea;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v4, 0xa

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, p0, v0, v1, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 92
    :cond_4
    :try_start_2
    iget-wide v0, p0, Lcea;->aTh:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 97
    :cond_5
    const v2, 0x1001e

    goto :goto_2

    .line 110
    :cond_6
    const v2, 0x1001c

    goto :goto_4

    :cond_7
    move-wide v0, v4

    goto :goto_3
.end method
