.class public final Luj;
.super Landroid/view/View;
.source "PG"


# static fields
.field private static FD:F


# instance fields
.field private FE:Landroid/graphics/Bitmap;

.field private FF:I

.field private FG:I

.field private FH:Landroid/graphics/Point;

.field private FI:Landroid/graphics/Rect;

.field private FJ:Z

.field private FK:F

.field FL:Landroid/animation/ValueAnimator;

.field private FM:F

.field private FN:F

.field private FO:F

.field private FP:F

.field private ob:Landroid/graphics/Paint;

.field private rI:Landroid/graphics/Bitmap;

.field private xu:Lcom/android/launcher3/DragLayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Luj;->FD:F

    return-void
.end method

.method public constructor <init>(Lcom/android/launcher3/Launcher;Landroid/graphics/Bitmap;IIIIIIF)V
    .locals 10

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 41
    const/4 v2, 0x0

    iput-object v2, p0, Luj;->FH:Landroid/graphics/Point;

    .line 42
    const/4 v2, 0x0

    iput-object v2, p0, Luj;->FI:Landroid/graphics/Rect;

    .line 43
    const/4 v2, 0x0

    iput-object v2, p0, Luj;->xu:Lcom/android/launcher3/DragLayer;

    .line 44
    const/4 v2, 0x0

    iput-boolean v2, p0, Luj;->FJ:Z

    .line 45
    const/4 v2, 0x0

    iput v2, p0, Luj;->FK:F

    .line 48
    const/4 v2, 0x0

    iput v2, p0, Luj;->FM:F

    .line 49
    const/4 v2, 0x0

    iput v2, p0, Luj;->FN:F

    .line 50
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Luj;->FO:F

    .line 53
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Luj;->FP:F

    .line 69
    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    iput-object v2, p0, Luj;->xu:Lcom/android/launcher3/DragLayer;

    .line 70
    move/from16 v0, p9

    iput v0, p0, Luj;->FO:F

    .line 72
    invoke-virtual {p0}, Luj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 73
    const v3, 0x7f0d0062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v4, v3

    .line 74
    const v3, 0x7f0d0063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v5, v3

    .line 75
    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 76
    move/from16 v0, p7

    int-to-float v3, v0

    add-float/2addr v2, v3

    move/from16 v0, p7

    int-to-float v3, v0

    div-float v7, v2, v3

    .line 79
    move/from16 v0, p9

    invoke-virtual {p0, v0}, Luj;->setScaleX(F)V

    .line 80
    move/from16 v0, p9

    invoke-virtual {p0, v0}, Luj;->setScaleY(F)V

    .line 83
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Luj;->FL:Landroid/animation/ValueAnimator;

    .line 84
    iget-object v2, p0, Luj;->FL:Landroid/animation/ValueAnimator;

    const-wide/16 v8, 0x96

    invoke-virtual {v2, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 85
    iget-object v8, p0, Luj;->FL:Landroid/animation/ValueAnimator;

    new-instance v2, Luk;

    move-object v3, p0

    move/from16 v6, p9

    invoke-direct/range {v2 .. v7}, Luk;-><init>(Luj;FFFF)V

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 110
    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, p7

    move/from16 v1, p8

    invoke-static {p2, v2, v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Luj;->rI:Landroid/graphics/Bitmap;

    .line 111
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, p7

    move/from16 v1, p8

    invoke-direct {v2, v3, v4, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Luj;->FI:Landroid/graphics/Rect;

    .line 114
    iput p3, p0, Luj;->FF:I

    .line 115
    iput p4, p0, Luj;->FG:I

    .line 118
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 119
    invoke-virtual {p0, v2, v2}, Luj;->measure(II)V

    .line 120
    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Luj;->ob:Landroid/graphics/Paint;

    .line 121
    return-void

    .line 83
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic a(Luj;F)F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Luj;->FM:F

    add-float/2addr v0, p1

    iput v0, p0, Luj;->FM:F

    return v0
.end method

.method static synthetic b(Luj;)F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Luj;->FM:F

    return v0
.end method

.method static synthetic b(Luj;F)F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Luj;->FN:F

    add-float/2addr v0, p1

    iput v0, p0, Luj;->FN:F

    return v0
.end method

.method static synthetic c(Luj;)F
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Luj;->FN:F

    return v0
.end method

.method static synthetic c(Luj;F)F
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Luj;->FK:F

    return p1
.end method

.method static synthetic gg()F
    .locals 1

    .prologue
    .line 32
    sget v0, Luj;->FD:F

    return v0
.end method


# virtual methods
.method public final G(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 258
    iget-object v0, p0, Luj;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/DragLayer;->addView(Landroid/view/View;)V

    .line 261
    new-instance v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/android/launcher3/DragLayer$LayoutParams;-><init>(II)V

    .line 262
    iget-object v1, p0, Luj;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    .line 263
    iget-object v1, p0, Luj;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    .line 264
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    .line 265
    invoke-virtual {p0, v0}, Luj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 266
    iget v0, p0, Luj;->FF:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Luj;->setTranslationX(F)V

    .line 267
    iget v0, p0, Luj;->FG:I

    sub-int v0, p2, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Luj;->setTranslationY(F)V

    .line 269
    new-instance v0, Lum;

    invoke-direct {v0, p0}, Lum;-><init>(Luj;)V

    invoke-virtual {p0, v0}, Luj;->post(Ljava/lang/Runnable;)Z

    .line 274
    return-void
.end method

.method final H(II)V
    .locals 2

    .prologue
    .line 294
    iget v0, p0, Luj;->FF:I

    sub-int v0, p1, v0

    iget v1, p0, Luj;->FM:F

    float-to-int v1, v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Luj;->setTranslationX(F)V

    .line 295
    iget v0, p0, Luj;->FG:I

    sub-int v0, p2, v0

    iget v1, p0, Luj;->FN:F

    float-to-int v1, v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Luj;->setTranslationY(F)V

    .line 296
    return-void
.end method

.method public final a(Landroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Luj;->FH:Landroid/graphics/Point;

    .line 154
    return-void
.end method

.method public final bb(I)V
    .locals 4

    .prologue
    .line 215
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 216
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 217
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 218
    new-instance v1, Lul;

    invoke-direct {v1, p0}, Lul;-><init>(Luj;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 224
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 225
    return-void

    .line 215
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final d(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Luj;->FI:Landroid/graphics/Rect;

    .line 162
    return-void
.end method

.method public final e(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Luj;->FE:Landroid/graphics/Bitmap;

    .line 212
    return-void
.end method

.method public final eo()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Luj;->FP:F

    return v0
.end method

.method public final fX()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Luj;->FI:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public final fY()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Luj;->FH:Landroid/graphics/Point;

    return-object v0
.end method

.method public final fZ()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Luj;->FI:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final ga()F
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Luj;->FO:F

    return v0
.end method

.method public final gb()V
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Luj;->getScaleX()F

    move-result v0

    iput v0, p0, Luj;->FO:F

    .line 174
    return-void
.end method

.method public final gd()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Luj;->FJ:Z

    return v0
.end method

.method public final ge()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Luj;->FL:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Luj;->FL:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Luj;->FL:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 280
    :cond_0
    return-void
.end method

.method public final gf()V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    iput v0, p0, Luj;->FN:F

    iput v0, p0, Luj;->FM:F

    .line 284
    invoke-virtual {p0}, Luj;->requestLayout()V

    .line 285
    return-void
.end method

.method public final l(F)V
    .locals 0

    .prologue
    .line 125
    iput p1, p0, Luj;->FP:F

    .line 126
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/high16 v5, 0x437f0000    # 255.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 184
    iput-boolean v0, p0, Luj;->FJ:Z

    .line 193
    iget v1, p0, Luj;->FK:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    iget-object v1, p0, Luj;->FE:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 194
    :goto_0
    if-eqz v0, :cond_0

    .line 195
    iget v1, p0, Luj;->FK:F

    sub-float v1, v4, v1

    mul-float/2addr v1, v5

    float-to-int v1, v1

    .line 196
    iget-object v2, p0, Luj;->ob:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 198
    :cond_0
    iget-object v1, p0, Luj;->rI:Landroid/graphics/Bitmap;

    iget-object v2, p0, Luj;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 199
    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Luj;->ob:Landroid/graphics/Paint;

    iget v1, p0, Luj;->FK:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 201
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 202
    iget-object v0, p0, Luj;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    iget-object v1, p0, Luj;->FE:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 203
    iget-object v1, p0, Luj;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    iget-object v2, p0, Luj;->FE:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 204
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 205
    iget-object v0, p0, Luj;->FE:Landroid/graphics/Bitmap;

    iget-object v1, p0, Luj;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 206
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 208
    :cond_1
    return-void

    .line 193
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Luj;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Luj;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Luj;->setMeasuredDimension(II)V

    .line 179
    return-void
.end method

.method final remove()V
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Luj;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Luj;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    .line 302
    :cond_0
    return-void
.end method

.method public final setAlpha(F)V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 246
    iget-object v0, p0, Luj;->ob:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 247
    invoke-virtual {p0}, Luj;->invalidate()V

    .line 248
    return-void
.end method

.method public final setColor(I)V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Luj;->ob:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Luj;->ob:Landroid/graphics/Paint;

    .line 231
    :cond_0
    if-eqz p1, :cond_1

    .line 232
    iget-object v0, p0, Luj;->ob:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 236
    :goto_0
    invoke-virtual {p0}, Luj;->invalidate()V

    .line 237
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Luj;->ob:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0
.end method
