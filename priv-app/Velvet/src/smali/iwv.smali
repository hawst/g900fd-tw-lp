.class public final Liwv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field private ajm:Ljava/lang/String;

.field private dMx:Ljcn;

.field private dMy:F

.field private dMz:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Liwv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liwv;->ajm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liwv;->afb:Ljava/lang/String;

    iput-object v1, p0, Liwv;->dMx:Ljcn;

    const/4 v0, 0x0

    iput v0, p0, Liwv;->dMy:F

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Liwv;->dMz:[I

    iput-object v1, p0, Liwv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwv;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwv;->ajm:Ljava/lang/String;

    iget v0, p0, Liwv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwv;->afb:Ljava/lang/String;

    iget v0, p0, Liwv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwv;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liwv;->dMx:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Liwv;->dMx:Ljcn;

    :cond_1
    iget-object v0, p0, Liwv;->dMx:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Liwv;->dMy:F

    iget v0, p0, Liwv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liwv;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Liwv;->dMz:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Liwv;->dMz:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Liwv;->dMz:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Liwv;->dMz:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Liwv;->dMz:[I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Liwv;->dMz:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Liwv;->dMz:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Liwv;->dMz:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Liwv;->dMz:[I

    :cond_b
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget v0, p0, Liwv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liwv;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Liwv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liwv;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Liwv;->dMx:Ljcn;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Liwv;->dMx:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_2
    iget v0, p0, Liwv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Liwv;->dMy:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    :cond_3
    iget-object v0, p0, Liwv;->dMz:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Liwv;->dMz:[I

    array-length v0, v0

    if-lez v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Liwv;->dMz:[I

    array-length v1, v1

    if-ge v0, v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Liwv;->dMz:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Liwv;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Liwv;->ajm:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Liwv;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Liwv;->afb:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Liwv;->dMx:Ljcn;

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Liwv;->dMx:Ljcn;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Liwv;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget v3, p0, Liwv;->dMy:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Liwv;->dMz:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Liwv;->dMz:[I

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    :goto_0
    iget-object v3, p0, Liwv;->dMz:[I

    array-length v3, v3

    if-ge v1, v3, :cond_4

    iget-object v3, p0, Liwv;->dMz:[I

    aget v3, v3, v1

    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    add-int/2addr v0, v2

    iget-object v1, p0, Liwv;->dMz:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    return v0
.end method
