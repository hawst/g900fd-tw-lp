.class public Lhjb;
.super Lhjm;
.source "PG"

# interfaces
.implements Lhjl;


# static fields
.field private static dip:Ljava/util/Map;


# instance fields
.field private aWy:Ljava/util/List;

.field private final bgq:Lchk;

.field private dK:Ljava/lang/Object;

.field private final dhE:Lenw;

.field private diq:Lhjg;

.field private dir:Lhjj;

.field private dis:Z

.field private dit:I

.field private diu:I
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private div:I
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mBluetoothDevice:Lhji;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDeviceCapabilityManager:Lenm;

.field private final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xb

    const/16 v6, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 106
    new-instance v0, Lijn;

    invoke-direct {v0}, Lijn;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "DEVICE_STATE_UNKNOWN"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "DEVICE_STATE_CONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "DEVICE_STATE_NONE"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SCO_STATE_DISCONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SCO_STATE_CONNECTING"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SCO_STATE_CONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    move-result-object v0

    sput-object v0, Lhjb;->dip:Ljava/util/Map;

    .line 117
    new-instance v0, Lijn;

    invoke-direct {v0}, Lijn;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_CONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_CONNECTING"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_DISCONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_DISCONNECTING"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_AUDIO_CONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_AUDIO_CONNECTING"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_AUDIO_DISCONNECTED"

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    return-void
.end method

.method public constructor <init>(Lenm;Landroid/media/AudioManager;Landroid/content/Context;Lchk;Ljava/util/concurrent/Executor;Lenw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-direct {p0}, Lhjm;-><init>()V

    .line 139
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhjb;->dK:Ljava/lang/Object;

    .line 144
    const/4 v0, 0x2

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lhjb;->aWy:Ljava/util/List;

    .line 149
    iput-boolean v1, p0, Lhjb;->dis:Z

    .line 154
    iput v1, p0, Lhjb;->diu:I

    .line 156
    const/16 v0, 0xa

    iput v0, p0, Lhjb;->div:I

    .line 164
    iput-object p1, p0, Lhjb;->mDeviceCapabilityManager:Lenm;

    .line 165
    iput-object p2, p0, Lhjb;->mAudioManager:Landroid/media/AudioManager;

    .line 166
    iput-object p3, p0, Lhjb;->mContext:Landroid/content/Context;

    .line 167
    iput-object p4, p0, Lhjb;->bgq:Lchk;

    .line 168
    iput-object p5, p0, Lhjb;->mExecutor:Ljava/util/concurrent/Executor;

    .line 169
    iput-object p6, p0, Lhjb;->dhE:Lenw;

    .line 170
    return-void
.end method

.method public static aW(Ljava/lang/String;Ljava/lang/String;)Litj;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 587
    new-instance v1, Litj;

    invoke-direct {v1}, Litj;-><init>()V

    .line 588
    if-eqz p0, :cond_0

    .line 589
    invoke-virtual {v1, p0}, Litj;->pI(Ljava/lang/String;)Litj;

    .line 592
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 593
    sget-object v2, Lifa;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 594
    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 595
    invoke-static {v0}, Lgnn;->T([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Litj;->pJ(Ljava/lang/String;)Litj;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 601
    const-string v0, ":"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 602
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xc

    if-lt v2, v3, :cond_1

    .line 605
    const/4 v2, 0x0

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 606
    invoke-virtual {v1, v0}, Litj;->pK(Ljava/lang/String;)Litj;

    .line 609
    :cond_1
    return-object v1

    .line 596
    :catch_0
    move-exception v0

    .line 597
    const-string v2, "BluetoothController"

    const-string v3, "MD5 not available"

    invoke-static {v2, v3, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private kY(I)V
    .locals 5

    .prologue
    .line 351
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 352
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 353
    :try_start_0
    iget v2, p0, Lhjb;->div:I

    .line 354
    iput p1, p0, Lhjb;->div:I

    .line 357
    iget-object v0, p0, Lhjb;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhje;

    .line 358
    iget v4, p0, Lhjb;->div:I

    if-eq v4, v2, :cond_0

    .line 359
    iget v4, p0, Lhjb;->div:I

    invoke-interface {v0, v2, v4}, Lhje;->bg(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method final a(ILhji;)V
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 540
    invoke-virtual {p0, p2}, Lhjb;->c(Lhji;)V

    .line 544
    :goto_0
    return-void

    .line 542
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhjb;->c(Lhji;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Landroid/content/Intent;Lhji;)V
    .locals 7

    .prologue
    .line 513
    iget-object v6, p0, Lhjb;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lhjc;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BluetoothController: onReceive[intent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lhjc;-><init>(Lhjb;Ljava/lang/String;[ILandroid/content/Intent;Lhji;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 530
    return-void
.end method

.method public final a(Lhje;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    .line 183
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 184
    :try_start_0
    iget-object v0, p0, Lhjb;->aWy:Ljava/util/List;

    const-class v2, Lhje;

    invoke-static {p2, v2, p1}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lhjk;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 461
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 462
    iget-boolean v0, p0, Lhjb;->dis:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lhjb;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhjb;->dis:Z

    .line 463
    :cond_0
    check-cast p1, Lhjj;

    iput-object p1, p0, Lhjb;->dir:Lhjj;

    .line 464
    iget-object v0, p0, Lhjb;->dir:Lhjj;

    invoke-virtual {v0}, Lhjj;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 465
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 467
    invoke-virtual {p0, v3}, Lhjb;->c(Lhji;)V

    .line 480
    :goto_0
    return-void

    .line 470
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhji;

    .line 471
    iget-object v1, p0, Lhjb;->dir:Lhjj;

    invoke-virtual {v1, v0}, Lhjj;->h(Lhji;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 474
    invoke-virtual {p0, v0}, Lhjb;->c(Lhji;)V

    goto :goto_0

    .line 477
    :cond_2
    invoke-virtual {p0, v3}, Lhjb;->c(Lhji;)V

    goto :goto_0
.end method

.method protected aPM()Lhjg;
    .locals 1

    .prologue
    .line 174
    invoke-static {}, Lhjf;->aPT()Lhjg;

    move-result-object v0

    return-object v0
.end method

.method public final aPN()V
    .locals 5

    .prologue
    .line 195
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 196
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    iget-object v0, p0, Lhjb;->diq:Lhjg;

    if-nez v0, :cond_0

    iget v0, p0, Lhjb;->diu:I

    if-eqz v0, :cond_1

    .line 202
    :cond_0
    monitor-exit v1

    .line 207
    :goto_0
    return-void

    .line 204
    :cond_1
    new-instance v0, Lerc;

    invoke-direct {v0}, Lerc;-><init>()V

    invoke-virtual {v0}, Lerc;->avy()Lerc;

    .line 205
    iget-object v0, p0, Lhjb;->dK:Ljava/lang/Object;

    invoke-virtual {p0}, Lhjb;->aPM()Lhjg;

    move-result-object v0

    iput-object v0, p0, Lhjb;->diq:Lhjg;

    iget-object v0, p0, Lhjb;->diq:Lhjg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhjb;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    const-string v0, "BluetoothController"

    const-string v2, "BT not available: no off call adapter"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    iput v0, p0, Lhjb;->diu:I

    .line 207
    :cond_3
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 205
    :cond_4
    :try_start_1
    iget-object v2, p0, Lhjb;->diq:Lhjg;

    iget-object v3, p0, Lhjb;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lhjb;->mExecutor:Ljava/util/concurrent/Executor;

    const-class v4, Lhjl;

    invoke-static {v0, v4, p0}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjl;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Lhjg;->a(Landroid/content/Context;Lhjl;I)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "BluetoothController"

    const-string v2, "BT not available: no headset profile"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    iput v0, p0, Lhjb;->diu:I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lhjb;->diq:Lhjg;

    iget-object v0, v0, Lhjg;->diz:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x2

    iput v0, p0, Lhjb;->diu:I

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lhjb;->diq:Lhjg;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lhjg;->getProfileConnectionState(I)I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    if-nez v0, :cond_3

    :cond_7
    const/4 v0, 0x2

    iput v0, p0, Lhjb;->diu:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final aPO()I
    .locals 2

    .prologue
    .line 279
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 280
    :try_start_0
    iget v0, p0, Lhjb;->diu:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPP()Lhji;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 290
    :try_start_0
    iget v0, p0, Lhjb;->diu:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lhjb;->mBluetoothDevice:Lhji;

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPQ()I
    .locals 2

    .prologue
    .line 298
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 299
    :try_start_0
    iget v0, p0, Lhjb;->div:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPR()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 404
    iget v0, p0, Lhjb;->dit:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 405
    const-string v0, "BluetoothController"

    const-string v1, "stopSco: Invalid connection type, returning"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 409
    invoke-virtual {p0}, Lhjb;->aPN()V

    .line 410
    new-instance v0, Lerc;

    invoke-direct {v0}, Lerc;-><init>()V

    invoke-virtual {v0}, Lerc;->avy()Lerc;

    .line 411
    iget v0, p0, Lhjb;->div:I

    if-eq v0, v2, :cond_0

    .line 415
    invoke-direct {p0, v2}, Lhjb;->kY(I)V

    .line 416
    const-string v0, "BluetoothController"

    const-string v1, "Stopping VR"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhjb;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lhjb;->dit:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lhjb;->dir:Lhjj;

    iget-object v1, p0, Lhjb;->mBluetoothDevice:Lhji;

    invoke-virtual {v0, v1}, Lhjj;->e(Lhji;)Z

    move-result v0

    :goto_1
    if-nez v0, :cond_0

    .line 417
    const-string v0, "BluetoothController"

    const-string v1, "stopSco: stopVoiceRecognition failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 416
    :cond_2
    iget v0, p0, Lhjb;->dit:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lhjb;->dir:Lhjj;

    invoke-virtual {v0}, Lhjj;->aPX()Z

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhjb;->dir:Lhjj;

    iget-object v1, p0, Lhjb;->mBluetoothDevice:Lhji;

    invoke-virtual {v0, v1}, Lhjj;->g(Lhji;)Z

    move-result v0

    goto :goto_1
.end method

.method public final aPS()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 485
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 486
    iget-boolean v0, p0, Lhjb;->dis:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjb;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjb;->dis:Z

    .line 487
    :cond_0
    iput-object v1, p0, Lhjb;->dir:Lhjj;

    .line 488
    invoke-virtual {p0, v1}, Lhjb;->c(Lhji;)V

    .line 489
    return-void
.end method

.method public final c(Lhji;)V
    .locals 6
    .param p1    # Lhji;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 318
    const-string v1, "BluetoothController"

    if-nez p1, :cond_2

    const-string v0, "No BT device"

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 321
    iget-object v1, p0, Lhjb;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 322
    :try_start_0
    iget v2, p0, Lhjb;->div:I

    .line 323
    iget v3, p0, Lhjb;->diu:I

    .line 324
    iget-object v0, p0, Lhjb;->mBluetoothDevice:Lhji;

    .line 325
    if-nez p1, :cond_3

    .line 326
    const/16 v0, 0xa

    iput v0, p0, Lhjb;->div:I

    .line 327
    const/4 v0, 0x2

    iput v0, p0, Lhjb;->diu:I

    .line 328
    const/4 v0, 0x0

    iput-object v0, p0, Lhjb;->mBluetoothDevice:Lhji;

    .line 338
    :goto_1
    iget-object v0, p0, Lhjb;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhje;

    .line 339
    iget v5, p0, Lhjb;->div:I

    if-eq v5, v2, :cond_1

    .line 340
    iget v5, p0, Lhjb;->div:I

    invoke-interface {v0, v2, v5}, Lhje;->bg(II)V

    .line 342
    :cond_1
    iget v5, p0, Lhjb;->diu:I

    if-eq v5, v3, :cond_0

    .line 343
    iget v5, p0, Lhjb;->diu:I

    invoke-interface {v0}, Lhje;->aPC()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 318
    :cond_2
    const-string v0, "BT device connected"

    goto :goto_0

    .line 330
    :cond_3
    :try_start_1
    iput-object p1, p0, Lhjb;->mBluetoothDevice:Lhji;

    .line 331
    const/4 v0, 0x1

    iput v0, p0, Lhjb;->diu:I

    goto :goto_1

    .line 346
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final kZ(I)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 375
    iput p1, p0, Lhjb;->dit:I

    .line 376
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 377
    const-string v0, "BluetoothController"

    const-string v1, "startSco: Invalid connection type, returning"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lhjb;->dhE:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 381
    invoke-virtual {p0}, Lhjb;->aPN()V

    .line 382
    new-instance v0, Lerc;

    invoke-direct {v0}, Lerc;-><init>()V

    invoke-virtual {v0}, Lerc;->avy()Lerc;

    .line 383
    iget v0, p0, Lhjb;->div:I

    if-ne v0, v2, :cond_0

    .line 389
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lhjb;->kY(I)V

    .line 390
    const-string v0, "BluetoothController"

    const-string v1, "Starting VR"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x155

    invoke-virtual {p0, v0}, Lhjb;->lb(I)V

    iget-object v0, p0, Lhjb;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lhjb;->dit:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lhjb;->dir:Lhjj;

    iget-object v1, p0, Lhjb;->mBluetoothDevice:Lhji;

    invoke-virtual {v0, v1}, Lhjj;->d(Lhji;)Z

    move-result v0

    :goto_1
    if-nez v0, :cond_0

    .line 391
    const-string v0, "BluetoothController"

    const-string v1, "startSco: startVoiceRecognition failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-direct {p0, v2}, Lhjb;->kY(I)V

    goto :goto_0

    .line 390
    :cond_2
    iget v0, p0, Lhjb;->dit:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lhjb;->dir:Lhjj;

    invoke-virtual {v0}, Lhjj;->aPW()Z

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhjb;->dir:Lhjj;

    iget-object v1, p0, Lhjb;->mBluetoothDevice:Lhji;

    invoke-virtual {v0, v1}, Lhjj;->f(Lhji;)Z

    move-result v0

    goto :goto_1
.end method

.method final la(I)V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/16 v2, 0xa

    const/16 v1, 0xb

    .line 551
    if-ne p1, v3, :cond_3

    .line 552
    iget v0, p0, Lhjb;->div:I

    if-eq v0, v1, :cond_2

    .line 553
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "onScoStateChange: Not expecting a transition to STATE_AUDIO_CONNECTED when mScoState == "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Lhjb;->div:I

    sget-object v2, Lhjb;->dip:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Caused by another app, probably the dialer."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    :cond_0
    :goto_1
    return-void

    .line 553
    :cond_1
    const-string v0, "[Illegal value]"

    goto :goto_0

    .line 557
    :cond_2
    const/16 v0, 0x156

    invoke-virtual {p0, v0}, Lhjb;->lb(I)V

    .line 559
    invoke-direct {p0, v3}, Lhjb;->kY(I)V

    goto :goto_1

    .line 561
    :cond_3
    if-ne p1, v2, :cond_5

    .line 562
    iget v0, p0, Lhjb;->div:I

    if-ne v0, v1, :cond_4

    .line 563
    const/16 v0, 0x157

    invoke-virtual {p0, v0}, Lhjb;->lb(I)V

    .line 566
    :cond_4
    invoke-direct {p0, v2}, Lhjb;->kY(I)V

    goto :goto_1

    .line 568
    :cond_5
    iget v0, p0, Lhjb;->div:I

    if-eq v0, v1, :cond_0

    .line 569
    const-string v0, "BluetoothController"

    const-string v1, "Not expecting STATE_AUDIO_CONNECTING"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final lb(I)V
    .locals 3

    .prologue
    .line 576
    invoke-static {p1}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 577
    iget-object v1, p0, Lhjb;->mBluetoothDevice:Lhji;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhjb;->bgq:Lchk;

    invoke-virtual {v1}, Lchk;->HB()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 578
    iget-object v1, p0, Lhjb;->mBluetoothDevice:Lhji;

    invoke-virtual {v1}, Lhji;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhjb;->mBluetoothDevice:Lhji;

    invoke-virtual {v2}, Lhji;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhjb;->aW(Ljava/lang/String;Ljava/lang/String;)Litj;

    move-result-object v1

    iput-object v1, v0, Litu;->dIN:Litj;

    .line 581
    :cond_0
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 582
    return-void
.end method
