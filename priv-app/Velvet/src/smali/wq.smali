.class public Lwq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public AY:I

.field public AZ:I

.field public Bb:I

.field public Bc:I

.field public Bd:J

.field public JA:J

.field public JB:I

.field public JC:I

.field public JD:Z

.field public JE:[I

.field public Jk:Ljava/lang/CharSequence;

.field public Jl:Lahz;

.field public Jz:I

.field public id:J

.field public title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    const/4 v0, 0x1

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-wide v2, p0, Lwq;->id:J

    .line 63
    iput-wide v2, p0, Lwq;->JA:J

    .line 68
    iput-wide v2, p0, Lwq;->Bd:J

    .line 73
    iput v1, p0, Lwq;->Bb:I

    .line 78
    iput v1, p0, Lwq;->Bc:I

    .line 83
    iput v0, p0, Lwq;->AY:I

    .line 88
    iput v0, p0, Lwq;->AZ:I

    .line 93
    iput v0, p0, Lwq;->JB:I

    .line 98
    iput v0, p0, Lwq;->JC:I

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lwq;->JD:Z

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lwq;->JE:[I

    .line 123
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v0

    iput-object v0, p0, Lwq;->Jl:Lahz;

    .line 124
    return-void
.end method

.method constructor <init>(Lwq;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    const/4 v0, 0x1

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-wide v2, p0, Lwq;->id:J

    .line 63
    iput-wide v2, p0, Lwq;->JA:J

    .line 68
    iput-wide v2, p0, Lwq;->Bd:J

    .line 73
    iput v1, p0, Lwq;->Bb:I

    .line 78
    iput v1, p0, Lwq;->Bc:I

    .line 83
    iput v0, p0, Lwq;->AY:I

    .line 88
    iput v0, p0, Lwq;->AZ:I

    .line 93
    iput v0, p0, Lwq;->JB:I

    .line 98
    iput v0, p0, Lwq;->JC:I

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lwq;->JD:Z

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lwq;->JE:[I

    .line 127
    invoke-virtual {p0, p1}, Lwq;->c(Lwq;)V

    .line 129
    invoke-static {p0}, Lzi;->e(Lwq;)V

    .line 130
    return-void
.end method

.method static a(Landroid/content/ContentValues;II)V
    .locals 2

    .prologue
    .line 174
    const-string v0, "cellX"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 175
    const-string v0, "cellY"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 176
    return-void
.end method

.method static a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 195
    if-eqz p1, :cond_0

    .line 196
    invoke-static {p1}, Lwq;->g(Landroid/graphics/Bitmap;)[B

    move-result-object v0

    .line 197
    const-string v1, "icon"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 199
    :cond_0
    return-void
.end method

.method public static g(Landroid/graphics/Bitmap;)[B
    .locals 3

    .prologue
    .line 181
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    .line 182
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 184
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 185
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 186
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 187
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 190
    :goto_0
    return-object v0

    .line 189
    :catch_0
    move-exception v0

    const-string v0, "Favorite"

    const-string v1, "Could not write icon"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/content/Context;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 157
    const-string v0, "itemType"

    iget v1, p0, Lwq;->Jz:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    const-string v0, "container"

    iget-wide v2, p0, Lwq;->JA:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 159
    const-string v0, "screen"

    iget-wide v2, p0, Lwq;->Bd:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 160
    const-string v0, "cellX"

    iget v1, p0, Lwq;->Bb:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 161
    const-string v0, "cellY"

    iget v1, p0, Lwq;->Bc:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 162
    const-string v0, "spanX"

    iget v1, p0, Lwq;->AY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 163
    const-string v0, "spanY"

    iget v1, p0, Lwq;->AZ:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 164
    invoke-static {p1}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v0

    iget-object v1, p0, Lwq;->Jl:Lahz;

    invoke-virtual {v0, v1}, Laia;->c(Lahz;)J

    move-result-wide v0

    .line 165
    const-string v2, "profileId"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 167
    iget-wide v0, p0, Lwq;->Bd:J

    const-wide/16 v2, -0xc9

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Screen id should not be EXTRA_EMPTY_SCREEN_ID"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    return-void
.end method

.method bX()V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public final c(Lwq;)V
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p1, Lwq;->id:J

    iput-wide v0, p0, Lwq;->id:J

    .line 134
    iget v0, p1, Lwq;->Bb:I

    iput v0, p0, Lwq;->Bb:I

    .line 135
    iget v0, p1, Lwq;->Bc:I

    iput v0, p0, Lwq;->Bc:I

    .line 136
    iget v0, p1, Lwq;->AY:I

    iput v0, p0, Lwq;->AY:I

    .line 137
    iget v0, p1, Lwq;->AZ:I

    iput v0, p0, Lwq;->AZ:I

    .line 138
    iget-wide v0, p1, Lwq;->Bd:J

    iput-wide v0, p0, Lwq;->Bd:J

    .line 139
    iget v0, p1, Lwq;->Jz:I

    iput v0, p0, Lwq;->Jz:I

    .line 140
    iget-wide v0, p1, Lwq;->JA:J

    iput-wide v0, p0, Lwq;->JA:J

    .line 141
    iget-object v0, p1, Lwq;->Jl:Lahz;

    iput-object v0, p0, Lwq;->Jl:Lahz;

    .line 142
    iget-object v0, p1, Lwq;->Jk:Ljava/lang/CharSequence;

    iput-object v0, p0, Lwq;->Jk:Ljava/lang/CharSequence;

    .line 143
    return-void
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected Intent"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Item(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lwq;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lwq;->Jz:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " container="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lwq;->JA:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lwq;->Bd:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lwq;->Bb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lwq;->Bc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lwq;->AY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lwq;->AZ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dropPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lwq;->JE:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lwq;->Jl:Lahz;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
