.class public final enum Ldyf;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum bQC:Ldyf;

.field public static final enum bQD:Ldyf;

.field public static final enum bQE:Ldyf;

.field public static final enum bQF:Ldyf;

.field public static final enum bQG:Ldyf;

.field private static final synthetic bQJ:[Ldyf;


# instance fields
.field public final bQH:I

.field public final bQI:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Ldyf;

    const-string v1, "MORNING"

    const v3, 0x7f0a08af

    const v4, 0x7f0a08b3

    const v5, 0x7f0a08b7

    const/16 v6, 0x9

    move v7, v2

    invoke-direct/range {v0 .. v7}, Ldyf;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v0, Ldyf;->bQC:Ldyf;

    .line 15
    new-instance v3, Ldyf;

    const-string v4, "AFTERNOON"

    const v6, 0x7f0a08b0

    const v7, 0x7f0a08b4

    const v8, 0x7f0a08b8

    const/16 v9, 0xd

    move v5, v11

    move v10, v11

    invoke-direct/range {v3 .. v10}, Ldyf;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Ldyf;->bQD:Ldyf;

    .line 20
    new-instance v3, Ldyf;

    const-string v4, "EVENING"

    const v6, 0x7f0a08b1

    const v7, 0x7f0a08b5

    const v8, 0x7f0a08b9

    const/16 v9, 0x12

    move v5, v12

    move v10, v12

    invoke-direct/range {v3 .. v10}, Ldyf;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Ldyf;->bQE:Ldyf;

    .line 25
    new-instance v3, Ldyf;

    const-string v4, "NIGHT"

    const v6, 0x7f0a08b2

    const v7, 0x7f0a08b6

    const v8, 0x7f0a08ba

    const/16 v9, 0x14

    move v5, v13

    move v10, v13

    invoke-direct/range {v3 .. v10}, Ldyf;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Ldyf;->bQF:Ldyf;

    .line 30
    new-instance v3, Ldyf;

    const-string v4, "TIME_UNSPECIFIED"

    const/16 v9, 0x9

    move v5, v14

    move v6, v2

    move v7, v2

    move v8, v2

    move v10, v14

    invoke-direct/range {v3 .. v10}, Ldyf;-><init>(Ljava/lang/String;IIIIII)V

    sput-object v3, Ldyf;->bQG:Ldyf;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Ldyf;

    sget-object v1, Ldyf;->bQC:Ldyf;

    aput-object v1, v0, v2

    sget-object v1, Ldyf;->bQD:Ldyf;

    aput-object v1, v0, v11

    sget-object v1, Ldyf;->bQE:Ldyf;

    aput-object v1, v0, v12

    sget-object v1, Ldyf;->bQF:Ldyf;

    aput-object v1, v0, v13

    sget-object v1, Ldyf;->bQG:Ldyf;

    aput-object v1, v0, v14

    sput-object v0, Ldyf;->bQJ:[Ldyf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIII)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p6, p0, Ldyf;->bQH:I

    .line 38
    iput p7, p0, Ldyf;->bQI:I

    .line 39
    return-void
.end method

.method public static gW(I)Ldyf;
    .locals 5

    .prologue
    .line 49
    invoke-static {}, Ldyf;->values()[Ldyf;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 50
    iget v4, v0, Ldyf;->bQI:I

    if-ne v4, p0, :cond_0

    .line 54
    :goto_1
    return-object v0

    .line 49
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Ldyf;
    .locals 1

    .prologue
    .line 9
    const-class v0, Ldyf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldyf;

    return-object v0
.end method

.method public static values()[Ldyf;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Ldyf;->bQJ:[Ldyf;

    invoke-virtual {v0}, [Ldyf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldyf;

    return-object v0
.end method
