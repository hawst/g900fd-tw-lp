.class public final Ldqx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# static fields
.field public static final bGB:Ldqx;

.field private static final bGd:[F

.field private static final bGe:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    new-instance v0, Ldqx;

    invoke-direct {v0}, Ldqx;-><init>()V

    sput-object v0, Ldqx;->bGB:Ldqx;

    .line 21
    const/16 v0, 0x65

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Ldqx;->bGd:[F

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    sget-object v1, Ldqx;->bGd:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sput v0, Ldqx;->bGe:F

    return-void

    .line 21
    :array_0
    .array-data 4
        0x0
        0x3d2d0db5
        0x3da236cf
        0x3de7a7ca    # 0.113113f
        0x3e143958    # 0.14475f
        0x3e32be06
        0x3e4face6
        0x3e6b3aac
        0x3e82c7fc
        0x3e8f661f
        0x3e9b8477
        0x3ea72da1    # 0.32652f
        0x3eb26a66
        0x3ebd4281
        0x3ec7bc7f
        0x3ed1de05    # 0.409897f
        0x3edbac4f
        0x3ee52bb2
        0x3eee79ab
        0x3ef7afc9
        0x3f00664d
        0x3f04e736
        0x3f0959f7
        0x3f0dbdb6
        0x3f121199
        0x3f1654d6
        0x3f1a8694    # 0.603616f
        0x3f1ea609
        0x3f22b24a
        0x3f26aa8f
        0x3f2a8e1d
        0x3f2e5c0c
        0x3f3213a1
        0x3f35b435
        0x3f393d21
        0x3f3cadac
        0x3f40054f
        0x3f434385
        0x3f4667f9
        0x3f497236
        0x3f4c61fa    # 0.79837f
        0x3f4f3733
        0x3f51f19d
        0x3f54914b
        0x3f57163c
        0x3f5980b2
        0x3f5bd0e1
        0x3f5e070c
        0x3f602396
        0x3f6226f6
        0x3f6411b2
        0x3f65e450
        0x3f679f88
        0x3f6943e1    # 0.911192f
        0x3f6ad224
        0x3f6c4af9
        0x3f6daf29
        0x3f6eff6d
        0x3f703c8e
        0x3f716755
        0x3f72806b
        0x3f7388b9
        0x3f7480e9
        0x3f7569b1
        0x3f7643dd
        0x3f771013
        0x3f77cf0b
        0x3f78816f    # 0.970725f
        0x3f7927e5
        0x3f79c305
        0x3f7a5376
        0x3f7ad9be
        0x3f7b5665
        0x3f7bca10
        0x3f7c3526
        0x3f7c981c
        0x3f7cf388
        0x3f7d47bf
        0x3f7d9525
        0x3f7ddc40
        0x3f7e1d54
        0x3f7e58d6
        0x3f7e8f08
        0x3f7ec040
        0x3f7eecd0
        0x3f7f14fd
        0x3f7f3919
        0x3f7f5958
        0x3f7f760c
        0x3f7f8f58
        0x3f7fa58f
        0x3f7fb8d4
        0x3f7fc958
        0x3f7fd74d
        0x3f7fe2f8
        0x3f7fec68
        0x3f7ff3cf
        0x3f7ff951
        0x3f7ffd1e    # 0.999956f
        0x3f7fff47    # 0.999989f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 5

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 41
    cmpl-float v2, p1, v0

    if-ltz v2, :cond_0

    .line 57
    :goto_0
    return v0

    .line 45
    :cond_0
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_1

    move v0, v1

    .line 46
    goto :goto_0

    .line 49
    :cond_1
    sget-object v0, Ldqx;->bGd:[F

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    sget-object v1, Ldqx;->bGd:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 53
    int-to-float v1, v0

    sget v2, Ldqx;->bGe:F

    mul-float/2addr v1, v2

    .line 54
    sub-float v1, p1, v1

    .line 55
    sget v2, Ldqx;->bGe:F

    div-float/2addr v1, v2

    .line 57
    sget-object v2, Ldqx;->bGd:[F

    aget v2, v2, v0

    sget-object v3, Ldqx;->bGd:[F

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    sget-object v4, Ldqx;->bGd:[F

    aget v0, v4, v0

    sub-float v0, v3, v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_0
.end method
