.class final Lgzv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leen;


# instance fields
.field final synthetic dap:Lgyz;


# direct methods
.method constructor <init>(Lgyz;)V
    .locals 0

    .prologue
    .line 1928
    iput-object p1, p0, Lgzv;->dap:Lgyz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ligi;)Z
    .locals 1

    .prologue
    .line 1989
    const/4 v0, 0x0

    return v0
.end method

.method public final j(Lcom/google/android/shared/search/Suggestion;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1931
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-static {v0}, Lgyz;->a(Lgyz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1942
    :goto_0
    return-void

    .line 1936
    :cond_0
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0, v2, p1}, Ledu;->b(ILcom/google/android/shared/search/Suggestion;)V

    .line 1937
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    iget-object v0, v0, Lgyz;->mSearchServiceClient:Lhaa;

    iget-object v1, p0, Lgzv;->dap:Lgyz;

    invoke-virtual {v1}, Lgyz;->aLX()Ledu;

    move-result-object v1

    iput v2, v1, Ledu;->bVW:I

    invoke-virtual {v1}, Ledu;->anZ()Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lhaa;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    goto :goto_0
.end method

.method public final k(Lcom/google/android/shared/search/Suggestion;)V
    .locals 2

    .prologue
    .line 1946
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-static {v0}, Lgyz;->a(Lgyz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1953
    :goto_0
    return-void

    .line 1950
    :cond_0
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ledu;->b(ILcom/google/android/shared/search/Suggestion;)V

    .line 1951
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    iget-object v0, v0, Lgyz;->mSearchServiceClient:Lhaa;

    iget-object v1, p0, Lgzv;->dap:Lgyz;

    invoke-virtual {v1}, Lgyz;->aLX()Ledu;

    move-result-object v1

    invoke-virtual {v1}, Ledu;->anZ()Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lhaa;->b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    goto :goto_0
.end method

.method public final l(Lcom/google/android/shared/search/Suggestion;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1958
    iget-object v2, p0, Lgzv;->dap:Lgyz;

    invoke-static {v2}, Lgyz;->a(Lgyz;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1970
    :goto_0
    return v0

    .line 1962
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1964
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    iget-object v0, v0, Lgyz;->cZT:Lhac;

    new-instance v2, Lgzw;

    const-string v3, "Remove suggestion"

    invoke-direct {v2, p0, v3, p1}, Lgzw;-><init>(Lgzv;Ljava/lang/String;Lcom/google/android/shared/search/Suggestion;)V

    invoke-interface {v0, p1, v2}, Lhac;->a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/Runnable;)V

    move v0, v1

    .line 1970
    goto :goto_0
.end method

.method public final m(Lcom/google/android/shared/search/Suggestion;)V
    .locals 4

    .prologue
    .line 1975
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-static {v0}, Lgyz;->a(Lgyz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1983
    :goto_0
    return-void

    .line 1979
    :cond_0
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    iget-object v0, v0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lgzv;->dap:Lgyz;

    iget-object v1, v1, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    .line 1981
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0, p1}, Ledu;->n(Lcom/google/android/shared/search/Suggestion;)Ledu;

    .line 1982
    iget-object v0, p0, Lgzv;->dap:Lgyz;

    invoke-virtual {v0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    iget-object v1, p0, Lgzv;->dap:Lgyz;

    iget-object v1, v1, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    goto :goto_0
.end method
