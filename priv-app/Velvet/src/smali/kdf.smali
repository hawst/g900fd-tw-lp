.class final enum Lkdf;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum eTm:Lkdf;

.field public static final enum eTn:Lkdf;

.field public static final enum eTo:Lkdf;

.field public static final enum eTp:Lkdf;

.field private static final synthetic eTq:[Lkdf;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lkdf;

    const-string v1, "SPDY_SYN_STREAM"

    invoke-direct {v0, v1, v2}, Lkdf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkdf;->eTm:Lkdf;

    .line 20
    new-instance v0, Lkdf;

    const-string v1, "SPDY_REPLY"

    invoke-direct {v0, v1, v3}, Lkdf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkdf;->eTn:Lkdf;

    .line 21
    new-instance v0, Lkdf;

    const-string v1, "SPDY_HEADERS"

    invoke-direct {v0, v1, v4}, Lkdf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkdf;->eTo:Lkdf;

    .line 22
    new-instance v0, Lkdf;

    const-string v1, "HTTP_20_HEADERS"

    invoke-direct {v0, v1, v5}, Lkdf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkdf;->eTp:Lkdf;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lkdf;

    sget-object v1, Lkdf;->eTm:Lkdf;

    aput-object v1, v0, v2

    sget-object v1, Lkdf;->eTn:Lkdf;

    aput-object v1, v0, v3

    sget-object v1, Lkdf;->eTo:Lkdf;

    aput-object v1, v0, v4

    sget-object v1, Lkdf;->eTp:Lkdf;

    aput-object v1, v0, v5

    sput-object v0, Lkdf;->eTq:[Lkdf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkdf;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lkdf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkdf;

    return-object v0
.end method

.method public static values()[Lkdf;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lkdf;->eTq:[Lkdf;

    invoke-virtual {v0}, [Lkdf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkdf;

    return-object v0
.end method


# virtual methods
.method public final bzg()Z
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lkdf;->eTn:Lkdf;

    if-eq p0, v0, :cond_0

    sget-object v0, Lkdf;->eTo:Lkdf;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzh()Z
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lkdf;->eTm:Lkdf;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzi()Z
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lkdf;->eTo:Lkdf;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzj()Z
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lkdf;->eTn:Lkdf;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
