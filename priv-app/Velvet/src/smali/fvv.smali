.class public final Lfvv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bLG:I

.field cCH:Z

.field private final cte:Ljbp;

.field private final mEntry:Lizj;

.field mName:Ljava/lang/String;

.field private final mRoute:Liyg;


# direct methods
.method public constructor <init>(Lizj;ILiyg;Ljbp;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvv;->cCH:Z

    .line 37
    iput-object p1, p0, Lfvv;->mEntry:Lizj;

    .line 38
    const/16 v0, 0xba

    iput v0, p0, Lfvv;->bLG:I

    .line 39
    iput-object p3, p0, Lfvv;->mRoute:Liyg;

    .line 40
    iput-object p4, p0, Lfvv;->cte:Ljbp;

    .line 41
    invoke-virtual {p4}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfvv;->mName:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method final aDh()Lanh;
    .locals 12
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lfvv;->mName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvv;->cte:Ljbp;

    invoke-virtual {v0}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "QpLocationModuleBuilder"

    const-string v2, "No name or address on location, skipping location module"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_0
    return-object v1

    .line 61
    :cond_0
    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    .line 66
    iget-object v0, p0, Lfvv;->mRoute:Liyg;

    if-eqz v0, :cond_6

    .line 67
    iget-object v0, p0, Lfvv;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->aEz()I

    move-result v0

    invoke-static {v0}, Lgba;->jQ(I)Lgba;

    move-result-object v3

    .line 68
    iget-object v0, p0, Lfvv;->mRoute:Liyg;

    invoke-static {v0}, Lgaz;->c(Liyg;)Ljava/lang/String;

    move-result-object v2

    .line 69
    iget-object v0, p0, Lfvv;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbL()Ljava/lang/String;

    move-result-object v0

    .line 73
    :goto_1
    iget-boolean v5, p0, Lfvv;->cCH:Z

    if-nez v5, :cond_4

    .line 74
    iget-object v0, p0, Lfvv;->cte:Ljbp;

    invoke-static {v0}, Lgaz;->f(Ljbp;)Ljava/lang/String;

    move-result-object v0

    .line 75
    iget-object v2, p0, Lfvv;->cte:Ljbp;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "http://maps.google.com/maps/?q=%s&entry=r"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v0, v7, v11

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljbp;->bar()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "&cid="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljbp;->baq()J

    move-result-wide v6

    invoke-static {v6, v7}, Liuv;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    const-string v0, "&layer=t"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&entry=r"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_3
    new-instance v2, Lfsa;

    iget v3, p0, Lfvv;->bLG:I

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0201b8

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 85
    new-instance v1, Laon;

    invoke-direct {v1}, Laon;-><init>()V

    .line 87
    iget-object v2, p0, Lfvv;->cte:Ljbp;

    invoke-virtual {v2}, Ljbp;->bfb()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 88
    iget-object v2, p0, Lfvv;->cte:Ljbp;

    invoke-virtual {v2}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laon;->cb(Ljava/lang/String;)Laon;

    .line 89
    iget-object v2, p0, Lfvv;->mName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 90
    iget-object v2, p0, Lfvv;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laon;->ca(Ljava/lang/String;)Laon;

    .line 98
    :cond_2
    :goto_4
    const/16 v2, 0xc

    invoke-virtual {v4, v2}, Lanh;->cm(I)Lanh;

    .line 99
    iput-object v1, v4, Lanh;->agA:Laon;

    .line 100
    iput-object v0, v4, Lanh;->ahs:Lani;

    .line 102
    iget-object v0, p0, Lfvv;->mEntry:Lizj;

    iput-object v0, v4, Lanh;->ahu:Lizj;

    move-object v1, v4

    .line 104
    goto/16 :goto_0

    .line 75
    :cond_3
    invoke-virtual {v2}, Ljbp;->nH()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljbp;->nI()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "ll=%s,%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljbp;->mR()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-virtual {v2}, Ljbp;->mS()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v10

    invoke-static {v0, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "&"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 77
    :cond_4
    iget-object v5, p0, Lfvv;->cte:Ljbp;

    invoke-static {v5, v3, v2, v10, v0}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 93
    :cond_5
    iget-object v2, p0, Lfvv;->mName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 94
    iget-object v2, p0, Lfvv;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Laon;->cb(Ljava/lang/String;)Laon;

    goto :goto_4

    :cond_6
    move-object v0, v1

    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_1
.end method
