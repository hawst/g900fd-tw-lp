.class public Ldlb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aZF:Ljava/lang/String;

.field private bBd:Z

.field private bBe:Z

.field private bBf:Z

.field private bBg:I

.field private bBh:Z

.field private bBi:I

.field private bfs:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iput-boolean v0, p0, Ldlb;->bBd:Z

    .line 393
    iput-boolean v0, p0, Ldlb;->bBe:Z

    .line 394
    iput-boolean v0, p0, Ldlb;->bBf:Z

    .line 397
    const/4 v0, -0x1

    iput v0, p0, Ldlb;->bBi:I

    .line 403
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iput-boolean v0, p0, Ldlb;->bBd:Z

    .line 393
    iput-boolean v0, p0, Ldlb;->bBe:Z

    .line 394
    iput-boolean v0, p0, Ldlb;->bBf:Z

    .line 397
    const/4 v0, -0x1

    iput v0, p0, Ldlb;->bBi:I

    .line 411
    iput-object p1, p0, Ldlb;->aZF:Ljava/lang/String;

    .line 412
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 415
    invoke-direct {p0, p1}, Ldlb;-><init>(Ljava/lang/String;)V

    .line 416
    iput-object p2, p0, Ldlb;->bfs:Ljava/util/Map;

    .line 417
    return-void
.end method


# virtual methods
.method public final acN()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Ldlb;->bBe:Z

    return v0
.end method

.method public final acO()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Ldlb;->bBh:Z

    return v0
.end method

.method public final acP()I
    .locals 1

    .prologue
    .line 500
    iget v0, p0, Ldlb;->bBg:I

    return v0
.end method

.method public final acQ()I
    .locals 1

    .prologue
    .line 523
    iget v0, p0, Ldlb;->bBi:I

    return v0
.end method

.method public final dy(Z)V
    .locals 1

    .prologue
    .line 464
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldlb;->bBe:Z

    .line 465
    return-void
.end method

.method public final dz(Z)V
    .locals 0

    .prologue
    .line 485
    iput-boolean p1, p0, Ldlb;->bBh:Z

    .line 486
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 528
    instance-of v1, p1, Ldlb;

    if-eqz v1, :cond_0

    .line 529
    check-cast p1, Ldlb;

    .line 530
    iget-object v1, p0, Ldlb;->aZF:Ljava/lang/String;

    iget-object v2, p1, Ldlb;->aZF:Ljava/lang/String;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldlb;->bfs:Ljava/util/Map;

    iget-object v2, p1, Ldlb;->bfs:Ljava/util/Map;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 533
    :cond_0
    return v0
.end method

.method public final fX(I)V
    .locals 1

    .prologue
    .line 496
    const v0, 0x15180

    iput v0, p0, Ldlb;->bBg:I

    .line 497
    return-void
.end method

.method public final fY(I)V
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x0

    iput v0, p0, Ldlb;->bBi:I

    .line 515
    return-void
.end method

.method public final getFollowRedirects()Z
    .locals 1

    .prologue
    .line 459
    iget-boolean v0, p0, Ldlb;->bBd:Z

    return v0
.end method

.method public final getHeaders()Ljava/util/Map;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Ldlb;->bfs:Ljava/util/Map;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Ldlb;->aZF:Ljava/lang/String;

    return-object v0
.end method

.method public final getUseCaches()Z
    .locals 1

    .prologue
    .line 477
    iget-boolean v0, p0, Ldlb;->bBf:Z

    return v0
.end method

.method public final setFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldlb;->bBd:Z

    .line 456
    return-void
.end method

.method public final setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Ldlb;->bfs:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 449
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldlb;->bfs:Ljava/util/Map;

    .line 451
    :cond_0
    iget-object v0, p0, Ldlb;->bfs:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    return-void
.end method

.method public final setUseCaches(Z)V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldlb;->bBf:Z

    .line 474
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GetRequest{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldlb;->aZF:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
