.class final Lfdv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdt;


# instance fields
.field private synthetic cpC:Ljava/lang/String;

.field private synthetic cpD:Lfdu;


# direct methods
.method constructor <init>(Lfdu;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lfdv;->cpD:Lfdu;

    iput-object p2, p0, Lfdv;->cpC:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final acquire()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 185
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v2, v0, Lfdu;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 186
    :try_start_0
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 187
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    iget-object v3, p0, Lfdv;->cpC:Ljava/lang/String;

    invoke-virtual {v0, p0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 189
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v1, v0, Lfdu;->dK:Ljava/lang/Object;

    iget-object v1, v0, Lfdu;->cpv:Landroid/os/HandlerThread;

    if-nez v1, :cond_2

    new-instance v1, Lfdw;

    const-string v3, "LocationOracleImpl"

    const/16 v4, 0xa

    invoke-direct {v1, v0, v3, v4}, Lfdw;-><init>(Lfdu;Ljava/lang/String;I)V

    iput-object v1, v0, Lfdu;->cpv:Landroid/os/HandlerThread;

    iget-object v0, v0, Lfdu;->cpv:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 191
    :cond_0
    :goto_1
    monitor-exit v2

    return-void

    .line 186
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 189
    :cond_2
    invoke-virtual {v0}, Lfdu;->ayD()Landroid/os/Handler;

    move-result-object v1

    new-instance v3, Lfdx;

    invoke-direct {v3, v0}, Lfdx;-><init>(Lfdu;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected final finalize()V
    .locals 3

    .prologue
    .line 209
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 210
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lfdv;->release()V

    .line 212
    const-string v0, "LocationOracleImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Forgot to release lock from: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfdv;->cpC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :cond_0
    return-void
.end method

.method public final release()V
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v1, v0, Lfdu;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 199
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    iget-object v0, v0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lfdv;->cpD:Lfdu;

    invoke-virtual {v0}, Lfdu;->ayD()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lfdy;

    invoke-direct {v3, v0}, Lfdy;-><init>(Lfdu;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 203
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
