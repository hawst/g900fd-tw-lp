.class public final Lfza;
.super Lfsz;
.source "PG"


# instance fields
.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfsz;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 32
    iput-object p4, p0, Lfza;->mClock:Lemp;

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 37
    const v0, 0x7f04011b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final aDn()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lfza;->mContext:Landroid/content/Context;

    const v1, 0x7f0a050b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final aDo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lfza;->mContext:Landroid/content/Context;

    const v1, 0x7f0a050c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 42
    const v0, 0x7f040136

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final bv(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 71
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 72
    iget-object v7, v0, Lanh;->ahn:Lapb;

    .line 73
    new-instance v0, Lfze;

    iget-object v1, p0, Lfza;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfza;->mCardContainer:Lfmt;

    invoke-virtual {p0}, Lfza;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lfza;->mActivityHelper:Lfzw;

    iget-object v5, p0, Lfro;->cBc:Lanh;

    iget-object v5, v5, Lanh;->ahu:Lizj;

    iget-object v6, p0, Lfza;->mClock:Lemp;

    invoke-direct/range {v0 .. v6}, Lfze;-><init>(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Lfzw;Lizj;Lemp;)V

    .line 77
    iget-object v1, v7, Lapb;->akG:Lapc;

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, v7, Lapb;->akG:Lapc;

    invoke-virtual {v0, p1, v1}, Lfze;->a(Landroid/view/View;Lapc;)V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-static {p1}, Lfze;->bA(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final bw(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const-wide v8, 0x4053200000000000L    # 76.5

    const-wide v6, 0x3fe6666666666666L    # 0.7

    .line 86
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 87
    iget-object v0, v0, Lanh;->ahn:Lapb;

    .line 89
    invoke-virtual {v0}, Lapb;->rR()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 92
    const v1, 0x7f1101a9

    invoke-virtual {v0}, Lapb;->rQ()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lfza;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 93
    new-instance v1, Lfsa;

    const/16 v2, 0xb1

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    invoke-virtual {v0}, Lapb;->rT()I

    move-result v2

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    iget-object v0, v0, Lapb;->akE:[Laoo;

    aget-object v0, v0, v3

    invoke-virtual {v1, v0}, Lfsa;->a(Laoo;)Lani;

    move-result-object v1

    .line 97
    invoke-virtual {p0, p1, v1}, Lfza;->a(Landroid/view/View;Lani;)V

    .line 99
    const v0, 0x7f1102eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 100
    if-eqz v0, :cond_0

    .line 101
    iget-object v1, v1, Lani;->ahC:Lanb;

    invoke-virtual {v1}, Lanb;->oh()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 102
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 107
    instance-of v1, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v1, :cond_1

    .line 108
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    .line 110
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v8

    double-to-int v2, v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, v6

    add-double/2addr v4, v8

    double-to-int v3, v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-double v4, v0

    mul-double/2addr v4, v6

    add-double/2addr v4, v8

    double-to-int v0, v4

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 115
    const v1, 0x7f110272

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 120
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 47
    const v0, 0x7f040135

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final d(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 53
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 54
    iget-object v0, v1, Lanh;->ahn:Lapb;

    .line 57
    const v2, 0x7f1101a9

    invoke-virtual {v0}, Lapb;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lfza;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 58
    const v2, 0x7f110080

    invoke-virtual {v0}, Lapb;->ol()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lfza;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 60
    iget-object v0, v1, Lanh;->ahs:Lani;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahC:Lanb;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahC:Lanb;

    invoke-virtual {v0}, Lanb;->oi()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const v0, 0x7f1102eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 62
    if-eqz v0, :cond_0

    .line 63
    iget-object v1, v1, Lanh;->ahs:Lani;

    iget-object v1, v1, Lani;->ahC:Lanb;

    invoke-virtual {v1}, Lanb;->oh()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 64
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 67
    :cond_0
    return-void
.end method
