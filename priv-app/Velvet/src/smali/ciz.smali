.class public final Lciz;
.super Lciy;
.source "PG"


# instance fields
.field final mBgExecutor:Ljava/util/concurrent/Executor;

.field final mSearchSettings:Lcke;

.field final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcke;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Lciy;-><init>()V

    .line 30
    iput-object p1, p0, Lciz;->mSearchSettings:Lcke;

    .line 31
    iput-object p2, p0, Lciz;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 32
    iput-object p3, p0, Lciz;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 33
    invoke-virtual {p0}, Lciz;->KQ()V

    .line 34
    return-void
.end method


# virtual methods
.method public final KQ()V
    .locals 12

    .prologue
    .line 40
    iget-object v0, p0, Lciz;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Op()Lciw;

    move-result-object v0

    .line 41
    iget-object v1, v0, Lciw;->aXO:[Lcix;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lciw;->aXO:[Lcix;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 66
    :cond_0
    return-void

    .line 44
    :cond_1
    iget-object v10, v0, Lciw;->aXO:[Lcix;

    array-length v11, v10

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v11, :cond_0

    aget-object v0, v10, v9

    .line 48
    invoke-virtual {v0}, Lcix;->KJ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ldzb;->valueOf(Ljava/lang/String;)Ldzb;

    move-result-object v1

    .line 50
    if-nez v1, :cond_2

    .line 51
    sget-object v1, Ldzb;->bRt:Ldzb;

    .line 53
    :cond_2
    invoke-virtual {v0}, Lcix;->KK()Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-virtual {v0}, Lcix;->KL()Ljava/lang/String;

    move-result-object v3

    .line 55
    invoke-virtual {v0}, Lcix;->KM()J

    move-result-wide v4

    .line 56
    invoke-virtual {v0}, Lcix;->KN()J

    move-result-wide v6

    .line 57
    invoke-virtual {v0}, Lcix;->KO()Ljava/lang/String;

    move-result-object v8

    .line 58
    new-instance v0, Lcom/google/android/search/shared/contact/PersonShortcut;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/search/shared/contact/PersonShortcut;-><init>(Ldzb;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    .line 64
    iget-object v1, p0, Lciz;->aXW:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0
.end method

.method public final KR()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lciz;->aXW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Lcix;

    move v1, v2

    .line 73
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_2

    .line 74
    iget-object v0, p0, Lciz;->aXW:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonShortcut;

    .line 78
    new-instance v4, Lcix;

    invoke-direct {v4}, Lcix;-><init>()V

    .line 79
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->amn()Ldzb;

    move-result-object v5

    invoke-virtual {v5}, Ldzb;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcix;->gz(Ljava/lang/String;)Lcix;

    .line 80
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->KK()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcix;->gA(Ljava/lang/String;)Lcix;

    .line 81
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->amo()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 82
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->amo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcix;->gB(Ljava/lang/String;)Lcix;

    .line 84
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->KM()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcix;->S(J)Lcix;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->KN()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcix;->T(J)Lcix;

    .line 86
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->KO()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 87
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->KO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcix;->gC(Ljava/lang/String;)Lcix;

    .line 92
    :cond_1
    aput-object v4, v3, v1

    .line 73
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 95
    :cond_2
    new-instance v0, Lciw;

    invoke-direct {v0}, Lciw;-><init>()V

    .line 96
    iput-object v3, v0, Lciw;->aXO:[Lcix;

    .line 98
    new-instance v1, Lcja;

    invoke-direct {v1, p0}, Lcja;-><init>(Lciz;)V

    const/4 v3, 0x1

    new-array v3, v3, [Lciw;

    aput-object v0, v3, v2

    invoke-virtual {v1, v3}, Lcja;->a([Ljava/lang/Object;)Lenp;

    .line 99
    return-void
.end method
