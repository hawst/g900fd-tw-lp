.class public Ldkq;
.super Landroid/database/DataSetObservable;
.source "PG"


# instance fields
.field private final aMD:Leqo;

.field private bAT:Ldkt;

.field private final dK:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Leqo;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    .line 34
    iput-object p1, p0, Ldkq;->aMD:Leqo;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldkq;->dK:Ljava/lang/Object;

    .line 36
    return-void
.end method

.method static synthetic a(Ldkq;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method


# virtual methods
.method public final a(Ldkt;)V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Ldkq;->aMD:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p0, p1}, Ldkq;->b(Ldkt;)V

    .line 61
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v1, p0, Ldkq;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 48
    :try_start_0
    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-ne v0, p1, :cond_3

    .line 49
    :cond_1
    iput-object p1, p0, Ldkq;->bAT:Ldkt;

    .line 61
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 51
    :cond_3
    :goto_1
    :try_start_1
    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-eq v0, p1, :cond_2

    .line 52
    iget-object v0, p0, Ldkq;->aMD:Leqo;

    new-instance v2, Ldkr;

    const-string v3, "ForceableLock: obtain"

    invoke-direct {v2, p0, v3, p1}, Ldkr;-><init>(Ldkq;Ljava/lang/String;Ldkt;)V

    invoke-interface {v0, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 58
    iget-object v0, p0, Ldkq;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method final b(Ldkt;)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Ldkq;->aMD:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 67
    iget-object v1, p0, Ldkq;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    :try_start_0
    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-eq v0, p1, :cond_0

    .line 69
    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    invoke-interface {v0}, Ldkt;->Na()V

    .line 71
    :cond_0
    iput-object p1, p0, Ldkq;->bAT:Ldkt;

    .line 72
    iget-object v0, p0, Ldkq;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 73
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Ldkt;)Z
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Ldkq;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_0
    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-ne v0, p1, :cond_1

    .line 79
    :cond_0
    iput-object p1, p0, Ldkq;->bAT:Ldkt;

    .line 80
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Ldkt;)V
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Ldkq;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 95
    :try_start_0
    iget-object v0, p0, Ldkq;->bAT:Ldkt;

    if-ne v0, p1, :cond_0

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Ldkq;->bAT:Ldkt;

    .line 97
    invoke-virtual {p0}, Ldkq;->notifyChanged()V

    .line 99
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public notifyChanged()V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Ldkq;->aMD:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-super {p0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 114
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Ldkq;->aMD:Leqo;

    new-instance v1, Ldks;

    const-string v2, "ForceableLock: notifyChanged"

    invoke-direct {v1, p0, v2}, Ldks;-><init>(Ldkq;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
