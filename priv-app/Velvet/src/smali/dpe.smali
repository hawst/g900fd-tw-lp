.class public final Ldpe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bEB:Ldpf;

.field private final bbd:Lenw;

.field private final bhl:Ljava/util/concurrent/Executor;

.field final mEventBus:Ldda;

.field final mSearchController:Lcjt;

.field private final mVss:Lhhq;


# direct methods
.method public constructor <init>(Lhhq;Ljava/util/concurrent/Executor;Lcjt;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Ldpe;->mVss:Lhhq;

    .line 71
    iput-object p2, p0, Ldpe;->bhl:Ljava/util/concurrent/Executor;

    .line 72
    iput-object p3, p0, Ldpe;->mSearchController:Lcjt;

    .line 73
    iget-object v0, p0, Ldpe;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nf()Ldda;

    move-result-object v0

    iput-object v0, p0, Ldpe;->mEventBus:Ldda;

    .line 74
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Ldpe;->bbd:Lenw;

    .line 75
    return-void
.end method

.method static aN(Lcom/google/android/shared/search/Query;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.speech.extra.DICTATION_MODE"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 131
    :cond_0
    return v0
.end method


# virtual methods
.method public final aM(Lcom/google/android/shared/search/Query;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v2

    invoke-static {v2}, Lifv;->gX(Z)V

    .line 80
    iget-object v2, p0, Ldpe;->bbd:Lenw;

    invoke-virtual {v2}, Lenw;->auS()Lenw;

    .line 82
    iget-object v2, p0, Ldpe;->bEB:Ldpf;

    if-nez v2, :cond_1

    .line 83
    iget-object v2, p0, Ldpe;->mVss:Lhhq;

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    new-instance v3, Lgmy;

    invoke-direct {v3}, Lgmy;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqY()Landroid/net/Uri;

    move-result-object v4

    iput-object v4, v3, Lgmy;->cPM:Landroid/net/Uri;

    invoke-virtual {v3}, Lgmy;->aHB()Lgmx;

    move-result-object v3

    new-instance v4, Lgnk;

    invoke-direct {v4}, Lgnk;-><init>()V

    invoke-virtual {v4, v0}, Lgnk;->kt(I)Lgnk;

    move-result-object v4

    invoke-static {p1}, Ldpe;->aN(Lcom/google/android/shared/search/Query;)Z

    move-result v5

    if-nez v5, :cond_2

    :goto_0
    iput-boolean v0, v4, Lgnk;->cFP:Z

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "android.speech.extra.LANGUAGE"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v5

    invoke-static {v5, v0}, Lgnq;->g(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v5

    if-eqz v5, :cond_3

    :goto_1
    iput-object v0, v4, Lgnk;->cQb:Ljava/lang/String;

    invoke-virtual {v2}, Lhym;->aHP()Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.speech.extra.PROFANITY_FILTER"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iput-boolean v0, v4, Lgnk;->cPu:Z

    sget-object v0, Lgjo;->cMK:Lgjo;

    iput-object v0, v4, Lgnk;->cQe:Lgjo;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "calling_package"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, v4, Lgnk;->cQd:Ljava/lang/String;

    iput-object v3, v4, Lgnk;->cPY:Lgmx;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lgnk;->mRequestId:Ljava/lang/String;

    invoke-virtual {v4}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    .line 84
    new-instance v1, Ldpf;

    invoke-direct {v1, p0, p1}, Ldpf;-><init>(Ldpe;Lcom/google/android/shared/search/Query;)V

    iput-object v1, p0, Ldpe;->bEB:Ldpf;

    .line 85
    iget-object v1, p0, Ldpe;->mVss:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v1

    iget-object v2, p0, Ldpe;->bEB:Ldpf;

    iget-object v3, p0, Ldpe;->bhl:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 87
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 83
    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v5

    invoke-static {v5, v0}, Lgnq;->d(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v5, "TranscriptionWorker"

    const-string v6, "The locale should be specified in BCP47"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v5, v6, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_2
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Ldpe;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 92
    iget-object v0, p0, Ldpe;->bEB:Ldpf;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Ldpe;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Ldpe;->bEB:Ldpf;

    invoke-virtual {v1}, Ldpf;->zK()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Ldpe;->bEB:Ldpf;

    invoke-virtual {v0}, Ldpf;->aen()Z

    move-result v0

    .line 95
    const/4 v1, 0x0

    iput-object v1, p0, Ldpe;->bEB:Ldpf;

    .line 96
    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Ldpe;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 100
    :cond_0
    return-void
.end method
