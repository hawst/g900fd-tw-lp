.class public final Ljmo;
.super Ljsl;
.source "PG"


# static fields
.field public static final eud:Ljsm;


# instance fields
.field private aez:I

.field private eue:Ljava/lang/String;

.field public euf:[Ljoq;

.field private eug:Ljava/lang/String;

.field private euh:[Ljsc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 4294
    const/16 v0, 0xb

    const-class v1, Ljmo;

    const v2, 0xc20a78a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmo;->eud:Ljsm;

    .line 4300
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4357
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4358
    const/4 v0, 0x0

    iput v0, p0, Ljmo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmo;->eue:Ljava/lang/String;

    invoke-static {}, Ljoq;->brt()[Ljoq;

    move-result-object v0

    iput-object v0, p0, Ljmo;->euf:[Ljoq;

    const-string v0, ""

    iput-object v0, p0, Ljmo;->eug:Ljava/lang/String;

    invoke-static {}, Ljsc;->btI()[Ljsc;

    move-result-object v0

    iput-object v0, p0, Ljmo;->euh:[Ljsc;

    const/4 v0, 0x0

    iput-object v0, p0, Ljmo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmo;->eCz:I

    .line 4359
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4287
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmo;->eue:Ljava/lang/String;

    iget v0, p0, Ljmo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmo;->eug:Ljava/lang/String;

    iget v0, p0, Ljmo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmo;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljmo;->euh:[Ljsc;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsc;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljmo;->euh:[Ljsc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljsc;

    invoke-direct {v3}, Ljsc;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljmo;->euh:[Ljsc;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljsc;

    invoke-direct {v3}, Ljsc;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljmo;->euh:[Ljsc;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljmo;->euf:[Ljoq;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljoq;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljmo;->euf:[Ljoq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljoq;

    invoke-direct {v3}, Ljoq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljmo;->euf:[Ljoq;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljoq;

    invoke-direct {v3}, Ljoq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljmo;->euf:[Ljoq;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4375
    iget v0, p0, Ljmo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4376
    const/4 v0, 0x1

    iget-object v2, p0, Ljmo;->eue:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4378
    :cond_0
    iget v0, p0, Ljmo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4379
    const/4 v0, 0x3

    iget-object v2, p0, Ljmo;->eug:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4381
    :cond_1
    iget-object v0, p0, Ljmo;->euh:[Ljsc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljmo;->euh:[Ljsc;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 4382
    :goto_0
    iget-object v2, p0, Ljmo;->euh:[Ljsc;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 4383
    iget-object v2, p0, Ljmo;->euh:[Ljsc;

    aget-object v2, v2, v0

    .line 4384
    if-eqz v2, :cond_2

    .line 4385
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 4382
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4389
    :cond_3
    iget-object v0, p0, Ljmo;->euf:[Ljoq;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljmo;->euf:[Ljoq;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 4390
    :goto_1
    iget-object v0, p0, Ljmo;->euf:[Ljoq;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 4391
    iget-object v0, p0, Ljmo;->euf:[Ljoq;

    aget-object v0, v0, v1

    .line 4392
    if-eqz v0, :cond_4

    .line 4393
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 4390
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4397
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4398
    return-void
.end method

.method public final getMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4335
    iget-object v0, p0, Ljmo;->eug:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4402
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4403
    iget v2, p0, Ljmo;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 4404
    const/4 v2, 0x1

    iget-object v3, p0, Ljmo;->eue:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4407
    :cond_0
    iget v2, p0, Ljmo;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 4408
    const/4 v2, 0x3

    iget-object v3, p0, Ljmo;->eug:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4411
    :cond_1
    iget-object v2, p0, Ljmo;->euh:[Ljsc;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljmo;->euh:[Ljsc;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 4412
    :goto_0
    iget-object v3, p0, Ljmo;->euh:[Ljsc;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 4413
    iget-object v3, p0, Ljmo;->euh:[Ljsc;

    aget-object v3, v3, v0

    .line 4414
    if-eqz v3, :cond_2

    .line 4415
    const/4 v4, 0x4

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4412
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 4420
    :cond_4
    iget-object v2, p0, Ljmo;->euf:[Ljoq;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljmo;->euf:[Ljoq;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 4421
    :goto_1
    iget-object v2, p0, Ljmo;->euf:[Ljoq;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 4422
    iget-object v2, p0, Ljmo;->euf:[Ljoq;

    aget-object v2, v2, v1

    .line 4423
    if-eqz v2, :cond_5

    .line 4424
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4421
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4429
    :cond_6
    return v0
.end method

.method public final xg(Ljava/lang/String;)Ljmo;
    .locals 1

    .prologue
    .line 4338
    if-nez p1, :cond_0

    .line 4339
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4341
    :cond_0
    iput-object p1, p0, Ljmo;->eug:Ljava/lang/String;

    .line 4342
    iget v0, p0, Ljmo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmo;->aez:I

    .line 4343
    return-object p0
.end method
