.class public final Ljad;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dVX:[Ljad;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private dVY:Ljcn;

.field private dVZ:[Ljac;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljad;->aez:I

    iput-object v1, p0, Ljad;->dVY:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljad;->akf:Ljava/lang/String;

    invoke-static {}, Ljac;->bdG()[Ljac;

    move-result-object v0

    iput-object v0, p0, Ljad;->dVZ:[Ljac;

    iput-object v1, p0, Ljad;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljad;->eCz:I

    return-void
.end method

.method public static bdH()[Ljad;
    .locals 2

    sget-object v0, Ljad;->dVX:[Ljad;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljad;->dVX:[Ljad;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljad;

    sput-object v0, Ljad;->dVX:[Ljad;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljad;->dVX:[Ljad;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljad;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljad;->dVY:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljad;->dVY:Ljcn;

    :cond_1
    iget-object v0, p0, Ljad;->dVY:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljad;->akf:Ljava/lang/String;

    iget v0, p0, Ljad;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljad;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljad;->dVZ:[Ljac;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljac;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljad;->dVZ:[Ljac;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljac;

    invoke-direct {v3}, Ljac;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljad;->dVZ:[Ljac;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljac;

    invoke-direct {v3}, Ljac;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljad;->dVZ:[Ljac;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget-object v0, p0, Ljad;->dVY:Ljcn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljad;->dVY:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_0
    iget v0, p0, Ljad;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljad;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljad;->dVZ:[Ljac;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljad;->dVZ:[Ljac;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljad;->dVZ:[Ljac;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ljad;->dVZ:[Ljac;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 5

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget-object v1, p0, Ljad;->dVY:Ljcn;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljad;->dVY:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljad;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljad;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljad;->dVZ:[Ljac;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljad;->dVZ:[Ljac;

    array-length v1, v1

    if-lez v1, :cond_4

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljad;->dVZ:[Ljac;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljad;->dVZ:[Ljac;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    :cond_4
    return v0
.end method
