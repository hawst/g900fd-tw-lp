.class public final Ljqi;
.super Ljsl;
.source "PG"


# static fields
.field public static final eyH:Ljsm;


# instance fields
.field public eyI:Ljoq;

.field public eyJ:Ljpv;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1392
    const/16 v0, 0xb

    const-class v1, Ljqi;

    const/16 v2, 0x1f4a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqi;->eyH:Ljsm;

    .line 1398
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1409
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1410
    iput-object v0, p0, Ljqi;->eyI:Ljoq;

    iput-object v0, p0, Ljqi;->eyJ:Ljpv;

    iput-object v0, p0, Ljqi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqi;->eCz:I

    .line 1411
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljqi;->eyI:Ljoq;

    if-nez v0, :cond_1

    new-instance v0, Ljoq;

    invoke-direct {v0}, Ljoq;-><init>()V

    iput-object v0, p0, Ljqi;->eyI:Ljoq;

    :cond_1
    iget-object v0, p0, Ljqi;->eyI:Ljoq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljqi;->eyJ:Ljpv;

    if-nez v0, :cond_2

    new-instance v0, Ljpv;

    invoke-direct {v0}, Ljpv;-><init>()V

    iput-object v0, p0, Ljqi;->eyJ:Ljpv;

    :cond_2
    iget-object v0, p0, Ljqi;->eyJ:Ljpv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1424
    iget-object v0, p0, Ljqi;->eyI:Ljoq;

    if-eqz v0, :cond_0

    .line 1425
    const/4 v0, 0x1

    iget-object v1, p0, Ljqi;->eyI:Ljoq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1427
    :cond_0
    iget-object v0, p0, Ljqi;->eyJ:Ljpv;

    if-eqz v0, :cond_1

    .line 1428
    const/4 v0, 0x2

    iget-object v1, p0, Ljqi;->eyJ:Ljpv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1430
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1431
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1435
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1436
    iget-object v1, p0, Ljqi;->eyI:Ljoq;

    if-eqz v1, :cond_0

    .line 1437
    const/4 v1, 0x1

    iget-object v2, p0, Ljqi;->eyI:Ljoq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1440
    :cond_0
    iget-object v1, p0, Ljqi;->eyJ:Ljpv;

    if-eqz v1, :cond_1

    .line 1441
    const/4 v1, 0x2

    iget-object v2, p0, Ljqi;->eyJ:Ljpv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1444
    :cond_1
    return v0
.end method
