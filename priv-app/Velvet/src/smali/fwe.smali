.class public final Lfwe;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    .line 52
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 53
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v4, v0, Lizj;->dSu:Ljdc;

    .line 55
    new-instance v5, Lanh;

    invoke-direct {v5}, Lanh;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lanh;->cm(I)Lanh;

    invoke-virtual {v4}, Ljdc;->bgT()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lfsa;

    const/16 v1, 0x51

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    const v1, 0x7f02016e

    invoke-virtual {v0, v1}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    invoke-virtual {v4}, Ljdc;->bgS()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v5, Lanh;->ahs:Lani;

    :cond_0
    iget-object v0, v4, Ljdc;->aeB:Ljbp;

    if-eqz v0, :cond_5

    iget-object v0, v4, Ljdc;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v4}, Ljdc;->bgV()Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4}, Ljdc;->bgU()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v1, 0x0

    invoke-static {p1, v6, v7, v1}, Lesi;->b(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v4}, Ljdc;->bai()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v4}, Ljdc;->tn()Ljava/lang/String;

    move-result-object v2

    :goto_2
    const-string v6, " \u00b7 "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/CharSequence;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    const/4 v0, 0x1

    aput-object v1, v7, v0

    const/4 v0, 0x2

    aput-object v2, v7, v0

    invoke-static {v6, v7}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lany;

    invoke-direct {v1}, Lany;-><init>()V

    iput-object v1, v5, Lanh;->agw:Lany;

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v5, Lanh;->ahu:Lizj;

    invoke-virtual {v4}, Ljdc;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lany;->bh(Ljava/lang/String;)Lany;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Lany;->bi(Ljava/lang/String;)Lany;

    :cond_1
    invoke-virtual {v4}, Ljdc;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Ljdc;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lany;->bk(Ljava/lang/String;)Lany;

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v5, v0}, Lanh;->cn(I)Lanh;

    invoke-static {v3, v5}, Lfwe;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 56
    iget-object v0, v4, Ljdc;->aiX:Ljcn;

    if-eqz v0, :cond_4

    iget-object v0, v4, Ljdc;->aiX:Ljcn;

    invoke-static {v0}, Lgbm;->c(Ljcn;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {p1}, Leot;->au(Landroid/content/Context;)I

    move-result v5

    const v6, 0x7f0d0130

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v8, v0

    const/4 v0, 0x1

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    cmpl-double v2, v8, v10

    if-lez v2, :cond_8

    const/16 v2, 0x200

    if-gt v5, v2, :cond_8

    const/16 v2, 0x200

    if-gt v6, v2, :cond_8

    const/4 v0, 0x4

    :cond_3
    :goto_3
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "w"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "h"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "scale"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    new-instance v2, Lanz;

    invoke-direct {v2}, Lanz;-><init>()V

    iput-object v2, v1, Lanh;->agy:Lanz;

    iget-object v2, v1, Lanh;->agy:Lanz;

    new-instance v5, Laoi;

    invoke-direct {v5}, Laoi;-><init>()V

    iput-object v5, v2, Lanz;->aiU:Laoi;

    iget-object v2, v1, Lanh;->agy:Lanz;

    iget-object v2, v2, Lanz;->aiU:Laoi;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoi;->bQ(Ljava/lang/String;)Laoi;

    invoke-virtual {v4}, Ljdc;->bgT()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Ljdc;->bgS()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lfsa;

    const/16 v4, 0x51

    invoke-direct {v2, v4}, Lfsa;-><init>(I)V

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iget-object v1, v1, Lanh;->agy:Lanz;

    iget-object v1, v1, Lanz;->aiU:Laoi;

    iput-object v0, v1, Laoi;->aju:Lani;

    :cond_4
    const/4 v0, 0x0

    invoke-static {v3, v0}, Lfwe;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 58
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 59
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 61
    return-object v1

    .line 55
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 56
    :cond_8
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v8, v10

    if-lez v2, :cond_3

    const/16 v2, 0x400

    if-gt v5, v2, :cond_3

    const/16 v2, 0x400

    if-gt v6, v2, :cond_3

    const/4 v0, 0x2

    goto/16 :goto_3
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lfwe;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 47
    iget-object v0, v0, Lang;->ags:[Lanh;

    return-object v0
.end method
