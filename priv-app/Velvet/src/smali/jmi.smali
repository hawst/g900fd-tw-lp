.class public final Ljmi;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private ajm:Ljava/lang/String;

.field private esX:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6477
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6478
    const/4 v0, 0x0

    iput v0, p0, Ljmi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmi;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmi;->ajm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmi;->esX:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmi;->aiK:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljmi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmi;->eCz:I

    .line 6479
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 6370
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmi;->agq:Ljava/lang/String;

    iget v0, p0, Ljmi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmi;->ajm:Ljava/lang/String;

    iget v0, p0, Ljmi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmi;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmi;->esX:Ljava/lang/String;

    iget v0, p0, Ljmi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmi;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmi;->aiK:Ljava/lang/String;

    iget v0, p0, Ljmi;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljmi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6495
    iget v0, p0, Ljmi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6496
    const/4 v0, 0x1

    iget-object v1, p0, Ljmi;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6498
    :cond_0
    iget v0, p0, Ljmi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6499
    const/4 v0, 0x2

    iget-object v1, p0, Ljmi;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6501
    :cond_1
    iget v0, p0, Ljmi;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6502
    const/4 v0, 0x3

    iget-object v1, p0, Ljmi;->esX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6504
    :cond_2
    iget v0, p0, Ljmi;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6505
    const/4 v0, 0x4

    iget-object v1, p0, Ljmi;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6507
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6508
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6392
    iget-object v0, p0, Ljmi;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6414
    iget-object v0, p0, Ljmi;->ajm:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6512
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6513
    iget v1, p0, Ljmi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6514
    const/4 v1, 0x1

    iget-object v2, p0, Ljmi;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6517
    :cond_0
    iget v1, p0, Ljmi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6518
    const/4 v1, 0x2

    iget-object v2, p0, Ljmi;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6521
    :cond_1
    iget v1, p0, Ljmi;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6522
    const/4 v1, 0x3

    iget-object v2, p0, Ljmi;->esX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6525
    :cond_2
    iget v1, p0, Ljmi;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 6526
    const/4 v1, 0x4

    iget-object v2, p0, Ljmi;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6529
    :cond_3
    return v0
.end method

.method public final wW(Ljava/lang/String;)Ljmi;
    .locals 1

    .prologue
    .line 6395
    if-nez p1, :cond_0

    .line 6396
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6398
    :cond_0
    iput-object p1, p0, Ljmi;->agq:Ljava/lang/String;

    .line 6399
    iget v0, p0, Ljmi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmi;->aez:I

    .line 6400
    return-object p0
.end method
