.class public final Lahc;
.super Ljsr;
.source "PG"


# instance fields
.field private XY:I

.field public XZ:J

.field public Ya:J

.field public Yb:I

.field public Yc:[Lahd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Ljsr;-><init>()V

    .line 271
    invoke-virtual {p0}, Lahc;->lG()Lahc;

    .line 272
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 238
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Ljsu;->b(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lahc;->XY:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lahc;->XZ:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lahc;->Ya:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lahc;->Yb:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lahc;->Yc:[Lahd;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lahd;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lahc;->Yc:[Lahd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lahd;

    invoke-direct {v3}, Lahd;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lahc;->Yc:[Lahd;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lahd;

    invoke-direct {v3}, Lahd;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lahc;->Yc:[Lahd;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 287
    const/4 v0, 0x1

    iget v1, p0, Lahc;->XY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 288
    const/4 v0, 0x2

    iget-wide v2, p0, Lahc;->XZ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 289
    iget-wide v0, p0, Lahc;->Ya:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 290
    const/4 v0, 0x3

    iget-wide v2, p0, Lahc;->Ya:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 292
    :cond_0
    iget v0, p0, Lahc;->Yb:I

    if-eqz v0, :cond_1

    .line 293
    const/4 v0, 0x4

    iget v1, p0, Lahc;->Yb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 295
    :cond_1
    iget-object v0, p0, Lahc;->Yc:[Lahd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lahc;->Yc:[Lahd;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 296
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lahc;->Yc:[Lahd;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 297
    iget-object v1, p0, Lahc;->Yc:[Lahd;

    aget-object v1, v1, v0

    .line 298
    if-eqz v1, :cond_2

    .line 299
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 296
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 303
    :cond_3
    invoke-super {p0, p1}, Ljsr;->a(Ljsj;)V

    .line 304
    return-void
.end method

.method protected final lF()I
    .locals 7

    .prologue
    .line 308
    invoke-super {p0}, Ljsr;->lF()I

    move-result v0

    .line 309
    const/4 v1, 0x1

    iget v2, p0, Lahc;->XY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    const/4 v1, 0x2

    iget-wide v2, p0, Lahc;->XZ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    iget-wide v2, p0, Lahc;->Ya:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 314
    const/4 v1, 0x3

    iget-wide v2, p0, Lahc;->Ya:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    :cond_0
    iget v1, p0, Lahc;->Yb:I

    if-eqz v1, :cond_1

    .line 318
    const/4 v1, 0x4

    iget v2, p0, Lahc;->Yb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    :cond_1
    iget-object v1, p0, Lahc;->Yc:[Lahd;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lahc;->Yc:[Lahd;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 322
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lahc;->Yc:[Lahd;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 323
    iget-object v2, p0, Lahc;->Yc:[Lahd;

    aget-object v2, v2, v0

    .line 324
    if-eqz v2, :cond_2

    .line 325
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 322
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 330
    :cond_4
    return v0
.end method

.method public final lG()Lahc;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 275
    iput v0, p0, Lahc;->XY:I

    .line 276
    iput-wide v2, p0, Lahc;->XZ:J

    .line 277
    iput-wide v2, p0, Lahc;->Ya:J

    .line 278
    iput v0, p0, Lahc;->Yb:I

    .line 279
    invoke-static {}, Lahd;->lH()[Lahd;

    move-result-object v0

    iput-object v0, p0, Lahc;->Yc:[Lahd;

    .line 280
    const/4 v0, -0x1

    iput v0, p0, Lahc;->eCz:I

    .line 281
    return-object p0
.end method
