.class public final Ljzo;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eNW:[Ljzo;


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field public eNX:[Ljzh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3120
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3121
    const/4 v0, 0x0

    iput v0, p0, Ljzo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzo;->afb:Ljava/lang/String;

    invoke-static {}, Ljzh;->bwP()[Ljzh;

    move-result-object v0

    iput-object v0, p0, Ljzo;->eNX:[Ljzh;

    const/4 v0, 0x0

    iput-object v0, p0, Ljzo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzo;->eCz:I

    .line 3122
    return-void
.end method

.method public static bxh()[Ljzo;
    .locals 2

    .prologue
    .line 3082
    sget-object v0, Ljzo;->eNW:[Ljzo;

    if-nez v0, :cond_1

    .line 3083
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3085
    :try_start_0
    sget-object v0, Ljzo;->eNW:[Ljzo;

    if-nez v0, :cond_0

    .line 3086
    const/4 v0, 0x0

    new-array v0, v0, [Ljzo;

    sput-object v0, Ljzo;->eNW:[Ljzo;

    .line 3088
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3090
    :cond_1
    sget-object v0, Ljzo;->eNW:[Ljzo;

    return-object v0

    .line 3088
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Ai(Ljava/lang/String;)Ljzo;
    .locals 1

    .prologue
    .line 3101
    if-nez p1, :cond_0

    .line 3102
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3104
    :cond_0
    iput-object p1, p0, Ljzo;->afb:Ljava/lang/String;

    .line 3105
    iget v0, p0, Ljzo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzo;->aez:I

    .line 3106
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3076
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzo;->afb:Ljava/lang/String;

    iget v0, p0, Ljzo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzo;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzo;->eNX:[Ljzh;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljzh;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzo;->eNX:[Ljzh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljzh;

    invoke-direct {v3}, Ljzh;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzo;->eNX:[Ljzh;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljzh;

    invoke-direct {v3}, Ljzh;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljzo;->eNX:[Ljzh;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 3136
    iget v0, p0, Ljzo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3137
    const/4 v0, 0x1

    iget-object v1, p0, Ljzo;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3139
    :cond_0
    iget-object v0, p0, Ljzo;->eNX:[Ljzh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljzo;->eNX:[Ljzh;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3140
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljzo;->eNX:[Ljzh;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3141
    iget-object v1, p0, Ljzo;->eNX:[Ljzh;

    aget-object v1, v1, v0

    .line 3142
    if-eqz v1, :cond_1

    .line 3143
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 3140
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3147
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3148
    return-void
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3098
    iget-object v0, p0, Ljzo;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 3152
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3153
    iget v1, p0, Ljzo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3154
    const/4 v1, 0x1

    iget-object v2, p0, Ljzo;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3157
    :cond_0
    iget-object v1, p0, Ljzo;->eNX:[Ljzh;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljzo;->eNX:[Ljzh;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 3158
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljzo;->eNX:[Ljzh;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3159
    iget-object v2, p0, Ljzo;->eNX:[Ljzh;

    aget-object v2, v2, v0

    .line 3160
    if-eqz v2, :cond_1

    .line 3161
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3158
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 3166
    :cond_3
    return v0
.end method
