.class final Lfmn;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic anv:Lemy;

.field private synthetic cwj:Lfml;

.field private synthetic cwk:Ljava/util/Collection;


# direct methods
.method varargs constructor <init>(Lfml;Ljava/lang/String;Lerk;[ILjava/util/Collection;Lemy;)V
    .locals 0

    .prologue
    .line 702
    iput-object p1, p0, Lfmn;->cwj:Lfml;

    iput-object p5, p0, Lfmn;->cwk:Ljava/util/Collection;

    iput-object p6, p0, Lfmn;->anv:Lemy;

    invoke-direct {p0, p2, p3, p4}, Lenp;-><init>(Ljava/lang/String;Lerk;[I)V

    return-void
.end method

.method private varargs aBe()Ljava/util/List;
    .locals 4

    .prologue
    .line 705
    iget-object v0, p0, Lfmn;->cwj:Lfml;

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v1

    .line 706
    if-eqz v1, :cond_1

    .line 707
    iget-object v0, p0, Lfmn;->cwk:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 709
    iget-object v0, p0, Lfmn;->cwk:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdj;

    .line 710
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 714
    :cond_0
    :try_start_0
    invoke-interface {v1, v2}, Lfor;->U(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 721
    :goto_1
    return-object v0

    .line 715
    :catch_0
    move-exception v0

    .line 716
    const-string v1, "NowRemoteClient"

    const-string v2, "Error resolving training questions"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 721
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 702
    invoke-direct {p0}, Lfmn;->aBe()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 702
    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lfmn;->anv:Lemy;

    invoke-interface {v0, p1}, Lemy;->aj(Ljava/lang/Object;)Z

    return-void
.end method
