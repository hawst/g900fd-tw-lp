.class public final Lkm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final jO:Ljava/util/List;

.field private final jP:I

.field public jQ:Lkp;

.field public jR:Lkp;

.field public jS:Lkp;

.field public jT:Lkp;

.field private jU:Lkp;

.field private jV:Lkp;


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 7

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lkm;->jO:Ljava/util/List;

    .line 191
    invoke-direct {p0}, Lkm;->bu()I

    move-result v0

    iput v0, p0, Lkm;->jP:I

    .line 193
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f333333    # 0.7f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lkm;->a(FFFFFF)Lkp;

    move-result-object v0

    iput-object v0, p0, Lkm;->jQ:Lkp;

    .line 196
    const v1, 0x3f3d70a4    # 0.74f

    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lkm;->a(FFFFFF)Lkp;

    move-result-object v0

    iput-object v0, p0, Lkm;->jU:Lkp;

    .line 199
    const v1, 0x3e851eb8    # 0.26f

    const/4 v2, 0x0

    const v3, 0x3ee66666    # 0.45f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lkm;->a(FFFFFF)Lkp;

    move-result-object v0

    iput-object v0, p0, Lkm;->jS:Lkp;

    .line 202
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f333333    # 0.7f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lkm;->a(FFFFFF)Lkp;

    move-result-object v0

    iput-object v0, p0, Lkm;->jR:Lkp;

    .line 205
    const v1, 0x3f3d70a4    # 0.74f

    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lkm;->a(FFFFFF)Lkp;

    move-result-object v0

    iput-object v0, p0, Lkm;->jV:Lkp;

    .line 208
    const v1, 0x3e851eb8    # 0.26f

    const/4 v2, 0x0

    const v3, 0x3ee66666    # 0.45f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lkm;->a(FFFFFF)Lkp;

    move-result-object v0

    iput-object v0, p0, Lkm;->jT:Lkp;

    .line 212
    iget-object v0, p0, Lkm;->jQ:Lkp;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkm;->jS:Lkp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkm;->jS:Lkp;

    invoke-static {v0}, Lkm;->a(Lkp;)[F

    move-result-object v0

    const/4 v1, 0x2

    const/high16 v2, 0x3f000000    # 0.5f

    aput v2, v0, v1

    new-instance v1, Lkp;

    invoke-static {v0}, Lkl;->b([F)I

    move-result v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lkp;-><init>(II)V

    iput-object v1, p0, Lkm;->jQ:Lkp;

    :cond_0
    iget-object v0, p0, Lkm;->jS:Lkp;

    if-nez v0, :cond_1

    iget-object v0, p0, Lkm;->jQ:Lkp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkm;->jQ:Lkp;

    invoke-static {v0}, Lkm;->a(Lkp;)[F

    move-result-object v0

    const/4 v1, 0x2

    const v2, 0x3e851eb8    # 0.26f

    aput v2, v0, v1

    new-instance v1, Lkp;

    invoke-static {v0}, Lkl;->b([F)I

    move-result v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lkp;-><init>(II)V

    iput-object v1, p0, Lkm;->jS:Lkp;

    .line 213
    :cond_1
    return-void
.end method

.method public static O(I)V
    .locals 2

    .prologue
    .line 517
    if-gtz p0, :cond_0

    .line 518
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numColors must be 1 of greater"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_0
    return-void
.end method

.method private a(FFFFFF)Lkp;
    .locals 10

    .prologue
    .line 329
    const/4 v3, 0x0

    .line 330
    const/4 v1, 0x0

    .line 332
    iget-object v0, p0, Lkm;->jO:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkp;

    .line 333
    invoke-virtual {v0}, Lkp;->bv()[F

    move-result-object v2

    const/4 v4, 0x1

    aget v4, v2, v4

    .line 334
    invoke-virtual {v0}, Lkp;->bv()[F

    move-result-object v2

    const/4 v5, 0x2

    aget v5, v2, v5

    .line 336
    cmpl-float v2, v4, p5

    if-ltz v2, :cond_5

    cmpg-float v2, v4, p6

    if-gtz v2, :cond_5

    cmpl-float v2, v5, p2

    if-ltz v2, :cond_5

    cmpg-float v2, v5, p3

    if-gtz v2, :cond_5

    iget-object v2, p0, Lkm;->jQ:Lkp;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lkm;->jS:Lkp;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lkm;->jU:Lkp;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lkm;->jR:Lkp;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lkm;->jT:Lkp;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lkm;->jV:Lkp;

    if-ne v2, v0, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_5

    .line 339
    iget v2, v0, Lkp;->kc:I

    iget v7, p0, Lkm;->jP:I

    const/4 v8, 0x6

    new-array v8, v8, [F

    const/4 v9, 0x0

    invoke-static {v4, p4}, Lkm;->f(FF)F

    move-result v4

    aput v4, v8, v9

    const/4 v4, 0x1

    const/high16 v9, 0x40400000    # 3.0f

    aput v9, v8, v4

    const/4 v4, 0x2

    invoke-static {v5, p1}, Lkm;->f(FF)F

    move-result v5

    aput v5, v8, v4

    const/4 v4, 0x3

    const/high16 v5, 0x40c00000    # 6.0f

    aput v5, v8, v4

    const/4 v4, 0x4

    int-to-float v2, v2

    int-to-float v5, v7

    div-float/2addr v2, v5

    aput v2, v8, v4

    const/4 v2, 0x5

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v8, v2

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_2
    array-length v7, v8

    if-ge v2, v7, :cond_2

    aget v7, v8, v2

    add-int/lit8 v9, v2, 0x1

    aget v9, v8, v9

    mul-float/2addr v7, v9

    add-float/2addr v5, v7

    add-float/2addr v4, v9

    add-int/lit8 v2, v2, 0x2

    goto :goto_2

    .line 336
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 339
    :cond_2
    div-float v2, v5, v4

    .line 341
    if-eqz v3, :cond_3

    cmpl-float v4, v2, v1

    if-lez v4, :cond_5

    :cond_3
    move-object v1, v0

    move v0, v2

    :goto_3
    move-object v3, v1

    move v1, v0

    .line 346
    goto/16 :goto_0

    .line 348
    :cond_4
    return-object v3

    :cond_5
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method

.method public static a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 508
    if-nez p0, :cond_0

    .line 509
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bitmap can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 511
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 512
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bitmap can not be recycled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 514
    :cond_1
    return-void
.end method

.method private static a(Lkp;)[F
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 475
    new-array v0, v3, [F

    .line 476
    invoke-virtual {p0}, Lkp;->bv()[F

    move-result-object v1

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 477
    return-object v0
.end method

.method public static b(Landroid/graphics/Bitmap;I)Lkm;
    .locals 3

    .prologue
    .line 128
    invoke-static {p0}, Lkm;->a(Landroid/graphics/Bitmap;)V

    .line 129
    invoke-static {p1}, Lkm;->O(I)V

    .line 132
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x64

    if-gt v0, v1, :cond_1

    move-object v0, p0

    .line 135
    :goto_0
    invoke-static {v0, p1}, Lkh;->a(Landroid/graphics/Bitmap;I)Lkh;

    move-result-object v1

    .line 138
    if-eq v0, p0, :cond_0

    .line 139
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 143
    :cond_0
    new-instance v0, Lkm;

    invoke-virtual {v1}, Lkh;->bp()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lkm;-><init>(Ljava/util/List;)V

    return-object v0

    .line 132
    :cond_1
    const/high16 v1, 0x42c80000    # 100.0f

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private bu()I
    .locals 3

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 381
    iget-object v1, p0, Lkm;->jO:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkp;

    .line 382
    iget v0, v0, Lkp;->kc:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 383
    goto :goto_0

    .line 384
    :cond_0
    return v1
.end method

.method private static f(FF)F
    .locals 2

    .prologue
    .line 489
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    if-ne p0, p1, :cond_1

    .line 426
    :cond_0
    :goto_0
    return v0

    .line 392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 393
    goto :goto_0

    .line 396
    :cond_3
    check-cast p1, Lkm;

    .line 398
    iget-object v2, p0, Lkm;->jO:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lkm;->jO:Ljava/util/List;

    iget-object v3, p1, Lkm;->jO:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 399
    goto :goto_0

    .line 398
    :cond_5
    iget-object v2, p1, Lkm;->jO:Ljava/util/List;

    if-nez v2, :cond_4

    .line 401
    :cond_6
    iget-object v2, p0, Lkm;->jT:Lkp;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lkm;->jT:Lkp;

    iget-object v3, p1, Lkm;->jT:Lkp;

    invoke-virtual {v2, v3}, Lkp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 403
    goto :goto_0

    .line 401
    :cond_8
    iget-object v2, p1, Lkm;->jT:Lkp;

    if-nez v2, :cond_7

    .line 405
    :cond_9
    iget-object v2, p0, Lkm;->jS:Lkp;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lkm;->jS:Lkp;

    iget-object v3, p1, Lkm;->jS:Lkp;

    invoke-virtual {v2, v3}, Lkp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 407
    goto :goto_0

    .line 405
    :cond_b
    iget-object v2, p1, Lkm;->jS:Lkp;

    if-nez v2, :cond_a

    .line 409
    :cond_c
    iget-object v2, p0, Lkm;->jV:Lkp;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lkm;->jV:Lkp;

    iget-object v3, p1, Lkm;->jV:Lkp;

    invoke-virtual {v2, v3}, Lkp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 411
    goto :goto_0

    .line 409
    :cond_e
    iget-object v2, p1, Lkm;->jV:Lkp;

    if-nez v2, :cond_d

    .line 413
    :cond_f
    iget-object v2, p0, Lkm;->jU:Lkp;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lkm;->jU:Lkp;

    iget-object v3, p1, Lkm;->jU:Lkp;

    invoke-virtual {v2, v3}, Lkp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 415
    goto :goto_0

    .line 413
    :cond_11
    iget-object v2, p1, Lkm;->jU:Lkp;

    if-nez v2, :cond_10

    .line 417
    :cond_12
    iget-object v2, p0, Lkm;->jR:Lkp;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lkm;->jR:Lkp;

    iget-object v3, p1, Lkm;->jR:Lkp;

    invoke-virtual {v2, v3}, Lkp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 419
    goto/16 :goto_0

    .line 417
    :cond_14
    iget-object v2, p1, Lkm;->jR:Lkp;

    if-nez v2, :cond_13

    .line 421
    :cond_15
    iget-object v2, p0, Lkm;->jQ:Lkp;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lkm;->jQ:Lkp;

    iget-object v3, p1, Lkm;->jQ:Lkp;

    invoke-virtual {v2, v3}, Lkp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 423
    goto/16 :goto_0

    .line 421
    :cond_16
    iget-object v2, p1, Lkm;->jQ:Lkp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 431
    iget-object v0, p0, Lkm;->jO:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkm;->jO:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 432
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lkm;->jQ:Lkp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkm;->jQ:Lkp;

    invoke-virtual {v0}, Lkp;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 433
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lkm;->jR:Lkp;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkm;->jR:Lkp;

    invoke-virtual {v0}, Lkp;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 434
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lkm;->jS:Lkp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkm;->jS:Lkp;

    invoke-virtual {v0}, Lkp;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 435
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lkm;->jT:Lkp;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lkm;->jT:Lkp;

    invoke-virtual {v0}, Lkp;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 436
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lkm;->jU:Lkp;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lkm;->jU:Lkp;

    invoke-virtual {v0}, Lkp;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 437
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lkm;->jV:Lkp;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lkm;->jV:Lkp;

    invoke-virtual {v1}, Lkp;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 438
    return v0

    :cond_1
    move v0, v1

    .line 431
    goto :goto_0

    :cond_2
    move v0, v1

    .line 432
    goto :goto_1

    :cond_3
    move v0, v1

    .line 433
    goto :goto_2

    :cond_4
    move v0, v1

    .line 434
    goto :goto_3

    :cond_5
    move v0, v1

    .line 435
    goto :goto_4

    :cond_6
    move v0, v1

    .line 436
    goto :goto_5
.end method
