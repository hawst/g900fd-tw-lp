.class public final Ljla;
.super Ljsl;
.source "PG"


# static fields
.field public static final eqX:Ljsm;


# instance fields
.field private aez:I

.field private eqY:Ljava/lang/String;

.field private eqZ:Ljava/lang/String;

.field private era:Ljlc;

.field private erb:Ljlq;

.field private erc:Ljava/lang/String;

.field private erd:Ljpe;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 4002
    const/16 v0, 0xb

    const-class v1, Ljla;

    const v2, 0xc8fb202

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljla;->eqX:Ljsm;

    .line 4008
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4090
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4091
    const/4 v0, 0x0

    iput v0, p0, Ljla;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljla;->eqY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljla;->eqZ:Ljava/lang/String;

    iput-object v1, p0, Ljla;->era:Ljlc;

    iput-object v1, p0, Ljla;->erb:Ljlq;

    const-string v0, ""

    iput-object v0, p0, Ljla;->erc:Ljava/lang/String;

    iput-object v1, p0, Ljla;->erd:Ljpe;

    iput-object v1, p0, Ljla;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljla;->eCz:I

    .line 4092
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3995
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljla;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljla;->eqY:Ljava/lang/String;

    iget v0, p0, Ljla;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljla;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljla;->eqZ:Ljava/lang/String;

    iget v0, p0, Ljla;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljla;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljla;->era:Ljlc;

    if-nez v0, :cond_1

    new-instance v0, Ljlc;

    invoke-direct {v0}, Ljlc;-><init>()V

    iput-object v0, p0, Ljla;->era:Ljlc;

    :cond_1
    iget-object v0, p0, Ljla;->era:Ljlc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljla;->erb:Ljlq;

    if-nez v0, :cond_2

    new-instance v0, Ljlq;

    invoke-direct {v0}, Ljlq;-><init>()V

    iput-object v0, p0, Ljla;->erb:Ljlq;

    :cond_2
    iget-object v0, p0, Ljla;->erb:Ljlq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljla;->erc:Ljava/lang/String;

    iget v0, p0, Ljla;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljla;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljla;->erd:Ljpe;

    if-nez v0, :cond_3

    new-instance v0, Ljpe;

    invoke-direct {v0}, Ljpe;-><init>()V

    iput-object v0, p0, Ljla;->erd:Ljpe;

    :cond_3
    iget-object v0, p0, Ljla;->erd:Ljpe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4110
    iget v0, p0, Ljla;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4111
    const/4 v0, 0x1

    iget-object v1, p0, Ljla;->eqY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4113
    :cond_0
    iget v0, p0, Ljla;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4114
    const/4 v0, 0x2

    iget-object v1, p0, Ljla;->eqZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4116
    :cond_1
    iget-object v0, p0, Ljla;->era:Ljlc;

    if-eqz v0, :cond_2

    .line 4117
    const/4 v0, 0x3

    iget-object v1, p0, Ljla;->era:Ljlc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4119
    :cond_2
    iget-object v0, p0, Ljla;->erb:Ljlq;

    if-eqz v0, :cond_3

    .line 4120
    const/4 v0, 0x4

    iget-object v1, p0, Ljla;->erb:Ljlq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4122
    :cond_3
    iget v0, p0, Ljla;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 4123
    const/4 v0, 0x5

    iget-object v1, p0, Ljla;->erc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4125
    :cond_4
    iget-object v0, p0, Ljla;->erd:Ljpe;

    if-eqz v0, :cond_5

    .line 4126
    const/4 v0, 0x6

    iget-object v1, p0, Ljla;->erd:Ljpe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4128
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4129
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4133
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4134
    iget v1, p0, Ljla;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4135
    const/4 v1, 0x1

    iget-object v2, p0, Ljla;->eqY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4138
    :cond_0
    iget v1, p0, Ljla;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4139
    const/4 v1, 0x2

    iget-object v2, p0, Ljla;->eqZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4142
    :cond_1
    iget-object v1, p0, Ljla;->era:Ljlc;

    if-eqz v1, :cond_2

    .line 4143
    const/4 v1, 0x3

    iget-object v2, p0, Ljla;->era:Ljlc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4146
    :cond_2
    iget-object v1, p0, Ljla;->erb:Ljlq;

    if-eqz v1, :cond_3

    .line 4147
    const/4 v1, 0x4

    iget-object v2, p0, Ljla;->erb:Ljlq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4150
    :cond_3
    iget v1, p0, Ljla;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 4151
    const/4 v1, 0x5

    iget-object v2, p0, Ljla;->erc:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4154
    :cond_4
    iget-object v1, p0, Ljla;->erd:Ljpe;

    if-eqz v1, :cond_5

    .line 4155
    const/4 v1, 0x6

    iget-object v2, p0, Ljla;->erd:Ljpe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4158
    :cond_5
    return v0
.end method
