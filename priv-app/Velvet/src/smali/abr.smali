.class final enum Labr;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum Pc:Labr;

.field public static final enum Pd:Labr;

.field public static final enum Pe:Labr;

.field public static final enum Pf:Labr;

.field public static final enum Pg:Labr;

.field public static final enum Ph:Labr;

.field public static final enum Pi:Labr;

.field public static final enum Pj:Labr;

.field public static final enum Pk:Labr;

.field public static final enum Pl:Labr;

.field private static final synthetic Pm:[Labr;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Labr;

    const-string v1, "TRANSLATION_X"

    invoke-direct {v0, v1, v3}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pc:Labr;

    .line 31
    new-instance v0, Labr;

    const-string v1, "TRANSLATION_Y"

    invoke-direct {v0, v1, v4}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pd:Labr;

    .line 32
    new-instance v0, Labr;

    const-string v1, "SCALE_X"

    invoke-direct {v0, v1, v5}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pe:Labr;

    .line 33
    new-instance v0, Labr;

    const-string v1, "SCALE_Y"

    invoke-direct {v0, v1, v6}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pf:Labr;

    .line 34
    new-instance v0, Labr;

    const-string v1, "ROTATION_Y"

    invoke-direct {v0, v1, v7}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pg:Labr;

    .line 35
    new-instance v0, Labr;

    const-string v1, "ALPHA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Ph:Labr;

    .line 36
    new-instance v0, Labr;

    const-string v1, "START_DELAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pi:Labr;

    .line 37
    new-instance v0, Labr;

    const-string v1, "DURATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pj:Labr;

    .line 38
    new-instance v0, Labr;

    const-string v1, "INTERPOLATOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pk:Labr;

    .line 39
    new-instance v0, Labr;

    const-string v1, "WITH_LAYER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Labr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Labr;->Pl:Labr;

    .line 29
    const/16 v0, 0xa

    new-array v0, v0, [Labr;

    sget-object v1, Labr;->Pc:Labr;

    aput-object v1, v0, v3

    sget-object v1, Labr;->Pd:Labr;

    aput-object v1, v0, v4

    sget-object v1, Labr;->Pe:Labr;

    aput-object v1, v0, v5

    sget-object v1, Labr;->Pf:Labr;

    aput-object v1, v0, v6

    sget-object v1, Labr;->Pg:Labr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Labr;->Ph:Labr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Labr;->Pi:Labr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Labr;->Pj:Labr;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Labr;->Pk:Labr;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Labr;->Pl:Labr;

    aput-object v2, v0, v1

    sput-object v0, Labr;->Pm:[Labr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Labr;
    .locals 1

    .prologue
    .line 29
    const-class v0, Labr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Labr;

    return-object v0
.end method

.method public static values()[Labr;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Labr;->Pm:[Labr;

    invoke-virtual {v0}, [Labr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Labr;

    return-object v0
.end method
