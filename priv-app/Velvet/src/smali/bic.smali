.class final Lbic;
.super Lck;

# interfaces
.implements Lbhk;
.implements Lbhl;


# instance fields
.field public final aBT:Lbhi;

.field aBU:Z

.field private aBV:Lbgm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbhi;)V
    .locals 0

    invoke-direct {p0, p1}, Lck;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lbic;->aBT:Lbhi;

    return-void
.end method

.method private b(Lbgm;)V
    .locals 1

    iput-object p1, p0, Lbic;->aBV:Lbgm;

    iget-boolean v0, p0, Lck;->cq:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lck;->dC:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lbic;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lbgm;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbic;->aBU:Z

    invoke-direct {p0, p1}, Lbic;->b(Lbgm;)V

    return-void
.end method

.method public final dw(I)V
    .locals 0

    return-void
.end method

.method public final j(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbic;->aBU:Z

    sget-object v0, Lbgm;->aAI:Lbgm;

    invoke-direct {p0, v0}, Lbic;->b(Lbgm;)V

    return-void
.end method

.method protected final onReset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbic;->aBV:Lbgm;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbic;->aBU:Z

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->b(Lbhk;)V

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->b(Lbhl;)V

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0}, Lbhi;->disconnect()V

    return-void
.end method

.method protected final onStartLoading()V
    .locals 1

    invoke-super {p0}, Lck;->onStartLoading()V

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->a(Lbhk;)V

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->a(Lbhl;)V

    iget-object v0, p0, Lbic;->aBV:Lbgm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbic;->aBV:Lbgm;

    invoke-virtual {p0, v0}, Lbic;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbic;->aBU:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0}, Lbhi;->connect()V

    :cond_1
    return-void
.end method

.method protected final onStopLoading()V
    .locals 1

    iget-object v0, p0, Lbic;->aBT:Lbhi;

    invoke-interface {v0}, Lbhi;->disconnect()V

    return-void
.end method
