.class public final Limd;
.super Liiw;
.source "PG"


# instance fields
.field dEn:Z

.field dEo:I

.field dEp:I

.field dEq:I

.field dEr:Linz;

.field dEs:Linz;

.field dEt:J

.field dEu:J

.field dEv:Limf;

.field dEw:Lifc;

.field dEx:Lifc;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 145
    invoke-direct {p0}, Liiw;-><init>()V

    .line 124
    iput v0, p0, Limd;->dEo:I

    .line 125
    iput v0, p0, Limd;->dEp:I

    .line 126
    iput v0, p0, Limd;->dEq:I

    .line 131
    iput-wide v2, p0, Limd;->dEt:J

    .line 132
    iput-wide v2, p0, Limd;->dEu:J

    .line 145
    return-void
.end method


# virtual methods
.method final aXp()I
    .locals 2

    .prologue
    .line 212
    iget v0, p0, Limd;->dEo:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Limd;->dEo:I

    goto :goto_0
.end method

.method final aXq()I
    .locals 2

    .prologue
    .line 280
    iget v0, p0, Limd;->dEp:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Limd;->dEp:I

    goto :goto_0
.end method

.method final aXr()Linz;
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Limd;->dEr:Linz;

    sget-object v1, Linz;->dFI:Linz;

    invoke-static {v0, v1}, Lifo;->n(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linz;

    return-object v0
.end method

.method final aXs()Linz;
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Limd;->dEs:Linz;

    sget-object v1, Linz;->dFI:Linz;

    invoke-static {v0, v1}, Lifo;->n(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linz;

    return-object v0
.end method

.method public final aXt()Ljava/util/concurrent/ConcurrentMap;
    .locals 4

    .prologue
    .line 588
    iget-boolean v0, p0, Limd;->dEn:Z

    if-nez v0, :cond_0

    .line 589
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Limd;->aXp()I

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-virtual {p0}, Limd;->aXq()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    .line 591
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Limd;->dEv:Limf;

    if-nez v0, :cond_1

    new-instance v0, Limn;

    invoke-direct {v0, p0}, Limn;-><init>(Limd;)V

    :goto_1
    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    goto :goto_0

    :cond_1
    new-instance v0, Lime;

    invoke-direct {v0, p0}, Lime;-><init>(Limd;)V

    goto :goto_1
.end method

.method e(JLjava/util/concurrent/TimeUnit;)V
    .locals 11

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    iget-wide v4, p0, Limd;->dEt:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "expireAfterWrite was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Limd;->dEt:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 479
    iget-wide v4, p0, Limd;->dEu:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "expireAfterAccess was already set to %s ns"

    new-array v4, v1, [Ljava/lang/Object;

    iget-wide v6, p0, Limd;->dEu:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lifv;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 481
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-ltz v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "duration cannot be negative: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 482
    return-void

    :cond_0
    move v0, v2

    .line 477
    goto :goto_0

    :cond_1
    move v0, v2

    .line 479
    goto :goto_1

    :cond_2
    move v0, v2

    .line 481
    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, -0x1

    .line 680
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v0

    .line 681
    iget v1, p0, Limd;->dEo:I

    if-eq v1, v3, :cond_0

    .line 682
    const-string v1, "initialCapacity"

    iget v2, p0, Limd;->dEo:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    .line 684
    :cond_0
    iget v1, p0, Limd;->dEp:I

    if-eq v1, v3, :cond_1

    .line 685
    const-string v1, "concurrencyLevel"

    iget v2, p0, Limd;->dEp:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    .line 687
    :cond_1
    iget v1, p0, Limd;->dEq:I

    if-eq v1, v3, :cond_2

    .line 688
    const-string v1, "maximumSize"

    iget v2, p0, Limd;->dEq:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    .line 690
    :cond_2
    iget-wide v2, p0, Limd;->dEt:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 691
    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Limd;->dEt:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 693
    :cond_3
    iget-wide v2, p0, Limd;->dEu:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 694
    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Limd;->dEu:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 696
    :cond_4
    iget-object v1, p0, Limd;->dEr:Linz;

    if-eqz v1, :cond_5

    .line 697
    const-string v1, "keyStrength"

    iget-object v2, p0, Limd;->dEr:Linz;

    invoke-virtual {v2}, Linz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Liez;->po(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 699
    :cond_5
    iget-object v1, p0, Limd;->dEs:Linz;

    if-eqz v1, :cond_6

    .line 700
    const-string v1, "valueStrength"

    iget-object v2, p0, Limd;->dEs:Linz;

    invoke-virtual {v2}, Linz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Liez;->po(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 702
    :cond_6
    iget-object v1, p0, Limd;->dEw:Lifc;

    if-eqz v1, :cond_7

    .line 703
    const-string v1, "keyEquivalence"

    invoke-virtual {v0, v1}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    .line 705
    :cond_7
    iget-object v1, p0, Limd;->dEx:Lifc;

    if-eqz v1, :cond_8

    .line 706
    const-string v1, "valueEquivalence"

    invoke-virtual {v0, v1}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    .line 708
    :cond_8
    iget-object v1, p0, Limd;->dCT:Liml;

    if-eqz v1, :cond_9

    .line 709
    const-string v1, "removalListener"

    invoke-virtual {v0, v1}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    .line 711
    :cond_9
    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
