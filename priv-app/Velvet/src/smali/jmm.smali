.class public final Ljmm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eub:[Ljmm;


# instance fields
.field private aez:I

.field private aji:Ljava/lang/String;

.field private eth:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6646
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6647
    const/4 v0, 0x0

    iput v0, p0, Ljmm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmm;->eth:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmm;->aji:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljmm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmm;->eCz:I

    .line 6648
    return-void
.end method

.method public static bql()[Ljmm;
    .locals 2

    .prologue
    .line 6589
    sget-object v0, Ljmm;->eub:[Ljmm;

    if-nez v0, :cond_1

    .line 6590
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 6592
    :try_start_0
    sget-object v0, Ljmm;->eub:[Ljmm;

    if-nez v0, :cond_0

    .line 6593
    const/4 v0, 0x0

    new-array v0, v0, [Ljmm;

    sput-object v0, Ljmm;->eub:[Ljmm;

    .line 6595
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6597
    :cond_1
    sget-object v0, Ljmm;->eub:[Ljmm;

    return-object v0

    .line 6595
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 6583
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmm;->eth:Ljava/lang/String;

    iget v0, p0, Ljmm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmm;->aji:Ljava/lang/String;

    iget v0, p0, Ljmm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmm;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6662
    iget v0, p0, Ljmm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6663
    const/4 v0, 0x1

    iget-object v1, p0, Ljmm;->eth:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6665
    :cond_0
    iget v0, p0, Ljmm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6666
    const/4 v0, 0x2

    iget-object v1, p0, Ljmm;->aji:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6668
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6669
    return-void
.end method

.method public final bqm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6605
    iget-object v0, p0, Ljmm;->eth:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6673
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6674
    iget v1, p0, Ljmm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6675
    const/4 v1, 0x1

    iget-object v2, p0, Ljmm;->eth:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6678
    :cond_0
    iget v1, p0, Ljmm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6679
    const/4 v1, 0x2

    iget-object v2, p0, Ljmm;->aji:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6682
    :cond_1
    return v0
.end method

.method public final qw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6627
    iget-object v0, p0, Ljmm;->aji:Ljava/lang/String;

    return-object v0
.end method

.method public final xf(Ljava/lang/String;)Ljmm;
    .locals 1

    .prologue
    .line 6630
    if-nez p1, :cond_0

    .line 6631
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6633
    :cond_0
    iput-object p1, p0, Ljmm;->aji:Ljava/lang/String;

    .line 6634
    iget v0, p0, Ljmm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmm;->aez:I

    .line 6635
    return-object p0
.end method
