.class public final Lhgr;
.super Landroid/service/voice/AlwaysOnHotwordDetector$Callback;
.source "PG"


# instance fields
.field private synthetic dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;


# direct methods
.method public constructor <init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-direct {p0}, Landroid/service/voice/AlwaysOnHotwordDetector$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAvailabilityChanged(I)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0, p1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;I)I

    .line 164
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->c(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V

    .line 165
    return-void
.end method

.method public final onDetected(Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)V
    .locals 8

    .prologue
    const/4 v1, 0x2

    const/4 v7, 0x0

    .line 107
    invoke-virtual {p1}, Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;->getTriggerAudio()[B

    move-result-object v2

    .line 108
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0, p1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)I

    move-result v3

    .line 112
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 124
    :goto_0
    invoke-virtual {p1}, Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;->getCaptureAudioFormat()Landroid/media/AudioFormat;

    move-result-object v4

    .line 125
    if-eqz v4, :cond_3

    .line 126
    invoke-virtual {v4}, Landroid/media/AudioFormat;->getChannelMask()I

    move-result v5

    const/16 v6, 0x10

    if-ne v5, v6, :cond_0

    invoke-virtual {v4}, Landroid/media/AudioFormat;->getEncoding()I

    move-result v4

    if-eq v4, v1, :cond_3

    .line 128
    :cond_0
    const-string v0, "GsaVoiceInteractionSrv"

    const-string v1, "Invalid DSP audio format."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 143
    :goto_1
    return-void

    .line 118
    :cond_1
    if-nez v2, :cond_2

    if-gez v3, :cond_2

    .line 119
    const-string v0, "GsaVoiceInteractionSrv"

    const-string v1, "No trigger audio present"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 122
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    :cond_3
    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    const-class v5, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 137
    const-string v4, "com.google.android.search.core.action.HOTWORD_TRIGGERED_ON_DSP"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string v4, "com.google.android.search.core.extra.HOTWORD_BYTES"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 139
    const-string v2, "com.google.android.search.core.extra.HOTWORD_VERIFICATION_MODE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 140
    const-string v0, "com.google.android.search.core.extra.HOTWORD_SAMPLING_RATE"

    iget-object v2, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v2, p1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 141
    const-string v0, "com.google.android.search.core.extra.HOTWORD_CAPTURE_SESSION"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 142
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-virtual {v0, v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method

.method public final onError()V
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 156
    iget-object v0, p0, Lhgr;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;J)V

    .line 158
    :cond_0
    return-void
.end method

.method public final onRecognitionPaused()V
    .locals 3

    .prologue
    .line 169
    const-string v0, "GsaVoiceInteractionSrv"

    const-string v1, "onRecognitionPaused"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 170
    return-void
.end method

.method public final onRecognitionResumed()V
    .locals 3

    .prologue
    .line 174
    const-string v0, "GsaVoiceInteractionSrv"

    const-string v1, "onRecognitionResumed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 175
    return-void
.end method
