.class public final Ljsx;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eCY:[Ljsx;


# instance fields
.field private aez:I

.field private eCZ:Z

.field private eDa:[I

.field private eDb:[I

.field private eDc:I

.field private eiV:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 459
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 460
    iput v1, p0, Ljsx;->aez:I

    iput v1, p0, Ljsx;->eiV:I

    iput-boolean v1, p0, Ljsx;->eCZ:Z

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljsx;->eDa:[I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljsx;->eDb:[I

    iput v1, p0, Ljsx;->eDc:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljsx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljsx;->eCz:I

    .line 461
    return-void
.end method

.method public static bul()[Ljsx;
    .locals 2

    .prologue
    .line 383
    sget-object v0, Ljsx;->eCY:[Ljsx;

    if-nez v0, :cond_1

    .line 384
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 386
    :try_start_0
    sget-object v0, Ljsx;->eCY:[Ljsx;

    if-nez v0, :cond_0

    .line 387
    const/4 v0, 0x0

    new-array v0, v0, [Ljsx;

    sput-object v0, Ljsx;->eCY:[Ljsx;

    .line 389
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_1
    sget-object v0, Ljsx;->eCY:[Ljsx;

    return-object v0

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 377
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljsx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljsx;->eiV:I

    iget v0, p0, Ljsx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsx;->eCZ:Z

    iget v0, p0, Ljsx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsx;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1d

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljsx;->eDa:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljsx;->eDa:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljsx;->eDa:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljsx;->eDa:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Ljsx;->eDa:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [I

    if-eqz v0, :cond_4

    iget-object v4, p0, Ljsx;->eDa:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljsx;->eDa:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    iput-object v3, p0, Ljsx;->eDa:[I

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x25

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljsx;->eDb:[I

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljsx;->eDb:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljsx;->eDb:[I

    array-length v0, v0

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljsx;->eDb:[I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Ljsx;->eDb:[I

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v3, v0

    new-array v3, v3, [I

    if-eqz v0, :cond_a

    iget-object v4, p0, Ljsx;->eDb:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v4, v3

    if-ge v0, v4, :cond_c

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Ljsx;->eDb:[I

    array-length v0, v0

    goto :goto_7

    :cond_c
    iput-object v3, p0, Ljsx;->eDb:[I

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsx;->eDc:I

    iget v0, p0, Ljsx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsx;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_4
        0x1d -> :sswitch_3
        0x22 -> :sswitch_6
        0x25 -> :sswitch_5
        0x28 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 478
    iget v0, p0, Ljsx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x1

    iget v2, p0, Ljsx;->eiV:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 481
    :cond_0
    iget v0, p0, Ljsx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 482
    const/4 v0, 0x2

    iget-boolean v2, p0, Ljsx;->eCZ:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 484
    :cond_1
    iget-object v0, p0, Ljsx;->eDa:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljsx;->eDa:[I

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 485
    :goto_0
    iget-object v2, p0, Ljsx;->eDa:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 486
    const/4 v2, 0x3

    iget-object v3, p0, Ljsx;->eDa:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->br(II)V

    .line 485
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 489
    :cond_2
    iget-object v0, p0, Ljsx;->eDb:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljsx;->eDb:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 490
    :goto_1
    iget-object v0, p0, Ljsx;->eDb:[I

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 491
    const/4 v0, 0x4

    iget-object v2, p0, Ljsx;->eDb:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->br(II)V

    .line 490
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 494
    :cond_3
    iget v0, p0, Ljsx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 495
    const/4 v0, 0x5

    iget v1, p0, Ljsx;->eDc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 497
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 498
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 502
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 503
    iget v1, p0, Ljsx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 504
    const/4 v1, 0x1

    iget v2, p0, Ljsx;->eiV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 507
    :cond_0
    iget v1, p0, Ljsx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 508
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljsx;->eCZ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 511
    :cond_1
    iget-object v1, p0, Ljsx;->eDa:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljsx;->eDa:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 512
    iget-object v1, p0, Ljsx;->eDa:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 513
    add-int/2addr v0, v1

    .line 514
    iget-object v1, p0, Ljsx;->eDa:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 516
    :cond_2
    iget-object v1, p0, Ljsx;->eDb:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljsx;->eDb:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 517
    iget-object v1, p0, Ljsx;->eDb:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 518
    add-int/2addr v0, v1

    .line 519
    iget-object v1, p0, Ljsx;->eDb:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 521
    :cond_3
    iget v1, p0, Ljsx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 522
    const/4 v1, 0x5

    iget v2, p0, Ljsx;->eDc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_4
    return v0
.end method
