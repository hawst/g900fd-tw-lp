.class final enum Ligs;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum dBI:Ligs;

.field public static final enum dBJ:Ligs;

.field public static final enum dBK:Ligs;

.field public static final enum dBL:Ligs;

.field private static final synthetic dBM:[Ligs;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Ligs;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Ligs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligs;->dBI:Ligs;

    .line 75
    new-instance v0, Ligs;

    const-string v1, "NOT_READY"

    invoke-direct {v0, v1, v3}, Ligs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligs;->dBJ:Ligs;

    .line 78
    new-instance v0, Ligs;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Ligs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligs;->dBK:Ligs;

    .line 81
    new-instance v0, Ligs;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Ligs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ligs;->dBL:Ligs;

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [Ligs;

    sget-object v1, Ligs;->dBI:Ligs;

    aput-object v1, v0, v2

    sget-object v1, Ligs;->dBJ:Ligs;

    aput-object v1, v0, v3

    sget-object v1, Ligs;->dBK:Ligs;

    aput-object v1, v0, v4

    sget-object v1, Ligs;->dBL:Ligs;

    aput-object v1, v0, v5

    sput-object v0, Ligs;->dBM:[Ligs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ligs;
    .locals 1

    .prologue
    .line 70
    const-class v0, Ligs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ligs;

    return-object v0
.end method

.method public static values()[Ligs;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Ligs;->dBM:[Ligs;

    invoke-virtual {v0}, [Ligs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ligs;

    return-object v0
.end method
