.class public final Lglt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cOR:I


# instance fields
.field private final bhO:Lggg;

.field private final cOP:Lgjo;

.field private final cOQ:Lglp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, -0x1

    sput v0, Lglt;->cOR:I

    return-void
.end method

.method constructor <init>(Lgjo;Lggg;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lglt;->cOP:Lgjo;

    .line 74
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggg;

    iput-object v0, p0, Lglt;->bhO:Lggg;

    .line 75
    new-instance v0, Lglp;

    invoke-direct {v0}, Lglp;-><init>()V

    iput-object v0, p0, Lglt;->cOQ:Lglp;

    .line 76
    return-void
.end method

.method private a([Ljava/lang/String;I)Ljww;
    .locals 13

    .prologue
    const/4 v4, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 217
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v0, 0xf

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 219
    const-wide/16 v0, 0x0

    .line 220
    sget v3, Lglt;->cOR:I

    move-object v2, v4

    .line 221
    :goto_0
    array-length v6, p1

    if-ge p2, v6, :cond_3

    .line 222
    aget-object v6, p1, p2

    .line 224
    const-string v7, "XX_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 225
    invoke-static {v6}, Lgle;->nd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 226
    invoke-static {v6}, Lgle;->ne(Ljava/lang/String;)D

    move-result-wide v0

    .line 229
    :cond_0
    const-string v7, "_p"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 231
    const/4 v7, 0x2

    :try_start_0
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 238
    :cond_1
    :goto_1
    const-string v7, "_d"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 239
    invoke-virtual {v6, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 234
    :catch_0
    move-exception v7

    const-string v7, "RecognizerEventProcessor"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Invalid semantic tag: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 244
    :cond_3
    if-eqz v2, :cond_b

    .line 246
    new-instance v5, Ljks;

    invoke-direct {v5}, Ljks;-><init>()V

    invoke-virtual {v5, v2}, Ljks;->wq(Ljava/lang/String;)Ljks;

    invoke-virtual {v5, v2}, Ljks;->wr(Ljava/lang/String;)Ljks;

    new-instance v6, Ljyz;

    invoke-direct {v6}, Ljyz;-><init>()V

    invoke-virtual {v6, v0, v1}, Ljyz;->x(D)Ljyz;

    sget-object v0, Ljyy;->eMR:Ljsm;

    invoke-virtual {v5, v0, v6}, Ljks;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    const-string v1, "RecognizerEventProcessor"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v0, "n="

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v12, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "+"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", t="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    sget v0, Lglt;->cOR:I

    if-eq v3, v0, :cond_6

    new-instance v0, Ljlc;

    invoke-direct {v0}, Ljlc;-><init>()V

    if-ne v3, v11, :cond_9

    const-string v4, "home"

    :cond_4
    :goto_3
    if-eqz v4, :cond_5

    invoke-virtual {v0, v4}, Ljlc;->wC(Ljava/lang/String;)Ljlc;

    :cond_5
    new-array v1, v11, [Ljlc;

    aput-object v0, v1, v10

    iput-object v1, v5, Ljks;->eqg:[Ljlc;

    :cond_6
    new-instance v0, Ljmg;

    invoke-direct {v0}, Ljmg;-><init>()V

    new-array v1, v11, [Ljks;

    aput-object v5, v1, v10

    iput-object v1, v0, Ljmg;->etw:[Ljks;

    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    new-instance v2, Ljlm;

    invoke-direct {v2}, Ljlm;-><init>()V

    iput-object v2, v1, Ljkt;->eqo:Ljlm;

    iget-object v2, v1, Ljkt;->eqo:Ljlm;

    invoke-virtual {v2, v11}, Ljlm;->ix(Z)Ljlm;

    sget-object v2, Ljmg;->etv:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    invoke-static {v1}, Lglt;->c(Ljkt;)Ljww;

    move-result-object v4

    .line 253
    :cond_7
    :goto_4
    return-object v4

    .line 246
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "+"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_9
    const/4 v1, 0x3

    if-ne v3, v1, :cond_a

    const-string v4, "work"

    goto :goto_3

    :cond_a
    if-ne v3, v12, :cond_4

    const-string v4, "cell"

    goto :goto_3

    .line 248
    :cond_b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 250
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljlc;

    invoke-direct {v1}, Ljlc;-><init>()V

    invoke-virtual {v1, v0}, Ljlc;->wB(Ljava/lang/String;)Ljlc;

    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    new-instance v2, Ljmg;

    invoke-direct {v2}, Ljmg;-><init>()V

    sget-object v3, Ljmg;->etv:Ljsm;

    invoke-virtual {v0, v3, v2}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    new-array v3, v11, [Ljks;

    iput-object v3, v2, Ljmg;->etw:[Ljks;

    iget-object v3, v2, Ljmg;->etw:[Ljks;

    new-instance v4, Ljks;

    invoke-direct {v4}, Ljks;-><init>()V

    aput-object v4, v3, v10

    iget-object v3, v2, Ljmg;->etw:[Ljks;

    aget-object v3, v3, v10

    new-array v4, v11, [Ljlc;

    iput-object v4, v3, Ljks;->eqg:[Ljlc;

    iget-object v2, v2, Ljmg;->etw:[Ljks;

    aget-object v2, v2, v10

    iget-object v2, v2, Ljks;->eqg:[Ljlc;

    aput-object v1, v2, v10

    invoke-static {v0}, Lglt;->c(Ljkt;)Ljww;

    move-result-object v4

    goto :goto_4
.end method

.method private static c(Ljww;)Lehu;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lehv;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lehv;-><init>(ILjww;)V

    return-object v0
.end method

.method private static c(Ljkt;)Ljww;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 336
    new-instance v0, Ljrp;

    invoke-direct {v0}, Ljrp;-><init>()V

    .line 337
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljrp;->rS(I)Ljrp;

    .line 338
    new-array v1, v2, [Ljkt;

    aput-object p0, v1, v3

    iput-object v1, v0, Ljrp;->eBt:[Ljkt;

    .line 341
    invoke-virtual {v0, v2}, Ljrp;->iY(Z)Ljrp;

    .line 343
    new-instance v1, Ljps;

    invoke-direct {v1}, Ljps;-><init>()V

    .line 344
    new-array v2, v2, [Ljrp;

    aput-object v0, v2, v3

    iput-object v2, v1, Ljps;->exV:[Ljrp;

    .line 346
    new-instance v0, Ljww;

    invoke-direct {v0}, Ljww;-><init>()V

    invoke-virtual {v0, v3}, Ljww;->sQ(I)Ljww;

    move-result-object v0

    new-instance v2, Ljxe;

    invoke-direct {v2}, Ljxe;-><init>()V

    sget-object v3, Ljxe;->eKo:Ljsm;

    invoke-virtual {v0, v3, v2}, Ljww;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    iput-object v1, v2, Ljxe;->eKp:Ljps;

    return-object v0
.end method

.method private static e(Ljvv;)Ljvv;
    .locals 2

    .prologue
    .line 350
    new-instance v0, Ljvv;

    invoke-direct {v0}, Ljvv;-><init>()V

    .line 352
    :try_start_0
    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 353
    invoke-static {p0, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    :goto_0
    iget-object v1, p0, Ljvv;->eIm:Ljvw;

    iput-object v1, v0, Ljvv;->eIo:Ljvw;

    .line 356
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method final d(Ljvv;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lglt;->cOP:Lgjo;

    sget-object v4, Lgjo;->cMK:Lgjo;

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lglt;->cOP:Lgjo;

    sget-object v4, Lgjo;->cMN:Lgjo;

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lglt;->cOP:Lgjo;

    sget-object v4, Lgjo;->cMP:Lgjo;

    if-ne v0, v4, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 85
    invoke-virtual {p1}, Ljvv;->bvb()Z

    move-result v0

    if-nez v0, :cond_3

    .line 86
    const-string v0, "RecognizerEventProcessor"

    const-string v2, "Received recognition event without type."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 100
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 79
    goto :goto_0

    .line 90
    :cond_3
    invoke-virtual {p1}, Ljvv;->getStatus()I

    move-result v0

    if-eqz v0, :cond_4

    .line 91
    const-string v0, "RecognizerEventProcessor"

    const-string v2, "Error from embedded recognizer."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 95
    :cond_4
    iget-object v0, p0, Lglt;->cOP:Lgjo;

    sget-object v4, Lgjo;->cMP:Lgjo;

    if-ne v0, v4, :cond_11

    .line 96
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p1, Ljvv;->eIm:Ljvw;

    if-eqz v0, :cond_5

    iget-object v2, p1, Ljvv;->eIm:Ljvw;

    iget-object v0, v2, Ljvw;->eIk:[Ljvs;

    array-length v4, v0

    if-gtz v4, :cond_6

    :cond_5
    move-object v0, v3

    :goto_2
    if-eqz v0, :cond_10

    iget-object v1, p0, Lglt;->bhO:Lggg;

    invoke-static {p1}, Lglt;->e(Ljvv;)Ljvv;

    move-result-object v2

    invoke-static {v2}, Lcel;->a(Ljvv;)Ljww;

    move-result-object v2

    invoke-static {v2}, Lglt;->c(Ljww;)Lehu;

    move-result-object v2

    invoke-interface {v1, v2}, Lggg;->a(Lehu;)V

    iget-object v1, p0, Lglt;->bhO:Lggg;

    invoke-static {v0}, Lglt;->c(Ljww;)Lehu;

    move-result-object v0

    invoke-interface {v1, v0}, Lggg;->a(Lehu;)V

    iget-object v0, p0, Lglt;->bhO:Lggg;

    invoke-static {}, Lcel;->Dp()Ljww;

    move-result-object v1

    invoke-static {v1}, Lglt;->c(Ljww;)Lehu;

    move-result-object v1

    invoke-interface {v0, v1}, Lggg;->a(Lehu;)V

    goto :goto_1

    :cond_6
    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_5

    iget-object v5, v2, Ljvw;->eIk:[Ljvs;

    aget-object v5, v5, v0

    iget-object v6, v5, Ljvs;->eHS:Ljvy;

    if-eqz v6, :cond_f

    iget-object v0, v5, Ljvs;->eHS:Ljvy;

    iget-object v0, v0, Ljvy;->eIu:[Lkmq;

    array-length v2, v0

    if-eqz v2, :cond_7

    aget-object v2, v0, v1

    iget-object v2, v2, Lkmq;->faK:[Lkmr;

    array-length v2, v2

    if-nez v2, :cond_8

    :cond_7
    move-object v0, v3

    goto :goto_2

    :cond_8
    aget-object v0, v0, v1

    iget-object v0, v0, Lkmq;->faK:[Lkmr;

    aget-object v0, v0, v1

    iget-object v2, v0, Lkmr;->value:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lkmr;->value:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    move-object v0, v3

    goto :goto_2

    :cond_a
    iget-object v0, v0, Lkmr;->value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    :goto_4
    array-length v4, v2

    if-ge v0, v4, :cond_e

    aget-object v4, v2, v0

    if-eqz v4, :cond_d

    const-string v5, "_call"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v2, v0}, Lglt;->a([Ljava/lang/String;I)Ljww;

    move-result-object v0

    goto :goto_2

    :cond_b
    const-string v5, "_cancel"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const-string v6, "_okay"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v5, v6

    const-string v6, "_call_back"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v5, v6

    const-string v6, "_respond"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v5, v6

    const-string v6, "_hotword"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v5, v6

    const-string v6, "_next"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    or-int/2addr v5, v6

    const-string v6, "_select"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v5, v6

    if-eqz v5, :cond_c

    new-instance v0, Ljww;

    invoke-direct {v0}, Ljww;-><init>()V

    invoke-virtual {v0, v1}, Ljww;->sQ(I)Ljww;

    move-result-object v0

    new-instance v1, Ljxg;

    invoke-direct {v1}, Ljxg;-><init>()V

    sget-object v2, Ljxg;->eKr:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljww;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    iput-object p1, v1, Ljxg;->eKs:Ljvv;

    goto/16 :goto_2

    :cond_c
    const-string v5, "_other"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_e
    move-object v0, v3

    goto/16 :goto_2

    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    :cond_10
    iget-object v0, p0, Lglt;->bhO:Lggg;

    new-instance v1, Leim;

    invoke-direct {v1}, Leim;-><init>()V

    invoke-interface {v0, v1}, Lggg;->b(Leiq;)V

    goto/16 :goto_1

    .line 98
    :cond_11
    iget-object v0, p0, Lglt;->cOQ:Lglp;

    invoke-virtual {v0, p1}, Lglp;->c(Ljvv;)V

    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_13

    iget-object v0, p0, Lglt;->cOQ:Lglp;

    invoke-virtual {v0}, Lglp;->aHi()Ljvv;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v1, p0, Lglt;->bhO:Lggg;

    invoke-static {v0}, Lcel;->a(Ljvv;)Ljww;

    move-result-object v0

    invoke-static {v0}, Lglt;->c(Ljww;)Lehu;

    move-result-object v0

    invoke-interface {v1, v0}, Lggg;->a(Lehu;)V

    :cond_12
    iget-object v0, p0, Lglt;->bhO:Lggg;

    invoke-static {}, Lcel;->Dp()Ljww;

    move-result-object v1

    invoke-static {v1}, Lglt;->c(Ljww;)Lehu;

    move-result-object v1

    invoke-interface {v0, v1}, Lggg;->a(Lehu;)V

    goto/16 :goto_1

    :cond_13
    iget-object v0, p0, Lglt;->bhO:Lggg;

    invoke-static {p1}, Lcel;->a(Ljvv;)Ljww;

    move-result-object v1

    invoke-static {v1}, Lglt;->c(Ljww;)Lehu;

    move-result-object v1

    invoke-interface {v0, v1}, Lggg;->a(Lehu;)V

    goto/16 :goto_1
.end method
