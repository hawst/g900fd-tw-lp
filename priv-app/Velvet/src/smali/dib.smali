.class public Ldib;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aVQ:Ldgc;

.field private final bxH:Ldgm;

.field private final byA:Ljava/lang/Object;

.field private byB:Ljava/util/Map;

.field private final mGsaConfigFlags:Lchk;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lchk;Ldgm;Ldgc;Lcke;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldib;->byA:Ljava/lang/Object;

    .line 55
    iput-object p1, p0, Ldib;->mGsaConfigFlags:Lchk;

    .line 56
    iput-object p2, p0, Ldib;->bxH:Ldgm;

    .line 57
    iput-object p3, p0, Ldib;->aVQ:Ldgc;

    .line 58
    iput-object p4, p0, Ldib;->mSettings:Lcke;

    .line 60
    new-instance v0, Ldic;

    invoke-direct {v0, p0}, Ldic;-><init>(Ldib;)V

    invoke-virtual {p1, v0}, Lchk;->a(Lchm;)V

    .line 71
    iget-object v1, p0, Ldib;->byA:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    invoke-virtual {p1}, Lchk;->Gk()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldgl;->jD(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ldib;->byB:Ljava/util/Map;

    .line 74
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/util/Collection;I)Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 92
    new-instance v4, Lbca;

    invoke-direct {v4}, Lbca;-><init>()V

    .line 95
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 96
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 97
    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-virtual {v0}, Ldht;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Ldht;->acb()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v6, v0}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v0, v7, v7}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v6, p0, Ldib;->byA:Ljava/lang/Object;

    monitor-enter v6

    .line 104
    :try_start_0
    iget-object v1, p0, Ldib;->byB:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 105
    iget-object v1, p0, Ldib;->byB:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    .line 108
    :goto_1
    iget-object v0, p0, Ldib;->byB:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 109
    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Ldib;->byB:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 113
    iget-object v8, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->auC:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 114
    invoke-virtual {v4, v0}, Lbca;->a(Lcom/google/android/gms/appdatasearch/CorpusId;)Lbca;

    .line 115
    invoke-virtual {v4, v0, v1}, Lbca;->a(Lcom/google/android/gms/appdatasearch/CorpusId;I)Lbca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 117
    :cond_2
    :try_start_1
    iget-object v8, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lbca;->eL(Ljava/lang/String;)Lbca;

    .line 118
    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Lbca;->f(Ljava/lang/String;I)Lbca;

    goto :goto_2

    .line 127
    :cond_3
    iget-object v0, p0, Ldib;->byB:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 128
    invoke-interface {v5, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 130
    :cond_4
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 134
    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->auC:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 135
    invoke-virtual {v4, v0}, Lbca;->a(Lcom/google/android/gms/appdatasearch/CorpusId;)Lbca;

    .line 136
    invoke-virtual {v4, v0, v2}, Lbca;->a(Lcom/google/android/gms/appdatasearch/CorpusId;I)Lbca;

    goto :goto_4

    .line 138
    :cond_5
    iget-object v5, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lbca;->eL(Ljava/lang/String;)Lbca;

    .line 139
    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Lbca;->f(Ljava/lang/String;I)Lbca;

    goto :goto_4

    .line 148
    :cond_6
    iget-object v0, p0, Ldib;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NK()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 149
    iput v3, v4, Lbca;->avx:I

    .line 152
    :cond_7
    iput p2, v4, Lbca;->avy:I

    .line 154
    invoke-virtual {v4}, Lbca;->xb()Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    move-result-object v0

    return-object v0

    :cond_8
    move v2, v3

    goto/16 :goto_1
.end method

.method static synthetic a(Ldib;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldib;->byA:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Ldib;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Ldib;->byB:Ljava/util/Map;

    return-object p1
.end method

.method public static acd()Ldid;
    .locals 3

    .prologue
    .line 158
    new-instance v0, Ldid;

    const/4 v1, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldid;-><init>(ILjava/util/List;)V

    return-object v0
.end method

.method static synthetic b(Ldib;)Lchk;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldib;->mGsaConfigFlags:Lchk;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/util/Collection;Ldhr;Lemy;)V
    .locals 11

    .prologue
    .line 82
    iget-object v7, p0, Ldib;->bxH:Ldgm;

    iget-object v0, p2, Ldhr;->bxX:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v8

    iget v9, p2, Ldhr;->bxY:I

    iget v0, p2, Ldhr;->avs:I

    invoke-direct {p0, p1, v0}, Ldib;->a(Ljava/util/Collection;I)Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    move-result-object v10

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    move v6, v0

    :goto_0
    new-instance v0, Ldie;

    iget-object v1, p0, Ldib;->aVQ:Ldgc;

    iget-object v3, p2, Ldhr;->bxX:Lcom/google/android/shared/search/Query;

    iget-boolean v4, p2, Ldhr;->bxZ:Z

    move-object v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ldie;-><init>(Ldgc;Ljava/util/Collection;Lcom/google/android/shared/search/Query;ZLemy;)V

    move-object v1, v7

    move-object v2, v8

    move v3, v9

    move-object v4, v10

    move v5, v6

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Ldgm;->a(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;ZLemy;)V

    .line 88
    return-void

    .line 82
    :cond_1
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0
.end method
