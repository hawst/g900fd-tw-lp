.class public final Laie;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laiq;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation


# static fields
.field private static final YJ:Z


# instance fields
.field private FW:I

.field private IL:Landroid/graphics/Canvas;

.field private final TT:I

.field private YK:Laik;

.field private YL:Lqz;

.field private YM:Landroid/graphics/Rect;

.field private YN:Landroid/graphics/Rect;

.field private YO:Landroid/graphics/BitmapFactory$Options;

.field private ny:I

.field private wo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 151
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Laie;->YJ:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Laif;)V
    .locals 12

    .prologue
    const/16 v11, 0x800

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laie;->YM:Landroid/graphics/Rect;

    .line 375
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laie;->YN:Landroid/graphics/Rect;

    .line 380
    invoke-static {p1}, Laim;->E(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Laie;->FW:I

    .line 381
    iget v0, p2, Laif;->TT:I

    iput v0, p0, Laie;->TT:I

    .line 382
    iget-object v0, p2, Laif;->YK:Laik;

    iput-object v0, p0, Laie;->YK:Laik;

    .line 383
    iget-object v0, p0, Laie;->YK:Laik;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Laie;->YK:Laik;

    invoke-interface {v0}, Laik;->getWidth()I

    move-result v0

    iput v0, p0, Laie;->ny:I

    .line 385
    iget-object v0, p0, Laie;->YK:Laik;

    invoke-interface {v0}, Laik;->getHeight()I

    move-result v0

    iput v0, p0, Laie;->wo:I

    .line 386
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    .line 387
    iget-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 388
    iget-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v10, v0, Landroid/graphics/BitmapFactory$Options;->inPreferQualityOverSpeed:Z

    .line 389
    iget-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 390
    iget v0, p2, Laif;->FX:I

    .line 391
    if-eqz v0, :cond_0

    .line 392
    const/16 v1, 0x400

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 396
    iget-object v1, p2, Laif;->YP:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 397
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-gt v1, v11, :cond_7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-gt v1, v11, :cond_7

    .line 398
    new-instance v1, Lra;

    invoke-direct {v1, v0}, Lra;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Laie;->YL:Lqz;

    .line 408
    :cond_0
    :goto_1
    return-void

    .line 396
    :cond_1
    int-to-float v0, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    float-to-double v4, v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpg-double v0, v4, v6

    if-gtz v0, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v3, v0, :cond_4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ne v4, v0, :cond_4

    move-object v0, v1

    :goto_2
    move-object v1, v0

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :cond_5
    invoke-static {v3, v4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    new-instance v2, Landroid/graphics/Paint;

    const/4 v4, 0x6

    invoke-direct {v2, v4}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v3, v1, v8, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    :cond_6
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1, v0, v9}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 400
    :cond_7
    const-string v1, "BitmapRegionTileSource"

    const-string v2, "Failed to create preview of apropriate size!  in: %dx%d, out: %dx%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Laie;->ny:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    iget v4, p0, Laie;->wo:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(IIILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 437
    iget v1, p0, Laie;->FW:I

    .line 438
    sget-boolean v0, Laie;->YJ:Z

    if-nez v0, :cond_4

    .line 439
    shl-int v0, v1, p1

    iget-object v2, p0, Laie;->YM:Landroid/graphics/Rect;

    add-int v3, p2, v0

    add-int/2addr v0, p3

    invoke-virtual {v2, p2, p3, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Laie;->YN:Landroid/graphics/Rect;

    iget v2, p0, Laie;->ny:I

    iget v3, p0, Laie;->wo:I

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    shl-int v2, v5, p1

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget-object v0, p0, Laie;->YK:Laik;

    iget-object v2, p0, Laie;->YN:Landroid/graphics/Rect;

    iget-object v3, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    invoke-interface {v0, v2, v3}, Laik;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "BitmapRegionTileSource"

    const-string v3, "fail in decoding region"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Laie;->YM:Landroid/graphics/Rect;

    iget-object v3, p0, Laie;->YN:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 463
    :cond_1
    :goto_0
    return-object v0

    .line 439
    :cond_2
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Laie;->IL:Landroid/graphics/Canvas;

    if-nez v2, :cond_3

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    iput-object v2, p0, Laie;->IL:Landroid/graphics/Canvas;

    :cond_3
    iget-object v2, p0, Laie;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Laie;->IL:Landroid/graphics/Canvas;

    iget-object v3, p0, Laie;->YN:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Laie;->YM:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    shr-int/2addr v3, p1

    int-to-float v3, v3

    iget-object v4, p0, Laie;->YN:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Laie;->YM:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    shr-int/2addr v4, p1

    int-to-float v4, v4

    invoke-virtual {v2, v0, v3, v4, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Laie;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object v0, v1

    goto :goto_0

    .line 442
    :cond_4
    shl-int v0, v1, p1

    .line 443
    iget-object v2, p0, Laie;->YM:Landroid/graphics/Rect;

    add-int v3, p2, v0

    add-int/2addr v0, p3

    invoke-virtual {v2, p2, p3, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 445
    if-nez p4, :cond_5

    .line 446
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p4

    .line 449
    :cond_5
    iget-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    shl-int v1, v5, p1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 450
    iget-object v0, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iput-object p4, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 453
    :try_start_0
    iget-object v0, p0, Laie;->YK:Laik;

    iget-object v1, p0, Laie;->YM:Landroid/graphics/Rect;

    iget-object v2, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    invoke-interface {v0, v1, v2}, Laik;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 455
    iget-object v1, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v1, v0, :cond_6

    iget-object v1, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 456
    iget-object v1, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iput-object v6, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 460
    :cond_6
    if-nez v0, :cond_1

    .line 461
    const-string v1, "BitmapRegionTileSource"

    const-string v2, "fail in decoding region"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 455
    :catchall_0
    move-exception v0

    iget-object v1, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v1, p4, :cond_7

    iget-object v1, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_7

    .line 456
    iget-object v1, p0, Laie;->YO:Landroid/graphics/BitmapFactory$Options;

    iput-object v6, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    :cond_7
    throw v0
.end method

.method public final getRotation()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Laie;->TT:I

    return v0
.end method

.method public final gh()I
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Laie;->FW:I

    return v0
.end method

.method public final gi()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Laie;->ny:I

    return v0
.end method

.method public final gj()I
    .locals 1

    .prologue
    .line 422
    iget v0, p0, Laie;->wo:I

    return v0
.end method

.method public final gk()Lqz;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Laie;->YL:Lqz;

    return-object v0
.end method
