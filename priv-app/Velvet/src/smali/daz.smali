.class public Ldaz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aRN:Landroid/app/NotificationManager;

.field private final mContext:Landroid/content/Context;

.field private final mService:Lcom/google/android/search/core/service/SearchService;

.field private final mVoiceSearchServices:Lhhq;


# direct methods
.method public constructor <init>(Lcom/google/android/search/core/service/SearchService;Landroid/content/Context;Lhhq;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Ldaz;->mService:Lcom/google/android/search/core/service/SearchService;

    .line 50
    iput-object p2, p0, Ldaz;->mContext:Landroid/content/Context;

    .line 51
    iput-object p3, p0, Ldaz;->mVoiceSearchServices:Lhhq;

    .line 52
    iget-object v0, p0, Ldaz;->mContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Ldaz;->aRN:Landroid/app/NotificationManager;

    .line 54
    return-void
.end method

.method private tR()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Ldaz;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 271
    iget-object v1, p0, Ldaz;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->ue()Lcse;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v0}, Lgcq;->getPrompt()Ljava/lang/String;

    move-result-object v0

    .line 273
    if-nez v0, :cond_0

    .line 274
    const-string v0, "Ok Google"

    .line 276
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final fy(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 64
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    if-ne p1, v1, :cond_2

    .line 71
    iget-object v0, p0, Ldaz;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Ldaz;->mContext:Landroid/content/Context;

    const-string v3, "com.google.android.search.core.service.SearchService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 72
    :cond_2
    if-nez p1, :cond_0

    .line 74
    iget-object v0, p0, Ldaz;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/service/SearchService;->stopForeground(Z)V

    .line 75
    iget-object v0, p0, Ldaz;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Ldaz;->mContext:Landroid/content/Context;

    const-string v3, "com.google.android.search.core.service.SearchService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public final fz(I)V
    .locals 13

    .prologue
    const v12, 0x7f020198

    const/4 v11, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 83
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 102
    :goto_0
    return-void

    .line 88
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_1

    move-object v0, v1

    .line 89
    :goto_1
    if-eqz v0, :cond_9

    .line 90
    iget-object v4, p0, Ldaz;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v4, v2, v0}, Lcom/google/android/search/core/service/SearchService;->startForeground(ILandroid/app/Notification;)V

    .line 95
    :goto_2
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_f

    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_a

    move v5, v2

    :goto_3
    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_b

    move v4, v2

    :goto_4
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_c

    move v0, v2

    :goto_5
    if-eqz v4, :cond_d

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Ldaz;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/e300/E300Activity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-direct {p0}, Ldaz;->tR()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v5, p0, Ldaz;->mContext:Landroid/content/Context;

    const-string v6, "com.google.android.search.core.service.SearchService"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.search.core.action.NOTIFICATION_DISMISSED"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.search.core.extra.NOTIFICATION_FLAG"

    const/16 v6, 0x20

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    iget-object v5, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v5, v3, v4, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    new-instance v5, Landroid/app/Notification$Builder;

    iget-object v6, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Ldaz;->mContext:Landroid/content/Context;

    const v7, 0x7f0a096f

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v5

    iget-object v6, p0, Ldaz;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0970

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {v6, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 96
    :goto_6
    if-eqz v0, :cond_10

    .line 97
    iget-object v1, p0, Ldaz;->aRN:Landroid/app/NotificationManager;

    invoke-virtual {v1, v11, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 88
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_5

    move v0, v2

    :goto_7
    and-int/lit8 v4, p1, 0x8

    if-eqz v4, :cond_6

    move v5, v2

    :goto_8
    and-int/lit8 v4, p1, 0x10

    if-eqz v4, :cond_7

    move v4, v2

    :goto_9
    if-eqz v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    const-string v6, "com.google.android.search.core.action.ENTER_MANUAL_CAR_MODE"

    iget-object v7, p0, Ldaz;->mContext:Landroid/content/Context;

    const-class v8, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, v6, v1, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v6, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v6, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v6, Landroid/content/Intent;

    iget-object v7, p0, Ldaz;->mContext:Landroid/content/Context;

    const-class v8, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, ":android:show_fragment"

    const-class v8, Lcom/google/android/search/core/preferences/HandsFreeSettingsFragment;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v7, v3, v6, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.google.android.search.core.action.EXIT_MANUAL_CAR_MODE"

    iget-object v9, p0, Ldaz;->mContext:Landroid/content/Context;

    const-class v10, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v7, v8, v1, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "exit-point"

    invoke-virtual {v7, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v8, p0, Ldaz;->mContext:Landroid/content/Context;

    const/high16 v9, 0x8000000

    invoke-static {v8, v3, v7, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    new-instance v8, Landroid/app/Notification$Builder;

    iget-object v9, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v9, p0, Ldaz;->mContext:Landroid/content/Context;

    const v10, 0x7f0a096b

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f020222

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const v8, 0x7f0201d0

    iget-object v9, p0, Ldaz;->mContext:Landroid/content/Context;

    const v10, 0x7f0a09c0

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9, v7}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const v7, 0x7f020217

    iget-object v8, p0, Ldaz;->mContext:Landroid/content/Context;

    const v9, 0x7f0a082b

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8, v6}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    const-string v0, ""

    if-eqz v5, :cond_2

    invoke-direct {p0}, Ldaz;->tR()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v8, p0, Ldaz;->mContext:Landroid/content/Context;

    const v9, 0x7f0a096d

    new-array v10, v2, [Ljava/lang/Object;

    aput-object v7, v10, v3

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    if-eqz v4, :cond_4

    if-eqz v5, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Ldaz;->mContext:Landroid/content/Context;

    const v5, 0x7f0a096c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_4
    new-instance v4, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v4, v6}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    move v0, v3

    goto/16 :goto_7

    :cond_6
    move v5, v3

    goto/16 :goto_8

    :cond_7
    move v4, v3

    goto/16 :goto_9

    :cond_8
    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Ldaz;->mContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v4, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v4, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v4, Landroid/app/Notification$Builder;

    iget-object v5, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Ldaz;->mContext:Landroid/content/Context;

    const v6, 0x7f0a096a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f020140

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_1

    .line 92
    :cond_9
    iget-object v0, p0, Ldaz;->mService:Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, v2}, Lcom/google/android/search/core/service/SearchService;->stopForeground(Z)V

    goto/16 :goto_2

    :cond_a
    move v5, v3

    .line 95
    goto/16 :goto_3

    :cond_b
    move v4, v3

    goto/16 :goto_4

    :cond_c
    move v0, v3

    goto/16 :goto_5

    :cond_d
    if-eqz v5, :cond_e

    invoke-direct {p0}, Ldaz;->tR()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/Notification$Builder;

    iget-object v4, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Ldaz;->mContext:Landroid/content/Context;

    const v5, 0x7f0a096e

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v4, p0, Ldaz;->mContext:Landroid/content/Context;

    const v5, 0x7f0a096e

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_6

    :cond_e
    if-eqz v0, :cond_f

    const/16 v0, 0x15b

    invoke-static {v0}, Lege;->ht(I)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Ldaz;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/e300/E300Activity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-direct {p0}, Ldaz;->tR()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v5, p0, Ldaz;->mContext:Landroid/content/Context;

    const-string v6, "com.google.android.search.core.service.SearchService"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.search.core.action.NOTIFICATION_DISMISSED"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.search.core.extra.NOTIFICATION_FLAG"

    const/16 v6, 0x80

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    iget-object v5, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-static {v5, v3, v4, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    new-instance v5, Landroid/app/Notification$Builder;

    iget-object v6, p0, Ldaz;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Ldaz;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0971

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {v6, v7, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Ldaz;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0972

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    goto/16 :goto_6

    :cond_f
    move-object v0, v1

    goto/16 :goto_6

    .line 100
    :cond_10
    iget-object v0, p0, Ldaz;->aRN:Landroid/app/NotificationManager;

    invoke-virtual {v0, v11}, Landroid/app/NotificationManager;->cancel(I)V

    goto/16 :goto_0
.end method
