.class public Leua;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private ciX:Ljava/util/List;

.field private ciY:Ljava/util/List;

.field private ciZ:Leub;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final dK:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Leua;->dK:Ljava/lang/Object;

    .line 106
    return-void
.end method

.method private a(Letj;J)V
    .locals 4

    .prologue
    .line 91
    iget-object v1, p0, Leua;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    const-string v0, "[Trace]"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Leua;->ciY:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Leua;->ciY:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leub;

    .line 95
    invoke-virtual {v0, p1, p2, p3}, Leub;->a(Letj;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 98
    :cond_0
    :try_start_1
    iget-object v0, p0, Leua;->ciX:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Leua;->ciX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leua;

    .line 100
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v3

    invoke-direct {v0, v3, p2, p3}, Leua;->a(Letj;J)V

    goto :goto_1

    .line 103
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 87
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Leua;->a(Letj;J)V

    .line 88
    return-void
.end method

.method public final avT()Leua;
    .locals 3

    .prologue
    .line 74
    iget-object v1, p0, Leua;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 75
    :try_start_0
    iget-object v0, p0, Leua;->ciX:Ljava/util/List;

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Leua;->ciX:Ljava/util/List;

    .line 78
    :cond_0
    new-instance v0, Leua;

    invoke-direct {v0}, Leua;-><init>()V

    .line 79
    iget-object v2, p0, Leua;->ciX:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final beginSection(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 42
    iget-object v1, p0, Leua;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_0
    new-instance v0, Leub;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Leua;->ciZ:Leub;

    invoke-direct {v0, p1, v2, v3, v4}, Leub;-><init>(Ljava/lang/String;JLeub;)V

    .line 45
    iget-object v2, p0, Leua;->ciZ:Leub;

    if-nez v2, :cond_1

    .line 46
    iget-object v2, p0, Leua;->ciY:Ljava/util/List;

    if-nez v2, :cond_0

    .line 47
    const/4 v2, 0x1

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Leua;->ciY:Ljava/util/List;

    .line 49
    :cond_0
    iget-object v2, p0, Leua;->ciY:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    :goto_0
    iput-object v0, p0, Leua;->ciZ:Leub;

    .line 54
    monitor-exit v1

    return-void

    .line 51
    :cond_1
    iget-object v2, p0, Leua;->ciZ:Leub;

    iget-object v3, v2, Leub;->cjc:Ljava/util/List;

    if-nez v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v2, Leub;->cjc:Ljava/util/List;

    :cond_2
    iget-object v2, v2, Leub;->cjc:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final endSection()V
    .locals 4

    .prologue
    .line 59
    iget-object v1, p0, Leua;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-object v0, p0, Leua;->ciZ:Leub;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "Tracer"

    const-string v2, "All sections have already ended!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 66
    :goto_0
    monitor-exit v1

    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Leua;->ciZ:Leub;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Leub;->cjb:J

    .line 64
    iget-object v0, p0, Leua;->ciZ:Leub;

    iget-object v0, v0, Leub;->cja:Leub;

    iput-object v0, p0, Leua;->ciZ:Leub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
