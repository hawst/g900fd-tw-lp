.class final Libd;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic dxc:Landroid/widget/TextView;

.field private synthetic dxd:I

.field private synthetic dxe:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Libc;Landroid/widget/TextView;ILjava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 92
    iput-object p2, p0, Libd;->dxc:Landroid/widget/TextView;

    iput p3, p0, Libd;->dxd:I

    iput-object p4, p0, Libd;->dxe:Ljava/lang/CharSequence;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    .line 95
    iget-object v0, p0, Libd;->dxc:Landroid/widget/TextView;

    iget v1, p0, Libd;->dxd:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 96
    iget-object v0, p0, Libd;->dxc:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 97
    iget-object v0, p0, Libd;->dxc:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 98
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Libd;->dxe:Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 99
    invoke-static {}, Libc;->aUI()Landroid/text/style/ForegroundColorSpan;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 101
    iget-object v1, p0, Libd;->dxc:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    return-void
.end method
