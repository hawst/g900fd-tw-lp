.class final Lhlc;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic djK:Lhlb;


# direct methods
.method constructor <init>(Lhlb;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lhlc;->djK:Lhlb;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 166
    check-cast p1, [Ljava/lang/Long;

    iget-object v0, p0, Lhlc;->djK:Lhlb;

    iget-object v0, v0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0}, Lcom/google/android/search/shared/ui/WebImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Leng;->a(Landroid/content/ContentResolver;JZ)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhlc;->djK:Lhlb;

    iget-object v0, v0, Lhlb;->djI:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lhlc;->djK:Lhlb;

    iget-object v1, p0, Lhlc;->djK:Lhlb;

    invoke-virtual {v1}, Lhlb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lhlb;->djI:Landroid/graphics/drawable/Drawable;

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 166
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lhlc;->djK:Lhlb;

    invoke-virtual {v0}, Lhlb;->aQy()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhlc;->djK:Lhlb;

    iget-object v0, v0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhlc;->djK:Lhlb;

    iget-object v0, v0, Lhlb;->djw:Lcom/google/android/search/shared/ui/WebImageView;

    iget-object v1, p0, Lhlc;->djK:Lhlb;

    iget-object v1, v1, Lhlb;->djI:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
