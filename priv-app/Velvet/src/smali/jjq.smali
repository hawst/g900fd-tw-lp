.class public final Ljjq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dVN:J

.field public epa:[Ljjr;

.field public epb:[Ljkt;

.field private epc:Z

.field public epd:[Ljjz;

.field public epe:Liwe;

.field public epf:Ljjt;

.field private epg:Z

.field private eph:Z

.field public epi:Ljjz;

.field public epj:Ljjm;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 124
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 125
    iput v1, p0, Ljjq;->aez:I

    invoke-static {}, Ljjr;->bnZ()[Ljjr;

    move-result-object v0

    iput-object v0, p0, Ljjq;->epa:[Ljjr;

    invoke-static {}, Ljkt;->bot()[Ljkt;

    move-result-object v0

    iput-object v0, p0, Ljjq;->epb:[Ljkt;

    iput-boolean v1, p0, Ljjq;->epc:Z

    invoke-static {}, Ljjz;->bof()[Ljjz;

    move-result-object v0

    iput-object v0, p0, Ljjq;->epd:[Ljjz;

    iput-object v2, p0, Ljjq;->epe:Liwe;

    iput-object v2, p0, Ljjq;->epf:Ljjt;

    iput-boolean v1, p0, Ljjq;->epg:Z

    iput-boolean v1, p0, Ljjq;->eph:Z

    iput-object v2, p0, Ljjq;->epi:Ljjz;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljjq;->dVN:J

    iput-object v2, p0, Ljjq;->epj:Ljjm;

    iput-object v2, p0, Ljjq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjq;->eCz:I

    .line 126
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjq;->epa:[Ljjr;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljjr;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljjq;->epa:[Ljjr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljjr;

    invoke-direct {v3}, Ljjr;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljjq;->epa:[Ljjr;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljjr;

    invoke-direct {v3}, Ljjr;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjq;->epa:[Ljjr;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjq;->epb:[Ljkt;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljkt;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljjq;->epb:[Ljkt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljkt;

    invoke-direct {v3}, Ljkt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljjq;->epb:[Ljkt;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljkt;

    invoke-direct {v3}, Ljkt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjq;->epb:[Ljkt;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljjq;->epc:Z

    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjq;->aez:I

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjq;->epd:[Ljjz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljjz;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljjq;->epd:[Ljjz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Ljjz;

    invoke-direct {v3}, Ljjz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljjq;->epd:[Ljjz;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Ljjz;

    invoke-direct {v3}, Ljjz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjq;->epd:[Ljjz;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljjq;->epe:Liwe;

    if-nez v0, :cond_a

    new-instance v0, Liwe;

    invoke-direct {v0}, Liwe;-><init>()V

    iput-object v0, p0, Ljjq;->epe:Liwe;

    :cond_a
    iget-object v0, p0, Ljjq;->epe:Liwe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ljjq;->epf:Ljjt;

    if-nez v0, :cond_b

    new-instance v0, Ljjt;

    invoke-direct {v0}, Ljjt;-><init>()V

    iput-object v0, p0, Ljjq;->epf:Ljjt;

    :cond_b
    iget-object v0, p0, Ljjq;->epf:Ljjt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljjq;->epg:Z

    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjq;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljjq;->eph:Z

    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjq;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljjq;->epi:Ljjz;

    if-nez v0, :cond_c

    new-instance v0, Ljjz;

    invoke-direct {v0}, Ljjz;-><init>()V

    iput-object v0, p0, Ljjq;->epi:Ljjz;

    :cond_c
    iget-object v0, p0, Ljjq;->epi:Ljjz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljjq;->dVN:J

    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjq;->aez:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljjq;->epj:Ljjm;

    if-nez v0, :cond_d

    new-instance v0, Ljjm;

    invoke-direct {v0}, Ljjm;-><init>()V

    iput-object v0, p0, Ljjq;->epj:Ljjm;

    :cond_d
    iget-object v0, p0, Ljjq;->epj:Ljjm;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Ljjq;->epa:[Ljjr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljjq;->epa:[Ljjr;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 150
    :goto_0
    iget-object v2, p0, Ljjq;->epa:[Ljjr;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 151
    iget-object v2, p0, Ljjq;->epa:[Ljjr;

    aget-object v2, v2, v0

    .line 152
    if-eqz v2, :cond_0

    .line 153
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 150
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Ljjq;->epb:[Ljkt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljjq;->epb:[Ljkt;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 158
    :goto_1
    iget-object v2, p0, Ljjq;->epb:[Ljkt;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 159
    iget-object v2, p0, Ljjq;->epb:[Ljkt;

    aget-object v2, v2, v0

    .line 160
    if-eqz v2, :cond_2

    .line 161
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 158
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 165
    :cond_3
    iget v0, p0, Ljjq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 166
    const/4 v0, 0x3

    iget-boolean v2, p0, Ljjq;->epc:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 168
    :cond_4
    iget-object v0, p0, Ljjq;->epd:[Ljjz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljjq;->epd:[Ljjz;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 169
    :goto_2
    iget-object v0, p0, Ljjq;->epd:[Ljjz;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 170
    iget-object v0, p0, Ljjq;->epd:[Ljjz;

    aget-object v0, v0, v1

    .line 171
    if-eqz v0, :cond_5

    .line 172
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 169
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 176
    :cond_6
    iget-object v0, p0, Ljjq;->epe:Liwe;

    if-eqz v0, :cond_7

    .line 177
    const/4 v0, 0x5

    iget-object v1, p0, Ljjq;->epe:Liwe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 179
    :cond_7
    iget-object v0, p0, Ljjq;->epf:Ljjt;

    if-eqz v0, :cond_8

    .line 180
    const/4 v0, 0x6

    iget-object v1, p0, Ljjq;->epf:Ljjt;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 182
    :cond_8
    iget v0, p0, Ljjq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_9

    .line 183
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljjq;->epg:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 185
    :cond_9
    iget v0, p0, Ljjq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_a

    .line 186
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljjq;->eph:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 188
    :cond_a
    iget-object v0, p0, Ljjq;->epi:Ljjz;

    if-eqz v0, :cond_b

    .line 189
    const/16 v0, 0x9

    iget-object v1, p0, Ljjq;->epi:Ljjz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 191
    :cond_b
    iget v0, p0, Ljjq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_c

    .line 192
    const/16 v0, 0xa

    iget-wide v2, p0, Ljjq;->dVN:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 194
    :cond_c
    iget-object v0, p0, Ljjq;->epj:Ljjm;

    if-eqz v0, :cond_d

    .line 195
    const/16 v0, 0xb

    iget-object v1, p0, Ljjq;->epj:Ljjm;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 197
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 198
    return-void
.end method

.method public final bnY()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Ljjq;->eph:Z

    return v0
.end method

.method public final dw(J)Ljjq;
    .locals 1

    .prologue
    .line 108
    iput-wide p1, p0, Ljjq;->dVN:J

    .line 109
    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjq;->aez:I

    .line 110
    return-object p0
.end method

.method public final getTimestamp()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Ljjq;->dVN:J

    return-wide v0
.end method

.method public final ip(Z)Ljjq;
    .locals 1

    .prologue
    .line 39
    iput-boolean p1, p0, Ljjq;->epc:Z

    .line 40
    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjq;->aez:I

    .line 41
    return-object p0
.end method

.method public final iq(Z)Ljjq;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljjq;->eph:Z

    .line 87
    iget v0, p0, Ljjq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjq;->aez:I

    .line 88
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 203
    iget-object v2, p0, Ljjq;->epa:[Ljjr;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljjq;->epa:[Ljjr;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 204
    :goto_0
    iget-object v3, p0, Ljjq;->epa:[Ljjr;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 205
    iget-object v3, p0, Ljjq;->epa:[Ljjr;

    aget-object v3, v3, v0

    .line 206
    if-eqz v3, :cond_0

    .line 207
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 204
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 212
    :cond_2
    iget-object v2, p0, Ljjq;->epb:[Ljkt;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljjq;->epb:[Ljkt;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 213
    :goto_1
    iget-object v3, p0, Ljjq;->epb:[Ljkt;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 214
    iget-object v3, p0, Ljjq;->epb:[Ljkt;

    aget-object v3, v3, v0

    .line 215
    if-eqz v3, :cond_3

    .line 216
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 213
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 221
    :cond_5
    iget v2, p0, Ljjq;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_6

    .line 222
    const/4 v2, 0x3

    iget-boolean v3, p0, Ljjq;->epc:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 225
    :cond_6
    iget-object v2, p0, Ljjq;->epd:[Ljjz;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljjq;->epd:[Ljjz;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 226
    :goto_2
    iget-object v2, p0, Ljjq;->epd:[Ljjz;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 227
    iget-object v2, p0, Ljjq;->epd:[Ljjz;

    aget-object v2, v2, v1

    .line 228
    if-eqz v2, :cond_7

    .line 229
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 226
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 234
    :cond_8
    iget-object v1, p0, Ljjq;->epe:Liwe;

    if-eqz v1, :cond_9

    .line 235
    const/4 v1, 0x5

    iget-object v2, p0, Ljjq;->epe:Liwe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_9
    iget-object v1, p0, Ljjq;->epf:Ljjt;

    if-eqz v1, :cond_a

    .line 239
    const/4 v1, 0x6

    iget-object v2, p0, Ljjq;->epf:Ljjt;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_a
    iget v1, p0, Ljjq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_b

    .line 243
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljjq;->epg:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 246
    :cond_b
    iget v1, p0, Ljjq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_c

    .line 247
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljjq;->eph:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 250
    :cond_c
    iget-object v1, p0, Ljjq;->epi:Ljjz;

    if-eqz v1, :cond_d

    .line 251
    const/16 v1, 0x9

    iget-object v2, p0, Ljjq;->epi:Ljjz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_d
    iget v1, p0, Ljjq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_e

    .line 255
    const/16 v1, 0xa

    iget-wide v2, p0, Ljjq;->dVN:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_e
    iget-object v1, p0, Ljjq;->epj:Ljjm;

    if-eqz v1, :cond_f

    .line 259
    const/16 v1, 0xb

    iget-object v2, p0, Ljjq;->epj:Ljjm;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_f
    return v0
.end method
