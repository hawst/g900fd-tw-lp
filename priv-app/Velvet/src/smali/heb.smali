.class public final Lheb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic ddO:Landroid/view/View;

.field private synthetic ddP:Landroid/view/View;

.field private synthetic ddQ:I

.field private synthetic ddR:I

.field private synthetic ddS:Landroid/view/View;

.field private synthetic ddT:I

.field private synthetic ddU:I

.field private synthetic ddV:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;IILandroid/view/View;IILandroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lheb;->ddO:Landroid/view/View;

    iput-object p2, p0, Lheb;->ddP:Landroid/view/View;

    iput p3, p0, Lheb;->ddQ:I

    iput p4, p0, Lheb;->ddR:I

    iput-object p5, p0, Lheb;->ddS:Landroid/view/View;

    iput p6, p0, Lheb;->ddT:I

    iput p7, p0, Lheb;->ddU:I

    iput-object p8, p0, Lheb;->ddV:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 92
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 95
    iget-object v1, p0, Lheb;->ddO:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 98
    iget-object v1, p0, Lheb;->ddP:Landroid/view/View;

    iget v2, p0, Lheb;->ddQ:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 99
    iget-object v1, p0, Lheb;->ddP:Landroid/view/View;

    iget v2, p0, Lheb;->ddR:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 102
    iget-object v1, p0, Lheb;->ddS:Landroid/view/View;

    sub-float v2, v0, v4

    iget v3, p0, Lheb;->ddQ:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 103
    iget-object v1, p0, Lheb;->ddS:Landroid/view/View;

    sub-float v2, v0, v4

    iget v3, p0, Lheb;->ddR:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 106
    sub-float v1, v4, v0

    iget v2, p0, Lheb;->ddT:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 107
    sub-float v0, v4, v0

    iget v2, p0, Lheb;->ddU:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 108
    iget-object v2, p0, Lheb;->ddV:Landroid/graphics/Rect;

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 109
    iget-object v2, p0, Lheb;->ddV:Landroid/graphics/Rect;

    iget-object v3, p0, Lheb;->ddS:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int v1, v3, v1

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 110
    iget-object v1, p0, Lheb;->ddV:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 111
    iget-object v1, p0, Lheb;->ddV:Landroid/graphics/Rect;

    iget-object v2, p0, Lheb;->ddS:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 121
    iget-object v0, p0, Lheb;->ddS:Landroid/view/View;

    iget-object v1, p0, Lheb;->ddV:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    .line 122
    return-void
.end method
