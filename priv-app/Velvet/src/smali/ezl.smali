.class public final Lezl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lezi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lezg;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 15
    iget-object v0, p2, Lezg;->cmA:Lezb;

    invoke-virtual {v0, p1}, Lezb;->b(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    .line 18
    invoke-static {p1}, Lfpc;->e(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpc;

    move-result-object v0

    invoke-virtual {v0}, Lfpc;->aCB()Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 20
    iget-object v0, p2, Lezg;->cmz:Leze;

    iget-object v0, v0, Leze;->mCalendarDataProvider:Leym;

    invoke-interface {v0}, Leym;->aww()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamm;

    invoke-virtual {v0}, Lamm;->nd()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lamm;->nb()J

    move-result-wide v0

    move-wide v2, v0

    :goto_0
    sget-object v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyp:Ljava/lang/String;

    new-instance v1, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    invoke-direct {v1}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->bn(J)V

    .line 22
    :cond_1
    return-void

    :cond_2
    move-wide v2, v4

    goto :goto_0
.end method
