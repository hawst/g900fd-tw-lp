.class public final Lach;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic Rp:I

.field private synthetic Rq:I

.field private synthetic Rr:Lcom/android/launcher3/PagedView;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/PagedView;II)V
    .locals 0

    .prologue
    .line 1823
    iput-object p1, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iput p2, p0, Lach;->Rp:I

    iput p3, p0, Lach;->Rq:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 1827
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget v1, p0, Lach;->Rp:I

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 1832
    iget v0, p0, Lach;->Rq:I

    iget v1, p0, Lach;->Rp:I

    if-ge v0, v1, :cond_1

    move v1, v2

    .line 1833
    :goto_0
    iget v0, p0, Lach;->Rq:I

    iget v4, p0, Lach;->Rp:I

    if-ge v0, v4, :cond_2

    iget v0, p0, Lach;->Rq:I

    add-int/lit8 v0, v0, 0x1

    .line 1835
    :goto_1
    iget v4, p0, Lach;->Rq:I

    iget v5, p0, Lach;->Rp:I

    if-le v4, v5, :cond_3

    iget v4, p0, Lach;->Rq:I

    add-int/lit8 v4, v4, -0x1

    :goto_2
    move v5, v0

    .line 1837
    :goto_3
    if-gt v5, v4, :cond_4

    .line 1838
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-virtual {v0, v5}, Lcom/android/launcher3/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1842
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v0

    iget-object v7, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-virtual {v7, v5}, Lcom/android/launcher3/PagedView;->by(I)I

    move-result v7

    add-int/2addr v7, v0

    .line 1843
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-virtual {v0}, Lcom/android/launcher3/PagedView;->jb()I

    move-result v0

    iget-object v8, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    add-int v9, v5, v1

    invoke-virtual {v8, v9}, Lcom/android/launcher3/PagedView;->by(I)I

    move-result v8

    add-int/2addr v8, v0

    .line 1847
    const/16 v0, 0x64

    invoke-virtual {v6, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    .line 1848
    if-eqz v0, :cond_0

    .line 1849
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1852
    :cond_0
    sub-int v0, v7, v8

    int-to-float v0, v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1853
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1854
    iget-object v7, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget v7, v7, Lcom/android/launcher3/PagedView;->QX:I

    int-to-long v8, v7

    invoke-virtual {v0, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1855
    new-array v7, v3, [Landroid/animation/Animator;

    const-string v8, "translationX"

    new-array v9, v3, [F

    const/4 v10, 0x0

    aput v10, v9, v11

    invoke-static {v6, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1857
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1858
    invoke-virtual {v6, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1837
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    :cond_1
    move v1, v3

    .line 1832
    goto :goto_0

    .line 1833
    :cond_2
    iget v0, p0, Lach;->Rp:I

    goto :goto_1

    .line 1835
    :cond_3
    iget v4, p0, Lach;->Rp:I

    goto :goto_2

    .line 1861
    :cond_4
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v1, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v1, v1, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PagedView;->removeView(Landroid/view/View;)V

    .line 1862
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v0, v0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    invoke-static {}, Lcom/android/launcher3/PagedView;->jy()V

    .line 1863
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v1, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v1, v1, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    iget v3, p0, Lach;->Rp:I

    invoke-virtual {v0, v1, v3}, Lcom/android/launcher3/PagedView;->addView(Landroid/view/View;I)V

    .line 1864
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    iget-object v0, v0, Lcom/android/launcher3/PagedView;->Ra:Landroid/view/View;

    iget v0, p0, Lach;->Rp:I

    invoke-static {}, Lcom/android/launcher3/PagedView;->jz()V

    .line 1865
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-static {v0, v2}, Lcom/android/launcher3/PagedView;->a(Lcom/android/launcher3/PagedView;I)I

    .line 1866
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-static {v0}, Lcom/android/launcher3/PagedView;->a(Lcom/android/launcher3/PagedView;)Lcom/android/launcher3/PageIndicator;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1867
    iget-object v0, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-static {v0}, Lcom/android/launcher3/PagedView;->a(Lcom/android/launcher3/PagedView;)Lcom/android/launcher3/PageIndicator;

    move-result-object v0

    iget-object v1, p0, Lach;->Rr:Lcom/android/launcher3/PagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/PagedView;->jg()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PageIndicator;->br(I)V

    .line 1869
    :cond_5
    return-void
.end method
