.class public final Lgbm;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static M(Landroid/content/Intent;)Lixx;
    .locals 7

    .prologue
    .line 193
    new-instance v2, Lixx;

    invoke-direct {v2}, Lixx;-><init>()V

    .line 194
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lixx;->rd(Ljava/lang/String;)Lixx;

    .line 197
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 198
    invoke-virtual {p0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lixx;->rc(Ljava/lang/String;)Lixx;

    .line 200
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 201
    if-eqz v3, :cond_6

    .line 202
    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 203
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 204
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 205
    new-instance v6, Lixy;

    invoke-direct {v6}, Lixy;-><init>()V

    .line 206
    invoke-virtual {v6, v0}, Lixy;->rg(Ljava/lang/String;)Lixy;

    .line 207
    if-eqz v1, :cond_2

    .line 208
    instance-of v0, v1, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 209
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v6, v0}, Lixy;->hj(Z)Lixy;

    .line 216
    :cond_2
    :goto_1
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 210
    :cond_3
    instance-of v0, v1, Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 211
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Lixy;->ck(J)Lixy;

    goto :goto_1

    .line 213
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lixy;->rh(Ljava/lang/String;)Lixy;

    goto :goto_1

    .line 218
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lixy;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lixy;

    iput-object v0, v2, Lixx;->dOR:[Lixy;

    .line 220
    :cond_6
    return-object v2
.end method

.method public static M(Lizj;)Ljava/lang/String;
    .locals 2
    .param p0    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 232
    if-eqz p0, :cond_0

    iget-object v0, p0, Lizj;->dSh:Ljas;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lizj;->dSh:Ljas;

    invoke-virtual {v0}, Ljas;->beB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lizj;->dSh:Ljas;

    invoke-virtual {v0}, Ljas;->beA()Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 237
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static N(Lizj;)J
    .locals 2

    .prologue
    .line 287
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 292
    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 293
    invoke-static {v0}, Liuu;->ad([B)J
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 298
    :goto_0
    return-wide v0

    .line 297
    :catch_0
    move-exception v0

    const-string v0, "SidekickProtoUtils"

    const-string v1, "MD5 not available for entry proto hashes, using array hash code"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    invoke-static {p0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    invoke-virtual {v0}, Lgbg;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public static Q([B)Lizj;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-static {v0, p0}, Leqh;->b(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    return-object v0
.end method

.method public static R([B)Liwk;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Liwk;

    invoke-direct {v0}, Liwk;-><init>()V

    invoke-static {v0, p0}, Leqh;->b(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Liwk;

    return-object v0
.end method

.method public static varargs a(Lizj;I[I)Liwk;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 249
    iget-object v4, p0, Lizj;->dUo:[Liwk;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 250
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 260
    :cond_0
    :goto_1
    return-object v0

    .line 252
    :cond_1
    array-length v1, p2

    if-eqz v1, :cond_2

    .line 253
    array-length v6, p2

    move v1, v2

    :goto_2
    if-ge v1, v6, :cond_2

    aget v7, p2, v1

    .line 254
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v8

    if-eq v8, v7, :cond_0

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 249
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 260
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 3

    .prologue
    .line 116
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 119
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 120
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 121
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {p0, p1, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 124
    return-void
.end method

.method public static b(Landroid/content/Intent;Ljava/lang/String;)Lizj;
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 52
    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Intent;Ljava/lang/String;)Lizq;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 64
    new-instance v1, Lizq;

    invoke-direct {v1}, Lizq;-><init>()V

    invoke-static {v1, v0}, Leqh;->b(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lizq;

    return-object v0
.end method

.method public static c(Ljcn;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Ljcn;->qG()Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    const/4 v0, 0x0

    .line 277
    :cond_0
    :goto_0
    return-object v0

    .line 273
    :cond_1
    invoke-virtual {p0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 274
    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 89
    if-nez v0, :cond_0

    move-object v0, v1

    .line 105
    :goto_0
    return-object v0

    .line 91
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 92
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 93
    instance-of v4, v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    if-nez v4, :cond_1

    .line 94
    const-string v0, "SidekickProtoUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid parcelable in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 98
    :cond_1
    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 100
    :try_start_0
    const-class v4, Lizj;

    invoke-virtual {v0, v4}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 102
    :catch_0
    move-exception v0

    const-string v0, "SidekickProtoUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid entry in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 105
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    move-object v0, v2

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p1}, Lgbm;->c(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static j(Lizj;I)V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lizj;->dUo:[Liwk;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 174
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 175
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwk;

    .line 177
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v0

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    .line 180
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 183
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lizj;->dUo:[Liwk;

    array-length v2, v2

    if-eq v0, v2, :cond_2

    .line 184
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Liwk;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liwk;

    iput-object v0, p0, Lizj;->dUo:[Liwk;

    .line 186
    :cond_2
    return-void
.end method
