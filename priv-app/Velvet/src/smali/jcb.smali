.class public final Ljcb;
.super Ljsl;
.source "PG"


# instance fields
.field private aeB:Ljbp;

.field private aez:I

.field private afX:Ljava/lang/String;

.field private afZ:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field public aiX:Ljcn;

.field private ajh:Ljava/lang/String;

.field private dNV:I

.field private dZS:Ljava/lang/String;

.field private dZT:Ljava/lang/String;

.field private dZU:Ljava/lang/String;

.field private dZV:Ljava/lang/String;

.field private dZW:F


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v2, p0, Ljcb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcb;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcb;->afX:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcb;->dZS:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcb;->ajh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcb;->dZT:Ljava/lang/String;

    iput-object v1, p0, Ljcb;->aiX:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljcb;->afZ:Ljava/lang/String;

    iput-object v1, p0, Ljcb;->aeB:Ljbp;

    const-string v0, ""

    iput-object v0, p0, Ljcb;->dZU:Ljava/lang/String;

    iput v2, p0, Ljcb;->dNV:I

    const-string v0, ""

    iput-object v0, p0, Ljcb;->dZV:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljcb;->dZW:F

    iput-object v1, p0, Ljcb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcb;->eCz:I

    return-void
.end method


# virtual methods
.method public final V(F)Ljcb;
    .locals 1

    const v0, 0x40666666    # 3.6f

    iput v0, p0, Ljcb;->dZW:F

    iget v0, p0, Ljcb;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->afh:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->afX:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcb;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljcb;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljcb;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Ljcb;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->afZ:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljcb;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljcb;->aeB:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljcb;->aeB:Ljbp;

    :cond_2
    iget-object v0, p0, Ljcb;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->dZU:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljcb;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljcb;->dNV:I

    iget v0, p0, Ljcb;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljcb;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljcb;->dZW:F

    iget v0, p0, Ljcb;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljcb;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->dZV:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljcb;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->dZS:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljcb;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->dZT:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljcb;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcb;->ajh:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljcb;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4d -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljcb;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljcb;->afX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljcb;->aiX:Ljcn;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljcb;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_2
    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Ljcb;->afZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljcb;->aeB:Ljbp;

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Ljcb;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_4
    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Ljcb;->dZU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_5
    iget v0, p0, Ljcb;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    iget v1, p0, Ljcb;->dNV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_6
    iget v0, p0, Ljcb;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget v1, p0, Ljcb;->dZW:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    :cond_7
    iget v0, p0, Ljcb;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-object v1, p0, Ljcb;->dZV:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_8
    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget-object v1, p0, Ljcb;->dZS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_9
    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    iget-object v1, p0, Ljcb;->dZT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_a
    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_b

    const/16 v0, 0xd

    iget-object v1, p0, Ljcb;->ajh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bfT()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->dZS:Ljava/lang/String;

    return-object v0
.end method

.method public final bfU()Z
    .locals 1

    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfV()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->dZT:Ljava/lang/String;

    return-object v0
.end method

.method public final bfW()Z
    .locals 1

    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfX()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->dZU:Ljava/lang/String;

    return-object v0
.end method

.method public final bfY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->dZV:Ljava/lang/String;

    return-object v0
.end method

.method public final bfZ()Z
    .locals 1

    iget v0, p0, Ljcb;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bga()F
    .locals 1

    iget v0, p0, Ljcb;->dZW:F

    return v0
.end method

.method public final bgb()Z
    .locals 1

    iget v0, p0, Ljcb;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljcb;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljcb;->afX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljcb;->aiX:Ljcn;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljcb;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Ljcb;->afZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljcb;->aeB:Ljbp;

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Ljcb;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget-object v2, p0, Ljcb;->dZU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Ljcb;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    iget v2, p0, Ljcb;->dNV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Ljcb;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget v2, p0, Ljcb;->dZW:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Ljcb;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Ljcb;->dZV:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    iget-object v2, p0, Ljcb;->dZS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    iget-object v2, p0, Ljcb;->dZT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Ljcb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_b

    const/16 v1, 0xd

    iget-object v2, p0, Ljcb;->ajh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    return v0
.end method

.method public final oo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->afZ:Ljava/lang/String;

    return-object v0
.end method

.method public final pf()Z
    .locals 1

    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qu()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcb;->ajh:Ljava/lang/String;

    return-object v0
.end method

.method public final qv()Z
    .locals 1

    iget v0, p0, Ljcb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final sO(Ljava/lang/String;)Ljcb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcb;->afh:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method

.method public final sP(Ljava/lang/String;)Ljcb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcb;->dZS:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method

.method public final sQ(Ljava/lang/String;)Ljcb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcb;->ajh:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method

.method public final sR(Ljava/lang/String;)Ljcb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcb;->dZT:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method

.method public final sS(Ljava/lang/String;)Ljcb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcb;->dZU:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method

.method public final sT(Ljava/lang/String;)Ljcb;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcb;->dZV:Ljava/lang/String;

    iget v0, p0, Ljcb;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljcb;->aez:I

    return-object p0
.end method
