.class public final Lgck;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglx;


# instance fields
.field private final cFO:Lglx;

.field private final cFP:Z

.field private final mAudioCallback:Lgcd;


# direct methods
.method public constructor <init>(Lglx;Lgcd;Z)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lgck;->cFO:Lglx;

    .line 35
    iput-object p2, p0, Lgck;->mAudioCallback:Lgcd;

    .line 36
    iput-boolean p3, p0, Lgck;->cFP:Z

    .line 37
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->CZ()V

    .line 101
    return-void
.end method

.method public final Nr()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->Nr()V

    .line 142
    return-void
.end method

.method public final Ns()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->Ns()V

    .line 159
    return-void
.end method

.method public final Nv()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->Nv()V

    .line 136
    return-void
.end method

.method public final Nw()V
    .locals 1

    .prologue
    .line 127
    const/16 v0, 0x5a

    invoke-static {v0}, Lege;->ht(I)V

    .line 129
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->Nw()V

    .line 130
    return-void
.end method

.method public final a(Lieb;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->a(Lieb;)V

    .line 83
    return-void
.end method

.method public final a(Ljps;)V
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0xc

    invoke-static {v0}, Lege;->ht(I)V

    .line 52
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->a(Ljps;)V

    .line 53
    return-void
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1, p2}, Lglx;->a(Ljvv;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public final a(Ljxb;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->a(Ljxb;)V

    .line 65
    return-void
.end method

.method public final aEM()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->aEM()V

    .line 89
    return-void
.end method

.method public final au(J)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lgck;->mAudioCallback:Lgcd;

    invoke-interface {v0, p1, p2}, Lgcd;->bv(J)V

    .line 107
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1, p2}, Lglx;->au(J)V

    .line 108
    return-void
.end method

.method public final b(Ljwp;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->b(Ljwp;)V

    .line 59
    return-void
.end method

.method public final bw(J)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1, p2}, Lglx;->au(J)V

    .line 153
    return-void
.end method

.method public final c(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->c(Lcom/google/android/shared/speech/HotwordResult;)V

    .line 77
    return-void
.end method

.method public final c(Leiq;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->c(Leiq;)V

    .line 95
    return-void
.end method

.method public final d(Ljyl;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->d(Ljyl;)V

    .line 148
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lgck;->cFP:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lgck;->mAudioCallback:Lgcd;

    invoke-interface {v0}, Lgcd;->aEI()V

    .line 119
    :cond_0
    const/4 v0, 0x6

    invoke-static {v0}, Lege;->ht(I)V

    .line 121
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0}, Lglx;->onEndOfSpeech()V

    .line 122
    return-void
.end method

.method public final v([B)V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x1e

    invoke-static {v0}, Lege;->ht(I)V

    .line 44
    iget-object v0, p0, Lgck;->cFO:Lglx;

    invoke-interface {v0, p1}, Lglx;->v([B)V

    .line 45
    return-void
.end method
