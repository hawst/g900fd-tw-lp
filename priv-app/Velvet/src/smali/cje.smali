.class public final Lcje;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile aYj:[Lcje;


# instance fields
.field private aYk:Ljava/lang/String;

.field private aYl:Ljava/lang/String;

.field private aez:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcje;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lcje;->aYk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcje;->aYl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcje;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcje;->eCz:I

    .line 74
    return-void
.end method

.method public static KS()[Lcje;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcje;->aYj:[Lcje;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lcje;->aYj:[Lcje;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lcje;

    sput-object v0, Lcje;->aYj:[Lcje;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcje;->aYj:[Lcje;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final KT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcje;->aYk:Ljava/lang/String;

    return-object v0
.end method

.method public final KU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcje;->aYl:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcje;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcje;->aYk:Ljava/lang/String;

    iget v0, p0, Lcje;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcje;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcje;->aYl:Ljava/lang/String;

    iget v0, p0, Lcje;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcje;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 88
    iget v0, p0, Lcje;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    iget-object v1, p0, Lcje;->aYk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 91
    :cond_0
    iget v0, p0, Lcje;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 92
    const/4 v0, 0x2

    iget-object v1, p0, Lcje;->aYl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 94
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 95
    return-void
.end method

.method public final gD(Ljava/lang/String;)Lcje;
    .locals 1

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lcje;->aYk:Ljava/lang/String;

    .line 38
    iget v0, p0, Lcje;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcje;->aez:I

    .line 39
    return-object p0
.end method

.method public final gE(Ljava/lang/String;)Lcje;
    .locals 1

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iput-object p1, p0, Lcje;->aYl:Ljava/lang/String;

    .line 60
    iget v0, p0, Lcje;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcje;->aez:I

    .line 61
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 99
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 100
    iget v1, p0, Lcje;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 101
    const/4 v1, 0x1

    iget-object v2, p0, Lcje;->aYk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_0
    iget v1, p0, Lcje;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 105
    const/4 v1, 0x2

    iget-object v2, p0, Lcje;->aYl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_1
    return v0
.end method
