.class final Ldig;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lepo;


# instance fields
.field final byI:Ljava/util/ArrayList;

.field final mExecutor:Lepo;


# direct methods
.method public constructor <init>(Lepo;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldig;->byI:Ljava/util/ArrayList;

    .line 49
    iput-object p1, p0, Ldig;->mExecutor:Lepo;

    .line 50
    return-void
.end method

.method static a(Ljava/util/List;I[Lepn;)[Lepn;
    .locals 2

    .prologue
    .line 86
    monitor-enter p0

    .line 87
    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 88
    const/4 v1, 0x0

    invoke-interface {p0, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 89
    invoke-interface {v1, p2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lepn;

    .line 90
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 91
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lepn;)V
    .locals 2

    .prologue
    .line 54
    iget-object v1, p0, Ldig;->byI:Ljava/util/ArrayList;

    monitor-enter v1

    .line 56
    :try_start_0
    iget-object v0, p0, Ldig;->byI:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ace()V
    .locals 2

    .prologue
    .line 102
    iget-object v1, p0, Ldig;->byI:Ljava/util/ArrayList;

    monitor-enter v1

    .line 103
    :try_start_0
    iget-object v0, p0, Ldig;->byI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 104
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    iget-object v0, p0, Ldig;->mExecutor:Lepo;

    invoke-interface {v0}, Lepo;->ace()V

    .line 106
    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
