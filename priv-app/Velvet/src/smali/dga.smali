.class public final Ldga;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldeb;


# instance fields
.field private final mSource:Ldgb;


# direct methods
.method public constructor <init>(Ldgb;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Ldga;->mSource:Ldgb;

    .line 23
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ldem;ILdef;Lddw;)V
    .locals 5

    .prologue
    .line 29
    monitor-enter p0

    const/4 v1, 0x0

    .line 30
    :try_start_0
    invoke-virtual {p1}, Ldem;->abn()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 31
    invoke-interface {v0}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldga;->mSource:Ldgb;

    invoke-interface {v4}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 37
    :cond_1
    if-eqz v1, :cond_5

    .line 38
    invoke-interface {p4}, Lddw;->getCount()I

    move-result v0

    sub-int v0, p2, v0

    .line 39
    invoke-interface {v1}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 40
    if-gtz v1, :cond_4

    .line 46
    :cond_2
    invoke-interface {p4}, Lddw;->aaZ()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    .line 43
    :cond_4
    add-int/lit8 v1, v1, -0x1

    .line 44
    :try_start_1
    invoke-interface {p4, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 47
    :cond_5
    :try_start_2
    invoke-virtual {p1}, Ldem;->abl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 48
    invoke-interface {p4}, Lddw;->aaZ()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
