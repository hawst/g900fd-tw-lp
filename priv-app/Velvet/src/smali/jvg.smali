.class public final Ljvg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eGW:[Ljvg;


# instance fields
.field private aez:I

.field private eGX:I

.field private eGY:I

.field private eGZ:I

.field private eHa:J

.field private eHb:[Ljve;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2934
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2935
    iput v0, p0, Ljvg;->aez:I

    iput v0, p0, Ljvg;->eGX:I

    iput v0, p0, Ljvg;->eGY:I

    iput v0, p0, Ljvg;->eGZ:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljvg;->eHa:J

    invoke-static {}, Ljve;->buU()[Ljve;

    move-result-object v0

    iput-object v0, p0, Ljvg;->eHb:[Ljve;

    const/4 v0, 0x0

    iput-object v0, p0, Ljvg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvg;->eCz:I

    .line 2936
    return-void
.end method

.method public static buV()[Ljvg;
    .locals 2

    .prologue
    .line 2842
    sget-object v0, Ljvg;->eGW:[Ljvg;

    if-nez v0, :cond_1

    .line 2843
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2845
    :try_start_0
    sget-object v0, Ljvg;->eGW:[Ljvg;

    if-nez v0, :cond_0

    .line 2846
    const/4 v0, 0x0

    new-array v0, v0, [Ljvg;

    sput-object v0, Ljvg;->eGW:[Ljvg;

    .line 2848
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2850
    :cond_1
    sget-object v0, Ljvg;->eGW:[Ljvg;

    return-object v0

    .line 2848
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2836
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljvg;->eGX:I

    iget v0, p0, Ljvg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljvg;->eGY:I

    iget v0, p0, Ljvg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljvg;->eGZ:I

    iget v0, p0, Ljvg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvg;->eHa:J

    iget v0, p0, Ljvg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljvg;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvg;->eHb:[Ljve;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljve;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljvg;->eHb:[Ljve;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljve;

    invoke-direct {v3}, Ljve;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljvg;->eHb:[Ljve;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljve;

    invoke-direct {v3}, Ljve;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvg;->eHb:[Ljve;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 2953
    iget v0, p0, Ljvg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2954
    const/4 v0, 0x1

    iget v1, p0, Ljvg;->eGX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2956
    :cond_0
    iget v0, p0, Ljvg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2957
    const/4 v0, 0x2

    iget v1, p0, Ljvg;->eGY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2959
    :cond_1
    iget v0, p0, Ljvg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2960
    const/4 v0, 0x3

    iget v1, p0, Ljvg;->eGZ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2962
    :cond_2
    iget v0, p0, Ljvg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2963
    const/4 v0, 0x4

    iget-wide v2, p0, Ljvg;->eHa:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2965
    :cond_3
    iget-object v0, p0, Ljvg;->eHb:[Ljve;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljvg;->eHb:[Ljve;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2966
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljvg;->eHb:[Ljve;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 2967
    iget-object v1, p0, Ljvg;->eHb:[Ljve;

    aget-object v1, v1, v0

    .line 2968
    if-eqz v1, :cond_4

    .line 2969
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 2966
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2973
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2974
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 2978
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2979
    iget v1, p0, Ljvg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2980
    const/4 v1, 0x1

    iget v2, p0, Ljvg;->eGX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2983
    :cond_0
    iget v1, p0, Ljvg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2984
    const/4 v1, 0x2

    iget v2, p0, Ljvg;->eGY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2987
    :cond_1
    iget v1, p0, Ljvg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2988
    const/4 v1, 0x3

    iget v2, p0, Ljvg;->eGZ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2991
    :cond_2
    iget v1, p0, Ljvg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2992
    const/4 v1, 0x4

    iget-wide v2, p0, Ljvg;->eHa:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2995
    :cond_3
    iget-object v1, p0, Ljvg;->eHb:[Ljve;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljvg;->eHb:[Ljve;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 2996
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljvg;->eHb:[Ljve;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 2997
    iget-object v2, p0, Ljvg;->eHb:[Ljve;

    aget-object v2, v2, v0

    .line 2998
    if-eqz v2, :cond_4

    .line 2999
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2996
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 3004
    :cond_6
    return v0
.end method
