.class public final Lagg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lagt;


# instance fields
.field private synthetic Xk:Lahv;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Workspace;Lahv;)V
    .locals 0

    .prologue
    .line 4960
    iput-object p2, p0, Lagg;->Xk:Lahv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lwq;Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4963
    instance-of v0, p1, Ladh;

    if-eqz v0, :cond_2

    instance-of v0, p2, Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_2

    .line 4964
    check-cast p1, Ladh;

    .line 4965
    invoke-virtual {p1}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v0

    .line 4966
    invoke-virtual {p1}, Ladh;->kg()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lagg;->Xk:Lahv;

    iget-object v1, v1, Lahv;->packageName:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4968
    iget-object v0, p0, Lagg;->Xk:Lahv;

    iget v0, v0, Lahv;->progress:I

    invoke-virtual {p1, v0}, Ladh;->bI(I)V

    .line 4969
    iget-object v0, p0, Lagg;->Xk:Lahv;

    iget v0, v0, Lahv;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 4971
    iget v0, p1, Ladh;->status:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p1, Ladh;->status:I

    .line 4973
    :cond_0
    check-cast p2, Lcom/android/launcher3/BubbleTextView;

    invoke-virtual {p2, v2}, Lcom/android/launcher3/BubbleTextView;->D(Z)V

    .line 4984
    :cond_1
    :goto_0
    return v2

    .line 4975
    :cond_2
    instance-of v0, p2, Lacz;

    if-eqz v0, :cond_1

    instance-of v0, p1, Lyy;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lyy;

    iget-object v0, v0, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lagg;->Xk:Lahv;

    iget-object v1, v1, Lahv;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4979
    check-cast p1, Lyy;

    iget-object v0, p0, Lagg;->Xk:Lahv;

    iget v0, v0, Lahv;->progress:I

    iput v0, p1, Lyy;->Mr:I

    .line 4980
    check-cast p2, Lacz;

    invoke-virtual {p2}, Lacz;->jV()V

    goto :goto_0
.end method
