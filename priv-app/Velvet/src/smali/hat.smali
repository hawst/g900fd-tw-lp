.class final Lhat;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcmc;
.implements Ljava/lang/Runnable;


# instance fields
.field private final dbd:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private synthetic dbe:Lhas;

.field private dz:Landroid/net/Uri;

.field private mResponse:Lcma;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lhas;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 136
    iput-object p1, p0, Lhat;->dbe:Lhas;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lhat;->dbd:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 137
    iput-object p2, p0, Lhat;->dz:Landroid/net/Uri;

    .line 138
    return-void
.end method

.method private aMS()Z
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lhat;->dbd:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    return v0
.end method

.method private aMT()V
    .locals 4

    .prologue
    .line 247
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget v0, v0, Lhas;->dbb:I

    if-ltz v0, :cond_0

    .line 248
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daX:Lhau;

    const-string v1, "Too many auth failures"

    invoke-static {v0, v1}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Too many auth failures"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Livy;->h(Ljava/lang/Throwable;)Z

    .line 256
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daX:Lhau;

    const-string v1, "Retrying after auth failure"

    invoke-static {v0, v1}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    .line 252
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget v1, v0, Lhas;->dbb:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lhas;->dbb:I

    .line 253
    iget-object v0, p0, Lhat;->dbe:Lhas;

    const/4 v1, 0x0

    iput v1, v0, Lhas;->dbc:I

    .line 254
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, v0, Lhap;->aTW:Ljava/util/concurrent/Executor;

    new-instance v1, Lhat;

    iget-object v2, p0, Lhat;->dbe:Lhas;

    iget-object v3, p0, Lhat;->dbe:Lhas;

    iget-object v3, v3, Lhas;->daZ:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Lhat;-><init>(Lhas;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private aMU()V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, v0, Lhap;->mCookies:Lgpf;

    iget-object v1, p0, Lhat;->dz:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v2}, Lcma;->PQ()Ldyj;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgpf;->a(Ljava/lang/String;Ldyj;)V

    .line 265
    return-void
.end method


# virtual methods
.method public final PW()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 160
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    invoke-virtual {v0}, Livy;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v0}, Lcma;->PR()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lhat;->aMS()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 166
    iget-object v0, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v0}, Lcma;->PS()Lefq;

    move-result-object v1

    instance-of v0, v1, Ldlc;

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lhat;->aMU()V

    move-object v0, v1

    check-cast v0, Ldlc;

    invoke-virtual {v0}, Ldlc;->getErrorCode()I

    move-result v0

    iget-object v2, p0, Lhat;->dbe:Lhas;

    iget-object v2, v2, Lhas;->daY:Lhap;

    iget-object v2, p0, Lhat;->dbe:Lhas;

    iget-object v2, v2, Lhas;->daX:Lhau;

    new-instance v3, Ldls;

    const-string v4, "Redirect status code %d"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-direct {v3, v4, v5}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2, v3}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    check-cast v1, Ldlc;

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget v0, v0, Lhas;->dbc:I

    const/16 v2, 0xa

    if-lt v0, v2, :cond_2

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daX:Lhau;

    const-string v1, "Too many redirects"

    invoke-static {v0, v1}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Too many redirects"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Livy;->h(Ljava/lang/Throwable;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ldlc;->getErrorCode()I

    move-result v2

    invoke-virtual {v1}, Ldlc;->acR()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->isRelative()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lhat;->dz:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lhat;->dz:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_3
    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daY:Lhap;

    iget-object v1, v1, Lhap;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->q(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daY:Lhap;

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daX:Lhau;

    new-instance v3, Ldls;

    const-string v4, "$d redirect to insecure URI %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v0}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-direct {v3, v4, v5}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v1, v3}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Redirect to insecure URI"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Livy;->h(Ljava/lang/Throwable;)Z

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v3, p0, Lhat;->dbe:Lhas;

    iget-object v3, v3, Lhas;->dba:Lijp;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daY:Lhap;

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daX:Lhau;

    new-instance v3, Ldls;

    const-string v4, "%d redirect to auth failure URI %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v0}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-direct {v3, v4, v5}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v1, v3}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    invoke-direct {p0}, Lhat;->aMT()V

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daY:Lhap;

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daX:Lhau;

    new-instance v3, Ldls;

    const-string v4, "%d redirect to %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-static {v0}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-direct {v3, v4, v5}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v1, v3}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget v2, v1, Lhas;->dbc:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lhas;->dbc:I

    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daY:Lhap;

    iget-object v1, v1, Lhap;->aTW:Ljava/util/concurrent/Executor;

    new-instance v2, Lhat;

    iget-object v3, p0, Lhat;->dbe:Lhas;

    invoke-direct {v2, v3, v0}, Lhat;-><init>(Lhas;Landroid/net/Uri;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_6
    instance-of v0, v1, Left;

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lhat;->aMU()V

    move-object v0, v1

    check-cast v0, Left;

    invoke-virtual {v0}, Left;->getErrorCode()I

    move-result v0

    iget-object v2, p0, Lhat;->dbe:Lhas;

    iget-object v2, v2, Lhas;->daY:Lhap;

    iget-object v2, p0, Lhat;->dbe:Lhas;

    iget-object v2, v2, Lhas;->daX:Lhau;

    new-instance v3, Ldls;

    const-string v4, "Status code %d"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-direct {v3, v4, v5}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2, v3}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    const/16 v2, 0x193

    if-eq v0, v2, :cond_7

    const/16 v2, 0x191

    if-ne v0, v2, :cond_8

    :cond_7
    invoke-direct {p0}, Lhat;->aMT()V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    invoke-virtual {v0, v1}, Livy;->h(Ljava/lang/Throwable;)Z

    goto/16 :goto_0

    .line 167
    :cond_9
    iget-object v0, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v0}, Lcma;->hasHeaders()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lhat;->aMS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daX:Lhau;

    const-string v1, "Successful response ready"

    invoke-static {v0, v1}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    invoke-direct {p0}, Lhat;->aMU()V

    new-instance v0, Ldyo;

    new-instance v1, Lcom/google/android/search/shared/api/UriRequest;

    iget-object v2, p0, Lhat;->dz:Landroid/net/Uri;

    invoke-direct {v1, v2}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;)V

    iget-object v2, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v2}, Lcma;->PQ()Ldyj;

    move-result-object v2

    invoke-static {v2}, Leeb;->c(Ldyj;)Leeb;

    move-result-object v2

    iget-object v3, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v3}, Lcma;->PT()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    new-instance v1, Lhao;

    iget-object v2, p0, Lhat;->mResponse:Lcma;

    invoke-direct {v1, v0, v2}, Lhao;-><init>(Ldyo;Ljava/io/Closeable;)V

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    invoke-virtual {v0, v1}, Livy;->bB(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final run()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 142
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daW:Livy;

    invoke-virtual {v0}, Livy;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, v0, Lhap;->mCookies:Lgpf;

    iget-object v1, p0, Lhat;->dz:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgpf;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daY:Lhap;

    iget-object v2, p0, Lhat;->dz:Landroid/net/Uri;

    new-instance v3, Ldlb;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ldlb;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ldlb;->setFollowRedirects(Z)V

    invoke-virtual {v3, v4}, Ldlb;->dy(Z)V

    iget-object v1, v1, Lhap;->mFlags:Lchk;

    invoke-virtual {v1}, Lchk;->JE()Z

    move-result v1

    invoke-virtual {v3, v1}, Ldlb;->dz(Z)V

    invoke-virtual {v3, v4}, Ldlb;->setUseCaches(Z)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Cookie"

    invoke-virtual {v3, v1, v0}, Ldlb;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_1
    :try_start_0
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v0, v0, Lhas;->daY:Lhap;

    iget-object v0, v0, Lhap;->daU:Lcmd;

    const/16 v1, 0xf

    invoke-virtual {v0, v3, v1}, Lcmd;->a(Ldlb;I)Lcma;

    move-result-object v0

    iput-object v0, p0, Lhat;->mResponse:Lcma;

    .line 151
    iget-object v0, p0, Lhat;->mResponse:Lcma;

    invoke-virtual {v0, p0}, Lcma;->a(Lcmc;)V

    .line 152
    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v1, v0, Lhas;->daY:Lhap;

    iget-object v0, p0, Lhat;->dbe:Lhas;

    iget-object v4, v0, Lhas;->daW:Livy;

    iget-object v5, p0, Lhat;->mResponse:Lcma;

    new-instance v0, Lhaq;

    const-string v2, "Cancel response"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct/range {v0 .. v5}, Lhaq;-><init>(Lhap;Ljava/lang/String;[ILivq;Lcma;)V

    iget-object v1, v1, Lhap;->daT:Ljava/util/concurrent/Executor;

    invoke-interface {v4, v0, v1}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    iget-object v1, p0, Lhat;->dbe:Lhas;

    iget-object v1, v1, Lhas;->daW:Livy;

    invoke-virtual {v1, v0}, Livy;->h(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
