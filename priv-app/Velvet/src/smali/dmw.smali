.class public final Ldmw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic this$0:Ldmv;


# direct methods
.method public constructor <init>(Ldmv;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Ldmw;->this$0:Ldmv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onJsEvents(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 84
    iget-object v1, p0, Ldmw;->this$0:Ldmv;

    iget-object v1, v1, Ldmv;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 85
    const-string v1, "TS"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 86
    const-string v1, "SU"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v1, p0, Ldmw;->this$0:Ldmv;

    iget-object v1, v1, Ldmv;->aXe:Ljava/util/concurrent/Executor;

    new-instance v2, Ldmx;

    const-string v3, "dispatchEvents"

    invoke-direct {v2, p0, v3, v0}, Ldmx;-><init>(Ldmw;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
