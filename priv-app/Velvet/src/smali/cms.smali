.class public final Lcms;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field public final bcv:Ljava/lang/String;

.field public final bcw:Ljava/lang/String;

.field public final bcx:Ljtg;

.field public final bcy:Ljyw;

.field public final bcz:Ljava/lang/Boolean;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljtg;Ljyw;Ljava/lang/Boolean;)V
    .locals 0
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p1, p0, Lcms;->bcv:Ljava/lang/String;

    .line 195
    iput-object p2, p0, Lcms;->bcw:Ljava/lang/String;

    .line 196
    iput-object p3, p0, Lcms;->bcx:Ljtg;

    .line 197
    iput-object p4, p0, Lcms;->bcy:Ljyw;

    .line 198
    iput-object p5, p0, Lcms;->bcz:Ljava/lang/Boolean;

    .line 199
    return-void
.end method


# virtual methods
.method public final QC()Z
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcms;->bcy:Ljyw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcms;->bcy:Ljyw;

    invoke-virtual {v0}, Ljyw;->bwA()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcms;->bcy:Ljyw;

    invoke-virtual {v0}, Ljyw;->bwz()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 253
    const-string v0, "SRP MetaData"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 254
    const-string v0, "eventId"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcms;->bcv:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 255
    const-string v0, "rewrittenQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcms;->bcw:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 256
    const-string v0, "assistOverlayValue"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcms;->bcz:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 258
    return-void
.end method

.method public final a(Ljyw;)V
    .locals 2

    .prologue
    .line 206
    if-nez p1, :cond_0

    .line 207
    const-string v0, "Velvet.SearchResult"

    const-string v1, "updateSearchMetadata: update without a metadata object!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcms;->bcy:Ljyw;

    invoke-static {v0, p1}, Leqh;->b(Ljsr;Ljsr;)V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 235
    instance-of v1, p1, Lcms;

    if-nez v1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v0

    .line 236
    :cond_1
    check-cast p1, Lcms;

    .line 237
    iget-object v1, p0, Lcms;->bcv:Ljava/lang/String;

    iget-object v2, p1, Lcms;->bcv:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcms;->bcw:Ljava/lang/String;

    iget-object v2, p1, Lcms;->bcw:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcms;->bcx:Ljtg;

    iget-object v2, p1, Lcms;->bcx:Ljtg;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcms;->bcy:Ljyw;

    iget-object v2, p1, Lcms;->bcy:Ljyw;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcms;->bcz:Ljava/lang/Boolean;

    iget-object v2, p1, Lcms;->bcz:Ljava/lang/Boolean;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SrpMetadata{eventId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcms;->bcv:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rewrtittenQuery: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcms;->bcw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", metadata: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcms;->bcy:Ljyw;

    invoke-static {v1}, Leqh;->d(Ljsr;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", showInAssistOverlay: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcms;->bcz:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
