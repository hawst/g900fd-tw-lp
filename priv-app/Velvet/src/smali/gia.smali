.class public Lgia;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lgia;->mContentResolver:Landroid/content/ContentResolver;

    .line 29
    return-void
.end method


# virtual methods
.method public final mI(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 37
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 38
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 39
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "lookup"

    aput-object v4, v2, v0

    const/4 v0, 0x2

    const-string v4, "display_name"

    aput-object v4, v2, v0

    const/4 v0, 0x3

    const-string v4, "times_contacted"

    aput-object v4, v2, v0

    const/4 v0, 0x4

    const-string v4, "number"

    aput-object v4, v2, v0

    const/4 v0, 0x5

    const-string v4, "label"

    aput-object v4, v2, v0

    .line 47
    iget-object v0, p0, Lgia;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    .line 49
    new-instance v1, Lgib;

    invoke-direct {v1, p0, v7}, Lgib;-><init>(Lgia;Ljava/util/List;)V

    .line 69
    if-eqz v0, :cond_0

    .line 70
    invoke-static {v1, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    .line 72
    :cond_0
    invoke-static {v7}, Lcom/google/android/search/shared/contact/Person;->I(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
