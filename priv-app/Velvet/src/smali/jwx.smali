.class public final Ljwx;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJO:Ljsm;


# instance fields
.field private aez:I

.field private dza:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1763
    const/16 v0, 0xb

    const-class v1, Ljwx;

    const v2, 0xd1390a2

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwx;->eJO:Ljsm;

    .line 1769
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1798
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1799
    const/4 v0, 0x0

    iput v0, p0, Ljwx;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljwx;->dza:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljwx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwx;->eCz:I

    .line 1800
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1756
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwx;->dza:Ljava/lang/String;

    iget v0, p0, Ljwx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwx;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1813
    iget v0, p0, Ljwx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1814
    const/4 v0, 0x1

    iget-object v1, p0, Ljwx;->dza:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1816
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1817
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1821
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1822
    iget v1, p0, Ljwx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1823
    const/4 v1, 0x1

    iget-object v2, p0, Ljwx;->dza:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1826
    :cond_0
    return v0
.end method

.method public final zI(Ljava/lang/String;)Ljwx;
    .locals 1

    .prologue
    .line 1782
    if-nez p1, :cond_0

    .line 1783
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1785
    :cond_0
    iput-object p1, p0, Ljwx;->dza:Ljava/lang/String;

    .line 1786
    iget v0, p0, Ljwx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwx;->aez:I

    .line 1787
    return-object p0
.end method
