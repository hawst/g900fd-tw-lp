.class final Lhzd;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic aSa:Lcom/google/android/shared/search/Query;

.field private synthetic bhc:Lefk;

.field private synthetic dkx:Ljkt;

.field private synthetic dvc:Z

.field private synthetic dvd:Z

.field private synthetic dve:Z

.field private synthetic dvf:Z

.field private synthetic dvg:Z

.field private synthetic dvh:Z

.field private synthetic dvi:Z

.field private synthetic dvj:Z

.field private synthetic dvk:Z

.field private synthetic dvl:Lhzc;


# direct methods
.method constructor <init>(Lhzc;Ljkt;ZLcom/google/android/shared/search/Query;ZZZZZZZZLefk;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lhzd;->dvl:Lhzc;

    iput-object p2, p0, Lhzd;->dkx:Ljkt;

    iput-boolean p3, p0, Lhzd;->dvc:Z

    iput-object p4, p0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    iput-boolean p5, p0, Lhzd;->dvd:Z

    iput-boolean p6, p0, Lhzd;->dve:Z

    iput-boolean p7, p0, Lhzd;->dvf:Z

    iput-boolean p8, p0, Lhzd;->dvg:Z

    iput-boolean p9, p0, Lhzd;->dvh:Z

    iput-boolean p10, p0, Lhzd;->dvi:Z

    iput-boolean p11, p0, Lhzd;->dvj:Z

    iput-boolean p12, p0, Lhzd;->dvk:Z

    iput-object p13, p0, Lhzd;->bhc:Lefk;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 21

    .prologue
    .line 567
    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    iget-object v3, v2, Ljkt;->eqn:Ljku;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvc:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhzd;->dkx:Ljkt;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3, v4}, Lhzc;->a(Ljkt;Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvd:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    sget-object v4, Ljmo;->eud:Ljsm;

    invoke-virtual {v2, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljmo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3, v2, v4}, Lhzc;->a(Ljmo;Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dve:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v10, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhzd;->dkx:Ljkt;

    sget-object v4, Ljlh;->ers:Ljsm;

    invoke-virtual {v3, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v3

    move-object v13, v3

    check-cast v13, Ljlh;

    const/4 v3, 0x0

    iget-object v4, v13, Ljlh;->ert:[Ljoq;

    array-length v4, v4

    if-lez v4, :cond_1d

    iget-object v3, v10, Lhzc;->mContactLabelConverter:Ldyv;

    iget-object v4, v10, Lhzc;->mRelationshipManager:Lcjg;

    iget-object v5, v10, Lhzc;->mPersonShortcutManager:Lciy;

    iget-object v6, v10, Lhzc;->mGsaConfigFlags:Lchk;

    iget-object v7, v10, Lhzc;->mContactLookup:Lghy;

    sget-object v8, Ldzb;->bRp:Ldzb;

    iget-object v9, v13, Ljlh;->ert:[Ljoq;

    const/4 v11, 0x0

    aget-object v9, v9, v11

    iget-object v10, v10, Lhzc;->mDiscourseContextSupplier:Ligi;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-static/range {v2 .. v12}, Lico;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v2

    :goto_1
    new-instance v3, Lcom/google/android/search/shared/actions/EmailAction;

    invoke-virtual {v13}, Ljlh;->getSubject()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13}, Ljlh;->getBody()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/search/shared/actions/EmailAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvf:Z

    if-eqz v2, :cond_a

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljku;->bbg()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    iget-object v2, v2, Ljkt;->eqn:Ljku;

    invoke-virtual {v2}, Ljku;->oY()I

    move-result v2

    move v14, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    sget-object v3, Ljkx;->eqH:Ljsm;

    invoke-virtual {v2, v3}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Ljkx;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    invoke-virtual {v2}, Ljkt;->bou()Z

    move-result v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lhzd;->dvg:Z

    move/from16 v17, v0

    iget-object v0, v13, Ljkx;->eqI:Ljns;

    move-object/from16 v18, v0

    const/16 v2, 0x29

    if-ne v14, v2, :cond_5

    if-eqz v18, :cond_5

    invoke-virtual/range {v18 .. v18}, Ljns;->agW()I

    move-result v19

    if-eqz v19, :cond_4

    const/4 v12, 0x1

    :goto_3
    invoke-virtual/range {v18 .. v18}, Ljns;->bqX()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Ljns;->bqY()Z

    move-result v3

    invoke-virtual/range {v18 .. v18}, Ljns;->bqV()J

    move-result-wide v4

    invoke-virtual/range {v18 .. v18}, Ljns;->bqW()J

    move-result-wide v6

    invoke-virtual {v13}, Ljkx;->boF()Z

    move-result v8

    iget-object v9, v15, Lhzc;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual/range {v18 .. v18}, Ljns;->aTg()I

    move-result v10

    iget-object v11, v15, Lhzc;->mContext:Landroid/content/Context;

    invoke-static/range {v2 .. v12}, Licz;->a(Ljava/lang/String;ZJJZLandroid/content/ContentResolver;ILandroid/content/Context;Z)Ljava/util/List;

    move-result-object v2

    const-class v3, Ljnm;

    invoke-static {v2, v3}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljnm;

    iget-object v3, v15, Lhzc;->cUE:Lgru;

    move/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v3, v2, v0, v1}, Lgru;->a([Ljnm;IZ)V

    iget-object v3, v13, Ljkx;->eqJ:[Ljnm;

    invoke-static {v2, v3}, Licz;->a([Ljnm;[Ljnm;)Ljava/util/List;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    if-nez v16, :cond_6

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    const/16 v2, 0x29

    move v14, v2

    goto :goto_2

    :cond_4
    const/4 v12, 0x0

    goto :goto_3

    :cond_5
    iget-object v2, v13, Ljkx;->eqJ:[Ljnm;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    goto :goto_4

    :cond_6
    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    if-eqz v18, :cond_8

    invoke-virtual/range {v18 .. v18}, Ljns;->bqV()J

    move-result-wide v4

    invoke-virtual/range {v18 .. v18}, Ljns;->bqW()J

    move-result-wide v6

    :cond_7
    :goto_5
    new-instance v2, Lcom/google/android/search/shared/actions/AgendaAction;

    move v8, v14

    move-object v9, v13

    invoke-direct/range {v2 .. v9}, Lcom/google/android/search/shared/actions/AgendaAction;-><init>(Ljava/util/List;JJILjkx;)V

    goto/16 :goto_0

    :cond_8
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljnm;

    iget-object v8, v2, Ljnm;->euY:Ljno;

    if-eqz v8, :cond_9

    iget-object v4, v2, Ljnm;->euY:Ljno;

    invoke-virtual {v4}, Ljno;->Pu()J

    move-result-wide v4

    iget-object v2, v2, Ljnm;->euY:Ljno;

    invoke-virtual {v2}, Ljno;->bqN()I

    move-result v2

    int-to-long v8, v2

    add-long/2addr v4, v8

    :cond_9
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljnm;

    iget-object v8, v2, Ljnm;->euY:Ljno;

    if-eqz v8, :cond_7

    iget-object v6, v2, Ljnm;->euY:Ljno;

    invoke-virtual {v6}, Ljno;->Pu()J

    move-result-wide v6

    iget-object v2, v2, Ljnm;->euY:Ljno;

    invoke-virtual {v2}, Ljno;->bqN()I

    move-result v2

    int-to-long v8, v2

    add-long/2addr v6, v8

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvh:Z

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v9, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    sget-object v3, Ljmb;->eti:Ljsm;

    invoke-virtual {v2, v3}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljmb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    iget-object v2, v8, Ljmb;->etl:[Ljava/lang/String;

    array-length v2, v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v8}, Ljmb;->bpx()Z

    move-result v3

    if-eqz v3, :cond_c

    const/4 v4, 0x1

    :goto_7
    iget-object v5, v8, Ljmb;->etl:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    if-eqz v2, :cond_d

    iget-object v2, v9, Lhzc;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Licl;->a(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v3

    :goto_8
    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual {v8}, Ljmb;->bpz()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v8}, Ljmb;->bpB()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v8}, Ljmb;->bpA()I

    move-result v2

    move v5, v2

    :goto_9
    invoke-virtual {v8}, Ljmb;->bpy()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move-object v2, v6

    move v6, v5

    move v5, v7

    :goto_a
    invoke-virtual {v8, v6}, Ljmb;->qL(I)Ljmb;

    move v7, v5

    move-object v5, v2

    :goto_b
    new-instance v2, Lcom/google/android/search/shared/actions/MessageSearchAction;

    invoke-virtual {v8}, Ljmb;->bpA()I

    move-result v6

    invoke-direct/range {v2 .. v8}, Lcom/google/android/search/shared/actions/MessageSearchAction;-><init>(Ljava/util/List;ILjava/lang/String;IZLjmb;)V

    goto/16 :goto_0

    :cond_b
    const/4 v2, 0x0

    goto :goto_6

    :cond_c
    const/4 v4, 0x2

    goto :goto_7

    :cond_d
    iget-object v2, v9, Lhzc;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v6

    const/16 v2, 0xa

    invoke-virtual {v6, v5, v2}, Lghy;->d(Ljava/util/List;I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lghy;->bz(J)Lcom/google/android/search/shared/contact/Person;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/google/android/search/shared/contact/Person;->h(Lcom/google/android/search/shared/contact/Person;)Lcom/google/android/search/shared/contact/Person;

    goto :goto_c

    :cond_e
    iget-object v2, v9, Lhzc;->mContext:Landroid/content/Context;

    invoke-static {v5, v3, v2}, Licl;->a(Ljava/util/List;ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v3

    goto :goto_8

    :cond_f
    const/4 v2, 0x0

    move v5, v2

    goto :goto_9

    :pswitch_0
    const-string v2, "ActionV2Processor"

    const-string v9, "has UserInstructionType but it\'s type NONE"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v2, v9, v10}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v2, v6

    move v6, v5

    move v5, v7

    goto :goto_a

    :pswitch_1
    add-int/lit8 v6, v5, 0x1

    iget-object v5, v9, Lhzc;->mMessageSearchTtsUtil:Lgtj;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepe;

    iget-object v9, v5, Lgtj;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f100043

    iget-object v11, v2, Lepe;->mMessages:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v2, Lepe;->mMessages:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-virtual {v2}, Lepe;->avo()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v12, v13

    invoke-virtual {v9, v10, v11, v12}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3, v6}, Lgtj;->e(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    goto/16 :goto_a

    :pswitch_2
    iget-object v6, v9, Lhzc;->mMessageSearchTtsUtil:Lgtj;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lepe;

    invoke-virtual {v6, v2}, Lgtj;->a(Lepe;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v5}, Lgtj;->e(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x1

    move/from16 v20, v6

    move v6, v5

    move/from16 v5, v20

    goto/16 :goto_a

    :pswitch_3
    iget-object v2, v9, Lhzc;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0687

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move v6, v5

    move v5, v7

    goto/16 :goto_a

    :pswitch_4
    iget-object v2, v9, Lhzc;->mMessageSearchTtsUtil:Lgtj;

    iget-object v2, v2, Lgtj;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0683

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move v6, v5

    move v5, v7

    goto/16 :goto_a

    :cond_10
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, v9, Lhzc;->mMessageSearchTtsUtil:Lgtj;

    const/4 v5, 0x2

    if-ne v4, v5, :cond_11

    iget-object v2, v2, Lgtj;->mContext:Landroid/content/Context;

    const v5, 0x7f0a067f

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_d
    const/4 v5, 0x0

    :goto_e
    const/4 v6, 0x0

    invoke-virtual {v8, v6}, Ljmb;->qL(I)Ljmb;

    move v7, v5

    move-object v5, v2

    goto/16 :goto_b

    :cond_11
    iget-object v2, v2, Lgtj;->mContext:Landroid/content/Context;

    const v5, 0x7f0a067e

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_d

    :cond_12
    iget-object v2, v9, Lhzc;->mMessageSearchTtsUtil:Lgtj;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Lgtj;->aq(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6}, Lgtj;->e(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    goto :goto_e

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvi:Z

    if-eqz v2, :cond_14

    const/4 v2, 0x0

    if-eqz v3, :cond_1c

    invoke-virtual {v3}, Ljku;->boA()Z

    move-result v2

    move v3, v2

    :goto_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    sget-object v6, Ljmr;->eur:Ljsm;

    invoke-virtual {v2, v6}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljmr;

    invoke-virtual {v4, v5, v2, v3}, Lhzc;->a(Lcom/google/android/shared/search/Query;Ljmr;Z)Lcom/google/android/search/shared/actions/ShowContactInformationAction;

    move-result-object v2

    goto/16 :goto_0

    :cond_14
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvj:Z

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    sget-object v3, Ljmh;->ety:Ljsm;

    invoke-virtual {v2, v3}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljmh;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lhzd;->dvg:Z

    new-instance v3, Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-direct {v3, v2}, Lcom/google/android/search/shared/actions/PlayMediaAction;-><init>(Ljmh;)V

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aia()Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aib()Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahY()Z

    move-result v2

    if-nez v2, :cond_15

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahZ()Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_15
    iget-object v2, v4, Lhzc;->duT:Licp;

    invoke-virtual {v2, v3, v5}, Licp;->a(Lcom/google/android/search/shared/actions/PlayMediaAction;Z)Z

    move-result v2

    if-nez v2, :cond_16

    if-eqz v5, :cond_17

    :cond_16
    move-object v2, v3

    goto/16 :goto_0

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_19
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhzd;->dvk:Z

    if-eqz v2, :cond_1b

    if-eqz v3, :cond_1a

    invoke-virtual {v3}, Ljku;->bbg()Z

    move-result v2

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    iget-object v2, v2, Ljkt;->eqn:Ljku;

    invoke-virtual {v2}, Ljku;->oY()I

    move-result v2

    move v3, v2

    :goto_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lhzd;->dvl:Lhzc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhzd;->dkx:Ljkt;

    sget-object v6, Ljrc;->ezW:Ljsm;

    invoke-virtual {v2, v6}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljrc;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhzd;->dkx:Ljkt;

    invoke-virtual {v6}, Ljkt;->bou()Z

    move-result v6

    invoke-virtual {v4, v5, v3, v2, v6}, Lhzc;->a(Lcom/google/android/shared/search/Query;ILjrc;Z)Lcom/google/android/search/shared/actions/modular/ModularAction;

    move-result-object v2

    goto/16 :goto_0

    :cond_1a
    const/16 v2, 0x31

    move v3, v2

    goto :goto_10

    :cond_1b
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_1c
    move v3, v2

    goto/16 :goto_f

    :cond_1d
    move-object v2, v3

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 567
    check-cast p1, Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhzd;->dvl:Lhzc;

    iget-object v1, p0, Lhzd;->dkx:Ljkt;

    invoke-virtual {v0, v1, p1}, Lhzc;->a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;)V

    new-array v0, v2, [Lcom/google/android/search/shared/actions/VoiceAction;

    aput-object p1, v0, v3

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lhzd;->bhc:Lefk;

    invoke-interface {v1, v0}, Lefk;->ar(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lhzd;->aSa:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/search/shared/actions/PuntAction;

    const-string v1, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;Z)V

    new-array v1, v2, [Lcom/google/android/search/shared/actions/VoiceAction;

    aput-object v0, v1, v3

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method
