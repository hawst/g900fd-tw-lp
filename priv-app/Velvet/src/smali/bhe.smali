.class public abstract Lbhe;
.super Ljava/lang/Object;

# interfaces
.implements Lbhh;
.implements Lbhm;


# instance fields
.field private final aAV:Ljava/lang/Object;

.field protected aAW:Lbhf;

.field private final aAX:Ljava/util/concurrent/CountDownLatch;

.field private final aAY:Ljava/util/ArrayList;

.field private aAZ:Lbhp;

.field private volatile aBa:Lbho;

.field private volatile aBb:Z

.field private aBc:Z

.field private aBd:Z

.field private aBe:Lbjg;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbhe;->aAV:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbhe;->aAX:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhe;->aAY:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lbhe;)V
    .locals 0

    invoke-direct {p0}, Lbhe;->yt()V

    return-void
.end method

.method private d(Lbho;)V
    .locals 3

    iput-object p1, p0, Lbhe;->aBa:Lbho;

    const/4 v0, 0x0

    iput-object v0, p0, Lbhe;->aBe:Lbjg;

    iget-object v0, p0, Lbhe;->aAX:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lbhe;->aBa:Lbho;

    invoke-interface {v0}, Lbho;->wZ()Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lbhe;->aAZ:Lbhp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhe;->aAW:Lbhf;

    invoke-virtual {v0}, Lbhf;->yu()V

    iget-boolean v0, p0, Lbhe;->aBc:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbhe;->aAW:Lbhf;

    iget-object v1, p0, Lbhe;->aAZ:Lbhp;

    invoke-direct {p0}, Lbhe;->yp()Lbho;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbhf;->a(Lbhp;Lbho;)V

    :cond_0
    iget-object v0, p0, Lbhe;->aAY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbhe;->aAY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private isCanceled()Z
    .locals 2

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbhe;->aBc:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private yo()Z
    .locals 4

    iget-object v0, p0, Lbhe;->aAX:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private yp()Lbho;
    .locals 3

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbhe;->aBb:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lbjr;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lbjr;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbhe;->aBa:Lbho;

    invoke-virtual {p0}, Lbhe;->yr()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private ys()V
    .locals 2

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->aBo:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lbhe;->a(Lcom/google/android/gms/common/api/Status;)Lbho;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbhe;->c(Lbho;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhe;->aBd:Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private yt()V
    .locals 2

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->aBp:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lbhe;->a(Lcom/google/android/gms/common/api/Status;)Lbho;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbhe;->c(Lbho;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhe;->aBd:Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Lbho;
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "await must not be called on the UI thread when time is greater than zero."

    invoke-static {v0, v3}, Lbjr;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lbhe;->aBb:Z

    if-nez v0, :cond_3

    :goto_1
    const-string v0, "Result has already been consumed."

    invoke-static {v2, v0}, Lbjr;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lbhe;->aAX:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lbhe;->yt()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lbjr;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbhe;->yp()Lbho;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lbhe;->ys()V

    goto :goto_2
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/Status;)Lbho;
.end method

.method protected final a(Lbhf;)V
    .locals 0

    iput-object p1, p0, Lbhe;->aAW:Lbhf;

    return-void
.end method

.method public final a(Lbhp;)V
    .locals 3

    iget-boolean v0, p0, Lbhe;->aBb:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lbjr;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lbhe;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbhe;->aAW:Lbhf;

    invoke-direct {p0}, Lbhe;->yp()Lbho;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lbhf;->a(Lbhp;Lbho;)V

    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    iput-object p1, p0, Lbhe;->aAZ:Lbhp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method protected final a(Lbjg;)V
    .locals 2

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lbhe;->aBe:Lbjg;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final synthetic al(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lbho;

    invoke-virtual {p0, p1}, Lbhe;->c(Lbho;)V

    return-void
.end method

.method public final c(Lbho;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lbhe;->aBd:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lbhe;->aBc:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p1}, Lbhd;->b(Lbho;)V

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lbjr;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lbhe;->aBb:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lbjr;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lbhe;->d(Lbho;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final cancel()V
    .locals 2

    iget-object v1, p0, Lbhe;->aAV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbhe;->aBc:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbhe;->aBb:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbhe;->aBe:Lbjg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lbhe;->aBe:Lbjg;

    invoke-interface {v0}, Lbjg;->cancel()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    iget-object v0, p0, Lbhe;->aBa:Lbho;

    invoke-static {v0}, Lbhd;->b(Lbho;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbhe;->aAZ:Lbhp;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhe;->aBc:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->aBq:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lbhe;->a(Lcom/google/android/gms/common/api/Status;)Lbho;

    move-result-object v0

    invoke-direct {p0, v0}, Lbhe;->d(Lbho;)V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final yq()Lbho;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "await must not be called on the UI thread"

    invoke-static {v0, v3}, Lbjr;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lbhe;->aBb:Z

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Result has already been consumed"

    invoke-static {v1, v0}, Lbjr;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lbhe;->aAX:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-direct {p0}, Lbhe;->yo()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lbjr;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbhe;->yp()Lbho;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lbhe;->ys()V

    goto :goto_2
.end method

.method protected yr()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhe;->aBb:Z

    iput-object v1, p0, Lbhe;->aBa:Lbho;

    iput-object v1, p0, Lbhe;->aAZ:Lbhp;

    return-void
.end method
