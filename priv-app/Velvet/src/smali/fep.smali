.class public final Lfep;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mSignedCipherHelper:Lfdh;


# direct methods
.method public constructor <init>(Lfdh;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lfep;->mSignedCipherHelper:Lfdh;

    .line 39
    return-void
.end method

.method public static P([B)Landroid/location/Location;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 156
    array-length v1, p0

    const/16 v2, 0x14

    if-eq v1, v2, :cond_0

    .line 166
    :goto_0
    return-object v0

    .line 160
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 161
    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 163
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 164
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 165
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/location/Location;->setAccuracy(F)V

    move-object v0, v1

    .line 166
    goto :goto_0
.end method

.method public static h(Landroid/location/Location;)[B
    .locals 4

    .prologue
    .line 92
    const/16 v0, 0x14

    new-array v0, v0, [B

    .line 93
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 94
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 95
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 96
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    .line 97
    return-object v0
.end method


# virtual methods
.method public final O([B)Landroid/location/Location;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lfep;->mSignedCipherHelper:Lfdh;

    invoke-interface {v0, p1}, Lfdh;->J([B)[B

    move-result-object v0

    .line 141
    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lfep;->P([B)Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcyg;Ljava/lang/String;)Landroid/location/Location;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 110
    .line 112
    invoke-interface {p1, p2, v0}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v1

    .line 113
    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {p0, v1}, Lfep;->O([B)Landroid/location/Location;

    move-result-object v0

    .line 118
    if-nez v0, :cond_0

    .line 119
    const-string v1, "LocationStorage"

    const-string v2, "Clearing bad lastloc from prefs"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-interface {p1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    .line 123
    const-string v2, "lastloc"

    invoke-interface {v1, v2}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 124
    invoke-interface {v1}, Lcyh;->apply()V

    .line 129
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/location/Location;Lcyg;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-interface {p2}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 53
    if-nez p1, :cond_0

    .line 54
    invoke-interface {v0, p3}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 65
    :goto_0
    invoke-interface {v0}, Lcyh;->apply()V

    .line 67
    :goto_1
    return-void

    .line 57
    :cond_0
    invoke-virtual {p0, p1}, Lfep;->g(Landroid/location/Location;)[B

    move-result-object v1

    .line 58
    if-nez v1, :cond_1

    .line 60
    const-string v0, "LocationStorage"

    const-string v1, "error writing sidekick location (crypto fail)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 63
    :cond_1
    invoke-interface {v0, p3, v1}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    goto :goto_0
.end method

.method public final g(Landroid/location/Location;)[B
    .locals 2

    .prologue
    .line 77
    invoke-static {p1}, Lfep;->h(Landroid/location/Location;)[B

    move-result-object v0

    .line 79
    if-eqz v0, :cond_0

    iget-object v1, p0, Lfep;->mSignedCipherHelper:Lfdh;

    invoke-interface {v1, v0}, Lfdh;->I([B)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
