.class public final Lfym;
.super Lfsz;
.source "PG"


# static fields
.field static final cDp:[Ljava/lang/Integer;


# instance fields
.field private cDq:Landroid/widget/PopupWindow;

.field private cDr:Landroid/widget/ListPopupWindow;

.field private cDs:Lfyv;

.field private final mClock:Lemp;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 52
    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lfym;->cDp:[Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lfsz;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 78
    iput-object p4, p0, Lfym;->mClock:Lemp;

    .line 79
    return-void
.end method

.method private A(Landroid/view/View;I)V
    .locals 5

    .prologue
    .line 214
    if-nez p2, :cond_0

    .line 215
    iget-object v0, p0, Lfym;->mContext:Landroid/content/Context;

    const v1, 0x7f0a029a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 224
    :goto_0
    const v0, 0x7f11031a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    return-void

    .line 217
    :cond_0
    const v0, 0x7f10002d

    .line 218
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agM:Laoy;

    invoke-virtual {v1}, Laoy;->rK()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agM:Laoy;

    invoke-virtual {v0}, Laoy;->rJ()I

    move-result v0

    .line 221
    :cond_1
    iget-object v1, p0, Lfym;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method private G(Lizj;)V
    .locals 4

    .prologue
    .line 537
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f11025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 538
    iget-object v0, p0, Lfym;->cDs:Lfyv;

    if-nez v0, :cond_0

    .line 539
    new-instance v0, Lfyv;

    invoke-direct {v0, p0}, Lfyv;-><init>(Lfym;)V

    iput-object v0, p0, Lfym;->cDs:Lfyv;

    .line 540
    iget-object v0, p0, Lfym;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lfym;->cDs:Lfyv;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.now.ENTRIES_UPDATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 543
    :cond_0
    iget-object v0, p0, Lfym;->mCardContainer:Lfmt;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lfmt;->c(Lizj;Z)V

    .line 544
    return-void
.end method

.method private Ui()V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f11025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 550
    if-eqz v0, :cond_0

    .line 551
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 553
    :cond_0
    return-void
.end method

.method private static a(Landroid/widget/ArrayAdapter;Landroid/view/ViewGroup;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 388
    .line 389
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 390
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 391
    const/4 v1, 0x0

    move-object v2, v1

    move v1, v0

    .line 392
    :goto_0
    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 393
    invoke-virtual {p0, v1, v2, p1}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 394
    if-eqz v2, :cond_0

    .line 395
    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 396
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 392
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 399
    :cond_1
    return v0
.end method

.method static synthetic a(Lfym;I)I
    .locals 1

    .prologue
    .line 50
    const v0, 0x7f110341

    if-eq p1, v0, :cond_2

    const v0, 0x7f110343

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f110347

    if-ne p1, v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const v0, 0x7f110345

    if-ne p1, v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lfym;Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lfym;->cDr:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic a(Lfym;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lfym;->cDq:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private static a(Landroid/widget/TextView;I)V
    .locals 5

    .prologue
    .line 110
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 111
    if-eqz v3, :cond_0

    .line 112
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, p1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 110
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_1
    return-void
.end method

.method static synthetic a(Lfym;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lfym;->Ui()V

    return-void
.end method

.method static synthetic a(Lfym;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lfym;->bz(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lfym;Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lfym;->A(Landroid/view/View;I)V

    return-void
.end method

.method private aDS()I
    .locals 4

    .prologue
    .line 500
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agM:Laoy;

    .line 501
    iget-object v0, v0, Laoy;->akw:Ljhk;

    .line 502
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Ljhk;->elQ:Ljhj;

    invoke-virtual {v0}, Ljhj;->blN()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method static synthetic b(Lfym;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lfym;->by(Landroid/view/View;)V

    return-void
.end method

.method private by(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agM:Laoy;

    .line 232
    iget-object v0, v0, Laoy;->akw:Ljhk;

    .line 233
    iget-object v1, p0, Lfym;->mContext:Landroid/content/Context;

    iget-object v0, v0, Ljhk;->elQ:Ljhj;

    invoke-virtual {v0}, Ljhj;->blM()I

    move-result v0

    invoke-static {v1, v0}, Lfym;->n(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 235
    const v0, 0x7f11034a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    return-void
.end method

.method private bz(Landroid/view/View;)V
    .locals 11

    .prologue
    const v10, 0x7f110342

    const/4 v3, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const v8, 0x3ec28f5c    # 0.38f

    const/4 v2, 0x1

    .line 242
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agM:Laoy;

    .line 243
    iget-object v0, v0, Laoy;->akw:Ljhk;

    .line 246
    iget-object v1, p0, Lfym;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b00b4

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 247
    iget-object v1, p0, Lfym;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x106000c

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 248
    iget-object v0, v0, Ljhk;->elQ:Ljhj;

    invoke-virtual {v0}, Ljhj;->aEz()I

    move-result v9

    .line 249
    if-nez v9, :cond_0

    move v1, v2

    .line 253
    :goto_0
    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    move v4, v5

    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 255
    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    move v1, v7

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 258
    if-ne v9, v2, :cond_3

    move v1, v2

    .line 259
    :goto_3
    const v0, 0x7f110344

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_4

    move v4, v5

    :goto_4
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 261
    const v0, 0x7f110344

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    move v1, v7

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 264
    const/4 v0, 0x3

    if-ne v9, v0, :cond_6

    move v1, v2

    .line 265
    :goto_6
    const v0, 0x7f110348

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_7

    move v4, v5

    :goto_7
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 267
    const v0, 0x7f110348

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_8

    move v1, v7

    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 270
    const/4 v0, 0x2

    if-ne v9, v0, :cond_9

    .line 271
    :goto_9
    const v0, 0x7f110346

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v2, :cond_a

    :goto_a
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 273
    const v0, 0x7f110346

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v2, :cond_b

    :goto_b
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 275
    return-void

    :cond_0
    move v1, v3

    .line 249
    goto :goto_0

    :cond_1
    move v4, v6

    .line 253
    goto :goto_1

    :cond_2
    move v1, v8

    .line 255
    goto :goto_2

    :cond_3
    move v1, v3

    .line 258
    goto :goto_3

    :cond_4
    move v4, v6

    .line 259
    goto :goto_4

    :cond_5
    move v1, v8

    .line 261
    goto :goto_5

    :cond_6
    move v1, v3

    .line 264
    goto :goto_6

    :cond_7
    move v4, v6

    .line 265
    goto :goto_7

    :cond_8
    move v1, v8

    .line 267
    goto :goto_8

    :cond_9
    move v2, v3

    .line 270
    goto :goto_9

    :cond_a
    move v5, v6

    .line 271
    goto :goto_a

    :cond_b
    move v7, v8

    .line 273
    goto :goto_b
.end method

.method public static n(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    .prologue
    const v0, 0x7f0a0299

    .line 370
    packed-switch p1, :pswitch_data_0

    .line 378
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 372
    :pswitch_0
    const v0, 0x7f0a0297

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 374
    :pswitch_1
    const v0, 0x7f0a0298

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 376
    :pswitch_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 370
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 92
    const v0, 0x7f040133

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final aCZ()V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0}, Lfsz;->aCZ()V

    .line 87
    invoke-direct {p0}, Lfym;->Ui()V

    .line 88
    return-void
.end method

.method protected final aDn()Ljava/lang/String;
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lfym;->mContext:Landroid/content/Context;

    const v1, 0x7f0a050b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final aDo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lfym;->mContext:Landroid/content/Context;

    const v1, 0x7f0a050c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 97
    const v0, 0x7f040132

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b00b0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 101
    const v0, 0x7f11034a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 102
    invoke-static {v0, v2}, Lfym;->a(Landroid/widget/TextView;I)V

    .line 103
    const v0, 0x7f11031a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 104
    invoke-static {v0, v2}, Lfym;->a(Landroid/widget/TextView;I)V

    .line 106
    return-object v1
.end method

.method final b(Landroid/content/Context;Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 308
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 309
    new-instance v0, Lfyq;

    const v3, 0x7f040176

    sget-object v4, Lfym;->cDp:[Ljava/lang/Integer;

    move-object v1, p0

    move-object v2, p1

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lfyq;-><init>(Lfym;Landroid/content/Context;I[Ljava/lang/Integer;Landroid/view/LayoutInflater;Landroid/content/Context;)V

    .line 326
    new-instance v2, Landroid/widget/ListPopupWindow;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v3, 0x7f090174

    invoke-direct {v1, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v2, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    .line 328
    invoke-virtual {v2, v0}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 329
    invoke-virtual {v2, p2}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 330
    invoke-virtual {v2, v7}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 331
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget v3, v1, Landroid/graphics/Rect;->left:I

    rsub-int/lit8 v3, v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    iget v1, v1, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v1, v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 333
    :cond_0
    new-instance v1, Lfyr;

    invoke-direct {v1, p0, p2, v2}, Lfyr;-><init>(Lfym;Landroid/view/View;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {v2, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 344
    new-instance v1, Lfys;

    invoke-direct {v1, p0, p2}, Lfys;-><init>(Lfym;Landroid/view/View;)V

    invoke-virtual {v2, v1}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 350
    invoke-virtual {p2, v7}, Landroid/view/View;->setSelected(Z)V

    .line 352
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lfym;->a(Landroid/widget/ArrayAdapter;Landroid/view/ViewGroup;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/ListPopupWindow;->setContentWidth(I)V

    .line 353
    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->show()V

    .line 354
    iput-object v2, p0, Lfym;->cDr:Landroid/widget/ListPopupWindow;

    .line 355
    return-void
.end method

.method protected final bv(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 146
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v8, v0, Lanh;->agM:Laoy;

    .line 147
    iget-object v1, v8, Laoy;->akw:Ljhk;

    .line 150
    invoke-direct {p0, p1}, Lfym;->bz(Landroid/view/View;)V

    .line 151
    new-instance v0, Lfyn;

    invoke-direct {v0, p0, p1}, Lfyn;-><init>(Lfym;Landroid/view/View;)V

    .line 159
    const v2, 0x7f110341

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v2, 0x7f110343

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    const v2, 0x7f110345

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    const v2, 0x7f110347

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-direct {p0, p1}, Lfym;->by(Landroid/view/View;)V

    .line 170
    invoke-virtual {v1}, Ljhk;->blP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    const v0, 0x7f11034a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 184
    :goto_0
    invoke-direct {p0}, Lfym;->aDS()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lfym;->A(Landroid/view/View;I)V

    .line 185
    invoke-virtual {v1}, Ljhk;->blQ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    const v0, 0x7f11031a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 198
    :goto_1
    const v0, 0x7f110090

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewGroup;

    .line 200
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 201
    iget-object v0, v8, Laoy;->agX:Lapj;

    if-eqz v0, :cond_0

    .line 202
    new-instance v0, Lfze;

    iget-object v1, p0, Lfym;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfym;->mCardContainer:Lfmt;

    invoke-virtual {p0}, Lfym;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lfym;->mActivityHelper:Lfzw;

    iget-object v5, p0, Lfro;->cBc:Lanh;

    iget-object v5, v5, Lanh;->ahu:Lizj;

    iget-object v6, p0, Lfym;->mClock:Lemp;

    invoke-direct/range {v0 .. v6}, Lfze;-><init>(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Lfzw;Lizj;Lemp;)V

    iget-object v1, v8, Laoy;->agX:Lapj;

    const/4 v2, 0x1

    invoke-virtual {v0, v7, v1, v2}, Lfze;->a(Landroid/view/ViewGroup;Lapj;Z)V

    .line 205
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 207
    :cond_0
    return-void

    .line 174
    :cond_1
    const v0, 0x7f110349

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lfyo;

    invoke-direct {v2, p0}, Lfyo;-><init>(Lfym;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 189
    :cond_2
    const v0, 0x7f11034b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lfyp;

    invoke-direct {v1, p0}, Lfyp;-><init>(Lfym;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method final c(Landroid/content/Context;Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 416
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040024

    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 419
    const v0, 0x7f1100aa

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 421
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v3, v1, Lanh;->agM:Laoy;

    .line 422
    invoke-direct {p0}, Lfym;->aDS()I

    move-result v4

    .line 424
    new-instance v5, Landroid/widget/PopupWindow;

    invoke-direct {v5, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 426
    const/16 v1, 0x5a

    .line 427
    invoke-virtual {v3}, Laoy;->rI()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 428
    invoke-virtual {v3}, Laoy;->rH()I

    move-result v1

    .line 430
    :cond_0
    div-int/lit8 v1, v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 431
    div-int/lit8 v1, v4, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 432
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x106000b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setBackgroundColor(I)V

    .line 433
    new-instance v1, Lfyt;

    invoke-direct {v1, p0, p2, v5}, Lfyt;-><init>(Lfym;Landroid/view/View;Landroid/widget/PopupWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 462
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 463
    invoke-virtual {v5, v0}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 464
    invoke-virtual {v5, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 467
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 468
    int-to-double v2, v1

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v6

    double-to-int v1, v2

    invoke-virtual {v5, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 469
    const/4 v1, -0x2

    invoke-virtual {v5, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 471
    invoke-virtual {v5, v8}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 473
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 474
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 475
    iget v0, v1, Landroid/graphics/Rect;->left:I

    rsub-int/lit8 v0, v0, 0x0

    iget v1, v1, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v1, v1, 0x0

    invoke-virtual {v5, p2, v0, v1}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 476
    iput-object v5, p0, Lfym;->cDq:Landroid/widget/PopupWindow;

    .line 478
    new-instance v0, Lfyu;

    invoke-direct {v0, p0, p2}, Lfyu;-><init>(Lfym;Landroid/view/View;)V

    invoke-virtual {v5, v0}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 484
    invoke-virtual {p2, v8}, Landroid/view/View;->setSelected(Z)V

    .line 485
    return-void
.end method

.method protected final d(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 120
    iget-object v2, v0, Lanh;->agM:Laoy;

    .line 122
    const v0, 0x7f1101a9

    invoke-virtual {v2}, Laoy;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v3}, Lfym;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 123
    const v0, 0x7f110080

    invoke-virtual {v2}, Laoy;->ol()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v3}, Lfym;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 124
    const v0, 0x7f11034c

    iget-object v3, v2, Laoy;->akw:Ljhk;

    invoke-virtual {v3}, Ljhk;->blR()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v3}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 128
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahC:Lanb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahC:Lanb;

    invoke-virtual {v0}, Lanb;->oi()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const v0, 0x7f1102eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 132
    if-eqz v0, :cond_0

    .line 133
    iget-object v3, p0, Lfro;->cBc:Lanh;

    iget-object v3, v3, Lanh;->ahs:Lani;

    iget-object v3, v3, Lani;->ahC:Lanb;

    invoke-virtual {v3}, Lanb;->oh()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 134
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    :cond_0
    const v0, 0x7f110095

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 139
    if-eqz p2, :cond_1

    iget-object v1, p0, Lfym;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b00b1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 140
    :cond_1
    iget-object v2, v2, Laoy;->akz:[Laoz;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Lfyx;->a(Landroid/widget/LinearLayout;[Laoz;Ljava/lang/String;I)V

    .line 142
    return-void
.end method

.method final jL(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 299
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahu:Lizj;

    .line 300
    iget-object v1, v0, Lizj;->dUv:Ljhk;

    iget-object v1, v1, Ljhk;->elQ:Ljhj;

    invoke-virtual {v1, p1}, Ljhj;->pU(I)Ljhj;

    .line 301
    iget-object v1, p0, Lfym;->mCardContainer:Lfmt;

    const/16 v2, 0x9a

    invoke-interface {v1, v0, v2, v3, v3}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 302
    invoke-direct {p0, v0}, Lfym;->G(Lizj;)V

    .line 303
    const/4 v0, 0x1

    return v0
.end method

.method final jM(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 407
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahu:Lizj;

    .line 408
    iget-object v1, v0, Lizj;->dUv:Ljhk;

    iget-object v1, v1, Ljhk;->elQ:Ljhj;

    invoke-virtual {v1, p1}, Ljhj;->pS(I)Ljhj;

    .line 409
    iget-object v1, p0, Lfym;->mCardContainer:Lfmt;

    const/16 v2, 0x9a

    invoke-interface {v1, v0, v2, v3, v3}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 410
    invoke-direct {p0, v0}, Lfym;->G(Lizj;)V

    .line 411
    const/4 v0, 0x1

    return v0
.end method

.method final jN(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 492
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahu:Lizj;

    .line 493
    iget-object v1, v0, Lizj;->dUv:Ljhk;

    iget-object v1, v1, Ljhk;->elQ:Ljhj;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Lius;->bY(J)I

    move-result v2

    invoke-virtual {v1, v2}, Ljhj;->pT(I)Ljhj;

    .line 495
    iget-object v1, p0, Lfym;->mCardContainer:Lfmt;

    const/16 v2, 0x9a

    invoke-interface {v1, v0, v2, v6, v6}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 496
    invoke-direct {p0, v0}, Lfym;->G(Lizj;)V

    .line 497
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 507
    invoke-super {p0, p1}, Lfsz;->onViewDetachedFromWindow(Landroid/view/View;)V

    .line 509
    iget-object v0, p0, Lfym;->cDq:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lfym;->cDq:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 511
    iput-object v1, p0, Lfym;->cDq:Landroid/widget/PopupWindow;

    .line 513
    :cond_0
    iget-object v0, p0, Lfym;->cDr:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lfym;->cDr:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 515
    iput-object v1, p0, Lfym;->cDr:Landroid/widget/ListPopupWindow;

    .line 519
    :cond_1
    iget-object v0, p0, Lfym;->cDs:Lfyv;

    if-eqz v0, :cond_2

    .line 520
    iget-object v0, p0, Lfym;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lfym;->cDs:Lfyv;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 522
    :cond_2
    return-void
.end method
