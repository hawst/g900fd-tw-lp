.class public abstract Lcol;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lkem;ZLjava/lang/String;)[B
    .locals 10

    .prologue
    const/16 v9, 0x2c

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-virtual {p0}, Lkem;->aoM()Ljava/lang/String;

    move-result-object v5

    .line 50
    const-string v3, "0"

    .line 53
    iget-object v0, p0, Lkem;->eUR:Lken;

    if-eqz v0, :cond_5

    .line 54
    iget-object v0, p0, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->bzy()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcol;->cy(Z)Ljava/lang/String;

    move-result-object v3

    .line 55
    new-instance v0, Ljava/lang/String;

    iget-object v4, p0, Lkem;->eUR:Lken;

    invoke-virtual {v4}, Lken;->acS()[B

    move-result-object v4

    sget-object v6, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v4, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-static {v0}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 65
    iget-object v6, p0, Lkem;->eUQ:Lkel;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lkem;->eUQ:Lkel;

    iget-object v6, v6, Lkel;->eUJ:Ljyw;

    if-eqz v6, :cond_0

    .line 66
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\"ectcm\":"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lkem;->eUQ:Lkel;

    iget-object v7, v7, Lkel;->eUJ:Ljyw;

    invoke-static {v7}, Lcol;->b(Ljsr;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\"ectais\":"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lkem;->bzG()Z

    move-result v7

    invoke-static {v7}, Lcol;->cy(Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v6, p0, Lkem;->eUN:[I

    if-eqz v6, :cond_1

    iget-object v6, p0, Lkem;->eUN:[I

    array-length v6, v6

    if-lez v6, :cond_1

    .line 70
    invoke-static {v9}, Lifj;->e(C)Lifj;

    move-result-object v6

    iget-object v7, p0, Lkem;->eUN:[I

    invoke-static {v7}, Lius;->w([I)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v6, v7}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    .line 71
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\"modes\":["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_1
    iget-object v6, p0, Lkem;->eUR:Lken;

    if-eqz v6, :cond_3

    .line 74
    iget-object v6, p0, Lkem;->eUR:Lken;

    invoke-virtual {v6}, Lken;->bhf()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 75
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\"fp\":\""

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lkem;->eUR:Lken;

    invoke-virtual {v7}, Lken;->bzH()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_2
    iget-object v6, p0, Lkem;->eUR:Lken;

    iget-object v6, v6, Lken;->eUY:[Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lkem;->eUR:Lken;

    iget-object v6, v6, Lken;->eUY:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_3

    .line 79
    invoke-static {v9}, Lifj;->e(C)Lifj;

    move-result-object v6

    iget-object v7, p0, Lkem;->eUR:Lken;

    iget-object v7, v7, Lken;->eUY:[Ljava/lang/String;

    invoke-virtual {v6, v7}, Lifj;->b([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 80
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\"fpil\":\""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_3
    invoke-static {v9}, Lifj;->e(C)Lifj;

    move-result-object v6

    invoke-virtual {v6, v4}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    .line 85
    if-eqz p1, :cond_8

    const-string v4, "/*\"\"*/"

    .line 87
    :goto_2
    const-string v7, "{\"e\":\"%s\",\"c\":%s,\"u\":\"%s\",\"d\":%s,\"a\":{%s}}%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v2

    aput-object v3, v8, v1

    const/4 v1, 0x2

    aput-object p2, v8, v1

    const/4 v1, 0x3

    aput-object v0, v8, v1

    const/4 v0, 0x4

    aput-object v6, v8, v0

    const/4 v0, 0x5

    aput-object v4, v8, v0

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v2

    .line 54
    goto/16 :goto_0

    .line 56
    :cond_5
    iget-object v0, p0, Lkem;->eUQ:Lkel;

    if-eqz v0, :cond_7

    .line 57
    iget-object v0, p0, Lkem;->eUQ:Lkel;

    invoke-virtual {v0}, Lkel;->bzy()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcol;->cy(Z)Ljava/lang/String;

    move-result-object v3

    .line 58
    iget-object v0, p0, Lkem;->eUQ:Lkel;

    iget-object v0, v0, Lkel;->eUG:Ljrp;

    invoke-static {v0}, Lcol;->b(Ljsr;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 57
    goto :goto_3

    .line 60
    :cond_7
    const-string v0, ""

    .line 61
    const-string v4, "PelletParser"

    const-string v6, "No SRP or action in pellet"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v4, v6, v7}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 85
    :cond_8
    const-string v4, ""

    goto :goto_2
.end method

.method private static b(Ljsr;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/String;

    invoke-static {p0}, Leqh;->e(Ljsr;)[B

    move-result-object v2

    sget-object v3, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static cy(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    if-eqz p0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method


# virtual methods
.method public abstract QY()Lcog;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method
