.class public final Lcxr;
.super Lcyc;
.source "PG"


# instance fields
.field private final mConfig:Lcjs;

.field private final mLoginHelper:Lcrh;


# direct methods
.method public constructor <init>(Lcjs;Lcrh;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 14
    iput-object p1, p0, Lcxr;->mConfig:Lcjs;

    .line 15
    iput-object p2, p0, Lcxr;->mLoginHelper:Lcrh;

    .line 16
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcxr;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Ma()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxr;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
