.class public final Ldmg;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private final bCi:Ljava/io/InputStream;

.field private final bCj:Ljava/io/InputStream;

.field private bCk:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    .line 39
    instance-of v0, p1, Ldki;

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 40
    instance-of v0, p2, Ldki;

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 41
    iput-object p1, p0, Ldmg;->bCi:Ljava/io/InputStream;

    .line 42
    iput-object p2, p0, Ldmg;->bCj:Ljava/io/InputStream;

    .line 43
    iget-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Ljava/io/SequenceInputStream;

    invoke-direct {v1, p1, p2}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/SequenceInputStream;

    invoke-virtual {v0}, Ljava/io/SequenceInputStream;->available()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/SequenceInputStream;

    invoke-virtual {v0}, Ljava/io/SequenceInputStream;->close()V

    .line 64
    return-void
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/SequenceInputStream;

    invoke-virtual {v0}, Ljava/io/SequenceInputStream;->read()I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/SequenceInputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/SequenceInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public final reset()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Ldmg;->bCi:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 80
    iget-object v0, p0, Ldmg;->bCj:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 81
    iget-object v0, p0, Ldmg;->bCi:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 82
    iget-object v0, p0, Ldmg;->bCj:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 84
    iget-object v0, p0, Ldmg;->bCk:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Ljava/io/SequenceInputStream;

    iget-object v2, p0, Ldmg;->bCi:Ljava/io/InputStream;

    iget-object v3, p0, Ldmg;->bCj:Ljava/io/InputStream;

    invoke-direct {v1, v2, v3}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-void
.end method
