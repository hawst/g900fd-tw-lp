.class public final Lckw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bai:Lckw;


# instance fields
.field private aPP:I

.field private final baj:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lckw;

    invoke-direct {v0}, Lckw;-><init>()V

    sput-object v0, Lckw;->bai:Lckw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lckw;->aPP:I

    .line 26
    const/4 v0, 0x0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lckw;->baj:Ljava/util/List;

    return-void
.end method

.method public static OY()Lckw;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lckw;

    invoke-direct {v0}, Lckw;-><init>()V

    return-object v0
.end method

.method public static OZ()Lckw;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lckw;->bai:Lckw;

    return-object v0
.end method

.method private Pc()V
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p0}, Lckw;->Pd()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 95
    iget-object v0, p0, Lckw;->baj:Ljava/util/List;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to DebugFeatures too early from main thread"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_0
    return-void
.end method


# virtual methods
.method public final Pa()Z
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lckw;->Pc()V

    .line 71
    iget v0, p0, Lckw;->aPP:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Pb()Z
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lckw;->Pc()V

    .line 90
    iget v0, p0, Lckw;->aPP:I

    invoke-static {v0}, Ledw;->hm(I)Z

    move-result v0

    return v0
.end method

.method public final Pd()Z
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lckw;->aPP:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcke;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-interface {p1}, Lcke;->Og()I

    move-result v0

    iput v0, p0, Lckw;->aPP:I

    .line 42
    iget-object v0, p0, Lckw;->baj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lckw;->baj:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/IllegalStateException;

    throw v0

    .line 47
    :cond_0
    invoke-virtual {p0}, Lckw;->Pa()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    :goto_0
    iget-object v0, p0, Lckw;->baj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 49
    iget-object v0, p0, Lckw;->baj:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/IllegalStateException;

    .line 52
    const-string v1, "DebugFeatures"

    const-string v2, "premature call to get method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 55
    :cond_1
    iget-object v0, p0, Lckw;->baj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 57
    :cond_2
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DebugFeatures{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lckw;->aPP:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
