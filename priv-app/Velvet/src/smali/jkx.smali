.class public final Ljkx;
.super Ljsl;
.source "PG"


# static fields
.field public static final eqH:Ljsm;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public eqI:Ljns;

.field public eqJ:[Ljnm;

.field private eqK:Z

.field private eqL:Z

.field private eqM:I

.field private eqN:Ljava/lang/String;

.field private eqO:Ljava/lang/String;

.field private eqP:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12321
    const/16 v0, 0xb

    const-class v1, Ljkx;

    const v2, 0x16fa0e82

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljkx;->eqH:Ljsm;

    .line 12327
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 12485
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12486
    iput v1, p0, Ljkx;->aez:I

    iput-object v2, p0, Ljkx;->eqI:Ljns;

    invoke-static {}, Ljnm;->bqI()[Ljnm;

    move-result-object v0

    iput-object v0, p0, Ljkx;->eqJ:[Ljnm;

    iput-boolean v1, p0, Ljkx;->eqK:Z

    iput-boolean v1, p0, Ljkx;->eqL:Z

    iput v1, p0, Ljkx;->eqM:I

    const-string v0, ""

    iput-object v0, p0, Ljkx;->eqN:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljkx;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljkx;->eqO:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljkx;->eqP:Ljava/lang/String;

    iput-object v2, p0, Ljkx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkx;->eCz:I

    .line 12487
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12314
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljkx;->eqI:Ljns;

    if-nez v0, :cond_1

    new-instance v0, Ljns;

    invoke-direct {v0}, Ljns;-><init>()V

    iput-object v0, p0, Ljkx;->eqI:Ljns;

    :cond_1
    iget-object v0, p0, Ljkx;->eqI:Ljns;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljkx;->eqJ:[Ljnm;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljnm;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljkx;->eqJ:[Ljnm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljnm;

    invoke-direct {v3}, Ljnm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljkx;->eqJ:[Ljnm;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljnm;

    invoke-direct {v3}, Ljnm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljkx;->eqJ:[Ljnm;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljkx;->eqK:Z

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkx;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljkx;->eqL:Z

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkx;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljkx;->eqM:I

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkx;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkx;->eqN:Ljava/lang/String;

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljkx;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkx;->afh:Ljava/lang/String;

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljkx;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkx;->eqO:Ljava/lang/String;

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljkx;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkx;->eqP:Ljava/lang/String;

    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljkx;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 12508
    iget-object v0, p0, Ljkx;->eqI:Ljns;

    if-eqz v0, :cond_0

    .line 12509
    const/4 v0, 0x1

    iget-object v1, p0, Ljkx;->eqI:Ljns;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 12511
    :cond_0
    iget-object v0, p0, Ljkx;->eqJ:[Ljnm;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljkx;->eqJ:[Ljnm;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 12512
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljkx;->eqJ:[Ljnm;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 12513
    iget-object v1, p0, Ljkx;->eqJ:[Ljnm;

    aget-object v1, v1, v0

    .line 12514
    if-eqz v1, :cond_1

    .line 12515
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 12512
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12519
    :cond_2
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 12520
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljkx;->eqK:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12522
    :cond_3
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 12523
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljkx;->eqL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12525
    :cond_4
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 12526
    const/4 v0, 0x5

    iget v1, p0, Ljkx;->eqM:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 12528
    :cond_5
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 12529
    const/4 v0, 0x6

    iget-object v1, p0, Ljkx;->eqN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12531
    :cond_6
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    .line 12532
    const/4 v0, 0x7

    iget-object v1, p0, Ljkx;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12534
    :cond_7
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 12535
    const/16 v0, 0x8

    iget-object v1, p0, Ljkx;->eqO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12537
    :cond_8
    iget v0, p0, Ljkx;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 12538
    const/16 v0, 0x9

    iget-object v1, p0, Ljkx;->eqP:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12540
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12541
    return-void
.end method

.method public final agZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12400
    iget-object v0, p0, Ljkx;->eqN:Ljava/lang/String;

    return-object v0
.end method

.method public final aha()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12444
    iget-object v0, p0, Ljkx;->eqO:Ljava/lang/String;

    return-object v0
.end method

.method public final ahb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12466
    iget-object v0, p0, Ljkx;->eqP:Ljava/lang/String;

    return-object v0
.end method

.method public final boE()Z
    .locals 1

    .prologue
    .line 12343
    iget-boolean v0, p0, Ljkx;->eqK:Z

    return v0
.end method

.method public final boF()Z
    .locals 1

    .prologue
    .line 12362
    iget-boolean v0, p0, Ljkx;->eqL:Z

    return v0
.end method

.method public final boG()I
    .locals 1

    .prologue
    .line 12381
    iget v0, p0, Ljkx;->eqM:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12422
    iget-object v0, p0, Ljkx;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final iv(Z)Ljkx;
    .locals 1

    .prologue
    .line 12346
    iput-boolean p1, p0, Ljkx;->eqK:Z

    .line 12347
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkx;->aez:I

    .line 12348
    return-object p0
.end method

.method public final iw(Z)Ljkx;
    .locals 1

    .prologue
    .line 12365
    iput-boolean p1, p0, Ljkx;->eqL:Z

    .line 12366
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkx;->aez:I

    .line 12367
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 12545
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12546
    iget-object v1, p0, Ljkx;->eqI:Ljns;

    if-eqz v1, :cond_0

    .line 12547
    const/4 v1, 0x1

    iget-object v2, p0, Ljkx;->eqI:Ljns;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12550
    :cond_0
    iget-object v1, p0, Ljkx;->eqJ:[Ljnm;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljkx;->eqJ:[Ljnm;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 12551
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljkx;->eqJ:[Ljnm;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 12552
    iget-object v2, p0, Ljkx;->eqJ:[Ljnm;

    aget-object v2, v2, v0

    .line 12553
    if-eqz v2, :cond_1

    .line 12554
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 12551
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 12559
    :cond_3
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    .line 12560
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljkx;->eqK:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12563
    :cond_4
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 12564
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljkx;->eqL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12567
    :cond_5
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 12568
    const/4 v1, 0x5

    iget v2, p0, Ljkx;->eqM:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12571
    :cond_6
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 12572
    const/4 v1, 0x6

    iget-object v2, p0, Ljkx;->eqN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12575
    :cond_7
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_8

    .line 12576
    const/4 v1, 0x7

    iget-object v2, p0, Ljkx;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12579
    :cond_8
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_9

    .line 12580
    const/16 v1, 0x8

    iget-object v2, p0, Ljkx;->eqO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12583
    :cond_9
    iget v1, p0, Ljkx;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_a

    .line 12584
    const/16 v1, 0x9

    iget-object v2, p0, Ljkx;->eqP:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12587
    :cond_a
    return v0
.end method

.method public final qA(I)Ljkx;
    .locals 1

    .prologue
    .line 12384
    iput p1, p0, Ljkx;->eqM:I

    .line 12385
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkx;->aez:I

    .line 12386
    return-object p0
.end method

.method public final ww(Ljava/lang/String;)Ljkx;
    .locals 1

    .prologue
    .line 12403
    if-nez p1, :cond_0

    .line 12404
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12406
    :cond_0
    iput-object p1, p0, Ljkx;->eqN:Ljava/lang/String;

    .line 12407
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljkx;->aez:I

    .line 12408
    return-object p0
.end method

.method public final wx(Ljava/lang/String;)Ljkx;
    .locals 1

    .prologue
    .line 12425
    if-nez p1, :cond_0

    .line 12426
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12428
    :cond_0
    iput-object p1, p0, Ljkx;->afh:Ljava/lang/String;

    .line 12429
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljkx;->aez:I

    .line 12430
    return-object p0
.end method

.method public final wy(Ljava/lang/String;)Ljkx;
    .locals 1

    .prologue
    .line 12447
    if-nez p1, :cond_0

    .line 12448
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12450
    :cond_0
    iput-object p1, p0, Ljkx;->eqO:Ljava/lang/String;

    .line 12451
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljkx;->aez:I

    .line 12452
    return-object p0
.end method

.method public final wz(Ljava/lang/String;)Ljkx;
    .locals 1

    .prologue
    .line 12469
    if-nez p1, :cond_0

    .line 12470
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12472
    :cond_0
    iput-object p1, p0, Ljkx;->eqP:Ljava/lang/String;

    .line 12473
    iget v0, p0, Ljkx;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljkx;->aez:I

    .line 12474
    return-object p0
.end method
