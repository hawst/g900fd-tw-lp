.class public final Ladx;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private TN:Landroid/net/Uri;

.field private TO:[B

.field private TP:I

.field private TQ:Landroid/graphics/RectF;

.field private TR:I

.field private TS:I

.field private TT:I

.field private TU:Ljava/lang/String;

.field private TV:Z

.field private TW:Z

.field private TX:Landroid/graphics/Bitmap;

.field private TY:Ljava/lang/Runnable;

.field private TZ:Lady;

.field private Ua:Z

.field private mContext:Landroid/content/Context;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    .line 505
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Ladx;->TN:Landroid/net/Uri;

    .line 464
    const/4 v0, 0x0

    iput v0, p0, Ladx;->TP:I

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Ladx;->TQ:Landroid/graphics/RectF;

    .line 468
    const-string v0, "jpg"

    iput-object v0, p0, Ladx;->TU:Ljava/lang/String;

    .line 506
    iput-object p1, p0, Ladx;->mContext:Landroid/content/Context;

    .line 507
    iput p3, p0, Ladx;->TP:I

    .line 508
    iput-object p2, p0, Ladx;->mResources:Landroid/content/res/Resources;

    move-object v0, p0

    move-object v1, p4

    move v2, p5

    move v3, p6

    move v4, p7

    move/from16 v5, p8

    move/from16 v6, p9

    move-object/from16 v7, p10

    .line 509
    invoke-direct/range {v0 .. v7}, Ladx;->a(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 511
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    .line 496
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Ladx;->TN:Landroid/net/Uri;

    .line 464
    const/4 v0, 0x0

    iput v0, p0, Ladx;->TP:I

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Ladx;->TQ:Landroid/graphics/RectF;

    .line 468
    const-string v0, "jpg"

    iput-object v0, p0, Ladx;->TU:Ljava/lang/String;

    .line 497
    iput-object p1, p0, Ladx;->mContext:Landroid/content/Context;

    .line 498
    iput-object p2, p0, Ladx;->TN:Landroid/net/Uri;

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    move v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    .line 499
    invoke-direct/range {v0 .. v7}, Ladx;->a(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 501
    return-void
.end method

.method public constructor <init>([BLandroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    .line 488
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Ladx;->TN:Landroid/net/Uri;

    .line 464
    const/4 v0, 0x0

    iput v0, p0, Ladx;->TP:I

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Ladx;->TQ:Landroid/graphics/RectF;

    .line 468
    const-string v0, "jpg"

    iput-object v0, p0, Ladx;->TU:Ljava/lang/String;

    .line 489
    iput-object p1, p0, Ladx;->TO:[B

    .line 490
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-direct/range {v0 .. v7}, Ladx;->a(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V

    .line 492
    return-void
.end method

.method private a(Landroid/graphics/RectF;IIIZZLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Ladx;->TQ:Landroid/graphics/RectF;

    .line 516
    iput p2, p0, Ladx;->TT:I

    .line 517
    iput p3, p0, Ladx;->TR:I

    .line 518
    iput p4, p0, Ladx;->TS:I

    .line 519
    iput-boolean p5, p0, Ladx;->TV:Z

    .line 520
    iput-boolean p6, p0, Ladx;->TW:Z

    .line 521
    iput-object p7, p0, Ladx;->TY:Ljava/lang/Runnable;

    .line 522
    return-void
.end method

.method private kp()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 538
    iget-object v0, p0, Ladx;->TN:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget v0, p0, Ladx;->TP:I

    if-nez v0, :cond_0

    iget-object v0, p0, Ladx;->TO:[B

    if-nez v0, :cond_0

    .line 539
    const-string v0, "Launcher3.CropActivity"

    const-string v1, "cannot read original file, no input URI, resource ID, or image byte array given"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 543
    :cond_0
    :try_start_0
    iget-object v0, p0, Ladx;->TN:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 544
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Ladx;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Ladx;->TN:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 553
    :catch_0
    move-exception v0

    .line 554
    const-string v1, "Launcher3.CropActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cannot read file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Ladx;->TN:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 546
    :cond_1
    :try_start_1
    iget-object v0, p0, Ladx;->TO:[B

    if-eqz v0, :cond_2

    .line 549
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Ladx;->TO:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_1

    .line 551
    :cond_2
    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Ladx;->mResources:Landroid/content/res/Resources;

    iget v2, p0, Ladx;->TP:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lady;)V
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Ladx;->TZ:Lady;

    .line 526
    return-void
.end method

.method public final ap(Z)V
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladx;->Ua:Z

    .line 530
    return-void
.end method

.method public final b(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 575
    iput-object p1, p0, Ladx;->TQ:Landroid/graphics/RectF;

    .line 576
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 459
    invoke-virtual {p0}, Ladx;->ks()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 533
    iput-object p1, p0, Ladx;->TY:Ljava/lang/Runnable;

    .line 534
    return-void
.end method

.method public final kq()Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 561
    invoke-direct {p0}, Ladx;->kp()Ljava/io/InputStream;

    move-result-object v1

    .line 562
    if-eqz v1, :cond_0

    .line 563
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 564
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 565
    invoke-static {v1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 566
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 567
    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eqz v1, :cond_0

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eqz v1, :cond_0

    .line 568
    new-instance v0, Landroid/graphics/Point;

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v2, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 571
    :cond_0
    return-object v0
.end method

.method public final kr()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Ladx;->TX:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final ks()Z
    .locals 12

    .prologue
    .line 582
    const/4 v4, 0x0

    .line 585
    const/4 v0, 0x0

    .line 586
    iget-boolean v1, p0, Ladx;->TV:Z

    if-eqz v1, :cond_0

    .line 587
    iget-object v0, p0, Ladx;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 591
    :cond_0
    iget-boolean v1, p0, Ladx;->TV:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Ladx;->Ua:Z

    if-eqz v1, :cond_3

    .line 593
    :try_start_0
    invoke-direct {p0}, Ladx;->kp()Ljava/io/InputStream;

    move-result-object v1

    .line 594
    if-eqz v1, :cond_1

    .line 595
    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 596
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    :cond_1
    :goto_0
    if-nez v4, :cond_2

    const/4 v0, 0x1

    .line 805
    :goto_1
    return v0

    .line 598
    :catch_0
    move-exception v0

    .line 599
    const-string v1, "Launcher3.CropActivity"

    const-string v2, "cannot write stream to wallpaper"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 600
    const/4 v4, 0x1

    goto :goto_0

    .line 602
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 605
    :cond_3
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 606
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 607
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 609
    invoke-virtual {p0}, Ladx;->kq()Landroid/graphics/Point;

    move-result-object v7

    .line 610
    iget v2, p0, Ladx;->TT:I

    if-lez v2, :cond_5

    .line 611
    iget v2, p0, Ladx;->TT:I

    int-to-float v2, v2

    invoke-virtual {v6, v2}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 612
    iget v2, p0, Ladx;->TT:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 614
    iget-object v2, p0, Ladx;->TQ:Landroid/graphics/RectF;

    invoke-virtual {v2, v5}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 615
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iput-object v2, p0, Ladx;->TQ:Landroid/graphics/RectF;

    .line 617
    if-nez v7, :cond_4

    .line 618
    const-string v0, "Launcher3.CropActivity"

    const-string v1, "cannot get bounds for image"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    const/4 v0, 0x0

    goto :goto_1

    .line 623
    :cond_4
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget v8, v7, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    aput v8, v2, v3

    const/4 v3, 0x1

    iget v8, v7, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    aput v8, v2, v3

    .line 624
    invoke-virtual {v6, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 625
    const/4 v3, 0x0

    const/4 v8, 0x0

    aget v8, v2, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    aput v8, v2, v3

    .line 626
    const/4 v3, 0x1

    const/4 v8, 0x1

    aget v8, v2, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    aput v8, v2, v3

    .line 628
    iget-object v3, p0, Ladx;->TQ:Landroid/graphics/RectF;

    const/4 v8, 0x0

    aget v8, v2, v8

    neg-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    const/4 v9, 0x1

    aget v2, v2, v9

    neg-float v2, v2

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v2, v9

    invoke-virtual {v3, v8, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 629
    iget-object v2, p0, Ladx;->TQ:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 630
    iget-object v1, p0, Ladx;->TQ:Landroid/graphics/RectF;

    iget v2, v7, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, v7, Landroid/graphics/Point;->y:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    .line 634
    :cond_5
    iget-object v1, p0, Ladx;->TQ:Landroid/graphics/RectF;

    invoke-virtual {v1, v5}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 636
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_6

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gtz v1, :cond_7

    .line 637
    :cond_6
    const-string v0, "Launcher3.CropActivity"

    const-string v1, "crop has bad values for full size image"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 643
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Ladx;->TR:I

    div-int/2addr v2, v3

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v8, p0, Ladx;->TS:I

    div-int/2addr v3, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 646
    const/4 v3, 0x0

    .line 647
    const/4 v2, 0x0

    .line 649
    :try_start_1
    invoke-direct {p0}, Ladx;->kp()Ljava/io/InputStream;

    move-result-object v2

    .line 650
    if-nez v2, :cond_8

    .line 651
    const-string v1, "Launcher3.CropActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "cannot get input stream for uri="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Ladx;->TN:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 653
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 661
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 655
    :cond_8
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v2, v1}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v3

    .line 656
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 660
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 664
    :goto_2
    const/4 v1, 0x0

    .line 665
    if-eqz v3, :cond_a

    .line 667
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 668
    const/4 v2, 0x1

    if-le v8, v2, :cond_9

    .line 669
    iput v8, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 671
    :cond_9
    invoke-virtual {v3, v5, v1}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 672
    invoke-virtual {v3}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 675
    :cond_a
    if-nez v1, :cond_11

    .line 677
    invoke-direct {p0}, Ladx;->kp()Ljava/io/InputStream;

    move-result-object v3

    .line 678
    const/4 v2, 0x0

    .line 679
    if-eqz v3, :cond_c

    .line 680
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 681
    const/4 v9, 0x1

    if-le v8, v9, :cond_b

    .line 682
    iput v8, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 684
    :cond_b
    const/4 v8, 0x0

    invoke-static {v3, v8, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 685
    invoke-static {v3}, Lqm;->a(Ljava/io/Closeable;)V

    .line 687
    :cond_c
    if-eqz v2, :cond_11

    .line 689
    iget v1, v7, Landroid/graphics/Point;->x:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/2addr v1, v3

    .line 690
    iget-object v3, p0, Ladx;->TQ:Landroid/graphics/RectF;

    iget v7, v3, Landroid/graphics/RectF;->left:F

    int-to-float v8, v1

    div-float/2addr v7, v8

    iput v7, v3, Landroid/graphics/RectF;->left:F

    .line 691
    iget-object v3, p0, Ladx;->TQ:Landroid/graphics/RectF;

    iget v7, v3, Landroid/graphics/RectF;->top:F

    int-to-float v8, v1

    div-float/2addr v7, v8

    iput v7, v3, Landroid/graphics/RectF;->top:F

    .line 692
    iget-object v3, p0, Ladx;->TQ:Landroid/graphics/RectF;

    iget v7, v3, Landroid/graphics/RectF;->bottom:F

    int-to-float v8, v1

    div-float/2addr v7, v8

    iput v7, v3, Landroid/graphics/RectF;->bottom:F

    .line 693
    iget-object v3, p0, Ladx;->TQ:Landroid/graphics/RectF;

    iget v7, v3, Landroid/graphics/RectF;->right:F

    int-to-float v1, v1

    div-float v1, v7, v1

    iput v1, v3, Landroid/graphics/RectF;->right:F

    .line 694
    iget-object v1, p0, Ladx;->TQ:Landroid/graphics/RectF;

    invoke-virtual {v1, v5}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 697
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-le v1, v3, :cond_d

    .line 699
    iget v1, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v5, Landroid/graphics/Rect;->right:I

    .line 701
    :cond_d
    iget v1, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-le v1, v3, :cond_e

    .line 703
    iget v1, v5, Landroid/graphics/Rect;->left:I

    const/4 v3, 0x0

    iget v7, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v1, v3

    .line 705
    iget v3, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v1

    iput v3, v5, Landroid/graphics/Rect;->left:I

    .line 706
    iget v3, v5, Landroid/graphics/Rect;->right:I

    sub-int v1, v3, v1

    iput v1, v5, Landroid/graphics/Rect;->right:I

    .line 708
    :cond_e
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v1, v3, :cond_f

    .line 710
    iget v1, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, v5, Landroid/graphics/Rect;->bottom:I

    .line 712
    :cond_f
    iget v1, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v1, v3, :cond_10

    .line 714
    iget v1, v5, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x0

    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v1, v3

    .line 716
    iget v3, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v1

    iput v3, v5, Landroid/graphics/Rect;->top:I

    .line 717
    iget v3, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v3, v1

    iput v1, v5, Landroid/graphics/Rect;->bottom:I

    .line 720
    :cond_10
    iget v1, v5, Landroid/graphics/Rect;->left:I

    iget v3, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-static {v2, v1, v3, v7, v5}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 726
    :cond_11
    if-nez v1, :cond_12

    .line 727
    const-string v0, "Launcher3.CropActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot decode file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ladx;->TN:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 657
    :catch_1
    move-exception v1

    .line 658
    :try_start_3
    const-string v9, "Launcher3.CropActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "cannot open region decoder for file: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Ladx;->TN:Landroid/net/Uri;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 660
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    .line 661
    throw v0

    .line 731
    :cond_12
    iget v2, p0, Ladx;->TR:I

    if-lez v2, :cond_13

    iget v2, p0, Ladx;->TS:I

    if-gtz v2, :cond_14

    :cond_13
    iget v2, p0, Ladx;->TT:I

    if-lez v2, :cond_17

    .line 732
    :cond_14
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    aput v5, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    aput v5, v2, v3

    .line 733
    invoke-virtual {v6, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 734
    const/4 v3, 0x0

    const/4 v5, 0x0

    aget v5, v2, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    aput v5, v2, v3

    .line 735
    const/4 v3, 0x1

    const/4 v5, 0x1

    aget v5, v2, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    aput v5, v2, v3

    .line 737
    iget v3, p0, Ladx;->TR:I

    if-lez v3, :cond_15

    iget v3, p0, Ladx;->TS:I

    if-gtz v3, :cond_16

    .line 738
    :cond_15
    const/4 v3, 0x0

    aget v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, p0, Ladx;->TR:I

    .line 739
    const/4 v3, 0x1

    aget v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iput v3, p0, Ladx;->TS:I

    .line 742
    :cond_16
    new-instance v3, Landroid/graphics/RectF;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget v7, v2, v7

    const/4 v8, 0x1

    aget v8, v2, v8

    invoke-direct {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 743
    new-instance v5, Landroid/graphics/RectF;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v8, p0, Ladx;->TR:I

    int-to-float v8, v8

    iget v9, p0, Ladx;->TS:I

    int-to-float v9, v9

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 745
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 746
    iget v7, p0, Ladx;->TT:I

    if-nez v7, :cond_1a

    .line 747
    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v6, v3, v5, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 765
    :goto_3
    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 767
    if-eqz v2, :cond_17

    .line 768
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 769
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 770
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 771
    invoke-virtual {v3, v1, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    move-object v1, v2

    .line 776
    :cond_17
    iget-boolean v2, p0, Ladx;->TW:Z

    if-eqz v2, :cond_18

    .line 777
    iput-object v1, p0, Ladx;->TX:Landroid/graphics/Bitmap;

    .line 781
    :cond_18
    iget-object v2, p0, Ladx;->TU:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/launcher3/WallpaperCropActivity;->y(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/launcher3/WallpaperCropActivity;->x(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v2

    .line 785
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x800

    invoke-direct {v3, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 786
    const/16 v5, 0x5a

    invoke-virtual {v1, v2, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 788
    iget-boolean v1, p0, Ladx;->TV:Z

    if-eqz v1, :cond_1d

    if-eqz v0, :cond_1d

    .line 790
    :try_start_4
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 791
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v2}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V

    .line 792
    iget-object v0, p0, Ladx;->TZ:Lady;

    if-eqz v0, :cond_19

    .line 793
    iget-object v0, p0, Ladx;->TZ:Lady;

    invoke-interface {v0, v1}, Lady;->b([B)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_19
    move v0, v4

    .line 805
    :goto_4
    if-nez v0, :cond_1c

    const/4 v0, 0x1

    goto/16 :goto_1

    .line 749
    :cond_1a
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 750
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    neg-int v8, v8

    int-to-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 751
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 752
    iget v9, p0, Ladx;->TT:I

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 753
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 754
    const/4 v10, 0x0

    aget v10, v2, v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    const/4 v11, 0x1

    aget v2, v2, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v2, v11

    invoke-virtual {v9, v10, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 755
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 756
    sget-object v10, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v3, v5, v10}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 758
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 759
    invoke-virtual {v3, v8, v7}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 760
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 761
    invoke-virtual {v7, v2, v9}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 762
    invoke-virtual {v6, v7, v3}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    goto/16 :goto_3

    .line 795
    :catch_2
    move-exception v0

    .line 796
    const-string v1, "Launcher3.CropActivity"

    const-string v2, "cannot write stream to wallpaper"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 797
    const/4 v0, 0x1

    .line 798
    goto :goto_4

    .line 801
    :cond_1b
    const-string v0, "Launcher3.CropActivity"

    const-string v1, "cannot compress bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    const/4 v0, 0x1

    goto :goto_4

    .line 805
    :cond_1c
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_1d
    move v0, v4

    goto :goto_4
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Ladx;->TY:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladx;->TY:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method
