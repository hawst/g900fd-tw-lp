.class public Lhhq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field final aqp:Ljava/lang/Object;

.field private bEi:Lhum;

.field public ble:Lhxr;

.field private cHz:Lhhc;

.field cKp:Lgho;

.field private dho:Lhvf;

.field dhp:Lghj;

.field private dhq:Lgfb;

.field public dhr:Lclk;

.field private dhs:Lgni;

.field private dht:Lhrk;

.field public dhu:Leum;

.field private dhv:Lhiz;

.field private dhw:Lcsd;

.field public final mAsyncServices:Lema;

.field private mAudioController:Lgem;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioRouter:Lhhu;

.field private mAudioStore:Lgfb;

.field private mBluetoothController:Lhjb;

.field public mContactLookup:Lghy;

.field public final mContext:Landroid/content/Context;

.field public final mCoreSearchServices:Lcfo;

.field private final mDeviceCapabilityManager:Lenm;

.field private mDeviceParams:Lgmz;

.field private mGreco3Container:Lgiw;

.field private mHotwordDataManager:Lcse;

.field private mHypothesisToSuggestionSpansConverter:Lgel;

.field private mLocalTtsManager:Lice;

.field private mOfflineActionsManager:Lgkg;

.field private final mPreferenceController:Lchr;

.field private mRecognizer:Lgdb;

.field public final mSettings:Lhym;

.field private mSoundManager:Lhik;

.field mSpeechContext:Lgij;

.field private mSpeechLevelSource:Lequ;

.field private mSpeechLibFactory:Lgdn;

.field private mSuggestionLogger:Lgma;

.field private mTtsAudioPlayer:Lhix;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lema;Lchr;Lcfo;Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 152
    new-instance v0, Lhym;

    invoke-virtual {p4}, Lcfo;->BK()Lcke;

    move-result-object v3

    invoke-virtual {p4}, Lcfo;->DD()Lcjs;

    move-result-object v4

    invoke-virtual {p4}, Lcfo;->BL()Lchk;

    move-result-object v5

    invoke-virtual {p4}, Lcfo;->DG()Ldkx;

    move-result-object v6

    invoke-virtual {p2}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    move-object v1, p1

    move-object v2, p3

    invoke-direct/range {v0 .. v7}, Lhym;-><init>(Landroid/content/Context;Lchr;Lcke;Lcjs;Lchk;Ldkx;Ljava/util/concurrent/ExecutorService;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, Lhhq;-><init>(Landroid/content/Context;Lema;Lchr;Lcfo;Ljava/lang/Object;Lhym;)V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lema;Lchr;Lcfo;Ljava/lang/Object;Lhym;)V
    .locals 2

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p1, p0, Lhhq;->mContext:Landroid/content/Context;

    .line 164
    iput-object p3, p0, Lhhq;->mPreferenceController:Lchr;

    .line 165
    iput-object p2, p0, Lhhq;->mAsyncServices:Lema;

    .line 166
    invoke-virtual {p4}, Lcfo;->DX()Lenm;

    move-result-object v0

    iput-object v0, p0, Lhhq;->mDeviceCapabilityManager:Lenm;

    .line 167
    iput-object p6, p0, Lhhq;->mSettings:Lhym;

    .line 168
    iget-object v0, p0, Lhhq;->mSettings:Lhym;

    new-instance v1, Lhhr;

    invoke-direct {v1, p0, p6}, Lhhr;-><init>(Lhhq;Lhym;)V

    invoke-virtual {v0, v1}, Lhym;->b(Lhyo;)V

    .line 183
    iput-object p4, p0, Lhhq;->mCoreSearchServices:Lcfo;

    .line 184
    iput-object p5, p0, Lhhq;->aqp:Ljava/lang/Object;

    .line 185
    iget-object v0, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {p0}, Lhhq;->aOW()Lcsd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lchk;->a(Lchm;)V

    .line 187
    return-void
.end method

.method private aPd()V
    .locals 17

    .prologue
    .line 503
    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mAsyncServices:Lema;

    const-string v2, "AudioRouter"

    invoke-virtual {v1, v2}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    .line 504
    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-interface {v1}, Leqo;->Du()Z

    .line 505
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "AudioRouter"

    aput-object v3, v1, v2

    new-instance v7, Lenw;

    invoke-direct {v7}, Lenw;-><init>()V

    .line 508
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "AudioRouter"

    aput-object v3, v1, v2

    new-instance v14, Lenw;

    invoke-direct {v14}, Lenw;-><init>()V

    .line 510
    invoke-virtual/range {p0 .. p0}, Lhhq;->aPa()Landroid/media/AudioManager;

    move-result-object v3

    .line 511
    new-instance v1, Lhjb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhhq;->mDeviceCapabilityManager:Lenm;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhhq;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    invoke-direct/range {v1 .. v7}, Lhjb;-><init>(Lenm;Landroid/media/AudioManager;Landroid/content/Context;Lchk;Ljava/util/concurrent/Executor;Lenw;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lhhq;->mBluetoothController:Lhjb;

    .line 517
    const/16 v16, 0x0

    .line 518
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 519
    new-instance v16, Lhiv;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lld;->d(Landroid/content/Context;)Lld;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Lhiv;-><init>(Lld;)V

    .line 521
    :cond_0
    new-instance v8, Lhhw;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lhhq;->mSettings:Lhym;

    move-object/from16 v0, p0

    iget-object v15, v0, Lhhq;->mBluetoothController:Lhjb;

    move-object v11, v3

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v16}, Lhhw;-><init>(Lemp;Lhym;Landroid/media/AudioManager;Ljava/util/concurrent/Executor;Lenw;Lenw;Lhjb;Lhiv;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lhhq;->mAudioRouter:Lhhu;

    .line 524
    move-object/from16 v0, p0

    iget-object v2, v0, Lhhq;->mBluetoothController:Lhjb;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mAudioRouter:Lhhu;

    check-cast v1, Lhhw;

    invoke-virtual {v2, v1, v6}, Lhjb;->a(Lhje;Ljava/util/concurrent/Executor;)V

    .line 525
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 705
    const-string v0, "VoiceSearchServices state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 707
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 708
    :try_start_0
    iget-object v0, p0, Lhhq;->dht:Lhrk;

    .line 709
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    if-eqz v0, :cond_0

    .line 711
    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 715
    :goto_0
    return-void

    .line 709
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 713
    :cond_0
    const-string v0, "LanguageUpdateController not initialized"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lchk;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0}, Lhhq;->isLowRamDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const/4 v0, 0x0

    .line 359
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p2}, Lchk;->gm(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method protected aAp()Lgnf;
    .locals 5

    .prologue
    .line 286
    new-instance v0, Lcdm;

    new-instance v1, Lhzq;

    iget-object v2, p0, Lhhq;->mSettings:Lhym;

    iget-object v3, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DD()Lcjs;

    move-result-object v3

    iget-object v4, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DI()Lcpn;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lhzq;-><init>(Lhym;Lcjs;Lcpn;)V

    iget-object v2, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->Ek()Lccz;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcdm;-><init>(Ligi;Lccz;)V

    .line 291
    new-instance v1, Lgnf;

    iget-object v2, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lgnf;-><init>(Lemp;Lcdr;)V

    return-object v1
.end method

.method public aAq()Lgno;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DY()Lgno;

    move-result-object v0

    return-object v0
.end method

.method public final aAs()Lequ;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lhhq;->mSpeechLevelSource:Lequ;

    if-nez v0, :cond_0

    .line 533
    new-instance v0, Lequ;

    invoke-direct {v0}, Lequ;-><init>()V

    iput-object v0, p0, Lhhq;->mSpeechLevelSource:Lequ;

    .line 535
    :cond_0
    iget-object v0, p0, Lhhq;->mSpeechLevelSource:Lequ;

    return-object v0
.end method

.method public final aOR()Lgdb;
    .locals 20

    .prologue
    .line 194
    invoke-static {}, Lenu;->auR()V

    .line 195
    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mRecognizer:Lgdb;

    if-nez v1, :cond_3

    .line 196
    const-string v1, "VS.Container"

    const-string v2, "create_speech_recognizer"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mAsyncServices:Lema;

    const-string v2, "GrecoExecutor"

    invoke-virtual {v1, v2}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lhhq;->aOT()Lgem;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mSpeechLibFactory:Lgdn;

    if-nez v1, :cond_2

    new-instance v12, Lhhn;

    invoke-virtual/range {p0 .. p0}, Lhhq;->aAq()Lgno;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->dhs:Lgni;

    if-nez v1, :cond_1

    new-instance v14, Lgni;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DY()Lgno;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->Ed()Lhzl;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->Ee()Lhzj;

    move-result-object v18

    new-instance v19, Lhzr;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DP()Lcob;

    move-result-object v1

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->El()Lfdr;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v1, v2}, Lhzr;-><init>(Lcob;Lfdr;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lhhq;->mSettings:Lhym;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mDeviceParams:Lgmz;

    if-nez v1, :cond_0

    new-instance v1, Lgna;

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJg()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lhhq;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DS()Lckg;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DD()Lcjs;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->BK()Lcke;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lgna;-><init>(Ljava/lang/String;Landroid/content/Context;Ligi;Lcjs;Lcke;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lhhq;->mDeviceParams:Lgmz;

    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lhhq;->mDeviceParams:Lgmz;

    invoke-virtual/range {p0 .. p0}, Lhhq;->aOS()Ligi;

    move-result-object v1

    invoke-interface {v1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lgij;

    move-object v1, v14

    move-object v2, v15

    move-object/from16 v3, v16

    move-object/from16 v4, v17

    move-object/from16 v5, v18

    move-object/from16 v6, v19

    invoke-direct/range {v1 .. v9}, Lgni;-><init>(Lgln;Lgno;Lhzs;Lhzs;Lglo;Lgdo;Lgmz;Lgij;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lhhq;->dhs:Lgni;

    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lhhq;->dhs:Lgni;

    new-instance v15, Lgnb;

    new-instance v1, Lgnc;

    new-instance v2, Lglq;

    invoke-direct {v2}, Lglq;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lhhq;->aOV()Lgiw;

    move-result-object v3

    invoke-virtual {v3}, Lgiw;->aGg()Lgjh;

    move-result-object v3

    new-instance v4, Lglr;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhhq;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v5}, Lenm;->auH()Z

    move-result v5

    invoke-direct {v4, v5}, Lglr;-><init>(Z)V

    invoke-virtual/range {p0 .. p0}, Lhhq;->aAs()Lequ;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lhhq;->mSettings:Lhym;

    const/4 v7, 0x2

    const/16 v8, 0x1f40

    invoke-direct/range {v1 .. v8}, Lgnc;-><init>(Lgiv;Lgjh;Lgjp;Lequ;Lgdo;II)V

    invoke-virtual/range {p0 .. p0}, Lhhq;->aAp()Lgnf;

    move-result-object v2

    new-instance v3, Lgne;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhhq;->mSettings:Lhym;

    invoke-direct {v3, v4}, Lgne;-><init>(Lgdo;)V

    new-instance v4, Lgnd;

    invoke-virtual/range {p0 .. p0}, Lhhq;->ue()Lcse;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DL()Lcrh;

    move-result-object v6

    invoke-virtual {v6}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lgnd;-><init>(Lcrz;Ljava/lang/String;)V

    invoke-direct {v15, v1, v2, v3, v4}, Lgnb;-><init>(Lgnc;Lgnf;Lgne;Lgnd;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lhhq;->mSettings:Lhym;

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lhhq;->mAsyncServices:Lema;

    move-object v1, v12

    move-object v2, v13

    move-object v3, v14

    move-object v4, v15

    invoke-direct/range {v1 .. v9}, Lhhn;-><init>(Lgno;Lgni;Lgnb;Lgdo;Lchk;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lema;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lhhq;->mSpeechLibFactory:Lgdn;

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mSpeechLibFactory:Lgdn;

    invoke-static {v10, v11, v1}, Lgdc;->a(Ljava/util/concurrent/ExecutorService;Lgem;Lgdn;)Lgdb;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lhhq;->mRecognizer:Lgdb;

    .line 199
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lhhq;->mRecognizer:Lgdb;

    return-object v1
.end method

.method public final aOS()Ligi;
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lhhs;

    invoke-direct {v0, p0}, Lhhs;-><init>(Lhhq;)V

    return-object v0
.end method

.method public final aOT()Lgem;
    .locals 8

    .prologue
    .line 245
    invoke-static {}, Lenu;->auR()V

    .line 246
    iget-object v0, p0, Lhhq;->mAudioController:Lgem;

    if-nez v0, :cond_1

    .line 247
    new-instance v0, Lgem;

    iget-object v1, p0, Lhhq;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lhhq;->mSettings:Lhym;

    invoke-virtual {p0}, Lhhq;->aAs()Lequ;

    move-result-object v3

    invoke-virtual {p0}, Lhhq;->aOZ()Lhik;

    move-result-object v4

    invoke-virtual {p0}, Lhhq;->aPb()Lhhu;

    move-result-object v5

    iget-object v6, p0, Lhhq;->cHz:Lhhc;

    if-nez v6, :cond_0

    new-instance v6, Lhhc;

    invoke-virtual {p0}, Lhhq;->aAq()Lgno;

    move-result-object v7

    invoke-direct {v6, v7}, Lhhc;-><init>(Lgno;)V

    iput-object v6, p0, Lhhq;->cHz:Lhhc;

    :cond_0
    iget-object v6, p0, Lhhq;->cHz:Lhhc;

    invoke-direct/range {v0 .. v6}, Lgem;-><init>(Landroid/content/Context;Lgdo;Lequ;Lgfr;Lhhu;Lhhc;)V

    iput-object v0, p0, Lhhq;->mAudioController:Lgem;

    .line 251
    :cond_1
    iget-object v0, p0, Lhhq;->mAudioController:Lgem;

    return-object v0
.end method

.method public final aOU()Lclq;
    .locals 2

    .prologue
    .line 393
    new-instance v0, Lclq;

    iget-object v1, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lclq;-><init>(Lhhq;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public final aOV()Lgiw;
    .locals 5

    .prologue
    .line 397
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 398
    :try_start_0
    iget-object v0, p0, Lhhq;->mGreco3Container:Lgiw;

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lhhq;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lhhq;->mPreferenceController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    iget-object v4, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->aus()Leqo;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lgiw;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)Lgiw;

    move-result-object v0

    iput-object v0, p0, Lhhq;->mGreco3Container:Lgiw;

    .line 404
    :cond_0
    iget-object v0, p0, Lhhq;->mGreco3Container:Lgiw;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aOW()Lcsd;
    .locals 5

    .prologue
    .line 421
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 422
    :try_start_0
    iget-object v0, p0, Lhhq;->dhw:Lcsd;

    if-nez v0, :cond_0

    .line 423
    new-instance v0, Lcsd;

    iget-object v2, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mSettings:Lhym;

    iget-object v4, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3, v4}, Lcsd;-><init>(Lcke;Lhym;Landroid/content/Context;)V

    iput-object v0, p0, Lhhq;->dhw:Lcsd;

    .line 426
    :cond_0
    iget-object v0, p0, Lhhq;->dhw:Lcsd;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aOX()Lhrk;
    .locals 7

    .prologue
    .line 431
    iget-object v6, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v6

    .line 432
    :try_start_0
    iget-object v0, p0, Lhhq;->dht:Lhrk;

    if-nez v0, :cond_0

    .line 433
    invoke-virtual {p0}, Lhhq;->aOV()Lgiw;

    move-result-object v0

    iget-object v1, p0, Lhhq;->mSettings:Lhym;

    iget-object v2, p0, Lhhq;->mContext:Landroid/content/Context;

    const-string v3, "download"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/DownloadManager;

    iget-object v3, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lhhq;->aOV()Lgiw;

    move-result-object v4

    invoke-virtual {v4}, Lgiw;->aGi()Lgjq;

    move-result-object v4

    iget-object v5, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lhrk;->a(Lgiw;Lhym;Landroid/app/DownloadManager;Landroid/content/Context;Lgjq;Ljava/util/concurrent/Executor;)Lhrk;

    move-result-object v0

    iput-object v0, p0, Lhhq;->dht:Lhrk;

    .line 439
    :cond_0
    iget-object v0, p0, Lhhq;->dht:Lhrk;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final aOY()Lhiz;
    .locals 3

    .prologue
    .line 447
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 448
    :try_start_0
    iget-object v0, p0, Lhhq;->dhv:Lhiz;

    if-nez v0, :cond_0

    .line 449
    new-instance v0, Lhiz;

    iget-object v2, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, Lhiz;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lhhq;->dhv:Lhiz;

    .line 451
    :cond_0
    iget-object v0, p0, Lhhq;->dhv:Lhiz;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 452
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aOZ()Lhik;
    .locals 4

    .prologue
    .line 469
    invoke-static {}, Lenu;->auR()V

    .line 470
    iget-object v0, p0, Lhhq;->mSoundManager:Lhik;

    if-nez v0, :cond_0

    .line 471
    new-instance v0, Lhik;

    iget-object v1, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lhhq;->aPb()Lhhu;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhik;-><init>(Landroid/content/Context;Lhhu;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lhhq;->mSoundManager:Lhik;

    .line 474
    :cond_0
    iget-object v0, p0, Lhhq;->mSoundManager:Lhik;

    return-object v0
.end method

.method public final aPa()Landroid/media/AudioManager;
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lhhq;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 479
    iget-object v0, p0, Lhhq;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lhhq;->mAudioManager:Landroid/media/AudioManager;

    .line 481
    :cond_0
    iget-object v0, p0, Lhhq;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method public final aPb()Lhhu;
    .locals 2

    .prologue
    .line 485
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 486
    :try_start_0
    iget-object v0, p0, Lhhq;->mAudioRouter:Lhhu;

    if-nez v0, :cond_0

    .line 487
    invoke-direct {p0}, Lhhq;->aPd()V

    .line 489
    :cond_0
    iget-object v0, p0, Lhhq;->mAudioRouter:Lhhu;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 490
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPc()Lhjb;
    .locals 2

    .prologue
    .line 494
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 495
    :try_start_0
    iget-object v0, p0, Lhhq;->mBluetoothController:Lhjb;

    if-nez v0, :cond_0

    .line 496
    invoke-direct {p0}, Lhhq;->aPd()V

    .line 498
    :cond_0
    iget-object v0, p0, Lhhq;->mBluetoothController:Lhjb;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPe()Lgma;
    .locals 1

    .prologue
    .line 539
    invoke-static {}, Lenu;->auR()V

    .line 540
    iget-object v0, p0, Lhhq;->mSuggestionLogger:Lgma;

    if-nez v0, :cond_0

    .line 541
    new-instance v0, Lgma;

    invoke-direct {v0}, Lgma;-><init>()V

    iput-object v0, p0, Lhhq;->mSuggestionLogger:Lgma;

    .line 543
    :cond_0
    iget-object v0, p0, Lhhq;->mSuggestionLogger:Lgma;

    return-object v0
.end method

.method public final aPf()Lgel;
    .locals 3

    .prologue
    .line 547
    iget-object v0, p0, Lhhq;->mHypothesisToSuggestionSpansConverter:Lgel;

    if-nez v0, :cond_0

    .line 548
    new-instance v0, Lgel;

    iget-object v1, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lhhq;->aPe()Lgma;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lgel;-><init>(Landroid/content/Context;Lgma;)V

    iput-object v0, p0, Lhhq;->mHypothesisToSuggestionSpansConverter:Lgel;

    .line 551
    :cond_0
    iget-object v0, p0, Lhhq;->mHypothesisToSuggestionSpansConverter:Lgel;

    return-object v0
.end method

.method public final aPg()Lgfb;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lhhq;->mAudioStore:Lgfb;

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Lgfq;

    invoke-direct {v0}, Lgfq;-><init>()V

    iput-object v0, p0, Lhhq;->mAudioStore:Lgfb;

    .line 558
    :cond_0
    iget-object v0, p0, Lhhq;->mAudioStore:Lgfb;

    return-object v0
.end method

.method public final aPh()Lgfb;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lhhq;->dhq:Lgfb;

    if-nez v0, :cond_0

    .line 567
    new-instance v0, Lgfq;

    invoke-direct {v0}, Lgfq;-><init>()V

    iput-object v0, p0, Lhhq;->dhq:Lgfb;

    .line 569
    :cond_0
    iget-object v0, p0, Lhhq;->dhq:Lgfb;

    return-object v0
.end method

.method public final aPi()Lhum;
    .locals 3

    .prologue
    .line 573
    invoke-static {}, Lenu;->auR()V

    .line 574
    iget-object v0, p0, Lhhq;->bEi:Lhum;

    if-nez v0, :cond_0

    .line 575
    new-instance v0, Lhum;

    iget-object v1, p0, Lhhq;->mSettings:Lhym;

    iget-object v2, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lhum;-><init>(Lhhq;Lhym;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lhhq;->bEi:Lhum;

    .line 578
    :cond_0
    iget-object v0, p0, Lhhq;->bEi:Lhum;

    return-object v0
.end method

.method public final aPj()Lice;
    .locals 8

    .prologue
    .line 582
    invoke-static {}, Lenu;->auR()V

    .line 583
    iget-object v0, p0, Lhhq;->mLocalTtsManager:Lice;

    if-nez v0, :cond_0

    .line 584
    new-instance v0, Lice;

    iget-object v1, p0, Lhhq;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-virtual {p0}, Lhhq;->aPb()Lhhu;

    move-result-object v4

    iget-object v5, p0, Lhhq;->mSettings:Lhym;

    new-instance v6, Lidc;

    iget-object v7, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lidc;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v6}, Lice;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lhhu;Lhym;Lidc;)V

    iput-object v0, p0, Lhhq;->mLocalTtsManager:Lice;

    .line 588
    :cond_0
    iget-object v0, p0, Lhhq;->mLocalTtsManager:Lice;

    return-object v0
.end method

.method public final aPk()Lgkg;
    .locals 5

    .prologue
    .line 596
    invoke-static {}, Lenu;->auR()V

    .line 597
    iget-object v0, p0, Lhhq;->mOfflineActionsManager:Lgkg;

    if-nez v0, :cond_0

    .line 598
    new-instance v0, Lgkg;

    iget-object v1, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lhhq;->aOV()Lgiw;

    move-result-object v2

    invoke-virtual {v2}, Lgiw;->aGh()Lgix;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mSettings:Lhym;

    iget-object v4, p0, Lhhq;->mAsyncServices:Lema;

    invoke-direct {v0, v1, v2, v3, v4}, Lgkg;-><init>(Landroid/content/Context;Lgix;Lgdo;Lema;)V

    iput-object v0, p0, Lhhq;->mOfflineActionsManager:Lgkg;

    .line 601
    :cond_0
    iget-object v0, p0, Lhhq;->mOfflineActionsManager:Lgkg;

    return-object v0
.end method

.method public final aPl()Lhvf;
    .locals 6

    .prologue
    .line 606
    iget-object v1, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v1

    .line 607
    :try_start_0
    iget-object v0, p0, Lhhq;->dho:Lhvf;

    if-nez v0, :cond_0

    .line 608
    new-instance v0, Lhvf;

    iget-object v2, p0, Lhhq;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    iget-object v4, p0, Lhhq;->mSettings:Lhym;

    iget-object v5, p0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v5}, Lhym;->getHotwordConfigMap()Ljava/util/Map;

    iget-object v5, p0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v5}, Lhym;->JV()I

    move-result v5

    invoke-direct {v0, v2, v3, v4, v5}, Lhvf;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lhym;I)V

    iput-object v0, p0, Lhhq;->dho:Lhvf;

    .line 611
    iget-object v0, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iget-object v2, p0, Lhhq;->dho:Lhvf;

    invoke-virtual {v0, v2}, Lchk;->a(Lchm;)V

    .line 615
    :cond_0
    iget-object v0, p0, Lhhq;->dho:Lhvf;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aPm()Lhix;
    .locals 4

    .prologue
    .line 620
    invoke-static {}, Lenu;->auR()V

    .line 621
    iget-object v0, p0, Lhhq;->mTtsAudioPlayer:Lhix;

    if-nez v0, :cond_0

    .line 622
    new-instance v0, Lhix;

    invoke-virtual {p0}, Lhhq;->aPb()Lhhu;

    move-result-object v1

    iget-object v2, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhix;-><init>(Lhhu;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lhhq;->mTtsAudioPlayer:Lhix;

    .line 625
    :cond_0
    iget-object v0, p0, Lhhq;->mTtsAudioPlayer:Lhix;

    return-object v0
.end method

.method public isLowRamDevice()Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lesp;->ax(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final nX(Ljava/lang/String;)Lgkp;
    .locals 10

    .prologue
    .line 329
    iget-object v0, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iget-object v1, p0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lhhq;->a(Lchk;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 332
    iget-object v0, p0, Lhhq;->mAsyncServices:Lema;

    const-string v1, "PumpkinTagger"

    invoke-virtual {v0, v1}, Lema;->gd(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    .line 333
    iget-object v0, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v3

    .line 334
    iget-object v0, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iget-object v1, p0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lchk;->gn(Ljava/lang/String;)Z

    move-result v2

    .line 337
    new-instance v0, Lgkp;

    iget-object v1, p0, Lhhq;->mCoreSearchServices:Lcfo;

    new-instance v5, Lgio;

    iget-object v6, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6, p1}, Lgio;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v6, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v6

    iget-object v7, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->Es()Libo;

    move-result-object v7

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v8

    invoke-virtual {v8}, Lgql;->SC()Lcfo;

    move-result-object v8

    invoke-virtual {v8}, Lcfo;->BL()Lchk;

    move-result-object v8

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v9

    invoke-virtual {v9}, Lgql;->aJW()Leai;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lgkp;-><init>(Lcfo;ZLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgio;Lghy;Libo;Lchk;Leai;)V

    return-object v0
.end method

.method public final ue()Lcse;
    .locals 9

    .prologue
    .line 409
    iget-object v8, p0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v8

    .line 410
    :try_start_0
    iget-object v0, p0, Lhhq;->mHotwordDataManager:Lcse;

    if-nez v0, :cond_0

    .line 411
    new-instance v0, Lcse;

    iget-object v1, p0, Lhhq;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    iget-object v3, p0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    iget-object v4, p0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v5, p0, Lhhq;->mSettings:Lhym;

    iget-object v6, p0, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcgg;->aVr:Lcgg;

    invoke-virtual {v7}, Lcgg;->isEnabled()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcse;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgdm;Lgdo;Ljava/lang/String;Z)V

    iput-object v0, p0, Lhhq;->mHotwordDataManager:Lcse;

    .line 416
    :cond_0
    iget-object v0, p0, Lhhq;->mHotwordDataManager:Lcse;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method
