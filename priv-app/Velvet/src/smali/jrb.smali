.class public final Ljrb;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezS:Ljsm;


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field public ezT:Ljpe;

.field public ezU:Ljpe;

.field public ezV:Ljpx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3071
    const/16 v0, 0xb

    const-class v1, Ljrb;

    const v2, 0x1ce33f6a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljrb;->ezS:Ljsm;

    .line 3077
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3115
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3116
    const/4 v0, 0x0

    iput v0, p0, Ljrb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljrb;->ajB:Ljava/lang/String;

    iput-object v1, p0, Ljrb;->ezT:Ljpe;

    iput-object v1, p0, Ljrb;->ezU:Ljpe;

    iput-object v1, p0, Ljrb;->ezV:Ljpx;

    iput-object v1, p0, Ljrb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrb;->eCz:I

    .line 3117
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3064
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrb;->ajB:Ljava/lang/String;

    iget v0, p0, Ljrb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrb;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljrb;->ezT:Ljpe;

    if-nez v0, :cond_1

    new-instance v0, Ljpe;

    invoke-direct {v0}, Ljpe;-><init>()V

    iput-object v0, p0, Ljrb;->ezT:Ljpe;

    :cond_1
    iget-object v0, p0, Ljrb;->ezT:Ljpe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljrb;->ezV:Ljpx;

    if-nez v0, :cond_2

    new-instance v0, Ljpx;

    invoke-direct {v0}, Ljpx;-><init>()V

    iput-object v0, p0, Ljrb;->ezV:Ljpx;

    :cond_2
    iget-object v0, p0, Ljrb;->ezV:Ljpx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljrb;->ezU:Ljpe;

    if-nez v0, :cond_3

    new-instance v0, Ljpe;

    invoke-direct {v0}, Ljpe;-><init>()V

    iput-object v0, p0, Ljrb;->ezU:Ljpe;

    :cond_3
    iget-object v0, p0, Ljrb;->ezU:Ljpe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3133
    iget v0, p0, Ljrb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3134
    const/4 v0, 0x1

    iget-object v1, p0, Ljrb;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3136
    :cond_0
    iget-object v0, p0, Ljrb;->ezT:Ljpe;

    if-eqz v0, :cond_1

    .line 3137
    const/4 v0, 0x2

    iget-object v1, p0, Ljrb;->ezT:Ljpe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3139
    :cond_1
    iget-object v0, p0, Ljrb;->ezV:Ljpx;

    if-eqz v0, :cond_2

    .line 3140
    const/4 v0, 0x3

    iget-object v1, p0, Ljrb;->ezV:Ljpx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3142
    :cond_2
    iget-object v0, p0, Ljrb;->ezU:Ljpe;

    if-eqz v0, :cond_3

    .line 3143
    const/4 v0, 0x4

    iget-object v1, p0, Ljrb;->ezU:Ljpe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3145
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3146
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3150
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3151
    iget v1, p0, Ljrb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3152
    const/4 v1, 0x1

    iget-object v2, p0, Ljrb;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3155
    :cond_0
    iget-object v1, p0, Ljrb;->ezT:Ljpe;

    if-eqz v1, :cond_1

    .line 3156
    const/4 v1, 0x2

    iget-object v2, p0, Ljrb;->ezT:Ljpe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3159
    :cond_1
    iget-object v1, p0, Ljrb;->ezV:Ljpx;

    if-eqz v1, :cond_2

    .line 3160
    const/4 v1, 0x3

    iget-object v2, p0, Ljrb;->ezV:Ljpx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3163
    :cond_2
    iget-object v1, p0, Ljrb;->ezU:Ljpe;

    if-eqz v1, :cond_3

    .line 3164
    const/4 v1, 0x4

    iget-object v2, p0, Ljrb;->ezU:Ljpe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3167
    :cond_3
    return v0
.end method
