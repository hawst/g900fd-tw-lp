.class public final Laph;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile alh:[Laph;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ahq:I

.field private ali:I

.field private alj:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9504
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 9505
    iput v1, p0, Laph;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laph;->agq:Ljava/lang/String;

    iput v1, p0, Laph;->ali:I

    iput v1, p0, Laph;->ahq:I

    iput v1, p0, Laph;->alj:I

    const/4 v0, 0x0

    iput-object v0, p0, Laph;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laph;->eCz:I

    .line 9506
    return-void
.end method

.method public static sL()[Laph;
    .locals 2

    .prologue
    .line 9412
    sget-object v0, Laph;->alh:[Laph;

    if-nez v0, :cond_1

    .line 9413
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 9415
    :try_start_0
    sget-object v0, Laph;->alh:[Laph;

    if-nez v0, :cond_0

    .line 9416
    const/4 v0, 0x0

    new-array v0, v0, [Laph;

    sput-object v0, Laph;->alh:[Laph;

    .line 9418
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9420
    :cond_1
    sget-object v0, Laph;->alh:[Laph;

    return-object v0

    .line 9418
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laph;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laph;->agq:Ljava/lang/String;

    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laph;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Laph;->ali:I

    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laph;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Laph;->ahq:I

    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laph;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Laph;->alj:I

    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laph;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 9522
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9523
    const/4 v0, 0x1

    iget-object v1, p0, Laph;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9525
    :cond_0
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9526
    const/4 v0, 0x2

    iget v1, p0, Laph;->ali:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 9528
    :cond_1
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 9529
    const/4 v0, 0x3

    iget v1, p0, Laph;->ahq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 9531
    :cond_2
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 9532
    const/4 v0, 0x4

    iget v1, p0, Laph;->alj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 9534
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9535
    return-void
.end method

.method public final cS(I)Laph;
    .locals 1

    .prologue
    .line 9453
    iput p1, p0, Laph;->ali:I

    .line 9454
    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laph;->aez:I

    .line 9455
    return-object p0
.end method

.method public final cT(I)Laph;
    .locals 1

    .prologue
    .line 9472
    iput p1, p0, Laph;->ahq:I

    .line 9473
    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laph;->aez:I

    .line 9474
    return-object p0
.end method

.method public final cU(I)Laph;
    .locals 1

    .prologue
    .line 9491
    iput p1, p0, Laph;->alj:I

    .line 9492
    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laph;->aez:I

    .line 9493
    return-object p0
.end method

.method public final df(Ljava/lang/String;)Laph;
    .locals 1

    .prologue
    .line 9431
    if-nez p1, :cond_0

    .line 9432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9434
    :cond_0
    iput-object p1, p0, Laph;->agq:Ljava/lang/String;

    .line 9435
    iget v0, p0, Laph;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laph;->aez:I

    .line 9436
    return-object p0
.end method

.method public final getBackgroundColor()I
    .locals 1

    .prologue
    .line 9469
    iget v0, p0, Laph;->ahq:I

    return v0
.end method

.method public final getForegroundColor()I
    .locals 1

    .prologue
    .line 9450
    iget v0, p0, Laph;->ali:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9428
    iget-object v0, p0, Laph;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final hasBackgroundColor()Z
    .locals 1

    .prologue
    .line 9477
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasForegroundColor()Z
    .locals 1

    .prologue
    .line 9458
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 9539
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9540
    iget v1, p0, Laph;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9541
    const/4 v1, 0x1

    iget-object v2, p0, Laph;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9544
    :cond_0
    iget v1, p0, Laph;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9545
    const/4 v1, 0x2

    iget v2, p0, Laph;->ali:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 9548
    :cond_1
    iget v1, p0, Laph;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9549
    const/4 v1, 0x3

    iget v2, p0, Laph;->ahq:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 9552
    :cond_2
    iget v1, p0, Laph;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 9553
    const/4 v1, 0x4

    iget v2, p0, Laph;->alj:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 9556
    :cond_3
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 9439
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final sM()Laph;
    .locals 1

    .prologue
    .line 9442
    const-string v0, ""

    iput-object v0, p0, Laph;->agq:Ljava/lang/String;

    .line 9443
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Laph;->aez:I

    .line 9444
    return-object p0
.end method

.method public final sN()I
    .locals 1

    .prologue
    .line 9488
    iget v0, p0, Laph;->alj:I

    return v0
.end method

.method public final sO()Z
    .locals 1

    .prologue
    .line 9496
    iget v0, p0, Laph;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
