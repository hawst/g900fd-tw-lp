.class public final Lbmo;
.super Lbir;


# instance fields
.field private final aFE:Lbml;

.field private final aFF:Ljava/lang/String;

.field private final aFz:Lbmv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbgp;Lbgq;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lbir;-><init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V

    new-instance v0, Lbmr;

    invoke-direct {v0, p0, v1}, Lbmr;-><init>(Lbmo;B)V

    iput-object v0, p0, Lbmo;->aFz:Lbmv;

    new-instance v0, Lbml;

    iget-object v1, p0, Lbmo;->aFz:Lbmv;

    invoke-direct {v0, p1, v1}, Lbml;-><init>(Landroid/content/Context;Lbmv;)V

    iput-object v0, p0, Lbmo;->aFE:Lbml;

    iput-object p4, p0, Lbmo;->aFF:Ljava/lang/String;

    new-instance v0, Lbmz;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbmo;->aFz:Lbmv;

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;Lbmv;Ljava/lang/String;)V

    iget-object v0, p0, Lbmo;->aFz:Lbmv;

    new-instance v1, Lbmd;

    invoke-direct {v1, p1, v3, v3, v0}, Lbmd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lbmv;)V

    iget-object v0, p0, Lbmo;->aFz:Lbmv;

    new-instance v1, Lbld;

    invoke-direct {v1, p1, v0}, Lbld;-><init>(Landroid/content/Context;Lbmv;)V

    return-void
.end method

.method static synthetic a(Lbmo;)V
    .locals 0

    invoke-virtual {p0}, Lbmo;->yS()V

    return-void
.end method


# virtual methods
.method public final a(JLandroid/app/PendingIntent;)V
    .locals 5

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbmo;->yS()V

    invoke-static {p3}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "detectionIntervalMillis must be >= 0"

    invoke-static {v0, v2}, Lbjr;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lbmo;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    invoke-interface {v0, p1, p2, v1, p3}, Lbmi;->a(JZLandroid/app/PendingIntent;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;Lbrl;)V
    .locals 3

    invoke-virtual {p0}, Lbmo;->yS()V

    const-string v0, "PendingIntent must be specified."

    invoke-static {p1, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "OnRemoveGeofencesResultListener not provided."

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    invoke-virtual {p0}, Lbmo;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    iget-object v2, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lbmi;->a(Landroid/app/PendingIntent;Lbmf;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Lbmq;

    invoke-direct {v0, p2, p0}, Lbmq;-><init>(Lbrl;Lbmo;)V

    move-object v1, v0

    goto :goto_0
.end method

.method protected final a(Lbjm;Lbiv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    iget-object v2, p0, Lbmo;->aFF:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x5d3f18

    iget-object v2, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjm;->e(Lbjj;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/internal/nj;Lbrm;Landroid/os/Looper;)V
    .locals 3

    iget-object v1, p0, Lbmo;->aFE:Lbml;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbmo;->aFE:Lbml;

    iget-object v2, v0, Lbml;->aFz:Lbmv;

    invoke-interface {v2}, Lbmv;->yS()V

    invoke-virtual {v0, p2, p3}, Lbml;->a(Lbrm;Landroid/os/Looper;)Lbmn;

    move-result-object v2

    iget-object v0, v0, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    invoke-interface {v0, p1, v2}, Lbmi;->a(Lcom/google/android/gms/internal/nj;Lbro;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/util/List;Landroid/app/PendingIntent;Lbrk;)V
    .locals 3

    invoke-virtual {p0}, Lbmo;->yS()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "At least one geofence must be specified."

    invoke-static {v0, v1}, Lbjr;->b(ZLjava/lang/Object;)V

    const-string v0, "PendingIntent must be specified."

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "OnAddGeofencesResultListener not provided."

    invoke-static {p3, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    :goto_1
    invoke-virtual {p0}, Lbmo;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    iget-object v2, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lbmi;->a(Ljava/util/List;Landroid/app/PendingIntent;Lbmf;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lbmq;

    invoke-direct {v0, p3, p0}, Lbmq;-><init>(Lbrk;Lbmo;)V

    move-object v1, v0

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Lbrl;)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbmo;->yS()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "geofenceRequestIds can\'t be null nor empty."

    invoke-static {v0, v2}, Lbjr;->b(ZLjava/lang/Object;)V

    const-string v0, "OnRemoveGeofencesResultListener not provided."

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v0, v1, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-nez p2, :cond_1

    const/4 v1, 0x0

    move-object v2, v1

    :goto_1
    invoke-virtual {p0}, Lbmo;->yT()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lbmi;

    iget-object v3, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lbmi;->a([Ljava/lang/String;Lbmf;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v1, Lbmq;

    invoke-direct {v1, p2, p0}, Lbmq;-><init>(Lbrl;Lbmo;)V

    move-object v2, v1

    goto :goto_1
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .locals 1

    invoke-virtual {p0}, Lbmo;->yS()V

    invoke-static {p1}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lbmo;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    invoke-interface {v0, p1}, Lbmi;->b(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/internal/nj;Landroid/app/PendingIntent;)V
    .locals 2

    iget-object v0, p0, Lbmo;->aFE:Lbml;

    iget-object v1, v0, Lbml;->aFz:Lbmv;

    invoke-interface {v1}, Lbmv;->yS()V

    iget-object v0, v0, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    invoke-interface {v0, p1, p2}, Lbmi;->a(Lcom/google/android/gms/internal/nj;Landroid/app/PendingIntent;)V

    return-void
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lbmj;->m(Landroid/os/IBinder;)Lbmi;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/app/PendingIntent;)V
    .locals 2

    iget-object v0, p0, Lbmo;->aFE:Lbml;

    iget-object v1, v0, Lbml;->aFz:Lbmv;

    invoke-interface {v1}, Lbmv;->yS()V

    iget-object v0, v0, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    invoke-interface {v0, p1}, Lbmi;->c(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public final disconnect()V
    .locals 6

    iget-object v2, p0, Lbmo;->aFE:Lbml;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lbmo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lbmo;->aFE:Lbml;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v4, v3, Lbml;->aFB:Ljava/util/HashMap;

    monitor-enter v4
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, v3, Lbml;->aFB:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    if-eqz v0, :cond_0

    iget-object v1, v3, Lbml;->aFz:Lbmv;

    invoke-interface {v1}, Lbmv;->yT()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lbmi;

    invoke-interface {v1, v0}, Lbmi;->a(Lbro;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4

    throw v0
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_5
    iget-object v0, v3, Lbml;->aFB:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v1, p0, Lbmo;->aFE:Lbml;

    iget-boolean v0, v1, Lbml;->aFA:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v0, :cond_2

    :try_start_7
    iget-object v0, v1, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yS()V

    iget-object v0, v1, Lbml;->aFz:Lbmv;

    invoke-interface {v0}, Lbmv;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbmi;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lbmi;->bI(Z)V

    const/4 v0, 0x0

    iput-boolean v0, v1, Lbml;->aFA:Z
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_2
    :try_start_8
    invoke-super {p0}, Lbir;->disconnect()V

    monitor-exit v2

    return-void

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method public final zH()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lbmo;->aFE:Lbml;

    invoke-virtual {v0}, Lbml;->zH()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
