.class public final Ldlq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field ff:I

.field mLength:I

.field rQ:[B


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, Ldlq;->g([BII)V

    .line 62
    return-void
.end method


# virtual methods
.method final G([B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Ldlq;->g([BII)V

    .line 66
    return-void

    .line 65
    :cond_0
    array-length v0, p1

    goto :goto_0
.end method

.method public final H([B)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 102
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iget v2, p0, Ldlq;->mLength:I

    if-eq v0, v2, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    array-length v0, p1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Ldlq;->rQ:[B

    if-ne p1, v2, :cond_4

    iget v2, p0, Ldlq;->ff:I

    if-nez v2, :cond_4

    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    move v2, v1

    :goto_2
    if-eq v2, v0, :cond_3

    add-int/lit8 v3, v2, 0x0

    aget-byte v3, p1, v3

    iget-object v4, p0, Ldlq;->rQ:[B

    iget v5, p0, Ldlq;->ff:I

    add-int/2addr v5, v2

    aget-byte v4, v4, v5

    if-ne v3, v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public final b(B)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 121
    iget v1, p0, Ldlq;->mLength:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Ldlq;->rQ:[B

    iget v2, p0, Ldlq;->ff:I

    aget-byte v1, v1, v2

    if-ne v1, p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final g([BII)V
    .locals 1

    .prologue
    .line 69
    if-nez p1, :cond_1

    .line 70
    if-nez p2, :cond_0

    if-eqz p3, :cond_3

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 73
    :cond_1
    if-ltz p2, :cond_2

    if-ltz p3, :cond_2

    array-length v0, p1

    if-gt p2, v0, :cond_2

    array-length v0, p1

    sub-int/2addr v0, p2

    if-le p3, v0, :cond_3

    .line 75
    :cond_2
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 77
    :cond_3
    iput-object p1, p0, Ldlq;->rQ:[B

    .line 78
    iput p2, p0, Ldlq;->ff:I

    .line 79
    iput p3, p0, Ldlq;->mLength:I

    .line 80
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Ldlq;->rQ:[B

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Ldlq;->rQ:[B

    iget v2, p0, Ldlq;->ff:I

    iget v3, p0, Ldlq;->mLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    goto :goto_0
.end method
