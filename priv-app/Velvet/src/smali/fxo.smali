.class public final Lfxo;
.super Lfus;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;Lgbd;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    .line 45
    iput-object p5, p0, Lfxo;->mPhotoWithAttributionDecorator:Lgbd;

    .line 46
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;Lgbd;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 36
    iput-object p5, p0, Lfxo;->mPhotoWithAttributionDecorator:Lgbd;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 9

    .prologue
    const v8, 0x7f0d015b

    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 60
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    .line 61
    invoke-virtual {v3, v7}, Lanh;->cm(I)Lanh;

    .line 62
    new-instance v0, Lanj;

    invoke-direct {v0}, Lanj;-><init>()V

    iget-object v1, p2, Lizj;->dTr:Ljij;

    invoke-virtual {v1}, Ljij;->bnf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanj;->ao(Ljava/lang/String;)Lanj;

    move-result-object v0

    iget-object v1, p2, Lizj;->dTr:Ljij;

    invoke-virtual {v1}, Ljij;->bnh()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanj;->ap(Ljava/lang/String;)Lanj;

    move-result-object v0

    iput-object v0, v3, Lanh;->agF:Lanj;

    .line 65
    iget-object v0, p2, Lizj;->dTr:Ljij;

    iget-object v0, v0, Ljij;->ens:[Lixx;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lizj;->dTr:Ljij;

    iget-object v0, v0, Ljij;->ens:[Lixx;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 66
    iget-object v0, p2, Lizj;->dTr:Ljij;

    iget-object v4, v0, Ljij;->ens:[Lixx;

    array-length v5, v4

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_3

    aget-object v0, v4, v1

    invoke-virtual {v0}, Lixx;->bbc()I

    move-result v6

    if-ne v6, v7, :cond_2

    .line 67
    :goto_1
    new-instance v1, Lfsa;

    invoke-virtual {v0}, Lixx;->oY()I

    move-result v2

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    invoke-virtual {v1, v0}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    .line 69
    iput-object p2, v3, Lanh;->ahu:Lizj;

    .line 71
    :cond_0
    iget-object v0, p2, Lizj;->dTr:Ljij;

    iget-object v0, v0, Ljij;->aiS:Ljcn;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lfxo;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v1, p2, Lizj;->dTr:Ljij;

    iget-object v1, v1, Ljij;->aiS:Ljcn;

    invoke-virtual {v0, p1, v1, v8, v8}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_1

    .line 77
    iget-object v1, v3, Lanh;->agF:Lanj;

    new-instance v2, Laoi;

    invoke-direct {v2}, Laoi;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoi;->bQ(Ljava/lang/String;)Laoi;

    move-result-object v0

    iput-object v0, v1, Lanj;->ahM:Laoi;

    .line 80
    :cond_1
    return-object v3

    .line 66
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    aget-object v0, v4, v2

    goto :goto_1
.end method

.method public final aDq()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method public final aDv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTP:Ljig;

    invoke-virtual {v0}, Ljig;->bnc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aU(Landroid/content/Context;)Lanh;
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTP:Ljig;

    iget-object v0, v0, Ljig;->ahD:Lixx;

    .line 96
    new-instance v1, Lfsa;

    invoke-virtual {v0}, Lixx;->oY()I

    move-result v2

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfsa;->b(Lixx;)Lani;

    move-result-object v1

    .line 100
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    .line 101
    invoke-virtual {v0}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 103
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 104
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    .line 105
    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iput-object v3, v0, Lanh;->ahu:Lizj;

    .line 106
    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 107
    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 108
    return-object v0
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTP:Ljig;

    invoke-virtual {v0}, Ljig;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dTP:Ljig;

    if-eqz v1, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 120
    :goto_0
    return-object v0

    .line 119
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Lanh;

    iget-object v2, p2, Lizj;->dTr:Ljij;

    new-instance v3, Lfrs;

    invoke-virtual {v2}, Ljij;->bne()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p1, v4, p2}, Lfrs;-><init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V

    invoke-virtual {v2}, Ljij;->bng()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Ljij;->bnf()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfrs;->cBj:Ljava/lang/String;

    :cond_1
    invoke-virtual {v2}, Ljij;->bnd()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljij;->bnc()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfrs;->cBi:Ljava/lang/String;

    :cond_2
    iget-object v4, v2, Ljij;->aiS:Ljcn;

    if-eqz v4, :cond_3

    iget-object v2, v2, Ljij;->aiS:Ljcn;

    iget-object v4, p0, Lfxo;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-virtual {v3, v2, v4}, Lfrs;->a(Ljcn;Lgbd;)Lfrs;

    :cond_3
    invoke-virtual {v3}, Lfrs;->aDh()Lanh;

    move-result-object v2

    aput-object v2, v1, v0

    .line 120
    iget-object v2, p2, Lizj;->dTr:Ljij;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v2, Ljij;->ens:[Lixx;

    array-length v4, v2

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v2, v0

    invoke-virtual {v5}, Lixx;->rn()Z

    move-result v6

    if-eqz v6, :cond_4

    new-instance v6, Laoj;

    invoke-direct {v6}, Laoj;-><init>()V

    invoke-virtual {v5}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v7, Lanh;

    invoke-direct {v7}, Lanh;-><init>()V

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lanh;->cm(I)Lanh;

    iput-object v6, v7, Lanh;->agz:Laoj;

    new-instance v6, Lfsa;

    invoke-virtual {v5}, Lixx;->oY()I

    move-result v8

    invoke-direct {v6, v8}, Lfsa;-><init>(I)V

    invoke-virtual {v6, v5}, Lfsa;->b(Lixx;)Lani;

    move-result-object v5

    iput-object v5, v7, Lanh;->ahs:Lani;

    iput-object p2, v7, Lanh;->ahu:Lizj;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-static {v1, v3}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    goto :goto_0
.end method
