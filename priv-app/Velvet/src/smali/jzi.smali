.class public final Ljzi;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eDw:I

.field private eDx:I

.field private eND:I

.field private eNE:F

.field private eNF:I

.field private eNG:I

.field private eNH:I

.field public eNm:Ljzk;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2673
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2674
    iput v1, p0, Ljzi;->aez:I

    const/16 v0, 0xa

    iput v0, p0, Ljzi;->eDw:I

    const/16 v0, 0xfa

    iput v0, p0, Ljzi;->eDx:I

    const/16 v0, 0xc8

    iput v0, p0, Ljzi;->eND:I

    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Ljzi;->eNE:F

    const/16 v0, 0xf

    iput v0, p0, Ljzi;->eNF:I

    const/16 v0, 0x32

    iput v0, p0, Ljzi;->eNG:I

    iput-object v2, p0, Ljzi;->eNm:Ljzk;

    iput v1, p0, Ljzi;->eNH:I

    iput-object v2, p0, Ljzi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzi;->eCz:I

    .line 2675
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzi;->eDw:I

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzi;->eDx:I

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzi;->eND:I

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljzi;->eNE:F

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzi;->eNF:I

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzi;->eNG:I

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljzi;->eNm:Ljzk;

    if-nez v0, :cond_1

    new-instance v0, Ljzk;

    invoke-direct {v0}, Ljzk;-><init>()V

    iput-object v0, p0, Ljzi;->eNm:Ljzk;

    :cond_1
    iget-object v0, p0, Ljzi;->eNm:Ljzk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzi;->eNH:I

    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljzi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2695
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2696
    const/4 v0, 0x1

    iget v1, p0, Ljzi;->eDw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2698
    :cond_0
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2699
    const/4 v0, 0x2

    iget v1, p0, Ljzi;->eDx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2701
    :cond_1
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2702
    const/4 v0, 0x3

    iget v1, p0, Ljzi;->eND:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2704
    :cond_2
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2705
    const/4 v0, 0x4

    iget v1, p0, Ljzi;->eNE:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 2707
    :cond_3
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 2708
    const/4 v0, 0x5

    iget v1, p0, Ljzi;->eNF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2710
    :cond_4
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 2711
    const/4 v0, 0x6

    iget v1, p0, Ljzi;->eNG:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2713
    :cond_5
    iget-object v0, p0, Ljzi;->eNm:Ljzk;

    if-eqz v0, :cond_6

    .line 2714
    const/4 v0, 0x7

    iget-object v1, p0, Ljzi;->eNm:Ljzk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2716
    :cond_6
    iget v0, p0, Ljzi;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 2717
    const/16 v0, 0x8

    iget v1, p0, Ljzi;->eNH:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2719
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2720
    return-void
.end method

.method public final buv()I
    .locals 1

    .prologue
    .line 2540
    iget v0, p0, Ljzi;->eDw:I

    return v0
.end method

.method public final buw()I
    .locals 1

    .prologue
    .line 2559
    iget v0, p0, Ljzi;->eDx:I

    return v0
.end method

.method public final bwU()I
    .locals 1

    .prologue
    .line 2578
    iget v0, p0, Ljzi;->eND:I

    return v0
.end method

.method public final bwV()F
    .locals 1

    .prologue
    .line 2597
    iget v0, p0, Ljzi;->eNE:F

    return v0
.end method

.method public final bwW()I
    .locals 1

    .prologue
    .line 2657
    iget v0, p0, Ljzi;->eNH:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2724
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2725
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2726
    const/4 v1, 0x1

    iget v2, p0, Ljzi;->eDw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2729
    :cond_0
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2730
    const/4 v1, 0x2

    iget v2, p0, Ljzi;->eDx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2733
    :cond_1
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2734
    const/4 v1, 0x3

    iget v2, p0, Ljzi;->eND:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2737
    :cond_2
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2738
    const/4 v1, 0x4

    iget v2, p0, Ljzi;->eNE:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2741
    :cond_3
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 2742
    const/4 v1, 0x5

    iget v2, p0, Ljzi;->eNF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2745
    :cond_4
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 2746
    const/4 v1, 0x6

    iget v2, p0, Ljzi;->eNG:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2749
    :cond_5
    iget-object v1, p0, Ljzi;->eNm:Ljzk;

    if-eqz v1, :cond_6

    .line 2750
    const/4 v1, 0x7

    iget-object v2, p0, Ljzi;->eNm:Ljzk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2753
    :cond_6
    iget v1, p0, Ljzi;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 2754
    const/16 v1, 0x8

    iget v2, p0, Ljzi;->eNH:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2757
    :cond_7
    return v0
.end method

.method public final tr(I)Ljzi;
    .locals 1

    .prologue
    .line 2543
    const/4 v0, 0x1

    iput v0, p0, Ljzi;->eDw:I

    .line 2544
    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzi;->aez:I

    .line 2545
    return-object p0
.end method

.method public final ts(I)Ljzi;
    .locals 1

    .prologue
    .line 2562
    const/4 v0, 0x2

    iput v0, p0, Ljzi;->eDx:I

    .line 2563
    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzi;->aez:I

    .line 2564
    return-object p0
.end method

.method public final tt(I)Ljzi;
    .locals 1

    .prologue
    .line 2660
    const/16 v0, 0x7b

    iput v0, p0, Ljzi;->eNH:I

    .line 2661
    iget v0, p0, Ljzi;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljzi;->aez:I

    .line 2662
    return-object p0
.end method
