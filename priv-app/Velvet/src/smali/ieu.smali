.class public final Lieu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAS:Lieo;

.field private dAT:J

.field private dAU:I

.field private dAV:Z

.field private dAW:Lieo;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 245
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 246
    iput v2, p0, Lieu;->aez:I

    iput-object v3, p0, Lieu;->dAS:Lieo;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lieu;->dAT:J

    iput v2, p0, Lieu;->dAU:I

    iput-boolean v2, p0, Lieu;->dAV:Z

    iput-object v3, p0, Lieu;->dAW:Lieo;

    iput-object v3, p0, Lieu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lieu;->eCz:I

    .line 247
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lieu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lieu;->dAS:Lieo;

    if-nez v0, :cond_1

    new-instance v0, Lieo;

    invoke-direct {v0}, Lieo;-><init>()V

    iput-object v0, p0, Lieu;->dAS:Lieo;

    :cond_1
    iget-object v0, p0, Lieu;->dAS:Lieo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lieu;->dAT:J

    iget v0, p0, Lieu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lieu;->dAU:I

    iget v0, p0, Lieu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieu;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lieu;->dAV:Z

    iget v0, p0, Lieu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lieu;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lieu;->dAW:Lieo;

    if-nez v0, :cond_2

    new-instance v0, Lieo;

    invoke-direct {v0}, Lieo;-><init>()V

    iput-object v0, p0, Lieu;->dAW:Lieo;

    :cond_2
    iget-object v0, p0, Lieu;->dAW:Lieo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lieu;->dAS:Lieo;

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x1

    iget-object v1, p0, Lieu;->dAS:Lieo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 267
    :cond_0
    iget v0, p0, Lieu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 268
    const/4 v0, 0x2

    iget-wide v2, p0, Lieu;->dAT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 270
    :cond_1
    iget v0, p0, Lieu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 271
    const/4 v0, 0x3

    iget v1, p0, Lieu;->dAU:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 273
    :cond_2
    iget v0, p0, Lieu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 274
    const/4 v0, 0x4

    iget-boolean v1, p0, Lieu;->dAV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 276
    :cond_3
    iget-object v0, p0, Lieu;->dAW:Lieo;

    if-eqz v0, :cond_4

    .line 277
    const/4 v0, 0x5

    iget-object v1, p0, Lieu;->dAW:Lieo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 279
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 280
    return-void
.end method

.method public final aVS()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lieu;->dAU:I

    return v0
.end method

.method public final bP(J)Lieu;
    .locals 1

    .prologue
    .line 191
    iput-wide p1, p0, Lieu;->dAT:J

    .line 192
    iget v0, p0, Lieu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieu;->aez:I

    .line 193
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 284
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 285
    iget-object v1, p0, Lieu;->dAS:Lieo;

    if-eqz v1, :cond_0

    .line 286
    const/4 v1, 0x1

    iget-object v2, p0, Lieu;->dAS:Lieo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_0
    iget v1, p0, Lieu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 290
    const/4 v1, 0x2

    iget-wide v2, p0, Lieu;->dAT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_1
    iget v1, p0, Lieu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 294
    const/4 v1, 0x3

    iget v2, p0, Lieu;->dAU:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_2
    iget v1, p0, Lieu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 298
    const/4 v1, 0x4

    iget-boolean v2, p0, Lieu;->dAV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 301
    :cond_3
    iget-object v1, p0, Lieu;->dAW:Lieo;

    if-eqz v1, :cond_4

    .line 302
    const/4 v1, 0x5

    iget-object v2, p0, Lieu;->dAW:Lieo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    :cond_4
    return v0
.end method

.method public final lX(I)Lieu;
    .locals 1

    .prologue
    .line 210
    iput p1, p0, Lieu;->dAU:I

    .line 211
    iget v0, p0, Lieu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieu;->aez:I

    .line 212
    return-object p0
.end method
