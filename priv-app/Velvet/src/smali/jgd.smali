.class public final Ljgd;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public afA:Ljbj;

.field private ebr:Ljava/lang/String;

.field public ejr:Lizk;

.field public ejs:Ljcl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Ljgd;->aez:I

    iput-object v1, p0, Ljgd;->afA:Ljbj;

    const-string v0, ""

    iput-object v0, p0, Ljgd;->ebr:Ljava/lang/String;

    iput-object v1, p0, Ljgd;->ejr:Lizk;

    iput-object v1, p0, Ljgd;->ejs:Ljcl;

    iput-object v1, p0, Ljgd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgd;->eCz:I

    .line 88
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 36
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljgd;->afA:Ljbj;

    if-nez v0, :cond_1

    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    iput-object v0, p0, Ljgd;->afA:Ljbj;

    :cond_1
    iget-object v0, p0, Ljgd;->afA:Ljbj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgd;->ebr:Ljava/lang/String;

    iget v0, p0, Ljgd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgd;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljgd;->ejr:Lizk;

    if-nez v0, :cond_2

    new-instance v0, Lizk;

    invoke-direct {v0}, Lizk;-><init>()V

    iput-object v0, p0, Ljgd;->ejr:Lizk;

    :cond_2
    iget-object v0, p0, Ljgd;->ejr:Lizk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljgd;->ejs:Ljcl;

    if-nez v0, :cond_3

    new-instance v0, Ljcl;

    invoke-direct {v0}, Ljcl;-><init>()V

    iput-object v0, p0, Ljgd;->ejs:Ljcl;

    :cond_3
    iget-object v0, p0, Ljgd;->ejs:Ljcl;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Ljgd;->afA:Ljbj;

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iget-object v1, p0, Ljgd;->afA:Ljbj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 107
    :cond_0
    iget v0, p0, Ljgd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 108
    const/4 v0, 0x2

    iget-object v1, p0, Ljgd;->ebr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 110
    :cond_1
    iget-object v0, p0, Ljgd;->ejr:Lizk;

    if-eqz v0, :cond_2

    .line 111
    const/4 v0, 0x3

    iget-object v1, p0, Ljgd;->ejr:Lizk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 113
    :cond_2
    iget-object v0, p0, Ljgd;->ejs:Ljcl;

    if-eqz v0, :cond_3

    .line 114
    const/4 v0, 0x4

    iget-object v1, p0, Ljgd;->ejs:Ljcl;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 116
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 117
    return-void
.end method

.method public final bjP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ljgd;->ebr:Ljava/lang/String;

    return-object v0
.end method

.method public final bjQ()Z
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Ljgd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 121
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 122
    iget-object v1, p0, Ljgd;->afA:Ljbj;

    if-eqz v1, :cond_0

    .line 123
    const/4 v1, 0x1

    iget-object v2, p0, Ljgd;->afA:Ljbj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_0
    iget v1, p0, Ljgd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 127
    const/4 v1, 0x2

    iget-object v2, p0, Ljgd;->ebr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_1
    iget-object v1, p0, Ljgd;->ejr:Lizk;

    if-eqz v1, :cond_2

    .line 131
    const/4 v1, 0x3

    iget-object v2, p0, Ljgd;->ejr:Lizk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_2
    iget-object v1, p0, Ljgd;->ejs:Ljcl;

    if-eqz v1, :cond_3

    .line 135
    const/4 v1, 0x4

    iget-object v2, p0, Ljgd;->ejs:Ljcl;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_3
    return v0
.end method
