.class public final Lwo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private Js:J

.field private Jt:F

.field private Ju:F

.field public Jv:Landroid/animation/ValueAnimator;

.field private Jw:Z

.field public Jx:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/view/View;JFF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v3, p0, Lwo;->Jw:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lwo;->Jx:Ljava/lang/Object;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v2, v0, v1

    aput p5, v0, v3

    invoke-static {v0}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    .line 50
    iput-wide p2, p0, Lwo;->Js:J

    .line 51
    iput v2, p0, Lwo;->Jt:F

    .line 52
    iput p5, p0, Lwo;->Ju:F

    .line 54
    iget-object v0, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    new-instance v1, Lwp;

    invoke-direct {v1, p0}, Lwp;-><init>(Lwo;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 60
    return-void
.end method


# virtual methods
.method public bj(I)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 63
    iget-object v0, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v2

    .line 64
    if-ne p1, v11, :cond_0

    iget v0, p0, Lwo;->Ju:F

    move v1, v0

    .line 65
    :goto_0
    iget-boolean v0, p0, Lwo;->Jw:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lwo;->Jt:F

    .line 69
    :goto_1
    iget-object v4, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    .line 73
    iget-wide v4, p0, Lwo;->Js:J

    sub-long v2, v4, v2

    .line 77
    iget-object v4, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    const-wide/16 v6, 0x0

    iget-wide v8, p0, Lwo;->Js:J

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 79
    iget-object v2, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v0, v3, v10

    aput v1, v3, v11

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 80
    iget-object v0, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 81
    iput-boolean v10, p0, Lwo;->Jw:Z

    .line 82
    return-void

    .line 64
    :cond_0
    iget v0, p0, Lwo;->Jt:F

    move v1, v0

    goto :goto_0

    .line 65
    :cond_1
    iget-object v0, p0, Lwo;->Jv:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_1
.end method
