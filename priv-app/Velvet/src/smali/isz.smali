.class public final Lisz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dHj:[Lisz;


# instance fields
.field private aXU:J

.field private aez:I

.field private afb:Ljava/lang/String;

.field public dHk:[Ljava/lang/String;

.field private dHl:J

.field public dHm:[Litb;

.field public dHn:[Lita;

.field public dHo:[Litc;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 210
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 211
    const/4 v0, 0x0

    iput v0, p0, Lisz;->aez:I

    iput-wide v2, p0, Lisz;->aXU:J

    const-string v0, ""

    iput-object v0, p0, Lisz;->afb:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lisz;->dHk:[Ljava/lang/String;

    iput-wide v2, p0, Lisz;->dHl:J

    invoke-static {}, Litb;->aYC()[Litb;

    move-result-object v0

    iput-object v0, p0, Lisz;->dHm:[Litb;

    invoke-static {}, Lita;->aYB()[Lita;

    move-result-object v0

    iput-object v0, p0, Lisz;->dHn:[Lita;

    invoke-static {}, Litc;->aYD()[Litc;

    move-result-object v0

    iput-object v0, p0, Lisz;->dHo:[Litc;

    const/4 v0, 0x0

    iput-object v0, p0, Lisz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lisz;->eCz:I

    .line 212
    return-void
.end method

.method public static aYA()[Lisz;
    .locals 2

    .prologue
    .line 125
    sget-object v0, Lisz;->dHj:[Lisz;

    if-nez v0, :cond_1

    .line 126
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    sget-object v0, Lisz;->dHj:[Lisz;

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    new-array v0, v0, [Lisz;

    sput-object v0, Lisz;->dHj:[Lisz;

    .line 131
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    :cond_1
    sget-object v0, Lisz;->dHj:[Lisz;

    return-object v0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final KN()J
    .locals 2

    .prologue
    .line 141
    iget-wide v0, p0, Lisz;->aXU:J

    return-wide v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lisz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lisz;->aXU:J

    iget v0, p0, Lisz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lisz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lisz;->afb:Ljava/lang/String;

    iget v0, p0, Lisz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lisz;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lisz;->dHk:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lisz;->dHk:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lisz;->dHk:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lisz;->dHk:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lisz;->dHl:J

    iget v0, p0, Lisz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lisz;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lisz;->dHm:[Litb;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Litb;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lisz;->dHm:[Litb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Litb;

    invoke-direct {v3}, Litb;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lisz;->dHm:[Litb;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Litb;

    invoke-direct {v3}, Litb;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lisz;->dHm:[Litb;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lisz;->dHn:[Lita;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lita;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lisz;->dHn:[Lita;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lita;

    invoke-direct {v3}, Lita;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lisz;->dHn:[Lita;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lita;

    invoke-direct {v3}, Lita;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lisz;->dHn:[Lita;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lisz;->dHo:[Litc;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Litc;

    if-eqz v0, :cond_a

    iget-object v3, p0, Lisz;->dHo:[Litc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    new-instance v3, Litc;

    invoke-direct {v3}, Litc;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lisz;->dHo:[Litc;

    array-length v0, v0

    goto :goto_7

    :cond_c
    new-instance v3, Litc;

    invoke-direct {v3}, Litc;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lisz;->dHo:[Litc;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 231
    iget v0, p0, Lisz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x1

    iget-wide v2, p0, Lisz;->aXU:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 234
    :cond_0
    iget v0, p0, Lisz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 235
    const/4 v0, 0x2

    iget-object v2, p0, Lisz;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 237
    :cond_1
    iget-object v0, p0, Lisz;->dHk:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lisz;->dHk:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 238
    :goto_0
    iget-object v2, p0, Lisz;->dHk:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 239
    iget-object v2, p0, Lisz;->dHk:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 240
    if-eqz v2, :cond_2

    .line 241
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 238
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_3
    iget v0, p0, Lisz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 246
    const/4 v0, 0x4

    iget-wide v2, p0, Lisz;->dHl:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 248
    :cond_4
    iget-object v0, p0, Lisz;->dHm:[Litb;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lisz;->dHm:[Litb;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 249
    :goto_1
    iget-object v2, p0, Lisz;->dHm:[Litb;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 250
    iget-object v2, p0, Lisz;->dHm:[Litb;

    aget-object v2, v2, v0

    .line 251
    if-eqz v2, :cond_5

    .line 252
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 249
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 256
    :cond_6
    iget-object v0, p0, Lisz;->dHn:[Lita;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lisz;->dHn:[Lita;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 257
    :goto_2
    iget-object v2, p0, Lisz;->dHn:[Lita;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 258
    iget-object v2, p0, Lisz;->dHn:[Lita;

    aget-object v2, v2, v0

    .line 259
    if-eqz v2, :cond_7

    .line 260
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 257
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 264
    :cond_8
    iget-object v0, p0, Lisz;->dHo:[Litc;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lisz;->dHo:[Litc;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 265
    :goto_3
    iget-object v0, p0, Lisz;->dHo:[Litc;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 266
    iget-object v0, p0, Lisz;->dHo:[Litc;

    aget-object v0, v0, v1

    .line 267
    if-eqz v0, :cond_9

    .line 268
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 265
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 272
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 273
    return-void
.end method

.method public final bU(J)Lisz;
    .locals 1

    .prologue
    .line 144
    iput-wide p1, p0, Lisz;->aXU:J

    .line 145
    iget v0, p0, Lisz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lisz;->aez:I

    .line 146
    return-object p0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lisz;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 277
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 278
    iget v1, p0, Lisz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 279
    const/4 v1, 0x1

    iget-wide v4, p0, Lisz;->aXU:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    :cond_0
    iget v1, p0, Lisz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 283
    const/4 v1, 0x2

    iget-object v3, p0, Lisz;->afb:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    :cond_1
    iget-object v1, p0, Lisz;->dHk:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lisz;->dHk:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 289
    :goto_0
    iget-object v5, p0, Lisz;->dHk:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 290
    iget-object v5, p0, Lisz;->dHk:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 291
    if-eqz v5, :cond_2

    .line 292
    add-int/lit8 v4, v4, 0x1

    .line 293
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 289
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :cond_3
    add-int/2addr v0, v3

    .line 298
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 300
    :cond_4
    iget v1, p0, Lisz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 301
    const/4 v1, 0x4

    iget-wide v4, p0, Lisz;->dHl:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_5
    iget-object v1, p0, Lisz;->dHm:[Litb;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lisz;->dHm:[Litb;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v0

    move v0, v2

    .line 305
    :goto_1
    iget-object v3, p0, Lisz;->dHm:[Litb;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 306
    iget-object v3, p0, Lisz;->dHm:[Litb;

    aget-object v3, v3, v0

    .line 307
    if-eqz v3, :cond_6

    .line 308
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v1, v3

    .line 305
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v1

    .line 313
    :cond_8
    iget-object v1, p0, Lisz;->dHn:[Lita;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lisz;->dHn:[Lita;

    array-length v1, v1

    if-lez v1, :cond_b

    move v1, v0

    move v0, v2

    .line 314
    :goto_2
    iget-object v3, p0, Lisz;->dHn:[Lita;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 315
    iget-object v3, p0, Lisz;->dHn:[Lita;

    aget-object v3, v3, v0

    .line 316
    if-eqz v3, :cond_9

    .line 317
    const/4 v4, 0x6

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v1, v3

    .line 314
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    move v0, v1

    .line 322
    :cond_b
    iget-object v1, p0, Lisz;->dHo:[Litc;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lisz;->dHo:[Litc;

    array-length v1, v1

    if-lez v1, :cond_d

    .line 323
    :goto_3
    iget-object v1, p0, Lisz;->dHo:[Litc;

    array-length v1, v1

    if-ge v2, v1, :cond_d

    .line 324
    iget-object v1, p0, Lisz;->dHo:[Litc;

    aget-object v1, v1, v2

    .line 325
    if-eqz v1, :cond_c

    .line 326
    const/4 v3, 0x7

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 331
    :cond_d
    return v0
.end method

.method public final py(Ljava/lang/String;)Lisz;
    .locals 1

    .prologue
    .line 163
    if-nez p1, :cond_0

    .line 164
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 166
    :cond_0
    iput-object p1, p0, Lisz;->afb:Ljava/lang/String;

    .line 167
    iget v0, p0, Lisz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lisz;->aez:I

    .line 168
    return-object p0
.end method
