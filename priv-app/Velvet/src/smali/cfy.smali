.class public Lcfy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aUJ:I

.field public final aUK:Landroid/net/Uri;

.field public final aUL:Landroid/net/Uri;

.field public final aUM:I

.field public final aUN:Ljava/util/Map;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final aqV:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 35
    move-object v0, p0

    move-object v3, v1

    move-object v4, v1

    move v5, v2

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcfy;-><init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;ILjava/util/Map;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;ILjava/util/Map;)V
    .locals 0
    .param p6    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcfy;->aqV:Ljava/lang/String;

    .line 41
    iput p2, p0, Lcfy;->aUJ:I

    .line 42
    iput-object p3, p0, Lcfy;->aUK:Landroid/net/Uri;

    .line 43
    iput-object p4, p0, Lcfy;->aUL:Landroid/net/Uri;

    .line 44
    iput p5, p0, Lcfy;->aUM:I

    .line 45
    iput-object p6, p0, Lcfy;->aUN:Ljava/util/Map;

    .line 46
    return-void
.end method

.method protected static a([Ljjb;)Ljava/util/Map;
    .locals 5

    .prologue
    .line 97
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 98
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 99
    iget-object v4, v3, Ljjb;->name:Ljava/lang/String;

    iget-object v3, v3, Ljjb;->value:Ljava/lang/String;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 106
    instance-of v1, p1, Lcfy;

    if-eqz v1, :cond_0

    .line 107
    check-cast p1, Lcfy;

    .line 108
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcfy;->aqV:Ljava/lang/String;

    iget-object v2, p1, Lcfy;->aqV:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 111
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcfy;->aqV:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Corpus["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcfy;->aqV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
