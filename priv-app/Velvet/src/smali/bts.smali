.class public Lbts;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static aJD:I


# instance fields
.field private final aJE:Ljava/util/concurrent/ConcurrentHashMap;

.field private aJF:Lbtt;

.field private final aJG:Ljava/util/LinkedList;

.field private aJH:Z

.field private aJI:F

.field private aJJ:F

.field public final mClient:Lbhi;

.field public final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, -0x1

    sput v0, Lbts;->aJD:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbhi;Z)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbts;->aJE:Ljava/util/concurrent/ConcurrentHashMap;

    .line 65
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    .line 75
    iput-object p1, p0, Lbts;->mContext:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lbts;->mClient:Lbhi;

    .line 77
    iput-boolean p3, p0, Lbts;->aJH:Z

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lbts;->aJJ:F

    .line 80
    const v1, 0x7f0c0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lbts;->aJI:F

    .line 81
    return-void
.end method

.method private AP()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lbts;->aJF:Lbtt;

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtt;

    iput-object v0, p0, Lbts;->aJF:Lbtt;

    .line 154
    iget-object v0, p0, Lbts;->aJF:Lbtt;

    invoke-virtual {v0}, Lbtt;->AO()V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 262
    int-to-float v0, p1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne p1, v1, :cond_0

    if-ne v0, v2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float v3, p1

    int-to-float v4, v1

    div-float/2addr v3, v4

    int-to-float v4, v0

    int-to-float v6, v2

    div-float/2addr v4, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v5, v4, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    int-to-float v3, p1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v0, v0

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v0, v1

    mul-float/2addr v0, v7

    div-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    sub-float/2addr v0, v6

    float-to-int v0, v0

    int-to-float v6, v2

    mul-float/2addr v6, v7

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    sub-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int v0, v2, v4

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method static synthetic a(Lbts;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 35
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lbts;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    sget v1, Lbts;->aJD:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lbts;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lbts;->aJD:I

    :cond_0
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v1, Lbts;->aJD:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic a(Lbts;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lbts;->aJH:Z

    return v0
.end method

.method static synthetic b(Lbts;)F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lbts;->aJI:F

    return v0
.end method

.method static synthetic c(Lbts;)F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lbts;->aJJ:F

    return v0
.end method

.method static synthetic d(Lbts;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lbts;->aJE:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method


# virtual methods
.method public final a(Lbtt;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lbts;->aJE:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, Lbtt;->aGe:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v1, p1, Lbtt;->aJL:Landroid/widget/ImageView;

    iget-object v0, p0, Lbts;->aJE:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p1, Lbtt;->aGe:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    iget-object v0, p1, Lbtt;->aJL:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lbts;->c(Landroid/widget/ImageView;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p1, Lbtt;->aJL:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lbts;->c(Landroid/widget/ImageView;)V

    iget-object v1, p0, Lbts;->mClient:Lbhi;

    invoke-interface {v1}, Lbhi;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lbts;->AP()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lbtt;Landroid/graphics/Bitmap;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 159
    .line 161
    :try_start_0
    iget-object v0, p0, Lbts;->aJF:Lbtt;

    if-eq v0, p3, :cond_1

    .line 162
    const-string v0, "AvatarManager"

    const-string v1, "Got a different request than we\'re waiting for!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    invoke-direct {p0}, Lbts;->AP()V

    .line 188
    if-eqz p2, :cond_0

    .line 190
    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lbts;->aJF:Lbtt;

    .line 167
    iget-object v0, p3, Lbtt;->aJL:Landroid/widget/ImageView;

    .line 171
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p3, :cond_2

    iget-boolean v0, p3, Lbtt;->aJK:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_3

    .line 185
    :cond_2
    invoke-direct {p0}, Lbts;->AP()V

    .line 188
    if-eqz p2, :cond_0

    .line 190
    :try_start_3
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 176
    :cond_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->yk()Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p2, :cond_5

    .line 177
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Avatar loaded: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  pfd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180
    :cond_5
    if-eqz p2, :cond_6

    .line 181
    new-instance v0, Lbtu;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lbtu;-><init>(Lbts;Lbtt;Landroid/os/ParcelFileDescriptor;Landroid/graphics/Bitmap;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lbtu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object p2, v6

    .line 185
    :cond_6
    invoke-direct {p0}, Lbts;->AP()V

    .line 188
    if-eqz p2, :cond_0

    .line 190
    :try_start_5
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 192
    :catch_1
    move-exception v0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    .line 186
    invoke-direct {p0}, Lbts;->AP()V

    .line 188
    if-eqz p2, :cond_7

    .line 190
    :try_start_6
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 192
    :cond_7
    :goto_1
    throw v0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public final c(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 130
    const/4 v0, 0x0

    move v1, v0

    .line 132
    :goto_0
    iget-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 133
    iget-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtt;

    iget-object v0, v0, Lbtt;->aJL:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_0

    .line 134
    iget-object v0, p0, Lbts;->aJG:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 136
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 140
    :cond_1
    iget-object v0, p0, Lbts;->aJF:Lbtt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbts;->aJF:Lbtt;

    iget-object v0, v0, Lbtt;->aJL:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_2

    .line 141
    iget-object v0, p0, Lbts;->aJF:Lbtt;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lbtt;->aJK:Z

    .line 143
    :cond_2
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lbts;->mContext:Landroid/content/Context;

    return-object v0
.end method
