.class public abstract Ldjz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bAB:Lcng;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bAC:Ljava/util/Queue;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private volatile bAD:Ljava/util/concurrent/Future;

.field private final bbV:Ljava/util/concurrent/atomic/AtomicInteger;

.field protected final bbk:Legl;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mConsumer:Ldkc;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Queue;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V
    .locals 2
    .param p1    # Ljava/util/Queue;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcng;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Ldjz;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 190
    iput-object p2, p0, Ldjz;->bAB:Lcng;

    .line 191
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Ldjz;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 192
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legl;

    iput-object v0, p0, Ldjz;->bbk:Legl;

    .line 193
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Queue;->size()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 194
    iput-object p1, p0, Ldjz;->bAC:Ljava/util/Queue;

    .line 195
    iget-object v0, p0, Ldjz;->bAC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkc;

    iput-object v0, p0, Ldjz;->mConsumer:Ldkc;

    .line 196
    return-void

    .line 193
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private acx()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Ldjz;->bAD:Ljava/util/concurrent/Future;

    .line 228
    if-eqz v0, :cond_0

    .line 230
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 232
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract PX()V
.end method

.method protected final Qd()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 242
    iget-object v0, p0, Ldjz;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :goto_0
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    invoke-interface {v0}, Ldkc;->PO()V

    .line 245
    iget-object v0, p0, Ldjz;->bAC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkc;

    iput-object v0, p0, Ldjz;->mConsumer:Ldkc;

    goto :goto_0

    :cond_0
    move v0, v1

    .line 249
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final acw()V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream cancelled"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ldjz;->c(Ljava/lang/Exception;)Z

    .line 223
    invoke-direct {p0}, Ldjz;->acx()V

    .line 224
    return-void
.end method

.method protected final acy()Z
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    invoke-interface {v0}, Ldkc;->PO()V

    .line 291
    iget-object v0, p0, Ldjz;->bAC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkc;

    iput-object v0, p0, Ldjz;->mConsumer:Ldkc;

    .line 292
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    if-nez v0, :cond_0

    .line 293
    invoke-direct {p0}, Ldjz;->acx()V

    .line 294
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final b(Ldkd;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 274
    iget-object v1, p0, Ldjz;->mConsumer:Ldkc;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    iget-object v1, p0, Ldjz;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 276
    if-eq v1, v0, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 280
    :goto_0
    return v0

    .line 279
    :cond_0
    iget-object v1, p0, Ldjz;->mConsumer:Ldkc;

    invoke-interface {v1, p1}, Ldkc;->c(Ldkd;)V

    goto :goto_0
.end method

.method protected final c(Ljava/lang/Exception;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 258
    iget-object v0, p0, Ldjz;->bbV:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    :goto_0
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Ldjz;->mConsumer:Ldkc;

    invoke-interface {v0, p1}, Ldkc;->d(Ljava/lang/Exception;)V

    .line 261
    iget-object v0, p0, Ldjz;->bAC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldkc;

    iput-object v0, p0, Ldjz;->mConsumer:Ldkc;

    goto :goto_0

    :cond_0
    move v0, v1

    .line 265
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public start()V
    .locals 6

    .prologue
    .line 202
    iget-object v0, p0, Ldjz;->bAB:Lcng;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must provide ChunkReaderFactory or override start()"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 204
    iget-object v0, p0, Ldjz;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Ldka;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "runReadTask, reqId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Ldjz;->bbk:Legl;

    iget-wide v4, v4, Legl;->amT:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-direct {v1, p0, v2, v3, v4}, Ldka;-><init>(Ldjz;Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Ldjz;->bAD:Ljava/util/concurrent/Future;

    .line 212
    return-void

    .line 202
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 204
    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public abstract stop()V
.end method
