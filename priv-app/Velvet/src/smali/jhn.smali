.class public final Ljhn;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile elZ:[Ljhn;


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private agv:I

.field private ahL:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private ema:J

.field private emb:Ljava/lang/String;

.field private emc:D


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljhn;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhn;->ema:J

    const-string v0, ""

    iput-object v0, p0, Ljhn;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljhn;->emb:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljhn;->agv:I

    const-string v0, ""

    iput-object v0, p0, Ljhn;->ahL:Ljava/lang/String;

    iput-object v2, p0, Ljhn;->aeB:Ljbp;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhn;->emc:D

    iput-object v2, p0, Ljhn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhn;->eCz:I

    return-void
.end method

.method public static blY()[Ljhn;
    .locals 2

    sget-object v0, Ljhn;->elZ:[Ljhn;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljhn;->elZ:[Ljhn;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljhn;

    sput-object v0, Ljhn;->elZ:[Ljhn;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljhn;->elZ:[Ljhn;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljhn;->ema:J

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhn;->aiK:Ljava/lang/String;

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhn;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_4
    iput v0, p0, Ljhn;->agv:I

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhn;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhn;->ahL:Ljava/lang/String;

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljhn;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljhn;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljhn;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljhn;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhn;->emb:Ljava/lang/String;

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhn;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljhn;->emc:D

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljhn;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x39 -> :sswitch_8
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x3 -> :sswitch_4
        0x10 -> :sswitch_4
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v2, p0, Ljhn;->ema:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    :cond_0
    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljhn;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljhn;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_2
    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljhn;->ahL:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljhn;->aeB:Ljbp;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljhn;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_4
    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljhn;->emb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_5
    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-wide v2, p0, Ljhn;->emc:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final blZ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhn;->emb:Ljava/lang/String;

    return-object v0
.end method

.method public final bma()Z
    .locals 1

    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmb()D
    .locals 2

    iget-wide v0, p0, Ljhn;->emc:D

    return-wide v0
.end method

.method public final bmc()Z
    .locals 1

    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhn;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Ljhn;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljhn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Ljhn;->ema:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljhn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljhn;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljhn;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljhn;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljhn;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljhn;->ahL:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljhn;->aeB:Ljbp;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljhn;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljhn;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljhn;->emb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Ljhn;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-wide v2, p0, Ljhn;->emc:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_6
    return v0
.end method

.method public final oP()Z
    .locals 1

    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pW(I)Ljhn;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Ljhn;->agv:I

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhn;->aez:I

    return-object p0
.end method

.method public final pX()Z
    .locals 1

    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pc()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljhn;->ahL:Ljava/lang/String;

    return-object v0
.end method

.method public final pd()Z
    .locals 1

    iget v0, p0, Ljhn;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final vf(Ljava/lang/String;)Ljhn;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhn;->aiK:Ljava/lang/String;

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhn;->aez:I

    return-object p0
.end method

.method public final vg(Ljava/lang/String;)Ljhn;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljhn;->ahL:Ljava/lang/String;

    iget v0, p0, Ljhn;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljhn;->aez:I

    return-object p0
.end method
