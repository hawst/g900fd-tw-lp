.class public final Leyc;
.super Lexn;
.source "PG"


# instance fields
.field private final clb:Ljbp;

.field private final mCallback:Lefk;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lizj;Liwk;Ljbp;Lfcx;Lfaq;Lefk;Lemp;)V
    .locals 6
    .param p6    # Lfaq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lefk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 31
    move-object v0, p0

    move-object v1, p5

    move-object v2, p6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lexn;-><init>(Lfcx;Lfaq;Lizj;Liwk;Lemp;)V

    .line 32
    iput-object p7, p0, Leyc;->mCallback:Lefk;

    .line 33
    iput-object p1, p0, Leyc;->mContext:Landroid/content/Context;

    .line 34
    iput-object p4, p0, Leyc;->clb:Ljbp;

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(Liwk;J)Lizv;
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Leyc;->clb:Ljbp;

    if-eqz v0, :cond_0

    .line 40
    invoke-super {p0, p1, p2, p3}, Lexn;->a(Liwk;J)Lizv;

    move-result-object v0

    .line 41
    iget-object v1, p0, Leyc;->clb:Ljbp;

    iput-object v1, v0, Lizv;->dVH:Ljbp;

    .line 45
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lexn;->a(Liwk;J)Lizv;

    move-result-object v0

    goto :goto_0
.end method

.method protected final b(Ljeh;)V
    .locals 6

    .prologue
    const v0, 0x7f0a0147

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 51
    if-eqz p1, :cond_3

    .line 52
    iget-object v1, p1, Ljeh;->eeg:Liwm;

    if-eqz v1, :cond_2

    iget-object v1, p1, Ljeh;->eeg:Liwm;

    invoke-virtual {v1}, Liwm;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 55
    const/4 v1, 0x0

    invoke-super {p0, v1}, Lexn;->b(Ljeh;)V

    .line 56
    iget-object v1, p0, Leyc;->mContext:Landroid/content/Context;

    iget-object v2, p1, Ljeh;->eeg:Liwm;

    invoke-virtual {v2}, Liwm;->aZt()I

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_0

    const v0, 0x7f0a031f

    :cond_0
    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 62
    iget-object v0, p0, Leyc;->mCallback:Lefk;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Leyc;->mCallback:Lefk;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lefk;->ar(Ljava/lang/Object;)V

    .line 81
    :cond_1
    :goto_0
    return-void

    .line 66
    :cond_2
    invoke-super {p0, p1}, Lexn;->b(Ljeh;)V

    .line 67
    iget-object v0, p0, Leyc;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0320

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 69
    iget-object v0, p0, Leyc;->mCallback:Lefk;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Leyc;->mCallback:Lefk;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lefk;->ar(Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :cond_3
    invoke-super {p0, p1}, Lexn;->b(Ljeh;)V

    .line 75
    iget-object v1, p0, Leyc;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 77
    iget-object v0, p0, Leyc;->mCallback:Lefk;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Leyc;->mCallback:Lefk;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lefk;->ar(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Ljeh;

    invoke-virtual {p0, p1}, Leyc;->b(Ljeh;)V

    return-void
.end method
