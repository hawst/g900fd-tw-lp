.class public abstract Lguv;
.super Lgxi;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field bth:Lcom/google/android/velvet/ActionData;

.field final cWP:Lhgz;

.field final cWQ:Ljava/util/List;

.field final cWR:Leds;

.field private cWS:Lguw;

.field private cWT:Lhkb;

.field private cWU:Z

.field private cWV:Lgux;

.field final mActionCardEventLogger:Legs;

.field final mGsaConfigFlags:Lchk;

.field private final mVelvetFactory:Lgpu;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;Lgpu;Lhgz;Lchk;Legs;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lguv;->cWQ:Ljava/util/List;

    .line 110
    iput-object p3, p0, Lguv;->mVelvetFactory:Lgpu;

    .line 111
    iput-object p4, p0, Lguv;->cWP:Lhgz;

    .line 112
    new-instance v0, Leds;

    invoke-direct {v0}, Leds;-><init>()V

    iput-object v0, p0, Lguv;->cWR:Leds;

    .line 113
    iput-object p5, p0, Lguv;->mGsaConfigFlags:Lchk;

    .line 114
    iput-object p6, p0, Lguv;->mActionCardEventLogger:Legs;

    .line 115
    return-void
.end method

.method public static E(Lcom/google/android/search/shared/actions/VoiceAction;)Ljse;
    .locals 10
    .param p0    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 252
    instance-of v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-nez v0, :cond_0

    .line 260
    :goto_0
    return-object v1

    .line 255
    :cond_0
    check-cast p0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 256
    new-instance v2, Ljse;

    invoke-direct {v2}, Ljse;-><init>()V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agy()Ldxl;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    if-nez v0, :cond_4

    :cond_1
    move-object v0, v1

    :goto_2
    iput-object v0, v2, Ljse;->eCc:[Ljsg;

    .line 259
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajh()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_2
    move-object v0, v1

    :goto_3
    iput-object v0, v2, Ljse;->eCd:[Ljsf;

    move-object v1, v2

    .line 260
    goto :goto_0

    :cond_3
    move v0, v4

    .line 258
    goto :goto_1

    :cond_4
    invoke-virtual {v5}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    new-instance v6, Ljsg;

    invoke-direct {v6}, Ljsg;-><init>()V

    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljsg;->dH(J)Ljsg;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljsg;->yF(Ljava/lang/String;)Ljsg;

    :cond_7
    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->KK()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    invoke-virtual {v6, v0}, Ljsg;->yJ(Ljava/lang/String;)Ljsg;

    :cond_8
    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->akB()Ldzb;

    move-result-object v5

    sget-object v7, Ldzb;->bRq:Ldzb;

    if-ne v5, v7, :cond_b

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljsg;->yH(Ljava/lang/String;)Ljsg;

    :cond_9
    :goto_4
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljsg;->yG(Ljava/lang/String;)Ljsg;

    :cond_a
    new-array v0, v3, [Ljsg;

    aput-object v6, v0, v4

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->akB()Ldzb;

    move-result-object v5

    sget-object v7, Ldzb;->bRp:Ldzb;

    if-ne v5, v7, :cond_9

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljsg;->yI(Ljava/lang/String;)Ljsg;

    goto :goto_4

    .line 259
    :cond_c
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    new-instance v4, Ljsf;

    invoke-direct {v4}, Ljsf;-><init>()V

    invoke-virtual {v4, v0}, Ljsf;->yE(Ljava/lang/String;)Ljsf;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_e
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljsf;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljsf;

    goto/16 :goto_3
.end method

.method public static a(Ldbd;)I
    .locals 1
    .param p0    # Ldbd;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 232
    if-nez p0, :cond_0

    .line 233
    const/4 v0, 0x0

    .line 241
    :goto_0
    return v0

    .line 235
    :cond_0
    invoke-virtual {p0}, Ldbd;->VJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    const/4 v0, 0x2

    goto :goto_0

    .line 238
    :cond_1
    invoke-virtual {p0}, Ldbd;->VU()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 239
    const/4 v0, 0x3

    goto :goto_0

    .line 241
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/shared/search/Query;Ldbd;)Legu;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 981
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1015
    :cond_0
    :goto_0
    return-object v0

    .line 984
    :cond_1
    invoke-virtual {p1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v8

    .line 985
    if-eqz v8, :cond_0

    .line 988
    invoke-virtual {p1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v12

    .line 989
    invoke-virtual {p1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v6

    .line 992
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v0

    long-to-int v7, v0

    .line 994
    :goto_1
    invoke-static {p1}, Lguv;->a(Ldbd;)I

    move-result v10

    .line 995
    invoke-static {v12}, Lguv;->E(Lcom/google/android/search/shared/actions/VoiceAction;)Ljse;

    move-result-object v11

    .line 997
    invoke-virtual {v8, v9}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v8, v9}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v0

    sget-object v1, Ljrc;->ezW:Ljsm;

    invoke-virtual {v0, v1}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v9, 0x1

    .line 1000
    :cond_2
    new-instance v0, Legu;

    invoke-virtual {v8}, Lcom/google/android/velvet/ActionData;->aoM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqK()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alk()I

    move-result v6

    invoke-virtual {v8}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v8

    invoke-direct/range {v0 .. v11}, Legu;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;IIIZILjse;)V

    .line 1012
    if-eqz v12, :cond_0

    .line 1013
    invoke-interface {v12, v0}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Legu;)V

    goto :goto_0

    .line 992
    :cond_3
    const/4 v7, -0x1

    goto :goto_1
.end method

.method private a(Ldda;Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;)Lguy;
    .locals 6

    .prologue
    .line 618
    iget-object v0, p0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    if-nez v0, :cond_3

    .line 621
    instance-of v0, p3, Lcom/google/android/search/shared/actions/errors/SearchError;

    if-nez v0, :cond_0

    instance-of v0, p3, Lcom/google/android/search/shared/actions/PuntAction;

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 622
    sget-object v4, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    .line 627
    :goto_1
    iget-object v0, p0, Lguv;->mVelvetFactory:Lgpu;

    invoke-virtual {p0}, Lguv;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->wr()Leoj;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lgpu;->a(Lgxi;Ldda;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Leoj;)Lhhe;

    move-result-object v1

    .line 630
    iget-object v0, p0, Lguv;->cWT:Lhkb;

    if-nez v0, :cond_1

    iget-object v0, p0, Lguv;->mVelvetFactory:Lgpu;

    invoke-virtual {p0}, Lguv;->aLH()Lgyz;

    move-result-object v2

    invoke-virtual {v2}, Lgyz;->wr()Leoj;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgpu;->a(Leoj;)Lhkb;

    move-result-object v0

    iput-object v0, p0, Lguv;->cWT:Lhkb;

    :cond_1
    iget-object v0, p0, Lguv;->cWT:Lhkb;

    invoke-virtual {v0, p3, v1}, Lhkb;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ledr;)Lhjo;

    move-result-object v2

    .line 632
    if-nez v2, :cond_4

    .line 634
    const/4 v0, 0x0

    .line 639
    :goto_2
    return-object v0

    .line 621
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 624
    :cond_3
    iget-object v4, p0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    goto :goto_1

    .line 637
    :cond_4
    invoke-virtual {v2, p3}, Lhjo;->b(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 639
    new-instance v0, Lguy;

    invoke-direct {v0, v1, v2}, Lguy;-><init>(Lhhe;Lhjo;)V

    goto :goto_2
.end method

.method private static a(Lguy;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lguy;->cXh:Lhjo;

    invoke-virtual {v0}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/search/shared/actions/ButtonAction;

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lguy;->cXg:Lhhe;

    invoke-virtual {v0, p1, p2}, Lhhe;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    .line 647
    :cond_0
    return-void
.end method

.method private a(Ldda;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 346
    invoke-virtual {p1}, Ldda;->aaj()Ldbd;

    move-result-object v3

    .line 351
    invoke-virtual {v3}, Ldbd;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lguv;->cWR:Leds;

    invoke-virtual {v0, v2}, Leds;->eM(Z)Z

    move-result v0

    .line 383
    :cond_0
    :goto_0
    return v0

    .line 355
    :cond_1
    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 356
    invoke-virtual {p1}, Ldda;->aak()Ldcw;

    move-result-object v4

    .line 358
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Ldcw;->isDone()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 363
    :goto_1
    if-nez p2, :cond_2

    if-eqz v0, :cond_2

    iget-object v4, p0, Lguv;->cWR:Leds;

    .line 364
    :cond_2
    invoke-virtual {v3}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v4

    .line 368
    invoke-virtual {v3, v4}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    .line 370
    invoke-virtual {p1}, Ldda;->aaj()Ldbd;

    move-result-object v5

    invoke-virtual {v5}, Ldbd;->VG()Z

    move-result v5

    invoke-static {v4, v3, v5}, Leds;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;Z)I

    move-result v4

    if-eq v4, v1, :cond_4

    .line 373
    iget-object v0, p0, Lguv;->cWR:Leds;

    invoke-virtual {v0, v2}, Leds;->eM(Z)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 358
    goto :goto_1

    .line 376
    :cond_4
    iget-object v2, p0, Lguv;->cWR:Leds;

    invoke-virtual {v2, v1}, Leds;->eM(Z)Z

    move-result v2

    .line 377
    iget-object v4, p0, Lguv;->cWR:Leds;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Leds;->v(Ljava/lang/CharSequence;)Z

    move-result v3

    or-int/2addr v2, v3

    .line 378
    iget-object v3, p0, Lguv;->cWR:Leds;

    invoke-virtual {v3, v0}, Leds;->eL(Z)Z

    move-result v0

    or-int/2addr v0, v2

    .line 379
    if-eqz p2, :cond_0

    .line 380
    iget-object v2, p0, Lguv;->cWR:Leds;

    iput-boolean v1, v2, Leds;->bbz:Z

    iput-boolean v1, v2, Leds;->bVT:Z

    or-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected static aKG()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method private aKH()V
    .locals 2

    .prologue
    .line 680
    iget-object v0, p0, Lguv;->cWV:Lgux;

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lguv;->cWV:Lgux;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lgux;->buZ:Z

    .line 683
    :cond_0
    return-void
.end method

.method private aKI()V
    .locals 2

    .prologue
    .line 694
    invoke-direct {p0}, Lguv;->aKH()V

    .line 696
    new-instance v0, Lgux;

    iget-object v1, p0, Lguv;->cWS:Lguw;

    invoke-direct {v0, p0, v1}, Lgux;-><init>(Lguv;Lguw;)V

    iput-object v0, p0, Lguv;->cWV:Lgux;

    .line 697
    iget-object v0, p0, Lguv;->cWV:Lgux;

    invoke-virtual {p0, v0}, Lguv;->a(Lesj;)V

    .line 698
    return-void
.end method


# virtual methods
.method public a(Lddb;)V
    .locals 21

    .prologue
    .line 175
    invoke-virtual/range {p0 .. p0}, Lguv;->aKE()Z

    move-result v2

    if-nez v2, :cond_1

    .line 224
    :cond_0
    return-void

    .line 178
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lddb;->Nf()Ldda;

    move-result-object v9

    .line 182
    invoke-virtual/range {p1 .. p1}, Lddb;->aax()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {p1 .. p1}, Lddb;->aaz()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 183
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lguv;->cWU:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3}, Lguv;->a(Ldda;Z)Z

    move-result v3

    or-int/2addr v2, v3

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lguv;->cWU:Z

    .line 186
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lddb;->aax()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 187
    invoke-virtual {v9}, Ldda;->aaj()Ldbd;

    move-result-object v2

    .line 188
    invoke-virtual {v9}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v10

    .line 189
    invoke-virtual {v2}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    .line 190
    move-object/from16 v0, p0

    iget-object v3, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    if-eqz v3, :cond_6

    invoke-virtual {v9}, Ldda;->aaj()Ldbd;

    move-result-object v3

    invoke-virtual {v3}, Ldbd;->VH()Z

    move-result v3

    if-nez v3, :cond_6

    .line 192
    new-instance v3, Lguw;

    invoke-static {v10, v2}, Lguv;->a(Lcom/google/android/shared/search/Query;Ldbd;)Legu;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    invoke-direct {v3, v4, v5}, Lguw;-><init>(Legu;Lcom/google/android/velvet/ActionData;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lguv;->cWS:Lguw;

    .line 198
    :goto_0
    invoke-virtual {v2}, Ldbd;->VO()Ljava/util/List;

    move-result-object v11

    .line 201
    if-eqz v11, :cond_7

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_0

    .line 206
    if-eqz v11, :cond_4

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    invoke-interface {v11, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/actions/VoiceAction;

    instance-of v3, v2, Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lguv;->cWQ:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lguy;

    iget-object v3, v3, Lguy;->cXh:Lhjo;

    invoke-virtual {v3}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v3

    if-eq v3, v2, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10, v2}, Lguv;->a(Ldda;Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;)Lguy;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v3, v2, Lguy;->cXh:Lhjo;

    invoke-virtual {v3}, Lhjo;->start()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lguv;->cWQ:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lguv;->cWU:Z

    .line 208
    :cond_4
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v12

    invoke-static {v11}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    invoke-virtual {v9}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v14

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    invoke-virtual {v2}, Lguy;->aKL()Z

    move-result v16

    if-nez v16, :cond_5

    iget-object v2, v2, Lguy;->cXh:Lhjo;

    invoke-virtual {v2}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    invoke-interface {v15, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 196
    :cond_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lguv;->cWS:Lguw;

    goto/16 :goto_0

    .line 201
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    invoke-virtual {v2}, Lguy;->aKL()Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lguv;->cWU:Z

    const/4 v4, 0x2

    iput v4, v2, Lguy;->ag:I

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lguv;->cWU:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lguv;->cWU:Z

    invoke-direct/range {p0 .. p0}, Lguv;->aKI()V

    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 208
    :cond_b
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    :cond_c
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v11, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_d
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    :goto_5
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lguy;->aKL()Z

    move-result v8

    if-nez v8, :cond_d

    :cond_e
    if-eqz v7, :cond_21

    if-eqz v2, :cond_16

    iget-object v0, v2, Lguy;->cXh:Lhjo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v8

    if-ne v8, v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    invoke-static {v2, v10, v3}, Lguv;->a(Lguy;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    invoke-virtual {v9}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->Vy()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v9}, Ldda;->aaj()Ldbd;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v8}, Lhjo;->b(Ldbd;Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_4

    :cond_f
    const/4 v2, 0x0

    goto :goto_5

    :cond_10
    invoke-interface {v15, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_12

    move-object/from16 v20, v2

    move v2, v6

    move-object/from16 v6, v20

    :goto_6
    iget-object v8, v6, Lguy;->cXh:Lhjo;

    invoke-virtual {v8}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v8

    if-eq v8, v3, :cond_11

    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_11

    const/4 v8, 0x1

    const/4 v2, 0x2

    iput v2, v6, Lguy;->ag:I

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    move-object v6, v2

    move v2, v8

    goto :goto_6

    :cond_11
    iget-object v8, v6, Lguy;->cXh:Lhjo;

    invoke-virtual {v8}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v8

    if-ne v8, v3, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    invoke-static {v6, v10, v3}, Lguv;->a(Lguy;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    move v6, v2

    goto/16 :goto_4

    :cond_12
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v18

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_14

    invoke-interface {v8}, Lcom/google/android/search/shared/actions/VoiceAction;->agw()Z

    move-result v8

    if-eqz v8, :cond_13

    const/4 v8, 0x1

    :goto_7
    if-eqz v8, :cond_16

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lhjo;->b(Lcom/google/android/search/shared/actions/VoiceAction;)V

    iget-object v2, v2, Lguy;->cXg:Lhhe;

    move-object/from16 v0, p0

    iget-object v3, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v2, v10, v3}, Lhhe;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    invoke-virtual/range {v17 .. v17}, Lhjo;->start()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lguv;->cWU:Z

    goto/16 :goto_4

    :cond_13
    instance-of v8, v3, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-eqz v8, :cond_14

    const/4 v8, 0x1

    goto :goto_7

    :cond_14
    const/4 v8, 0x0

    goto :goto_7

    :cond_15
    move-object/from16 v20, v6

    move v6, v2

    move-object/from16 v2, v20

    :cond_16
    const/4 v7, 0x0

    move v4, v6

    move v6, v7

    :goto_8
    if-eqz v2, :cond_17

    const/4 v4, 0x1

    const/4 v7, 0x2

    iput v7, v2, Lguy;->ag:I

    :cond_17
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v10, v3}, Lguv;->a(Ldda;Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;)Lguy;

    move-result-object v7

    if-eqz v7, :cond_1e

    iget-object v2, v7, Lguy;->cXh:Lhjo;

    invoke-virtual {v2}, Lhjo;->start()V

    if-ne v3, v14, :cond_20

    const/4 v3, 0x1

    :goto_9
    const/4 v2, 0x1

    invoke-interface {v12, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v3

    move v7, v6

    move v6, v4

    move v4, v2

    goto/16 :goto_4

    :cond_18
    :goto_a
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v6, 0x1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    const/4 v3, 0x2

    iput v3, v2, Lguy;->ag:I

    goto :goto_a

    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v5, :cond_1a

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2}, Lguv;->a(Ldda;Z)Z

    :cond_1a
    if-nez v4, :cond_1b

    if-eqz v6, :cond_1e

    :cond_1b
    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_1c

    .line 209
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lguv;->cWU:Z

    .line 213
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lguv;->cWU:Z

    if-eqz v2, :cond_1d

    .line 214
    invoke-virtual {v9}, Ldda;->aan()Ldcy;

    move-result-object v2

    invoke-virtual {v2}, Ldcy;->ZB()Z

    move-result v2

    .line 217
    if-eqz v2, :cond_1d

    .line 218
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lguv;->cWU:Z

    .line 219
    invoke-direct/range {p0 .. p0}, Lguv;->aKI()V

    .line 223
    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lguv;->Nf()Ldda;

    move-result-object v2

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->yo()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ldbd;->Vu()Z

    move-result v3

    if-eqz v3, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    iget-object v2, v2, Lguy;->cXh:Lhjo;

    invoke-virtual {v2}, Lhjo;->anO()Z

    goto :goto_c

    .line 208
    :cond_1e
    const/4 v2, 0x0

    goto :goto_b

    .line 223
    :cond_1f
    invoke-virtual {v2}, Ldbd;->Vv()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lguy;

    iget-object v2, v2, Lguy;->cXh:Lhjo;

    invoke-virtual {v2}, Lhjo;->aQc()V

    goto :goto_d

    :cond_20
    move v3, v5

    goto/16 :goto_9

    :cond_21
    move v4, v6

    move v6, v7

    goto/16 :goto_8
.end method

.method public final a(Lejw;)V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 128
    instance-of v2, v0, Lhjs;

    if-eqz v2, :cond_0

    .line 129
    check-cast v0, Lhjs;

    invoke-virtual {v0}, Lhjs;->aQe()V

    goto :goto_0

    .line 132
    :cond_1
    return-void
.end method

.method public a(Letj;)V
    .locals 1

    .prologue
    .line 921
    invoke-super {p0, p1}, Lgxi;->a(Letj;)V

    .line 922
    const-string v0, "AbstractActionCardsPresenter"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 923
    iget-object v0, p0, Lguv;->cWQ:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lu(Ljava/lang/String;)V

    .line 924
    return-void
.end method

.method protected aKE()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method protected aKF()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lguv;->aKH()V

    .line 145
    return-void
.end method

.method protected bB(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 166
    return-void
.end method

.method public final cN()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 928
    invoke-virtual {p0}, Lguv;->aKE()Z

    move-result v1

    if-nez v1, :cond_0

    .line 937
    :goto_0
    return v0

    .line 932
    :cond_0
    iget-object v1, p0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguy;

    .line 933
    iget-object v3, v0, Lguy;->cXh:Lhjo;

    invoke-virtual {v3}, Lhjo;->aAU()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 934
    iget-object v0, v0, Lguy;->cXh:Lhjo;

    invoke-virtual {v0}, Lhjo;->cN()Z

    move-result v0

    or-int/2addr v0, v1

    :goto_2
    move v1, v0

    .line 936
    goto :goto_1

    :cond_1
    move v0, v1

    .line 937
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public fT(Z)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->wj()Lekf;

    move-result-object v0

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    if-lez v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lguv;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->aaa()V

    .line 139
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 651
    invoke-virtual {p0}, Lguv;->aKE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    :goto_0
    return-void

    .line 656
    :cond_0
    invoke-virtual {p0}, Lguv;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v1

    iget-object v0, p0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Ldbd;->p(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lguv;->cWQ:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguy;

    iget-object v0, v0, Lguy;->cXh:Lhjo;

    invoke-virtual {v0}, Lhjo;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    goto :goto_1
.end method
