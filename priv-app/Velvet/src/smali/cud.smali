.class public final Lcud;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final biR:Lcue;

.field final biS:Lcuf;

.field final biT:Lcug;

.field final biU:Lcuh;

.field biV:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467
    new-instance v0, Lcue;

    invoke-direct {v0}, Lcue;-><init>()V

    iput-object v0, p0, Lcud;->biR:Lcue;

    .line 468
    new-instance v0, Lcuf;

    invoke-direct {v0}, Lcuf;-><init>()V

    iput-object v0, p0, Lcud;->biS:Lcuf;

    .line 469
    new-instance v0, Lcug;

    invoke-direct {v0}, Lcug;-><init>()V

    iput-object v0, p0, Lcud;->biT:Lcug;

    .line 470
    new-instance v0, Lcuh;

    invoke-direct {v0}, Lcuh;-><init>()V

    iput-object v0, p0, Lcud;->biU:Lcuh;

    .line 471
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcud;->biV:J

    .line 472
    return-void
.end method


# virtual methods
.method public final SX()Lctp;
    .locals 18

    .prologue
    .line 545
    new-instance v17, Lctp;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcud;->biR:Lcue;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-wide v2, v0, Lcue;->biV:J

    move-object/from16 v0, v16

    iget-object v4, v0, Lcue;->bjd:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcue;->bje:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcue;->bjf:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcue;->biW:Ljava/lang/StringBuilder;

    invoke-static {v7}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v16

    iget-wide v8, v0, Lcue;->bjh:J

    move-object/from16 v0, v16

    iget-object v10, v0, Lcue;->biX:Ljava/lang/StringBuilder;

    invoke-static {v10}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v16

    iget-object v11, v0, Lcue;->biZ:Ljava/lang/StringBuilder;

    invoke-static {v11}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v16

    iget-object v12, v0, Lcue;->bja:Ljava/lang/StringBuilder;

    invoke-static {v12}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v16

    iget-object v13, v0, Lcue;->bjb:Ljava/lang/StringBuilder;

    invoke-static {v13}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v16

    iget-object v14, v0, Lcue;->biY:Ljava/lang/StringBuilder;

    invoke-static {v14}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v16

    iget-object v15, v0, Lcue;->bjc:Ljava/lang/StringBuilder;

    invoke-static {v15}, Lcue;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    iget-object v0, v0, Lcue;->bjg:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v2 .. v16}, Lcto;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcud;->biS:Lcuf;

    new-instance v4, Ljava/util/HashMap;

    iget-object v3, v3, Lcuf;->bji:Ljava/util/Map;

    invoke-direct {v4, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcud;->biT:Lcug;

    new-instance v5, Ljava/util/HashMap;

    iget-object v3, v3, Lcug;->bji:Ljava/util/Map;

    invoke-direct {v5, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcud;->biU:Lcuh;

    new-instance v6, Ljava/util/HashMap;

    iget-object v3, v3, Lcuh;->bji:Ljava/util/Map;

    invoke-direct {v6, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v4, v5, v6}, Lctp;-><init>(Landroid/content/ContentValues;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v17
.end method

.method public final clear()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 475
    iget-object v0, p0, Lcud;->biR:Lcue;

    iget-object v1, v0, Lcue;->biW:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, v0, Lcue;->biX:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, v0, Lcue;->biY:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, v0, Lcue;->biZ:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, v0, Lcue;->bja:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, v0, Lcue;->bjb:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, v0, Lcue;->bjc:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    iput-wide v4, v0, Lcue;->biV:J

    iput-object v3, v0, Lcue;->bjd:Ljava/lang/String;

    iput-object v3, v0, Lcue;->bjf:Ljava/lang/String;

    iput-object v3, v0, Lcue;->bje:Ljava/lang/String;

    iput-wide v4, v0, Lcue;->bjh:J

    .line 476
    iget-object v0, p0, Lcud;->biS:Lcuf;

    iget-object v0, v0, Lcuf;->bji:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 477
    iget-object v0, p0, Lcud;->biT:Lcug;

    iget-object v0, v0, Lcug;->bji:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 478
    iget-object v0, p0, Lcud;->biU:Lcuh;

    iget-object v0, v0, Lcuh;->bji:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 479
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcud;->biV:J

    .line 480
    return-void
.end method
