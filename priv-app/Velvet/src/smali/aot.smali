.class public final Laot;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private akk:I

.field private akl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15368
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15369
    iput v0, p0, Laot;->aez:I

    iput v0, p0, Laot;->akk:I

    const-string v0, ""

    iput-object v0, p0, Laot;->akl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laot;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laot;->eCz:I

    .line 15370
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 15308
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laot;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Laot;->akk:I

    iget v0, p0, Laot;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laot;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laot;->akl:Ljava/lang/String;

    iget v0, p0, Laot;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laot;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 15384
    iget v0, p0, Laot;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15385
    const/4 v0, 0x1

    iget v1, p0, Laot;->akk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 15387
    :cond_0
    iget v0, p0, Laot;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15388
    const/4 v0, 0x2

    iget-object v1, p0, Laot;->akl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15390
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15391
    return-void
.end method

.method public final cK(I)Laot;
    .locals 1

    .prologue
    .line 15333
    iput p1, p0, Laot;->akk:I

    .line 15334
    iget v0, p0, Laot;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laot;->aez:I

    .line 15335
    return-object p0
.end method

.method public final cr(Ljava/lang/String;)Laot;
    .locals 1

    .prologue
    .line 15352
    if-nez p1, :cond_0

    .line 15353
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15355
    :cond_0
    iput-object p1, p0, Laot;->akl:Ljava/lang/String;

    .line 15356
    iget v0, p0, Laot;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laot;->aez:I

    .line 15357
    return-object p0
.end method

.method public final getIconResId()I
    .locals 1

    .prologue
    .line 15330
    iget v0, p0, Laot;->akk:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 15395
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 15396
    iget v1, p0, Laot;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15397
    const/4 v1, 0x1

    iget v2, p0, Laot;->akk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 15400
    :cond_0
    iget v1, p0, Laot;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15401
    const/4 v1, 0x2

    iget-object v2, p0, Laot;->akl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15404
    :cond_1
    return v0
.end method

.method public final rw()Z
    .locals 1

    .prologue
    .line 15338
    iget v0, p0, Laot;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15349
    iget-object v0, p0, Laot;->akl:Ljava/lang/String;

    return-object v0
.end method

.method public final ry()Z
    .locals 1

    .prologue
    .line 15360
    iget v0, p0, Laot;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
