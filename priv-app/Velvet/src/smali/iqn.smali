.class final Liqn;
.super Liki;
.source "PG"


# instance fields
.field private final transient dGn:Lijj;


# direct methods
.method constructor <init>(Lijj;Ljava/util/Comparator;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p2}, Liki;-><init>(Ljava/util/Comparator;)V

    .line 54
    iput-object p1, p0, Liqn;->dGn:Lijj;

    .line 55
    invoke-virtual {p1}, Lijj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 56
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bp(II)Liki;
    .locals 3

    .prologue
    .line 241
    if-nez p1, :cond_0

    invoke-virtual {p0}, Liqn;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 247
    :goto_0
    return-object p0

    .line 243
    :cond_0
    if-ge p1, p2, :cond_1

    .line 244
    new-instance v0, Liqn;

    iget-object v1, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v1, p1, p2}, Lijj;->bm(II)Lijj;

    move-result-object v1

    iget-object v2, p0, Liqn;->dDm:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Liqn;-><init>(Lijj;Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0

    .line 247
    :cond_1
    iget-object v0, p0, Liqn;->dDm:Ljava/util/Comparator;

    invoke-static {v0}, Liqn;->a(Ljava/util/Comparator;)Liki;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/Object;ZLjava/lang/Object;Z)Liki;
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0, p1, p2}, Liqn;->m(Ljava/lang/Object;Z)Liki;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Liki;->l(Ljava/lang/Object;Z)Liki;

    move-result-object v0

    return-object v0
.end method

.method final aWH()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v0}, Lijj;->aWH()Z

    move-result v0

    return v0
.end method

.method public final aWI()Lirv;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v0}, Lijj;->aWI()Lirv;

    move-result-object v0

    return-object v0
.end method

.method final aWV()Lijj;
    .locals 2

    .prologue
    .line 273
    new-instance v0, Lijv;

    iget-object v1, p0, Liqn;->dGn:Lijj;

    invoke-direct {v0, p0, v1}, Lijv;-><init>(Liki;Lijj;)V

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    if-nez p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    :try_start_0
    iget-object v1, p0, Liqn;->dDm:Ljava/util/Comparator;

    iget-object v2, p0, Liqn;->dGn:Lijj;

    invoke-static {v2, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 78
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-virtual {p0}, Liqn;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v2, p1}, Lire;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-gt v2, v0, :cond_2

    .line 89
    :cond_0
    invoke-super {p0, p1}, Liki;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 125
    :cond_1
    :goto_0
    return v0

    .line 96
    :cond_2
    iget-object v2, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v2}, Lijj;->aWI()Lirv;

    move-result-object v3

    .line 97
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 98
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 102
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 104
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v5, v2}, Liqn;->w(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    .line 106
    if-nez v5, :cond_4

    .line 108
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 113
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_1

    .line 115
    :cond_4
    if-lez v5, :cond_3

    move v0, v1

    .line 116
    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 122
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 125
    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 155
    if-ne p1, p0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    instance-of v2, p1, Ljava/util/Set;

    if-nez v2, :cond_2

    move v0, v1

    .line 159
    goto :goto_0

    .line 162
    :cond_2
    check-cast p1, Ljava/util/Set;

    .line 163
    invoke-virtual {p0}, Liqn;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 164
    goto :goto_0

    .line 167
    :cond_3
    iget-object v2, p0, Liqn;->dDm:Ljava/util/Comparator;

    invoke-static {v2, p1}, Lire;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 168
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 170
    :try_start_0
    iget-object v3, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v3}, Lijj;->aWI()Lirv;

    move-result-object v3

    .line 171
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 172
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 173
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 174
    if-eqz v5, :cond_5

    invoke-virtual {p0, v4, v5}, Liqn;->w(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-eqz v4, :cond_4

    :cond_5
    move v0, v1

    .line 176
    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 183
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 186
    :cond_6
    invoke-virtual {p0, p1}, Liqn;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method

.method public final first()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Liqn;->dGn:Lijj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final indexOf(Ljava/lang/Object;)I
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 253
    if-nez p1, :cond_1

    .line 268
    :cond_0
    :goto_0
    return v0

    .line 258
    :cond_1
    :try_start_0
    iget-object v1, p0, Liqn;->dGn:Lijj;

    invoke-virtual {p0}, Liqn;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lirk;->dGE:Lirk;

    sget-object v4, Lirg;->dGC:Lirg;

    invoke-static {v1, p1, v2, v3, v4}, Lirf;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lirk;Lirg;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 268
    if-ltz v1, :cond_0

    iget-object v2, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v2, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 261
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v0}, Lijj;->aWI()Lirv;

    move-result-object v0

    return-object v0
.end method

.method final l(Ljava/lang/Object;Z)Liki;
    .locals 5

    .prologue
    .line 202
    if-eqz p2, :cond_0

    .line 203
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Liqn;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lirk;->dGH:Lirk;

    sget-object v4, Lirg;->dGB:Lirg;

    invoke-static {v0, v1, v2, v3, v4}, Lirf;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lirk;Lirg;)I

    move-result v0

    .line 209
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Liqn;->bp(II)Liki;

    move-result-object v0

    return-object v0

    .line 206
    :cond_0
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Liqn;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lirk;->dGG:Lirk;

    sget-object v4, Lirg;->dGB:Lirg;

    invoke-static {v0, v1, v2, v3, v4}, Lirf;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lirk;Lirg;)I

    move-result v0

    goto :goto_0
.end method

.method public final last()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {p0}, Liqn;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final m(Ljava/lang/Object;Z)Liki;
    .locals 5

    .prologue
    .line 222
    if-eqz p2, :cond_0

    .line 223
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Liqn;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lirk;->dGG:Lirk;

    sget-object v4, Lirg;->dGB:Lirg;

    invoke-static {v0, v1, v2, v3, v4}, Lirf;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lirk;Lirg;)I

    move-result v0

    .line 229
    :goto_0
    invoke-virtual {p0}, Liqn;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Liqn;->bp(II)Liki;

    move-result-object v0

    return-object v0

    .line 226
    :cond_0
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Liqn;->comparator()Ljava/util/Comparator;

    move-result-object v2

    sget-object v3, Lirk;->dGH:Lirk;

    sget-object v4, Lirg;->dGB:Lirg;

    invoke-static {v0, v1, v2, v3, v4}, Lirf;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lirk;Lirg;)I

    move-result v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v0}, Lijj;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Liqn;->dGn:Lijj;

    invoke-virtual {v0, p1}, Lijj;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
