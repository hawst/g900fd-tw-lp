.class public final Lfzi;
.super Lfrk;
.source "PG"


# static fields
.field private static final cDH:Landroid/net/Uri;


# instance fields
.field private cDI:Ljava/lang/String;

.field private cDJ:Ljava/lang/String;

.field private cDK:Lfzk;

.field private final mRunner:Lerp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "http://translate.google.com/m/translate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lfzi;->cDH:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lerp;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lfrk;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 46
    iput-object p4, p0, Lfzi;->mRunner:Lerp;

    .line 47
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lfzi;->cDK:Lfzk;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lfzi;->cDK:Lfzk;

    invoke-virtual {v0}, Lfzk;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 100
    iget-object v0, p0, Lfzi;->cDK:Lfzk;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfzk;->cancel(Z)Z

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lfzi;->cDK:Lfzk;

    .line 104
    :cond_0
    new-instance v0, Lfzk;

    iget-object v1, p0, Lfzi;->mCardContainer:Lfmt;

    invoke-direct {v0, v1, p2, p3, p4}, Lfzk;-><init>(Lfmt;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lfzi;->cDK:Lfzk;

    .line 106
    iget-object v0, p0, Lfzi;->mRunner:Lerp;

    new-instance v1, Lfzl;

    iget-object v2, p0, Lfzi;->cDK:Lfzk;

    invoke-direct {v1, p1, v2}, Lfzl;-><init>(Ljava/lang/String;Lfzk;)V

    const-wide/16 v2, 0x12c

    invoke-interface {v0, v1, v2, v3}, Lerp;->a(Lesk;J)V

    .line 109
    return-void
.end method

.method static synthetic access$000()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lfzi;->cDH:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lfzi;->cDI:Ljava/lang/String;

    iget-object v1, p0, Lfzi;->cDJ:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, v1}, Lfzi;->a(Ljava/lang/String;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method protected final aCZ()V
    .locals 10

    .prologue
    .line 56
    invoke-super {p0}, Lfrk;->aCZ()V

    .line 58
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agV:Lapk;

    .line 59
    invoke-virtual {v0}, Lapk;->sR()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfzi;->cDI:Ljava/lang/String;

    .line 60
    invoke-virtual {v0}, Lapk;->sS()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzi;->cDJ:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 65
    const v0, 0x7f1102eb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    const v2, 0x7f0200ed

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 67
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 68
    const v0, 0x7f1101a9

    iget-object v2, p0, Lfzi;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0309

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 73
    const v0, 0x7f11035c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 74
    const v0, 0x7f1102f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 75
    iget-object v8, p0, Lfzi;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfzi;->mCardContainer:Lfmt;

    iget-object v6, p0, Lfzi;->cDI:Ljava/lang/String;

    iget-object v7, p0, Lfzi;->cDJ:Ljava/lang/String;

    new-instance v0, Lfzj;

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v3, v1, Lanh;->ahu:Lizj;

    const/16 v4, 0x69

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lfzj;-><init>(Lfzi;Lfmt;Lizj;ILandroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    return-void
.end method

.method protected final aDa()Lank;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agV:Lapk;

    iget-object v0, v0, Lapk;->aie:Lank;

    return-object v0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 51
    const v0, 0x7f04013c

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/String;Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lfzi;->cDJ:Ljava/lang/String;

    iget-object v1, p0, Lfzi;->cDI:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, v1}, Lfzi;->a(Ljava/lang/String;Landroid/widget/EditText;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method
