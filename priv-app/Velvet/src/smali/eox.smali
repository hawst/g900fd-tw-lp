.class public final Leox;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/CharSequence;


# instance fields
.field public final bgm:I

.field private final cgQ:[C

.field public final fi:I


# direct methods
.method public constructor <init>([CII)V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-object p1, p0, Leox;->cgQ:[C

    .line 156
    iput p2, p0, Leox;->bgm:I

    .line 157
    iput p3, p0, Leox;->fi:I

    .line 158
    return-void
.end method

.method private ij(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 194
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Leox;->cgQ:[C

    iget v2, p0, Leox;->bgm:I

    add-int/2addr v2, p1

    invoke-virtual {p0}, Leox;->length()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method


# virtual methods
.method public final a(Leox;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 172
    invoke-virtual {p0}, Leox;->length()I

    move-result v2

    .line 173
    invoke-virtual {p1}, Leox;->length()I

    move-result v1

    if-le v2, v1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return v0

    .line 174
    :cond_1
    iget v3, p0, Leox;->bgm:I

    .line 175
    iget v4, p1, Leox;->bgm:I

    .line 176
    iget-object v5, p0, Leox;->cgQ:[C

    .line 177
    iget-object v6, p1, Leox;->cgQ:[C

    move v1, v0

    .line 178
    :goto_1
    if-ge v1, v2, :cond_2

    .line 179
    add-int v7, v3, v1

    aget-char v7, v5, v7

    invoke-static {v7}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v7

    add-int v8, v4, v1

    aget-char v8, v6, v8

    invoke-static {v8}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v8

    if-ne v7, v8, :cond_0

    .line 178
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 184
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final charAt(I)C
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Leox;->cgQ:[C

    iget v1, p0, Leox;->bgm:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public final length()I
    .locals 2

    .prologue
    .line 162
    iget v0, p0, Leox;->fi:I

    iget v1, p0, Leox;->bgm:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final synthetic subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1}, Leox;->ij(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    invoke-virtual {p0}, Leox;->length()I

    invoke-direct {p0, v0}, Leox;->ij(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
