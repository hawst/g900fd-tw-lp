.class public final Ljbm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dYK:[Ljbm;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47090
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 47091
    const/4 v0, 0x0

    iput v0, p0, Ljbm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbm;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbm;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljbm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbm;->eCz:I

    .line 47092
    return-void
.end method

.method public static beY()[Ljbm;
    .locals 2

    .prologue
    .line 47033
    sget-object v0, Ljbm;->dYK:[Ljbm;

    if-nez v0, :cond_1

    .line 47034
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 47036
    :try_start_0
    sget-object v0, Ljbm;->dYK:[Ljbm;

    if-nez v0, :cond_0

    .line 47037
    const/4 v0, 0x0

    new-array v0, v0, [Ljbm;

    sput-object v0, Ljbm;->dYK:[Ljbm;

    .line 47039
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47041
    :cond_1
    sget-object v0, Ljbm;->dYK:[Ljbm;

    return-object v0

    .line 47039
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 47027
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbm;->agq:Ljava/lang/String;

    iget v0, p0, Ljbm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbm;->dHr:Ljava/lang/String;

    iget v0, p0, Ljbm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbm;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 47106
    iget v0, p0, Ljbm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 47107
    const/4 v0, 0x1

    iget-object v1, p0, Ljbm;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 47109
    :cond_0
    iget v0, p0, Ljbm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 47110
    const/4 v0, 0x2

    iget-object v1, p0, Ljbm;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 47112
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 47113
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47049
    iget-object v0, p0, Ljbm;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47071
    iget-object v0, p0, Ljbm;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 47117
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 47118
    iget v1, p0, Ljbm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 47119
    const/4 v1, 0x1

    iget-object v2, p0, Ljbm;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47122
    :cond_0
    iget v1, p0, Ljbm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 47123
    const/4 v1, 0x2

    iget-object v2, p0, Ljbm;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47126
    :cond_1
    return v0
.end method

.method public final sr(Ljava/lang/String;)Ljbm;
    .locals 1

    .prologue
    .line 47052
    if-nez p1, :cond_0

    .line 47053
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47055
    :cond_0
    iput-object p1, p0, Ljbm;->agq:Ljava/lang/String;

    .line 47056
    iget v0, p0, Ljbm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbm;->aez:I

    .line 47057
    return-object p0
.end method

.method public final ss(Ljava/lang/String;)Ljbm;
    .locals 1

    .prologue
    .line 47074
    if-nez p1, :cond_0

    .line 47075
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47077
    :cond_0
    iput-object p1, p0, Ljbm;->dHr:Ljava/lang/String;

    .line 47078
    iget v0, p0, Ljbm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbm;->aez:I

    .line 47079
    return-object p0
.end method
