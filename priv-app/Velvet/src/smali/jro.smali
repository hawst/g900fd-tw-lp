.class public final Ljro;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eBc:[Ljro;


# instance fields
.field private aez:I

.field private ajk:Ljava/lang/String;

.field private dAq:I

.field private dAr:I

.field private dPv:[B

.field private eBd:Ljava/lang/String;

.field private eBe:[B

.field private eBf:I

.field private eBg:I

.field private eBh:[Ljnk;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 617
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 618
    iput v1, p0, Ljro;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljro;->ajk:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljro;->dPv:[B

    iput v1, p0, Ljro;->dAq:I

    iput v1, p0, Ljro;->dAr:I

    const-string v0, ""

    iput-object v0, p0, Ljro;->eBd:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljro;->eBe:[B

    iput v1, p0, Ljro;->eBf:I

    iput v1, p0, Ljro;->eBg:I

    invoke-static {}, Ljnk;->bqH()[Ljnk;

    move-result-object v0

    iput-object v0, p0, Ljro;->eBh:[Ljnk;

    const/4 v0, 0x0

    iput-object v0, p0, Ljro;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljro;->eCz:I

    .line 619
    return-void
.end method

.method public static btw()[Ljro;
    .locals 2

    .prologue
    .line 437
    sget-object v0, Ljro;->eBc:[Ljro;

    if-nez v0, :cond_1

    .line 438
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 440
    :try_start_0
    sget-object v0, Ljro;->eBc:[Ljro;

    if-nez v0, :cond_0

    .line 441
    const/4 v0, 0x0

    new-array v0, v0, [Ljro;

    sput-object v0, Ljro;->eBc:[Ljro;

    .line 443
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    :cond_1
    sget-object v0, Ljro;->eBc:[Ljro;

    return-object v0

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 431
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljro;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljro;->ajk:Ljava/lang/String;

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljro;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljro;->eBd:Ljava/lang/String;

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljro;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljro;->eBh:[Ljnk;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljnk;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljro;->eBh:[Ljnk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljnk;

    invoke-direct {v3}, Ljnk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljro;->eBh:[Ljnk;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljnk;

    invoke-direct {v3}, Ljnk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljro;->eBh:[Ljnk;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljro;->dAq:I

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljro;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljro;->dAr:I

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljro;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljro;->eBf:I

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljro;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljro;->eBg:I

    iget v0, p0, Ljro;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljro;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljro;->dPv:[B

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljro;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljro;->eBe:[B

    iget v0, p0, Ljro;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljro;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 640
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 641
    const/4 v0, 0x1

    iget-object v1, p0, Ljro;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 643
    :cond_0
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 644
    const/4 v0, 0x2

    iget-object v1, p0, Ljro;->eBd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 646
    :cond_1
    iget-object v0, p0, Ljro;->eBh:[Ljnk;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljro;->eBh:[Ljnk;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 647
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljro;->eBh:[Ljnk;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 648
    iget-object v1, p0, Ljro;->eBh:[Ljnk;

    aget-object v1, v1, v0

    .line 649
    if-eqz v1, :cond_2

    .line 650
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 647
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 654
    :cond_3
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 655
    const/4 v0, 0x4

    iget v1, p0, Ljro;->dAq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 657
    :cond_4
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 658
    const/4 v0, 0x5

    iget v1, p0, Ljro;->dAr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 660
    :cond_5
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 661
    const/4 v0, 0x6

    iget v1, p0, Ljro;->eBf:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 663
    :cond_6
    iget v0, p0, Ljro;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 664
    const/4 v0, 0x7

    iget v1, p0, Ljro;->eBg:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 666
    :cond_7
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 667
    const/16 v0, 0x8

    iget-object v1, p0, Ljro;->dPv:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 669
    :cond_8
    iget v0, p0, Ljro;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_9

    .line 670
    const/16 v0, 0x9

    iget-object v1, p0, Ljro;->eBe:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 672
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 673
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 677
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 678
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 679
    const/4 v1, 0x1

    iget-object v2, p0, Ljro;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    :cond_0
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_1

    .line 683
    const/4 v1, 0x2

    iget-object v2, p0, Ljro;->eBd:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 686
    :cond_1
    iget-object v1, p0, Ljro;->eBh:[Ljnk;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljro;->eBh:[Ljnk;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 687
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljro;->eBh:[Ljnk;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 688
    iget-object v2, p0, Ljro;->eBh:[Ljnk;

    aget-object v2, v2, v0

    .line 689
    if-eqz v2, :cond_2

    .line 690
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 687
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 695
    :cond_4
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 696
    const/4 v1, 0x4

    iget v2, p0, Ljro;->dAq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_5
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 700
    const/4 v1, 0x5

    iget v2, p0, Ljro;->dAr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 703
    :cond_6
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 704
    const/4 v1, 0x6

    iget v2, p0, Ljro;->eBf:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    :cond_7
    iget v1, p0, Ljro;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 708
    const/4 v1, 0x7

    iget v2, p0, Ljro;->eBg:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    :cond_8
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_9

    .line 712
    const/16 v1, 0x8

    iget-object v2, p0, Ljro;->dPv:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 715
    :cond_9
    iget v1, p0, Ljro;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_a

    .line 716
    const/16 v1, 0x9

    iget-object v2, p0, Ljro;->eBe:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 719
    :cond_a
    return v0
.end method
