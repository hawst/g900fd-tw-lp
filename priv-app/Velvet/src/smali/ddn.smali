.class public final Lddn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bue:Ldes;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private bug:Landroid/util/SparseArray;

.field public buh:Z

.field private bui:Z

.field private final mClock:Lemp;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mFlags:Lchk;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mSuggestions:Ldem;


# direct methods
.method public constructor <init>(Ldes;Lchk;Lemp;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldes;

    iput-object v0, p0, Lddn;->bue:Ldes;

    .line 39
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lddn;->mFlags:Lchk;

    .line 40
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemp;

    iput-object v0, p0, Lddn;->mClock:Lemp;

    .line 41
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddn;->buh:Z

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lddn;->bui:Z

    .line 44
    return-void
.end method

.method private aaX()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 76
    new-instance v4, Landroid/util/SparseArray;

    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v4, v0}, Landroid/util/SparseArray;-><init>(I)V

    move v2, v3

    .line 77
    :goto_0
    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 78
    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 79
    if-eqz v0, :cond_2

    .line 80
    invoke-interface {v0}, Ldef;->getCount()I

    move-result v1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 83
    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/search/Suggestion;

    .line 84
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 85
    new-instance v7, Lded;

    invoke-direct {v7}, Lded;-><init>()V

    invoke-virtual {v7, v1}, Lded;->h(Lcom/google/android/shared/search/Suggestion;)Lded;

    move-result-object v1

    iput-boolean v3, v1, Lded;->hO:Z

    invoke-virtual {v1}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 88
    :cond_0
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 91
    :cond_1
    iget-object v1, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    new-instance v6, Ldei;

    invoke-interface {v0}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {v6, v7, v0, v5}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    invoke-virtual {v4, v1, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 77
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 94
    :cond_3
    iput-object v4, p0, Lddn;->bug:Landroid/util/SparseArray;

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddn;->bui:Z

    .line 96
    return-void
.end method


# virtual methods
.method public final a(Landroid/util/SparseArray;Z)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 123
    if-nez p2, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ldef;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v2

    :goto_1
    if-nez v0, :cond_1

    .line 124
    :cond_0
    iput-object p1, p0, Lddn;->bug:Landroid/util/SparseArray;

    move v2, v3

    .line 127
    :cond_1
    return v2

    .line 123
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final aaY()Landroid/util/SparseArray;
    .locals 4

    .prologue
    .line 102
    iget-boolean v0, p0, Lddn;->buh:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lddn;->mSuggestions:Ldem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lddn;->mSuggestions:Ldem;

    invoke-virtual {v0}, Ldem;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iget-object v1, p0, Lddn;->bue:Ldes;

    iget-object v2, p0, Lddn;->mSuggestions:Ldem;

    iget-object v3, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-interface {v1, v2, v3, v0}, Ldes;->a(Ldem;Landroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lddn;->a(Landroid/util/SparseArray;Z)Z

    iget-boolean v0, p0, Lddn;->bui:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lddn;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lddn;->mSuggestions:Ldem;

    invoke-virtual {v2}, Ldem;->getCreationTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lddn;->mFlags:Lchk;

    invoke-virtual {v2}, Lchk;->Fy()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-direct {p0}, Lddn;->aaX()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lddn;->buh:Z

    .line 105
    :cond_1
    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    return-object v0
.end method

.method public final b(Ldem;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 51
    iput-object p1, p0, Lddn;->mSuggestions:Ldem;

    .line 52
    new-instance v4, Landroid/util/SparseArray;

    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v4, v0}, Landroid/util/SparseArray;-><init>(I)V

    move v2, v3

    :goto_0
    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ldef;->getCount()I

    move-result v1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/search/Suggestion;

    new-instance v7, Lded;

    invoke-direct {v7}, Lded;-><init>()V

    invoke-virtual {v7, v1}, Lded;->h(Lcom/google/android/shared/search/Suggestion;)Lded;

    move-result-object v1

    iput-boolean v8, v1, Lded;->buZ:Z

    iput-boolean v8, v1, Lded;->hO:Z

    invoke-virtual {v1}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lddn;->bug:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    new-instance v6, Ldei;

    invoke-interface {v0}, Ldef;->QJ()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {v6, v7, v0, v5}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    invoke-virtual {v4, v1, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iput-object v4, p0, Lddn;->bug:Landroid/util/SparseArray;

    iput-boolean v3, p0, Lddn;->bui:Z

    .line 53
    iput-boolean v8, p0, Lddn;->buh:Z

    .line 54
    return-void
.end method
