.class public final Lfwv;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lfwv;->bj(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 36
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 37
    const/4 v2, 0x1

    new-array v2, v2, [Lanh;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Lang;->ags:[Lanh;

    .line 38
    return-object v1
.end method

.method protected final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lfwv;->bj(Landroid/content/Context;)Lanh;

    move-result-object v0

    return-object v0
.end method

.method public final bj(Landroid/content/Context;)Lanh;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 52
    new-instance v8, Lanh;

    invoke-direct {v8}, Lanh;-><init>()V

    .line 53
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v8, Lanh;->ahu:Lizj;

    .line 54
    invoke-virtual {v8, v4}, Lanh;->cm(I)Lanh;

    .line 55
    new-instance v0, Lany;

    invoke-direct {v0}, Lany;-><init>()V

    iput-object v0, v8, Lanh;->agw:Lany;

    .line 58
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v9, v0, Lizj;->dSs:Liyd;

    .line 59
    iget-object v0, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 62
    const/4 v6, 0x3

    iget-object v0, v9, Liyd;->dPw:[Liye;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Liye;->bbu()Ljava/lang/String;

    move-result-object v7

    move-object v0, p1

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, v8, Lanh;->agw:Lany;

    invoke-virtual {v1, v0}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 69
    iget-object v0, v8, Lanh;->agw:Lany;

    invoke-virtual {v9}, Liyd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 72
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lanh;->aV(Z)Lanh;

    .line 74
    return-object v8
.end method
