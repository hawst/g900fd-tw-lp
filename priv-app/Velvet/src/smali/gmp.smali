.class public final Lgmp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgmj;


# instance fields
.field private final aTA:Ljava/util/concurrent/Future;

.field private aTB:Ljava/util/concurrent/Future;

.field private final aTD:Ljwe;

.field private final aTE:Lces;

.field private final aTr:Ljava/lang/String;

.field private final aTz:Ljava/util/concurrent/Future;

.field private final cHf:Lgni;

.field private final cPm:Lger;

.field private final cPn:Ljwe;

.field private cPo:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cPp:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cPq:Ljxh;

.field private final cPr:Ljxa;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mRequestId:Ljava/lang/String;

.field private final mSessionParams:Lgnj;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lger;Lgni;Lgnj;)V
    .locals 11

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 67
    iput-object p2, p0, Lgmp;->cPm:Lger;

    .line 68
    iput-object p3, p0, Lgmp;->cHf:Lgni;

    .line 69
    new-instance v0, Lces;

    const-wide/16 v2, 0x3e8

    invoke-direct {v0, v2, v3}, Lces;-><init>(J)V

    iput-object v0, p0, Lgmp;->aTE:Lces;

    .line 71
    iput-object p4, p0, Lgmp;->mSessionParams:Lgnj;

    .line 72
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->tF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTr:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgmp;->mRequestId:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->getMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lgmp;->cHf:Lgni;

    iget-object v1, v1, Lgni;->cPW:Lhzs;

    iget-object v2, p0, Lgmp;->mRequestId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lhzs;->oQ(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lgmp;->cPo:Ljava/util/concurrent/Future;

    .line 75
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->getMode()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lgmp;->cHf:Lgni;

    iget-object v1, v1, Lgni;->cPX:Lhzs;

    iget-object v2, p0, Lgmp;->mRequestId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lhzs;->oQ(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lgmp;->cPp:Ljava/util/concurrent/Future;

    .line 76
    iget-object v0, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lgmr;

    iget-object v2, p0, Lgmp;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mNetworkInformation:Lgno;

    invoke-direct {v1, v2}, Lgmr;-><init>(Lgno;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTz:Ljava/util/concurrent/Future;

    .line 77
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->aHC()Lgmx;

    move-result-object v0

    new-instance v1, Ljwe;

    invoke-direct {v1}, Ljwe;-><init>()V

    invoke-virtual {v0}, Lgmx;->getEncoding()I

    move-result v2

    invoke-virtual {v1, v2}, Ljwe;->sF(I)Ljwe;

    move-result-object v1

    invoke-virtual {v0}, Lgmx;->getSamplingRate()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Ljwe;->ac(F)Ljwe;

    move-result-object v0

    iput-object v0, p0, Lgmp;->cPn:Ljwe;

    .line 78
    iget-object v7, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lgmt;

    iget-object v1, p0, Lgmp;->cHf:Lgni;

    iget-object v1, v1, Lgni;->mLocationHelper:Lglo;

    iget-object v2, p0, Lgmp;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mSpeechContext:Lgij;

    iget-object v3, p0, Lgmp;->cHf:Lgni;

    iget-object v3, v3, Lgni;->mSpeechSettings:Lgdo;

    iget-object v4, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v4}, Lgnj;->xU()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lgmp;->cHf:Lgni;

    iget-object v5, v5, Lgni;->mDeviceParams:Lgmz;

    iget-object v6, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v6}, Lgnj;->aHI()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lgmt;-><init>(Lglo;Lgij;Lgdo;Ljava/lang/String;Lgmz;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTA:Ljava/util/concurrent/Future;

    .line 79
    invoke-direct {p0}, Lgmp;->aHq()Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTB:Ljava/util/concurrent/Future;

    .line 80
    new-instance v0, Lgmu;

    iget-object v1, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v1}, Lgnj;->aHR()Ljtp;

    move-result-object v1

    iget-object v2, p0, Lgmp;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mSpeechSettings:Lgdo;

    iget-object v3, p0, Lgmp;->cHf:Lgni;

    iget-object v3, v3, Lgni;->mSpeechContext:Lgij;

    iget-object v4, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v4}, Lgnj;->aHO()Z

    move-result v4

    iget-object v5, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v5}, Lgnj;->aHV()Z

    move-result v5

    iget-object v6, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v6}, Lgnj;->isSuggestionsEnabled()Z

    move-result v6

    iget-object v7, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v7}, Lgnj;->aHQ()I

    move-result v7

    iget-object v8, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v8}, Lgnj;->Hq()Z

    move-result v8

    iget-object v9, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v9}, Lgnj;->aHP()Z

    move-result v9

    iget-object v10, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v10}, Lgnj;->xL()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lgmu;-><init>(Ljtp;Lgdo;Lgij;ZZZIZZLjava/lang/String;)V

    invoke-virtual {v0}, Lgmu;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxh;

    iput-object v0, p0, Lgmp;->cPq:Ljxh;

    .line 81
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->FH()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lgmp;->cPr:Ljxa;

    .line 82
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->aHC()Lgmx;

    move-result-object v0

    invoke-virtual {v0}, Lgmx;->aHA()[B

    move-result-object v0

    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v0}, Lgnj;->aHC()Lgmx;

    move-result-object v0

    new-instance v1, Ljwe;

    invoke-direct {v1}, Ljwe;-><init>()V

    invoke-virtual {v0}, Lgmx;->aHx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljwe;->sF(I)Ljwe;

    move-result-object v1

    invoke-virtual {v0}, Lgmx;->aHy()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Ljwe;->ac(F)Ljwe;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTD:Ljwe;

    .line 87
    :goto_3
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 75
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 81
    :cond_2
    new-instance v0, Ljxa;

    invoke-direct {v0}, Ljxa;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljxa;->jn(Z)Ljxa;

    goto :goto_2

    .line 85
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lgmp;->aTD:Ljwe;

    goto :goto_3
.end method

.method private aHq()Ljava/util/concurrent/Future;
    .locals 7

    .prologue
    .line 226
    iget-object v6, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lgmp;->cHf:Lgni;

    iget-object v0, v0, Lgni;->mAuthTokenHelper:Lgln;

    iget-object v1, p0, Lgmp;->cHf:Lgni;

    iget-object v1, v1, Lgni;->mSpeechSettings:Lgdo;

    iget-object v2, p0, Lgmp;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mLocationHelper:Lglo;

    iget-object v3, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v3}, Lgnj;->aHG()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v4}, Lgnj;->aHH()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v5}, Lgnj;->aqW()Landroid/location/Location;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lgmv;->a(Lgln;Lgdo;Lglo;Ljava/lang/String;Ljava/util/List;Landroid/location/Location;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final Dl()Lgmk;
    .locals 14

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lgmp;->cPm:Lger;

    invoke-interface {v0}, Lger;->df()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 97
    new-instance v0, Lgmn;

    iget-object v1, p0, Lgmp;->cPo:Ljava/util/concurrent/Future;

    iget-object v2, p0, Lgmp;->cPp:Ljava/util/concurrent/Future;

    iget-object v3, p0, Lgmp;->aTz:Ljava/util/concurrent/Future;

    iget-object v4, p0, Lgmp;->aTA:Ljava/util/concurrent/Future;

    iget-object v5, p0, Lgmp;->aTB:Ljava/util/concurrent/Future;

    iget-object v6, p0, Lgmp;->cPn:Ljwe;

    iget-object v7, p0, Lgmp;->aTD:Ljwe;

    iget-object v8, p0, Lgmp;->cPq:Ljxh;

    iget-object v9, p0, Lgmp;->cPr:Ljxa;

    iget-object v10, p0, Lgmp;->mRequestId:Ljava/lang/String;

    iget-object v11, p0, Lgmp;->aTr:Ljava/lang/String;

    iget-object v12, p0, Lgmp;->cHf:Lgni;

    iget-object v12, v12, Lgni;->mSpeechSettings:Lgdo;

    invoke-direct/range {v0 .. v12}, Lgmn;-><init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljava/util/concurrent/Future;Ljwe;Ljwe;Ljxh;Ljxa;Ljava/lang/String;Ljava/lang/String;Lgdo;)V

    .line 110
    new-instance v2, Lgmi;

    iget-object v1, p0, Lgmp;->cHf:Lgni;

    iget-object v1, v1, Lgni;->cPW:Lhzs;

    iget-object v3, p0, Lgmp;->mRequestId:Ljava/lang/String;

    invoke-direct {v2, v1, v3}, Lgmi;-><init>(Lhzs;Ljava/lang/String;)V

    .line 112
    new-instance v3, Lgmh;

    iget-object v1, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v1}, Lgnj;->aHC()Lgmx;

    move-result-object v1

    invoke-virtual {v1}, Lgmx;->getEncoding()I

    move-result v1

    iget-object v4, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v4}, Lgnj;->aHC()Lgmx;

    move-result-object v4

    invoke-virtual {v4}, Lgmx;->getEncoding()I

    move-result v4

    invoke-static {v4}, Lgfd;->kh(I)I

    move-result v4

    invoke-direct {v3, v13, v1, v4}, Lgmh;-><init>(Ljava/io/InputStream;II)V

    .line 116
    iget-object v1, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v1}, Lgnj;->aHC()Lgmx;

    move-result-object v1

    invoke-virtual {v1}, Lgmx;->aHA()[B

    move-result-object v1

    .line 117
    if-eqz v1, :cond_0

    .line 119
    new-instance v4, Lgmg;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v1, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v1}, Lgnj;->aHC()Lgmx;

    move-result-object v1

    invoke-virtual {v1}, Lgmx;->aHx()I

    move-result v1

    iget-object v6, p0, Lgmp;->mSessionParams:Lgnj;

    invoke-virtual {v6}, Lgnj;->aHC()Lgmx;

    move-result-object v6

    invoke-virtual {v6}, Lgmx;->aHx()I

    move-result v6

    invoke-static {v6}, Lgfd;->kh(I)I

    move-result v6

    invoke-direct {v4, v5, v1, v6}, Lgmg;-><init>(Ljava/io/InputStream;II)V

    .line 124
    new-instance v1, Lgmk;

    const/4 v5, 0x4

    new-array v5, v5, [Lceq;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    const/4 v0, 0x2

    aput-object v3, v5, v0

    const/4 v0, 0x3

    aput-object v4, v5, v0

    invoke-direct {v1, v5}, Lgmk;-><init>([Lceq;)V

    move-object v0, v1

    .line 126
    :goto_0
    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Leie;

    const v2, 0x10016

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 126
    :cond_0
    new-instance v1, Lgmk;

    const/4 v4, 0x3

    new-array v4, v4, [Lceq;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    invoke-direct {v1, v4}, Lgmk;-><init>([Lceq;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 157
    iget-object v0, p0, Lgmp;->cPo:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lgmp;->cPo:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 160
    :cond_0
    iget-object v0, p0, Lgmp;->cPp:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lgmp;->cPp:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 163
    :cond_1
    iget-object v0, p0, Lgmp;->aTz:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 164
    iget-object v0, p0, Lgmp;->aTA:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 165
    iget-object v0, p0, Lgmp;->aTB:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 166
    return-void
.end method

.method public final refresh()V
    .locals 4

    .prologue
    .line 134
    :try_start_0
    iget-object v0, p0, Lgmp;->aTE:Lces;

    iget-object v1, p0, Lgmp;->aTB:Ljava/util/concurrent/Future;

    invoke-virtual {v0, v1}, Lces;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwy;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    if-nez v0, :cond_0

    .line 143
    invoke-direct {p0}, Lgmp;->aHq()Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTB:Ljava/util/concurrent/Future;

    .line 152
    :goto_1
    return-void

    .line 136
    :catch_0
    move-exception v0

    const-string v0, "VoiceSearchRequestProducerFactory"

    const-string v1, "Could not get S3UserInfo for refresh."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 137
    const/4 v0, 0x0

    goto :goto_0

    .line 146
    :cond_0
    iget-object v1, p0, Lgmp;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lgmp;->cHf:Lgni;

    iget-object v2, v2, Lgni;->mAuthTokenHelper:Lgln;

    iget-object v3, p0, Lgmp;->cHf:Lgni;

    iget-object v3, v3, Lgni;->mSpeechSettings:Lgdo;

    invoke-static {v2, v0, v3}, Lgmv;->a(Lgln;Ljwy;Lgdo;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgmp;->aTB:Ljava/util/concurrent/Future;

    goto :goto_1
.end method
