.class public final Lftv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ledl;


# static fields
.field private static final cBS:D


# instance fields
.field private final cBT:D

.field private final mConsumer:Lemy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    const/4 v0, -0x1

    invoke-static {v0}, Lftv;->jD(I)D

    move-result-wide v0

    sput-wide v0, Lftv;->cBS:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLemy;)V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    if-eqz p2, :cond_0

    const v0, 0x7f0c006d

    .line 41
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lftv;->cBT:D

    .line 42
    iput-object p3, p0, Lftv;->mConsumer:Lemy;

    .line 43
    return-void

    .line 38
    :cond_0
    const v0, 0x7f0c006c

    goto :goto_0
.end method

.method private b(Lkp;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide v6, 0x3fa999999999999aL    # 0.05

    .line 102
    if-nez p1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 103
    :cond_1
    iget v1, p1, Lkp;->kb:I

    .line 104
    sget-wide v2, Lftv;->cBS:D

    add-double/2addr v2, v6

    invoke-static {v1}, Lftv;->jD(I)D

    move-result-wide v4

    add-double/2addr v4, v6

    div-double/2addr v2, v4

    iget-wide v4, p0, Lftv;->cBT:D

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_0

    .line 105
    iget-object v0, p0, Lftv;->mConsumer:Lemy;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 106
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static jD(I)D
    .locals 6

    .prologue
    .line 122
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {v0}, Lftv;->jE(I)D

    move-result-wide v0

    const-wide v2, 0x3fcb367a0f9096bcL    # 0.2126

    mul-double/2addr v0, v2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {v2}, Lftv;->jE(I)D

    move-result-wide v2

    const-wide v4, 0x3fe6e2eb1c432ca5L    # 0.7152

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {v2}, Lftv;->jE(I)D

    move-result-wide v2

    const-wide v4, 0x3fb27bb2fec56d5dL    # 0.0722

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private static jE(I)D
    .locals 4

    .prologue
    .line 128
    int-to-double v0, p0

    const-wide v2, 0x406fe00000000000L    # 255.0

    div-double/2addr v0, v2

    .line 129
    const-wide v2, 0x3fa41c8216c61523L    # 0.03928

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    const-wide v2, 0x4029d70a3d70a3d7L    # 12.92

    div-double/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    const-wide v2, 0x3fac28f5c28f5c29L    # 0.055

    add-double/2addr v0, v2

    const-wide v2, 0x3ff0e147ae147ae1L    # 1.055

    div-double/2addr v0, v2

    const-wide v2, 0x4003333333333333L    # 2.4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method final b(Lkm;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p1, Lkm;->jQ:Lkp;

    .line 70
    invoke-direct {p0, v0}, Lftv;->b(Lkp;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p1, Lkm;->jS:Lkp;

    .line 76
    invoke-direct {p0, v0}, Lftv;->b(Lkp;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p1, Lkm;->jR:Lkp;

    .line 82
    invoke-direct {p0, v0}, Lftv;->b(Lkp;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p1, Lkm;->jT:Lkp;

    .line 88
    invoke-direct {p0, v0}, Lftv;->b(Lkp;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lftv;->mConsumer:Lemy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final f(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 47
    const/4 v0, 0x0

    .line 48
    instance-of v1, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 49
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 51
    :cond_0
    if-nez v0, :cond_1

    .line 62
    :goto_0
    return-void

    .line 54
    :cond_1
    new-instance v1, Lftw;

    invoke-direct {v1, p0}, Lftw;-><init>(Lftv;)V

    invoke-static {v0}, Lkm;->a(Landroid/graphics/Bitmap;)V

    invoke-static {v3}, Lkm;->O(I)V

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v2, Lkn;

    invoke-direct {v2, v3, v1}, Lkn;-><init>(ILko;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "task can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_4

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v2, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
