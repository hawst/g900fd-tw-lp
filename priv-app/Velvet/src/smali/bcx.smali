.class public final Lbcx;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbju;->n(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->auB:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awI:[I

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;I[IZ)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awJ:[B

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;I[BZ)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awK:[Landroid/os/Bundle;

    invoke-static {p1, v1, v2, p2, v3}, Lbju;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awL:[Landroid/os/Bundle;

    invoke-static {p1, v1, v2, p2, v3}, Lbju;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awM:[Landroid/os/Bundle;

    invoke-static {p1, v1, v2, p2, v3}, Lbju;->a(Landroid/os/Parcel;I[Landroid/os/Parcelable;IZ)V

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awN:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;I[IZ)V

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awP:[Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;I[Ljava/lang/String;Z)V

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awQ:[B

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;I[BZ)V

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awR:[D

    if-eqz v1, :cond_0

    const/16 v2, 0xb

    invoke-static {p1, v2}, Lbju;->n(Landroid/os/Parcel;I)I

    move-result v2

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeDoubleArray([D)V

    invoke-static {p1, v2}, Lbju;->o(Landroid/os/Parcel;I)V

    :cond_0
    invoke-static {p1, v0}, Lbju;->o(Landroid/os/Parcel;I)V

    return-void
.end method

.method public static c(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 15

    invoke-static {p0}, Lbjs;->x(Landroid/os/Parcel;)I

    move-result v13

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v13, :cond_1

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v14, 0xffff

    and-int/2addr v14, v0

    sparse-switch v14, :sswitch_data_0

    invoke-static {p0, v0}, Lbjs;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p0, v0}, Lbjs;->h(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :sswitch_1
    invoke-static {p0, v0}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_2
    invoke-static {p0, v0}, Lbjs;->k(Landroid/os/Parcel;I)[I

    move-result-object v3

    goto :goto_0

    :sswitch_3
    invoke-static {p0, v0}, Lbjs;->j(Landroid/os/Parcel;I)[B

    move-result-object v4

    goto :goto_0

    :sswitch_4
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p0, v0, v5}, Lbjs;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    move-object v5, v0

    goto :goto_0

    :sswitch_5
    sget-object v6, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p0, v0, v6}, Lbjs;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    move-object v6, v0

    goto :goto_0

    :sswitch_6
    sget-object v7, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p0, v0, v7}, Lbjs;->b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Bundle;

    move-object v7, v0

    goto :goto_0

    :sswitch_7
    invoke-static {p0, v0}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v8

    goto :goto_0

    :sswitch_8
    invoke-static {p0, v0}, Lbjs;->k(Landroid/os/Parcel;I)[I

    move-result-object v9

    goto :goto_0

    :sswitch_9
    invoke-static {p0, v0}, Lbjs;->l(Landroid/os/Parcel;I)[Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    :sswitch_a
    invoke-static {p0, v0}, Lbjs;->j(Landroid/os/Parcel;I)[B

    move-result-object v11

    goto :goto_0

    :sswitch_b
    invoke-static {p0, v0}, Lbjs;->a(Landroid/os/Parcel;I)I

    move-result v12

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v14

    if-nez v12, :cond_0

    const/4 v0, 0x0

    :goto_1
    move-object v12, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->createDoubleArray()[D

    move-result-object v0

    add-int/2addr v12, v14

    invoke-virtual {p0, v12}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v13, :cond_2

    new-instance v0, Lbjt;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Overread allowed size end="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbjt;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(ILjava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;I[I[Ljava/lang/String;[B[D)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0x3e8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lbcx;->c(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/appdatasearch/SearchResults;

    return-object v0
.end method
