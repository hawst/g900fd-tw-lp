.class public final Lfur;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfyd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 42
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUb:Liww;

    .line 44
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 48
    new-instance v2, Lftn;

    const v3, 0x7f0a04d3

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Liww;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lftn;-><init>(Ljava/lang/String;)V

    .line 50
    iget-object v3, v0, Liww;->dMA:Ljbc;

    if-eqz v3, :cond_0

    .line 51
    iget-object v0, v0, Liww;->dMA:Ljbc;

    invoke-static {v0}, Ledx;->a(Ljbc;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 52
    new-instance v3, Lfsa;

    const/16 v4, 0xba

    invoke-direct {v3, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f0201b8

    invoke-virtual {v3, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {v2, v0, v3}, Lftn;->a(Lani;Lizj;)Lftn;

    .line 58
    :cond_0
    invoke-virtual {v2}, Lftn;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v2, 0x23

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    move-result-object v0

    .line 62
    new-instance v2, Laot;

    invoke-direct {v2}, Laot;-><init>()V

    const v3, 0x7f0a04d4

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laot;->cr(Ljava/lang/String;)Laot;

    move-result-object v2

    const v3, 0x7f0201e4

    invoke-virtual {v2, v3}, Laot;->cK(I)Laot;

    move-result-object v2

    iput-object v2, v0, Lanh;->agZ:Laot;

    .line 65
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 66
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v2, Lang;

    invoke-direct {v2}, Lang;-><init>()V

    .line 69
    invoke-virtual {v2, v7}, Lang;->aS(Z)Lang;

    .line 70
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v2, Lang;->ags:[Lanh;

    .line 71
    return-object v2
.end method

.method public final b(Ljava/lang/String;Lfmt;)Ljava/util/List;
    .locals 11
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUb:Liww;

    .line 81
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v0, v0, Liww;->dMC:[Lixn;

    .line 95
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 96
    array-length v6, v0

    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-ge v5, v6, :cond_3

    aget-object v7, v0, v5

    .line 97
    iget-object v1, v7, Lixn;->dNR:Ljcn;

    if-eqz v1, :cond_2

    iget-object v1, v7, Lixn;->dNR:Ljcn;

    invoke-virtual {v1}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 99
    :goto_2
    iget-object v1, v7, Lixn;->dMA:Ljbc;

    if-eqz v1, :cond_4

    .line 100
    new-instance v1, Lfsa;

    const/4 v8, 0x3

    invoke-direct {v1, v8}, Lfsa;-><init>(I)V

    const v8, 0x7f0201b8

    invoke-virtual {v1, v8}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    iget-object v8, v7, Lixn;->dMA:Ljbc;

    invoke-static {v8}, Ledx;->a(Ljbc;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8, v2}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    .line 105
    :goto_3
    new-instance v8, Lfyc;

    invoke-virtual {v7}, Lixn;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, " \u00b7 "

    iget-object v7, v7, Lixn;->dOb:[Ljava/lang/String;

    invoke-static {v10, v7}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {v8, v9, v7, v3, v1}, Lfyc;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Lani;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 84
    :cond_0
    new-instance v1, Lixp;

    invoke-direct {v1}, Lixp;-><init>()V

    .line 85
    iget-object v0, v0, Liww;->dMA:Ljbc;

    iput-object v0, v1, Lixp;->dMA:Ljbc;

    .line 86
    invoke-virtual {v1, p1}, Lixp;->qN(Ljava/lang/String;)Lixp;

    .line 87
    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfml;->a(Lixp;)Lixq;

    move-result-object v0

    .line 89
    if-nez v0, :cond_1

    .line 111
    :goto_4
    return-object v2

    .line 92
    :cond_1
    iget-object v0, v0, Lixq;->dOf:[Lixn;

    goto :goto_0

    :cond_2
    move-object v3, v2

    .line 97
    goto :goto_2

    :cond_3
    move-object v2, v4

    .line 111
    goto :goto_4

    :cond_4
    move-object v1, v2

    goto :goto_3
.end method
