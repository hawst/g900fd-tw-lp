.class public Lfmq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aD:Ljava/lang/String;

.field private synthetic cwj:Lfml;


# direct methods
.method constructor <init>(Lfml;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1071
    iput-object p1, p0, Lfmq;->cwj:Lfml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072
    iput-object p2, p0, Lfmq;->aD:Ljava/lang/String;

    .line 1073
    return-void
.end method


# virtual methods
.method public final aBf()Z
    .locals 2

    .prologue
    .line 1083
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    iget-object v1, v0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1084
    :try_start_0
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    invoke-virtual {v0}, Lfml;->connect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1085
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwc:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1086
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1088
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 1089
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final release()V
    .locals 5

    .prologue
    .line 1097
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    iget-object v1, v0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1098
    :try_start_0
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwc:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1099
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    iget-object v0, v0, Lfml;->cwc:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1100
    iget-object v0, p0, Lfmq;->cwj:Lfml;

    iget-object v2, v0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, v0, Lfml;->cwf:Lfmp;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lfml;->mAppContext:Landroid/content/Context;

    iget-object v4, v0, Lfml;->cwf:Lfmp;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v3, 0x0

    iput-object v3, v0, Lfml;->cwe:Lfor;

    const/4 v3, 0x0

    iput-object v3, v0, Lfml;->cwf:Lfmp;

    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1102
    :cond_1
    :try_start_2
    monitor-exit v1

    return-void

    .line 1100
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1102
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1107
    iget-object v0, p0, Lfmq;->aD:Ljava/lang/String;

    return-object v0
.end method
