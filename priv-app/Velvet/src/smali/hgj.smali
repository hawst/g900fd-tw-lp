.class public final Lhgj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static DBG:Z

.field private static volatile cIG:I

.field private static final dgt:[I

.field private static dgz:Lijm;


# instance fields
.field private final aOw:Ljava/util/concurrent/Executor;

.field private final dfL:I

.field private final dgu:[I

.field private final dgv:Lhhu;

.field private dgw:Landroid/content/res/AssetFileDescriptor;

.field private dgx:Landroid/media/MediaPlayer;

.field private dgy:I

.field private hV:J

.field private mAddress:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mSettings:Lhym;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 29
    const/4 v0, 0x1

    sput-boolean v0, Lhgj;->DBG:Z

    .line 33
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhgj;->dgt:[I

    .line 66
    new-instance v0, Lijn;

    invoke-direct {v0}, Lijn;-><init>()V

    const/16 v1, 0x12c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    move-result-object v0

    sput-object v0, Lhgj;->dgz:Lijm;

    return-void

    .line 33
    nop

    :array_0
    .array-data 4
        0x12c
        0x12c
        0xc8
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lhhu;Ljava/util/concurrent/Executor;Lhym;I)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-object v0, Lhgj;->dgt:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lhgj;->dgu:[I

    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lhgj;->dgy:I

    .line 77
    iput-object p1, p0, Lhgj;->mContext:Landroid/content/Context;

    .line 78
    iput p5, p0, Lhgj;->dfL:I

    .line 79
    iput-object p2, p0, Lhgj;->dgv:Lhhu;

    .line 80
    iput-object p3, p0, Lhgj;->aOw:Ljava/util/concurrent/Executor;

    .line 81
    iput-object p4, p0, Lhgj;->mSettings:Lhym;

    .line 82
    invoke-virtual {p4}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgj;->mAddress:Ljava/lang/String;

    .line 83
    return-void
.end method

.method static synthetic a(Lhgj;J)J
    .locals 1

    .prologue
    .line 25
    iput-wide p1, p0, Lhgj;->hV:J

    return-wide p1
.end method

.method static synthetic a(Lhgj;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lhgj;->dgx:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic a(Lhgj;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lhgj;->aOy()V

    return-void
.end method

.method private aOA()V
    .locals 1

    .prologue
    .line 252
    :try_start_0
    iget-object v0, p0, Lhgj;->dgw:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic aOD()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lhgj;->cIG:I

    return v0
.end method

.method static synthetic aOE()I
    .locals 2

    .prologue
    .line 25
    sget v0, Lhgj;->cIG:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lhgj;->cIG:I

    return v0
.end method

.method public static aOw()I
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lhgj;->dgt:[I

    array-length v0, v0

    return v0
.end method

.method private aOy()V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lhgj;->dgx:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 174
    :try_start_0
    iget-object v0, p0, Lhgj;->dgx:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_0
    iget-object v0, p0, Lhgj;->dgx:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lhgj;->dgx:Landroid/media/MediaPlayer;

    .line 182
    :cond_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    const-string v1, "AudioTester"

    const-string v2, "Could not stop playing"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private aOz()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 189
    iget v0, p0, Lhgj;->dgy:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhgj;->dgy:I

    .line 190
    iget v0, p0, Lhgj;->dgy:I

    sget-object v2, Lhgj;->dgt:[I

    array-length v2, v2

    if-ne v0, v2, :cond_0

    .line 192
    invoke-direct {p0}, Lhgj;->aOA()V

    move v0, v1

    .line 246
    :goto_0
    return v0

    .line 195
    :cond_0
    sget-object v0, Lhgj;->dgz:Lijm;

    sget-object v2, Lhgj;->dgt:[I

    iget v3, p0, Lhgj;->dgy:I

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lijm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 196
    if-nez v0, :cond_1

    .line 197
    const-string v0, "AudioTester"

    const-string v2, "Incorrect type passed."

    new-instance v3, Ljava/lang/Error;

    invoke-direct {v3}, Ljava/lang/Error;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 198
    goto :goto_0

    .line 201
    :cond_1
    sget-boolean v2, Lhgj;->DBG:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Test "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lhgj;->dgt:[I

    iget v4, p0, Lhgj;->dgy:I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    :cond_2
    new-instance v2, Lhgk;

    const-string v3, "Audio setup"

    new-array v1, v1, [I

    invoke-direct {v2, p0, v3, v1, v0}, Lhgk;-><init>(Lhgj;Ljava/lang/String;[ILjava/lang/Integer;)V

    .line 245
    iget-object v0, p0, Lhgj;->aOw:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 246
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lhgj;)Lhhu;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lhgj;->dgv:Lhhu;

    return-object v0
.end method

.method static synthetic c(Lhgj;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lhgj;->dgx:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic d(Lhgj;)Landroid/content/res/AssetFileDescriptor;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lhgj;->dgw:Landroid/content/res/AssetFileDescriptor;

    return-object v0
.end method


# virtual methods
.method public final aOB()V
    .locals 2

    .prologue
    .line 262
    const/4 v0, -0x1

    iput v0, p0, Lhgj;->dgy:I

    .line 263
    iget-object v0, p0, Lhgj;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 264
    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lhgj;->dgw:Landroid/content/res/AssetFileDescriptor;

    .line 265
    invoke-direct {p0}, Lhgj;->aOz()Z

    .line 266
    return-void
.end method

.method public final aOC()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 270
    iput v3, p0, Lhgj;->dgy:I

    .line 272
    const/4 v0, 0x0

    sget-object v1, Lhgj;->dgt:[I

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 273
    iget-object v2, p0, Lhgj;->dgu:[I

    aput v3, v2, v0

    .line 272
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    :cond_0
    invoke-direct {p0}, Lhgj;->aOy()V

    .line 276
    invoke-direct {p0}, Lhgj;->aOA()V

    .line 277
    return-void
.end method

.method public final aOx()Landroid/util/Pair;
    .locals 10

    .prologue
    const/16 v9, 0x1388

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 110
    iget v0, p0, Lhgj;->dgy:I

    sget-object v1, Lhgj;->dgt:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 112
    new-instance v0, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 167
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget v0, p0, Lhgj;->dgy:I

    if-gez v0, :cond_1

    .line 117
    new-instance v0, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_1
    invoke-direct {p0}, Lhgj;->aOy()V

    .line 125
    iget-wide v0, p0, Lhgj;->hV:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    .line 127
    const v0, 0x7fffffff

    .line 136
    :goto_1
    iget-object v1, p0, Lhgj;->dgu:[I

    iget v3, p0, Lhgj;->dgy:I

    aput v0, v1, v3

    .line 137
    sget-boolean v1, Lhgj;->DBG:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Delay for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lhgj;->dgt:[I

    iget v6, p0, Lhgj;->dgy:I

    aget v3, v3, v6

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    invoke-direct {p0}, Lhgj;->aOz()Z

    move-result v6

    .line 141
    if-nez v6, :cond_8

    .line 143
    iget-object v1, p0, Lhgj;->dgu:[I

    aget v3, v1, v5

    .line 146
    iget-object v1, p0, Lhgj;->dgu:[I

    array-length v7, v1

    move v1, v5

    :goto_2
    if-ge v2, v7, :cond_5

    .line 147
    iget-object v8, p0, Lhgj;->dgu:[I

    aget v8, v8, v2

    if-ge v8, v3, :cond_3

    .line 148
    iget-object v1, p0, Lhgj;->dgu:[I

    aget v1, v1, v2

    move v3, v1

    move v1, v2

    .line 146
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 133
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v6, p0, Lhgj;->hV:J

    sub-long/2addr v0, v6

    long-to-int v0, v0

    goto :goto_1

    .line 152
    :cond_5
    sget-boolean v2, Lhgj;->DBG:Z

    if-eqz v2, :cond_6

    const-string v2, "AudioTester"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Storing least delay "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lhgj;->dgu:[I

    aget v7, v7, v1

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " for "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v7, Lhgj;->dgt:[I

    aget v7, v7, v1

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_6
    iget-object v2, p0, Lhgj;->dgu:[I

    aget v2, v2, v1

    if-gt v2, v9, :cond_7

    iget-object v2, p0, Lhgj;->dgu:[I

    aget v2, v2, v5

    if-le v2, v9, :cond_9

    .line 159
    :cond_7
    const/16 v1, 0x64

    move v2, v1

    move v1, v4

    .line 165
    :goto_3
    iget-object v3, p0, Lhgj;->mSettings:Lhym;

    iget-object v4, p0, Lhgj;->mAddress:Ljava/lang/String;

    iget v5, p0, Lhgj;->dfL:I

    invoke-virtual {v3, v4, v5, v2, v1}, Lhym;->b(Ljava/lang/String;III)V

    .line 167
    :cond_8
    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 162
    :cond_9
    sget-object v2, Lhgj;->dgt:[I

    aget v2, v2, v1

    .line 163
    iget-object v3, p0, Lhgj;->dgu:[I

    aget v1, v3, v1

    goto :goto_3
.end method

.method public final getCurrentIndex()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lhgj;->dgy:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
