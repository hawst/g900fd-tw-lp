.class public final Lcwy;
.super Lcyc;
.source "PG"

# interfaces
.implements Liav;


# instance fields
.field final amX:Lcsq;

.field private final blw:Z

.field private final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcfo;

.field private final mIntentStarter:Leoj;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Lcke;Lhym;Lcfo;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 75
    iput-object p2, p0, Lcwy;->mVoiceSettings:Lhym;

    .line 76
    iput-object p3, p0, Lcwy;->mCoreSearchServices:Lcfo;

    .line 77
    iput-object p4, p0, Lcwy;->mContext:Landroid/content/Context;

    .line 78
    instance-of v0, p4, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v0, :cond_0

    .line 79
    check-cast p4, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {p4}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->wr()Leoj;

    move-result-object v0

    iput-object v0, p0, Lcwy;->mIntentStarter:Leoj;

    .line 84
    :goto_0
    iget-object v0, p0, Lcwy;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcwy;->mIntentStarter:Leoj;

    invoke-virtual {p2}, Lhym;->aFc()Ljava/lang/String;

    invoke-static {v0, p2, v1}, Lcss;->a(Landroid/content/Context;Lhym;Leoj;)Lcsq;

    move-result-object v0

    iput-object v0, p0, Lcwy;->amX:Lcsq;

    .line 86
    iget-object v0, p0, Lcwy;->amX:Lcsq;

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lhym;->aTR()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcwy;->blw:Z

    .line 87
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcwy;->mIntentStarter:Leoj;

    goto :goto_0

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static iY(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 195
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.googlequicksearchbox.CHANGE_VOICESEARCH_LANGUAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "language"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 119
    check-cast p1, Lcom/google/android/voicesearch/ui/LanguagePreference;

    .line 120
    invoke-virtual {p1, p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->a(Liav;)V

    .line 122
    invoke-virtual {p0, p1}, Lcwy;->a(Lcom/google/android/voicesearch/ui/LanguagePreference;)V

    .line 123
    return-void
.end method

.method public final a(Lcom/google/android/voicesearch/ui/LanguagePreference;)V
    .locals 5

    .prologue
    .line 174
    iget-object v0, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->c(Landroid/content/Context;Lhym;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 178
    invoke-virtual {p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a085c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 192
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v1, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTH()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185
    invoke-virtual {p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0860

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 189
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    .line 131
    iget-object v0, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3, p2}, Lgnq;->a(Ljze;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v3, p3}, Lgnq;->a(Ljze;Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lgnq;->a(Ljava/lang/String;Ljze;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, p2, p3, v3}, Lhym;->b(Ljava/lang/String;Ljava/util/List;Z)V

    const/4 v0, 0x1

    .line 134
    :goto_0
    invoke-virtual {p0, p1}, Lcwy;->a(Lcom/google/android/voicesearch/ui/LanguagePreference;)V

    .line 136
    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcwy;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ef()Lgpc;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lgpc;->acG()V

    .line 140
    invoke-virtual {v0}, Lgpc;->acF()V

    .line 141
    iget-object v0, p0, Lcwy;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcwy;->mVoiceSettings:Lhym;

    invoke-static {v0, v3}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->a(Landroid/content/Context;Lhym;)V

    .line 142
    iget-object v0, p0, Lcwy;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lbwz;->ae(Landroid/content/Context;)V

    .line 143
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-boolean v0, p0, Lcwy;->blw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcwy;->amX:Lcsq;

    invoke-interface {v0}, Lcsq;->SO()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcwy;->amX:Lcsq;

    new-instance v2, Lcwz;

    invoke-direct {v2, p0, v1}, Lcwz;-><init>(Lcwy;B)V

    invoke-interface {v0, v2}, Lcsq;->f(Lcsr;)Z

    .line 147
    :cond_0
    :goto_1
    iget-object v0, p0, Lcwy;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcwy;->iY(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    :cond_1
    return-void

    .line 144
    :cond_2
    iget-object v0, p0, Lcwy;->amX:Lcsq;

    invoke-interface {v0}, Lcsq;->SI()Z

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0}, Lcyc;->onResume()V

    .line 104
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Lcyc;->onStart()V

    .line 94
    iget-boolean v0, p0, Lcwy;->blw:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcwy;->amX:Lcsq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcsq;->a(Lcsr;)V

    .line 98
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcwy;->blw:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcwy;->amX:Lcsq;

    invoke-interface {v0}, Lcsq;->disconnect()V

    .line 113
    :cond_0
    invoke-super {p0}, Lcyc;->onStop()V

    .line 114
    return-void
.end method
