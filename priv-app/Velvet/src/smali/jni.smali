.class public final Ljni;
.super Ljsl;
.source "PG"


# static fields
.field public static final euQ:Ljsm;


# instance fields
.field private aez:I

.field private euR:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 238
    const/16 v0, 0xb

    const-class v1, Ljni;

    const v2, 0x21e5d55a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljni;->euQ:Ljsm;

    .line 244
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 270
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 271
    iput v0, p0, Ljni;->aez:I

    iput-boolean v0, p0, Ljni;->euR:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljni;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljni;->eCz:I

    .line 272
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljni;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljni;->euR:Z

    iget v0, p0, Ljni;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljni;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 285
    iget v0, p0, Ljni;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljni;->euR:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 288
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 289
    return-void
.end method

.method public final bqG()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Ljni;->euR:Z

    return v0
.end method

.method public final iL(Z)Ljni;
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljni;->euR:Z

    .line 258
    iget v0, p0, Ljni;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljni;->aez:I

    .line 259
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 293
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 294
    iget v1, p0, Ljni;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 295
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljni;->euR:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 298
    :cond_0
    return v0
.end method
