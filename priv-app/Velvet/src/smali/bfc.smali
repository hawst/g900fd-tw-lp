.class public final enum Lbfc;
.super Ljava/lang/Enum;


# static fields
.field private static enum ayA:Lbfc;

.field private static enum ayB:Lbfc;

.field private static enum ayC:Lbfc;

.field private static enum ayD:Lbfc;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static enum ayE:Lbfc;

.field public static final enum ayF:Lbfc;

.field public static final enum ayG:Lbfc;

.field public static final enum ayH:Lbfc;

.field public static final enum ayI:Lbfc;

.field public static final enum ayJ:Lbfc;

.field public static final enum ayK:Lbfc;

.field private static enum ayL:Lbfc;

.field private static enum ayM:Lbfc;

.field private static enum ayN:Lbfc;

.field private static enum ayO:Lbfc;

.field private static enum ayP:Lbfc;

.field private static enum ayQ:Lbfc;

.field private static enum ayR:Lbfc;

.field private static enum ayS:Lbfc;

.field private static enum ayT:Lbfc;

.field private static enum ayU:Lbfc;

.field private static enum ayV:Lbfc;

.field private static enum ayW:Lbfc;

.field private static enum ayX:Lbfc;

.field private static enum ayY:Lbfc;

.field private static enum ayZ:Lbfc;

.field private static enum ayo:Lbfc;

.field private static enum ayp:Lbfc;

.field private static enum ayq:Lbfc;

.field private static enum ayr:Lbfc;

.field private static enum ays:Lbfc;

.field private static enum ayt:Lbfc;

.field private static enum ayu:Lbfc;

.field private static enum ayv:Lbfc;

.field private static enum ayw:Lbfc;

.field private static enum ayx:Lbfc;

.field private static enum ayy:Lbfc;

.field private static enum ayz:Lbfc;

.field private static enum aza:Lbfc;

.field private static enum azb:Lbfc;

.field private static enum azc:Lbfc;

.field private static enum azd:Lbfc;

.field private static enum aze:Lbfc;

.field private static enum azf:Lbfc;

.field private static enum azg:Lbfc;

.field private static final synthetic azi:[Lbfc;


# instance fields
.field private final azh:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lbfc;

    const-string v1, "SUCCESS"

    const-string v2, "Ok"

    invoke-direct {v0, v1, v4, v2}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayo:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "BAD_AUTHENTICATION"

    const-string v2, "BadAuthentication"

    invoke-direct {v0, v1, v5, v2}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayp:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NEEDS_2F"

    const-string v2, "InvalidSecondFactor"

    invoke-direct {v0, v1, v6, v2}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayq:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NOT_VERIFIED"

    const-string v2, "NotVerified"

    invoke-direct {v0, v1, v7, v2}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayr:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "TERMS_NOT_AGREED"

    const-string v2, "TermsNotAgreed"

    invoke-direct {v0, v1, v8, v2}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ays:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayt:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x6

    const-string v3, "UNKNOWN_ERR"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayu:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "ACCOUNT_DELETED"

    const/4 v2, 0x7

    const-string v3, "AccountDeleted"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayv:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "ACCOUNT_DISABLED"

    const/16 v2, 0x8

    const-string v3, "AccountDisabled"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayw:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "SERVICE_DISABLED"

    const/16 v2, 0x9

    const-string v3, "ServiceDisabled"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayx:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "SERVICE_UNAVAILABLE"

    const/16 v2, 0xa

    const-string v3, "ServiceUnavailable"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayy:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "CAPTCHA"

    const/16 v2, 0xb

    const-string v3, "CaptchaRequired"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayz:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0xc

    const-string v3, "NetworkError"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayA:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0xd

    const-string v3, "UserCancel"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayB:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "PERMISSION_DENIED"

    const/16 v2, 0xe

    const-string v3, "PermissionDenied"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayC:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DEVICE_MANAGEMENT_REQUIRED"

    const/16 v2, 0xf

    const-string v3, "DeviceManagementRequiredOrSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayD:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_INTERNAL_ERROR"

    const/16 v2, 0x10

    const-string v3, "DeviceManagementInternalError"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayE:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_SYNC_DISABLED"

    const/16 v2, 0x11

    const-string v3, "DeviceManagementSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayF:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_ADMIN_BLOCKED"

    const/16 v2, 0x12

    const-string v3, "DeviceManagementAdminBlocked"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayG:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_ADMIN_PENDING_APPROVAL"

    const/16 v2, 0x13

    const-string v3, "DeviceManagementAdminPendingApproval"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayH:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_STALE_SYNC_REQUIRED"

    const/16 v2, 0x14

    const-string v3, "DeviceManagementStaleSyncRequired"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayI:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_DEACTIVATED"

    const/16 v2, 0x15

    const-string v3, "DeviceManagementDeactivated"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayJ:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DM_REQUIRED"

    const/16 v2, 0x16

    const-string v3, "DeviceManagementRequired"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayK:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "CLIENT_LOGIN_DISABLED"

    const/16 v2, 0x17

    const-string v3, "ClientLoginDisabled"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayL:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NEED_PERMISSION"

    const/16 v2, 0x18

    const-string v3, "NeedPermission"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayM:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "BAD_PASSWORD"

    const/16 v2, 0x19

    const-string v3, "WeakPassword"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayN:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "ALREADY_HAS_GMAIL"

    const/16 v2, 0x1a

    const-string v3, "ALREADY_HAS_GMAIL"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayO:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "BAD_REQUEST"

    const/16 v2, 0x1b

    const-string v3, "BadRequest"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayP:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "BAD_USERNAME"

    const/16 v2, 0x1c

    const-string v3, "BadUsername"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayQ:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "LOGIN_FAIL"

    const/16 v2, 0x1d

    const-string v3, "LoginFail"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayR:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NOT_LOGGED_IN"

    const/16 v2, 0x1e

    const-string v3, "NotLoggedIn"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayS:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NO_GMAIL"

    const/16 v2, 0x1f

    const-string v3, "NoGmail"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayT:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "REQUEST_DENIED"

    const/16 v2, 0x20

    const-string v3, "RequestDenied"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayU:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "SERVER_ERROR"

    const/16 v2, 0x21

    const-string v3, "ServerError"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayV:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "USERNAME_UNAVAILABLE"

    const/16 v2, 0x22

    const-string v3, "UsernameUnavailable"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayW:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0x23

    const-string v3, "DeletedGmail"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayX:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "SOCKET_TIMEOUT"

    const/16 v2, 0x24

    const-string v3, "SocketTimeout"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayY:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "EXISTING_USERNAME"

    const/16 v2, 0x25

    const-string v3, "ExistingUsername"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->ayZ:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "NEEDS_BROWSER"

    const/16 v2, 0x26

    const-string v3, "NeedsBrowser"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->aza:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "GPLUS_OTHER"

    const/16 v2, 0x27

    const-string v3, "GPlusOther"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->azb:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "GPLUS_NICKNAME"

    const/16 v2, 0x28

    const-string v3, "GPlusNickname"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->azc:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "GPLUS_INVALID_CHAR"

    const/16 v2, 0x29

    const-string v3, "GPlusInvalidChar"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->azd:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "GPLUS_INTERSTITIAL"

    const/16 v2, 0x2a

    const-string v3, "GPlusInterstitial"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->aze:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "GPLUS_PROFILE_ERROR"

    const/16 v2, 0x2b

    const-string v3, "ProfileUpgradeError"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->azf:Lbfc;

    new-instance v0, Lbfc;

    const-string v1, "INVALID_SCOPE"

    const/16 v2, 0x2c

    const-string v3, "INVALID_SCOPE"

    invoke-direct {v0, v1, v2, v3}, Lbfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbfc;->azg:Lbfc;

    const/16 v0, 0x2d

    new-array v0, v0, [Lbfc;

    sget-object v1, Lbfc;->ayo:Lbfc;

    aput-object v1, v0, v4

    sget-object v1, Lbfc;->ayp:Lbfc;

    aput-object v1, v0, v5

    sget-object v1, Lbfc;->ayq:Lbfc;

    aput-object v1, v0, v6

    sget-object v1, Lbfc;->ayr:Lbfc;

    aput-object v1, v0, v7

    sget-object v1, Lbfc;->ays:Lbfc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbfc;->ayt:Lbfc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbfc;->ayu:Lbfc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbfc;->ayv:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbfc;->ayw:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbfc;->ayx:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbfc;->ayy:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbfc;->ayz:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbfc;->ayA:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbfc;->ayB:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbfc;->ayC:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbfc;->ayD:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbfc;->ayE:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbfc;->ayF:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lbfc;->ayG:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lbfc;->ayH:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lbfc;->ayI:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lbfc;->ayJ:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lbfc;->ayK:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lbfc;->ayL:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lbfc;->ayM:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lbfc;->ayN:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lbfc;->ayO:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lbfc;->ayP:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lbfc;->ayQ:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lbfc;->ayR:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lbfc;->ayS:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lbfc;->ayT:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lbfc;->ayU:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lbfc;->ayV:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lbfc;->ayW:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lbfc;->ayX:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lbfc;->ayY:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lbfc;->ayZ:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lbfc;->aza:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lbfc;->azb:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lbfc;->azc:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lbfc;->azd:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lbfc;->aze:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lbfc;->azf:Lbfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lbfc;->azg:Lbfc;

    aput-object v2, v0, v1

    sput-object v0, Lbfc;->azi:[Lbfc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lbfc;->azh:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbfc;
    .locals 1

    const-class v0, Lbfc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbfc;

    return-object v0
.end method

.method public static values()[Lbfc;
    .locals 1

    sget-object v0, Lbfc;->azi:[Lbfc;

    invoke-virtual {v0}, [Lbfc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbfc;

    return-object v0
.end method


# virtual methods
.method public final xS()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbfc;->azh:Ljava/lang/String;

    return-object v0
.end method
