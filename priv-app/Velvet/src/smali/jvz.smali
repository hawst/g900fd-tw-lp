.class public final Ljvz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eIv:[Ljvz;


# instance fields
.field private aez:I

.field private eIw:Z

.field private eIx:Ljava/lang/String;

.field private eIy:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1800
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1801
    iput v0, p0, Ljvz;->aez:I

    iput-boolean v0, p0, Ljvz;->eIw:Z

    const-string v0, ""

    iput-object v0, p0, Ljvz;->eIx:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljvz;->eIy:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljvz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvz;->eCz:I

    .line 1802
    return-void
.end method

.method public static bvh()[Ljvz;
    .locals 2

    .prologue
    .line 1727
    sget-object v0, Ljvz;->eIv:[Ljvz;

    if-nez v0, :cond_1

    .line 1728
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1730
    :try_start_0
    sget-object v0, Ljvz;->eIv:[Ljvz;

    if-nez v0, :cond_0

    .line 1731
    const/4 v0, 0x0

    new-array v0, v0, [Ljvz;

    sput-object v0, Ljvz;->eIv:[Ljvz;

    .line 1733
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1735
    :cond_1
    sget-object v0, Ljvz;->eIv:[Ljvz;

    return-object v0

    .line 1733
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1721
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljvz;->eIw:Z

    iget v0, p0, Ljvz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljvz;->eIx:Ljava/lang/String;

    iget v0, p0, Ljvz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljvz;->eIy:F

    iget v0, p0, Ljvz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1817
    iget v0, p0, Ljvz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1818
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljvz;->eIw:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1820
    :cond_0
    iget v0, p0, Ljvz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1821
    const/4 v0, 0x2

    iget-object v1, p0, Ljvz;->eIx:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1823
    :cond_1
    iget v0, p0, Ljvz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1824
    const/4 v0, 0x3

    iget v1, p0, Ljvz;->eIy:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 1826
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1827
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1831
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1832
    iget v1, p0, Ljvz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1833
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljvz;->eIw:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1836
    :cond_0
    iget v1, p0, Ljvz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1837
    const/4 v1, 0x2

    iget-object v2, p0, Ljvz;->eIx:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1840
    :cond_1
    iget v1, p0, Ljvz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1841
    const/4 v1, 0x3

    iget v2, p0, Ljvz;->eIy:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1844
    :cond_2
    return v0
.end method
