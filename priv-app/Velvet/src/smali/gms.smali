.class public final Lgms;
.super Lgmq;
.source "PG"


# instance fields
.field private final anB:I

.field private final cPs:Z

.field private final cPt:Z

.field private final cPu:Z

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Lgdo;IZZZ)V
    .locals 1

    .prologue
    .line 19
    const-string v0, "RecognizerSessionParamsBuilderTask"

    invoke-direct {p0, v0}, Lgmq;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lgms;->mSpeechSettings:Lgdo;

    .line 21
    iput p2, p0, Lgms;->anB:I

    .line 22
    iput-boolean p3, p0, Lgms;->cPs:Z

    .line 23
    iput-boolean p4, p0, Lgms;->cPt:Z

    .line 24
    iput-boolean p5, p0, Lgms;->cPu:Z

    .line 25
    return-void
.end method


# virtual methods
.method protected final synthetic aHr()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7
    new-instance v0, Ljwb;

    invoke-direct {v0}, Ljwb;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljwb;->sE(I)Ljwb;

    iget-boolean v1, p0, Lgms;->cPu:Z

    invoke-virtual {v0, v1}, Ljwb;->jc(Z)Ljwb;

    iget v1, p0, Lgms;->anB:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Ljwb;->ab(F)Ljwb;

    iget-boolean v1, p0, Lgms;->cPs:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Ljwb;->je(Z)Ljwb;

    :cond_0
    iget-boolean v1, p0, Lgms;->cPt:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgms;->mSpeechSettings:Lgdo;

    invoke-interface {v1}, Lgdo;->aER()Ljze;

    move-result-object v1

    invoke-virtual {v0, v2}, Ljwb;->jd(Z)Ljwb;

    invoke-static {v1}, Lgmu;->b(Ljze;)Ljtl;

    move-result-object v1

    iput-object v1, v0, Ljwb;->eIB:Ljtl;

    :cond_1
    return-object v0
.end method
