.class public final Lfgk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lffk;


# instance fields
.field private final mEntry:Lizj;


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lfgk;->mEntry:Lizj;

    .line 21
    return-void
.end method


# virtual methods
.method public final aK(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lfgk;->mEntry:Lizj;

    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->b(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    .line 30
    const-string v1, "actions_to_execute"

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 32
    const-string v1, "invalidate_after_action"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 33
    return-object v0
.end method

.method public final aL(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lfgk;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfgk;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lfgk;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhP()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a03e9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aif()I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f02021b

    return v0
.end method

.method public final aza()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "SNOOZE_REMINDER"

    return-object v0
.end method

.method public final azb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "service"

    return-object v0
.end method

.method public final azc()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public final isActive()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method
