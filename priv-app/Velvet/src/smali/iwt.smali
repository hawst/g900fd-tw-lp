.class public final Liwt;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dMu:[Ljde;

.field private dMv:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5093
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5094
    const/4 v0, 0x0

    iput v0, p0, Liwt;->aez:I

    invoke-static {}, Ljde;->bgW()[Ljde;

    move-result-object v0

    iput-object v0, p0, Liwt;->dMu:[Ljde;

    const/4 v0, 0x1

    iput v0, p0, Liwt;->dMv:I

    const/4 v0, 0x0

    iput-object v0, p0, Liwt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwt;->eCz:I

    .line 5095
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5048
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Liwt;->dMu:[Ljde;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljde;

    if-eqz v0, :cond_1

    iget-object v3, p0, Liwt;->dMu:[Ljde;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljde;

    invoke-direct {v3}, Ljde;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Liwt;->dMu:[Ljde;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljde;

    invoke-direct {v3}, Ljde;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Liwt;->dMu:[Ljde;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Liwt;->dMv:I

    iget v0, p0, Liwt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwt;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 5109
    iget-object v0, p0, Liwt;->dMu:[Ljde;

    if-eqz v0, :cond_1

    iget-object v0, p0, Liwt;->dMu:[Ljde;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5110
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Liwt;->dMu:[Ljde;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 5111
    iget-object v1, p0, Liwt;->dMu:[Ljde;

    aget-object v1, v1, v0

    .line 5112
    if-eqz v1, :cond_0

    .line 5113
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 5110
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5117
    :cond_1
    iget v0, p0, Liwt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 5118
    const/4 v0, 0x2

    iget v1, p0, Liwt;->dMv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5120
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5121
    return-void
.end method

.method public final aZy()I
    .locals 1

    .prologue
    .line 5077
    iget v0, p0, Liwt;->dMv:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 5125
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 5126
    iget-object v0, p0, Liwt;->dMu:[Ljde;

    if-eqz v0, :cond_1

    iget-object v0, p0, Liwt;->dMu:[Ljde;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5127
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Liwt;->dMu:[Ljde;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 5128
    iget-object v2, p0, Liwt;->dMu:[Ljde;

    aget-object v2, v2, v0

    .line 5129
    if-eqz v2, :cond_0

    .line 5130
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 5127
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5135
    :cond_1
    iget v0, p0, Liwt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 5136
    const/4 v0, 0x2

    iget v2, p0, Liwt;->dMv:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 5139
    :cond_2
    return v1
.end method

.method public final nh(I)Liwt;
    .locals 1

    .prologue
    .line 5080
    iput p1, p0, Liwt;->dMv:I

    .line 5081
    iget v0, p0, Liwt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwt;->aez:I

    .line 5082
    return-object p0
.end method
