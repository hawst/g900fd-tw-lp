.class public final Ljth;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eDq:I

.field private eDr:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 69
    iput v0, p0, Ljth;->aez:I

    iput v0, p0, Ljth;->eDq:I

    iput v0, p0, Ljth;->eDr:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljth;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljth;->eCz:I

    .line 70
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljth;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljth;->eDq:I

    iget v0, p0, Ljth;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljth;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljth;->eDr:I

    iget v0, p0, Ljth;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljth;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Ljth;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    iget v1, p0, Ljth;->eDq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 87
    :cond_0
    iget v0, p0, Ljth;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 88
    const/4 v0, 0x2

    iget v1, p0, Ljth;->eDr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 90
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 91
    return-void
.end method

.method public final bur()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Ljth;->eDq:I

    return v0
.end method

.method public final bus()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Ljth;->eDr:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 96
    iget v1, p0, Ljth;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 97
    const/4 v1, 0x1

    iget v2, p0, Ljth;->eDq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_0
    iget v1, p0, Ljth;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 101
    const/4 v1, 0x2

    iget v2, p0, Ljth;->eDr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_1
    return v0
.end method
