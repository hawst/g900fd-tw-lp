.class final Lhbd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhbx;


# instance fields
.field private synthetic dbv:Lhax;

.field private final dbx:I

.field private final dby:Lhbq;


# direct methods
.method public constructor <init>(Lhax;ILhbq;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lhbd;->dbv:Lhax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 427
    iput p2, p0, Lhbd;->dbx:I

    .line 428
    iput-object p3, p0, Lhbd;->dby:Lhbq;

    .line 429
    return-void
.end method

.method private aNg()Z
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lhbd;->dby:Lhbq;

    iget-object v1, p0, Lhbd;->dbv:Lhax;

    iget-object v1, v1, Lhax;->dbn:Lhav;

    iget-object v1, v1, Lhav;->dbm:Lhbq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final R(Landroid/net/Uri;)V
    .locals 5

    .prologue
    const v4, 0x7f0a05df

    .line 456
    invoke-direct {p0}, Lhbd;->aNg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    :try_start_0
    iget-object v1, v0, Lhax;->mUrlHelper:Lcpn;

    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2, p1}, Lcpn;->a(Lcom/google/android/shared/search/Query;Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, v0, Lhax;->mAppContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lhgn;->a(Landroid/content/Context;Lcom/google/android/shared/search/Query;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, v0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v2, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->startActivity(Landroid/content/Intent;)V

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    iget-object v1, v0, Lhax;->mUrlHelper:Lcpn;

    invoke-virtual {v1, p1}, Lcpn;->p(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, v0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v2, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "Velvet.InAppWebPagePresenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No activity found to open: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->kP(I)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v1, "Velvet.InAppWebPagePresenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, v4}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->kP(I)V

    goto :goto_0
.end method

.method public final aMZ()V
    .locals 1

    .prologue
    .line 440
    invoke-direct {p0}, Lhbd;->aNg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    iget-object v0, v0, Lhax;->dbn:Lhav;

    invoke-virtual {v0}, Lhav;->aMZ()V

    .line 443
    :cond_0
    return-void
.end method

.method public final aNf()V
    .locals 1

    .prologue
    .line 475
    invoke-direct {p0}, Lhbd;->aNg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    iget-object v0, v0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->finish()V

    .line 478
    :cond_0
    return-void
.end method

.method public final aV(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 470
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    iget-object v0, v0, Lhax;->dbs:Lhbl;

    new-instance v1, Ldls;

    const-string v2, "[%d] %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lhbd;->dbx:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-direct {v1, v2, v3}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lhbl;->add(Ljava/lang/Object;)V

    .line 471
    return-void
.end method

.method public final aW(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 447
    invoke-direct {p0}, Lhbd;->aNg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    invoke-virtual {v0}, Lhax;->aNc()V

    .line 449
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    new-instance v1, Ldls;

    const-string v2, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v1, v2, v3}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v1, v0, Lhax;->dbt:Ldls;

    .line 450
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    iget-object v0, v0, Lhax;->dbs:Lhbl;

    iget-object v1, p0, Lhbd;->dbv:Lhax;

    iget-object v1, v1, Lhax;->dbt:Ldls;

    invoke-virtual {v0, v1}, Lhbl;->add(Ljava/lang/Object;)V

    .line 452
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V
    .locals 2

    .prologue
    .line 463
    invoke-direct {p0}, Lhbd;->aNg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    iget-object v1, v0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v1, p1}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->c(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    invoke-virtual {v0}, Lhax;->aNe()V

    .line 466
    :cond_0
    return-void
.end method

.method public final pageReady()V
    .locals 1

    .prologue
    .line 433
    invoke-direct {p0}, Lhbd;->aNg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lhbd;->dbv:Lhax;

    invoke-virtual {v0}, Lhax;->aNd()V

    iget-object v0, v0, Lhax;->dbn:Lhav;

    invoke-virtual {v0}, Lhav;->aMX()V

    .line 436
    :cond_0
    return-void
.end method
