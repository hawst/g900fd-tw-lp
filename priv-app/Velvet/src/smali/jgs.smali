.class public final Ljgs;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ekU:[Ljgs;


# instance fields
.field private aez:I

.field private eiQ:Ljava/lang/String;

.field private ekV:Ljgr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50974
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50975
    const/4 v0, 0x0

    iput v0, p0, Ljgs;->aez:I

    iput-object v1, p0, Ljgs;->ekV:Ljgr;

    const-string v0, ""

    iput-object v0, p0, Ljgs;->eiQ:Ljava/lang/String;

    iput-object v1, p0, Ljgs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgs;->eCz:I

    .line 50976
    return-void
.end method

.method public static blj()[Ljgs;
    .locals 2

    .prologue
    .line 50936
    sget-object v0, Ljgs;->ekU:[Ljgs;

    if-nez v0, :cond_1

    .line 50937
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 50939
    :try_start_0
    sget-object v0, Ljgs;->ekU:[Ljgs;

    if-nez v0, :cond_0

    .line 50940
    const/4 v0, 0x0

    new-array v0, v0, [Ljgs;

    sput-object v0, Ljgs;->ekU:[Ljgs;

    .line 50942
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50944
    :cond_1
    sget-object v0, Ljgs;->ekU:[Ljgs;

    return-object v0

    .line 50942
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 50930
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljgs;->ekV:Ljgr;

    if-nez v0, :cond_1

    new-instance v0, Ljgr;

    invoke-direct {v0}, Ljgr;-><init>()V

    iput-object v0, p0, Ljgs;->ekV:Ljgr;

    :cond_1
    iget-object v0, p0, Ljgs;->ekV:Ljgr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgs;->eiQ:Ljava/lang/String;

    iget v0, p0, Ljgs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgs;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 50990
    iget-object v0, p0, Ljgs;->ekV:Ljgr;

    if-eqz v0, :cond_0

    .line 50991
    const/4 v0, 0x1

    iget-object v1, p0, Ljgs;->ekV:Ljgr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 50993
    :cond_0
    iget v0, p0, Ljgs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 50994
    const/4 v0, 0x2

    iget-object v1, p0, Ljgs;->eiQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 50996
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 50997
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 51001
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 51002
    iget-object v1, p0, Ljgs;->ekV:Ljgr;

    if-eqz v1, :cond_0

    .line 51003
    const/4 v1, 0x1

    iget-object v2, p0, Ljgs;->ekV:Ljgr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51006
    :cond_0
    iget v1, p0, Ljgs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 51007
    const/4 v1, 0x2

    iget-object v2, p0, Ljgs;->eiQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51010
    :cond_1
    return v0
.end method
