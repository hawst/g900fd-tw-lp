.class public Lgce;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgda;


# instance fields
.field final aSr:Ljava/util/concurrent/ScheduledExecutorService;

.field aTn:Z

.field protected bqM:Z

.field cFA:J

.field cFB:Ljava/util/concurrent/ScheduledFuture;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cFC:Ljava/util/concurrent/ScheduledFuture;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cFD:Lggg;

.field private cFs:I

.field private cFt:I

.field protected final cFu:J

.field private final cFv:Z

.field private final cFw:Z

.field private cFx:Z

.field final cFy:J

.field protected cFz:I

.field protected final mClock:Lemp;

.field final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mRecognitionDispatcher:Lgil;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mStateMachine:Leqx;


# direct methods
.method public constructor <init>(IIJZLgil;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lgdo;)V
    .locals 5

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-string v0, "DefaultRecognitionState"

    sget-object v1, Lgcj;->cFI:Lgcj;

    invoke-static {v0, v1}, Leqx;->a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFI:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFJ:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFI:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFK:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFI:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFL:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFJ:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFK:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFI:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFM:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFJ:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFM:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFK:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFM:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgcj;->cFL:Lgcj;

    const/4 v2, 0x1

    new-array v2, v2, [Lgcj;

    const/4 v3, 0x0

    sget-object v4, Lgcj;->cFM:Lgcj;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Leqy;->chJ:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Leqy;->chN:Z

    const/4 v1, 0x0

    iput-boolean v1, v0, Leqy;->chK:Z

    invoke-virtual {v0}, Leqy;->avx()Leqx;

    move-result-object v0

    iput-object v0, p0, Lgce;->mStateMachine:Leqx;

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lgce;->cFz:I

    .line 110
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgce;->cFA:J

    .line 130
    iput p1, p0, Lgce;->cFs:I

    .line 131
    iput p2, p0, Lgce;->cFt:I

    .line 132
    iput-wide p3, p0, Lgce;->cFu:J

    .line 133
    iput-boolean p5, p0, Lgce;->cFv:Z

    .line 134
    iput-object p6, p0, Lgce;->mRecognitionDispatcher:Lgil;

    .line 135
    iput-object p7, p0, Lgce;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 136
    iput-object p8, p0, Lgce;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    .line 137
    iput-object p9, p0, Lgce;->mClock:Lemp;

    .line 138
    invoke-interface {p10}, Lgdo;->Hr()Z

    move-result v0

    iput-boolean v0, p0, Lgce;->cFw:Z

    .line 139
    invoke-interface {p10}, Lgdo;->Hq()Z

    move-result v0

    iput-boolean v0, p0, Lgce;->cFx:Z

    .line 140
    invoke-interface {p10}, Lgdo;->Hs()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lgce;->cFy:J

    .line 142
    return-void
.end method

.method private kd(I)Lgcj;
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lgce;->cFs:I

    if-ne p1, v0, :cond_0

    .line 309
    sget-object v0, Lgcj;->cFK:Lgcj;

    .line 311
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lgcj;->cFL:Lgcj;

    goto :goto_0
.end method


# virtual methods
.method public a(Lgco;)V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method public final a(Lggg;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lgce;->cFD:Lggg;

    .line 147
    return-void
.end method

.method public final aEK()V
    .locals 2

    .prologue
    .line 233
    iget-boolean v0, p0, Lgce;->bqM:Z

    if-nez v0, :cond_0

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgce;->bqM:Z

    .line 235
    iget-boolean v0, p0, Lgce;->cFv:Z

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lgce;->mRecognitionDispatcher:Lgil;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lgil;->km(I)V

    .line 243
    :cond_0
    return-void
.end method

.method public final aEL()V
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lgce;->cFD:Lggg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v1, Lgcj;->cFI:Lgcj;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v1, Lgcj;->cFJ:Lgcj;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    :cond_0
    iget v0, p0, Lgce;->cFs:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 348
    iget-object v0, p0, Lgce;->cFD:Lggg;

    new-instance v1, Lehy;

    invoke-direct {v1}, Lehy;-><init>()V

    invoke-interface {v0, v1}, Lggg;->b(Leiq;)V

    .line 354
    :cond_1
    :goto_0
    return-void

    .line 349
    :cond_2
    iget v0, p0, Lgce;->cFs:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 350
    iget-object v0, p0, Lgce;->cFD:Lggg;

    new-instance v1, Leie;

    const v2, 0x10017

    invoke-direct {v1, v2}, Leie;-><init>(I)V

    invoke-interface {v0, v1}, Lggg;->b(Leiq;)V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 151
    iget-object v0, p0, Lgce;->cFB:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lgce;->cFB:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 154
    :cond_0
    iget-object v0, p0, Lgce;->cFC:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lgce;->cFC:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 157
    :cond_1
    iput-boolean v1, p0, Lgce;->aTn:Z

    .line 158
    return-void
.end method

.method public final jX(I)I
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 162
    invoke-virtual {p0, p1}, Lgce;->kc(I)V

    .line 163
    iget v2, p0, Lgce;->cFs:I

    if-eq p1, v2, :cond_1

    iget v2, p0, Lgce;->cFt:I

    if-eq p1, v2, :cond_1

    .line 164
    const-string v2, "DefaultRecognitionState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected response from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 168
    :cond_1
    iget-object v2, p0, Lgce;->mStateMachine:Leqx;

    invoke-direct {p0, p1}, Lgce;->kd(I)Lgcj;

    move-result-object v3

    invoke-virtual {v2, v3}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 169
    goto :goto_0

    .line 170
    :cond_2
    iget-object v2, p0, Lgce;->mStateMachine:Leqx;

    sget-object v3, Lgcj;->cFI:Lgcj;

    invoke-virtual {v2, v3}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lgce;->mStateMachine:Leqx;

    sget-object v3, Lgcj;->cFJ:Lgcj;

    invoke-virtual {v2, v3}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    :cond_3
    iget v0, p0, Lgce;->cFs:I

    if-ne p1, v0, :cond_4

    .line 173
    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v2, Lgcj;->cFK:Lgcj;

    invoke-virtual {v0, v2}, Leqx;->a(Ljava/lang/Enum;)V

    move v0, v1

    .line 174
    goto :goto_0

    .line 176
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public jY(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    .line 185
    invoke-virtual {p0, p1}, Lgce;->kc(I)V

    .line 186
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    iget-boolean v2, p0, Lgce;->cFw:Z

    :goto_0
    if-nez v2, :cond_3

    .line 204
    :cond_0
    :goto_1
    return v0

    .line 186
    :cond_1
    if-ne p1, v0, :cond_2

    iget-boolean v2, p0, Lgce;->cFx:Z

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    .line 193
    :cond_3
    iget v2, p0, Lgce;->cFz:I

    if-nez v2, :cond_5

    .line 194
    iput p1, p0, Lgce;->cFz:I

    .line 195
    iget v2, p0, Lgce;->cFz:I

    if-ne v2, v0, :cond_4

    .line 198
    new-instance v0, Lgch;

    invoke-direct {v0, p0}, Lgch;-><init>(Lgce;)V

    invoke-virtual {v0}, Lgch;->run()V

    :cond_4
    move v0, v1

    .line 200
    goto :goto_1

    .line 201
    :cond_5
    iget v2, p0, Lgce;->cFz:I

    if-ne p1, v2, :cond_0

    move v0, v1

    .line 202
    goto :goto_1
.end method

.method public jZ(I)I
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Lgce;->kc(I)V

    .line 213
    iget v0, p0, Lgce;->cFz:I

    if-eqz v0, :cond_0

    iget v0, p0, Lgce;->cFz:I

    if-ne v0, p1, :cond_1

    .line 215
    :cond_0
    const/4 v0, 0x0

    .line 217
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ka(I)I
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0, p1}, Lgce;->kb(I)I

    move-result v0

    return v0
.end method

.method public final kb(I)I
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 257
    iget v2, p0, Lgce;->cFs:I

    if-eq p1, v2, :cond_1

    iget v2, p0, Lgce;->cFt:I

    if-eq p1, v2, :cond_1

    .line 258
    const-string v2, "DefaultRecognitionState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected error from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 262
    :cond_1
    iget-object v2, p0, Lgce;->mStateMachine:Leqx;

    sget-object v3, Lgcj;->cFI:Lgcj;

    invoke-virtual {v2, v3}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 263
    iget v2, p0, Lgce;->cFs:I

    if-ne p1, v2, :cond_3

    .line 264
    iget v0, p0, Lgce;->cFt:I

    if-eqz v0, :cond_2

    .line 265
    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v1, Lgcj;->cFL:Lgcj;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    .line 266
    const/4 v0, 0x1

    goto :goto_0

    .line 268
    :cond_2
    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v2, Lgcj;->cFM:Lgcj;

    invoke-virtual {v0, v2}, Leqx;->a(Ljava/lang/Enum;)V

    move v0, v1

    .line 269
    goto :goto_0

    .line 272
    :cond_3
    iget-object v1, p0, Lgce;->mStateMachine:Leqx;

    sget-object v2, Lgcj;->cFJ:Lgcj;

    invoke-virtual {v1, v2}, Leqx;->a(Ljava/lang/Enum;)V

    goto :goto_0

    .line 275
    :cond_4
    iget-object v2, p0, Lgce;->mStateMachine:Leqx;

    sget-object v3, Lgcj;->cFJ:Lgcj;

    invoke-virtual {v2, v3}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 276
    iget v2, p0, Lgce;->cFs:I

    if-ne p1, v2, :cond_5

    .line 277
    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v2, Lgcj;->cFM:Lgcj;

    invoke-virtual {v0, v2}, Leqx;->a(Ljava/lang/Enum;)V

    move v0, v1

    .line 278
    goto :goto_0

    .line 280
    :cond_5
    const-string v2, "DefaultRecognitionState"

    const-string v3, "Repeated error from secondary engine."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 283
    :cond_6
    iget-object v2, p0, Lgce;->mStateMachine:Leqx;

    invoke-direct {p0, p1}, Lgce;->kd(I)Lgcj;

    move-result-object v3

    invoke-virtual {v2, v3}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v2, Lgcj;->cFM:Lgcj;

    invoke-virtual {v0, v2}, Leqx;->a(Ljava/lang/Enum;)V

    move v0, v1

    .line 285
    goto :goto_0
.end method

.method public final kc(I)V
    .locals 2

    .prologue
    .line 297
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 298
    iget-object v0, p0, Lgce;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgce;->cFA:J

    .line 300
    :cond_0
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 5

    .prologue
    .line 247
    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v1, Lgcj;->cFI:Lgcj;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgce;->mStateMachine:Leqx;

    sget-object v1, Lgcj;->cFJ:Lgcj;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lgce;->cFC:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgce;->cFC:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    :cond_1
    iget-object v0, p0, Lgce;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lgcf;

    const-string v2, "Timeout"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lgcf;-><init>(Lgce;Ljava/lang/String;[I)V

    iget-wide v2, p0, Lgce;->cFu:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lgce;->cFC:Ljava/util/concurrent/ScheduledFuture;

    .line 248
    :cond_2
    return-void
.end method
