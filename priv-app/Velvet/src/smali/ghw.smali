.class public final Lghw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mRelationshipNameLookup:Leai;


# direct methods
.method public constructor <init>(Leai;)V
    .locals 0
    .param p1    # Leai;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lghw;->mRelationshipNameLookup:Leai;

    .line 37
    return-void
.end method

.method private static d(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 209
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object p0

    .line 213
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 214
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 215
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 216
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 220
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move-object p0, v1

    .line 221
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/DocumentResults;)Ljava/util/List;
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/gms/appdatasearch/DocumentResults;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 52
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 53
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 54
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 55
    const-string v2, "name"

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/appdatasearch/DocumentResults;->l(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 57
    const-string v2, "lookup_key"

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/appdatasearch/DocumentResults;->l(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 59
    const-string v2, "nickname"

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/appdatasearch/DocumentResults;->l(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 67
    new-instance v1, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/search/shared/contact/Person;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 68
    invoke-virtual {p0, v1, v9}, Lghw;->a(Lcom/google/android/search/shared/contact/Person;Ljava/lang/String;)V

    .line 69
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_0
    return-object v7
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;)Ljava/util/Map;
    .locals 10
    .param p1    # Lcom/google/android/gms/appdatasearch/SearchResults;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 127
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/SearchResults;->xf()Lbci;

    move-result-object v8

    .line 128
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v9

    .line 129
    :cond_0
    :goto_0
    invoke-virtual {v8}, Lbci;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    invoke-virtual {v8}, Lbci;->xj()Lbch;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lbch;->xg()Ljava/lang/String;

    move-result-object v1

    .line 132
    const-string v2, "contact_id"

    invoke-virtual {v0, v2}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 136
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 141
    const-string v5, "data"

    invoke-virtual {v0, v5}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 142
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 144
    const-string v5, "label"

    invoke-virtual {v0, v5}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 152
    invoke-static {v1}, Ldzb;->kx(Ljava/lang/String;)Ldzb;

    move-result-object v1

    .line 153
    new-instance v0, Lcom/google/android/search/shared/contact/Contact;

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 162
    if-nez v1, :cond_1

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v9, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    const-string v1, "IcingContactExtractor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Could not parse contact id: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168
    :cond_2
    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 169
    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 170
    invoke-static {v1}, Lcom/google/android/search/shared/contact/Contact;->C(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 171
    if-eqz p2, :cond_3

    .line 172
    invoke-static {v1, p2}, Lghw;->d(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 174
    :cond_3
    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 176
    :cond_4
    return-object v9
.end method

.method public final a(Lcom/google/android/search/shared/contact/Person;Ljava/lang/String;)V
    .locals 9
    .param p1    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 182
    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    return-void

    .line 188
    :cond_1
    const-string v0, "\u0085"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 189
    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_3

    aget-object v7, v5, v0

    .line 190
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 191
    iget-object v8, p0, Lghw;->mRelationshipNameLookup:Leai;

    invoke-interface {v8, v7}, Leai;->kH(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 192
    iget-object v8, p0, Lghw;->mRelationshipNameLookup:Leai;

    invoke-interface {v8, v7}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Relationship;)V

    .line 189
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 194
    :cond_2
    invoke-virtual {p1, v7}, Lcom/google/android/search/shared/contact/Person;->kB(Ljava/lang/String;)V

    goto :goto_2

    .line 188
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/appdatasearch/SearchResults;)Ljava/util/List;
    .locals 9
    .param p1    # Lcom/google/android/gms/appdatasearch/SearchResults;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 86
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/SearchResults;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbch;

    .line 88
    const-wide/16 v2, 0x0

    .line 90
    :try_start_0
    invoke-virtual {v0}, Lbch;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 94
    :goto_1
    const-string v1, "name"

    invoke-virtual {v0, v1}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 96
    const-string v1, "lookup_key"

    invoke-virtual {v0, v1}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 98
    const-string v1, "nickname"

    invoke-virtual {v0, v1}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/google/android/search/shared/contact/Person;

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/search/shared/contact/Person;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 107
    invoke-virtual {p0, v1, v0}, Lghw;->a(Lcom/google/android/search/shared/contact/Person;Ljava/lang/String;)V

    .line 108
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    const-string v4, "IcingContactExtractor"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not parse contact id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbch;->getUri()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 110
    :cond_0
    return-object v7
.end method
