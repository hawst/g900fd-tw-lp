.class public final Lexf;
.super Lewj;
.source "PG"

# interfaces
.implements Lewu;
.implements Lhpp;


# instance fields
.field cld:Lexj;

.field private cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

.field clf:Lexl;

.field mEntry:Lizj;

.field mPresenter:Lhpn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lewj;-><init>()V

    .line 55
    return-void
.end method

.method public static a(Ljdx;Lcom/google/android/search/shared/actions/RemindersConfigFlags;)Lcom/google/android/search/shared/actions/SetReminderAction;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 199
    new-instance v3, Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-direct {v3, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;-><init>(Lcom/google/android/search/shared/actions/RemindersConfigFlags;)V

    .line 201
    invoke-virtual {p0}, Ljdx;->bhU()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->km(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->em(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Ljdx;->bib()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 207
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aix()V

    .line 208
    const/4 v0, 0x0

    .line 209
    invoke-virtual {p0}, Ljdx;->bic()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 210
    sget-object v0, Ldyf;->bQG:Ldyf;

    .line 228
    :cond_0
    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ldyf;)V

    .line 229
    invoke-virtual {p0}, Ljdx;->bia()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/search/shared/actions/SetReminderAction;->ax(J)V

    move v0, v1

    .line 305
    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->dj(I)V

    .line 306
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aih()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljdx;->edd:Ljdy;

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Ljdx;->edd:Ljdy;

    invoke-static {v0, v3}, Ldxx;->a(Ljdy;Lcom/google/android/search/shared/actions/SetReminderAction;)V

    .line 312
    :cond_1
    return-object v3

    .line 211
    :cond_2
    invoke-virtual {p0}, Ljdx;->bic()I

    move-result v4

    if-ne v4, v2, :cond_0

    invoke-virtual {p0}, Ljdx;->bie()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    invoke-virtual {p0}, Ljdx;->bid()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 215
    :pswitch_0
    sget-object v0, Ldyf;->bQC:Ldyf;

    goto :goto_0

    .line 218
    :pswitch_1
    sget-object v0, Ldyf;->bQD:Ldyf;

    goto :goto_0

    .line 221
    :pswitch_2
    sget-object v0, Ldyf;->bQE:Ldyf;

    goto :goto_0

    .line 224
    :pswitch_3
    sget-object v0, Ldyf;->bQF:Ldyf;

    goto :goto_0

    .line 231
    :cond_3
    iget-object v0, p0, Ljdx;->aeB:Ljbp;

    if-eqz v0, :cond_7

    .line 233
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    .line 234
    iget-object v0, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->bff()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 236
    iget-object v0, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->bfe()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 242
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiA()Ljpd;

    move-result-object v0

    .line 274
    :goto_2
    if-eqz v0, :cond_4

    .line 275
    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->e(Ljpd;)V

    :cond_4
    move v0, v2

    .line 277
    goto :goto_1

    .line 238
    :pswitch_4
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiB()Ljpd;

    move-result-object v0

    goto :goto_2

    .line 246
    :cond_5
    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    .line 247
    iget-object v0, v1, Ljbp;->dYZ:Lixu;

    if-eqz v0, :cond_6

    iget-object v0, v1, Ljbp;->dYZ:Lixu;

    iget-object v0, v0, Lixu;->dOG:Lixv;

    if-eqz v0, :cond_6

    iget-object v0, v1, Ljbp;->dYZ:Lixu;

    iget-object v0, v0, Lixu;->dOG:Lixv;

    iget-object v0, v0, Lixv;->dMA:Ljbc;

    if-eqz v0, :cond_6

    iget-object v0, v1, Ljbp;->dYZ:Lixu;

    invoke-virtual {v0}, Lixu;->baV()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 251
    iget-object v0, v1, Ljbp;->dYZ:Lixu;

    iget-object v0, v0, Lixu;->dOG:Lixv;

    iget-object v0, v0, Lixv;->dMA:Ljbc;

    .line 253
    new-instance v4, Ljpf;

    invoke-direct {v4}, Ljpf;-><init>()V

    invoke-virtual {v0}, Ljbc;->beH()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljpf;->dE(J)Ljpf;

    move-result-object v4

    invoke-virtual {v0}, Ljbc;->beJ()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljpf;->dF(J)Ljpf;

    move-result-object v0

    .line 257
    new-instance v4, Ljpc;

    invoke-direct {v4}, Ljpc;-><init>()V

    .line 259
    iput-object v0, v4, Ljpc;->exb:Ljpf;

    .line 261
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    iget-object v5, v1, Ljbp;->dYZ:Lixu;

    invoke-virtual {v5}, Lixu;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    move-result-object v0

    .line 263
    new-instance v5, Ljpb;

    invoke-direct {v5}, Ljpb;-><init>()V

    iput-object v5, v0, Ljpd;->exz:Ljpb;

    .line 264
    iget-object v5, v0, Ljpd;->exz:Ljpb;

    iput-object v4, v5, Ljpb;->ewY:Ljpc;

    .line 265
    iget-object v4, v0, Ljpd;->exz:Ljpb;

    iget-object v1, v1, Ljbp;->dYZ:Lixu;

    invoke-virtual {v1}, Lixu;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljpb;->xU(Ljava/lang/String;)Ljpb;

    goto :goto_2

    .line 267
    :cond_6
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    move-result-object v0

    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xY(Ljava/lang/String;)Ljpd;

    move-result-object v0

    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->mR()D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljpd;->u(D)Ljpd;

    move-result-object v0

    iget-object v1, p0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->mS()D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljpd;->v(D)Ljpd;

    move-result-object v0

    goto/16 :goto_2

    .line 277
    :cond_7
    iget-object v0, p0, Ljdx;->ebd:Ljbr;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v0}, Ljbr;->bfn()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v0}, Ljbr;->baV()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 281
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    .line 282
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    iget-object v1, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v1}, Ljbr;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    move-result-object v0

    .line 286
    new-instance v1, Ljpa;

    invoke-direct {v1}, Ljpa;-><init>()V

    iput-object v1, v0, Ljpd;->exA:Ljpa;

    .line 287
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    iget-object v4, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v4}, Ljbr;->bfm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljpa;->xR(Ljava/lang/String;)Ljpa;

    .line 288
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    iget-object v4, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v4}, Ljbr;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljpa;->xS(Ljava/lang/String;)Ljpa;

    .line 290
    iget-object v1, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v1}, Ljbr;->bfp()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 291
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    iget-object v4, p0, Ljdx;->ebd:Ljbr;

    invoke-virtual {v4}, Ljbr;->bfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljpa;->xT(Ljava/lang/String;)Ljpa;

    .line 294
    :cond_8
    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->e(Ljpd;)V

    move v0, v2

    .line 295
    goto/16 :goto_1

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aii()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Ljdx;->bif()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 297
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aix()V

    .line 298
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    .line 299
    const/4 v0, 0x4

    goto/16 :goto_1

    .line 301
    :cond_a
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aix()V

    .line 302
    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    move v0, v1

    .line 303
    goto/16 :goto_1

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 236
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lexf;Lexl;Z)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lexl;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setEnabled(Z)V

    const/4 v0, -0x2

    invoke-virtual {p1, v0}, Lexl;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method static a(Lizj;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 317
    invoke-static {p0}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->b(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 320
    return-void
.end method

.method public static b(Landroid/app/Fragment;Lizj;)Lexf;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 61
    const-string v1, "entry"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 63
    new-instance v1, Lexf;

    invoke-direct {v1}, Lexf;-><init>()V

    .line 64
    invoke-virtual {v1, v0}, Lexf;->setArguments(Landroid/os/Bundle;)V

    .line 65
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lexf;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 66
    return-object v1
.end method

.method static g(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 325
    return-void
.end method


# virtual methods
.method final b(Lizj;)V
    .locals 3

    .prologue
    .line 329
    invoke-virtual {p0}, Lexf;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-static {v0, p1}, Lewq;->a(Landroid/app/Fragment;Lizj;)Lewq;

    move-result-object v0

    .line 331
    invoke-virtual {p0}, Lexf;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "delete_reminder_dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 333
    const-string v2, "delete_dialog"

    invoke-virtual {v0, v1, v2}, Lewq;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 334
    return-void
.end method

.method public final bC(Z)V
    .locals 4

    .prologue
    .line 162
    invoke-virtual {p0}, Lexf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p0}, Lexf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lexf;->cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    const v2, 0x7f110198

    invoke-virtual {v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lexf;->bf(Landroid/view/View;)V

    .line 168
    const v1, 0x7f0a08a7

    invoke-static {v0, v1}, Lexf;->g(Landroid/content/Context;I)V

    .line 169
    iget-object v1, p0, Lexf;->mPresenter:Lhpn;

    iget-object v1, v1, Lhpn;->dkV:Lhqs;

    iget-object v2, p0, Lexf;->mPresenter:Lhpn;

    iget-object v2, v2, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    new-instance v3, Lexi;

    invoke-direct {v3, p0, v0}, Lexi;-><init>(Lexf;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Lhqs;->a(Lcom/google/android/search/shared/actions/SetReminderAction;Lefk;)V

    goto :goto_0
.end method

.method public final lC(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lexf;->cld:Lexj;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lexf;->cld:Lexj;

    invoke-interface {v0, p1}, Lexj;->lC(Ljava/lang/String;)V

    .line 341
    :cond_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    .line 90
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v3

    .line 92
    invoke-virtual {v3}, Lfdb;->axI()Lfcx;

    move-result-object v4

    .line 95
    if-eqz p1, :cond_1

    .line 97
    const-string v0, "entry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    iput-object v0, p0, Lexf;->mEntry:Lizj;

    .line 98
    const-string v0, "reminder_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/SetReminderAction;

    move-object v1, v0

    .line 107
    :goto_0
    new-instance v0, Lhpn;

    invoke-direct {v0, v1}, Lhpn;-><init>(Lcom/google/android/search/shared/actions/SetReminderAction;)V

    iput-object v0, p0, Lexf;->mPresenter:Lhpn;

    .line 108
    iget-object v0, p0, Lexf;->mPresenter:Lhpn;

    new-instance v5, Lhqs;

    invoke-virtual {v2}, Lgql;->aJU()Lgpu;

    move-result-object v6

    invoke-virtual {v6}, Lgpu;->aJH()Lhlr;

    move-result-object v6

    new-instance v7, Lhla;

    invoke-virtual {p0}, Lexf;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v3}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v3

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    invoke-direct {v7, v8, v4, v3, v2}, Lhla;-><init>(Landroid/content/Context;Lfcx;Lfaq;Lemp;)V

    invoke-direct {v5, v6, v7}, Lhqs;-><init>(Lhlr;Lhla;)V

    iput-object v5, v0, Lhpn;->dkV:Lhqs;

    .line 112
    iget-object v0, p0, Lexf;->mPresenter:Lhpn;

    iget-object v2, v0, Lhpn;->dkV:Lhqs;

    new-instance v3, Lhpo;

    invoke-direct {v3, v0}, Lhpo;-><init>(Lhpn;)V

    iget-object v0, v2, Lhqs;->aoG:Lhla;

    new-instance v2, Ldkp;

    iget-object v0, v0, Lhla;->mNetworkClient:Lfcx;

    invoke-direct {v2, v0, v3}, Ldkp;-><init>(Lfcx;Lefk;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldkp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 114
    invoke-virtual {p0}, Lexf;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 115
    instance-of v2, v0, Lexj;

    if-eqz v2, :cond_0

    .line 116
    check-cast v0, Lexj;

    iput-object v0, p0, Lexf;->cld:Lexj;

    .line 119
    :cond_0
    invoke-virtual {p0}, Lexf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 120
    const v2, 0x7f04006e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iput-object v0, p0, Lexf;->cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    .line 122
    iget-object v0, p0, Lexf;->cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aim()Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lcom/google/android/search/shared/actions/RemindersConfigFlags;)V

    .line 124
    iget-object v0, p0, Lexf;->mPresenter:Lhpn;

    iget-object v1, p0, Lexf;->cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iput-object v1, v0, Lhpn;->dnE:Lhpq;

    .line 125
    iget-object v0, p0, Lexf;->mPresenter:Lhpn;

    iput-object p0, v0, Lhpn;->dnF:Lhpp;

    .line 127
    iget-object v0, p0, Lexf;->cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, p0, Lexf;->mPresenter:Lhpn;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lhpn;)V

    .line 129
    iget-object v0, p0, Lexf;->mPresenter:Lhpn;

    invoke-virtual {v0}, Lhpn;->ui()V

    .line 131
    new-instance v0, Lexl;

    invoke-virtual {p0}, Lexf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lexf;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f0a03f1

    invoke-direct {v0, v1, v2, v3}, Lexl;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;I)V

    iput-object v0, p0, Lexf;->clf:Lexl;

    .line 133
    iget-object v0, p0, Lexf;->clf:Lexl;

    iget-object v1, p0, Lexf;->cle:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-virtual {v0, v1}, Lexl;->setView(Landroid/view/View;)V

    .line 134
    iget-object v0, p0, Lexf;->clf:Lexl;

    const v1, 0x7f0a046b

    new-instance v2, Lexg;

    invoke-direct {v2, p0}, Lexg;-><init>(Lexf;)V

    invoke-virtual {v0, v1, v2}, Lexl;->a(ILandroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lexf;->clf:Lexl;

    const v1, 0x7f0a03f7

    new-instance v2, Lexh;

    invoke-direct {v2, p0}, Lexh;-><init>(Lexf;)V

    invoke-virtual {v0, v1, v2}, Lexl;->b(ILandroid/view/View$OnClickListener;)V

    .line 152
    iget-object v0, p0, Lexf;->clf:Lexl;

    return-object v0

    .line 101
    :cond_1
    invoke-virtual {p0}, Lexf;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entry"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    iput-object v0, p0, Lexf;->mEntry:Lizj;

    .line 102
    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lexf;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dSU:Ljdx;

    invoke-virtual {p0}, Lexf;->getActivity()Landroid/app/Activity;

    invoke-virtual {v0}, Lchk;->GL()Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    move-result-object v0

    invoke-static {v1, v0}, Lexf;->a(Ljdx;Lcom/google/android/search/shared/actions/RemindersConfigFlags;)Lcom/google/android/search/shared/actions/SetReminderAction;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Lewj;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 83
    const-string v0, "reminder_action"

    iget-object v1, p0, Lexf;->mPresenter:Lhpn;

    iget-object v1, v1, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    const-string v0, "entry"

    iget-object v1, p0, Lexf;->mEntry:Lizj;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 85
    return-void
.end method
