.class final Lkdo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final eTG:Ljava/io/DataInputStream;

.field private final eTH:Lkdr;

.field eTI:I


# direct methods
.method constructor <init>(Ljava/io/InputStream;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lkdp;

    invoke-direct {v0, p0, p1}, Lkdp;-><init>(Lkdo;Ljava/io/InputStream;)V

    .line 47
    new-instance v1, Lkdq;

    invoke-direct {v1, p0}, Lkdq;-><init>(Lkdo;)V

    .line 59
    new-instance v2, Lkdr;

    invoke-direct {v2, v0, v1}, Lkdr;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V

    iput-object v2, p0, Lkdo;->eTH:Lkdr;

    .line 60
    new-instance v0, Ljava/io/DataInputStream;

    iget-object v1, p0, Lkdo;->eTH:Lkdr;

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lkdo;->eTG:Ljava/io/DataInputStream;

    .line 61
    return-void
.end method

.method private readString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 114
    iget-object v0, p0, Lkdo;->eTG:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 115
    new-array v1, v0, [B

    .line 116
    iget-object v2, p0, Lkdo;->eTG:Ljava/io/DataInputStream;

    invoke-static {v2, v1}, Lkbt;->a(Ljava/io/InputStream;[B)V

    .line 117
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "UTF-8"

    invoke-direct {v2, v1, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lkdo;->eTG:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 122
    return-void
.end method

.method public final tQ(I)Ljava/util/List;
    .locals 6

    .prologue
    .line 75
    iget v0, p0, Lkdo;->eTI:I

    add-int/2addr v0, p1

    iput v0, p0, Lkdo;->eTI:I

    .line 77
    :try_start_0
    iget-object v0, p0, Lkdo;->eTG:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 78
    if-gez v1, :cond_0

    .line 79
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "numberOfPairs < 0: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 81
    :cond_0
    const/16 v0, 0x400

    if-le v1, v0, :cond_1

    .line 82
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "numberOfPairs > 1024: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    mul-int/lit8 v0, v1, 0x2

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 85
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 86
    invoke-direct {p0}, Lkdo;->readString()Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-direct {p0}, Lkdo;->readString()Ljava/lang/String;

    move-result-object v4

    .line 88
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "name.length == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_2
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_3
    iget v0, p0, Lkdo;->eTI:I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkdo;->eTH:Lkdr;

    invoke-virtual {v0}, Lkdr;->fill()V

    iget v0, p0, Lkdo;->eTI:I

    if-eqz v0, :cond_4

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "compressedLimit > 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lkdo;->eTI:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 95
    :cond_4
    return-object v2
.end method
