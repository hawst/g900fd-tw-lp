.class final Lcjx;
.super Landroid/database/DataSetObserver;
.source "PG"


# instance fields
.field private final mCorpora:Lcfu;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;


# direct methods
.method constructor <init>(Lcfu;Lcom/google/android/search/core/state/QueryState;)V
    .locals 0

    .prologue
    .line 1502
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 1503
    iput-object p1, p0, Lcjx;->mCorpora:Lcfu;

    .line 1504
    iput-object p2, p0, Lcjx;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 1505
    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 1

    .prologue
    .line 1515
    iget-object v0, p0, Lcjx;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1516
    iget-object v0, p0, Lcjx;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xz()V

    .line 1517
    iget-object v0, p0, Lcjx;->mCorpora:Lcfu;

    invoke-virtual {v0, p0}, Lcfu;->unregisterObserver(Ljava/lang/Object;)V

    .line 1519
    :cond_0
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 1508
    iget-object v0, p0, Lcjx;->mCorpora:Lcfu;

    invoke-virtual {v0, p0}, Lcfu;->registerObserver(Ljava/lang/Object;)V

    .line 1509
    iget-object v0, p0, Lcjx;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->ed()V

    .line 1510
    invoke-virtual {p0}, Lcjx;->onChanged()V

    .line 1511
    return-void
.end method
