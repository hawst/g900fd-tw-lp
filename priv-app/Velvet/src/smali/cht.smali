.class final Lcht;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lchv;


# instance fields
.field private synthetic aXs:Ljava/util/Map;

.field private synthetic aXt:Lchs;


# direct methods
.method constructor <init>(Lchs;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcht;->aXt:Lchs;

    iput-object p2, p0, Lcht;->aXs:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final gs(Ljava/lang/String;)Lcyh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 299
    const-string v3, "google_account"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 301
    iget-object v0, p0, Lcht;->aXt:Lchs;

    invoke-virtual {v0}, Lchs;->Ky()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 302
    iget-object v0, p0, Lcht;->aXt:Lchs;

    invoke-virtual {v0}, Lchs;->Kx()Lcyh;

    move-result-object v0

    .line 326
    :goto_0
    return-object v0

    .line 303
    :cond_0
    const-string v3, "web_corpora_json"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "web_corpora_json_url"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 305
    :cond_1
    iget-object v0, p0, Lcht;->aXt:Lchs;

    invoke-virtual {v0}, Lchs;->Ky()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-object v0, v1

    .line 306
    goto :goto_0

    .line 307
    :cond_2
    const-string v3, "lastloc"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "session_key"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "web_corpora_config"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "gstatic_configuration_data"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "gstatic_configuration_override_1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "configuration_bytes_key_"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 313
    :cond_3
    iget-object v0, p0, Lcht;->aXs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_4

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 316
    :try_start_0
    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 317
    iget-object v2, p0, Lcht;->aXt:Lchs;

    invoke-virtual {v2}, Lchs;->Ky()Lcyh;

    move-result-object v2

    invoke-interface {v2, p1, v0}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_1
    move-object v0, v1

    .line 321
    goto :goto_0

    .line 323
    :cond_5
    const-string v1, "first_run_screens"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "any_account_can_run_the_google"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "opted_in_version_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "user_can_run_the_google_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "last_configuration_saved_time_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_6
    move v1, v2

    :goto_2
    if-nez v1, :cond_7

    const-string v1, "enableTestPlatformLogging"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    move v0, v2

    .line 326
    :cond_8
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcht;->aXt:Lchs;

    invoke-virtual {v0}, Lchs;->Kx()Lcyh;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v1, v0

    .line 323
    goto :goto_2

    .line 326
    :cond_a
    iget-object v0, p0, Lcht;->aXt:Lchs;

    invoke-virtual {v0}, Lchs;->Ky()Lcyh;

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
