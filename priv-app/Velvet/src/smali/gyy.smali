.class public abstract Lgyy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private final aD:Ljava/lang/String;

.field private final bpZ:Ldcs;

.field mPresenter:Lgyz;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lgyy;->aD:Ljava/lang/String;

    .line 62
    new-instance v0, Ldcs;

    invoke-direct {v0}, Ldcs;-><init>()V

    iput-object v0, p0, Lgyy;->bpZ:Ldcs;

    .line 63
    return-void
.end method


# virtual methods
.method public final Nf()Ldda;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lgyy;->bpZ:Ldcs;

    invoke-static {}, Lenu;->auR()V

    iget-object v0, v0, Ldcs;->mEventBus:Ldda;

    return-object v0
.end method

.method protected abstract X(Landroid/os/Bundle;)V
.end method

.method public Y(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public final a(Lddc;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lgyy;->bpZ:Ldcs;

    invoke-virtual {v0, p1}, Ldcs;->a(Lddc;)V

    .line 115
    return-void
.end method

.method public a(Letj;)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public a(Lgyv;Lgyt;)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public final a(Lgyz;Ldda;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 162
    iput-object p1, p0, Lgyy;->mPresenter:Lgyz;

    .line 168
    iget-object v0, p0, Lgyy;->bpZ:Ldcs;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lgyy;->bpZ:Ldcs;

    invoke-virtual {v0, p2}, Ldcs;->b(Ldda;)V

    .line 171
    :cond_0
    invoke-virtual {p0, p3}, Lgyy;->X(Landroid/os/Bundle;)V

    .line 172
    return-void
.end method

.method public aAU()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aJU()Lgpu;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->aJU()Lgpu;

    move-result-object v0

    return-object v0
.end method

.method protected abstract aKF()V
.end method

.method public aLH()Lgyz;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    return-object v0
.end method

.method public final b(Lddc;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lgyy;->bpZ:Ldcs;

    invoke-virtual {v0, p1}, Ldcs;->b(Lddc;)V

    .line 119
    return-void
.end method

.method public cN()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public final getDimensionPixelSize(I)I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0, p1}, Lgyz;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lgyy;->aD:Ljava/lang/String;

    return-object v0
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-virtual {p0}, Lgyy;->aKF()V

    .line 184
    iget-object v0, p0, Lgyy;->bpZ:Ldcs;

    invoke-virtual {v0, v1}, Ldcs;->b(Ldda;)V

    .line 185
    iput-object v1, p0, Lgyy;->mPresenter:Lgyz;

    .line 186
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public final wj()Lekf;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->wj()Lekf;

    move-result-object v0

    return-object v0
.end method
