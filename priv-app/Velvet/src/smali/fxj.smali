.class public final Lfxj;
.super Lfus;
.source "PG"


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 46
    return-void
.end method

.method public static a(Landroid/content/Context;Ljic;)Lanh;
    .locals 17
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    new-instance v6, Lanh;

    invoke-direct {v6}, Lanh;-><init>()V

    .line 108
    const/16 v1, 0x10

    invoke-virtual {v6, v1}, Lanh;->cm(I)Lanh;

    .line 109
    const/4 v2, 0x1

    .line 111
    invoke-virtual/range {p1 .. p1}, Ljic;->getType()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    .line 112
    :goto_0
    new-instance v8, Laou;

    invoke-direct {v8}, Laou;-><init>()V

    .line 113
    if-eqz v1, :cond_4

    .line 114
    const v1, 0x7f0a04df

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Laou;->cs(Ljava/lang/String;)Laou;

    .line 119
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljic;->bmN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 120
    invoke-virtual/range {p1 .. p1}, Ljic;->bmN()Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {v8, v1}, Laou;->ct(Ljava/lang/String;)Laou;

    .line 122
    new-instance v2, Lfsa;

    const/16 v3, 0xc6

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0201e1

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v6, Lanh;->ahs:Lani;

    .line 125
    const/4 v1, 0x0

    .line 128
    :goto_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 130
    move-object/from16 v0, p1

    iget-object v2, v0, Ljic;->emW:[Ljid;

    array-length v2, v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_11

    .line 131
    const/4 v4, 0x0

    .line 132
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 133
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 134
    const/4 v5, 0x0

    .line 135
    const/4 v3, 0x0

    .line 136
    move-object/from16 v0, p1

    iget-object v12, v0, Ljic;->emW:[Ljid;

    array-length v13, v12

    const/4 v2, 0x0

    move v7, v2

    move-object v2, v3

    move v3, v4

    :goto_3
    if-ge v7, v13, :cond_7

    aget-object v14, v12, v7

    .line 137
    invoke-virtual {v14}, Ljid;->aZG()Ljava/lang/String;

    move-result-object v15

    .line 138
    invoke-virtual {v14}, Ljid;->bmR()Ljava/lang/String;

    move-result-object v4

    .line 139
    invoke-static {v15}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 140
    :cond_0
    invoke-static {v15}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_5

    .line 141
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    const/4 v3, 0x1

    .line 146
    :goto_4
    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_6

    :goto_5
    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_1
    invoke-virtual {v14}, Ljid;->aZS()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_19

    .line 149
    invoke-virtual {v14}, Ljid;->aZS()Ljava/lang/String;

    move-result-object v4

    .line 151
    :goto_6
    invoke-virtual {v14}, Ljid;->bmQ()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 152
    invoke-virtual {v14}, Ljid;->bmQ()Ljava/lang/String;

    move-result-object v2

    .line 136
    :cond_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move-object v5, v4

    goto :goto_3

    .line 111
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 116
    :cond_4
    const v1, 0x7f0a04de

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Laou;->cs(Ljava/lang/String;)Laou;

    goto/16 :goto_1

    .line 144
    :cond_5
    const-string v15, "\u2014"

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 146
    :cond_6
    const-string v4, "\u2014"

    goto :goto_5

    .line 155
    :cond_7
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 156
    :cond_8
    new-instance v4, Lfyg;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lfyg;-><init>(Landroid/content/Context;)V

    .line 157
    if-eqz v3, :cond_10

    .line 158
    const v3, 0x7f0a04e3

    iget-object v7, v4, Lfyg;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x3

    invoke-virtual {v4, v3, v10, v7}, Lfyg;->g(Ljava/lang/String;Ljava/util/List;I)Lfyg;

    .line 159
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f100040

    const/4 v10, 0x1

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v3, v7, v10, v12}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x1

    invoke-virtual {v4, v3, v11, v7}, Lfyg;->g(Ljava/lang/String;Ljava/util/List;I)Lfyg;

    .line 169
    :goto_7
    invoke-virtual {v4}, Lfyg;->aDR()Laow;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_9
    invoke-virtual/range {p1 .. p1}, Ljic;->aZW()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 173
    new-instance v3, Lfyg;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lfyg;-><init>(Landroid/content/Context;)V

    .line 174
    const v4, 0x7f0a04e4

    invoke-virtual/range {p1 .. p1}, Ljic;->aZW()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x3

    invoke-virtual {v3, v4, v7, v10}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    .line 176
    invoke-virtual {v3}, Lfyg;->aDR()Laow;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_a
    invoke-static {v5}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 180
    :cond_b
    new-instance v3, Lfyg;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lfyg;-><init>(Landroid/content/Context;)V

    .line 181
    invoke-static {v5}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 182
    const v4, 0x7f0a04e2

    const/4 v7, 0x3

    invoke-virtual {v3, v4, v5, v7}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    .line 184
    :cond_c
    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 185
    const v4, 0x7f0a04e0

    invoke-virtual {v3, v4, v2}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    .line 187
    :cond_d
    invoke-virtual {v3}, Lfyg;->aDR()Laow;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_e
    :goto_8
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_f

    .line 223
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Laow;

    invoke-interface {v9, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Laow;

    iput-object v1, v8, Laou;->akm:[Laow;

    .line 224
    const/4 v1, 0x0

    .line 226
    :cond_f
    if-eqz v1, :cond_18

    .line 227
    const/4 v1, 0x0

    .line 231
    :goto_9
    return-object v1

    .line 164
    :cond_10
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f100040

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v3, v7, v10, v12}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v7, ", "

    invoke-static {v7, v11}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x4

    invoke-virtual {v4, v3, v7, v10}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    goto/16 :goto_7

    .line 189
    :cond_11
    move-object/from16 v0, p1

    iget-object v2, v0, Ljic;->emW:[Ljid;

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_e

    .line 190
    new-instance v2, Lfyg;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lfyg;-><init>(Landroid/content/Context;)V

    .line 191
    move-object/from16 v0, p1

    iget-object v3, v0, Ljic;->emW:[Ljid;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    .line 192
    invoke-virtual {v3}, Ljid;->aZG()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 193
    const v4, 0x7f0a04e3

    invoke-virtual {v3}, Ljid;->aZG()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x2

    invoke-virtual {v2, v4, v5, v7}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    .line 196
    :cond_12
    invoke-virtual {v3}, Ljid;->bmR()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 197
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100040

    const/4 v7, 0x1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v4, v5, v7, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljid;->bmR()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    invoke-virtual {v2, v4, v5, v7}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    .line 202
    :cond_13
    invoke-virtual {v3}, Ljid;->bmQ()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_14

    .line 203
    const v4, 0x7f0a04e0

    invoke-virtual {v3}, Ljid;->bmQ()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    .line 205
    :cond_14
    iget-object v4, v2, Lfyg;->cDn:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_15

    .line 206
    invoke-virtual {v2}, Lfyg;->aDR()Laow;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_15
    new-instance v2, Lfyg;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lfyg;-><init>(Landroid/content/Context;)V

    .line 210
    invoke-virtual/range {p1 .. p1}, Ljic;->aZW()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_16

    .line 211
    const v4, 0x7f0a04e4

    invoke-virtual/range {p1 .. p1}, Ljic;->aZW()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x2

    invoke-virtual {v2, v4, v5, v7}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    .line 214
    :cond_16
    invoke-virtual {v3}, Ljid;->aZS()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 215
    const v4, 0x7f0a04e2

    invoke-virtual {v3}, Ljid;->aZS()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v3, v5}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    .line 217
    :cond_17
    iget-object v3, v2, Lfyg;->cDn:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    .line 218
    invoke-virtual {v2}, Lfyg;->aDR()Laow;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 230
    :cond_18
    iput-object v8, v6, Lanh;->agN:Laou;

    move-object v1, v6

    .line 231
    goto/16 :goto_9

    :cond_19
    move-object v4, v5

    goto/16 :goto_6

    :cond_1a
    move v1, v2

    goto/16 :goto_2
.end method

.method private a(Ljic;Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const v3, 0x7f0a04d8

    const v6, 0x7f0a04d7

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 364
    invoke-virtual {p1}, Ljic;->getType()I

    move-result v0

    if-ne v0, v5, :cond_0

    move v0, v1

    .line 365
    :goto_0
    invoke-virtual {p1}, Ljic;->bmG()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Ljic;->bmK()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Ljic;->bmI()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Ljic;->bmM()Z

    move-result v4

    if-nez v4, :cond_2

    .line 367
    const-string v1, "QpTransportationListEntryAdapter"

    const-string v2, "Unexpected Entry. Doesn\'t have enough info for title."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    if-eqz v0, :cond_1

    const v0, 0x7f0a04dd

    .line 369
    :goto_1
    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 364
    goto :goto_0

    .line 368
    :cond_1
    const v0, 0x7f0a04dc

    goto :goto_1

    .line 371
    :cond_2
    invoke-virtual {p1}, Ljic;->bmG()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Ljic;->bmK()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Ljic;->bmF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljic;->bmJ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 373
    invoke-virtual {p1}, Ljic;->getType()I

    move-result v0

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljic;->bmI()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1}, Ljic;->bmM()Z

    move-result v4

    if-eqz v4, :cond_4

    new-array v0, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljic;->bmH()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p1}, Ljic;->bmL()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p2, v6, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Ljic;->bmI()Z

    move-result v4

    if-eqz v4, :cond_6

    if-eqz v0, :cond_5

    move v0, v3

    :goto_4
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljic;->bmH()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    const v0, 0x7f0a04d9

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_7

    const v0, 0x7f0a04da

    :goto_5
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljic;->bmL()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    const v0, 0x7f0a04db

    goto :goto_5

    .line 375
    :cond_8
    invoke-virtual {p1}, Ljic;->getType()I

    move-result v0

    if-ne v0, v5, :cond_9

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljic;->bmG()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p1}, Ljic;->bmK()Z

    move-result v4

    if-eqz v4, :cond_a

    new-array v0, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljic;->bmF()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p1}, Ljic;->bmJ()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p2, v6, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    move v0, v2

    goto :goto_6

    :cond_a
    invoke-virtual {p1}, Ljic;->bmG()Z

    move-result v4

    if-eqz v4, :cond_c

    if-eqz v0, :cond_b

    :goto_7
    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljic;->bmF()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p2, v3, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_b
    const v3, 0x7f0a04d9

    goto :goto_7

    :cond_c
    if-eqz v0, :cond_d

    const v0, 0x7f0a04da

    :goto_8
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljic;->bmJ()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_d
    const v0, 0x7f0a04db

    goto :goto_8
.end method

.method public static b(Landroid/content/Context;Ljic;)Lanh;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 237
    new-instance v4, Lapl;

    invoke-direct {v4}, Lapl;-><init>()V

    .line 238
    invoke-virtual {p1}, Ljic;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    move v1, v2

    .line 242
    :goto_0
    new-instance v5, Lapm;

    invoke-direct {v5}, Lapm;-><init>()V

    .line 243
    const v0, 0x7f0a04eb

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dj(Ljava/lang/String;)Lapm;

    .line 244
    invoke-virtual {p1}, Ljic;->bmH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 245
    if-eqz v1, :cond_a

    .line 246
    invoke-virtual {p1}, Ljic;->bmH()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dl(Ljava/lang/String;)Lapm;

    :goto_1
    move v0, v3

    .line 252
    :goto_2
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljic;->bmF()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 253
    invoke-virtual {p1}, Ljic;->bmF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dk(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 256
    :cond_0
    invoke-virtual {p1}, Ljic;->bmO()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 257
    iget-object v6, p1, Ljic;->emR:Ljhl;

    invoke-static {p0, v6}, Lesi;->a(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v6

    .line 258
    invoke-static {v6}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 259
    invoke-virtual {v5, v6}, Lapm;->dm(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 262
    :cond_1
    iget-object v6, p1, Ljic;->emR:Ljhl;

    invoke-static {p0, v6}, Lesi;->b(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v6

    .line 263
    invoke-static {v6}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 264
    invoke-virtual {v5, v6}, Lapm;->dn(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 268
    :cond_2
    invoke-virtual {p1}, Ljic;->bck()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 269
    invoke-virtual {p1}, Ljic;->bck()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->do(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 272
    :cond_3
    if-nez v0, :cond_4

    .line 273
    iget-object v0, v4, Lapl;->alq:[Lapm;

    invoke-static {v0, v5}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapm;

    iput-object v0, v4, Lapl;->alq:[Lapm;

    .line 278
    :cond_4
    new-instance v5, Lapm;

    invoke-direct {v5}, Lapm;-><init>()V

    .line 279
    const v0, 0x7f0a04ec

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dj(Ljava/lang/String;)Lapm;

    .line 280
    invoke-virtual {p1}, Ljic;->bmL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 281
    if-eqz v1, :cond_b

    .line 282
    invoke-virtual {p1}, Ljic;->bmL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dl(Ljava/lang/String;)Lapm;

    :goto_3
    move v0, v3

    .line 288
    :goto_4
    if-eqz v1, :cond_5

    invoke-virtual {p1}, Ljic;->bmJ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 289
    invoke-virtual {p1}, Ljic;->bmJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dk(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 292
    :cond_5
    iget-object v1, p1, Ljic;->emV:Ljhl;

    invoke-static {p0, v1}, Lesi;->a(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 294
    invoke-virtual {v5, v1}, Lapm;->dm(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 297
    :cond_6
    invoke-virtual {p1}, Ljic;->bmO()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 298
    iget-object v1, p1, Ljic;->emV:Ljhl;

    invoke-static {p0, v1}, Lesi;->b(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v1

    .line 299
    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 300
    invoke-virtual {v5, v1}, Lapm;->dn(Ljava/lang/String;)Lapm;

    move v0, v3

    .line 304
    :cond_7
    if-nez v0, :cond_8

    .line 305
    iget-object v0, v4, Lapl;->alq:[Lapm;

    invoke-static {v0, v5}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapm;

    iput-object v0, v4, Lapl;->alq:[Lapm;

    .line 308
    :cond_8
    iget-object v0, v4, Lapl;->alq:[Lapm;

    array-length v0, v0

    if-nez v0, :cond_c

    .line 309
    const/4 v0, 0x0

    .line 315
    :goto_5
    return-object v0

    :cond_9
    move v1, v3

    .line 238
    goto/16 :goto_0

    .line 248
    :cond_a
    invoke-virtual {p1}, Ljic;->bmH()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dk(Ljava/lang/String;)Lapm;

    goto/16 :goto_1

    .line 284
    :cond_b
    invoke-virtual {p1}, Ljic;->bmL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lapm;->dk(Ljava/lang/String;)Lapm;

    goto :goto_3

    .line 312
    :cond_c
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 313
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 314
    iput-object v4, v0, Lanh;->ahf:Lapl;

    goto :goto_5

    :cond_d
    move v0, v2

    goto :goto_4

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lfus;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 51
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 52
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lfxj;->cCl:Lftx;

    invoke-virtual {v0, p1, p3}, Lftx;->j(Landroid/content/Context;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p2, Lizj;->dTQ:Ljic;

    invoke-direct {p0, v0, p1}, Lfxj;->a(Ljic;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lizj;->dTQ:Ljic;

    invoke-virtual {v1}, Ljic;->ol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfuj;->aO(Ljava/lang/String;Ljava/lang/String;)Lanh;

    move-result-object v0

    .line 95
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 96
    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljic;Z)Lanh;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 333
    new-instance v3, Lftn;

    invoke-direct {p0, p2, p1}, Lfxj;->a(Ljic;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lftn;-><init>(Ljava/lang/String;)V

    .line 336
    if-nez p3, :cond_0

    invoke-virtual {p2}, Ljic;->bmO()Z

    move-result v0

    if-nez v0, :cond_2

    .line 338
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 339
    iget-object v0, p2, Ljic;->emR:Ljhl;

    invoke-static {p1, v0}, Lesi;->b(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p2}, Ljic;->bmO()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 342
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 345
    :goto_0
    iget-object v5, p2, Ljic;->emR:Ljhl;

    invoke-static {p1, v5}, Lesi;->a(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v5

    .line 347
    invoke-static {v5}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 348
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 351
    if-eqz v0, :cond_4

    const v0, 0x7f0a04ee

    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, ", "

    invoke-static {v5, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lftn;->ceJ:Ljava/lang/String;

    .line 357
    :cond_2
    invoke-virtual {p2}, Ljic;->bmE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358
    invoke-virtual {p2}, Ljic;->bmD()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lftn;->cBj:Ljava/lang/String;

    .line 360
    :cond_3
    invoke-virtual {v3}, Lftn;->aDh()Lanh;

    move-result-object v0

    return-object v0

    .line 351
    :cond_4
    const v0, 0x7f0a04ed

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTR:Ljau;

    iget-object v0, v0, Ljau;->dQX:Ljhe;

    invoke-virtual {v0}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 4

    .prologue
    .line 73
    iget-object v1, p2, Lizj;->dTQ:Ljic;

    .line 74
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 75
    iget-object v0, v1, Ljic;->dMM:Ljcn;

    if-eqz v0, :cond_0

    iget-object v0, v1, Ljic;->dMM:Ljcn;

    invoke-virtual {v0}, Ljcn;->qG()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 77
    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Lfxj;->a(Landroid/content/Context;Ljic;Z)Lanh;

    move-result-object v3

    invoke-static {v2, v3}, Lfxj;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 79
    if-eqz v0, :cond_1

    .line 80
    new-instance v0, Lfrt;

    iget-object v3, v1, Ljic;->dMM:Ljcn;

    invoke-virtual {v3}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lfrt;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lfrt;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :goto_1
    invoke-static {p1, v1}, Lfxj;->a(Landroid/content/Context;Ljic;)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfxj;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 86
    invoke-virtual {p0, p1, v1}, Lfxj;->c(Landroid/content/Context;Ljic;)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfxj;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 87
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    return-object v0

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 82
    :cond_1
    invoke-static {p1, v1}, Lfxj;->b(Landroid/content/Context;Ljic;)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfxj;->a(Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c(Landroid/content/Context;Ljic;)Lanh;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321
    iget-object v0, p2, Ljic;->dMX:[Ljbg;

    invoke-static {v0}, Lgbb;->a([Ljbg;)Ljbg;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_0

    .line 324
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    const/16 v2, 0xc7

    invoke-static {p1, v1, v0, v2}, Lfsx;->a(Landroid/content/Context;Lizj;Ljbg;I)Lanh;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lfxj;->aAw()Lizj;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 65
    invoke-virtual {p0, p1, v0}, Lfxj;->b(Landroid/content/Context;Lizj;)[Lanh;

    move-result-object v0

    return-object v0
.end method
