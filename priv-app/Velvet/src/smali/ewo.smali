.class public final Lewo;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field private ckH:Lewp;

.field private ckI:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 105
    return-void
.end method


# virtual methods
.method final fh(Z)V
    .locals 3

    .prologue
    .line 81
    invoke-virtual {p0}, Lewo;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    .line 85
    invoke-virtual {p0}, Lewo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v1}, Leyf;->a(Landroid/app/FragmentManager;)V

    .line 88
    if-eqz p1, :cond_0

    .line 89
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJr()Lfdb;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v1

    .line 94
    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfaq;->s(Ljava/util/Collection;)V

    .line 98
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lewo;->ckH:Lewp;

    .line 99
    iget-boolean v0, p0, Lewo;->ckI:Z

    if-nez v0, :cond_1

    .line 100
    invoke-virtual {p0}, Lewo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "deletePlaceWorkerFragment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 103
    :cond_1
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v4

    .line 47
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v5

    .line 49
    invoke-virtual {p0}, Lewo;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    const-string v1, "entry_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lgbm;->Q([B)Lizj;

    move-result-object v2

    .line 51
    const-string v1, "action_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->R([B)Liwk;

    move-result-object v3

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lewo;->setRetainInstance(Z)V

    .line 57
    invoke-virtual {p0}, Lewo;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Leyf;->a(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    .line 60
    new-instance v0, Lewp;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lewp;-><init>(Lewo;Lizj;Liwk;Lfcx;Lemp;)V

    iput-object v0, p0, Lewo;->ckH:Lewp;

    .line 61
    iget-object v0, p0, Lewo;->ckH:Lewp;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lewp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 62
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lewo;->ckH:Lewp;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lewo;->ckH:Lewp;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lewp;->cancel(Z)Z

    .line 69
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lewo;->ckH:Lewp;

    .line 70
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 71
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lewo;->ckI:Z

    .line 76
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 77
    return-void
.end method
