.class public final Ljbc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dXL:J

.field private dXM:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 23591
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 23592
    const/4 v0, 0x0

    iput v0, p0, Ljbc;->aez:I

    iput-wide v2, p0, Ljbc;->dXL:J

    iput-wide v2, p0, Ljbc;->dXM:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljbc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbc;->eCz:I

    .line 23593
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 23534
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljbc;->dXL:J

    iget v0, p0, Ljbc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljbc;->dXM:J

    iget v0, p0, Ljbc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 23607
    iget v0, p0, Ljbc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 23608
    const/4 v0, 0x1

    iget-wide v2, p0, Ljbc;->dXL:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 23610
    :cond_0
    iget v0, p0, Ljbc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 23611
    const/4 v0, 0x2

    iget-wide v2, p0, Ljbc;->dXM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 23613
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 23614
    return-void
.end method

.method public final beH()J
    .locals 2

    .prologue
    .line 23556
    iget-wide v0, p0, Ljbc;->dXL:J

    return-wide v0
.end method

.method public final beI()Z
    .locals 1

    .prologue
    .line 23564
    iget v0, p0, Ljbc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beJ()J
    .locals 2

    .prologue
    .line 23575
    iget-wide v0, p0, Ljbc;->dXM:J

    return-wide v0
.end method

.method public final beK()Z
    .locals 1

    .prologue
    .line 23583
    iget v0, p0, Ljbc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cD(J)Ljbc;
    .locals 1

    .prologue
    .line 23559
    iput-wide p1, p0, Ljbc;->dXL:J

    .line 23560
    iget v0, p0, Ljbc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbc;->aez:I

    .line 23561
    return-object p0
.end method

.method public final cE(J)Ljbc;
    .locals 1

    .prologue
    .line 23578
    iput-wide p1, p0, Ljbc;->dXM:J

    .line 23579
    iget v0, p0, Ljbc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbc;->aez:I

    .line 23580
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 23618
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 23619
    iget v1, p0, Ljbc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 23620
    const/4 v1, 0x1

    iget-wide v2, p0, Ljbc;->dXL:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 23623
    :cond_0
    iget v1, p0, Ljbc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 23624
    const/4 v1, 0x2

    iget-wide v2, p0, Ljbc;->dXM:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 23627
    :cond_1
    return v0
.end method
