.class final Lgup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic cWF:Lguh;

.field private synthetic cWG:Ljava/util/List;

.field private synthetic cWH:Landroid/app/PendingIntent;

.field private synthetic cWI:Lbrk;


# direct methods
.method constructor <init>(Lguh;Ljava/util/List;Landroid/app/PendingIntent;Lbrk;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lgup;->cWF:Lguh;

    iput-object p2, p0, Lgup;->cWG:Ljava/util/List;

    iput-object p3, p0, Lgup;->cWH:Landroid/app/PendingIntent;

    iput-object p4, p0, Lgup;->cWI:Lbrk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private ub()Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 205
    iget-object v0, p0, Lgup;->cWF:Lguh;

    iget-object v0, v0, Lcgl;->aWd:Lbgo;

    check-cast v0, Lbrj;

    iget-object v1, p0, Lgup;->cWG:Ljava/util/List;

    iget-object v4, p0, Lgup;->cWH:Landroid/app/PendingIntent;

    iget-object v5, p0, Lgup;->cWI:Lbrk;

    if-eqz v1, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrh;

    instance-of v7, v1, Lcom/google/android/gms/internal/nl;

    const-string v8, "Geofence must be created using Geofence.Builder."

    invoke-static {v7, v8}, Lbjr;->b(ZLjava/lang/Object;)V

    check-cast v1, Lcom/google/android/gms/internal/nl;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_1
    :try_start_0
    iget-object v0, v0, Lbrj;->aHw:Lbmo;

    invoke-virtual {v0, v1, v4, v5}, Lbmo;->a(Ljava/util/List;Landroid/app/PendingIntent;Lbrk;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    return-object v3

    .line 205
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move-object v1, v3

    goto :goto_1
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Lgup;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
