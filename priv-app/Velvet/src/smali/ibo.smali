.class public final Libo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final dxm:Landroid/content/Intent;


# instance fields
.field private final dkv:Lduv;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Libo;->dxm:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Lduv;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Libo;->dkv:Lduv;

    .line 41
    iput-object p2, p0, Libo;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 42
    iput-object p3, p0, Libo;->mResources:Landroid/content/res/Resources;

    .line 43
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;
    .locals 6

    .prologue
    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 107
    iget-object v0, p0, Libo;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_2

    .line 109
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 110
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 111
    iget-object v4, p0, Libo;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 113
    if-eqz p1, :cond_1

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 114
    :cond_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 115
    iget-object v5, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    iget-object v5, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    iget-object v3, p0, Libo;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v5, Lcom/google/android/shared/util/App;

    invoke-virtual {v0, v3}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v4, v3, v0}, Lcom/google/android/shared/util/App;-><init>(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 121
    :cond_2
    return-object v1
.end method

.method public static bU(Landroid/content/Context;)Libo;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Lduv;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJs()Lchr;

    move-result-object v1

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-direct {v0, v1}, Lduv;-><init>(Landroid/content/SharedPreferences;)V

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 50
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 51
    new-instance v3, Libo;

    invoke-direct {v3, v0, v1, v2}, Libo;-><init>(Lduv;Landroid/content/pm/PackageManager;Landroid/content/res/Resources;)V

    return-object v3
.end method


# virtual methods
.method public final T(Landroid/net/Uri;)Lcom/google/android/shared/util/App;
    .locals 4

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/google/android/shared/util/App;

    iget-object v2, p0, Libo;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a084a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200c7

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/shared/util/App;-><init>(Landroid/content/Intent;Ljava/lang/String;I)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/shared/util/App;
    .locals 4

    .prologue
    .line 146
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 147
    iget-object v0, p0, Libo;->dkv:Lduv;

    invoke-virtual {v0, p1}, Lduv;->kl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 149
    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 153
    :goto_1
    return-object v0

    .line 146
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final am(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Libo;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 68
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final an(Landroid/content/Intent;)Ljava/util/List;
    .locals 1

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 96
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Libo;->a(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final bc(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Libo;->dkv:Lduv;

    invoke-virtual {v0, p1, p2}, Lduv;->at(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    return-void
.end method

.method public final oW(Ljava/lang/String;)Ljava/util/List;
    .locals 3

    .prologue
    .line 80
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    .line 82
    :cond_0
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_1

    .line 84
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Libo;->a(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_1
    sget-object v0, Libo;->dxm:Landroid/content/Intent;

    invoke-direct {p0, p1, v0}, Libo;->a(Ljava/lang/String;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
