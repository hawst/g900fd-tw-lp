.class public final Ljcy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eba:[Ljcy;


# instance fields
.field private aez:I

.field private aiK:Ljava/lang/String;

.field public dYZ:Lixu;

.field public eaZ:Ljcx;

.field private ebb:Ljava/lang/String;

.field public ebc:Ljbq;

.field public ebd:Ljbr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19463
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 19464
    const/4 v0, 0x0

    iput v0, p0, Ljcy;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcy;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcy;->ebb:Ljava/lang/String;

    iput-object v1, p0, Ljcy;->eaZ:Ljcx;

    iput-object v1, p0, Ljcy;->dYZ:Lixu;

    iput-object v1, p0, Ljcy;->ebc:Ljbq;

    iput-object v1, p0, Ljcy;->ebd:Ljbr;

    iput-object v1, p0, Ljcy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcy;->eCz:I

    .line 19465
    return-void
.end method

.method public static bgQ()[Ljcy;
    .locals 2

    .prologue
    .line 19394
    sget-object v0, Ljcy;->eba:[Ljcy;

    if-nez v0, :cond_1

    .line 19395
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 19397
    :try_start_0
    sget-object v0, Ljcy;->eba:[Ljcy;

    if-nez v0, :cond_0

    .line 19398
    const/4 v0, 0x0

    new-array v0, v0, [Ljcy;

    sput-object v0, Ljcy;->eba:[Ljcy;

    .line 19400
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19402
    :cond_1
    sget-object v0, Ljcy;->eba:[Ljcy;

    return-object v0

    .line 19400
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 19388
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcy;->aiK:Ljava/lang/String;

    iget v0, p0, Ljcy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcy;->ebb:Ljava/lang/String;

    iget v0, p0, Ljcy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcy;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljcy;->eaZ:Ljcx;

    if-nez v0, :cond_1

    new-instance v0, Ljcx;

    invoke-direct {v0}, Ljcx;-><init>()V

    iput-object v0, p0, Ljcy;->eaZ:Ljcx;

    :cond_1
    iget-object v0, p0, Ljcy;->eaZ:Ljcx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljcy;->dYZ:Lixu;

    if-nez v0, :cond_2

    new-instance v0, Lixu;

    invoke-direct {v0}, Lixu;-><init>()V

    iput-object v0, p0, Ljcy;->dYZ:Lixu;

    :cond_2
    iget-object v0, p0, Ljcy;->dYZ:Lixu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljcy;->ebc:Ljbq;

    if-nez v0, :cond_3

    new-instance v0, Ljbq;

    invoke-direct {v0}, Ljbq;-><init>()V

    iput-object v0, p0, Ljcy;->ebc:Ljbq;

    :cond_3
    iget-object v0, p0, Ljcy;->ebc:Ljbq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljcy;->ebd:Ljbr;

    if-nez v0, :cond_4

    new-instance v0, Ljbr;

    invoke-direct {v0}, Ljbr;-><init>()V

    iput-object v0, p0, Ljcy;->ebd:Ljbr;

    :cond_4
    iget-object v0, p0, Ljcy;->ebd:Ljbr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 19483
    iget v0, p0, Ljcy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 19484
    const/4 v0, 0x1

    iget-object v1, p0, Ljcy;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 19486
    :cond_0
    iget v0, p0, Ljcy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 19487
    const/4 v0, 0x2

    iget-object v1, p0, Ljcy;->ebb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 19489
    :cond_1
    iget-object v0, p0, Ljcy;->eaZ:Ljcx;

    if-eqz v0, :cond_2

    .line 19490
    const/4 v0, 0x3

    iget-object v1, p0, Ljcy;->eaZ:Ljcx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 19492
    :cond_2
    iget-object v0, p0, Ljcy;->dYZ:Lixu;

    if-eqz v0, :cond_3

    .line 19493
    const/4 v0, 0x4

    iget-object v1, p0, Ljcy;->dYZ:Lixu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 19495
    :cond_3
    iget-object v0, p0, Ljcy;->ebc:Ljbq;

    if-eqz v0, :cond_4

    .line 19496
    const/4 v0, 0x5

    iget-object v1, p0, Ljcy;->ebc:Ljbq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 19498
    :cond_4
    iget-object v0, p0, Ljcy;->ebd:Ljbr;

    if-eqz v0, :cond_5

    .line 19499
    const/4 v0, 0x6

    iget-object v1, p0, Ljcy;->ebd:Ljbr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 19501
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 19502
    return-void
.end method

.method public final aRx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19432
    iget-object v0, p0, Ljcy;->ebb:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19410
    iget-object v0, p0, Ljcy;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 19506
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 19507
    iget v1, p0, Ljcy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 19508
    const/4 v1, 0x1

    iget-object v2, p0, Ljcy;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19511
    :cond_0
    iget v1, p0, Ljcy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 19512
    const/4 v1, 0x2

    iget-object v2, p0, Ljcy;->ebb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19515
    :cond_1
    iget-object v1, p0, Ljcy;->eaZ:Ljcx;

    if-eqz v1, :cond_2

    .line 19516
    const/4 v1, 0x3

    iget-object v2, p0, Ljcy;->eaZ:Ljcx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19519
    :cond_2
    iget-object v1, p0, Ljcy;->dYZ:Lixu;

    if-eqz v1, :cond_3

    .line 19520
    const/4 v1, 0x4

    iget-object v2, p0, Ljcy;->dYZ:Lixu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19523
    :cond_3
    iget-object v1, p0, Ljcy;->ebc:Ljbq;

    if-eqz v1, :cond_4

    .line 19524
    const/4 v1, 0x5

    iget-object v2, p0, Ljcy;->ebc:Ljbq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19527
    :cond_4
    iget-object v1, p0, Ljcy;->ebd:Ljbr;

    if-eqz v1, :cond_5

    .line 19528
    const/4 v1, 0x6

    iget-object v2, p0, Ljcy;->ebd:Ljbr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19531
    :cond_5
    return v0
.end method

.method public final tq(Ljava/lang/String;)Ljcy;
    .locals 1

    .prologue
    .line 19413
    if-nez p1, :cond_0

    .line 19414
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19416
    :cond_0
    iput-object p1, p0, Ljcy;->aiK:Ljava/lang/String;

    .line 19417
    iget v0, p0, Ljcy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcy;->aez:I

    .line 19418
    return-object p0
.end method

.method public final tr(Ljava/lang/String;)Ljcy;
    .locals 1

    .prologue
    .line 19435
    if-nez p1, :cond_0

    .line 19436
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19438
    :cond_0
    iput-object p1, p0, Ljcy;->ebb:Ljava/lang/String;

    .line 19439
    iget v0, p0, Ljcy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcy;->aez:I

    .line 19440
    return-object p0
.end method
