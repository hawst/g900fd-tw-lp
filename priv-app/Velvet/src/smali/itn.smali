.class public final Litn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHS:I

.field private dHT:I

.field private dHU:I

.field private dHV:I

.field private dHW:I

.field private dHX:I

.field private dHY:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4331
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4332
    iput v0, p0, Litn;->aez:I

    iput v0, p0, Litn;->dHS:I

    iput v0, p0, Litn;->dHT:I

    iput v0, p0, Litn;->dHU:I

    iput v0, p0, Litn;->dHV:I

    iput v0, p0, Litn;->dHW:I

    iput v0, p0, Litn;->dHX:I

    iput v0, p0, Litn;->dHY:I

    const/4 v0, 0x0

    iput-object v0, p0, Litn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litn;->eCz:I

    .line 4333
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 4179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHS:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHT:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHU:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHV:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHW:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHX:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litn;->dHY:I

    iget v0, p0, Litn;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Litn;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 4352
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4353
    const/4 v0, 0x1

    iget v1, p0, Litn;->dHS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4355
    :cond_0
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4356
    const/4 v0, 0x2

    iget v1, p0, Litn;->dHT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4358
    :cond_1
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 4359
    const/4 v0, 0x3

    iget v1, p0, Litn;->dHU:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4361
    :cond_2
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 4362
    const/4 v0, 0x4

    iget v1, p0, Litn;->dHV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4364
    :cond_3
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 4365
    const/4 v0, 0x5

    iget v1, p0, Litn;->dHW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4367
    :cond_4
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 4368
    const/4 v0, 0x6

    iget v1, p0, Litn;->dHX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4370
    :cond_5
    iget v0, p0, Litn;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 4371
    const/4 v0, 0x7

    iget v1, p0, Litn;->dHY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4373
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4374
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 4378
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4379
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4380
    const/4 v1, 0x1

    iget v2, p0, Litn;->dHS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4383
    :cond_0
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4384
    const/4 v1, 0x2

    iget v2, p0, Litn;->dHT:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4387
    :cond_1
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 4388
    const/4 v1, 0x3

    iget v2, p0, Litn;->dHU:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4391
    :cond_2
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 4392
    const/4 v1, 0x4

    iget v2, p0, Litn;->dHV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4395
    :cond_3
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 4396
    const/4 v1, 0x5

    iget v2, p0, Litn;->dHW:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4399
    :cond_4
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 4400
    const/4 v1, 0x6

    iget v2, p0, Litn;->dHX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4403
    :cond_5
    iget v1, p0, Litn;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 4404
    const/4 v1, 0x7

    iget v2, p0, Litn;->dHY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4407
    :cond_6
    return v0
.end method
