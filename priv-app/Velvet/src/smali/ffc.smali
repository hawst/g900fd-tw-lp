.class public final Lffc;
.super Lfeu;
.source "PG"


# instance fields
.field private final cpU:Ljbp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cpZ:Ljag;

.field private final mDirectionsLauncher:Lgah;

.field private mTravelReport:Lgca;


# direct methods
.method public constructor <init>(Lizj;Ljbp;Lgah;Lemp;)V
    .locals 7
    .param p2    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 45
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 46
    iput-object p2, p0, Lffc;->cpU:Ljbp;

    .line 47
    iput-object p3, p0, Lffc;->mDirectionsLauncher:Lgah;

    .line 48
    invoke-virtual {p0}, Lffc;->getEntry()Lizj;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, v0, Lizj;->dSi:Ljae;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lizj;->dSi:Ljae;

    iget-object v4, v0, Ljae;->dWa:[Ljag;

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    iget-object v6, v0, Ljag;->dWs:Ljah;

    if-eqz v6, :cond_2

    :goto_1
    iput-object v0, p0, Lffc;->cpZ:Ljag;

    .line 50
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    iget-object v2, v0, Ljag;->dWf:Ljaf;

    if-eqz v2, :cond_0

    iget-object v0, v0, Ljag;->dWf:Ljaf;

    iget-object v2, v0, Ljaf;->dMF:[Liyg;

    array-length v2, v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Ljaf;->dMF:[Liyg;

    aget-object v1, v0, v3

    .line 53
    :cond_0
    if-eqz v1, :cond_1

    .line 54
    new-instance v0, Lgca;

    invoke-direct {v0, v1, p4}, Lgca;-><init>(Liyg;Lemp;)V

    iput-object v0, p0, Lffc;->mTravelReport:Lgca;

    .line 57
    :cond_1
    return-void

    .line 48
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 65
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0a01e1

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "%s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lffc;->cpZ:Ljag;

    invoke-virtual {v4}, Ljag;->aZD()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lffc;->cpZ:Ljag;

    invoke-virtual {v4}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 187
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    iget-object v0, v0, Ljag;->dWs:Ljah;

    .line 188
    new-instance v1, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04a0

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lffc;->cpZ:Ljag;

    invoke-virtual {v5}, Ljag;->aZC()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lffc;->cpZ:Ljag;

    invoke-virtual {v5}, Ljag;->aZE()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    const/4 v5, 0x2

    invoke-virtual {v0}, Ljah;->bdV()I

    move-result v6

    invoke-static {p1, v6, v7}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Ljah;->bdU()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {p1, v6, v7, v10}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lffz;-><init>(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    iget-object v0, v0, Ljag;->dWf:Ljaf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lffc;->cpZ:Ljag;

    iget-object v0, v0, Ljag;->dWf:Ljaf;

    iget-object v0, v0, Ljaf;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    iget-object v0, v0, Ljag;->dWf:Ljaf;

    iget-object v0, v0, Ljaf;->aeB:Ljbp;

    invoke-virtual {v1, v0, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    .line 205
    :cond_0
    invoke-virtual {v1}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 76
    iget-object v1, p0, Lffc;->cpZ:Ljag;

    if-nez v1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-object v0

    .line 78
    :cond_1
    iget-object v1, p0, Lffc;->cpZ:Ljag;

    iget-object v1, v1, Ljag;->dWs:Ljah;

    if-eqz v1, :cond_0

    .line 80
    iget-object v0, p0, Lffc;->cpZ:Ljag;

    iget-object v0, v0, Ljag;->dWs:Ljah;

    .line 81
    invoke-virtual {v0}, Ljah;->bdU()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {p1, v2, v3, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-virtual {v0}, Ljah;->bdV()I

    move-result v0

    invoke-static {p1, v0, v6}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 87
    const v2, 0x7f0a01e4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string v0, "event"

    return-object v0
.end method

.method public final ayW()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x2

    return v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 94
    const v0, 0x7f020224

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lfgb;->cqD:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;
    .locals 6

    .prologue
    .line 123
    iget-object v0, p0, Lffc;->mTravelReport:Lgca;

    if-nez v0, :cond_0

    .line 124
    invoke-super {p0, p1}, Lfeu;->d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    .line 126
    :cond_0
    iget-object v0, p0, Lffc;->mTravelReport:Lgca;

    iget-object v1, v0, Lgca;->mRoute:Liyg;

    .line 127
    const/4 v0, 0x0

    .line 128
    if-eqz v1, :cond_1

    .line 129
    iget-object v2, p0, Lffc;->cpZ:Ljag;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lffc;->cpZ:Ljag;

    iget-object v2, v2, Ljag;->dWf:Ljaf;

    if-eqz v2, :cond_1

    .line 130
    iget-object v2, p0, Lffc;->cpZ:Ljag;

    iget-object v2, v2, Ljag;->dWf:Ljaf;

    .line 131
    iget-object v3, v2, Ljaf;->aeB:Ljbp;

    if-eqz v3, :cond_1

    .line 132
    invoke-static {p1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v3

    .line 134
    new-instance v0, Lffj;

    iget-object v4, p0, Lffc;->mDirectionsLauncher:Lgah;

    iget-object v5, p0, Lffc;->cpU:Ljbp;

    iget-object v2, v2, Ljaf;->aeB:Ljbp;

    invoke-direct {v0, v3, v4, v2, v1}, Lffj;-><init>(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Lgah;Ljbp;Liyg;)V

    .line 140
    :cond_1
    if-eqz v0, :cond_2

    .line 141
    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    goto :goto_0

    .line 144
    :cond_2
    invoke-super {p0, p1}, Lfeu;->d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;

    move-result-object v0

    goto :goto_0
.end method
