.class public final Lbzy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lym;


# instance fields
.field private synthetic aPT:Lcom/google/android/launcher/GEL;


# direct methods
.method public constructor <init>(Lcom/google/android/launcher/GEL;)V
    .locals 0

    .prologue
    .line 1391
    iput-object p1, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final ae(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1395
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v0, v0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v1, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-boolean v1, v1, Lcom/google/android/launcher/GEL;->aPK:Z

    iget-object v2, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-boolean v2, v2, Lcom/google/android/launcher/GEL;->aPM:Z

    invoke-virtual {v0, v1, v2}, Lfmd;->x(ZZ)V

    .line 1396
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iput-boolean v3, v0, Lcom/google/android/launcher/GEL;->aPK:Z

    .line 1397
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iput-boolean v3, v0, Lcom/google/android/launcher/GEL;->aPM:Z

    .line 1400
    if-nez p1, :cond_0

    .line 1401
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    invoke-static {v0, v3}, Lcom/google/android/launcher/GEL;->b(Lcom/google/android/launcher/GEL;Z)V

    .line 1403
    :cond_0
    return-void
.end method

.method public final if()V
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v0, v0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v1, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    invoke-virtual {v1}, Lcom/google/android/launcher/GEL;->isChangingConfigurations()Z

    move-result v1

    invoke-virtual {v0, v1}, Lfmd;->fr(Z)V

    .line 1408
    return-void
.end method

.method public final ig()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1426
    iget-object v1, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v1, v1, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v2, v1, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atK()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    move v1, v0

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return v0

    :cond_0
    iget-object v1, v1, Lfmd;->cvI:Lflw;

    iget-object v1, v1, Lflw;->cvw:Lflr;

    iget-object v1, v1, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aue()Z

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final m(F)V
    .locals 2

    .prologue
    .line 1412
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v0, v0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_0

    .line 1413
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v0, v0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, p1}, Ldpx;->D(F)V

    .line 1415
    :cond_0
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v0, v0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_1

    .line 1416
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    iget-object v0, v0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    invoke-virtual {v0, p1}, Lfmd;->D(F)V

    .line 1418
    :cond_1
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    invoke-static {v0}, Lcom/google/android/launcher/GEL;->e(Lcom/google/android/launcher/GEL;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1419
    iget-object v0, p0, Lbzy;->aPT:Lcom/google/android/launcher/GEL;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/launcher/GEL;->b(Lcom/google/android/launcher/GEL;Z)V

    .line 1421
    :cond_2
    return-void
.end method
