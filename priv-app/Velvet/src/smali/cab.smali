.class public final Lcab;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static aPV:Lcab;


# instance fields
.field public final aPu:Lenz;

.field public final mNowRemoteClient:Lfml;

.field public final mTaskRunner:Lerk;


# direct methods
.method private constructor <init>(Lerk;Lfml;Lenz;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcab;->mTaskRunner:Lerk;

    .line 33
    iput-object p2, p0, Lcab;->mNowRemoteClient:Lfml;

    .line 34
    iput-object p3, p0, Lcab;->aPu:Lenz;

    .line 35
    return-void
.end method

.method public static declared-synchronized af(Landroid/content/Context;)Lcab;
    .locals 5

    .prologue
    .line 38
    const-class v1, Lcab;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lenu;->auR()V

    .line 39
    sget-object v0, Lcab;->aPV:Lcab;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Leoe;

    invoke-direct {v0}, Leoe;-><init>()V

    new-instance v2, Leoa;

    const-string v3, "GELServices"

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Leoa;-><init>(Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    new-instance v3, Lcac;

    invoke-direct {v3, v0, v2}, Lcac;-><init>(Leqo;Ljava/util/concurrent/Executor;)V

    new-instance v0, Lfml;

    invoke-direct {v0, p0, v3}, Lfml;-><init>(Landroid/content/Context;Lerk;)V

    new-instance v2, Lenz;

    invoke-direct {v2, p0}, Lenz;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcab;

    invoke-direct {v4, v3, v0, v2}, Lcab;-><init>(Lerk;Lfml;Lenz;)V

    sput-object v4, Lcab;->aPV:Lcab;

    .line 42
    :cond_0
    sget-object v0, Lcab;->aPV:Lcab;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
