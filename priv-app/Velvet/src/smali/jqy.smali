.class public final Ljqy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ezH:[Ljqy;


# instance fields
.field private aez:I

.field private afW:Ljava/lang/String;

.field private akf:Ljava/lang/String;

.field public ezI:[I

.field public ezJ:[I

.field private ezK:I

.field private ezL:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3496
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3497
    iput v1, p0, Ljqy;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqy;->akf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljqy;->afW:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljqy;->ezI:[I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljqy;->ezJ:[I

    iput v1, p0, Ljqy;->ezK:I

    iput v1, p0, Ljqy;->ezL:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqy;->eCz:I

    .line 3498
    return-void
.end method

.method public static bsL()[Ljqy;
    .locals 2

    .prologue
    .line 3395
    sget-object v0, Ljqy;->ezH:[Ljqy;

    if-nez v0, :cond_1

    .line 3396
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3398
    :try_start_0
    sget-object v0, Ljqy;->ezH:[Ljqy;

    if-nez v0, :cond_0

    .line 3399
    const/4 v0, 0x0

    new-array v0, v0, [Ljqy;

    sput-object v0, Ljqy;->ezH:[Ljqy;

    .line 3401
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3403
    :cond_1
    sget-object v0, Ljqy;->ezH:[Ljqy;

    return-object v0

    .line 3401
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3389
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqy;->akf:Ljava/lang/String;

    iget v0, p0, Ljqy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqy;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x18

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqy;->ezI:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqy;->ezI:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqy;->ezI:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljqy;->ezI:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljqy;->ezI:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljqy;->ezI:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljqy;->ezI:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljqy;->ezI:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqy;->ezK:I

    iget v0, p0, Ljqy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqy;->aez:I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqy;->ezL:I

    iget v0, p0, Ljqy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqy;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqy;->afW:Ljava/lang/String;

    iget v0, p0, Ljqy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqy;->aez:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqy;->ezJ:[I

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljqy;->ezJ:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Ljqy;->ezJ:[I

    array-length v0, v0

    goto :goto_6

    :cond_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljqy;->ezJ:[I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_b

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljqy;->ezJ:[I

    if-nez v2, :cond_d

    move v2, v1

    :goto_9
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_c

    iget-object v4, p0, Ljqy;->ezJ:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_a
    array-length v4, v0

    if-ge v2, v4, :cond_e

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_d
    iget-object v2, p0, Ljqy;->ezJ:[I

    array-length v2, v2

    goto :goto_9

    :cond_e
    iput-object v0, p0, Ljqy;->ezJ:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3516
    iget v0, p0, Ljqy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3517
    const/4 v0, 0x1

    iget-object v2, p0, Ljqy;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3519
    :cond_0
    iget-object v0, p0, Ljqy;->ezI:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljqy;->ezI:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 3520
    :goto_0
    iget-object v2, p0, Ljqy;->ezI:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3521
    const/4 v2, 0x3

    iget-object v3, p0, Ljqy;->ezI:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 3520
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3524
    :cond_1
    iget v0, p0, Ljqy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3525
    const/4 v0, 0x4

    iget v2, p0, Ljqy;->ezK:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 3527
    :cond_2
    iget v0, p0, Ljqy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 3528
    const/4 v0, 0x5

    iget v2, p0, Ljqy;->ezL:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 3530
    :cond_3
    iget v0, p0, Ljqy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 3531
    const/4 v0, 0x6

    iget-object v2, p0, Ljqy;->afW:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3533
    :cond_4
    iget-object v0, p0, Ljqy;->ezJ:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljqy;->ezJ:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 3534
    :goto_1
    iget-object v0, p0, Ljqy;->ezJ:[I

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 3535
    const/4 v0, 0x7

    iget-object v2, p0, Ljqy;->ezJ:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 3534
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3538
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3539
    return-void
.end method

.method public final bsM()I
    .locals 1

    .prologue
    .line 3461
    iget v0, p0, Ljqy;->ezK:I

    return v0
.end method

.method public final bsN()Z
    .locals 1

    .prologue
    .line 3469
    iget v0, p0, Ljqy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsO()I
    .locals 1

    .prologue
    .line 3480
    iget v0, p0, Ljqy;->ezL:I

    return v0
.end method

.method public final bsP()Z
    .locals 1

    .prologue
    .line 3488
    iget v0, p0, Ljqy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3411
    iget-object v0, p0, Ljqy;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 3543
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3544
    iget v1, p0, Ljqy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3545
    const/4 v1, 0x1

    iget-object v3, p0, Ljqy;->akf:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3548
    :cond_0
    iget-object v1, p0, Ljqy;->ezI:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljqy;->ezI:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 3550
    :goto_0
    iget-object v4, p0, Ljqy;->ezI:[I

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 3551
    iget-object v4, p0, Ljqy;->ezI:[I

    aget v4, v4, v1

    .line 3552
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 3550
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3555
    :cond_1
    add-int/2addr v0, v3

    .line 3556
    iget-object v1, p0, Ljqy;->ezI:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3558
    :cond_2
    iget v1, p0, Ljqy;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 3559
    const/4 v1, 0x4

    iget v3, p0, Ljqy;->ezK:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3562
    :cond_3
    iget v1, p0, Ljqy;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 3563
    const/4 v1, 0x5

    iget v3, p0, Ljqy;->ezL:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3566
    :cond_4
    iget v1, p0, Ljqy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 3567
    const/4 v1, 0x6

    iget-object v3, p0, Ljqy;->afW:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3570
    :cond_5
    iget-object v1, p0, Ljqy;->ezJ:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljqy;->ezJ:[I

    array-length v1, v1

    if-lez v1, :cond_7

    move v1, v2

    .line 3572
    :goto_1
    iget-object v3, p0, Ljqy;->ezJ:[I

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 3573
    iget-object v3, p0, Ljqy;->ezJ:[I

    aget v3, v3, v2

    .line 3574
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 3572
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3577
    :cond_6
    add-int/2addr v0, v1

    .line 3578
    iget-object v1, p0, Ljqy;->ezJ:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3580
    :cond_7
    return v0
.end method

.method public final yq(Ljava/lang/String;)Ljqy;
    .locals 1

    .prologue
    .line 3414
    if-nez p1, :cond_0

    .line 3415
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3417
    :cond_0
    iput-object p1, p0, Ljqy;->akf:Ljava/lang/String;

    .line 3418
    iget v0, p0, Ljqy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqy;->aez:I

    .line 3419
    return-object p0
.end method
