.class public final Ladg;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private final SP:[I

.field private final SQ:Landroid/app/WallpaperManager;

.field public SR:Z

.field public SS:Z

.field public zG:I

.field public zH:I

.field public zK:I

.field public zO:I

.field public zP:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Ladg;->SP:[I

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Ladg;->SS:Z

    .line 51
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    iput-object v0, p0, Ladg;->SQ:Landroid/app/WallpaperManager;

    .line 52
    return-void
.end method


# virtual methods
.method public final ad(Landroid/view/View;)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 141
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 142
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v7

    .line 143
    iget v1, p0, Ladg;->zG:I

    .line 144
    iget v2, p0, Ladg;->zH:I

    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 146
    iget-boolean v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bt:Z

    if-nez v3, :cond_1

    .line 147
    iget v3, p0, Ladg;->zO:I

    iget v4, p0, Ladg;->zP:I

    invoke-virtual {p0}, Ladg;->ke()Z

    move-result v5

    iget v6, p0, Ladg;->zK:I

    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher3/CellLayout$LayoutParams;->a(IIIIZI)V

    .line 150
    instance-of v1, p1, Lyx;

    if-nez v1, :cond_0

    .line 154
    invoke-virtual {p0}, Ladg;->kd()I

    move-result v1

    .line 155
    const/4 v2, 0x0

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    div-float/2addr v1, v9

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 156
    iget v2, v7, Ltu;->Dv:I

    int-to-float v2, v2

    div-float/2addr v2, v9

    float-to-int v2, v2

    .line 157
    invoke-virtual {p1, v2, v1, v2, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 165
    :cond_0
    :goto_0
    iget v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 166
    iget v0, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 168
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 169
    return-void

    .line 160
    :cond_1
    iput v8, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 161
    iput v8, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 162
    invoke-virtual {p0}, Ladg;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    .line 163
    invoke-virtual {p0}, Ladg;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method public final b(IIIIII)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Ladg;->zG:I

    .line 57
    iput p2, p0, Ladg;->zH:I

    .line 58
    iput p3, p0, Ladg;->zO:I

    .line 59
    iput p4, p0, Ladg;->zP:I

    .line 60
    iput p5, p0, Ladg;->zK:I

    .line 61
    return-void
.end method

.method public final cancelLongPress()V
    .locals 3

    .prologue
    .line 221
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 224
    invoke-virtual {p0}, Ladg;->getChildCount()I

    move-result v1

    .line 225
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 226
    invoke-virtual {p0, v0}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 227
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 94
    return-void
.end method

.method public final kd()I
    .locals 3

    .prologue
    .line 134
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 135
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 136
    invoke-virtual {p0}, Ladg;->getMeasuredHeight()I

    move-result v1

    iget-boolean v2, p0, Ladg;->SR:Z

    if-eqz v2, :cond_0

    iget v0, v0, Ltu;->DV:I

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_0
    iget v0, v0, Ltu;->DM:I

    goto :goto_0
.end method

.method public ke()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 172
    iget-boolean v2, p0, Ladg;->SS:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Ladg;->getLayoutDirection()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected final onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 181
    invoke-virtual {p0}, Ladg;->getChildCount()I

    move-result v8

    .line 182
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_1

    .line 183
    invoke-virtual {p0, v7}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 184
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    .line 185
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 186
    iget v3, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 187
    iget v5, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 188
    iget v0, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    add-int/2addr v0, v3

    iget v2, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    add-int/2addr v2, v5

    invoke-virtual {v1, v3, v5, v0, v2}, Landroid/view/View;->layout(IIII)V

    .line 190
    iget-boolean v0, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bv:Z

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bv:Z

    .line 193
    iget-object v6, p0, Ladg;->SP:[I

    .line 194
    invoke-virtual {p0, v6}, Ladg;->getLocationOnScreen([I)V

    .line 195
    iget-object v0, p0, Ladg;->SQ:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Ladg;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const-string v2, "android.home.drop"

    const/4 v9, 0x0

    aget v9, v6, v9

    add-int/2addr v3, v9

    iget v9, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->width:I

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v3, v9

    const/4 v9, 0x1

    aget v6, v6, v9

    add-int/2addr v5, v6

    iget v4, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->height:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/app/WallpaperManager;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)V

    .line 182
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 202
    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 5

    .prologue
    .line 98
    invoke-virtual {p0}, Ladg;->getChildCount()I

    move-result v1

    .line 100
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 101
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 102
    invoke-virtual {p0, v0, v2}, Ladg;->setMeasuredDimension(II)V

    .line 104
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 105
    invoke-virtual {p0, v0}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 107
    invoke-virtual {p0, v2}, Ladg;->ad(Landroid/view/View;)V

    .line 104
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_1
    return-void
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 211
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 212
    if-eqz p1, :cond_0

    .line 213
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 214
    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 215
    invoke-virtual {p0, v0}, Ladg;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    .line 217
    :cond_0
    return-void
.end method

.method public final setChildrenDrawingCacheEnabled(Z)V
    .locals 4

    .prologue
    .line 233
    invoke-virtual {p0}, Ladg;->getChildCount()I

    move-result v1

    .line 234
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 235
    invoke-virtual {p0, v0}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 236
    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 238
    invoke-virtual {v2}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p1, :cond_0

    .line 239
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 234
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_1
    return-void
.end method

.method public final setChildrenDrawnWithCacheEnabled(Z)V
    .locals 0

    .prologue
    .line 246
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 247
    return-void
.end method

.method public final shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return v0
.end method
