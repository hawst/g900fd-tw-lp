.class public Lcdt;
.super Lepm;
.source "PG"


# instance fields
.field private final aSB:Lcdf;

.field private final aSY:Lcdw;

.field private final aSZ:Ljava/util/concurrent/ExecutorService;

.field private final aSn:Lcdr;

.field private final aSo:Lgmj;

.field private final aSq:Z

.field private aTa:Ljava/util/concurrent/Future;

.field mS3Connection:Lcdq;


# direct methods
.method public constructor <init>(Lcdf;Lgmj;Lcdr;Lcdw;ZLjava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 79
    const-string v0, "S3Session"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 80
    iput-object p1, p0, Lcdt;->aSB:Lcdf;

    .line 81
    iput-object p2, p0, Lcdt;->aSo:Lgmj;

    .line 82
    iput-object p3, p0, Lcdt;->aSn:Lcdr;

    .line 83
    iput-boolean p5, p0, Lcdt;->aSq:Z

    .line 84
    iput-object p6, p0, Lcdt;->aSZ:Ljava/util/concurrent/ExecutorService;

    .line 85
    iput-object p4, p0, Lcdt;->aSY:Lcdw;

    .line 86
    return-void

    .line 79
    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method


# virtual methods
.method protected Df()V
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 162
    :cond_0
    return-void
.end method

.method final declared-synchronized close()V
    .locals 2

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcdt;->aTa:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 105
    iget-object v0, p0, Lcdt;->aSY:Lcdw;

    invoke-interface {v0}, Lcdw;->CZ()V

    .line 106
    iget-object v0, p0, Lcdt;->aSZ:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcdu;

    invoke-direct {v1, p0}, Lcdu;-><init>(Lcdt;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 113
    iget-object v0, p0, Lcdt;->aSZ:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lcdt;->mS3Connection:Lcdq;

    if-eqz v0, :cond_0

    .line 169
    const-string v0, "S3Session"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "S3Session not closed, connection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcdt;->mS3Connection:Lcdq;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcdt;->mS3Connection:Lcdq;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 173
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 174
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 119
    const/4 v1, 0x0

    .line 122
    :try_start_0
    iget-object v0, p0, Lcdt;->aSo:Lgmj;

    invoke-interface {v0}, Lgmj;->Dl()Lgmk;

    move-result-object v1

    .line 124
    const/4 v0, 0x7

    invoke-static {v0}, Lege;->ht(I)V

    iget-object v0, p0, Lcdt;->aSY:Lcdw;

    invoke-interface {v0}, Lcdw;->CW()V

    .line 126
    iget-object v0, p0, Lcdt;->aSn:Lcdr;

    iget-boolean v2, p0, Lcdt;->aSq:Z

    invoke-interface {v0, v2}, Lcdr;->cg(Z)Lcdq;

    move-result-object v0

    iput-object v0, p0, Lcdt;->mS3Connection:Lcdq;

    .line 127
    invoke-virtual {p0}, Lcdt;->Df()V

    .line 130
    invoke-virtual {v1}, Lgmk;->aHo()Ljwv;

    move-result-object v0

    .line 131
    iget-object v2, p0, Lcdt;->mS3Connection:Lcdq;

    new-instance v3, Lcdv;

    iget-object v4, p0, Lcdt;->aSB:Lcdf;

    iget-object v5, p0, Lcdt;->aSY:Lcdw;

    invoke-direct {v3, v4, v5}, Lcdv;-><init>(Lcdf;Lcdw;)V

    invoke-interface {v2, v3, v0}, Lcdq;->a(Lcdf;Ljwv;)V

    .line 134
    const/16 v0, 0x8

    invoke-static {v0}, Lege;->ht(I)V

    iget-object v0, p0, Lcdt;->aSY:Lcdw;

    invoke-interface {v0}, Lcdw;->CX()V

    .line 135
    invoke-virtual {p0}, Lcdt;->Df()V

    .line 138
    :goto_0
    invoke-virtual {v1}, Lgmk;->aHo()Ljwv;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {p0}, Lcdt;->Df()V

    .line 140
    iget-object v2, p0, Lcdt;->mS3Connection:Lcdq;

    invoke-interface {v2, v0}, Lcdq;->a(Ljwv;)V

    .line 141
    invoke-virtual {p0}, Lcdt;->Df()V
    :try_end_0
    .catch Leie; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_1
    iget-object v2, p0, Lcdt;->aSB:Lcdf;

    invoke-interface {v2, v0}, Lcdf;->c(Leie;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v1}, Lgmk;->aHp()V

    .line 155
    :cond_0
    :goto_1
    return-void

    .line 144
    :cond_1
    const/16 v0, 0x17

    :try_start_2
    invoke-static {v0}, Lege;->ht(I)V
    :try_end_2
    .catch Leie; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151
    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v1}, Lgmk;->aHp()V

    goto :goto_1

    .line 151
    :catch_1
    move-exception v0

    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {v1}, Lgmk;->aHp()V

    goto :goto_1

    .line 151
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 152
    invoke-virtual {v1}, Lgmk;->aHp()V

    :cond_2
    throw v0
.end method

.method final declared-synchronized start()V
    .locals 1

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcdt;->aSZ:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcdt;->aTa:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
