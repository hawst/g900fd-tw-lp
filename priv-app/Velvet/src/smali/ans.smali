.class public final Lans;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public aiw:Lanj;

.field private aix:Ljava/lang/String;

.field private aiy:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3796
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3797
    iput v1, p0, Lans;->aez:I

    iput-object v2, p0, Lans;->aiw:Lanj;

    const-string v0, ""

    iput-object v0, p0, Lans;->aix:Ljava/lang/String;

    iput-boolean v1, p0, Lans;->aiy:Z

    iput-object v2, p0, Lans;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lans;->eCz:I

    .line 3798
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3733
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lans;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lans;->aiw:Lanj;

    if-nez v0, :cond_1

    new-instance v0, Lanj;

    invoke-direct {v0}, Lanj;-><init>()V

    iput-object v0, p0, Lans;->aiw:Lanj;

    :cond_1
    iget-object v0, p0, Lans;->aiw:Lanj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lans;->aix:Ljava/lang/String;

    iget v0, p0, Lans;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lans;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lans;->aiy:Z

    iget v0, p0, Lans;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lans;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3813
    iget-object v0, p0, Lans;->aiw:Lanj;

    if-eqz v0, :cond_0

    .line 3814
    const/4 v0, 0x1

    iget-object v1, p0, Lans;->aiw:Lanj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3816
    :cond_0
    iget v0, p0, Lans;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 3817
    const/4 v0, 0x2

    iget-object v1, p0, Lans;->aix:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3819
    :cond_1
    iget v0, p0, Lans;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 3820
    const/4 v0, 0x3

    iget-boolean v1, p0, Lans;->aiy:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 3822
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3823
    return-void
.end method

.method public final aU(Ljava/lang/String;)Lans;
    .locals 1

    .prologue
    .line 3761
    if-nez p1, :cond_0

    .line 3762
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3764
    :cond_0
    iput-object p1, p0, Lans;->aix:Ljava/lang/String;

    .line 3765
    iget v0, p0, Lans;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lans;->aez:I

    .line 3766
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3827
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3828
    iget-object v1, p0, Lans;->aiw:Lanj;

    if-eqz v1, :cond_0

    .line 3829
    const/4 v1, 0x1

    iget-object v2, p0, Lans;->aiw:Lanj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3832
    :cond_0
    iget v1, p0, Lans;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 3833
    const/4 v1, 0x2

    iget-object v2, p0, Lans;->aix:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3836
    :cond_1
    iget v1, p0, Lans;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 3837
    const/4 v1, 0x3

    iget-boolean v2, p0, Lans;->aiy:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3840
    :cond_2
    return v0
.end method

.method public final pL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3758
    iget-object v0, p0, Lans;->aix:Ljava/lang/String;

    return-object v0
.end method

.method public final pM()Z
    .locals 1

    .prologue
    .line 3769
    iget v0, p0, Lans;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pN()Z
    .locals 1

    .prologue
    .line 3780
    iget-boolean v0, p0, Lans;->aiy:Z

    return v0
.end method
