.class public final Lfoy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 563
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-class v0, Ljdk;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljdk;

    const-class v1, Ljde;

    invoke-static {p1, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Ljde;

    new-instance v3, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-direct {v3, v2, v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;-><init>(Ljava/util/Map;Ljdk;Ljde;)V

    return-object v3
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 563
    new-array v0, p1, [Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    return-object v0
.end method
