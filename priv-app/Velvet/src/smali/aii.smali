.class public final Laii;
.super Laif;
.source "PG"


# instance fields
.field private dz:Landroid/net/Uri;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;I)V
    .locals 1

    .prologue
    .line 262
    const/16 v0, 0x400

    invoke-direct {p0, v0}, Laif;-><init>(I)V

    .line 263
    iput-object p1, p0, Laii;->mContext:Landroid/content/Context;

    .line 264
    iput-object p2, p0, Laii;->dz:Landroid/net/Uri;

    .line 265
    return-void
.end method

.method private kp()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Laii;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Laii;->dz:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 268
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v1
.end method


# virtual methods
.method public final a(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 291
    :try_start_0
    invoke-direct {p0}, Laii;->kp()Ljava/io/InputStream;

    move-result-object v2

    .line 292
    const/4 v0, 0x0

    invoke-static {v2, v0, p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 293
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :goto_0
    return-object v0

    .line 295
    :catch_0
    move-exception v0

    .line 296
    const-string v2, "BitmapRegionTileSource"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to load URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Laii;->dz:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 297
    goto :goto_0
.end method

.method public final a(Lqp;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 302
    const/4 v2, 0x0

    .line 304
    :try_start_0
    invoke-direct {p0}, Laii;->kp()Ljava/io/InputStream;

    move-result-object v2

    .line 305
    invoke-virtual {p1, v2}, Lqp;->a(Ljava/io/InputStream;)V

    .line 306
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x1

    .line 316
    :goto_0
    return v0

    .line 308
    :catch_0
    move-exception v1

    .line 309
    :try_start_1
    const-string v3, "BitmapRegionTileSource"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to load URI "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Laii;->dz:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 311
    :catch_1
    move-exception v1

    .line 312
    :try_start_2
    const-string v3, "BitmapRegionTileSource"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to load URI "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Laii;->dz:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 313
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 314
    :catch_2
    move-exception v1

    .line 315
    :try_start_3
    const-string v3, "BitmapRegionTileSource"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to read EXIF for URI "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Laii;->dz:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 316
    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Lqm;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public final lP()Laik;
    .locals 4

    .prologue
    .line 273
    :try_start_0
    invoke-direct {p0}, Laii;->kp()Ljava/io/InputStream;

    move-result-object v1

    .line 274
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lail;->a(Ljava/io/InputStream;Z)Lail;

    move-result-object v0

    .line 276
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 277
    if-nez v0, :cond_0

    .line 278
    invoke-direct {p0}, Laii;->kp()Ljava/io/InputStream;

    move-result-object v1

    .line 279
    invoke-static {v1}, Laij;->d(Ljava/io/InputStream;)Laij;

    move-result-object v0

    .line 280
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :cond_0
    :goto_0
    return-object v0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    const-string v1, "BitmapRegionTileSource"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Laii;->dz:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 285
    const/4 v0, 0x0

    goto :goto_0
.end method
