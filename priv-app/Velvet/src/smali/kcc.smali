.class public Lkcc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final eRb:Ljava/net/CacheResponse;


# instance fields
.field connected:Z

.field private eQV:Ljava/net/CacheRequest;

.field protected final eRc:Lkcp;

.field protected final eRd:Lkas;

.field private eRe:Lkav;

.field protected eRf:Lkac;

.field protected eRg:Lkcx;

.field private eRh:Ljava/io/OutputStream;

.field private eRi:Lkcz;

.field private eRj:Ljava/io/InputStream;

.field private eRk:Ljava/io/InputStream;

.field private eRl:Ljava/net/CacheResponse;

.field private eRm:J

.field private eRn:Z

.field final eRo:Ljava/net/URI;

.field final eRp:Lkcs;

.field eRq:Lkcu;

.field private eRr:Lkcu;

.field private eRs:Ljava/io/InputStream;

.field private eRt:Z

.field private eRu:Z

.field protected final method:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lkcd;

    invoke-direct {v0}, Lkcd;-><init>()V

    sput-object v0, Lkcc;->eRb:Ljava/net/CacheResponse;

    return-void
.end method

.method public constructor <init>(Lkas;Lkcp;Ljava/lang/String;Lkcq;Lkac;Lkcw;)V
    .locals 3

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkcc;->eRm:J

    .line 155
    iput-object p1, p0, Lkcc;->eRd:Lkas;

    .line 156
    iput-object p2, p0, Lkcc;->eRc:Lkcp;

    .line 157
    iput-object p3, p0, Lkcc;->method:Ljava/lang/String;

    .line 158
    iput-object p5, p0, Lkcc;->eRf:Lkac;

    .line 159
    iput-object p6, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    .line 162
    :try_start_0
    invoke-static {}, Lkbl;->byd()Lkbl;

    invoke-interface {p2}, Lkcp;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lkbl;->c(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lkcc;->eRo:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    new-instance v0, Lkcs;

    iget-object v1, p0, Lkcc;->eRo:Ljava/net/URI;

    new-instance v2, Lkcq;

    invoke-direct {v2, p4}, Lkcq;-><init>(Lkcq;)V

    invoke-direct {v0, v1, v2}, Lkcs;-><init>(Ljava/net/URI;Lkcq;)V

    iput-object v0, p0, Lkcc;->eRp:Lkcs;

    .line 168
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Lkcu;Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lkcc;->eRk:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 333
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 335
    :cond_0
    iput-object p1, p0, Lkcc;->eRq:Lkcu;

    .line 336
    if-eqz p2, :cond_1

    .line 337
    invoke-direct {p0, p2}, Lkcc;->q(Ljava/io/InputStream;)V

    .line 339
    :cond_1
    return-void
.end method

.method private byx()Ljava/lang/String;
    .locals 4

    .prologue
    .line 551
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->bxG()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "HTTP/1.1"

    .line 553
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lkcc;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v1}, Lkcp;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {p0}, Lkcc;->byy()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 551
    :cond_1
    const-string v0, "HTTP/1.0"

    goto :goto_0

    .line 553
    :cond_2
    invoke-static {v1}, Lkcc;->e(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static byz()Ljava/lang/String;
    .locals 2

    .prologue
    .line 596
    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 597
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Java"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 571
    invoke-virtual {p0}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v0

    .line 572
    if-nez v0, :cond_1

    .line 573
    const-string v0, "/"

    .line 577
    :cond_0
    :goto_0
    return-object v0

    .line 574
    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 575
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 601
    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    .line 602
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 603
    if-lez v1, :cond_0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkbt;->Au(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 604
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 606
    :cond_0
    return-object v0
.end method

.method private q(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 457
    iput-object p1, p0, Lkcc;->eRj:Ljava/io/InputStream;

    .line 458
    iget-boolean v0, p0, Lkcc;->eRn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0}, Lkcu;->byT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0}, Lkcu;->byU()V

    .line 467
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0}, Lkcu;->byV()V

    .line 468
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, p1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lkcc;->eRk:Ljava/io/InputStream;

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_0
    iput-object p1, p0, Lkcc;->eRk:Ljava/io/InputStream;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lkcq;)V
    .locals 3

    .prologue
    .line 681
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxL()Ljava/net/CookieHandler;

    move-result-object v0

    .line 682
    if-eqz v0, :cond_0

    .line 683
    iget-object v1, p0, Lkcc;->eRo:Ljava/net/URI;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lkcq;->jK(Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 685
    :cond_0
    return-void
.end method

.method protected a(Ljava/net/CacheResponse;)Z
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x1

    return v0
.end method

.method public final byA()V
    .locals 6

    .prologue
    .line 614
    invoke-virtual {p0}, Lkcc;->byp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 615
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    iget-object v1, p0, Lkcc;->eRe:Lkav;

    invoke-virtual {v0, v1}, Lkcu;->b(Lkav;)V

    .line 674
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    if-nez v0, :cond_2

    .line 620
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "readResponse() without sendRequest()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 623
    :cond_2
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    invoke-virtual {v0}, Lkav;->bxU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    iget-wide v0, p0, Lkcc;->eRm:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 628
    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    instance-of v0, v0, Lkcw;

    if-eqz v0, :cond_3

    .line 629
    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    check-cast v0, Lkcw;

    invoke-virtual {v0}, Lkcw;->bza()I

    move-result v0

    .line 630
    iget-object v1, p0, Lkcc;->eRp:Lkcs;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lkcs;->setContentLength(J)V

    .line 632
    :cond_3
    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    invoke-interface {v0}, Lkcz;->byE()V

    .line 635
    :cond_4
    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    if-eqz v0, :cond_5

    .line 636
    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 637
    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    instance-of v0, v0, Lkcw;

    if-eqz v0, :cond_5

    .line 638
    iget-object v1, p0, Lkcc;->eRi:Lkcz;

    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    check-cast v0, Lkcw;

    invoke-interface {v1, v0}, Lkcz;->a(Lkcw;)V

    .line 642
    :cond_5
    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    invoke-interface {v0}, Lkcz;->byD()V

    .line 644
    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    invoke-interface {v0}, Lkcz;->byF()Lkcu;

    move-result-object v0

    iput-object v0, p0, Lkcc;->eRq:Lkcu;

    .line 645
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    iget-wide v2, p0, Lkcc;->eRm:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lkcu;->n(JJ)V

    .line 646
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    iget-object v1, p0, Lkcc;->eRe:Lkav;

    invoke-virtual {v0, v1}, Lkcu;->b(Lkav;)V

    .line 648
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    sget-object v1, Lkav;->ePS:Lkav;

    if-ne v0, v1, :cond_7

    .line 649
    iget-object v0, p0, Lkcc;->eRr:Lkcu;

    iget-object v1, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0, v1}, Lkcu;->a(Lkcu;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 650
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkcc;->jI(Z)V

    .line 651
    iget-object v0, p0, Lkcc;->eRr:Lkcu;

    iget-object v1, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0, v1}, Lkcu;->b(Lkcu;)Lkcu;

    move-result-object v0

    .line 652
    iput-object v0, p0, Lkcc;->eRq:Lkcu;

    .line 658
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxN()Lkau;

    move-result-object v0

    .line 659
    invoke-interface {v0}, Lkau;->bxI()V

    .line 660
    iget-object v1, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    iget-object v2, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v2}, Lkcp;->byI()Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkau;->a(Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V

    .line 662
    iget-object v0, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lkcc;->q(Ljava/io/InputStream;)V

    goto/16 :goto_0

    .line 665
    :cond_6
    iget-object v0, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 669
    :cond_7
    invoke-virtual {p0}, Lkcc;->byw()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 670
    iget-object v0, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v0}, Lkcp;->getUseCaches()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxN()Lkau;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v1, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v1}, Lkcp;->byI()Ljava/net/HttpURLConnection;

    move-result-object v1

    iget-object v2, p0, Lkcc;->eRq:Lkcu;

    iget-object v3, p0, Lkcc;->eRp:Lkcs;

    invoke-virtual {v2, v3}, Lkcu;->a(Lkcs;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-interface {v0, v1, v2}, Lkau;->b(Ljava/lang/String;Ljava/net/URI;)V

    .line 673
    :cond_8
    :goto_1
    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    iget-object v1, p0, Lkcc;->eQV:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lkcz;->a(Ljava/net/CacheRequest;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lkcc;->q(Ljava/io/InputStream;)V

    goto/16 :goto_0

    .line 670
    :cond_9
    iget-object v2, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-interface {v0, v2, v1}, Lkau;->put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;

    move-result-object v0

    iput-object v0, p0, Lkcc;->eQV:Ljava/net/CacheRequest;

    goto :goto_1
.end method

.method protected byB()Lkay;
    .locals 1

    .prologue
    .line 677
    const/4 v0, 0x0

    return-object v0
.end method

.method public final byk()Ljava/net/URI;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lkcc;->eRo:Ljava/net/URI;

    return-object v0
.end method

.method public final byl()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 180
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    if-eqz v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->eRZ:Lkcq;

    invoke-direct {p0}, Lkcc;->byx()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkcq;->Ay(Ljava/lang/String;)V

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->aen:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    invoke-static {}, Lkcc;->byz()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lkcs;->aen:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "User-Agent"

    invoke-virtual {v2, v3}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_2
    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "User-Agent"

    invoke-virtual {v2, v3, v1}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lkcs;->aen:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->ePZ:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v1, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v1}, Lkcp;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-static {v1}, Lkcc;->f(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lkcs;->ePZ:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Host"

    invoke-virtual {v2, v3}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_4
    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Host"

    invoke-virtual {v2, v3, v1}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lkcs;->ePZ:Ljava/lang/String;

    :cond_5
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->bxG()I

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->eSi:Ljava/lang/String;

    if-nez v0, :cond_8

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    const-string v1, "Keep-Alive"

    iget-object v2, v0, Lkcs;->eSi:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Connection"

    invoke-virtual {v2, v3}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_7
    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Connection"

    invoke-virtual {v2, v3, v1}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lkcs;->eSi:Ljava/lang/String;

    :cond_8
    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->eSj:Ljava/lang/String;

    if-nez v0, :cond_a

    iput-boolean v6, p0, Lkcc;->eRn:Z

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    const-string v1, "gzip"

    iget-object v2, v0, Lkcs;->eSj:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Accept-Encoding"

    invoke-virtual {v2, v3}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_9
    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Accept-Encoding"

    invoke-virtual {v2, v3, v1}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lkcs;->eSj:Ljava/lang/String;

    :cond_a
    invoke-virtual {p0}, Lkcc;->byn()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->bfI:Ljava/lang/String;

    if-nez v0, :cond_c

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    const-string v1, "application/x-www-form-urlencoded"

    iget-object v2, v0, Lkcs;->bfI:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Content-Type"

    invoke-virtual {v2, v3}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_b
    iget-object v2, v0, Lkcs;->eRZ:Lkcq;

    const-string v3, "Content-Type"

    invoke-virtual {v2, v3, v1}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lkcs;->bfI:Ljava/lang/String;

    :cond_c
    iget-object v0, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v0}, Lkcp;->getIfModifiedSince()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lkcc;->eRp:Lkcs;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lkcs;->b(Ljava/util/Date;)V

    :cond_d
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxL()Ljava/net/CookieHandler;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v1, p0, Lkcc;->eRp:Lkcs;

    iget-object v2, p0, Lkcc;->eRo:Ljava/net/URI;

    iget-object v3, p0, Lkcc;->eRp:Lkcs;

    iget-object v3, v3, Lkcs;->eRZ:Lkcq;

    invoke-virtual {v3, v5}, Lkcq;->jK(Z)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lkcs;->v(Ljava/util/Map;)V

    .line 185
    :cond_e
    sget-object v0, Lkav;->ePT:Lkav;

    iput-object v0, p0, Lkcc;->eRe:Lkav;

    iget-object v0, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v0}, Lkcp;->getUseCaches()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxN()Lkau;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v1, p0, Lkcc;->eRo:Ljava/net/URI;

    iget-object v2, p0, Lkcc;->method:Ljava/lang/String;

    iget-object v3, p0, Lkcc;->eRp:Lkcs;

    iget-object v3, v3, Lkcs;->eRZ:Lkcq;

    invoke-virtual {v3, v5}, Lkcq;->jK(Z)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lkau;->get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-virtual {p0, v0}, Lkcc;->a(Ljava/net/CacheResponse;)Z

    move-result v2

    if-eqz v2, :cond_f

    if-eqz v1, :cond_f

    iget-object v2, p0, Lkcc;->eRs:Ljava/io/InputStream;

    if-nez v2, :cond_14

    :cond_f
    iget-object v0, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 186
    :cond_10
    :goto_1
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxN()Lkau;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_11

    .line 188
    iget-object v1, p0, Lkcc;->eRe:Lkav;

    invoke-interface {v0, v1}, Lkau;->a(Lkav;)V

    .line 195
    :cond_11
    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-boolean v0, v0, Lkcs;->eSe:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lkcc;->eRe:Lkav;

    invoke-virtual {v0}, Lkav;->bxU()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 196
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    sget-object v1, Lkav;->ePS:Lkav;

    if-ne v0, v1, :cond_12

    .line 197
    iget-object v0, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 199
    :cond_12
    sget-object v0, Lkav;->ePR:Lkav;

    iput-object v0, p0, Lkcc;->eRe:Lkav;

    .line 200
    sget-object v0, Lkcc;->eRb:Ljava/net/CacheResponse;

    iput-object v0, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    .line 201
    iget-object v0, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, v6}, Lkcq;->a(Ljava/util/Map;Z)Lkcq;

    move-result-object v0

    .line 202
    new-instance v1, Lkcu;

    iget-object v2, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Lkcu;-><init>(Ljava/net/URI;Lkcq;)V

    iget-object v0, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lkcc;->a(Lkcu;Ljava/io/InputStream;)V

    .line 205
    :cond_13
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    invoke-virtual {v0}, Lkav;->bxU()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 206
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-nez v0, :cond_1a

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-nez v0, :cond_1a

    iget-object v0, p0, Lkcc;->eRg:Lkcx;

    if-nez v0, :cond_19

    iget-object v0, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_18

    new-instance v0, Ljava/net/UnknownHostException;

    iget-object v1, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_14
    invoke-static {v1, v6}, Lkcq;->a(Ljava/util/Map;Z)Lkcq;

    move-result-object v1

    new-instance v2, Lkcu;

    iget-object v3, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-direct {v2, v3, v1}, Lkcu;-><init>(Ljava/net/URI;Lkcq;)V

    iput-object v2, p0, Lkcc;->eRr:Lkcu;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lkcc;->eRr:Lkcu;

    iget-object v5, p0, Lkcc;->eRp:Lkcs;

    invoke-virtual {v1, v2, v3, v5}, Lkcu;->a(JLkcs;)Lkav;

    move-result-object v1

    iput-object v1, p0, Lkcc;->eRe:Lkav;

    iget-object v1, p0, Lkcc;->eRe:Lkav;

    sget-object v2, Lkav;->ePR:Lkav;

    if-ne v1, v2, :cond_15

    iput-object v0, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    iget-object v0, p0, Lkcc;->eRr:Lkcu;

    iget-object v1, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-direct {p0, v0, v1}, Lkcc;->a(Lkcu;Ljava/io/InputStream;)V

    goto/16 :goto_1

    :cond_15
    iget-object v1, p0, Lkcc;->eRe:Lkav;

    sget-object v2, Lkav;->ePS:Lkav;

    if-ne v1, v2, :cond_16

    iput-object v0, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    goto/16 :goto_1

    :cond_16
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    sget-object v1, Lkav;->ePT:Lkav;

    if-ne v0, v1, :cond_17

    iget-object v0, p0, Lkcc;->eRs:Ljava/io/InputStream;

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    goto/16 :goto_1

    :cond_17
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 206
    :cond_18
    iget-object v0, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxO()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    :goto_2
    new-instance v0, Lkab;

    iget-object v2, p0, Lkcc;->eRo:Ljava/net/URI;

    invoke-static {v2}, Lkbt;->a(Ljava/net/URI;)I

    move-result v2

    iget-object v5, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v5}, Lkas;->bxP()Lkap;

    move-result-object v5

    iget-object v6, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v6}, Lkas;->bxK()Ljava/net/Proxy;

    move-result-object v6

    iget-object v7, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v7}, Lkas;->bxT()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lkab;-><init>(Ljava/lang/String;ILjavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lkap;Ljava/net/Proxy;Ljava/util/List;)V

    new-instance v1, Lkcx;

    iget-object v3, p0, Lkcc;->eRo:Ljava/net/URI;

    iget-object v2, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v2}, Lkas;->getProxySelector()Ljava/net/ProxySelector;

    move-result-object v4

    iget-object v2, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v2}, Lkas;->bxQ()Lkad;

    move-result-object v5

    sget-object v6, Lkbi;->eQA:Lkbi;

    iget-object v2, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v2}, Lkas;->bxS()Lkax;

    move-result-object v7

    move-object v2, v0

    invoke-direct/range {v1 .. v7}, Lkcx;-><init>(Lkab;Ljava/net/URI;Ljava/net/ProxySelector;Lkad;Lkbi;Lkax;)V

    iput-object v1, p0, Lkcc;->eRg:Lkcx;

    :cond_19
    iget-object v0, p0, Lkcc;->eRg:Lkcx;

    iget-object v1, p0, Lkcc;->method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lkcx;->AE(Ljava/lang/String;)Lkac;

    move-result-object v0

    iput-object v0, p0, Lkcc;->eRf:Lkac;

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1b

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    iget-object v1, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v1}, Lkas;->getConnectTimeout()I

    move-result v1

    iget-object v2, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v2}, Lkas;->getReadTimeout()I

    move-result v2

    invoke-virtual {p0}, Lkcc;->byB()Lkay;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lkac;->a(IILkay;)V

    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxQ()Lkad;

    move-result-object v0

    iget-object v1, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0, v1}, Lkad;->b(Lkac;)V

    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxS()Lkax;

    move-result-object v0

    iget-object v1, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v1}, Lkac;->bxC()Lkaw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkax;->a(Lkaw;)V

    :goto_3
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {p0, v0}, Lkcc;->c(Lkac;)V

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->bxC()Lkaw;

    move-result-object v0

    iget-object v0, v0, Lkaw;->eOL:Ljava/net/Proxy;

    iget-object v1, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v1}, Lkas;->bxK()Ljava/net/Proxy;

    move-result-object v1

    if-eq v0, v1, :cond_1a

    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->eRZ:Lkcq;

    invoke-direct {p0}, Lkcc;->byx()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkcq;->Ay(Ljava/lang/String;)V

    :cond_1a
    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    if-eqz v0, :cond_1c

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1b
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    iget-object v1, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v1}, Lkas;->getReadTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Lkac;->tF(I)V

    goto :goto_3

    :cond_1c
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0, p0}, Lkac;->a(Lkcc;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkcz;

    iput-object v0, p0, Lkcc;->eRi:Lkcz;

    invoke-virtual {p0}, Lkcc;->byn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    invoke-interface {v0}, Lkcz;->byC()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    goto/16 :goto_0

    .line 207
    :cond_1d
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxQ()Lkad;

    move-result-object v0

    iget-object v1, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0, v1}, Lkad;->a(Lkac;)V

    .line 209
    iput-object v4, p0, Lkcc;->eRf:Lkac;

    goto/16 :goto_0

    :cond_1e
    move-object v3, v4

    goto/16 :goto_2
.end method

.method public final bym()V
    .locals 4

    .prologue
    .line 321
    iget-wide v0, p0, Lkcc;->eRm:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 322
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 324
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lkcc;->eRm:J

    .line 325
    return-void
.end method

.method final byn()Z
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lkcc;->method:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkcc;->method:Ljava/lang/String;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkcc;->method:Ljava/lang/String;

    const-string v1, "PATCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final byo()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lkcc;->eRe:Lkav;

    if-nez v0, :cond_0

    .line 348
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 350
    :cond_0
    iget-object v0, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    return-object v0
.end method

.method public final byp()Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final byq()Lkcs;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lkcc;->eRp:Lkcs;

    return-object v0
.end method

.method public final byr()Lkcu;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 365
    :cond_0
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    return-object v0
.end method

.method public final bys()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    if-nez v0, :cond_0

    .line 377
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 379
    :cond_0
    iget-object v0, p0, Lkcc;->eRk:Ljava/io/InputStream;

    return-object v0
.end method

.method public final byt()Ljava/net/CacheResponse;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lkcc;->eRl:Ljava/net/CacheResponse;

    return-object v0
.end method

.method public final byu()Lkac;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    return-object v0
.end method

.method public final byv()V
    .locals 2

    .prologue
    .line 424
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcc;->eRt:Z

    .line 425
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lkcc;->eRu:Z

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxQ()Lkad;

    move-result-object v0

    iget-object v1, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0, v1}, Lkad;->a(Lkac;)V

    .line 427
    const/4 v0, 0x0

    iput-object v0, p0, Lkcc;->eRf:Lkac;

    .line 429
    :cond_0
    return-void
.end method

.method public final byw()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 479
    iget-object v2, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v2}, Lkcu;->byX()Lkcq;

    move-result-object v2

    invoke-virtual {v2}, Lkcq;->getResponseCode()I

    move-result v2

    .line 482
    iget-object v3, p0, Lkcc;->method:Ljava/lang/String;

    const-string v4, "HEAD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 499
    :cond_0
    :goto_0
    return v0

    .line 486
    :cond_1
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 489
    goto :goto_0

    .line 495
    :cond_3
    iget-object v2, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v2}, Lkcu;->getContentLength()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v2}, Lkcu;->isChunked()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 496
    goto :goto_0
.end method

.method protected byy()Z
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkcc;->eRc:Lkcp;

    invoke-interface {v0}, Lkcp;->usingProxy()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->bxC()Lkaw;

    move-result-object v0

    iget-object v0, v0, Lkaw;->eOL:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Lkac;)V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lkcc;->eRc:Lkcp;

    invoke-virtual {p1}, Lkac;->bxC()Lkaw;

    move-result-object v1

    iget-object v1, v1, Lkaw;->eOL:Ljava/net/Proxy;

    invoke-interface {v0, v1}, Lkcp;->b(Ljava/net/Proxy;)V

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcc;->connected:Z

    .line 314
    return-void
.end method

.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    if-nez v0, :cond_0

    .line 370
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 372
    :cond_0
    iget-object v0, p0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0}, Lkcu;->byX()Lkcq;

    move-result-object v0

    invoke-virtual {v0}, Lkcq;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public final jI(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 438
    iget-object v0, p0, Lkcc;->eRk:Ljava/io/InputStream;

    iget-object v1, p0, Lkcc;->eRs:Ljava/io/InputStream;

    if-ne v0, v1, :cond_0

    .line 439
    iget-object v0, p0, Lkcc;->eRk:Ljava/io/InputStream;

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 442
    :cond_0
    iget-boolean v0, p0, Lkcc;->eRu:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_2

    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcc;->eRu:Z

    .line 445
    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkcc;->eRi:Lkcz;

    iget-object v1, p0, Lkcc;->eRh:Ljava/io/OutputStream;

    iget-object v2, p0, Lkcc;->eRj:Ljava/io/InputStream;

    invoke-interface {v0, p1, v1, v2}, Lkcz;->a(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 447
    :cond_1
    iget-object v0, p0, Lkcc;->eRf:Lkac;

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 448
    iput-object v3, p0, Lkcc;->eRf:Lkac;

    .line 454
    :cond_2
    :goto_0
    return-void

    .line 449
    :cond_3
    iget-boolean v0, p0, Lkcc;->eRt:Z

    if-eqz v0, :cond_2

    .line 450
    iget-object v0, p0, Lkcc;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxQ()Lkad;

    move-result-object v0

    iget-object v1, p0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0, v1}, Lkad;->a(Lkac;)V

    .line 451
    iput-object v3, p0, Lkcc;->eRf:Lkac;

    goto :goto_0
.end method
