.class public final Ljrs;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eBF:[Ljrs;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private eBG:Ljava/lang/String;

.field private eBH:Ljava/lang/String;

.field private eBI:Ljava/lang/String;

.field private eBJ:[B

.field private eBK:Ljava/lang/String;

.field public eBL:Ljrq;

.field private eBM:[Ljoy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1078
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1079
    const/4 v0, 0x0

    iput v0, p0, Ljrs;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljrs;->eBG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrs;->eBH:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrs;->eBI:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljrs;->eBJ:[B

    const-string v0, ""

    iput-object v0, p0, Ljrs;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljrs;->eBK:Ljava/lang/String;

    iput-object v1, p0, Ljrs;->eBL:Ljrq;

    invoke-static {}, Ljoy;->brD()[Ljoy;

    move-result-object v0

    iput-object v0, p0, Ljrs;->eBM:[Ljoy;

    iput-object v1, p0, Ljrs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrs;->eCz:I

    .line 1080
    return-void
.end method

.method public static btD()[Ljrs;
    .locals 2

    .prologue
    .line 927
    sget-object v0, Ljrs;->eBF:[Ljrs;

    if-nez v0, :cond_1

    .line 928
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 930
    :try_start_0
    sget-object v0, Ljrs;->eBF:[Ljrs;

    if-nez v0, :cond_0

    .line 931
    const/4 v0, 0x0

    new-array v0, v0, [Ljrs;

    sput-object v0, Ljrs;->eBF:[Ljrs;

    .line 933
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 935
    :cond_1
    sget-object v0, Ljrs;->eBF:[Ljrs;

    return-object v0

    .line 933
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 921
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrs;->eBG:Ljava/lang/String;

    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrs;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrs;->eBI:Ljava/lang/String;

    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljrs;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljrs;->eBJ:[B

    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljrs;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrs;->afh:Ljava/lang/String;

    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljrs;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrs;->eBK:Ljava/lang/String;

    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljrs;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljrs;->eBL:Ljrq;

    if-nez v0, :cond_1

    new-instance v0, Ljrq;

    invoke-direct {v0}, Ljrq;-><init>()V

    iput-object v0, p0, Ljrs;->eBL:Ljrq;

    :cond_1
    iget-object v0, p0, Ljrs;->eBL:Ljrq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrs;->eBH:Ljava/lang/String;

    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrs;->aez:I

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrs;->eBM:[Ljoy;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljoy;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljrs;->eBM:[Ljoy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljoy;

    invoke-direct {v3}, Ljoy;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljrs;->eBM:[Ljoy;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljoy;

    invoke-direct {v3}, Ljoy;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrs;->eBM:[Ljoy;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 1100
    iget v0, p0, Ljrs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1101
    const/4 v0, 0x1

    iget-object v1, p0, Ljrs;->eBG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1103
    :cond_0
    iget v0, p0, Ljrs;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 1104
    const/4 v0, 0x2

    iget-object v1, p0, Ljrs;->eBI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1106
    :cond_1
    iget v0, p0, Ljrs;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 1107
    const/4 v0, 0x3

    iget-object v1, p0, Ljrs;->eBJ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 1109
    :cond_2
    iget v0, p0, Ljrs;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 1110
    const/4 v0, 0x4

    iget-object v1, p0, Ljrs;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1112
    :cond_3
    iget v0, p0, Ljrs;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 1113
    const/4 v0, 0x5

    iget-object v1, p0, Ljrs;->eBK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1115
    :cond_4
    iget-object v0, p0, Ljrs;->eBL:Ljrq;

    if-eqz v0, :cond_5

    .line 1116
    const/4 v0, 0x6

    iget-object v1, p0, Ljrs;->eBL:Ljrq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1118
    :cond_5
    iget v0, p0, Ljrs;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 1119
    const/4 v0, 0x7

    iget-object v1, p0, Ljrs;->eBH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1121
    :cond_6
    iget-object v0, p0, Ljrs;->eBM:[Ljoy;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljrs;->eBM:[Ljoy;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 1122
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljrs;->eBM:[Ljoy;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 1123
    iget-object v1, p0, Ljrs;->eBM:[Ljoy;

    aget-object v1, v1, v0

    .line 1124
    if-eqz v1, :cond_7

    .line 1125
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 1122
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1129
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1130
    return-void
.end method

.method public final ahK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Ljrs;->eBH:Ljava/lang/String;

    return-object v0
.end method

.method public final ahL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 943
    iget-object v0, p0, Ljrs;->eBG:Ljava/lang/String;

    return-object v0
.end method

.method public final btE()Ljava/lang/String;
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Ljrs;->eBI:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Ljrs;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 1134
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1135
    iget v1, p0, Ljrs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1136
    const/4 v1, 0x1

    iget-object v2, p0, Ljrs;->eBG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1139
    :cond_0
    iget v1, p0, Ljrs;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 1140
    const/4 v1, 0x2

    iget-object v2, p0, Ljrs;->eBI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1143
    :cond_1
    iget v1, p0, Ljrs;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 1144
    const/4 v1, 0x3

    iget-object v2, p0, Ljrs;->eBJ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1147
    :cond_2
    iget v1, p0, Ljrs;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 1148
    const/4 v1, 0x4

    iget-object v2, p0, Ljrs;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1151
    :cond_3
    iget v1, p0, Ljrs;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 1152
    const/4 v1, 0x5

    iget-object v2, p0, Ljrs;->eBK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1155
    :cond_4
    iget-object v1, p0, Ljrs;->eBL:Ljrq;

    if-eqz v1, :cond_5

    .line 1156
    const/4 v1, 0x6

    iget-object v2, p0, Ljrs;->eBL:Ljrq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1159
    :cond_5
    iget v1, p0, Ljrs;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 1160
    const/4 v1, 0x7

    iget-object v2, p0, Ljrs;->eBH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1163
    :cond_6
    iget-object v1, p0, Ljrs;->eBM:[Ljoy;

    if-eqz v1, :cond_9

    iget-object v1, p0, Ljrs;->eBM:[Ljoy;

    array-length v1, v1

    if-lez v1, :cond_9

    .line 1164
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljrs;->eBM:[Ljoy;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 1165
    iget-object v2, p0, Ljrs;->eBM:[Ljoy;

    aget-object v2, v2, v0

    .line 1166
    if-eqz v2, :cond_7

    .line 1167
    const/16 v3, 0x8

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1164
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    move v0, v1

    .line 1172
    :cond_9
    return v0
.end method

.method public final yB(Ljava/lang/String;)Ljrs;
    .locals 1

    .prologue
    .line 946
    if-nez p1, :cond_0

    .line 947
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 949
    :cond_0
    iput-object p1, p0, Ljrs;->eBG:Ljava/lang/String;

    .line 950
    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrs;->aez:I

    .line 951
    return-object p0
.end method

.method public final yC(Ljava/lang/String;)Ljrs;
    .locals 1

    .prologue
    .line 968
    if-nez p1, :cond_0

    .line 969
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 971
    :cond_0
    iput-object p1, p0, Ljrs;->eBH:Ljava/lang/String;

    .line 972
    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrs;->aez:I

    .line 973
    return-object p0
.end method

.method public final yD(Ljava/lang/String;)Ljrs;
    .locals 1

    .prologue
    .line 1034
    if-nez p1, :cond_0

    .line 1035
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1037
    :cond_0
    iput-object p1, p0, Ljrs;->afh:Ljava/lang/String;

    .line 1038
    iget v0, p0, Ljrs;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljrs;->aez:I

    .line 1039
    return-object p0
.end method
