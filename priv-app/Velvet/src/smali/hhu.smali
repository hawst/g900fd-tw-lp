.class public interface abstract Lhhu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final dhB:[Ljava/lang/String;

.field public static final dhC:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "ROUTE_BLUETOOTH_REQUIRED"

    aput-object v1, v0, v2

    const-string v1, "ROUTE_BLUETOOTH_PREFERRED"

    aput-object v1, v0, v3

    const-string v1, "ROUTE_NO_BLUETOOTH"

    aput-object v1, v0, v4

    const-string v1, "ROUTE_NO_AUDIO"

    aput-object v1, v0, v5

    sput-object v0, Lhhu;->dhB:[Ljava/lang/String;

    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CONNECTION_TYPE_ANY"

    aput-object v1, v0, v2

    const-string v1, "CONNECTION_TYPE_NONE"

    aput-object v1, v0, v3

    const-string v1, "CONNECTION_TYPE_BVRA"

    aput-object v1, v0, v4

    const-string v1, "CONNECTION_TYPE_VIRTUAL_VOICE_CALL"

    aput-object v1, v0, v5

    const-string v1, "CONNECTION_TYPE_RAW_SCO"

    aput-object v1, v0, v6

    sput-object v0, Lhhu;->dhC:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract a(IILhhv;Z)V
    .param p3    # Lhhv;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract aPo()V
.end method

.method public abstract aPp()V
.end method

.method public abstract aPq()I
.end method

.method public abstract aPr()I
.end method

.method public abstract aPs()Z
.end method

.method public abstract gr(Z)V
.end method

.method public abstract gs(Z)V
.end method

.method public abstract mB(Ljava/lang/String;)V
.end method

.method public abstract nY(Ljava/lang/String;)Z
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method
