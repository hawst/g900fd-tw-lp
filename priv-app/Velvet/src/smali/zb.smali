.class public final Lzb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public GS:Landroid/view/LayoutInflater;

.field public xZ:Lcom/android/launcher3/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    .line 59
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    const v2, 0x1030128

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lzb;->GS:Landroid/view/LayoutInflater;

    .line 61
    return-void
.end method

.method static synthetic a(Lzb;)Lcom/android/launcher3/Launcher;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    return-object v0
.end method

.method static synthetic a(Lzb;Landroid/view/View;Ljava/lang/Runnable;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 39
    const/16 v0, 0xc8

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    new-instance v1, Lzh;

    invoke-direct {v1, p0, p1, p3, p2}, Lzh;-><init>(Lzb;Landroid/view/View;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method static synthetic b(Lzb;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lzb;->ir()V

    return-void
.end method

.method private iq()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->id()V

    .line 106
    new-instance v0, Lzc;

    invoke-direct {v0, p0}, Lzc;-><init>(Lzb;)V

    .line 118
    iget-object v1, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher3/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method

.method private ir()V
    .locals 2

    .prologue
    .line 180
    new-instance v0, Lzg;

    invoke-direct {v0, p0}, Lzg;-><init>(Lzb;)V

    .line 186
    iget-object v1, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher3/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 187
    return-void
.end method

.method public static p(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 257
    const-string v0, "com.android.launcher3.prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 259
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 260
    const-string v1, "cling_gel.workspace.dismissed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 261
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 262
    return-void
.end method


# virtual methods
.method public final af(Z)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 122
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    const v1, 0x7f11023b

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/ViewGroup;

    .line 123
    iget-object v0, p0, Lzb;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f0400be

    invoke-virtual {v0, v1, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 125
    new-instance v0, Lze;

    invoke-direct {v0, p0}, Lze;-><init>(Lzb;)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 135
    const v0, 0x7f110255

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewGroup;

    .line 136
    iget-object v1, p0, Lzb;->GS:Landroid/view/LayoutInflater;

    if-eqz p1, :cond_1

    const v0, 0x7f0400c0

    :goto_0
    invoke-virtual {v1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 138
    const v0, 0x7f110256

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    const-string v0, "crop_bg_top_and_sides"

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    new-instance v0, Lsq;

    iget-object v1, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f020034

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lsq;-><init>(Landroid/graphics/drawable/Drawable;ZZZZ)V

    .line 143
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 146
    :cond_0
    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 148
    if-eqz p1, :cond_2

    .line 177
    :goto_1
    return-void

    .line 136
    :cond_1
    const v0, 0x7f0400bf

    goto :goto_0

    .line 154
    :cond_2
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lzf;

    invoke-direct {v1, p0, v7}, Lzf;-><init>(Lzb;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method

.method public final is()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 250
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ht()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 251
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    const-string v0, "cling_gel.workspace.dismissed"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "cling_gel.migration.dismissed"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    :goto_1
    return v1

    :cond_0
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    const-string v4, "accessibility"

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v0, v4, :cond_2

    move v0, v1

    :goto_2
    iget-object v4, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v4

    if-eqz v0, :cond_3

    array-length v0, v4

    if-nez v0, :cond_3

    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    const-string v4, "user"

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "no_modify_accounts"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "skip_first_use_hints"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 66
    const v1, 0x7f110270

    if-ne v0, v1, :cond_1

    .line 68
    invoke-direct {p0}, Lzb;->iq()V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const v1, 0x7f11026f

    if-ne v0, v1, :cond_2

    .line 71
    iget-object v0, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hs()Lzi;

    move-result-object v0

    .line 72
    invoke-virtual {v0, v3, v4}, Lzi;->b(ZZ)V

    .line 73
    const/16 v1, -0x3e9

    const/4 v2, 0x3

    invoke-virtual {v0, v3, v1, v2}, Lzi;->a(ZII)V

    .line 77
    const-string v0, "com.android.launcher3.prefs"

    .line 78
    iget-object v1, p0, Lzb;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, v0, v3}, Lcom/android/launcher3/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 79
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 80
    const-string v1, "launcher.user_migrated_from_old_data"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 81
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 83
    invoke-direct {p0}, Lzb;->iq()V

    goto :goto_0

    .line 84
    :cond_2
    const v1, 0x7f110256

    if-ne v0, v1, :cond_0

    .line 85
    invoke-direct {p0}, Lzb;->ir()V

    goto :goto_0
.end method
