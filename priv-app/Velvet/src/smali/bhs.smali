.class final Lbhs;
.super Ljava/lang/Object;

# interfaces
.implements Lbhi;


# instance fields
.field private aBA:Z

.field private aBB:I

.field aBC:J

.field final aBD:Landroid/os/Handler;

.field final aBE:Landroid/os/Bundle;

.field private final aBF:Ljava/util/Map;

.field aBG:Z

.field private final aBH:Ljava/util/Set;

.field final aBI:Ljava/util/Set;

.field private final aBJ:Lbhk;

.field private final aBK:Lbjb;

.field private final aBf:Lbhx;

.field private final aBk:Landroid/os/Looper;

.field final aBs:Ljava/util/concurrent/locks/Lock;

.field private final aBt:Ljava/util/concurrent/locks/Condition;

.field private final aBu:Lbiz;

.field private aBv:Ljava/util/Queue;

.field aBw:Lbgm;

.field aBx:I

.field volatile aBy:I

.field volatile aBz:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V
    .locals 10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lbhs;->aBt:Ljava/util/concurrent/locks/Condition;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    const/4 v0, 0x4

    iput v0, p0, Lbhs;->aBy:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbhs;->aBA:Z

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lbhs;->aBC:J

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbhs;->aBE:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbhs;->aBF:Ljava/util/Map;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbhs;->aBH:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbhs;->aBI:Ljava/util/Set;

    new-instance v0, Lbht;

    invoke-direct {v0, p0}, Lbht;-><init>(Lbhs;)V

    iput-object v0, p0, Lbhs;->aBf:Lbhx;

    new-instance v0, Lbhu;

    invoke-direct {v0, p0}, Lbhu;-><init>(Lbhs;)V

    iput-object v0, p0, Lbhs;->aBJ:Lbhk;

    new-instance v0, Lbhv;

    invoke-direct {v0, p0}, Lbhv;-><init>(Lbhs;)V

    iput-object v0, p0, Lbhs;->aBK:Lbjb;

    new-instance v0, Lbiz;

    iget-object v1, p0, Lbhs;->aBK:Lbjb;

    invoke-direct {v0, p2, v1}, Lbiz;-><init>(Landroid/os/Looper;Lbjb;)V

    iput-object v0, p0, Lbhs;->aBu:Lbiz;

    iput-object p2, p0, Lbhs;->aBk:Landroid/os/Looper;

    new-instance v0, Lbhy;

    invoke-direct {v0, p0, p2}, Lbhy;-><init>(Lbhs;Landroid/os/Looper;)V

    iput-object v0, p0, Lbhs;->aBD:Landroid/os/Handler;

    invoke-interface {p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhk;

    iget-object v2, p0, Lbhs;->aBu:Lbiz;

    invoke-virtual {v2, v0}, Lbiz;->a(Lbhk;)V

    goto :goto_0

    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhl;

    iget-object v2, p0, Lbhs;->aBu:Lbiz;

    invoke-virtual {v2, v0}, Lbiz;->a(Lbgq;)V

    goto :goto_1

    :cond_1
    invoke-interface {p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbgx;

    iget-object v0, v1, Lbgx;->aAS:Lbhb;

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    iget-object v8, p0, Lbhs;->aBF:Ljava/util/Map;

    iget-object v9, v1, Lbgx;->aAT:Lbhc;

    iget-object v5, p0, Lbhs;->aBJ:Lbhk;

    new-instance v6, Lbhw;

    invoke-direct {v6, p0, v0}, Lbhw;-><init>(Lbhs;Lbhb;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v6}, Lbhb;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lbhk;Lbhl;)Lbha;

    move-result-object v0

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    iget-object v0, p3, Lcom/google/android/gms/common/internal/ClientSettings;->aCM:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->yP()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lbhs;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lbhs;->aBB:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbhs;->aBB:I

    iget v0, p0, Lbhs;->aBB:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lbhs;->aBw:Lbgm;

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lbhs;->aBA:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbhs;->dG(I)V

    invoke-virtual {p0}, Lbhs;->yB()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbhs;->aBD:Landroid/os/Handler;

    iget-object v1, p0, Lbhs;->aBD:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lbhs;->aBC:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    iput-boolean v4, p0, Lbhs;->aBG:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lbhs;->aBu:Lbiz;

    iget-object v1, p0, Lbhs;->aBw:Lbgm;

    invoke-virtual {v0, v1}, Lbiz;->c(Lbgm;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lbhs;->aBy:I

    invoke-direct {p0}, Lbhs;->yC()V

    iget-object v0, p0, Lbhs;->aBt:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lbhs;->yA()V

    iget-boolean v0, p0, Lbhs;->aBA:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lbhs;->aBA:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbhs;->dG(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lbhs;->aBE:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lbhs;->aBu:Lbiz;

    invoke-virtual {v1, v0}, Lbiz;->m(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lbhs;->aBE:Landroid/os/Bundle;

    goto :goto_2
.end method

.method private a(Lbhz;)V
    .locals 2

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-interface {p1}, Lbhz;->yv()Lbhc;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lbjr;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbhs;->aBI:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lbhs;->aBf:Lbhx;

    invoke-interface {p1, v0}, Lbhz;->a(Lbhx;)V

    invoke-virtual {p0}, Lbhs;->yB()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lbhz;->c(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {p1}, Lbhz;->yv()Lbhc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbhs;->a(Lbhc;)Lbha;

    move-result-object v0

    invoke-interface {p1, v0}, Lbhz;->b(Lbha;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private yA()V
    .locals 3

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lbhs;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbhs;->yB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lbjr;->a(ZLjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhz;

    invoke-direct {p0, v0}, Lbhs;->a(Lbhz;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method private yC()V
    .locals 2

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lbhs;->aBz:I

    iget-object v0, p0, Lbhs;->aBD:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(Lbhc;)Lbha;
    .locals 2

    iget-object v0, p0, Lbhs;->aBF:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbha;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final a(Lbhg;)Lbhg;
    .locals 2

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    new-instance v0, Lbhf;

    iget-object v1, p0, Lbhs;->aBk:Landroid/os/Looper;

    invoke-direct {v0, v1}, Lbhf;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p1, v0}, Lbhg;->a(Lbhf;)V

    invoke-virtual {p0}, Lbhs;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbhs;->b(Lbhg;)Lbhg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lbhk;)V
    .locals 1

    iget-object v0, p0, Lbhs;->aBu:Lbiz;

    invoke-virtual {v0, p1}, Lbiz;->a(Lbhk;)V

    return-void
.end method

.method public final a(Lbhl;)V
    .locals 1

    iget-object v0, p0, Lbhs;->aBu:Lbiz;

    invoke-virtual {v0, p1}, Lbiz;->a(Lbgq;)V

    return-void
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)Lbgm;
    .locals 5

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v1}, Lbjr;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lbhs;->connect()V

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    :cond_0
    invoke-virtual {p0}, Lbhs;->isConnecting()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    :try_start_1
    iget-object v2, p0, Lbhs;->aBt:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    new-instance v0, Lbgm;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Lbgm;

    const/16 v1, 0xf

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lbhs;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lbgm;->aAI:Lbgm;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :cond_3
    :try_start_4
    iget-object v0, p0, Lbhs;->aBw:Lbgm;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbhs;->aBw:Lbgm;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :cond_4
    :try_start_5
    new-instance v0, Lbgm;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbgm;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b(Lbhg;)Lbhg;
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbhs;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbhs;->yB()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Lbjr;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lbhs;->yA()V

    :try_start_0
    invoke-direct {p0, p1}, Lbhs;->a(Lbhz;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v1}, Lbhs;->dG(I)V

    goto :goto_1
.end method

.method public final b(Lbhk;)V
    .locals 4

    iget-object v0, p0, Lbhs;->aBu:Lbiz;

    invoke-static {p1}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lbiz;->aDe:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lbiz;->aDe:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lbiz;->aDe:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionCallbacks(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-boolean v2, v0, Lbiz;->aDg:Z

    if-eqz v2, :cond_0

    iget-object v0, v0, Lbiz;->aDf:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lbhl;)V
    .locals 4

    iget-object v0, p0, Lbhs;->aBu:Lbiz;

    invoke-static {p1}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lbiz;->aDh:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lbiz;->aDh:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lbiz;->aDh:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionFailedListener(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final connect()V
    .locals 2

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lbhs;->aBA:Z

    invoke-virtual {p0}, Lbhs;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbhs;->isConnecting()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lbhs;->aBG:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lbhs;->aBw:Lbgm;

    const/4 v0, 0x1

    iput v0, p0, Lbhs;->aBy:I

    iget-object v0, p0, Lbhs;->aBE:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lbhs;->aBF:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lbhs;->aBB:I

    iget-object v0, p0, Lbhs;->aBF:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbha;

    invoke-interface {v0}, Lbha;->connect()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method dG(I)V
    .locals 5

    const/4 v1, 0x3

    const/4 v4, -0x1

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lbhs;->aBy:I

    if-eq v0, v1, :cond_a

    if-ne p1, v4, :cond_4

    invoke-virtual {p0}, Lbhs;->isConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhz;

    invoke-interface {v0}, Lbhz;->cancel()V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_1
    iget-object v0, p0, Lbhs;->aBI:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhz;

    invoke-interface {v0}, Lbhz;->cancel()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbhs;->aBI:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lbhs;->aBH:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbia;

    const/4 v2, 0x0

    iput-object v2, v0, Lbia;->aBN:Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lbhs;->aBH:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lbhs;->aBw:Lbgm;

    if-nez v0, :cond_4

    iget-object v0, p0, Lbhs;->aBv:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhs;->aBA:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_3
    return-void

    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lbhs;->isConnecting()Z

    move-result v0

    invoke-virtual {p0}, Lbhs;->isConnected()Z

    move-result v1

    const/4 v2, 0x3

    iput v2, p0, Lbhs;->aBy:I

    if-eqz v0, :cond_6

    if-ne p1, v4, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, Lbhs;->aBw:Lbgm;

    :cond_5
    iget-object v0, p0, Lbhs;->aBt:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbhs;->aBG:Z

    iget-object v0, p0, Lbhs;->aBF:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbha;

    invoke-interface {v0}, Lbha;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Lbha;->disconnect()V

    goto :goto_4

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhs;->aBG:Z

    const/4 v0, 0x4

    iput v0, p0, Lbhs;->aBy:I

    if-eqz v1, :cond_a

    if-eq p1, v4, :cond_9

    iget-object v0, p0, Lbhs;->aBu:Lbiz;

    invoke-virtual {v0, p1}, Lbiz;->dM(I)V

    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbhs;->aBG:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_a
    iget-object v0, p0, Lbhs;->aBs:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public final disconnect()V
    .locals 1

    invoke-direct {p0}, Lbhs;->yC()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbhs;->dG(I)V

    return-void
.end method

.method public final isConnected()Z
    .locals 2

    iget v0, p0, Lbhs;->aBy:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isConnecting()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lbhs;->aBy:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method yB()Z
    .locals 1

    iget v0, p0, Lbhs;->aBz:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
