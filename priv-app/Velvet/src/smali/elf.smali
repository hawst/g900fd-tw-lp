.class public final Lelf;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final GS:Landroid/view/LayoutInflater;

.field private final cex:I

.field private final cey:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    iput-object p2, p0, Lelf;->cey:Ljava/util/List;

    .line 28
    iget-object v0, p0, Lelf;->cey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lelf;->cex:I

    .line 29
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lelf;->GS:Landroid/view/LayoutInflater;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 33
    array-length v0, p2

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lelf;->cey:Ljava/util/List;

    .line 34
    array-length v2, p2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p2, v0

    .line 35
    iget-object v4, p0, Lelf;->cey:Ljava/util/List;

    new-instance v5, Lell;

    invoke-direct {v5, v3, v6, v6, v1}, Lell;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lelf;->cey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lelf;->cex:I

    .line 38
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lelf;->GS:Landroid/view/LayoutInflater;

    .line 39
    return-void
.end method


# virtual methods
.method public final aB(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    iget v0, p0, Lelf;->cex:I

    invoke-virtual {p0, v0}, Lelf;->if(I)Lell;

    move-result-object v3

    .line 105
    iget-object v0, v3, Lell;->ceK:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-object p1, v3, Lell;->ceK:Ljava/lang/String;

    .line 106
    iget-object v4, v3, Lell;->ceL:Ljava/lang/String;

    invoke-static {v4, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    :goto_1
    iput-object p2, v3, Lell;->ceL:Ljava/lang/String;

    or-int/2addr v0, v1

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lelf;->notifyDataSetChanged()V

    .line 111
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 105
    goto :goto_0

    :cond_2
    move v1, v2

    .line 106
    goto :goto_1
.end method

.method public final aul()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lelf;->cex:I

    return v0
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    invoke-virtual {p0, p1}, Lelf;->if(I)Lell;

    move-result-object v3

    .line 123
    iget-object v0, v3, Lelh;->bLI:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-object p2, v3, Lelh;->bLI:Ljava/lang/String;

    .line 124
    iget-object v4, v3, Lell;->ceJ:Ljava/lang/String;

    invoke-static {v4, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    :goto_1
    iput-object p3, v3, Lell;->ceJ:Ljava/lang/String;

    or-int/2addr v0, v1

    .line 125
    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lelf;->notifyDataSetChanged()V

    .line 128
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 123
    goto :goto_0

    :cond_2
    move v1, v2

    .line 124
    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lelf;->cey:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 82
    if-nez p2, :cond_0

    .line 83
    iget-object v0, p0, Lelf;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f0400bc

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 86
    :cond_0
    invoke-virtual {p0, p1}, Lelf;->if(I)Lell;

    move-result-object v2

    .line 87
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 88
    const v1, 0x1020015

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 90
    iget-object v3, v2, Lelh;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, v2, Lell;->ceJ:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, v2, Lell;->ceJ:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    :goto_0
    return-object p2

    .line 96
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lelf;->if(I)Lell;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 53
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 58
    if-nez p2, :cond_0

    .line 59
    iget-object v0, p0, Lelf;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f0400bd

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 62
    :cond_0
    invoke-virtual {p0, p1}, Lelf;->if(I)Lell;

    move-result-object v3

    .line 63
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    const v1, 0x1020015

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    iget-object v2, v3, Lell;->ceK:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v3, Lell;->ceK:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v2, v3, Lell;->ceL:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v3, Lell;->ceL:Ljava/lang/String;

    .line 69
    :goto_1
    if-eqz v2, :cond_3

    .line 70
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 77
    :goto_2
    return-object p2

    .line 66
    :cond_1
    iget-object v2, v3, Lelh;->bLI:Ljava/lang/String;

    goto :goto_0

    .line 68
    :cond_2
    iget-object v2, v3, Lell;->ceJ:Ljava/lang/String;

    goto :goto_1

    .line 74
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_2
.end method

.method public final if(I)Lell;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lelf;->cey:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lell;

    return-object v0
.end method
