.class public final Ljuh;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eFb:I

.field private eFc:Z

.field private eFd:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 268
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 269
    iput v1, p0, Ljuh;->aez:I

    const/16 v0, 0x32

    iput v0, p0, Ljuh;->eFb:I

    iput-boolean v1, p0, Ljuh;->eFc:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljuh;->eFd:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljuh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuh;->eCz:I

    .line 270
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 192
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljuh;->eFb:I

    iget v0, p0, Ljuh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljuh;->eFc:Z

    iget v0, p0, Ljuh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuh;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljuh;->eFd:Z

    iget v0, p0, Ljuh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljuh;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 285
    iget v0, p0, Ljuh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 286
    const/4 v0, 0x1

    iget v1, p0, Ljuh;->eFb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 288
    :cond_0
    iget v0, p0, Ljuh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 289
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljuh;->eFc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 291
    :cond_1
    iget v0, p0, Ljuh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 292
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljuh;->eFd:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 294
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 295
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 299
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 300
    iget v1, p0, Ljuh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 301
    const/4 v1, 0x1

    iget v2, p0, Ljuh;->eFb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_0
    iget v1, p0, Ljuh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 305
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljuh;->eFc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 308
    :cond_1
    iget v1, p0, Ljuh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 309
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljuh;->eFd:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 312
    :cond_2
    return v0
.end method
