.class public final Ljcr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private dHR:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18845
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 18846
    const/4 v0, 0x0

    iput v0, p0, Ljcr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcr;->ajB:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljcr;->dHR:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljcr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcr;->eCz:I

    .line 18847
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 18780
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcr;->ajB:Ljava/lang/String;

    iget v0, p0, Ljcr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljcr;->dHR:I

    iget v0, p0, Ljcr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcr;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 18861
    iget v0, p0, Ljcr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 18862
    const/4 v0, 0x1

    iget-object v1, p0, Ljcr;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 18864
    :cond_0
    iget v0, p0, Ljcr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 18865
    const/4 v0, 0x2

    iget v1, p0, Ljcr;->dHR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 18867
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 18868
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 18872
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 18873
    iget v1, p0, Ljcr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 18874
    const/4 v1, 0x1

    iget-object v2, p0, Ljcr;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18877
    :cond_0
    iget v1, p0, Ljcr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 18878
    const/4 v1, 0x2

    iget v2, p0, Ljcr;->dHR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 18881
    :cond_1
    return v0
.end method

.method public final oK(I)Ljcr;
    .locals 1

    .prologue
    .line 18832
    iput p1, p0, Ljcr;->dHR:I

    .line 18833
    iget v0, p0, Ljcr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcr;->aez:I

    .line 18834
    return-object p0
.end method

.method public final to(Ljava/lang/String;)Ljcr;
    .locals 1

    .prologue
    .line 18810
    if-nez p1, :cond_0

    .line 18811
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18813
    :cond_0
    iput-object p1, p0, Ljcr;->ajB:Ljava/lang/String;

    .line 18814
    iget v0, p0, Ljcr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcr;->aez:I

    .line 18815
    return-object p0
.end method
