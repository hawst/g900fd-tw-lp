.class public final Lfez;
.super Lfbs;
.source "PG"


# instance fields
.field private final mCalendarDataProvider:Leym;

.field private final mClock:Lemp;

.field private final mDirectionsLauncher:Lgah;

.field private final mLocationOracle:Lfdr;

.field private final mReminderSmartActionUtil:Leqm;


# direct methods
.method public constructor <init>(Lfdr;Leym;Lgah;Lemp;Lfbp;Leqm;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p5}, Lfbs;-><init>(Lfbp;)V

    .line 44
    iput-object p1, p0, Lfez;->mLocationOracle:Lfdr;

    .line 45
    iput-object p2, p0, Lfez;->mCalendarDataProvider:Leym;

    .line 46
    iput-object p3, p0, Lfez;->mDirectionsLauncher:Lgah;

    .line 47
    iput-object p4, p0, Lfez;->mClock:Lemp;

    .line 48
    iput-object p6, p0, Lfez;->mReminderSmartActionUtil:Leqm;

    .line 49
    return-void
.end method

.method private static a(Ljge;)Z
    .locals 1
    .param p0    # Ljge;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 186
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljge;->bjT()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljge;->bjS()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private azd()Ljbp;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lfez;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v0

    return-object v0
.end method

.method private static f(Lizj;)Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->oP()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a(ILizj;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 30
    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Lffa;

    invoke-direct {v0, p2}, Lffa;-><init>(Lizj;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p2, Lizj;->dSf:Lixr;

    iget-object v2, p0, Lfez;->mCalendarDataProvider:Leym;

    invoke-virtual {v1}, Lixr;->baD()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Leym;->lE(Ljava/lang/String;)Lamk;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v1, v2}, Lfzy;->a(Lixr;Lamk;)Ljbp;

    move-result-object v4

    new-instance v0, Lfev;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v3

    iget-object v5, p0, Lfez;->mDirectionsLauncher:Lgah;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfev;-><init>(Lizj;Lamk;Ljbp;Ljbp;Lgah;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lfew;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v1

    iget-object v2, p2, Lizj;->dTk:Lixs;

    invoke-static {v2}, Lgaa;->a(Lixs;)Ljbp;

    move-result-object v2

    iget-object v3, p0, Lfez;->mDirectionsLauncher:Lgah;

    invoke-direct {v0, p2, v1, v2, v3}, Lfew;-><init>(Lizj;Ljbp;Ljbp;Lgah;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lffg;

    invoke-direct {v0, p2}, Lffg;-><init>(Lizj;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p2, Lizj;->dUr:Ljcg;

    invoke-virtual {v1}, Ljcg;->oP()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    new-instance v0, Lffc;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v1

    iget-object v2, p0, Lfez;->mDirectionsLauncher:Lgah;

    iget-object v3, p0, Lfez;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2, v3}, Lffc;-><init>(Lizj;Ljbp;Lgah;Lemp;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lffb;

    iget-object v1, p2, Lizj;->dSi:Ljae;

    iget-object v2, p0, Lfez;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lgbi;->a(Ljae;J)Lgbi;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lffb;-><init>(Lizj;Lgbi;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lffd;

    invoke-direct {v0, p2}, Lffd;-><init>(Lizj;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lfff;

    iget-object v1, p0, Lfez;->mClock:Lemp;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v2

    iget-object v3, p0, Lfez;->mDirectionsLauncher:Lgah;

    invoke-direct {v0, p2, v1, v2, v3}, Lfff;-><init>(Lizj;Lemp;Ljbp;Lgah;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lffh;

    invoke-direct {v0, p2}, Lffh;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lfgh;

    invoke-direct {v0, p2}, Lfgh;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lfgi;

    iget-object v1, p0, Lfez;->mClock:Lemp;

    iget-object v2, p0, Lfez;->mReminderSmartActionUtil:Leqm;

    invoke-direct {v0, p2, v1, v2}, Lfgi;-><init>(Lizj;Lemp;Leqm;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lffg;

    invoke-direct {v0, p2}, Lffg;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lfgl;

    invoke-direct {v0, p2}, Lfgl;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lfgn;

    invoke-direct {v0, p2}, Lfgn;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lfgp;

    invoke-direct {v0, p2}, Lfgp;-><init>(Lizj;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_d
        0xb -> :sswitch_5
        0xc -> :sswitch_4
        0xd -> :sswitch_b
        0xe -> :sswitch_1
        0x13 -> :sswitch_8
        0x22 -> :sswitch_0
        0x2a -> :sswitch_7
        0x2b -> :sswitch_9
        0x38 -> :sswitch_6
        0x40 -> :sswitch_2
        0x42 -> :sswitch_3
        0x48 -> :sswitch_a
        0x5e -> :sswitch_c
    .end sparse-switch
.end method

.method protected final synthetic a(ILizq;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 30
    sparse-switch p1, :sswitch_data_0

    move-object v0, v1

    :cond_0
    :goto_0
    move-object v1, v0

    :cond_1
    return-object v1

    :sswitch_0
    iget-object v2, p2, Lizq;->dUX:[Lizj;

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v5, v4, Lizj;->dUr:Ljcg;

    invoke-virtual {v4}, Lizj;->getType()I

    move-result v6

    const/16 v7, 0xc

    if-ne v6, v7, :cond_3

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljcg;->oP()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    new-instance v0, Lffc;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v1

    iget-object v2, p0, Lfez;->mDirectionsLauncher:Lgah;

    iget-object v3, p0, Lfez;->mClock:Lemp;

    invoke-direct {v0, v4, v1, v2, v3}, Lffc;-><init>(Lizj;Ljbp;Lgah;Lemp;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lffb;

    iget-object v1, v4, Lizj;->dSi:Ljae;

    iget-object v2, p0, Lfez;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lgbi;->a(Ljae;J)Lgbi;

    move-result-object v1

    invoke-direct {v0, v4, v1}, Lffb;-><init>(Lizj;Lgbi;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :sswitch_1
    iget-object v2, p2, Lizq;->dUX:[Lizj;

    array-length v3, v2

    :goto_2
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v5, v4, Lizj;->dUr:Ljcg;

    if-eqz v5, :cond_5

    iget-object v5, v4, Lizj;->dUr:Ljcg;

    invoke-virtual {v5}, Ljcg;->oP()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, v4, Lizj;->dSm:Ljgg;

    if-eqz v5, :cond_5

    iget-object v5, v4, Lizj;->dSm:Ljgg;

    iget-object v5, v5, Ljgg;->ejy:Ljgh;

    if-eqz v5, :cond_5

    iget-object v5, v4, Lizj;->dSm:Ljgg;

    iget-object v5, v5, Ljgg;->ejy:Ljgh;

    iget-object v6, v5, Ljgh;->ejD:Ljge;

    invoke-static {v6}, Lfez;->a(Ljge;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v5, v5, Ljgh;->ejE:Ljge;

    invoke-static {v5}, Lfez;->a(Ljge;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    new-instance v0, Lfgl;

    invoke-direct {v0, v4}, Lfgl;-><init>(Lizj;)V

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :sswitch_2
    iget-object v3, p2, Lizq;->dUX:[Lizj;

    array-length v4, v3

    move v2, v0

    :goto_3
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    invoke-virtual {p0, v0}, Lfez;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    if-nez v0, :cond_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x71 -> :sswitch_1
        0x76 -> :sswitch_2
        0x7c -> :sswitch_0
    .end sparse-switch
.end method

.method protected final synthetic a(Lizj;Ljal;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 30
    packed-switch p3, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lfgo;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v1

    iget-object v2, p0, Lfez;->mDirectionsLauncher:Lgah;

    iget-object v3, p0, Lfez;->mClock:Lemp;

    invoke-direct {v0, p1, v1, v2, v3}, Lfgo;-><init>(Lizj;Ljbp;Lgah;Lemp;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lfgm;

    invoke-direct {p0}, Lfez;->azd()Ljbp;

    move-result-object v1

    iget-object v2, p0, Lfez;->mDirectionsLauncher:Lgah;

    iget-object v3, p0, Lfez;->mClock:Lemp;

    invoke-direct {v0, p1, v1, v2, v3}, Lfgm;-><init>(Lizj;Ljbp;Lgah;Lemp;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected final c(Lizj;Ljal;I)Z
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Lfbs;->c(Lizj;Ljal;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lfez;->f(Lizj;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e(Lizj;)Z
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1}, Lfbs;->e(Lizj;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lfez;->f(Lizj;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic g(Lizj;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 30
    invoke-static {p1}, Lfez;->f(Lizj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    new-instance v0, Lffe;

    const v1, 0x7f030003

    invoke-direct {v0, p1, v1}, Lffe;-><init>(Lizj;I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
