.class public final Lcvf;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final bkS:Lcvl;


# instance fields
.field public bkT:Ljava/util/Map;

.field public bkU:Ljava/util/Set;

.field private bkV:I

.field private final mContext:Landroid/content/Context;

.field private final mCoreSearchServices:Lcfo;

.field private final mVelvetServices:Lgql;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcvl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcvl;-><init>(B)V

    sput-object v0, Lcvf;->bkS:Lcvl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lgql;)V
    .locals 5

    .prologue
    .line 78
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcvf;->bkV:I

    .line 79
    iput-object p1, p0, Lcvf;->mContext:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcvf;->mVelvetServices:Lgql;

    .line 81
    invoke-virtual {p2}, Lgql;->SC()Lcfo;

    move-result-object v0

    iput-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->Fs()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcvf;->bkT:Ljava/util/Map;

    .line 85
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0}, Lcke;->Oq()Ljava/util/Set;

    move-result-object v0

    .line 87
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcvf;->bkU:Ljava/util/Set;

    .line 88
    if-eqz v0, :cond_0

    .line 89
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91
    :try_start_0
    iget-object v2, p0, Lcvf;->bkU:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 94
    :catch_0
    move-exception v2

    const-string v2, "DebugOverrideSettingsController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Throwing away non-int flag override: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method private Th()V
    .locals 4

    .prologue
    .line 359
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lcvk;

    const-string v2, "updateGsaConfigAndNotifyListeners"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lcvk;-><init>(Lcvf;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 369
    return-void
.end method

.method static synthetic a(Lcvf;)Lcfo;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    return-object v0
.end method

.method private a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V
    .locals 3

    .prologue
    .line 111
    invoke-virtual {p2}, Ljjg;->getId()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "id:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    if-eqz p4, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "*"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_0
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljjg;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setPersistent(Z)V

    .line 116
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 117
    check-cast p3, Landroid/preference/PreferenceGroup;

    invoke-virtual {p3, p1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 118
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 122
    sget-object v0, Lcvf;->bkS:Lcvl;

    iget-object v1, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Liki;->a(Ljava/util/Comparator;Ljava/util/Collection;)Liki;

    move-result-object v0

    invoke-virtual {v0}, Liki;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 124
    iget-object v1, p0, Lcvf;->bkU:Ljava/util/Set;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "id:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 125
    iget-object v1, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljjg;

    .line 126
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lchh;->eH(I)I

    move-result v0

    .line 127
    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcvg;

    iget-object v4, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v4}, Lcvg;-><init>(Lcvf;Landroid/content/Context;)V

    .line 135
    invoke-virtual {v1}, Ljjg;->TO()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 136
    invoke-direct {p0, v0, v1, p1, v3}, Lcvf;->a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V

    goto :goto_0

    .line 138
    :cond_0
    new-instance v4, Lcvh;

    iget-object v5, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcvh;-><init>(Lcvf;Landroid/content/Context;)V

    .line 145
    if-ne v0, v6, :cond_1

    .line 146
    invoke-virtual {v1}, Ljjg;->getIntValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 147
    invoke-virtual {v4}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 149
    invoke-direct {p0, v4, v1, p1, v3}, Lcvf;->a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V

    goto :goto_0

    .line 151
    :cond_1
    invoke-virtual {v1}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 152
    invoke-direct {p0, v4, v1, p1, v3}, Lcvf;->a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V

    goto :goto_0

    .line 157
    :cond_2
    iget-object v0, p0, Lcvf;->bkU:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 158
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lchh;->eH(I)I

    move-result v1

    .line 159
    iget-object v3, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 161
    if-nez v1, :cond_4

    .line 164
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lchh;->eE(I)Z

    move-result v1

    .line 165
    new-instance v3, Lcvi;

    iget-object v4, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcvi;-><init>(Lcvf;Landroid/content/Context;)V

    .line 172
    invoke-virtual {v3, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 173
    new-instance v4, Ljjg;

    invoke-direct {v4}, Ljjg;-><init>()V

    .line 174
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Ljjg;->qo(I)Ljjg;

    .line 175
    invoke-virtual {v4, v1}, Ljjg;->in(Z)Ljjg;

    .line 176
    iget-object v0, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-virtual {v4}, Ljjg;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    invoke-direct {p0, v3, v4, p1, v6}, Lcvf;->a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V

    goto :goto_1

    .line 179
    :cond_4
    new-instance v3, Lcvj;

    iget-object v4, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcvj;-><init>(Lcvf;Landroid/content/Context;)V

    .line 186
    if-ne v1, v6, :cond_5

    .line 187
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lchh;->getInt(I)I

    move-result v1

    .line 188
    new-instance v4, Ljjg;

    invoke-direct {v4}, Ljjg;-><init>()V

    .line 189
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Ljjg;->qo(I)Ljjg;

    .line 190
    invoke-virtual {v4, v1}, Ljjg;->qn(I)Ljjg;

    .line 191
    iget-object v0, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-virtual {v4}, Ljjg;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-virtual {v4}, Ljjg;->getIntValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 193
    invoke-virtual {v3}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 194
    invoke-direct {p0, v3, v4, p1, v6}, Lcvf;->a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V

    goto/16 :goto_1

    .line 197
    :cond_5
    const/4 v4, 0x3

    if-ne v1, v4, :cond_6

    .line 198
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lchh;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    :goto_2
    new-instance v4, Ljjg;

    invoke-direct {v4}, Ljjg;-><init>()V

    .line 205
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Ljjg;->qo(I)Ljjg;

    .line 206
    invoke-virtual {v4, v1}, Ljjg;->wh(Ljava/lang/String;)Ljjg;

    .line 207
    iget-object v0, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-virtual {v4}, Ljjg;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    invoke-virtual {v4}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 209
    invoke-direct {p0, v3, v4, p1, v6}, Lcvf;->a(Landroid/preference/Preference;Ljjg;Landroid/preference/Preference;Z)V

    goto/16 :goto_1

    .line 202
    :cond_6
    const-string v1, ""

    goto :goto_2

    .line 217
    :cond_7
    new-instance v1, Landroid/preference/EditTextPreference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 218
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setInputType(I)V

    .line 219
    const v0, 0x7f0a0694

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setTitle(I)V

    .line 220
    const-string v0, ""

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 221
    const-string v0, "add_custom_flag_key"

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setKey(Ljava/lang/String;)V

    .line 222
    invoke-virtual {v1, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    move-object v0, p1

    .line 223
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 225
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 226
    const v0, 0x7f0a058f

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 227
    const v0, 0x7f0a0590

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 228
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 229
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 232
    const-string v0, ","

    iget-object v1, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->Ft()[I

    move-result-object v1

    invoke-static {v0, v1}, Lius;->e(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v0

    .line 234
    new-instance v1, Landroid/preference/EditTextPreference;

    iget-object v2, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 235
    const v2, 0x7f0a059c

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setTitle(I)V

    .line 236
    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 237
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setCursorVisible(Z)V

    move-object v0, p1

    .line 238
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 242
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 243
    const v0, 0x7f0a0599

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 244
    const-string v0, "clear_local_overrides_key"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 245
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 246
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 249
    new-instance v1, Landroid/preference/EditTextPreference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 250
    const-string v0, "agsad"

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v0

    invoke-virtual {v0}, Lcpn;->RG()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 253
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setCursorVisible(Z)V

    move-object v0, p1

    .line 254
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 258
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 259
    const v0, 0x7f0a0598

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 260
    const-string v0, "gsa_home"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 261
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 262
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 265
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 266
    const v0, 0x7f0a0595

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 267
    const-string v0, "log_contacts_to_clearcut"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 268
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 269
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 272
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 273
    const v0, 0x7f0a0596

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 274
    const-string v0, "log_contact_accounts_to_clearcut"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 276
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 279
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 280
    const v0, 0x7f0a0597

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 281
    const-string v0, "remove_person_shortcuts"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 282
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 283
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 285
    new-instance v0, Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcvf;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;)V

    .line 286
    const v1, 0x7f0a059a

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setTitle(I)V

    .line 287
    const v1, 0x7f0a059b

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 289
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 290
    const-string v1, "remove_experiments_overrides"

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setKey(Ljava/lang/String;)V

    .line 291
    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 292
    check-cast p1, Landroid/preference/PreferenceGroup;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 293
    return-void
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 297
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, "add_custom_flag_key"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p2

    .line 301
    check-cast v0, Ljava/lang/String;

    .line 303
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 304
    invoke-static {v0}, Lchh;->eH(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 305
    iput v0, p0, Lcvf;->bkV:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 329
    const-string v2, "id:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 352
    :cond_1
    :goto_1
    return v4

    .line 307
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    const-string v2, "Invalid ID."

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 310
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    const-string v2, "Invalid ID."

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_0

    .line 312
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v2, "remove_experiments_overrides"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 316
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_4

    .line 317
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, v4}, Lcke;->cs(Z)V

    .line 318
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    new-instance v2, Ljjf;

    invoke-direct {v2}, Ljjf;-><init>()V

    invoke-interface {v0, v2}, Lcke;->a(Ljjf;)V

    .line 319
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    new-instance v2, Ljjf;

    invoke-direct {v2}, Ljjf;-><init>()V

    invoke-interface {v0, v2}, Lcke;->b(Ljjf;)V

    .line 320
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcke;->hc(Ljava/lang/String;)V

    .line 321
    invoke-direct {p0}, Lcvf;->Th()V

    goto :goto_0

    .line 323
    :cond_4
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, v1}, Lcke;->cs(Z)V

    goto :goto_0

    .line 332
    :cond_5
    const-string v2, "id:"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 333
    iget-object v2, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 337
    invoke-static {v0}, Lchh;->eH(I)I

    move-result v2

    .line 338
    iget-object v3, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjg;

    .line 339
    if-nez v2, :cond_6

    .line 340
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljjg;->in(Z)Ljjg;

    goto/16 :goto_1

    .line 341
    :cond_6
    if-ne v2, v4, :cond_7

    .line 344
    :try_start_2
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v1

    .line 348
    :goto_2
    invoke-virtual {v0, v1}, Ljjg;->qn(I)Ljjg;

    goto/16 :goto_1

    .line 350
    :cond_7
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljjg;->wh(Ljava/lang/String;)Ljjg;

    goto/16 :goto_1

    .line 346
    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 373
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "remove_person_shortcuts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcvf;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aKa()Lciy;

    move-result-object v0

    invoke-virtual {v0}, Lciy;->KP()V

    .line 415
    :goto_0
    return v3

    .line 376
    :cond_0
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "log_contact_accounts_to_clearcut"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "log_contact_accounts_to_clearcut"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    goto :goto_0

    .line 381
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "log_contacts_to_clearcut"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "log_contacts_to_clearcut_unconditionally"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    goto :goto_0

    .line 386
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gsa_home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 387
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "send_gsa_home_request"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clear_local_overrides_key"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 391
    iget-object v0, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcvf;->bkU:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 392
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    new-instance v1, Ljjf;

    invoke-direct {v1}, Ljjf;-><init>()V

    invoke-interface {v0, v1}, Lcke;->b(Ljjf;)V

    .line 394
    invoke-direct {p0}, Lcvf;->Th()V

    .line 395
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcke;->c(Ljava/util/Set;)V

    goto/16 :goto_0

    .line 397
    :cond_4
    iget v0, p0, Lcvf;->bkV:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 398
    iget-object v0, p0, Lcvf;->bkU:Ljava/util/Set;

    iget v1, p0, Lcvf;->bkV:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 400
    iget-object v0, p0, Lcvf;->bkU:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 401
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 403
    :cond_5
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, v1}, Lcke;->c(Ljava/util/Set;)V

    .line 404
    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_6

    .line 405
    iget-object v0, p0, Lcvf;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->recreate()V

    .line 408
    :cond_6
    new-instance v1, Ljjf;

    invoke-direct {v1}, Ljjf;-><init>()V

    .line 409
    iget-object v0, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcvf;->bkT:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Ljjg;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljjg;

    iput-object v0, v1, Ljjf;->eoH:[Ljjg;

    .line 411
    iget-object v0, p0, Lcvf;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, v1}, Lcke;->b(Ljjf;)V

    .line 414
    invoke-direct {p0}, Lcvf;->Th()V

    goto/16 :goto_0
.end method
