.class public final Liuv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final dKX:[J

.field private static final dKY:[I

.field private static final dKZ:[I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    .line 353
    const/16 v0, 0x25

    new-array v0, v0, [J

    sput-object v0, Liuv;->dKX:[J

    .line 354
    const/16 v0, 0x25

    new-array v0, v0, [I

    sput-object v0, Liuv;->dKY:[I

    .line 355
    const/16 v0, 0x25

    new-array v0, v0, [I

    sput-object v0, Liuv;->dKZ:[I

    .line 357
    new-instance v5, Ljava/math/BigInteger;

    const-string v0, "10000000000000000"

    const/16 v1, 0x10

    invoke-direct {v5, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 358
    const/4 v0, 0x2

    move v4, v0

    :goto_0
    const/16 v0, 0x24

    if-gt v4, v0, :cond_8

    .line 359
    sget-object v2, Liuv;->dKX:[J

    int-to-long v0, v4

    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-gez v3, :cond_1

    const-wide/16 v6, -0x1

    invoke-static {v6, v7, v0, v1}, Liuv;->compare(JJ)I

    move-result v0

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_1
    aput-wide v0, v2, v4

    .line 360
    sget-object v6, Liuv;->dKY:[I

    int-to-long v0, v4

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_6

    const-wide/16 v2, -0x1

    invoke-static {v2, v3, v0, v1}, Liuv;->compare(JJ)I

    move-result v2

    if-gez v2, :cond_4

    const-wide/16 v0, -0x1

    :goto_2
    long-to-int v0, v0

    aput v0, v6, v4

    .line 361
    sget-object v0, Liuv;->dKZ:[I

    invoke-virtual {v5, v4}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v4

    .line 358
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 359
    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_1

    :cond_1
    const-wide/16 v6, -0x1

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-ltz v3, :cond_2

    const-wide/16 v6, -0x1

    div-long v0, v6, v0

    goto :goto_1

    :cond_2
    const-wide v6, 0x7fffffffffffffffL

    div-long/2addr v6, v0

    const/4 v3, 0x1

    shl-long/2addr v6, v3

    const-wide/16 v8, -0x1

    mul-long v10, v6, v0

    sub-long/2addr v8, v10

    invoke-static {v8, v9, v0, v1}, Liuv;->compare(JJ)I

    move-result v0

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    int-to-long v0, v0

    add-long/2addr v0, v6

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    .line 360
    :cond_4
    const-wide/16 v2, -0x1

    :cond_5
    :goto_4
    sub-long v0, v2, v0

    goto :goto_2

    :cond_6
    const-wide/16 v2, -0x1

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-ltz v2, :cond_7

    const-wide/16 v2, -0x1

    rem-long v0, v2, v0

    goto :goto_2

    :cond_7
    const-wide v2, 0x7fffffffffffffffL

    div-long/2addr v2, v0

    const/4 v7, 0x1

    shl-long/2addr v2, v7

    const-wide/16 v8, -0x1

    mul-long/2addr v2, v0

    sub-long v2, v8, v2

    invoke-static {v2, v3, v0, v1}, Liuv;->compare(JJ)I

    move-result v7

    if-gez v7, :cond_5

    const-wide/16 v0, 0x0

    goto :goto_4

    .line 363
    :cond_8
    return-void
.end method

.method private static compare(JJ)I
    .locals 6

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 72
    xor-long v0, p0, v2

    xor-long/2addr v2, p2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static toString(J)Ljava/lang/String;
    .locals 10

    .prologue
    .line 307
    const/4 v0, 0x1

    const-string v1, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x40

    new-array v7, v0, [C

    array-length v0, v7

    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-gez v1, :cond_2

    const/16 v1, 0x20

    ushr-long v4, p0, v1

    const-wide v2, 0xffffffffL

    and-long/2addr v2, p0

    const-wide/16 v8, 0xa

    rem-long v8, v4, v8

    const/16 v1, 0x20

    shl-long/2addr v8, v1

    add-long/2addr v2, v8

    const-wide/16 v8, 0xa

    div-long/2addr v4, v8

    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-gtz v1, :cond_1

    const-wide/16 v8, 0x0

    cmp-long v1, v4, v8

    if-lez v1, :cond_3

    :cond_1
    add-int/lit8 v6, v0, -0x1

    const-wide/16 v0, 0xa

    rem-long v0, v2, v0

    long-to-int v0, v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Character;->forDigit(II)C

    move-result v0

    aput-char v0, v7, v6

    const-wide/16 v0, 0xa

    div-long v0, v2, v0

    const-wide/16 v2, 0xa

    rem-long v2, v4, v2

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    add-long/2addr v0, v2

    const-wide/16 v2, 0xa

    div-long v2, v4, v2

    move-wide v4, v2

    move-wide v2, v0

    move v0, v6

    goto :goto_1

    :cond_2
    :goto_2
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-lez v1, :cond_3

    add-int/lit8 v0, v0, -0x1

    const-wide/16 v2, 0xa

    rem-long v2, p0, v2

    long-to-int v1, v2

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/lang/Character;->forDigit(II)C

    move-result v1

    aput-char v1, v7, v0

    const-wide/16 v2, 0xa

    div-long/2addr p0, v2

    goto :goto_2

    :cond_3
    new-instance v1, Ljava/lang/String;

    array-length v2, v7

    sub-int/2addr v2, v0

    invoke-direct {v1, v7, v0, v2}, Ljava/lang/String;-><init>([CII)V

    move-object v0, v1

    goto :goto_0
.end method
