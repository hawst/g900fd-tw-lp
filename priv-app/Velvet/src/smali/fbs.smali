.class public abstract Lfbs;
.super Lfkb;
.source "PG"


# instance fields
.field private final mEntryValidator:Lfbp;


# direct methods
.method public constructor <init>(Lfbp;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lfkb;-><init>()V

    .line 18
    iput-object p1, p0, Lfbs;->mEntryValidator:Lfbp;

    .line 19
    return-void
.end method


# virtual methods
.method protected abstract a(ILizj;)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method protected abstract a(ILizq;)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method protected abstract a(Lizj;Ljal;I)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method protected final b(Lizj;Ljal;I)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0, p1, p2, p3}, Lfbs;->c(Lizj;Ljal;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lfbs;->a(Lizj;Ljal;I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(ILizj;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p2}, Lfbs;->e(Lizj;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lfbs;->a(ILizj;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c(ILizq;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lfbs;->mEntryValidator:Lfbp;

    invoke-static {p1, p2}, Lfbp;->b(ILizq;)Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lfbs;->a(ILizq;)Ljava/lang/Object;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c(Lizj;Ljal;I)Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lfbs;->mEntryValidator:Lfbp;

    invoke-static {}, Lfbp;->axl()Ljava/lang/Boolean;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Lizj;)Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lfbs;->mEntryValidator:Lfbp;

    invoke-virtual {v0, p1}, Lfbp;->d(Lizj;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
