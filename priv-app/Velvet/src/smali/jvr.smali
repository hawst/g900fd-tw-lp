.class public final Ljvr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dIj:I

.field private eHO:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 860
    const/16 v0, 0xb

    const-class v1, Ljvr;

    const v2, 0xe2e6642

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 871
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 916
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 917
    iput v0, p0, Ljvr;->aez:I

    iput v0, p0, Ljvr;->dIj:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljvr;->eHO:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljvr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvr;->eCz:I

    .line 918
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 853
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljvr;->dIj:I

    iget v0, p0, Ljvr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljvr;->eHO:J

    iget v0, p0, Ljvr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvr;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 932
    iget v0, p0, Ljvr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 933
    const/4 v0, 0x1

    iget v1, p0, Ljvr;->dIj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 935
    :cond_0
    iget v0, p0, Ljvr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 936
    const/4 v0, 0x2

    iget-wide v2, p0, Ljvr;->eHO:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 938
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 939
    return-void
.end method

.method public final bvb()Z
    .locals 1

    .prologue
    .line 889
    iget v0, p0, Ljvr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bvc()J
    .locals 2

    .prologue
    .line 900
    iget-wide v0, p0, Ljvr;->eHO:J

    return-wide v0
.end method

.method public final dM(J)Ljvr;
    .locals 1

    .prologue
    .line 903
    iput-wide p1, p0, Ljvr;->eHO:J

    .line 904
    iget v0, p0, Ljvr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvr;->aez:I

    .line 905
    return-object p0
.end method

.method public final getEventType()I
    .locals 1

    .prologue
    .line 881
    iget v0, p0, Ljvr;->dIj:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 943
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 944
    iget v1, p0, Ljvr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 945
    const/4 v1, 0x1

    iget v2, p0, Ljvr;->dIj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 948
    :cond_0
    iget v1, p0, Ljvr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 949
    const/4 v1, 0x2

    iget-wide v2, p0, Ljvr;->eHO:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 952
    :cond_1
    return v0
.end method

.method public final sB(I)Ljvr;
    .locals 1

    .prologue
    .line 884
    iput p1, p0, Ljvr;->dIj:I

    .line 885
    iget v0, p0, Ljvr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvr;->aez:I

    .line 886
    return-object p0
.end method
