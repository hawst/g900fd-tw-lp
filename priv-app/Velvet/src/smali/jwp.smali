.class public final Ljwp;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJn:Ljsm;


# instance fields
.field private aez:I

.field private eJo:Ljava/lang/String;

.field private eJp:Z

.field private eJq:[B

.field private eJr:Z

.field public eJs:Ljvw;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 568
    const/16 v0, 0xb

    const-class v1, Ljwp;

    const v2, 0x12ceb82a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwp;->eJn:Ljsm;

    .line 574
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 666
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 667
    iput v1, p0, Ljwp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljwp;->eJo:Ljava/lang/String;

    iput-boolean v1, p0, Ljwp;->eJp:Z

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljwp;->eJq:[B

    iput-boolean v1, p0, Ljwp;->eJr:Z

    iput-object v2, p0, Ljwp;->eJs:Ljvw;

    iput-object v2, p0, Ljwp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwp;->eCz:I

    .line 668
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 561
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwp;->eJo:Ljava/lang/String;

    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwp;->eJp:Z

    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwp;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljwp;->eJq:[B

    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwp;->eJr:Z

    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwp;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljwp;->eJs:Ljvw;

    if-nez v0, :cond_1

    new-instance v0, Ljvw;

    invoke-direct {v0}, Ljvw;-><init>()V

    iput-object v0, p0, Ljwp;->eJs:Ljvw;

    :cond_1
    iget-object v0, p0, Ljwp;->eJs:Ljvw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 685
    iget v0, p0, Ljwp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 686
    const/4 v0, 0x1

    iget-object v1, p0, Ljwp;->eJo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 688
    :cond_0
    iget v0, p0, Ljwp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 689
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljwp;->eJp:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 691
    :cond_1
    iget v0, p0, Ljwp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 692
    const/4 v0, 0x3

    iget-object v1, p0, Ljwp;->eJq:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 694
    :cond_2
    iget v0, p0, Ljwp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 695
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljwp;->eJr:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 697
    :cond_3
    iget-object v0, p0, Ljwp;->eJs:Ljvw;

    if-eqz v0, :cond_4

    .line 698
    const/4 v0, 0x5

    iget-object v1, p0, Ljwp;->eJs:Ljvw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 700
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 701
    return-void
.end method

.method public final aA([B)Ljwp;
    .locals 1

    .prologue
    .line 628
    if-nez p1, :cond_0

    .line 629
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 631
    :cond_0
    iput-object p1, p0, Ljwp;->eJq:[B

    .line 632
    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwp;->aez:I

    .line 633
    return-object p0
.end method

.method public final bvo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Ljwp;->eJo:Ljava/lang/String;

    return-object v0
.end method

.method public final bvp()Z
    .locals 1

    .prologue
    .line 595
    iget v0, p0, Ljwp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bvq()Z
    .locals 1

    .prologue
    .line 606
    iget-boolean v0, p0, Ljwp;->eJp:Z

    return v0
.end method

.method public final bvr()[B
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Ljwp;->eJq:[B

    return-object v0
.end method

.method public final bvs()Z
    .locals 1

    .prologue
    .line 636
    iget v0, p0, Ljwp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bvt()Z
    .locals 1

    .prologue
    .line 647
    iget-boolean v0, p0, Ljwp;->eJr:Z

    return v0
.end method

.method public final jf(Z)Ljwp;
    .locals 1

    .prologue
    .line 609
    iput-boolean p1, p0, Ljwp;->eJp:Z

    .line 610
    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwp;->aez:I

    .line 611
    return-object p0
.end method

.method public final jg(Z)Ljwp;
    .locals 1

    .prologue
    .line 650
    iput-boolean p1, p0, Ljwp;->eJr:Z

    .line 651
    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwp;->aez:I

    .line 652
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 705
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 706
    iget v1, p0, Ljwp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 707
    const/4 v1, 0x1

    iget-object v2, p0, Ljwp;->eJo:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    :cond_0
    iget v1, p0, Ljwp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 711
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljwp;->eJp:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 714
    :cond_1
    iget v1, p0, Ljwp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 715
    const/4 v1, 0x3

    iget-object v2, p0, Ljwp;->eJq:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 718
    :cond_2
    iget v1, p0, Ljwp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 719
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljwp;->eJr:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 722
    :cond_3
    iget-object v1, p0, Ljwp;->eJs:Ljvw;

    if-eqz v1, :cond_4

    .line 723
    const/4 v1, 0x5

    iget-object v2, p0, Ljwp;->eJs:Ljvw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 726
    :cond_4
    return v0
.end method

.method public final zu(Ljava/lang/String;)Ljwp;
    .locals 1

    .prologue
    .line 587
    if-nez p1, :cond_0

    .line 588
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 590
    :cond_0
    iput-object p1, p0, Ljwp;->eJo:Ljava/lang/String;

    .line 591
    iget v0, p0, Ljwp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwp;->aez:I

    .line 592
    return-object p0
.end method
