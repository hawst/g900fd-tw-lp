.class Lmt;
.super Lmp;
.source "PG"

# interfaces
.implements Lly;


# instance fields
.field private mc:Llx;

.field private md:Lma;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmy;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1, p2}, Lmp;-><init>(Landroid/content/Context;Lmy;)V

    .line 717
    return-void
.end method


# virtual methods
.method public final R(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 721
    invoke-virtual {p0, p1}, Lmt;->U(Ljava/lang/Object;)I

    move-result v0

    .line 722
    if-ltz v0, :cond_0

    .line 723
    iget-object v1, p0, Lmt;->lU:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmr;

    .line 724
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 728
    :goto_0
    iget-object v2, v0, Lmr;->ma:Lkq;

    invoke-virtual {v2}, Lkq;->by()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 730
    new-instance v2, Lkr;

    iget-object v3, v0, Lmr;->ma:Lkq;

    invoke-direct {v2, v3}, Lkr;-><init>(Lkq;)V

    invoke-virtual {v2, v1}, Lkr;->U(I)Lkr;

    move-result-object v1

    invoke-virtual {v1}, Lkr;->bz()Lkq;

    move-result-object v1

    iput-object v1, v0, Lmr;->ma:Lkq;

    .line 734
    invoke-virtual {p0}, Lmt;->cd()V

    .line 737
    :cond_0
    return-void

    .line 726
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method protected a(Lmr;Lkr;)V
    .locals 3

    .prologue
    .line 742
    invoke-super {p0, p1, p2}, Lmp;->a(Lmr;Lkr;)V

    .line 744
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    iget-object v0, p2, Lkr;->mBundle:Landroid/os/Bundle;

    const-string v1, "enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 748
    :cond_0
    invoke-virtual {p0, p1}, Lmt;->b(Lmr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    iget-object v0, p2, Lkr;->mBundle:Landroid/os/Bundle;

    const-string v1, "connecting"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 752
    :cond_1
    iget-object v0, p1, Lmr;->lY:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_2

    .line 755
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, Lkr;->U(I)Lkr;

    .line 757
    :cond_2
    return-void
.end method

.method protected b(Lmr;)Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lmt;->md:Lma;

    if-nez v0, :cond_0

    .line 777
    new-instance v0, Lma;

    invoke-direct {v0}, Lma;-><init>()V

    iput-object v0, p0, Lmt;->md:Lma;

    .line 779
    :cond_0
    iget-object v0, p0, Lmt;->md:Lma;

    iget-object v1, p1, Lmr;->lY:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lma;->S(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected ce()V
    .locals 3

    .prologue
    .line 761
    invoke-super {p0}, Lmp;->ce()V

    .line 763
    iget-object v0, p0, Lmt;->mc:Llx;

    if-nez v0, :cond_0

    .line 764
    new-instance v0, Llx;

    iget-object v1, p0, Lkt;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lkt;->kh:Lkv;

    invoke-direct {v0, v1, v2}, Llx;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lmt;->mc:Llx;

    .line 767
    :cond_0
    iget-object v1, p0, Lmt;->mc:Llx;

    iget-boolean v0, p0, Lmt;->lS:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmt;->lR:I

    :goto_0
    invoke-virtual {v1, v0}, Llx;->X(I)V

    .line 768
    return-void

    .line 767
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final cf()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 772
    new-instance v0, Llz;

    invoke-direct {v0, p0}, Llz;-><init>(Lly;)V

    return-object v0
.end method
