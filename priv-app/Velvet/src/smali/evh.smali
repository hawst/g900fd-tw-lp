.class public final Levh;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final cjM:Lcom/google/android/sidekick/main/RemindersListActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/main/RemindersListActivity;)V
    .locals 0

    .prologue
    .line 560
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 561
    iput-object p1, p0, Levh;->cjM:Lcom/google/android/sidekick/main/RemindersListActivity;

    .line 562
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 555
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljbj;->om(I)Ljbj;

    move-result-object v0

    new-array v1, v4, [I

    const/16 v2, 0x2b

    aput v2, v1, v5

    iput-object v1, v0, Ljbj;->dYx:[I

    iget-object v1, p0, Levh;->cjM:Lcom/google/android/sidekick/main/RemindersListActivity;

    invoke-static {v1}, Lcom/google/android/sidekick/main/RemindersListActivity;->c(Lcom/google/android/sidekick/main/RemindersListActivity;)Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axX()Leuc;

    move-result-object v1

    invoke-virtual {v1}, Leuc;->avW()Ljava/util/Collection;

    move-result-object v1

    new-instance v2, Liyb;

    invoke-direct {v2}, Liyb;-><init>()V

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    new-array v3, v3, [Ljbt;

    iput-object v3, v2, Liyb;->dMd:[Ljbt;

    iget-object v3, v2, Liyb;->dMd:[Ljbt;

    invoke-interface {v1, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_0
    new-instance v1, Lizm;

    invoke-direct {v1}, Lizm;-><init>()V

    new-array v3, v4, [Ljbj;

    aput-object v0, v3, v5

    iput-object v3, v1, Lizm;->dUH:[Ljbj;

    iput-object v2, v1, Lizm;->dUG:Liyb;

    const/16 v0, 0xb

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljed;->hK(Z)Ljed;

    move-result-object v0

    iput-object v1, v0, Ljed;->edr:Lizm;

    iget-object v1, p0, Levh;->cjM:Lcom/google/android/sidekick/main/RemindersListActivity;

    invoke-static {v1}, Lcom/google/android/sidekick/main/RemindersListActivity;->d(Lcom/google/android/sidekick/main/RemindersListActivity;)Lfcx;

    move-result-object v1

    invoke-interface {v1, v0}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Ljeh;->aeC:Lizn;

    if-eqz v1, :cond_1

    iget-object v0, v0, Ljeh;->aeC:Lizn;

    iget-object v0, v0, Lizn;->dUI:[Lizo;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 555
    check-cast p1, [Lizo;

    iget-object v0, p0, Levh;->cjM:Lcom/google/android/sidekick/main/RemindersListActivity;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    iget-object v1, p0, Levh;->cjM:Lcom/google/android/sidekick/main/RemindersListActivity;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->a(Lcom/google/android/sidekick/main/RemindersListActivity;Z)Z

    iget-object v0, p0, Levh;->cjM:Lcom/google/android/sidekick/main/RemindersListActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/main/RemindersListActivity;->a([Lizo;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
