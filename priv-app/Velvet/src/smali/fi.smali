.class public final Lfi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static fZ:Lfj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 90
    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 91
    new-instance v0, Lfl;

    invoke-direct {v0}, Lfl;-><init>()V

    sput-object v0, Lfi;->fZ:Lfj;

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v0, Lfk;

    invoke-direct {v0}, Lfk;-><init>()V

    sput-object v0, Lfi;->fZ:Lfj;

    goto :goto_0
.end method

.method public static getAbsoluteGravity(II)I
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lfi;->fZ:Lfj;

    invoke-interface {v0, p0, p1}, Lfj;->getAbsoluteGravity(II)I

    move-result v0

    return v0
.end method
