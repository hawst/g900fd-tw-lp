.class public final Lfwb;
.super Lfus;
.source "PG"


# static fields
.field private static final cCI:Ljava/text/DecimalFormat;


# instance fields
.field private final mFifeImageUrlUtil:Lgan;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.0"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfwb;->cCI:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgan;Lgbr;)V
    .locals 6

    .prologue
    .line 37
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 39
    iput-object p4, p0, Lfwb;->mFifeImageUrlUtil:Lgan;

    .line 40
    return-void
.end method

.method private a(Landroid/content/Context;Lizj;I)Lanh;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 48
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    .line 49
    invoke-virtual {v3, p3}, Lanh;->cm(I)Lanh;

    .line 50
    iput-object p2, v3, Lanh;->ahu:Lizj;

    .line 52
    iget-object v0, p2, Lizj;->dTV:Ljcb;

    if-nez v0, :cond_7

    const-string v0, "QpNearbyProductEntryAdapter"

    const-string v2, "Unexpected Entry without nearby list entry."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 54
    :goto_0
    new-instance v4, Laod;

    invoke-direct {v4}, Laod;-><init>()V

    .line 57
    invoke-virtual {v0}, Ljcb;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->by(Ljava/lang/String;)Laod;

    .line 58
    invoke-virtual {v0}, Ljcb;->bfU()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    invoke-virtual {v0}, Ljcb;->bfT()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->bC(Ljava/lang/String;)Laod;

    .line 61
    :cond_0
    invoke-virtual {v0}, Ljcb;->qv()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {v0}, Ljcb;->qu()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->bB(Ljava/lang/String;)Laod;

    .line 64
    :cond_1
    invoke-virtual {v0}, Ljcb;->bfW()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 65
    invoke-virtual {v0}, Ljcb;->bfV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->bD(Ljava/lang/String;)Laod;

    .line 67
    :cond_2
    invoke-virtual {v0}, Ljcb;->bgb()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    sget-object v2, Lfwb;->cCI:Ljava/text/DecimalFormat;

    invoke-virtual {v0}, Ljcb;->bga()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->bz(Ljava/lang/String;)Laod;

    .line 69
    invoke-virtual {v0}, Ljcb;->bga()F

    move-result v2

    float-to-double v6, v2

    invoke-static {v6, v7}, Lgbe;->h(D)I

    move-result v2

    invoke-virtual {v4, v2}, Laod;->cA(I)Laod;

    .line 71
    invoke-virtual {v0}, Ljcb;->bfZ()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 72
    invoke-virtual {v0}, Ljcb;->bfY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->bA(Ljava/lang/String;)Laod;

    .line 75
    :cond_3
    iget-object v2, v0, Ljcb;->aiX:Ljcn;

    if-eqz v2, :cond_4

    iget-object v2, v0, Ljcb;->aiX:Ljcn;

    invoke-virtual {v2}, Ljcn;->qG()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 76
    iget-object v2, p0, Lfwb;->mFifeImageUrlUtil:Lgan;

    iget-object v5, v0, Ljcb;->aiX:Ljcn;

    const v6, 0x7f0d015c

    const v7, 0x7f0d015d

    invoke-virtual {v2, p1, v5, v6, v7}, Lgan;->a(Landroid/content/Context;Ljcn;II)Ljava/lang/String;

    move-result-object v2

    .line 78
    if-eqz v2, :cond_4

    .line 79
    invoke-virtual {v4, v2}, Laod;->bE(Ljava/lang/String;)Laod;

    .line 82
    :cond_4
    invoke-virtual {v0}, Ljcb;->pf()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 83
    invoke-virtual {v0}, Ljcb;->oo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Laod;->bF(Ljava/lang/String;)Laod;

    .line 86
    :cond_5
    new-instance v2, Lfsa;

    const/16 v5, 0xb8

    invoke-direct {v2, v5}, Lfsa;-><init>(I)V

    .line 87
    const/16 v5, 0x22

    if-ne p3, v5, :cond_6

    .line 88
    const v5, 0x7f020227

    invoke-virtual {v2, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    .line 91
    :cond_6
    invoke-virtual {v0}, Ljcb;->bfX()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v3, Lanh;->ahs:Lani;

    .line 93
    iput-object v4, v3, Lanh;->agI:Laod;

    .line 94
    return-object v3

    .line 52
    :cond_7
    iget-object v0, p2, Lizj;->dTV:Ljcb;

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x1c

    invoke-direct {p0, p1, p2, v0}, Lfwb;->a(Landroid/content/Context;Lizj;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lftn;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTW:Ljcc;

    invoke-virtual {v0}, Ljcc;->oo()Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    iput-object v0, p1, Lftn;->cBi:Ljava/lang/String;

    .line 118
    :cond_0
    return-void
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTW:Ljcc;

    invoke-virtual {v0}, Ljcc;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x1

    new-array v0, v0, [Lanh;

    .line 123
    const/4 v1, 0x0

    const/16 v2, 0x22

    invoke-direct {p0, p1, p2, v2}, Lfwb;->a(Landroid/content/Context;Lizj;I)Lanh;

    move-result-object v2

    aput-object v2, v0, v1

    .line 124
    return-object v0
.end method
