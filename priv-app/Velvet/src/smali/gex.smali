.class public final Lgex;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lger;


# instance fields
.field private final cHI:I

.field private final cIg:I

.field private final cIh:I

.field private final cIi:Lgfs;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cIj:Lger;

.field private cIk:Lgft;

.field private cIl:Lgey;


# direct methods
.method public constructor <init>(IIIILger;Lequ;)V
    .locals 1
    .param p6    # Lequ;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iput p2, p0, Lgex;->cHI:I

    .line 59
    const/16 v0, 0x1f4

    iput v0, p0, Lgex;->cIg:I

    .line 60
    const/16 v0, 0x3e8

    iput v0, p0, Lgex;->cIh:I

    .line 61
    iput-object p5, p0, Lgex;->cIj:Lger;

    .line 62
    if-nez p6, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lgex;->cIi:Lgfs;

    .line 64
    return-void

    .line 62
    :cond_0
    new-instance v0, Lgfs;

    invoke-direct {v0, p6}, Lgfs;-><init>(Lequ;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lglw;)V
    .locals 3
    .param p1    # Lglw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgex;->cIl:Lgey;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 114
    iget-object v0, p0, Lgex;->cIj:Lger;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    new-instance v0, Lgey;

    iget v1, p0, Lgex;->cHI:I

    iget-object v2, p0, Lgex;->cIi:Lgfs;

    invoke-direct {v0, v1, v2, p1}, Lgey;-><init>(ILgfs;Lglw;)V

    iput-object v0, p0, Lgex;->cIl:Lgey;

    .line 117
    iget-object v0, p0, Lgex;->cIk:Lgft;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lgex;->cIl:Lgey;

    iget-object v1, p0, Lgex;->cIk:Lgft;

    iget-object v1, v1, Lgft;->cIn:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Lgey;->setInputStream(Ljava/io/InputStream;)V

    .line 119
    iget-object v0, p0, Lgex;->cIl:Lgey;

    invoke-virtual {v0}, Lgey;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :cond_0
    monitor-exit p0

    return-void

    .line 113
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized df()Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgex;->cIj:Lger;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stopped"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 94
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgex;->cIk:Lgft;

    if-nez v0, :cond_1

    .line 95
    new-instance v0, Lgft;

    iget-object v1, p0, Lgex;->cIj:Lger;

    invoke-interface {v1}, Lger;->df()Ljava/io/InputStream;

    move-result-object v1

    iget v2, p0, Lgex;->cHI:I

    iget v3, p0, Lgex;->cIg:I

    iget v4, p0, Lgex;->cIh:I

    const/16 v5, 0x10

    invoke-direct/range {v0 .. v5}, Lgft;-><init>(Ljava/io/InputStream;IIII)V

    iput-object v0, p0, Lgex;->cIk:Lgft;

    .line 97
    iget-object v0, p0, Lgex;->cIl:Lgey;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lgex;->cIl:Lgey;

    iget-object v1, p0, Lgex;->cIk:Lgft;

    iget-object v1, v1, Lgft;->cIn:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Lgey;->setInputStream(Ljava/io/InputStream;)V

    .line 99
    iget-object v0, p0, Lgex;->cIl:Lgey;

    invoke-virtual {v0}, Lgey;->start()V

    .line 102
    :cond_1
    iget-object v0, p0, Lgex;->cIk:Lgft;

    invoke-virtual {v0}, Lgft;->aFH()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized shutdown()V
    .locals 1

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lgex;->stopListening()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lgex;->cIj:Lger;

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lgex;->cIk:Lgft;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized stopListening()V
    .locals 1

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgex;->cIl:Lgey;

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lgex;->cIl:Lgey;

    invoke-virtual {v0}, Lgey;->aFw()V

    .line 138
    iget-object v0, p0, Lgex;->cIk:Lgft;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lgex;->cIl:Lgey;

    invoke-virtual {v0}, Lgey;->interrupt()V

    .line 142
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lgex;->cIl:Lgey;

    .line 146
    :cond_1
    iget-object v0, p0, Lgex;->cIk:Lgft;

    if-nez v0, :cond_2

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lgex;->cIj:Lger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :cond_2
    monitor-exit p0

    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
