.class public final Lidv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final dyU:Ljava/lang/Runnable;

.field private dyV:Ljava/util/concurrent/ScheduledFuture;

.field volatile dyW:J

.field private final dyX:I

.field private final dyY:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(ILjava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 33
    const v0, 0xea60

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lidv;-><init>(ILjava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;)V

    .line 34
    return-void
.end method

.method public constructor <init>(ILjava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p3, p0, Lidv;->dyU:Ljava/lang/Runnable;

    .line 39
    iput p1, p0, Lidv;->dyX:I

    .line 40
    iput-object p2, p0, Lidv;->dyY:Ljava/util/concurrent/ScheduledExecutorService;

    .line 41
    return-void
.end method


# virtual methods
.method public final aSw()V
    .locals 4

    .prologue
    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget v2, p0, Lidv;->dyX:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lidv;->dyW:J

    .line 49
    return-void
.end method

.method final aVc()V
    .locals 6

    .prologue
    .line 72
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lidv;->dyW:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 75
    :try_start_0
    iget-object v2, p0, Lidv;->dyY:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lidw;

    invoke-direct {v3, p0}, Lidw;-><init>(Lidv;)V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lidv;->dyV:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final start()V
    .locals 0

    .prologue
    .line 55
    invoke-virtual {p0}, Lidv;->aSw()V

    .line 56
    invoke-virtual {p0}, Lidv;->aVc()V

    .line 57
    return-void
.end method

.method public final stop()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lidv;->dyV:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lidv;->dyV:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 67
    :cond_0
    iget-object v0, p0, Lidv;->dyY:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    .line 68
    return-void
.end method
