.class public final Lfsv;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 28
    return-void
.end method

.method private a(Landroid/view/View;IILanm;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p4}, Lanm;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 88
    invoke-virtual {p4}, Lanm;->pt()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    invoke-virtual {p4}, Lanm;->pu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0, p3}, Lfsv;->jy(I)V

    .line 92
    :cond_0
    invoke-virtual {p4}, Lanm;->ps()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p3, v0}, Lfsv;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 99
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 95
    const/4 v1, 0x6

    invoke-static {v1}, Lfwq;->jK(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 96
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;[Ljava/lang/String;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 73
    invoke-virtual {p0}, Lfsv;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04012d

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 76
    array-length v1, p2

    if-ge p3, v1, :cond_0

    aget-object v1, p2, p3

    .line 78
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v3, v2}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 82
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/TableRow$LayoutParams;->weight:F

    .line 83
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    return-void

    .line 76
    :cond_0
    iget-object v1, p0, Lfsv;->mContext:Landroid/content/Context;

    const v2, 0x7f0a021a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected final aCZ()V
    .locals 10

    .prologue
    .line 37
    iget-object v2, p0, Lfro;->mView:Landroid/view/View;

    .line 38
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 39
    iget-object v3, v0, Lanh;->ahh:Lanl;

    .line 41
    const v0, 0x7f11019f

    invoke-virtual {v3}, Lanl;->pl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 42
    const v0, 0x7f1102f8

    invoke-virtual {v3}, Lanl;->pn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lanl;->po()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    invoke-virtual {v3}, Lanl;->getHighlightColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 47
    :cond_0
    const v0, 0x7f1102f9

    invoke-virtual {v3}, Lanl;->pp()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 49
    iget-object v4, v3, Lanl;->ahX:Lanm;

    .line 50
    const v0, 0x7f1102fc

    const v1, 0x7f1102fa

    invoke-direct {p0, v2, v0, v1, v4}, Lfsv;->a(Landroid/view/View;IILanm;)V

    .line 52
    iget-object v5, v3, Lanl;->ahY:Lanm;

    .line 53
    const v0, 0x7f1102fd

    const v1, 0x7f1102fb

    invoke-direct {p0, v2, v0, v1, v5}, Lfsv;->a(Landroid/view/View;IILanm;)V

    .line 56
    iget-object v0, v4, Lanm;->aic:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, v5, Lanm;->aic:[Ljava/lang/String;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 57
    const v0, 0x7f1102fe

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 59
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 60
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_1

    .line 61
    new-instance v7, Landroid/widget/TableRow;

    iget-object v8, p0, Lfsv;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 62
    iget-object v8, v4, Lanm;->aic:[Ljava/lang/String;

    const/4 v9, 0x5

    invoke-direct {p0, v7, v8, v1, v9}, Lfsv;->a(Landroid/view/ViewGroup;[Ljava/lang/String;II)V

    .line 63
    iget-object v8, v5, Lanm;->aic:[Ljava/lang/String;

    const/4 v9, 0x6

    invoke-direct {p0, v7, v8, v1, v9}, Lfsv;->a(Landroid/view/ViewGroup;[Ljava/lang/String;II)V

    .line 64
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 67
    :cond_1
    const v0, 0x7f1102ff

    invoke-virtual {v3}, Lanl;->pq()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 68
    const v0, 0x7f110300

    invoke-virtual {v3}, Lanl;->pr()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 69
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 32
    const v0, 0x7f04010d

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
