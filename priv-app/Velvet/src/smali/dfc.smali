.class public final Ldfc;
.super Landroid/database/AbstractCursor;
.source "PG"


# static fields
.field private static final bcO:[Ljava/lang/String;


# instance fields
.field private final bvQ:Ldef;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_text_2_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_icon_2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "suggest_format"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "suggest_spinner_while_refreshing"

    aput-object v2, v0, v1

    sput-object v0, Ldfc;->bcO:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldef;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 65
    iput-object p1, p0, Ldfc;->bvQ:Ldef;

    .line 66
    return-void
.end method

.method private static aD(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 101
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Ldfc;->bcO:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ldfc;->bvQ:Ldef;

    invoke-interface {v0}, Ldef;->getCount()I

    move-result v0

    return v0
.end method

.method public final getDouble(I)D
    .locals 2

    .prologue
    .line 170
    :try_start_0
    invoke-virtual {p0, p1}, Ldfc;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 172
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final getFloat(I)F
    .locals 1

    .prologue
    .line 179
    :try_start_0
    invoke-virtual {p0, p1}, Ldfc;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 181
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getInt(I)I
    .locals 1

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 85
    invoke-virtual {p0}, Ldfc;->getPosition()I

    move-result v0

    .line 90
    :goto_0
    return v0

    .line 88
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Ldfc;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLong(I)J
    .locals 2

    .prologue
    .line 147
    :try_start_0
    invoke-virtual {p0, p1}, Ldfc;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 149
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final getShort(I)S
    .locals 1

    .prologue
    .line 161
    :try_start_0
    invoke-virtual {p0, p1}, Ldfc;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 163
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-object v1, p0, Ldfc;->bvQ:Ldef;

    invoke-virtual {p0}, Ldfc;->getPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ldef;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    .line 107
    sget-object v2, Ldfc;->bcO:[Ljava/lang/String;

    array-length v2, v2

    if-ge p1, v2, :cond_0

    .line 108
    packed-switch p1, :pswitch_data_0

    .line 136
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ldfc;->bcO:[Ljava/lang/String;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :pswitch_0
    invoke-virtual {p0}, Ldfc;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_0
    :pswitch_1
    return-object v0

    .line 112
    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ldfc;->aD(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114
    :pswitch_3
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->arZ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ldfc;->aD(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 116
    :pswitch_4
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asa()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :pswitch_5
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :pswitch_6
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asf()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 124
    :pswitch_7
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->ash()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 126
    :pswitch_8
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asi()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 128
    :pswitch_9
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132
    :pswitch_a
    const-string v0, "_-1"

    goto :goto_0

    .line 134
    :pswitch_b
    const-string v0, "false"

    goto :goto_0

    .line 140
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 108
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_1
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final isNull(I)Z
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Ldfc;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
