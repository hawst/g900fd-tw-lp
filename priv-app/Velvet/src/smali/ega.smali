.class public final Lega;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Legh;


# instance fields
.field public final bYe:Litu;

.field public final bYf:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Litu;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lega;->bYe:Litu;

    .line 22
    iput-object p2, p0, Lega;->bYf:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 27
    if-ne p0, p1, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v0

    .line 30
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 31
    goto :goto_0

    .line 33
    :cond_2
    check-cast p1, Lega;

    .line 34
    iget-object v2, p0, Lega;->bYe:Litu;

    iget-object v3, p1, Lega;->bYe:Litu;

    invoke-static {v2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lega;->bYf:Ljava/lang/String;

    iget-object v3, p1, Lega;->bYf:Ljava/lang/String;

    invoke-static {v2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lega;->bYe:Litu;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lega;->bYf:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    new-instance v1, Lifp;

    invoke-static {v0}, Lifo;->g(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lifp;-><init>(Ljava/lang/String;B)V

    const-string v0, "mEvent"

    iget-object v2, p0, Lega;->bYe:Litu;

    invoke-virtual {v1, v0, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "mGwsEventId"

    iget-object v2, p0, Lega;->bYf:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
