.class public final Lhzl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhzs;


# static fields
.field private static dvu:Ljava/lang/String;


# instance fields
.field private final dvv:Lhzn;

.field private final mGsaConfigFlags:Lchk;

.field private mQuery:Lcom/google/android/shared/search/Query;

.field private final mSdchManager:Lczz;

.field private final mSearchUrlHelper:Lcpn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "PinholeParamsBuilderTask"

    sput-object v0, Lhzl;->dvu:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcpn;Lczz;Lchk;Lhzn;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lhzl;->mSearchUrlHelper:Lcpn;

    .line 31
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczz;

    iput-object v0, p0, Lhzl;->mSdchManager:Lczz;

    .line 32
    iput-object p3, p0, Lhzl;->mGsaConfigFlags:Lchk;

    .line 33
    iput-object p4, p0, Lhzl;->dvv:Lhzn;

    .line 34
    return-void
.end method

.method static synthetic a(Lhzl;Lcom/google/android/shared/search/Query;Ljava/lang/String;)Ljwq;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18
    iget-object v0, p0, Lhzl;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhzl;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HJ()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iget-object v3, p0, Lhzl;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v3, p1, p2, v0}, Lcpn;->b(Lcom/google/android/shared/search/Query;Ljava/lang/String;Z)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v4

    new-instance v5, Ljwq;

    invoke-direct {v5}, Ljwq;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljwq;->zv(Ljava/lang/String;)Ljwq;

    iget-object v0, p0, Lhzl;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Iz()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5, v1}, Ljwq;->jh(Z)Ljwq;

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    new-array v6, v1, [Ljwo;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljwo;

    invoke-direct {v1}, Ljwo;-><init>()V

    aput-object v1, v6, v3

    aget-object v8, v6, v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljwo;->zs(Ljava/lang/String;)Ljwo;

    aget-object v1, v6, v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljwo;->zt(Ljava/lang/String;)Ljwo;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iput-object v6, v5, Ljwq;->eJv:[Ljwo;

    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcpn;->n(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    new-array v3, v1, [Ljwn;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljwn;

    invoke-direct {v1}, Ljwn;-><init>()V

    aput-object v1, v3, v2

    aget-object v7, v3, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljwn;->zq(Ljava/lang/String;)Ljwn;

    aget-object v1, v3, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljwn;->zr(Ljava/lang/String;)Ljwn;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iput-object v3, v5, Ljwq;->eJu:[Ljwn;

    iget-object v0, p0, Lhzl;->mSdchManager:Lczz;

    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lczz;->a(Landroid/net/Uri;Ljwq;)V

    return-object v5
.end method


# virtual methods
.method public final a(Lhzt;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lhzl;->dvv:Lhzn;

    invoke-virtual {v0, p1}, Lhzn;->b(Lhzt;)V

    .line 60
    return-void
.end method

.method public final a(Lhzt;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lhzl;->dvv:Lhzn;

    invoke-static {p2}, Leoi;->lj(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Lhzn;->a(Lhzt;J)V

    .line 55
    return-void
.end method

.method public final bd(Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lhzl;->mQuery:Lcom/google/android/shared/search/Query;

    .line 38
    return-void
.end method

.method public final oQ(Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lhzl;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 43
    new-instance v1, Lhzm;

    sget-object v2, Lhzl;->dvu:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v0, p1}, Lhzm;-><init>(Lhzl;Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    return-object v1
.end method
