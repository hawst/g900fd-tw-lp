.class public final Lcdj;
.super Ljava/lang/Thread;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final aSB:Lcdf;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final aSJ:Lcdh;

.field private final aSK:Ljava/net/URL;

.field private final aSL:Ljava/util/concurrent/CountDownLatch;

.field private aSM:Legq;

.field private final aSy:Ljzt;

.field private volatile lD:Z

.field private final mConnectionFactory:Lccz;


# direct methods
.method constructor <init>(Lcdh;Ljava/net/URL;Ljzt;Lccz;Lcdf;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 452
    const-string v0, "PairHttpReaderDown"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 453
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legq;

    iput-object v0, p0, Lcdj;->aSM:Legq;

    .line 454
    iput-object p1, p0, Lcdj;->aSJ:Lcdh;

    .line 455
    iput-object p2, p0, Lcdj;->aSK:Ljava/net/URL;

    .line 456
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcdj;->aSL:Ljava/util/concurrent/CountDownLatch;

    .line 457
    iput-boolean v1, p0, Lcdj;->lD:Z

    .line 458
    iput-object p3, p0, Lcdj;->aSy:Ljzt;

    .line 459
    iput-object p4, p0, Lcdj;->mConnectionFactory:Lccz;

    .line 460
    iput-object p5, p0, Lcdj;->aSB:Lcdf;

    .line 461
    return-void
.end method


# virtual methods
.method public final De()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 466
    invoke-virtual {p0}, Lcdj;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v2, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 469
    :try_start_0
    iget-object v0, p0, Lcdj;->aSL:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 471
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdj;->lD:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcdj;->lD:Z

    return v0

    :cond_1
    move v0, v1

    .line 466
    goto :goto_0

    .line 476
    :catch_0
    move-exception v0

    iput-boolean v1, p0, Lcdj;->lD:Z

    goto :goto_1
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdj;->lD:Z

    .line 485
    invoke-virtual {p0}, Lcdj;->interrupt()V

    .line 486
    return-void
.end method

.method public final run()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 493
    :try_start_0
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    iget-object v2, p0, Lcdj;->aSM:Legq;

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 496
    :try_start_1
    iget-object v0, p0, Lcdj;->mConnectionFactory:Lccz;

    iget-object v2, p0, Lcdj;->aSy:Ljzt;

    iget-object v2, v2, Ljzt;->eOr:Ljzm;

    iget-object v2, p0, Lcdj;->aSK:Ljava/net/URL;

    invoke-interface {v0, v2}, Lccz;->a(Ljava/net/URL;)Lbzh;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 498
    :try_start_2
    iget-object v2, p0, Lcdj;->aSy:Ljzt;

    iget-object v2, v2, Ljzt;->eOr:Ljzm;

    invoke-static {v0, v2}, Lcdh;->a(Lbzh;Ljzm;)V

    .line 499
    iget-object v2, p0, Lcdj;->aSJ:Lcdh;

    iget-object v2, p0, Lcdj;->aSy:Ljzt;

    iget-object v2, v2, Ljzt;->eOr:Ljzm;

    invoke-static {v0, v2}, Lcdh;->b(Lbzh;Ljzm;)V

    .line 501
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lbzh;->setUseCaches(Z)V

    .line 502
    invoke-static {v0}, Ldmc;->d(Lbzh;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 513
    :try_start_3
    iget-object v2, p0, Lcdj;->aSL:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 521
    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzh;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    const-string v2, "Download"

    const v3, 0x10010

    invoke-static {v0, v2, v3}, Lcds;->a(Lbzh;Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcdj;->aSK:Ljava/net/URL;

    const-string v4, "Download"

    invoke-static {v2, v3, v0, v4}, Lcds;->a(ILjava/net/URL;Lbzh;Ljava/lang/String;)V
    :try_end_4
    .catch Leie; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    new-instance v3, Lcem;

    invoke-static {v0}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v3, v2}, Lcem;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :goto_0
    :try_start_6
    iget-boolean v2, p0, Lcdj;->lD:Z

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lcem;->Dq()Ljww;

    move-result-object v2

    iget-object v4, p0, Lcdj;->aSJ:Lcdh;

    sget-object v5, Lcdk;->aSP:Lcdk;

    invoke-virtual {v4, v5}, Lcdh;->a(Lcdk;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v2, p0, Lcdj;->aSB:Lcdf;

    new-instance v4, Leie;

    const v5, 0x1000c

    invoke-direct {v4, v5}, Leie;-><init>(I)V

    invoke-interface {v2, v4}, Lcdf;->c(Leie;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_0
    :try_start_7
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V

    invoke-interface {v0}, Lbzh;->disconnect()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 523
    :goto_1
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 524
    :goto_2
    return-void

    .line 504
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_3
    if-eqz v0, :cond_1

    .line 505
    :try_start_8
    invoke-interface {v0}, Lbzh;->disconnect()V

    .line 508
    :cond_1
    iget-object v0, p0, Lcdj;->aSJ:Lcdh;

    sget-object v2, Lcdk;->aSQ:Lcdk;

    invoke-virtual {v0, v2}, Lcdh;->a(Lcdk;)Z

    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdj;->lD:Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 513
    :try_start_9
    iget-object v0, p0, Lcdj;->aSL:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 523
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_2

    .line 513
    :catchall_0
    move-exception v0

    :try_start_a
    iget-object v2, p0, Lcdj;->aSL:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 523
    :catchall_1
    move-exception v0

    sget-object v2, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0

    .line 521
    :catch_1
    move-exception v2

    :try_start_b
    iget-object v3, p0, Lcdj;->aSJ:Lcdh;

    sget-object v4, Lcdk;->aSQ:Lcdk;

    invoke-virtual {v3, v4}, Lcdh;->a(Lcdk;)Z

    move-result v3

    if-eqz v3, :cond_2

    instance-of v3, v2, Leii;

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcdj;->aSB:Lcdf;

    invoke-interface {v3, v2}, Lcdf;->c(Leie;)V

    :goto_4
    invoke-interface {v0}, Lbzh;->disconnect()V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcdj;->aSB:Lcdf;

    invoke-interface {v3, v2}, Lcdf;->b(Leie;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_4

    :cond_4
    :try_start_c
    invoke-virtual {v2}, Ljww;->getStatus()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_5

    invoke-virtual {v2}, Ljww;->getStatus()I

    move-result v4

    if-ne v4, v6, :cond_6

    :cond_5
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcdj;->lD:Z

    :cond_6
    iget-object v4, p0, Lcdj;->aSB:Lcdf;

    invoke-interface {v4, v2}, Lcdf;->b(Ljww;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto/16 :goto_0

    :catch_2
    move-exception v2

    :goto_5
    :try_start_d
    iget-boolean v4, p0, Lcdj;->lD:Z

    if-eqz v4, :cond_7

    const-string v4, "PairHttpConnection"

    const-string v5, "[Download] exception - exit %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-static {v7, v4, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    new-instance v4, Leie;

    const v5, 0x1000e

    invoke-direct {v4, v2, v5}, Leie;-><init>(Ljava/lang/Throwable;I)V

    iget-object v2, p0, Lcdj;->aSJ:Lcdh;

    sget-object v5, Lcdk;->aSQ:Lcdk;

    invoke-virtual {v2, v5}, Lcdh;->a(Lcdk;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcdj;->aSB:Lcdf;

    invoke-interface {v2, v4}, Lcdf;->c(Leie;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :cond_7
    :goto_6
    :try_start_e
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V

    invoke-interface {v0}, Lbzh;->disconnect()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_1

    :cond_8
    :try_start_f
    iget-object v2, p0, Lcdj;->aSB:Lcdf;

    invoke-interface {v2, v4}, Lcdf;->b(Leie;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    goto :goto_6

    :catchall_2
    move-exception v2

    :goto_7
    :try_start_10
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V

    invoke-interface {v0}, Lbzh;->disconnect()V

    throw v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :catchall_3
    move-exception v2

    move-object v3, v1

    goto :goto_7

    :catch_3
    move-exception v2

    move-object v3, v1

    goto :goto_5

    .line 504
    :catch_4
    move-exception v2

    goto/16 :goto_3
.end method
