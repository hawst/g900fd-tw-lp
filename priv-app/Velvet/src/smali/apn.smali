.class public final Lapn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiC:Ljava/lang/String;

.field private akk:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17587
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 17588
    iput v1, p0, Lapn;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lapn;->aiC:Ljava/lang/String;

    iput v1, p0, Lapn;->akk:I

    const/4 v0, 0x0

    iput-object v0, p0, Lapn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapn;->eCz:I

    .line 17589
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 17527
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapn;->aiC:Ljava/lang/String;

    iget v0, p0, Lapn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lapn;->akk:I

    iget v0, p0, Lapn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapn;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 17603
    iget v0, p0, Lapn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 17604
    const/4 v0, 0x1

    iget-object v1, p0, Lapn;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 17606
    :cond_0
    iget v0, p0, Lapn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 17607
    const/4 v0, 0x2

    iget v1, p0, Lapn;->akk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 17609
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 17610
    return-void
.end method

.method public final cW(I)Lapn;
    .locals 1

    .prologue
    .line 17574
    iput p1, p0, Lapn;->akk:I

    .line 17575
    iget v0, p0, Lapn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapn;->aez:I

    .line 17576
    return-object p0
.end method

.method public final dp(Ljava/lang/String;)Lapn;
    .locals 1

    .prologue
    .line 17552
    if-nez p1, :cond_0

    .line 17553
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17555
    :cond_0
    iput-object p1, p0, Lapn;->aiC:Ljava/lang/String;

    .line 17556
    iget v0, p0, Lapn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapn;->aez:I

    .line 17557
    return-object p0
.end method

.method public final getIconResId()I
    .locals 1

    .prologue
    .line 17571
    iget v0, p0, Lapn;->akk:I

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17549
    iget-object v0, p0, Lapn;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 17614
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 17615
    iget v1, p0, Lapn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 17616
    const/4 v1, 0x1

    iget-object v2, p0, Lapn;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17619
    :cond_0
    iget v1, p0, Lapn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 17620
    const/4 v1, 0x2

    iget v2, p0, Lapn;->akk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 17623
    :cond_1
    return v0
.end method
