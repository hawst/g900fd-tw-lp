.class public final Liwh;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dLP:[Liwh;


# instance fields
.field private aez:I

.field public dLQ:Liwg;

.field private dLR:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 335
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 336
    iput v0, p0, Liwh;->aez:I

    iput-object v1, p0, Liwh;->dLQ:Liwg;

    iput-boolean v0, p0, Liwh;->dLR:Z

    iput-object v1, p0, Liwh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwh;->eCz:I

    .line 337
    return-void
.end method

.method public static aZk()[Liwh;
    .locals 2

    .prologue
    .line 300
    sget-object v0, Liwh;->dLP:[Liwh;

    if-nez v0, :cond_1

    .line 301
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_0
    sget-object v0, Liwh;->dLP:[Liwh;

    if-nez v0, :cond_0

    .line 304
    const/4 v0, 0x0

    new-array v0, v0, [Liwh;

    sput-object v0, Liwh;->dLP:[Liwh;

    .line 306
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    :cond_1
    sget-object v0, Liwh;->dLP:[Liwh;

    return-object v0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 294
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Liwh;->dLQ:Liwg;

    if-nez v0, :cond_1

    new-instance v0, Liwg;

    invoke-direct {v0}, Liwg;-><init>()V

    iput-object v0, p0, Liwh;->dLQ:Liwg;

    :cond_1
    iget-object v0, p0, Liwh;->dLQ:Liwg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Liwh;->dLR:Z

    iget v0, p0, Liwh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwh;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Liwh;->dLQ:Liwg;

    if-eqz v0, :cond_0

    .line 352
    const/4 v0, 0x1

    iget-object v1, p0, Liwh;->dLQ:Liwg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 354
    :cond_0
    iget v0, p0, Liwh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 355
    const/4 v0, 0x2

    iget-boolean v1, p0, Liwh;->dLR:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 357
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 358
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 362
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 363
    iget-object v1, p0, Liwh;->dLQ:Liwg;

    if-eqz v1, :cond_0

    .line 364
    const/4 v1, 0x1

    iget-object v2, p0, Liwh;->dLQ:Liwg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_0
    iget v1, p0, Liwh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 368
    const/4 v1, 0x2

    iget-boolean v2, p0, Liwh;->dLR:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 371
    :cond_1
    return v0
.end method
