.class public final Ljmk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private alA:Ljava/lang/String;

.field private esY:I

.field public esZ:Ljkg;

.field private etT:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 6036
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6037
    iput v1, p0, Ljmk;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmk;->afh:Ljava/lang/String;

    iput v1, p0, Ljmk;->esY:I

    const-string v0, ""

    iput-object v0, p0, Ljmk;->alA:Ljava/lang/String;

    iput-object v2, p0, Ljmk;->esZ:Ljkg;

    const-string v0, ""

    iput-object v0, p0, Ljmk;->etT:Ljava/lang/String;

    iput-object v2, p0, Ljmk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmk;->eCz:I

    .line 6038
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5929
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmk;->afh:Ljava/lang/String;

    iget v0, p0, Ljmk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmk;->esY:I

    iget v0, p0, Ljmk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmk;->alA:Ljava/lang/String;

    iget v0, p0, Ljmk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmk;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljmk;->esZ:Ljkg;

    if-nez v0, :cond_1

    new-instance v0, Ljkg;

    invoke-direct {v0}, Ljkg;-><init>()V

    iput-object v0, p0, Ljmk;->esZ:Ljkg;

    :cond_1
    iget-object v0, p0, Ljmk;->esZ:Ljkg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmk;->etT:Ljava/lang/String;

    iget v0, p0, Ljmk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljmk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6055
    iget v0, p0, Ljmk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6056
    const/4 v0, 0x1

    iget-object v1, p0, Ljmk;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6058
    :cond_0
    iget v0, p0, Ljmk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6059
    const/4 v0, 0x2

    iget v1, p0, Ljmk;->esY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6061
    :cond_1
    iget v0, p0, Ljmk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6062
    const/4 v0, 0x3

    iget-object v1, p0, Ljmk;->alA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6064
    :cond_2
    iget-object v0, p0, Ljmk;->esZ:Ljkg;

    if-eqz v0, :cond_3

    .line 6065
    const/4 v0, 0x4

    iget-object v1, p0, Ljmk;->esZ:Ljkg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 6067
    :cond_3
    iget v0, p0, Ljmk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 6068
    const/4 v0, 0x6

    iget-object v1, p0, Ljmk;->etT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6070
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6071
    return-void
.end method

.method public final bpW()I
    .locals 1

    .prologue
    .line 5973
    iget v0, p0, Ljmk;->esY:I

    return v0
.end method

.method public final bpX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6017
    iget-object v0, p0, Ljmk;->etT:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5951
    iget-object v0, p0, Ljmk;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6075
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6076
    iget v1, p0, Ljmk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6077
    const/4 v1, 0x1

    iget-object v2, p0, Ljmk;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6080
    :cond_0
    iget v1, p0, Ljmk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6081
    const/4 v1, 0x2

    iget v2, p0, Ljmk;->esY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6084
    :cond_1
    iget v1, p0, Ljmk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6085
    const/4 v1, 0x3

    iget-object v2, p0, Ljmk;->alA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6088
    :cond_2
    iget-object v1, p0, Ljmk;->esZ:Ljkg;

    if-eqz v1, :cond_3

    .line 6089
    const/4 v1, 0x4

    iget-object v2, p0, Ljmk;->esZ:Ljkg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6092
    :cond_3
    iget v1, p0, Ljmk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 6093
    const/4 v1, 0x6

    iget-object v2, p0, Ljmk;->etT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6096
    :cond_4
    return v0
.end method

.method public final te()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5992
    iget-object v0, p0, Ljmk;->alA:Ljava/lang/String;

    return-object v0
.end method
