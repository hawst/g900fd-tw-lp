.class public final Lfqf;
.super Lfv;
.source "PG"


# instance fields
.field private final czu:Z

.field private czv:Ljava/util/ArrayList;

.field private czw:Ljava/util/ArrayList;

.field public czx:Ljava/util/ArrayList;

.field private czy:Ljava/util/Map;

.field private final mEntry:Lizj;

.field private final mQuestionViewListener:Lfqc;

.field private final mViewCreator:Lfqh;


# direct methods
.method public constructor <init>(Lfqh;Lizj;Lfqc;Z)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lfv;-><init>()V

    .line 62
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lfqf;->czy:Ljava/util/Map;

    .line 66
    iput-object p1, p0, Lfqf;->mViewCreator:Lfqh;

    .line 67
    iput-object p2, p0, Lfqf;->mEntry:Lizj;

    .line 68
    iput-object p3, p0, Lfqf;->mQuestionViewListener:Lfqc;

    .line 69
    iput-boolean p4, p0, Lfqf;->czu:Z

    .line 70
    return-void
.end method

.method private a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Ljdf;Ljava/util/Set;)V
    .locals 3
    .param p2    # Ljdf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 170
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCz()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 171
    invoke-virtual {v0, p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->c(Ljdf;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 173
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v2

    invoke-direct {p0, v0, v2, p3}, Lfqf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Ljdf;Ljava/util/Set;)V

    goto :goto_0

    .line 177
    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 8
    .param p2    # Ljava/util/ArrayList;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-static {p1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfqf;->czv:Ljava/util/ArrayList;

    .line 89
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->x(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 91
    if-nez p2, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v0, p0, Lfqf;->czw:Ljava/util/ArrayList;

    .line 93
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    .line 94
    iget-object v0, p0, Lfqf;->czy:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 96
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 97
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v5

    .line 98
    iget-object v3, p0, Lfqf;->mViewCreator:Lfqh;

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v3

    invoke-static {v3}, Lfqh;->jj(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    if-nez p2, :cond_5

    .line 104
    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCi()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_1
    const/4 v3, 0x1

    .line 105
    :goto_2
    if-eqz v3, :cond_2

    .line 106
    iget-object v6, p0, Lfqf;->czw:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v5

    invoke-direct {v7, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_2
    :goto_3
    new-instance v5, Lfqg;

    invoke-direct {v5, v0, v1, v3}, Lfqg;-><init>(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;IZ)V

    .line 113
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 114
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 120
    goto :goto_1

    :cond_3
    move-object v0, p2

    .line 91
    goto :goto_0

    :cond_4
    move v3, v2

    .line 104
    goto :goto_2

    .line 109
    :cond_5
    new-instance v3, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_3

    .line 116
    :cond_6
    iget-object v3, p0, Lfqf;->czy:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 121
    :cond_7
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 122
    iget-boolean v0, p0, Lfqf;->czu:Z

    if-eqz v0, :cond_8

    .line 123
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 125
    :cond_8
    iget-object v0, p0, Lfv;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 126
    return-void
.end method


# virtual methods
.method public final V()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lfqf;->czv:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 285
    :goto_1
    return-object v0

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 281
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 283
    const-string v1, "questions-state"

    iget-object v2, p0, Lfqf;->czv:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 284
    const-string v1, "answered-question-keys-state"

    iget-object v2, p0, Lfqf;->czw:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 230
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    .line 231
    iget-object v1, p0, Lfqf;->mViewCreator:Lfqh;

    iget-object v2, v0, Lfqg;->czz:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    iget-object v3, p0, Lfqf;->mEntry:Lizj;

    iget-object v4, p0, Lfqf;->mQuestionViewListener:Lfqc;

    invoke-virtual {v1, v2, p1, v3, v4}, Lfqh;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Landroid/view/ViewGroup;Lizj;Lfqc;)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lfqg;->mView:Landroid/view/View;

    .line 233
    iget-object v2, v0, Lfqg;->mView:Landroid/view/View;

    const v1, 0x7f110257

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090075

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 234
    :cond_0
    iget-object v1, v0, Lfqg;->mView:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 235
    return-object v0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 2

    .prologue
    .line 267
    if-eqz p1, :cond_0

    .line 268
    check-cast p1, Landroid/os/Bundle;

    .line 270
    const-string v0, "questions-state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 272
    const-string v1, "answered-question-keys-state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 274
    invoke-direct {p0, v0, v1}, Lfqf;->a(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 276
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 248
    check-cast p3, Lfqg;

    .line 249
    iget-object v0, p3, Lfqg;->mView:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 250
    const/4 v0, 0x0

    iput-object v0, p3, Lfqg;->mView:Landroid/view/View;

    .line 251
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 255
    check-cast p2, Lfqg;

    iget-object v0, p2, Lfqg;->mView:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ab(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfqf;->a(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 84
    return-void
.end method

.method public final d(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V
    .locals 7

    .prologue
    .line 135
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    invoke-static {v0, p2}, Leqh;->c(Ljsr;Ljsr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    new-instance v1, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    iget-object v0, v0, Lfqg;->czz:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    new-instance v3, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {v1, v3}, Lcom/google/android/sidekick/shared/training/QuestionKey;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 139
    :goto_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 140
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lfqf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Ljdf;Ljava/util/Set;)V

    .line 141
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 142
    invoke-direct {p0, v0, p2, v2}, Lfqf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Ljdf;Ljava/util/Set;)V

    .line 143
    invoke-static {v1, v2}, Liqs;->b(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v3

    .line 144
    invoke-static {v2, v1}, Liqs;->b(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v4

    .line 147
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdf;)V

    .line 149
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 153
    iget-object v5, p0, Lfqf;->czy:Ljava/util/Map;

    iget-object v6, p0, Lfqf;->czx:Ljava/util/ArrayList;

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    iget-object v0, v0, Lfqg;->czz:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    if-ne v0, v1, :cond_5

    :goto_4
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 138
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 153
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    const/4 v2, -0x1

    goto :goto_4

    .line 155
    :cond_7
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 156
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 157
    iget-object v2, p0, Lfqf;->czx:Ljava/util/ArrayList;

    iget-object v3, p0, Lfqf;->czy:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 159
    :cond_8
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 160
    iget-boolean v0, p0, Lfqf;->czu:Z

    if-eqz v0, :cond_9

    .line 161
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 165
    :cond_9
    iget-object v0, p0, Lfv;->mObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    goto/16 :goto_0
.end method

.method public final f(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 260
    check-cast p1, Lfqg;

    .line 261
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 262
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, -0x2

    :cond_0
    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final ji(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    iget-object v0, v0, Lfqg;->czz:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    return-object v0
.end method
