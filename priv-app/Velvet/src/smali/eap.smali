.class public final Leap;
.super Lerd;
.source "PG"


# instance fields
.field private final bSF:I

.field private final bSG:I

.field private final bSH:Lerd;


# direct methods
.method public constructor <init>(IILerd;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lerd;-><init>()V

    .line 40
    iput p1, p0, Leap;->bSF:I

    .line 41
    iput p2, p0, Leap;->bSG:I

    .line 42
    iput-object p3, p0, Leap;->bSH:Lerd;

    .line 43
    return-void
.end method


# virtual methods
.method public final clearCache()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Leap;->bSH:Lerd;

    invoke-virtual {v0, p1}, Lerd;->u(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final synthetic v(Landroid/net/Uri;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 30
    iget-object v0, p0, Leap;->bSH:Lerd;

    invoke-virtual {v0, p1}, Lerd;->v(Landroid/net/Uri;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v4, p0, Leap;->bSF:I

    if-gt v3, v4, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget v4, p0, Leap;->bSG:I

    if-le v3, v4, :cond_0

    :cond_2
    iget v0, p0, Leap;->bSF:I

    int-to-float v0, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    iget v3, p0, Leap;->bSG:I

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v1, v3, v0, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/graphics/Bitmap;->setDensity(I)V

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
