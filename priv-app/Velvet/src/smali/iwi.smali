.class public final Liwi;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dLS:[Liwi;


# instance fields
.field private aez:I

.field private dLT:Ljava/lang/String;

.field private dLU:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 214
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 215
    const/4 v0, 0x0

    iput v0, p0, Liwi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liwi;->dLT:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liwi;->dLU:D

    const/4 v0, 0x0

    iput-object v0, p0, Liwi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwi;->eCz:I

    .line 216
    return-void
.end method

.method public static aZl()[Liwi;
    .locals 2

    .prologue
    .line 160
    sget-object v0, Liwi;->dLS:[Liwi;

    if-nez v0, :cond_1

    .line 161
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    sget-object v0, Liwi;->dLS:[Liwi;

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    new-array v0, v0, [Liwi;

    sput-object v0, Liwi;->dLS:[Liwi;

    .line 166
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_1
    sget-object v0, Liwi;->dLS:[Liwi;

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwi;->dLT:Ljava/lang/String;

    iget v0, p0, Liwi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Liwi;->dLU:D

    iget v0, p0, Liwi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 230
    iget v0, p0, Liwi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 231
    const/4 v0, 0x1

    iget-object v1, p0, Liwi;->dLT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 233
    :cond_0
    iget v0, p0, Liwi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 234
    const/4 v0, 0x2

    iget-wide v2, p0, Liwi;->dLU:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 236
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 237
    return-void
.end method

.method public final aZm()D
    .locals 2

    .prologue
    .line 198
    iget-wide v0, p0, Liwi;->dLU:D

    return-wide v0
.end method

.method public final getFeatureName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Liwi;->dLT:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 241
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 242
    iget v1, p0, Liwi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 243
    const/4 v1, 0x1

    iget-object v2, p0, Liwi;->dLT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_0
    iget v1, p0, Liwi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 247
    const/4 v1, 0x2

    iget-wide v2, p0, Liwi;->dLU:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 250
    :cond_1
    return v0
.end method

.method public final m(D)Liwi;
    .locals 2

    .prologue
    .line 201
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Liwi;->dLU:D

    .line 202
    iget v0, p0, Liwi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwi;->aez:I

    .line 203
    return-object p0
.end method

.method public final pW(Ljava/lang/String;)Liwi;
    .locals 1

    .prologue
    .line 179
    if-nez p1, :cond_0

    .line 180
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 182
    :cond_0
    iput-object p1, p0, Liwi;->dLT:Ljava/lang/String;

    .line 183
    iget v0, p0, Liwi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwi;->aez:I

    .line 184
    return-object p0
.end method
