.class public abstract Lgle;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cOA:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "[\\Q/|*+?=;[]()<>${}\"\\\\E]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgle;->cOA:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static nd(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    const-string v0, "_"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 25
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 28
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1
.end method

.method public static ne(Ljava/lang/String;)D
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 32
    const-string v0, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 34
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x50

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method static nf(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lgle;->cOA:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static ng(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    invoke-static {p0}, Lgle;->nf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-static {v0}, Leqw;->J(Ljava/lang/CharSequence;)Leqw;

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1}, Likr;->a(Ljava/util/Iterator;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 46
    return-object v0
.end method


# virtual methods
.method protected abstract aHc()Ljava/util/Collection;
.end method

.method protected abstract b(Ljava/lang/StringBuilder;)V
.end method

.method protected abstract c(Ljava/lang/StringBuilder;)V
.end method

.method protected abstract d(Ljava/lang/StringBuilder;)V
.end method

.method public final e(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 61
    invoke-virtual {p0}, Lgle;->aHc()Ljava/util/Collection;

    move-result-object v1

    .line 63
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0, p1}, Lgle;->b(Ljava/lang/StringBuilder;)V

    .line 81
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p0, p1}, Lgle;->c(Ljava/lang/StringBuilder;)V

    .line 69
    invoke-virtual {p0, p1}, Lgle;->d(Ljava/lang/StringBuilder;)V

    .line 71
    const/4 v0, 0x1

    .line 72
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglj;

    .line 73
    if-nez v1, :cond_1

    .line 74
    const-string v1, " | "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :cond_1
    const/4 v1, 0x0

    .line 77
    invoke-virtual {v0, p1}, Lglj;->e(Ljava/lang/StringBuilder;)V

    goto :goto_1

    .line 79
    :cond_2
    const-string v0, ";\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
