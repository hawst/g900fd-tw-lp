.class public final Lfqk;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static F(IZ)Ljava/lang/Integer;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    packed-switch p0, :pswitch_data_0

    .line 111
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 101
    :pswitch_1
    if-eqz p1, :cond_0

    .line 102
    const v0, 0x7f04019f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_0
    :pswitch_2
    const v0, 0x7f0401a3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_3
    const v0, 0x7f0401a2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/view/View;Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 165
    const v0, 0x7f0d0139

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 166
    const v1, 0x7f0d0136

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 167
    const v2, 0x7f02002b

    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 168
    invoke-virtual {p0, v1, v0, v1, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 169
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;)V
    .locals 10
    .param p2    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 46
    const v0, 0x7f110257

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    .line 51
    iget-object v2, v1, Ljde;->ebz:Ljdf;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljde;->bhb()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1}, Ljde;->bha()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 54
    const v1, 0x7f0a0450

    invoke-static {v2, v1, v4, v5, v9}, Lesi;->a(Landroid/content/Context;IJZ)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 59
    :goto_0
    const v1, 0x7f1100b0

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 60
    if-nez v2, :cond_2

    .line 61
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 62
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 91
    :cond_0
    :goto_1
    return-void

    .line 57
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCu()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 66
    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lizj;->getType()I

    move-result v0

    const/16 v3, 0x2d

    if-eq v0, v3, :cond_3

    invoke-virtual {p2}, Lizj;->getType()I

    move-result v0

    const/16 v3, 0x2e

    if-ne v0, v3, :cond_4

    .line 69
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0a03b7

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, " "

    const-string v4, "&nbsp;"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 72
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<a href=\"https://support.google.com/chrome/?p=mobile_google_now\">"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</a>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v2, v3, v8

    aput-object v0, v3, v9

    invoke-static {v3}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lgab;->l(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    new-instance v0, Lfql;

    invoke-direct {v0}, Lfql;-><init>()V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 89
    :goto_2
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 87
    :cond_4
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public static aV(II)I
    .locals 2

    .prologue
    .line 180
    if-lez p0, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    add-int/lit8 v0, p1, -0x1

    invoke-static {p0, v0}, Lfqk;->aW(II)I

    move-result v0

    invoke-static {p0, p1}, Lfqk;->aW(II)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 183
    add-int/lit8 p1, p1, -0x1

    .line 185
    :cond_0
    return p1
.end method

.method public static aW(II)I
    .locals 2

    .prologue
    .line 195
    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 196
    div-int v0, p0, p1

    .line 198
    rem-int v1, p0, p1

    if-lez v1, :cond_0

    .line 199
    add-int/lit8 v0, v0, 0x1

    .line 201
    :cond_0
    return v0

    .line 195
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 39
    const v0, 0x7f110257

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 41
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 42
    return-void
.end method

.method public static i(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0152

    invoke-static {v0, p1, v1}, Lgai;->a(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static jk(I)I
    .locals 5

    .prologue
    const v1, 0x7f020106

    const/4 v0, 0x0

    .line 121
    if-nez p0, :cond_0

    .line 151
    :goto_0
    :pswitch_0
    return v0

    .line 125
    :cond_0
    packed-switch p0, :pswitch_data_0

    .line 149
    :pswitch_1
    const-string v1, "TrainingQuestionViewHelper"

    const-string v2, "Missing resource mapping for icon %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 127
    :pswitch_2
    const v0, 0x7f02011c

    goto :goto_0

    .line 129
    :pswitch_3
    const v0, 0x7f020119

    goto :goto_0

    .line 131
    :pswitch_4
    const v0, 0x7f02011a

    goto :goto_0

    .line 133
    :pswitch_5
    const v0, 0x7f02011b

    goto :goto_0

    :pswitch_6
    move v0, v1

    .line 135
    goto :goto_0

    .line 137
    :pswitch_7
    const v0, 0x7f0201bc

    goto :goto_0

    .line 139
    :pswitch_8
    const v0, 0x7f02012c

    goto :goto_0

    .line 141
    :pswitch_9
    const v0, 0x7f0201b2

    goto :goto_0

    :pswitch_a
    move v0, v1

    .line 143
    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_a
    .end packed-switch
.end method
