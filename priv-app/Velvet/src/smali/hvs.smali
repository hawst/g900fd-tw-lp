.class public final Lhvs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static DEBUG:Z


# instance fields
.field private final aXe:Ljava/util/concurrent/Executor;

.field private cq:Z

.field private final drj:Lhve;

.field private dso:Lhvn;

.field private mSessionParams:Lgnj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lhvs;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Lhve;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lhvs;->drj:Lhve;

    .line 45
    iput-object p2, p0, Lhvs;->aXe:Ljava/util/concurrent/Executor;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;Z)Lgnj;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v3, Lgnk;

    invoke-direct {v3}, Lgnk;-><init>()V

    iput-object p1, v3, Lgnk;->cQb:Ljava/lang/String;

    iput-object p2, v3, Lgnk;->cQc:Ljava/util/List;

    iput-boolean p3, v3, Lgnk;->cPu:Z

    iget-object v0, p0, Lhvs;->drj:Lhve;

    invoke-interface {v0}, Lhve;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v3, Lgnk;->cPw:Ljtp;

    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lgnk;->kt(I)Lgnk;

    move-result-object v0

    iput-boolean v2, v0, Lgnk;->cFP:Z

    sget-object v3, Lgjo;->cMK:Lgjo;

    iput-object v3, v0, Lgnk;->cQe:Lgjo;

    iget-object v3, p0, Lhvs;->drj:Lhve;

    invoke-interface {v3}, Lhve;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v3

    if-eqz v3, :cond_2

    iget v3, v3, Landroid/view/inputmethod/EditorInfo;->inputType:I

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    if-nez v3, :cond_2

    :goto_1
    iput-boolean v1, v0, Lgnk;->cQi:Z

    invoke-virtual {v0}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v4, Ljtp;

    invoke-direct {v4}, Ljtp;-><init>()V

    iget-object v5, v0, Landroid/view/inputmethod/EditorInfo;->label:Ljava/lang/CharSequence;

    invoke-static {v5}, Lerr;->aP(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljtp;->yS(Ljava/lang/String;)Ljtp;

    move-result-object v4

    iget-object v5, v0, Landroid/view/inputmethod/EditorInfo;->hintText:Ljava/lang/CharSequence;

    invoke-static {v5}, Lerr;->aP(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljtp;->yT(Ljava/lang/String;)Ljtp;

    move-result-object v4

    iget-object v5, v0, Landroid/view/inputmethod/EditorInfo;->packageName:Ljava/lang/String;

    invoke-static {v5}, Lerr;->aP(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljtp;->yP(Ljava/lang/String;)Ljtp;

    move-result-object v4

    iget v5, v0, Landroid/view/inputmethod/EditorInfo;->fieldId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lerr;->aP(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljtp;->yR(Ljava/lang/String;)Ljtp;

    move-result-object v4

    iget-object v5, v0, Landroid/view/inputmethod/EditorInfo;->fieldName:Ljava/lang/String;

    invoke-static {v5}, Lerr;->aP(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljtp;->yQ(Ljava/lang/String;)Ljtp;

    move-result-object v4

    iget v5, v0, Landroid/view/inputmethod/EditorInfo;->inputType:I

    invoke-virtual {v4, v5}, Ljtp;->sv(I)Ljtp;

    move-result-object v4

    iget v0, v0, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-virtual {v4, v0}, Ljtp;->sw(I)Ljtp;

    move-result-object v4

    iget-object v0, p0, Lhvs;->drj:Lhve;

    invoke-interface {v0}, Lhve;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v5, Landroid/view/inputmethod/ExtractedTextRequest;

    invoke-direct {v5}, Landroid/view/inputmethod/ExtractedTextRequest;-><init>()V

    invoke-interface {v0, v5, v2}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, v0, Landroid/view/inputmethod/ExtractedText;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_1

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Ljtp;->jb(Z)Ljtp;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final a(Lgnj;Lhvn;)V
    .locals 5

    .prologue
    .line 56
    iget-object v0, p0, Lhvs;->dso:Lhvn;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lhvs;->dso:Lhvn;

    invoke-virtual {v0}, Lhvn;->invalidate()V

    .line 60
    :cond_0
    iput-object p2, p0, Lhvs;->dso:Lhvn;

    .line 61
    iput-object p1, p0, Lhvs;->mSessionParams:Lgnj;

    .line 62
    iget-object v0, p0, Lhvs;->drj:Lhve;

    invoke-interface {v0}, Lhve;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Lhvs;->mSessionParams:Lgnj;

    iget-object v2, p0, Lhvs;->dso:Lhvn;

    iget-object v3, p0, Lhvs;->aXe:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvs;->cq:Z

    .line 65
    return-void
.end method

.method public final aSV()Lgnj;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lhvs;->mSessionParams:Lgnj;

    return-object v0
.end method

.method public final aSW()Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lhvs;->dso:Lhvn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvs;->dso:Lhvn;

    invoke-virtual {v0}, Lhvn;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aSX()V
    .locals 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lhvs;->cq:Z

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lhvs;->drj:Lhve;

    invoke-interface {v0}, Lhve;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Lhvs;->mSessionParams:Lgnj;

    invoke-virtual {v1}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lhvs;->dso:Lhvn;

    invoke-virtual {v0}, Lhvn;->invalidate()V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lhvs;->dso:Lhvn;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvs;->cq:Z

    .line 136
    :cond_0
    return-void
.end method

.method public final stopListening()V
    .locals 2

    .prologue
    .line 142
    iget-boolean v0, p0, Lhvs;->cq:Z

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lhvs;->drj:Lhve;

    invoke-interface {v0}, Lhve;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Lhvs;->mSessionParams:Lgnj;

    invoke-virtual {v1}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lgdb;->mr(Ljava/lang/String;)V

    .line 145
    :cond_0
    return-void
.end method
