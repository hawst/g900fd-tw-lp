.class final Leyl;
.super Landroid/database/ContentObserver;
.source "PG"


# instance fields
.field private clR:J

.field private clS:I

.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemp;)V
    .locals 2

    .prologue
    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 136
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leyl;->clR:J

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Leyl;->clS:I

    .line 141
    iput-object p1, p0, Leyl;->mAppContext:Landroid/content/Context;

    .line 142
    iput-object p2, p0, Leyl;->mClock:Lemp;

    .line 143
    return-void
.end method


# virtual methods
.method public final deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    return v0
.end method

.method public final onChange(Z)V
    .locals 6

    .prologue
    .line 157
    iget-object v0, p0, Leyl;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    .line 158
    iget-wide v2, p0, Leyl;->clR:J

    const-wide/16 v4, 0x1770

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 166
    iput-wide v0, p0, Leyl;->clR:J

    .line 167
    const/4 v0, 0x0

    iput v0, p0, Leyl;->clS:I

    .line 170
    iget-object v0, p0, Leyl;->mAppContext:Landroid/content/Context;

    sget-object v1, Lfzy;->cEe:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->h(Landroid/content/Context;Ljava/lang/String;)V

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iget v0, p0, Leyl;->clS:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leyl;->clS:I

    goto :goto_0
.end method
