.class public final Lhfu;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private atJ:Landroid/widget/ProgressBar;

.field private dfK:Lhgj;

.field private dfL:I

.field private dfM:I

.field private dfN:Landroid/widget/TextView;

.field private dfO:Landroid/widget/TextView;

.field private dfP:Ljava/lang/Thread;

.field private dfQ:Z

.field private dfR:Ljava/lang/Runnable;

.field private mAddress:Ljava/lang/String;

.field final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lhfu;->mHandler:Landroid/os/Handler;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhfu;->dfQ:Z

    .line 67
    new-instance v0, Lhfv;

    invoke-direct {v0, p0}, Lhfv;-><init>(Lhfu;)V

    iput-object v0, p0, Lhfu;->dfR:Ljava/lang/Runnable;

    .line 113
    return-void
.end method

.method public static I(Ljava/lang/String;I)Lhfu;
    .locals 4

    .prologue
    .line 119
    new-instance v0, Lhfu;

    invoke-direct {v0}, Lhfu;-><init>()V

    .line 122
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 123
    const-string v2, "key-is-car"

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    const-string v2, "key-bt-address"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v0, v1}, Lhfu;->setArguments(Landroid/os/Bundle;)V

    .line 127
    return-object v0
.end method

.method static synthetic a(Lhfu;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lhfu;->atJ:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic a(Lhfu;Z)Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhfu;->dfQ:Z

    return v0
.end method

.method private aOq()V
    .locals 6

    .prologue
    .line 163
    iget-object v0, p0, Lhfu;->dfO:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Lhfu;->dfK:Lhgj;

    invoke-virtual {v0}, Lhgj;->getCurrentIndex()I

    move-result v0

    .line 167
    invoke-virtual {p0}, Lhfu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 168
    if-eqz v1, :cond_0

    .line 171
    iget-object v2, p0, Lhfu;->dfO:Landroid/widget/TextView;

    const v3, 0x7f0a06ec

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget v5, p0, Lhfu;->dfM:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private aOr()V
    .locals 2

    .prologue
    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhfu;->dfQ:Z

    .line 240
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lhfu;->dfR:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    .line 242
    iget-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 243
    return-void
.end method

.method private aOs()V
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p0}, Lhfu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lhgb;

    .line 265
    if-nez v0, :cond_0

    .line 266
    const-string v0, "AudioDialog"

    const-string v1, "Parent is null"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-virtual {p0}, Lhfu;->dismiss()V

    .line 273
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v1, p0, Lhfu;->mAddress:Ljava/lang/String;

    invoke-interface {v0}, Lhgb;->aNJ()V

    .line 272
    invoke-virtual {p0}, Lhfu;->dismiss()V

    goto :goto_0
.end method

.method static synthetic b(Lhfu;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    :cond_0
    iget-object v0, p0, Lhfu;->dfK:Lhgj;

    invoke-virtual {v0}, Lhgj;->aOx()Landroid/util/Pair;

    move-result-object v0

    invoke-direct {p0}, Lhfu;->aOq()V

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lhfu;->aOs()V

    :cond_1
    invoke-direct {p0}, Lhfu;->aOr()V

    return-void
.end method

.method static synthetic c(Lhfu;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lhfu;->dfQ:Z

    return v0
.end method

.method static synthetic d(Lhfu;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lhfu;->aOs()V

    return-void
.end method

.method static synthetic e(Lhfu;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lhfu;->dfN:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lhfu;)Lhgj;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lhfu;->dfK:Lhgj;

    return-object v0
.end method

.method static synthetic g(Lhfu;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lhfu;->aOr()V

    return-void
.end method

.method static synthetic h(Lhfu;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lhfu;->aOq()V

    return-void
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 147
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJU()Lgpu;

    move-result-object v0

    iget v1, p0, Lhfu;->dfL:I

    invoke-virtual {v0, p1, v1}, Lgpu;->r(Landroid/content/Context;I)Lhgj;

    move-result-object v0

    iput-object v0, p0, Lhfu;->dfK:Lhgj;

    .line 148
    invoke-static {}, Lhgj;->aOw()I

    move-result v0

    iput v0, p0, Lhfu;->dfM:I

    .line 149
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 133
    invoke-virtual {p0}, Lhfu;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 134
    if-nez v0, :cond_0

    .line 141
    :goto_0
    return-void

    .line 138
    :cond_0
    const-string v1, "key-bt-address"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhfu;->mAddress:Ljava/lang/String;

    .line 139
    const-string v1, "key-is-car"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhfu;->dfL:I

    .line 140
    const/4 v0, 0x1

    const v1, 0x1030073

    invoke-virtual {p0, v0, v1}, Lhfu;->setStyle(II)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 178
    const v0, 0x7f040027

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 181
    new-instance v2, Lhfy;

    invoke-direct {v2, p0}, Lhfy;-><init>(Lhfu;)V

    .line 189
    const v0, 0x7f1100b6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 190
    const v0, 0x7f1100b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 194
    const v0, 0x7f1100b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 195
    new-instance v5, Lhfz;

    invoke-direct {v5, p0}, Lhfz;-><init>(Lhfu;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    const v0, 0x7f1100b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhfu;->dfN:Landroid/widget/TextView;

    .line 202
    const v0, 0x7f1100b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhfu;->dfO:Landroid/widget/TextView;

    .line 203
    const v0, 0x7f1100b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lhfu;->atJ:Landroid/widget/ProgressBar;

    .line 204
    iget-object v0, p0, Lhfu;->atJ:Landroid/widget/ProgressBar;

    const/16 v5, 0x46

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 207
    new-instance v0, Lhga;

    invoke-direct {v0, p0, v3, v2, v4}, Lhga;-><init>(Lhfu;Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View;)V

    .line 222
    iget-object v2, p0, Lhfu;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 223
    return-object v1
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lhfu;->dfP:Ljava/lang/Thread;

    .line 232
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 233
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 158
    iget-object v0, p0, Lhfu;->dfK:Lhgj;

    invoke-virtual {v0}, Lhgj;->aOC()V

    .line 159
    return-void
.end method
