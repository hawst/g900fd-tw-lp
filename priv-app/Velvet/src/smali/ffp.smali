.class public final Lffp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfbb;


# static fields
.field static final cqj:Lifw;

.field private static final cqk:Lifw;

.field private static final cql:Lifg;

.field private static final cqm:Lifg;


# instance fields
.field private final ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final cks:Ljava/util/concurrent/CountDownLatch;

.field private volatile cqn:Lamw;

.field private final cqo:Ljava/lang/Object;

.field private final mAppContext:Landroid/content/Context;

.field private final mAsyncServices:Lema;

.field private final mClock:Lemp;

.field private final mFileBytesReader:Lfcj;

.field private final mFileBytesWriter:Lfck;

.field private final mGeofenceHelper:Lffl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lffq;

    invoke-direct {v0}, Lffq;-><init>()V

    .line 127
    sput-object v0, Lffp;->cqj:Lifw;

    invoke-static {v0}, Lifx;->c(Lifw;)Lifw;

    move-result-object v0

    sput-object v0, Lffp;->cqk:Lifw;

    .line 129
    new-instance v0, Lffr;

    invoke-direct {v0}, Lffr;-><init>()V

    sput-object v0, Lffp;->cql:Lifg;

    .line 137
    new-instance v0, Lffs;

    invoke-direct {v0}, Lffs;-><init>()V

    sput-object v0, Lffp;->cqm:Lifg;

    return-void
.end method

.method public constructor <init>(Lfcj;Lfck;Lemp;Landroid/content/Context;Lema;Lffl;)V
    .locals 2

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lffp;->cqo:Ljava/lang/Object;

    .line 162
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lffp;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 163
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lffp;->cks:Ljava/util/concurrent/CountDownLatch;

    .line 168
    iput-object p1, p0, Lffp;->mFileBytesReader:Lfcj;

    .line 169
    iput-object p2, p0, Lffp;->mFileBytesWriter:Lfck;

    .line 170
    iput-object p3, p0, Lffp;->mClock:Lemp;

    .line 171
    iput-object p4, p0, Lffp;->mAppContext:Landroid/content/Context;

    .line 172
    iput-object p5, p0, Lffp;->mAsyncServices:Lema;

    .line 173
    iput-object p6, p0, Lffp;->mGeofenceHelper:Lffl;

    .line 174
    return-void
.end method

.method private static a(Lamw;Ljava/lang/String;)Lamx;
    .locals 12
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1112
    const/16 v0, 0x5f

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1113
    iget-object v6, p0, Lamw;->afD:[Lamx;

    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_2

    aget-object v0, v6, v3

    .line 1114
    iget-object v8, v0, Lamx;->afN:[J

    array-length v9, v8

    move v1, v2

    :goto_1
    if-ge v1, v9, :cond_1

    aget-wide v10, v8, v1

    .line 1115
    cmp-long v10, v10, v4

    if-nez v10, :cond_0

    .line 1120
    :goto_2
    return-object v0

    .line 1114
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1113
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1120
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(Lamw;)V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lamw;->afD:[Lamx;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 238
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    sget-object v2, Lffp;->cql:Lifg;

    invoke-static {v1, v2}, Likr;->a(Ljava/util/Iterator;Lifg;)Ljava/util/Iterator;

    move-result-object v1

    .line 240
    sget-object v2, Lffp;->cqk:Lifw;

    invoke-static {v1, v2}, Likr;->a(Ljava/util/Iterator;Lifw;)Z

    .line 241
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lamw;->afD:[Lamx;

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 242
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lamx;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamx;

    iput-object v0, p0, Lamw;->afD:[Lamx;

    .line 245
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Ljbj;Z)V
    .locals 16

    .prologue
    .line 444
    const/4 v2, 0x0

    invoke-static {v2}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 445
    const/4 v2, 0x0

    invoke-static {v2}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v7

    .line 446
    move-object/from16 v0, p0

    iget-object v8, v0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v8

    .line 447
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lffp;->cqn:Lamw;

    new-instance v3, Lamw;

    invoke-direct {v3}, Lamw;-><init>()V

    invoke-static {v2, v3}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v2

    check-cast v2, Lamw;

    .line 449
    new-instance v9, Lffu;

    invoke-direct {v9, v2}, Lffu;-><init>(Lamw;)V

    .line 453
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    sget-object v4, Lffp;->cqm:Lifg;

    invoke-static {v3, v4}, Likr;->a(Ljava/util/Iterator;Lifg;)Ljava/util/Iterator;

    move-result-object v3

    .line 455
    sget-object v4, Lffp;->cqk:Lifw;

    invoke-static {v3, v4}, Likr;->a(Ljava/util/Iterator;Lifw;)Z

    .line 458
    iget-object v3, v2, Lamw;->afD:[Lamx;

    invoke-static {v3}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v10

    .line 460
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 461
    invoke-static/range {p2 .. p2}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v12

    .line 462
    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 463
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lamx;

    .line 464
    iget-object v4, v3, Lamx;->afA:Ljbj;

    invoke-static {v4}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v4

    invoke-virtual {v12, v4}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 465
    const/4 v5, 0x0

    .line 470
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lizj;

    .line 471
    iget-object v14, v3, Lamx;->entry:Lizj;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14}, Lffp;->e(Lizj;Lizj;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 473
    iget-object v5, v3, Lamx;->entry:Lizj;

    .line 475
    iput-object v4, v3, Lamx;->entry:Lizj;

    .line 478
    invoke-static {v3}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v14

    .line 480
    iget-object v15, v3, Lamx;->afN:[J

    array-length v15, v15

    if-eqz v15, :cond_1

    .line 482
    iget-object v15, v3, Lamx;->entry:Lizj;

    iget-object v15, v15, Lizj;->dUr:Ljcg;

    iget-object v5, v5, Lizj;->dUr:Ljcg;

    invoke-virtual {v14, v15, v5}, Lfgd;->a(Ljcg;Ljcg;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 485
    iget-object v5, v3, Lamx;->afN:[J

    invoke-static {v7, v5}, Lffp;->a(Ljava/util/List;[J)V

    .line 486
    sget-object v5, Ljsu;->eCB:[J

    iput-object v5, v3, Lamx;->afN:[J

    .line 487
    :cond_1
    invoke-virtual {v14, v3, v6, v9}, Lfgd;->a(Lamx;Ljava/util/List;Ligi;)V

    :cond_2
    :goto_2
    move-object v5, v4

    .line 493
    goto :goto_1

    .line 495
    :cond_3
    if-nez v5, :cond_4

    .line 497
    if-eqz p3, :cond_0

    invoke-virtual {v3}, Lamx;->nP()Z

    move-result v4

    if-nez v4, :cond_0

    .line 499
    invoke-interface {v11}, Ljava/util/Iterator;->remove()V

    .line 501
    iget-object v3, v3, Lamx;->afN:[J

    invoke-static {v7, v3}, Lffp;->a(Ljava/util/List;[J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 541
    :catchall_0
    move-exception v2

    monitor-exit v8

    throw v2

    .line 505
    :cond_4
    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 510
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lffp;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v12, 0x3e8

    div-long/2addr v4, v12

    .line 511
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lizj;

    .line 513
    new-instance v12, Lamx;

    invoke-direct {v12}, Lamx;-><init>()V

    invoke-virtual {v12, v4, v5}, Lamx;->z(J)Lamx;

    move-result-object v12

    .line 515
    iput-object v3, v12, Lamx;->entry:Lizj;

    .line 516
    move-object/from16 v0, p2

    iput-object v0, v12, Lamx;->afA:Ljbj;

    .line 517
    iget-object v13, v3, Lizj;->dUr:Ljcg;

    invoke-virtual {v13}, Ljcg;->nY()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 518
    iget-object v3, v3, Lizj;->dUr:Ljcg;

    invoke-virtual {v3}, Ljcg;->nX()I

    move-result v3

    invoke-virtual {v12, v3}, Lamx;->cg(I)Lamx;

    .line 520
    :cond_6
    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 522
    invoke-static {v12}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v3

    .line 524
    invoke-static {}, Lfgd;->azD()V

    .line 525
    invoke-virtual {v3, v12, v6, v9}, Lfgd;->a(Lamx;Ljava/util/List;Ligi;)V

    goto :goto_3

    .line 529
    :cond_7
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lamx;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lamx;

    iput-object v3, v2, Lamw;->afD:[Lamx;

    .line 532
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 533
    move-object/from16 v0, p0

    iget-object v3, v0, Lffp;->mGeofenceHelper:Lffl;

    invoke-virtual {v3, v7}, Lffl;->O(Ljava/util/List;)V

    .line 535
    :cond_8
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 536
    move-object/from16 v0, p0

    iget-object v3, v0, Lffp;->mGeofenceHelper:Lffl;

    invoke-virtual {v3, v6}, Lffl;->N(Ljava/util/List;)V

    .line 539
    :cond_9
    move-object/from16 v0, p0

    iput-object v2, v0, Lffp;->cqn:Lamw;

    .line 540
    invoke-direct/range {p0 .. p0}, Lffp;->flush()V

    .line 541
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_a
    move-object v4, v5

    goto/16 :goto_2
.end method

.method private static a(Ljava/util/List;[J)V
    .locals 6

    .prologue
    .line 1124
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-wide v2, p1, v0

    .line 1125
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "notification_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1127
    :cond_0
    return-void
.end method

.method private awE()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 177
    invoke-static {}, Lenu;->auQ()V

    .line 181
    iget-object v2, p0, Lffp;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 182
    invoke-static {}, Lenu;->auQ()V

    new-instance v2, Lamw;

    invoke-direct {v2}, Lamw;-><init>()V

    iget-object v3, p0, Lffp;->mFileBytesReader:Lfcj;

    const-string v4, "notifications_store"

    const/high16 v5, 0x80000

    invoke-virtual {v3, v4, v5}, Lfcj;->B(Ljava/lang/String;I)[B

    move-result-object v3

    if-eqz v3, :cond_1

    :try_start_0
    invoke-static {v2, v3}, Ljsr;->c(Ljsr;[B)Ljsr;

    invoke-static {v2}, Lffp;->a(Lamw;)V

    invoke-virtual {p0, v2}, Lffp;->b(Lamw;)V

    invoke-virtual {p0, v2}, Lffp;->c(Lamw;)V

    iget-object v3, v2, Lamw;->afD:[Lamx;

    array-length v4, v3

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-static {v5}, Lfgd;->a(Lamx;)Lfgd;

    invoke-static {}, Lfgd;->azD()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Lffp;->d(Lamw;)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iput-object v2, p0, Lffp;->cqn:Lamw;

    .line 188
    iget-object v0, p0, Lffp;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :goto_2
    move v0, v1

    .line 200
    :goto_3
    return v0

    .line 182
    :catch_0
    move-exception v0

    const-string v3, "NotificationStore"

    const-string v4, "Error reading notifications from disk"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 191
    :cond_2
    :try_start_1
    iget-object v2, p0, Lffp;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 193
    :catch_1
    move-exception v1

    const-string v1, "NotificationStore"

    const-string v2, "Initialization latch wait interrupted"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3
.end method

.method private e(Lizj;Lizj;)Z
    .locals 10

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1219
    invoke-virtual {p1}, Lizj;->getType()I

    move-result v0

    invoke-virtual {p2}, Lizj;->getType()I

    move-result v4

    if-eq v0, v4, :cond_1

    .line 1317
    :cond_0
    :goto_0
    return v2

    .line 1224
    :cond_1
    invoke-virtual {p1}, Lizj;->bcR()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lizj;->bcR()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1225
    invoke-virtual {p1}, Lizj;->bcQ()J

    move-result-wide v4

    invoke-virtual {p2}, Lizj;->bcQ()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    .line 1231
    :cond_2
    invoke-virtual {p1}, Lizj;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_3
    move v2, v1

    .line 1317
    goto :goto_0

    .line 1236
    :sswitch_0
    iget-object v0, p1, Lizj;->dSc:Ljal;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v3, p2, Lizj;->dSc:Ljal;

    if-eqz v3, :cond_6

    move v3, v1

    :goto_2
    if-ne v0, v3, :cond_0

    .line 1239
    invoke-static {p1}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v4

    .line 1240
    invoke-static {p2}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v5

    .line 1241
    iget-object v0, v4, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    iget-object v3, v5, Ljal;->dWL:Ljak;

    if-eqz v3, :cond_8

    move v3, v1

    :goto_4
    if-ne v0, v3, :cond_0

    .line 1245
    invoke-virtual {v4}, Ljal;->getEventType()I

    move-result v0

    invoke-virtual {v5}, Ljal;->getEventType()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 1248
    invoke-virtual {v4}, Ljal;->bek()J

    move-result-wide v6

    invoke-virtual {v5}, Ljal;->bek()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    .line 1252
    iget-object v0, v4, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_4

    .line 1253
    iget-object v6, v4, Ljal;->dWL:Ljak;

    .line 1254
    iget-object v7, v5, Ljal;->dWL:Ljak;

    .line 1256
    invoke-virtual {v6}, Ljak;->bed()I

    move-result v0

    invoke-virtual {v7}, Ljak;->bed()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 1260
    iget-object v0, v6, Ljak;->clz:Ljcu;

    if-eqz v0, :cond_9

    move v0, v1

    :goto_5
    iget-object v3, v7, Ljak;->clz:Ljcu;

    if-eqz v3, :cond_a

    move v3, v1

    :goto_6
    if-ne v0, v3, :cond_0

    .line 1265
    iget-object v0, p0, Lffp;->mAppContext:Landroid/content/Context;

    invoke-static {v0, v6}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lffp;->mAppContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1275
    :cond_4
    iget-object v0, v4, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-ne v0, v1, :cond_3

    iget-object v0, v5, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-ne v0, v1, :cond_3

    iget-object v0, v4, Ljal;->dMF:[Liyg;

    aget-object v0, v0, v2

    iget-object v0, v0, Liyg;->dPL:Liyh;

    if-eqz v0, :cond_3

    iget-object v0, v5, Ljal;->dMF:[Liyg;

    aget-object v0, v0, v2

    iget-object v0, v0, Liyg;->dPL:Liyh;

    if-eqz v0, :cond_3

    .line 1279
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_b

    move v0, v1

    :goto_7
    iget-object v3, p2, Lizj;->dUr:Ljcg;

    if-eqz v3, :cond_c

    move v3, v1

    :goto_8
    if-ne v0, v3, :cond_0

    .line 1281
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_3

    .line 1282
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    iget-object v1, p2, Lizj;->dUr:Ljcg;

    invoke-static {v1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 1236
    goto/16 :goto_1

    :cond_6
    move v3, v2

    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 1241
    goto/16 :goto_3

    :cond_8
    move v3, v2

    goto/16 :goto_4

    :cond_9
    move v0, v2

    .line 1260
    goto :goto_5

    :cond_a
    move v3, v2

    goto :goto_6

    :cond_b
    move v0, v2

    .line 1279
    goto :goto_7

    :cond_c
    move v3, v2

    goto :goto_8

    .line 1288
    :sswitch_1
    iget-object v0, p1, Lizj;->dSU:Ljdx;

    if-nez v0, :cond_d

    .line 1289
    iget-object v0, p2, Lizj;->dSU:Ljdx;

    if-nez v0, :cond_0

    move v2, v1

    goto/16 :goto_0

    .line 1290
    :cond_d
    iget-object v0, p2, Lizj;->dSU:Ljdx;

    if-eqz v0, :cond_0

    .line 1293
    iget-object v0, p1, Lizj;->dSU:Ljdx;

    .line 1294
    iget-object v3, p2, Lizj;->dSU:Ljdx;

    .line 1295
    invoke-virtual {v0}, Ljdx;->bhV()Z

    move-result v4

    if-nez v4, :cond_e

    .line 1296
    invoke-virtual {v3}, Ljdx;->bhV()Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    goto/16 :goto_0

    .line 1297
    :cond_e
    invoke-virtual {v3}, Ljdx;->bhV()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1300
    invoke-virtual {v0}, Ljdx;->bhU()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljdx;->bhU()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_0

    .line 1303
    :sswitch_2
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_10

    move v0, v1

    :goto_9
    iget-object v4, p2, Lizj;->dUr:Ljcg;

    if-eqz v4, :cond_11

    move v4, v1

    :goto_a
    if-ne v0, v4, :cond_0

    .line 1307
    iget-object v0, p1, Lizj;->dUr:Ljcg;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lizj;->dUr:Ljcg;

    invoke-virtual {v0}, Ljcg;->getType()I

    move-result v0

    .line 1309
    :goto_b
    iget-object v4, p2, Lizj;->dUr:Ljcg;

    if-eqz v4, :cond_f

    iget-object v3, p2, Lizj;->dUr:Ljcg;

    invoke-virtual {v3}, Ljcg;->getType()I

    move-result v3

    .line 1311
    :cond_f
    if-eq v0, v3, :cond_3

    goto/16 :goto_0

    :cond_10
    move v0, v2

    .line 1303
    goto :goto_9

    :cond_11
    move v4, v2

    goto :goto_a

    :cond_12
    move v0, v3

    .line 1307
    goto :goto_b

    .line 1231
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2b -> :sswitch_1
        0x38 -> :sswitch_2
    .end sparse-switch
.end method

.method private flush()V
    .locals 4

    .prologue
    .line 330
    invoke-static {}, Lenu;->auQ()V

    .line 331
    iget-object v0, p0, Lffp;->cqo:Ljava/lang/Object;

    .line 333
    iget-object v0, p0, Lffp;->mFileBytesWriter:Lfck;

    const-string v1, "notifications_store"

    iget-object v2, p0, Lffp;->cqn:Lamw;

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    const/high16 v3, 0x80000

    invoke-virtual {v0, v1, v2, v3}, Lfck;->a(Ljava/lang/String;[BI)Z

    .line 335
    return-void
.end method

.method private k(Lizj;)Lamx;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 584
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v1

    if-nez v1, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-object v0

    .line 586
    :cond_1
    iget-object v1, p0, Lffp;->cqn:Lamw;

    iget-object v1, v1, Lamw;->afD:[Lamx;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 587
    iget-object v1, p0, Lffp;->cqn:Lamw;

    iget-object v3, v1, Lamw;->afD:[Lamx;

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 588
    iget-object v5, v1, Lamx;->entry:Lizj;

    .line 589
    invoke-direct {p0, p1, v5}, Lffp;->e(Lizj;Lizj;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 590
    goto :goto_0

    .line 587
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public final S(Ljava/util/List;)V
    .locals 10

    .prologue
    .line 1046
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1069
    :goto_0
    return-void

    .line 1048
    :cond_0
    iget-object v3, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v3

    .line 1049
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v1, Lamw;

    invoke-direct {v1}, Lamw;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 1050
    const/4 v1, 0x0

    .line 1052
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrh;

    .line 1053
    invoke-interface {v1}, Lbrh;->zK()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lffp;->a(Lamw;Ljava/lang/String;)Lamx;

    move-result-object v5

    .line 1056
    if-nez v5, :cond_1

    .line 1057
    const-string v5, "NotificationStore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Couldn\'t find notification for: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1069
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1059
    :cond_1
    :try_start_1
    iget-object v1, p0, Lffp;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Lamx;->y(J)Lamx;

    .line 1061
    const/4 v1, 0x1

    move v2, v1

    .line 1063
    goto :goto_1

    .line 1065
    :cond_2
    if-eqz v2, :cond_3

    .line 1066
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 1067
    invoke-direct {p0}, Lffp;->flush()V

    .line 1069
    :cond_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final T(Ljava/util/List;)Ljava/util/List;
    .locals 8

    .prologue
    .line 1082
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 1106
    :goto_0
    return-object v0

    .line 1084
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1085
    iget-object v3, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v3

    .line 1086
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v1, Lamw;

    invoke-direct {v1}, Lamw;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 1088
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrh;

    .line 1089
    invoke-interface {v1}, Lbrh;->zK()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lffp;->a(Lamw;Ljava/lang/String;)Lamx;

    move-result-object v5

    .line 1091
    if-nez v5, :cond_2

    .line 1092
    const-string v5, "NotificationStore"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Couldn\'t find notification for: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1105
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1093
    :cond_2
    :try_start_1
    invoke-virtual {v5}, Lamx;->nU()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1095
    invoke-virtual {v5}, Lamx;->nV()Lamx;

    .line 1096
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Lamx;->aM(Z)Lamx;

    .line 1097
    iget-object v1, v5, Lamx;->entry:Lizj;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1101
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1102
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 1103
    invoke-direct {p0}, Lffp;->flush()V

    .line 1105
    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 1106
    goto :goto_0
.end method

.method public final a(JLjbj;)V
    .locals 11

    .prologue
    .line 916
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 944
    :goto_0
    return-void

    .line 918
    :cond_0
    iget-object v3, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v3

    .line 919
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v1, Lamw;

    invoke-direct {v1}, Lamw;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 921
    const/4 v2, 0x0

    .line 923
    iget-object v1, v0, Lamw;->afE:[Lamy;

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 925
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 926
    invoke-static {p3}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v6

    .line 927
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 928
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lamy;

    .line 929
    iget-object v7, v1, Lamy;->afA:Ljbj;

    invoke-static {v7}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v7

    .line 930
    invoke-virtual {v6, v7}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v1}, Lamy;->oa()J

    move-result-wide v8

    cmp-long v1, v8, p1

    if-gtz v1, :cond_3

    .line 933
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 934
    const/4 v1, 0x1

    :goto_2
    move v2, v1

    .line 936
    goto :goto_1

    .line 938
    :cond_1
    if-eqz v2, :cond_2

    .line 939
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lamy;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lamy;

    iput-object v1, v0, Lamw;->afE:[Lamy;

    .line 941
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 942
    invoke-direct {p0}, Lffp;->flush()V

    .line 944
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final a(JLjbj;Z[B)V
    .locals 17
    .param p5    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 740
    invoke-direct/range {p0 .. p0}, Lffp;->awE()Z

    move-result v2

    if-nez v2, :cond_0

    .line 812
    :goto_0
    return-void

    .line 742
    :cond_0
    const/4 v5, 0x0

    .line 743
    const/4 v4, 0x0

    .line 748
    invoke-static/range {p3 .. p3}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v6

    .line 749
    move-object/from16 v0, p0

    iget-object v2, v0, Lffp;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v8, 0x3e8

    div-long v8, v2, v8

    .line 751
    move-object/from16 v0, p0

    iget-object v7, v0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v7

    .line 752
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lffp;->cqn:Lamw;

    new-instance v3, Lamw;

    invoke-direct {v3}, Lamw;-><init>()V

    invoke-static {v2, v3}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v2

    check-cast v2, Lamw;

    .line 754
    iget-object v10, v2, Lamw;->afE:[Lamy;

    array-length v11, v10

    const/4 v3, 0x0

    move/from16 v16, v3

    move v3, v4

    move v4, v5

    move/from16 v5, v16

    :goto_1
    if-ge v5, v11, :cond_4

    aget-object v12, v10, v5

    .line 755
    iget-object v13, v12, Lamy;->afA:Ljbj;

    invoke-static {v13}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v13

    .line 756
    invoke-virtual {v6, v13}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 757
    const/4 v3, 0x1

    .line 759
    if-nez p4, :cond_1

    invoke-virtual {v12}, Lamy;->oa()J

    move-result-wide v14

    cmp-long v13, p1, v14

    if-ltz v13, :cond_1

    invoke-virtual {v12}, Lamy;->oa()J

    move-result-wide v14

    cmp-long v13, v14, v8

    if-gez v13, :cond_2

    .line 769
    :cond_1
    move-wide/from16 v0, p1

    invoke-virtual {v12, v0, v1}, Lamy;->A(J)Lamy;

    .line 770
    if-eqz p5, :cond_3

    .line 771
    move-object/from16 v0, p5

    invoke-virtual {v12, v0}, Lamy;->d([B)Lamy;

    .line 775
    :goto_2
    const/4 v4, 0x1

    .line 754
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 773
    :cond_3
    invoke-virtual {v12}, Lamy;->od()Lamy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 812
    :catchall_0
    move-exception v2

    monitor-exit v7

    throw v2

    .line 790
    :cond_4
    if-nez v3, :cond_6

    .line 791
    :try_start_1
    new-instance v3, Lamy;

    invoke-direct {v3}, Lamy;-><init>()V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lamy;->A(J)Lamy;

    move-result-object v3

    .line 793
    move-object/from16 v0, p3

    iput-object v0, v3, Lamy;->afA:Ljbj;

    .line 794
    if-eqz p5, :cond_5

    .line 795
    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Lamy;->d([B)Lamy;

    .line 797
    :cond_5
    iget-object v4, v2, Lamw;->afE:[Lamy;

    invoke-static {v4, v3}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lamy;

    iput-object v3, v2, Lamw;->afE:[Lamy;

    .line 799
    const/4 v4, 0x1

    .line 808
    :cond_6
    if-eqz v4, :cond_7

    .line 809
    move-object/from16 v0, p0

    iput-object v2, v0, Lffp;->cqn:Lamw;

    .line 810
    invoke-direct/range {p0 .. p0}, Lffp;->flush()V

    .line 812
    :cond_7
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final a(Lffx;)V
    .locals 8

    .prologue
    .line 977
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 992
    :goto_0
    return-void

    .line 979
    :cond_0
    iget-object v2, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v2

    .line 980
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v1, Lamw;

    invoke-direct {v1}, Lamw;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 982
    iget-object v3, v0, Lamw;->afD:[Lamx;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 983
    invoke-static {v5}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v6

    .line 985
    iget-object v7, p0, Lffp;->mClock:Lemp;

    invoke-virtual {v6, v5, p1, v7}, Lfgd;->a(Lamx;Lffx;Lemp;)V

    .line 982
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 988
    :cond_1
    invoke-virtual {p1}, Lffx;->azx()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 989
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 990
    invoke-direct {p0}, Lffp;->flush()V

    .line 992
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lizj;Ljbj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 408
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    sget-object v0, Lffp;->cqk:Lifw;

    iget-object v1, p1, Lizj;->dUr:Ljcg;

    invoke-interface {v0, v1}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415
    const/4 v0, 0x1

    new-array v0, v0, [Lizj;

    aput-object p1, v0, v2

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0, p2, v2}, Lffp;->a(Ljava/util/List;Ljbj;Z)V

    goto :goto_0
.end method

.method public final a(Lizo;Ljbj;)V
    .locals 2

    .prologue
    .line 388
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p1, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_1

    new-instance v0, Lffw;

    invoke-direct {v0}, Lffw;-><init>()V

    invoke-virtual {v0, p1}, Lffw;->h(Lizo;)V

    iget-object v0, v0, Lffw;->cqt:Ljava/util/List;

    .line 397
    :goto_1
    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lffp;->a(Ljava/util/List;Ljbj;Z)V

    goto :goto_0

    .line 396
    :cond_1
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljie;)V
    .locals 0

    .prologue
    .line 1444
    return-void
.end method

.method public final axk()V
    .locals 0

    .prologue
    .line 1429
    return-void
.end method

.method public final azm()Ljava/util/Collection;
    .locals 10

    .prologue
    .line 602
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 620
    :cond_0
    return-object v0

    .line 604
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 605
    iget-object v1, p0, Lffp;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 606
    iget-object v1, p0, Lffp;->cqn:Lamw;

    iget-object v4, v1, Lamw;->afD:[Lamx;

    array-length v5, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 607
    invoke-static {v6}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v7

    .line 609
    invoke-virtual {v7, v6, v2, v3}, Lfgd;->b(Lamx;J)J

    move-result-wide v8

    .line 613
    cmp-long v7, v8, v2

    if-gtz v7, :cond_2

    .line 614
    iget-object v6, v6, Lamx;->entry:Lizj;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 606
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final azn()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 654
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 672
    :goto_0
    return-void

    .line 656
    :cond_0
    iget-object v3, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v3

    .line 657
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v2, Lamw;

    invoke-direct {v2}, Lamw;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 660
    iget-object v4, v0, Lamw;->afD:[Lamx;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 661
    invoke-virtual {v6}, Lamx;->nP()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Lamx;->nR()Z

    move-result v7

    if-nez v7, :cond_1

    .line 663
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Lamx;->aM(Z)Lamx;

    .line 664
    const/4 v1, 0x1

    .line 660
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 668
    :cond_2
    if-eqz v1, :cond_3

    .line 669
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 670
    invoke-direct {p0}, Lffp;->flush()V

    .line 672
    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final azo()Ljava/lang/Long;
    .locals 11

    .prologue
    .line 820
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 829
    :goto_0
    return-object v0

    .line 822
    :cond_0
    const-wide v2, 0x7fffffffffffffffL

    .line 823
    iget-object v0, p0, Lffp;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 824
    iget-object v0, p0, Lffp;->cqn:Lamw;

    iget-object v6, v0, Lamw;->afE:[Lamy;

    array-length v7, v6

    const/4 v0, 0x0

    move v10, v0

    move-wide v0, v2

    move v2, v10

    :goto_1
    if-ge v2, v7, :cond_2

    aget-object v3, v6, v2

    .line 825
    invoke-virtual {v3}, Lamy;->oa()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-ltz v8, :cond_1

    .line 826
    invoke-virtual {v3}, Lamy;->oa()J

    move-result-wide v8

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 824
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 829
    :cond_2
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final azp()Ljava/lang/Long;
    .locals 11

    .prologue
    .line 837
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 849
    :goto_0
    return-object v0

    .line 839
    :cond_0
    const-wide v2, 0x7fffffffffffffffL

    .line 840
    iget-object v0, p0, Lffp;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 841
    iget-object v0, p0, Lffp;->cqn:Lamw;

    iget-object v6, v0, Lamw;->afD:[Lamx;

    array-length v7, v6

    const/4 v0, 0x0

    move v10, v0

    move-wide v0, v2

    move v2, v10

    :goto_1
    if-ge v2, v7, :cond_2

    aget-object v3, v6, v2

    .line 842
    invoke-static {v3}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v8

    .line 844
    invoke-virtual {v8, v3}, Lfgd;->d(Lamx;)J

    move-result-wide v8

    .line 845
    cmp-long v3, v8, v4

    if-lez v3, :cond_1

    .line 846
    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 841
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 849
    :cond_2
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final azq()Ljava/util/Collection;
    .locals 10

    .prologue
    .line 858
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 869
    :cond_0
    return-object v0

    .line 860
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 861
    iget-object v1, p0, Lffp;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 862
    iget-object v1, p0, Lffp;->cqn:Lamw;

    iget-object v4, v1, Lamw;->afD:[Lamx;

    array-length v5, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 863
    invoke-static {v6}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v7

    .line 865
    invoke-virtual {v7, v6}, Lfgd;->d(Lamx;)J

    move-result-wide v8

    cmp-long v7, v8, v2

    if-gtz v7, :cond_2

    .line 866
    iget-object v6, v6, Lamx;->entry:Lizj;

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 862
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final azr()Ljava/util/Collection;
    .locals 8

    .prologue
    .line 877
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 888
    :cond_0
    return-object v0

    .line 879
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 880
    iget-object v1, p0, Lffp;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 881
    iget-object v1, p0, Lffp;->cqn:Lamw;

    iget-object v4, v1, Lamw;->afD:[Lamx;

    array-length v5, v4

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 882
    invoke-static {v6}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v7

    .line 884
    invoke-virtual {v7, v6, v2, v3}, Lfgd;->a(Lamx;J)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 885
    iget-object v6, v6, Lamx;->entry:Lizj;

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 881
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final azs()Ljava/util/List;
    .locals 11
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 895
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 908
    :cond_0
    return-object v0

    .line 897
    :cond_1
    iget-object v0, p0, Lffp;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    .line 898
    const/4 v1, 0x0

    .line 899
    iget-object v0, p0, Lffp;->cqn:Lamw;

    iget-object v4, v0, Lamw;->afE:[Lamy;

    array-length v5, v4

    const/4 v0, 0x0

    move v10, v0

    move-object v0, v1

    move v1, v10

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 900
    invoke-virtual {v6}, Lamy;->oa()J

    move-result-wide v8

    cmp-long v7, v8, v2

    if-gtz v7, :cond_3

    .line 901
    if-nez v0, :cond_2

    .line 902
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 904
    :cond_2
    new-instance v7, Lffv;

    iget-object v8, v6, Lamy;->afA:Ljbj;

    invoke-virtual {v6}, Lamy;->ob()[B

    move-result-object v6

    invoke-direct {v7, v8, v6}, Lffv;-><init>(Ljbj;[B)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 899
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final azt()Ljava/lang/Long;
    .locals 11

    .prologue
    .line 953
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 964
    :goto_0
    return-object v0

    .line 954
    :cond_0
    iget-object v0, p0, Lffp;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v4, v0, v2

    .line 955
    const-wide v2, 0x7fffffffffffffffL

    .line 956
    iget-object v0, p0, Lffp;->cqn:Lamw;

    iget-object v6, v0, Lamw;->afD:[Lamx;

    array-length v7, v6

    const/4 v0, 0x0

    move v10, v0

    move-wide v0, v2

    move v2, v10

    :goto_1
    if-ge v2, v7, :cond_2

    aget-object v3, v6, v2

    .line 957
    invoke-static {v3}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v8

    .line 959
    invoke-virtual {v8, v3, v4, v5}, Lfgd;->b(Lamx;J)J

    move-result-wide v8

    .line 960
    cmp-long v3, v8, v4

    if-lez v3, :cond_1

    .line 961
    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 956
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 964
    :cond_2
    const-wide v2, 0x7fffffffffffffffL

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final azu()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1003
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1035
    :goto_0
    return-void

    .line 1007
    :cond_0
    iget-object v2, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v2

    .line 1008
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v3, Lamw;

    invoke-direct {v3}, Lamw;-><init>()V

    invoke-static {v0, v3}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 1010
    const/4 v3, 0x0

    invoke-static {v3}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 1011
    const/4 v4, 0x0

    invoke-static {v4}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 1012
    new-instance v5, Lffu;

    invoke-direct {v5, v0}, Lffu;-><init>(Lamw;)V

    .line 1013
    iget-object v6, v0, Lamw;->afD:[Lamx;

    array-length v7, v6

    :goto_1
    if-ge v1, v7, :cond_2

    aget-object v8, v6, v1

    .line 1014
    invoke-static {v8}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v9

    .line 1016
    iget-object v10, v8, Lamx;->afN:[J

    array-length v10, v10

    if-eqz v10, :cond_1

    .line 1017
    iget-object v10, v8, Lamx;->afN:[J

    invoke-static {v4, v10}, Lffp;->a(Ljava/util/List;[J)V

    .line 1018
    const/4 v10, 0x0

    new-array v10, v10, [J

    iput-object v10, v8, Lamx;->afN:[J

    .line 1020
    :cond_1
    invoke-virtual {v9, v8, v3, v5}, Lfgd;->a(Lamx;Ljava/util/List;Ligi;)V

    .line 1013
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1023
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1025
    iget-object v1, p0, Lffp;->mGeofenceHelper:Lffl;

    invoke-virtual {v1, v4}, Lffl;->O(Ljava/util/List;)V

    .line 1027
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1028
    iget-object v1, p0, Lffp;->mGeofenceHelper:Lffl;

    invoke-virtual {v1, v3}, Lffl;->N(Ljava/util/List;)V

    .line 1031
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1032
    :cond_5
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 1033
    invoke-direct {p0}, Lffp;->flush()V

    .line 1035
    :cond_6
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final azv()Ljava/util/List;
    .locals 7

    .prologue
    .line 1137
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 1155
    :goto_0
    return-object v0

    .line 1139
    :cond_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    iget-object v0, v0, Lamw;->afD:[Lamx;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 1143
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_0

    .line 1147
    :cond_1
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 1148
    iget-object v0, p0, Lffp;->cqn:Lamw;

    iget-object v2, v0, Lamw;->afD:[Lamx;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 1149
    iget-object v4, v4, Lamx;->afA:Ljbj;

    .line 1150
    invoke-virtual {v4}, Ljbj;->nK()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_2

    .line 1151
    invoke-static {v4}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v5

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1155
    :cond_3
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method public final azw()Lamw;
    .locals 1

    .prologue
    .line 1351
    iget-object v0, p0, Lffp;->cqn:Lamw;

    return-object v0
.end method

.method public final b(Lamw;)V
    .locals 8

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 251
    iget-object v0, p0, Lffp;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 252
    iget-object v0, p1, Lamw;->afD:[Lamx;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 254
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 255
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamx;

    .line 257
    invoke-static {v0}, Lfgd;->a(Lamx;)Lfgd;

    move-result-object v6

    .line 259
    iget-object v7, p1, Lamw;->afE:[Lamy;

    invoke-virtual {v6, v0, v2, v3, v7}, Lfgd;->a(Lamx;J[Lamy;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 261
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 262
    iget-object v0, v0, Lamx;->afN:[J

    invoke-static {v1, v0}, Lffp;->a(Ljava/util/List;[J)V

    goto :goto_0

    .line 265
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p1, Lamw;->afD:[Lamx;

    array-length v2, v2

    if-eq v0, v2, :cond_2

    .line 266
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lamx;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamx;

    iput-object v0, p1, Lamw;->afD:[Lamx;

    .line 269
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 270
    iget-object v0, p0, Lffp;->mGeofenceHelper:Lffl;

    invoke-virtual {v0, v1}, Lffl;->O(Ljava/util/List;)V

    .line 272
    :cond_3
    return-void
.end method

.method public final b(Lizj;Lizj;)V
    .locals 0

    .prologue
    .line 1439
    return-void
.end method

.method public final b(Lizj;Ljava/util/Collection;)V
    .locals 2
    .param p2    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1453
    iget-object v0, p0, Lffp;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    .line 1454
    new-instance v1, Lfft;

    invoke-direct {v1, p0, p1}, Lfft;-><init>(Lffp;Lizj;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 1461
    return-void
.end method

.method public final c(Lamw;)V
    .locals 10

    .prologue
    .line 278
    iget-object v0, p0, Lffp;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 280
    iget-object v0, p1, Lamw;->afE:[Lamy;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 281
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 283
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamy;

    .line 285
    invoke-virtual {v0}, Lamy;->oa()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    cmp-long v0, v6, v2

    if-gez v0, :cond_0

    .line 287
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p1, Lamw;->afE:[Lamy;

    array-length v2, v2

    if-eq v0, v2, :cond_2

    .line 292
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lamy;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamy;

    iput-object v0, p1, Lamw;->afE:[Lamy;

    .line 294
    :cond_2
    return-void
.end method

.method public final clearAll()V
    .locals 3

    .prologue
    .line 1358
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1364
    :goto_0
    return-void

    .line 1360
    :cond_0
    iget-object v1, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v1

    .line 1361
    :try_start_0
    new-instance v0, Lamw;

    invoke-direct {v0}, Lamw;-><init>()V

    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 1362
    iget-object v0, p0, Lffp;->mFileBytesWriter:Lfck;

    const-string v2, "notifications_store"

    invoke-virtual {v0, v2}, Lfck;->lH(Ljava/lang/String;)V

    .line 1363
    iget-object v0, p0, Lffp;->mGeofenceHelper:Lffl;

    invoke-virtual {v0}, Lffl;->axw()V

    .line 1364
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Lamw;)V
    .locals 8

    .prologue
    .line 298
    iget-object v0, p1, Lamw;->afG:[Lamz;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    iget-object v0, p1, Lamw;->afG:[Lamz;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 301
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 303
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lffp;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    sub-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    .line 305
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 306
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamz;

    .line 307
    invoke-virtual {v0}, Lamz;->of()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-gez v0, :cond_2

    .line 309
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 313
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p1, Lamw;->afG:[Lamz;

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 314
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lamz;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamz;

    iput-object v0, p1, Lamw;->afG:[Lamz;

    goto :goto_0
.end method

.method public final e(Lamw;)V
    .locals 2

    .prologue
    .line 1344
    iget-object v0, p0, Lffp;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1345
    iput-object p1, p0, Lffp;->cqn:Lamw;

    .line 1346
    iget-object v0, p0, Lffp;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1347
    return-void
.end method

.method public final g(Lizj;I)V
    .locals 6

    .prologue
    .line 343
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v1, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v1

    .line 347
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    invoke-virtual {p0, v0}, Lffp;->d(Lamw;)V

    .line 350
    new-instance v0, Lamz;

    invoke-direct {v0}, Lamz;-><init>()V

    .line 351
    iput-object p1, v0, Lamz;->entry:Lizj;

    .line 352
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Lamz;->C(J)Lamz;

    .line 353
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lffp;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lamz;->B(J)Lamz;

    .line 356
    iget-object v2, p0, Lffp;->cqn:Lamw;

    iget-object v3, p0, Lffp;->cqn:Lamw;

    iget-object v3, v3, Lamw;->afG:[Lamz;

    invoke-static {v3, v0}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamz;

    iput-object v0, v2, Lamw;->afG:[Lamz;

    .line 358
    invoke-direct {p0}, Lffp;->flush()V

    .line 359
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i(Lizj;)Lizj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 551
    invoke-direct {p0, p1}, Lffp;->k(Lizj;)Lamx;

    move-result-object v0

    .line 552
    if-eqz v0, :cond_0

    .line 553
    iget-object v0, v0, Lamx;->entry:Lizj;

    .line 555
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iG(I)Lizj;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 369
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v1

    if-nez v1, :cond_0

    .line 379
    :goto_0
    return-object v0

    .line 371
    :cond_0
    iget-object v2, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v2

    .line 373
    :try_start_0
    iget-object v1, p0, Lffp;->cqn:Lamw;

    iget-object v3, v1, Lamw;->afG:[Lamz;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 374
    invoke-virtual {v5}, Lamz;->og()J

    move-result-wide v6

    int-to-long v8, p1

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 375
    iget-object v0, v5, Lamz;->entry:Lizj;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 373
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 378
    :cond_2
    monitor-exit v2

    goto :goto_0
.end method

.method public final j(Lizj;)I
    .locals 3

    .prologue
    .line 568
    invoke-direct {p0, p1}, Lffp;->k(Lizj;)Lamx;

    move-result-object v1

    .line 569
    const/4 v0, 0x0

    .line 570
    if-eqz v1, :cond_2

    .line 571
    const/4 v0, 0x1

    .line 572
    invoke-virtual {v1}, Lamx;->nR()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 573
    const/4 v0, 0x3

    .line 575
    :cond_0
    invoke-virtual {v1}, Lamx;->nP()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 576
    or-int/lit8 v0, v0, 0x4

    .line 578
    :cond_1
    invoke-virtual {v1}, Lamx;->nP()Z

    .line 580
    :cond_2
    return v0
.end method

.method public final l(Lizj;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 627
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 646
    :goto_0
    return-void

    .line 629
    :cond_0
    iget-object v4, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v4

    .line 630
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v3, Lamw;

    invoke-direct {v3}, Lamw;-><init>()V

    invoke-static {v0, v3}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 633
    iget-object v5, v0, Lamw;->afD:[Lamx;

    array-length v6, v5

    move v3, v1

    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    .line 634
    iget-object v8, v7, Lamx;->entry:Lizj;

    .line 635
    invoke-direct {p0, p1, v8}, Lffp;->e(Lizj;Lizj;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 637
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lamx;->aM(Z)Lamx;

    move v1, v2

    .line 633
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 642
    :cond_2
    if-eqz v1, :cond_3

    .line 643
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 644
    invoke-direct {p0}, Lffp;->flush()V

    .line 646
    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method public final m(Lizj;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 680
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 700
    :goto_0
    return-void

    .line 682
    :cond_0
    iget-object v3, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v3

    .line 683
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v2, Lamw;

    invoke-direct {v2}, Lamw;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 686
    iget-object v4, v0, Lamw;->afD:[Lamx;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 687
    iget-object v7, v6, Lamx;->entry:Lizj;

    invoke-direct {p0, p1, v7}, Lffp;->e(Lizj;Lizj;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 688
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Lamx;->aM(Z)Lamx;

    .line 693
    const/4 v1, 0x1

    .line 686
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 696
    :cond_2
    if-eqz v1, :cond_3

    .line 697
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 698
    invoke-direct {p0}, Lffp;->flush()V

    .line 700
    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final n(Lizj;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 709
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    :goto_0
    return-void

    .line 711
    :cond_0
    iget-object v4, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v4

    .line 712
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v3, Lamw;

    invoke-direct {v3}, Lamw;-><init>()V

    invoke-static {v0, v3}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 715
    iget-object v5, v0, Lamw;->afD:[Lamx;

    array-length v6, v5

    move v3, v1

    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    .line 716
    iget-object v8, v7, Lamx;->entry:Lizj;

    .line 717
    invoke-direct {p0, p1, v8}, Lffp;->e(Lizj;Lizj;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 719
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lamx;->aN(Z)Lamx;

    move v1, v2

    .line 715
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 724
    :cond_2
    if-eqz v1, :cond_3

    .line 725
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 726
    invoke-direct {p0}, Lffp;->flush()V

    .line 728
    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method public final o(Lizj;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1376
    invoke-direct {p0}, Lffp;->awE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1402
    :goto_0
    return v2

    .line 1378
    :cond_0
    iget-object v3, p0, Lffp;->cqo:Ljava/lang/Object;

    monitor-enter v3

    .line 1379
    :try_start_0
    iget-object v0, p0, Lffp;->cqn:Lamw;

    new-instance v1, Lamw;

    invoke-direct {v1}, Lamw;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamw;

    .line 1382
    iget-object v1, v0, Lamw;->afD:[Lamx;

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1384
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1385
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1386
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lamx;

    .line 1387
    iget-object v1, v1, Lamx;->entry:Lizj;

    .line 1388
    invoke-direct {p0, p1, v1}, Lffp;->e(Lizj;Lizj;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1390
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 1391
    const/4 v1, 0x1

    :goto_2
    move v2, v1

    .line 1393
    goto :goto_1

    .line 1395
    :cond_1
    if-eqz v2, :cond_2

    .line 1396
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lamx;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lamx;

    iput-object v1, v0, Lamw;->afD:[Lamx;

    .line 1398
    iput-object v0, p0, Lffp;->cqn:Lamw;

    .line 1399
    invoke-direct {p0}, Lffp;->flush()V

    .line 1402
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1403
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final onInvalidated()V
    .locals 0

    .prologue
    .line 1424
    return-void
.end method
