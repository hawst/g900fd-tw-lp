.class public final Lfyk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final mClock:Lemp;

.field private final mStringEvaluator:Lgbr;


# direct methods
.method public constructor <init>(Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lfyk;->mClock:Lemp;

    .line 68
    iput-object p2, p0, Lfyk;->mStringEvaluator:Lgbr;

    .line 69
    return-void
.end method

.method private static a(Landroid/content/Context;Ljhk;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 259
    invoke-virtual {p1}, Ljhk;->blO()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {v2}, Lesi;->im(I)I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Ljhk;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 270
    invoke-virtual {p1}, Ljhk;->blO()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static bq(J)Z
    .locals 2

    .prologue
    .line 250
    const-wide/16 v0, 0xf

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static br(J)Z
    .locals 2

    .prologue
    .line 304
    invoke-static {p0, p1}, Lius;->bY(J)I

    move-result v0

    .line 305
    const/16 v1, 0xf

    if-gt v0, v1, :cond_0

    const/16 v1, -0x1e

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Ljhk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p1, Ljhk;->elQ:Ljhj;

    invoke-virtual {v0}, Ljhj;->blM()I

    move-result v0

    invoke-static {p0, v0}, Lfyk;->m(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static m(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    const v0, 0x7f0a028f

    .line 288
    packed-switch p1, :pswitch_data_0

    .line 296
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 290
    :pswitch_0
    const v0, 0x7f0a0290

    .line 291
    goto :goto_0

    .line 293
    :pswitch_1
    const v0, 0x7f0a0291

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;)Lanh;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 74
    const/16 v6, 0x5a

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;I)Lanh;
    .locals 12
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 81
    invoke-static/range {p4 .. p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p3, Lizj;->dUv:Ljhk;

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/16 v2, 0xf

    invoke-virtual {v3, v2}, Lanh;->cm(I)Lanh;

    new-instance v6, Laoy;

    invoke-direct {v6}, Laoy;-><init>()V

    iput-object v6, v3, Lanh;->agM:Laoy;

    iget-object v2, p3, Lizj;->dUv:Ljhk;

    iput-object v2, v6, Laoy;->akw:Ljhk;

    iget-object v2, p3, Lizj;->dUv:Ljhk;

    invoke-virtual {v2}, Ljhk;->blO()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lfyk;->bp(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Lfyk;->bq(J)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v2}, Ljhk;->blO()J

    move-result-wide v8

    invoke-static {v8, v9}, Lesi;->aY(J)Z

    move-result v4

    iget-object v7, v2, Ljhk;->elQ:Ljhj;

    invoke-virtual {v7}, Ljhj;->blM()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_b

    if-eqz v4, :cond_a

    const v4, 0x7f0a029c

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1, v2}, Lfyk;->a(Landroid/content/Context;Ljhk;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-virtual {p1, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v6, v2}, Laoy;->cA(Ljava/lang/String;)Laoy;

    :cond_2
    const/16 v2, 0x5a

    move/from16 v0, p6

    if-eq v0, v2, :cond_3

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Laoy;->cM(I)Laoy;

    :cond_3
    new-instance v2, Lfyl;

    invoke-direct {v2, p0}, Lfyl;-><init>(Lfyk;)V

    invoke-static {v5, v2}, Lilw;->a(Ljava/util/List;Lifg;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_4
    const/4 v2, 0x0

    move-object v4, v2

    :goto_2
    if-eqz v4, :cond_5

    iget-object v2, p3, Lizj;->dUv:Ljhk;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2}, Ljhk;->blO()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    const/4 v2, 0x1

    invoke-virtual {v4, v8, v9, v2}, Lgca;->d(JZ)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_14

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v7, 0x1

    invoke-static {p1, v2, v7}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    :goto_3
    const-string v7, " \u00b7 "

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/CharSequence;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    const/4 v2, 0x1

    invoke-virtual {v4, p1}, Lgca;->bv(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v6, v2}, Laoy;->cB(Ljava/lang/String;)Laoy;

    :cond_5
    iget-object v4, v4, Lgca;->mRoute:Liyg;

    iget-object v2, v4, Liyg;->dPO:[Ljhn;

    array-length v2, v2

    if-lez v2, :cond_6

    invoke-static {p1, p2, v4}, Lfyw;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Liyg;)[Laoz;

    move-result-object v2

    iput-object v2, v6, Laoy;->akz:[Laoz;

    :cond_6
    iget-object v2, p3, Lizj;->dUv:Ljhk;

    invoke-virtual {v2}, Ljhk;->blO()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lfyk;->bp(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Lfyk;->br(J)Z

    move-result v7

    const v2, 0x7f0201aa

    iget-object v8, p3, Lizj;->dUv:Ljhk;

    iget-object v8, v8, Ljhk;->elQ:Ljhj;

    invoke-virtual {v8}, Ljhj;->aEz()I

    move-result v8

    invoke-static {v8}, Lgba;->jQ(I)Lgba;

    move-result-object v8

    if-nez v7, :cond_7

    sget-object v2, Lgba;->cEB:Lgba;

    if-ne v8, v2, :cond_15

    const v2, 0x7f02011b

    :cond_7
    :goto_4
    const/4 v9, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v4, v9}, Lgaz;->a(Ljbp;Liyg;Z)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    new-instance v9, Lfsa;

    const/16 v10, 0xb

    invoke-direct {v9, v10}, Lfsa;-><init>(I)V

    invoke-virtual {v9, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    const/4 v9, 0x0

    invoke-virtual {v2, v4, v9}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v2

    iput-object v2, v3, Lanh;->ahs:Lani;

    :cond_8
    new-instance v2, Lfsa;

    const/16 v4, 0x97

    invoke-direct {v2, v4}, Lfsa;-><init>(I)V

    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Lfsa;->jC(I)Lani;

    move-result-object v2

    iput-object v2, v3, Lanh;->aht:Lani;

    iput-object p3, v3, Lanh;->ahu:Lizj;

    if-eqz v7, :cond_18

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b00b7

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    :goto_5
    invoke-virtual {v3, v2}, Lanh;->cn(I)Lanh;

    sget-object v2, Lgba;->cEB:Lgba;

    if-ne v8, v2, :cond_9

    iget-object v2, p0, Lfyk;->mStringEvaluator:Lgbr;

    move-object/from16 v0, p5

    invoke-static {p1, v0, v5, v2}, Lgbx;->a(Landroid/content/Context;Ljbp;Ljava/lang/Iterable;Lgbr;)Lapj;

    move-result-object v2

    iput-object v2, v6, Laoy;->agX:Lapj;

    :cond_9
    move-object v2, v3

    goto/16 :goto_0

    :cond_a
    const v4, 0x7f0a029e

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1, v2}, Lfyk;->b(Landroid/content/Context;Ljhk;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-virtual {p1, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_b
    if-eqz v4, :cond_c

    const v4, 0x7f0a029b

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1, v2}, Lfyk;->c(Landroid/content/Context;Ljhk;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p1, v2}, Lfyk;->a(Landroid/content/Context;Ljhk;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-virtual {p1, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_c
    const v4, 0x7f0a029d

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p1, v2}, Lfyk;->c(Landroid/content/Context;Ljhk;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p1, v2}, Lfyk;->b(Landroid/content/Context;Ljhk;)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v7, v8

    invoke-virtual {p1, v4, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_d
    invoke-static {v8, v9}, Lfyk;->br(J)Z

    move-result v4

    if-eqz v4, :cond_10

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-lez v4, :cond_f

    invoke-static {v8, v9}, Lius;->bY(J)I

    move-result v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f100012

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v4, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v7, v2, Ljhk;->elQ:Ljhj;

    invoke-virtual {v7}, Ljhj;->blM()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_e

    const v2, 0x7f0a02a0

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {p1, v2, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_e
    const v7, 0x7f0a029f

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p1, v2}, Lfyk;->c(Landroid/content/Context;Ljhk;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x1

    aput-object v4, v8, v2

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_f
    const v2, 0x7f0a02a1

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_10
    const v2, 0x7f0a02a2

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_11
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_12
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgca;

    invoke-virtual {v2}, Lgca;->aDF()Liyh;

    move-result-object v8

    if-eqz v8, :cond_12

    invoke-virtual {v2}, Lgca;->aDF()Liyh;

    move-result-object v8

    invoke-virtual {v8}, Liyh;->sI()Z

    move-result v8

    if-eqz v8, :cond_12

    move-object v4, v2

    goto/16 :goto_2

    :cond_13
    const/4 v2, 0x0

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgca;

    move-object v4, v2

    goto/16 :goto_2

    :cond_14
    const-string v2, ""

    goto/16 :goto_3

    :cond_15
    sget-object v2, Lgba;->cEC:Lgba;

    if-ne v8, v2, :cond_16

    const v2, 0x7f020119

    goto/16 :goto_4

    :cond_16
    sget-object v2, Lgba;->cEA:Lgba;

    if-ne v8, v2, :cond_17

    const v2, 0x7f02011c

    goto/16 :goto_4

    :cond_17
    const v2, 0x7f02011a

    goto/16 :goto_4

    :cond_18
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b00ba

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_5
.end method

.method public final bp(J)J
    .locals 5

    .prologue
    .line 245
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lfyk;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, p1, v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    return-wide v0
.end method
