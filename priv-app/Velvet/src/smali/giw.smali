.class public final Lgiw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cLG:[I


# instance fields
.field private final blj:Ligi;

.field private final cLH:Lgjh;

.field private final cLI:Lgjq;

.field private final mGreco3DataManager:Lgix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lgiw;->cLG:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x3
        0x4
        0x5
        0x7
        0x8
    .end array-data
.end method

.method private constructor <init>(Lgix;Lgjh;Lgjq;Ligi;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lgiw;->mGreco3DataManager:Lgix;

    .line 58
    iput-object p2, p0, Lgiw;->cLH:Lgjh;

    .line 59
    iput-object p3, p0, Lgiw;->cLI:Lgjq;

    .line 60
    iput-object p4, p0, Lgiw;->blj:Ligi;

    .line 61
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)Lgiw;
    .locals 7

    .prologue
    .line 39
    new-instance v6, Lgip;

    invoke-direct {v6, p0}, Lgip;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v2, Lgjq;

    invoke-direct {v2, p1}, Lgjq;-><init>(Landroid/content/SharedPreferences;)V

    .line 42
    new-instance v0, Lgix;

    sget-object v3, Lgiw;->cLG:[I

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lgix;-><init>(Landroid/content/Context;Lgjq;[ILjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 45
    new-instance v1, Lgjh;

    new-instance v3, Lhqt;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Lhqt;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {v1, v0, v2, v3}, Lgjh;-><init>(Lgix;Lgjq;Lgiq;)V

    .line 47
    invoke-virtual {v0, v1}, Lgix;->a(Lgjd;)V

    .line 49
    new-instance v3, Lgiw;

    invoke-direct {v3, v0, v1, v2, v6}, Lgiw;-><init>(Lgix;Lgjh;Lgjq;Ligi;)V

    return-object v3
.end method


# virtual methods
.method public final aGg()Lgjh;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lgiw;->cLH:Lgjh;

    return-object v0
.end method

.method public final aGh()Lgix;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lgiw;->mGreco3DataManager:Lgix;

    return-object v0
.end method

.method public final aGi()Lgjq;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lgiw;->cLI:Lgjq;

    return-object v0
.end method

.method public final aGj()Ligi;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lgiw;->blj:Ligi;

    return-object v0
.end method
