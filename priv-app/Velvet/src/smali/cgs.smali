.class public final Lcgs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livq;


# instance fields
.field private aWr:Livq;


# direct methods
.method private constructor <init>(Livq;)V
    .locals 0

    .prologue
    .line 418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419
    iput-object p1, p0, Lcgs;->aWr:Livq;

    .line 420
    return-void
.end method

.method public static au(Ljava/lang/Object;)Lcgs;
    .locals 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 411
    new-instance v0, Lcgs;

    invoke-static {p0}, Livg;->bC(Ljava/lang/Object;)Livq;

    move-result-object v1

    invoke-direct {v0, v1}, Lcgs;-><init>(Livq;)V

    return-object v0
.end method

.method public static b(Livq;)Lcgs;
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcgs;

    invoke-direct {v0, p0}, Lcgs;-><init>(Livq;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Throwable;)Lcgs;
    .locals 2

    .prologue
    .line 415
    new-instance v0, Lcgs;

    invoke-static {p0}, Livg;->i(Ljava/lang/Throwable;)Livq;

    move-result-object v1

    invoke-direct {v0, v1}, Lcgs;-><init>(Livq;)V

    return-object v0
.end method


# virtual methods
.method public final Fg()Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 454
    invoke-static {}, Lenu;->auQ()V

    .line 456
    :try_start_0
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0}, Livq;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 462
    :goto_0
    return-object v0

    .line 460
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 462
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 461
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final Fh()Z
    .locals 1

    .prologue
    .line 492
    invoke-virtual {p0}, Lcgs;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    :try_start_0
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0}, Livq;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    const/4 v0, 0x1

    .line 501
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 499
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lemy;Lerp;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-static {v0, p1, p2, p3}, Lemz;->a(Livq;Lemy;Lerp;Ljava/lang/String;)V

    .line 511
    return-void
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0, p1, p2}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 529
    return-void
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 467
    invoke-static {}, Lenu;->auQ()V

    .line 469
    :try_start_0
    iget-object v0, p0, Lcgs;->aWr:Livq;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v2, v3, p3}, Livq;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 477
    :goto_0
    return-object v0

    .line 475
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 477
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 476
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0, p1}, Livq;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 433
    invoke-virtual {p0}, Lcgs;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    invoke-static {}, Lenu;->auQ()V

    .line 436
    :cond_0
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0}, Livq;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 443
    invoke-static {}, Lenu;->auQ()V

    .line 444
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0, p1, p2, p3}, Livq;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0}, Livq;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcgs;->aWr:Livq;

    invoke-interface {v0}, Livq;->isDone()Z

    move-result v0

    return v0
.end method
