.class public final enum Lfgb;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cqA:Lfgb;

.field public static final enum cqB:Lfgb;

.field public static final enum cqC:Lfgb;

.field public static final enum cqD:Lfgb;

.field public static final enum cqE:Lfgb;

.field public static final enum cqF:Lfgb;

.field public static final enum cqG:Lfgb;

.field public static final enum cqH:Lfgb;

.field public static final enum cqI:Lfgb;

.field public static final enum cqJ:Lfgb;

.field private static final synthetic cqL:[Lfgb;

.field public static final enum cqx:Lfgb;

.field public static final enum cqy:Lfgb;

.field public static final enum cqz:Lfgb;


# instance fields
.field private final cqK:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x3

    .line 77
    new-instance v0, Lfgb;

    const-string v1, "TRAFFIC_NOTIFICATION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqx:Lfgb;

    .line 78
    new-instance v0, Lfgb;

    const-string v1, "CALENDAR_TIME_TO_LEAVE_NOTIFICATION"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqy:Lfgb;

    .line 79
    new-instance v0, Lfgb;

    const-string v1, "LOW_PRIORITY_NOTIFICATION"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqz:Lfgb;

    .line 80
    new-instance v0, Lfgb;

    const-string v1, "PUBLIC_ALERT_NOTIFICATION"

    invoke-direct {v0, v1, v4, v7}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqA:Lfgb;

    .line 81
    new-instance v0, Lfgb;

    const-string v1, "RESTAURANT_TIME_TO_LEAVE_NOTIFICATION"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqB:Lfgb;

    .line 82
    new-instance v0, Lfgb;

    const-string v1, "EVENT_TIME_TO_LEAVE_NOTIFICATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v5, v2}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqC:Lfgb;

    .line 83
    new-instance v0, Lfgb;

    const-string v1, "FLIGHT_TIME_TO_LEAVE_NOTIFICATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v6, v2}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqD:Lfgb;

    .line 84
    new-instance v0, Lfgb;

    const-string v1, "REMINDER_NOTIFICATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v7, v2}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqE:Lfgb;

    .line 85
    new-instance v0, Lfgb;

    const-string v1, "LAST_TRAIN_HOME_NOTIFICATION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v8, v2}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqF:Lfgb;

    .line 86
    new-instance v0, Lfgb;

    const-string v1, "BARCODE_NOTIFICATION"

    const/16 v2, 0x9

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqG:Lfgb;

    .line 87
    new-instance v0, Lfgb;

    const-string v1, "FLIGHT_STATUS_WARNING_NOTIFICATION"

    const/16 v2, 0xa

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqH:Lfgb;

    .line 88
    new-instance v0, Lfgb;

    const-string v1, "GENERIC_ENTRY_NOTIFICATION"

    const/16 v2, 0xb

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqI:Lfgb;

    .line 89
    new-instance v0, Lfgb;

    const-string v1, "TRAFFIC_INCIDENT_NOTIFICATION"

    const/16 v2, 0xc

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lfgb;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfgb;->cqJ:Lfgb;

    .line 76
    const/16 v0, 0xd

    new-array v0, v0, [Lfgb;

    const/4 v1, 0x0

    sget-object v2, Lfgb;->cqx:Lfgb;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lfgb;->cqy:Lfgb;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lfgb;->cqz:Lfgb;

    aput-object v2, v0, v1

    sget-object v1, Lfgb;->cqA:Lfgb;

    aput-object v1, v0, v4

    const/4 v1, 0x4

    sget-object v2, Lfgb;->cqB:Lfgb;

    aput-object v2, v0, v1

    sget-object v1, Lfgb;->cqC:Lfgb;

    aput-object v1, v0, v5

    sget-object v1, Lfgb;->cqD:Lfgb;

    aput-object v1, v0, v6

    sget-object v1, Lfgb;->cqE:Lfgb;

    aput-object v1, v0, v7

    sget-object v1, Lfgb;->cqF:Lfgb;

    aput-object v1, v0, v8

    const/16 v1, 0x9

    sget-object v2, Lfgb;->cqG:Lfgb;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lfgb;->cqH:Lfgb;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lfgb;->cqI:Lfgb;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lfgb;->cqJ:Lfgb;

    aput-object v2, v0, v1

    sput-object v0, Lfgb;->cqL:[Lfgb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 94
    iput p3, p0, Lfgb;->cqK:I

    .line 95
    return-void
.end method

.method public static K(Landroid/content/Intent;)Lfgb;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 109
    const-string v1, "com.google.android.apps.sidekick.FROM_NOTIFICATION"

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 110
    if-gez v3, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 111
    :cond_1
    invoke-static {}, Lfgb;->values()[Lfgb;

    move-result-object v4

    array-length v5, v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v1, v4, v2

    .line 112
    iget v6, v1, Lfgb;->cqK:I

    if-ne v6, v3, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 111
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lfgb;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lfgb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfgb;

    return-object v0
.end method

.method public static values()[Lfgb;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lfgb;->cqL:[Lfgb;

    invoke-virtual {v0}, [Lfgb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfgb;

    return-object v0
.end method


# virtual methods
.method public final J(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 105
    const-string v0, "com.google.android.apps.sidekick.FROM_NOTIFICATION"

    iget v1, p0, Lfgb;->cqK:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    return-void
.end method

.method public final azC()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lfgb;->cqK:I

    return v0
.end method
