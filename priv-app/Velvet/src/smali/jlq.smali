.class public final Ljlq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiK:Ljava/lang/String;

.field private dYT:Ljava/lang/String;

.field private dYU:D

.field private esA:D

.field private esB:D

.field private esC:D

.field private esD:D

.field private esE:Ljava/lang/String;

.field private esF:Ljava/lang/String;

.field private esG:Ljava/lang/String;

.field private esH:Ljava/lang/String;

.field public esI:Ljne;

.field private esJ:Ljlr;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 1828
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1829
    const/4 v0, 0x0

    iput v0, p0, Ljlq;->aez:I

    iput-wide v2, p0, Ljlq;->esA:D

    iput-wide v2, p0, Ljlq;->esB:D

    iput-wide v2, p0, Ljlq;->esC:D

    iput-wide v2, p0, Ljlq;->esD:D

    iput-wide v2, p0, Ljlq;->dYU:D

    const-string v0, ""

    iput-object v0, p0, Ljlq;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlq;->esE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlq;->esF:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlq;->dYT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlq;->esG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlq;->esH:Ljava/lang/String;

    iput-object v1, p0, Ljlq;->esI:Ljne;

    iput-object v1, p0, Ljlq;->esJ:Ljlr;

    iput-object v1, p0, Ljlq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlq;->eCz:I

    .line 1830
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1439
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljlq;->esA:D

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljlq;->esB:D

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlq;->aiK:Ljava/lang/String;

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlq;->esF:Ljava/lang/String;

    iget v0, p0, Ljlq;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlq;->dYT:Ljava/lang/String;

    iget v0, p0, Ljlq;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlq;->esE:Ljava/lang/String;

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlq;->esH:Ljava/lang/String;

    iget v0, p0, Ljlq;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljlq;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljlq;->esC:D

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlq;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljlq;->esD:D

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljlq;->aez:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljlq;->esI:Ljne;

    if-nez v0, :cond_1

    new-instance v0, Ljne;

    invoke-direct {v0}, Ljne;-><init>()V

    iput-object v0, p0, Ljlq;->esI:Ljne;

    :cond_1
    iget-object v0, p0, Ljlq;->esI:Ljne;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlq;->esG:Ljava/lang/String;

    iget v0, p0, Ljlq;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljlq;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljlq;->dYU:D

    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljlq;->aez:I

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ljlq;->esJ:Ljlr;

    if-nez v0, :cond_2

    new-instance v0, Ljlr;

    invoke-direct {v0}, Ljlr;-><init>()V

    iput-object v0, p0, Ljlq;->esJ:Ljlr;

    :cond_2
    iget-object v0, p0, Ljlq;->esJ:Ljlr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x41 -> :sswitch_8
        0x49 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x61 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1855
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1856
    const/4 v0, 0x1

    iget-wide v2, p0, Ljlq;->esA:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1858
    :cond_0
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1859
    const/4 v0, 0x2

    iget-wide v2, p0, Ljlq;->esB:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1861
    :cond_1
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_2

    .line 1862
    const/4 v0, 0x3

    iget-object v1, p0, Ljlq;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1864
    :cond_2
    iget v0, p0, Ljlq;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_3

    .line 1865
    const/4 v0, 0x4

    iget-object v1, p0, Ljlq;->esF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1867
    :cond_3
    iget v0, p0, Ljlq;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_4

    .line 1868
    const/4 v0, 0x5

    iget-object v1, p0, Ljlq;->dYT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1870
    :cond_4
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 1871
    const/4 v0, 0x6

    iget-object v1, p0, Ljlq;->esE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1873
    :cond_5
    iget v0, p0, Ljlq;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_6

    .line 1874
    const/4 v0, 0x7

    iget-object v1, p0, Ljlq;->esH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1876
    :cond_6
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    .line 1877
    const/16 v0, 0x8

    iget-wide v2, p0, Ljlq;->esC:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1879
    :cond_7
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 1880
    const/16 v0, 0x9

    iget-wide v2, p0, Ljlq;->esD:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1882
    :cond_8
    iget-object v0, p0, Ljlq;->esI:Ljne;

    if-eqz v0, :cond_9

    .line 1883
    const/16 v0, 0xa

    iget-object v1, p0, Ljlq;->esI:Ljne;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1885
    :cond_9
    iget v0, p0, Ljlq;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_a

    .line 1886
    const/16 v0, 0xb

    iget-object v1, p0, Ljlq;->esG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1888
    :cond_a
    iget v0, p0, Ljlq;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_b

    .line 1889
    const/16 v0, 0xc

    iget-wide v2, p0, Ljlq;->dYU:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1891
    :cond_b
    iget-object v0, p0, Ljlq;->esJ:Ljlr;

    if-eqz v0, :cond_c

    .line 1892
    const/16 v0, 0xd

    iget-object v1, p0, Ljlq;->esJ:Ljlr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1894
    :cond_c
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1895
    return-void
.end method

.method public final bpq()D
    .locals 2

    .prologue
    .line 1598
    iget-wide v0, p0, Ljlq;->esA:D

    return-wide v0
.end method

.method public final bpr()D
    .locals 2

    .prologue
    .line 1617
    iget-wide v0, p0, Ljlq;->esB:D

    return-wide v0
.end method

.method public final bps()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1715
    iget-object v0, p0, Ljlq;->esE:Ljava/lang/String;

    return-object v0
.end method

.method public final bpt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1803
    iget-object v0, p0, Ljlq;->esH:Ljava/lang/String;

    return-object v0
.end method

.method public final getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1759
    iget-object v0, p0, Ljlq;->dYT:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1693
    iget-object v0, p0, Ljlq;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1899
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1900
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1901
    const/4 v1, 0x1

    iget-wide v2, p0, Ljlq;->esA:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1904
    :cond_0
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1905
    const/4 v1, 0x2

    iget-wide v2, p0, Ljlq;->esB:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1908
    :cond_1
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_2

    .line 1909
    const/4 v1, 0x3

    iget-object v2, p0, Ljlq;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1912
    :cond_2
    iget v1, p0, Ljlq;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_3

    .line 1913
    const/4 v1, 0x4

    iget-object v2, p0, Ljlq;->esF:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1916
    :cond_3
    iget v1, p0, Ljlq;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_4

    .line 1917
    const/4 v1, 0x5

    iget-object v2, p0, Ljlq;->dYT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1920
    :cond_4
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 1921
    const/4 v1, 0x6

    iget-object v2, p0, Ljlq;->esE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1924
    :cond_5
    iget v1, p0, Ljlq;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_6

    .line 1925
    const/4 v1, 0x7

    iget-object v2, p0, Ljlq;->esH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1928
    :cond_6
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 1929
    const/16 v1, 0x8

    iget-wide v2, p0, Ljlq;->esC:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1932
    :cond_7
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    .line 1933
    const/16 v1, 0x9

    iget-wide v2, p0, Ljlq;->esD:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1936
    :cond_8
    iget-object v1, p0, Ljlq;->esI:Ljne;

    if-eqz v1, :cond_9

    .line 1937
    const/16 v1, 0xa

    iget-object v2, p0, Ljlq;->esI:Ljne;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1940
    :cond_9
    iget v1, p0, Ljlq;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_a

    .line 1941
    const/16 v1, 0xb

    iget-object v2, p0, Ljlq;->esG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1944
    :cond_a
    iget v1, p0, Ljlq;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_b

    .line 1945
    const/16 v1, 0xc

    iget-wide v2, p0, Ljlq;->dYU:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1948
    :cond_b
    iget-object v1, p0, Ljlq;->esJ:Ljlr;

    if-eqz v1, :cond_c

    .line 1949
    const/16 v1, 0xd

    iget-object v2, p0, Ljlq;->esJ:Ljlr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1952
    :cond_c
    return v0
.end method

.method public final s(D)Ljlq;
    .locals 2

    .prologue
    .line 1601
    const-wide v0, 0x4042e723a29c779aL    # 37.805775

    iput-wide v0, p0, Ljlq;->esA:D

    .line 1602
    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlq;->aez:I

    .line 1603
    return-object p0
.end method

.method public final t(D)Ljlq;
    .locals 2

    .prologue
    .line 1620
    const-wide v0, -0x3fa1651593e5fb71L    # -122.42055800000001

    iput-wide v0, p0, Ljlq;->esB:D

    .line 1621
    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlq;->aez:I

    .line 1622
    return-object p0
.end method

.method public final wI(Ljava/lang/String;)Ljlq;
    .locals 1

    .prologue
    .line 1696
    if-nez p1, :cond_0

    .line 1697
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1699
    :cond_0
    iput-object p1, p0, Ljlq;->aiK:Ljava/lang/String;

    .line 1700
    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljlq;->aez:I

    .line 1701
    return-object p0
.end method

.method public final wJ(Ljava/lang/String;)Ljlq;
    .locals 1

    .prologue
    .line 1718
    if-nez p1, :cond_0

    .line 1719
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1721
    :cond_0
    iput-object p1, p0, Ljlq;->esE:Ljava/lang/String;

    .line 1722
    iget v0, p0, Ljlq;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljlq;->aez:I

    .line 1723
    return-object p0
.end method

.method public final wK(Ljava/lang/String;)Ljlq;
    .locals 1

    .prologue
    .line 1762
    if-nez p1, :cond_0

    .line 1763
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1765
    :cond_0
    iput-object p1, p0, Ljlq;->dYT:Ljava/lang/String;

    .line 1766
    iget v0, p0, Ljlq;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljlq;->aez:I

    .line 1767
    return-object p0
.end method

.method public final wL(Ljava/lang/String;)Ljlq;
    .locals 1

    .prologue
    .line 1806
    if-nez p1, :cond_0

    .line 1807
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1809
    :cond_0
    iput-object p1, p0, Ljlq;->esH:Ljava/lang/String;

    .line 1810
    iget v0, p0, Ljlq;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljlq;->aez:I

    .line 1811
    return-object p0
.end method
