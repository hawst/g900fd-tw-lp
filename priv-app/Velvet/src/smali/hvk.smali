.class public final Lhvk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhve;


# instance fields
.field private synthetic dsh:Landroid/inputmethodservice/InputMethodService;

.field private synthetic dsi:Landroid/os/PowerManager;

.field private synthetic dsj:Lhhq;


# direct methods
.method public constructor <init>(Landroid/inputmethodservice/InputMethodService;Landroid/os/PowerManager;Lhhq;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    iput-object p2, p0, Lhvk;->dsi:Landroid/os/PowerManager;

    iput-object p3, p0, Lhvk;->dsj:Lhhq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final aOR()Lgdb;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lhvk;->dsj:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    return-object v0
.end method

.method public final aSx()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 207
    iget-object v1, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v1}, Landroid/inputmethodservice/InputMethodService;->getWindow()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget-object v1, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 208
    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->switchToLastInputMethod(Landroid/os/IBinder;)Z

    .line 209
    return-void
.end method

.method public final aSy()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    invoke-static {v0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bQ(Landroid/content/Context;)V

    .line 214
    return-void
.end method

.method public final getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public final getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lhvk;->dsh:Landroid/inputmethodservice/InputMethodService;

    invoke-virtual {v0}, Landroid/inputmethodservice/InputMethodService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final isScreenOn()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lhvk;->dsi:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method
