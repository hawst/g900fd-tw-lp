.class public final Ljdf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ebD:[Ljdf;


# instance fields
.field private aez:I

.field private ebE:Z

.field private ebF:I

.field public ebG:[I

.field public ebH:Ljdg;

.field public ebI:Ljdg;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v0, p0, Ljdf;->aez:I

    iput-boolean v0, p0, Ljdf;->ebE:Z

    iput v0, p0, Ljdf;->ebF:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljdf;->ebG:[I

    iput-object v1, p0, Ljdf;->ebH:Ljdg;

    iput-object v1, p0, Ljdf;->ebI:Ljdg;

    iput-object v1, p0, Ljdf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdf;->eCz:I

    return-void
.end method

.method public static bhg()[Ljdf;
    .locals 2

    sget-object v0, Ljdf;->ebD:[Ljdf;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdf;->ebD:[Ljdf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljdf;

    sput-object v0, Ljdf;->ebD:[Ljdf;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljdf;->ebD:[Ljdf;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljdf;->ebE:Z

    iget v0, p0, Ljdf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdf;->ebF:I

    iget v0, p0, Ljdf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdf;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x20

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdf;->ebG:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljdf;->ebG:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljdf;->ebG:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljdf;->ebG:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljdf;->ebG:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljdf;->ebG:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljdf;->ebG:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljdf;->ebG:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljdf;->ebH:Ljdg;

    if-nez v0, :cond_8

    new-instance v0, Ljdg;

    invoke-direct {v0}, Ljdg;-><init>()V

    iput-object v0, p0, Ljdf;->ebH:Ljdg;

    :cond_8
    iget-object v0, p0, Ljdf;->ebH:Ljdg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ljdf;->ebI:Ljdg;

    if-nez v0, :cond_9

    new-instance v0, Ljdg;

    invoke-direct {v0}, Ljdg;-><init>()V

    iput-object v0, p0, Ljdf;->ebI:Ljdg;

    :cond_9
    iget-object v0, p0, Ljdf;->ebI:Ljdg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget v0, p0, Ljdf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-boolean v1, p0, Ljdf;->ebE:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_0
    iget v0, p0, Ljdf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget v1, p0, Ljdf;->ebF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    iget-object v0, p0, Ljdf;->ebG:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljdf;->ebG:[I

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljdf;->ebG:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    const/4 v1, 0x4

    iget-object v2, p0, Ljdf;->ebG:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljdf;->ebH:Ljdg;

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Ljdf;->ebH:Ljdg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget-object v0, p0, Ljdf;->ebI:Ljdg;

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Ljdf;->ebI:Ljdg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhh()Z
    .locals 1

    iget-boolean v0, p0, Ljdf;->ebE:Z

    return v0
.end method

.method public final bhi()Z
    .locals 1

    iget v0, p0, Ljdf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhj()I
    .locals 1

    iget v0, p0, Ljdf;->ebF:I

    return v0
.end method

.method public final bhk()Z
    .locals 1

    iget v0, p0, Ljdf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hH(Z)Ljdf;
    .locals 1

    iput-boolean p1, p0, Ljdf;->ebE:Z

    iget v0, p0, Ljdf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdf;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Ljdf;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-boolean v3, p0, Ljdf;->ebE:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Ljdf;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Ljdf;->ebF:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Ljdf;->ebG:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljdf;->ebG:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    iget-object v3, p0, Ljdf;->ebG:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Ljdf;->ebG:[I

    aget v3, v3, v1

    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    add-int/2addr v0, v2

    iget-object v1, p0, Ljdf;->ebG:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljdf;->ebH:Ljdg;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljdf;->ebH:Ljdg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Ljdf;->ebI:Ljdg;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljdf;->ebI:Ljdg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    return v0
.end method

.method public final oL(I)Ljdf;
    .locals 1

    iput p1, p0, Ljdf;->ebF:I

    iget v0, p0, Ljdf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdf;->aez:I

    return-object p0
.end method
