.class public final Lamp;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afq:Ljava/lang/String;

.field public afr:Lamq;

.field private afs:Z

.field private aft:I

.field private afu:J

.field private afv:Z

.field public afw:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 729
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 730
    iput v2, p0, Lamp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lamp;->afq:Ljava/lang/String;

    iput-object v3, p0, Lamp;->afr:Lamq;

    iput-boolean v2, p0, Lamp;->afs:Z

    iput v2, p0, Lamp;->aft:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamp;->afu:J

    iput-boolean v2, p0, Lamp;->afv:Z

    iput-object v3, p0, Lamp;->afw:Lizj;

    iput-object v3, p0, Lamp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamp;->eCz:I

    .line 731
    return-void
.end method


# virtual methods
.method public final X(Ljava/lang/String;)Lamp;
    .locals 1

    .prologue
    .line 631
    if-nez p1, :cond_0

    .line 632
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 634
    :cond_0
    iput-object p1, p0, Lamp;->afq:Ljava/lang/String;

    .line 635
    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamp;->aez:I

    .line 636
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 469
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamp;->afq:Ljava/lang/String;

    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamp;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lamp;->afr:Lamq;

    if-nez v0, :cond_1

    new-instance v0, Lamq;

    invoke-direct {v0}, Lamq;-><init>()V

    iput-object v0, p0, Lamp;->afr:Lamq;

    :cond_1
    iget-object v0, p0, Lamp;->afr:Lamq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamp;->afs:Z

    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lamp;->aft:I

    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamp;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamp;->afu:J

    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamp;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamp;->afv:Z

    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamp;->aez:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lamp;->afw:Lizj;

    if-nez v0, :cond_2

    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    iput-object v0, p0, Lamp;->afw:Lizj;

    :cond_2
    iget-object v0, p0, Lamp;->afw:Lizj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 750
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 751
    const/4 v0, 0x1

    iget-object v1, p0, Lamp;->afq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 753
    :cond_0
    iget-object v0, p0, Lamp;->afr:Lamq;

    if-eqz v0, :cond_1

    .line 754
    const/4 v0, 0x2

    iget-object v1, p0, Lamp;->afr:Lamq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 756
    :cond_1
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 757
    const/4 v0, 0x3

    iget-boolean v1, p0, Lamp;->afs:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 759
    :cond_2
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 760
    const/4 v0, 0x4

    iget v1, p0, Lamp;->aft:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 762
    :cond_3
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 763
    const/4 v0, 0x5

    iget-wide v2, p0, Lamp;->afu:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 765
    :cond_4
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 766
    const/4 v0, 0x6

    iget-boolean v1, p0, Lamp;->afv:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 768
    :cond_5
    iget-object v0, p0, Lamp;->afw:Lizj;

    if-eqz v0, :cond_6

    .line 769
    const/4 v0, 0x7

    iget-object v1, p0, Lamp;->afw:Lizj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 771
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 772
    return-void
.end method

.method public final aK(Z)Lamp;
    .locals 1

    .prologue
    .line 656
    iput-boolean p1, p0, Lamp;->afs:Z

    .line 657
    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamp;->aez:I

    .line 658
    return-object p0
.end method

.method public final aL(Z)Lamp;
    .locals 1

    .prologue
    .line 713
    const/4 v0, 0x1

    iput-boolean v0, p0, Lamp;->afv:Z

    .line 714
    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamp;->aez:I

    .line 715
    return-object p0
.end method

.method public final ce(I)Lamp;
    .locals 1

    .prologue
    .line 675
    iput p1, p0, Lamp;->aft:I

    .line 676
    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamp;->aez:I

    .line 677
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 776
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 777
    iget v1, p0, Lamp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 778
    const/4 v1, 0x1

    iget-object v2, p0, Lamp;->afq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_0
    iget-object v1, p0, Lamp;->afr:Lamq;

    if-eqz v1, :cond_1

    .line 782
    const/4 v1, 0x2

    iget-object v2, p0, Lamp;->afr:Lamq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_1
    iget v1, p0, Lamp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 786
    const/4 v1, 0x3

    iget-boolean v2, p0, Lamp;->afs:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 789
    :cond_2
    iget v1, p0, Lamp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 790
    const/4 v1, 0x4

    iget v2, p0, Lamp;->aft:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 793
    :cond_3
    iget v1, p0, Lamp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 794
    const/4 v1, 0x5

    iget-wide v2, p0, Lamp;->afu:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 797
    :cond_4
    iget v1, p0, Lamp;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 798
    const/4 v1, 0x6

    iget-boolean v2, p0, Lamp;->afv:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 801
    :cond_5
    iget-object v1, p0, Lamp;->afw:Lizj;

    if-eqz v1, :cond_6

    .line 802
    const/4 v1, 0x7

    iget-object v2, p0, Lamp;->afw:Lizj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 805
    :cond_6
    return v0
.end method

.method public final nA()Z
    .locals 1

    .prologue
    .line 680
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nB()Lamp;
    .locals 1

    .prologue
    .line 683
    const/4 v0, 0x0

    iput v0, p0, Lamp;->aft:I

    .line 684
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lamp;->aez:I

    .line 685
    return-object p0
.end method

.method public final nC()J
    .locals 2

    .prologue
    .line 691
    iget-wide v0, p0, Lamp;->afu:J

    return-wide v0
.end method

.method public final nD()Z
    .locals 1

    .prologue
    .line 699
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nE()Lamp;
    .locals 2

    .prologue
    .line 702
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamp;->afu:J

    .line 703
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lamp;->aez:I

    .line 704
    return-object p0
.end method

.method public final nF()Z
    .locals 1

    .prologue
    .line 710
    iget-boolean v0, p0, Lamp;->afv:Z

    return v0
.end method

.method public final nG()Z
    .locals 1

    .prologue
    .line 718
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lamp;->afq:Ljava/lang/String;

    return-object v0
.end method

.method public final nv()Z
    .locals 1

    .prologue
    .line 639
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nw()Z
    .locals 1

    .prologue
    .line 653
    iget-boolean v0, p0, Lamp;->afs:Z

    return v0
.end method

.method public final nx()Z
    .locals 1

    .prologue
    .line 661
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ny()Lamp;
    .locals 1

    .prologue
    .line 664
    const/4 v0, 0x0

    iput-boolean v0, p0, Lamp;->afs:Z

    .line 665
    iget v0, p0, Lamp;->aez:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lamp;->aez:I

    .line 666
    return-object p0
.end method

.method public final nz()I
    .locals 1

    .prologue
    .line 672
    iget v0, p0, Lamp;->aft:I

    return v0
.end method

.method public final w(J)Lamp;
    .locals 1

    .prologue
    .line 694
    iput-wide p1, p0, Lamp;->afu:J

    .line 695
    iget v0, p0, Lamp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamp;->aez:I

    .line 696
    return-object p0
.end method
