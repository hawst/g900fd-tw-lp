.class public final Lfxr;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 36
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 38
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 39
    invoke-virtual {v1, v6}, Lang;->aS(Z)Lang;

    .line 40
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 43
    iget-object v3, v0, Lizj;->dTh:Ljiq;

    new-instance v4, Lftn;

    invoke-virtual {v3}, Ljiq;->bnm()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lftn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljiq;->bnq()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lftn;->ceJ:Ljava/lang/String;

    iget-object v5, v3, Ljiq;->enD:Ljcn;

    if-eqz v5, :cond_0

    iget-object v5, v3, Ljiq;->enD:Ljcn;

    invoke-virtual {v5}, Ljcn;->qG()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v3, v3, Ljiq;->enD:Ljcn;

    iput-object v3, v4, Lftn;->coM:Ljcn;

    :cond_0
    invoke-virtual {v4}, Lftn;->aDh()Lanh;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v3, v0, Lizj;->dTh:Ljiq;

    iget-object v3, v3, Ljiq;->enE:Ljcn;

    if-eqz v3, :cond_1

    .line 47
    new-instance v3, Lfrt;

    iget-object v4, v0, Lizj;->dTh:Ljiq;

    iget-object v4, v4, Ljiq;->enE:Ljcn;

    invoke-virtual {v4}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lfrt;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lizj;->dTh:Ljiq;

    invoke-virtual {v4}, Ljiq;->bnn()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfrt;->cBo:Ljava/lang/String;

    invoke-virtual {v3}, Lfrt;->aDh()Lanh;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_1
    iget-object v3, v0, Lizj;->dTh:Ljiq;

    invoke-virtual {v3}, Ljiq;->bnp()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 52
    new-instance v3, Lgat;

    invoke-direct {v3}, Lgat;-><init>()V

    iget-object v4, v0, Lizj;->dTh:Ljiq;

    invoke-virtual {v4}, Ljiq;->bno()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgat;->N(Landroid/net/Uri;)Lgat;

    move-result-object v3

    iget-object v4, v0, Lizj;->dTh:Ljiq;

    invoke-virtual {v4}, Ljiq;->bnm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgat;->mk(Ljava/lang/String;)Lgat;

    move-result-object v3

    invoke-virtual {v3}, Lgat;->aEc()Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lfsa;

    const/16 v5, 0x6b

    invoke-direct {v4, v5}, Lfsa;-><init>(I)V

    const v5, 0x7f02017c

    invoke-virtual {v4, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v4

    invoke-virtual {v3, v6}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v6}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v3

    new-instance v4, Laoj;

    invoke-direct {v4}, Laoj;-><init>()V

    const v5, 0x7f0a042e

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v5, Lanh;

    invoke-direct {v5}, Lanh;-><init>()V

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lanh;->cm(I)Lanh;

    iput-object v0, v5, Lanh;->ahu:Lizj;

    iput-object v4, v5, Lanh;->agz:Laoj;

    iput-object v3, v5, Lanh;->ahs:Lani;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 56
    return-object v1
.end method
