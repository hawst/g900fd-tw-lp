.class public final Lgmc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglz;


# instance fields
.field private final mAuthTokenHelper:Lgln;

.field private final mConnectionFactory:Lccz;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mSettings:Lgdo;


# direct methods
.method public constructor <init>(Lgdo;Lccz;Lgln;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lgmc;->mSettings:Lgdo;

    .line 57
    iput-object p2, p0, Lgmc;->mConnectionFactory:Lccz;

    .line 58
    iput-object p3, p0, Lgmc;->mAuthTokenHelper:Lgln;

    .line 59
    iput-object p4, p0, Lgmc;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/Future;Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 74
    .line 78
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    :try_start_0
    iget-object v0, p0, Lgmc;->mSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v2, v0, Ljze;->eNk:Ljzm;

    .line 86
    new-instance v4, Ljava/net/URL;

    invoke-virtual {v2}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lgmc;->mConnectionFactory:Lccz;

    invoke-interface {v0, v2}, Lccz;->a(Ljzm;)Lbzh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v3

    .line 89
    :try_start_1
    invoke-virtual {v2}, Ljzm;->bxf()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Ljzm;->bxe()I

    move-result v0

    :goto_1
    invoke-interface {v3, v0}, Lbzh;->es(I)V

    .line 91
    invoke-static {v3}, Ldmc;->d(Lbzh;)V

    .line 93
    new-instance v5, Lcej;

    invoke-virtual {v2}, Ljzm;->rA()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v3, v0}, Lcej;-><init>(Lbzh;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v0

    const-string v2, "clientlog"

    invoke-virtual {v0, v2}, Ljwv;->zH(Ljava/lang/String;)Ljwv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 97
    const-wide/16 v6, 0x3e8

    :try_start_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v6, v7, v0}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwy;

    .line 98
    sget-object v6, Ljwy;->eJP:Ljsm;

    invoke-virtual {v2, v6, v0}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 116
    :goto_2
    :try_start_3
    invoke-virtual {v5, v2}, Lcej;->c(Ljwv;)V

    .line 118
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwv;

    .line 119
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v6, v7}, Lcej;->a(Ljwv;ZZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 146
    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 147
    if-eqz v2, :cond_2

    .line 148
    invoke-interface {v2}, Lbzh;->disconnect()V

    :cond_2
    throw v0

    .line 89
    :cond_3
    const/16 v0, 0x400

    goto :goto_1

    .line 122
    :cond_4
    :try_start_4
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljwv;->jl(Z)Ljwv;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v6, 0x1

    invoke-virtual {v5, v0, v2, v6}, Lcej;->a(Ljwv;ZZ)V

    .line 124
    invoke-interface {v3}, Lbzh;->getResponseCode()I

    move-result v0

    .line 126
    const/16 v2, 0xc8

    if-eq v0, v2, :cond_5

    .line 127
    new-instance v2, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Http "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 130
    :cond_5
    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 133
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Redirect to "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_6
    invoke-static {v3}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;

    move-result-object v0

    .line 138
    new-instance v2, Lcem;

    invoke-direct {v2, v0}, Lcem;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 140
    :try_start_5
    invoke-virtual {v2}, Lcem;->Dq()Ljww;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Ljww;->getStatus()I

    move-result v0

    if-eq v0, v8, :cond_7

    .line 143
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Wrong response"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 146
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_4

    :cond_7
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 147
    if-eqz v3, :cond_0

    .line 148
    invoke-interface {v3}, Lbzh;->disconnect()V

    goto/16 :goto_0

    .line 146
    :catchall_2
    move-exception v0

    move-object v2, v1

    goto/16 :goto_4

    :catch_0
    move-exception v0

    goto/16 :goto_2

    .line 114
    :catch_1
    move-exception v0

    goto/16 :goto_2

    :catch_2
    move-exception v0

    goto/16 :goto_2
.end method

.method public final aHl()Ljava/util/concurrent/Future;
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lgmc;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lgmc;->mAuthTokenHelper:Lgln;

    new-instance v2, Ljwy;

    invoke-direct {v2}, Ljwy;-><init>()V

    iget-object v3, p0, Lgmc;->mSettings:Lgdo;

    invoke-static {v1, v2, v3}, Lgmv;->a(Lgln;Ljwy;Lgdo;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 168
    return-object v0
.end method

.method public final am(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvi;

    .line 66
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v3

    new-instance v4, Ljwg;

    invoke-direct {v4}, Ljwg;-><init>()V

    sget-object v5, Ljwg;->eIX:Ljsm;

    invoke-virtual {v3, v5, v4}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    iput-object v0, v4, Ljwg;->eIY:Ljvi;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {p0}, Lgmc;->aHl()Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lgmc;->a(Ljava/util/concurrent/Future;Ljava/util/ArrayList;)V

    .line 69
    return-void
.end method
