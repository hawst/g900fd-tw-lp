.class public final Lbwq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field public aMC:Ljava/util/concurrent/ScheduledExecutorService;

.field private aMD:Leqo;

.field private aME:Z

.field private aMF:I

.field private mAudioManager:Landroid/media/AudioManager;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 52
    iget-object v1, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lbwq;->aMC:Ljava/util/concurrent/ScheduledExecutorService;

    .line 53
    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lbwq;->aMD:Leqo;

    .line 54
    iput-object p1, p0, Lbwq;->mContext:Landroid/content/Context;

    .line 55
    iget-object v0, p0, Lbwq;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lbwq;->mAudioManager:Landroid/media/AudioManager;

    .line 56
    return-void
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 92
    const/16 v0, 0x8

    return v0
.end method

.method final a(Lbwt;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lbwq;->aMD:Leqo;

    new-instance v1, Lbws;

    const-string v2, "Send announcement"

    invoke-direct {v1, p0, v2, p1, p2}, Lbws;-><init>(Lbwq;Ljava/lang/String;Lbwt;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 123
    return-void
.end method

.method public final a(Lddb;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    .line 99
    iget-boolean v1, p0, Lbwq;->aME:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ldcw;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lbwq;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    iput v1, p0, Lbwq;->aMF:I

    .line 102
    iget-object v1, p0, Lbwq;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 103
    const/4 v1, 0x1

    iput-boolean v1, p0, Lbwq;->aME:Z

    .line 104
    :cond_0
    iget-boolean v1, p0, Lbwq;->aME:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldcw;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lbwq;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Lbwq;->aMF:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 107
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    .line 108
    iput-boolean v2, p0, Lbwq;->aME:Z

    .line 110
    :cond_1
    return-void
.end method
