.class abstract Lfgd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cqM:Lfgd;

.field private static final cqN:Lfgd;

.field private static final cqO:Lfgd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lfge;

    invoke-direct {v0}, Lfge;-><init>()V

    sput-object v0, Lfgd;->cqM:Lfgd;

    .line 49
    new-instance v0, Lfgf;

    invoke-direct {v0}, Lfgf;-><init>()V

    sput-object v0, Lfgd;->cqN:Lfgd;

    .line 51
    new-instance v0, Lfgg;

    invoke-direct {v0}, Lfgg;-><init>()V

    sput-object v0, Lfgd;->cqO:Lfgd;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    return-void
.end method

.method static a(Lamx;)Lfgd;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lamx;->entry:Lizj;

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-static {v0}, Lfgd;->a(Ljcg;)Lfgd;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljcg;)Lfgd;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 70
    iget-object v1, p0, Ljcg;->dRY:Ljie;

    if-nez v1, :cond_1

    .line 71
    sget-object v0, Lfgd;->cqM:Lfgd;

    .line 108
    :cond_0
    :goto_0
    return-object v0

    .line 73
    :cond_1
    iget-object v1, p0, Ljcg;->dRY:Ljie;

    .line 78
    iget-object v2, v1, Ljie;->end:[I

    array-length v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_0

    .line 81
    iget-object v2, v1, Ljie;->end:[I

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 85
    iget-object v2, p0, Ljcg;->dRY:Ljie;

    iget-object v2, v2, Ljie;->end:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    .line 86
    const/4 v3, 0x7

    if-ne v2, v3, :cond_2

    .line 88
    iget-object v1, v1, Ljie;->eaW:[Ljbp;

    array-length v1, v1

    if-eqz v1, :cond_0

    .line 95
    sget-object v0, Lfgd;->cqN:Lfgd;

    goto :goto_0

    .line 96
    :cond_2
    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 98
    invoke-virtual {v1}, Ljie;->bkv()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    sget-object v0, Lfgd;->cqO:Lfgd;

    goto :goto_0
.end method

.method protected static a(Lamx;JLjava/lang/Long;)Z
    .locals 7
    .param p3    # Ljava/lang/Long;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 266
    if-nez p3, :cond_0

    invoke-virtual {p0}, Lamx;->nY()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 281
    :goto_0
    return v0

    .line 269
    :cond_0
    invoke-virtual {p0}, Lamx;->nY()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lamx;->nX()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v0, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    .line 274
    :goto_1
    invoke-virtual {p0}, Lamx;->nW()J

    move-result-wide v4

    sub-long v4, p1, v4

    .line 275
    cmp-long v0, v4, v0

    if-lez v0, :cond_2

    .line 279
    const/4 v0, 0x1

    goto :goto_0

    .line 269
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 281
    goto :goto_0
.end method

.method static azD()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method private static b(Ljcg;)J
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Ljcg;->bdm()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Ljcg;->bdl()J

    move-result-wide v0

    .line 140
    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private static c(Lamx;)Z
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lamx;->nP()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lamx;->nR()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Lamx;Lffx;Lemp;)V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method a(Lamx;Ljava/util/List;Ligi;)V
    .locals 0

    .prologue
    .line 251
    return-void
.end method

.method final a(Lamx;J)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 178
    invoke-static {p1}, Lfgd;->c(Lamx;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 179
    invoke-virtual {p0, p1}, Lfgd;->d(Lamx;)J

    move-result-wide v2

    cmp-long v1, v2, p2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 181
    :cond_0
    return v0
.end method

.method abstract a(Lamx;J[Lamy;)Z
.end method

.method a(Ljcg;Ljcg;)Z
    .locals 2

    .prologue
    .line 245
    invoke-static {p1}, Lfgd;->a(Ljcg;)Lfgd;

    move-result-object v0

    invoke-static {p2}, Lfgd;->a(Ljcg;)Lfgd;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(Lamx;)J
.end method

.method final b(Lamx;J)J
    .locals 4

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 215
    invoke-virtual {p1}, Lamx;->nP()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lamx;->nR()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_2

    .line 227
    :cond_0
    :goto_1
    return-wide v0

    .line 215
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 220
    :cond_2
    iget-object v2, p1, Lamx;->entry:Lizj;

    iget-object v2, v2, Lizj;->dUr:Ljcg;

    .line 221
    invoke-static {v2}, Lfgd;->b(Ljcg;)J

    move-result-wide v2

    .line 222
    cmp-long v2, v2, p2

    if-ltz v2, :cond_0

    .line 227
    invoke-virtual {p0, p1}, Lfgd;->b(Lamx;)J

    move-result-wide v0

    goto :goto_1
.end method

.method protected final c(Lamx;J)Z
    .locals 2

    .prologue
    .line 287
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, Lfgd;->a(Lamx;JLjava/lang/Long;)Z

    move-result v0

    return v0
.end method

.method final d(Lamx;)J
    .locals 2

    .prologue
    .line 200
    invoke-static {p1}, Lfgd;->c(Lamx;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    const-wide v0, 0x7fffffffffffffffL

    .line 205
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p1, Lamx;->entry:Lizj;

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-static {v0}, Lfgd;->b(Ljcg;)J

    move-result-wide v0

    goto :goto_0
.end method
