.class public final Lhup;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aMD:Leqo;

.field private final drj:Lhve;

.field private final drk:Ljava/util/List;

.field private final drl:Lhvu;

.field private final drm:Ljava/lang/Runnable;

.field private drn:I

.field private dro:I

.field private drp:Z

.field private final drq:J

.field private final mHypothesisToSuggestionSpansConverter:Lgel;

.field private mRequestId:Ljava/lang/String;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lhve;Lgel;Lhym;Lhvu;Leqo;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lhuq;

    const-string v1, "Commit new text"

    invoke-direct {v0, p0, v1}, Lhuq;-><init>(Lhup;Ljava/lang/String;)V

    iput-object v0, p0, Lhup;->drm:Ljava/lang/Runnable;

    .line 79
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhve;

    iput-object v0, p0, Lhup;->drj:Lhve;

    .line 80
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqo;

    iput-object v0, p0, Lhup;->aMD:Leqo;

    .line 81
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lhup;->drk:Ljava/util/List;

    .line 83
    iput-object p3, p0, Lhup;->mSettings:Lhym;

    .line 85
    iput-object p4, p0, Lhup;->drl:Lhvu;

    .line 87
    iput-object p2, p0, Lhup;->mHypothesisToSuggestionSpansConverter:Lgel;

    .line 88
    iget-object v0, p0, Lhup;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNf:Ljzi;

    invoke-virtual {v0}, Ljzi;->bwU()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lhup;->drq:J

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Lhup;->dro:I

    .line 91
    return-void
.end method

.method private static a(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/ExtractedText;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 323
    if-nez p0, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-object v0

    .line 327
    :cond_1
    new-instance v1, Landroid/view/inputmethod/ExtractedTextRequest;

    invoke-direct {v1}, Landroid/view/inputmethod/ExtractedTextRequest;-><init>()V

    .line 328
    const/4 v2, 0x1

    iput v2, v1, Landroid/view/inputmethod/ExtractedTextRequest;->flags:I

    .line 329
    const/4 v2, 0x0

    invoke-interface {p0, v1, v2}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    move-result-object v1

    .line 330
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    iget-object v2, v1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    instance-of v2, v2, Landroid/text/Spanned;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 333
    goto :goto_0
.end method

.method private varargs a([Lhuu;)V
    .locals 4

    .prologue
    .line 244
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 247
    iget-object v0, p0, Lhup;->drj:Lhve;

    invoke-interface {v0}, Lhve;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 249
    if-nez v1, :cond_0

    .line 265
    :goto_0
    return-void

    .line 253
    :cond_0
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    .line 256
    :try_start_0
    iget-object v0, p0, Lhup;->drl:Lhvu;

    invoke-static {v1}, Lhup;->a(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/ExtractedText;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lhvu;->a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/ExtractedText;)V

    .line 258
    array-length v2, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p1, v0

    .line 259
    if-eqz v3, :cond_1

    .line 260
    invoke-virtual {v3, v1}, Lhuu;->b(Landroid/view/inputmethod/InputConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 264
    :cond_2
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    throw v0
.end method

.method private declared-synchronized aSj()V
    .locals 5

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhup;->drj:Lhve;

    invoke-interface {v0}, Lhve;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 125
    if-nez v1, :cond_0

    .line 152
    :goto_0
    monitor-exit p0

    return-void

    .line 128
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    :try_start_2
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    .line 134
    invoke-static {v1}, Lhup;->a(Landroid/view/inputmethod/InputConnection;)Landroid/view/inputmethod/ExtractedText;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 135
    if-nez v0, :cond_1

    .line 151
    :try_start_3
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 139
    :cond_1
    :try_start_4
    iget v2, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v2, v3

    if-ltz v2, :cond_2

    iget v2, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    if-ge v2, v3, :cond_2

    .line 142
    const-string v2, "DictationResultHandlerImpl"

    const-string v3, "Removing selected text"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget v2, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    iget v4, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    add-int/2addr v3, v4

    invoke-interface {v1, v2, v3}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    .line 146
    const/4 v2, 0x0

    iget v3, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    iget v4, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    sub-int/2addr v3, v4

    invoke-interface {v1, v2, v3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    .line 149
    :cond_2
    iget-object v2, p0, Lhup;->drl:Lhvu;

    invoke-interface {v2, v0}, Lhvu;->a(Landroid/view/inputmethod/ExtractedText;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 151
    :try_start_5
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private aSm()Lhut;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 168
    :goto_0
    iget-object v0, p0, Lhup;->drk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 183
    :cond_0
    :goto_1
    return-object v0

    .line 172
    :cond_1
    iget-object v0, p0, Lhup;->drk:Ljava/util/List;

    iget v2, p0, Lhup;->drn:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhut;

    .line 174
    invoke-virtual {v0}, Lhut;->aSq()Z

    move-result v2

    if-nez v2, :cond_0

    .line 178
    invoke-virtual {v0}, Lhut;->abc()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lhup;->drn:I

    iget-object v2, p0, Lhup;->drk:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 180
    iget v0, p0, Lhup;->drn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhup;->drn:I

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 183
    goto :goto_1
.end method

.method private aSn()Lhut;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 192
    .line 193
    iget-object v0, p0, Lhup;->drk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 194
    iget-object v0, p0, Lhup;->drk:Ljava/util/List;

    iget-object v2, p0, Lhup;->drk:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhut;

    .line 195
    invoke-virtual {v0}, Lhut;->abc()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 200
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 201
    new-instance v0, Lhut;

    iget-object v1, p0, Lhup;->mRequestId:Ljava/lang/String;

    iget v2, p0, Lhup;->dro:I

    iget-object v3, p0, Lhup;->mHypothesisToSuggestionSpansConverter:Lgel;

    invoke-direct {v0, v1, v2, v3}, Lhut;-><init>(Ljava/lang/String;ILgel;)V

    .line 203
    iget v1, p0, Lhup;->dro:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhup;->dro:I

    .line 204
    iget-object v1, p0, Lhup;->drk:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    :cond_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private aSo()V
    .locals 4

    .prologue
    .line 213
    :cond_0
    :goto_0
    invoke-direct {p0}, Lhup;->aSm()Lhut;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 215
    invoke-virtual {v1}, Lhut;->aSq()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lhuu;

    iget v2, v1, Lhut;->drs:I

    invoke-virtual {v1}, Lhut;->aSs()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lhuu;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v1}, Lhut;->aSs()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    iput v2, v1, Lhut;->drs:I

    .line 216
    :goto_1
    if-eqz v0, :cond_0

    .line 217
    const/4 v1, 0x1

    new-array v1, v1, [Lhuu;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lhup;->a([Lhuu;)V

    goto :goto_0

    .line 215
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 220
    :cond_2
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/shared/speech/Hypothesis;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 277
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhup;->drl:Lhvu;

    invoke-interface {v0, p1}, Lhvu;->b(Lcom/google/android/shared/speech/Hypothesis;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lhup;->drl:Lhvu;

    invoke-interface {v1}, Lhvu;->reset()V

    .line 280
    invoke-direct {p0}, Lhup;->aSn()Lhut;

    move-result-object v1

    .line 282
    invoke-virtual {v1, v0}, Lhut;->a(Lcom/google/android/shared/speech/Hypothesis;)Lhuu;

    move-result-object v2

    .line 284
    const/4 v0, 0x0

    .line 285
    if-eqz p2, :cond_0

    .line 286
    invoke-virtual {v1, p2}, Lhut;->P(Ljava/lang/CharSequence;)Lhuu;

    move-result-object v0

    .line 289
    :cond_0
    if-eqz v2, :cond_1

    .line 290
    const/4 v1, 0x2

    new-array v1, v1, [Lhuu;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lhup;->a([Lhuu;)V

    .line 292
    :cond_1
    iget-boolean v0, p0, Lhup;->drp:Z

    if-nez v0, :cond_2

    .line 293
    invoke-virtual {p0}, Lhup;->aSp()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :cond_2
    monitor-exit p0

    return-void

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized aSi()V
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhup;->drp:Z

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p0}, Lhup;->aSp()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :cond_0
    monitor-exit p0

    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aSk()V
    .locals 1

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lhup;->aSo()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    monitor-exit p0

    return-void

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aSl()V
    .locals 1

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lhup;->aSo()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized aSp()V
    .locals 4

    .prologue
    .line 298
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lhup;->drp:Z

    .line 299
    invoke-direct {p0}, Lhup;->aSm()Lhut;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 301
    if-nez v0, :cond_1

    .line 320
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 306
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lhut;->aSr()Lhuu;

    move-result-object v0

    .line 307
    if-eqz v0, :cond_2

    .line 308
    const/4 v1, 0x1

    new-array v1, v1, [Lhuu;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lhup;->a([Lhuu;)V

    .line 311
    :cond_2
    invoke-direct {p0}, Lhup;->aSm()Lhut;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhup;->drp:Z

    .line 318
    iget-object v0, p0, Lhup;->aMD:Leqo;

    iget-object v1, p0, Lhup;->drm:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 319
    iget-object v0, p0, Lhup;->aMD:Leqo;

    iget-object v1, p0, Lhup;->drm:Ljava/lang/Runnable;

    iget-wide v2, p0, Lhup;->drq:J

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized on(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lhup;->mRequestId:Ljava/lang/String;

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Lhup;->dro:I

    .line 107
    invoke-direct {p0}, Lhup;->aSj()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized oo(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhup;->drl:Lhvu;

    invoke-interface {v0, p1}, Lhvu;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    invoke-direct {p0}, Lhup;->aSn()Lhut;

    move-result-object v1

    .line 230
    invoke-virtual {v1, v0}, Lhut;->P(Ljava/lang/CharSequence;)Lhuu;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_0

    .line 232
    const/4 v1, 0x1

    new-array v1, v1, [Lhuu;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lhup;->a([Lhuu;)V

    .line 235
    :cond_0
    iget-boolean v0, p0, Lhup;->drp:Z

    if-nez v0, :cond_1

    .line 236
    invoke-virtual {p0}, Lhup;->aSp()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :cond_1
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized reset()V
    .locals 2

    .prologue
    .line 114
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lhup;->dro:I

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhup;->drp:Z

    .line 116
    iget-object v0, p0, Lhup;->aMD:Leqo;

    iget-object v1, p0, Lhup;->drm:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
