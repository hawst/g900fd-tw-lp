.class final Lhbe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhbx;


# instance fields
.field final dbz:Lhbx;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lhax;Ljava/util/concurrent/Executor;Lhbx;)V
    .locals 0

    .prologue
    .line 490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    iput-object p2, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 492
    iput-object p3, p0, Lhbe;->dbz:Lhbx;

    .line 493
    return-void
.end method


# virtual methods
.method public final R(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 527
    iget-object v0, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhbi;

    const-string v2, "User navigation"

    invoke-direct {v1, p0, v2, p1}, Lhbi;-><init>(Lhbe;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 533
    return-void
.end method

.method public final aMZ()V
    .locals 3

    .prologue
    .line 507
    iget-object v0, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhbg;

    const-string v2, "Title changed"

    invoke-direct {v1, p0, v2}, Lhbg;-><init>(Lhbe;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 513
    return-void
.end method

.method public final aNf()V
    .locals 3

    .prologue
    .line 552
    iget-object v0, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhbk;

    const-string v2, "Window closed"

    invoke-direct {v1, p0, v2}, Lhbk;-><init>(Lhbe;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 558
    return-void
.end method

.method public final aV(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lhbe;->dbz:Lhbx;

    invoke-interface {v0, p1}, Lhbx;->aV(Ljava/lang/Object;)V

    .line 548
    return-void
.end method

.method public final aW(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 517
    iget-object v0, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhbh;

    const-string v2, "Page load failed"

    invoke-direct {v1, p0, v2, p1}, Lhbh;-><init>(Lhbe;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 523
    return-void
.end method

.method public final b(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V
    .locals 3

    .prologue
    .line 537
    iget-object v0, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhbj;

    const-string v2, "Load uri in app"

    invoke-direct {v1, p0, v2, p1}, Lhbj;-><init>(Lhbe;Ljava/lang/String;Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 543
    return-void
.end method

.method public final pageReady()V
    .locals 3

    .prologue
    .line 497
    iget-object v0, p0, Lhbe;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhbf;

    const-string v2, "Page ready"

    invoke-direct {v1, p0, v2}, Lhbf;-><init>(Lhbe;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 503
    return-void
.end method
