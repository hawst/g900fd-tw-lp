.class public final Lfuf;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 11

    .prologue
    const v10, 0x7f110309

    const v9, 0x7f110308

    const v4, 0x7f1100b0

    const/16 v8, 0x11

    const/4 v7, 0x0

    .line 53
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 55
    iget-object v2, v0, Lanh;->agI:Laod;

    .line 57
    const v0, 0x7f1101a9

    invoke-virtual {v2}, Laod;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfuf;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 58
    const v0, 0x7f11030a

    invoke-virtual {v2}, Laod;->qy()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfuf;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 61
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v2}, Laod;->oo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v0}, Lfuf;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 65
    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 66
    invoke-virtual {v2}, Laod;->qp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {v2}, Laod;->qo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00bd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v0, v4}, Lgab;->d(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 70
    :cond_1
    invoke-virtual {v2}, Laod;->qr()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    new-instance v0, Landroid/text/style/ImageSpan;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, Laod;->qq()I

    move-result v5

    const/4 v6, 0x1

    invoke-direct {v0, v4, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    .line 73
    const-string v4, "  "

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 74
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v3, v0, v4, v5, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 77
    :cond_2
    invoke-virtual {v2}, Laod;->qt()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 79
    const-string v0, "  "

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 81
    :cond_3
    invoke-virtual {v2}, Laod;->qs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 83
    :cond_4
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 84
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    :cond_5
    invoke-virtual {v2}, Laod;->qx()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 90
    invoke-virtual {v2}, Laod;->qv()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 93
    invoke-virtual {v2}, Laod;->qu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 94
    new-instance v0, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v0}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v3, v0, v7, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 96
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v0, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v3, v0, v7, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 99
    const-string v0, "  "

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 101
    :cond_6
    invoke-virtual {v2}, Laod;->qw()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 102
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    :cond_7
    invoke-virtual {v2}, Laod;->ok()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 107
    const v0, 0x7f11030b

    invoke-virtual {v2}, Laod;->oj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lfuf;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 109
    :cond_8
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 5

    .prologue
    const v0, 0x7f040117

    .line 34
    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->getType()I

    move-result v1

    .line 36
    iget-object v2, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v2}, Lanh;->getType()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 44
    const-string v2, "NearbyProductModulePresenter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad module type for nearby products: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :goto_0
    :sswitch_0
    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 41
    :sswitch_1
    const v0, 0x7f040118

    .line 42
    goto :goto_0

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x1c -> :sswitch_0
        0x22 -> :sswitch_1
    .end sparse-switch
.end method
