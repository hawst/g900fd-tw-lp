.class public final Libc;
.super Libf;
.source "PG"


# static fields
.field private static final dxb:Landroid/text/style/ForegroundColorSpan;


# instance fields
.field private final dwk:Libb;

.field private final dwm:I

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 35
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/4 v1, 0x0

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v0, Libc;->dxb:Landroid/text/style/ForegroundColorSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Libb;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Libf;-><init>()V

    .line 49
    iput-object p1, p0, Libc;->mResources:Landroid/content/res/Resources;

    .line 50
    iput-object p2, p0, Libc;->dwk:Libb;

    .line 51
    iput p3, p0, Libc;->dwm:I

    .line 52
    return-void
.end method

.method static synthetic aUI()Landroid/text/style/ForegroundColorSpan;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Libc;->dxb:Landroid/text/style/ForegroundColorSpan;

    return-object v0
.end method

.method private static b(Landroid/transition/TransitionValues;)V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 134
    instance-of v1, v0, Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    check-cast v0, Landroid/widget/TextView;

    .line 139
    iget-object v1, p0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-virtual {v0}, Landroid/widget/TextView;->getGravity()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/transition/TransitionValues;Ljava/lang/CharSequence;Landroid/transition/TransitionValues;Ljava/lang/CharSequence;)Landroid/animation/Animator;
    .locals 10

    .prologue
    .line 69
    iget-object v1, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 70
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 73
    check-cast v0, Landroid/widget/TextView;

    .line 75
    iget-object v2, p0, Libc;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0b00ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 76
    iget-object v2, p0, Libc;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0b00ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 77
    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, v2

    .line 79
    :goto_1
    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v6, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v6, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 82
    :goto_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 83
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 87
    iget v3, p0, Libc;->dwm:I

    iget-object v6, p0, Libc;->dwk:Libb;

    iget v6, v6, Libb;->factor:I

    mul-int/2addr v3, v6

    .line 88
    const-string v6, "translationY"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v9

    aput v9, v7, v8

    const/4 v8, 0x1

    int-to-float v3, v3

    aput v3, v7, v8

    invoke-static {v0, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 90
    const-string v6, "alpha"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v1

    aput v1, v7, v8

    const/4 v1, 0x1

    const/4 v8, 0x0

    aput v8, v7, v1

    invoke-static {v0, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 92
    new-instance v6, Libd;

    invoke-direct {v6, p0, v0, v2, p4}, Libd;-><init>(Libc;Landroid/widget/TextView;ILjava/lang/CharSequence;)V

    invoke-virtual {v1, v6}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 106
    new-instance v2, Landroid/animation/ValueAnimator;

    invoke-direct {v2}, Landroid/animation/ValueAnimator;-><init>()V

    .line 107
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 108
    new-instance v6, Libk;

    invoke-interface {p4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v0, v7, v5}, Libk;-><init>(Landroid/widget/TextView;Ljava/lang/String;I)V

    .line 110
    invoke-virtual {v2, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 111
    invoke-virtual {v2, v6}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 112
    const/4 v6, 0x2

    new-array v6, v6, [I

    const/4 v7, 0x0

    const/4 v8, 0x1

    aput v8, v6, v7

    const/4 v7, 0x1

    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result v8

    aput v8, v6, v7

    invoke-virtual {v2, v6}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 113
    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    mul-int/lit8 v6, v6, 0x21

    int-to-long v6, v6

    invoke-virtual {v2, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 116
    new-instance v6, Landroid/animation/ArgbEvaluator;

    invoke-direct {v6}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v4

    invoke-static {v6, v7}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 118
    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 119
    new-instance v5, Libe;

    invoke-direct {v5, p0, v0, v4}, Libe;-><init>(Libc;Landroid/widget/TextView;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 126
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 127
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/Animator;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    const/4 v2, 0x2

    aput-object v4, v5, v2

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 128
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto/16 :goto_0

    .line 77
    :cond_2
    const v2, 0x800003

    move v3, v2

    goto/16 :goto_1

    .line 79
    :cond_3
    const v2, 0x800003

    goto/16 :goto_2
.end method

.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 62
    invoke-super {p0, p1}, Libf;->captureEndValues(Landroid/transition/TransitionValues;)V

    .line 63
    invoke-static {p1}, Libc;->b(Landroid/transition/TransitionValues;)V

    .line 64
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0, p1}, Libf;->captureStartValues(Landroid/transition/TransitionValues;)V

    .line 57
    invoke-static {p1}, Libc;->b(Landroid/transition/TransitionValues;)V

    .line 58
    return-void
.end method
