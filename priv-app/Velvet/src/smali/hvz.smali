.class public final Lhvz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aof:Landroid/widget/TextView;

.field private bTv:[Landroid/view/View;

.field private final dsC:Lhvy;

.field public dsD:Landroid/view/View;

.field public dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

.field public dsF:Landroid/widget/ImageView;

.field private dsG:Landroid/view/View$OnClickListener;

.field private dsH:Landroid/view/View$OnClickListener;

.field public dsI:Landroid/widget/ImageView;

.field private dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

.field private dsK:Landroid/widget/RelativeLayout;

.field private dsL:Landroid/widget/Button;

.field public dsM:Landroid/widget/TextView;

.field public dsN:Landroid/view/View;

.field private dsO:[Landroid/view/View;

.field public dsP:Lhwf;

.field private final dsw:Z

.field public final mContext:Landroid/content/Context;

.field private final mSpeechLevelSource:Lequ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lequ;Z)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lhvz;->mContext:Landroid/content/Context;

    .line 87
    iput-object p2, p0, Lhvz;->mSpeechLevelSource:Lequ;

    .line 88
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v0, Lhvy;

    invoke-direct {v0, v1}, Lhvy;-><init>(Landroid/util/DisplayMetrics;)V

    iput-object v0, p0, Lhvz;->dsC:Lhvy;

    .line 89
    iput-boolean p3, p0, Lhvz;->dsw:Z

    .line 90
    return-void
.end method


# virtual methods
.method public final a(Lhwe;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 93
    iget-object v5, p0, Lhvz;->dsC:Lhvy;

    iget-object v1, p0, Lhvz;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f090187

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/view/ContextThemeWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v7, :cond_0

    move v2, v3

    :goto_0
    if-eqz v2, :cond_1

    iget v1, v5, Lhvy;->dsB:I

    :goto_1
    const/4 v6, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f110214

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    if-eqz v2, :cond_2

    iget v0, v5, Lhvy;->dsz:I

    :goto_2
    iput v0, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput-object v1, p0, Lhvz;->dsD:Landroid/view/View;

    .line 95
    new-instance v0, Lhwa;

    invoke-direct {v0, p0, p1}, Lhwa;-><init>(Lhvz;Lhwe;)V

    iput-object v0, p0, Lhvz;->dsG:Landroid/view/View$OnClickListener;

    .line 102
    new-instance v0, Lhwb;

    invoke-direct {v0, p0, p1}, Lhwb;-><init>(Lhvz;Lhwe;)V

    iput-object v0, p0, Lhvz;->dsH:Landroid/view/View$OnClickListener;

    .line 111
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f1101a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhvz;->aof:Landroid/widget/TextView;

    .line 113
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f11021d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iput-object v0, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    .line 115
    iget-object v0, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    iget-object v1, p0, Lhvz;->mSpeechLevelSource:Lequ;

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->c(Lequ;)V

    .line 117
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f11021f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    .line 119
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f110215

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iput-object v0, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    .line 121
    iget-object v0, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    iget-boolean v1, p0, Lhvz;->dsw:Z

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->gK(Z)V

    .line 122
    iget-object v0, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->a(Lhvx;)V

    .line 124
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f110217

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lhvz;->dsF:Landroid/widget/ImageView;

    .line 125
    iget-object v0, p0, Lhvz;->dsF:Landroid/widget/ImageView;

    new-instance v1, Lhwc;

    invoke-direct {v1, p0, p1}, Lhwc;-><init>(Lhvz;Lhwe;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f11021a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhvz;->dsM:Landroid/widget/TextView;

    .line 137
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f11021e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lhvz;->dsN:Landroid/view/View;

    .line 139
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f11021b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lhvz;->dsL:Landroid/widget/Button;

    .line 140
    iget-object v0, p0, Lhvz;->dsL:Landroid/widget/Button;

    new-instance v1, Lhwd;

    invoke-direct {v1, p0, p1}, Lhwd;-><init>(Lhvz;Lhwe;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    const v1, 0x7f110216

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lhvz;->dsK:Landroid/widget/RelativeLayout;

    .line 150
    new-array v0, v9, [Landroid/view/View;

    iget-object v1, p0, Lhvz;->dsD:Landroid/view/View;

    const v2, 0x7f110219

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lhvz;->dsD:Landroid/view/View;

    const v2, 0x7f110218

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lhvz;->dsD:Landroid/view/View;

    const v2, 0x7f11021a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v7

    iget-object v1, p0, Lhvz;->dsK:Landroid/widget/RelativeLayout;

    aput-object v1, v0, v8

    iput-object v0, p0, Lhvz;->dsO:[Landroid/view/View;

    .line 157
    const/16 v0, 0x8

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lhvz;->aof:Landroid/widget/TextView;

    aput-object v1, v0, v4

    iget-object v1, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    aput-object v1, v0, v3

    iget-object v1, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    aput-object v1, v0, v7

    iget-object v1, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v1, v0, v8

    iget-object v1, p0, Lhvz;->dsF:Landroid/widget/ImageView;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    iget-object v2, p0, Lhvz;->dsM:Landroid/widget/TextView;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lhvz;->dsL:Landroid/widget/Button;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lhvz;->dsN:Landroid/view/View;

    aput-object v2, v0, v1

    iput-object v0, p0, Lhvz;->bTv:[Landroid/view/View;

    .line 162
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    return-object v0

    :cond_0
    move v2, v4

    .line 93
    goto/16 :goto_0

    :cond_1
    iget v1, v5, Lhvy;->dsA:I

    goto/16 :goto_1

    :cond_2
    iget v0, v5, Lhvy;->dsy:I

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;[Ljzh;)V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->a(Ljava/lang/String;[Ljzh;)V

    .line 298
    return-void
.end method

.method public final aSY()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 196
    sget-object v0, Lhwf;->dsR:Lhwf;

    iput-object v0, p0, Lhvz;->dsP:Lhwf;

    .line 198
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 199
    new-array v0, v1, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lhvz;->c([Landroid/view/View;)V

    .line 201
    iget-object v0, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    const v1, 0x7f02016d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 202
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhvz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    return-void
.end method

.method public final aSZ()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 210
    sget-object v0, Lhwf;->dsT:Lhwf;

    iput-object v0, p0, Lhvz;->dsP:Lhwf;

    .line 212
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 213
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v2, v0, v1

    iget-object v1, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lhvz;->dsM:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lhvz;->c([Landroid/view/View;)V

    .line 215
    iget-object v0, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    const v1, 0x7f02024d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 216
    iget-object v0, p0, Lhvz;->dsH:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lhvz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Lhvz;->dsM:Landroid/widget/TextView;

    const v1, 0x7f0a0025

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 219
    iget-object v0, p0, Lhvz;->dsK:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lhvz;->mContext:Landroid/content/Context;

    const v2, 0x7f0a08de

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 221
    return-void
.end method

.method public final aTa()V
    .locals 5

    .prologue
    const v4, 0x7f0a08de

    const/4 v3, 0x1

    .line 225
    sget-object v0, Lhwf;->dsV:Lhwf;

    iput-object v0, p0, Lhvz;->dsP:Lhwf;

    .line 227
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 228
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v2, v0, v1

    iget-object v1, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lhvz;->dsM:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lhvz;->c([Landroid/view/View;)V

    .line 230
    iget-object v0, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    const v1, 0x7f020250

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 231
    iget-object v0, p0, Lhvz;->dsH:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lhvz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v0, p0, Lhvz;->dsM:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 234
    iget-object v0, p0, Lhvz;->dsK:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lhvz;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 236
    return-void
.end method

.method public final aTb()Z
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lhvz;->dsP:Lhwf;

    sget-object v1, Lhwf;->dsT:Lhwf;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aTc()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 271
    sget-object v0, Lhwf;->dsW:Lhwf;

    iput-object v0, p0, Lhvz;->dsP:Lhwf;

    .line 273
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 274
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lhvz;->aof:Landroid/widget/TextView;

    aput-object v2, v0, v1

    iget-object v1, p0, Lhvz;->dsN:Landroid/view/View;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lhvz;->dsL:Landroid/widget/Button;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lhvz;->c([Landroid/view/View;)V

    .line 276
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhvz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    iget-object v0, p0, Lhvz;->aof:Landroid/widget/TextView;

    const v1, 0x7f0a08e0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 278
    return-void
.end method

.method public varargs c([Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 170
    invoke-static {p1}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    .line 172
    iget-object v3, p0, Lhvz;->bTv:[Landroid/view/View;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 173
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->setEnabled(Z)V

    .line 172
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_1
    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lhvz;->dsJ:Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;

    invoke-virtual {v5, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->setEnabled(Z)V

    goto :goto_1

    .line 175
    :cond_2
    return-void
.end method

.method public final gL(Z)V
    .locals 5

    .prologue
    const v4, 0x7f0a0032

    const/4 v3, 0x0

    .line 252
    sget-object v0, Lhwf;->dsU:Lhwf;

    iput-object v0, p0, Lhvz;->dsP:Lhwf;

    .line 254
    iget-object v0, p0, Lhvz;->dsD:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 255
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lhvz;->dsE:Lcom/google/android/voicesearch/ime/view/LanguageSpinner;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lhvz;->dsF:Landroid/widget/ImageView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lhvz;->dsM:Landroid/widget/TextView;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lhvz;->c([Landroid/view/View;)V

    .line 257
    if-eqz p1, :cond_0

    .line 258
    iget-object v0, p0, Lhvz;->dsN:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 261
    :cond_0
    iget-object v0, p0, Lhvz;->dsI:Landroid/widget/ImageView;

    const v1, 0x7f02024a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 262
    iget-object v0, p0, Lhvz;->dsG:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lhvz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    iget-object v0, p0, Lhvz;->dsM:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 265
    iget-object v0, p0, Lhvz;->dsK:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lhvz;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 267
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    .line 327
    iget-object v1, p0, Lhvz;->dsO:[Landroid/view/View;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 328
    if-eqz v3, :cond_0

    .line 329
    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    if-nez p1, :cond_0

    .line 331
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 327
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_1
    return-void
.end method
