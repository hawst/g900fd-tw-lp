.class public final Lhmb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlk;


# instance fields
.field private final aYZ:Lciu;

.field private final bOj:Ldxd;

.field private final bOr:Ldwc;

.field private final bWE:Leei;

.field private final dkM:Lduv;

.field private final dkN:Leol;

.field private final dka:Ligi;

.field private final mCalendarHelper:Libp;

.field private final mContext:Landroid/content/Context;

.field private final mDiscourseContextSupplier:Ligi;

.field private final mHttpActionExecutor:Lhlr;

.field private final mIntentStarter:Leoj;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Leoj;Landroid/content/Context;Ldxd;Landroid/content/pm/PackageManager;Lhlr;Ligi;Lduv;Lciu;Libp;Leei;Ligi;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lhmb;->mIntentStarter:Leoj;

    .line 93
    iput-object p2, p0, Lhmb;->mContext:Landroid/content/Context;

    .line 94
    new-instance v0, Ldwc;

    invoke-direct {v0, p4, p10}, Ldwc;-><init>(Landroid/content/pm/PackageManager;Leei;)V

    iput-object v0, p0, Lhmb;->bOr:Ldwc;

    .line 95
    iput-object p3, p0, Lhmb;->bOj:Ldxd;

    .line 96
    iput-object p4, p0, Lhmb;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 97
    iput-object p5, p0, Lhmb;->mHttpActionExecutor:Lhlr;

    .line 98
    iput-object p6, p0, Lhmb;->dka:Ligi;

    .line 99
    iput-object p7, p0, Lhmb;->dkM:Lduv;

    .line 100
    new-instance v0, Leok;

    invoke-direct {v0}, Leok;-><init>()V

    iput-object v0, p0, Lhmb;->dkN:Leol;

    .line 101
    iput-object p8, p0, Lhmb;->aYZ:Lciu;

    .line 102
    iput-object p9, p0, Lhmb;->mCalendarHelper:Libp;

    .line 103
    iput-object p10, p0, Lhmb;->bWE:Leei;

    .line 104
    iput-object p11, p0, Lhmb;->mDiscourseContextSupplier:Ligi;

    .line 105
    return-void
.end method


# virtual methods
.method public final synthetic G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 7

    .prologue
    .line 66
    move-object v0, p1

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget-object v1, p0, Lhmb;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lhmb;->bOr:Ldwc;

    iget-object v3, p0, Lhmb;->bOj:Ldxd;

    iget-object v4, p0, Lhmb;->mIntentStarter:Leoj;

    iget-object v5, p0, Lhmb;->dkM:Lduv;

    iget-object v6, p0, Lhmb;->bWE:Leei;

    invoke-static/range {v0 .. v6}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;Landroid/content/pm/PackageManager;Ldwc;Ldxd;Leoj;Lduv;Leei;)Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/modular/ModularAction;I)Z
    .locals 12

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    sparse-switch p2, :sswitch_data_0

    move-object v11, v0

    .line 137
    :goto_0
    if-nez v11, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 141
    :goto_1
    return v0

    .line 118
    :sswitch_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajs()Ljqs;

    move-result-object v0

    move-object v11, v0

    .line 119
    goto :goto_0

    .line 121
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajt()Ljqs;

    move-result-object v0

    move-object v11, v0

    .line 122
    goto :goto_0

    .line 124
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->aju()Ljqs;

    move-result-object v0

    move-object v11, v0

    .line 125
    goto :goto_0

    .line 127
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajv()Ljqs;

    move-result-object v0

    move-object v11, v0

    .line 128
    goto :goto_0

    .line 130
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajq()Ljqs;

    move-result-object v0

    move-object v11, v0

    .line 131
    goto :goto_0

    .line 133
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajr()Ljqs;

    move-result-object v0

    move-object v11, v0

    goto :goto_0

    .line 141
    :cond_0
    sget-object v0, Ljqd;->eyo:Ljsm;

    invoke-virtual {v11, v0}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqd;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_4

    sget-object v0, Ljqz;->ezM:Ljsm;

    invoke-virtual {v11, v0}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqz;

    if-nez v0, :cond_b

    const/4 v0, 0x0

    :goto_3
    if-nez v0, :cond_4

    sget-object v0, Ljqr;->ezl:Ljsm;

    invoke-virtual {v11, v0}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqr;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljqr;->bqD()Z

    move-result v1

    if-nez v1, :cond_d

    :cond_1
    const/4 v0, 0x0

    :goto_4
    if-nez v0, :cond_4

    sget-object v0, Ljqc;->eyi:Ljsm;

    invoke-virtual {v11, v0}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqc;

    if-nez v0, :cond_14

    const/4 v0, 0x0

    :goto_5
    if-nez v0, :cond_4

    sget-object v0, Ljre;->eAh:Ljsm;

    invoke-virtual {v11, v0}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljre;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljre;->btd()Z

    move-result v1

    if-nez v1, :cond_1d

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    :cond_3
    :goto_6
    const/4 v0, 0x0

    :goto_7
    if-eqz v0, :cond_21

    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v1

    iget-object v1, v1, Ljrl;->eAU:Ljqf;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v2

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljqf;->bso()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhmb;->dkM:Lduv;

    invoke-virtual {v1}, Ljqf;->bsn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lduv;->at(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lhmb;->bOr:Ldwc;

    iget-object v3, p0, Lhmb;->bOj:Ldxd;

    invoke-virtual {v1, v0, p1, v3}, Ldwc;->a(Ljqd;Ldxb;Ldxd;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_7

    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lhmb;->mIntentStarter:Leoj;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v2, v3}, Leoj;->c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_8
    invoke-virtual {v0}, Ljqd;->bsk()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v0, p0, Lhmb;->mIntentStarter:Leoj;

    iget-object v2, p0, Lhmb;->dkN:Leol;

    invoke-interface {v0, v1, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    move-result v0

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v0}, Ljqd;->bsl()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lhmb;->bWE:Leei;

    invoke-virtual {v0, v1}, Leei;->x(Landroid/content/Intent;)Z

    move-result v0

    goto/16 :goto_2

    :cond_a
    iget-object v0, p0, Lhmb;->mIntentStarter:Leoj;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto/16 :goto_2

    :cond_b
    new-instance v2, Ljkt;

    invoke-direct {v2}, Ljkt;-><init>()V

    sget-object v3, Ljrc;->ezW:Ljsm;

    iget-object v1, p0, Lhmb;->dka:Ligi;

    invoke-interface {v1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1, v1, v4, v5, v6}, Lcom/google/android/search/shared/actions/modular/ModularAction;->b(ZZZZ)Ljrc;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    iget-object v1, p0, Lhmb;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcky;

    invoke-virtual {v1}, Lcky;->Pl()Ljkt;

    move-result-object v1

    if-eqz v1, :cond_c

    iget-object v1, v1, Ljkt;->eqn:Ljku;

    iput-object v1, v2, Ljkt;->eqn:Ljku;

    :cond_c
    iget-object v1, p0, Lhmb;->mHttpActionExecutor:Lhlr;

    invoke-virtual {v0}, Ljqz;->aiw()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lhlr;->a(Ljkt;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_3

    :cond_d
    invoke-virtual {v0}, Ljqr;->bqC()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v1

    if-nez v1, :cond_f

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_f
    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->akc()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->akb()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto/16 :goto_4

    :pswitch_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v1, :cond_10

    if-nez v2, :cond_10

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    goto/16 :goto_4

    :cond_10
    if-nez v1, :cond_11

    if-eqz v2, :cond_11

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    goto/16 :goto_4

    :cond_11
    const/4 v0, 0x1

    goto/16 :goto_4

    :pswitch_1
    iget-object v0, p0, Lhmb;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-eqz v1, :cond_13

    if-nez v2, :cond_13

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :cond_12
    :goto_8
    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_13
    if-nez v1, :cond_12

    if-eqz v2, :cond_12

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_8

    :cond_14
    invoke-virtual {v0}, Ljqc;->St()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v0}, Ljqc;->abx()Ljava/lang/String;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v0}, Ljqc;->bsb()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {v0}, Ljqc;->bsa()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    instance-of v3, v1, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    if-eqz v3, :cond_15

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v3

    if-eqz v3, :cond_15

    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :cond_15
    const-string v5, ""

    invoke-virtual {v0}, Ljqc;->bsh()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {v0}, Ljqc;->bsg()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    instance-of v3, v1, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    if-eqz v3, :cond_16

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v3

    if-eqz v3, :cond_16

    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->isCompleted()Z

    move-result v3

    if-eqz v3, :cond_16

    new-instance v3, Ljqu;

    invoke-direct {v3}, Ljqu;-><init>()V

    const/16 v6, 0x16

    invoke-virtual {v3, v6}, Ljqu;->rC(I)Ljqu;

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v6}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->b(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v1

    invoke-virtual {v1}, Ldws;->Pd()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-virtual {v1}, Ldws;->getString()Ljava/lang/String;

    move-result-object v5

    :cond_16
    const-wide/16 v6, -0x1

    invoke-virtual {v0}, Ljqc;->bsd()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {v0}, Ljqc;->bsc()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    instance-of v3, v1, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    if-eqz v3, :cond_17

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v3

    if-eqz v3, :cond_17

    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v3

    if-eqz v3, :cond_17

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajy()J

    move-result-wide v6

    :cond_17
    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-nez v1, :cond_19

    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_19
    const-wide/16 v8, -0x1

    invoke-virtual {v0}, Ljqc;->bsf()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Ljqc;->bse()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    if-eqz v1, :cond_1b

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v1

    if-eqz v1, :cond_1b

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajy()J

    move-result-wide v8

    :cond_1a
    :goto_9
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_1c

    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_1b
    instance-of v1, v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    if-eqz v1, :cond_1a

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v1

    if-eqz v1, :cond_1a

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long v8, v6, v0

    goto :goto_9

    :cond_1c
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/android/search/shared/util/Reminder;->bWy:Lcom/google/android/search/shared/util/Reminder;

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lhmb;->mCalendarHelper:Libp;

    const/4 v3, 0x0

    invoke-virtual/range {v1 .. v10}, Libp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/List;)Z

    move-result v0

    goto/16 :goto_5

    :cond_1d
    invoke-virtual {v0}, Ljre;->bte()I

    move-result v1

    if-nez v1, :cond_1e

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    goto/16 :goto_6

    :cond_1e
    invoke-virtual {v0}, Ljre;->btc()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    if-eqz v2, :cond_1f

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1f
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v1

    invoke-virtual {v0}, Ljre;->btc()I

    move-result v0

    invoke-virtual {v1, v0}, Ldvv;->gD(I)V

    goto/16 :goto_6

    :cond_20
    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    invoke-virtual {v0}, Ljre;->bte()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const/4 v0, 0x0

    goto/16 :goto_7

    :pswitch_2
    iget-object v0, p0, Lhmb;->aYZ:Lciu;

    invoke-virtual {v0, v3, v2}, Lciu;->a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    :goto_a
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amw()V

    const/4 v0, 0x1

    goto/16 :goto_7

    :pswitch_3
    iget-object v0, p0, Lhmb;->aYZ:Lciu;

    invoke-virtual {v0, v3, v2}, Lciu;->b(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    goto :goto_a

    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 116
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_5
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x67 -> :sswitch_0
        0x68 -> :sswitch_3
    .end sparse-switch

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 1

    .prologue
    .line 66
    check-cast p1, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {p0, p1, p2}, Lhmb;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;I)Z

    move-result v0

    return v0
.end method
