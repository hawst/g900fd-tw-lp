.class public final Ljac;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dVU:[Ljac;


# instance fields
.field private aez:I

.field private dPG:I

.field private dVV:I

.field private dVW:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v0, p0, Ljac;->aez:I

    iput v0, p0, Ljac;->dVV:I

    iput v0, p0, Ljac;->dPG:I

    iput v0, p0, Ljac;->dVW:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljac;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljac;->eCz:I

    return-void
.end method

.method public static bdG()[Ljac;
    .locals 2

    sget-object v0, Ljac;->dVU:[Ljac;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljac;->dVU:[Ljac;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljac;

    sput-object v0, Ljac;->dVU:[Ljac;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljac;->dVU:[Ljac;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljac;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljac;->dVV:I

    iget v0, p0, Ljac;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljac;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljac;->dPG:I

    iget v0, p0, Ljac;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljac;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljac;->dVW:I

    iget v0, p0, Ljac;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljac;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljac;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljac;->dVV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget v0, p0, Ljac;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljac;->dPG:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    iget v0, p0, Ljac;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljac;->dVW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljac;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljac;->dVV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljac;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljac;->dPG:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljac;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljac;->dVW:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    return v0
.end method
