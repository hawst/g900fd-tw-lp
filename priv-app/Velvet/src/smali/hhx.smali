.class final Lhhx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field final synthetic dhT:Lhhw;


# direct methods
.method constructor <init>(Lhhw;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lhhx;->dhT:Lhhw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAudioFocusChange(I)V
    .locals 5

    .prologue
    .line 75
    const-string v0, "AudioRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Audio focus change "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, -0x2

    if-eq p1, v0, :cond_0

    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 80
    :cond_0
    iget-object v0, p0, Lhhx;->dhT:Lhhw;

    iget-object v0, v0, Lhhw;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lhhy;

    const-string v2, "AudioRouter"

    const-string v3, "AudioFocusLost"

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-direct {v1, p0, v2, v3, v4}, Lhhy;-><init>(Lhhx;Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 92
    :cond_1
    return-void
.end method
