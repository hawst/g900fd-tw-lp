.class public final Lclq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bbd:Lenw;

.field bbe:Ljava/lang/String;

.field bbf:Z

.field cq:Z

.field mListener:Lclt;

.field private final mMainThreadExecutor:Ljava/util/concurrent/Executor;

.field final mVss:Lhhq;


# direct methods
.method public constructor <init>(Lhhq;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lclq;->mVss:Lhhq;

    .line 63
    iput-object p2, p0, Lclq;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    .line 64
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lclq;->bbd:Lenw;

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Lclt;Lcom/google/android/shared/search/Query;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 69
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 71
    iget-object v0, p0, Lclq;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 73
    iget-boolean v0, p0, Lclq;->cq:Z

    if-nez v0, :cond_1

    .line 74
    iput-object p1, p0, Lclq;->mListener:Lclt;

    .line 75
    iput-boolean v1, p0, Lclq;->cq:Z

    .line 76
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v0

    iput-boolean v0, p0, Lclq;->bbf:Z

    .line 77
    new-instance v0, Lgmy;

    invoke-direct {v0}, Lgmy;-><init>()V

    iput-boolean v2, v0, Lgmy;->cIK:Z

    iput-boolean v2, v0, Lgmy;->cPF:Z

    const/16 v1, 0x2b11

    iput v1, v0, Lgmy;->cPK:I

    invoke-virtual {v0}, Lgmy;->aHB()Lgmx;

    move-result-object v1

    new-instance v2, Lgnk;

    invoke-direct {v2}, Lgnk;-><init>()V

    const-string v0, "en-US"

    iput-object v0, v2, Lgnk;->cQb:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    :goto_2
    invoke-virtual {v2, v0}, Lgnk;->kt(I)Lgnk;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->alf()Z

    move-result v2

    iput-boolean v2, v0, Lgnk;->cQj:Z

    iput-object v1, v0, Lgnk;->cPY:Lgmx;

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgnk;->mRequestId:Ljava/lang/String;

    invoke-virtual {v0}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lclq;->bbe:Ljava/lang/String;

    .line 79
    iget-object v1, p0, Lclq;->mVss:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v1

    new-instance v2, Lclr;

    invoke-direct {v2, p0}, Lclr;-><init>(Lclq;)V

    iget-object v3, p0, Lclq;->mMainThreadExecutor:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 82
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 68
    goto :goto_0

    :cond_3
    move v0, v2

    .line 69
    goto :goto_1

    .line 77
    :cond_4
    const/4 v0, 0x7

    goto :goto_2
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lclq;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lclq;->ct(Z)V

    .line 89
    return-void
.end method

.method final ct(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-boolean v0, p0, Lclq;->cq:Z

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lclq;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Lclq;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 95
    iput-object v2, p0, Lclq;->mListener:Lclt;

    .line 96
    iput-object v2, p0, Lclq;->bbe:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lclq;->cq:Z

    .line 98
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lclq;->bbf:Z

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lclq;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 102
    :cond_0
    return-void
.end method
