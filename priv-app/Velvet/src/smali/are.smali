.class public final Lare;
.super Larc;
.source "PG"


# static fields
.field private static final ani:Lcom/google/android/shared/search/SearchBoxStats;

.field private static final anj:Lcom/google/android/search/shared/service/ClientConfig;


# instance fields
.field ank:Ljava/util/List;

.field private anl:Larv;

.field private anm:Lecp;

.field ann:Lecq;

.field private ano:Landroid/widget/ViewFlipper;

.field private final anp:Lecr;

.field private final anq:Lary;

.field mAsyncServices:Lema;

.field private mGsaConfigFlags:Lchk;

.field private mLoginHelper:Lcrh;

.field private mVoiceSettings:Lhym;

.field private mVss:Lhhq;

.field private sY:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 53
    const-string v0, "speakerid-enrollment"

    const-string v1, "android-search-app"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lare;->ani:Lcom/google/android/shared/search/SearchBoxStats;

    .line 57
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x80000

    sget-object v1, Lare;->ani:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    sput-object v0, Lare;->anj:Lcom/google/android/search/shared/service/ClientConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Larc;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lare;->sY:I

    .line 74
    new-instance v0, Larf;

    invoke-direct {v0, p0}, Larf;-><init>(Lare;)V

    iput-object v0, p0, Lare;->anp:Lecr;

    .line 316
    new-instance v0, Larh;

    invoke-direct {v0, p0}, Larh;-><init>(Lare;)V

    iput-object v0, p0, Lare;->anq:Lary;

    .line 348
    return-void
.end method

.method static synthetic a(Lare;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lare;->ank:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    iget-object v1, p0, Lare;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgep;->p(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private ai(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 166
    const v0, 0x7f110183

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    if-eqz v0, :cond_0

    .line 168
    const v1, 0x7f0a0703

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {}, Lare;->tR()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lare;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :cond_0
    const v0, 0x7f110184

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    if-eqz v0, :cond_1

    .line 173
    const v1, 0x7f0a0704

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {}, Lare;->tR()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lare;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    :cond_1
    return-void
.end method

.method static synthetic b(Lare;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-virtual {p0}, Lare;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    const v1, 0x7f0a099f

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0, v2}, Lare;->bq(Z)V

    iput v2, p0, Lare;->sY:I

    invoke-direct {p0}, Lare;->tU()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lare;->bq(Z)V

    :cond_0
    return-void
.end method

.method private bq(Z)V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lare;->ann:Lecq;

    invoke-virtual {v0, p1}, Lecq;->bq(Z)V

    .line 304
    return-void
.end method

.method private br(Z)V
    .locals 3

    .prologue
    .line 308
    if-nez p1, :cond_0

    .line 309
    iget-object v0, p0, Lare;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lare;->mLoginHelper:Lcrh;

    invoke-virtual {v2}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcse;->a([BLjava/lang/String;)V

    .line 310
    iget-object v0, p0, Lare;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iget-object v1, p0, Lare;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcse;->iJ(Ljava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 312
    :cond_0
    iget-object v0, p0, Lare;->anl:Larv;

    iget-object v1, p0, Lare;->ank:Ljava/util/List;

    iget-object v2, p0, Lare;->anq:Lary;

    invoke-virtual {v0, v1, v2}, Larv;->a(Ljava/util/List;Lary;)V

    goto :goto_0
.end method

.method public static tS()Lare;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lare;

    invoke-direct {v0}, Lare;-><init>()V

    return-object v0
.end method

.method private tU()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lare;->ano:Landroid/widget/ViewFlipper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 257
    return-void
.end method

.method private tV()V
    .locals 2

    .prologue
    .line 260
    const/16 v0, 0x129

    invoke-virtual {p0, v0}, Lare;->db(I)V

    .line 262
    iget-object v0, p0, Lare;->ano:Landroid/widget/ViewFlipper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 263
    return-void
.end method

.method private tW()V
    .locals 2

    .prologue
    .line 266
    const/16 v0, 0x12a

    invoke-virtual {p0, v0}, Lare;->db(I)V

    .line 268
    iget-object v0, p0, Lare;->ano:Landroid/widget/ViewFlipper;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 269
    return-void
.end method

.method private tX()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lare;->ano:Landroid/widget/ViewFlipper;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 277
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lare;->br(Z)V

    .line 278
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 92
    invoke-super {p0, p1}, Larc;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iput-object v0, p0, Lare;->mVss:Lhhq;

    .line 95
    iget-object v0, p0, Lare;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lare;->mVoiceSettings:Lhym;

    .line 96
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lare;->mAsyncServices:Lema;

    .line 97
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lare;->mGsaConfigFlags:Lchk;

    .line 98
    new-instance v0, Lark;

    invoke-direct {v0, p0}, Lark;-><init>(Lare;)V

    iput-object v0, p0, Lare;->anm:Lecp;

    .line 99
    new-instance v0, Lecq;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    iget-object v2, p0, Lare;->anp:Lecr;

    iget-object v3, p0, Lare;->anm:Lecp;

    sget-object v4, Lare;->anj:Lcom/google/android/search/shared/service/ClientConfig;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lare;->ann:Lecq;

    .line 101
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Lare;->mLoginHelper:Lcrh;

    .line 102
    new-instance v0, Larv;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    iget-object v2, p0, Lare;->mVss:Lhhq;

    iget-object v3, p0, Lare;->mAsyncServices:Lema;

    iget-object v4, p0, Lare;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lare;->mLoginHelper:Lcrh;

    invoke-virtual {v5}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lare;->mVoiceSettings:Lhym;

    invoke-virtual {v6}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x3e80

    const/4 v8, 0x1

    invoke-direct/range {v0 .. v8}, Larv;-><init>(Landroid/content/Context;Lhhq;Lema;Lchk;Ljava/lang/String;Ljava/lang/String;II)V

    iput-object v0, p0, Lare;->anl:Larv;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lare;->ank:Ljava/util/List;

    .line 107
    if-eqz p1, :cond_0

    .line 108
    iget-object v0, p0, Lare;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lare;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->any()V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 117
    const v0, 0x7f04006a

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 119
    const v0, 0x7f110188

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    const v2, 0x7f0a0700

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Lare;->tR()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lare;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const v0, 0x7f110182

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lare;->ai(Landroid/view/View;)V

    .line 123
    const v0, 0x7f110185

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lare;->ai(Landroid/view/View;)V

    .line 124
    const v0, 0x7f110186

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lare;->ai(Landroid/view/View;)V

    .line 125
    const v0, 0x7f110187

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lare;->ai(Landroid/view/View;)V

    .line 127
    const v0, 0x7f11018a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Larg;

    invoke-direct {v2, p0}, Larg;-><init>(Lare;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v0, 0x7f110189

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lare;->ano:Landroid/widget/ViewFlipper;

    .line 137
    if-eqz p3, :cond_0

    .line 138
    const-string v0, "key_hotword_count"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lare;->sY:I

    .line 140
    iget v0, p0, Lare;->sY:I

    packed-switch v0, :pswitch_data_0

    .line 162
    :goto_0
    return-object v1

    .line 143
    :pswitch_0
    invoke-direct {p0}, Lare;->tU()V

    goto :goto_0

    .line 147
    :pswitch_1
    invoke-direct {p0}, Lare;->tV()V

    goto :goto_0

    .line 151
    :pswitch_2
    invoke-direct {p0}, Lare;->tW()V

    goto :goto_0

    .line 155
    :pswitch_3
    invoke-direct {p0}, Lare;->tX()V

    goto :goto_0

    .line 159
    :cond_0
    invoke-direct {p0}, Lare;->tU()V

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Larc;->onDestroy()V

    .line 180
    iget-object v0, p0, Lare;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    .line 181
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 212
    invoke-super {p0, p1}, Larc;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 213
    iget-object v0, p0, Lare;->ann:Lecq;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lare;->ann:Lecq;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lecq;->a(Landroid/os/Bundle;Z)V

    .line 216
    :cond_0
    const-string v0, "key_hotword_count"

    iget v1, p0, Lare;->sY:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 217
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 185
    invoke-super {p0}, Larc;->onStart()V

    .line 186
    iget-object v0, p0, Lare;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 187
    invoke-virtual {p0}, Lare;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget v0, p0, Lare;->sY:I

    if-nez v0, :cond_0

    .line 190
    const/16 v0, 0x127

    invoke-virtual {p0, v0}, Lare;->db(I)V

    .line 192
    invoke-direct {p0, v2}, Lare;->br(Z)V

    .line 194
    :cond_0
    iget v0, p0, Lare;->sY:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 196
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lare;->bq(Z)V

    .line 201
    :cond_1
    :goto_0
    return-void

    .line 198
    :cond_2
    invoke-direct {p0, v2}, Lare;->bq(Z)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lare;->bq(Z)V

    .line 206
    iget-object v0, p0, Lare;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 207
    invoke-super {p0}, Larc;->onStop()V

    .line 208
    return-void
.end method

.method public final tT()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 222
    iget v0, p0, Lare;->sY:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lare;->sY:I

    .line 223
    invoke-virtual {p0}, Lare;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    :goto_0
    return-void

    .line 228
    :cond_0
    invoke-direct {p0, v2}, Lare;->bq(Z)V

    .line 229
    iget v0, p0, Lare;->sY:I

    packed-switch v0, :pswitch_data_0

    .line 250
    invoke-virtual {p0, v2}, Lare;->bp(Z)V

    goto :goto_0

    .line 233
    :pswitch_0
    invoke-direct {p0}, Lare;->tV()V

    .line 234
    invoke-direct {p0, v1}, Lare;->bq(Z)V

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-direct {p0}, Lare;->tW()V

    .line 240
    invoke-direct {p0, v1}, Lare;->bq(Z)V

    goto :goto_0

    .line 245
    :pswitch_2
    invoke-direct {p0}, Lare;->tX()V

    .line 246
    invoke-direct {p0, v1}, Lare;->bq(Z)V

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
