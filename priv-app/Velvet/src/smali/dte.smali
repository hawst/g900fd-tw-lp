.class public final Ldte;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bJZ:Ldtf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ldtf;

    invoke-direct {v0, p1}, Ldtf;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldte;->bJZ:Ldtf;

    .line 33
    return-void
.end method

.method public constructor <init>(Ldtf;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Ldte;->bJZ:Ldtf;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/inputmethod/InputMethodSubtype;)Landroid/view/inputmethod/InputMethodInfo;
    .locals 4
    .param p1    # Landroid/view/inputmethod/InputMethodSubtype;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58
    if-eqz p1, :cond_2

    .line 59
    iget-object v0, p0, Ldte;->bJZ:Ldtf;

    iget-object v0, v0, Ldtf;->xe:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 60
    iget-object v1, p0, Ldte;->bJZ:Ldtf;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, Ldtf;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodSubtype;

    .line 61
    invoke-virtual {p1, v1}, Landroid/view/inputmethod/InputMethodSubtype;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final afR()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 80
    iget-object v1, p0, Ldte;->bJZ:Ldtf;

    iget-object v1, v1, Ldtf;->xe:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v1

    .line 81
    invoke-virtual {p0, v1}, Ldte;->a(Landroid/view/inputmethod/InputMethodSubtype;)Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v2

    .line 83
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    move-result-object v3

    const-string v4, "keyboard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v1

    const-string v3, "en_US"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldte;->bJZ:Ldtf;

    invoke-static {v2}, Ldtf;->a(Landroid/view/inputmethod/InputMethodInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
