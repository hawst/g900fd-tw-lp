.class public final Laqa;
.super Ljsl;
.source "PG"


# instance fields
.field public amf:Ljha;

.field public amg:Ljdn;

.field public amh:[Ljde;

.field public ami:[Lapz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 38
    iput-object v1, p0, Laqa;->amf:Ljha;

    iput-object v1, p0, Laqa;->amg:Ljdn;

    invoke-static {}, Ljde;->bgW()[Ljde;

    move-result-object v0

    iput-object v0, p0, Laqa;->amh:[Ljde;

    invoke-static {}, Lapz;->tJ()[Lapz;

    move-result-object v0

    iput-object v0, p0, Laqa;->ami:[Lapz;

    iput-object v1, p0, Laqa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laqa;->eCz:I

    .line 39
    return-void
.end method

.method public static e([B)Laqa;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Laqa;

    invoke-direct {v0}, Laqa;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Laqa;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laqa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Laqa;->amf:Ljha;

    if-nez v0, :cond_1

    new-instance v0, Ljha;

    invoke-direct {v0}, Ljha;-><init>()V

    iput-object v0, p0, Laqa;->amf:Ljha;

    :cond_1
    iget-object v0, p0, Laqa;->amf:Ljha;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Laqa;->amg:Ljdn;

    if-nez v0, :cond_2

    new-instance v0, Ljdn;

    invoke-direct {v0}, Ljdn;-><init>()V

    iput-object v0, p0, Laqa;->amg:Ljdn;

    :cond_2
    iget-object v0, p0, Laqa;->amg:Ljdn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Laqa;->amh:[Ljde;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljde;

    if-eqz v0, :cond_3

    iget-object v3, p0, Laqa;->amh:[Ljde;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljde;

    invoke-direct {v3}, Ljde;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Laqa;->amh:[Ljde;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljde;

    invoke-direct {v3}, Ljde;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Laqa;->amh:[Ljde;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Laqa;->ami:[Lapz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lapz;

    if-eqz v0, :cond_6

    iget-object v3, p0, Laqa;->ami:[Lapz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lapz;

    invoke-direct {v3}, Lapz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Laqa;->ami:[Lapz;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lapz;

    invoke-direct {v3}, Lapz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Laqa;->ami:[Lapz;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Laqa;->amf:Ljha;

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x1

    iget-object v2, p0, Laqa;->amf:Ljha;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 57
    :cond_0
    iget-object v0, p0, Laqa;->amg:Ljdn;

    if-eqz v0, :cond_1

    .line 58
    const/4 v0, 0x2

    iget-object v2, p0, Laqa;->amg:Ljdn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 60
    :cond_1
    iget-object v0, p0, Laqa;->amh:[Ljde;

    if-eqz v0, :cond_3

    iget-object v0, p0, Laqa;->amh:[Ljde;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 61
    :goto_0
    iget-object v2, p0, Laqa;->amh:[Ljde;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 62
    iget-object v2, p0, Laqa;->amh:[Ljde;

    aget-object v2, v2, v0

    .line 63
    if-eqz v2, :cond_2

    .line 64
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 61
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_3
    iget-object v0, p0, Laqa;->ami:[Lapz;

    if-eqz v0, :cond_5

    iget-object v0, p0, Laqa;->ami:[Lapz;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 69
    :goto_1
    iget-object v0, p0, Laqa;->ami:[Lapz;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 70
    iget-object v0, p0, Laqa;->ami:[Lapz;

    aget-object v0, v0, v1

    .line 71
    if-eqz v0, :cond_4

    .line 72
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 69
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 77
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 82
    iget-object v2, p0, Laqa;->amf:Ljha;

    if-eqz v2, :cond_0

    .line 83
    const/4 v2, 0x1

    iget-object v3, p0, Laqa;->amf:Ljha;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_0
    iget-object v2, p0, Laqa;->amg:Ljdn;

    if-eqz v2, :cond_1

    .line 87
    const/4 v2, 0x2

    iget-object v3, p0, Laqa;->amg:Ljdn;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 90
    :cond_1
    iget-object v2, p0, Laqa;->amh:[Ljde;

    if-eqz v2, :cond_4

    iget-object v2, p0, Laqa;->amh:[Ljde;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 91
    :goto_0
    iget-object v3, p0, Laqa;->amh:[Ljde;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 92
    iget-object v3, p0, Laqa;->amh:[Ljde;

    aget-object v3, v3, v0

    .line 93
    if-eqz v3, :cond_2

    .line 94
    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 91
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 99
    :cond_4
    iget-object v2, p0, Laqa;->ami:[Lapz;

    if-eqz v2, :cond_6

    iget-object v2, p0, Laqa;->ami:[Lapz;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 100
    :goto_1
    iget-object v2, p0, Laqa;->ami:[Lapz;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 101
    iget-object v2, p0, Laqa;->ami:[Lapz;

    aget-object v2, v2, v1

    .line 102
    if-eqz v2, :cond_5

    .line 103
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 108
    :cond_6
    return v0
.end method
