.class final Lfaz;
.super Lfbk;
.source "PG"


# instance fields
.field private cmM:Lfba;

.field private cnF:Ljava/lang/Iterable;

.field private cnG:Ljava/util/Map;

.field private cnH:Z

.field final cnI:Ljava/util/List;


# direct methods
.method constructor <init>(Lfba;Ljava/lang/Iterable;Ljava/util/Map;Z)V
    .locals 1

    .prologue
    .line 1521
    invoke-direct {p0}, Lfbk;-><init>()V

    .line 1522
    iput-object p1, p0, Lfaz;->cmM:Lfba;

    .line 1523
    iput-object p2, p0, Lfaz;->cnF:Ljava/lang/Iterable;

    .line 1524
    iput-object p3, p0, Lfaz;->cnG:Ljava/util/Map;

    .line 1525
    iput-boolean p4, p0, Lfaz;->cnH:Z

    .line 1526
    invoke-static {p2}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfaz;->cnI:Ljava/util/List;

    .line 1527
    return-void
.end method

.method private c(Lizj;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1563
    iget-object v0, p0, Lfaz;->cnF:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 1564
    invoke-virtual {p1}, Lizj;->getType()I

    move-result v1

    invoke-virtual {v0}, Lizj;->getType()I

    move-result v2

    if-ne v1, v2, :cond_2

    move v1, v3

    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lizj;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-virtual {v0}, Lizj;->bcR()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v3

    :goto_2
    if-eqz v1, :cond_0

    .line 1565
    iget-object v1, p0, Lfaz;->cnI:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1566
    invoke-virtual {p1}, Lizj;->bcT()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lizj;->bcT()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_1
    move v1, v3

    :goto_3
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lizj;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    move v1, v3

    :goto_4
    if-eqz v1, :cond_0

    .line 1568
    new-instance v1, Lizj;

    invoke-direct {v1}, Lizj;-><init>()V

    invoke-static {p1, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Lizj;

    .line 1569
    invoke-virtual {v0}, Lizj;->bdg()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {p1}, Lizj;->bdh()Lizj;

    invoke-static {v0, p1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    .line 1570
    :goto_5
    iget-boolean v2, p0, Lfbk;->cnX:Z

    if-eqz v2, :cond_0

    .line 1571
    iget-object v2, p0, Lfaz;->cmM:Lfba;

    invoke-virtual {v2, v1, p1, v0}, Lfba;->a(Lizj;Lizj;Lizj;)V

    goto :goto_0

    :cond_2
    move v1, v4

    .line 1564
    goto :goto_1

    :pswitch_0
    move v1, v3

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lizj;->bcR()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lizj;->bcQ()J

    move-result-wide v6

    invoke-virtual {v0}, Lizj;->bcQ()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-nez v1, :cond_4

    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v4

    goto :goto_2

    .line 1566
    :cond_5
    invoke-virtual {v0}, Lizj;->bcS()J

    move-result-wide v6

    invoke-virtual {p1}, Lizj;->bcS()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-lez v1, :cond_6

    move v1, v3

    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_3

    :sswitch_0
    iget-object v1, v0, Lizj;->dSu:Ljdc;

    invoke-virtual {v1}, Ljdc;->xi()D

    move-result-wide v6

    iget-object v1, p1, Lizj;->dSu:Ljdc;

    invoke-virtual {v1}, Ljdc;->xi()D

    move-result-wide v8

    cmpl-double v1, v6, v8

    if-lez v1, :cond_7

    move v1, v3

    goto :goto_4

    :cond_7
    move v1, v4

    goto :goto_4

    :sswitch_1
    iget-object v1, p1, Lizj;->dSk:Ljgj;

    invoke-virtual {v1}, Ljgj;->bjY()Z

    move-result v1

    if-nez v1, :cond_8

    move v1, v3

    goto :goto_4

    :cond_8
    move v1, v4

    goto :goto_4

    .line 1569
    :cond_9
    :try_start_0
    iget-boolean v2, p0, Lfaz;->cnH:Z

    invoke-static {p1, v0, v2}, Lfhb;->a(Ljsr;Ljsr;Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v2

    const-string v6, "EntryProvider"

    const-string v7, "Failure to merge a partial entry"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 1576
    :cond_a
    return-void

    .line 1564
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch

    .line 1566
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_1
        0x13 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method protected final a(Lgbg;Lizj;)V
    .locals 0

    .prologue
    .line 1559
    invoke-direct {p0, p2}, Lfaz;->c(Lizj;)V

    .line 1560
    return-void
.end method

.method protected final a(Lizq;)V
    .locals 1

    .prologue
    .line 1552
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    if-eqz v0, :cond_0

    .line 1553
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    invoke-direct {p0, v0}, Lfaz;->c(Lizj;)V

    .line 1555
    :cond_0
    return-void
.end method

.method protected final d(Lizo;)V
    .locals 8

    .prologue
    .line 1535
    iget-object v2, p1, Lizo;->dUS:[Lizp;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1536
    iget-object v0, v4, Lizp;->dRY:Ljie;

    if-eqz v0, :cond_1

    .line 1537
    iget-object v0, v4, Lizp;->dRY:Ljie;

    invoke-virtual {v0}, Ljie;->bmT()J

    move-result-wide v6

    .line 1538
    iget-object v0, p0, Lfaz;->cnG:Ljava/util/Map;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljie;

    .line 1539
    if-eqz v0, :cond_1

    .line 1540
    iget-boolean v5, p0, Lfbk;->cnX:Z

    if-eqz v5, :cond_0

    .line 1541
    iget-object v5, p0, Lfaz;->cmM:Lfba;

    invoke-virtual {v5, v4, v0}, Lfba;->a(Lizp;Ljie;)V

    .line 1543
    :cond_0
    iput-object v0, v4, Lizp;->dRY:Ljie;

    .line 1535
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1547
    :cond_2
    invoke-super {p0, p1}, Lfbk;->d(Lizo;)V

    .line 1548
    return-void
.end method
