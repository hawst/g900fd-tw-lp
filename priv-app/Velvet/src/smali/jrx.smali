.class public final Ljrx;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field private eBS:Ljava/lang/String;

.field private eBT:I

.field private evK:Ljnb;

.field private exQ:[Ljnc;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 101
    iput v1, p0, Ljrx;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljrx;->agv:I

    iput-object v2, p0, Ljrx;->evK:Ljnb;

    const-string v0, ""

    iput-object v0, p0, Ljrx;->eBS:Ljava/lang/String;

    invoke-static {}, Ljnc;->bqy()[Ljnc;

    move-result-object v0

    iput-object v0, p0, Ljrx;->exQ:[Ljnc;

    iput v1, p0, Ljrx;->eBT:I

    iput-object v2, p0, Ljrx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrx;->eCz:I

    .line 102
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljrx;->agv:I

    iget v0, p0, Ljrx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrx;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljrx;->evK:Ljnb;

    if-nez v0, :cond_1

    new-instance v0, Ljnb;

    invoke-direct {v0}, Ljnb;-><init>()V

    iput-object v0, p0, Ljrx;->evK:Ljnb;

    :cond_1
    iget-object v0, p0, Ljrx;->evK:Ljnb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrx;->eBS:Ljava/lang/String;

    iget v0, p0, Ljrx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrx;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrx;->exQ:[Ljnc;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljnc;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljrx;->exQ:[Ljnc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljnc;

    invoke-direct {v3}, Ljnc;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljrx;->exQ:[Ljnc;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljnc;

    invoke-direct {v3}, Ljnc;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrx;->exQ:[Ljnc;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrx;->eBT:I

    iget v0, p0, Ljrx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljrx;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 119
    iget v0, p0, Ljrx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 120
    const/4 v0, 0x1

    iget v1, p0, Ljrx;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 122
    :cond_0
    iget-object v0, p0, Ljrx;->evK:Ljnb;

    if-eqz v0, :cond_1

    .line 123
    const/4 v0, 0x2

    iget-object v1, p0, Ljrx;->evK:Ljnb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 125
    :cond_1
    iget v0, p0, Ljrx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 126
    const/4 v0, 0x3

    iget-object v1, p0, Ljrx;->eBS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 128
    :cond_2
    iget-object v0, p0, Ljrx;->exQ:[Ljnc;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljrx;->exQ:[Ljnc;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 129
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljrx;->exQ:[Ljnc;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 130
    iget-object v1, p0, Ljrx;->exQ:[Ljnc;

    aget-object v1, v1, v0

    .line 131
    if-eqz v1, :cond_3

    .line 132
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 129
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_4
    iget v0, p0, Ljrx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 137
    const/4 v0, 0x5

    iget v1, p0, Ljrx;->eBT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bs(II)V

    .line 139
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 140
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 144
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 145
    iget v1, p0, Ljrx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 146
    const/4 v1, 0x1

    iget v2, p0, Ljrx;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_0
    iget-object v1, p0, Ljrx;->evK:Ljnb;

    if-eqz v1, :cond_1

    .line 150
    const/4 v1, 0x2

    iget-object v2, p0, Ljrx;->evK:Ljnb;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_1
    iget v1, p0, Ljrx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 154
    const/4 v1, 0x3

    iget-object v2, p0, Ljrx;->eBS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_2
    iget-object v1, p0, Ljrx;->exQ:[Ljnc;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljrx;->exQ:[Ljnc;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 158
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljrx;->exQ:[Ljnc;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 159
    iget-object v2, p0, Ljrx;->exQ:[Ljnc;

    aget-object v2, v2, v0

    .line 160
    if-eqz v2, :cond_3

    .line 161
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 158
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 166
    :cond_5
    iget v1, p0, Ljrx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 167
    const/4 v1, 0x5

    iget v2, p0, Ljrx;->eBT:I

    invoke-static {v1, v2}, Ljsj;->bw(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_6
    return v0
.end method
