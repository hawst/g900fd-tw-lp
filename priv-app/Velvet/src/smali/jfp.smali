.class public final Ljfp;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efQ:I

.field private egu:Z

.field private eiL:Z

.field private eiM:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 10556
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10557
    iput v1, p0, Ljfp;->aez:I

    iput-object v2, p0, Ljfp;->efN:Ljez;

    iput v1, p0, Ljfp;->efQ:I

    iput-boolean v0, p0, Ljfp;->eiL:Z

    iput-boolean v0, p0, Ljfp;->eiM:Z

    iput-boolean v0, p0, Ljfp;->egu:Z

    iput-object v2, p0, Ljfp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfp;->eCz:I

    .line 10558
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 10458
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljfp;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfp;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljfp;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfp;->eiL:Z

    iget v0, p0, Ljfp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfp;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfp;->eiM:Z

    iget v0, p0, Ljfp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfp;->egu:Z

    iget v0, p0, Ljfp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfp;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfp;->efQ:I

    iget v0, p0, Ljfp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfp;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 10575
    iget-object v0, p0, Ljfp;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 10576
    const/4 v0, 0x1

    iget-object v1, p0, Ljfp;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 10578
    :cond_0
    iget v0, p0, Ljfp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 10579
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfp;->eiL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10581
    :cond_1
    iget v0, p0, Ljfp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 10582
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljfp;->eiM:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10584
    :cond_2
    iget v0, p0, Ljfp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 10585
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfp;->egu:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 10587
    :cond_3
    iget v0, p0, Ljfp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 10588
    const/4 v0, 0x5

    iget v1, p0, Ljfp;->efQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 10590
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10591
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 10595
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10596
    iget-object v1, p0, Ljfp;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 10597
    const/4 v1, 0x1

    iget-object v2, p0, Ljfp;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10600
    :cond_0
    iget v1, p0, Ljfp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 10601
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfp;->eiL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10604
    :cond_1
    iget v1, p0, Ljfp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 10605
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljfp;->eiM:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10608
    :cond_2
    iget v1, p0, Ljfp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 10609
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfp;->egu:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10612
    :cond_3
    iget v1, p0, Ljfp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    .line 10613
    const/4 v1, 0x5

    iget v2, p0, Ljfp;->efQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10616
    :cond_4
    return v0
.end method
