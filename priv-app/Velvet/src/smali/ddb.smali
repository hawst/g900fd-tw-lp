.class public Lddb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bqt:Lcom/google/android/shared/util/BitFlags;

.field private mEventBus:Ldda;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lddb;-><init>(Ldda;I)V

    .line 453
    return-void
.end method

.method constructor <init>(Ldda;I)V
    .locals 6

    .prologue
    .line 445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 446
    iput-object p1, p0, Lddb;->mEventBus:Ldda;

    .line 447
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    const-class v1, Lddj;

    const-string v2, ""

    int-to-long v4, p2

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;Ljava/lang/String;J)V

    iput-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 448
    return-void
.end method


# virtual methods
.method public final Nf()Ldda;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lddb;->mEventBus:Ldda;

    return-object v0
.end method

.method public final aaA()Z
    .locals 4

    .prologue
    .line 492
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaB()Z
    .locals 4

    .prologue
    .line 499
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaC()Z
    .locals 4

    .prologue
    .line 506
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaD()Z
    .locals 4

    .prologue
    .line 513
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaE()Z
    .locals 4

    .prologue
    .line 520
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaF()Z
    .locals 4

    .prologue
    .line 527
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaG()Z
    .locals 4

    .prologue
    .line 534
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaH()Z
    .locals 4

    .prologue
    .line 548
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaI()Lcom/google/android/shared/util/BitFlags;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    return-object v0
.end method

.method public final aaw()Z
    .locals 4

    .prologue
    .line 464
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aax()Z
    .locals 4

    .prologue
    .line 471
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aay()Z
    .locals 4

    .prologue
    .line 478
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aaz()Z
    .locals 4

    .prologue
    .line 485
    iget-object v0, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final c(Ldda;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lddb;->mEventBus:Ldda;

    .line 559
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 563
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Event"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lddb;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
