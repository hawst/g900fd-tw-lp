.class public Lbtf;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbuh;
.implements Lbuj;


# instance fields
.field private aIV:Lbtg;

.field private aIW:Lbti;

.field private aIX:Lbth;

.field private aIY:Lbuo;

.field private aIZ:Ljava/util/List;

.field private aJa:Landroid/widget/ListView;

.field public aJb:Landroid/widget/FrameLayout;

.field private aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

.field private aJd:Lbtv;

.field private aJe:Lbtm;

.field private aJf:I

.field private aJg:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

.field private aJh:Z

.field private aJi:Landroid/view/ViewGroup;

.field private aJj:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

.field private aJk:Lbtk;

.field public aJl:Landroid/view/View;

.field private aJm:Lbtj;

.field private aJn:Z

.field private aJo:Z

.field private aJp:I

.field private aJq:I

.field private aJr:Z

.field private mClient:Lbhi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbtf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0, p1, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    iput-boolean v1, p0, Lbtf;->aJn:Z

    .line 76
    iput-boolean v1, p0, Lbtf;->aJo:Z

    .line 89
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 90
    new-array v0, v1, [I

    const v1, 0x7f01004e

    aput v1, v0, v2

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 92
    invoke-static {}, Lbtf;->AK()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lbtf;->aJr:Z

    .line 93
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040004

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f110402

    invoke-virtual {p0, v0}, Lbtf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1103e4

    invoke-virtual {p0, v0}, Lbtf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, p0, Lbtf;->aJj:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v0, p0, Lbtf;->aJj:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f11005a

    invoke-virtual {p0, v0}, Lbtf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iput-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-boolean v1, p0, Lbtf;->aJr:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->bT(Z)V

    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbuj;)V

    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbuh;)V

    const v0, 0x7f110055

    invoke-virtual {p0, v0}, Lbtf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbtf;->aJa:Landroid/widget/ListView;

    iget-object v0, p0, Lbtf;->aJa:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f11005b

    invoke-virtual {p0, v0}, Lbtf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    iput-object v0, p0, Lbtf;->aJg:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    const/4 v0, -0x1

    iput v0, p0, Lbtf;->aJf:I

    const v0, 0x7f110059

    invoke-virtual {p0, v0}, Lbtf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v2}, Lbtf;->setNavigationMode(I)V

    .line 95
    return-void
.end method

.method public static AK()Z
    .locals 2

    .prologue
    .line 727
    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lbuo;Z)V
    .locals 3

    .prologue
    .line 459
    iget-object v0, p0, Lbtf;->aIY:Lbuo;

    .line 460
    iput-object p1, p0, Lbtf;->aIY:Lbuo;

    .line 461
    iget-object v1, p0, Lbtf;->aIZ:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 462
    iget-object v1, p0, Lbtf;->aIZ:Ljava/util/List;

    iget-object v2, p0, Lbtf;->aIY:Lbuo;

    invoke-static {v1, v0, v2}, Lbtv;->a(Ljava/util/List;Lbuo;Lbuo;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbtf;->aIZ:Ljava/util/List;

    .line 463
    if-nez p2, :cond_0

    .line 464
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lbtf;->aIY:Lbuo;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->c(Lbuo;)V

    .line 466
    :cond_0
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    iget-object v1, p0, Lbtf;->aIZ:Ljava/util/List;

    invoke-virtual {v0, v1}, Lbtv;->m(Ljava/util/List;)V

    .line 471
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->c(Lbuo;)V

    goto :goto_0
.end method

.method private a(ZLandroid/view/animation/Interpolator;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 601
    if-eqz p1, :cond_0

    move v0, v1

    move v3, v2

    .line 608
    :goto_0
    iget-object v4, p0, Lbtf;->aJg:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    const-string v5, "animatedHeightFraction"

    const/4 v6, 0x2

    new-array v6, v6, [F

    int-to-float v3, v3

    aput v3, v6, v2

    int-to-float v0, v0

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 610
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 611
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 612
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 613
    return-void

    :cond_0
    move v0, v2

    move v3, v1

    .line 606
    goto :goto_0
.end method

.method private bO(Z)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    const v5, 0x3f4ccccd    # 0.8f

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 569
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getNavigationMode()I

    move-result v0

    .line 570
    packed-switch v0, :pswitch_data_0

    .line 597
    :goto_0
    return-void

    .line 572
    :pswitch_0
    if-eqz p1, :cond_0

    .line 573
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 574
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 575
    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 576
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v4, v0}, Lbtf;->a(ZLandroid/view/animation/Interpolator;)V

    .line 580
    :goto_1
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lbtf;->aJg:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    .line 578
    :cond_0
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 585
    :pswitch_1
    if-eqz p1, :cond_1

    .line 586
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 587
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 588
    const-wide/16 v2, 0x85

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 589
    const/4 v0, 0x1

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v0, v1}, Lbtf;->a(ZLandroid/view/animation/Interpolator;)V

    .line 593
    :goto_2
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 594
    iget-object v0, p0, Lbtf;->aJg:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    .line 591
    :cond_1
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    .line 570
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private ef(I)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 381
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lbtf;->aJq:I

    .line 382
    return-void
.end method

.method private eg(I)V
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setNavigationMode(I)V

    .line 557
    return-void
.end method

.method private k(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 374
    invoke-virtual {p1, p2}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 375
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lbtf;->aJp:I

    .line 377
    return-void
.end method


# virtual methods
.method public final AJ()V
    .locals 2

    .prologue
    .line 561
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbtf;->bO(Z)V

    .line 562
    iget-object v0, p0, Lbtf;->aJm:Lbtj;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lbtf;->aJm:Lbtj;

    iget-object v1, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getNavigationMode()I

    move-result v1

    invoke-interface {v0, v1}, Lbtj;->eh(I)V

    .line 566
    :cond_0
    return-void
.end method

.method public final a(Lbtg;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lbtf;->aIV:Lbtg;

    .line 511
    return-void
.end method

.method public final a(Lbth;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lbtf;->aIX:Lbth;

    .line 504
    return-void
.end method

.method public final a(Lbti;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lbtf;->aIW:Lbti;

    .line 497
    return-void
.end method

.method public final a(Lbtj;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lbtf;->aJm:Lbtj;

    .line 525
    return-void
.end method

.method public final a(Lbtk;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lbtf;->aJk:Lbtk;

    .line 518
    return-void
.end method

.method public final a(Lbuo;)V
    .locals 2

    .prologue
    .line 649
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbtf;->a(Lbuo;Z)V

    .line 650
    iget-object v0, p0, Lbtf;->aIV:Lbtg;

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lbtf;->aIV:Lbtg;

    iget-object v1, p0, Lbtf;->aIY:Lbuo;

    invoke-interface {v0, v1}, Lbtg;->b(Lbuo;)V

    .line 653
    :cond_0
    return-void
.end method

.method public final a(Lbuo;Lbuo;)V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbuo;Lbuo;)V

    .line 490
    return-void
.end method

.method public final a(Ljava/util/List;Lbuo;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 265
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    if-nez v0, :cond_0

    new-instance v0, Lbtv;

    invoke-virtual {p0}, Lbtf;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lbtf;->aJf:I

    invoke-direct {v0, v1, v2, v3, v3}, Lbtv;-><init>(Landroid/content/Context;ILbtz;Lbtx;)V

    iput-object v0, p0, Lbtf;->aJd:Lbtv;

    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0, v4}, Lbtv;->bS(Z)V

    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    iget-object v1, p0, Lbtf;->aJe:Lbtm;

    invoke-virtual {v0, v1}, Lbtv;->a(Lbtm;)V

    iget-object v0, p0, Lbtf;->aJa:Landroid/widget/ListView;

    iget-object v1, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    iget-boolean v1, p0, Lbtf;->aJn:Z

    invoke-virtual {v0, v1}, Lbtv;->bQ(Z)V

    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    iget-boolean v1, p0, Lbtf;->aJo:Z

    invoke-virtual {v0, v1}, Lbtv;->bR(Z)V

    :cond_0
    iput-object p1, p0, Lbtf;->aIZ:Ljava/util/List;

    iget-object v0, p0, Lbtf;->aIZ:Ljava/util/List;

    if-nez v0, :cond_1

    iput-object v3, p0, Lbtf;->aIY:Lbuo;

    :cond_1
    invoke-direct {p0, p2, v4}, Lbtf;->a(Lbuo;Z)V

    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    iget-object v1, p0, Lbtf;->aIZ:Ljava/util/List;

    invoke-virtual {v0, v1}, Lbtv;->m(Ljava/util/List;)V

    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbuo;Lbuo;)V

    .line 266
    return-void
.end method

.method public final bN(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 122
    iget-boolean v0, p0, Lbtf;->aJh:Z

    if-eq v0, p1, :cond_0

    .line 123
    iput-boolean p1, p0, Lbtf;->aJh:Z

    .line 124
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->offsetTopAndBottom(I)V

    .line 125
    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->offsetTopAndBottom(I)V

    .line 126
    iget-boolean v0, p0, Lbtf;->aJh:Z

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 133
    :goto_0
    iput v1, p0, Lbtf;->aJp:I

    .line 134
    iput v1, p0, Lbtf;->aJq:I

    .line 136
    :cond_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final disconnect()V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0}, Lbtv;->disconnect()V

    .line 662
    :cond_0
    return-void
.end method

.method public final f(Lbhi;)V
    .locals 3

    .prologue
    .line 641
    iput-object p1, p0, Lbtf;->mClient:Lbhi;

    .line 642
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lbtf;->mClient:Lbhi;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->f(Lbhi;)V

    .line 643
    new-instance v0, Lbtm;

    invoke-virtual {p0}, Lbtf;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lbtf;->mClient:Lbhi;

    invoke-direct {v0, v1, v2}, Lbtm;-><init>(Landroid/content/Context;Lbhi;)V

    iput-object v0, p0, Lbtf;->aJe:Lbtm;

    .line 644
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lbtf;->aJe:Lbtm;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbtm;)V

    .line 645
    return-void
.end method

.method public getNestedScrollAxes()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x2

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 666
    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_1

    .line 667
    iget-object v0, p0, Lbtf;->aJk:Lbtk;

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lbtf;->aJk:Lbtk;

    invoke-interface {v0}, Lbtk;->AN()V

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 670
    :cond_1
    iget-object v0, p0, Lbtf;->aJj:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    if-ne p1, v0, :cond_0

    .line 671
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getNavigationMode()I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lbtf;->eg(I)V

    .line 675
    iget-object v0, p0, Lbtf;->aJj:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v3, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getNavigationMode()I

    move-result v3

    if-ne v3, v2, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->bP(Z)V

    .line 678
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {p0}, Lbtf;->AJ()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 671
    goto :goto_1

    :cond_3
    move v2, v1

    .line 675
    goto :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0, p3}, Lbtv;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 618
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0, p3}, Lbtv;->ei(I)Lbuo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbtf;->a(Lbuo;Z)V

    .line 620
    iget-object v0, p0, Lbtf;->aIV:Lbtg;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lbtf;->aIV:Lbtg;

    iget-object v1, p0, Lbtf;->aIY:Lbuo;

    invoke-interface {v0, v1}, Lbtg;->b(Lbuo;)V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0, p3}, Lbtv;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 624
    iget-object v0, p0, Lbtf;->aIX:Lbth;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lbtf;->aIX:Lbth;

    invoke-interface {v0}, Lbth;->AL()V

    goto :goto_0

    .line 627
    :cond_2
    iget-object v0, p0, Lbtf;->aJd:Lbtv;

    invoke-virtual {v0, p3}, Lbtv;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 628
    iget-object v0, p0, Lbtf;->aIW:Lbti;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lbtf;->aIW:Lbti;

    invoke-interface {v0}, Lbti;->AM()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 290
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 295
    iget-boolean v0, p0, Lbtf;->aJh:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    .line 296
    :goto_0
    iget v1, p0, Lbtf;->aJp:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 297
    iget v1, p0, Lbtf;->aJp:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 299
    :cond_0
    iget v0, p0, Lbtf;->aJq:I

    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 300
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    iget v1, p0, Lbtf;->aJq:I

    iget-object v2, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 302
    :cond_1
    return-void

    .line 295
    :cond_2
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    .line 270
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 271
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lbtf;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 272
    invoke-virtual {p0, v0}, Lbtf;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 273
    iget-object v2, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 274
    iget-boolean v0, p0, Lbtf;->aJh:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 276
    :goto_1
    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 280
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lbtf;->getHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->measure(II)V

    .line 286
    :cond_0
    return-void

    .line 274
    :cond_1
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getMeasuredHeight()I

    move-result v0

    goto :goto_1

    .line 271
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 309
    iget-boolean v0, p0, Lbtf;->aJh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    .line 310
    :goto_0
    if-nez p4, :cond_1

    cmpg-float v1, p3, v2

    if-gez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-gez v1, :cond_1

    .line 312
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    neg-int v1, v1

    invoke-direct {p0, v0, v1}, Lbtf;->k(Landroid/view/View;I)V

    .line 313
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0, v0}, Lbtf;->ef(I)V

    .line 314
    const/4 v0, 0x1

    .line 326
    :goto_1
    return v0

    .line 309
    :cond_0
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 315
    :cond_1
    if-eqz p4, :cond_3

    cmpl-float v1, p3, v2

    if-lez v1, :cond_3

    .line 316
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_2

    .line 320
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lbtf;->k(Landroid/view/View;I)V

    .line 322
    :cond_2
    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_3

    .line 323
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lbtf;->ef(I)V

    .line 326
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 339
    iget-boolean v0, p0, Lbtf;->aJh:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    .line 340
    :goto_0
    iget-object v1, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getNavigationMode()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 371
    :cond_0
    :goto_1
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 345
    :cond_2
    if-lez p3, :cond_6

    .line 347
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-lez v1, :cond_6

    .line 349
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-le v1, p3, :cond_3

    .line 350
    neg-int v1, p3

    .line 356
    :goto_2
    if-eqz v1, :cond_0

    .line 358
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_4

    .line 359
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0, v0, v3}, Lbtf;->k(Landroid/view/View;I)V

    .line 363
    :goto_3
    iget-object v3, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_5

    .line 364
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v3, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lbtf;->ef(I)V

    .line 368
    :goto_4
    aput v2, p4, v2

    .line 369
    aput v1, p4, v5

    goto :goto_1

    .line 352
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    goto :goto_2

    .line 361
    :cond_4
    invoke-direct {p0, v0, v1}, Lbtf;->k(Landroid/view/View;I)V

    goto :goto_3

    .line 366
    :cond_5
    invoke-direct {p0, v1}, Lbtf;->ef(I)V

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 392
    const/4 v0, 0x0

    .line 393
    iget-boolean v1, p0, Lbtf;->aJh:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbtf;->aJi:Landroid/view/ViewGroup;

    .line 394
    :goto_0
    if-gez p5, :cond_5

    .line 395
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-gez v2, :cond_5

    .line 396
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    if-gt p5, v0, :cond_0

    .line 397
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result p5

    .line 403
    :cond_0
    :goto_1
    if-eqz p5, :cond_1

    .line 405
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    if-lez v0, :cond_3

    .line 406
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0, v1, v0}, Lbtf;->k(Landroid/view/View;I)V

    .line 410
    :goto_2
    iget-object v0, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-le v0, v2, :cond_4

    .line 411
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lbtf;->aJb:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lbtf;->ef(I)V

    .line 416
    :cond_1
    :goto_3
    return-void

    .line 393
    :cond_2
    iget-object v1, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 408
    :cond_3
    neg-int v0, p5

    invoke-direct {p0, v1, v0}, Lbtf;->k(Landroid/view/View;I)V

    goto :goto_2

    .line 413
    :cond_4
    neg-int v0, p5

    invoke-direct {p0, v0}, Lbtf;->ef(I)V

    goto :goto_3

    :cond_5
    move p5, v0

    goto :goto_1
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    return v0
.end method

.method public final setNavigationMode(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 544
    invoke-direct {p0, p1}, Lbtf;->eg(I)V

    .line 545
    invoke-direct {p0, v1}, Lbtf;->bO(Z)V

    .line 546
    iget-object v2, p0, Lbtf;->aJj:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v3, p0, Lbtf;->aJc:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getNavigationMode()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->bP(Z)V

    .line 549
    return-void

    :cond_0
    move v0, v1

    .line 546
    goto :goto_0
.end method
