.class public final Lta;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final synthetic AQ:Lcom/android/launcher3/CellLayout;

.field BA:F

.field BB:F

.field BC:F

.field BD:Z

.field public BE:Landroid/animation/Animator;

.field public Bw:Landroid/view/View;

.field public Bx:F

.field public By:F

.field Bz:F

.field public mode:I


# direct methods
.method public constructor <init>(Lcom/android/launcher3/CellLayout;Landroid/view/View;IIIIIII)V
    .locals 10

    .prologue
    .line 2181
    iput-object p1, p0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lta;->BD:Z

    .line 2182
    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->f(Lcom/android/launcher3/CellLayout;)[I

    move-result-object v5

    move-object v0, p1

    move v1, p4

    move v2, p5

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(IIII[I)V

    .line 2183
    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->f(Lcom/android/launcher3/CellLayout;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 2184
    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->f(Lcom/android/launcher3/CellLayout;)[I

    move-result-object v0

    const/4 v1, 0x1

    aget v7, v0, v1

    .line 2185
    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->f(Lcom/android/launcher3/CellLayout;)[I

    move-result-object v5

    move-object v0, p1

    move/from16 v1, p6

    move/from16 v2, p7

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(IIII[I)V

    .line 2186
    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->f(Lcom/android/launcher3/CellLayout;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2187
    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->f(Lcom/android/launcher3/CellLayout;)[I

    move-result-object v1

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 2188
    sub-int v2, v0, v6

    .line 2189
    sub-int/2addr v1, v7

    .line 2190
    const/4 v0, 0x0

    iput v0, p0, Lta;->Bx:F

    .line 2191
    const/4 v0, 0x0

    iput v0, p0, Lta;->By:F

    .line 2192
    if-nez p3, :cond_2

    const/4 v0, -0x1

    .line 2193
    :goto_0
    if-ne v2, v1, :cond_0

    if-eqz v2, :cond_1

    .line 2195
    :cond_0
    if-nez v1, :cond_3

    .line 2196
    neg-int v0, v0

    int-to-float v0, v0

    int-to-float v1, v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->g(Lcom/android/launcher3/CellLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lta;->Bx:F

    .line 2207
    :cond_1
    :goto_1
    iput p3, p0, Lta;->mode:I

    .line 2208
    invoke-virtual {p2}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iput v0, p0, Lta;->Bz:F

    .line 2209
    invoke-virtual {p2}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iput v0, p0, Lta;->BA:F

    .line 2210
    invoke-virtual {p1}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lta;->BB:F

    .line 2211
    invoke-virtual {p2}, Landroid/view/View;->getScaleX()F

    move-result v0

    iput v0, p0, Lta;->BC:F

    .line 2212
    iput-object p2, p0, Lta;->Bw:Landroid/view/View;

    .line 2213
    return-void

    .line 2192
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 2197
    :cond_3
    if-nez v2, :cond_4

    .line 2198
    neg-int v0, v0

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->g(Lcom/android/launcher3/CellLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lta;->By:F

    goto :goto_1

    .line 2200
    :cond_4
    int-to-float v3, v1

    int-to-float v4, v2

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    .line 2201
    neg-int v3, v0

    int-to-float v3, v3

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->g(Lcom/android/launcher3/CellLayout;)F

    move-result v8

    float-to-double v8, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    mul-double/2addr v2, v6

    double-to-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lta;->Bx:F

    .line 2203
    neg-int v0, v0

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {p1}, Lcom/android/launcher3/CellLayout;->g(Lcom/android/launcher3/CellLayout;)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lta;->By:F

    goto :goto_1
.end method


# virtual methods
.method public fl()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2268
    iget-object v0, p0, Lta;->BE:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 2269
    iget-object v0, p0, Lta;->BE:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 2272
    :cond_0
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 2273
    iput-object v0, p0, Lta;->BE:Landroid/animation/Animator;

    .line 2274
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lta;->Bw:Landroid/view/View;

    const-string v3, "scaleX"

    new-array v4, v7, [F

    iget-object v5, p0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v5

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lta;->Bw:Landroid/view/View;

    const-string v3, "scaleY"

    new-array v4, v7, [F

    iget-object v5, p0, Lta;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher3/CellLayout;->eM()F

    move-result v5

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    iget-object v3, p0, Lta;->Bw:Landroid/view/View;

    const-string v4, "translationX"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lta;->Bw:Landroid/view/View;

    const-string v4, "translationY"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2280
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2281
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2282
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2283
    return-void
.end method
