.class public Lgnj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final Oq:I

.field private final aTr:Ljava/lang/String;

.field private final aTs:Ljava/lang/String;

.field private final anC:I

.field private final aok:Z

.field private final axT:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bZx:Landroid/location/Location;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bhQ:F

.field private final bhR:[F

.field private final bhS:[F

.field private final bhT:[F

.field private final cFP:Z

.field private final cFx:Z

.field private final cOM:Lgjl;

.field private final cPA:I

.field private final cPY:Lgmx;

.field private final cPZ:Z

.field private final cPu:Z

.field private final cPw:Ljtp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cQa:Z

.field private final cQb:Ljava/lang/String;

.field private final cQc:Ljava/util/List;

.field private final cQd:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cQe:Lgjo;

.field private final cQf:Z

.field private final cQg:Z

.field private final cQh:Z

.field private final cQi:Z

.field private final cQj:Z

.field private final cQk:I

.field private final cQl:I

.field private final mRequestId:Ljava/lang/String;


# direct methods
.method constructor <init>(ILgmx;ZZZLjava/lang/String;Ljava/util/List;Ljava/lang/String;Lgjl;Lgjo;ZIF[F[F[FZZZZILandroid/location/Location;Ljtp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput p1, p0, Lgnj;->Oq:I

    .line 136
    iput-object p2, p0, Lgnj;->cPY:Lgmx;

    .line 137
    iput-boolean p3, p0, Lgnj;->cPZ:Z

    .line 138
    iput-boolean p4, p0, Lgnj;->cQa:Z

    .line 139
    iput-boolean p5, p0, Lgnj;->cFP:Z

    .line 140
    iput-object p6, p0, Lgnj;->cQb:Ljava/lang/String;

    .line 141
    iput-object p7, p0, Lgnj;->cQc:Ljava/util/List;

    .line 142
    iput-object p8, p0, Lgnj;->cQd:Ljava/lang/String;

    .line 143
    iput-object p9, p0, Lgnj;->cOM:Lgjl;

    .line 144
    iput-object p10, p0, Lgnj;->cQe:Lgjo;

    .line 145
    iput-boolean p11, p0, Lgnj;->cQf:Z

    .line 146
    iput p12, p0, Lgnj;->anC:I

    .line 147
    iput p13, p0, Lgnj;->bhQ:F

    .line 148
    iput-object p14, p0, Lgnj;->bhR:[F

    .line 149
    move-object/from16 v0, p15

    iput-object v0, p0, Lgnj;->bhS:[F

    .line 150
    move-object/from16 v0, p16

    iput-object v0, p0, Lgnj;->bhT:[F

    .line 151
    move/from16 v0, p17

    iput-boolean v0, p0, Lgnj;->cQg:Z

    .line 152
    move/from16 v0, p18

    iput-boolean v0, p0, Lgnj;->cQh:Z

    .line 153
    move/from16 v0, p19

    iput-boolean v0, p0, Lgnj;->cPu:Z

    .line 154
    move/from16 v0, p20

    iput-boolean v0, p0, Lgnj;->cQi:Z

    .line 155
    move/from16 v0, p21

    iput v0, p0, Lgnj;->cPA:I

    .line 156
    move-object/from16 v0, p22

    iput-object v0, p0, Lgnj;->bZx:Landroid/location/Location;

    .line 157
    move-object/from16 v0, p23

    iput-object v0, p0, Lgnj;->cPw:Ljtp;

    .line 158
    move-object/from16 v0, p24

    iput-object v0, p0, Lgnj;->aTs:Ljava/lang/String;

    .line 159
    move-object/from16 v0, p25

    iput-object v0, p0, Lgnj;->mRequestId:Ljava/lang/String;

    .line 160
    move-object/from16 v0, p26

    iput-object v0, p0, Lgnj;->aTr:Ljava/lang/String;

    .line 161
    move/from16 v0, p27

    iput-boolean v0, p0, Lgnj;->cQj:Z

    .line 162
    move/from16 v0, p28

    iput-boolean v0, p0, Lgnj;->cFx:Z

    .line 163
    move/from16 v0, p29

    iput v0, p0, Lgnj;->cQk:I

    .line 164
    move/from16 v0, p30

    iput v0, p0, Lgnj;->cQl:I

    .line 165
    move/from16 v0, p31

    iput-boolean v0, p0, Lgnj;->aok:Z

    .line 166
    move-object/from16 v0, p32

    iput-object v0, p0, Lgnj;->axT:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public static kr(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ltz p0, :cond_2

    const/16 v0, 0x9

    if-gt p0, v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Unsupported mode"

    invoke-static {v0, v3}, Lifv;->c(ZLjava/lang/Object;)V

    .line 71
    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-ne p0, v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 70
    goto :goto_0
.end method

.method public static ks(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ltz p0, :cond_2

    const/16 v0, 0x9

    if-gt p0, v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Unsupported mode"

    invoke-static {v0, v3}, Lifv;->c(ZLjava/lang/Object;)V

    .line 76
    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-ne p0, v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 75
    goto :goto_0
.end method


# virtual methods
.method public final FH()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lgnj;->aok:Z

    return v0
.end method

.method public final HL()[F
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lgnj;->bhS:[F

    return-object v0
.end method

.method public final HM()[F
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lgnj;->bhT:[F

    return-object v0
.end method

.method public final Hq()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lgnj;->cFx:Z

    return v0
.end method

.method public final aHC()Lgmx;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lgnj;->cPY:Lgmx;

    return-object v0
.end method

.method public final aHD()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lgnj;->cPZ:Z

    return v0
.end method

.method public final aHE()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lgnj;->cQa:Z

    return v0
.end method

.method public final aHF()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lgnj;->cFP:Z

    return v0
.end method

.method public final aHG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lgnj;->cQb:Ljava/lang/String;

    return-object v0
.end method

.method public final aHH()Ljava/util/List;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lgnj;->cQc:Ljava/util/List;

    return-object v0
.end method

.method public final aHI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lgnj;->cQd:Ljava/lang/String;

    return-object v0
.end method

.method public final aHJ()Lgjl;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lgnj;->cOM:Lgjl;

    return-object v0
.end method

.method public final aHK()Lgjo;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lgnj;->cQe:Lgjo;

    return-object v0
.end method

.method public final aHL()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lgnj;->cQf:Z

    return v0
.end method

.method public final aHM()F
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lgnj;->bhQ:F

    return v0
.end method

.method public final aHN()[F
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lgnj;->bhR:[F

    return-object v0
.end method

.method public final aHO()Z
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lgnj;->cQh:Z

    return v0
.end method

.method public final aHP()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lgnj;->cPu:Z

    return v0
.end method

.method public final aHQ()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lgnj;->cPA:I

    return v0
.end method

.method public final aHR()Ljtp;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lgnj;->cPw:Ljtp;

    return-object v0
.end method

.method public final aHS()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lgnj;->cQj:Z

    return v0
.end method

.method public final aHT()I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lgnj;->cQk:I

    return v0
.end method

.method public final aHU()I
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lgnj;->cQl:I

    return v0
.end method

.method public final aHV()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 323
    iget v1, p0, Lgnj;->Oq:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lgnj;->Oq:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lgnj;->Oq:I

    if-eqz v1, :cond_0

    iget v1, p0, Lgnj;->Oq:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqW()Landroid/location/Location;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lgnj;->bZx:Landroid/location/Location;

    return-object v0
.end method

.method public final asQ()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lgnj;->anC:I

    return v0
.end method

.method public final b(Lgdo;)I
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lgnj;->cQe:Lgjo;

    sget-object v1, Lgjo;->cMK:Lgjo;

    if-ne v0, v1, :cond_1

    .line 331
    iget v0, p0, Lgnj;->Oq:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 332
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNg:Lkaa;

    invoke-virtual {v0}, Lkaa;->bwW()I

    move-result v0

    .line 343
    :goto_0
    return v0

    .line 335
    :cond_0
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNf:Ljzi;

    invoke-virtual {v0}, Ljzi;->bwW()I

    move-result v0

    goto :goto_0

    .line 339
    :cond_1
    iget-object v0, p0, Lgnj;->cQe:Lgjo;

    sget-object v1, Lgjo;->cMP:Lgjo;

    if-ne v0, v1, :cond_2

    .line 340
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNg:Lkaa;

    invoke-virtual {v0}, Lkaa;->bwW()I

    move-result v0

    goto :goto_0

    .line 343
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final c(Lgdo;)Ljzk;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 348
    iget v0, p0, Lgnj;->Oq:I

    packed-switch v0, :pswitch_data_0

    .line 365
    :pswitch_0
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNg:Lkaa;

    iget-object v0, v0, Lkaa;->eNm:Ljzk;

    .line 368
    :goto_0
    iget-boolean v1, p0, Lgnj;->cQg:Z

    if-nez v1, :cond_0

    .line 369
    const/16 v1, 0x4e20

    invoke-virtual {v0, v1}, Ljzk;->tu(I)Ljzk;

    .line 371
    :cond_0
    return-object v0

    .line 350
    :pswitch_1
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNf:Ljzi;

    iget-object v0, v0, Ljzi;->eNm:Ljzk;

    goto :goto_0

    .line 353
    :pswitch_2
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNe:Ljzn;

    iget-object v0, v0, Ljzn;->eNm:Ljzk;

    goto :goto_0

    .line 356
    :pswitch_3
    iget-boolean v0, p0, Lgnj;->cFP:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNh:Ljzy;

    iget-object v0, v0, Ljzy;->eNm:Ljzk;

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNf:Ljzi;

    iget-object v0, v0, Ljzi;->eNm:Ljzk;

    goto :goto_0

    .line 362
    :pswitch_4
    invoke-interface {p1}, Lgdo;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNg:Lkaa;

    iget-object v0, v0, Lkaa;->eNm:Ljzk;

    goto :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final getMode()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lgnj;->Oq:I

    return v0
.end method

.method public final isSuggestionsEnabled()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lgnj;->cQi:Z

    return v0
.end method

.method public final tF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lgnj;->aTr:Ljava/lang/String;

    return-object v0
.end method

.method public final xL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lgnj;->axT:Ljava/lang/String;

    return-object v0
.end method

.method public final xU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lgnj;->aTs:Ljava/lang/String;

    return-object v0
.end method

.method public final zK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lgnj;->mRequestId:Ljava/lang/String;

    return-object v0
.end method
