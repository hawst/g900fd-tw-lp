.class public final Leus;
.super Lenp;
.source "PG"


# instance fields
.field private final aTr:Ljava/lang/String;

.field private synthetic cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

.field private final cjp:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 145
    iput-object p1, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    .line 146
    const-string v0, "LoadServiceUrl"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, p2, p3, v1}, Lenp;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    .line 147
    iput-object p4, p0, Leus;->cjp:Landroid/net/Uri;

    .line 148
    iput-object p5, p0, Leus;->aTr:Ljava/lang/String;

    .line 149
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v1, v1, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mLoginHelper:Lcrh;

    iget-object v2, p0, Leus;->cjp:Landroid/net/Uri;

    iget-object v3, p0, Leus;->aTr:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcrh;->c(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    iget-object v0, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v0, v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    iget-object v0, v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->dz:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 140
    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->loadUrl(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "GoogleServiceWebviewWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get login link for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Leus;->aTr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Leus;->cjp:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0386

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Leus;->cjo:Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    goto :goto_0
.end method
