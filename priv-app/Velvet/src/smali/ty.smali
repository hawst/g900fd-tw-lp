.class public final Lty;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static Er:I

.field public static Es:I


# instance fields
.field private AC:Z

.field private EA:Ljava/util/ArrayList;

.field private EB:Luo;

.field private EC:Landroid/os/IBinder;

.field private ED:Landroid/view/View;

.field private EE:Landroid/view/View;

.field private EF:Luh;

.field private EG:Lua;

.field private EH:Luo;

.field private EI:Landroid/view/inputmethod/InputMethodManager;

.field private EJ:[I

.field private EK:J

.field private EL:I

.field private EM:Landroid/graphics/Rect;

.field private EN:I

.field private Et:Landroid/graphics/Rect;

.field private final Eu:[I

.field private Ev:I

.field private Ew:I

.field private Ex:I

.field private Ey:Luq;

.field private Ez:Ljava/util/ArrayList;

.field private fW:Landroid/view/VelocityTracker;

.field private hd:I

.field private mHandler:Landroid/os/Handler;

.field private xZ:Lcom/android/launcher3/Launcher;

.field private zU:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    sput v0, Lty;->Er:I

    .line 44
    const/4 v0, 0x1

    sput v0, Lty;->Es:I

    return-void
.end method

.method public constructor <init>(Lcom/android/launcher3/Launcher;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lty;->Et:Landroid/graphics/Rect;

    .line 65
    new-array v0, v2, [I

    iput-object v0, p0, Lty;->Eu:[I

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lty;->Ez:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lty;->EA:Ljava/util/ArrayList;

    .line 97
    iput v3, p0, Lty;->hd:I

    .line 98
    new-instance v0, Lua;

    invoke-direct {v0, p0}, Lua;-><init>(Lty;)V

    iput-object v0, p0, Lty;->EG:Lua;

    .line 104
    new-array v0, v2, [I

    iput-object v0, p0, Lty;->EJ:[I

    .line 105
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lty;->EK:J

    .line 106
    iput v3, p0, Lty;->EL:I

    .line 108
    new-array v0, v2, [I

    iput-object v0, p0, Lty;->zU:[I

    .line 109
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lty;->EM:Landroid/graphics/Rect;

    .line 140
    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 141
    iput-object p1, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    .line 142
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lty;->mHandler:Landroid/os/Handler;

    .line 143
    const v1, 0x7f0d0080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lty;->Ex:I

    .line 144
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lty;->fW:Landroid/view/VelocityTracker;

    .line 146
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 147
    const v2, 0x7f0c000b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lty;->EN:I

    .line 149
    return-void
.end method

.method private E(II)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 481
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-object v0, v0, Luq;->Gd:Luj;

    invoke-virtual {v0, p1, p2}, Luj;->H(II)V

    .line 484
    iget-object v0, p0, Lty;->Eu:[I

    .line 485
    invoke-direct {p0, p1, p2, v0}, Lty;->d(II[I)Luo;

    move-result-object v1

    .line 486
    iget-object v2, p0, Lty;->Ey:Luq;

    aget v3, v0, v6

    iput v3, v2, Luq;->x:I

    .line 487
    iget-object v2, p0, Lty;->Ey:Luq;

    aget v0, v0, v7

    iput v0, v2, Luq;->y:I

    .line 488
    invoke-direct {p0, v1}, Lty;->a(Luo;)V

    .line 491
    iget v0, p0, Lty;->EL:I

    int-to-double v0, v0

    iget-object v2, p0, Lty;->EJ:[I

    aget v2, v2, v6

    sub-int/2addr v2, p1

    int-to-double v2, v2

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget-object v4, p0, Lty;->EJ:[I

    aget v4, v4, v7

    sub-int/2addr v4, p2

    int-to-double v4, v4

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lty;->EL:I

    .line 493
    iget-object v0, p0, Lty;->EJ:[I

    aput p1, v0, v6

    .line 494
    iget-object v0, p0, Lty;->EJ:[I

    aput p2, v0, v7

    .line 495
    invoke-direct {p0, p1, p2}, Lty;->F(II)V

    .line 496
    return-void
.end method

.method private F(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 524
    iget-object v0, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    move-result v0

    .line 525
    iget v3, p0, Lty;->EL:I

    if-ge v3, v0, :cond_1

    const/16 v0, 0x384

    .line 526
    :goto_0
    iget-object v3, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v5

    .line 527
    invoke-virtual {v5}, Lcom/android/launcher3/DragLayer;->getLayoutDirection()I

    move-result v3

    if-ne v3, v2, :cond_2

    move v4, v2

    .line 528
    :goto_1
    if-eqz v4, :cond_3

    move v3, v2

    .line 529
    :goto_2
    if-eqz v4, :cond_4

    .line 531
    :goto_3
    iget v4, p0, Lty;->Ex:I

    if-ge p1, v4, :cond_5

    .line 532
    iget v1, p0, Lty;->hd:I

    if-nez v1, :cond_0

    .line 533
    iput v2, p0, Lty;->hd:I

    .line 534
    iget-object v1, p0, Lty;->EF:Luh;

    invoke-interface {v1, p1, p2, v3}, Luh;->h(III)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {v5}, Lcom/android/launcher3/DragLayer;->fP()V

    .line 536
    iget-object v1, p0, Lty;->EG:Lua;

    invoke-virtual {v1, v3}, Lua;->setDirection(I)V

    .line 537
    iget-object v1, p0, Lty;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lty;->EG:Lua;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 552
    :cond_0
    :goto_4
    return-void

    .line 525
    :cond_1
    const/16 v0, 0x1f4

    goto :goto_0

    :cond_2
    move v4, v1

    .line 527
    goto :goto_1

    :cond_3
    move v3, v1

    .line 528
    goto :goto_2

    :cond_4
    move v1, v2

    .line 529
    goto :goto_3

    .line 540
    :cond_5
    iget-object v3, p0, Lty;->ED:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, p0, Lty;->Ex:I

    sub-int/2addr v3, v4

    if-le p1, v3, :cond_6

    .line 541
    iget v3, p0, Lty;->hd:I

    if-nez v3, :cond_0

    .line 542
    iput v2, p0, Lty;->hd:I

    .line 543
    iget-object v2, p0, Lty;->EF:Luh;

    invoke-interface {v2, p1, p2, v1}, Luh;->h(III)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 544
    invoke-virtual {v5}, Lcom/android/launcher3/DragLayer;->fP()V

    .line 545
    iget-object v2, p0, Lty;->EG:Lua;

    invoke-virtual {v2, v1}, Lua;->setDirection(I)V

    .line 546
    iget-object v1, p0, Lty;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lty;->EG:Lua;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4

    .line 550
    :cond_6
    invoke-direct {p0}, Lty;->fI()V

    goto :goto_4
.end method

.method static synthetic a(Lty;I)I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lty;->hd:I

    return v0
.end method

.method private a(Lui;)Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 620
    iget-object v1, p0, Lty;->EB:Luo;

    if-nez v1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-object v0

    .line 621
    :cond_1
    invoke-interface {p1}, Lui;->el()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 623
    iget-object v1, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 624
    iget-object v2, p0, Lty;->fW:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v3, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 626
    iget-object v1, p0, Lty;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    iget v2, p0, Lty;->EN:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 628
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lty;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    iget-object v3, p0, Lty;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 630
    new-instance v2, Landroid/graphics/PointF;

    const/4 v3, 0x0

    const/high16 v4, -0x40800000    # -1.0f

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 631
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual {v1}, Landroid/graphics/PointF;->length()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/PointF;->length()F

    move-result v2

    mul-float/2addr v2, v4

    div-float v2, v3, v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 633
    float-to-double v2, v2

    const-wide v4, 0x4041800000000000L    # 35.0

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_0

    move-object v0, v1

    .line 634
    goto :goto_0
.end method

.method static synthetic a(Lty;)Luh;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lty;->EF:Luh;

    return-object v0
.end method

.method private a(Landroid/graphics/PointF;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 641
    iget-object v1, p0, Lty;->Eu:[I

    .line 643
    iget-object v3, p0, Lty;->Ey:Luq;

    aget v4, v1, v0

    iput v4, v3, Luq;->x:I

    .line 644
    iget-object v3, p0, Lty;->Ey:Luq;

    aget v1, v1, v2

    iput v1, v3, Luq;->y:I

    .line 648
    iget-object v1, p0, Lty;->EH:Luo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lty;->EB:Luo;

    iget-object v3, p0, Lty;->EH:Luo;

    if-eq v1, v3, :cond_0

    .line 649
    iget-object v1, p0, Lty;->EH:Luo;

    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v1, v3}, Luo;->e(Luq;)V

    .line 654
    :cond_0
    iget-object v1, p0, Lty;->EB:Luo;

    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v1, v3}, Luo;->c(Luq;)V

    .line 657
    iget-object v1, p0, Lty;->Ey:Luq;

    iput-boolean v2, v1, Luq;->Gc:Z

    .line 658
    iget-object v1, p0, Lty;->EB:Luo;

    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v1, v3}, Luo;->e(Luq;)V

    .line 659
    iget-object v1, p0, Lty;->EB:Luo;

    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v1, v3}, Luo;->a(Luq;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 660
    iget-object v0, p0, Lty;->EB:Luo;

    iget-object v1, p0, Lty;->Ey:Luq;

    iget-object v3, p0, Lty;->Ey:Luq;

    iget v3, v3, Luq;->x:I

    iget-object v3, p0, Lty;->Ey:Luq;

    iget v3, v3, Luq;->y:I

    invoke-interface {v0, v1, p1}, Luo;->a(Luq;Landroid/graphics/PointF;)V

    move v1, v2

    .line 664
    :goto_0
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-object v3, v0, Luq;->Gf:Lui;

    iget-object v0, p0, Lty;->EB:Luo;

    check-cast v0, Landroid/view/View;

    iget-object v4, p0, Lty;->Ey:Luq;

    invoke-interface {v3, v0, v4, v2, v1}, Lui;->a(Landroid/view/View;Luq;ZZ)V

    .line 666
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lty;II)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lty;->F(II)V

    return-void
.end method

.method private a(Luo;)V
    .locals 2

    .prologue
    .line 507
    if-eqz p1, :cond_3

    .line 508
    iget-object v0, p0, Lty;->EH:Luo;

    if-eq v0, p1, :cond_1

    .line 509
    iget-object v0, p0, Lty;->EH:Luo;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lty;->EH:Luo;

    iget-object v1, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v1}, Luo;->e(Luq;)V

    .line 512
    :cond_0
    iget-object v0, p0, Lty;->Ey:Luq;

    invoke-interface {p1, v0}, Luo;->c(Luq;)V

    .line 514
    :cond_1
    iget-object v0, p0, Lty;->Ey:Luq;

    invoke-interface {p1, v0}, Luo;->d(Luq;)V

    .line 520
    :cond_2
    :goto_0
    iput-object p1, p0, Lty;->EH:Luo;

    .line 521
    return-void

    .line 516
    :cond_3
    iget-object v0, p0, Lty;->EH:Luo;

    if-eqz v0, :cond_2

    .line 517
    iget-object v0, p0, Lty;->EH:Luo;

    iget-object v1, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v1}, Luo;->e(Luq;)V

    goto :goto_0
.end method

.method private aN()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 345
    iget-boolean v1, p0, Lty;->AC:Z

    if-eqz v1, :cond_2

    .line 346
    iput-boolean v0, p0, Lty;->AC:Z

    .line 347
    invoke-direct {p0}, Lty;->fI()V

    .line 349
    iget-object v1, p0, Lty;->Ey:Luq;

    iget-object v1, v1, Luq;->Gd:Luj;

    if-eqz v1, :cond_1

    .line 350
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-boolean v0, v0, Luq;->Gh:Z

    .line 351
    if-nez v0, :cond_0

    .line 352
    iget-object v1, p0, Lty;->Ey:Luq;

    iget-object v1, v1, Luq;->Gd:Luj;

    invoke-virtual {v1}, Luj;->remove()V

    .line 354
    :cond_0
    iget-object v1, p0, Lty;->Ey:Luq;

    iput-object v2, v1, Luq;->Gd:Luj;

    .line 358
    :cond_1
    if-nez v0, :cond_2

    .line 359
    iget-object v0, p0, Lty;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltz;

    .line 360
    invoke-interface {v0}, Ltz;->eK()V

    goto :goto_0

    .line 365
    :cond_2
    iget-object v0, p0, Lty;->fW:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lty;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v2, p0, Lty;->fW:Landroid/view/VelocityTracker;

    .line 366
    :cond_3
    return-void
.end method

.method static synthetic b(Lty;I)I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lty;->EL:I

    return v0
.end method

.method static synthetic b(Lty;)Lcom/android/launcher3/Launcher;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    return-object v0
.end method

.method static synthetic c(Lty;)[I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lty;->EJ:[I

    return-object v0
.end method

.method private d(II[I)Luo;
    .locals 5

    .prologue
    .line 687
    iget-object v2, p0, Lty;->Et:Landroid/graphics/Rect;

    .line 689
    iget-object v3, p0, Lty;->Ez:Ljava/util/ArrayList;

    .line 690
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 691
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 692
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Luo;

    .line 693
    invoke-interface {v0}, Luo;->eJ()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 694
    invoke-interface {v0, v2}, Luo;->c(Landroid/graphics/Rect;)V

    .line 698
    iget-object v4, p0, Lty;->Ey:Luq;

    iput p1, v4, Luq;->x:I

    .line 699
    iget-object v4, p0, Lty;->Ey:Luq;

    iput p2, v4, Luq;->y:I

    .line 700
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 702
    const/4 v1, 0x0

    aput p1, p3, v1

    .line 703
    const/4 v1, 0x1

    aput p2, p3, v1

    .line 704
    iget-object v1, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v1, p3}, Lcom/android/launcher3/DragLayer;->c(Landroid/view/View;[I)F

    .line 709
    :goto_1
    return-object v0

    .line 691
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 709
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private fI()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 471
    iget-object v0, p0, Lty;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lty;->EG:Lua;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 472
    iget v0, p0, Lty;->hd:I

    if-ne v0, v2, :cond_0

    .line 473
    const/4 v0, 0x0

    iput v0, p0, Lty;->hd:I

    .line 474
    iget-object v0, p0, Lty;->EG:Lua;

    invoke-virtual {v0, v2}, Lua;->setDirection(I)V

    .line 475
    iget-object v0, p0, Lty;->EF:Luh;

    invoke-interface {v0}, Luh;->fW()Z

    .line 476
    iget-object v0, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fQ()V

    .line 478
    :cond_0
    return-void
.end method

.method private j(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lty;->fW:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 757
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lty;->fW:Landroid/view/VelocityTracker;

    .line 759
    :cond_0
    iget-object v0, p0, Lty;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 760
    return-void
.end method

.method static j(Luq;)V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Luq;->Gf:Lui;

    invoke-interface {v0}, Lui;->ek()V

    .line 384
    return-void
.end method

.method private n(FF)[I
    .locals 4

    .prologue
    .line 390
    iget-object v0, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    iget-object v1, p0, Lty;->EM:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/DragLayer;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 391
    iget-object v0, p0, Lty;->zU:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lty;->EM:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Lty;->EM:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 392
    iget-object v0, p0, Lty;->zU:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lty;->EM:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lty;->EM:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {p2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 393
    iget-object v0, p0, Lty;->zU:[I

    return-object v0
.end method

.method private o(FF)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 669
    iget-object v3, p0, Lty;->Eu:[I

    .line 670
    float-to-int v0, p1

    float-to-int v4, p2

    invoke-direct {p0, v0, v4, v3}, Lty;->d(II[I)Luo;

    move-result-object v0

    .line 672
    iget-object v4, p0, Lty;->Ey:Luq;

    aget v5, v3, v2

    iput v5, v4, Luq;->x:I

    .line 673
    iget-object v4, p0, Lty;->Ey:Luq;

    aget v3, v3, v1

    iput v3, v4, Luq;->y:I

    .line 675
    if-eqz v0, :cond_0

    .line 676
    iget-object v3, p0, Lty;->Ey:Luq;

    iput-boolean v1, v3, Luq;->Gc:Z

    .line 677
    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v3}, Luo;->e(Luq;)V

    .line 678
    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v3}, Luo;->a(Luq;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 679
    iget-object v3, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v3}, Luo;->b(Luq;)V

    .line 683
    :goto_0
    iget-object v3, p0, Lty;->Ey:Luq;

    iget-object v3, v3, Luq;->Gf:Lui;

    check-cast v0, Landroid/view/View;

    iget-object v4, p0, Lty;->Ey:Luq;

    invoke-interface {v3, v0, v4, v2, v1}, Lui;->a(Landroid/view/View;Luq;ZZ)V

    .line 684
    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public final L(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lty;->EE:Landroid/view/View;

    .line 464
    return-void
.end method

.method public final M(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 773
    iput-object p1, p0, Lty;->ED:Landroid/view/View;

    .line 774
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;IILui;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;F)Luj;
    .locals 13

    .prologue
    .line 208
    iget-object v2, p0, Lty;->EI:Landroid/view/inputmethod/InputMethodManager;

    if-nez v2, :cond_0

    .line 209
    iget-object v2, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p0, Lty;->EI:Landroid/view/inputmethod/InputMethodManager;

    .line 212
    :cond_0
    iget-object v2, p0, Lty;->EI:Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, Lty;->EC:Landroid/os/IBinder;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 214
    iget-object v2, p0, Lty;->EA:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltz;

    .line 215
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-interface {v2, v0, v1}, Ltz;->a(Lui;Ljava/lang/Object;)V

    goto :goto_0

    .line 218
    :cond_1
    iget v2, p0, Lty;->Ev:I

    sub-int v5, v2, p2

    .line 219
    iget v2, p0, Lty;->Ew:I

    sub-int v6, v2, p3

    .line 221
    if-nez p8, :cond_4

    const/4 v2, 0x0

    move v3, v2

    .line 222
    :goto_1
    if-nez p8, :cond_5

    const/4 v2, 0x0

    .line 224
    :goto_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lty;->AC:Z

    .line 226
    new-instance v4, Luq;

    invoke-direct {v4}, Luq;-><init>()V

    iput-object v4, p0, Lty;->Ey:Luq;

    .line 228
    iget-object v4, p0, Lty;->Ey:Luq;

    const/4 v7, 0x0

    iput-boolean v7, v4, Luq;->Gc:Z

    .line 229
    iget-object v4, p0, Lty;->Ey:Luq;

    iget v7, p0, Lty;->Ev:I

    add-int/2addr v3, p2

    sub-int v3, v7, v3

    iput v3, v4, Luq;->Ga:I

    .line 230
    iget-object v3, p0, Lty;->Ey:Luq;

    iget v4, p0, Lty;->Ew:I

    add-int v2, v2, p3

    sub-int v2, v4, v2

    iput v2, v3, Luq;->Gb:I

    .line 231
    iget-object v2, p0, Lty;->Ey:Luq;

    move-object/from16 v0, p4

    iput-object v0, v2, Luq;->Gf:Lui;

    .line 232
    iget-object v2, p0, Lty;->Ey:Luq;

    move-object/from16 v0, p5

    iput-object v0, v2, Luq;->Ge:Ljava/lang/Object;

    .line 234
    iget-object v12, p0, Lty;->Ey:Luq;

    new-instance v2, Luj;

    iget-object v3, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    move-object v4, p1

    move/from16 v11, p9

    invoke-direct/range {v2 .. v11}, Luj;-><init>(Lcom/android/launcher3/Launcher;Landroid/graphics/Bitmap;IIIIIIF)V

    iput-object v2, v12, Luq;->Gd:Luj;

    .line 237
    if-eqz p7, :cond_2

    .line 238
    new-instance v3, Landroid/graphics/Point;

    move-object/from16 v0, p7

    invoke-direct {v3, v0}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v2, v3}, Luj;->a(Landroid/graphics/Point;)V

    .line 240
    :cond_2
    if-eqz p8, :cond_3

    .line 241
    new-instance v3, Landroid/graphics/Rect;

    move-object/from16 v0, p8

    invoke-direct {v3, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v2, v3}, Luj;->d(Landroid/graphics/Rect;)V

    .line 244
    :cond_3
    iget-object v3, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/launcher3/DragLayer;->performHapticFeedback(I)Z

    .line 245
    iget v3, p0, Lty;->Ev:I

    iget v4, p0, Lty;->Ew:I

    invoke-virtual {v2, v3, v4}, Luj;->G(II)V

    .line 246
    iget v3, p0, Lty;->Ev:I

    iget v4, p0, Lty;->Ew:I

    invoke-direct {p0, v3, v4}, Lty;->E(II)V

    .line 247
    return-object v2

    .line 221
    :cond_4
    move-object/from16 v0, p8

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move v3, v2

    goto :goto_1

    .line 222
    :cond_5
    move-object/from16 v0, p8

    iget v2, v0, Landroid/graphics/Rect;->top:I

    goto/16 :goto_2
.end method

.method public final a(Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 717
    iput-object p1, p0, Lty;->EC:Landroid/os/IBinder;

    .line 718
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Bitmap;Lui;Ljava/lang/Object;ILandroid/graphics/Point;F)V
    .locals 11

    .prologue
    .line 169
    iget-object v4, p0, Lty;->Eu:[I

    .line 170
    iget-object v1, p0, Lty;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v1

    invoke-virtual {v1, p1, v4}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;[I)F

    .line 171
    if-eqz p6, :cond_1

    move-object/from16 v0, p6

    iget v1, v0, Landroid/graphics/Point;->x:I

    move v2, v1

    .line 172
    :goto_0
    if-eqz p6, :cond_2

    move-object/from16 v0, p6

    iget v1, v0, Landroid/graphics/Point;->y:I

    .line 173
    :goto_1
    const/4 v3, 0x0

    aget v3, v4, v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    add-int/2addr v3, v5

    add-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float v3, v3, p7

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v3, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    float-to-int v3, v3

    add-int/2addr v3, v2

    .line 175
    const/4 v2, 0x1

    aget v2, v4, v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v1, v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, p7

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v2, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    float-to-int v2, v2

    add-int v4, v1, v2

    .line 178
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    move/from16 v7, p5

    move/from16 v10, p7

    invoke-virtual/range {v1 .. v10}, Lty;->a(Landroid/graphics/Bitmap;IILui;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;F)Luj;

    .line 181
    if-nez p5, :cond_0

    .line 182
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 184
    :cond_0
    return-void

    .line 171
    :cond_1
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    .line 172
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 323
    iget-object v0, p0, Lty;->Ey:Luq;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-object v0, v0, Luq;->Ge:Ljava/lang/Object;

    .line 325
    instance-of v1, v0, Ladh;

    if-eqz v1, :cond_2

    .line 326
    check-cast v0, Ladh;

    .line 327
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrr;

    .line 329
    if-eqz v0, :cond_0

    iget-object v3, v0, Ladh;->intent:Landroid/content/Intent;

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 331
    iget-object v3, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    .line 332
    if-eqz v3, :cond_3

    iget-object v1, v1, Lrr;->xr:Landroid/content/ComponentName;

    invoke-virtual {v3, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    .line 334
    :goto_0
    if-eqz v1, :cond_0

    .line 335
    invoke-virtual {p0}, Lty;->fF()V

    .line 342
    :cond_2
    return-void

    .line 332
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ltz;)V
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lty;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 725
    return-void
.end method

.method public final a(Luh;)V
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Lty;->EF:Luh;

    .line 714
    return-void
.end method

.method public final a(Luj;)V
    .locals 2

    .prologue
    .line 372
    invoke-virtual {p1}, Luj;->remove()V

    .line 374
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-boolean v0, v0, Luq;->Gh:Z

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lty;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltz;

    .line 377
    invoke-interface {v0}, Ltz;->eK()V

    goto :goto_0

    .line 380
    :cond_0
    return-void
.end method

.method public final b(Luo;)V
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lty;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    return-void
.end method

.method public final c(Luo;)V
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lty;->Ez:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 746
    return-void
.end method

.method public final d(Luo;)V
    .locals 0

    .prologue
    .line 752
    iput-object p1, p0, Lty;->EB:Luo;

    .line 753
    return-void
.end method

.method public final dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lty;->EE:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lty;->EE:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final fD()Z
    .locals 1

    .prologue
    .line 299
    iget-boolean v0, p0, Lty;->AC:Z

    return v0
.end method

.method public final fE()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lty;->AC:Z

    return v0
.end method

.method public final fF()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 310
    iget-boolean v0, p0, Lty;->AC:Z

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Lty;->EH:Luo;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lty;->EH:Luo;

    iget-object v1, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v1}, Luo;->e(Luq;)V

    .line 314
    :cond_0
    iget-object v0, p0, Lty;->Ey:Luq;

    iput-boolean v3, v0, Luq;->Gh:Z

    .line 315
    iget-object v0, p0, Lty;->Ey:Luq;

    iput-boolean v2, v0, Luq;->AX:Z

    .line 316
    iget-object v0, p0, Lty;->Ey:Luq;

    iput-boolean v2, v0, Luq;->Gc:Z

    .line 317
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-object v0, v0, Luq;->Gf:Lui;

    const/4 v1, 0x0

    iget-object v2, p0, Lty;->Ey:Luq;

    invoke-interface {v0, v1, v2, v3, v3}, Lui;->a(Landroid/view/View;Luq;ZZ)V

    .line 319
    :cond_1
    invoke-direct {p0}, Lty;->aN()V

    .line 320
    return-void
.end method

.method public final fG()J
    .locals 2

    .prologue
    .line 397
    iget-boolean v0, p0, Lty;->AC:Z

    if-eqz v0, :cond_0

    .line 398
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 400
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lty;->EK:J

    goto :goto_0
.end method

.method public final fH()V
    .locals 2

    .prologue
    .line 405
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lty;->EK:J

    .line 406
    return-void
.end method

.method public final fJ()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 499
    iget-object v0, p0, Lty;->Eu:[I

    .line 500
    iget-object v1, p0, Lty;->EJ:[I

    aget v1, v1, v3

    iget-object v2, p0, Lty;->EJ:[I

    aget v2, v2, v4

    invoke-direct {p0, v1, v2, v0}, Lty;->d(II[I)Luo;

    move-result-object v1

    .line 501
    iget-object v2, p0, Lty;->Ey:Luq;

    aget v3, v0, v3

    iput v3, v2, Luq;->x:I

    .line 502
    iget-object v2, p0, Lty;->Ey:Luq;

    aget v0, v0, v4

    iput v0, v2, Luq;->y:I

    .line 503
    invoke-direct {p0, v1}, Lty;->a(Luo;)V

    .line 504
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 413
    invoke-direct {p0, p1}, Lty;->j(Landroid/view/MotionEvent;)V

    .line 422
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 423
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lty;->n(FF)[I

    move-result-object v2

    .line 424
    const/4 v3, 0x0

    aget v3, v2, v3

    .line 425
    const/4 v4, 0x1

    aget v2, v2, v4

    .line 427
    packed-switch v1, :pswitch_data_0

    .line 456
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lty;->AC:Z

    return v0

    .line 432
    :pswitch_1
    iput v3, p0, Lty;->Ev:I

    .line 433
    iput v2, p0, Lty;->Ew:I

    .line 434
    iput-object v0, p0, Lty;->EH:Luo;

    goto :goto_0

    .line 437
    :pswitch_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lty;->EK:J

    .line 438
    iget-boolean v1, p0, Lty;->AC:Z

    if-eqz v1, :cond_0

    .line 439
    iget-object v1, p0, Lty;->Ey:Luq;

    iget-object v1, v1, Luq;->Gf:Lui;

    invoke-direct {p0, v1}, Lty;->a(Lui;)Landroid/graphics/PointF;

    move-result-object v1

    .line 440
    iget-object v4, p0, Lty;->Ey:Luq;

    iget-object v4, v4, Luq;->Ge:Ljava/lang/Object;

    invoke-static {v4}, Lcom/android/launcher3/DeleteDropTarget;->Z(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 443
    :goto_1
    if-eqz v0, :cond_1

    .line 444
    invoke-direct {p0, v0}, Lty;->a(Landroid/graphics/PointF;)V

    .line 449
    :cond_0
    :goto_2
    invoke-direct {p0}, Lty;->aN()V

    goto :goto_0

    .line 446
    :cond_1
    int-to-float v0, v3

    int-to-float v1, v2

    invoke-direct {p0, v0, v1}, Lty;->o(FF)V

    goto :goto_2

    .line 452
    :pswitch_3
    invoke-virtual {p0}, Lty;->fF()V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 558
    iget-boolean v2, p0, Lty;->AC:Z

    if-nez v2, :cond_0

    .line 611
    :goto_0
    return v0

    .line 563
    :cond_0
    invoke-direct {p0, p1}, Lty;->j(Landroid/view/MotionEvent;)V

    .line 565
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 566
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v3, v4}, Lty;->n(FF)[I

    move-result-object v3

    .line 567
    aget v4, v3, v0

    .line 568
    aget v3, v3, v1

    .line 570
    packed-switch v2, :pswitch_data_0

    :goto_1
    move v0, v1

    .line 611
    goto :goto_0

    .line 573
    :pswitch_0
    iput v4, p0, Lty;->Ev:I

    .line 574
    iput v3, p0, Lty;->Ew:I

    .line 576
    iget v2, p0, Lty;->Ex:I

    if-lt v4, v2, :cond_1

    iget-object v2, p0, Lty;->ED:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget v5, p0, Lty;->Ex:I

    sub-int/2addr v2, v5

    if-le v4, v2, :cond_2

    .line 577
    :cond_1
    iput v1, p0, Lty;->hd:I

    .line 578
    iget-object v0, p0, Lty;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lty;->EG:Lua;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 582
    :goto_2
    invoke-direct {p0, v4, v3}, Lty;->E(II)V

    goto :goto_1

    .line 580
    :cond_2
    iput v0, p0, Lty;->hd:I

    goto :goto_2

    .line 585
    :pswitch_1
    invoke-direct {p0, v4, v3}, Lty;->E(II)V

    goto :goto_1

    .line 589
    :pswitch_2
    invoke-direct {p0, v4, v3}, Lty;->E(II)V

    .line 590
    iget-object v0, p0, Lty;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lty;->EG:Lua;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 592
    iget-boolean v0, p0, Lty;->AC:Z

    if-eqz v0, :cond_4

    .line 593
    iget-object v0, p0, Lty;->Ey:Luq;

    iget-object v0, v0, Luq;->Gf:Lui;

    invoke-direct {p0, v0}, Lty;->a(Lui;)Landroid/graphics/PointF;

    move-result-object v0

    .line 594
    iget-object v2, p0, Lty;->Ey:Luq;

    iget-object v2, v2, Luq;->Ge:Ljava/lang/Object;

    invoke-static {v2}, Lcom/android/launcher3/DeleteDropTarget;->Z(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 595
    const/4 v0, 0x0

    .line 597
    :cond_3
    if-eqz v0, :cond_5

    .line 598
    invoke-direct {p0, v0}, Lty;->a(Landroid/graphics/PointF;)V

    .line 603
    :cond_4
    :goto_3
    invoke-direct {p0}, Lty;->aN()V

    goto :goto_1

    .line 600
    :cond_5
    int-to-float v0, v4

    int-to-float v2, v3

    invoke-direct {p0, v0, v2}, Lty;->o(FF)V

    goto :goto_3

    .line 606
    :pswitch_3
    iget-object v0, p0, Lty;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lty;->EG:Lua;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 607
    invoke-virtual {p0}, Lty;->fF()V

    goto :goto_1

    .line 570
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
