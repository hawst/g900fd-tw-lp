.class public Lguh;
.super Lcgl;
.source "PG"


# instance fields
.field final cWB:Ljava/util/concurrent/atomic/AtomicLong;

.field final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lerk;)V
    .locals 6

    .prologue
    .line 57
    const-string v1, "GmsLocationProvider"

    const-wide/16 v4, 0x7530

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcgl;-><init>(Ljava/lang/String;Landroid/content/Context;Lerk;J)V

    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lguh;->cWB:Ljava/util/concurrent/atomic/AtomicLong;

    .line 58
    iput-object p1, p0, Lguh;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/content/Context;Lbgp;Lbgq;)Lbgo;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0, p1, p2, p3}, Lguh;->d(Landroid/content/Context;Lbgp;Lbgq;)Lbrj;

    move-result-object v0

    return-object v0
.end method

.method public final aKA()Lcgs;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Lgum;

    invoke-direct {v0, p0}, Lgum;-><init>(Lguh;)V

    const-string v1, "requestNewLocation"

    invoke-virtual {p0, v0, v1}, Lguh;->b(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final aKB()Lcgs;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Lguo;

    invoke-direct {v0, p0}, Lguo;-><init>(Lguh;)V

    const-string v1, "getLastLocation"

    invoke-virtual {p0, v0, v1}, Lguh;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method protected final aKC()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 262
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.velvet.location.GMS_CORE_HIGH_POWER_LOCATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lguh;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/velvet/location/LocationReceiver;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 264
    iget-object v1, p0, Lguh;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final aKx()Lcgs;
    .locals 2

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lguh;->fS(Z)Landroid/app/PendingIntent;

    move-result-object v0

    .line 113
    new-instance v1, Lguj;

    invoke-direct {v1, p0, v0}, Lguj;-><init>(Lguh;Landroid/app/PendingIntent;)V

    const-string v0, "stopBackgroundUpdates"

    invoke-virtual {p0, v1, v0}, Lguh;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final aKy()Lcgs;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lguk;

    invoke-direct {v0, p0}, Lguk;-><init>(Lguh;)V

    const-string v1, "startHighPowerUpdates"

    invoke-virtual {p0, v0, v1}, Lguh;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final aKz()Lcgs;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lgul;

    invoke-direct {v0, p0}, Lgul;-><init>(Lguh;)V

    const-string v1, "stopHighPowerUpdates"

    invoke-virtual {p0, v0, v1}, Lguh;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/app/PendingIntent;Lbrl;)V
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lgur;

    invoke-direct {v0, p0, p1, p2}, Lgur;-><init>(Lguh;Landroid/app/PendingIntent;Lbrl;)V

    invoke-virtual {p0, v0}, Lguh;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 245
    return-void
.end method

.method public final b(Ljava/util/List;Landroid/app/PendingIntent;Lbrk;)V
    .locals 1

    .prologue
    .line 202
    new-instance v0, Lgup;

    invoke-direct {v0, p0, p1, p2, p3}, Lgup;-><init>(Lguh;Ljava/util/List;Landroid/app/PendingIntent;Lbrk;)V

    invoke-virtual {p0, v0}, Lguh;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 209
    return-void
.end method

.method public final b(Ljava/util/List;Lbrl;)V
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lguq;

    invoke-direct {v0, p0, p1, p2}, Lguq;-><init>(Lguh;Ljava/util/List;Lbrl;)V

    invoke-virtual {p0, v0}, Lguh;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 227
    return-void
.end method

.method public final bH(J)Lcgs;
    .locals 7

    .prologue
    .line 77
    iget-object v0, p0, Lguh;->cWB:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    invoke-static {v0}, Lcgs;->au(Ljava/lang/Object;)Lcgs;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 82
    :cond_0
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->Ag()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/location/LocationRequest;->M(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/location/LocationRequest;->N(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v2

    .line 89
    const/16 v0, 0x66

    invoke-virtual {v2, v0}, Lcom/google/android/gms/location/LocationRequest;->dZ(I)Lcom/google/android/gms/location/LocationRequest;

    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lguh;->fS(Z)Landroid/app/PendingIntent;

    move-result-object v3

    .line 93
    new-instance v0, Lgui;

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lgui;-><init>(Lguh;Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;J)V

    const-string v1, "startBackgroundUpdates"

    invoke-virtual {p0, v0, v1}, Lguh;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    goto :goto_0
.end method

.method protected d(Landroid/content/Context;Lbgp;Lbgq;)Lbrj;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lbrj;

    invoke-direct {v0, p1, p2, p3}, Lbrj;-><init>(Landroid/content/Context;Lbgp;Lbgq;)V

    return-object v0
.end method

.method protected fS(Z)Landroid/app/PendingIntent;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 255
    if-eqz p1, :cond_0

    const/high16 v0, 0x8000000

    .line 256
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.velvet.location.GMS_CORE_LOCATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lguh;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/velvet/location/LocationReceiver;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 258
    iget-object v2, p0, Lguh;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3, v1, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    .line 255
    :cond_0
    const/high16 v0, 0x20000000

    goto :goto_0
.end method
