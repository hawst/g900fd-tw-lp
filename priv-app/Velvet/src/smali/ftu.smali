.class public final Lftu;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 4

    .prologue
    const v3, 0x7f110098

    .line 33
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agy:Lanz;

    invoke-virtual {v0}, Lanz;->pN()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0, v3}, Lftu;->jy(I)V

    .line 41
    :cond_0
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agy:Lanz;

    iget-object v1, v1, Lanz;->aiU:Laoi;

    invoke-virtual {v1}, Laoi;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v3, v1}, Lftu;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 45
    iget-object v0, p0, Lftu;->cBb:Lfuz;

    invoke-virtual {v0, p0}, Lfuz;->b(Lfro;)I

    move-result v1

    .line 46
    iget-object v0, p0, Lftu;->cBb:Lfuz;

    invoke-virtual {v0}, Lfuz;->aDz()I

    move-result v2

    .line 47
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;

    .line 50
    if-nez v1, :cond_3

    .line 51
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->hi(I)V

    .line 57
    :cond_1
    :goto_0
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-eqz v0, :cond_2

    .line 58
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lftu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020296

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 61
    :cond_2
    return-void

    .line 52
    :cond_3
    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    .line 53
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->hi(I)V

    goto :goto_0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 26
    const v0, 0x7f04011a

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
