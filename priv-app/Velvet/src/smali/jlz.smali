.class public final Ljlz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private etb:Ljava/lang/String;

.field private etc:Ljava/lang/String;

.field private etd:[Ljma;

.field private ete:D

.field private etf:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13917
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13918
    iput v2, p0, Ljlz;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlz;->etb:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlz;->etc:Ljava/lang/String;

    invoke-static {}, Ljma;->bpw()[Ljma;

    move-result-object v0

    iput-object v0, p0, Ljlz;->etd:[Ljma;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljlz;->ete:D

    iput v2, p0, Ljlz;->etf:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljlz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlz;->eCz:I

    .line 13919
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13813
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlz;->etb:Ljava/lang/String;

    iget v0, p0, Ljlz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlz;->etc:Ljava/lang/String;

    iget v0, p0, Ljlz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlz;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlz;->etd:[Ljma;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljma;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljlz;->etd:[Ljma;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljma;

    invoke-direct {v3}, Ljma;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljlz;->etd:[Ljma;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljma;

    invoke-direct {v3}, Ljma;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljlz;->etd:[Ljma;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    iput-wide v2, p0, Ljlz;->ete:D

    iget v0, p0, Ljlz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlz;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlz;->etf:I

    iget v0, p0, Ljlz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljlz;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x21 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 13936
    iget v0, p0, Ljlz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13937
    const/4 v0, 0x1

    iget-object v1, p0, Ljlz;->etb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13939
    :cond_0
    iget v0, p0, Ljlz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13940
    const/4 v0, 0x2

    iget-object v1, p0, Ljlz;->etc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13942
    :cond_1
    iget-object v0, p0, Ljlz;->etd:[Ljma;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljlz;->etd:[Ljma;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 13943
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljlz;->etd:[Ljma;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 13944
    iget-object v1, p0, Ljlz;->etd:[Ljma;

    aget-object v1, v1, v0

    .line 13945
    if-eqz v1, :cond_2

    .line 13946
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 13943
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13950
    :cond_3
    iget v0, p0, Ljlz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 13951
    const/4 v0, 0x4

    iget-wide v2, p0, Ljlz;->ete:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 13953
    :cond_4
    iget v0, p0, Ljlz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 13954
    const/4 v0, 0x5

    iget v1, p0, Ljlz;->etf:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 13956
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13957
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 13961
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13962
    iget v1, p0, Ljlz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13963
    const/4 v1, 0x1

    iget-object v2, p0, Ljlz;->etb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13966
    :cond_0
    iget v1, p0, Ljlz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13967
    const/4 v1, 0x2

    iget-object v2, p0, Ljlz;->etc:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13970
    :cond_1
    iget-object v1, p0, Ljlz;->etd:[Ljma;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljlz;->etd:[Ljma;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 13971
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljlz;->etd:[Ljma;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 13972
    iget-object v2, p0, Ljlz;->etd:[Ljma;

    aget-object v2, v2, v0

    .line 13973
    if-eqz v2, :cond_2

    .line 13974
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13971
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 13979
    :cond_4
    iget v1, p0, Ljlz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 13980
    const/4 v1, 0x4

    iget-wide v2, p0, Ljlz;->ete:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 13983
    :cond_5
    iget v1, p0, Ljlz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 13984
    const/4 v1, 0x5

    iget v2, p0, Ljlz;->etf:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13987
    :cond_6
    return v0
.end method
