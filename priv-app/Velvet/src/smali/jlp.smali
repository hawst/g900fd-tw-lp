.class public final Ljlp;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile esz:[Ljlp;


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private aiC:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10004
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 10005
    const/4 v0, 0x0

    iput v0, p0, Ljlp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlp;->aeE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlp;->aiC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljlp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlp;->eCz:I

    .line 10006
    return-void
.end method

.method public static bpp()[Ljlp;
    .locals 2

    .prologue
    .line 9947
    sget-object v0, Ljlp;->esz:[Ljlp;

    if-nez v0, :cond_1

    .line 9948
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 9950
    :try_start_0
    sget-object v0, Ljlp;->esz:[Ljlp;

    if-nez v0, :cond_0

    .line 9951
    const/4 v0, 0x0

    new-array v0, v0, [Ljlp;

    sput-object v0, Ljlp;->esz:[Ljlp;

    .line 9953
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9955
    :cond_1
    sget-object v0, Ljlp;->esz:[Ljlp;

    return-object v0

    .line 9953
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9941
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlp;->aeE:Ljava/lang/String;

    iget v0, p0, Ljlp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlp;->aiC:Ljava/lang/String;

    iget v0, p0, Ljlp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlp;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 10020
    iget v0, p0, Ljlp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 10021
    const/4 v0, 0x1

    iget-object v1, p0, Ljlp;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 10023
    :cond_0
    iget v0, p0, Ljlp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 10024
    const/4 v0, 0x2

    iget-object v1, p0, Ljlp;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 10026
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 10027
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 10031
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 10032
    iget v1, p0, Ljlp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 10033
    const/4 v1, 0x1

    iget-object v2, p0, Ljlp;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10036
    :cond_0
    iget v1, p0, Ljlp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 10037
    const/4 v1, 0x2

    iget-object v2, p0, Ljlp;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10040
    :cond_1
    return v0
.end method
