.class public final Lizp;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dUT:[Lizp;


# instance fields
.field private aez:I

.field public afA:Ljbj;

.field public dRY:Ljie;

.field private dUU:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33894
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 33895
    const/4 v0, 0x0

    iput v0, p0, Lizp;->aez:I

    iput-object v2, p0, Lizp;->afA:Ljbj;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lizp;->dUU:J

    iput-object v2, p0, Lizp;->dRY:Ljie;

    iput-object v2, p0, Lizp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizp;->eCz:I

    .line 33896
    return-void
.end method

.method public static bdn()[Lizp;
    .locals 2

    .prologue
    .line 33856
    sget-object v0, Lizp;->dUT:[Lizp;

    if-nez v0, :cond_1

    .line 33857
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 33859
    :try_start_0
    sget-object v0, Lizp;->dUT:[Lizp;

    if-nez v0, :cond_0

    .line 33860
    const/4 v0, 0x0

    new-array v0, v0, [Lizp;

    sput-object v0, Lizp;->dUT:[Lizp;

    .line 33862
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33864
    :cond_1
    sget-object v0, Lizp;->dUT:[Lizp;

    return-object v0

    .line 33862
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 33850
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lizp;->afA:Ljbj;

    if-nez v0, :cond_1

    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    iput-object v0, p0, Lizp;->afA:Ljbj;

    :cond_1
    iget-object v0, p0, Lizp;->afA:Ljbj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lizp;->dUU:J

    iget v0, p0, Lizp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizp;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lizp;->dRY:Ljie;

    if-nez v0, :cond_2

    new-instance v0, Ljie;

    invoke-direct {v0}, Ljie;-><init>()V

    iput-object v0, p0, Lizp;->dRY:Ljie;

    :cond_2
    iget-object v0, p0, Lizp;->dRY:Ljie;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 33911
    iget-object v0, p0, Lizp;->afA:Ljbj;

    if-eqz v0, :cond_0

    .line 33912
    const/4 v0, 0x1

    iget-object v1, p0, Lizp;->afA:Ljbj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 33914
    :cond_0
    iget v0, p0, Lizp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 33915
    const/4 v0, 0x2

    iget-wide v2, p0, Lizp;->dUU:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 33917
    :cond_1
    iget-object v0, p0, Lizp;->dRY:Ljie;

    if-eqz v0, :cond_2

    .line 33918
    const/4 v0, 0x5

    iget-object v1, p0, Lizp;->dRY:Ljie;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 33920
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 33921
    return-void
.end method

.method public final bdo()J
    .locals 2

    .prologue
    .line 33875
    iget-wide v0, p0, Lizp;->dUU:J

    return-wide v0
.end method

.method public final cu(J)Lizp;
    .locals 1

    .prologue
    .line 33878
    iput-wide p1, p0, Lizp;->dUU:J

    .line 33879
    iget v0, p0, Lizp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizp;->aez:I

    .line 33880
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 33925
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 33926
    iget-object v1, p0, Lizp;->afA:Ljbj;

    if-eqz v1, :cond_0

    .line 33927
    const/4 v1, 0x1

    iget-object v2, p0, Lizp;->afA:Ljbj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33930
    :cond_0
    iget v1, p0, Lizp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 33931
    const/4 v1, 0x2

    iget-wide v2, p0, Lizp;->dUU:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 33934
    :cond_1
    iget-object v1, p0, Lizp;->dRY:Ljie;

    if-eqz v1, :cond_2

    .line 33935
    const/4 v1, 0x5

    iget-object v2, p0, Lizp;->dRY:Ljie;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33938
    :cond_2
    return v0
.end method
