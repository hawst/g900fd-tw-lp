.class public interface abstract Ledr;
.super Ljava/lang/Object;
.source "PG"


# virtual methods
.method public abstract A(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method

.method public abstract B(Lcom/google/android/search/shared/actions/VoiceAction;)Z
.end method

.method public abstract C(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method

.method public abstract D(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method

.method public abstract GR()Z
.end method

.method public abstract VC()V
.end method

.method public abstract VG()Z
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/VoiceAction;Lesk;)J
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/VoiceAction;Z)V
.end method

.method public abstract a(Lesj;)V
.end method

.method public abstract aG(Ljava/lang/Object;)V
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract abj()Lcom/google/android/shared/search/Query;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract anJ()Z
.end method

.method public abstract anK()V
.end method

.method public abstract anL()Z
.end method

.method public abstract anM()Lesm;
.end method

.method public abstract anN()V
.end method

.method public abstract anO()Z
.end method

.method public abstract anP()Z
.end method

.method public abstract anQ()V
.end method

.method public abstract anR()Z
.end method

.method public abstract anS()V
.end method

.method public abstract cZ(I)V
.end method

.method public abstract e(Lcom/google/android/search/shared/actions/errors/SearchError;)V
.end method

.method public abstract eK(Z)V
.end method

.method public abstract hk(I)V
.end method

.method public abstract hl(I)V
.end method

.method public abstract m(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
.end method

.method public abstract n(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
.end method

.method public abstract o(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method

.method public abstract p(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method

.method public abstract q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract uF()I
.end method

.method public abstract uv()V
.end method

.method public abstract w(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method

.method public abstract wr()Leoj;
.end method

.method public abstract x(Lcom/google/android/search/shared/actions/VoiceAction;)Z
.end method

.method public abstract y(Lcom/google/android/search/shared/actions/VoiceAction;)Z
.end method

.method public abstract z(Lcom/google/android/search/shared/actions/VoiceAction;)V
.end method
