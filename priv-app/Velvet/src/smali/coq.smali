.class public final Lcoq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcne;


# instance fields
.field private final bdZ:I

.field private final bdv:I

.field private final mExtrasConsumer:Lcoi;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mInputStream:Ljava/io/InputStream;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;ILcoi;I)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcoi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcoq;->mInputStream:Ljava/io/InputStream;

    .line 40
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoi;

    iput-object v0, p0, Lcoq;->mExtrasConsumer:Lcoi;

    .line 41
    iput p2, p0, Lcoq;->bdv:I

    .line 42
    iput p4, p0, Lcoq;->bdZ:I

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Lcnf;)V
    .locals 7
    .param p1    # Lcnf;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 51
    .line 55
    iget-object v0, p0, Lcoq;->mExtrasConsumer:Lcoi;

    new-array v2, v1, [I

    invoke-interface {v0, v2}, Lcoi;->g([I)V

    .line 58
    iget-object v0, p0, Lcoq;->mExtrasConsumer:Lcoi;

    invoke-interface {v0, v1}, Lcoi;->cu(Z)V

    move v0, v1

    move v2, v1

    .line 62
    :goto_0
    iget v3, p0, Lcoq;->bdZ:I

    new-array v6, v3, [B

    move v4, v1

    .line 64
    :goto_1
    iget v3, p0, Lcoq;->bdZ:I

    if-ge v4, v3, :cond_0

    .line 66
    const-string v3, "RawChunkReader required non-closed sink"

    invoke-interface {p1, v3}, Lcnf;->ht(Ljava/lang/String;)V

    .line 76
    :try_start_0
    iget-object v3, p0, Lcoq;->mInputStream:Ljava/io/InputStream;

    iget v5, p0, Lcoq;->bdZ:I

    sub-int/2addr v5, v4

    invoke-virtual {v3, v6, v4, v5}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 86
    if-ltz v3, :cond_0

    .line 87
    add-int/2addr v3, v4

    move v4, v3

    .line 90
    goto :goto_1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    invoke-interface {p1, v0}, Lcnf;->b(Ljava/lang/Exception;)V

    .line 82
    throw v0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    new-instance v1, Lefs;

    const v2, 0x3000f

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 92
    :cond_0
    add-int v3, v2, v4

    .line 94
    iget-object v2, p0, Lcoq;->mExtrasConsumer:Lcoi;

    invoke-interface {v2, v3}, Lcoi;->fe(I)V

    .line 97
    if-nez v4, :cond_1

    .line 98
    if-nez v3, :cond_6

    .line 99
    new-instance v0, Lefs;

    const v1, 0x30003

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0

    .line 105
    :cond_1
    add-int/lit8 v2, v0, 0x1

    .line 106
    iget v5, p0, Lcoq;->bdZ:I

    if-ne v4, v5, :cond_2

    new-instance v4, Ldkd;

    invoke-direct {v4, v6, v0}, Ldkd;-><init>([BI)V

    move-object v0, v4

    .line 108
    :goto_2
    invoke-interface {p1, v0}, Lcnf;->a(Ldkd;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 109
    new-instance v0, Lefs;

    const v1, 0x30009

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0

    .line 106
    :cond_2
    new-instance v5, Ldkd;

    invoke-direct {v5, v6, v4, v0}, Ldkd;-><init>([BII)V

    move-object v0, v5

    goto :goto_2

    .line 112
    :cond_3
    iget v0, p0, Lcoq;->bdv:I

    if-le v3, v0, :cond_4

    .line 113
    new-instance v0, Lefs;

    const v1, 0x30002

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0

    .line 115
    :cond_4
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 116
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    move v2, v3

    .line 118
    goto :goto_0

    .line 101
    :cond_6
    return-void
.end method
