.class final Lcgq;
.super Ljava/util/concurrent/FutureTask;
.source "PG"

# interfaces
.implements Leri;
.implements Livq;


# instance fields
.field private final aWo:Livd;

.field private final aWp:Ljava/lang/String;

.field private final aWq:Ljava/lang/String;

.field private mName:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 550
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 541
    new-instance v0, Livd;

    invoke-direct {v0}, Livd;-><init>()V

    iput-object v0, p0, Lcgq;->aWo:Livd;

    .line 551
    iput-object p2, p0, Lcgq;->aWq:Ljava/lang/String;

    .line 552
    iput-object p3, p0, Lcgq;->aWp:Ljava/lang/String;

    .line 553
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcgq;->aWo:Livd;

    invoke-virtual {v0, p1, p2}, Livd;->b(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 566
    return-void
.end method

.method protected final done()V
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcgq;->aWo:Livd;

    invoke-virtual {v0}, Livd;->execute()V

    .line 561
    return-void
.end method

.method public final getPermissions()I
    .locals 1

    .prologue
    .line 577
    const/4 v0, 0x1

    return v0
.end method

.method public final setException(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 570
    invoke-super {p0, p1}, Ljava/util/concurrent/FutureTask;->setException(Ljava/lang/Throwable;)V

    .line 571
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 582
    iget-object v0, p0, Lcgq;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 583
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcgq;->aWq:Ljava/lang/String;

    iget-object v2, p0, Lcgq;->aWp:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lesp;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcgq;->mName:Ljava/lang/String;

    .line 585
    :cond_0
    iget-object v0, p0, Lcgq;->mName:Ljava/lang/String;

    return-object v0
.end method
