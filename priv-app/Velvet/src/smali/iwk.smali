.class public final Liwk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dLV:[Liwk;


# instance fields
.field private aez:I

.field public afA:Ljbj;

.field private agv:I

.field private dLW:Ljava/lang/String;

.field private dLX:Ljava/lang/String;

.field private dLY:Z

.field private dLZ:[B

.field private dMa:I

.field private dMb:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Liwk;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Liwk;->agv:I

    const-string v0, ""

    iput-object v0, p0, Liwk;->dLW:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liwk;->dLX:Ljava/lang/String;

    iput-boolean v1, p0, Liwk;->dLY:Z

    iput-object v2, p0, Liwk;->afA:Ljbj;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Liwk;->dLZ:[B

    iput v1, p0, Liwk;->dMa:I

    iput v1, p0, Liwk;->dMb:I

    iput-object v2, p0, Liwk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwk;->eCz:I

    return-void
.end method

.method public static aZn()[Liwk;
    .locals 2

    sget-object v0, Liwk;->dLV:[Liwk;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Liwk;->dLV:[Liwk;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Liwk;

    sput-object v0, Liwk;->dLV:[Liwk;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Liwk;->dLV:[Liwk;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Liwk;->agv:I

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwk;->dLW:Ljava/lang/String;

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Liwk;->dLY:Z

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Liwk;->afA:Ljbj;

    if-nez v0, :cond_1

    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    iput-object v0, p0, Liwk;->afA:Ljbj;

    :cond_1
    iget-object v0, p0, Liwk;->afA:Ljbj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Liwk;->dLZ:[B

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liwk;->dMa:I

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwk;->dLX:Ljava/lang/String;

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liwk;->dMb:I

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Liwk;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Liwk;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liwk;->dLW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Liwk;->dLY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_2
    iget-object v0, p0, Liwk;->afA:Ljbj;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Liwk;->afA:Ljbj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Liwk;->dLZ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    :cond_4
    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Liwk;->dMa:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_5
    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Liwk;->dLX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_6
    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Liwk;->dMb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final aZo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liwk;->dLW:Ljava/lang/String;

    return-object v0
.end method

.method public final aZp()Z
    .locals 1

    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aZq()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liwk;->dLX:Ljava/lang/String;

    return-object v0
.end method

.method public final aZr()Z
    .locals 1

    iget v0, p0, Liwk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aZs()I
    .locals 1

    iget v0, p0, Liwk;->dMb:I

    return v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Liwk;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Liwk;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Liwk;->dLW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Liwk;->dLY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Liwk;->afA:Ljbj;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Liwk;->afA:Ljbj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Liwk;->dLZ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Liwk;->dMa:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Liwk;->dLX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Liwk;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Liwk;->dMb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    return v0
.end method

.method public final nb(I)Liwk;
    .locals 1

    iput p1, p0, Liwk;->agv:I

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwk;->aez:I

    return-object p0
.end method

.method public final nc(I)Liwk;
    .locals 1

    iput p1, p0, Liwk;->dMb:I

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Liwk;->aez:I

    return-object p0
.end method

.method public final pX(Ljava/lang/String;)Liwk;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Liwk;->dLW:Ljava/lang/String;

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwk;->aez:I

    return-object p0
.end method

.method public final pY(Ljava/lang/String;)Liwk;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Liwk;->dLX:Ljava/lang/String;

    iget v0, p0, Liwk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liwk;->aez:I

    return-object p0
.end method
