.class public final Ljef;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afZ:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field private ahA:Z

.field private aiK:Ljava/lang/String;

.field public aiX:Ljcn;

.field public ajE:Ljei;

.field private ajk:Ljava/lang/String;

.field private dNK:J

.field private dRI:Ljava/lang/String;

.field private dRd:Z

.field private dRe:Z

.field private edT:Ljava/lang/String;

.field private edU:Ljava/lang/String;

.field private edV:Ljava/lang/String;

.field private edW:J

.field private edX:J

.field private edY:J


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 61180
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 61181
    iput v1, p0, Ljef;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljef;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljef;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljef;->ajk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljef;->edT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljef;->edU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljef;->edV:Ljava/lang/String;

    iput-wide v2, p0, Ljef;->dNK:J

    iput-wide v2, p0, Ljef;->edW:J

    iput-object v4, p0, Ljef;->aiX:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljef;->dRI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljef;->afZ:Ljava/lang/String;

    iput-wide v2, p0, Ljef;->edX:J

    iput-object v4, p0, Ljef;->ajE:Ljei;

    iput-wide v2, p0, Ljef;->edY:J

    iput-boolean v1, p0, Ljef;->dRe:Z

    iput-boolean v1, p0, Ljef;->dRd:Z

    iput-boolean v1, p0, Ljef;->ahA:Z

    iput-object v4, p0, Ljef;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljef;->eCz:I

    .line 61182
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 60846
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljef;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->afh:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->aiK:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->ajk:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljef;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljef;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Ljef;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->edT:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->edV:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljef;->dNK:J

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljef;->edW:J

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljef;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->edU:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljef;->ajE:Ljei;

    if-nez v0, :cond_2

    new-instance v0, Ljei;

    invoke-direct {v0}, Ljei;-><init>()V

    iput-object v0, p0, Ljef;->ajE:Ljei;

    :cond_2
    iget-object v0, p0, Ljef;->ajE:Ljei;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->dRI:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljef;->afZ:Ljava/lang/String;

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    iput-wide v0, p0, Ljef;->edY:J

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljef;->dRe:Z

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljef;->dRd:Z

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljef;->edX:J

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljef;->ahA:Z

    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljef;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x69 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 61211
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 61212
    const/4 v0, 0x1

    iget-object v1, p0, Ljef;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61214
    :cond_0
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 61215
    const/4 v0, 0x2

    iget-object v1, p0, Ljef;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61217
    :cond_1
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 61218
    const/4 v0, 0x3

    iget-object v1, p0, Ljef;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61220
    :cond_2
    iget-object v0, p0, Ljef;->aiX:Ljcn;

    if-eqz v0, :cond_3

    .line 61221
    const/4 v0, 0x4

    iget-object v1, p0, Ljef;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 61223
    :cond_3
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 61224
    const/4 v0, 0x5

    iget-object v1, p0, Ljef;->edT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61226
    :cond_4
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 61227
    const/4 v0, 0x6

    iget-object v1, p0, Ljef;->edV:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61229
    :cond_5
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 61230
    const/4 v0, 0x7

    iget-wide v2, p0, Ljef;->dNK:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 61232
    :cond_6
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 61233
    const/16 v0, 0x8

    iget-wide v2, p0, Ljef;->edW:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 61235
    :cond_7
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    .line 61236
    const/16 v0, 0x9

    iget-object v1, p0, Ljef;->edU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61238
    :cond_8
    iget-object v0, p0, Ljef;->ajE:Ljei;

    if-eqz v0, :cond_9

    .line 61239
    const/16 v0, 0xa

    iget-object v1, p0, Ljef;->ajE:Ljei;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 61241
    :cond_9
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_a

    .line 61242
    const/16 v0, 0xb

    iget-object v1, p0, Ljef;->dRI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61244
    :cond_a
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_b

    .line 61245
    const/16 v0, 0xc

    iget-object v1, p0, Ljef;->afZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 61247
    :cond_b
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_c

    .line 61248
    const/16 v0, 0xd

    iget-wide v2, p0, Ljef;->edY:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->i(IJ)V

    .line 61250
    :cond_c
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_d

    .line 61251
    const/16 v0, 0xe

    iget-boolean v1, p0, Ljef;->dRe:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 61253
    :cond_d
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_e

    .line 61254
    const/16 v0, 0xf

    iget-boolean v1, p0, Ljef;->dRd:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 61256
    :cond_e
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_f

    .line 61257
    const/16 v0, 0x10

    iget-wide v2, p0, Ljef;->edX:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 61259
    :cond_f
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_10

    .line 61260
    const/16 v0, 0x11

    iget-boolean v1, p0, Ljef;->ahA:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 61262
    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 61263
    return-void
.end method

.method public final ban()J
    .locals 2

    .prologue
    .line 61000
    iget-wide v0, p0, Ljef;->dNK:J

    return-wide v0
.end method

.method public final bao()Z
    .locals 1

    .prologue
    .line 61008
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bcs()Z
    .locals 1

    .prologue
    .line 61145
    iget-boolean v0, p0, Ljef;->dRd:Z

    return v0
.end method

.method public final bio()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60956
    iget-object v0, p0, Ljef;->edU:Ljava/lang/String;

    return-object v0
.end method

.method public final bip()Z
    .locals 1

    .prologue
    .line 60967
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final biq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60978
    iget-object v0, p0, Ljef;->edV:Ljava/lang/String;

    return-object v0
.end method

.method public final bir()Z
    .locals 1

    .prologue
    .line 60989
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bis()J
    .locals 2

    .prologue
    .line 61019
    iget-wide v0, p0, Ljef;->edW:J

    return-wide v0
.end method

.method public final bit()Z
    .locals 1

    .prologue
    .line 61027
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final biu()J
    .locals 2

    .prologue
    .line 61107
    iget-wide v0, p0, Ljef;->edY:J

    return-wide v0
.end method

.method public final biv()Z
    .locals 1

    .prologue
    .line 61126
    iget-boolean v0, p0, Ljef;->dRe:Z

    return v0
.end method

.method public final cZ(J)Ljef;
    .locals 2

    .prologue
    .line 61003
    const-wide/16 v0, 0x3039

    iput-wide v0, p0, Ljef;->dNK:J

    .line 61004
    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljef;->aez:I

    .line 61005
    return-object p0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60890
    iget-object v0, p0, Ljef;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60868
    iget-object v0, p0, Ljef;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60912
    iget-object v0, p0, Ljef;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method public final hN(Z)Ljef;
    .locals 1

    .prologue
    .line 61148
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljef;->dRd:Z

    .line 61149
    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljef;->aez:I

    .line 61150
    return-object p0
.end method

.method public final hO(Z)Ljef;
    .locals 1

    .prologue
    .line 61167
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljef;->ahA:Z

    .line 61168
    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljef;->aez:I

    .line 61169
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 61267
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 61268
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 61269
    const/4 v1, 0x1

    iget-object v2, p0, Ljef;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61272
    :cond_0
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 61273
    const/4 v1, 0x2

    iget-object v2, p0, Ljef;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61276
    :cond_1
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 61277
    const/4 v1, 0x3

    iget-object v2, p0, Ljef;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61280
    :cond_2
    iget-object v1, p0, Ljef;->aiX:Ljcn;

    if-eqz v1, :cond_3

    .line 61281
    const/4 v1, 0x4

    iget-object v2, p0, Ljef;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61284
    :cond_3
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 61285
    const/4 v1, 0x5

    iget-object v2, p0, Ljef;->edT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61288
    :cond_4
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 61289
    const/4 v1, 0x6

    iget-object v2, p0, Ljef;->edV:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61292
    :cond_5
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 61293
    const/4 v1, 0x7

    iget-wide v2, p0, Ljef;->dNK:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 61296
    :cond_6
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 61297
    const/16 v1, 0x8

    iget-wide v2, p0, Ljef;->edW:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 61300
    :cond_7
    iget v1, p0, Ljef;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_8

    .line 61301
    const/16 v1, 0x9

    iget-object v2, p0, Ljef;->edU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61304
    :cond_8
    iget-object v1, p0, Ljef;->ajE:Ljei;

    if-eqz v1, :cond_9

    .line 61305
    const/16 v1, 0xa

    iget-object v2, p0, Ljef;->ajE:Ljei;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61308
    :cond_9
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_a

    .line 61309
    const/16 v1, 0xb

    iget-object v2, p0, Ljef;->dRI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61312
    :cond_a
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_b

    .line 61313
    const/16 v1, 0xc

    iget-object v2, p0, Ljef;->afZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61316
    :cond_b
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_c

    .line 61317
    const/16 v1, 0xd

    iget-wide v2, p0, Ljef;->edY:J

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 61320
    :cond_c
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_d

    .line 61321
    const/16 v1, 0xe

    iget-boolean v2, p0, Ljef;->dRe:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 61324
    :cond_d
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_e

    .line 61325
    const/16 v1, 0xf

    iget-boolean v2, p0, Ljef;->dRd:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 61328
    :cond_e
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_f

    .line 61329
    const/16 v1, 0x10

    iget-wide v2, p0, Ljef;->edX:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 61332
    :cond_f
    iget v1, p0, Ljef;->aez:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_10

    .line 61333
    const/16 v1, 0x11

    iget-boolean v2, p0, Ljef;->ahA:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 61336
    :cond_10
    return v0
.end method

.method public final oX()Z
    .locals 1

    .prologue
    .line 61164
    iget-boolean v0, p0, Ljef;->ahA:Z

    return v0
.end method

.method public final oo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61063
    iget-object v0, p0, Ljef;->afZ:Ljava/lang/String;

    return-object v0
.end method

.method public final pX()Z
    .locals 1

    .prologue
    .line 60901
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 60879
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pf()Z
    .locals 1

    .prologue
    .line 61074
    iget v0, p0, Ljef;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qG()Z
    .locals 1

    .prologue
    .line 60923
    iget v0, p0, Ljef;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tV(Ljava/lang/String;)Ljef;
    .locals 1

    .prologue
    .line 60871
    if-nez p1, :cond_0

    .line 60872
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60874
    :cond_0
    iput-object p1, p0, Ljef;->afh:Ljava/lang/String;

    .line 60875
    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljef;->aez:I

    .line 60876
    return-object p0
.end method

.method public final tW(Ljava/lang/String;)Ljef;
    .locals 1

    .prologue
    .line 60893
    if-nez p1, :cond_0

    .line 60894
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60896
    :cond_0
    iput-object p1, p0, Ljef;->aiK:Ljava/lang/String;

    .line 60897
    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljef;->aez:I

    .line 60898
    return-object p0
.end method

.method public final tX(Ljava/lang/String;)Ljef;
    .locals 1

    .prologue
    .line 60915
    if-nez p1, :cond_0

    .line 60916
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60918
    :cond_0
    iput-object p1, p0, Ljef;->ajk:Ljava/lang/String;

    .line 60919
    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljef;->aez:I

    .line 60920
    return-object p0
.end method

.method public final tY(Ljava/lang/String;)Ljef;
    .locals 1

    .prologue
    .line 60959
    if-nez p1, :cond_0

    .line 60960
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60962
    :cond_0
    iput-object p1, p0, Ljef;->edU:Ljava/lang/String;

    .line 60963
    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljef;->aez:I

    .line 60964
    return-object p0
.end method

.method public final tZ(Ljava/lang/String;)Ljef;
    .locals 1

    .prologue
    .line 60981
    if-nez p1, :cond_0

    .line 60982
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60984
    :cond_0
    iput-object p1, p0, Ljef;->edV:Ljava/lang/String;

    .line 60985
    iget v0, p0, Ljef;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljef;->aez:I

    .line 60986
    return-object p0
.end method

.method public final ua(Ljava/lang/String;)Ljef;
    .locals 1

    .prologue
    .line 61066
    if-nez p1, :cond_0

    .line 61067
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61069
    :cond_0
    iput-object p1, p0, Ljef;->afZ:Ljava/lang/String;

    .line 61070
    iget v0, p0, Ljef;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljef;->aez:I

    .line 61071
    return-object p0
.end method
