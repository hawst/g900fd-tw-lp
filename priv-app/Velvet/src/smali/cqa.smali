.class public final Lcqa;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field private final bfZ:J

.field private final mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 654
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v0

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcqa;-><init>(Lcpx;J)V

    .line 656
    return-void
.end method

.method private constructor <init>(Lcpx;J)V
    .locals 2

    .prologue
    .line 645
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 646
    iput-object p1, p0, Lcqa;->mUserInteractionLogger:Lcpx;

    .line 647
    iput-wide p2, p0, Lcqa;->bfZ:J

    .line 650
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcqa;->setRetainInstance(Z)V

    .line 651
    return-void
.end method

.method synthetic constructor <init>(Lcpx;JB)V
    .locals 0

    .prologue
    .line 641
    invoke-direct {p0, p1, p2, p3}, Lcqa;-><init>(Lcpx;J)V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 660
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 661
    iget-object v0, p0, Lcqa;->mUserInteractionLogger:Lcpx;

    iget-wide v2, p0, Lcqa;->bfZ:J

    invoke-static {v0, v2, v3}, Lcpx;->a(Lcpx;J)V

    .line 662
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 666
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 667
    iget-object v0, p0, Lcqa;->mUserInteractionLogger:Lcpx;

    invoke-static {v0}, Lcpx;->a(Lcpx;)V

    .line 668
    const-string v0, "QSB.UserInteractionLogger"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    return-void
.end method
