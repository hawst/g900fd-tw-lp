.class final Lgej;
.super Lepm;
.source "PG"


# instance fields
.field private final aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private synthetic cHo:Lgeg;

.field private final cHp:Lepm;

.field private final cHq:J


# direct methods
.method public constructor <init>(Lgeg;Ljava/lang/String;Lepm;J)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    iput-object p1, p0, Lgej;->cHo:Lgeg;

    .line 158
    new-array v0, v1, [I

    invoke-direct {p0, p2, v0}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 155
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lgej;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 159
    iput-object p3, p0, Lgej;->cHp:Lepm;

    .line 160
    iput-wide p4, p0, Lgej;->cHq:J

    .line 161
    return-void
.end method


# virtual methods
.method public final invalidate()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lgej;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 165
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 169
    iget-object v0, p0, Lgej;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-wide v0, p0, Lgej;->cHq:J

    iget-object v2, p0, Lgej;->cHo:Lgeg;

    iget-object v2, v2, Lgeg;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lgej;->cHo:Lgeg;

    iget-wide v4, v4, Lgeg;->cFA:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 174
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 175
    const-string v0, "VSRecognitionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Timed out "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lgej;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lgej;->cHo:Lgeg;

    iget-object v0, v0, Lgeg;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lgej;->cHp:Lepm;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 179
    :cond_1
    iget-object v2, p0, Lgej;->cHo:Lgeg;

    iget-object v3, p0, Lgej;->cHo:Lgeg;

    iget-object v3, v3, Lgeg;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v4, 0xa

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, p0, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, v2, Lgeg;->cFB:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method
