.class public final Lfkf;
.super Lfjz;
.source "PG"


# instance fields
.field private final ctM:Lizr;

.field private ctN:J

.field private ctO:I

.field private ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field private ctQ:Lelg;

.field private final ctR:[Lfkl;

.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lfzw;Lgbd;)V
    .locals 12

    .prologue
    const v11, 0x7f0a04b4

    const v10, 0x7f0a0454

    const v9, 0x7f0a0453

    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0, p1, p2}, Lfjz;-><init>(Lizj;Lfzw;)V

    .line 71
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lfkf;->ctN:J

    .line 72
    iput v1, p0, Lfkf;->ctO:I

    .line 78
    const/4 v2, 0x3

    new-array v2, v2, [Lfkl;

    new-instance v3, Lfkl;

    const v4, 0x7f0a04b5

    invoke-direct {v3, v11, v4, v9, v10}, Lfkl;-><init>(IIII)V

    aput-object v3, v2, v1

    const/4 v3, 0x1

    new-instance v4, Lfkl;

    const v5, 0x7f0a04b6

    const v6, 0x7f0a04b8

    const v7, 0x7f0a05d4

    const v8, 0x7f0a05d5

    invoke-direct {v4, v5, v6, v7, v8}, Lfkl;-><init>(IIII)V

    aput-object v4, v2, v3

    new-instance v3, Lfkl;

    const v4, 0x7f0a04b5

    invoke-direct {v3, v11, v4, v9, v10}, Lfkl;-><init>(IIII)V

    aput-object v3, v2, v0

    iput-object v2, p0, Lfkf;->ctR:[Lfkl;

    .line 92
    iput-object p3, p0, Lfkf;->mPhotoWithAttributionDecorator:Lgbd;

    .line 93
    iget-object v2, p1, Lizj;->dSK:Lizr;

    iput-object v2, p0, Lfkf;->ctM:Lizr;

    .line 94
    iget-object v2, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v2}, Lizr;->bdv()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    iput v0, p0, Lfkf;->ctO:I

    .line 95
    return-void

    :cond_0
    move v0, v1

    .line 94
    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/view/LayoutInflater;)Lelg;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v0, v0, Lizr;->dVh:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 104
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v3, v0, Lizr;->dVh:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v6, v3, v0

    .line 105
    const/16 v7, 0x10

    invoke-static {p1, v6, v7, v9, v1}, Lesi;->a(Landroid/content/Context;Ljava/lang/String;IZZ)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 109
    new-instance v8, Lelh;

    invoke-direct {v8, v7, v6, v1}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    new-instance v0, Lelh;

    const v1, 0x7f0a04b1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v5, v9}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v0, Lfkg;

    const v3, 0x7f0400ac

    invoke-static {v2}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lfkg;-><init>(Lfkf;Landroid/content/Context;I[Lelh;Lelh;Landroid/view/LayoutInflater;)V

    return-object v0
.end method

.method public static lP(Ljava/lang/String;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 383
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 384
    invoke-virtual {v0, p0}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 385
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f0d01f0

    const/16 v7, 0x2f

    const/4 v8, 0x1

    const v6, 0x7f02010f

    const/4 v2, 0x0

    .line 147
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f090178

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 149
    const v0, 0x7f0400ab

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 150
    const v0, 0x7f110082

    iget-object v1, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v1}, Lizr;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 151
    const v0, 0x7f110227

    iget-object v1, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v1}, Lizr;->bdu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 153
    const v0, 0x7f110228

    iget-object v1, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v1}, Lizr;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v0, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 155
    const v0, 0x7f110225

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 156
    const v0, 0x7f110212

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;

    .line 158
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->hi(I)V

    .line 161
    iget-object v1, p0, Lfkf;->ctM:Lizr;

    iget-object v1, v1, Lizr;->aiX:Ljcn;

    if-eqz v1, :cond_4

    .line 162
    iget-object v0, p0, Lfkf;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v1, p0, Lfkf;->ctM:Lizr;

    iget-object v1, v1, Lizr;->aiX:Ljcn;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljcn;->qG()Z

    move-result v6

    if-nez v6, :cond_1

    .line 176
    :cond_0
    :goto_0
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v0, v0, Lizr;->dVh:[Ljava/lang/String;

    array-length v0, v0

    if-ne v0, v8, :cond_5

    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v0, v0, Lizr;->dVh:[Ljava/lang/String;

    aget-object v0, v0, v2

    const/16 v1, 0x54

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    .line 178
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v0, v0, Lizr;->dVh:[Ljava/lang/String;

    aget-object v0, v0, v2

    .line 179
    const v1, 0x7f11022b

    const/16 v5, 0x10

    invoke-static {p1, v0, v5, v8, v2}, Lesi;->a(Landroid/content/Context;Ljava/lang/String;IZZ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v4, v1, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 200
    :goto_1
    const v0, 0x7f0400c1

    invoke-virtual {v3, v0, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 202
    const v0, 0x7f110258

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 203
    return-object v1

    .line 162
    :cond_1
    invoke-virtual {v0, p1, v1, v9, v9}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_0

    const v0, 0x7f110212

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v7

    iget-object v7, v7, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    iget-object v0, v1, Ljcn;->eaQ:Lixa;

    if-eqz v0, :cond_2

    iget-object v0, v1, Ljcn;->eaQ:Lixa;

    invoke-virtual {v0}, Lixa;->pb()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lixa;->getTitle()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_2
    const v0, 0x7f110213

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lixa;->getUrl()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 166
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "android.resource://"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v5

    iget-object v5, v5, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->a(Ljava/lang/String;Lesm;)V

    goto/16 :goto_0

    .line 184
    :cond_5
    const v0, 0x7f11022c

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 185
    invoke-direct {p0, p1, v3}, Lfkf;->a(Landroid/content/Context;Landroid/view/LayoutInflater;)Lelg;

    move-result-object v0

    iput-object v0, p0, Lfkf;->ctQ:Lelg;

    .line 186
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lfkf;->ctQ:Lelg;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 188
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v0}, Lizr;->bdq()Ljava/lang/String;

    move-result-object v5

    move v0, v2

    move v1, v2

    .line 190
    :goto_3
    iget-object v6, p0, Lfkf;->ctM:Lizr;

    iget-object v6, v6, Lizr;->dVh:[Ljava/lang/String;

    array-length v6, v6

    if-ge v0, v6, :cond_7

    .line 192
    iget-object v6, p0, Lfkf;->ctM:Lizr;

    iget-object v6, v6, Lizr;->dVh:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v1, v0

    .line 190
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 196
    :cond_7
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 197
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public final a(Landroid/app/Activity;Lfmt;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 239
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    new-instance v1, Lfkh;

    invoke-direct {v1, p0, p1, p2}, Lfkh;-><init>(Lfkf;Landroid/app/Activity;Lfmt;)V

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 249
    :cond_0
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->g(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    move-result-object v0

    .line 251
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->nt()J

    move-result-wide v0

    iput-wide v0, p0, Lfkf;->ctN:J

    .line 254
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    invoke-static {v0}, Lfpc;->e(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpc;

    move-result-object v0

    .line 256
    invoke-virtual {p0, v0}, Lfkf;->a(Lfpc;)Z

    move-result v5

    .line 258
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v0}, Lizr;->bdv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    const v0, 0x7f110229

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 261
    :cond_1
    iget v0, p0, Lfkf;->ctO:I

    invoke-virtual {p0, p3, v0}, Lfkf;->x(Landroid/view/View;I)V

    .line 264
    const v0, 0x7f11025a

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 265
    new-instance v1, Lfki;

    invoke-direct {v1, p0, p2, p3}, Lfki;-><init>(Lfkf;Lfmt;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    const v0, 0x7f11025b

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/Button;

    .line 286
    new-instance v0, Lfkj;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lfkj;-><init>(Lfkf;Landroid/view/View;Lfmt;Landroid/app/Activity;Z)V

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    return-void
.end method

.method final a(Landroid/content/Context;Lfmt;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 350
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->aua()I

    move-result v0

    if-ne p3, v0, :cond_0

    .line 366
    :goto_0
    return-void

    .line 354
    :cond_0
    iget-object v0, p0, Lfkf;->ctQ:Lelg;

    invoke-virtual {v0, p3}, Lelg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    iget-boolean v0, v0, Lelh;->ceC:Z

    if-eqz v0, :cond_1

    .line 355
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->atZ()V

    .line 356
    const/16 v0, 0xb0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lfkf;->a(Landroid/content/Context;Lfmt;IZ)V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v1, p0, Lfkf;->ctQ:Lelg;

    iget-object v0, p0, Lfkf;->ctQ:Lelg;

    invoke-virtual {v0, p3}, Lelg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    invoke-virtual {v0}, Lelh;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lelg;->F(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, Lfjz;->mEntry:Lizj;

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 362
    iget-object v1, v0, Lizj;->dSK:Lizr;

    iget-object v2, p0, Lfkf;->ctM:Lizr;

    iget-object v2, v2, Lizr;->dVh:[Ljava/lang/String;

    aget-object v2, v2, p3

    invoke-virtual {v1, v2}, Lizr;->rO(Ljava/lang/String;)Lizr;

    .line 363
    const/16 v1, 0xa9

    invoke-interface {p2, v0, v1, v3, v3}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lfmt;IZ)V
    .locals 19

    .prologue
    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lfkf;->ctM:Lizr;

    invoke-virtual {v2}, Lizr;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 399
    invoke-virtual/range {p0 .. p0}, Lfkf;->aAC()Landroid/text/format/Time;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 402
    const-wide/32 v6, 0x36ee80

    add-long/2addr v6, v4

    .line 403
    move-object/from16 v0, p0

    iget-wide v11, v0, Lfkf;->ctN:J

    .line 405
    if-eqz p4, :cond_0

    .line 406
    const-string v3, ""

    const-string v8, ""

    invoke-static/range {v2 .. v8}, Ledv;->a(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 408
    const-string v3, "calendar_id"

    invoke-virtual {v2, v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 409
    const-string v3, "accessLevel"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 410
    move-object/from16 v0, p0

    iget-object v3, v0, Lfjz;->mActivityHelper:Lfzw;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v2}, Lfzw;->h(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 411
    move-object/from16 v0, p0

    iget-object v2, v0, Lfjz;->mEntry:Lizj;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lfmt;->u(Lizj;)V

    .line 437
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lfjz;->mEntry:Lizj;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v2, v1, v3, v4}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 438
    return-void

    .line 413
    :cond_0
    new-instance v8, Lfkk;

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-object v13, v2

    move-wide v14, v4

    move-wide/from16 v16, v6

    move-object/from16 v18, p1

    invoke-direct/range {v8 .. v18}, Lfkk;-><init>(Lfkf;Lfmt;JLjava/lang/String;JJLandroid/content/Context;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v8, v2}, Lfkk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final a(Lfmt;Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 209
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v0, v0, Lizr;->dNE:Lixx;

    if-nez v0, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    iget-object v0, v0, Lizr;->dNE:Lixx;

    iget-object v1, v0, Lixx;->dOR:[Lixy;

    array-length v2, v1

    move v0, v4

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 215
    iget-object v5, v3, Lixy;->dOz:Ljbg;

    if-eqz v5, :cond_2

    iget-object v5, v3, Lixy;->dOz:Ljbg;

    invoke-virtual {v5}, Ljbg;->beR()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 216
    const v0, 0x7f110224

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    new-instance v0, Lfrb;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, v3, Lixy;->dOz:Ljbg;

    invoke-virtual {v2}, Ljbg;->beQ()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v3}, Lizr;->getTitle()Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0xaa

    const-string v7, "mail"

    sget-object v8, Lgau;->cEv:[Ljava/lang/String;

    const/4 v9, 0x0

    move-object v5, p0

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Lfrb;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLfkd;ILjava/lang/String;[Ljava/lang/String;Lizj;Lfmt;)V

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 213
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final a(Lfpc;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 315
    invoke-virtual {p1}, Lfpc;->aCC()I

    move-result v2

    if-le v2, v0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 319
    :cond_1
    iget-object v2, p0, Lfkf;->ctM:Lizr;

    iget-object v3, v2, Lizr;->dVh:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 320
    const/16 v6, 0x54

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 319
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 324
    goto :goto_0
.end method

.method public final aAB()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lfkf;->ctO:I

    return v0
.end method

.method public final aAC()Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lfkf;->ctM:Lizr;

    invoke-virtual {v0}, Lizr;->bdq()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfkf;->lP(Ljava/lang/String;)Landroid/text/format/Time;

    move-result-object v0

    .line 378
    :goto_0
    return-object v0

    .line 372
    :cond_0
    iget-object v0, p0, Lfkf;->ctQ:Lelg;

    iget-object v1, p0, Lfkf;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lelg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    iget-object v0, v0, Lelh;->ceB:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 374
    if-eqz v0, :cond_1

    .line 375
    invoke-static {v0}, Lfkf;->lP(Ljava/lang/String;)Landroid/text/format/Time;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final x(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 334
    iput p2, p0, Lfkf;->ctO:I

    .line 335
    iget-object v0, p0, Lfkf;->ctR:[Lfkl;

    aget-object v0, v0, p2

    .line 336
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 337
    const v2, 0x7f110257

    iget v3, v0, Lfkl;->ctY:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 339
    const v2, 0x7f1100b0

    iget v3, v0, Lfkl;->ctZ:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 341
    const v2, 0x7f11025b

    iget v3, v0, Lfkl;->cua:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 343
    const v2, 0x7f11025a

    iget v0, v0, Lfkl;->cub:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 345
    return-void
.end method
