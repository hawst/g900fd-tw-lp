.class public final Lcjd;
.super Ljsl;
.source "PG"


# instance fields
.field public aYh:[Lcje;

.field public aYi:[Lcje;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 173
    invoke-static {}, Lcje;->KS()[Lcje;

    move-result-object v0

    iput-object v0, p0, Lcjd;->aYh:[Lcje;

    invoke-static {}, Lcje;->KS()[Lcje;

    move-result-object v0

    iput-object v0, p0, Lcjd;->aYi:[Lcje;

    const/4 v0, 0x0

    iput-object v0, p0, Lcjd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcjd;->eCz:I

    .line 174
    return-void
.end method

.method public static o([B)Lcjd;
    .locals 1

    .prologue
    .line 291
    new-instance v0, Lcjd;

    invoke-direct {v0}, Lcjd;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lcjd;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcjd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lcjd;->aYh:[Lcje;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcje;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcjd;->aYh:[Lcje;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcje;

    invoke-direct {v3}, Lcje;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcjd;->aYh:[Lcje;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcje;

    invoke-direct {v3}, Lcje;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lcjd;->aYh:[Lcje;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lcjd;->aYi:[Lcje;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcje;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcjd;->aYi:[Lcje;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcje;

    invoke-direct {v3}, Lcje;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcjd;->aYi:[Lcje;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcje;

    invoke-direct {v3}, Lcje;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lcjd;->aYi:[Lcje;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 187
    iget-object v0, p0, Lcjd;->aYh:[Lcje;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcjd;->aYh:[Lcje;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 188
    :goto_0
    iget-object v2, p0, Lcjd;->aYh:[Lcje;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 189
    iget-object v2, p0, Lcjd;->aYh:[Lcje;

    aget-object v2, v2, v0

    .line 190
    if-eqz v2, :cond_0

    .line 191
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 188
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_1
    iget-object v0, p0, Lcjd;->aYi:[Lcje;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcjd;->aYi:[Lcje;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 196
    :goto_1
    iget-object v0, p0, Lcjd;->aYi:[Lcje;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 197
    iget-object v0, p0, Lcjd;->aYi:[Lcje;

    aget-object v0, v0, v1

    .line 198
    if-eqz v0, :cond_2

    .line 199
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 196
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 203
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 204
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 208
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 209
    iget-object v2, p0, Lcjd;->aYh:[Lcje;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcjd;->aYh:[Lcje;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 210
    :goto_0
    iget-object v3, p0, Lcjd;->aYh:[Lcje;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 211
    iget-object v3, p0, Lcjd;->aYh:[Lcje;

    aget-object v3, v3, v0

    .line 212
    if-eqz v3, :cond_0

    .line 213
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 210
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 218
    :cond_2
    iget-object v2, p0, Lcjd;->aYi:[Lcje;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcjd;->aYi:[Lcje;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 219
    :goto_1
    iget-object v2, p0, Lcjd;->aYi:[Lcje;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 220
    iget-object v2, p0, Lcjd;->aYi:[Lcje;

    aget-object v2, v2, v1

    .line 221
    if-eqz v2, :cond_3

    .line 222
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 219
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 227
    :cond_4
    return v0
.end method
