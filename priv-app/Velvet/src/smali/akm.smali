.class public final Lakm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalx;


# static fields
.field private static final abF:Ljava/lang/Object;

.field private static abP:Lakm;


# instance fields
.field private abG:Landroid/content/Context;

.field private abH:Lakd;

.field private volatile abI:Lakf;

.field private abJ:I

.field private abK:Z

.field private abL:Z

.field private abM:Lake;

.field private abN:Lakl;

.field private abO:Z

.field private connected:Z

.field private handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lakm;->abF:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/16 v0, 0x708

    iput v0, p0, Lakm;->abJ:I

    .line 45
    iput-boolean v1, p0, Lakm;->abK:Z

    .line 48
    iput-boolean v1, p0, Lakm;->connected:Z

    .line 52
    iput-boolean v1, p0, Lakm;->abL:Z

    .line 54
    new-instance v0, Lakn;

    invoke-direct {v0, p0}, Lakn;-><init>(Lakm;)V

    iput-object v0, p0, Lakm;->abM:Lake;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lakm;->abO:Z

    .line 80
    return-void
.end method

.method static synthetic a(Lakm;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lakm;->connected:Z

    return v0
.end method

.method static synthetic b(Lakm;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lakm;->abJ:I

    return v0
.end method

.method static synthetic c(Lakm;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lakm;->abO:Z

    return v0
.end method

.method static synthetic d(Lakm;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static mr()Lakm;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lakm;->abP:Lakm;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lakm;

    invoke-direct {v0}, Lakm;-><init>()V

    sput-object v0, Lakm;->abP:Lakm;

    .line 76
    :cond_0
    sget-object v0, Lakm;->abP:Lakm;

    return-object v0
.end method

.method static synthetic mt()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lakm;->abF:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method final declared-synchronized a(Landroid/content/Context;Lakf;)V
    .locals 1

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakm;->abG:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 135
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lakm;->abG:Landroid/content/Context;

    .line 137
    iget-object v0, p0, Lakm;->abI:Lakf;

    if-nez v0, :cond_0

    .line 138
    iput-object p2, p0, Lakm;->abI:Lakf;

    .line 139
    iget-boolean v0, p0, Lakm;->abK:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-interface {p2}, Lakf;->mp()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized az(Z)V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lakm;->abO:Z

    invoke-virtual {p0, v0, p1}, Lakm;->e(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized bZ(I)V
    .locals 3

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 190
    const-string v0, "Need to call initialize() and be in fallback mode to start dispatch."

    invoke-static {v0}, Lalm;->F(Ljava/lang/String;)I

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lakm;->abJ:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :goto_0
    monitor-exit p0

    return-void

    .line 195
    :cond_0
    :try_start_1
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->adC:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 197
    iget-boolean v0, p0, Lakm;->abO:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lakm;->connected:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lakm;->abJ:I

    if-lez v0, :cond_1

    .line 198
    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lakm;->abF:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 200
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lakm;->abJ:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e(ZZ)V
    .locals 4

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lakm;->abO:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lakm;->connected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    .line 223
    :goto_0
    monitor-exit p0

    return-void

    .line 212
    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    :try_start_1
    iget v0, p0, Lakm;->abJ:I

    if-lez v0, :cond_2

    .line 213
    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lakm;->abF:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 215
    :cond_2
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    iget v0, p0, Lakm;->abJ:I

    if-lez v0, :cond_3

    .line 216
    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lakm;->handler:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lakm;->abF:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lakm;->abJ:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 219
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "PowerSaveMode "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_4

    if-nez p2, :cond_5

    :cond_4
    const-string v0, "initiated."

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 221
    iput-boolean p1, p0, Lakm;->abO:Z

    .line 222
    iput-boolean p2, p0, Lakm;->connected:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 219
    :cond_5
    :try_start_2
    const-string v0, "terminated."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized mp()V
    .locals 2

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakm;->abI:Lakf;

    if-nez v0, :cond_0

    .line 178
    const-string v0, "dispatch call queued.  Need to call GAServiceManager.getInstance().initialize()."

    invoke-static {v0}, Lalm;->F(Ljava/lang/String;)I

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lakm;->abK:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :goto_0
    monitor-exit p0

    return-void

    .line 183
    :cond_0
    :try_start_1
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->adB:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 184
    iget-object v0, p0, Lakm;->abI:Lakf;

    invoke-interface {v0}, Lakf;->mp()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized ms()Lakd;
    .locals 4

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lakm;->abH:Lakd;

    if-nez v0, :cond_1

    .line 156
    iget-object v0, p0, Lakm;->abG:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant get a store unless we have a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 161
    :cond_0
    :try_start_1
    new-instance v0, Lalt;

    iget-object v1, p0, Lakm;->abM:Lake;

    iget-object v2, p0, Lakm;->abG:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lalt;-><init>(Lake;Landroid/content/Context;)V

    iput-object v0, p0, Lakm;->abH:Lakd;

    .line 163
    :cond_1
    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 165
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lakm;->abG:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lako;

    invoke-direct {v2, p0}, Lako;-><init>(Lakm;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    iget v0, p0, Lakm;->abJ:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lakm;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lakm;->handler:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lakm;->abF:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lakm;->abJ:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 167
    :cond_2
    iget-object v0, p0, Lakm;->abN:Lakl;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lakm;->abL:Z

    if-eqz v0, :cond_3

    .line 168
    new-instance v0, Lakl;

    invoke-direct {v0, p0}, Lakl;-><init>(Lalx;)V

    iput-object v0, p0, Lakm;->abN:Lakl;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lakm;->abG:Landroid/content/Context;

    iget-object v2, p0, Lakm;->abN:Lakl;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 170
    :cond_3
    iget-object v0, p0, Lakm;->abH:Lakd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method
