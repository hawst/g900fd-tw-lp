.class public final Laqj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final amC:Laqj;


# instance fields
.field public final amA:Ljava/lang/String;

.field public final amB:Z

.field public final amz:Ljava/lang/String;

.field public final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Laqj;

    invoke-direct {v0}, Laqj;-><init>()V

    sput-object v0, Laqj;->amC:Laqj;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const-string v0, "DEFAULT"

    iput-object v0, p0, Laqj;->mName:Ljava/lang/String;

    .line 140
    const-string v0, ""

    iput-object v0, p0, Laqj;->amz:Ljava/lang/String;

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Laqj;->amA:Ljava/lang/String;

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Laqj;->amB:Z

    .line 143
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Laqj;->mName:Ljava/lang/String;

    .line 91
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v9

    .line 92
    invoke-static {p2, v2}, Laqj;->c(Ljava/lang/String;I)I

    move-result v0

    .line 93
    if-ne v0, v9, :cond_0

    new-instance v0, Laqk;

    const-string v1, "Empty rule"

    invoke-direct {v0, v1}, Laqk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    if-eqz v0, :cond_1

    new-instance v0, Laqk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Rule with leading whitespace: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laqk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_1
    add-int/lit8 v3, v0, 0x1

    invoke-static {p2, v3}, Laqj;->b(Ljava/lang/String;I)I

    move-result v3

    .line 100
    invoke-virtual {p2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laqj;->amz:Ljava/lang/String;

    .line 102
    const/4 v0, 0x0

    move v6, v2

    move-object v7, v0

    move v0, v3

    .line 105
    :goto_0
    if-eq v0, v9, :cond_4

    add-int/lit8 v0, v0, 0x1

    invoke-static {p2, v0}, Laqj;->c(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v9, :cond_4

    .line 106
    add-int/lit8 v0, v4, 0x1

    invoke-static {p2, v0}, Laqj;->b(Ljava/lang/String;I)I

    move-result v8

    .line 107
    sub-int v5, v8, v4

    .line 109
    const/4 v0, 0x7

    if-ne v0, v5, :cond_2

    const-string v0, "rewrite"

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eq v8, v9, :cond_2

    add-int/lit8 v0, v8, 0x1

    invoke-static {p2, v0}, Laqj;->c(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v9, :cond_2

    .line 111
    add-int/lit8 v3, v0, 0x1

    invoke-static {p2, v3}, Laqj;->b(Ljava/lang/String;I)I

    move-result v3

    .line 112
    invoke-virtual {p2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    move v0, v3

    goto :goto_0

    .line 113
    :cond_2
    const/4 v0, 0x5

    if-ne v0, v5, :cond_3

    const-string v0, "block"

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3

    move v6, v1

    move v0, v8

    .line 114
    goto :goto_0

    .line 116
    :cond_3
    new-instance v0, Laqk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal rule: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Laqk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_4
    iput-object v7, p0, Laqj;->amA:Ljava/lang/String;

    .line 121
    iput-boolean v6, p0, Laqj;->amB:Z

    .line 122
    return-void
.end method

.method private static final b(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 125
    const/16 v0, 0x20

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 126
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method private static final c(Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 130
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 131
    :goto_0
    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x20

    if-ne v1, v2, :cond_0

    .line 132
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 134
    :cond_0
    return p1
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 68
    check-cast p1, Laqj;

    iget-object v0, p1, Laqj;->amz:Ljava/lang/String;

    iget-object v1, p0, Laqj;->amz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final dZ(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    iget-boolean v0, p0, Laqj;->amB:Z

    if-eqz v0, :cond_1

    .line 152
    const/4 p1, 0x0

    .line 156
    :cond_0
    :goto_0
    return-object p1

    .line 153
    :cond_1
    iget-object v0, p0, Laqj;->amA:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Laqj;->amA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laqj;->amz:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
