.class public final Lcvu;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final bll:Ljava/util/HashSet;

.field private final blm:Ljava/util/HashSet;

.field final mContext:Landroid/content/Context;

.field final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 41
    iput-object p1, p0, Lcvu;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcvu;->mPreferences:Landroid/content/SharedPreferences;

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcvu;->bll:Ljava/util/HashSet;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcvu;->blm:Ljava/util/HashSet;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 9

    .prologue
    const v8, 0x7f0a0591

    const/4 v2, 0x0

    .line 50
    invoke-static {}, Lcgg;->values()[Lcgg;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    move-object v0, p1

    .line 51
    check-cast v0, Landroid/preference/PreferenceGroup;

    new-instance v6, Lcvw;

    iget-object v7, p0, Lcvu;->mContext:Landroid/content/Context;

    invoke-direct {v6, p0, v7}, Lcvw;-><init>(Lcvu;Landroid/content/Context;)V

    invoke-virtual {v5}, Lcgg;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v2}, Lerr;->r(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/SwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcgg;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/SwitchPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcgg;->isEnabled()Z

    move-result v5

    invoke-virtual {v6, v5}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    invoke-virtual {v6, v2}, Landroid/preference/SwitchPreference;->setPersistent(Z)V

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 50
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 54
    :cond_0
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, Lcvu;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 55
    const v0, 0x7f0a058e

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 56
    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object v0, p1

    .line 57
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 59
    check-cast p1, Landroid/preference/PreferenceGroup;

    new-instance v0, Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcvu;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v8}, Landroid/preference/EditTextPreference;->setTitle(I)V

    invoke-virtual {v0, v8}, Landroid/preference/EditTextPreference;->setDialogTitle(I)V

    const v1, 0x7f0a0592

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setDialogMessage(I)V

    const-string v1, ","

    invoke-static {v1}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v1

    iget-object v2, p0, Lcvu;->mPreferences:Landroid/content/SharedPreferences;

    const-string v3, "now_opted_in_experiments"

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    new-instance v1, Lcvv;

    invoke-direct {v1, p0}, Lcvv;-><init>(Lcvu;)V

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 60
    return-void
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 127
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcvu;->blm:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 129
    iget-object v1, p0, Lcvu;->bll:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 134
    :goto_0
    const v0, 0x7f0a0594

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 135
    const/4 v0, 0x1

    return v0

    .line 131
    :cond_0
    iget-object v1, p0, Lcvu;->blm:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 132
    iget-object v1, p0, Lcvu;->bll:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcvu;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "enabled_features"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lesp;->e(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 142
    iget-object v0, p0, Lcvu;->mPreferences:Landroid/content/SharedPreferences;

    const-string v3, "disabled_features"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lesp;->e(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    .line 144
    iget-object v0, p0, Lcvu;->bll:Ljava/util/HashSet;

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 145
    iget-object v0, p0, Lcvu;->blm:Ljava/util/HashSet;

    invoke-interface {v2, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 146
    iget-object v0, p0, Lcvu;->blm:Ljava/util/HashSet;

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 147
    iget-object v0, p0, Lcvu;->bll:Ljava/util/HashSet;

    invoke-interface {v3, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 150
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 151
    invoke-static {}, Lcgg;->values()[Lcgg;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    .line 152
    invoke-virtual {v7}, Lcgg;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    invoke-interface {v2, v4}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 155
    invoke-interface {v3, v4}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 158
    iget-object v0, p0, Lcvu;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "enabled_features"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 159
    iget-object v0, p0, Lcvu;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "disabled_features"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 160
    invoke-static {v1}, Ljava/lang/System;->exit(I)V

    .line 161
    const/4 v0, 0x1

    return v0
.end method
