.class public final Lgii;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final cJY:Lchk;

.field private cLe:Ljava/util/List;

.field private final mClock:Lemp;

.field public mSearchResult:Lcmq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lchk;Lemp;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lgii;->cJY:Lchk;

    .line 31
    iput-object p2, p0, Lgii;->mClock:Lemp;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgii;->cLe:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 8

    .prologue
    .line 54
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 55
    iget-object v0, p0, Lgii;->cJY:Lchk;

    invoke-virtual {v0}, Lchk;->FV()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lgii;->mSearchResult:Lcmq;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lgii;->mSearchResult:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qt()Lcms;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcms;->bcy:Ljyw;

    if-eqz v1, :cond_0

    .line 60
    iget-object v0, v0, Lcms;->bcy:Ljyw;

    iget-object v1, v0, Ljyw;->eME:[Ljava/lang/String;

    .line 61
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    .line 63
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    iget-object v0, p0, Lgii;->cLe:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    .line 68
    iget-object v0, p0, Lgii;->cLe:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lgii;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    .line 70
    iget-object v0, p0, Lgii;->cLe:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 67
    :goto_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 73
    :cond_1
    iget-object v0, p0, Lgii;->cLe:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 77
    :cond_2
    return-object v2
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lgii;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final p(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lgii;->cLe:Ljava/util/List;

    iget-object v1, p0, Lgii;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method
