.class public abstract Lfro;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field cBb:Lfuz;

.field cBc:Lanh;

.field protected final mActivityHelper:Lfzw;

.field protected final mCardContainer:Lfmt;

.field protected final mContext:Landroid/content/Context;

.field mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lfro;->mContext:Landroid/content/Context;

    .line 64
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmt;

    iput-object v0, p0, Lfro;->mCardContainer:Lfmt;

    .line 65
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfzw;

    iput-object v0, p0, Lfro;->mActivityHelper:Lfzw;

    .line 66
    return-void
.end method

.method protected static b(Landroid/view/View;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 406
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    invoke-static {p0, p1, p2}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 409
    :cond_0
    return-void
.end method

.method private jv(I)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f110012

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 139
    return-void
.end method


# virtual methods
.method public W(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 431
    return-void
.end method

.method public final a(Lanh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 110
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    iput-object v0, p0, Lfro;->cBc:Lanh;

    .line 111
    invoke-virtual {p0}, Lfro;->aCZ()V

    .line 112
    invoke-virtual {p0}, Lfro;->aDb()V

    .line 113
    invoke-virtual {p0}, Lfro;->aDc()V

    .line 114
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lfuz;Lanh;)Landroid/view/View;
    .locals 3

    .prologue
    .line 72
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfuz;

    iput-object v0, p0, Lfro;->cBb:Lfuz;

    .line 73
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    iput-object v0, p0, Lfro;->cBc:Lanh;

    .line 75
    invoke-virtual {p0}, Lfro;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfro;->b(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 76
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f11000d

    invoke-virtual {p2}, Lanh;->oT()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 77
    iget-object v0, p2, Lanh;->ahu:Lizj;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f110002

    iget-object v2, p2, Lanh;->ahu:Lizj;

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 83
    invoke-virtual {p0}, Lfro;->aCZ()V

    .line 84
    invoke-virtual {p0}, Lfro;->aDb()V

    .line 85
    invoke-virtual {p0}, Lfro;->aDc()V

    .line 87
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    return-object v0
.end method

.method protected final a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 396
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 398
    iget-object v1, p0, Lfro;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v1, v1, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, p3, v1}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 399
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 402
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/view/View;Lani;)V
    .locals 7

    .prologue
    .line 277
    if-nez p2, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahu:Lizj;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lani;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a module entry. Card type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v2}, Lfuz;->getEntry()Lizj;

    move-result-object v2

    invoke-virtual {v2}, Lizj;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    new-instance v0, Lfsb;

    iget-object v1, p0, Lfro;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfro;->mCardContainer:Lfmt;

    iget-object v3, p0, Lfro;->mActivityHelper:Lfzw;

    invoke-direct {v0, v1, v2, v3, p0}, Lfsb;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lfro;)V

    .line 286
    invoke-virtual {v0, p2}, Lfsb;->a(Lani;)Ljava/lang/Runnable;

    move-result-object v6

    .line 288
    if-eqz v6, :cond_0

    .line 290
    new-instance v0, Lfrq;

    iget-object v2, p0, Lfro;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v3, v1, Lanh;->ahu:Lizj;

    invoke-virtual {p2}, Lani;->oY()I

    move-result v4

    iget-object v5, p2, Lani;->ahD:Lixx;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lfrq;-><init>(Lfro;Lfmt;Lizj;ILixx;Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected final a(Landroid/view/View;Laoi;Z)V
    .locals 5
    .param p2    # Laoi;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f110212

    const/4 v4, 0x0

    .line 351
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Laoi;->qG()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 352
    const v0, 0x7f1102ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 353
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 355
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;

    .line 358
    if-eqz p3, :cond_0

    .line 359
    invoke-virtual {p0, v2}, Lfro;->jy(I)V

    .line 362
    :cond_0
    invoke-virtual {p2}, Laoi;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lfro;->mCardContainer:Lfmt;

    invoke-interface {v3}, Lfmt;->aAD()Lfml;

    move-result-object v3

    iget-object v3, v3, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->a(Landroid/net/Uri;Lesm;)V

    .line 366
    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->hi(I)V

    .line 369
    iget-object v0, p2, Laoi;->aju:Lani;

    if-eqz v0, :cond_1

    .line 370
    iget-object v0, p2, Laoi;->aju:Lani;

    invoke-virtual {p0, v1, v0}, Lfro;->a(Landroid/view/View;Lani;)V

    .line 374
    :cond_1
    invoke-virtual {p2}, Laoi;->pd()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 375
    const v0, 0x7f110213

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 376
    invoke-virtual {p2}, Laoi;->pc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 380
    iget-object v2, p2, Laoi;->ajv:Lani;

    if-eqz v2, :cond_2

    .line 381
    iget-object v2, p2, Laoi;->ajv:Lani;

    invoke-virtual {p0, v0, v2}, Lfro;->a(Landroid/view/View;Lani;)V

    .line 386
    :cond_2
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 388
    :cond_3
    return-void
.end method

.method protected abstract aCZ()V
.end method

.method protected aDb()V
    .locals 8

    .prologue
    const-wide v6, 0x3fe999999999999aL    # 0.8

    .line 145
    const/4 v0, 0x0

    .line 146
    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->hasBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 147
    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lfro;->jx(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->getBackgroundColor()I

    move-result v1

    invoke-direct {p0, v1}, Lfro;->jv(I)V

    .line 172
    :cond_0
    :goto_0
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->ahs:Lani;

    if-eqz v1, :cond_1

    .line 173
    iget-object v1, p0, Lfro;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020296

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 175
    if-eqz v0, :cond_7

    .line 176
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v2, v3, v0

    invoke-direct {v1, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    .line 182
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 184
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 185
    iget-object v2, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    .line 186
    iget-object v3, p0, Lfro;->mView:Landroid/view/View;

    invoke-static {v3}, Leot;->bc(Landroid/view/View;)I

    move-result v3

    .line 187
    iget-object v4, p0, Lfro;->mView:Landroid/view/View;

    invoke-static {v4}, Leot;->bd(Landroid/view/View;)I

    move-result v4

    .line 189
    iget-object v5, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 191
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    invoke-static {v0, v3, v1, v4, v2}, Leot;->a(Landroid/view/View;IIII)V

    .line 193
    :cond_2
    return-void

    .line 149
    :cond_3
    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->oR()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 150
    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->oQ()I

    move-result v0

    invoke-virtual {p0, v0}, Lfro;->jx(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 151
    :cond_4
    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->oV()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v0}, Lfuz;->aDA()I

    move-result v0

    .line 154
    if-nez v0, :cond_5

    iget-object v0, p0, Lfro;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 157
    :cond_5
    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->oW()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 160
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v2, v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-double v4, v3

    mul-double/2addr v4, v6

    double-to-int v3, v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-double v4, v0

    mul-double/2addr v4, v6

    double-to-int v0, v4

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 167
    :cond_6
    invoke-virtual {p0, v0}, Lfro;->jx(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 168
    invoke-direct {p0, v0}, Lfro;->jv(I)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_7
    move-object v0, v2

    .line 178
    goto/16 :goto_1
.end method

.method protected aDc()V
    .locals 8

    .prologue
    .line 230
    invoke-virtual {p0}, Lfro;->aDd()V

    .line 232
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->ahs:Lani;

    invoke-virtual {p0, v0, v1}, Lfro;->a(Landroid/view/View;Lani;)V

    .line 244
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v7, p0, Lfro;->mView:Landroid/view/View;

    new-instance v0, Lfrp;

    iget-object v2, p0, Lfro;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v3, v1, Lanh;->ahu:Lizj;

    const/16 v4, 0xf7

    const/4 v5, 0x0

    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lfrp;-><init>(Lfro;Lfmt;Lizj;ILixx;Ljava/lang/Integer;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected final aDd()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 247
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->ahs:Lani;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v1, v0, Lanh;->ahs:Lani;

    .line 250
    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->oX()Z

    move-result v0

    if-nez v0, :cond_2

    .line 251
    iget-object v0, v1, Lani;->ahC:Lanb;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, v1, Lani;->ahC:Lanb;

    invoke-virtual {v0}, Lanb;->oi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v2, 0x7f1102eb

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 254
    if-eqz v0, :cond_0

    .line 255
    iget-object v1, v1, Lani;->ahC:Lanb;

    invoke-virtual {v1}, Lanb;->oh()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 256
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, v1, Lani;->ahC:Lanb;

    invoke-virtual {v0}, Lanb;->ok()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v2, 0x7f110323

    iget-object v1, v1, Lani;->ahC:Lanb;

    invoke-virtual {v1}, Lanb;->oj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v2, v1}, Lfro;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    goto :goto_0

    .line 266
    :cond_2
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f1102f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 267
    if-eqz v1, :cond_0

    .line 268
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 269
    invoke-static {v0, v3, v3, v3, v3}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 270
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final aDe()Lanh;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lfro;->cBc:Lanh;

    return-object v0
.end method

.method protected final aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v0}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v0

    return-object v0
.end method

.method public final aDg()Lfuz;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lfro;->cBb:Lfuz;

    return-object v0
.end method

.method protected abstract b(Landroid/view/LayoutInflater;)Landroid/view/View;
.end method

.method public final b(Lanh;)Z
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p1}, Lanh;->getType()I

    move-result v0

    iget-object v1, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->getType()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final fA(Z)V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    const v1, 0x7f110011

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 131
    return-void
.end method

.method protected final getLayoutInflater()Landroid/view/LayoutInflater;
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->hasBackgroundColor()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->oV()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 323
    :goto_0
    iget-object v1, p0, Lfro;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->wu()Z

    move-result v1

    .line 326
    if-eqz v0, :cond_3

    .line 327
    if-eqz v1, :cond_2

    .line 328
    const v0, 0x7f09017b

    .line 340
    :goto_1
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lfro;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 341
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0

    .line 321
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_2
    const v0, 0x7f09017a

    goto :goto_1

    .line 333
    :cond_3
    if-eqz v1, :cond_4

    .line 334
    const v0, 0x7f090179

    goto :goto_1

    .line 336
    :cond_4
    const v0, 0x7f090178

    goto :goto_1
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    return-object v0
.end method

.method public jw(I)V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0}, Lfro;->aDb()V

    .line 203
    return-void
.end method

.method protected final jx(I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lfro;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 211
    new-instance v1, Lfxu;

    invoke-direct {v1, p1, v0}, Lfxu;-><init>(II)V

    .line 215
    iget-object v0, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v0, p0}, Lfuz;->b(Lfro;)I

    move-result v0

    .line 216
    iget-object v2, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v2}, Lfuz;->aDz()I

    move-result v2

    .line 217
    if-nez v0, :cond_1

    .line 218
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lfxu;->hi(I)V

    .line 223
    :cond_0
    :goto_0
    return-object v1

    .line 219
    :cond_1
    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_0

    .line 220
    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Lfxu;->hi(I)V

    goto :goto_0
.end method

.method protected final jy(I)V
    .locals 5

    .prologue
    .line 438
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 439
    new-instance v1, Lftv;

    iget-object v2, p0, Lfro;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lfro;->mCardContainer:Lfmt;

    invoke-interface {v3}, Lfmt;->wu()Z

    move-result v3

    new-instance v4, Lfrr;

    invoke-direct {v4, p0}, Lfrr;-><init>(Lfro;)V

    invoke-direct {v1, v2, v3, v4}, Lftv;-><init>(Landroid/content/Context;ZLemy;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ledl;)V

    .line 441
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 416
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 423
    return-void
.end method
