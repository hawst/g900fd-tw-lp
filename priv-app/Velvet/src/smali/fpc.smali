.class public Lfpc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cyp:Ljava/lang/String;


# instance fields
.field private final mBundle:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lfpc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfpc;->cyp:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lfpc;->mBundle:Landroid/os/Bundle;

    .line 40
    return-void
.end method

.method public static b(Landroid/accounts/Account;I)Landroid/os/Bundle;
    .locals 2
    .param p0    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 30
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 31
    if-eqz p0, :cond_0

    .line 32
    const-string v1, "active_account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 34
    :cond_0
    const-string v1, "num_accounts"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 35
    return-object v0
.end method

.method public static e(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpc;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lfpc;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 23
    if-eqz v0, :cond_0

    .line 24
    new-instance v1, Lfpc;

    invoke-direct {v1, v0}, Lfpc;-><init>(Landroid/os/Bundle;)V

    move-object v0, v1

    .line 26
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final aCB()Landroid/accounts/Account;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lfpc;->mBundle:Landroid/os/Bundle;

    const-string v1, "active_account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    return-object v0
.end method

.method public final aCC()I
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lfpc;->mBundle:Landroid/os/Bundle;

    const-string v1, "num_accounts"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
