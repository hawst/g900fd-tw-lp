.class public abstract Lcmy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bcJ:Ljava/util/List;

.field private volatile mConsumer:Lcmz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcmy;->bcJ:Ljava/util/List;

    return-void
.end method

.method protected static a(Lgpf;Ldyj;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0, p2, p1}, Lgpf;->a(Ljava/lang/String;Ldyj;)V

    .line 154
    return-void
.end method

.method protected static b(Ldyj;)Leeb;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 160
    invoke-static {p0}, Leeb;->c(Ldyj;)Leeb;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final QE()Lcmz;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcmy;->mConsumer:Lcmz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 113
    iget-object v0, p0, Lcmy;->mConsumer:Lcmz;

    return-object v0

    .line 112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcmz;)V
    .locals 1
    .param p1    # Lcmz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 97
    iget-object v0, p0, Lcmy;->mConsumer:Lcmz;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 98
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmz;

    iput-object v0, p0, Lcmy;->mConsumer:Lcmz;

    .line 99
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final c(Lcom/google/android/velvet/ActionData;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 122
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-ne p1, v0, :cond_2

    .line 127
    :cond_0
    iget-object v0, p0, Lcmy;->bcJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcmy;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->b(Lcom/google/android/velvet/ActionData;)V

    .line 143
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-virtual {p0}, Lcmy;->QE()Lcmz;

    move-result-object v1

    iget-object v0, p0, Lcmy;->bcJ:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActionData;

    invoke-interface {v1, v0}, Lcmz;->b(Lcom/google/android/velvet/ActionData;)V

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, p0, Lcmy;->bcJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 135
    const-string v0, "Velvet.SearchResultFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Multiple actions received. Using only the first.  New action="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Existing actions="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcmy;->bcJ:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 141
    :cond_3
    iget-object v0, p0, Lcmy;->bcJ:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected abstract cancel()V
.end method
