.class public final Ldnm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aMD:Leqo;

.field final aRU:Ligi;

.field private final aTW:Ljava/util/concurrent/Executor;

.field private final bDB:Ligi;

.field private final bDC:Lepp;

.field private final bDD:Ljava/lang/Runnable;

.field private bDE:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bDF:Lhyq;

.field private bDG:Lgrx;

.field private bDH:Livq;

.field public mActionCardEventLogger:Legs;

.field private final mContext:Landroid/content/Context;

.field private final mDiscourseContextSupplier:Ligi;

.field final mEventBus:Ldda;

.field private final mFactory:Lgpu;

.field private final mGsaConfigFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPersonShortcutManager:Lciy;

.field final mVoiceSearchServices:Lhhq;


# direct methods
.method public constructor <init>(Ldda;Lgpu;Lhhq;Lcrh;Ligi;Ligi;Ljava/util/concurrent/Executor;Leqo;Landroid/content/Context;Lchk;Lciy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object v0, p0, Ldnm;->mActionCardEventLogger:Legs;

    .line 107
    iput-object v0, p0, Ldnm;->bDE:Lcom/google/android/shared/search/Query;

    .line 126
    iput-object p1, p0, Ldnm;->mEventBus:Ldda;

    .line 127
    iput-object p2, p0, Ldnm;->mFactory:Lgpu;

    .line 128
    iput-object p3, p0, Ldnm;->mVoiceSearchServices:Lhhq;

    .line 129
    iput-object p4, p0, Ldnm;->mLoginHelper:Lcrh;

    .line 130
    iput-object p5, p0, Ldnm;->aRU:Ligi;

    .line 131
    iput-object p6, p0, Ldnm;->mDiscourseContextSupplier:Ligi;

    .line 132
    iput-object p7, p0, Ldnm;->aTW:Ljava/util/concurrent/Executor;

    .line 133
    invoke-virtual {p9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Ldnm;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 134
    iput-object p9, p0, Ldnm;->mContext:Landroid/content/Context;

    .line 135
    iput-object p8, p0, Ldnm;->aMD:Leqo;

    .line 136
    iput-object p10, p0, Ldnm;->mGsaConfigFlags:Lchk;

    .line 137
    iput-object p11, p0, Ldnm;->mPersonShortcutManager:Lciy;

    .line 138
    new-instance v0, Ldnn;

    invoke-direct {v0, p0, p9}, Ldnn;-><init>(Ldnm;Landroid/content/Context;)V

    iput-object v0, p0, Ldnm;->bDB:Ligi;

    .line 153
    new-instance v0, Ldnp;

    const-string v1, "ShowExecutionStates"

    invoke-direct {v0, p0, v1}, Ldnp;-><init>(Ldnm;Ljava/lang/String;)V

    iput-object v0, p0, Ldnm;->bDC:Lepp;

    .line 159
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    iput-object v0, p0, Ldnm;->mActionCardEventLogger:Legs;

    .line 160
    new-instance v0, Ldnq;

    invoke-direct {v0, p0}, Ldnq;-><init>(Ldnm;)V

    iput-object v0, p0, Ldnm;->bDD:Ljava/lang/Runnable;

    .line 166
    return-void
.end method

.method private a(Ldbd;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 478
    invoke-virtual {p1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    .line 479
    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2, v6}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v1

    .line 480
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aIB()Ljyw;

    move-result-object v0

    iget-object v3, v0, Ljyw;->eAp:Liwg;

    .line 482
    :goto_1
    iget-object v0, p0, Ldnm;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {p1, p2}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aoM()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aIA()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v6, 0x1

    :cond_0
    iget-object v2, p0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Leqt;->H(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Lcky;->a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;Liwg;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljava/lang/String;ZLjava/lang/String;)V

    .line 487
    return-void

    :cond_1
    move-object v1, v0

    .line 479
    goto :goto_0

    :cond_2
    move-object v3, v0

    .line 480
    goto :goto_1
.end method

.method public static a(Ldbd;Ldcw;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 316
    invoke-virtual {p1}, Ldcw;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ldcw;->Zv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    :cond_0
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    .line 318
    invoke-virtual {p0, v1}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    .line 321
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->aln()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ldbd;->Vq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    invoke-virtual {p0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    .line 329
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 332
    :goto_0
    const/16 v3, 0x11e

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/search/shared/actions/VoiceAction;->a(ILcom/google/android/search/shared/actions/utils/CardDecision;I)V

    .line 336
    invoke-virtual {p0, v1, v4, v4}, Ldbd;->a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V

    .line 344
    :cond_1
    return-void

    .line 329
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;I)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 352
    iget-object v0, p0, Ldnm;->aRU:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhll;

    invoke-virtual {v0, p1}, Lhll;->I(Lcom/google/android/search/shared/actions/VoiceAction;)Lhlk;

    move-result-object v6

    .line 357
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v0

    if-nez v0, :cond_0

    if-eq p2, v1, :cond_1

    :cond_0
    move v0, v1

    .line 358
    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 364
    :goto_1
    if-nez v0, :cond_3

    .line 365
    invoke-virtual {p0, p1}, Ldnm;->v(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 406
    :goto_2
    return-void

    :cond_1
    move v0, v9

    .line 357
    goto :goto_0

    :cond_2
    move v0, v9

    .line 358
    goto :goto_1

    .line 369
    :cond_3
    new-instance v0, Ldnr;

    const-string v2, "ActionWorker execute"

    iget-object v3, p0, Ldnm;->aMD:Leqo;

    iget-object v4, p0, Ldnm;->aTW:Ljava/util/concurrent/Executor;

    const/4 v1, 0x2

    new-array v5, v1, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    move-object v7, p1

    move v8, p2

    invoke-direct/range {v0 .. v8}, Ldnr;-><init>(Ldnm;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[ILhlk;Lcom/google/android/search/shared/actions/VoiceAction;I)V

    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldnr;->a([Ljava/lang/Object;)Lenp;

    goto :goto_2

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public final a(Lddb;)V
    .locals 19

    .prologue
    .line 169
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v11

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v12

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aao()Ldcu;

    move-result-object v13

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aak()Ldcw;

    move-result-object v14

    .line 173
    invoke-virtual {v13}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v15

    .line 174
    invoke-virtual {v12}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v16

    .line 175
    invoke-virtual/range {p1 .. p1}, Lddb;->aax()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 176
    invoke-virtual {v11}, Ldbd;->VW()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->bDH:Livq;

    if-nez v2, :cond_0

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mFactory:Lgpu;

    invoke-virtual {v2}, Lgpu;->aJw()Livq;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ldnm;->bDH:Livq;

    .line 179
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->bDH:Livq;

    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->bDD:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldnm;->aMD:Leqo;

    invoke-interface {v2, v3, v4}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 182
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->yo()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    if-eqz v4, :cond_1

    if-nez v2, :cond_11

    .line 183
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v10

    invoke-virtual {v10}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-interface {v8}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Lcom/google/android/shared/util/MatchingAppInfo;->avm()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ldns;

    const-string v4, "ActionWorker refresh"

    move-object/from16 v0, p0

    iget-object v5, v0, Ldnm;->aMD:Leqo;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldnm;->aTW:Ljava/util/concurrent/Executor;

    const/4 v3, 0x2

    new-array v7, v3, [I

    fill-array-data v7, :array_0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v10}, Ldns;-><init>(Ldnm;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[ILcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/shared/util/MatchingAppInfo;Ldbd;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Ldns;->a([Ljava/lang/Object;)Lenp;

    .line 184
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v9

    invoke-virtual {v9}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v4

    invoke-virtual {v9}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v3

    invoke-virtual {v9}, Ldbd;->VA()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v4, :cond_3

    if-nez v3, :cond_16

    .line 185
    :cond_3
    :goto_1
    invoke-virtual {v11}, Ldbd;->Vw()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v11}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v2}, Ldnm;->a(Ldbd;Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 187
    :cond_4
    :goto_2
    invoke-virtual {v11}, Ldbd;->Vs()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 188
    invoke-virtual {v11}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    .line 189
    if-eqz v2, :cond_5

    .line 190
    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->mPersonShortcutManager:Lciy;

    invoke-virtual {v3, v2}, Lciy;->d(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 194
    :cond_5
    invoke-virtual {v11}, Ldbd;->Vp()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    .line 195
    if-eqz v2, :cond_7

    .line 198
    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aaj()Ldbd;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ldnm;->bDF:Lhyq;

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Ldnm;->mFactory:Lgpu;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldnm;->mEventBus:Ldda;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldnm;->bDB:Ligi;

    move-object/from16 v0, p0

    iget-object v7, v0, Ldnm;->aRU:Ligi;

    invoke-virtual {v4, v5, v6, v7}, Lgpu;->a(Ldda;Ligi;Ligi;)Lhyq;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ldnm;->bDF:Lhyq;

    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Ldnm;->bDF:Lhyq;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldnm;->mLoginHelper:Lcrh;

    invoke-virtual {v5}, Lcrh;->St()Z

    move-result v5

    move-object/from16 v0, v16

    invoke-virtual {v4, v0, v2, v5}, Lhyq;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Z)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {v3, v2}, Ldbd;->d(Lcom/google/android/velvet/ActionData;)V

    .line 201
    :cond_7
    invoke-virtual {v11}, Ldbd;->VE()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 202
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->GW()J

    move-result-wide v2

    .line 203
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_1a

    .line 204
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->bDC:Lepp;

    invoke-virtual {v2}, Lepp;->run()V

    .line 234
    :cond_8
    :goto_3
    invoke-virtual {v12}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v3

    .line 235
    invoke-virtual {v11}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    invoke-virtual {v11, v2}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_1b

    const/4 v2, 0x1

    .line 237
    :goto_4
    if-eqz v2, :cond_9

    if-eqz v3, :cond_9

    .line 238
    const-string v4, "ActionWorker"

    const-string v5, "Ignoring countdown for eyes-free execution"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v4, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 239
    const v4, 0x1065603

    invoke-static {v4}, Lhwt;->lx(I)V

    .line 246
    :cond_9
    invoke-virtual {v15}, Lcom/google/android/search/shared/service/ClientConfig;->anq()Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual/range {p1 .. p1}, Lddb;->aaz()Z

    move-result v4

    if-nez v4, :cond_a

    invoke-virtual/range {p1 .. p1}, Lddb;->aax()Z

    move-result v4

    if-nez v4, :cond_a

    invoke-virtual/range {p1 .. p1}, Lddb;->aaD()Z

    move-result v4

    if-eqz v4, :cond_e

    :cond_a
    invoke-virtual {v15}, Lcom/google/android/search/shared/service/ClientConfig;->anh()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-virtual {v15}, Lcom/google/android/search/shared/service/ClientConfig;->amV()Z

    move-result v4

    if-nez v4, :cond_b

    if-nez v3, :cond_c

    if-eqz v2, :cond_c

    :cond_b
    invoke-virtual {v11}, Ldbd;->VH()Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_c
    invoke-virtual {v15}, Lcom/google/android/search/shared/service/ClientConfig;->amV()Z

    move-result v2

    invoke-static/range {v16 .. v16}, Ldcu;->ar(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    invoke-virtual {v13}, Ldcu;->Zf()Z

    move-result v4

    if-eqz v2, :cond_d

    if-nez v3, :cond_d

    if-nez v4, :cond_1c

    :cond_d
    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_e

    .line 254
    invoke-static {v11, v14}, Ldnm;->a(Ldbd;Ldcw;)V

    .line 257
    :cond_e
    invoke-virtual {v11}, Ldbd;->VD()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    .line 258
    if-eqz v2, :cond_f

    .line 260
    invoke-virtual {v11}, Ldbd;->VV()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Ldnm;->a(Lcom/google/android/search/shared/actions/VoiceAction;I)V

    .line 264
    :cond_f
    const-string v3, "marvin"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v14}, Ldcw;->isDone()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual/range {p1 .. p1}, Lddb;->aaw()Z

    move-result v3

    if-eqz v3, :cond_10

    if-nez v2, :cond_10

    if-eqz v16, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->bDE:Lcom/google/android/shared/search/Query;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 268
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Ldos;->a(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    .line 271
    if-eqz v2, :cond_10

    .line 272
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Ldnm;->bDE:Lcom/google/android/shared/search/Query;

    .line 275
    :cond_10
    return-void

    .line 182
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aaj()Ldbd;

    move-result-object v3

    invoke-virtual {v3}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v3

    if-eqz v3, :cond_12

    invoke-interface {v3}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_12
    invoke-virtual/range {p0 .. p0}, Ldnm;->adW()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v2

    sget-object v3, Ljrc;->ezW:Ljsm;

    invoke-virtual {v2, v3}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljrc;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljrc;->bss()I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v2, Ljrc;->ezY:[Ljrl;

    if-eqz v3, :cond_1

    iget-object v7, v2, Ljrc;->ezY:[Ljrl;

    array-length v8, v7

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v8, :cond_15

    aget-object v2, v7, v3

    iget-object v9, v2, Ljrl;->eAH:[Ljqs;

    if-eqz v9, :cond_14

    iget-object v9, v2, Ljrl;->eAH:[Ljqs;

    array-length v10, v9

    const/4 v2, 0x0

    :goto_7
    if-ge v2, v10, :cond_14

    aget-object v17, v9, v2

    if-eqz v17, :cond_13

    invoke-virtual/range {v17 .. v17}, Ljqs;->bsA()Z

    move-result v18

    if-eqz v18, :cond_13

    invoke-virtual/range {v17 .. v17}, Ljqs;->bss()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_14
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-static {v4, v2}, Lguv;->a(Lcom/google/android/shared/search/Query;Ldbd;)Legu;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Ldnm;->mActionCardEventLogger:Legs;

    invoke-virtual {v3, v5, v6, v2}, Legs;->a(ILjava/util/List;Legu;)V

    goto/16 :goto_0

    .line 184
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->bDG:Lgrx;

    if-nez v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mFactory:Lgpu;

    invoke-virtual {v2}, Lgpu;->aJz()Lgrx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Ldnm;->bDG:Lgrx;

    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->bDG:Lgrx;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v5}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v6}, Ldda;->aao()Ldcu;

    move-result-object v6

    invoke-virtual {v6}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v7}, Ldda;->aan()Ldcy;

    move-result-object v7

    invoke-virtual {v7}, Ldcy;->Zx()Z

    move-result v7

    invoke-virtual {v9}, Ldbd;->VS()Z

    move-result v8

    invoke-virtual/range {v2 .. v8}, Lgrx;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/service/ClientConfig;ZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    invoke-virtual {v9, v3, v2}, Ldbd;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3}, Ldnm;->a(Ldbd;Lcom/google/android/search/shared/actions/VoiceAction;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v4}, Ldda;->aak()Ldcw;

    move-result-object v4

    if-eqz v2, :cond_18

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alf()Z

    move-result v5

    if-eqz v5, :cond_18

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ale()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->Zv()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v6, v0, Ldnm;->mGsaConfigFlags:Lchk;

    invoke-virtual {v6}, Lchk;->GV()Z

    move-result v6

    invoke-virtual {v4, v3, v5, v2, v6}, Ldcw;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    :cond_18
    invoke-virtual {v4, v3}, Ldcw;->t(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto/16 :goto_1

    .line 185
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Ldnm;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v2}, Ligi;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcky;

    invoke-virtual {v2}, Lcky;->Nd()V

    goto/16 :goto_2

    .line 206
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Ldnm;->aMD:Leqo;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldnm;->bDC:Lepp;

    invoke-interface {v4, v5, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_3

    .line 235
    :cond_1b
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 246
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 183
    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method adW()Z
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    invoke-virtual {v1}, Ldbd;->VH()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final v(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 3

    .prologue
    .line 568
    iget-object v0, p0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 569
    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v1

    .line 570
    if-eqz v1, :cond_0

    .line 571
    const/16 v2, 0xcc

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v1

    invoke-virtual {v2, v1}, Litu;->mC(I)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 577
    :cond_0
    invoke-virtual {v0, p1}, Ldbd;->k(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 578
    return-void
.end method
