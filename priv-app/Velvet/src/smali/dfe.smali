.class public final Ldfe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public bvV:Lchg;

.field public bvW:Lctb;

.field final mAppContext:Landroid/content/Context;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field public final mCoreSearchServices:Lcfo;


# direct methods
.method public constructor <init>(Lcfo;Ljava/util/concurrent/Executor;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Ldfe;->mCoreSearchServices:Lcfo;

    .line 51
    iput-object p2, p0, Ldfe;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 52
    iput-object p3, p0, Ldfe;->mAppContext:Landroid/content/Context;

    .line 53
    return-void
.end method


# virtual methods
.method public final fP(I)V
    .locals 8

    .prologue
    .line 93
    iget-object v0, p0, Ldfe;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v6

    .line 94
    if-nez p1, :cond_0

    invoke-virtual {v6}, Lcjs;->MM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v6}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v4

    .line 96
    iget-object v0, p0, Ldfe;->mAppContext:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    .line 98
    :goto_0
    iget-object v7, p0, Ldfe;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Ldfi;

    iget-object v1, p0, Ldfe;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Ldfe;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Ldfe;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcjs;->MK()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Ldfi;-><init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 102
    :cond_0
    return-void

    .line 96
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method
