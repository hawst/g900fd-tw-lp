.class public final Lgie;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leai;


# instance fields
.field private final bCq:Ljava/util/Locale;

.field private final cKW:Ljtd;


# direct methods
.method public constructor <init>(Ljtd;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iput-object v0, p0, Lgie;->bCq:Ljava/util/Locale;

    .line 26
    iput-object p1, p0, Lgie;->cKW:Ljtd;

    .line 27
    return-void
.end method


# virtual methods
.method public final kH(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 31
    if-nez p1, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 34
    :cond_1
    iget-object v2, p0, Lgie;->bCq:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 36
    iget-object v2, p0, Lgie;->cKW:Ljtd;

    iget-object v5, v2, Ljtd;->eDm:[Ljte;

    array-length v6, v5

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_0

    aget-object v2, v5, v3

    .line 37
    invoke-virtual {v2}, Ljte;->getCanonicalName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v0, v1

    .line 38
    goto :goto_0

    .line 40
    :cond_2
    iget-object v7, v2, Ljte;->eDl:[Ljava/lang/String;

    array-length v8, v7

    move v2, v0

    :goto_2
    if-ge v2, v8, :cond_4

    aget-object v9, v7, v2

    .line 41
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    move v0, v1

    .line 42
    goto :goto_0

    .line 40
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 36
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1
.end method

.method public final kI(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    if-nez p1, :cond_1

    .line 75
    :cond_0
    return-object v0

    .line 56
    :cond_1
    iget-object v1, p0, Lgie;->bCq:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 58
    iget-object v1, p0, Lgie;->cKW:Ljtd;

    iget-object v6, v1, Ljtd;->eDm:[Ljte;

    array-length v7, v6

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    aget-object v8, v6, v4

    .line 60
    invoke-virtual {v8}, Ljte;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 70
    :goto_1
    if-eqz v1, :cond_2

    .line 71
    invoke-virtual {v8}, Ljte;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, v8, Ljte;->eDl:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 58
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 63
    :cond_3
    iget-object v9, v8, Ljte;->eDl:[Ljava/lang/String;

    array-length v10, v9

    move v1, v3

    :goto_2
    if-ge v1, v10, :cond_5

    aget-object v11, v9, v1

    .line 64
    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    move v1, v2

    .line 66
    goto :goto_1

    .line 63
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method public final kJ(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 81
    if-nez p1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-object v0

    .line 84
    :cond_1
    iget-object v1, p0, Lgie;->bCq:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 86
    iget-object v2, p0, Lgie;->cKW:Ljtd;

    iget-object v5, v2, Ljtd;->eDm:[Ljte;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v7, v5, v4

    .line 87
    invoke-virtual {v7}, Ljte;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 88
    goto :goto_0

    .line 90
    :cond_2
    iget-object v8, v7, Ljte;->eDl:[Ljava/lang/String;

    array-length v9, v8

    move v2, v3

    :goto_2
    if-ge v2, v9, :cond_4

    aget-object v10, v8, v2

    .line 91
    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 92
    invoke-virtual {v7}, Ljte;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 90
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 86
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1
.end method

.method public final kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lgie;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    if-nez v1, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v0, p1, v1}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
