.class public final Lczu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private box:Ljava/util/List;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lemp;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lczu;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lczu;->mExecutor:Ljava/util/concurrent/Executor;

    .line 64
    iput-object p3, p0, Lczu;->mClock:Lemp;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lczu;->box:Ljava/util/List;

    .line 66
    return-void
.end method


# virtual methods
.method public final Up()Ljava/util/List;
    .locals 10

    .prologue
    .line 123
    const/4 v0, 0x1

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 124
    iget-object v0, p0, Lczu;->box:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    move-object v0, v6

    .line 148
    :goto_0
    return-object v0

    .line 128
    :cond_0
    const-wide v4, 0x7fffffffffffffffL

    .line 129
    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lczu;->box:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 134
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 141
    cmp-long v8, v2, v4

    if-gez v8, :cond_2

    :goto_2
    move-object v1, v0

    move-wide v4, v2

    .line 145
    goto :goto_1

    .line 135
    :catch_0
    move-exception v2

    .line 136
    const-string v3, "Sdch.DictionaryCache"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Bad cache file name: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v3, v2, v8}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 137
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 147
    :cond_1
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v6

    .line 148
    goto :goto_0

    :cond_2
    move-object v0, v1

    move-wide v2, v4

    goto :goto_2
.end method

.method protected final declared-synchronized a(Lczt;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lczu;->box:Ljava/util/List;

    if-eqz v0, :cond_1

    move v0, v4

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 83
    invoke-virtual {p0}, Lczu;->Up()Ljava/util/List;

    move-result-object v0

    .line 84
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    new-instance v2, Ljava/io/File;

    iget-object v5, p0, Lczu;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    const-string v6, "sdch_cache"

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Sdch.DictionaryCache"

    const-string v6, "Unable to delete cache entry: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-static {v2, v6, v7}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    iget-object v2, p0, Lczu;->box:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v3

    goto :goto_0

    .line 88
    :cond_2
    :try_start_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lczu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "sdch_cache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_3
    new-instance v5, Ljava/io/File;

    iget-object v1, p0, Lczu;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    const/4 v2, 0x0

    .line 93
    :try_start_2
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 94
    :try_start_3
    invoke-virtual {p1, v1}, Lczt;->a(Ljava/io/OutputStream;)V

    .line 95
    iget-object v0, p0, Lczu;->box:Ljava/util/List;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 102
    :try_start_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    move v0, v3

    .line 105
    :goto_2
    if-eqz v0, :cond_4

    .line 106
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_4

    .line 107
    const-string v0, "Sdch.DictionaryCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t delete dictionary cache file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 110
    :cond_4
    monitor-exit p0

    return-void

    .line 96
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 97
    :goto_3
    :try_start_5
    const-string v2, "Sdch.DictionaryCache"

    const-string v4, "Couldn\'t open file for writing : "

    invoke-static {v2, v4, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 102
    :try_start_6
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v0, v3

    .line 103
    goto :goto_2

    .line 98
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 99
    :goto_4
    :try_start_7
    const-string v2, "Sdch.DictionaryCache"

    const-string v3, "Couldn\'t write to file : "

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 102
    :try_start_8
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    move v0, v4

    .line 103
    goto :goto_2

    .line 102
    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_5
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_2
    move-exception v0

    goto :goto_5

    .line 98
    :catch_2
    move-exception v0

    goto :goto_4

    .line 96
    :catch_3
    move-exception v0

    goto :goto_3
.end method

.method final declared-synchronized b(Lefk;)V
    .locals 9

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lczu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "sdch_cache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 177
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 178
    const/4 v0, 0x5

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lczu;->box:Ljava/util/List;

    .line 179
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Lefk;->ar(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    :goto_0
    monitor-exit p0

    return-void

    .line 183
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 184
    array-length v0, v4

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 185
    array-length v0, v4

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 187
    array-length v7, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_1

    aget-object v0, v4, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    const/4 v2, 0x0

    .line 190
    :try_start_2
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 191
    :try_start_3
    invoke-static {v1}, Lczt;->h(Ljava/io/InputStream;)Lczt;

    move-result-object v2

    .line 192
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 197
    :try_start_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 187
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 194
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 195
    :goto_3
    :try_start_5
    const-string v2, "Sdch.DictionaryCache"

    const-string v8, "Error parsing cache entry"

    invoke-static {v2, v8, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 197
    :try_start_6
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_7
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    .line 201
    :cond_1
    iput-object v6, p0, Lczu;->box:Ljava/util/List;

    .line 202
    invoke-interface {p1, v5}, Lefk;->ar(Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 197
    :catchall_2
    move-exception v0

    goto :goto_4

    .line 194
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public final jm(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lczu;->box:Ljava/util/List;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lczu;->box:Ljava/util/List;

    .line 117
    :cond_0
    iget-object v0, p0, Lczu;->box:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method
