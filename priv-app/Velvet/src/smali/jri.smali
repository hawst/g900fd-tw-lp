.class public final Ljri;
.super Ljsl;
.source "PG"


# static fields
.field public static final eAv:Ljsm;


# instance fields
.field private aez:I

.field private eAw:I

.field private eAx:J

.field private eAy:J

.field public eAz:Ljpy;

.field private eqc:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2851
    const/16 v0, 0xb

    const-class v1, Ljri;

    const v2, 0x1efd4a12

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljri;->eAv:Ljsm;

    .line 2857
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 2943
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2944
    iput v2, p0, Ljri;->aez:I

    iput-wide v0, p0, Ljri;->eqc:J

    iput v2, p0, Ljri;->eAw:I

    iput-wide v0, p0, Ljri;->eAx:J

    iput-wide v0, p0, Ljri;->eAy:J

    iput-object v3, p0, Ljri;->eAz:Ljpy;

    iput-object v3, p0, Ljri;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljri;->eCz:I

    .line 2945
    return-void
.end method


# virtual methods
.method public final Pu()J
    .locals 2

    .prologue
    .line 2867
    iget-wide v0, p0, Ljri;->eqc:J

    return-wide v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 2844
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljri;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljri;->eqc:J

    iget v0, p0, Ljri;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljri;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljri;->eAw:I

    iget v0, p0, Ljri;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljri;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljri;->eAx:J

    iget v0, p0, Ljri;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljri;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljri;->eAy:J

    iget v0, p0, Ljri;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljri;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljri;->eAz:Ljpy;

    if-nez v0, :cond_1

    new-instance v0, Ljpy;

    invoke-direct {v0}, Ljpy;-><init>()V

    iput-object v0, p0, Ljri;->eAz:Ljpy;

    :cond_1
    iget-object v0, p0, Ljri;->eAz:Ljpy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 2962
    iget v0, p0, Ljri;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2963
    const/4 v0, 0x1

    iget-wide v2, p0, Ljri;->eqc:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2965
    :cond_0
    iget v0, p0, Ljri;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2966
    const/4 v0, 0x2

    iget v1, p0, Ljri;->eAw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2968
    :cond_1
    iget v0, p0, Ljri;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2969
    const/4 v0, 0x3

    iget-wide v2, p0, Ljri;->eAx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2971
    :cond_2
    iget v0, p0, Ljri;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2972
    const/4 v0, 0x4

    iget-wide v2, p0, Ljri;->eAy:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2974
    :cond_3
    iget-object v0, p0, Ljri;->eAz:Ljpy;

    if-eqz v0, :cond_4

    .line 2975
    const/4 v0, 0x5

    iget-object v1, p0, Ljri;->eAz:Ljpy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2977
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2978
    return-void
.end method

.method public final bom()Z
    .locals 1

    .prologue
    .line 2875
    iget v0, p0, Ljri;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final btl()I
    .locals 1

    .prologue
    .line 2886
    iget v0, p0, Ljri;->eAw:I

    return v0
.end method

.method public final btm()Z
    .locals 1

    .prologue
    .line 2894
    iget v0, p0, Ljri;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dG(J)Ljri;
    .locals 1

    .prologue
    .line 2870
    iput-wide p1, p0, Ljri;->eqc:J

    .line 2871
    iget v0, p0, Ljri;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljri;->aez:I

    .line 2872
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 2982
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2983
    iget v1, p0, Ljri;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2984
    const/4 v1, 0x1

    iget-wide v2, p0, Ljri;->eqc:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2987
    :cond_0
    iget v1, p0, Ljri;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2988
    const/4 v1, 0x2

    iget v2, p0, Ljri;->eAw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2991
    :cond_1
    iget v1, p0, Ljri;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2992
    const/4 v1, 0x3

    iget-wide v2, p0, Ljri;->eAx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2995
    :cond_2
    iget v1, p0, Ljri;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2996
    const/4 v1, 0x4

    iget-wide v2, p0, Ljri;->eAy:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2999
    :cond_3
    iget-object v1, p0, Ljri;->eAz:Ljpy;

    if-eqz v1, :cond_4

    .line 3000
    const/4 v1, 0x5

    iget-object v2, p0, Ljri;->eAz:Ljpy;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3003
    :cond_4
    return v0
.end method

.method public final rP(I)Ljri;
    .locals 1

    .prologue
    .line 2889
    iput p1, p0, Ljri;->eAw:I

    .line 2890
    iget v0, p0, Ljri;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljri;->aez:I

    .line 2891
    return-object p0
.end method
