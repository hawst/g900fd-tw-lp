.class public final Lezn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mPredictiveCardsPreferences:Lcxs;


# direct methods
.method public constructor <init>(Lcxs;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lezn;->mPredictiveCardsPreferences:Lcxs;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Liyg;)V
    .locals 5
    .param p2    # Liyg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-static {}, Lenu;->auQ()V

    .line 29
    iget-object v0, p0, Lezn;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v0}, Lcxs;->TJ()Ljel;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Ljel;->bjc()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljel;->aCF()I

    move-result v0

    .line 34
    :goto_0
    sget-object v2, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->cyp:Ljava/lang/String;

    new-instance v3, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    invoke-direct {v3, v0}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;-><init>(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    .line 38
    invoke-static {p2}, Lgae;->a(Liyg;)Ljava/lang/Integer;

    move-result-object v2

    .line 39
    if-nez v2, :cond_0

    .line 40
    invoke-static {p2}, Lgae;->b(Liyg;)I

    move-result v2

    .line 43
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 44
    iget-object v3, p0, Lezn;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v3}, Lcxs;->TH()Lcxi;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 50
    :goto_1
    invoke-virtual {v0, v2, v1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->aT(II)V

    .line 52
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 30
    goto :goto_0

    .line 47
    :cond_2
    iget-object v3, p0, Lezn;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v3}, Lcxs;->TH()Lcxi;

    move-result-object v3

    const/4 v4, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method
