.class final Lftc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private synthetic cBH:Landroid/view/ViewGroup;

.field private synthetic cBI:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lfsz;Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 214
    iput-object p2, p0, Lftc;->cBH:Landroid/view/ViewGroup;

    iput-object p3, p0, Lftc;->cBI:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    .line 218
    iget-object v0, p0, Lftc;->cBH:Landroid/view/ViewGroup;

    iget-object v1, p0, Lftc;->cBI:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lftc;->cBI:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x0

    iget-object v4, p0, Lftc;->cBH:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 222
    iget-object v0, p0, Lftc;->cBH:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 223
    iget-object v0, p0, Lftc;->cBH:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 224
    return-void
.end method
