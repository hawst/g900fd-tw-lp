.class public final Ljw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field iY:Ljava/lang/Object;

.field iZ:Ljx;


# direct methods
.method private constructor <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    const/16 v0, 0xe

    if-lt p1, v0, :cond_0

    .line 264
    new-instance v0, Lka;

    invoke-direct {v0}, Lka;-><init>()V

    iput-object v0, p0, Ljw;->iZ:Ljx;

    .line 270
    :goto_0
    iget-object v0, p0, Ljw;->iZ:Ljx;

    invoke-interface {v0, p2, p3}, Ljx;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ljw;->iY:Ljava/lang/Object;

    .line 271
    return-void

    .line 265
    :cond_0
    const/16 v0, 0x9

    if-lt p1, v0, :cond_1

    .line 266
    new-instance v0, Ljz;

    invoke-direct {v0}, Ljz;-><init>()V

    iput-object v0, p0, Ljw;->iZ:Ljx;

    goto :goto_0

    .line 268
    :cond_1
    new-instance v0, Ljy;

    invoke-direct {v0}, Ljy;-><init>()V

    iput-object v0, p0, Ljw;->iZ:Ljx;

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 254
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, v0, p1, p2}, Ljw;-><init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 256
    return-void
.end method


# virtual methods
.method public final abortAnimation()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Ljw;->iZ:Ljx;

    iget-object v1, p0, Ljw;->iY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljx;->I(Ljava/lang/Object;)V

    .line 431
    return-void
.end method
