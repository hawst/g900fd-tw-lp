.class public final Liwp;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public aiX:Ljcn;

.field private ajj:Ljava/lang/String;

.field private dMi:J

.field public dMj:Lixx;

.field private dMk:Ljava/lang/String;

.field private dzG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56672
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 56673
    const/4 v0, 0x0

    iput v0, p0, Liwp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liwp;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liwp;->dzG:Ljava/lang/String;

    iput-object v2, p0, Liwp;->aiX:Ljcn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liwp;->dMi:J

    iput-object v2, p0, Liwp;->dMj:Lixx;

    const-string v0, ""

    iput-object v0, p0, Liwp;->ajj:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liwp;->dMk:Ljava/lang/String;

    iput-object v2, p0, Liwp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwp;->eCz:I

    .line 56674
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 56540
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwp;->afh:Ljava/lang/String;

    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwp;->dzG:Ljava/lang/String;

    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwp;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liwp;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Liwp;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Liwp;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Liwp;->dMi:J

    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liwp;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Liwp;->dMj:Lixx;

    if-nez v0, :cond_2

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Liwp;->dMj:Lixx;

    :cond_2
    iget-object v0, p0, Liwp;->dMj:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwp;->dMk:Ljava/lang/String;

    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Liwp;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwp;->ajj:Ljava/lang/String;

    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Liwp;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 56693
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 56694
    const/4 v0, 0x1

    iget-object v1, p0, Liwp;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56696
    :cond_0
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 56697
    const/4 v0, 0x2

    iget-object v1, p0, Liwp;->dzG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56699
    :cond_1
    iget-object v0, p0, Liwp;->aiX:Ljcn;

    if-eqz v0, :cond_2

    .line 56700
    const/4 v0, 0x3

    iget-object v1, p0, Liwp;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 56702
    :cond_2
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 56703
    const/4 v0, 0x4

    iget-wide v2, p0, Liwp;->dMi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 56705
    :cond_3
    iget-object v0, p0, Liwp;->dMj:Lixx;

    if-eqz v0, :cond_4

    .line 56706
    const/4 v0, 0x5

    iget-object v1, p0, Liwp;->dMj:Lixx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 56708
    :cond_4
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 56709
    const/4 v0, 0x6

    iget-object v1, p0, Liwp;->dMk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56711
    :cond_5
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 56712
    const/4 v0, 0x7

    iget-object v1, p0, Liwp;->ajj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 56714
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 56715
    return-void
.end method

.method public final aVj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56584
    iget-object v0, p0, Liwp;->dzG:Ljava/lang/String;

    return-object v0
.end method

.method public final aVk()Z
    .locals 1

    .prologue
    .line 56595
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aZv()Z
    .locals 1

    .prologue
    .line 56642
    iget v0, p0, Liwp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cb(J)Liwp;
    .locals 2

    .prologue
    .line 56612
    const-wide/32 v0, 0x53d7e120

    iput-wide v0, p0, Liwp;->dMi:J

    .line 56613
    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liwp;->aez:I

    .line 56614
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56562
    iget-object v0, p0, Liwp;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 56719
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 56720
    iget v1, p0, Liwp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 56721
    const/4 v1, 0x1

    iget-object v2, p0, Liwp;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56724
    :cond_0
    iget v1, p0, Liwp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 56725
    const/4 v1, 0x2

    iget-object v2, p0, Liwp;->dzG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56728
    :cond_1
    iget-object v1, p0, Liwp;->aiX:Ljcn;

    if-eqz v1, :cond_2

    .line 56729
    const/4 v1, 0x3

    iget-object v2, p0, Liwp;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56732
    :cond_2
    iget v1, p0, Liwp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 56733
    const/4 v1, 0x4

    iget-wide v2, p0, Liwp;->dMi:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 56736
    :cond_3
    iget-object v1, p0, Liwp;->dMj:Lixx;

    if-eqz v1, :cond_4

    .line 56737
    const/4 v1, 0x5

    iget-object v2, p0, Liwp;->dMj:Lixx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56740
    :cond_4
    iget v1, p0, Liwp;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 56741
    const/4 v1, 0x6

    iget-object v2, p0, Liwp;->dMk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56744
    :cond_5
    iget v1, p0, Liwp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 56745
    const/4 v1, 0x7

    iget-object v2, p0, Liwp;->ajj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56748
    :cond_6
    return v0
.end method

.method public final qa(Ljava/lang/String;)Liwp;
    .locals 1

    .prologue
    .line 56565
    if-nez p1, :cond_0

    .line 56566
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56568
    :cond_0
    iput-object p1, p0, Liwp;->afh:Ljava/lang/String;

    .line 56569
    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwp;->aez:I

    .line 56570
    return-object p0
.end method

.method public final qb(Ljava/lang/String;)Liwp;
    .locals 1

    .prologue
    .line 56587
    if-nez p1, :cond_0

    .line 56588
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56590
    :cond_0
    iput-object p1, p0, Liwp;->dzG:Ljava/lang/String;

    .line 56591
    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwp;->aez:I

    .line 56592
    return-object p0
.end method

.method public final qc(Ljava/lang/String;)Liwp;
    .locals 1

    .prologue
    .line 56634
    if-nez p1, :cond_0

    .line 56635
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56637
    :cond_0
    iput-object p1, p0, Liwp;->ajj:Ljava/lang/String;

    .line 56638
    iget v0, p0, Liwp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Liwp;->aez:I

    .line 56639
    return-object p0
.end method

.method public final qy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56631
    iget-object v0, p0, Liwp;->ajj:Ljava/lang/String;

    return-object v0
.end method
