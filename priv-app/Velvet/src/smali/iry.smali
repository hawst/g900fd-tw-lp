.class abstract Liry;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lisg;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lisg;
    .locals 2

    .prologue
    .line 48
    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Liry;->aa([B)Lisg;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method public Q(Ljava/lang/CharSequence;)Lisg;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lifa;->UTF_16LE:Ljava/nio/charset/Charset;

    invoke-direct {p0, p1, v0}, Liry;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lisg;

    move-result-object v0

    return-object v0
.end method
