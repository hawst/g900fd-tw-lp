.class final Lfnb;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic cwK:Lfmv;


# direct methods
.method constructor <init>(Lfmv;)V
    .locals 0

    .prologue
    .line 945
    iput-object p1, p0, Lfnb;->cwK:Lfmv;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 945
    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBb()Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfnb;->cwK:Lfmv;

    invoke-virtual {v1, v0}, Lfmv;->a(Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;)Lfna;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 945
    check-cast p1, Lfna;

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    invoke-virtual {v0}, Lfmv;->aBk()V

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-boolean v2, v0, Lfmv;->cwF:Z

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iput-boolean v4, v0, Lfmv;->cwF:Z

    if-nez p1, :cond_1

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->cwG:Lizj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    iget-object v2, p0, Lfnb;->cwK:Lfmv;

    iget-object v2, v2, Lfmv;->cwG:Lizj;

    invoke-interface {v0, v2}, Lfnc;->w(Lizj;)V

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iput-object v1, v0, Lfmv;->cwG:Lizj;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->getResponseCode()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAY()V

    :cond_2
    :goto_1
    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAV()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v1, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBZ()Ljcn;

    move-result-object v1

    iget-object v2, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aCa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfmv;->a(Ljcn;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAT()V

    iget-object v6, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBW()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBX()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBY()Landroid/content/Intent;

    move-result-object v5

    new-instance v0, Lfvr;

    iget-object v1, v6, Lfmv;->mModulePresenterFactory:Lftz;

    iget-object v2, v6, Lfmv;->mClock:Lemp;

    invoke-direct/range {v0 .. v5}, Lfvr;-><init>(Lftz;Lemp;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    iget-object v1, v6, Lfmv;->mPresenter:Lfnc;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, Lfnc;->a(Lfkd;I)V

    goto :goto_1

    :cond_4
    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v3, p1, Lfna;->cof:Ljava/util/List;

    invoke-virtual {v0, v3}, Lfmv;->aa(Ljava/util/List;)V

    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->cwG:Lizj;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    iget-object v3, p0, Lfnb;->cwK:Lfmv;

    iget-object v3, v3, Lfmv;->cwG:Lizj;

    invoke-direct {v0, v3, v4}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;-><init>(Lizj;I)V

    iget-object v3, p0, Lfnb;->cwK:Lfmv;

    iput-object v1, v3, Lfmv;->cwG:Lizj;

    :goto_2
    iget-boolean v1, p1, Lfna;->cwN:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfnb;->cwK:Lfmv;

    iget-object v1, v1, Lfmv;->mPresenter:Lfnc;

    iget-object v3, p1, Lfna;->cof:Ljava/util/List;

    iget-object v4, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v4

    invoke-interface {v1, v3, v4, v0, v2}, Lfnc;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;Z)V

    :goto_3
    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBp()V

    goto :goto_1

    :cond_5
    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    invoke-direct {v0, v5}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;-><init>(Z)V

    :cond_6
    iget-object v1, p0, Lfnb;->cwK:Lfmv;

    iget-object v1, v1, Lfmv;->mPresenter:Lfnc;

    iget-object v2, p1, Lfna;->cof:Ljava/util/List;

    iget-object v3, p1, Lfna;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lfnc;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;)V

    goto :goto_3

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lfnb;->cwK:Lfmv;

    iget-object v0, v0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0}, Lfns;->aBq()V

    .line 950
    return-void
.end method
