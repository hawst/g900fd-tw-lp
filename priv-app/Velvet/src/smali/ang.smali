.class public final Lang;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public ags:[Lanh;

.field private agt:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50
    iput v1, p0, Lang;->aez:I

    invoke-static {}, Lanh;->oO()[Lanh;

    move-result-object v0

    iput-object v0, p0, Lang;->ags:[Lanh;

    iput-boolean v1, p0, Lang;->agt:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lang;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lang;->eCz:I

    .line 51
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lang;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lang;->ags:[Lanh;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lanh;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lang;->ags:[Lanh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lang;->ags:[Lanh;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lang;->ags:[Lanh;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lang;->agt:Z

    iget v0, p0, Lang;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lang;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lang;->ags:[Lanh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lang;->ags:[Lanh;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 66
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lang;->ags:[Lanh;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 67
    iget-object v1, p0, Lang;->ags:[Lanh;

    aget-object v1, v1, v0

    .line 68
    if-eqz v1, :cond_0

    .line 69
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 66
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    iget v0, p0, Lang;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 74
    const/4 v0, 0x2

    iget-boolean v1, p0, Lang;->agt:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 76
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 77
    return-void
.end method

.method public final aS(Z)Lang;
    .locals 1

    .prologue
    .line 36
    iput-boolean p1, p0, Lang;->agt:Z

    .line 37
    iget v0, p0, Lang;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lang;->aez:I

    .line 38
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 81
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 82
    iget-object v0, p0, Lang;->ags:[Lanh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lang;->ags:[Lanh;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 83
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lang;->ags:[Lanh;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 84
    iget-object v2, p0, Lang;->ags:[Lanh;

    aget-object v2, v2, v0

    .line 85
    if-eqz v2, :cond_0

    .line 86
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 83
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_1
    iget v0, p0, Lang;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 92
    const/4 v0, 0x2

    iget-boolean v2, p0, Lang;->agt:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 95
    :cond_2
    return v1
.end method

.method public final oN()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lang;->agt:Z

    return v0
.end method
