.class final Lfmk;
.super Lepp;
.source "PG"


# instance fields
.field private final coZ:Lfjs;

.field private final cvN:Landroid/content/Intent;

.field private final cvT:Z

.field private final mNowRemoteClient:Lfml;


# direct methods
.method constructor <init>(Lfml;Lfjs;ZLandroid/content/Intent;)V
    .locals 1
    .param p4    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 738
    const-string v0, "ShowActionLogger"

    invoke-direct {p0, v0}, Lepp;-><init>(Ljava/lang/String;)V

    .line 739
    iput-object p1, p0, Lfmk;->mNowRemoteClient:Lfml;

    .line 740
    iput-object p2, p0, Lfmk;->coZ:Lfjs;

    .line 741
    iput-boolean p3, p0, Lfmk;->cvT:Z

    .line 742
    iput-object p4, p0, Lfmk;->cvN:Landroid/content/Intent;

    .line 743
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 747
    const-string v1, "LAUNCHER_NOW_SCREEN_SHOWN"

    iget-boolean v0, p0, Lfmk;->cvT:Z

    if-nez v0, :cond_1

    const-string v0, "Swipe"

    :cond_0
    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->aJ(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    .line 749
    iget-object v1, p0, Lfmk;->mNowRemoteClient:Lfml;

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 750
    iget-object v0, p0, Lfmk;->mNowRemoteClient:Lfml;

    invoke-virtual {v0, p0}, Lfml;->e(Lesk;)V

    .line 754
    iget-boolean v0, p0, Lfmk;->cvT:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfmk;->cvN:Landroid/content/Intent;

    if-eqz v0, :cond_5

    .line 755
    iget-object v1, p0, Lfmk;->cvN:Landroid/content/Intent;

    const-string v0, "notification_entries"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "notification_entries"

    invoke-static {v1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    const/16 v3, 0x17

    invoke-static {v0, v3, v4, v4}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->b(Lizj;ILixx;Ljava/lang/Integer;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    iget-object v3, p0, Lfmk;->mNowRemoteClient:Lfml;

    invoke-virtual {v3, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    goto :goto_1

    .line 747
    :cond_1
    iget-object v0, p0, Lfmk;->cvN:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfmk;->cvN:Landroid/content/Intent;

    const-string v2, "source"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const-string v0, "ASSIST_GESTURE"

    goto :goto_0

    .line 755
    :cond_3
    const-string v0, "target_entry"

    invoke-static {v1, v0}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lfmk;->coZ:Lfjs;

    invoke-interface {v2, v0}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    if-nez v0, :cond_4

    const-string v2, "target_group_entry_tree"

    invoke-static {v1, v2}, Lgbm;->c(Landroid/content/Intent;Ljava/lang/String;)Lizq;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lfmk;->coZ:Lfjs;

    invoke-interface {v0, v1}, Lfjs;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    :cond_4
    if-eqz v0, :cond_5

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    const/16 v1, 0x1b

    invoke-static {v0, v1, v4, v4}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->b(Lizj;ILixx;Ljava/lang/Integer;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    iget-object v1, p0, Lfmk;->mNowRemoteClient:Lfml;

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 757
    :cond_5
    return-void
.end method
