.class public final Ldtr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bKU:Ldsv;

.field final bKV:Ldtb;

.field private bKW:Landroid/animation/TimeAnimator;

.field public bKX:Z


# direct methods
.method public constructor <init>(Ldsv;Ldtb;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ldtr;->bKU:Ldsv;

    .line 24
    iput-object p2, p0, Ldtr;->bKV:Ldtb;

    .line 30
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Ldtr;->bKW:Landroid/animation/TimeAnimator;

    .line 31
    iget-object v0, p0, Ldtr;->bKW:Landroid/animation/TimeAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    .line 32
    iget-object v0, p0, Ldtr;->bKW:Landroid/animation/TimeAnimator;

    new-instance v1, Ldts;

    invoke-direct {v1, p0}, Ldts;-><init>(Ldtr;)V

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final eg(Z)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldtr;->bKX:Z

    .line 48
    if-eqz p1, :cond_0

    .line 49
    iget-object v0, p0, Ldtr;->bKW:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    .line 53
    :goto_0
    iget-object v0, p0, Ldtr;->bKU:Ldsv;

    invoke-virtual {v0, p1}, Ldsv;->setEnabled(Z)V

    .line 54
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Ldtr;->bKW:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->end()V

    goto :goto_0
.end method
