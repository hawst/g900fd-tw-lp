.class public abstract Lczs;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private mClosed:Z

.field private mDelegate:Ljava/io/InputStream;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method

.method private declared-synchronized cV()V
    .locals 2

    .prologue
    .line 30
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lczs;->mClosed:Z

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream already closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 33
    :cond_0
    :try_start_1
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    if-nez v0, :cond_1

    .line 34
    invoke-virtual {p0}, Lczs;->Uo()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method protected abstract Uo()Ljava/io/InputStream;
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lczs;->mClosed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 48
    :goto_0
    monitor-exit p0

    return-void

    .line 43
    :cond_0
    :try_start_1
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    .line 47
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczs;->mClosed:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public read()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 53
    invoke-direct {p0}, Lczs;->cV()V

    .line 55
    :cond_0
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 61
    invoke-direct {p0}, Lczs;->cV()V

    .line 63
    :cond_0
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 69
    invoke-direct {p0}, Lczs;->cV()V

    .line 71
    :cond_0
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public skip(J)J
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 77
    invoke-direct {p0}, Lczs;->cV()V

    .line 79
    :cond_0
    iget-object v0, p0, Lczs;->mDelegate:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    return-wide v0
.end method
