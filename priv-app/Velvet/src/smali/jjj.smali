.class public final Ljjj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eoN:[Ljjj;


# instance fields
.field private aez:I

.field private aib:Ljava/lang/String;

.field private eoO:I

.field private eoP:I

.field private eoQ:[I

.field public eoR:[Ljjk;

.field private eoS:[B

.field private eoT:I

.field private eoU:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 339
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 340
    iput v1, p0, Ljjj;->aez:I

    iput v1, p0, Ljjj;->eoO:I

    iput v1, p0, Ljjj;->eoP:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljjj;->eoQ:[I

    invoke-static {}, Ljjk;->bnS()[Ljjk;

    move-result-object v0

    iput-object v0, p0, Ljjj;->eoR:[Ljjk;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljjj;->eoS:[B

    iput v1, p0, Ljjj;->eoT:I

    const-string v0, ""

    iput-object v0, p0, Ljjj;->aib:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljjj;->eoU:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljjj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjj;->eCz:I

    .line 341
    return-void
.end method

.method public static bnR()[Ljjj;
    .locals 2

    .prologue
    .line 200
    sget-object v0, Ljjj;->eoN:[Ljjj;

    if-nez v0, :cond_1

    .line 201
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 203
    :try_start_0
    sget-object v0, Ljjj;->eoN:[Ljjj;

    if-nez v0, :cond_0

    .line 204
    const/4 v0, 0x0

    new-array v0, v0, [Ljjj;

    sput-object v0, Ljjj;->eoN:[Ljjj;

    .line 206
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :cond_1
    sget-object v0, Ljjj;->eoN:[Ljjj;

    return-object v0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljjj;->eoO:I

    iget v0, p0, Ljjj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljjj;->eoP:I

    iget v0, p0, Ljjj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjj;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljjj;->eoQ:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljjj;->eoQ:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljjj;->eoQ:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljjj;->eoQ:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljjj;->eoQ:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Ljjj;->eoQ:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljjj;->eoQ:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljjj;->eoQ:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljjj;->eoQ:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljjj;->eoR:[Ljjk;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljjk;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljjj;->eoR:[Ljjk;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljjk;

    invoke-direct {v3}, Ljjk;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljjj;->eoR:[Ljjk;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Ljjk;

    invoke-direct {v3}, Ljjk;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v1, p0, Ljjj;->eoR:[Ljjk;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljjj;->eoS:[B

    iget v0, p0, Ljjj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjj;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljjj;->eoT:I

    iget v0, p0, Ljjj;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjj;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjj;->aib:Ljava/lang/String;

    iget v0, p0, Ljjj;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljjj;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljjj;->eoU:I

    iget v0, p0, Ljjj;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljjj;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x30 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 361
    iget v0, p0, Ljjj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 362
    const/4 v0, 0x1

    iget v2, p0, Ljjj;->eoO:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 364
    :cond_0
    iget v0, p0, Ljjj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 365
    const/4 v0, 0x2

    iget v2, p0, Ljjj;->eoP:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 367
    :cond_1
    iget-object v0, p0, Ljjj;->eoQ:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljjj;->eoQ:[I

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 368
    :goto_0
    iget-object v2, p0, Ljjj;->eoQ:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 369
    const/4 v2, 0x3

    iget-object v3, p0, Ljjj;->eoQ:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372
    :cond_2
    iget-object v0, p0, Ljjj;->eoR:[Ljjk;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljjj;->eoR:[Ljjk;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 373
    :goto_1
    iget-object v0, p0, Ljjj;->eoR:[Ljjk;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 374
    iget-object v0, p0, Ljjj;->eoR:[Ljjk;

    aget-object v0, v0, v1

    .line 375
    if-eqz v0, :cond_3

    .line 376
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 373
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 380
    :cond_4
    iget v0, p0, Ljjj;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 381
    const/4 v0, 0x5

    iget-object v1, p0, Ljjj;->eoS:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 383
    :cond_5
    iget v0, p0, Ljjj;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 384
    const/4 v0, 0x6

    iget v1, p0, Ljjj;->eoT:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 386
    :cond_6
    iget v0, p0, Ljjj;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    .line 387
    const/4 v0, 0x7

    iget-object v1, p0, Ljjj;->aib:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 389
    :cond_7
    iget v0, p0, Ljjj;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 390
    const/16 v0, 0x8

    iget v1, p0, Ljjj;->eoU:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 392
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 393
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 397
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 398
    iget v1, p0, Ljjj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 399
    const/4 v1, 0x1

    iget v3, p0, Ljjj;->eoO:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 402
    :cond_0
    iget v1, p0, Ljjj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 403
    const/4 v1, 0x2

    iget v3, p0, Ljjj;->eoP:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 406
    :cond_1
    iget-object v1, p0, Ljjj;->eoQ:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljjj;->eoQ:[I

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    .line 408
    :goto_0
    iget-object v4, p0, Ljjj;->eoQ:[I

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 409
    iget-object v4, p0, Ljjj;->eoQ:[I

    aget v4, v4, v1

    .line 410
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 408
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 413
    :cond_2
    add-int/2addr v0, v3

    .line 414
    iget-object v1, p0, Ljjj;->eoQ:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 416
    :cond_3
    iget-object v1, p0, Ljjj;->eoR:[Ljjk;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljjj;->eoR:[Ljjk;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 417
    :goto_1
    iget-object v1, p0, Ljjj;->eoR:[Ljjk;

    array-length v1, v1

    if-ge v2, v1, :cond_5

    .line 418
    iget-object v1, p0, Ljjj;->eoR:[Ljjk;

    aget-object v1, v1, v2

    .line 419
    if-eqz v1, :cond_4

    .line 420
    const/4 v3, 0x4

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 417
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 425
    :cond_5
    iget v1, p0, Ljjj;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_6

    .line 426
    const/4 v1, 0x5

    iget-object v2, p0, Ljjj;->eoS:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 429
    :cond_6
    iget v1, p0, Ljjj;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_7

    .line 430
    const/4 v1, 0x6

    iget v2, p0, Ljjj;->eoT:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 433
    :cond_7
    iget v1, p0, Ljjj;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_8

    .line 434
    const/4 v1, 0x7

    iget-object v2, p0, Ljjj;->aib:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 437
    :cond_8
    iget v1, p0, Ljjj;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_9

    .line 438
    const/16 v1, 0x8

    iget v2, p0, Ljjj;->eoU:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 441
    :cond_9
    return v0
.end method

.method public final ps()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Ljjj;->aib:Ljava/lang/String;

    return-object v0
.end method
