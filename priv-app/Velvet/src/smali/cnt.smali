.class public final Lcnt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final axG:Landroid/content/UriMatcher;

.field private bdf:Lcqe;

.field private final mContext:Landroid/content/Context;

.field private final mVelvetServices:Lgql;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgql;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcnt;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcnt;->mVelvetServices:Lgql;

    .line 41
    iget-object v0, p0, Lcnt;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".google"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    const-string v2, "search_suggest_query"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "search_suggest_query/*"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "search_suggest_shortcut"

    invoke-virtual {v1, v0, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v2, "search_suggest_shortcut/*"

    invoke-virtual {v1, v0, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcnt;->axG:Landroid/content/UriMatcher;

    .line 42
    return-void
.end method

.method private declared-synchronized QK()Lcqe;
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcnt;->bdf:Lcqe;

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcnt;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJU()Lgpu;

    move-result-object v0

    invoke-virtual {v0}, Lgpu;->aJK()Lcqe;

    move-result-object v0

    iput-object v0, p0, Lcnt;->bdf:Lcqe;

    .line 48
    :cond_0
    iget-object v0, p0, Lcnt;->bdf:Lcqe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final k(Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 70
    iget-object v0, p0, Lcnt;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 72
    if-nez v0, :cond_1

    .line 73
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 74
    :goto_0
    new-instance v2, Ldfc;

    invoke-direct {p0}, Lcnt;->QK()Lcqe;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lcqe;->hs(Ljava/lang/String;)Ldef;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Ldei;

    invoke-direct {p0}, Lcnt;->QK()Lcqe;

    move-result-object v3

    invoke-interface {v3}, Lcqe;->QJ()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    move-object v0, v1

    :goto_1
    invoke-direct {v2, v0}, Ldfc;-><init>(Ldef;)V

    move-object v0, v2

    .line 78
    :goto_2
    return-object v0

    .line 73
    :cond_0
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    goto :goto_0

    .line 77
    :cond_1
    if-ne v0, v1, :cond_2

    .line 78
    const/4 v0, 0x0

    goto :goto_2

    .line 80
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
