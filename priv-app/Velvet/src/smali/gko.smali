.class public final Lgko;
.super Ljuq;
.source "PG"


# instance fields
.field private final mRelationshipNameLookup:Leai;


# direct methods
.method public constructor <init>(Leai;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljuq;-><init>()V

    .line 13
    iput-object p1, p0, Lgko;->mRelationshipNameLookup:Leai;

    .line 14
    return-void
.end method


# virtual methods
.method public final mY(Ljava/lang/String;)F
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lgko;->mRelationshipNameLookup:Leai;

    invoke-interface {v0, p1}, Leai;->kH(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lgkp;->na(Ljava/lang/String;)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mZ(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Lgko;->mRelationshipNameLookup:Leai;

    invoke-interface {v0, p1}, Leai;->kH(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    const-string v0, "PumpkinRelationshipValidator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid relationship name."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 28
    invoke-super {p0, p1}, Ljuq;->mZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    iget-object v0, p0, Lgko;->mRelationshipNameLookup:Leai;

    invoke-interface {v0, p1}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
