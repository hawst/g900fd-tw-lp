.class final Lkef;
.super Ljava/io/InputStream;
.source "PG"


# static fields
.field private static synthetic $assertionsDisabled:Z


# instance fields
.field private bBF:I

.field private final buffer:[B

.field private eQb:Z

.field private eUw:Z

.field private eUx:I

.field private synthetic eUy:Lkee;

.field private pos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 326
    const-class v0, Lkee;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lkef;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lkee;)V
    .locals 1

    .prologue
    .line 326
    iput-object p1, p0, Lkef;->eUy:Lkee;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 339
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, Lkef;->buffer:[B

    .line 342
    const/4 v0, -0x1

    iput v0, p0, Lkef;->pos:I

    .line 361
    const/4 v0, 0x0

    iput v0, p0, Lkef;->eUx:I

    return-void
.end method

.method synthetic constructor <init>(Lkee;B)V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0, p1}, Lkef;-><init>(Lkee;)V

    return-void
.end method

.method static synthetic a(Lkef;)Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lkef;->eUw:Z

    return v0
.end method

.method static synthetic a(Lkef;Z)Z
    .locals 0

    .prologue
    .line 326
    iput-boolean p1, p0, Lkef;->eUw:Z

    return p1
.end method

.method static synthetic b(Lkef;)Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lkef;->eQb:Z

    return v0
.end method

.method private bxV()V
    .locals 3

    .prologue
    .line 524
    iget-boolean v0, p0, Lkef;->eQb:Z

    if-eqz v0, :cond_0

    .line 525
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 527
    :cond_0
    iget-object v0, p0, Lkef;->eUy:Lkee;

    invoke-static {v0}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 528
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stream was reset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lkef;->eUy:Lkee;

    invoke-static {v2}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 530
    :cond_1
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 4

    .prologue
    .line 364
    iget-object v1, p0, Lkef;->eUy:Lkee;

    monitor-enter v1

    .line 365
    :try_start_0
    invoke-direct {p0}, Lkef;->bxV()V

    .line 366
    iget v0, p0, Lkef;->pos:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 367
    const/4 v0, 0x0

    monitor-exit v1

    .line 371
    :goto_0
    return v0

    .line 368
    :cond_0
    iget v0, p0, Lkef;->bBF:I

    iget v2, p0, Lkef;->pos:I

    if-le v0, v2, :cond_1

    .line 369
    iget v0, p0, Lkef;->bBF:I

    iget v2, p0, Lkef;->pos:I

    sub-int/2addr v0, v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 371
    :cond_1
    :try_start_1
    iget v0, p0, Lkef;->bBF:I

    iget-object v2, p0, Lkef;->buffer:[B

    array-length v2, v2

    iget v3, p0, Lkef;->pos:I

    sub-int/2addr v2, v3

    add-int/2addr v0, v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 516
    iget-object v1, p0, Lkef;->eUy:Lkee;

    monitor-enter v1

    .line 517
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lkef;->eQb:Z

    .line 518
    iget-object v0, p0, Lkef;->eUy:Lkee;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 519
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    iget-object v0, p0, Lkef;->eUy:Lkee;

    invoke-static {v0}, Lkee;->f(Lkee;)V

    .line 521
    return-void

    .line 519
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final d(Ljava/io/InputStream;I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 456
    sget-boolean v1, Lkef;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lkef;->eUy:Lkee;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 458
    :cond_0
    if-nez p2, :cond_1

    .line 512
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-object v3, p0, Lkef;->eUy:Lkee;

    monitor-enter v3

    .line 468
    :try_start_0
    iget-boolean v4, p0, Lkef;->eUw:Z

    .line 469
    iget v5, p0, Lkef;->pos:I

    .line 470
    iget v6, p0, Lkef;->bBF:I

    .line 471
    iget v1, p0, Lkef;->bBF:I

    .line 472
    iget-object v2, p0, Lkef;->buffer:[B

    array-length v2, v2

    invoke-virtual {p0}, Lkef;->available()I

    move-result v7

    sub-int/2addr v2, v7

    if-le p2, v2, :cond_2

    const/4 v2, 0x1

    .line 473
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    if-eqz v2, :cond_3

    .line 477
    int-to-long v0, p2

    invoke-static {p1, v0, v1}, Lkbt;->b(Ljava/io/InputStream;J)J

    .line 478
    iget-object v0, p0, Lkef;->eUy:Lkee;

    sget-object v1, Lkdb;->eTb:Lkdb;

    invoke-virtual {v0, v1}, Lkee;->b(Lkdb;)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 472
    goto :goto_1

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 483
    :cond_3
    if-eqz v4, :cond_4

    .line 484
    int-to-long v0, p2

    invoke-static {p1, v0, v1}, Lkbt;->b(Ljava/io/InputStream;J)J

    goto :goto_0

    .line 491
    :cond_4
    if-ge v5, v1, :cond_7

    .line 492
    iget-object v2, p0, Lkef;->buffer:[B

    array-length v2, v2

    sub-int/2addr v2, v1

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 493
    iget-object v3, p0, Lkef;->buffer:[B

    invoke-static {p1, v3, v1, v2}, Lkbt;->b(Ljava/io/InputStream;[BII)V

    .line 494
    add-int/2addr v1, v2

    .line 495
    sub-int/2addr p2, v2

    .line 496
    iget-object v2, p0, Lkef;->buffer:[B

    array-length v2, v2

    if-ne v1, v2, :cond_7

    .line 500
    :goto_2
    if-lez p2, :cond_5

    .line 501
    iget-object v1, p0, Lkef;->buffer:[B

    invoke-static {p1, v1, v0, p2}, Lkbt;->b(Ljava/io/InputStream;[BII)V

    .line 502
    add-int/2addr v0, p2

    .line 505
    :cond_5
    iget-object v1, p0, Lkef;->eUy:Lkee;

    monitor-enter v1

    .line 507
    :try_start_1
    iput v0, p0, Lkef;->bBF:I

    .line 508
    iget v0, p0, Lkef;->pos:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_6

    .line 509
    iput v6, p0, Lkef;->pos:I

    .line 510
    iget-object v0, p0, Lkef;->eUy:Lkee;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 512
    :cond_6
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 377
    invoke-static {p0}, Lkbt;->n(Ljava/io/InputStream;)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 10

    .prologue
    .line 381
    iget-object v4, p0, Lkef;->eUy:Lkee;

    monitor-enter v4

    .line 382
    :try_start_0
    array-length v0, p1

    invoke-static {v0, p2, p3}, Lkbt;->D(III)V

    .line 383
    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    iget-object v5, p0, Lkef;->eUy:Lkee;

    invoke-static {v5}, Lkee;->d(Lkee;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long v2, v0, v2

    iget-object v0, p0, Lkef;->eUy:Lkee;

    invoke-static {v0}, Lkee;->d(Lkee;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :cond_0
    :goto_0
    :try_start_1
    iget v5, p0, Lkef;->pos:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_3

    iget-boolean v5, p0, Lkef;->eUw:Z

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lkef;->eQb:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lkef;->eUy:Lkee;

    invoke-static {v5}, Lkee;->e(Lkee;)Lkdb;

    move-result-object v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lkef;->eUy:Lkee;

    invoke-static {v5}, Lkee;->d(Lkee;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    iget-object v5, p0, Lkef;->eUy:Lkee;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 383
    :cond_1
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-lez v5, :cond_2

    :try_start_3
    iget-object v5, p0, Lkef;->eUy:Lkee;

    invoke-virtual {v5, v0, v1}, Ljava/lang/Object;->wait(J)V

    iget-object v0, p0, Lkef;->eUy:Lkee;

    invoke-static {v0}, Lkee;->d(Lkee;)J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    sub-long/2addr v0, v6

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/net/SocketTimeoutException;

    invoke-direct {v0}, Ljava/net/SocketTimeoutException;-><init>()V

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 384
    :cond_3
    :try_start_4
    invoke-direct {p0}, Lkef;->bxV()V

    .line 386
    iget v0, p0, Lkef;->pos:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 387
    const/4 v0, -0x1

    monitor-exit v4

    .line 423
    :goto_1
    return v0

    .line 390
    :cond_4
    const/4 v0, 0x0

    .line 393
    iget v1, p0, Lkef;->bBF:I

    iget v2, p0, Lkef;->pos:I

    if-gt v1, v2, :cond_5

    .line 394
    iget-object v0, p0, Lkef;->buffer:[B

    array-length v0, v0

    iget v1, p0, Lkef;->pos:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 395
    iget-object v1, p0, Lkef;->buffer:[B

    iget v2, p0, Lkef;->pos:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 396
    iget v1, p0, Lkef;->pos:I

    add-int/2addr v1, v0

    iput v1, p0, Lkef;->pos:I

    .line 397
    add-int/lit8 v0, v0, 0x0

    .line 398
    iget v1, p0, Lkef;->pos:I

    iget-object v2, p0, Lkef;->buffer:[B

    array-length v2, v2

    if-ne v1, v2, :cond_5

    .line 399
    const/4 v1, 0x0

    iput v1, p0, Lkef;->pos:I

    .line 404
    :cond_5
    if-ge v0, p3, :cond_6

    .line 405
    iget v1, p0, Lkef;->bBF:I

    iget v2, p0, Lkef;->pos:I

    sub-int/2addr v1, v2

    sub-int v2, p3, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 406
    iget-object v2, p0, Lkef;->buffer:[B

    iget v3, p0, Lkef;->pos:I

    add-int v5, p2, v0

    invoke-static {v2, v3, p1, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 407
    iget v2, p0, Lkef;->pos:I

    add-int/2addr v2, v1

    iput v2, p0, Lkef;->pos:I

    .line 408
    add-int/2addr v0, v1

    .line 412
    :cond_6
    iget v1, p0, Lkef;->eUx:I

    add-int/2addr v1, v0

    iput v1, p0, Lkef;->eUx:I

    .line 413
    iget v1, p0, Lkef;->eUx:I

    const v2, 0x8000

    if-lt v1, v2, :cond_7

    .line 414
    iget-object v1, p0, Lkef;->eUy:Lkee;

    invoke-static {v1}, Lkee;->c(Lkee;)Lkdx;

    move-result-object v1

    iget-object v2, p0, Lkef;->eUy:Lkee;

    invoke-static {v2}, Lkee;->b(Lkee;)I

    move-result v2

    iget v3, p0, Lkef;->eUx:I

    invoke-virtual {v1, v2, v3}, Lkdx;->bD(II)V

    .line 415
    const/4 v1, 0x0

    iput v1, p0, Lkef;->eUx:I

    .line 418
    :cond_7
    iget v1, p0, Lkef;->pos:I

    iget v2, p0, Lkef;->bBF:I

    if-ne v1, v2, :cond_8

    .line 419
    const/4 v1, -0x1

    iput v1, p0, Lkef;->pos:I

    .line 420
    const/4 v1, 0x0

    iput v1, p0, Lkef;->bBF:I

    .line 423
    :cond_8
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
