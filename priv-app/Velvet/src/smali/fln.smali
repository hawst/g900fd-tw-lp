.class public final Lfln;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lekg;
.implements Lfmu;


# instance fields
.field private final cmH:[Lizj;

.field private cuR:J

.field private cuS:Z

.field private cuT:I

.field private cuU:I

.field private cuV:I

.field private cuW:I

.field private cuX:Z

.field private cuY:Z

.field private final mCardContainer:Lfmt;

.field private final mClock:Lemp;

.field private mScrollView:Lekf;

.field private final mView:Landroid/view/View;


# direct methods
.method private varargs constructor <init>(Landroid/view/View;Lfmt;Lemp;[Lizj;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lfln;->mView:Landroid/view/View;

    .line 59
    iput-object p2, p0, Lfln;->mCardContainer:Lfmt;

    .line 60
    iput-object p4, p0, Lfln;->cmH:[Lizj;

    .line 61
    iput-object p3, p0, Lfln;->mClock:Lemp;

    .line 62
    return-void
.end method

.method public static varargs a(Landroid/view/View;Lfmt;Lemp;[Lizj;)V
    .locals 8

    .prologue
    const v7, 0x7f110005

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    array-length v0, p3

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 78
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-virtual {p0, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 86
    array-length v4, p3

    move v3, v2

    move v1, v2

    :goto_2
    if-ge v3, v4, :cond_2

    aget-object v5, p3, v3

    .line 87
    const/16 v0, 0xd7

    new-array v6, v2, [I

    invoke-static {v5, v0, v6}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 88
    add-int/lit8 v0, v1, 0x1

    aput-object v5, p3, v1

    .line 86
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    :cond_0
    move v0, v2

    .line 77
    goto :goto_0

    :cond_1
    move v1, v2

    .line 82
    goto :goto_1

    .line 92
    :cond_2
    if-eqz v1, :cond_3

    .line 93
    new-instance v2, Lfln;

    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizj;

    invoke-direct {v2, p0, p1, p2, v0}, Lfln;-><init>(Landroid/view/View;Lfmt;Lemp;[Lizj;)V

    .line 100
    invoke-virtual {p0, v7, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 102
    invoke-virtual {p0, v2}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 103
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 104
    invoke-virtual {v2, p0}, Lfln;->onViewAttachedToWindow(Landroid/view/View;)V

    .line 107
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method private aAK()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    .line 124
    iget-object v1, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v1}, Lekf;->ate()I

    move-result v1

    add-int/2addr v1, v0

    .line 125
    iget-boolean v2, p0, Lfln;->cuX:Z

    if-nez v2, :cond_0

    iget v2, p0, Lfln;->cuV:I

    if-ne v0, v2, :cond_0

    iget v2, p0, Lfln;->cuW:I

    if-eq v1, v2, :cond_1

    .line 128
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lfln;->cuX:Z

    .line 129
    iget v2, p0, Lfln;->cuT:I

    iget v3, p0, Lfln;->cuU:I

    invoke-direct {p0, v2, v3, v0, v1}, Lfln;->f(IIII)V

    .line 131
    :cond_1
    return-void
.end method

.method private aAL()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lfln;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 149
    iget v0, p0, Lfln;->cuU:I

    iget v1, p0, Lfln;->cuT:I

    if-ne v0, v1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-boolean v0, p0, Lfln;->cuY:Z

    if-nez v0, :cond_0

    .line 155
    iget v0, p0, Lfln;->cuU:I

    iget v1, p0, Lfln;->cuV:I

    if-lt v0, v1, :cond_0

    .line 158
    iget v0, p0, Lfln;->cuT:I

    iget v1, p0, Lfln;->cuW:I

    if-gt v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lfln;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lfln;->cuR:J

    .line 164
    iget-object v0, p0, Lfln;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 165
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lfln;->cuS:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private aAM()V
    .locals 12

    .prologue
    .line 170
    iget-wide v0, p0, Lfln;->cuR:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lfln;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 172
    iget-wide v0, p0, Lfln;->cuR:J

    sub-long v4, v2, v0

    .line 182
    iget-object v0, p0, Lfln;->cmH:[Lizj;

    array-length v0, v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 183
    iget-object v6, p0, Lfln;->cmH:[Lizj;

    array-length v7, v6

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 184
    new-instance v9, Lgam;

    const/16 v10, 0xd7

    const/4 v11, 0x0

    new-array v11, v11, [I

    invoke-static {v8, v10, v11}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v10

    invoke-direct {v9, v8, v10, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v9}, Lgam;->aDZ()Lizv;

    move-result-object v8

    .line 187
    invoke-virtual {v8, v4, v5}, Lizv;->cx(J)Lizv;

    .line 188
    iget-boolean v9, p0, Lfln;->cuS:Z

    invoke-virtual {v8, v9}, Lizv;->hy(Z)Lizv;

    .line 189
    iget v9, p0, Lfln;->cuT:I

    invoke-virtual {v8, v9}, Lizv;->ob(I)Lizv;

    .line 190
    iget v9, p0, Lfln;->cuU:I

    iget v10, p0, Lfln;->cuT:I

    sub-int/2addr v9, v10

    invoke-virtual {v8, v9}, Lizv;->nY(I)Lizv;

    .line 191
    iget v9, p0, Lfln;->cuV:I

    invoke-virtual {v8, v9}, Lizv;->nZ(I)Lizv;

    .line 192
    iget v9, p0, Lfln;->cuW:I

    invoke-virtual {v8, v9}, Lizv;->oa(I)Lizv;

    .line 193
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_0
    iget-object v0, p0, Lfln;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfml;->V(Ljava/util/List;)V

    .line 198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfln;->cuR:J

    .line 200
    :cond_1
    return-void
.end method

.method public static b(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 111
    const v0, 0x7f110005

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfln;

    .line 112
    if-eqz v0, :cond_0

    .line 113
    iget-boolean v1, v0, Lfln;->cuY:Z

    if-eq v1, p1, :cond_0

    iput-boolean p1, v0, Lfln;->cuY:Z

    if-eqz p1, :cond_1

    invoke-direct {v0}, Lfln;->aAM()V

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-direct {v0}, Lfln;->aAL()V

    goto :goto_0
.end method

.method private f(IIII)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lfln;->aAM()V

    .line 137
    iput p1, p0, Lfln;->cuT:I

    .line 138
    iput p2, p0, Lfln;->cuU:I

    .line 139
    iput p3, p0, Lfln;->cuV:I

    .line 140
    iput p4, p0, Lfln;->cuW:I

    .line 143
    invoke-direct {p0}, Lfln;->aAL()V

    .line 144
    return-void
.end method


# virtual methods
.method public final aa(II)V
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lfln;->cuV:I

    if-eq v0, p1, :cond_0

    .line 311
    invoke-direct {p0}, Lfln;->aAM()V

    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfln;->cuX:Z

    .line 314
    :cond_0
    return-void
.end method

.method public final dr(I)V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public final iX(I)V
    .locals 0

    .prologue
    .line 263
    if-nez p1, :cond_0

    .line 264
    invoke-direct {p0}, Lfln;->aAL()V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-direct {p0}, Lfln;->aAM()V

    goto :goto_0
.end method

.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    .line 244
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    iget-object v1, p0, Lfln;->mView:Landroid/view/View;

    invoke-interface {v0, v1}, Lekf;->aH(Landroid/view/View;)I

    move-result v1

    .line 245
    iget-object v0, p0, Lfln;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int v2, v1, v0

    .line 246
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v3

    .line 247
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->ate()I

    move-result v0

    add-int v4, v3, v0

    .line 248
    iget v0, p0, Lfln;->cuT:I

    if-ne v1, v0, :cond_0

    iget v0, p0, Lfln;->cuU:I

    if-ne v2, v0, :cond_0

    iget v0, p0, Lfln;->cuV:I

    if-ne v3, v0, :cond_0

    iget v0, p0, Lfln;->cuW:I

    if-eq v4, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 254
    :goto_0
    if-eqz v0, :cond_1

    .line 255
    invoke-direct {p0, v1, v2, v3, v4}, Lfln;->f(IIII)V

    .line 257
    :cond_1
    return-void

    .line 248
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 210
    iget-object v0, p0, Lfln;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->wj()Lekf;

    move-result-object v0

    iput-object v0, p0, Lfln;->mScrollView:Lekf;

    .line 211
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    if-nez v0, :cond_0

    .line 212
    const-string v0, "EntryViewRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scroll container is null, not logging card for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfln;->cmH:[Lizj;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lizj;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 225
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v0, p0}, Lekf;->a(Lekg;)V

    .line 219
    iget-object v0, p0, Lfln;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 220
    iget-object v0, p0, Lfln;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAE()Landroid/database/Observable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V

    .line 223
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    iput v0, p0, Lfln;->cuV:I

    .line 224
    iget v0, p0, Lfln;->cuV:I

    iget-object v1, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v1}, Lekf;->ate()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lfln;->cuW:I

    goto :goto_0
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    invoke-direct {p0}, Lfln;->aAM()V

    .line 231
    iget-object v0, p0, Lfln;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 232
    iget-object v0, p0, Lfln;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAE()Landroid/database/Observable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/database/Observable;->unregisterObserver(Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lfln;->mScrollView:Lekf;

    invoke-interface {v0, p0}, Lekf;->b(Lekg;)V

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lfln;->mScrollView:Lekf;

    .line 235
    iput v1, p0, Lfln;->cuT:I

    .line 236
    iput v1, p0, Lfln;->cuU:I

    .line 237
    iput v1, p0, Lfln;->cuV:I

    .line 238
    iput v1, p0, Lfln;->cuW:I

    .line 239
    return-void
.end method

.method public final vS()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lfln;->aAK()V

    .line 319
    return-void
.end method

.method public final vT()V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Lfln;->aAK()V

    .line 324
    return-void
.end method

.method public final vU()V
    .locals 0

    .prologue
    .line 328
    return-void
.end method

.method public final vV()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method public final vW()V
    .locals 0

    .prologue
    .line 340
    return-void
.end method
