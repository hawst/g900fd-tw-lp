.class public final Ljnb;
.super Ljsl;
.source "PG"


# instance fields
.field private aeI:D

.field private aeJ:D

.field private aez:I

.field private agq:Ljava/lang/String;

.field private dYT:Ljava/lang/String;

.field private euI:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 309
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 310
    const/4 v0, 0x0

    iput v0, p0, Ljnb;->aez:I

    iput-wide v2, p0, Ljnb;->aeI:D

    iput-wide v2, p0, Ljnb;->aeJ:D

    const-string v0, ""

    iput-object v0, p0, Ljnb;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnb;->dYT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnb;->euI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljnb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnb;->eCz:I

    .line 311
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 186
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljnb;->aeI:D

    iget v0, p0, Ljnb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Ljnb;->aeJ:D

    iget v0, p0, Ljnb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnb;->agq:Ljava/lang/String;

    iget v0, p0, Ljnb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnb;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnb;->dYT:Ljava/lang/String;

    iget v0, p0, Ljnb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnb;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnb;->euI:Ljava/lang/String;

    iget v0, p0, Ljnb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljnb;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 328
    iget v0, p0, Ljnb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 329
    const/4 v0, 0x1

    iget-wide v2, p0, Ljnb;->aeI:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 331
    :cond_0
    iget v0, p0, Ljnb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 332
    const/4 v0, 0x2

    iget-wide v2, p0, Ljnb;->aeJ:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 334
    :cond_1
    iget v0, p0, Ljnb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 335
    const/4 v0, 0x3

    iget-object v1, p0, Ljnb;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 337
    :cond_2
    iget v0, p0, Ljnb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 338
    const/4 v0, 0x4

    iget-object v1, p0, Ljnb;->dYT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 340
    :cond_3
    iget v0, p0, Ljnb;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 341
    const/4 v0, 0x5

    iget-object v1, p0, Ljnb;->euI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 343
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 344
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 348
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 349
    iget v1, p0, Ljnb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 350
    const/4 v1, 0x1

    iget-wide v2, p0, Ljnb;->aeI:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 353
    :cond_0
    iget v1, p0, Ljnb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 354
    const/4 v1, 0x2

    iget-wide v2, p0, Ljnb;->aeJ:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 357
    :cond_1
    iget v1, p0, Ljnb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 358
    const/4 v1, 0x3

    iget-object v2, p0, Ljnb;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    :cond_2
    iget v1, p0, Ljnb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 362
    const/4 v1, 0x4

    iget-object v2, p0, Ljnb;->dYT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_3
    iget v1, p0, Ljnb;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 366
    const/4 v1, 0x5

    iget-object v2, p0, Ljnb;->euI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_4
    return v0
.end method
