.class public final Ljen;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efO:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12425
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12426
    const/4 v0, 0x0

    iput v0, p0, Ljen;->aez:I

    iput-object v1, p0, Ljen;->efN:Ljez;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljen;->efO:Z

    iput-object v1, p0, Ljen;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljen;->eCz:I

    .line 12427
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 12384
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljen;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljen;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljen;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljen;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljen;->efO:Z

    iget v0, p0, Ljen;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljen;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12441
    iget-object v0, p0, Ljen;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 12442
    const/4 v0, 0x1

    iget-object v1, p0, Ljen;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 12444
    :cond_0
    iget v0, p0, Ljen;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 12445
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljen;->efO:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12447
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12448
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12452
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12453
    iget-object v1, p0, Ljen;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 12454
    const/4 v1, 0x1

    iget-object v2, p0, Ljen;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12457
    :cond_0
    iget v1, p0, Ljen;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 12458
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljen;->efO:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12461
    :cond_1
    return v0
.end method
