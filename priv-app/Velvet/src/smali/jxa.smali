.class public final Ljxa;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJX:Ljsm;


# instance fields
.field private aez:I

.field private eJY:Z

.field private eJZ:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljxa;

    const v2, 0x1d85dd42

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxa;->eJX:Ljsm;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 67
    iput v0, p0, Ljxa;->aez:I

    iput-boolean v0, p0, Ljxa;->eJY:Z

    iput-boolean v0, p0, Ljxa;->eJZ:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljxa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxa;->eCz:I

    .line 68
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxa;->eJY:Z

    iget v0, p0, Ljxa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxa;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxa;->eJZ:Z

    iget v0, p0, Ljxa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxa;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Ljxa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljxa;->eJY:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 85
    :cond_0
    iget v0, p0, Ljxa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 86
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljxa;->eJZ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 88
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 89
    return-void
.end method

.method public final jn(Z)Ljxa;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljxa;->eJY:Z

    .line 35
    iget v0, p0, Ljxa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxa;->aez:I

    .line 36
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 94
    iget v1, p0, Ljxa;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljxa;->eJY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 98
    :cond_0
    iget v1, p0, Ljxa;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 99
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljxa;->eJZ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    return v0
.end method
