.class public final Lfhj;
.super Landroid/app/DialogFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Landroid/app/Fragment;)Lfhj;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    const-string v2, "question_key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 49
    new-instance v2, Lfhj;

    invoke-direct {v2}, Lfhj;-><init>()V

    .line 50
    invoke-virtual {v2, v0}, Lfhj;->setArguments(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {v2, p1, v1}, Lfhj;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 52
    return-object v2

    :cond_0
    move v0, v1

    .line 44
    goto :goto_0
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-virtual {p0}, Lfhj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    const-string v1, "question_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v1

    .line 64
    if-nez p1, :cond_2

    if-eqz v1, :cond_2

    iget-object v1, v1, Ljdf;->ebG:[I

    invoke-static {v1}, Lius;->w([I)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v1

    move-object v2, v1

    .line 68
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCq()Ljava/util/List;

    move-result-object v1

    .line 69
    if-nez v1, :cond_0

    .line 70
    const-string v4, "MultipleSelectQuestionDialogFragment"

    const-string v5, "multiple select question missing options"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    if-nez v1, :cond_3

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v1

    move-object v4, v1

    .line 75
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    new-array v7, v1, [Ljava/lang/String;

    .line 76
    if-nez v2, :cond_4

    .line 77
    :goto_2
    const/4 v1, 0x0

    move v5, v1

    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_6

    .line 78
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfpa;

    .line 79
    iget-object v6, v1, Lfpa;->cyk:Ljava/lang/String;

    .line 80
    if-eqz v6, :cond_5

    :goto_4
    aput-object v6, v7, v5

    .line 81
    if-eqz v3, :cond_1

    .line 82
    iget-object v1, v1, Lfpa;->cyl:Ljava/lang/Integer;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    aput-boolean v1, v3, v5

    .line 77
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    :cond_2
    move-object v2, v3

    .line 64
    goto :goto_0

    :cond_3
    move-object v4, v1

    .line 72
    goto :goto_1

    .line 76
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [Z

    goto :goto_2

    .line 80
    :cond_5
    const-string v6, ""

    goto :goto_4

    .line 86
    :cond_6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lfhj;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 87
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v5, Lfhm;

    invoke-direct {v5, p0}, Lfhm;-><init>(Lfhj;)V

    invoke-virtual {v2, v7, v3, v5}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    new-instance v5, Lfhl;

    invoke-direct {v5, p0, v4, v0}, Lfhl;-><init>(Lfhj;Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V

    invoke-virtual {v2, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    new-instance v3, Lfhk;

    invoke-direct {v3, p0}, Lfhk;-><init>(Lfhj;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 127
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
