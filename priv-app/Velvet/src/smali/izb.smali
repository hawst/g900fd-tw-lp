.class public final Lizb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAh:Ljava/lang/String;

.field private dRA:J

.field private dRy:Ljava/lang/String;

.field private dRz:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31611
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 31612
    const/4 v0, 0x0

    iput v0, p0, Lizb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lizb;->dRy:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lizb;->dAh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lizb;->dRz:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lizb;->dRA:J

    const/4 v0, 0x0

    iput-object v0, p0, Lizb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizb;->eCz:I

    .line 31613
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 31507
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizb;->dRy:Ljava/lang/String;

    iget v0, p0, Lizb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizb;->dAh:Ljava/lang/String;

    iget v0, p0, Lizb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizb;->dRz:Ljava/lang/String;

    iget v0, p0, Lizb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lizb;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lizb;->dRA:J

    iget v0, p0, Lizb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lizb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 31629
    iget v0, p0, Lizb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 31630
    const/4 v0, 0x1

    iget-object v1, p0, Lizb;->dRy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31632
    :cond_0
    iget v0, p0, Lizb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 31633
    const/4 v0, 0x2

    iget-object v1, p0, Lizb;->dAh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31635
    :cond_1
    iget v0, p0, Lizb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 31636
    const/4 v0, 0x3

    iget-object v1, p0, Lizb;->dRz:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31638
    :cond_2
    iget v0, p0, Lizb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 31639
    const/4 v0, 0x4

    iget-wide v2, p0, Lizb;->dRA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 31641
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 31642
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 31646
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 31647
    iget v1, p0, Lizb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 31648
    const/4 v1, 0x1

    iget-object v2, p0, Lizb;->dRy:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31651
    :cond_0
    iget v1, p0, Lizb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 31652
    const/4 v1, 0x2

    iget-object v2, p0, Lizb;->dAh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31655
    :cond_1
    iget v1, p0, Lizb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 31656
    const/4 v1, 0x3

    iget-object v2, p0, Lizb;->dRz:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31659
    :cond_2
    iget v1, p0, Lizb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 31660
    const/4 v1, 0x4

    iget-wide v2, p0, Lizb;->dRA:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 31663
    :cond_3
    return v0
.end method
