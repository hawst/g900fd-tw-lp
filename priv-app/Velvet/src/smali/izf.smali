.class public final Lizf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dRM:[Lizf;


# instance fields
.field private aez:I

.field private dRN:I

.field public dRO:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v0, p0, Lizf;->aez:I

    iput v0, p0, Lizf;->dRN:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Lizf;->dRO:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lizf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizf;->eCz:I

    return-void
.end method

.method public static bcL()[Lizf;
    .locals 2

    sget-object v0, Lizf;->dRM:[Lizf;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lizf;->dRM:[Lizf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lizf;

    sput-object v0, Lizf;->dRM:[Lizf;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lizf;->dRM:[Lizf;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lizf;->dRN:I

    iget v0, p0, Lizf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizf;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x15

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizf;->dRO:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lizf;->dRO:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lizf;->dRO:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lizf;->dRO:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Lizf;->dRO:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [I

    if-eqz v0, :cond_4

    iget-object v4, p0, Lizf;->dRO:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lizf;->dRO:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    iput-object v3, p0, Lizf;->dRO:[I

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget v0, p0, Lizf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lizf;->dRN:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget-object v0, p0, Lizf;->dRO:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lizf;->dRO:[I

    array-length v0, v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lizf;->dRO:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lizf;->dRO:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bcM()I
    .locals 1

    iget v0, p0, Lizf;->dRN:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Lizf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lizf;->dRN:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lizf;->dRO:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lizf;->dRO:[I

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lizf;->dRO:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    iget-object v1, p0, Lizf;->dRO:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public final nU(I)Lizf;
    .locals 1

    iput p1, p0, Lizf;->dRN:I

    iget v0, p0, Lizf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizf;->aez:I

    return-object p0
.end method
