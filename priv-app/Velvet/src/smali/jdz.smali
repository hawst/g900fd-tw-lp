.class public final Ljdz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private edc:I

.field private edm:I

.field private edn:I

.field private edo:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v0, p0, Ljdz;->aez:I

    iput v0, p0, Ljdz;->edm:I

    iput v0, p0, Ljdz;->edn:I

    iput v0, p0, Ljdz;->edo:I

    const/4 v0, 0x1

    iput v0, p0, Ljdz;->edc:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljdz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdz;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdz;->edm:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdz;->edn:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdz;->edo:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdz;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdz;->edc:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdz;->edm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljdz;->edn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljdz;->edo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_2
    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Ljdz;->edc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bid()I
    .locals 1

    iget v0, p0, Ljdz;->edc:I

    return v0
.end method

.method public final bie()Z
    .locals 1

    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bil()Z
    .locals 1

    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bim()Z
    .locals 1

    iget v0, p0, Ljdz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHour()I
    .locals 1

    iget v0, p0, Ljdz;->edm:I

    return v0
.end method

.method public final getMinute()I
    .locals 1

    iget v0, p0, Ljdz;->edn:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljdz;->edm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljdz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljdz;->edn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljdz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljdz;->edo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljdz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Ljdz;->edc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    return v0
.end method

.method public final pc(I)Ljdz;
    .locals 1

    const/16 v0, 0xa

    iput v0, p0, Ljdz;->edm:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdz;->aez:I

    return-object p0
.end method

.method public final pd(I)Ljdz;
    .locals 1

    const/16 v0, 0xc

    iput v0, p0, Ljdz;->edn:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdz;->aez:I

    return-object p0
.end method

.method public final pe(I)Ljdz;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Ljdz;->edc:I

    iget v0, p0, Ljdz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdz;->aez:I

    return-object p0
.end method
