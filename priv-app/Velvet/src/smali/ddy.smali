.class public final Lddy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldeb;


# static fields
.field private static final bur:Ljava/util/Comparator;


# instance fields
.field private final boT:Ldeb;

.field private final mClock:Lemp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lddz;

    invoke-direct {v0}, Lddz;-><init>()V

    sput-object v0, Lddy;->bur:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ldeb;Lemp;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lddy;->boT:Ldeb;

    .line 37
    iput-object p2, p0, Lddy;->mClock:Lemp;

    .line 38
    return-void
.end method

.method public static a(Ldeb;Ldem;I)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 70
    new-instance v1, Ldej;

    const-string v0, "icing-and-web"

    invoke-virtual {p1}, Ldem;->abj()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 74
    const/4 v0, 0x0

    invoke-interface {p0, p1, p2, v0, v1}, Ldeb;->a(Ldem;ILdef;Lddw;)V

    .line 77
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ldem;->fN(I)Ljava/util/List;

    move-result-object v4

    .line 79
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 81
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Lddw;->c(Ljava/lang/Iterable;)I

    .line 85
    :cond_0
    invoke-interface {v1}, Lddw;->abe()Ljava/util/List;

    move-result-object v0

    sget-object v4, Lddy;->bur:Ljava/util/Comparator;

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 88
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Ldem;->fN(I)Ljava/util/List;

    move-result-object v4

    .line 90
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 91
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 92
    new-instance v5, Ldej;

    const-string v4, "omnibox-suggestions"

    invoke-virtual {p1}, Ldem;->abj()Lcom/google/android/shared/search/Query;

    move-result-object v6

    invoke-direct {v5, v4, v6}, Ldej;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 95
    invoke-interface {v5, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    .line 97
    invoke-interface {v1}, Lddw;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/search/Suggestion;

    .line 98
    invoke-interface {v5, v1}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 99
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asF()I

    move-result v1

    const/4 v7, -0x1

    if-ne v1, v7, :cond_3

    move v1, v2

    :goto_2
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 105
    new-instance v1, Lded;

    invoke-direct {v1}, Lded;-><init>()V

    invoke-virtual {v1, v0}, Lded;->h(Lcom/google/android/shared/search/Suggestion;)Lded;

    move-result-object v1

    iput v4, v1, Lded;->bvg:I

    invoke-virtual {v1}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v1

    invoke-interface {v5, v3, v1}, Lddw;->a(ILcom/google/android/shared/search/Suggestion;)V

    .line 110
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    .line 111
    goto :goto_1

    :cond_2
    move v0, v3

    .line 80
    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 99
    goto :goto_2

    .line 112
    :cond_4
    invoke-interface {v5}, Lddw;->abe()Ljava/util/List;

    move-result-object v0

    .line 114
    :goto_3
    return-object v0

    :cond_5
    invoke-interface {v1}, Lddw;->abe()Ljava/util/List;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(Ljava/util/List;Ljava/util/List;ZIJLdfd;Lddw;)V
    .locals 8

    .prologue
    .line 123
    if-eqz p1, :cond_0

    .line 125
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 127
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p7}, Lddw;->getCount()I

    move-result v2

    if-ge v2, p3, :cond_0

    .line 128
    invoke-interface {p7, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_0

    .line 134
    :cond_0
    if-eqz p2, :cond_1

    .line 135
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 136
    invoke-interface {p7}, Lddw;->getCount()I

    move-result v2

    if-ge v2, p3, :cond_2

    .line 137
    invoke-interface {p7, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_1

    .line 142
    :cond_1
    invoke-virtual {p6, p4, p5}, Ldfd;->ar(J)J

    move-result-wide v2

    .line 143
    invoke-virtual {p6, p4, p5, p3}, Ldfd;->g(JI)I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 145
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 146
    invoke-interface {p7}, Lddw;->getCount()I

    move-result v5

    if-ge v5, v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asE()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-ltz v5, :cond_2

    .line 148
    invoke-interface {p7, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_2

    .line 154
    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    invoke-interface {p7}, Lddw;->getCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 157
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 158
    invoke-interface {p7}, Lddw;->getCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 160
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 161
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v3

    invoke-static {v3}, Lifv;->gY(Z)V

    .line 162
    invoke-interface {p7}, Lddw;->getCount()I

    move-result v3

    if-ge v3, v1, :cond_3

    .line 163
    invoke-interface {p7, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_3

    .line 168
    :cond_3
    if-eqz p2, :cond_4

    .line 169
    invoke-interface {p7}, Lddw;->aaZ()V

    .line 171
    :cond_4
    return-void
.end method

.method public static c(Ldem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-virtual {p0, v0}, Ldem;->fK(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ldem;->fK(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Ldem;->fK(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Ldem;ILdef;Lddw;)V
    .locals 8

    .prologue
    .line 44
    iget-object v0, p0, Lddy;->boT:Ldeb;

    invoke-static {v0, p1, p2}, Lddy;->a(Ldeb;Ldem;I)Ljava/util/List;

    move-result-object v0

    .line 46
    if-nez p3, :cond_0

    const/4 v1, 0x0

    .line 48
    :goto_0
    invoke-static {p1}, Lddy;->c(Ldem;)Z

    move-result v2

    iget-object v3, p0, Lddy;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {p1}, Ldem;->getCreationTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {p1}, Ldem;->abf()Ldfd;

    move-result-object v6

    move v3, p2

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lddy;->a(Ljava/util/List;Ljava/util/List;ZIJLdfd;Lddw;)V

    .line 58
    return-void

    .line 46
    :cond_0
    invoke-interface {p3}, Ldef;->abe()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method
