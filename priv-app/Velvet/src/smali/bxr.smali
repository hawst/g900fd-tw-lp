.class public final Lbxr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field aNt:Landroid/view/View;

.field private final aOb:Landroid/media/RemoteController$OnClientUpdateListener;

.field final aOc:Lejt;

.field final aOd:Lejt;

.field final aOe:Ljava/util/Vector;

.field aOf:[F

.field aOg:Landroid/view/View;

.field aOh:Z

.field aOi:Z

.field private aOj:Landroid/media/RemoteController;

.field aOk:Landroid/widget/ImageView;

.field private mActivity:Landroid/app/Activity;

.field mAudioManager:Landroid/media/AudioManager;

.field mState:Lbyi;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILbyi;Landroid/media/AudioManager;Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lbxs;

    invoke-direct {v0, p0}, Lbxs;-><init>(Lbxr;)V

    iput-object v0, p0, Lbxr;->aOb:Landroid/media/RemoteController$OnClientUpdateListener;

    .line 97
    new-instance v0, Lbxt;

    invoke-direct {v0, p0}, Lbxt;-><init>(Lbxr;)V

    iput-object v0, p0, Lbxr;->aOc:Lejt;

    .line 155
    new-instance v0, Lbxu;

    invoke-direct {v0, p0}, Lbxu;-><init>(Lbxr;)V

    iput-object v0, p0, Lbxr;->aOd:Lejt;

    .line 180
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbxr;->aOe:Ljava/util/Vector;

    .line 196
    iput-boolean v1, p0, Lbxr;->aOh:Z

    .line 201
    iput-boolean v1, p0, Lbxr;->aOi:Z

    .line 217
    iput-object p1, p0, Lbxr;->mActivity:Landroid/app/Activity;

    .line 218
    iput-object p3, p0, Lbxr;->mState:Lbyi;

    .line 219
    iput-object p4, p0, Lbxr;->mAudioManager:Landroid/media/AudioManager;

    .line 220
    new-instance v0, Landroid/media/RemoteController;

    iget-object v1, p0, Lbxr;->aOb:Landroid/media/RemoteController$OnClientUpdateListener;

    invoke-direct {v0, p1, v1}, Landroid/media/RemoteController;-><init>(Landroid/content/Context;Landroid/media/RemoteController$OnClientUpdateListener;)V

    iput-object v0, p0, Lbxr;->aOj:Landroid/media/RemoteController;

    .line 221
    iget-object v0, p0, Lbxr;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lbxr;->aOj:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteController(Landroid/media/RemoteController;)Z

    .line 222
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 223
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 224
    iget-object v1, p0, Lbxr;->aOj:Landroid/media/RemoteController;

    invoke-virtual {v1, v0, v0}, Landroid/media/RemoteController;->setArtworkConfiguration(II)Z

    .line 225
    iget-object v0, p0, Lbxr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbxr;->aOk:Landroid/widget/ImageView;

    .line 226
    iput-object p6, p0, Lbxr;->aOg:Landroid/view/View;

    .line 227
    iput-object p5, p0, Lbxr;->aNt:Landroid/view/View;

    .line 228
    return-void
.end method

.method static synthetic a(Lbxr;I)V
    .locals 2

    .prologue
    .line 27
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbxr;->aOk:Landroid/widget/ImageView;

    const v1, 0x7f02015c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbxr;->aOk:Landroid/widget/ImageView;

    const v1, 0x7f02015b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic a(Lbxr;IIF)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v1, p2, v0}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    invoke-virtual {v0, p3}, Landroid/view/View;->setX(F)V

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0}, Lbxr;->Bt()V

    return-void
.end method

.method static a(Ljava/lang/Boolean;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 298
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    const v0, 0x3ecccccd    # 0.4f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method static ae(II)Z
    .locals 1

    .prologue
    .line 375
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method Bt()V
    .locals 8

    .prologue
    const-wide/16 v6, 0xc8

    .line 347
    const/4 v0, 0x1

    .line 348
    iget-object v1, p0, Lbxr;->aOe:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 349
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, Lbxr;->aOf:[F

    aget v4, v4, v1

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    sub-float v0, v4, v0

    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->translationXBy(F)Landroid/view/ViewPropertyAnimator;

    .line 350
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 351
    goto :goto_0

    .line 352
    :cond_0
    iget-object v0, p0, Lbxr;->aOk:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 354
    return-void
.end method

.method public final ep(I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lbxr;->aOj:Landroid/media/RemoteController;

    if-nez v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lbxr;->aOj:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 397
    iget-object v0, p0, Lbxr;->aOj:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method public final o(III)V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lbxr;->aOe:Ljava/util/Vector;

    iget-object v1, p0, Lbxr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 238
    iget-object v0, p0, Lbxr;->aOe:Ljava/util/Vector;

    iget-object v1, p0, Lbxr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 239
    iget-object v0, p0, Lbxr;->aOe:Ljava/util/Vector;

    iget-object v1, p0, Lbxr;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, p3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 240
    return-void
.end method
