.class public final Ldpb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aMD:Leqo;

.field public final bEx:Ldog;

.field public final bEy:Ljava/lang/Runnable;

.field public final bEz:Ljava/lang/Runnable;

.field public bpr:Ldah;

.field private final mActionState:Ldbd;

.field private final mContext:Landroid/content/Context;

.field private final mGsaConfigFlags:Lchk;

.field private mIntentStarter:Leqp;

.field private final mLocalBroadcastManager:Lcn;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mScreenStateHelper:Ldmh;

.field private final mServiceForegroundHelper:Ldaz;

.field final mServiceState:Ldcu;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Ldcu;Landroid/content/Context;Lgql;Ldaz;Ldmh;Lcom/google/android/search/core/state/QueryState;Ldbd;Ldbh;Leqp;Landroid/telephony/TelephonyManager;Lcn;)V
    .locals 7

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v1, Ldpc;

    const-string v2, "setStartForSessionHandover"

    invoke-direct {v1, p0, v2}, Ldpc;-><init>(Ldpb;Ljava/lang/String;)V

    iput-object v1, p0, Ldpb;->bEy:Ljava/lang/Runnable;

    .line 68
    new-instance v1, Ldpd;

    invoke-direct {v1, p0}, Ldpd;-><init>(Ldpb;)V

    iput-object v1, p0, Ldpb;->bEz:Ljava/lang/Runnable;

    .line 87
    iput-object p1, p0, Ldpb;->mServiceState:Ldcu;

    .line 88
    iput-object p2, p0, Ldpb;->mContext:Landroid/content/Context;

    .line 89
    iget-object v1, p3, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    iput-object v1, p0, Ldpb;->aMD:Leqo;

    .line 90
    invoke-virtual {p3}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    iput-object v1, p0, Ldpb;->mGsaConfigFlags:Lchk;

    .line 91
    iput-object p4, p0, Ldpb;->mServiceForegroundHelper:Ldaz;

    .line 92
    iput-object p5, p0, Ldpb;->mScreenStateHelper:Ldmh;

    .line 93
    iput-object p6, p0, Ldpb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 94
    iput-object p7, p0, Ldpb;->mActionState:Ldbd;

    .line 95
    move-object/from16 v0, p9

    iput-object v0, p0, Ldpb;->mIntentStarter:Leqp;

    .line 96
    move-object/from16 v0, p10

    iput-object v0, p0, Ldpb;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 97
    move-object/from16 v0, p11

    iput-object v0, p0, Ldpb;->mLocalBroadcastManager:Lcn;

    .line 99
    new-instance v1, Ldog;

    iget-object v2, p0, Ldpb;->mContext:Landroid/content/Context;

    iget-object v3, p0, Ldpb;->mGsaConfigFlags:Lchk;

    iget-object v4, p0, Ldpb;->mServiceState:Ldcu;

    iget-object v5, p0, Ldpb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    move-object v6, p8

    invoke-direct/range {v1 .. v6}, Ldog;-><init>(Landroid/content/Context;Lchk;Ldcu;Lcom/google/android/search/core/state/QueryState;Ldbh;)V

    iput-object v1, p0, Ldpb;->bEx:Ldog;

    .line 101
    return-void
.end method

.method private varargs b([Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 257
    array-length v0, p1

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 260
    iget-object v0, p0, Ldpb;->bpr:Ldah;

    if-eqz v0, :cond_2

    .line 264
    iget-object v0, p0, Ldpb;->bpr:Ldah;

    invoke-virtual {v0, p1}, Ldah;->a([Landroid/content/Intent;)V

    .line 275
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 257
    goto :goto_0

    .line 265
    :cond_2
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcgg;->aVr:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    :cond_3
    array-length v0, p1

    :goto_2
    if-ge v2, v0, :cond_4

    aget-object v1, p1, v2

    .line 271
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 270
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 273
    :cond_4
    iget-object v0, p0, Ldpb;->mIntentStarter:Leqp;

    invoke-interface {v0, p1}, Leqp;->b([Landroid/content/Intent;)Z

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lddb;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x64

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 108
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    iget-object v0, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YF()Ljava/util/Set;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_0

    .line 111
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    .line 112
    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 113
    iget-object v0, p0, Ldpb;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/search/core/service/BroadcastListenerService;->a(Landroid/content/Context;[Ljava/lang/String;)V

    .line 115
    :cond_0
    iget-object v0, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YB()I

    move-result v0

    .line 116
    if-ne v0, v1, :cond_1

    .line 117
    iget-object v3, p0, Ldpb;->aMD:Leqo;

    iget-object v4, p0, Ldpb;->bEy:Ljava/lang/Runnable;

    invoke-interface {v3, v4}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 118
    iget-object v3, p0, Ldpb;->aMD:Leqo;

    iget-object v4, p0, Ldpb;->bEy:Ljava/lang/Runnable;

    iget-object v5, p0, Ldpb;->mGsaConfigFlags:Lchk;

    invoke-virtual {v5}, Lchk;->IE()I

    move-result v5

    int-to-long v6, v5

    invoke-interface {v3, v4, v6, v7}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 129
    :cond_1
    iget-object v3, p0, Ldpb;->mServiceForegroundHelper:Ldaz;

    invoke-virtual {v3, v0}, Ldaz;->fy(I)V

    .line 130
    iget-object v0, p0, Ldpb;->mServiceForegroundHelper:Ldaz;

    iget-object v3, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->YC()I

    move-result v3

    invoke-virtual {v0, v3}, Ldaz;->fz(I)V

    .line 132
    iget-object v0, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Zi()I

    move-result v0

    if-eqz v0, :cond_2

    packed-switch v0, :pswitch_data_0

    :cond_2
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    .line 190
    :cond_3
    :goto_1
    return-void

    .line 132
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.googlequicksearchbox"

    const-string v4, "com.google.android.handsfree.HandsFreeOverlayActivity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "handover-session-id"

    invoke-virtual {v0, v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "started-via-query"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-array v3, v1, [Landroid/content/Intent;

    aput-object v0, v3, v2

    invoke-direct {p0, v3}, Ldpb;->b([Landroid/content/Intent;)Z

    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Ldpb;->mContext:Landroid/content/Context;

    iget-object v3, p0, Ldpb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Ldpb;->mActionState:Ldbd;

    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v4

    invoke-static {v0, v3, v4, v2}, Lhgn;->a(Landroid/content/Context;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Z)Landroid/content/Intent;

    move-result-object v0

    new-array v3, v1, [Landroid/content/Intent;

    aput-object v0, v3, v2

    invoke-direct {p0, v3}, Ldpb;->b([Landroid/content/Intent;)Z

    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Ldpb;->mContext:Landroid/content/Context;

    iget-object v3, p0, Ldpb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Ldpb;->mActionState:Ldbd;

    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v4

    invoke-static {v0, v3, v4, v1}, Lhgn;->a(Landroid/content/Context;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v3, p0, Ldpb;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Ldpb;->mContext:Landroid/content/Context;

    iget-object v3, p0, Ldpb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Ldpb;->mActionState:Ldbd;

    invoke-virtual {v4}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v4

    const-string v5, "com.google.android.velvet.ui.VelvetLockscreenActivity"

    invoke-static {v0, v5}, Lhgn;->s(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, v3, v4, v8, v9}, Lhgn;->a(Landroid/content/Intent;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;J)V

    new-array v3, v1, [Landroid/content/Intent;

    aput-object v0, v3, v2

    invoke-direct {p0, v3}, Ldpb;->b([Landroid/content/Intent;)Z

    move v0, v1

    goto :goto_0

    .line 137
    :cond_4
    iget-object v3, p0, Ldpb;->mServiceState:Ldcu;

    iget-object v0, p0, Ldpb;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->ado()Z

    move-result v4

    iget-object v0, p0, Ldpb;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    invoke-virtual {v3, v4, v0}, Ldcu;->l(ZZ)I

    move-result v0

    .line 141
    if-ne v0, v1, :cond_b

    .line 143
    iget-object v0, p0, Ldpb;->aMD:Leqo;

    iget-object v3, p0, Ldpb;->bEz:Ljava/lang/Runnable;

    invoke-interface {v0, v3}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 144
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 145
    const/high16 v0, 0x10000000

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 146
    iget-object v0, p0, Ldpb;->mContext:Landroid/content/Context;

    const-class v4, Lbxh;

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 148
    const/4 v0, 0x0

    .line 151
    iget-object v4, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->Za()Z

    move-result v4

    if-nez v4, :cond_7

    .line 152
    iget-object v4, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->YL()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 153
    const-string v0, "driving-charging"

    .line 156
    :cond_5
    iget-object v4, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->YO()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 157
    const-string v0, "in-car-bluetooth"

    .line 160
    :cond_6
    if-eqz v0, :cond_7

    .line 161
    const-string v4, "start-reason"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    const-string v0, "start-time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 174
    :cond_7
    iget-object v0, p0, Ldpb;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 182
    :cond_8
    :goto_3
    iget-object v0, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YD()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 183
    invoke-static {v1}, Ldmh;->dE(Z)V

    .line 186
    :cond_9
    iget-object v0, p0, Ldpb;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YE()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187
    invoke-static {v2}, Ldmh;->dE(Z)V

    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 137
    goto :goto_2

    .line 175
    :cond_b
    const/4 v3, 0x3

    if-ne v0, v3, :cond_8

    .line 178
    new-instance v0, Landroid/content/Intent;

    const-string v3, "exit-hands-free-overlay"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 179
    iget-object v3, p0, Ldpb;->mLocalBroadcastManager:Lcn;

    invoke-virtual {v3, v0}, Lcn;->a(Landroid/content/Intent;)Z

    goto :goto_3

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
