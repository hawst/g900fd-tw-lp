.class public final Ljnv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aiK:Ljava/lang/String;

.field private evK:Ljnb;

.field private evL:[Ljava/lang/String;

.field private evM:Ljnc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2714
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2715
    const/4 v0, 0x0

    iput v0, p0, Ljnv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnv;->aiK:Ljava/lang/String;

    iput-object v1, p0, Ljnv;->evK:Ljnb;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljnv;->evL:[Ljava/lang/String;

    iput-object v1, p0, Ljnv;->evM:Ljnc;

    iput-object v1, p0, Ljnv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnv;->eCz:I

    .line 2716
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2664
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnv;->aiK:Ljava/lang/String;

    iget v0, p0, Ljnv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnv;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljnv;->evK:Ljnb;

    if-nez v0, :cond_1

    new-instance v0, Ljnb;

    invoke-direct {v0}, Ljnb;-><init>()V

    iput-object v0, p0, Ljnv;->evK:Ljnb;

    :cond_1
    iget-object v0, p0, Ljnv;->evK:Ljnb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljnv;->evL:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljnv;->evL:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljnv;->evL:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljnv;->evL:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljnv;->evM:Ljnc;

    if-nez v0, :cond_5

    new-instance v0, Ljnc;

    invoke-direct {v0}, Ljnc;-><init>()V

    iput-object v0, p0, Ljnv;->evM:Ljnc;

    :cond_5
    iget-object v0, p0, Ljnv;->evM:Ljnc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2732
    iget v0, p0, Ljnv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2733
    const/4 v0, 0x1

    iget-object v1, p0, Ljnv;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2735
    :cond_0
    iget-object v0, p0, Ljnv;->evK:Ljnb;

    if-eqz v0, :cond_1

    .line 2736
    const/4 v0, 0x2

    iget-object v1, p0, Ljnv;->evK:Ljnb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2738
    :cond_1
    iget-object v0, p0, Ljnv;->evL:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljnv;->evL:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2739
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljnv;->evL:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 2740
    iget-object v1, p0, Ljnv;->evL:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 2741
    if-eqz v1, :cond_2

    .line 2742
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2739
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2746
    :cond_3
    iget-object v0, p0, Ljnv;->evM:Ljnc;

    if-eqz v0, :cond_4

    .line 2747
    const/4 v0, 0x4

    iget-object v1, p0, Ljnv;->evM:Ljnc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2749
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2750
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2754
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2755
    iget v2, p0, Ljnv;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 2756
    const/4 v2, 0x1

    iget-object v3, p0, Ljnv;->aiK:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2759
    :cond_0
    iget-object v2, p0, Ljnv;->evK:Ljnb;

    if-eqz v2, :cond_1

    .line 2760
    const/4 v2, 0x2

    iget-object v3, p0, Ljnv;->evK:Ljnb;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2763
    :cond_1
    iget-object v2, p0, Ljnv;->evL:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljnv;->evL:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 2766
    :goto_0
    iget-object v4, p0, Ljnv;->evL:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 2767
    iget-object v4, p0, Ljnv;->evL:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 2768
    if-eqz v4, :cond_2

    .line 2769
    add-int/lit8 v3, v3, 0x1

    .line 2770
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2766
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2774
    :cond_3
    add-int/2addr v0, v2

    .line 2775
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 2777
    :cond_4
    iget-object v1, p0, Ljnv;->evM:Ljnc;

    if-eqz v1, :cond_5

    .line 2778
    const/4 v1, 0x4

    iget-object v2, p0, Ljnv;->evM:Ljnc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2781
    :cond_5
    return v0
.end method
