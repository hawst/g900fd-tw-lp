.class public final Lcoy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public beA:Landroid/database/ContentObserver;

.field final beu:Ljava/util/concurrent/Executor;

.field public final bev:Landroid/net/Uri;

.field public bew:Z

.field private volatile bex:Ljava/lang/String;

.field final bey:Ljava/lang/Runnable;

.field private final bez:Ljava/lang/Runnable;

.field public final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/Context;Lcjs;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcoz;

    const-string v1, "Peek RLZ"

    new-array v2, v4, [I

    aput v5, v2, v3

    invoke-direct {v0, p0, v1, v2}, Lcoz;-><init>(Lcoy;Ljava/lang/String;[I)V

    iput-object v0, p0, Lcoy;->bey:Ljava/lang/Runnable;

    .line 42
    new-instance v0, Lcpa;

    const-string v1, "Get RLZ"

    new-array v2, v4, [I

    aput v5, v2, v3

    invoke-direct {v0, p0, v1, v2}, Lcpa;-><init>(Lcoy;Ljava/lang/String;[I)V

    iput-object v0, p0, Lcoy;->bez:Ljava/lang/Runnable;

    .line 52
    iput-object p1, p0, Lcoy;->beu:Ljava/util/concurrent/Executor;

    .line 53
    iput-object p2, p0, Lcoy;->mContext:Landroid/content/Context;

    .line 54
    invoke-static {}, Lcjs;->Ln()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcoy;->bev:Landroid/net/Uri;

    .line 56
    iget-object v0, p0, Lcoy;->beu:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcoy;->bey:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final Rf()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcoy;->beu:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcoy;->bez:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 114
    iget-object v0, p0, Lcoy;->bex:Ljava/lang/String;

    return-object v0
.end method

.method public final cA(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 85
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcoy;->bev:Landroid/net/Uri;

    const-string v1, "peek"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 89
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcoy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 90
    if-eqz v1, :cond_4

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 91
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    move-object v0, v6

    .line 97
    :goto_1
    if-eqz v1, :cond_0

    .line 98
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 101
    :cond_0
    :goto_2
    iput-object v0, p0, Lcoy;->bex:Ljava/lang/String;

    .line 103
    return-void

    .line 85
    :cond_1
    iget-object v1, p0, Lcoy;->bev:Landroid/net/Uri;

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 95
    :goto_3
    :try_start_2
    const-string v2, "Velvet.RlzHelper"

    const-string v3, "Could not get RLZ: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 97
    if-eqz v1, :cond_3

    .line 98
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_2

    .line 97
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_4
    if-eqz v1, :cond_2

    .line 98
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 97
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 93
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_3
    move-object v0, v6

    goto :goto_2

    :cond_4
    move-object v0, v6

    goto :goto_1
.end method
