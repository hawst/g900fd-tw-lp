.class public final Lcla;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public baF:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private baG:J

.field public volatile baH:Z

.field private final baI:Lchm;

.field private final mActionConverter:Lidh;

.field private final mAssistContextHelper:Lckx;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mClock:Lemp;

.field public final mDiscourseContextSupplier:Ligi;

.field public final mGsaConfigFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field public final mRecentContextApiClient:Lclh;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Ligi;Lemp;Landroid/content/res/Resources;Lckx;Lidh;Landroid/content/Context;Lclh;Landroid/media/AudioManager;Lcrh;Lchk;)V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcla;->baG:J

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcla;->baH:Z

    .line 82
    new-instance v0, Lclb;

    invoke-direct {v0, p0}, Lclb;-><init>(Lcla;)V

    iput-object v0, p0, Lcla;->baI:Lchm;

    .line 103
    iput-object p1, p0, Lcla;->mDiscourseContextSupplier:Ligi;

    .line 104
    iput-object p2, p0, Lcla;->mClock:Lemp;

    .line 105
    iput-object p3, p0, Lcla;->mResources:Landroid/content/res/Resources;

    .line 106
    iput-object p4, p0, Lcla;->mAssistContextHelper:Lckx;

    .line 107
    iput-object p5, p0, Lcla;->mActionConverter:Lidh;

    .line 108
    iput-object p7, p0, Lcla;->mRecentContextApiClient:Lclh;

    .line 110
    iput-object p8, p0, Lcla;->mAudioManager:Landroid/media/AudioManager;

    .line 111
    iput-object p9, p0, Lcla;->mLoginHelper:Lcrh;

    .line 112
    iput-object p10, p0, Lcla;->mGsaConfigFlags:Lchk;

    .line 114
    iget-object v0, p0, Lcla;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lcla;->baI:Lchm;

    invoke-virtual {v0, v1}, Lchk;->a(Lchm;)V

    .line 115
    return-void
.end method

.method public static a(Lcld;)Ljjs;
    .locals 4

    .prologue
    .line 442
    new-instance v0, Ljjs;

    invoke-direct {v0}, Ljjs;-><init>()V

    invoke-virtual {p0}, Lcld;->Pu()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljjs;->dx(J)Ljjs;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcky;Lidh;)Ljkt;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 480
    invoke-virtual {p0}, Lcky;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    .line 481
    if-eqz v1, :cond_1

    .line 482
    invoke-virtual {p0}, Lcky;->Pl()Ljkt;

    move-result-object v6

    .line 484
    if-nez v6, :cond_2

    move-object v4, v5

    .line 488
    :goto_0
    if-nez v4, :cond_0

    .line 489
    invoke-virtual {p0}, Lcky;->Pq()I

    move-result v0

    .line 490
    if-eqz v0, :cond_0

    .line 491
    new-instance v2, Ljlf;

    invoke-direct {v2}, Ljlf;-><init>()V

    invoke-virtual {v2, v0}, Ljlf;->qB(I)Ljlf;

    move-result-object v4

    .line 496
    :cond_0
    if-eqz v6, :cond_3

    .line 497
    iget-object v3, v6, Ljkt;->eqn:Ljku;

    .line 505
    :goto_1
    invoke-virtual {p0}, Lcky;->Pp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    if-nez v6, :cond_5

    :goto_2
    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lidh;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljku;Ljlf;Ljlm;)Ljkt;

    move-result-object v5

    .line 512
    :cond_1
    return-object v5

    .line 484
    :cond_2
    sget-object v0, Ljlf;->erl:Ljsm;

    invoke-virtual {v6, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlf;

    move-object v4, v0

    goto :goto_0

    .line 499
    :cond_3
    invoke-virtual {p0}, Lcky;->Pk()Ljava/lang/String;

    move-result-object v0

    .line 500
    if-nez v0, :cond_4

    move-object v3, v5

    goto :goto_1

    :cond_4
    new-instance v2, Ljku;

    invoke-direct {v2}, Ljku;-><init>()V

    invoke-virtual {v2, v0}, Ljku;->ws(Ljava/lang/String;)Ljku;

    move-result-object v3

    goto :goto_1

    .line 505
    :cond_5
    iget-object v5, v6, Ljkt;->eqo:Ljlm;

    goto :goto_2
.end method

.method private a(Ljjq;Lclc;Ljjz;)Z
    .locals 10
    .param p3    # Ljjz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 335
    .line 336
    iget-object v0, p2, Lclc;->baz:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 337
    new-instance v0, Ljjz;

    invoke-direct {v0}, Ljjz;-><init>()V

    iget-object v1, p2, Lclc;->baz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljjz;->wp(Ljava/lang/String;)Ljjz;

    move-result-object v0

    iput-object v0, p1, Ljjq;->epi:Ljjz;

    move v1, v2

    .line 342
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 343
    iget-object v0, p2, Lclc;->baL:Lijj;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 345
    iget-object v0, p0, Lcla;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Kn()Z

    move-result v0

    .line 347
    if-eqz v0, :cond_3

    .line 348
    iget-object v0, p0, Lcla;->baF:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 350
    :try_start_0
    iget-object v0, p0, Lcla;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v5

    .line 351
    iget-object v0, p0, Lcla;->baF:Ljava/util/concurrent/Future;

    const-wide/16 v6, 0x64

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v6, v7, v8}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijj;

    .line 353
    iget-object v6, p0, Lcla;->mAssistContextHelper:Lckx;

    invoke-static {v5, v0}, Lckx;->a(Ljava/lang/String;Ljava/util/List;)Lijj;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 366
    :goto_1
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcla;->mAssistContextHelper:Lckx;

    invoke-static {p3}, Lckx;->a(Ljjz;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 367
    const-class v0, Ljjz;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lipz;->a(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v0

    aput-object p3, v0, v3

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    add-int/lit8 v1, v3, 0x1

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v0, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 354
    :catch_0
    move-exception v0

    .line 356
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_1

    .line 358
    :catch_1
    move-exception v0

    const-string v0, "DiscourseContextHelper"

    const-string v5, "AppDataSearch context fetch interrupted."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v9, v0, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 360
    :catch_2
    move-exception v0

    const-string v0, "DiscourseContextHelper"

    const-string v5, "AppDataSearch context fetch timed out."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v9, v0, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 363
    :cond_0
    const-string v0, "DiscourseContextHelper"

    const-string v5, "AppDataSearch context has not been prefetched."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v9, v0, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 367
    :cond_1
    check-cast v0, [Ljjz;

    iput-object v0, p1, Ljjq;->epd:[Ljjz;

    .line 376
    :goto_3
    return v2

    .line 370
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 371
    const-class v0, Ljjz;

    invoke-static {v4, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljjz;

    iput-object v0, p1, Ljjq;->epd:[Ljjz;

    goto :goto_3

    :cond_3
    move v2, v1

    goto :goto_3

    :cond_4
    move v1, v3

    goto/16 :goto_0
.end method

.method private a(Ljjq;Ljjm;)Z
    .locals 2
    .param p2    # Ljjm;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 385
    if-nez p2, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 389
    :cond_1
    iget-object v1, p0, Lcla;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Kn()Z

    move-result v1

    .line 395
    if-nez v1, :cond_2

    invoke-virtual {p2}, Ljjm;->bnV()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 396
    :cond_2
    iput-object p2, p1, Ljjq;->epj:Ljjm;

    .line 397
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcle;JJ)Ljjr;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 425
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 426
    invoke-interface {p1}, Lcle;->Pw()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcld;

    .line 427
    invoke-virtual {v0}, Lcld;->Pu()J

    move-result-wide v4

    .line 428
    cmp-long v3, v4, p2

    if-ltz v3, :cond_0

    cmp-long v3, v4, p4

    if-ltz v3, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v3, p4, v4

    if-nez v3, :cond_0

    .line 429
    :cond_1
    invoke-static {v0}, Lcla;->a(Lcld;)Ljjs;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 432
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 433
    invoke-interface {p1}, Lcle;->Pv()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcla;->aw(Ljava/lang/Object;)Ljjr;

    move-result-object v1

    .line 434
    const-class v0, Ljjs;

    invoke-static {v2, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljjs;

    iput-object v0, v1, Ljjr;->epl:[Ljjs;

    move-object v0, v1

    .line 437
    :goto_1
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final aw(Ljava/lang/Object;)Ljjr;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 448
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    instance-of v0, p1, Lcom/google/android/search/shared/contact/Person;

    if-eqz v0, :cond_2

    .line 450
    check-cast p1, Lcom/google/android/search/shared/contact/Person;

    .line 451
    new-instance v0, Ljjr;

    invoke-direct {v0}, Ljjr;-><init>()V

    .line 452
    new-array v1, v2, [I

    aput v2, v1, v3

    iput-object v1, v0, Ljjr;->epn:[I

    .line 454
    iget-object v1, p0, Lcla;->mActionConverter:Lidh;

    invoke-virtual {v1}, Lidh;->aUT()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 455
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljjr;->wo(Ljava/lang/String;)Ljjr;

    .line 456
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v1

    .line 457
    if-eqz v1, :cond_0

    .line 458
    invoke-virtual {v0, v1}, Ljjr;->wn(Ljava/lang/String;)Ljjr;

    .line 469
    :cond_0
    :goto_0
    return-object v0

    .line 461
    :cond_1
    iget-object v1, p0, Lcla;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a08d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljjr;->wn(Ljava/lang/String;)Ljjr;

    goto :goto_0

    .line 464
    :cond_2
    instance-of v0, p1, Lbyd;

    if-eqz v0, :cond_3

    .line 466
    new-instance v0, Ljjr;

    invoke-direct {v0}, Ljjr;-><init>()V

    .line 467
    new-array v1, v2, [I

    const/4 v2, 0x5

    aput v2, v1, v3

    iput-object v1, v0, Ljjr;->epn:[I

    .line 468
    iget-object v1, p0, Lcla;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a08d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljjr;->wn(Ljava/lang/String;)Ljjr;

    goto :goto_0

    .line 471
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lgnu;)Ljjq;
    .locals 14
    .param p1    # Lgnu;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcla;->mAssistContextHelper:Lckx;

    invoke-virtual {v0, p1}, Lckx;->a(Lgnu;)Ljjm;

    move-result-object v9

    .line 157
    if-eqz p1, :cond_2

    iget-object v0, p1, Lgnu;->cQy:Ljjz;

    move-object v7, v0

    .line 159
    :goto_0
    iget-wide v2, p0, Lcla;->baG:J

    .line 160
    iget-object v0, p0, Lcla;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 162
    iget-object v0, p0, Lcla;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcky;

    invoke-static {}, Lenu;->auQ()V

    new-instance v8, Ljjq;

    invoke-direct {v8}, Ljjq;-><init>()V

    invoke-virtual {v6}, Lcky;->Pt()Lclc;

    move-result-object v10

    iget-object v0, v6, Lcky;->bav:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v8, v0}, Ljjq;->ip(Z)Ljjq;

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    iget-object v0, v6, Lcky;->baw:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_4

    :cond_0
    const/4 v0, 0x0

    :goto_2
    or-int/2addr v1, v0

    iget-object v0, v6, Lcky;->bax:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_6

    const/4 v0, 0x0

    :goto_3
    or-int v11, v1, v0

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v10, Lclc;->baK:Lijp;

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcle;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcla;->a(Lcle;JJ)Ljjr;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 157
    :cond_2
    const/4 v0, 0x0

    move-object v7, v0

    goto :goto_0

    .line 162
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_4
    iget-object v11, v8, Ljjq;->epf:Ljjt;

    if-nez v11, :cond_5

    new-instance v11, Ljjt;

    invoke-direct {v11}, Ljjt;-><init>()V

    iput-object v11, v8, Ljjq;->epf:Ljjt;

    :cond_5
    iget-object v11, v8, Ljjq;->epf:Ljjt;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/String;

    invoke-interface {v0, v12}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v11, Ljjt;->epq:[Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v11, v8, Ljjq;->epf:Ljjt;

    if-nez v11, :cond_7

    new-instance v11, Ljjt;

    invoke-direct {v11}, Ljjt;-><init>()V

    iput-object v11, v8, Ljjq;->epf:Ljjt;

    :cond_7
    iget-object v11, v8, Ljjq;->epf:Ljjt;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/String;

    invoke-interface {v0, v12}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v11, Ljjt;->epr:[Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_3

    :cond_8
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    const-class v0, Ljjr;

    invoke-static {v12, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljjr;

    iput-object v0, v8, Ljjq;->epa:[Ljjr;

    const/4 v0, 0x1

    :goto_5
    or-int/2addr v0, v11

    invoke-direct {p0, v8, v10, v7}, Lcla;->a(Ljjq;Lclc;Ljjz;)Z

    move-result v1

    or-int/2addr v1, v0

    iget-object v0, p0, Lcla;->mActionConverter:Lidh;

    invoke-static {v6, v0}, Lcla;->a(Lcky;Lidh;)Ljkt;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v2, 0x1

    new-array v2, v2, [Ljkt;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v8, Ljjq;->epb:[Ljkt;

    const/4 v0, 0x1

    :goto_6
    or-int/2addr v1, v0

    invoke-virtual {v6}, Lcky;->Pj()Liwg;

    move-result-object v0

    if-eqz v0, :cond_b

    new-instance v2, Liwe;

    invoke-direct {v2}, Liwe;-><init>()V

    iput-object v2, v8, Ljjq;->epe:Liwe;

    new-instance v2, Liwh;

    invoke-direct {v2}, Liwh;-><init>()V

    iput-object v0, v2, Liwh;->dLQ:Liwg;

    iget-object v0, v8, Ljjq;->epe:Liwe;

    const/4 v3, 0x1

    new-array v3, v3, [Liwh;

    const/4 v6, 0x0

    aput-object v2, v3, v6

    iput-object v3, v0, Liwe;->dLM:[Liwh;

    const/4 v0, 0x1

    :goto_7
    or-int/2addr v0, v1

    invoke-direct {p0, v8, v9}, Lcla;->a(Ljjq;Ljjm;)Z

    move-result v1

    or-int/2addr v1, v0

    iget-object v0, p0, Lcla;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Kp()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcla;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcla;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Ljjq;->iq(Z)Ljjq;

    const/4 v0, 0x1

    :goto_8
    or-int/2addr v0, v1

    :goto_9
    if-eqz v0, :cond_d

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v4

    invoke-virtual {v8, v0, v1}, Ljjq;->dw(J)Ljjq;

    move-object v0, v8

    .line 166
    :goto_a
    iput-wide v4, p0, Lcla;->baG:J

    .line 172
    return-object v0

    .line 162
    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    const/4 v0, 0x0

    goto :goto_6

    :cond_b
    const/4 v0, 0x0

    goto :goto_7

    :cond_c
    const/4 v0, 0x0

    goto :goto_8

    :cond_d
    const/4 v0, 0x0

    goto :goto_a

    :cond_e
    move v0, v1

    goto :goto_9
.end method

.method public final c(Lgnu;)Ljjq;
    .locals 6
    .param p1    # Lgnu;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 177
    invoke-static {}, Lenu;->auQ()V

    .line 178
    iget-object v0, p0, Lcla;->mAssistContextHelper:Lckx;

    invoke-virtual {v0, p1}, Lckx;->a(Lgnu;)Ljjm;

    move-result-object v4

    .line 179
    if-eqz p1, :cond_1

    iget-object v0, p1, Lgnu;->cQy:Ljjz;

    move-object v1, v0

    .line 181
    :goto_0
    new-instance v3, Ljjq;

    invoke-direct {v3}, Ljjq;-><init>()V

    .line 183
    iget-object v0, p0, Lcla;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Pt()Lclc;

    move-result-object v0

    invoke-direct {p0, v3, v0, v1}, Lcla;->a(Ljjq;Lclc;Ljjz;)Z

    move-result v0

    .line 185
    invoke-direct {p0, v3, v4}, Lcla;->a(Ljjq;Ljjm;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 187
    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcla;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ljjq;->dw(J)Ljjq;

    move-object v2, v3

    .line 196
    :cond_0
    return-object v2

    :cond_1
    move-object v1, v2

    .line 179
    goto :goto_0
.end method
