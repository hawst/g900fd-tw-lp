.class final Lblj;
.super Lblu;


# instance fields
.field final synthetic aEL:Lblg;

.field private aEM:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method private constructor <init>(Lblg;)V
    .locals 2

    iput-object p1, p0, Lblj;->aEL:Lblg;

    invoke-direct {p0}, Lblu;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method synthetic constructor <init>(Lblg;B)V
    .locals 0

    invoke-direct {p0, p1}, Lblj;-><init>(Lblg;)V

    return-void
.end method

.method private d(JI)V
    .locals 3

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->h(Lblg;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->h(Lblg;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, p3}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lbhh;->al(Ljava/lang/Object;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private dV(I)Z
    .locals 3

    invoke-static {}, Lblg;->zs()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->i(Lblg;)Lbhh;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->i(Lblg;)Lbhh;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v2}, Lbhh;->al(Ljava/lang/Object;)V

    iget-object v0, p0, Lblj;->aEL:Lblg;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lblg;->b(Lblg;Lbhh;)Lbhh;

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final H(J)V
    .locals 1

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lblj;->d(JI)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0, p1}, Lblg;->a(Lblg;Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/ApplicationMetadata;->xU()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lblg;->a(Lblg;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0, p3}, Lblg;->b(Lblg;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lblg;->zr()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->c(Lblg;)Lbhh;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->c(Lblg;)Lbhh;

    move-result-object v7

    new-instance v0, Lblh;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lblh;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v7, v0}, Lbhh;->al(Ljava/lang/Object;)V

    iget-object v0, p0, Lblj;->aEL:Lblg;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lblg;->a(Lblg;Lbhh;)Lbhh;

    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Lcom/google/android/gms/internal/ig;)V
    .locals 3

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lblg;->zq()Lblv;

    move-result-object v0

    const-string v1, "onApplicationStatusChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->e(Lblg;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lblm;

    invoke-direct {v1, p0, p1}, Lblm;-><init>(Lblj;Lcom/google/android/gms/internal/ig;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/internal/il;)V
    .locals 3

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lblg;->zq()Lblv;

    move-result-object v0

    const-string v1, "onDeviceStatusChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->e(Lblg;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lbll;

    invoke-direct {v1, p0, p1}, Lbll;-><init>(Lblj;Lcom/google/android/gms/internal/il;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 5

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lblg;->zq()Lblv;

    move-result-object v0

    const-string v1, "IGNORING: Receive (type=binary, ns=%s) <%d bytes>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    array-length v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(JI)V
    .locals 1

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lblj;->d(JI)V

    goto :goto_0
.end method

.method public final dQ(I)V
    .locals 5

    invoke-virtual {p0}, Lblj;->zt()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lblg;->zq()Lblv;

    move-result-object v0

    const-string v1, "ICastDeviceControllerListener.onDisconnected: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lblj;->aEL:Lblg;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lblg;->dL(I)V

    goto :goto_0
.end method

.method public final dR(I)V
    .locals 4

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lblg;->zr()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->c(Lblg;)Lbhh;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->c(Lblg;)Lbhh;

    move-result-object v0

    new-instance v2, Lblh;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v3, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v2, v3}, Lblh;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v0, v2}, Lbhh;->al(Ljava/lang/Object;)V

    iget-object v0, p0, Lblj;->aEL:Lblg;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lblg;->a(Lblg;Lbhh;)Lbhh;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final dS(I)V
    .locals 1

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lblj;->dV(I)Z

    goto :goto_0
.end method

.method public final dT(I)V
    .locals 1

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lblj;->dV(I)Z

    goto :goto_0
.end method

.method public final dU(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0, v1}, Lblg;->a(Lblg;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0, v1}, Lblg;->b(Lblg;Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1}, Lblj;->dV(I)Z

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->d(Lblg;)Lbfm;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->e(Lblg;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lblk;

    invoke-direct {v1, p0, p1}, Lblk;-><init>(Lblj;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final s(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lblg;->zq()Lblv;

    move-result-object v0

    const-string v1, "Receive (type=text, ns=%s) %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lblj;->aEL:Lblg;

    invoke-static {v0}, Lblg;->e(Lblg;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lbln;

    invoke-direct {v1, p0, p1, p2}, Lbln;-><init>(Lblj;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final zt()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lblj;->aEL:Lblg;

    invoke-static {v1}, Lblg;->b(Lblg;)V

    goto :goto_0
.end method

.method public final zu()Z
    .locals 1

    iget-object v0, p0, Lblj;->aEM:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final zv()V
    .locals 3

    invoke-static {}, Lblg;->zq()Lblv;

    move-result-object v0

    const-string v1, "Deprecated callback: \"onStatusreceived\""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lblv;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
