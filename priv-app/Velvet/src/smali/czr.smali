.class public final Lczr;
.super Ljsl;
.source "PG"


# instance fields
.field public acp:Ljava/lang/String;

.field private aez:I

.field public boo:Ljava/lang/String;

.field public bop:Ljava/lang/String;

.field public boq:Ljava/lang/String;

.field public bor:J

.field private bos:Ljava/lang/String;

.field private bot:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 82
    iput v2, p0, Lczr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lczr;->boo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lczr;->bop:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lczr;->boq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lczr;->acp:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lczr;->bor:J

    const-string v0, ""

    iput-object v0, p0, Lczr;->bos:Ljava/lang/String;

    iput v2, p0, Lczr;->bot:I

    const/4 v0, 0x0

    iput-object v0, p0, Lczr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lczr;->eCz:I

    .line 83
    return-void
.end method

.method public static B([B)Lczr;
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lczr;

    invoke-direct {v0}, Lczr;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lczr;

    return-object v0
.end method


# virtual methods
.method public final Un()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lczr;->bos:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lczr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczr;->boo:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczr;->bop:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczr;->boq:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczr;->acp:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczr;->bos:Ljava/lang/String;

    iget v0, p0, Lczr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lczr;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lczr;->bor:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lczr;->bot:I

    iget v0, p0, Lczr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lczr;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 102
    const/4 v0, 0x1

    iget-object v1, p0, Lczr;->boo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 103
    const/4 v0, 0x2

    iget-object v1, p0, Lczr;->bop:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 104
    const/4 v0, 0x3

    iget-object v1, p0, Lczr;->boq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 105
    const/4 v0, 0x4

    iget-object v1, p0, Lczr;->acp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 106
    iget v0, p0, Lczr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x5

    iget-object v1, p0, Lczr;->bos:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 109
    :cond_0
    const/4 v0, 0x6

    iget-wide v2, p0, Lczr;->bor:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 110
    iget v0, p0, Lczr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x7

    iget v1, p0, Lczr;->bot:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 113
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 114
    return-void
.end method

.method public final jl(Ljava/lang/String;)Lczr;
    .locals 1

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49
    :cond_0
    iput-object p1, p0, Lczr;->bos:Ljava/lang/String;

    .line 50
    iget v0, p0, Lczr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lczr;->aez:I

    .line 51
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 118
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 119
    const/4 v1, 0x1

    iget-object v2, p0, Lczr;->boo:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    const/4 v1, 0x2

    iget-object v2, p0, Lczr;->bop:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    const/4 v1, 0x3

    iget-object v2, p0, Lczr;->boq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    const/4 v1, 0x4

    iget-object v2, p0, Lczr;->acp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    iget v1, p0, Lczr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 128
    const/4 v1, 0x5

    iget-object v2, p0, Lczr;->bos:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_0
    const/4 v1, 0x6

    iget-wide v2, p0, Lczr;->bor:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    iget v1, p0, Lczr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 134
    const/4 v1, 0x7

    iget v2, p0, Lczr;->bot:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_1
    return v0
.end method
