.class public final Lckq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcks;


# instance fields
.field private final bac:Ljava/lang/Runnable;

.field private final bad:Ljava/util/Map;

.field private bae:Z

.field private final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lcjs;Lcke;Lemp;Ljava/util/concurrent/Executor;)V
    .locals 10

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lckq;->mConfig:Lcjs;

    .line 62
    iput-object p2, p0, Lckq;->mSettings:Lcke;

    .line 63
    iput-object p3, p0, Lckq;->mClock:Lemp;

    .line 64
    iput-object p4, p0, Lckq;->mExecutor:Ljava/util/concurrent/Executor;

    .line 66
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lckq;->bad:Ljava/util/Map;

    .line 68
    new-instance v0, Lckr;

    const-string v1, "flush settings"

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {v0, p0, v1, v2}, Lckr;-><init>(Lckq;Ljava/lang/String;[I)V

    iput-object v0, p0, Lckq;->bac:Ljava/lang/Runnable;

    .line 76
    iget-object v0, p0, Lckq;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NN()Ljava/util/Map;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 78
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/String;

    .line 79
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 80
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v9, p0, Lckq;->bad:Ljava/util/Map;

    new-instance v0, Lent;

    iget-object v1, p0, Lckq;->mClock:Lemp;

    iget-object v2, p0, Lckq;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Lk()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb800

    invoke-direct/range {v0 .. v6}, Lent;-><init>(Lemp;JJLjava/lang/String;)V

    invoke-interface {v9, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 86
    :cond_1
    return-void
.end method

.method private OV()V
    .locals 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lckq;->bae:Z

    if-nez v0, :cond_0

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lckq;->bae:Z

    .line 152
    iget-object v0, p0, Lckq;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lckq;->bac:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 154
    :cond_0
    return-void
.end method

.method private declared-synchronized j(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lckq;->bad:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lent;

    .line 140
    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lent;

    iget-object v1, p0, Lckq;->mClock:Lemp;

    iget-object v2, p0, Lckq;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Lk()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb800

    invoke-direct/range {v0 .. v5}, Lent;-><init>(Lemp;JJ)V

    .line 143
    iget-object v1, p0, Lckq;->bad:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    :cond_0
    invoke-virtual {v0, p2, p3}, Lent;->aU(J)V

    .line 146
    invoke-direct {p0}, Lckq;->OV()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit p0

    return-void

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized OU()Ljava/util/Map;
    .locals 5

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 119
    iget-object v0, p0, Lckq;->bad:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lent;

    invoke-virtual {v1}, Lent;->auK()I

    move-result v1

    .line 122
    iget-object v4, p0, Lckq;->mConfig:Lcjs;

    invoke-virtual {v4}, Lcjs;->Ll()I

    move-result v4

    if-lt v1, v4, :cond_0

    .line 123
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 126
    :cond_1
    monitor-exit p0

    return-object v2
.end method

.method final declared-synchronized OW()V
    .locals 4

    .prologue
    .line 158
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lckq;->bae:Z

    .line 159
    iget-object v0, p0, Lckq;->bad:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 160
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lent;

    invoke-virtual {v1}, Lent;->auP()Ljava/lang/String;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_0

    .line 163
    iget-object v3, p0, Lckq;->mSettings:Lcke;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Lcke;->J(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 166
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;)V
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arW()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arW()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lckq;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lckq;->j(Ljava/lang/String;J)V

    .line 135
    :cond_0
    return-void
.end method

.method public final declared-synchronized e(Ljava/util/Map;)V
    .locals 12

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lckq;->mSettings:Lcke;

    invoke-interface {v2}, Lcke;->NM()Ljava/util/Map;

    move-result-object v2

    .line 97
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 98
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 100
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v10, v0

    .line 101
    if-eqz v10, :cond_0

    .line 103
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 105
    new-instance v2, Lent;

    iget-object v3, p0, Lckq;->mClock:Lemp;

    iget-object v4, p0, Lckq;->mConfig:Lcjs;

    invoke-virtual {v4}, Lcjs;->Lk()J

    move-result-wide v4

    const-wide/32 v6, 0xa4cb800

    const/4 v9, 0x1

    invoke-direct/range {v2 .. v9}, Lent;-><init>(Lemp;JJLjava/lang/String;Z)V

    .line 109
    iget-object v3, p0, Lckq;->bad:Ljava/util/Map;

    invoke-interface {v3, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 112
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lckq;->OV()V

    .line 113
    iget-object v2, p0, Lckq;->mSettings:Lcke;

    invoke-interface {v2}, Lcke;->NP()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    monitor-exit p0

    return-void
.end method
