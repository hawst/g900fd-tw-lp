.class public final enum Lgyt;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cZA:Lgyt;

.field public static final enum cZB:Lgyt;

.field public static final enum cZC:Lgyt;

.field public static final enum cZD:Lgyt;

.field public static final enum cZE:Lgyt;

.field public static final enum cZF:Lgyt;

.field public static final enum cZG:Lgyt;

.field public static final enum cZH:Lgyt;

.field public static final enum cZI:Lgyt;

.field public static final enum cZJ:Lgyt;

.field public static final enum cZK:Lgyt;

.field public static final enum cZL:Lgyt;

.field public static final enum cZM:Lgyt;

.field private static final synthetic cZN:[Lgyt;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lgyt;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZA:Lgyt;

    .line 17
    new-instance v0, Lgyt;

    const-string v1, "PREDICTIVE"

    invoke-direct {v0, v1, v4}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZB:Lgyt;

    .line 20
    new-instance v0, Lgyt;

    const-string v1, "VOICESEARCH"

    invoke-direct {v0, v1, v5}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZC:Lgyt;

    .line 27
    new-instance v0, Lgyt;

    const-string v1, "SUGGEST"

    invoke-direct {v0, v1, v6}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZD:Lgyt;

    .line 30
    new-instance v0, Lgyt;

    const-string v1, "RESULTS"

    invoke-direct {v0, v1, v7}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZE:Lgyt;

    .line 37
    new-instance v0, Lgyt;

    const-string v1, "RESULTS_SUGGEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZF:Lgyt;

    .line 40
    new-instance v0, Lgyt;

    const-string v1, "RESULTS_ACTIONS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZG:Lgyt;

    .line 43
    new-instance v0, Lgyt;

    const-string v1, "RESULTS_VOICESEARCH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZH:Lgyt;

    .line 46
    new-instance v0, Lgyt;

    const-string v1, "SUMMONS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZI:Lgyt;

    .line 49
    new-instance v0, Lgyt;

    const-string v1, "CONNECTION_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZJ:Lgyt;

    .line 56
    new-instance v0, Lgyt;

    const-string v1, "SUMMONS_SUGGEST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZK:Lgyt;

    .line 59
    new-instance v0, Lgyt;

    const-string v1, "SOUND_SEARCH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZL:Lgyt;

    .line 66
    new-instance v0, Lgyt;

    const-string v1, "VOICE_CORRECTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lgyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgyt;->cZM:Lgyt;

    .line 9
    const/16 v0, 0xd

    new-array v0, v0, [Lgyt;

    sget-object v1, Lgyt;->cZA:Lgyt;

    aput-object v1, v0, v3

    sget-object v1, Lgyt;->cZB:Lgyt;

    aput-object v1, v0, v4

    sget-object v1, Lgyt;->cZC:Lgyt;

    aput-object v1, v0, v5

    sget-object v1, Lgyt;->cZD:Lgyt;

    aput-object v1, v0, v6

    sget-object v1, Lgyt;->cZE:Lgyt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgyt;->cZF:Lgyt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgyt;->cZG:Lgyt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgyt;->cZH:Lgyt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgyt;->cZI:Lgyt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgyt;->cZJ:Lgyt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lgyt;->cZK:Lgyt;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lgyt;->cZL:Lgyt;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lgyt;->cZM:Lgyt;

    aput-object v2, v0, v1

    sput-object v0, Lgyt;->cZN:[Lgyt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 120
    return-void
.end method

.method public static aY(Lcom/google/android/shared/search/Query;)Lgyt;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqI()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lgyt;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lgyt;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lgyt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgyt;

    return-object v0
.end method

.method public static values()[Lgyt;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lgyt;->cZN:[Lgyt;

    invoke-virtual {v0}, [Lgyt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgyt;

    return-object v0
.end method


# virtual methods
.method public final aLI()Z
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lgyt;->cZB:Lgyt;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLJ()Z
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lgyt;->cZD:Lgyt;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgyt;->cZK:Lgyt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLK()Z
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lgyt;->cZE:Lgyt;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgyt;->cZF:Lgyt;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgyt;->cZM:Lgyt;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgyt;->cZG:Lgyt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLL()Z
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lgyt;->cZD:Lgyt;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgyt;->cZK:Lgyt;

    if-eq p0, v0, :cond_0

    sget-object v0, Lgyt;->cZI:Lgyt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLM()Z
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lgyt;->aLJ()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lgyt;->cZF:Lgyt;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLN()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lgyu;->cZO:[I

    invoke-virtual {p0}, Lgyt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 120
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 106
    :pswitch_0
    const-string v0, "suggest"

    goto :goto_0

    .line 112
    :pswitch_1
    const-string v0, "results"

    goto :goto_0

    .line 114
    :pswitch_2
    const-string v0, "summons"

    goto :goto_0

    .line 116
    :pswitch_3
    const-string v0, "error"

    goto :goto_0

    .line 118
    :pswitch_4
    const-string v0, "voicesearch"

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final aLO()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 130
    sget-object v0, Lgyu;->cZO:[I

    invoke-virtual {p0}, Lgyt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 142
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 132
    :pswitch_1
    const-string v0, "suggest"

    goto :goto_0

    .line 135
    :pswitch_2
    const-string v0, "voicesearchlang"

    goto :goto_0

    .line 137
    :pswitch_3
    const-string v0, "voicecorrection"

    goto :goto_0

    .line 139
    :pswitch_4
    const-string v0, "results"

    goto :goto_0

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
