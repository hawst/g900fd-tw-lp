.class final Lflg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final cuK:Lfry;

.field private final cuL:I

.field final cuM:Landroid/view/View;

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfry;ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput-object p1, p0, Lflg;->mContext:Landroid/content/Context;

    .line 284
    iput-object p2, p0, Lflg;->cuK:Lfry;

    .line 285
    iput p3, p0, Lflg;->cuL:I

    .line 286
    iput-object p4, p0, Lflg;->cuM:Landroid/view/View;

    .line 287
    return-void
.end method


# virtual methods
.method final aAJ()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 331
    iget v0, p0, Lflg;->cuL:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 332
    iget-object v0, p0, Lflg;->cuK:Lfry;

    iget v1, v0, Lfry;->cBt:I

    if-ltz v1, :cond_0

    iget-object v1, v0, Lfry;->mCardContainer:Lfmt;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lfry;->mEntry:Lizj;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lfry;->mCardContainer:Lfmt;

    iget-object v2, v0, Lfry;->mEntry:Lizj;

    iget v3, v0, Lfry;->cBt:I

    invoke-interface {v1, v2, v3, v4, v4}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    :cond_0
    iget-object v1, v0, Lfry;->cBr:Lfrz;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lfry;->cBr:Lfrz;

    invoke-interface {v0}, Lfrz;->onClick()V

    .line 336
    :cond_1
    :goto_0
    return-void

    .line 334
    :cond_2
    iget-object v0, p0, Lflg;->cuK:Lfry;

    iget v1, v0, Lfry;->cBu:I

    if-ltz v1, :cond_3

    iget-object v1, v0, Lfry;->mCardContainer:Lfmt;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lfry;->mEntry:Lizj;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lfry;->mCardContainer:Lfmt;

    iget-object v2, v0, Lfry;->mEntry:Lizj;

    iget v3, v0, Lfry;->cBu:I

    invoke-interface {v1, v2, v3, v4, v4}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    :cond_3
    iget-object v1, v0, Lfry;->cBs:Lfrz;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lfry;->cBs:Lfrz;

    invoke-interface {v0}, Lfrz;->onClick()V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 291
    iget-object v0, p0, Lflg;->cuK:Lfry;

    iget-boolean v0, v0, Lfry;->cBv:Z

    if-eqz v0, :cond_0

    .line 292
    new-instance v1, Lflh;

    invoke-direct {v1, p0}, Lflh;-><init>(Lflg;)V

    iget-object v0, p0, Lflg;->cuM:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    sget-object v2, Lekn;->cdS:Lekn;

    iput-object v2, v0, Lekm;->cdA:Lekn;

    iget-object v2, p0, Lflg;->cuM:Landroid/view/View;

    const v3, 0x7f11005e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-static {v2}, Leot;->bc(Landroid/view/View;)I

    move-result v2

    add-int/2addr v2, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v0, Lekm;->cdD:I

    iget-object v0, p0, Lflg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v2, Lfli;

    invoke-direct {v2, p0}, Lfli;-><init>(Lflg;)V

    invoke-static {v0, v2}, Lelv;->a(Landroid/content/res/Resources;Ljava/lang/Runnable;)V

    iget-object v0, p0, Lflg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c006a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    add-int/lit16 v0, v0, 0x190

    add-int/lit16 v0, v0, 0x12c

    int-to-long v4, v0

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_0
    invoke-virtual {p0}, Lflg;->aAJ()V

    goto :goto_0
.end method
