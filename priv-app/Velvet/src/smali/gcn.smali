.class public final Lgcn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lggg;


# instance fields
.field private final cFS:Lglx;

.field private final cFT:Lgda;

.field private final cFU:Ljava/util/List;

.field private cFV:Leiq;

.field private final mAudioCallback:Lgcd;

.field private final mRecognitionDispatcher:Lgil;


# direct methods
.method public constructor <init>(Lgcd;Lgil;Lglx;Lgda;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lgcn;->mAudioCallback:Lgcd;

    .line 46
    iput-object p2, p0, Lgcn;->mRecognitionDispatcher:Lgil;

    .line 47
    iput-object p3, p0, Lgcn;->cFS:Lglx;

    .line 48
    iput-object p4, p0, Lgcn;->cFT:Lgda;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgcn;->cFU:Ljava/util/List;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Lehu;)V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public final a(Leiq;)V
    .locals 3

    .prologue
    .line 105
    instance-of v0, p1, Leij;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lgcn;->cFT:Lgda;

    instance-of v0, v0, Lgcu;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lgcn;->cFT:Lgda;

    check-cast v0, Lgcu;

    invoke-virtual {p1}, Leiq;->ata()I

    move-result v1

    invoke-virtual {v0, v1}, Lgcu;->ke(I)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    instance-of v0, p1, Leia;

    if-eqz v0, :cond_2

    .line 116
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lgcn;->a(Leiq;Z)V

    goto :goto_0

    .line 118
    :cond_2
    const-string v0, "ErrorProcessor"

    const-string v1, "onNonFatalError"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 119
    invoke-static {p1}, Lege;->a(Lefr;)V

    goto :goto_0
.end method

.method public a(Leiq;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    invoke-virtual {p1}, Leiq;->ata()I

    move-result v1

    .line 66
    invoke-static {p1}, Lege;->a(Lefr;)V

    .line 68
    if-eqz p2, :cond_1

    .line 69
    iget-object v0, p0, Lgcn;->cFT:Lgda;

    invoke-interface {v0, v1}, Lgda;->ka(I)I

    move-result v0

    .line 76
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 101
    :cond_0
    :goto_1
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lgcn;->mRecognitionDispatcher:Lgil;

    invoke-virtual {v0, v1}, Lgil;->km(I)V

    .line 73
    iget-object v0, p0, Lgcn;->cFT:Lgda;

    invoke-interface {v0, v1}, Lgda;->kb(I)I

    move-result v0

    goto :goto_0

    .line 78
    :pswitch_0
    iget-object v0, p0, Lgcn;->cFV:Leiq;

    if-eqz v0, :cond_2

    .line 79
    iget-object p1, p0, Lgcn;->cFV:Leiq;

    .line 81
    :cond_2
    const-string v0, "ErrorProcessor"

    const-string v1, "onFatalError, processing error from engine(%d)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Leiq;->ata()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, p1, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lgcn;->mRecognitionDispatcher:Lgil;

    invoke-virtual {v0}, Lgil;->cancel()V

    .line 87
    iget-object v0, p0, Lgcn;->mAudioCallback:Lgcd;

    invoke-interface {v0}, Lgcd;->aEJ()V

    .line 88
    iget-object v0, p0, Lgcn;->cFS:Lglx;

    invoke-interface {v0, p1}, Lglx;->c(Leiq;)V

    goto :goto_1

    .line 91
    :pswitch_1
    const-string v0, "ErrorProcessor"

    const-string v2, "onFatalError, primary engine(%d) error, fallback to secondary"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v0, p1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 93
    iput-object p1, p0, Lgcn;->cFV:Leiq;

    .line 94
    iget-object v0, p0, Lgcn;->cFU:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgco;

    .line 95
    invoke-interface {v0}, Lgco;->aES()V

    goto :goto_2

    .line 99
    :pswitch_2
    const-string v0, "ErrorProcessor"

    const-string v2, "onFatalError, ignoring error from engine(%d): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    aput-object p1, v3, v5

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Leiq;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lgcn;->a(Leiq;Z)V

    .line 55
    return-void
.end method

.method public final b(Lgco;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lgcn;->cFU:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method
