.class public final Ljts;
.super Ljsl;
.source "PG"


# instance fields
.field private eEd:[[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1139
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1140
    sget-object v0, Ljsu;->eCF:[[B

    iput-object v0, p0, Ljts;->eEd:[[B

    const/4 v0, 0x0

    iput-object v0, p0, Ljts;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljts;->eCz:I

    .line 1141
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1119
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljts;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljts;->eEd:[[B

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljts;->eEd:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljts;->eEd:[[B

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljts;->eEd:[[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 1153
    iget-object v0, p0, Ljts;->eEd:[[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljts;->eEd:[[B

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1154
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljts;->eEd:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1155
    iget-object v1, p0, Ljts;->eEd:[[B

    aget-object v1, v1, v0

    .line 1156
    if-eqz v1, :cond_0

    .line 1157
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->c(I[B)V

    .line 1154
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1161
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1162
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1166
    invoke-super {p0}, Ljsl;->lF()I

    move-result v3

    .line 1167
    iget-object v1, p0, Ljts;->eEd:[[B

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljts;->eEd:[[B

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v0

    move v2, v0

    .line 1170
    :goto_0
    iget-object v4, p0, Ljts;->eEd:[[B

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 1171
    iget-object v4, p0, Ljts;->eEd:[[B

    aget-object v4, v4, v0

    .line 1172
    if-eqz v4, :cond_0

    .line 1173
    add-int/lit8 v2, v2, 0x1

    .line 1174
    invoke-static {v4}, Ljsj;->au([B)I

    move-result v4

    add-int/2addr v1, v4

    .line 1170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1178
    :cond_1
    add-int v0, v3, v1

    .line 1179
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 1181
    :goto_1
    return v0

    :cond_2
    move v0, v3

    goto :goto_1
.end method
