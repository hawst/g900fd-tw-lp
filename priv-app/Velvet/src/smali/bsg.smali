.class public final Lbsg;
.super Ljava/lang/Object;


# instance fields
.field public final aIt:Ljava/lang/String;

.field public final aIu:J

.field public aIx:Ljava/lang/String;

.field public aIy:J

.field public aIz:J

.field public final auO:Landroid/accounts/Account;


# direct methods
.method private constructor <init>(Landroid/accounts/Account;Ljava/lang/String;J)V
    .locals 3

    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lbsg;->aIy:J

    iput-wide v0, p0, Lbsg;->aIz:J

    const-string v0, "account"

    invoke-static {p1, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lbsg;->auO:Landroid/accounts/Account;

    const-string v0, "reason"

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbsg;->aIt:Ljava/lang/String;

    iput-wide p3, p0, Lbsg;->aIu:J

    return-void
.end method

.method public synthetic constructor <init>(Landroid/accounts/Account;Ljava/lang/String;JB)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lbsg;-><init>(Landroid/accounts/Account;Ljava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public final AE()Lcom/google/android/gms/location/reporting/UploadRequest;
    .locals 2

    new-instance v0, Lcom/google/android/gms/location/reporting/UploadRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/location/reporting/UploadRequest;-><init>(Lbsg;B)V

    return-object v0
.end method

.method public final Q(J)Lbsg;
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbsg;->aIy:J

    iput-wide v0, p0, Lbsg;->aIz:J

    return-object p0
.end method
