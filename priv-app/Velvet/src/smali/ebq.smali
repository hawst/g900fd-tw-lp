.class final Lebq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldyl;


# instance fields
.field private synthetic bUB:Lebm;


# direct methods
.method constructor <init>(Lebm;)V
    .locals 0

    .prologue
    .line 801
    iput-object p1, p0, Lebq;->bUB:Lebm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final UK()V
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dW(Z)V

    .line 895
    return-void
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 863
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    .line 864
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUs:Lebj;

    if-eqz v0, :cond_0

    .line 865
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUs:Lebj;

    invoke-interface {v0}, Lebj;->Ci()V

    .line 868
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    .line 869
    :goto_0
    iget-object v3, p0, Lebq;->bUB:Lebm;

    invoke-virtual {v3}, Lebm;->amL()V

    .line 871
    iget-object v3, p0, Lebq;->bUB:Lebm;

    iget-object v3, v3, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/search/searchplate/SearchPlate;->a(ILjava/lang/String;Z)V

    .line 873
    iget-object v3, p0, Lebq;->bUB:Lebm;

    iget-object v3, v3, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-eqz v3, :cond_1

    .line 874
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_3

    .line 875
    :goto_1
    iget-object v2, p0, Lebq;->bUB:Lebm;

    iget-object v2, v2, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->q(ZZ)V

    .line 877
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 868
    goto :goto_0

    :cond_3
    move v1, v2

    .line 874
    goto :goto_1
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 2

    .prologue
    .line 881
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 882
    iget-object v1, p0, Lebq;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiT()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 884
    iget-object v1, p0, Lebq;->bUB:Lebm;

    iget-object v1, v1, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/SearchPlate;->kg(Ljava/lang/String;)V

    .line 885
    return-void
.end method

.method public final c(IIZ)V
    .locals 2

    .prologue
    .line 824
    if-eqz p1, :cond_0

    .line 826
    iget-object v0, p0, Lebq;->bUB:Lebm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lebm;->eG(Z)V

    .line 828
    :cond_0
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget v0, v0, Lebm;->bUx:I

    if-ne v0, p1, :cond_1

    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    .line 846
    :goto_0
    return-void

    .line 832
    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-boolean v0, v0, Lebm;->bUz:Z

    if-eqz v0, :cond_2

    .line 833
    or-int/lit8 p2, p2, 0x4

    .line 839
    :cond_2
    if-nez p1, :cond_3

    .line 840
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTW:Lecc;

    const/high16 v1, 0x60000

    invoke-virtual {v0, v1}, Lecc;->setDescendantFocusability(I)V

    .line 845
    :goto_1
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/searchplate/SearchPlate;->c(IIZ)V

    goto :goto_0

    .line 842
    :cond_3
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTW:Lecc;

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Lecc;->setDescendantFocusability(I)V

    goto :goto_1
.end method

.method public final en(I)V
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->en(I)V

    .line 806
    return-void
.end method

.method public final fv(I)V
    .locals 1

    .prologue
    .line 889
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {}, Lcom/google/android/search/searchplate/SearchPlate;->afj()V

    .line 890
    return-void
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 817
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->n(Ljava/lang/CharSequence;)V

    .line 818
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 811
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/searchplate/SearchPlate;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    return-void
.end method

.method public final z(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 850
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-boolean v0, v0, Lebm;->bUq:Z

    if-eqz v0, :cond_0

    .line 851
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object p1

    .line 853
    :cond_0
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iput-object p1, v0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 854
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0, p1}, Lecq;->x(Lcom/google/android/shared/search/Query;)V

    .line 855
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, p0, Lebq;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 856
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_1

    .line 857
    iget-object v0, p0, Lebq;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    iget-object v1, p0, Lebq;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    .line 859
    :cond_1
    return-void
.end method
