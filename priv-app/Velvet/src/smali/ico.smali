.class public final Lico;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 15
    .param p0    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lciy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lghy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljoq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p8    # Ligi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p9    # Lifw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 281
    const/4 v4, 0x0

    .line 282
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->aJW()Leai;

    move-result-object v8

    .line 284
    invoke-virtual/range {p7 .. p7}, Ljoq;->getName()Ljava/lang/String;

    move-result-object v11

    .line 285
    move-object/from16 v0, p7

    iget-object v10, v0, Ljoq;->ewM:Ljop;

    .line 287
    invoke-virtual/range {p7 .. p7}, Ljoq;->brw()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p7 .. p7}, Ljoq;->brx()I

    move-result v2

    if-eqz v2, :cond_2

    .line 289
    :cond_0
    invoke-virtual/range {p7 .. p7}, Ljoq;->brz()Ljava/lang/String;

    move-result-object v2

    .line 290
    invoke-virtual/range {p7 .. p7}, Ljoq;->bry()Ljava/lang/String;

    move-result-object v3

    .line 291
    invoke-interface {v8, v3}, Leai;->kH(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 294
    invoke-interface {v8, v2}, Leai;->kH(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object v2, v3

    .line 300
    :cond_1
    new-instance v4, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v4, v2, v3}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    :goto_0
    move-object v12, v2

    .line 314
    :goto_1
    invoke-virtual/range {p7 .. p7}, Ljoq;->bru()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 319
    invoke-interface/range {p8 .. p8}, Ligi;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcky;

    invoke-virtual {v2}, Lcky;->Pn()Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v2

    .line 321
    if-eqz v2, :cond_3

    .line 322
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v3

    .line 323
    if-eqz v3, :cond_3

    .line 324
    new-instance v2, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v2, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 485
    :goto_2
    return-object v2

    .line 302
    :cond_2
    invoke-virtual/range {p4 .. p4}, Lchk;->FW()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v8, v11}, Leai;->kH(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 306
    const v2, 0xe99f3f

    invoke-static {v2}, Lhwt;->lx(I)V

    .line 307
    invoke-interface {v8, v11}, Leai;->kJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 310
    new-instance v4, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v4, v11, v2}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v12, v4

    goto :goto_1

    .line 327
    :cond_3
    invoke-interface/range {p8 .. p8}, Ligi;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcky;

    invoke-virtual {v2}, Lcky;->Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v2

    .line 329
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 330
    new-instance v3, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v3, v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    move-object v2, v3

    goto :goto_2

    .line 333
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 334
    :cond_5
    invoke-static/range {p7 .. p7}, Lico;->a(Ljoq;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 337
    move-object/from16 v0, p7

    iget-object v2, v0, Ljoq;->ewL:[Ljoj;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Ljoj;->ews:[Ljon;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljon;->getValue()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p6

    invoke-static {v2, v0, v3}, Lico;->a(Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v2

    goto :goto_2

    .line 349
    :cond_6
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 350
    move-object/from16 v0, p7

    iget-object v4, v0, Ljoq;->ewL:[Ljoj;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v5, :cond_8

    aget-object v6, v4, v2

    .line 351
    move-object/from16 v0, p1

    invoke-static {v0, v11, v6}, Lcom/google/android/search/shared/contact/Person;->a(Ldyv;Ljava/lang/String;Ljoj;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v6

    .line 353
    if-eqz v6, :cond_7

    .line 354
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 358
    :cond_8
    const/4 v2, 0x5

    new-array v2, v2, [Ldzl;

    const/4 v4, 0x0

    new-instance v5, Ldzv;

    invoke-direct {v5}, Ldzv;-><init>()V

    aput-object v5, v2, v4

    const/4 v4, 0x1

    new-instance v5, Ldzs;

    invoke-direct {v5}, Ldzs;-><init>()V

    aput-object v5, v2, v4

    const/4 v4, 0x2

    new-instance v5, Ldzu;

    invoke-direct {v5}, Ldzu;-><init>()V

    aput-object v5, v2, v4

    const/4 v4, 0x3

    new-instance v5, Ldzt;

    invoke-direct {v5}, Ldzt;-><init>()V

    aput-object v5, v2, v4

    const/4 v4, 0x4

    invoke-static/range {p6 .. p6}, Ldzl;->e(Ldzb;)Ldzl;

    move-result-object v5

    aput-object v5, v2, v4

    .line 380
    invoke-static {v3, v2}, Lcom/google/android/search/shared/contact/Person;->a(Ljava/util/List;[Ldzl;)Ljava/util/List;

    move-result-object v6

    .line 381
    if-eqz v10, :cond_a

    iget-object v2, v10, Ljop;->ewI:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 391
    if-eqz p10, :cond_9

    .line 392
    if-eqz v10, :cond_9

    if-eqz v12, :cond_9

    invoke-virtual {v12}, Lcom/google/android/search/shared/contact/Relationship;->amr()Z

    move-result v2

    if-nez v2, :cond_13

    .line 396
    :cond_9
    :goto_4
    new-instance v2, Lggr;

    move-object v3, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object/from16 v7, p2

    move-object/from16 v9, p5

    invoke-direct/range {v2 .. v12}, Lggr;-><init>(Lcom/google/android/shared/search/Query;Lchk;Ldzb;Ljava/util/List;Lcjg;Leai;Lghy;Ljop;Ljava/lang/String;Lcom/google/android/search/shared/contact/Relationship;)V

    .line 404
    move-object/from16 v0, p9

    iput-object v0, v2, Lggr;->cJL:Lifw;

    .line 405
    invoke-virtual {v2}, Lggr;->aFL()Ljava/util/List;

    move-result-object v6

    .line 414
    :cond_a
    new-instance v4, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    const/4 v2, 0x0

    move-object/from16 v0, p6

    invoke-direct {v4, v11, v6, v2, v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Ljava/lang/String;Ljava/util/List;ZLdzb;)V

    .line 417
    invoke-interface/range {p8 .. p8}, Ligi;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcky;

    invoke-virtual {v2}, Lcky;->Pn()Lcom/google/android/search/shared/actions/CommunicationAction;

    move-result-object v3

    if-eqz v3, :cond_15

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v2

    move-object v7, v2

    .line 419
    :goto_5
    if-eqz v7, :cond_b

    if-nez v12, :cond_b

    .line 433
    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Lcom/google/android/search/shared/contact/RelationshipStatus;)V

    .line 437
    :cond_b
    if-eqz v12, :cond_e

    .line 438
    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/android/search/shared/contact/RelationshipStatus;->f(Lcom/google/android/search/shared/contact/Relationship;)V

    .line 442
    const/4 v3, 0x0

    .line 443
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/Person;

    .line 444
    invoke-virtual {v2, v12}, Lcom/google/android/search/shared/contact/Person;->b(Lcom/google/android/search/shared/contact/Relationship;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 445
    const/4 v2, 0x1

    .line 450
    :goto_6
    if-nez v2, :cond_e

    .line 451
    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amu()V

    .line 454
    if-eqz v7, :cond_d

    .line 455
    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alv()Z

    move-result v2

    invoke-virtual {v4, v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ew(Z)V

    .line 463
    :cond_d
    invoke-virtual/range {p4 .. p4}, Lchk;->FX()Z

    move-result v2

    if-eqz v2, :cond_e

    if-nez v7, :cond_e

    .line 464
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ew(Z)V

    .line 469
    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alw()V

    .line 477
    :cond_e
    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->akB()Ldzb;

    move-result-object v2

    if-nez v2, :cond_1b

    sget-object v2, Ldzb;->bRt:Ldzb;

    move-object v5, v2

    :goto_7
    if-eqz v7, :cond_17

    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amg()Lcom/google/android/search/shared/contact/PersonShortcutKey;

    move-result-object v2

    if-eqz v2, :cond_17

    invoke-virtual {v7}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amg()Lcom/google/android/search/shared/contact/PersonShortcutKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->KK()Ljava/lang/String;

    move-result-object v2

    :goto_8
    const/4 v3, 0x0

    move-object/from16 v0, p7

    iget-object v6, v0, Ljoq;->ewM:Ljop;

    if-eqz v6, :cond_f

    iget-object v7, v6, Ljop;->ewy:Ljor;

    if-eqz v7, :cond_f

    iget-object v3, v6, Ljop;->ewy:Ljor;

    iget-object v3, v6, Ljop;->ewy:Ljor;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ldyv;->a(Ljor;)Ljava/lang/String;

    move-result-object v3

    :cond_f
    new-instance v6, Lcom/google/android/search/shared/contact/PersonShortcutKey;

    invoke-direct {v6, v5, v2, v3}, Lcom/google/android/search/shared/contact/PersonShortcutKey;-><init>(Ldzb;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Lciy;->a(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Lcom/google/android/search/shared/contact/PersonShortcut;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    .line 480
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/search/shared/contact/PersonShortcutKey;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/search/shared/contact/PersonShortcut;

    invoke-virtual {v4, v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->d(Lcom/google/android/search/shared/contact/PersonShortcutKey;)V

    if-eqz v3, :cond_12

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->amn()Ldzb;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_10
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v10

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonShortcut;->KN()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-nez v9, :cond_10

    invoke-virtual {v3, v6, v7}, Lcom/google/android/search/shared/contact/PersonShortcut;->aC(J)V

    invoke-virtual {v4, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->b(Lcom/google/android/search/shared/contact/PersonShortcut;)V

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v6}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Landroid/os/Parcelable;Z)V

    sget-object v6, Ldzb;->bRt:Ldzb;

    if-ne v5, v6, :cond_18

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v6}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->d(Ljava/util/List;Z)V

    :cond_11
    :goto_9
    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x133

    invoke-static {v2}, Lege;->ht(I)V

    invoke-virtual {v3}, Lcom/google/android/search/shared/contact/PersonShortcut;->KO()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_12

    sget-object v2, Ldzb;->bRt:Ldzb;

    if-ne v5, v2, :cond_19

    :cond_12
    :goto_a
    move-object v2, v4

    .line 485
    goto/16 :goto_2

    .line 392
    :cond_13
    const/4 v3, 0x0

    iget-object v4, v10, Ljop;->ewI:[Ljava/lang/String;

    array-length v5, v4

    const/4 v2, 0x0

    move v14, v2

    move v2, v3

    move v3, v14

    :goto_b
    if-ge v3, v5, :cond_14

    aget-object v2, v4, v3

    invoke-virtual {v12, v2}, Lcom/google/android/search/shared/contact/Relationship;->kG(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_14

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_14
    if-nez v2, :cond_9

    iget-object v2, v10, Ljop;->ewI:[Ljava/lang/String;

    invoke-virtual {v12}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v10, Ljop;->ewI:[Ljava/lang/String;

    goto/16 :goto_4

    .line 417
    :cond_15
    invoke-virtual {v2}, Lcky;->Po()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v2

    if-eqz v2, :cond_16

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-object v7, v2

    goto/16 :goto_5

    :cond_16
    const/4 v2, 0x0

    move-object v7, v2

    goto/16 :goto_5

    .line 477
    :cond_17
    invoke-virtual/range {p7 .. p7}, Ljoq;->getName()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    .line 480
    :cond_18
    invoke-virtual {v2, v5}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v6}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->d(Ljava/util/List;Z)V

    goto :goto_9

    :cond_19
    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->b(Landroid/os/Parcelable;Z)V

    goto :goto_a

    :cond_1b
    move-object v5, v2

    goto/16 :goto_7

    :cond_1c
    move v2, v3

    goto/16 :goto_6

    :cond_1d
    move-object v12, v4

    goto/16 :goto_1

    :cond_1e
    move-object v2, v4

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 251
    invoke-static {p0}, Lcom/google/android/search/shared/contact/Contact;->ku(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Contact;

    move-result-object v0

    .line 252
    invoke-virtual {v0, p2}, Lcom/google/android/search/shared/contact/Contact;->setName(Ljava/lang/String;)V

    .line 254
    new-instance v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/search/shared/contact/Person;

    invoke-static {v0}, Lcom/google/android/search/shared/contact/Person;->j(Lcom/google/android/search/shared/contact/Contact;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v2}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v1, p0, v0, v3, p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Ljava/lang/String;Ljava/util/List;ZLdzb;)V

    return-object v1
.end method

.method public static a(Ljks;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Ljks;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Ljks;->getName()Ljava/lang/String;

    move-result-object v0

    .line 135
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljks;->bor()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ljks;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 604
    const/4 v0, 0x0

    .line 605
    array-length v4, p0

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_9

    aget-object v5, p0, v1

    .line 606
    iget-object v6, v5, Ljks;->eqg:[Ljlc;

    array-length v7, v6

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_2

    aget-object v8, v6, v3

    .line 607
    invoke-virtual {v8}, Ljlc;->oP()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 608
    if-nez v0, :cond_0

    .line 609
    invoke-virtual {v8}, Ljlc;->getType()Ljava/lang/String;

    move-result-object v0

    .line 611
    :cond_0
    invoke-virtual {v8}, Ljlc;->boN()Ljlc;

    .line 606
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 614
    :cond_2
    iget-object v6, v5, Ljks;->eqh:[Ljlb;

    array-length v7, v6

    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_5

    aget-object v8, v6, v3

    .line 615
    invoke-virtual {v8}, Ljlb;->oP()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 616
    if-nez v0, :cond_3

    .line 617
    invoke-virtual {v8}, Ljlb;->getType()Ljava/lang/String;

    move-result-object v0

    .line 619
    :cond_3
    invoke-virtual {v8}, Ljlb;->boK()Ljlb;

    .line 614
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 622
    :cond_5
    iget-object v5, v5, Ljks;->eqi:[Ljld;

    array-length v6, v5

    move v3, v2

    :goto_3
    if-ge v3, v6, :cond_8

    aget-object v7, v5, v3

    .line 623
    invoke-virtual {v7}, Ljld;->oP()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 624
    if-nez v0, :cond_6

    .line 625
    invoke-virtual {v7}, Ljld;->getType()Ljava/lang/String;

    move-result-object v0

    .line 627
    :cond_6
    invoke-virtual {v7}, Ljld;->boP()Ljld;

    .line 622
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 605
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 631
    :cond_9
    return-object v0
.end method

.method public static a(Ljoq;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v2, p0, Ljoq;->ewM:Ljop;

    if-nez v2, :cond_1

    iget-object v2, p0, Ljoq;->ewL:[Ljoj;

    array-length v2, v2

    if-ne v2, v0, :cond_1

    .line 101
    iget-object v2, p0, Ljoq;->ewL:[Ljoj;

    aget-object v2, v2, v1

    .line 102
    invoke-virtual {v2}, Ljoj;->baV()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljoj;->brl()Z

    move-result v3

    if-nez v3, :cond_1

    .line 103
    iget-object v2, v2, Ljoj;->ews:[Ljon;

    .line 104
    array-length v3, v2

    if-ne v3, v0, :cond_0

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljon;->hasValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 104
    goto :goto_0

    :cond_1
    move v0, v1

    .line 107
    goto :goto_0
.end method

.method public static aC(Ljava/util/List;)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 234
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 236
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 246
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v3

    .line 234
    goto :goto_0

    .line 240
    :cond_2
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    move v4, v2

    .line 241
    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_0

    .line 242
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldzk;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Person;->a(Ldzk;)Z

    move-result v1

    if-nez v1, :cond_3

    move v2, v3

    .line 243
    goto :goto_1

    .line 241
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2
.end method

.method public static b(Ljps;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 671
    iget-object v0, p0, Ljps;->exV:[Ljrp;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 672
    iget-object v0, p0, Ljps;->exV:[Ljrp;

    aget-object v0, v0, v1

    .line 673
    iget-object v2, v0, Ljrp;->eBt:[Ljkt;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 674
    iget-object v0, v0, Ljrp;->eBt:[Ljkt;

    aget-object v0, v0, v1

    .line 675
    sget-object v2, Ljmg;->etv:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 676
    sget-object v2, Ljmg;->etv:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmg;

    .line 677
    iget-object v2, v0, Ljmg;->etw:[Ljks;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 678
    iget-object v0, v0, Ljmg;->etw:[Ljks;

    aget-object v0, v0, v1

    sget-object v2, Ljyy;->eMR:Ljsm;

    invoke-virtual {v0, v2}, Ljks;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 684
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 678
    goto :goto_0

    :cond_1
    move v0, v1

    .line 684
    goto :goto_0
.end method

.method public static be(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 155
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 156
    const-string v0, "android.intent.action.SENDTO"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    if-eqz p0, :cond_1

    .line 160
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p0, v0, v2

    .line 164
    :goto_0
    const-string v2, ","

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 165
    const-string v2, "smsto"

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 167
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    const-string v0, "sms_body"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    :cond_0
    return-object v1

    .line 162
    :cond_1
    new-array v0, v2, [Ljava/lang/String;

    goto :goto_0
.end method

.method public static k(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;
    .locals 3
    .param p0    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 178
    if-eqz p0, :cond_0

    .line 179
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 181
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "android.intent.action.MAIN"

    const-string v1, "android.intent.category.APP_CONTACTS"

    invoke-static {v0, v1}, Landroid/content/Intent;->makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static l(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 187
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public static pa(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    const-string v1, "tel"

    const/4 v2, 0x0

    invoke-static {v1, p0, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 151
    return-object v0
.end method

.method public static pb(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 201
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 203
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    move v2, v1

    move v0, v1

    .line 206
    :goto_0
    if-ge v2, v5, :cond_4

    .line 207
    aget-char v6, v3, v2

    .line 208
    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 209
    if-eqz v0, :cond_0

    .line 210
    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_0
    const/4 v0, 0x1

    .line 213
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 206
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 214
    :cond_1
    const/16 v7, 0x20

    if-ne v6, v7, :cond_3

    .line 215
    if-eqz v0, :cond_2

    .line 216
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 218
    :cond_2
    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 222
    :cond_3
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    .line 225
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
