.class abstract Lips;
.super Ljava/util/AbstractSet;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 803
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method abstract aWB()Lipn;
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 807
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0}, Lipn;->clear()V

    .line 808
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 811
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0, p1}, Lipn;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 815
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0, p1}, Lipn;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 819
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0}, Lipn;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 823
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0}, Lipn;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Lipt;

    invoke-direct {v1, p0}, Lipt;-><init>(Lips;)V

    invoke-static {v0, v1}, Likr;->a(Ljava/util/Iterator;Lifg;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 833
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0, p1}, Lipn;->bj(Ljava/lang/Object;)I

    move-result v0

    .line 834
    if-lez v0, :cond_0

    .line 835
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lipn;->j(Ljava/lang/Object;I)I

    .line 836
    const/4 v0, 0x1

    .line 838
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 842
    invoke-virtual {p0}, Lips;->aWB()Lipn;

    move-result-object v0

    invoke-interface {v0}, Lipn;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
