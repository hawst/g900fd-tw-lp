.class public final Ljqq;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezg:Ljsm;


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private dMI:Ljava/lang/String;

.field public ezh:[Ljqw;

.field public ezi:Ljpw;

.field public ezj:[Ljod;

.field private ezk:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1499
    const/16 v0, 0xb

    const-class v1, Ljqq;

    const/16 v2, 0x1f52

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqq;->ezg:Ljsm;

    .line 1505
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1584
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1585
    iput v1, p0, Ljqq;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqq;->ajB:Ljava/lang/String;

    invoke-static {}, Ljqw;->bsI()[Ljqw;

    move-result-object v0

    iput-object v0, p0, Ljqq;->ezh:[Ljqw;

    iput-object v2, p0, Ljqq;->ezi:Ljpw;

    invoke-static {}, Ljod;->brc()[Ljod;

    move-result-object v0

    iput-object v0, p0, Ljqq;->ezj:[Ljod;

    iput-boolean v1, p0, Ljqq;->ezk:Z

    const-string v0, ""

    iput-object v0, p0, Ljqq;->dMI:Ljava/lang/String;

    iput-object v2, p0, Ljqq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqq;->eCz:I

    .line 1586
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqq;->ajB:Ljava/lang/String;

    iget v0, p0, Ljqq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqq;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqq;->ezh:[Ljqw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljqw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqq;->ezh:[Ljqw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljqw;

    invoke-direct {v3}, Ljqw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqq;->ezh:[Ljqw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljqw;

    invoke-direct {v3}, Ljqw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqq;->ezh:[Ljqw;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljqq;->ezi:Ljpw;

    if-nez v0, :cond_4

    new-instance v0, Ljpw;

    invoke-direct {v0}, Ljpw;-><init>()V

    iput-object v0, p0, Ljqq;->ezi:Ljpw;

    :cond_4
    iget-object v0, p0, Ljqq;->ezi:Ljpw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqq;->ezj:[Ljod;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljod;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljqq;->ezj:[Ljod;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljod;

    invoke-direct {v3}, Ljod;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljqq;->ezj:[Ljod;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljod;

    invoke-direct {v3}, Ljod;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqq;->ezj:[Ljod;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqq;->ezk:Z

    iget v0, p0, Ljqq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqq;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqq;->dMI:Ljava/lang/String;

    iget v0, p0, Ljqq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqq;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1604
    iget v0, p0, Ljqq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1605
    const/4 v0, 0x1

    iget-object v2, p0, Ljqq;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 1607
    :cond_0
    iget-object v0, p0, Ljqq;->ezh:[Ljqw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljqq;->ezh:[Ljqw;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 1608
    :goto_0
    iget-object v2, p0, Ljqq;->ezh:[Ljqw;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1609
    iget-object v2, p0, Ljqq;->ezh:[Ljqw;

    aget-object v2, v2, v0

    .line 1610
    if-eqz v2, :cond_1

    .line 1611
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1608
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1615
    :cond_2
    iget-object v0, p0, Ljqq;->ezi:Ljpw;

    if-eqz v0, :cond_3

    .line 1616
    const/4 v0, 0x3

    iget-object v2, p0, Ljqq;->ezi:Ljpw;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 1618
    :cond_3
    iget-object v0, p0, Ljqq;->ezj:[Ljod;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljqq;->ezj:[Ljod;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1619
    :goto_1
    iget-object v0, p0, Ljqq;->ezj:[Ljod;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 1620
    iget-object v0, p0, Ljqq;->ezj:[Ljod;

    aget-object v0, v0, v1

    .line 1621
    if-eqz v0, :cond_4

    .line 1622
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 1619
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1626
    :cond_5
    iget v0, p0, Ljqq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 1627
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljqq;->ezk:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1629
    :cond_6
    iget v0, p0, Ljqq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    .line 1630
    const/4 v0, 0x6

    iget-object v1, p0, Ljqq;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1632
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1633
    return-void
.end method

.method public final bsw()Z
    .locals 1

    .prologue
    .line 1546
    iget-boolean v0, p0, Ljqq;->ezk:Z

    return v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1515
    iget-object v0, p0, Ljqq;->ajB:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1565
    iget-object v0, p0, Ljqq;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1637
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1638
    iget v2, p0, Ljqq;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 1639
    const/4 v2, 0x1

    iget-object v3, p0, Ljqq;->ajB:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1642
    :cond_0
    iget-object v2, p0, Ljqq;->ezh:[Ljqw;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljqq;->ezh:[Ljqw;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 1643
    :goto_0
    iget-object v3, p0, Ljqq;->ezh:[Ljqw;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1644
    iget-object v3, p0, Ljqq;->ezh:[Ljqw;

    aget-object v3, v3, v0

    .line 1645
    if-eqz v3, :cond_1

    .line 1646
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1643
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1651
    :cond_3
    iget-object v2, p0, Ljqq;->ezi:Ljpw;

    if-eqz v2, :cond_4

    .line 1652
    const/4 v2, 0x3

    iget-object v3, p0, Ljqq;->ezi:Ljpw;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1655
    :cond_4
    iget-object v2, p0, Ljqq;->ezj:[Ljod;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljqq;->ezj:[Ljod;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1656
    :goto_1
    iget-object v2, p0, Ljqq;->ezj:[Ljod;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1657
    iget-object v2, p0, Ljqq;->ezj:[Ljod;

    aget-object v2, v2, v1

    .line 1658
    if-eqz v2, :cond_5

    .line 1659
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1656
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1664
    :cond_6
    iget v1, p0, Ljqq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_7

    .line 1665
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljqq;->ezk:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1668
    :cond_7
    iget v1, p0, Ljqq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    .line 1669
    const/4 v1, 0x6

    iget-object v2, p0, Ljqq;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    :cond_8
    return v0
.end method

.method public final yj(Ljava/lang/String;)Ljqq;
    .locals 1

    .prologue
    .line 1518
    if-nez p1, :cond_0

    .line 1519
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1521
    :cond_0
    iput-object p1, p0, Ljqq;->ajB:Ljava/lang/String;

    .line 1522
    iget v0, p0, Ljqq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqq;->aez:I

    .line 1523
    return-object p0
.end method
