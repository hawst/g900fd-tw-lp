.class public final Leju;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lekg;


# instance fields
.field private cbW:I

.field private cbX:I

.field private cbY:F

.field private cbZ:F

.field private cca:Z

.field public ccb:Z

.field private ccc:I

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lekf;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Leju;->mView:Landroid/view/View;

    .line 30
    invoke-interface {p2, p0}, Lekf;->a(Lekg;)V

    .line 31
    invoke-interface {p2}, Lekf;->getScrollY()I

    move-result v0

    iput v0, p0, Leju;->ccc:I

    .line 32
    invoke-interface {p2}, Lekf;->getMaxScrollY()I

    .line 33
    return-void
.end method

.method private atN()V
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 95
    .line 96
    iget v1, p0, Leju;->cbX:I

    iget v2, p0, Leju;->cbW:I

    if-le v1, v2, :cond_1

    .line 97
    iget v1, p0, Leju;->ccc:I

    iget v2, p0, Leju;->cbW:I

    if-lt v1, v2, :cond_2

    .line 98
    iget v1, p0, Leju;->ccc:I

    iget v2, p0, Leju;->cbX:I

    if-ge v1, v2, :cond_0

    .line 100
    iget v0, p0, Leju;->ccc:I

    int-to-float v0, v0

    iget v1, p0, Leju;->cbW:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Leju;->cbX:I

    iget v2, p0, Leju;->cbW:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 114
    :cond_0
    :goto_0
    iget v1, p0, Leju;->cbZ:F

    iget v2, p0, Leju;->cbY:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p0, Leju;->cbY:F

    add-float/2addr v0, v1

    .line 115
    invoke-direct {p0, v0}, Leju;->setAlpha(F)V

    .line 116
    return-void

    .line 103
    :cond_1
    iget v1, p0, Leju;->cbW:I

    iget v2, p0, Leju;->cbX:I

    if-le v1, v2, :cond_0

    .line 104
    iget v1, p0, Leju;->ccc:I

    iget v2, p0, Leju;->cbX:I

    if-lt v1, v2, :cond_0

    .line 106
    iget v1, p0, Leju;->ccc:I

    iget v2, p0, Leju;->cbW:I

    if-ge v1, v2, :cond_2

    .line 107
    iget v1, p0, Leju;->ccc:I

    int-to-float v1, v1

    iget v2, p0, Leju;->cbX:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p0, Leju;->cbW:I

    iget v3, p0, Leju;->cbX:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0

    .line 110
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAlpha(F)V
    .locals 3

    .prologue
    .line 64
    iget-boolean v0, p0, Leju;->ccb:Z

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Leju;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    const/high16 v2, 0x43800000    # 256.0f

    mul-float/2addr v2, p1

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Leju;->mView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method


# virtual methods
.method public final L(F)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Leju;->cca:Z

    .line 60
    invoke-direct {p0, p1}, Leju;->setAlpha(F)V

    .line 61
    return-void
.end method

.method public final a(IIFF)V
    .locals 1

    .prologue
    .line 49
    iput p1, p0, Leju;->cbW:I

    .line 50
    iput p2, p0, Leju;->cbX:I

    .line 51
    iput p3, p0, Leju;->cbY:F

    .line 52
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Leju;->cbZ:F

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Leju;->cca:Z

    .line 54
    invoke-direct {p0}, Leju;->atN()V

    .line 55
    return-void
.end method

.method public final aP(II)V
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, p2, v0, v1}, Leju;->a(IIFF)V

    .line 41
    return-void
.end method

.method public final aa(II)V
    .locals 1

    .prologue
    .line 77
    iput p1, p0, Leju;->ccc:I

    .line 78
    iget-boolean v0, p0, Leju;->cca:Z

    if-nez v0, :cond_0

    .line 80
    invoke-direct {p0}, Leju;->atN()V

    .line 82
    :cond_0
    return-void
.end method

.method public final dr(I)V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public final vS()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public final vT()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public final vU()V
    .locals 0

    .prologue
    .line 129
    return-void
.end method

.method public final vV()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public final vW()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method
