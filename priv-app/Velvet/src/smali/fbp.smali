.class public final Lfbp;
.super Lfkb;
.source "PG"


# static fields
.field private static cob:Ljava/util/Set;


# instance fields
.field private final mCalendarDataProvider:Leym;

.field private final mContext:Landroid/content/Context;

.field private final mLocationOracle:Lfdr;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lfbp;->cob:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Leym;Landroid/content/Context;Lfdr;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lfkb;-><init>()V

    .line 42
    iput-object p1, p0, Lfbp;->mCalendarDataProvider:Leym;

    .line 43
    iput-object p2, p0, Lfbp;->mContext:Landroid/content/Context;

    .line 44
    iput-object p3, p0, Lfbp;->mLocationOracle:Lfdr;

    .line 45
    return-void
.end method

.method protected static axl()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected static b(ILizq;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 140
    sparse-switch p0, :sswitch_data_0

    .line 179
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    .line 142
    :sswitch_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 145
    :sswitch_1
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dTz:Ljeg;

    if-eqz v2, :cond_0

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    .line 148
    :sswitch_2
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dTw:Ljeg;

    if-eqz v2, :cond_1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    .line 151
    :sswitch_3
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dTB:Ljeg;

    if-eqz v2, :cond_2

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_3

    .line 154
    :sswitch_4
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dSS:Ljca;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dSS:Ljca;

    invoke-virtual {v2}, Ljca;->pb()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_4

    .line 158
    :sswitch_5
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dTW:Ljcc;

    if-eqz v2, :cond_4

    :goto_5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_5

    .line 161
    :sswitch_6
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dSA:Ljau;

    if-eqz v2, :cond_5

    :goto_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_6

    .line 164
    :sswitch_7
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dSM:Ljeg;

    if-eqz v2, :cond_6

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_7

    .line 167
    :sswitch_8
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dSn:Ljau;

    if-eqz v2, :cond_7

    :goto_8
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_8

    .line 170
    :sswitch_9
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dSl:Ljau;

    if-eqz v2, :cond_8

    :goto_9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_9

    .line 173
    :sswitch_a
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dTE:Ljau;

    if-eqz v2, :cond_9

    :goto_a
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_a

    .line 176
    :sswitch_b
    iget-object v2, p1, Lizq;->dUZ:Lizj;

    iget-object v2, v2, Lizj;->dTY:Ljeg;

    if-eqz v2, :cond_a

    :goto_b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto :goto_b

    .line 140
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_7
        0x26 -> :sswitch_4
        0x30 -> :sswitch_0
        0x4d -> :sswitch_2
        0x50 -> :sswitch_1
        0x52 -> :sswitch_3
        0x62 -> :sswitch_5
        0x64 -> :sswitch_b
        0x71 -> :sswitch_9
        0x73 -> :sswitch_8
        0x7b -> :sswitch_a
        0x8a -> :sswitch_6
    .end sparse-switch
.end method

.method private static f(Lizj;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 228
    new-array v1, v0, [I

    invoke-static {p0, p1, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 229
    if-eqz v1, :cond_0

    iget-object v1, v1, Liwk;->afA:Ljbj;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method protected final synthetic b(Lizj;Ljal;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic c(ILizj;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 26
    sparse-switch p1, :sswitch_data_0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    const/16 v0, 0xa0

    invoke-static {p2, v0}, Lfbp;->f(Lizj;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0xa1

    invoke-static {p2, v0}, Lfbp;->f(Lizj;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p2}, Lizj;->bcY()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p2, Lizj;->dSf:Lixr;

    invoke-virtual {v2}, Lixr;->baJ()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p2, Lizj;->dSf:Lixr;

    invoke-virtual {v2}, Lixr;->baI()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lfbp;->mCalendarDataProvider:Leym;

    iget-object v3, p2, Lizj;->dSf:Lixr;

    invoke-virtual {v3}, Lixr;->baD()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Leym;->lE(Ljava/lang/String;)Lamk;

    move-result-object v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_4
    const/16 v0, 0xc8

    invoke-static {p2, v0}, Lfbp;->f(Lizj;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_5
    iget-object v2, p2, Lizj;->dSi:Ljae;

    iget-object v2, v2, Ljae;->dWa:[Ljag;

    array-length v2, v2

    if-lez v2, :cond_3

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1

    :sswitch_6
    const-string v2, "Wifi"

    iget-object v3, p2, Lizj;->dSh:Ljas;

    invoke-virtual {v3}, Ljas;->beA()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lfbp;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lesp;->aA(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_2

    :cond_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_7
    iget-object v2, p2, Lizj;->dTg:Ljal;

    if-eqz v2, :cond_6

    iget-object v2, p2, Lizj;->dTg:Ljal;

    iget-object v2, v2, Ljal;->dMF:[Liyg;

    array-length v2, v2

    if-lez v2, :cond_6

    iget-object v2, p2, Lizj;->dTg:Ljal;

    iget-object v2, v2, Ljal;->dMF:[Liyg;

    aget-object v2, v2, v0

    iget-object v2, v2, Liyg;->dPL:Liyh;

    if-eqz v2, :cond_6

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v1, v0

    goto :goto_3

    :sswitch_8
    iget-object v2, p2, Lizj;->dTd:Ljek;

    invoke-virtual {v2}, Ljek;->biJ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    :goto_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    goto :goto_4

    :sswitch_9
    iget-object v2, p2, Lizj;->dSk:Ljgj;

    iget-object v3, v2, Ljgj;->ejN:[Ljgn;

    array-length v3, v3

    if-ne v3, v5, :cond_9

    sget-object v3, Lfbp;->cob:Ljava/util/Set;

    invoke-virtual {v2}, Ljgj;->bew()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2}, Ljgj;->getStatusCode()I

    move-result v2

    if-eq v2, v5, :cond_8

    if-eq v2, v1, :cond_8

    if-nez v2, :cond_9

    :cond_8
    :goto_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v1, v0

    goto :goto_5

    :sswitch_a
    iget-object v2, p2, Lizj;->dUg:Ljhd;

    if-eqz v2, :cond_a

    iget-object v2, p2, Lizj;->dUg:Ljhd;

    invoke-virtual {v2}, Ljhd;->bly()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p2, Lizj;->dUg:Ljhd;

    iget-object v2, v2, Ljhd;->elx:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    :goto_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    move v1, v0

    goto :goto_6

    :sswitch_b
    iget-object v2, p2, Lizj;->dTF:Ljhh;

    if-eqz v2, :cond_b

    :goto_7
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    move v1, v0

    goto :goto_7

    :sswitch_c
    const/16 v0, 0xa2

    invoke-static {p2, v0}, Lfbp;->f(Lizj;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :sswitch_d
    iget-object v2, p2, Lizj;->dTo:Ljdi;

    if-eqz v2, :cond_c

    iget-object v2, p2, Lizj;->dTo:Ljdi;

    iget-object v2, v2, Ljdi;->eca:Ljdj;

    if-eqz v2, :cond_c

    :goto_8
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_c
    move v1, v0

    goto :goto_8

    :sswitch_e
    iget-object v2, p2, Lizj;->dTS:Ljho;

    if-eqz v2, :cond_d

    iget-object v2, p2, Lizj;->dTS:Ljho;

    iget-object v2, v2, Ljho;->dPO:[Ljhn;

    array-length v2, v2

    if-lez v2, :cond_d

    :goto_9
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    move v1, v0

    goto :goto_9

    :sswitch_f
    iget-object v2, p2, Lizj;->dSg:Ljhv;

    if-eqz v2, :cond_f

    iget-object v2, p2, Lizj;->dSg:Ljhv;

    iget-object v2, v2, Ljhv;->emt:[Ljhw;

    array-length v2, v2

    if-lez v2, :cond_f

    iget-object v2, p2, Lizj;->dSg:Ljhv;

    iget-object v2, v2, Ljhv;->dPR:Ljbp;

    if-eqz v2, :cond_f

    move v2, v1

    :goto_a
    if-eqz v2, :cond_11

    iget-object v2, p0, Lfbp;->mLocationOracle:Lfdr;

    invoke-interface {v2}, Lfdr;->tv()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p2, Lizj;->dSg:Ljhv;

    iget-object v2, v2, Ljhv;->dPR:Ljbp;

    iget-object v3, p0, Lfbp;->mLocationOracle:Lfdr;

    invoke-interface {v3}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v3

    invoke-static {v2, v3}, Lgay;->a(Ljbp;Landroid/location/Location;)F

    move-result v3

    float-to-double v4, v3

    invoke-virtual {v2}, Ljbp;->bfc()D

    move-result-wide v2

    cmpg-double v2, v4, v2

    if-gtz v2, :cond_10

    move v2, v1

    :goto_b
    if-eqz v2, :cond_11

    :cond_e
    :goto_c
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_f
    move v2, v0

    goto :goto_a

    :cond_10
    move v2, v0

    goto :goto_b

    :cond_11
    move v1, v0

    goto :goto_c

    :sswitch_10
    iget-object v2, p2, Lizj;->dTq:Ljik;

    if-eqz v2, :cond_12

    iget-object v2, p2, Lizj;->dTq:Ljik;

    iget-object v2, v2, Ljik;->dSC:Ljcf;

    if-eqz v2, :cond_12

    :goto_d
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_12
    move v1, v0

    goto :goto_d

    :sswitch_11
    iget-object v2, p2, Lizj;->dSd:Ljir;

    iget-object v2, v2, Ljir;->enN:[Ljis;

    array-length v2, v2

    if-lez v2, :cond_13

    :goto_e
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_13
    move v1, v0

    goto :goto_e

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_11
        0x9 -> :sswitch_f
        0xb -> :sswitch_6
        0xc -> :sswitch_5
        0xd -> :sswitch_9
        0xe -> :sswitch_3
        0x30 -> :sswitch_0
        0x35 -> :sswitch_8
        0x38 -> :sswitch_7
        0x43 -> :sswitch_d
        0x44 -> :sswitch_10
        0x4b -> :sswitch_1
        0x4c -> :sswitch_2
        0x4e -> :sswitch_b
        0x57 -> :sswitch_c
        0x5e -> :sswitch_e
        0x78 -> :sswitch_4
        0x7e -> :sswitch_a
    .end sparse-switch
.end method

.method protected final synthetic c(ILizq;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-static {p1, p2}, Lfbp;->b(ILizq;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lizj;)Z
    .locals 1

    .prologue
    .line 52
    invoke-super {p0, p1}, Lfkb;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 53
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final e(Lizq;)Z
    .locals 1

    .prologue
    .line 59
    invoke-super {p0, p1}, Lfkb;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 60
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
