.class public final Ljud;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eEA:[Ljud;


# instance fields
.field private eEB:J

.field private eEC:J

.field private eED:F

.field private eEE:J

.field private eEF:J

.field private eEG:Ljava/lang/String;

.field private eEH:Ljava/lang/String;

.field private eEI:[J

.field private eEJ:F

.field private eEs:F

.field private eEt:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 67
    iput-wide v2, p0, Ljud;->eEB:J

    iput-wide v2, p0, Ljud;->eEC:J

    iput v1, p0, Ljud;->eED:F

    iput v1, p0, Ljud;->eEs:F

    iput v1, p0, Ljud;->eEt:F

    iput-wide v2, p0, Ljud;->eEE:J

    iput-wide v2, p0, Ljud;->eEF:J

    const-string v0, ""

    iput-object v0, p0, Ljud;->eEG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljud;->eEH:Ljava/lang/String;

    sget-object v0, Ljsu;->eCB:[J

    iput-object v0, p0, Ljud;->eEI:[J

    iput v1, p0, Ljud;->eEJ:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljud;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljud;->eCz:I

    .line 68
    return-void
.end method

.method public static buE()[Ljud;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Ljud;->eEA:[Ljud;

    if-nez v0, :cond_1

    .line 23
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 25
    :try_start_0
    sget-object v0, Ljud;->eEA:[Ljud;

    if-nez v0, :cond_0

    .line 26
    const/4 v0, 0x0

    new-array v0, v0, [Ljud;

    sput-object v0, Ljud;->eEA:[Ljud;

    .line 28
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    :cond_1
    sget-object v0, Ljud;->eEA:[Ljud;

    return-object v0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 16
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljud;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljud;->eEB:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljud;->eEC:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljud;->eED:F

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljud;->eEs:F

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljud;->eEt:F

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljud;->eEE:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljud;->eEF:J

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljud;->eEG:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljud;->eEH:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    const/16 v0, 0x78

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljud;->eEI:[J

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljud;->eEI:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljud;->eEI:[J

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Ljud;->eEI:[J

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btR()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljud;->eEI:[J

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljud;->eEI:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljud;->eEI:[J

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljud;->eEI:[J

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljud;->eEJ:F

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x30 -> :sswitch_1
        0x38 -> :sswitch_2
        0x45 -> :sswitch_3
        0x4d -> :sswitch_4
        0x55 -> :sswitch_5
        0x58 -> :sswitch_6
        0x60 -> :sswitch_7
        0x6a -> :sswitch_8
        0x72 -> :sswitch_9
        0x78 -> :sswitch_a
        0x7a -> :sswitch_b
        0x85 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 90
    const/4 v0, 0x6

    iget-wide v2, p0, Ljud;->eEB:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 91
    const/4 v0, 0x7

    iget-wide v2, p0, Ljud;->eEC:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 92
    const/16 v0, 0x8

    iget v1, p0, Ljud;->eED:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 93
    const/16 v0, 0x9

    iget v1, p0, Ljud;->eEs:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 94
    const/16 v0, 0xa

    iget v1, p0, Ljud;->eEt:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 95
    const/16 v0, 0xb

    iget-wide v2, p0, Ljud;->eEE:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 96
    const/16 v0, 0xc

    iget-wide v2, p0, Ljud;->eEF:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 97
    const/16 v0, 0xd

    iget-object v1, p0, Ljud;->eEG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 98
    const/16 v0, 0xe

    iget-object v1, p0, Ljud;->eEH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 99
    iget-object v0, p0, Ljud;->eEI:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljud;->eEI:[J

    array-length v0, v0

    if-lez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljud;->eEI:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 101
    const/16 v1, 0xf

    iget-object v2, p0, Ljud;->eEI:[J

    aget-wide v2, v2, v0

    invoke-virtual {p1, v1, v2, v3}, Ljsj;->h(IJ)V

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_0
    const/16 v0, 0x10

    iget v1, p0, Ljud;->eEJ:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 105
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 106
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 110
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 111
    const/4 v2, 0x6

    iget-wide v4, p0, Ljud;->eEB:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 113
    const/4 v2, 0x7

    iget-wide v4, p0, Ljud;->eEC:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 115
    const/16 v2, 0x8

    iget v3, p0, Ljud;->eED:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    .line 117
    const/16 v2, 0x9

    iget v3, p0, Ljud;->eEs:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    .line 119
    const/16 v2, 0xa

    iget v3, p0, Ljud;->eEt:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    .line 121
    const/16 v2, 0xb

    iget-wide v4, p0, Ljud;->eEE:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 123
    const/16 v2, 0xc

    iget-wide v4, p0, Ljud;->eEF:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 125
    const/16 v2, 0xd

    iget-object v3, p0, Ljud;->eEG:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 127
    const/16 v2, 0xe

    iget-object v3, p0, Ljud;->eEH:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v2, v1

    .line 129
    iget-object v1, p0, Ljud;->eEI:[J

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljud;->eEI:[J

    array-length v1, v1

    if-lez v1, :cond_1

    move v1, v0

    .line 131
    :goto_0
    iget-object v3, p0, Ljud;->eEI:[J

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 132
    iget-object v3, p0, Ljud;->eEI:[J

    aget-wide v4, v3, v0

    .line 133
    invoke-static {v4, v5}, Ljsj;->dJ(J)I

    move-result v3

    add-int/2addr v1, v3

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_0
    add-int v0, v2, v1

    .line 137
    iget-object v1, p0, Ljud;->eEI:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 139
    :goto_1
    const/16 v1, 0x10

    iget v2, p0, Ljud;->eEJ:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 141
    return v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method
