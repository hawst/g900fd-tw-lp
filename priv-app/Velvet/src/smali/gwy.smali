.class public final Lgwy;
.super Lgxi;
.source "PG"


# instance fields
.field private bqB:Lcom/google/android/search/shared/actions/errors/SearchError;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentView;)V
    .locals 1

    .prologue
    .line 23
    const-string v0, "error"

    invoke-direct {p0, v0, p1}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final X(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 28
    invoke-virtual {p0}, Lgwy;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v0

    iput-object v0, p0, Lgwy;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lgwy;->aLt()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {p0, v2}, Lgpu;->e(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/search/core/ui/ErrorView;

    move-result-object v2

    iget-object v3, p0, Lgwy;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/errors/SearchError;->abj()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Lgwy;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/search/core/ui/ErrorView;->d(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    new-instance v3, Lgwz;

    const-string v4, "tryAgainClickListener"

    invoke-direct {v3, p0, v4}, Lgwz;-><init>(Lgwy;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/android/search/core/ui/ErrorView;->k(Ljava/lang/Runnable;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgwy;->a([Landroid/view/View;)V

    .line 31
    return-void
.end method

.method protected final aKF()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lgxi;->cYu:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 36
    sget-object v0, Lgxi;->cYt:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 37
    return-void
.end method

.method final aLl()V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lgwy;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwy;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lgwy;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iget-object v1, p0, Lgwy;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiQ()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->I(Lcom/google/android/shared/search/Query;)V

    .line 56
    :cond_0
    return-void
.end method
