.class public final Lftn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bLI:Ljava/lang/String;

.field private cBK:Lani;

.field cBL:I

.field cBM:Ljcn;

.field cBi:Ljava/lang/String;

.field cBj:Ljava/lang/String;

.field ceJ:Ljava/lang/String;

.field coM:Ljcn;

.field private mEntry:Lizj;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lftn;->cBL:I

    .line 27
    iput-object p1, p0, Lftn;->bLI:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method final a(Lani;Lizj;)Lftn;
    .locals 1

    .prologue
    .line 51
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lani;

    iput-object v0, p0, Lftn;->cBK:Lani;

    .line 52
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lftn;->mEntry:Lizj;

    .line 53
    return-object p0
.end method

.method final aDh()Lanh;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 68
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 70
    new-instance v1, Lany;

    invoke-direct {v1}, Lany;-><init>()V

    .line 71
    iput-object v1, v0, Lanh;->agw:Lany;

    .line 72
    iget-object v2, p0, Lftn;->bLI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 74
    iget-object v2, p0, Lftn;->ceJ:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    iget-object v2, p0, Lftn;->ceJ:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 78
    :cond_0
    iget-object v2, p0, Lftn;->cBj:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    iget-object v2, p0, Lftn;->cBj:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lany;->bk(Ljava/lang/String;)Lany;

    .line 80
    iget v2, p0, Lftn;->cBL:I

    if-eqz v2, :cond_1

    .line 81
    iget v2, p0, Lftn;->cBL:I

    invoke-virtual {v1, v2}, Lany;->cz(I)Lany;

    .line 85
    :cond_1
    iget-object v2, p0, Lftn;->coM:Ljcn;

    if-eqz v2, :cond_2

    .line 86
    iget-object v2, p0, Lftn;->coM:Ljcn;

    iput-object v2, v1, Lany;->aiS:Ljcn;

    .line 89
    :cond_2
    iget-object v2, p0, Lftn;->cBM:Ljcn;

    if-eqz v2, :cond_3

    .line 90
    iget-object v2, p0, Lftn;->cBM:Ljcn;

    iput-object v2, v1, Lany;->aiT:Ljcn;

    .line 93
    :cond_3
    iget-object v2, p0, Lftn;->cBi:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 94
    iget-object v2, p0, Lftn;->cBi:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lany;->bj(Ljava/lang/String;)Lany;

    .line 97
    :cond_4
    iget-object v1, p0, Lftn;->cBK:Lani;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lftn;->mEntry:Lizj;

    if-eqz v1, :cond_5

    .line 98
    iget-object v1, p0, Lftn;->cBK:Lani;

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 99
    iget-object v1, p0, Lftn;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    .line 102
    :cond_5
    return-object v0
.end method
