.class public final Leak;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lesm;


# instance fields
.field final bSy:Lesm;

.field public final bSz:Lean;


# direct methods
.method public constructor <init>(Lesm;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Leak;->bSy:Lesm;

    .line 57
    new-instance v0, Lean;

    const/16 v1, 0x32

    invoke-direct {v0, p0, v1}, Lean;-><init>(Leak;I)V

    iput-object v0, p0, Leak;->bSz:Lean;

    .line 58
    return-void
.end method


# virtual methods
.method public final D(Landroid/net/Uri;)Leml;
    .locals 2

    .prologue
    .line 62
    invoke-static {p1}, Lesp;->aQ(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "0"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    :cond_0
    invoke-static {}, Lepr;->avq()Lepr;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 66
    :cond_1
    iget-object v0, p0, Leak;->bSz:Lean;

    invoke-virtual {v0, p1}, Lean;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leal;

    .line 67
    invoke-virtual {v0}, Leal;->amy()Leml;

    move-result-object v0

    goto :goto_0
.end method

.method public final clearCache()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Leak;->bSz:Lean;

    invoke-virtual {v0}, Lean;->evictAll()V

    .line 73
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Leak;->bSy:Lesm;

    invoke-interface {v0, p1}, Lesm;->u(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
