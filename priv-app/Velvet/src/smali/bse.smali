.class public final Lbse;
.super Ljava/lang/Object;

# interfaces
.implements Lbgo;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final aIl:Lbne;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbgp;Lbgq;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lbne;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3, v1}, Lbne;-><init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V

    iput-object v0, p0, Lbse;->aIl:Lbne;

    return-void
.end method


# virtual methods
.method public final K(J)I
    .locals 1

    iget-object v0, p0, Lbse;->aIl:Lbne;

    invoke-virtual {v0, p1, p2}, Lbne;->K(J)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    .locals 1

    iget-object v0, p0, Lbse;->aIl:Lbne;

    invoke-virtual {v0, p1}, Lbne;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 1

    iget-object v0, p0, Lbse;->aIl:Lbne;

    invoke-virtual {v0, p1}, Lbne;->c(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v0

    return-object v0
.end method

.method public final connect()V
    .locals 1

    iget-object v0, p0, Lbse;->aIl:Lbne;

    invoke-virtual {v0}, Lbne;->connect()V

    return-void
.end method

.method public final d(Landroid/accounts/Account;)I
    .locals 1

    iget-object v0, p0, Lbse;->aIl:Lbne;

    invoke-virtual {v0, p1}, Lbne;->d(Landroid/accounts/Account;)I

    move-result v0

    return v0
.end method

.method public final disconnect()V
    .locals 1

    iget-object v0, p0, Lbse;->aIl:Lbne;

    invoke-virtual {v0}, Lbne;->disconnect()V

    return-void
.end method
