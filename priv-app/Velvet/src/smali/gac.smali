.class public final Lgac;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Lfmt;Landroid/content/Intent;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 201
    if-eqz p2, :cond_0

    .line 202
    const/high16 v1, 0x10000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 204
    :cond_0
    invoke-interface {p0}, Lfmt;->wr()Leoj;

    move-result-object v1

    .line 205
    if-nez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    aput-object p1, v2, v0

    invoke-interface {v1, v2}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lfmt;Lixx;Z)Z
    .locals 3

    .prologue
    .line 174
    invoke-virtual {p1}, Lixx;->baX()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 175
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 177
    invoke-virtual {p1}, Lixx;->avd()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 178
    invoke-virtual {p1}, Lixx;->bbe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    invoke-static {p0, v0, p2}, Lgac;->a(Lfmt;Landroid/content/Intent;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const/4 v0, 0x1

    .line 196
    :goto_0
    return v0

    .line 185
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    :cond_1
    invoke-static {p0, v0, p2}, Lgac;->a(Lfmt;Landroid/content/Intent;Z)Z

    move-result v0

    goto :goto_0

    .line 190
    :cond_2
    invoke-virtual {p1}, Lixx;->baY()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 191
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Lixx;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, p2}, Lgac;->a(Lfmt;Landroid/content/Intent;Z)Z

    move-result v0

    goto :goto_0

    .line 192
    :cond_3
    invoke-virtual {p1}, Lixx;->bba()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 193
    invoke-virtual {p1}, Lixx;->baZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->a(Lixx;)Lcom/google/android/sidekick/shared/client/NowSearchOptions;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lfmt;->a(Ljava/lang/String;Lcom/google/android/sidekick/shared/client/NowSearchOptions;)Z

    move-result v0

    goto :goto_0

    .line 196
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
