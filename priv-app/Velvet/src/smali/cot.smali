.class public final Lcot;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldkt;
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final aUn:Lckg;

.field private aqW:Ljava/lang/String;

.field private final beb:Z

.field private final bec:Ljava/lang/String;

.field private final bed:Z

.field private bee:Ljava/lang/String;

.field private bef:J

.field private beg:Lcom/google/android/search/shared/api/UriRequest;

.field private beh:Ljava/lang/String;

.field private bei:Ljava/lang/String;

.field private bej:Z

.field private bek:Ljava/lang/String;

.field private bel:Landroid/net/Uri;

.field private bem:Z

.field private ben:Z

.field private beo:Z

.field private bep:J

.field beq:Z

.field private ber:Z

.field final dK:Ljava/lang/Object;

.field private final mClock:Lemp;

.field private final mCookies:Lgpf;

.field private final mCookiesLock:Ldkq;

.field private final mFactory:Lgpu;

.field private final mGsaConfig:Lchk;

.field private final mHttpHelper:Ldkx;

.field private final mLoginHelper:Lcrh;

.field private final mSettings:Lcke;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;

.field private final mUrlHelper:Lcpn;

.field mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lemp;Lchk;Lcke;Lcrh;Lcpn;Ldkx;Lgpf;Ldkq;Ljava/util/concurrent/Executor;Lckg;Lgpu;Z)V
    .locals 4

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p2, p0, Lcot;->mGsaConfig:Lchk;

    .line 117
    iput-object p1, p0, Lcot;->mClock:Lemp;

    .line 118
    iput-object p3, p0, Lcot;->mSettings:Lcke;

    .line 119
    iput-object p6, p0, Lcot;->mHttpHelper:Ldkx;

    .line 120
    iput-object p4, p0, Lcot;->mLoginHelper:Lcrh;

    .line 121
    iput-object p5, p0, Lcot;->mUrlHelper:Lcpn;

    .line 122
    iput-object p7, p0, Lcot;->mCookies:Lgpf;

    .line 123
    iput-object p8, p0, Lcot;->mCookiesLock:Ldkq;

    .line 124
    iput-object p9, p0, Lcot;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 125
    iput-object p10, p0, Lcot;->aUn:Lckg;

    .line 126
    iput-object p11, p0, Lcot;->mFactory:Lgpu;

    .line 128
    iget-object v0, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->ND()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcot;->bec:Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NY()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcot;->bee:Ljava/lang/String;

    .line 130
    iget-object v0, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NZ()Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NX()J

    move-result-wide v0

    iput-wide v0, p0, Lcot;->bef:J

    .line 133
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    .line 137
    iget-wide v0, p0, Lcot;->bef:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcot;->bee:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcot;->bee:Ljava/lang/String;

    iget-object v1, p0, Lcot;->bec:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcot;->bed:Z

    .line 143
    if-nez p12, :cond_1

    iget-wide v0, p0, Lcot;->bef:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcot;->beb:Z

    .line 146
    return-void

    .line 137
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private Df()V
    .locals 2

    .prologue
    .line 160
    iget-object v1, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcot;->ber:Z

    .line 164
    :cond_0
    iget-boolean v0, p0, Lcot;->ber:Z

    if-eqz v0, :cond_1

    .line 165
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private Rd()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 325
    iget-object v2, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v2}, Lcke;->NY()Ljava/lang/String;

    move-result-object v2

    .line 326
    iget-object v3, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v3}, Lcke;->NZ()Ljava/lang/String;

    move-result-object v3

    .line 327
    iget-object v4, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v4}, Lcke;->ND()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcot;->mUrlHelper:Lcpn;

    invoke-virtual {v2}, Lcpn;->Rq()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcot;->bef:J

    iget-object v4, p0, Lcot;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 364
    :goto_0
    return v0

    .line 336
    :cond_0
    iget-object v2, p0, Lcot;->mUrlHelper:Lcpn;

    invoke-virtual {v2}, Lcpn;->RE()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcot;->bel:Landroid/net/Uri;

    .line 337
    iget-object v2, p0, Lcot;->mLoginHelper:Lcrh;

    iget-object v3, p0, Lcot;->bel:Landroid/net/Uri;

    iget-object v4, p0, Lcot;->mGsaConfig:Lchk;

    invoke-virtual {v4}, Lchk;->Io()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcrh;->c(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 339
    if-nez v2, :cond_1

    .line 341
    iget-object v1, p0, Lcot;->mSettings:Lcke;

    const-string v2, ""

    invoke-interface {v1, v2}, Lcke;->gW(Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :cond_1
    iget-object v0, p0, Lcot;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcou;

    const-string v4, "Start load"

    invoke-direct {v3, p0, v4, v2}, Lcou;-><init>(Lcot;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 352
    iget-object v2, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 353
    :goto_1
    :try_start_0
    iget-boolean v0, p0, Lcot;->bem:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 355
    :try_start_1
    iget-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 357
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcot;->ber:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 364
    goto :goto_0
.end method

.method private Re()V
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Lcot;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p0, Lcot;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcov;

    const-string v2, "Destroy WebView"

    invoke-direct {v1, p0, v2}, Lcov;-><init>(Lcot;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 430
    iget-object v1, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 431
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcot;->beq:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 433
    :try_start_1
    iget-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 436
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcot;->ber:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 439
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 441
    :cond_1
    return-void
.end method

.method private ub()Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 173
    :try_start_0
    iget-boolean v0, p0, Lcot;->beb:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcot;->bed:Z

    if-eqz v0, :cond_2

    .line 175
    :cond_0
    iget-object v0, p0, Lcot;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->a(Ldkt;)V

    .line 176
    iget-boolean v0, p0, Lcot;->bed:Z

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcot;->mCookies:Lgpf;

    invoke-virtual {v0}, Lgpf;->aJa()V

    iget-object v0, p0, Lcot;->mCookies:Lgpf;

    invoke-virtual {v0}, Lgpf;->sync()V

    iget-object v0, p0, Lcot;->mSettings:Lcke;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcke;->gV(Ljava/lang/String;)V

    iget-object v0, p0, Lcot;->mSettings:Lcke;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcke;->gW(Ljava/lang/String;)V

    iget-object v0, p0, Lcot;->mSettings:Lcke;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lcke;->V(J)V

    const-string v0, ""

    iput-object v0, p0, Lcot;->bee:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcot;->bef:J

    .line 183
    :cond_1
    const-string v0, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v1, "refreshing search domain"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 184
    invoke-direct {p0}, Lcot;->Df()V

    .line 185
    invoke-virtual {p0}, Lcot;->Ra()Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-direct {p0}, Lcot;->Df()V

    .line 188
    invoke-virtual {p0, v0}, Lcot;->hx(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcot;->Rb()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    invoke-direct {p0}, Lcot;->Df()V

    .line 190
    invoke-virtual {p0}, Lcot;->Rc()V

    .line 191
    invoke-direct {p0}, Lcot;->Rd()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    const-string v0, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v1, "refreshing cookies"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 193
    :goto_0
    iget-object v0, p0, Lcot;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcot;->bep:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    iget-object v1, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcot;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    iget-wide v6, p0, Lcot;->bep:J

    sub-long v2, v6, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    iget-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    iget-wide v2, p0, Lcot;->bep:J

    iget-object v4, p0, Lcot;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 200
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v1, "refresh interrupted"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 202
    invoke-direct {p0}, Lcot;->Re()V

    .line 203
    iget-object v0, p0, Lcot;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->d(Ldkt;)V

    .line 205
    :goto_1
    return-object v8

    .line 179
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcot;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->c(Ldkt;)Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 181
    invoke-direct {p0}, Lcot;->Re()V

    .line 203
    iget-object v0, p0, Lcot;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->d(Ldkt;)V

    goto :goto_1

    .line 194
    :cond_3
    :try_start_5
    invoke-direct {p0}, Lcot;->Df()V

    .line 195
    iget-boolean v0, p0, Lcot;->beo:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcot;->ben:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcot;->mCookies:Lgpf;

    invoke-virtual {v0}, Lgpf;->sync()V

    iget-object v0, p0, Lcot;->mSettings:Lcke;

    iget-object v1, p0, Lcot;->bec:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcke;->gV(Ljava/lang/String;)V

    iget-object v0, p0, Lcot;->mSettings:Lcke;

    iget-object v1, p0, Lcot;->bel:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcke;->gW(Ljava/lang/String;)V

    iget-object v0, p0, Lcot;->mSettings:Lcke;

    iget-object v1, p0, Lcot;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcot;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Il()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lcke;->V(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 202
    :cond_4
    invoke-direct {p0}, Lcot;->Re()V

    .line 203
    iget-object v0, p0, Lcot;->mCookiesLock:Ldkq;

    invoke-virtual {v0, p0}, Ldkq;->d(Ldkt;)V

    goto :goto_1

    .line 202
    :catchall_1
    move-exception v0

    invoke-direct {p0}, Lcot;->Re()V

    .line 203
    iget-object v1, p0, Lcot;->mCookiesLock:Ldkq;

    invoke-virtual {v1, p0}, Ldkq;->d(Ldkt;)V

    throw v0
.end method


# virtual methods
.method public final Na()V
    .locals 2

    .prologue
    .line 153
    iget-object v1, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcot;->ber:Z

    .line 155
    iget-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 156
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ra()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 231
    const/4 v0, 0x0

    .line 233
    :try_start_0
    iget-object v1, p0, Lcot;->mUrlHelper:Lcpn;

    invoke-virtual {v1}, Lcpn;->RD()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v1

    iput-object v1, p0, Lcot;->beg:Lcom/google/android/search/shared/api/UriRequest;

    .line 234
    new-instance v1, Ldlb;

    iget-object v2, p0, Lcot;->beg:Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcot;->beg:Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v3}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 237
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldlb;->setUseCaches(Z)V

    .line 238
    iget-object v2, p0, Lcot;->mHttpHelper:Ldkx;

    const/16 v3, 0x9

    invoke-virtual {v2, v1, v3}, Ldkx;->b(Ldlb;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 243
    :goto_0
    return-object v0

    .line 239
    :catch_0
    move-exception v1

    .line 240
    const-string v2, "Search.RefreshSearchDomainAndCookiesTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Search parameters fetch failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v2, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final Rb()Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 282
    iget-object v1, p0, Lcot;->aqW:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    const-string v1, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v2, "Search parameters didn\'t specify domain"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 289
    :goto_0
    return v0

    .line 285
    :cond_0
    iget-object v1, p0, Lcot;->beh:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    const-string v1, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v2, "Search parameters didn\'t specify country code"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 289
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final Rc()V
    .locals 5

    .prologue
    .line 295
    iget-boolean v0, p0, Lcot;->bej:Z

    if-eqz v0, :cond_1

    const-string v0, "https"

    .line 297
    :goto_0
    iget-object v1, p0, Lcot;->mSettings:Lcke;

    iget-object v2, p0, Lcot;->aqW:Ljava/lang/String;

    iget-object v3, p0, Lcot;->beh:Ljava/lang/String;

    iget-object v4, p0, Lcot;->bei:Ljava/lang/String;

    invoke-static {v4}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v2, v3, v4}, Lcke;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcot;->beg:Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 302
    iget-object v1, p0, Lcot;->mUrlHelper:Lcpn;

    invoke-virtual {v1}, Lcpn;->Rq()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 309
    iget-object v1, p0, Lcot;->mSettings:Lcke;

    iget-object v2, p0, Lcot;->bek:Ljava/lang/String;

    invoke-static {v2}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcke;->gV(Ljava/lang/String;)V

    .line 310
    iget-object v1, p0, Lcot;->mSettings:Lcke;

    invoke-interface {v1, v0}, Lcke;->gW(Ljava/lang/String;)V

    .line 322
    :cond_0
    :goto_1
    return-void

    .line 295
    :cond_1
    const-string v0, "http"

    goto :goto_0

    .line 311
    :cond_2
    iget-object v1, p0, Lcot;->mUrlHelper:Lcpn;

    invoke-static {}, Lcpn;->Rv()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcot;->mSettings:Lcke;

    iget-object v1, p0, Lcot;->bek:Ljava/lang/String;

    invoke-static {v1}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcke;->gV(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcot;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method final h(ZZ)V
    .locals 6

    .prologue
    .line 446
    iget-object v1, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 447
    :try_start_0
    iput-boolean p1, p0, Lcot;->ben:Z

    .line 448
    iput-boolean p2, p0, Lcot;->beo:Z

    .line 449
    iget-object v0, p0, Lcot;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v2

    .line 450
    if-eqz p2, :cond_0

    .line 452
    iput-wide v2, p0, Lcot;->bep:J

    .line 460
    :goto_0
    iget-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 461
    monitor-exit v1

    return-void

    .line 453
    :cond_0
    if-eqz p1, :cond_1

    .line 455
    iget-object v0, p0, Lcot;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->In()I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcot;->bep:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 458
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcot;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Im()I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcot;->bep:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final hx(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v0, 0x0

    .line 248
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    const-string v1, "Search.RefreshSearchDomainAndCookiesTask"

    const-string v2, "Search parameters fetch failed"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v5, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 276
    :goto_0
    return v0

    .line 253
    :cond_0
    :try_start_0
    new-instance v1, Landroid/util/JsonReader;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 254
    invoke-virtual {v1}, Landroid/util/JsonReader;->beginObject()V

    .line 255
    :goto_1
    invoke-virtual {v1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    if-eq v2, v3, :cond_6

    .line 256
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 257
    const-string v3, "domain"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 258
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcot;->aqW:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 274
    :catch_0
    move-exception v1

    .line 275
    const-string v2, "Search.RefreshSearchDomainAndCookiesTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Search parameters parsing failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v5, v2, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    :cond_1
    :try_start_1
    const-string v3, "countryCode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 260
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcot;->beh:Ljava/lang/String;

    goto :goto_1

    .line 261
    :cond_2
    const-string v3, "userLang"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 262
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcot;->bei:Ljava/lang/String;

    goto :goto_1

    .line 263
    :cond_3
    const-string v3, "loggedInUser"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 264
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcot;->bek:Ljava/lang/String;

    goto :goto_1

    .line 265
    :cond_4
    const-string v3, "useSsl"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 266
    invoke-virtual {v1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v2

    iput-boolean v2, p0, Lcot;->bej:Z

    goto :goto_1

    .line 268
    :cond_5
    invoke-virtual {v1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_1

    .line 271
    :cond_6
    invoke-virtual {v1}, Landroid/util/JsonReader;->endObject()V

    .line 272
    invoke-virtual {v1}, Landroid/util/JsonReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 273
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method final l(Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 370
    iget-object v1, p0, Lcot;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 371
    :try_start_0
    iget-boolean v0, p0, Lcot;->ber:Z

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcot;->mFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJD()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcot;->mWebView:Landroid/webkit/WebView;

    .line 374
    iget-object v0, p0, Lcot;->aUn:Lckg;

    iget-object v2, p0, Lcot;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Lckg;->a(Landroid/webkit/WebView;)V

    .line 375
    iget-object v0, p0, Lcot;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcow;

    invoke-direct {v2, p0}, Lcow;-><init>(Lcot;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 376
    iget-object v0, p0, Lcot;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    iget-object v2, p0, Lcot;->aUn:Lckg;

    invoke-virtual {v2}, Lckg;->OJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcot;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcot;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Im()I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcot;->bep:J

    .line 383
    iget-object v0, p0, Lcot;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 385
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcot;->bem:Z

    .line 386
    iget-object v0, p0, Lcot;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 387
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 473
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
