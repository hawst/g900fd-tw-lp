.class public final Lgdc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdb;


# instance fields
.field private bbe:Ljava/lang/String;

.field private cFS:Lglx;

.field private final cGy:Lgcx;

.field final cGz:Leqx;

.field private final mAudioController:Lgem;

.field final mAudioRecorder:Lges;

.field private final mRecognitionDispatcher:Lgil;

.field private final mSpeechLibFactory:Lgdn;


# direct methods
.method public constructor <init>(Lgem;Lges;Lgil;Lgcx;Lgdn;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "RecognizerImpl"

    sget-object v1, Lgdf;->cGD:Lgdf;

    invoke-static {v0, v1}, Leqx;->a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgdf;->cGD:Lgdf;

    new-array v2, v4, [Lgdf;

    sget-object v3, Lgdf;->cGE:Lgdf;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgdf;->cGE:Lgdf;

    new-array v2, v4, [Lgdf;

    sget-object v3, Lgdf;->cGD:Lgdf;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgdf;->cGE:Lgdf;

    new-array v2, v4, [Lgdf;

    sget-object v3, Lgdf;->cGE:Lgdf;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgdf;->cGE:Lgdf;

    new-array v2, v4, [Lgdf;

    sget-object v3, Lgdf;->cGF:Lgdf;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgdf;->cGF:Lgdf;

    new-array v2, v4, [Lgdf;

    sget-object v3, Lgdf;->cGD:Lgdf;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    iput-boolean v4, v0, Leqy;->chN:Z

    iput-boolean v4, v0, Leqy;->chJ:Z

    invoke-virtual {v0}, Leqy;->avx()Leqx;

    move-result-object v0

    iput-object v0, p0, Lgdc;->cGz:Leqx;

    .line 115
    iput-object p1, p0, Lgdc;->mAudioController:Lgem;

    .line 116
    iput-object p2, p0, Lgdc;->mAudioRecorder:Lges;

    .line 117
    iput-object p3, p0, Lgdc;->mRecognitionDispatcher:Lgil;

    .line 118
    iput-object p4, p0, Lgdc;->cGy:Lgcx;

    .line 119
    iput-object p5, p0, Lgdc;->mSpeechLibFactory:Lgdn;

    .line 120
    return-void
.end method

.method public static a(Ljava/util/concurrent/ExecutorService;Lgem;Lgdn;)Lgdb;
    .locals 6

    .prologue
    .line 124
    new-instance v0, Lgdc;

    new-instance v2, Lges;

    invoke-direct {v2}, Lges;-><init>()V

    new-instance v3, Lgil;

    invoke-direct {v3, p0, p2}, Lgil;-><init>(Ljava/util/concurrent/ExecutorService;Lgdn;)V

    invoke-interface {p2}, Lgdn;->aEZ()Lgcx;

    move-result-object v4

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lgdc;-><init>(Lgem;Lges;Lgil;Lgcx;Lgdn;)V

    invoke-static {p0, v0}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdb;

    return-object v0
.end method

.method private static a(Lglx;Leiq;)V
    .locals 0

    .prologue
    .line 234
    invoke-static {p1}, Lege;->a(Lefr;)V

    .line 235
    invoke-interface {p0, p1}, Lglx;->c(Leiq;)V

    .line 236
    return-void
.end method

.method private a(Lgnj;Lglx;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 203
    iget-object v1, p0, Lgdc;->cGz:Leqx;

    sget-object v2, Lgdf;->cGD:Lgdf;

    invoke-virtual {v1, v2}, Leqx;->c(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205
    :try_start_0
    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    new-instance v2, Legq;

    iget-object v3, p0, Lgdc;->bbe:Ljava/lang/String;

    invoke-direct {v2, v3}, Legq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 208
    invoke-virtual {p1}, Lgnj;->getMode()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 211
    const-string v1, "RecognizerImpl"

    const-string v2, "Another recognition in progress, not starting hotword recognition."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 212
    new-instance v1, Leic;

    const-string v2, "Another recognition in progress"

    invoke-direct {v1, v2}, Leic;-><init>(Ljava/lang/String;)V

    invoke-static {p2, v1}, Lgdc;->a(Lglx;Leiq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v4}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 226
    :goto_0
    return v0

    .line 217
    :cond_0
    :try_start_1
    const-string v0, "RecognizerImpl"

    const-string v1, "Multiple recognitions in progress, the first will be cancelled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 218
    iget-object v0, p0, Lgdc;->cFS:Lglx;

    new-instance v1, Leip;

    invoke-direct {v1}, Leip;-><init>()V

    invoke-static {v0, v1}, Lgdc;->a(Lglx;Leiq;)V

    .line 220
    invoke-direct {p0}, Lgdc;->aEY()V

    .line 221
    iget-object v0, p0, Lgdc;->cGz:Leqx;

    sget-object v1, Lgdf;->cGD:Lgdf;

    invoke-virtual {v0, v1}, Leqx;->d(Ljava/lang/Enum;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 223
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v4}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 226
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v4}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method

.method private aEY()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 289
    iget-object v0, p0, Lgdc;->cGz:Leqx;

    sget-object v1, Lgdf;->cGD:Lgdf;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    .line 290
    iput-object v2, p0, Lgdc;->cFS:Lglx;

    .line 291
    iput-object v2, p0, Lgdc;->bbe:Ljava/lang/String;

    .line 292
    iget-object v0, p0, Lgdc;->mAudioController:Lgem;

    invoke-virtual {v0}, Lgem;->shutdown()V

    .line 293
    iget-object v0, p0, Lgdc;->mAudioRecorder:Lges;

    invoke-virtual {v0}, Lges;->aFq()V

    .line 294
    iget-object v0, p0, Lgdc;->mRecognitionDispatcher:Lgil;

    invoke-virtual {v0}, Lgil;->cancel()V

    .line 295
    return-void
.end method

.method private mw(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 324
    if-eqz p1, :cond_0

    iget-object v1, p0, Lgdc;->bbe:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 325
    :cond_0
    const-string v1, "RecognizerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Supplied requestId ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is not the one that is currently active ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lgdc;->bbe:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 329
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V
    .locals 15
    .param p4    # Lgfb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136
    const-class v2, Lglx;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-static {v0, v2, v1}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lglx;

    .line 138
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v10}, Lgdc;->a(Lgnj;Lglx;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 198
    :goto_0
    return-void

    .line 143
    :cond_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lgdc;->bbe:Ljava/lang/String;

    .line 144
    sget-object v2, Lege;->bYk:Ljava/lang/ThreadLocal;

    new-instance v3, Legq;

    iget-object v4, p0, Lgdc;->bbe:Ljava/lang/String;

    invoke-direct {v3, v4}, Legq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 145
    invoke-virtual/range {p1 .. p1}, Lgnj;->getMode()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Legm;->kR(Ljava/lang/String;)Legm;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Legm;)V

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lgnj;->getMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x1

    invoke-static {v2}, Legm;->hu(I)Legm;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Legm;)V

    .line 147
    :goto_1
    iget-object v2, p0, Lgdc;->mSpeechLibFactory:Lgdn;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lgdn;->a(Lgnj;)Lgcl;

    move-result-object v12

    .line 148
    invoke-interface {v12}, Lgcl;->aEN()Ljava/util/List;

    move-result-object v13

    .line 149
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    new-instance v2, Leik;

    invoke-direct {v2}, Leik;-><init>()V

    invoke-static {v10, v2}, Lgdc;->a(Lglx;Leiq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    sget-object v2, Lege;->bYk:Ljava/lang/ThreadLocal;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    :pswitch_0
    const/4 v2, 0x5

    :try_start_1
    invoke-static {v2}, Legm;->hu(I)Legm;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Legm;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 197
    :catchall_0
    move-exception v2

    sget-object v3, Lege;->bYk:Ljava/lang/ThreadLocal;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v2

    .line 145
    :pswitch_1
    const/4 v2, 0x7

    :try_start_2
    invoke-static {v2}, Legm;->hu(I)Legm;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Legm;)V

    goto :goto_1

    :pswitch_2
    const/4 v2, 0x3

    invoke-static {v2}, Legm;->hu(I)Legm;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Legm;)V

    goto :goto_1

    :pswitch_3
    const/4 v2, 0x2

    invoke-static {v2}, Legm;->hu(I)Legm;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Legm;)V

    goto :goto_1

    .line 154
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lgnj;->aHC()Lgmx;

    move-result-object v14

    .line 158
    invoke-virtual {v14}, Lgmx;->asV()[B

    move-result-object v2

    if-eqz v2, :cond_5

    .line 159
    new-instance v2, Lgde;

    invoke-direct {v2, p0, v14}, Lgde;-><init>(Lgdc;Lgmx;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v11, v2

    .line 169
    :goto_2
    if-eqz p4, :cond_3

    .line 171
    :try_start_3
    invoke-virtual {v14}, Lgmx;->getSamplingRate()I

    move-result v4

    .line 172
    iget-object v2, p0, Lgdc;->mAudioRecorder:Lges;

    invoke-interface {v11}, Lger;->df()Ljava/io/InputStream;

    move-result-object v3

    mul-int/lit8 v5, v4, 0x2

    div-int/lit16 v5, v5, 0x3e8

    mul-int/lit8 v5, v5, 0x14

    invoke-virtual/range {p1 .. p1}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lgnj;->aHT()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lgnj;->aHU()I

    move-result v9

    move-object/from16 v6, p4

    invoke-virtual/range {v2 .. v9}, Lges;->a(Ljava/io/InputStream;IILgfb;Ljava/lang/String;II)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 187
    :cond_3
    :try_start_4
    iget-object v2, p0, Lgdc;->cGz:Leqx;

    sget-object v3, Lgdf;->cGE:Lgdf;

    invoke-virtual {v2, v3}, Leqx;->a(Ljava/lang/Enum;)V

    .line 188
    invoke-virtual {v14}, Lgmx;->asV()[B

    move-result-object v2

    if-nez v2, :cond_4

    .line 189
    iget-object v2, p0, Lgdc;->mAudioController:Lgem;

    invoke-virtual {v14}, Lgmx;->aHw()Z

    move-result v3

    invoke-virtual {v2, v3, v10}, Lgem;->a(ZLglw;)V

    .line 192
    :cond_4
    iput-object v10, p0, Lgdc;->cFS:Lglx;

    .line 193
    iget-object v2, p0, Lgdc;->mRecognitionDispatcher:Lgil;

    iget-object v3, p0, Lgdc;->cGy:Lgcx;

    invoke-interface {v3, v13}, Lgcx;->ah(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lgdc;->bbe:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lgdc;->mt(Ljava/lang/String;)Lgcd;

    move-result-object v7

    move-object v4, v11

    move-object/from16 v5, p1

    move-object v6, v12

    move-object v8, v10

    invoke-virtual/range {v2 .. v8}, Lgil;->a(Ljava/util/Collection;Lger;Lgnj;Lgcl;Lgcd;Lglx;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 197
    sget-object v2, Lege;->bYk:Ljava/lang/ThreadLocal;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 166
    :cond_5
    :try_start_5
    iget-object v2, p0, Lgdc;->mAudioController:Lgem;

    invoke-virtual {v2, v14}, Lgem;->a(Lgmx;)Lger;

    move-result-object v2

    move-object v11, v2

    goto :goto_2

    .line 179
    :catch_0
    move-exception v2

    .line 180
    new-instance v3, Lehw;

    const v4, 0x60001

    invoke-direct {v3, v2, v4}, Lehw;-><init>(Ljava/lang/Throwable;I)V

    invoke-static {v10, v3}, Lgdc;->a(Lglx;Leiq;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 197
    sget-object v2, Lege;->bYk:Ljava/lang/ThreadLocal;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 145
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final mr(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 241
    iget-object v0, p0, Lgdc;->cGz:Leqx;

    sget-object v1, Lgdf;->cGE:Lgdf;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    :try_start_0
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    new-instance v1, Legq;

    invoke-direct {v1, p1}, Legq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 244
    invoke-virtual {p0, p1}, Lgdc;->mv(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 249
    :cond_0
    return-void

    .line 246
    :catchall_0
    move-exception v0

    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method

.method public final ms(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 254
    iget-object v0, p0, Lgdc;->cGz:Leqx;

    sget-object v1, Lgdf;->cGD:Lgdf;

    invoke-virtual {v0, v1}, Leqx;->c(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    invoke-direct {p0, p1}, Lgdc;->mw(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    :try_start_0
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    new-instance v1, Legq;

    invoke-direct {v1, p1}, Legq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 260
    iget-object v0, p0, Lgdc;->cFS:Lglx;

    invoke-interface {v0}, Lglx;->aEM()V

    .line 261
    invoke-virtual {p0, p1}, Lgdc;->mu(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method

.method public final mt(Ljava/lang/String;)Lgcd;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lgdd;

    invoke-direct {v0, p0, p1}, Lgdd;-><init>(Lgdc;Ljava/lang/String;)V

    return-object v0
.end method

.method final mu(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 278
    invoke-direct {p0, p1}, Lgdc;->mw(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 281
    :cond_0
    invoke-direct {p0}, Lgdc;->aEY()V

    goto :goto_0
.end method

.method final mv(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lgdc;->mw(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lgdc;->cGz:Leqx;

    sget-object v1, Lgdf;->cGE:Lgdf;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdc;->cGz:Leqx;

    sget-object v1, Lgdf;->cGF:Lgdf;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    iget-object v0, p0, Lgdc;->mAudioController:Lgem;

    invoke-virtual {v0}, Lgem;->stopListening()V

    iget-object v0, p0, Lgdc;->mAudioRecorder:Lges;

    invoke-virtual {v0}, Lges;->aFq()V

    goto :goto_0
.end method
