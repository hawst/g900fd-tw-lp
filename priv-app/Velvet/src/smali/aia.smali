.class public abstract Laia;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static D(Landroid/content/Context;)Laia;
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Laid;

    invoke-direct {v0, p0}, Laid;-><init>(Landroid/content/Context;)V

    .line 37
    :goto_0
    return-object v0

    .line 34
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 35
    new-instance v0, Laic;

    invoke-direct {v0, p0}, Laic;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Laib;

    invoke-direct {v0}, Laib;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/graphics/drawable/Drawable;Lahz;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;
.end method

.method public abstract c(Lahz;)J
.end method

.method public abstract getUserProfiles()Ljava/util/List;
.end method

.method public abstract m(J)Lahz;
.end method
