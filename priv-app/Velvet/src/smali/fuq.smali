.class public final Lfuq;
.super Lfuz;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 37
    iput-object p4, p0, Lfuq;->mPhotoWithAttributionDecorator:Lgbd;

    .line 38
    return-void
.end method

.method private static c(Landroid/content/pm/PackageManager;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 123
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 124
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 125
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 42
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 43
    iget-object v2, v0, Lizj;->dTa:Liwp;

    .line 44
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 45
    new-instance v4, Lfrs;

    invoke-virtual {v2}, Liwp;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p1, v5, v0}, Lfrs;-><init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V

    invoke-virtual {v2}, Liwp;->aVj()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lfrs;->cBj:Ljava/lang/String;

    iget-object v0, p0, Lfuq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfuq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhO()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfuq;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhN()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lfrs;->cBi:Ljava/lang/String;

    iput-boolean v6, v4, Lfrs;->cBf:Z

    :cond_0
    :goto_0
    iget-object v0, v2, Liwp;->aiX:Ljcn;

    iget-object v5, p0, Lfuq;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-virtual {v4, v0, v5}, Lfrs;->a(Ljcn;Lgbd;)Lfrs;

    const/16 v0, 0x21

    invoke-virtual {v4, v0}, Lfrs;->jz(I)Lanh;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v0, v2, Liwp;->dMj:Lixx;

    if-eqz v0, :cond_1

    iget-object v0, v2, Liwp;->dMj:Lixx;

    invoke-virtual {v0}, Lixx;->baX()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    :goto_1
    invoke-static {v3, v1}, Lfuq;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 48
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 49
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 50
    invoke-virtual {v1, v6}, Lang;->aS(Z)Lang;

    .line 51
    return-object v1

    .line 45
    :cond_2
    invoke-virtual {v2}, Liwp;->aZv()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Liwp;->qy()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lfrs;->cBi:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 46
    :cond_4
    new-instance v0, Laoj;

    invoke-direct {v0}, Laoj;-><init>()V

    iget-object v1, v2, Liwp;->dMj:Lixx;

    invoke-virtual {v1}, Lixx;->avd()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lfuq;->c(Landroid/content/pm/PackageManager;)Ljava/util/Set;

    move-result-object v1

    iget-object v4, v2, Liwp;->dMj:Lixx;

    invoke-virtual {v4}, Lixx;->bbe()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v2, Liwp;->dMj:Lixx;

    invoke-virtual {v1}, Lixx;->bbf()Lixx;

    :cond_5
    const v1, 0x7f0a035d

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v1, Lfsa;

    const/16 v4, 0x26

    invoke-direct {v1, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f0201bb

    invoke-virtual {v1, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    iget-object v2, v2, Liwp;->dMj:Lixx;

    invoke-virtual {v1, v2}, Lfsa;->b(Lixx;)Lani;

    move-result-object v2

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Lanh;->cm(I)Lanh;

    iput-object v0, v1, Lanh;->agz:Laoj;

    iput-object v2, v1, Lanh;->ahs:Lani;

    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v1, Lanh;->ahu:Lizj;

    invoke-virtual {v1, v6}, Lanh;->aW(Z)Lanh;

    invoke-virtual {v1, v6}, Lanh;->aX(Z)Lanh;

    goto/16 :goto_1
.end method

.method public final aW(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTa:Liwp;

    .line 61
    iget-object v1, v0, Liwp;->aiX:Ljcn;

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lfuq;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v0, v0, Liwp;->aiX:Ljcn;

    const v2, 0x7f0d0220

    const v3, 0x7f0d0214

    invoke-virtual {v1, p1, v0, v2, v3}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
