.class public Ldgy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bxB:Ldgz;

.field private bxC:Z

.field private bxD:Z


# direct methods
.method constructor <init>(Ldgz;Z)V
    .locals 0

    .prologue
    .line 421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422
    iput-object p1, p0, Ldgy;->bxB:Ldgz;

    .line 423
    iput-boolean p2, p0, Ldgy;->bxC:Z

    .line 424
    return-void
.end method


# virtual methods
.method public a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 468
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    iget-object v2, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v2}, Ldha;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v0, "Search.IcingConnection"

    const-string v2, "getDocuments when not connected."

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    move-object v0, v1

    :cond_1
    return-object v0

    :cond_2
    iget-object v0, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0, p1, p2, p3}, Ldha;->a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "Search.IcingConnection"

    const-string v2, "Got null results from getDocuments."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentResults;->hasError()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Search.IcingConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got error for getDocuments: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentResults;->wW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 484
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    iget-object v2, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v2}, Ldha;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v0, "Search.IcingConnection"

    const-string v2, "query when not connected."

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    move-object v0, v1

    :cond_1
    return-object v0

    :cond_2
    iget-object v0, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0, p1, p2}, Ldha;->a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "Search.IcingConnection"

    const-string v2, "Got null results from query."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->hasError()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Search.IcingConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got error for search: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->wW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 476
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    iget-object v1, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v1}, Ldha;->isConnected()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "Search.IcingConnection"

    const-string v1, "query when not connected."

    new-array v2, v7, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    move-object v0, v6

    :cond_1
    return-object v0

    :cond_2
    iget-object v0, v0, Ldgz;->bxE:Ldha;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Ldha;->a(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "Search.IcingConnection"

    const-string v1, "Got null results from query."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Search.IcingConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got error for search: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->wW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a([BZ)Z
    .locals 1

    .prologue
    .line 490
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    invoke-virtual {v0, p1, p2}, Ldgz;->a([BZ)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    invoke-virtual {v0, p1, p2, p3}, Ldgz;->b(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public connect()V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldgy;->bxD:Z

    .line 438
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    invoke-virtual {v0}, Ldgz;->abX()V

    .line 441
    :cond_0
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldgy;->bxD:Z

    .line 445
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    iget-object v1, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v1}, Ldha;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->disconnect()V

    .line 448
    :cond_0
    return-void
.end method

.method public du(Z)V
    .locals 1

    .prologue
    .line 428
    iget-boolean v0, p0, Ldgy;->bxC:Z

    if-eq v0, p1, :cond_0

    .line 429
    iput-boolean p1, p0, Ldgy;->bxC:Z

    .line 430
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Ldgy;->bxD:Z

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    invoke-virtual {v0}, Ldgz;->abX()V

    .line 434
    :cond_0
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Ldgy;->bxB:Ldgz;

    iget-object v0, v0, Ldgz;->bxE:Ldha;

    invoke-virtual {v0}, Ldha;->isConnected()Z

    move-result v0

    return v0
.end method

.method public l(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 451
    iget-boolean v1, p0, Ldgy;->bxC:Z

    if-eqz v1, :cond_0

    .line 452
    iget-object v1, p0, Ldgy;->bxB:Ldgz;

    iget-object v2, v1, Ldgz;->bxE:Ldha;

    invoke-virtual {v2}, Ldha;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "Search.IcingConnection"

    const-string v2, "setIncludeInGlobalSearch when not connected."

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v1, v2, v0}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 453
    :goto_0
    const/4 v0, 0x1

    .line 455
    :cond_0
    return v0

    .line 452
    :cond_1
    iget-object v0, v1, Ldgz;->bxE:Ldha;

    invoke-virtual {v0, p1, p2}, Ldha;->c(Ljava/lang/String;Z)V

    goto :goto_0
.end method
