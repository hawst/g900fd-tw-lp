.class public Lenh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leqp;


# instance fields
.field private cfS:Ljava/util/HashMap;

.field private cfT:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lenh;->mContext:Landroid/content/Context;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lenh;->cfT:Z

    .line 43
    return-void
.end method

.method private B(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    .line 153
    iget-object v1, p0, Lenh;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 155
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected final A(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 137
    iget-object v0, p0, Lenh;->cfS:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lenh;->cfS:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 140
    iget-object v0, p0, Lenh;->cfS:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v0, "ContextIntentStarter"

    const-string v2, "Package changed: %s -> %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object p1, v1

    .line 145
    :cond_0
    return-object p1
.end method

.method public a(Landroid/content/Intent;Ljava/lang/SecurityException;)V
    .locals 3

    .prologue
    .line 84
    const-string v0, "ContextIntentStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot start activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p2, v1, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method public final aC(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    iget-object v0, p0, Lenh;->cfS:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lenh;->cfS:Ljava/util/HashMap;

    .line 129
    :cond_0
    iget-object v0, p0, Lenh;->cfS:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_1
    return-void
.end method

.method public varargs b([Landroid/content/Intent;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 48
    array-length v5, p1

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, p1, v4

    .line 49
    invoke-virtual {p0, v2}, Lenh;->z(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 50
    const-string v2, "ContextIntentStarter"

    const-string v3, "Can\'t use startActivityForResult."

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 48
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 54
    :cond_0
    :try_start_0
    invoke-virtual {p0, v2}, Lenh;->A(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    .line 55
    iget-object v3, p0, Lenh;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lenh;->y(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 65
    :goto_2
    return v0

    .line 58
    :catch_0
    move-exception v3

    const-string v3, "ContextIntentStarter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "No activity found for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v6, v1, [Ljava/lang/Object;

    const/4 v7, 0x4

    invoke-static {v7, v3, v2, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 59
    :catch_1
    move-exception v3

    move-object v8, v3

    move-object v3, v2

    move-object v2, v8

    .line 60
    invoke-virtual {p0, v3, v2}, Lenh;->a(Landroid/content/Intent;Ljava/lang/SecurityException;)V

    goto :goto_1

    .line 63
    :cond_1
    const-string v2, "ContextIntentStarter"

    const-string v3, "No activity found for any of the %d intents"

    new-array v0, v0, [Ljava/lang/Object;

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-static {v2, v3, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {p0, p1}, Lenh;->e([Landroid/content/Intent;)V

    move v0, v1

    .line 65
    goto :goto_2
.end method

.method public final varargs c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lenh;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lepd;->a(Landroid/content/pm/PackageManager;[Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs e([Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 74
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 75
    const-string v4, "com.google.android.shared.util.SimpleIntentStarter.ERROR_TOAST_ID"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 76
    if-eqz v3, :cond_1

    .line 77
    iget-object v0, p0, Lenh;->mContext:Landroid/content/Context;

    invoke-static {v0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 81
    :cond_0
    return-void

    .line 74
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final fd(Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, Lenh;->cfT:Z

    .line 105
    return-void
.end method

.method protected final y(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-direct {p0, p1}, Lenh;->B(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_2

    .line 90
    const-string v1, "com.google.android.shared.util.SimpleIntentStarter.USE_FAST_TRANSITION"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    iget-boolean v0, p0, Lenh;->cfT:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lenh;->mContext:Landroid/content/Context;

    const v1, 0x7f050004

    const v2, 0x7f050005

    invoke-static {v0, v1, v2}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p0, Lenh;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v2}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_1
    const-string v1, "com.google.android.shared.util.SimpleIntentStarter.USE_NO_TRANSITION"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lenh;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v2}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 100
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final z(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-direct {p0, p1}, Lenh;->B(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    .line 114
    if-eqz v1, :cond_0

    const-string v2, "com.google.android.shared.util.SimpleIntentStarter.START_ACTIVITY_FOR_RESULT"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    const-string v0, "com.google.android.shared.util.SimpleIntentStarter.START_ACTIVITY_FOR_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x1

    .line 118
    :cond_0
    return v0
.end method
