.class public final Ldrr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic bHA:Lcom/google/android/search/searchplate/RecognizerView;

.field private synthetic bHB:I


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/RecognizerView;I)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Ldrr;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    iput p2, p0, Ldrr;->bHB:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 244
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 246
    iget-object v1, p0, Ldrr;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    iget v2, p0, Ldrr;->bHB:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v0, v3

    iget v3, p0, Ldrr;->bHB:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setTranslationY(F)V

    .line 247
    return-void
.end method
