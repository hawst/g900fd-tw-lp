.class final Lima;
.super Ljava/util/AbstractList;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private dCC:Lifg;

.field private dEk:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Lifg;)V
    .locals 1

    .prologue
    .line 443
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 444
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lima;->dEk:Ljava/util/List;

    .line 445
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifg;

    iput-object v0, p0, Lima;->dCC:Lifg;

    .line 446
    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lima;->dEk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 449
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lima;->dCC:Lifg;

    iget-object v1, p0, Lima;->dEk:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lifg;->as(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lima;->dEk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lima;->dCC:Lifg;

    iget-object v1, p0, Lima;->dEk:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lifg;->as(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lima;->dEk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
