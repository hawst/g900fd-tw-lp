.class public final Ljbv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public dMF:[Liyg;

.field public dMM:Ljcn;

.field private dMY:I

.field private dOA:J

.field private dOB:Ljava/lang/String;

.field public dOp:Ljck;

.field private dOs:Ljava/lang/String;

.field public dOz:Ljbg;

.field private dXz:Ljava/lang/String;

.field public dZq:[Lizt;

.field public dZr:Ljbp;

.field private dZs:J

.field private dZt:I

.field private dZu:Ljava/lang/String;

.field private dZv:Ljava/lang/String;

.field public dZw:[Lizu;

.field private dZx:J

.field private dZy:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v2, p0, Ljbv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbv;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbv;->dXz:Ljava/lang/String;

    invoke-static {}, Lizt;->bdw()[Lizt;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dZq:[Lizt;

    iput-object v1, p0, Ljbv;->dZr:Ljbp;

    iput-wide v4, p0, Ljbv;->dZs:J

    iput-wide v4, p0, Ljbv;->dOA:J

    invoke-static {}, Liyg;->bbv()[Liyg;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dMF:[Liyg;

    const-string v0, ""

    iput-object v0, p0, Ljbv;->dOs:Ljava/lang/String;

    iput v2, p0, Ljbv;->dZt:I

    iput-object v1, p0, Ljbv;->dMM:Ljcn;

    iput-object v1, p0, Ljbv;->dOz:Ljbg;

    iput v2, p0, Ljbv;->dMY:I

    const-string v0, ""

    iput-object v0, p0, Ljbv;->dOB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbv;->dZu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbv;->dZv:Ljava/lang/String;

    invoke-static {}, Lizu;->bdx()[Lizu;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dZw:[Lizu;

    iput-wide v4, p0, Ljbv;->dZx:J

    const-string v0, ""

    iput-object v0, p0, Ljbv;->dZy:Ljava/lang/String;

    iput-object v1, p0, Ljbv;->dOp:Ljck;

    iput-object v1, p0, Ljbv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbv;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->afh:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dXz:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbv;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbv;->dZq:[Lizt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lizt;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljbv;->dZq:[Lizt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lizt;

    invoke-direct {v3}, Lizt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljbv;->dZq:[Lizt;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lizt;

    invoke-direct {v3}, Lizt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbv;->dZq:[Lizt;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljbv;->dZr:Ljbp;

    if-nez v0, :cond_4

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljbv;->dZr:Ljbp;

    :cond_4
    iget-object v0, p0, Ljbv;->dZr:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbv;->dZs:J

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbv;->dOA:J

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbv;->dMF:[Liyg;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Liyg;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljbv;->dMF:[Liyg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljbv;->dMF:[Liyg;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbv;->dMF:[Liyg;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dOs:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbv;->dZt:I

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljbv;->dMM:Ljcn;

    if-nez v0, :cond_8

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbv;->dMM:Ljcn;

    :cond_8
    iget-object v0, p0, Ljbv;->dMM:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljbv;->dOz:Ljbg;

    if-nez v0, :cond_9

    new-instance v0, Ljbg;

    invoke-direct {v0}, Ljbg;-><init>()V

    iput-object v0, p0, Ljbv;->dOz:Ljbg;

    :cond_9
    iget-object v0, p0, Ljbv;->dOz:Ljbg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dOB:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dZu:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dZv:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x82

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbv;->dZw:[Lizu;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lizu;

    if-eqz v0, :cond_a

    iget-object v3, p0, Ljbv;->dZw:[Lizu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    new-instance v3, Lizu;

    invoke-direct {v3}, Lizu;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    iget-object v0, p0, Ljbv;->dZw:[Lizu;

    array-length v0, v0

    goto :goto_5

    :cond_c
    new-instance v3, Lizu;

    invoke-direct {v3}, Lizu;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbv;->dZw:[Lizu;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbv;->dZx:J

    iget v0, p0, Ljbv;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbv;->dZy:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Ljbv;->dOp:Ljck;

    if-nez v0, :cond_d

    new-instance v0, Ljck;

    invoke-direct {v0}, Ljck;-><init>()V

    iput-object v0, p0, Ljbv;->dOp:Ljck;

    :cond_d
    iget-object v0, p0, Ljbv;->dOp:Ljck;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbv;->dMY:I

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljbv;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa0 -> :sswitch_13
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v2, p0, Ljbv;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v2, p0, Ljbv;->dXz:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljbv;->dZq:[Lizt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljbv;->dZq:[Lizt;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljbv;->dZq:[Lizt;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljbv;->dZq:[Lizt;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljbv;->dZr:Ljbp;

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget-object v2, p0, Ljbv;->dZr:Ljbp;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_4
    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget-wide v2, p0, Ljbv;->dZs:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_5
    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-wide v2, p0, Ljbv;->dOA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_6
    iget-object v0, p0, Ljbv;->dMF:[Liyg;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljbv;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljbv;->dMF:[Liyg;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Ljbv;->dMF:[Liyg;

    aget-object v2, v2, v0

    if-eqz v2, :cond_7

    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    const/16 v0, 0x8

    iget-object v2, p0, Ljbv;->dOs:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_9
    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    const/16 v0, 0x9

    iget v2, p0, Ljbv;->dZt:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    :cond_a
    iget-object v0, p0, Ljbv;->dMM:Ljcn;

    if-eqz v0, :cond_b

    const/16 v0, 0xa

    iget-object v2, p0, Ljbv;->dMM:Ljcn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_b
    iget-object v0, p0, Ljbv;->dOz:Ljbg;

    if-eqz v0, :cond_c

    const/16 v0, 0xb

    iget-object v2, p0, Ljbv;->dOz:Ljbg;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_c
    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_d

    const/16 v0, 0xc

    iget-object v2, p0, Ljbv;->dOB:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_d
    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_e

    const/16 v0, 0xe

    iget-object v2, p0, Ljbv;->dZu:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_e
    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_f

    const/16 v0, 0xf

    iget-object v2, p0, Ljbv;->dZv:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_f
    iget-object v0, p0, Ljbv;->dZw:[Lizu;

    if-eqz v0, :cond_11

    iget-object v0, p0, Ljbv;->dZw:[Lizu;

    array-length v0, v0

    if-lez v0, :cond_11

    :goto_2
    iget-object v0, p0, Ljbv;->dZw:[Lizu;

    array-length v0, v0

    if-ge v1, v0, :cond_11

    iget-object v0, p0, Ljbv;->dZw:[Lizu;

    aget-object v0, v0, v1

    if-eqz v0, :cond_10

    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_11
    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_12

    const/16 v0, 0x11

    iget-wide v2, p0, Ljbv;->dZx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_12
    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_13

    const/16 v0, 0x12

    iget-object v1, p0, Ljbv;->dZy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_13
    iget-object v0, p0, Ljbv;->dOp:Ljck;

    if-eqz v0, :cond_14

    const/16 v0, 0x13

    iget-object v1, p0, Ljbv;->dOp:Ljck;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_14
    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_15

    const/16 v0, 0x14

    iget v1, p0, Ljbv;->dMY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_15
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final agN()J
    .locals 2

    iget-wide v0, p0, Ljbv;->dZs:J

    return-wide v0
.end method

.method public final baL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbv;->dOs:Ljava/lang/String;

    return-object v0
.end method

.method public final baQ()J
    .locals 2

    iget-wide v0, p0, Ljbv;->dOA:J

    return-wide v0
.end method

.method public final baR()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbv;->dOB:Ljava/lang/String;

    return-object v0
.end method

.method public final bfA()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbv;->dXz:Ljava/lang/String;

    return-object v0
.end method

.method public final bfB()Z
    .locals 1

    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfC()I
    .locals 1

    iget v0, p0, Ljbv;->dZt:I

    return v0
.end method

.method public final bfD()Z
    .locals 1

    iget v0, p0, Ljbv;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfE()Z
    .locals 1

    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfF()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbv;->dZy:Ljava/lang/String;

    return-object v0
.end method

.method public final bfG()Z
    .locals 1

    iget v0, p0, Ljbv;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cI(J)Ljbv;
    .locals 1

    iput-wide p1, p0, Ljbv;->dZs:J

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbv;->aez:I

    return-object p0
.end method

.method public final cJ(J)Ljbv;
    .locals 1

    iput-wide p1, p0, Ljbv;->dOA:J

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbv;->aez:I

    return-object p0
.end method

.method public final cK(J)Ljbv;
    .locals 2

    const-wide v0, 0x13f0cce15c0L

    iput-wide v0, p0, Ljbv;->dZx:J

    iget v0, p0, Ljbv;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljbv;->aez:I

    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbv;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Ljbv;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljbv;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Ljbv;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Ljbv;->dXz:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Ljbv;->dZq:[Lizt;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljbv;->dZq:[Lizt;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ljbv;->dZq:[Lizt;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Ljbv;->dZq:[Lizt;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    :cond_4
    iget-object v2, p0, Ljbv;->dZr:Ljbp;

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    iget-object v3, p0, Ljbv;->dZr:Ljbp;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Ljbv;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_6

    const/4 v2, 0x5

    iget-wide v4, p0, Ljbv;->dZs:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget v2, p0, Ljbv;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_7

    const/4 v2, 0x6

    iget-wide v4, p0, Ljbv;->dOA:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Ljbv;->dMF:[Liyg;

    if-eqz v2, :cond_a

    iget-object v2, p0, Ljbv;->dMF:[Liyg;

    array-length v2, v2

    if-lez v2, :cond_a

    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Ljbv;->dMF:[Liyg;

    array-length v3, v3

    if-ge v0, v3, :cond_9

    iget-object v3, p0, Ljbv;->dMF:[Liyg;

    aget-object v3, v3, v0

    if-eqz v3, :cond_8

    const/4 v4, 0x7

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_9
    move v0, v2

    :cond_a
    iget v2, p0, Ljbv;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_b

    const/16 v2, 0x8

    iget-object v3, p0, Ljbv;->dOs:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Ljbv;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_c

    const/16 v2, 0x9

    iget v3, p0, Ljbv;->dZt:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Ljbv;->dMM:Ljcn;

    if-eqz v2, :cond_d

    const/16 v2, 0xa

    iget-object v3, p0, Ljbv;->dMM:Ljcn;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Ljbv;->dOz:Ljbg;

    if-eqz v2, :cond_e

    const/16 v2, 0xb

    iget-object v3, p0, Ljbv;->dOz:Ljbg;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget v2, p0, Ljbv;->aez:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_f

    const/16 v2, 0xc

    iget-object v3, p0, Ljbv;->dOB:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_f
    iget v2, p0, Ljbv;->aez:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_10

    const/16 v2, 0xe

    iget-object v3, p0, Ljbv;->dZu:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_10
    iget v2, p0, Ljbv;->aez:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_11

    const/16 v2, 0xf

    iget-object v3, p0, Ljbv;->dZv:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_11
    iget-object v2, p0, Ljbv;->dZw:[Lizu;

    if-eqz v2, :cond_13

    iget-object v2, p0, Ljbv;->dZw:[Lizu;

    array-length v2, v2

    if-lez v2, :cond_13

    :goto_2
    iget-object v2, p0, Ljbv;->dZw:[Lizu;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    iget-object v2, p0, Ljbv;->dZw:[Lizu;

    aget-object v2, v2, v1

    if-eqz v2, :cond_12

    const/16 v3, 0x10

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_13
    iget v1, p0, Ljbv;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_14

    const/16 v1, 0x11

    iget-wide v2, p0, Ljbv;->dZx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget v1, p0, Ljbv;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_15

    const/16 v1, 0x12

    iget-object v2, p0, Ljbv;->dZy:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    iget-object v1, p0, Ljbv;->dOp:Ljck;

    if-eqz v1, :cond_16

    const/16 v1, 0x13

    iget-object v2, p0, Ljbv;->dOp:Ljck;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget v1, p0, Ljbv;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_17

    const/16 v1, 0x14

    iget v2, p0, Ljbv;->dMY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    return v0
.end method

.method public final sD(Ljava/lang/String;)Ljbv;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljbv;->afh:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbv;->aez:I

    return-object p0
.end method

.method public final sE(Ljava/lang/String;)Ljbv;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljbv;->dOs:Ljava/lang/String;

    iget v0, p0, Ljbv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbv;->aez:I

    return-object p0
.end method
