.class public final Legt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private bYT:I

.field private bYU:I

.field private bYV:Landroid/view/ViewGroup;

.field private bYW:Legu;

.field private mActionCardEventLogger:Legs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581
    const/4 v0, 0x0

    iput v0, p0, Legt;->bYU:I

    return-void
.end method

.method private aoL()V
    .locals 5

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 639
    iget v0, p0, Legt;->bYT:I

    if-eqz v0, :cond_0

    .line 651
    :goto_0
    return-void

    .line 642
    :cond_0
    iget v0, p0, Legt;->bYU:I

    if-ne v0, v2, :cond_1

    .line 643
    iget-object v0, p0, Legt;->mActionCardEventLogger:Legs;

    iget-object v1, p0, Legt;->bYV:Landroid/view/ViewGroup;

    iget-object v2, p0, Legt;->bYW:Legu;

    invoke-virtual {v0, v1, v2}, Legs;->a(Landroid/view/View;Legu;)V

    .line 644
    iput v3, p0, Legt;->bYU:I

    goto :goto_0

    .line 645
    :cond_1
    iget v0, p0, Legt;->bYU:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 646
    iget-object v0, p0, Legt;->mActionCardEventLogger:Legs;

    iget-object v1, p0, Legt;->bYV:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Legs;->aF(Landroid/view/View;)V

    .line 647
    iput v3, p0, Legt;->bYU:I

    goto :goto_0

    .line 649
    :cond_2
    const-string v0, "ActionCardEventLogger"

    const-string v1, "Invalid logging mode %d"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Legt;->bYU:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Legs;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 621
    iget v0, p0, Legt;->bYU:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 622
    const/4 v0, 0x2

    iput v0, p0, Legt;->bYU:I

    .line 623
    iput-object p1, p0, Legt;->mActionCardEventLogger:Legs;

    .line 624
    iput-object p2, p0, Legt;->bYV:Landroid/view/ViewGroup;

    .line 625
    invoke-direct {p0}, Legt;->aoL()V

    .line 626
    return-void

    .line 621
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Legs;Landroid/view/ViewGroup;Legu;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 604
    iget v0, p0, Legt;->bYU:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 605
    iput v1, p0, Legt;->bYU:I

    .line 606
    iput-object p1, p0, Legt;->mActionCardEventLogger:Legs;

    .line 607
    iput-object p3, p0, Legt;->bYW:Legu;

    .line 608
    iput-object p2, p0, Legt;->bYV:Landroid/view/ViewGroup;

    .line 609
    invoke-direct {p0}, Legt;->aoL()V

    .line 610
    return-void

    .line 604
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aoK()V
    .locals 1

    .prologue
    .line 590
    iget v0, p0, Legt;->bYT:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Legt;->bYT:I

    .line 591
    return-void
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 634
    iget v0, p0, Legt;->bYT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Legt;->bYT:I

    .line 635
    invoke-direct {p0}, Legt;->aoL()V

    .line 636
    return-void
.end method
