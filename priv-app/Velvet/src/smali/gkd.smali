.class public Lgkd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cLK:[I

.field private final cNf:Ljava/util/List;

.field private final cNg:Ljava/util/Map;

.field public final cNh:Ljava/util/List;

.field public final cNi:Ljava/util/List;

.field private cNj:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cNk:Ljzp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cNl:Z


# direct methods
.method constructor <init>([I)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lgkd;->cLK:[I

    .line 53
    const/4 v0, 0x4

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgkd;->cNh:Ljava/util/List;

    .line 54
    sget v0, Lgjl;->SIZE:I

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgkd;->cNi:Ljava/util/List;

    .line 55
    sget v0, Lgjl;->SIZE:I

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgkd;->cNf:Ljava/util/List;

    .line 57
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lgkd;->cNg:Ljava/util/Map;

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Lgjl;Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lgkd;->cNf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v1, "Grammars have already been processed, cannot add another."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 182
    invoke-virtual {p3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    array-length v1, v0

    sget v2, Lgjm;->cMG:I

    if-ge v1, v2, :cond_2

    .line 187
    :cond_0
    const-string v1, "LocaleResources"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lgjm;->cMG:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " files in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 197
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    invoke-virtual {p0, p3}, Lgkd;->o(Ljava/io/File;)Ljzp;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_1

    .line 194
    iget-object v1, p0, Lgkd;->cNi:Ljava/util/List;

    new-instance v2, Lgke;

    invoke-direct {v2, p1, p2, p3, v0}, Lgke;-><init>(Lgjl;Ljava/lang/String;Ljava/io/File;Ljzp;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final aGB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lgkd;->cNj:Ljava/lang/String;

    return-object v0
.end method

.method public final aGC()Ljzp;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgkd;->cNk:Ljzp;

    return-object v0
.end method

.method public final aGD()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lgkd;->cNl:Z

    return v0
.end method

.method public final aGE()Z
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lgkd;->cNj:Ljava/lang/String;

    const-string v1, "g3_models"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aGF()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lgkd;->cNh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    const-string v0, "LocaleResources"

    const-string v1, "No paths have been added, cannot process."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    const/4 v1, 0x0

    .line 128
    sget-object v0, Lgix;->cLJ:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 130
    iget-object v0, p0, Lgkd;->cNh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 131
    invoke-virtual {p0, v0}, Lgkd;->o(Ljava/io/File;)Ljzp;

    move-result-object v5

    .line 132
    if-nez v5, :cond_2

    .line 133
    const-string v5, "LocaleResources"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unparsable metadata at: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v5, v0, v6}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 137
    :cond_2
    iget-boolean v6, p0, Lgkd;->cNl:Z

    if-nez v6, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 138
    const/4 v6, 0x1

    iput-boolean v6, p0, Lgkd;->cNl:Z

    .line 147
    :cond_3
    iget-object v6, p0, Lgkd;->cLK:[I

    const v7, 0x7fffffff

    invoke-static {v5, v6, v7}, Lgkb;->a(Ljzp;[II)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 149
    iget-object v6, p0, Lgkd;->cNk:Ljzp;

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Ljzp;->getVersion()I

    move-result v6

    iget-object v7, p0, Lgkd;->cNk:Ljzp;

    invoke-virtual {v7}, Ljzp;->getVersion()I

    move-result v7

    if-le v6, v7, :cond_9

    .line 152
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lgkd;->cNj:Ljava/lang/String;

    .line 153
    iput-object v5, p0, Lgkd;->cNk:Ljzp;

    :goto_2
    move-object v1, v0

    .line 156
    goto :goto_1

    .line 158
    :cond_5
    iget-object v0, p0, Lgkd;->cNh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 160
    iget-object v0, p0, Lgkd;->cNk:Ljzp;

    if-nez v0, :cond_6

    .line 161
    const-string v0, "LocaleResources"

    const-string v1, "No compatible language pack metadata found."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 165
    :cond_6
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 166
    if-eqz v1, :cond_0

    .line 167
    array-length v3, v1

    move v0, v2

    :goto_3
    if-ge v0, v3, :cond_0

    aget-object v2, v1, v0

    .line 168
    invoke-static {v2}, Lgjo;->j(Ljava/io/File;)Lgjo;

    move-result-object v4

    .line 169
    if-eqz v4, :cond_8

    .line 170
    iget-object v5, p0, Lgkd;->cNg:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    :cond_7
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 171
    :cond_8
    const-string v4, "hotword_prompt.txt"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 172
    invoke-virtual {p0, v2}, Lgkd;->n(Ljava/io/File;)V

    goto :goto_4

    :cond_9
    move-object v0, v1

    goto :goto_2
.end method

.method public final aGG()Z
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lgkd;->cNk:Ljzp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgkd;->cNg:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 204
    iget-object v0, p0, Lgkd;->cNi:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgke;

    .line 205
    iget-object v2, v0, Lgke;->cNp:Ljzp;

    invoke-virtual {v2}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgkd;->cNk:Ljzp;

    invoke-virtual {v3}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    iget-object v2, p0, Lgkd;->cNf:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    :cond_1
    iget-object v0, p0, Lgkd;->cNi:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 212
    const/4 v0, 0x1

    .line 215
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Lgjl;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lgkd;->cNf:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgke;

    .line 85
    iget-object v2, v0, Lgke;->cNm:Lgjl;

    if-ne v2, p1, :cond_0

    iget-object v2, v0, Lgke;->cNn:Ljava/lang/String;

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    iget-object v0, v0, Lgke;->cNo:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lgjo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lgkd;->cNg:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final m(Ljava/io/File;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lgkd;->cNk:Ljzp;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Paths have already been processed, cannot add a new path."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lgkd;->cNh:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 220
    const/4 v2, 0x0

    .line 222
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    const/16 v3, 0x64

    invoke-direct {v1, v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 225
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 233
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 228
    :goto_1
    :try_start_2
    const-string v2, "LocaleResources"

    const-string v3, "Could not open hotword prompt file."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 232
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 229
    :catch_1
    move-exception v0

    .line 230
    :goto_2
    :try_start_3
    const-string v1, "LocaleResources"

    const-string v3, "Could not read hotword prompt file."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 232
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 229
    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    .line 227
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method protected o(Ljava/io/File;)Ljzp;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 244
    new-instance v0, Ljava/io/File;

    const-string v2, "metadata"

    invoke-direct {v0, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 248
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :try_start_1
    new-instance v0, Ljzp;

    invoke-direct {v0}, Ljzp;-><init>()V

    invoke-static {v0, v2}, Leqh;->a(Ljsr;Ljava/io/InputStream;)Ljsr;

    move-result-object v0

    check-cast v0, Ljzp;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 253
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    :goto_0
    return-object v0

    .line 251
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method
