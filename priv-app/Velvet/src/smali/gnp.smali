.class public final Lgnp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cQr:Ljava/lang/StringBuilder;

.field private cQs:Lijj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Ljvw;)Lijj;
    .locals 8
    .param p1    # Ljvw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 107
    if-nez p1, :cond_0

    .line 108
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 115
    :goto_0
    return-object v0

    .line 110
    :cond_0
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v4

    iget-object v5, p1, Ljvw;->eIk:[Ljvs;

    array-length v6, v5

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_3

    aget-object v1, v5, v2

    invoke-virtual {v1}, Ljvs;->hasText()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v7, v1, Ljvs;->eHY:Ljtn;

    if-eqz v7, :cond_2

    iget-object v1, v1, Ljvs;->eHY:Ljtn;

    iget-object v1, v1, Ljtn;->eDA:[Ljtm;

    :goto_3
    invoke-static {v0, v1}, Lcom/google/android/shared/speech/Hypothesis;->a(Ljava/lang/CharSequence;[Ljtm;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v0

    invoke-virtual {v4, v0}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    const-string v0, ""

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    iget-object v0, v4, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lgnp;->cQs:Lijj;

    .line 111
    iget-object v0, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 112
    iget-object v0, p0, Lgnp;->cQs:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 113
    iget-object v1, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lgnp;->cQs:Lijj;

    invoke-virtual {v0, v3}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-virtual {v0}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    :cond_4
    iget-object v0, p0, Lgnp;->cQs:Lijj;

    goto :goto_0
.end method

.method public final aHZ()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lgnp;->cQs:Lijj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljvv;)Landroid/util/Pair;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 61
    iget-object v2, p1, Ljvv;->eIm:Ljvw;

    if-eqz v2, :cond_0

    iget-object v2, p1, Ljvv;->eIm:Ljvw;

    iget-object v3, v2, Ljvw;->eIk:[Ljvs;

    array-length v3, v3

    if-lez v3, :cond_0

    iget-object v2, v2, Ljvw;->eIk:[Ljvs;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljvs;->hasText()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    :cond_0
    iget-object v2, p1, Ljvv;->eIn:Ljvu;

    if-eqz v2, :cond_5

    .line 70
    iget-object v4, p1, Ljvv;->eIn:Ljvu;

    .line 71
    iget-object v2, v4, Ljvu;->eIb:[Ljvt;

    array-length v5, v2

    move v3, v0

    move-object v2, v1

    .line 72
    :goto_0
    if-ge v3, v5, :cond_6

    .line 73
    iget-object v6, v4, Ljvu;->eIb:[Ljvt;

    aget-object v6, v6, v3

    .line 74
    invoke-virtual {v6}, Ljvt;->hasText()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 79
    if-nez v0, :cond_3

    invoke-virtual {v6}, Ljvt;->bvg()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6}, Ljvt;->bvf()D

    move-result-wide v8

    const-wide v10, 0x3feccccccccccccdL    # 0.9

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_3

    .line 81
    if-nez v1, :cond_1

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 84
    :cond_1
    invoke-virtual {v6}, Ljvt;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 86
    :cond_3
    if-nez v2, :cond_4

    .line 87
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    :cond_4
    invoke-virtual {v6}, Ljvt;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    move-object v2, v1

    .line 96
    :cond_6
    if-nez v1, :cond_7

    iget-object v0, p0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_2
    if-nez v2, :cond_8

    const-string v0, ""

    :goto_3
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
