.class public final Ljzs;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eOp:I

.field private eOq:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1085
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1086
    iput v0, p0, Ljzs;->aez:I

    iput v0, p0, Ljzs;->eOp:I

    iput v0, p0, Ljzs;->eOq:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljzs;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzs;->eCz:I

    .line 1087
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1028
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzs;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzs;->eOp:I

    iget v0, p0, Ljzs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzs;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzs;->eOq:I

    iget v0, p0, Ljzs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzs;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1101
    iget v0, p0, Ljzs;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1102
    const/4 v0, 0x1

    iget v1, p0, Ljzs;->eOp:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1104
    :cond_0
    iget v0, p0, Ljzs;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1105
    const/4 v0, 0x2

    iget v1, p0, Ljzs;->eOq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1107
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1108
    return-void
.end method

.method public final bxr()I
    .locals 1

    .prologue
    .line 1050
    iget v0, p0, Ljzs;->eOp:I

    return v0
.end method

.method public final bxs()I
    .locals 1

    .prologue
    .line 1069
    iget v0, p0, Ljzs;->eOq:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1112
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1113
    iget v1, p0, Ljzs;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1114
    const/4 v1, 0x1

    iget v2, p0, Ljzs;->eOp:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1117
    :cond_0
    iget v1, p0, Ljzs;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1118
    const/4 v1, 0x2

    iget v2, p0, Ljzs;->eOq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1121
    :cond_1
    return v0
.end method

.method public final tB(I)Ljzs;
    .locals 1

    .prologue
    .line 1053
    iput p1, p0, Ljzs;->eOp:I

    .line 1054
    iget v0, p0, Ljzs;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzs;->aez:I

    .line 1055
    return-object p0
.end method

.method public final tC(I)Ljzs;
    .locals 1

    .prologue
    .line 1072
    const/16 v0, 0x1f4

    iput v0, p0, Ljzs;->eOq:I

    .line 1073
    iget v0, p0, Ljzs;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzs;->aez:I

    .line 1074
    return-object p0
.end method
