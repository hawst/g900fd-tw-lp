.class public final Lita;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dHq:[Lita;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 663
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 664
    const/4 v0, 0x0

    iput v0, p0, Lita;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lita;->dHr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lita;->akf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lita;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lita;->eCz:I

    .line 665
    return-void
.end method

.method public static aYB()[Lita;
    .locals 2

    .prologue
    .line 606
    sget-object v0, Lita;->dHq:[Lita;

    if-nez v0, :cond_1

    .line 607
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 609
    :try_start_0
    sget-object v0, Lita;->dHq:[Lita;

    if-nez v0, :cond_0

    .line 610
    const/4 v0, 0x0

    new-array v0, v0, [Lita;

    sput-object v0, Lita;->dHq:[Lita;

    .line 612
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    :cond_1
    sget-object v0, Lita;->dHq:[Lita;

    return-object v0

    .line 612
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 600
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lita;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lita;->dHr:Ljava/lang/String;

    iget v0, p0, Lita;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lita;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lita;->akf:Ljava/lang/String;

    iget v0, p0, Lita;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lita;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 679
    iget v0, p0, Lita;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 680
    const/4 v0, 0x1

    iget-object v1, p0, Lita;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 682
    :cond_0
    iget v0, p0, Lita;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 683
    const/4 v0, 0x2

    iget-object v1, p0, Lita;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 685
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 686
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 690
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 691
    iget v1, p0, Lita;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 692
    const/4 v1, 0x1

    iget-object v2, p0, Lita;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    :cond_0
    iget v1, p0, Lita;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 696
    const/4 v1, 0x2

    iget-object v2, p0, Lita;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_1
    return v0
.end method

.method public final pA(Ljava/lang/String;)Lita;
    .locals 1

    .prologue
    .line 647
    if-nez p1, :cond_0

    .line 648
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 650
    :cond_0
    iput-object p1, p0, Lita;->akf:Ljava/lang/String;

    .line 651
    iget v0, p0, Lita;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lita;->aez:I

    .line 652
    return-object p0
.end method

.method public final pz(Ljava/lang/String;)Lita;
    .locals 1

    .prologue
    .line 625
    if-nez p1, :cond_0

    .line 626
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 628
    :cond_0
    iput-object p1, p0, Lita;->dHr:Ljava/lang/String;

    .line 629
    iget v0, p0, Lita;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lita;->aez:I

    .line 630
    return-object p0
.end method
