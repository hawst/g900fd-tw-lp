.class public final Lbiy;
.super Lbit;


# instance fields
.field private synthetic aCW:Lbir;

.field private aDb:Landroid/os/Bundle;

.field private aDc:Landroid/os/IBinder;

.field private statusCode:I


# direct methods
.method public constructor <init>(Lbir;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    iput-object p1, p0, Lbiy;->aCW:Lbir;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbit;-><init>(Lbir;Ljava/lang/Object;)V

    iput p2, p0, Lbiy;->statusCode:I

    iput-object p3, p0, Lbiy;->aDc:Landroid/os/IBinder;

    iput-object p4, p0, Lbiy;->aDb:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected final synthetic an(Ljava/lang/Object;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    check-cast p1, Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0, v5}, Lbir;->a(Lbir;I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lbiy;->statusCode:I

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lbiy;->aDb:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbiy;->aDb:Landroid/os/Bundle;

    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :goto_1
    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-static {v2}, Lbir;->d(Lbir;)Lbiw;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-static {v2}, Lbir;->e(Lbir;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbjc;->V(Landroid/content/Context;)Lbjc;

    move-result-object v2

    iget-object v3, p0, Lbiy;->aCW:Lbir;

    invoke-virtual {v3}, Lbir;->yQ()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbiy;->aCW:Lbir;

    invoke-static {v4}, Lbir;->d(Lbir;)Lbiw;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lbjc;->b(Ljava/lang/String;Lbiw;)V

    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-static {v2, v1}, Lbir;->a(Lbir;Lbiw;)Lbiw;

    :cond_1
    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-static {v2, v5}, Lbir;->a(Lbir;I)V

    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-static {v2, v1}, Lbir;->a(Lbir;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v1, p0, Lbiy;->aCW:Lbir;

    invoke-static {v1}, Lbir;->a(Lbir;)Lbiz;

    move-result-object v1

    new-instance v2, Lbgm;

    iget v3, p0, Lbiy;->statusCode:I

    invoke-direct {v2, v3, v0}, Lbgm;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lbiz;->c(Lbgm;)V

    goto :goto_0

    :sswitch_0
    :try_start_0
    iget-object v0, p0, Lbiy;->aDc:Landroid/os/IBinder;

    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-virtual {v2}, Lbir;->yR()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    iget-object v2, p0, Lbiy;->aCW:Lbir;

    iget-object v3, p0, Lbiy;->aDc:Landroid/os/IBinder;

    invoke-virtual {v2, v3}, Lbir;->c(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v2

    invoke-static {v0, v2}, Lbir;->a(Lbir;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0}, Lbir;->c(Lbir;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbir;->a(Lbir;I)V

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0}, Lbir;->a(Lbir;)Lbiz;

    move-result-object v0

    iget-object v2, v0, Lbiz;->aDe:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v3, v0, Lbiz;->aDd:Lbjb;

    invoke-interface {v3}, Lbjb;->yE()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbiz;->m(Landroid/os/Bundle;)V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    :cond_2
    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0}, Lbir;->e(Lbir;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjc;->V(Landroid/content/Context;)Lbjc;

    move-result-object v0

    iget-object v2, p0, Lbiy;->aCW:Lbir;

    invoke-virtual {v2}, Lbir;->yQ()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbiy;->aCW:Lbir;

    invoke-static {v3}, Lbir;->d(Lbir;)Lbiw;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbjc;->b(Ljava/lang/String;Lbiw;)V

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0, v1}, Lbir;->a(Lbir;Lbiw;)Lbiw;

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0, v5}, Lbir;->a(Lbir;I)V

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0, v1}, Lbir;->a(Lbir;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0}, Lbir;->a(Lbir;)Lbiz;

    move-result-object v0

    new-instance v2, Lbgm;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v1}, Lbgm;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, v2}, Lbiz;->c(Lbgm;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lbiy;->aCW:Lbir;

    invoke-static {v0, v5}, Lbir;->a(Lbir;I)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A fatal developer error has occurred. Check the logs for further information."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method protected final yU()V
    .locals 0

    return-void
.end method
