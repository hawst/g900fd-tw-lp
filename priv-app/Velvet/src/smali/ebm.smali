.class public Lebm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lebi;


# instance fields
.field aPF:Z

.field public final ann:Lecq;

.field private bB:Z

.field final bIS:Lcom/google/android/search/searchplate/SearchPlate;

.field bTL:Lecd;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field protected final bTW:Lecc;

.field private final bUA:Z

.field private final bUc:Ljava/lang/String;

.field final bUd:Landroid/content/Context;

.field final bUe:Lebc;

.field protected final bUf:Lcom/google/android/search/shared/ui/ReverseDrawRestrictedLayout;

.field protected final bUg:Landroid/widget/FrameLayout;

.field protected final bUh:Landroid/view/ViewGroup;

.field protected final bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

.field final bUj:Lebe;

.field final bUk:Ldsm;

.field final bUl:Ldsm;

.field final bUm:Landroid/view/View;

.field final bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

.field final bUo:Lebq;

.field protected bUp:Ledu;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field protected bUq:Z

.field bUr:[Landroid/content/Intent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bUs:Lebj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bUt:Z

.field public bUu:Z

.field bUv:Z

.field private bUw:Z

.field protected bUx:I

.field protected final bUy:Z

.field protected bUz:Z

.field private final mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

.field private final mClock:Lemp;

.field protected final mContext:Landroid/content/Context;

.field mIntentStarter:Leqp;

.field mQuery:Lcom/google/android/shared/search/Query;

.field private final mRunner:Lerp;

.field protected final mSpeechLevelSource:Lequ;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Lerp;Leqp;Leax;Leld;Ldtc;ZLandroid/graphics/Rect;Landroid/os/Bundle;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;Z)V
    .locals 14
    .param p4    # Leax;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ldtc;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v2, Lebq;

    invoke-direct {v2, p0}, Lebq;-><init>(Lebm;)V

    iput-object v2, p0, Lebm;->bUo:Lebq;

    .line 175
    new-instance v2, Lequ;

    invoke-direct {v2}, Lequ;-><init>()V

    iput-object v2, p0, Lebm;->mSpeechLevelSource:Lequ;

    .line 177
    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v2, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 191
    const/4 v2, -0x1

    iput v2, p0, Lebm;->bUx:I

    .line 217
    iput-object p1, p0, Lebm;->bUg:Landroid/widget/FrameLayout;

    .line 218
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lebm;->mContext:Landroid/content/Context;

    .line 219
    move-object/from16 v0, p2

    iput-object v0, p0, Lebm;->mRunner:Lerp;

    .line 220
    move-object/from16 v0, p3

    iput-object v0, p0, Lebm;->mIntentStarter:Leqp;

    .line 221
    move/from16 v0, p7

    iput-boolean v0, p0, Lebm;->bUy:Z

    .line 222
    move-object/from16 v0, p10

    iput-object v0, p0, Lebm;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    .line 223
    move-object/from16 v0, p11

    iput-object v0, p0, Lebm;->bUc:Ljava/lang/String;

    .line 224
    move/from16 v0, p12

    iput-boolean v0, p0, Lebm;->bUA:Z

    .line 225
    iget-object v2, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 228
    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object v3, p0, Lebm;->mContext:Landroid/content/Context;

    const v5, 0x7f090160

    invoke-direct {v2, v3, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lebm;->bUd:Landroid/content/Context;

    .line 229
    iget-object v2, p0, Lebm;->bUd:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 230
    const v3, 0x7f040158

    invoke-virtual {v2, v3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 232
    const v2, 0x7f1103ae

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lecc;

    iput-object v2, p0, Lebm;->bTW:Lecc;

    .line 233
    const v2, 0x7f1103b1

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/ui/ReverseDrawRestrictedLayout;

    iput-object v2, p0, Lebm;->bUf:Lcom/google/android/search/shared/ui/ReverseDrawRestrictedLayout;

    .line 234
    const v2, 0x7f1103b2

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    iput-object v2, p0, Lebm;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    .line 236
    const v2, 0x7f1103b3

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/searchplate/SearchPlate;

    iput-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    .line 237
    iget-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    new-instance v3, Ledi;

    iget-object v5, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Ledi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldta;)V

    .line 239
    new-instance v2, Ldsp;

    iget-object v3, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v5, p0, Lebm;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-direct {v2, v3, v5}, Ldsp;-><init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/View;)V

    .line 242
    const v2, 0x7f1103b4

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;

    .line 244
    move-object/from16 v0, p5

    invoke-virtual {v5, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->a(Leld;)V

    .line 247
    const v2, 0x7f0d00a7

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->ha(I)V

    .line 249
    new-instance v2, Leca;

    invoke-direct {v2, p0}, Leca;-><init>(Lebm;)V

    invoke-virtual {v5, v2}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->a(Lebb;)V

    .line 251
    iget-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v3, p0, Lebm;->mSpeechLevelSource:Lequ;

    invoke-static {v3}, Leef;->a(Lequ;)Ldtb;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldtb;)V

    .line 253
    iget-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {p0}, Lebm;->aey()Ldsk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldsk;)V

    .line 255
    new-instance v6, Leey;

    invoke-direct {v6}, Leey;-><init>()V

    .line 257
    const v2, 0x7f0c0031

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 258
    const v2, 0x7f0c0033

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 259
    new-instance v2, Lebc;

    sub-int/2addr v4, v3

    new-instance v7, Leel;

    new-instance v8, Lekz;

    iget-object v9, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lekz;-><init>(Landroid/content/Context;)V

    invoke-direct {v7, v8}, Leel;-><init>(Lekz;)V

    iget-object v8, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d009b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    new-instance v9, Leak;

    new-instance v10, Leap;

    new-instance v11, Leao;

    iget-object v12, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-direct {v11, v12}, Leao;-><init>(Landroid/content/Context;)V

    invoke-direct {v10, v8, v8, v11}, Leap;-><init>(IILerd;)V

    new-instance v8, Leme;

    new-instance v11, Leoa;

    const-string v12, "SearchOverlay"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Leoa;-><init>(Ljava/lang/String;I)V

    invoke-static {v11}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v11

    invoke-direct {v8, v11, v10}, Leme;-><init>(Ljava/util/concurrent/Executor;Lerd;)V

    new-instance v10, Leqf;

    iget-object v11, p0, Lebm;->mRunner:Lerp;

    invoke-interface {v11}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v11

    invoke-direct {v10, v11, v8}, Leqf;-><init>(Ljava/util/concurrent/Executor;Lesm;)V

    invoke-direct {v9, v10}, Leak;-><init>(Lesm;)V

    new-instance v8, Leaq;

    iget-object v10, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-direct {v8, v10}, Leaq;-><init>(Landroid/content/Context;)V

    invoke-static {v9, v8}, Lijj;->r(Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v8

    invoke-static {v8}, Lemn;->b(Lijj;)Lemn;

    move-result-object v8

    new-instance v9, Lela;

    const/4 v10, 0x2

    iget-object v11, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c0039

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    invoke-direct {v9, v10, v11}, Lela;-><init>(II)V

    new-instance v11, Lebt;

    invoke-direct {v11, p0}, Lebt;-><init>(Lebm;)V

    move-object/from16 v10, p4

    invoke-direct/range {v2 .. v11}, Lebc;-><init>(IILebd;Leey;Leeo;Lesm;Lela;Leax;Ligi;)V

    iput-object v2, p0, Lebm;->bUe:Lebc;

    .line 269
    new-instance v2, Lelj;

    const v3, 0x7f1103c1

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const v7, 0x7f1103c1

    invoke-direct/range {v2 .. v7}, Lelj;-><init>(IIIII)V

    .line 271
    iget-object v3, p0, Lebm;->bUe:Lebc;

    invoke-virtual {v3, v2}, Lebc;->a(Lelj;)V

    .line 273
    new-instance v4, Lebw;

    invoke-direct {v4, p0}, Lebw;-><init>(Lebm;)V

    .line 275
    iget-boolean v2, p0, Lebm;->bUy:Z

    if-eqz v2, :cond_1

    .line 276
    const v2, 0x7f1103af

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 277
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lebm;->bUh:Landroid/view/ViewGroup;

    .line 279
    iget-object v2, p0, Lebm;->bUh:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 280
    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p8

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v5

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 281
    const/4 v3, -0x2

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 282
    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p8

    iget v5, v0, Landroid/graphics/Rect;->top:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 283
    iget-object v3, p0, Lebm;->bUh:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 285
    iget-object v2, p0, Lebm;->bUh:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v2

    .line 286
    new-instance v3, Ldsm;

    const/4 v5, 0x1

    invoke-direct {v3, v5}, Ldsm;-><init>(Z)V

    iput-object v3, p0, Lebm;->bUk:Ldsm;

    .line 287
    new-instance v3, Ldsm;

    const/4 v5, 0x0

    invoke-direct {v3, v5}, Ldsm;-><init>(Z)V

    iput-object v3, p0, Lebm;->bUl:Ldsm;

    .line 288
    const/4 v3, 0x2

    iget-object v5, p0, Lebm;->bUk:Ldsm;

    invoke-virtual {v2, v3, v5}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 289
    const/4 v3, 0x3

    iget-object v5, p0, Lebm;->bUl:Ldsm;

    invoke-virtual {v2, v3, v5}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 290
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    invoke-virtual {v2, v3, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 292
    const v2, 0x7f110492

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lebm;->bUm:Landroid/view/View;

    .line 293
    iget-object v2, p0, Lebm;->bUm:Landroid/view/View;

    new-instance v3, Lebn;

    invoke-direct {v3, p0}, Lebn;-><init>(Lebm;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    const v2, 0x7f110493

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iput-object v2, p0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    .line 302
    iget-object v2, p0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->dM(Z)V

    .line 305
    iget-object v2, p0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    new-instance v3, Lebo;

    invoke-direct {v3, p0}, Lebo;-><init>(Lebm;)V

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b(Landroid/view/View$OnClickListener;)V

    .line 319
    :goto_0
    iget-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v2

    check-cast v2, Ldsm;

    .line 321
    iget-object v3, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v3

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v3

    check-cast v3, Ldsm;

    .line 323
    iget-object v5, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const v6, 0x7f1103c5

    invoke-virtual {v5, v6}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 324
    iget-object v6, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const v7, 0x7f1103c8

    invoke-virtual {v6, v7}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 325
    const/4 v7, 0x0

    invoke-virtual {v2, v5, v7}, Ldsm;->n(Landroid/view/View;I)V

    .line 326
    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Ldsm;->n(Landroid/view/View;I)V

    .line 327
    const/4 v2, 0x0

    invoke-virtual {v3, v5, v2}, Ldsm;->n(Landroid/view/View;I)V

    .line 328
    const/4 v2, 0x0

    invoke-virtual {v3, v6, v2}, Ldsm;->n(Landroid/view/View;I)V

    .line 330
    iget-object v2, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 331
    new-instance v3, Ldqq;

    iget-object v5, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v5}, Lebm;->ao(Landroid/content/Context;)I

    move-result v5

    const v6, 0x7f0b0074

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    const v7, 0x7f0d000b

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v3, v5, v6, v2}, Ldqq;-><init>(III)V

    .line 336
    iget-object v2, p0, Lebm;->bTW:Lecc;

    invoke-virtual {v2, v3}, Lecc;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 337
    new-instance v2, Lebe;

    iget-object v5, p0, Lebm;->bTW:Lecc;

    new-instance v6, Lebs;

    invoke-direct {v6, p0}, Lebs;-><init>(Lebm;)V

    invoke-direct {v2, p0, v5, v3, v6}, Lebe;-><init>(Lebi;Lecc;Ldqq;Lebh;)V

    iput-object v2, p0, Lebm;->bUj:Lebe;

    .line 340
    iget-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v2, v4}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldsj;)V

    .line 342
    iget-object v2, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldtc;)V

    .line 346
    iget-boolean v2, p0, Lebm;->bUA:Z

    if-eqz v2, :cond_0

    if-nez p9, :cond_0

    .line 347
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {p0, v2, v3, v4}, Lebm;->d(IIZ)V

    .line 350
    :cond_0
    move-object/from16 v0, p9

    invoke-virtual {p0, v0}, Lebm;->A(Landroid/os/Bundle;)Lecq;

    move-result-object v2

    iput-object v2, p0, Lebm;->ann:Lecq;

    .line 352
    iget-object v2, p0, Lebm;->bUe:Lebc;

    new-instance v3, Lecb;

    invoke-direct {v3, p0}, Lecb;-><init>(Lebm;)V

    iget-object v2, v2, Lebc;->bTy:Lebd;

    invoke-interface {v2, v3}, Lebd;->c(Landroid/view/View$OnClickListener;)V

    .line 353
    iget-object v2, p0, Lebm;->bUe:Lebc;

    invoke-virtual {p0}, Lebm;->aex()Leen;

    move-result-object v3

    iget-object v2, v2, Lebc;->bTy:Lebd;

    invoke-interface {v2, v3}, Lebd;->a(Leen;)V

    .line 356
    iget-object v2, p0, Lebm;->bTW:Lecc;

    new-instance v3, Lebr;

    invoke-direct {v3, p0}, Lebr;-><init>(Lebm;)V

    invoke-virtual {v2, v3}, Lecc;->a(Landroid/view/View$OnKeyListener;)V

    .line 358
    new-instance v2, Lere;

    iget-object v3, p0, Lebm;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lere;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lebm;->mClock:Lemp;

    .line 359
    return-void

    .line 312
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lebm;->bUh:Landroid/view/ViewGroup;

    .line 313
    const/4 v2, 0x0

    iput-object v2, p0, Lebm;->bUm:Landroid/view/View;

    .line 314
    const/4 v2, 0x0

    iput-object v2, p0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    .line 315
    const/4 v2, 0x0

    iput-object v2, p0, Lebm;->bUk:Ldsm;

    .line 316
    const/4 v2, 0x0

    iput-object v2, p0, Lebm;->bUl:Ldsm;

    goto/16 :goto_0
.end method

.method private clear(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 644
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v1, v0}, Lebm;->d(IIZ)V

    .line 645
    invoke-virtual {p0}, Lebm;->aeC()V

    .line 646
    iput-boolean v1, p0, Lebm;->bUv:Z

    .line 647
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 648
    iget-object v0, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v2, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v2}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 649
    iget-object v0, p0, Lebm;->bUs:Lebj;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lebm;->bUs:Lebj;

    invoke-interface {v0}, Lebj;->Cp()V

    .line 652
    :cond_0
    iget-object v0, p0, Lebm;->bTL:Lecd;

    if-eqz v0, :cond_1

    .line 653
    iget-object v0, p0, Lebm;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->close()V

    .line 655
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 644
    goto :goto_0
.end method

.method private f(Lcom/google/android/shared/search/Query;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 927
    new-instance v0, Ledu;

    iget-object v1, p0, Lebm;->bUc:Ljava/lang/String;

    iget-object v3, p0, Lebm;->mClock:Lemp;

    invoke-virtual {p0}, Lebm;->aez()I

    move-result v5

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Ledu;-><init>(Ljava/lang/String;Ljava/lang/String;Lemp;Lcom/google/android/shared/search/Query;I)V

    iput-object v0, p0, Lebm;->bUp:Ledu;

    .line 929
    return-void
.end method

.method private kL(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v0, p1}, Lebm;->f(Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    .line 920
    return-void
.end method


# virtual methods
.method protected A(Landroid/os/Bundle;)Lecq;
    .locals 6

    .prologue
    .line 718
    new-instance v0, Lecq;

    iget-object v1, p0, Lebm;->mContext:Landroid/content/Context;

    new-instance v2, Leby;

    invoke-direct {v2, p0}, Leby;-><init>(Lebm;)V

    invoke-virtual {p0}, Lebm;->aew()Lebz;

    move-result-object v3

    iget-object v4, p0, Lebm;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected Cq()V
    .locals 0

    .prologue
    .line 939
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 580
    iget-boolean v0, p0, Lebm;->bUv:Z

    if-eqz v0, :cond_0

    .line 596
    :goto_0
    return-void

    .line 584
    :cond_0
    iput-object p1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 586
    invoke-virtual {p0, p3}, Lebm;->kf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lebm;->kL(Ljava/lang/String;)V

    .line 587
    iget-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    iget-object v0, p0, Lebm;->bUp:Ledu;

    iget-object v3, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v3}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    .line 591
    :cond_1
    iget-object v3, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v0}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v4

    if-nez p2, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v4, v0}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 592
    if-nez p2, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p0, v1, v2, v0}, Lebm;->d(IIZ)V

    .line 593
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->UF()V

    .line 594
    iget-object v0, p0, Lebm;->ann:Lecq;

    iget-object v2, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Lecq;->x(Lcom/google/android/shared/search/Query;)V

    .line 595
    invoke-virtual {p0, v1}, Lebm;->eG(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 591
    goto :goto_1

    :cond_3
    move v0, v2

    .line 592
    goto :goto_2
.end method

.method public final a(Lebj;)V
    .locals 2

    .prologue
    .line 421
    iput-object p1, p0, Lebm;->bUs:Lebj;

    .line 422
    iget-object v0, p0, Lebm;->bUs:Lebj;

    if-eqz v0, :cond_0

    .line 423
    iget-object v1, p0, Lebm;->bUs:Lebj;

    iget v0, p0, Lebm;->bUx:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lebj;->bZ(Z)V

    .line 425
    :cond_0
    return-void

    .line 423
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lecd;)V
    .locals 2

    .prologue
    .line 385
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecd;

    iput-object v0, p0, Lebm;->bTL:Lecd;

    .line 386
    iget-object v1, p0, Lebm;->bUe:Lebc;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecd;

    iput-object v0, v1, Lebc;->bTL:Lecd;

    .line 387
    return-void
.end method

.method public final a(Leqp;)V
    .locals 1

    .prologue
    .line 380
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqp;

    iput-object v0, p0, Lebm;->mIntentStarter:Leqp;

    .line 381
    return-void
.end method

.method public a(Letj;)V
    .locals 4

    .prologue
    .line 943
    const-string v0, "SearchOverlayImpl"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 944
    const-string v0, "mSearchServiceClient"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 945
    const-string v0, "mQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 946
    const-string v0, "mPendingShowKeyboard"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->bUt:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 947
    const-string v0, "mHasWindowFocus"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->aPF:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 948
    const-string v0, "mSearchStarted"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->bUu:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 949
    const-string v0, "mStartingNewActivity"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->bUv:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 950
    const-string v0, "mHotwordRequested"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->bUw:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 951
    const-string v0, "mSearchPlateUiMode"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lebm;->bUx:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 952
    const-string v0, "mVerticalSearchBarMode"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->bUy:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 953
    const-string v0, "mDestroyed"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lebm;->bB:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 954
    return-void
.end method

.method public aO(Lcom/google/android/shared/search/Query;)V
    .locals 5

    .prologue
    .line 691
    iget-boolean v0, p0, Lebm;->bUq:Z

    if-eqz v0, :cond_0

    .line 692
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object p1

    .line 694
    :cond_0
    iget-object v0, p0, Lebm;->bUp:Ledu;

    if-nez v0, :cond_1

    .line 695
    const-string v0, "SearchOverlay"

    const-string v1, "Query committed with null FormulationLogging. Resetting to use the default source."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    new-instance v4, Leos;

    invoke-direct {v4}, Leos;-><init>()V

    invoke-static {v3, v0, v4, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 697
    invoke-virtual {p0, p1}, Lebm;->aP(Lcom/google/android/shared/search/Query;)V

    .line 699
    :cond_1
    iget-object v0, p0, Lebm;->bUp:Ledu;

    invoke-virtual {v0}, Ledu;->anZ()Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 700
    iget-object v0, p0, Lebm;->ann:Lecq;

    iget-object v1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    .line 701
    return-void
.end method

.method protected final aP(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 923
    const-string v0, "android-search-app"

    invoke-direct {p0, p1, v0}, Lebm;->f(Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    .line 924
    return-void
.end method

.method public aeA()Z
    .locals 1

    .prologue
    .line 454
    iget-boolean v0, p0, Lebm;->bUw:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lebm;->bUu:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aeB()Z
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x0

    return v0
.end method

.method protected aeC()V
    .locals 0

    .prologue
    .line 450
    return-void
.end method

.method public final aeq()Landroid/view/View;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lebm;->bTW:Lecc;

    return-object v0
.end method

.method public final aer()Landroid/view/View;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lebm;->bUg:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final aes()V
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 415
    iget-object v0, p0, Lebm;->bUj:Lebe;

    invoke-virtual {v0, v1, v1}, Lebe;->aC(II)V

    .line 416
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebm;->bUq:Z

    .line 417
    return-void
.end method

.method public final aeu()Z
    .locals 1

    .prologue
    .line 640
    iget-boolean v0, p0, Lebm;->bUu:Z

    return v0
.end method

.method public final aev()Lecq;
    .locals 1

    .prologue
    .line 1503
    iget-object v0, p0, Lebm;->ann:Lecq;

    return-object v0
.end method

.method protected aew()Lebz;
    .locals 1

    .prologue
    .line 367
    new-instance v0, Lebz;

    invoke-direct {v0, p0}, Lebz;-><init>(Lebm;)V

    return-object v0
.end method

.method protected aex()Leen;
    .locals 1

    .prologue
    .line 371
    new-instance v0, Lebu;

    invoke-direct {v0, p0}, Lebu;-><init>(Lebm;)V

    return-object v0
.end method

.method protected aey()Ldsk;
    .locals 1

    .prologue
    .line 375
    new-instance v0, Lebx;

    invoke-direct {v0, p0}, Lebx;-><init>(Lebm;)V

    return-object v0
.end method

.method public aez()I
    .locals 1

    .prologue
    .line 472
    const/4 v0, 0x2

    return v0
.end method

.method protected final amL()V
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lebm;->aeA()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lebm;->bUv:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lebm;->bB:Z

    if-nez v0, :cond_1

    .line 459
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    goto :goto_0
.end method

.method final amM()V
    .locals 2

    .prologue
    .line 706
    iget-object v0, p0, Lebm;->bUr:[Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lebm;->mIntentStarter:Leqp;

    iget-object v1, p0, Lebm;->bUr:[Landroid/content/Intent;

    invoke-interface {v0, v1}, Leqp;->b([Landroid/content/Intent;)Z

    move-result v0

    .line 708
    const/4 v1, 0x0

    iput-object v1, p0, Lebm;->bUr:[Landroid/content/Intent;

    .line 709
    if-eqz v0, :cond_0

    .line 710
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebm;->bUv:Z

    .line 711
    invoke-virtual {p0}, Lebm;->amL()V

    .line 714
    :cond_0
    return-void
.end method

.method protected ao(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 363
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0075

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final av(J)V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lebm;->eG(Z)V

    .line 555
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0, p1, p2}, Lecq;->aD(J)V

    .line 556
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 560
    iget-object v0, p0, Lebm;->ann:Lecq;

    const/4 v1, 0x2

    invoke-virtual {v0, p2, v1}, Lecq;->e(Landroid/os/Bundle;I)V

    .line 562
    const-string v0, "android.intent.extra.TEXT"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 563
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apA()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 564
    invoke-virtual {p0, p1}, Lebm;->kf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lebm;->kL(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 567
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0, v2}, Lebm;->d(IIZ)V

    .line 568
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->UF()V

    .line 569
    iget-object v0, p0, Lebm;->ann:Lecq;

    iget-object v1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lecq;->x(Lcom/google/android/shared/search/Query;)V

    .line 570
    invoke-virtual {p0, v2}, Lebm;->eG(Z)V

    .line 571
    return-void
.end method

.method public final bW(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 625
    iget v0, p0, Lebm;->bUx:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lebm;->aeB()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 628
    :goto_0
    iget-boolean v3, p0, Lebm;->bUv:Z

    if-nez v3, :cond_0

    .line 629
    iget-object v3, p0, Lebm;->ann:Lecq;

    invoke-virtual {v3}, Lecq;->cancel()V

    .line 632
    :cond_0
    invoke-virtual {p0, v2}, Lebm;->eG(Z)V

    .line 633
    invoke-virtual {p0}, Lebm;->amL()V

    .line 634
    invoke-direct {p0, p1}, Lebm;->clear(Z)V

    .line 635
    if-nez v0, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 625
    goto :goto_0

    :cond_2
    move v1, v2

    .line 635
    goto :goto_1
.end method

.method public d(IIZ)V
    .locals 1

    .prologue
    .line 773
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lebm;->bUA:Z

    if-eqz v0, :cond_1

    .line 774
    :cond_0
    iget-object v0, p0, Lebm;->bUo:Lebq;

    invoke-virtual {v0, p1, p2, p3}, Lebq;->c(IIZ)V

    .line 776
    :cond_1
    return-void
.end method

.method public dJ(Z)V
    .locals 1

    .prologue
    .line 477
    if-eqz p1, :cond_1

    .line 478
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->anz()V

    .line 482
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebm;->bB:Z

    .line 483
    invoke-virtual {p0}, Lebm;->amL()V

    .line 484
    iget-object v0, p0, Lebm;->bTL:Lecd;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lebm;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->close()V

    .line 487
    :cond_0
    return-void

    .line 480
    :cond_1
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    goto :goto_0
.end method

.method public final dc(Z)V
    .locals 4

    .prologue
    .line 671
    iget-boolean v0, p0, Lebm;->bUw:Z

    if-ne v0, p1, :cond_1

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 674
    :cond_1
    iput-boolean p1, p0, Lebm;->bUw:Z

    .line 675
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0, p1}, Lecq;->bq(Z)V

    .line 676
    invoke-virtual {p0}, Lebm;->amL()V

    .line 678
    if-nez p1, :cond_0

    .line 680
    iget-object v0, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/search/searchplate/SearchPlate;->a(ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method final eG(Z)V
    .locals 1

    .prologue
    .line 429
    iget-boolean v0, p0, Lebm;->bUu:Z

    if-eq v0, p1, :cond_1

    .line 430
    if-eqz p1, :cond_0

    .line 431
    invoke-virtual {p0}, Lebm;->aeC()V

    .line 433
    :cond_0
    iput-boolean p1, p0, Lebm;->bUu:Z

    .line 434
    invoke-virtual {p0}, Lebm;->amL()V

    .line 436
    :cond_1
    return-void
.end method

.method public final eH(Z)V
    .locals 0

    .prologue
    .line 1515
    iput-boolean p1, p0, Lebm;->bUz:Z

    .line 1516
    return-void
.end method

.method public final ke(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 605
    iget-boolean v0, p0, Lebm;->bUv:Z

    if-eqz v0, :cond_0

    .line 615
    :goto_0
    return-void

    .line 609
    :cond_0
    invoke-virtual {p0, p1}, Lebm;->kf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lebm;->kL(Ljava/lang/String;)V

    .line 611
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lebm;->eG(Z)V

    .line 612
    const/4 v0, 0x2

    const/4 v1, 0x0

    iget-boolean v2, p0, Lebm;->bUq:Z

    invoke-virtual {p0, v0, v1, v2}, Lebm;->d(IIZ)V

    .line 614
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aph()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lebm;->aO(Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public kf(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 468
    return-object p1
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 391
    if-eqz p1, :cond_0

    .line 392
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    .line 393
    invoke-virtual {p0, p1}, Lebm;->x(Landroid/os/Bundle;)V

    .line 394
    invoke-virtual {p0}, Lebm;->amL()V

    .line 399
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->any()V

    .line 397
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lebm;->d(IIZ)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 491
    iget-boolean v0, p0, Lebm;->bUv:Z

    if-eqz v0, :cond_0

    .line 495
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lebm;->eG(Z)V

    .line 496
    iget-object v0, p0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->cancel()V

    .line 497
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lebm;->clear(Z)V

    .line 499
    :cond_0
    iget-object v0, p0, Lebm;->bTL:Lecd;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lebm;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->onResume()V

    .line 502
    :cond_1
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 3

    .prologue
    .line 782
    iput-boolean p1, p0, Lebm;->aPF:Z

    .line 783
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lebm;->bUt:Z

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lebm;->mRunner:Lerp;

    new-instance v1, Lebp;

    const-string v2, "Show keyboard"

    invoke-direct {v1, p0, v2}, Lebp;-><init>(Lebm;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lerp;->a(Lesk;)V

    .line 794
    :cond_0
    return-void
.end method

.method protected v(IZ)V
    .locals 1

    .prologue
    .line 932
    if-eqz p1, :cond_0

    .line 933
    iget-object v0, p0, Lebm;->bUi:Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->gY(I)V

    .line 935
    :cond_0
    return-void
.end method

.method public final w(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 507
    iget-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    .line 508
    const-string v0, "search_overlay_impl:query"

    iget-object v1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 510
    :cond_0
    const-string v0, "search_overlay_impl:search_started"

    iget-boolean v1, p0, Lebm;->bUu:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 511
    iget-object v0, p0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_1

    .line 512
    const-string v0, "search_overlay_impl:search_box_stats"

    iget-object v1, p0, Lebm;->bUp:Ledu;

    invoke-virtual {v1}, Ledu;->anZ()Lehj;

    move-result-object v2

    iget-object v1, v1, Ledu;->bVX:Ljava/util/List;

    iput-object v1, v2, Lehj;->bVX:Ljava/util/List;

    invoke-virtual {v2}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 514
    :cond_1
    iget-object v0, p0, Lebm;->ann:Lecq;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lecq;->a(Landroid/os/Bundle;Z)V

    .line 515
    iget-object v0, p0, Lebm;->bUe:Lebc;

    invoke-virtual {v0, p1}, Lebc;->w(Landroid/os/Bundle;)V

    .line 516
    iget-object v0, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->w(Landroid/os/Bundle;)V

    .line 517
    const-string v0, "search_overlay_impl:search_plate_mode"

    iget v1, p0, Lebm;->bUx:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 518
    return-void
.end method

.method public x(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 522
    if-nez p1, :cond_0

    .line 550
    :goto_0
    return-void

    .line 525
    :cond_0
    const-string v0, "search_overlay_impl:search_box_stats"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 526
    new-instance v1, Ledu;

    const-string v0, "search_overlay_impl:search_box_stats"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/SearchBoxStats;

    iget-object v2, p0, Lebm;->mClock:Lemp;

    iget-object v3, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {v1, v0, v2, v3}, Ledu;-><init>(Lcom/google/android/shared/search/SearchBoxStats;Lemp;Lcom/google/android/shared/search/Query;)V

    iput-object v1, p0, Lebm;->bUp:Ledu;

    .line 530
    :cond_1
    const-string v0, "search_overlay_impl:query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 531
    const-string v0, "search_overlay_impl:query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 532
    iput-object v0, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    .line 533
    iget-object v0, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v1, p0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 535
    :cond_2
    const-string v0, "search_overlay_impl:search_started"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 536
    const-string v0, "search_overlay_impl:search_started"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lebm;->bUu:Z

    .line 538
    :cond_3
    const-string v0, "search_overlay_impl:search_plate_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 539
    const-string v0, "search_overlay_impl:search_plate_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebm;->bUx:I

    .line 541
    iget-object v0, p0, Lebm;->bUh:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 542
    iget v0, p0, Lebm;->bUx:I

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 544
    :goto_1
    iget-object v1, p0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setVisibility(I)V

    .line 545
    iget-object v1, p0, Lebm;->bUm:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 548
    :cond_4
    iget-object v0, p0, Lebm;->bUe:Lebc;

    invoke-virtual {v0, p1}, Lebc;->x(Landroid/os/Bundle;)V

    .line 549
    iget-object v0, p0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->x(Landroid/os/Bundle;)V

    goto :goto_0

    .line 542
    :cond_5
    const/4 v0, 0x4

    goto :goto_1
.end method
