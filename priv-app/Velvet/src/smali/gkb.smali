.class public final Lgkb;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljze;)Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lgkc;

    invoke-direct {v0, p0}, Lgkc;-><init>(Ljze;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;[Ljzp;[II)Ljava/util/Map;
    .locals 4

    .prologue
    .line 101
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 102
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 103
    invoke-static {v1, v0, p2, p3}, Lgkb;->a(Ljava/util/HashMap;Ljzp;[II)V

    goto :goto_0

    .line 105
    :cond_0
    array-length v2, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 106
    invoke-static {v1, v3, p2, p3}, Lgkb;->a(Ljava/util/HashMap;Ljzp;[II)V

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/lang/String;[Ljzp;)Ljzp;
    .locals 4

    .prologue
    .line 49
    array-length v2, p1

    .line 50
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 51
    aget-object v0, p1, v1

    .line 52
    invoke-virtual {v0}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 57
    :goto_1
    return-object v0

    .line 50
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/util/HashMap;Ljzp;[II)V
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-static {p1, p2, p3}, Lgkb;->a(Ljzp;[II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    invoke-virtual {v0}, Ljzp;->getVersion()I

    move-result v0

    invoke-virtual {p1}, Ljzp;->getVersion()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 118
    :cond_0
    invoke-virtual {p0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    :cond_1
    return-void
.end method

.method public static a(Ljzp;[II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 67
    .line 69
    iget-object v2, p0, Ljzp;->eOd:[I

    array-length v2, v2

    .line 70
    if-nez v2, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    iget-object v3, p0, Ljzp;->eOd:[I

    add-int/lit8 v2, v2, -0x1

    aget v3, v3, v2

    .line 76
    array-length v4, p1

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    aget v5, p1, v2

    .line 77
    if-ne v5, v3, :cond_3

    move v2, v1

    .line 83
    :goto_2
    if-eqz v2, :cond_0

    .line 90
    invoke-virtual {p0}, Ljzp;->bxn()I

    move-result v2

    if-ge p2, v2, :cond_2

    invoke-virtual {p0}, Ljzp;->bxo()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    .line 92
    goto :goto_0

    .line 76
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method public static b(Ljzp;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
