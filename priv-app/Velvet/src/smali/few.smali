.class public final Lfew;
.super Lfeu;
.source "PG"


# instance fields
.field private final cpU:Ljbp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cpW:Lixs;

.field private final mDestination:Ljbp;

.field private final mDirectionsLauncher:Lgah;


# direct methods
.method public constructor <init>(Lizj;Ljbp;Ljbp;Lgah;)V
    .locals 1
    .param p2    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 36
    iget-object v0, p1, Lizj;->dTk:Lixs;

    iput-object v0, p0, Lfew;->cpW:Lixs;

    .line 37
    iput-object p2, p0, Lfew;->cpU:Ljbp;

    .line 38
    iput-object p3, p0, Lfew;->mDestination:Ljbp;

    .line 39
    iput-object p4, p0, Lfew;->mDirectionsLauncher:Lgah;

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v0}, Lixs;->baS()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 95
    iget-object v0, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v0}, Lixs;->getType()I

    move-result v0

    if-ne v0, v8, :cond_0

    const v0, 0x7f0a04a2

    .line 97
    :goto_0
    iget-object v1, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v1}, Lixs;->getType()I

    move-result v1

    if-ne v1, v8, :cond_1

    iget-object v1, p0, Lfew;->cpW:Lixs;

    iget-object v1, v1, Lixs;->dOt:Ljhl;

    invoke-virtual {v1}, Ljhl;->akO()J

    move-result-wide v2

    .line 101
    :goto_1
    new-instance v1, Lffz;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v6}, Lixs;->baS()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {p1, v2, v3, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    const/4 v2, 0x2

    iget-object v3, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v3}, Lixs;->baQ()J

    move-result-wide v6

    invoke-static {p1, v6, v7, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lfew;->mDestination:Ljbp;

    invoke-virtual {v1, v0, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 95
    :cond_0
    const v0, 0x7f0a04a3

    goto :goto_0

    .line 97
    :cond_1
    iget-object v1, p0, Lfew;->cpW:Lixs;

    iget-object v1, v1, Lixs;->dOw:Ljhl;

    invoke-virtual {v1}, Ljhl;->akO()J

    move-result-wide v2

    goto :goto_1
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 62
    iget-object v0, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v0}, Lixs;->getType()I

    move-result v0

    if-ne v0, v6, :cond_0

    const v0, 0x7f0a0467

    .line 64
    :goto_0
    iget-object v1, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v1}, Lixs;->getType()I

    move-result v1

    if-ne v1, v6, :cond_1

    iget-object v1, p0, Lfew;->cpW:Lixs;

    iget-object v1, v1, Lixs;->dOt:Ljhl;

    invoke-virtual {v1}, Ljhl;->akO()J

    move-result-wide v2

    .line 67
    :goto_1
    new-array v1, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {p1, v2, v3, v6}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 62
    :cond_0
    const v0, 0x7f0a0468

    goto :goto_0

    .line 64
    :cond_1
    iget-object v1, p0, Lfew;->cpW:Lixs;

    iget-object v1, v1, Lixs;->dOw:Ljhl;

    invoke-virtual {v1}, Ljhl;->akO()J

    move-result-wide v2

    goto :goto_1
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const-string v0, "event"

    return-object v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 74
    const v0, 0x7f0202d9

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lfgb;->cqC:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lfew;->cpW:Lixs;

    invoke-virtual {v0}, Lixs;->baS()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;
    .locals 5

    .prologue
    .line 44
    iget-object v0, p0, Lfew;->cpW:Lixs;

    iget-object v0, v0, Lixs;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfew;->cpW:Lixs;

    iget-object v0, v0, Lixs;->dMF:[Liyg;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 46
    :goto_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v1

    .line 48
    new-instance v2, Lffj;

    iget-object v3, p0, Lfew;->mDirectionsLauncher:Lgah;

    iget-object v4, p0, Lfew;->cpU:Ljbp;

    iget-object v4, p0, Lfew;->mDestination:Ljbp;

    invoke-direct {v2, v1, v3, v4, v0}, Lffj;-><init>(Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;Lgah;Ljbp;Liyg;)V

    .line 50
    invoke-static {v2}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    return-object v0

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
