.class public final Lgnq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cQt:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lgnq;->cQt:Ljava/util/Locale;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 579
    if-eqz p0, :cond_0

    .line 580
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 581
    invoke-static {v0}, Lgnq;->isRtl(Ljava/lang/String;)Z

    move-result v0

    .line 582
    invoke-static {p1}, Lgnq;->isRtl(Ljava/lang/String;)Z

    move-result v1

    .line 583
    if-eq v0, v1, :cond_0

    .line 584
    invoke-static {v0}, Landroid/text/BidiFormatter;->getInstance(Z)Landroid/text/BidiFormatter;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 587
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljze;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 40
    :goto_0
    iget-object v4, p1, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 41
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v2, v1

    :goto_2
    if-ge v2, v7, :cond_2

    aget-object v8, v6, v2

    .line 42
    iget-object v9, v8, Ljzh;->eNz:[Ljava/lang/String;

    array-length v10, v9

    move v0, v1

    :goto_3
    if-ge v0, v10, :cond_1

    aget-object v11, v9, v0

    .line 43
    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 44
    invoke-virtual {v8}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v0

    .line 54
    :goto_4
    return-object v0

    .line 42
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 41
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 40
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 50
    :cond_3
    const-string v0, "_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 51
    const/16 v0, 0x5f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 54
    :cond_4
    const-string v0, "en-001"

    goto :goto_4
.end method

.method public static a(Ljze;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    invoke-static {p0, p1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 152
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p1, p2}, Lgnq;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a(Ljze;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 172
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 173
    invoke-static {p0, v0}, Lgnq;->c(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    .line 179
    :goto_1
    return-object v0

    .line 172
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 179
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljze;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .param p0    # Ljze;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 494
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 495
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 497
    invoke-static {p0, v0}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 498
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 499
    invoke-static {v3, v0, p2}, Lgnq;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 502
    :cond_1
    return-object v1
.end method

.method public static a(Ljze;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 92
    invoke-static {p0, p1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljze;Ljava/util/List;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    if-eqz p1, :cond_1

    .line 110
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 111
    :goto_0
    if-ge v2, v3, :cond_1

    .line 112
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lgnq;->a(Ljze;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 117
    :goto_1
    return v0

    .line 111
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 117
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static a([Ljzh;)[Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 156
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/CharSequence;

    .line 158
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 159
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_0
    return-object v1
.end method

.method public static an(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 527
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 528
    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 529
    invoke-interface {p0, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 530
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 531
    return-void
.end method

.method public static b(Ljze;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v3, p0, Ljze;->eNa:[Ljzo;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 123
    iget-object v5, v0, Ljzo;->eNX:[Ljzh;

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    .line 125
    invoke-virtual {v7}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 126
    invoke-virtual {v7}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 131
    :goto_2
    return-object v0

    .line 123
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 122
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 130
    :cond_2
    const-string v0, "SpokenLanguageUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No display name for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 131
    const-string v0, ""

    goto :goto_2
.end method

.method public static b(Ljze;[Ljava/lang/String;)[Ljzh;
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 287
    new-instance v4, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 288
    iget-object v5, p0, Ljze;->eNa:[Ljzo;

    array-length v6, v5

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v0, v5, v3

    .line 289
    iget-object v7, v0, Ljzo;->eNX:[Ljzh;

    array-length v8, v7

    move v2, v1

    :goto_1
    if-ge v2, v8, :cond_2

    aget-object v9, v7, v2

    .line 290
    array-length v10, p1

    move v0, v1

    :goto_2
    if-ge v0, v10, :cond_0

    aget-object v11, p1, v0

    .line 291
    invoke-virtual {v9}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 292
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 290
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 288
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 298
    :cond_3
    new-array v0, v1, [Ljzh;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljzh;

    return-object v0
.end method

.method public static c(Ljze;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 188
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 189
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v2, v1

    :goto_1
    if-ge v2, v7, :cond_2

    aget-object v8, v6, v2

    .line 190
    iget-object v9, v8, Ljzh;->eNz:[Ljava/lang/String;

    array-length v10, v9

    move v0, v1

    :goto_2
    if-ge v0, v10, :cond_1

    aget-object v11, v9, v0

    .line 191
    invoke-virtual {v11, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 192
    invoke-virtual {v8}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_3
    return-object v0

    .line 190
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 189
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 188
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 197
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static c(Ljze;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 61
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v0, v4, v2

    .line 63
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 64
    invoke-virtual {v8}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 67
    :cond_1
    return-object v3
.end method

.method public static c(Ljze;[Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1    # [Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 344
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    array-length v0, p1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 352
    const/4 v2, 0x1

    .line 353
    array-length v4, p1

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_4

    aget-object v5, p1, v3

    .line 354
    if-nez v5, :cond_1

    .line 374
    :cond_0
    :goto_1
    if-eqz v1, :cond_3

    .line 376
    invoke-static {v0}, Lgnq;->an(Ljava/util/List;)V

    .line 381
    :goto_2
    return-object v0

    .line 358
    :cond_1
    invoke-static {p0, v5}, Lgnq;->g(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v6

    .line 360
    if-eqz v6, :cond_2

    .line 361
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 364
    :cond_2
    invoke-static {p0, v5}, Lgnq;->d(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 365
    invoke-virtual {v5}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 381
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public static c(Ljava/util/List;Ljava/util/List;)[Z
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 544
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 545
    new-array v2, v1, [Z

    .line 546
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 547
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    aput-boolean v3, v2, v0

    .line 546
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 549
    :cond_0
    return-object v2
.end method

.method public static d(Ljze;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 71
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 72
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v0, v4, v2

    .line 73
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 74
    invoke-virtual {v8}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 72
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 77
    :cond_1
    return-object v3
.end method

.method public static d(Ljze;Ljava/lang/String;)Ljzh;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 205
    iget-object v5, p0, Ljze;->eNa:[Ljzo;

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v0, v5, v4

    .line 206
    iget-object v7, v0, Ljzo;->eNX:[Ljzh;

    array-length v8, v7

    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_2

    aget-object v0, v7, v3

    .line 207
    iget-object v9, v0, Ljzh;->eNz:[Ljava/lang/String;

    array-length v10, v9

    move v1, v2

    :goto_2
    if-ge v1, v10, :cond_1

    aget-object v11, v9, v1

    .line 208
    invoke-virtual {v11, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 214
    :goto_3
    return-object v0

    .line 207
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 206
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 205
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 214
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static e(Ljze;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 241
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 242
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v0, v4, v2

    .line 243
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    .line 244
    invoke-virtual {v8}, Ljzh;->bwT()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 245
    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 242
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 249
    :cond_2
    return-object v3
.end method

.method public static e(Ljze;Ljava/lang/String;)Ljzh;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 222
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 223
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v0, v6, v1

    .line 224
    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 229
    :goto_2
    return-object v0

    .line 223
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 222
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 229
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static f(Ljze;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 254
    iget-object v2, p0, Ljze;->eNd:[Ljzp;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 255
    invoke-virtual {v4}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_0
    return-object v1
.end method

.method public static f(Ljze;Ljava/lang/String;)Ljzh;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 262
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 263
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v0, v6, v1

    .line 264
    invoke-virtual {v0}, Ljzh;->bwT()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 269
    :goto_2
    return-object v0

    .line 263
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 262
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 269
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static g(Ljze;Ljava/lang/String;)Ljzh;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 276
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 277
    iget-object v6, v0, Ljzo;->eNX:[Ljzh;

    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v0, v6, v1

    .line 278
    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 283
    :goto_2
    return-object v0

    .line 277
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 276
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 283
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static h(Ljze;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 311
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    invoke-static {p0, p1}, Lgnq;->g(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 325
    :goto_0
    return-object p1

    .line 319
    :cond_0
    invoke-static {p0, p1}, Lgnq;->d(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_1

    .line 321
    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 325
    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public static i(Ljze;Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 386
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    iget-object v0, p0, Ljze;->eNa:[Ljzo;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    .line 390
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 391
    iget-object v3, p0, Ljze;->eNa:[Ljzo;

    aget-object v3, v3, v0

    .line 392
    iget-object v3, v3, Ljzo;->eNX:[Ljzh;

    array-length v3, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 393
    iget-object v3, p0, Ljze;->eNa:[Ljzo;

    aget-object v3, v3, v0

    iget-object v3, v3, Ljzo;->eNX:[Ljzh;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 390
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_0
    if-eqz p1, :cond_1

    .line 396
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljzo;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 399
    :cond_1
    iget-object v3, p0, Ljze;->eNa:[Ljzo;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljzo;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 404
    :cond_2
    return-object v2
.end method

.method private static isRtl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 594
    const-string v0, "ar"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "he"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "iw"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(Ljze;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 410
    iget-object v4, p0, Ljze;->eNa:[Ljzo;

    array-length v5, v4

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v2, v4, v3

    invoke-virtual {v2}, Ljzo;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 412
    :goto_1
    if-nez v2, :cond_2

    .line 413
    const-string v2, "SpokenLanguageUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#getDialectDisplayName - language not found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 423
    :goto_2
    return-object v0

    .line 410
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1

    .line 417
    :cond_2
    iget-object v1, v2, Ljzo;->eNX:[Ljzh;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/String;

    .line 419
    :goto_3
    array-length v3, v1

    if-ge v0, v3, :cond_3

    .line 420
    iget-object v3, v2, Ljzo;->eNX:[Ljzh;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 419
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 423
    goto :goto_2
.end method

.method public static k(Ljze;Ljava/lang/String;)Ljzh;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 444
    iget-object v5, p0, Ljze;->eNa:[Ljzo;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v0, v5, v4

    .line 445
    iget-object v7, v0, Ljzo;->eNX:[Ljzh;

    array-length v8, v7

    move v2, v3

    :goto_1
    if-ge v2, v8, :cond_1

    aget-object v0, v7, v2

    invoke-virtual {v0}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 446
    :goto_2
    if-eqz v0, :cond_2

    .line 450
    :goto_3
    return-object v0

    .line 445
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 444
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 450
    goto :goto_3
.end method

.method public static l(Ljze;Ljava/lang/String;)Ljava/util/Locale;
    .locals 2

    .prologue
    .line 519
    invoke-static {p0, p1}, Lgnq;->g(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v0

    .line 520
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljzh;->bwS()Z

    move-result v1

    if-nez v1, :cond_1

    .line 521
    :cond_0
    sget-object v0, Lgnq;->cQt:Ljava/util/Locale;

    .line 523
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Ljzh;->bwR()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lgnq;->cQt:Ljava/util/Locale;

    invoke-static {v0, v1}, Lepb;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method
