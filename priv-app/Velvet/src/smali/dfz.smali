.class public final Ldfz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private volatile bwL:I

.field public bwM:Ljava/lang/String;

.field private final mGsaConfigFlags:Lchk;


# direct methods
.method public constructor <init>(Lchk;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {}, Lenu;->auR()V

    .line 67
    iput-object p1, p0, Ldfz;->mGsaConfigFlags:Lchk;

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Ldfz;->bwL:I

    .line 69
    const-string v0, "com.google.android.googlequicksearchbox"

    iput-object v0, p0, Ldfz;->bwM:Ljava/lang/String;

    .line 70
    return-void
.end method

.method private i(ILjava/lang/String;)Z
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 75
    if-nez p2, :cond_0

    .line 76
    const-string p2, "com.google.android.googlequicksearchbox"

    .line 78
    :cond_0
    const/4 v0, 0x0

    .line 79
    iget v2, p0, Ldfz;->bwL:I

    if-eq v2, p1, :cond_1

    .line 80
    iput p1, p0, Ldfz;->bwL:I

    move v0, v1

    .line 83
    :cond_1
    iget-object v2, p0, Ldfz;->bwM:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 84
    iput-object p2, p0, Ldfz;->bwM:Ljava/lang/String;

    .line 87
    :goto_0
    return v1

    :cond_2
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/core/state/QueryState;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;)Z
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 97
    invoke-static {}, Lenu;->auR()V

    .line 98
    invoke-virtual {p1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    const/4 v0, 0x2

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    .line 130
    :goto_0
    return v0

    .line 102
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v1

    sget-object v2, Lgyt;->cZD:Lgyt;

    if-ne v1, v2, :cond_1

    .line 104
    invoke-direct {p0, v3, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 106
    invoke-virtual {p2}, Lcom/google/android/search/shared/service/ClientConfig;->ane()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    const/4 v0, 0x7

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/search/shared/service/ClientConfig;->ank()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 109
    invoke-virtual {p1}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    const/4 v0, 0x6

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 113
    :cond_3
    const/4 v0, 0x4

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 115
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 116
    invoke-virtual {p1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 119
    invoke-direct {p0, v3, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 122
    :cond_5
    const/4 v0, 0x3

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 125
    :cond_6
    iget-object v1, p0, Ldfz;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->HC()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 127
    const/4 v0, 0x5

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 130
    :cond_7
    const/4 v0, 0x0

    invoke-direct {p0, v0, p3}, Ldfz;->i(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final aD(Lcom/google/android/shared/search/Query;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 138
    iget v1, p0, Ldfz;->bwL:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldfz;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->GO()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abA()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 151
    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abB()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 159
    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_0

    iget v1, p0, Ldfz;->bwL:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abC()Z
    .locals 2

    .prologue
    .line 165
    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abD()Z
    .locals 2

    .prologue
    .line 169
    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abE()Z
    .locals 2

    .prologue
    .line 174
    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abF()Z
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abG()Z
    .locals 2

    .prologue
    .line 186
    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abH()Z
    .locals 2

    .prologue
    .line 190
    iget v0, p0, Ldfz;->bwL:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    invoke-static {}, Lenu;->auR()V

    .line 198
    iget-object v0, p0, Ldfz;->bwM:Ljava/lang/String;

    invoke-static {v0}, Lhgn;->nP(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Ldfz;->bwM:Ljava/lang/String;

    .line 201
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldfz;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Iw()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final abz()Z
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Ldfz;->bwL:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
