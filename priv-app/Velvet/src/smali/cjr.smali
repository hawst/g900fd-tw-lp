.class public final Lcjr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final aYY:Lghy;

.field private final aYZ:Lciu;

.field private final mRelationshipManager:Lcjg;


# direct methods
.method public constructor <init>(Lghy;Lciu;Lcjg;)V
    .locals 0
    .param p1    # Lghy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lciu;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcjr;->aYY:Lghy;

    .line 26
    iput-object p2, p0, Lcjr;->aYZ:Lciu;

    .line 27
    iput-object p3, p0, Lcjr;->mRelationshipManager:Lcjg;

    .line 28
    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    sget-object v0, Lcgg;->aVF:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    iget-object v0, p0, Lcjr;->aYY:Lghy;

    invoke-virtual {v0}, Lghy;->aFN()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcjr;->aYZ:Lciu;

    invoke-virtual {v1, v0}, Lciu;->b(Ljava/lang/Iterable;)V

    iget-object v0, p0, Lcjr;->mRelationshipManager:Lcjg;

    iget-object v1, v0, Lcjg;->aYx:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v1, v0, Lcjg;->aYy:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v1, v0, Lcjg;->aYw:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v1, v0, Lcjg;->aYz:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    invoke-virtual {v0}, Lcjg;->KW()V

    goto :goto_0
.end method
