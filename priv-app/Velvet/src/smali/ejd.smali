.class public final Lejd;
.super Landroid/widget/FrameLayout$LayoutParams;
.source "PG"


# instance fields
.field public cbc:Lcom/google/android/shared/ui/CoScrollContainer;

.field public cbd:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "regular"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "header"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x3
                to = "footer"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x4
                to = "header onwards"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x5
                to = "offscreen"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x6
                to = "draggable"
            .end subannotation
        }
    .end annotation
.end field

.field public cbe:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbf:Landroid/view/View;

.field public cbg:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbh:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbi:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbj:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbk:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbl:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbm:Lejf;

.field public cbn:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public cbo:J

.field public cbp:J

.field public final cbq:Ljava/lang/Runnable;

.field public cbr:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout_CoScrollContainer"
    .end annotation
.end field

.field public mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/shared/ui/CoScrollContainer;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1269
    invoke-direct {p0, p1, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1228
    new-instance v0, Leje;

    invoke-direct {v0, p0}, Leje;-><init>(Lejd;)V

    iput-object v0, p0, Lejd;->cbq:Ljava/lang/Runnable;

    .line 1270
    iput-object p2, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 1271
    sget-object v0, Lbwe;->aLP:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1272
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lejd;->cbe:Z

    .line 1274
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/ui/CoScrollContainer;)V
    .locals 1

    .prologue
    .line 1259
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lejd;-><init>(Lcom/google/android/shared/ui/CoScrollContainer;I)V

    .line 1260
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/ui/CoScrollContainer;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1263
    invoke-direct {p0, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1228
    new-instance v0, Leje;

    invoke-direct {v0, p0}, Leje;-><init>(Lejd;)V

    iput-object v0, p0, Lejd;->cbq:Ljava/lang/Runnable;

    .line 1264
    iput-object p1, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 1265
    iput p2, p0, Lejd;->cbd:I

    .line 1266
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/ui/CoScrollContainer;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 1277
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1228
    new-instance v0, Leje;

    invoke-direct {v0, p0}, Leje;-><init>(Lejd;)V

    iput-object v0, p0, Lejd;->cbq:Ljava/lang/Runnable;

    .line 1278
    iput-object p1, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 1279
    instance-of v0, p2, Lejd;

    if-eqz v0, :cond_0

    .line 1280
    check-cast p2, Lejd;

    .line 1281
    iget v0, p2, Lejd;->cbd:I

    iput v0, p0, Lejd;->cbd:I

    .line 1282
    iget-boolean v0, p2, Lejd;->cbe:Z

    iput-boolean v0, p0, Lejd;->cbe:Z

    .line 1283
    iget-object v0, p2, Lejd;->mView:Landroid/view/View;

    iput-object v0, p0, Lejd;->mView:Landroid/view/View;

    .line 1284
    iget-object v0, p2, Lejd;->cbf:Landroid/view/View;

    iput-object v0, p0, Lejd;->cbf:Landroid/view/View;

    .line 1285
    iget v0, p2, Lejd;->cbg:I

    iput v0, p0, Lejd;->cbg:I

    .line 1286
    iget v0, p2, Lejd;->cbj:I

    iput v0, p0, Lejd;->cbj:I

    .line 1287
    iget v0, p2, Lejd;->cbk:I

    iput v0, p0, Lejd;->cbk:I

    .line 1288
    iget-boolean v0, p2, Lejd;->cbl:Z

    iput-boolean v0, p0, Lejd;->cbl:Z

    .line 1289
    iget-object v0, p2, Lejd;->cbm:Lejf;

    iput-object v0, p0, Lejd;->cbm:Lejf;

    .line 1290
    iget v0, p2, Lejd;->cbn:I

    iput v0, p0, Lejd;->cbn:I

    .line 1292
    :cond_0
    return-void
.end method

.method private a(ILandroid/view/View;II)V
    .locals 2

    .prologue
    .line 1350
    iput p1, p0, Lejd;->cbd:I

    .line 1351
    iput-object p2, p0, Lejd;->cbf:Landroid/view/View;

    .line 1352
    iput p3, p0, Lejd;->cbg:I

    .line 1353
    iput p4, p0, Lejd;->cbi:I

    .line 1354
    iput p4, p0, Lejd;->cbh:I

    .line 1355
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_1

    .line 1356
    iget v0, p0, Lejd;->cbi:I

    if-lez v0, :cond_0

    .line 1357
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/shared/ui/CoScrollContainer;->cba:Z

    .line 1359
    :cond_0
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;)V

    .line 1360
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;I)V

    .line 1362
    :cond_1
    return-void
.end method


# virtual methods
.method public final aN(II)V
    .locals 3

    .prologue
    .line 1343
    const/4 v0, 0x1

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1345
    const/4 v0, 0x6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2, p2}, Lejd;->a(ILandroid/view/View;II)V

    .line 1346
    return-void
.end method

.method public final aO(II)V
    .locals 2

    .prologue
    .line 1366
    iput p1, p0, Lejd;->cbj:I

    .line 1367
    iput p2, p0, Lejd;->cbk:I

    .line 1369
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_0

    .line 1370
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;)V

    .line 1371
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;I)V

    .line 1373
    :cond_0
    return-void
.end method

.method public final atj()V
    .locals 1

    .prologue
    .line 1427
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_0

    .line 1428
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->atj()V

    .line 1430
    :cond_0
    return-void
.end method

.method public final ato()I
    .locals 5

    .prologue
    .line 1322
    iget-object v0, p0, Lejd;->cbm:Lejf;

    invoke-interface {v0}, Lejf;->ats()I

    move-result v0

    iget v1, p0, Lejd;->cbj:I

    iget v2, p0, Lejd;->cbk:I

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1324
    iget-object v1, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v1, :cond_0

    .line 1325
    iget v1, p0, Lejd;->cbj:I

    iget-object v2, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    iget v2, v2, Lcom/google/android/shared/ui/CoScrollContainer;->caQ:I

    sub-int/2addr v1, v2

    const/4 v2, 0x0

    iget v3, p0, Lejd;->cbk:I

    iget-object v4, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    iget v4, v4, Lcom/google/android/shared/ui/CoScrollContainer;->caR:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v2}, Lcom/google/android/shared/ui/CoScrollContainer;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1330
    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lejd;->cbk:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final atp()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1376
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1, v1}, Lejd;->a(ILandroid/view/View;II)V

    .line 1377
    return-void
.end method

.method public final atq()Z
    .locals 2

    .prologue
    .line 1380
    iget v0, p0, Lejd;->cbd:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final atr()F
    .locals 2

    .prologue
    .line 1420
    iget v0, p0, Lejd;->cbi:I

    int-to-float v0, v0

    iget v1, p0, Lejd;->cbh:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public final e(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 1339
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2, p3}, Lejd;->a(ILandroid/view/View;II)V

    .line 1340
    return-void
.end method

.method public final f(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1411
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    iget v1, v1, Lcom/google/android/shared/ui/CoScrollContainer;->caQ:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 1412
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    iget v1, v1, Lcom/google/android/shared/ui/CoScrollContainer;->caR:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 1413
    return-void
.end method

.method public final hK(I)I
    .locals 1

    .prologue
    .line 1302
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_0

    .line 1303
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->c(Lejd;I)I

    move-result p1

    .line 1305
    :cond_0
    return p1
.end method

.method public final hL(I)I
    .locals 1

    .prologue
    .line 1309
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_0

    .line 1310
    iget-object v0, p0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->d(Lejd;I)I

    move-result p1

    .line 1312
    :cond_0
    return p1
.end method

.method public final hM(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1334
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1335
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1, v1}, Lejd;->a(ILandroid/view/View;II)V

    .line 1336
    return-void

    :cond_0
    move v0, v1

    .line 1334
    goto :goto_0
.end method

.method public final setView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1296
    iput-object p1, p0, Lejd;->mView:Landroid/view/View;

    .line 1297
    instance-of v0, p1, Lejf;

    iput-boolean v0, p0, Lejd;->cbl:Z

    .line 1298
    iget-boolean v0, p0, Lejd;->cbl:Z

    if-eqz v0, :cond_0

    check-cast p1, Lejf;

    :goto_0
    iput-object p1, p0, Lejd;->cbm:Lejf;

    .line 1299
    return-void

    .line 1298
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1403
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
