.class public final Lhfr;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field private final GS:Landroid/view/LayoutInflater;

.field private final mOptOutSwitchHandler:Lcxn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcxn;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 563
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 564
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lhfr;->GS:Landroid/view/LayoutInflater;

    .line 565
    iput-object p2, p0, Lhfr;->mOptOutSwitchHandler:Lcxn;

    .line 566
    return-void
.end method

.method private static a(Landroid/preference/PreferenceActivity$Header;)I
    .locals 4

    .prologue
    .line 550
    iget-object v0, p0, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    const-string v1, "LOADING_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    const/4 v0, 0x3

    .line 557
    :goto_0
    return v0

    .line 552
    :cond_0
    iget-wide v0, p0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f1104c3

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 553
    const/4 v0, 0x2

    goto :goto_0

    .line 554
    :cond_1
    iget-object v0, p0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-nez v0, :cond_2

    .line 555
    const/4 v0, 0x0

    goto :goto_0

    .line 557
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Lhfs;Landroid/preference/PreferenceActivity$Header;)V
    .locals 3

    .prologue
    .line 728
    const/4 v0, 0x0

    .line 729
    iget v1, p1, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lhfs;->dfF:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 733
    const/4 v0, 0x1

    .line 735
    :cond_0
    iget-object v1, p0, Lhfs;->dfF:Landroid/widget/ImageView;

    iget v2, p1, Landroid/preference/PreferenceActivity$Header;->iconRes:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 736
    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lhfs;->dfF:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    .line 739
    :cond_1
    return-void
.end method

.method private b(Landroid/preference/PreferenceActivity$Header;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 724
    invoke-virtual {p0}, Lhfr;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 576
    const/4 v0, 0x0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 570
    invoke-virtual {p0, p1}, Lhfr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 571
    invoke-static {v0}, Lhfr;->a(Landroid/preference/PreferenceActivity$Header;)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v7, 0x1020016

    const v6, 0x1020010

    const/16 v9, 0x8

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 611
    invoke-virtual {p0, p1}, Lhfr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 612
    invoke-static {v0}, Lhfr;->a(Landroid/preference/PreferenceActivity$Header;)I

    move-result v5

    .line 615
    if-nez p2, :cond_1

    .line 616
    new-instance v4, Lhfs;

    invoke-direct {v4}, Lhfs;-><init>()V

    .line 617
    packed-switch v5, :pswitch_data_0

    move-object v2, v3

    .line 661
    :goto_0
    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v4

    .line 668
    :goto_1
    packed-switch v5, :pswitch_data_1

    .line 720
    :cond_0
    :goto_2
    return-object v2

    .line 619
    :pswitch_0
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lhfr;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x1010208

    invoke-direct {v2, v1, v3, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 622
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    .line 623
    mul-int/lit8 v6, v1, 0x2

    div-int/lit8 v7, v1, 0x2

    invoke-virtual {v2, v1, v6, v1, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    move-object v1, v2

    .line 629
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfG:Landroid/widget/TextView;

    goto :goto_0

    .line 633
    :pswitch_1
    iget-object v1, p0, Lhfr;->GS:Landroid/view/LayoutInflater;

    const v2, 0x7f0400ff

    invoke-virtual {v1, v2, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 636
    const v1, 0x7f11005e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v4, Lhfs;->dfF:Landroid/widget/ImageView;

    .line 637
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfG:Landroid/widget/TextView;

    .line 638
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfH:Landroid/widget/TextView;

    .line 639
    const v1, 0x7f1102dd

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, v4, Lhfs;->dfI:Landroid/widget/Switch;

    goto :goto_0

    .line 643
    :pswitch_2
    iget-object v1, p0, Lhfr;->GS:Landroid/view/LayoutInflater;

    const v2, 0x7f0400fd

    invoke-virtual {v1, v2, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 645
    const v1, 0x7f11005e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v4, Lhfs;->dfF:Landroid/widget/ImageView;

    .line 646
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfG:Landroid/widget/TextView;

    .line 647
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfH:Landroid/widget/TextView;

    goto :goto_0

    .line 651
    :pswitch_3
    iget-object v1, p0, Lhfr;->GS:Landroid/view/LayoutInflater;

    const v2, 0x7f0400ff

    invoke-virtual {v1, v2, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 653
    const v1, 0x7f11005e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v4, Lhfs;->dfF:Landroid/widget/ImageView;

    .line 654
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfG:Landroid/widget/TextView;

    .line 655
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v4, Lhfs;->dfH:Landroid/widget/TextView;

    .line 656
    const v1, 0x7f1102dd

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, v4, Lhfs;->dfI:Landroid/widget/Switch;

    .line 657
    const v1, 0x7f1102de

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v4, Lhfs;->dfJ:Landroid/widget/ProgressBar;

    goto/16 :goto_0

    .line 664
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhfs;

    move-object v2, p2

    goto/16 :goto_1

    .line 670
    :pswitch_4
    iget-object v1, v1, Lhfs;->dfG:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhfr;->b(Landroid/preference/PreferenceActivity$Header;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 674
    :pswitch_5
    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v6, 0x7f1104c3

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 675
    iget-object v4, p0, Lhfr;->mOptOutSwitchHandler:Lcxn;

    iget-object v5, v1, Lhfs;->dfI:Landroid/widget/Switch;

    invoke-virtual {v4, v5}, Lcxn;->a(Landroid/widget/Switch;)V

    .line 680
    :cond_2
    :pswitch_6
    iget-object v4, v1, Lhfs;->dfG:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhfr;->b(Landroid/preference/PreferenceActivity$Header;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 681
    invoke-static {v1, v0}, Lhfr;->a(Lhfs;Landroid/preference/PreferenceActivity$Header;)V

    .line 682
    invoke-virtual {p0}, Lhfr;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 683
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 684
    iget-object v5, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 685
    iget-object v5, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    iget-object v4, v0, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    if-eqz v4, :cond_3

    iget-object v0, v0, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    const-string v4, "notSelectableAndContainsLink"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 691
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 692
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 702
    :goto_3
    iget-object v0, v1, Lhfs;->dfJ:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, v1, Lhfs;->dfJ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 704
    iput-object v3, v1, Lhfs;->dfJ:Landroid/widget/ProgressBar;

    goto/16 :goto_2

    .line 696
    :cond_3
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 697
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    goto :goto_3

    .line 700
    :cond_4
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 709
    :pswitch_7
    iget-object v4, v1, Lhfs;->dfG:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lhfr;->b(Landroid/preference/PreferenceActivity$Header;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 710
    invoke-static {v1, v0}, Lhfr;->a(Lhfs;Landroid/preference/PreferenceActivity$Header;)V

    .line 711
    iget-object v4, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 712
    iget-object v4, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {p0}, Lhfr;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 713
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 714
    iget-object v0, v1, Lhfs;->dfH:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 715
    iget-object v0, v1, Lhfs;->dfI:Landroid/widget/Switch;

    invoke-virtual {v0, v9}, Landroid/widget/Switch;->setVisibility(I)V

    .line 716
    iget-object v0, v1, Lhfs;->dfJ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_2

    .line 617
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 668
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x4

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 605
    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 581
    invoke-virtual {p0, p1}, Lhfr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-static {v0}, Lhfr;->a(Landroid/preference/PreferenceActivity$Header;)I

    move-result v0

    .line 582
    if-eqz v0, :cond_0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    .line 595
    :goto_0
    return v0

    .line 587
    :cond_1
    invoke-virtual {p0, p1}, Lhfr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 588
    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v6, 0x7f1104c3

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 592
    iget-object v0, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 595
    goto :goto_0
.end method
