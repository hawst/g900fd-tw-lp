.class public final Lfgl;
.super Lfeu;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 38
    return-void
.end method

.method private a(Landroid/content/Context;Ljgj;ILjava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 136
    iget-object v1, p2, Ljgj;->ejL:[Ljgm;

    if-eqz v1, :cond_0

    iget-object v1, p2, Ljgj;->ejL:[Ljgm;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-object v0

    .line 139
    :cond_1
    invoke-static {p2, v4, p3}, Lfgl;->a(Ljgj;II)Ljava/lang/String;

    move-result-object v1

    .line 140
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 141
    invoke-static {p2, v5, p3}, Lfgl;->a(Ljgj;II)Ljava/lang/String;

    move-result-object v0

    .line 142
    const v2, 0x7f0a0264

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p4, v3, v4

    aput-object v1, v3, v5

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljgj;II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Ljgj;->ejL:[Ljgm;

    array-length v0, v0

    if-le v0, p1, :cond_0

    .line 149
    iget-object v0, p0, Ljgj;->ejL:[Ljgm;

    aget-object v0, v0, p1

    iget-object v0, v0, Ljgm;->ekh:[Ljava/lang/String;

    .line 150
    array-length v1, v0

    if-le v1, p2, :cond_0

    .line 151
    aget-object v0, v0, p2

    .line 154
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ljgn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Ljgn;->bjR()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Ljgn;->bjR()Ljava/lang/String;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljgn;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 256
    invoke-static {p1, p2}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 258
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 259
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 261
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "z"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 262
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 263
    const v2, 0x7f0a0242

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 265
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljge;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Ljge;->bjR()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    invoke-virtual {p0}, Ljge;->bjR()Ljava/lang/String;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljge;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v5, v0, Lizj;->dSk:Ljgj;

    .line 44
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v3, v0, Lizj;->dSm:Ljgg;

    .line 51
    if-eqz v5, :cond_3

    .line 52
    const/4 v0, 0x6

    invoke-virtual {v5}, Ljgj;->bew()I

    move-result v3

    if-ne v0, v3, :cond_1

    iget-object v0, v5, Ljgj;->ejY:Ljgl;

    invoke-virtual {v0}, Ljgl;->bkn()I

    move-result v0

    if-ne v0, v10, :cond_1

    move v0, v1

    .line 53
    :goto_0
    invoke-virtual {v5}, Ljgj;->bew()I

    move-result v3

    .line 54
    iget-object v6, v5, Ljgj;->ejN:[Ljgn;

    aget-object v6, v6, v2

    .line 55
    iget-object v5, v5, Ljgj;->ejN:[Ljgn;

    aget-object v8, v5, v1

    .line 56
    invoke-static {v6}, Lfgl;->a(Ljgn;)Ljava/lang/String;

    move-result-object v7

    .line 57
    invoke-static {v8}, Lfgl;->a(Ljgn;)Ljava/lang/String;

    move-result-object v5

    .line 58
    invoke-virtual {v6}, Ljgn;->bkG()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Ljgn;->bkG()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 59
    invoke-virtual {v6}, Ljgn;->rf()Ljava/lang/String;

    move-result-object v6

    .line 60
    invoke-virtual {v8}, Ljgn;->rf()Ljava/lang/String;

    move-result-object v4

    .line 86
    :goto_1
    invoke-static {v7}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 87
    invoke-static {v5}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 88
    if-eqz v6, :cond_8

    if-eqz v4, :cond_8

    if-nez v0, :cond_8

    .line 90
    if-ne v3, v11, :cond_7

    const v0, 0x7f0a0265

    .line 93
    :goto_2
    new-array v3, v11, [Ljava/lang/Object;

    aput-object v7, v3, v2

    aput-object v6, v3, v1

    aput-object v5, v3, v10

    const/4 v1, 0x3

    aput-object v4, v3, v1

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 95
    :cond_0
    :goto_3
    return-object v4

    :cond_1
    move v0, v2

    .line 52
    goto :goto_0

    :cond_2
    move-object v6, v4

    .line 65
    goto :goto_1

    :cond_3
    if-eqz v3, :cond_0

    iget-object v0, v3, Ljgg;->ejy:Ljgh;

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v3}, Ljgg;->bew()I

    move-result v0

    .line 67
    iget-object v5, v3, Ljgg;->ejy:Ljgh;

    iget-object v5, v5, Ljgh;->ejD:Ljge;

    .line 68
    iget-object v6, v3, Ljgg;->ejy:Ljgh;

    iget-object v7, v6, Ljgh;->ejE:Ljge;

    .line 69
    invoke-static {v5}, Lfgl;->b(Ljge;)Ljava/lang/String;

    move-result-object v6

    .line 70
    invoke-static {v7}, Lfgl;->b(Ljge;)Ljava/lang/String;

    move-result-object v5

    .line 72
    iget-object v7, v3, Ljgg;->ejy:Ljgh;

    iget-object v7, v7, Ljgh;->ejF:Ljgf;

    .line 73
    iget-object v8, v3, Ljgg;->ejy:Ljgh;

    iget-object v8, v8, Ljgh;->ejG:Ljgf;

    .line 74
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljgf;->bjV()Z

    move-result v9

    if-eqz v9, :cond_6

    if-eqz v8, :cond_6

    invoke-virtual {v8}, Ljgf;->bjV()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {v3}, Ljgg;->bjZ()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v3}, Ljgg;->bjY()Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v1

    :goto_4
    if-eqz v3, :cond_6

    .line 77
    invoke-virtual {v7}, Ljgf;->bjU()Ljava/lang/String;

    move-result-object v4

    .line 78
    invoke-virtual {v8}, Ljgf;->bjU()Ljava/lang/String;

    move-result-object v3

    move-object v7, v6

    move-object v6, v4

    move-object v4, v3

    move v3, v0

    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    .line 74
    goto :goto_4

    :cond_5
    move v3, v1

    goto :goto_4

    :cond_6
    move v3, v0

    move-object v7, v6

    move-object v6, v4

    move v0, v2

    .line 83
    goto :goto_1

    .line 90
    :cond_7
    const v0, 0x7f0a0261

    goto :goto_2

    .line 95
    :cond_8
    const v0, 0x7f0a0260

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v7, v3, v2

    aput-object v5, v3, v1

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method public final aG(Landroid/content/Context;)Lbk;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 104
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    .line 105
    if-eqz v0, :cond_0

    const/4 v1, 0x6

    invoke-virtual {v0}, Ljgj;->bew()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Ljgj;->getStatusCode()I

    move-result v1

    if-eq v1, v4, :cond_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 110
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    iget-object v2, v0, Ljgj;->ejN:[Ljgn;

    aget-object v2, v2, v5

    .line 112
    iget-object v3, v0, Ljgj;->ejN:[Ljgn;

    aget-object v3, v3, v4

    .line 113
    invoke-static {v2}, Lfgl;->a(Ljgn;)Ljava/lang/String;

    move-result-object v2

    .line 114
    invoke-static {v3}, Lfgl;->a(Ljgn;)Ljava/lang/String;

    move-result-object v3

    .line 115
    invoke-direct {p0, p1, v0, v5, v2}, Lfgl;->a(Landroid/content/Context;Ljgj;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    if-eqz v2, :cond_2

    .line 118
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    invoke-direct {p0, p1, v0, v4, v3}, Lfgl;->a(Landroid/content/Context;Ljgj;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_3

    .line 124
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    new-instance v0, Lax;

    invoke-direct {v0}, Lax;-><init>()V

    invoke-virtual {v0, v1}, Lax;->a(Ljava/lang/CharSequence;)Lax;

    move-result-object v0

    goto :goto_0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 182
    .line 184
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v4, v0, Lizj;->dSk:Ljgj;

    .line 186
    invoke-virtual {v4}, Ljgj;->getStatusCode()I

    move-result v6

    .line 187
    invoke-virtual {v4}, Ljgj;->bew()I

    move-result v0

    const/4 v5, 0x6

    if-ne v0, v5, :cond_4

    if-nez v6, :cond_4

    .line 189
    iget-object v0, v4, Ljgj;->ejY:Ljgl;

    invoke-virtual {v0}, Ljgl;->pr()Ljava/lang/String;

    move-result-object v0

    .line 191
    :goto_0
    invoke-virtual {v4}, Ljgj;->nk()J

    move-result-wide v4

    .line 211
    :goto_1
    packed-switch v6, :pswitch_data_0

    .line 224
    :cond_0
    :goto_2
    return-object v1

    .line 192
    :cond_1
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSm:Ljgg;

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSm:Ljgg;

    invoke-virtual {v0}, Ljgg;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_2

    :pswitch_0
    move v0, v2

    .line 206
    :goto_3
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v4

    iget-object v4, v4, Lizj;->dSm:Ljgg;

    invoke-virtual {v4}, Ljgg;->nk()J

    move-result-wide v4

    move v6, v0

    move-object v0, v1

    goto :goto_1

    :pswitch_1
    move v0, v3

    .line 199
    goto :goto_3

    .line 201
    :pswitch_2
    const/4 v0, 0x2

    .line 202
    goto :goto_3

    .line 213
    :pswitch_3
    if-nez v0, :cond_2

    const v0, 0x7f0a0248

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v1, v0

    goto :goto_2

    .line 215
    :pswitch_4
    const v0, 0x7f0a0268

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 217
    :pswitch_5
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v4

    .line 218
    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f0a0266

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lfgl;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-virtual {p1, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    const v4, 0x7f0a0267

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lfgl;->b(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-virtual {p1, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 193
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final ayY()I
    .locals 2

    .prologue
    const v1, 0x7f0200f1

    .line 230
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSk:Ljgj;

    invoke-virtual {v0}, Ljgj;->bew()I

    move-result v0

    .line 238
    :goto_0
    invoke-static {v0}, Lfwq;->jJ(I)I

    move-result v0

    .line 239
    if-eqz v0, :cond_2

    :goto_1
    return v0

    .line 232
    :cond_0
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSm:Ljgg;

    if-eqz v0, :cond_1

    .line 233
    invoke-virtual {p0}, Lfgl;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSm:Ljgg;

    invoke-virtual {v0}, Ljgg;->bew()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 235
    goto :goto_1

    :cond_2
    move v0, v1

    .line 239
    goto :goto_1
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 250
    sget-object v0, Lfgb;->cqz:Lfgb;

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method
