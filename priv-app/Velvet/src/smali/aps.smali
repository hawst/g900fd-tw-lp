.class public final Laps;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afY:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field private alK:Ljava/lang/String;

.field private alL:Ljava/lang/String;

.field private ala:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8731
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8732
    const/4 v0, 0x0

    iput v0, p0, Laps;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laps;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laps;->alK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laps;->afY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laps;->alL:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laps;->ala:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laps;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laps;->eCz:I

    .line 8733
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8602
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laps;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laps;->afh:Ljava/lang/String;

    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laps;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laps;->afY:Ljava/lang/String;

    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laps;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laps;->alK:Ljava/lang/String;

    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laps;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laps;->alL:Ljava/lang/String;

    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laps;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laps;->ala:Ljava/lang/String;

    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laps;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 8750
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8751
    const/4 v0, 0x1

    iget-object v1, p0, Laps;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8753
    :cond_0
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 8754
    const/4 v0, 0x2

    iget-object v1, p0, Laps;->afY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8756
    :cond_1
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 8757
    const/4 v0, 0x3

    iget-object v1, p0, Laps;->alK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8759
    :cond_2
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 8760
    const/4 v0, 0x4

    iget-object v1, p0, Laps;->alL:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8762
    :cond_3
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 8763
    const/4 v0, 0x5

    iget-object v1, p0, Laps;->ala:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8765
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8766
    return-void
.end method

.method public final dD(Ljava/lang/String;)Laps;
    .locals 1

    .prologue
    .line 8627
    if-nez p1, :cond_0

    .line 8628
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8630
    :cond_0
    iput-object p1, p0, Laps;->afh:Ljava/lang/String;

    .line 8631
    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laps;->aez:I

    .line 8632
    return-object p0
.end method

.method public final dE(Ljava/lang/String;)Laps;
    .locals 1

    .prologue
    .line 8649
    if-nez p1, :cond_0

    .line 8650
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8652
    :cond_0
    iput-object p1, p0, Laps;->alK:Ljava/lang/String;

    .line 8653
    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laps;->aez:I

    .line 8654
    return-object p0
.end method

.method public final dF(Ljava/lang/String;)Laps;
    .locals 1

    .prologue
    .line 8671
    if-nez p1, :cond_0

    .line 8672
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8674
    :cond_0
    iput-object p1, p0, Laps;->afY:Ljava/lang/String;

    .line 8675
    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laps;->aez:I

    .line 8676
    return-object p0
.end method

.method public final dG(Ljava/lang/String;)Laps;
    .locals 1

    .prologue
    .line 8693
    if-nez p1, :cond_0

    .line 8694
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8696
    :cond_0
    iput-object p1, p0, Laps;->alL:Ljava/lang/String;

    .line 8697
    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laps;->aez:I

    .line 8698
    return-object p0
.end method

.method public final dH(Ljava/lang/String;)Laps;
    .locals 1

    .prologue
    .line 8715
    if-nez p1, :cond_0

    .line 8716
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8718
    :cond_0
    iput-object p1, p0, Laps;->ala:Ljava/lang/String;

    .line 8719
    iget v0, p0, Laps;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laps;->aez:I

    .line 8720
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8624
    iget-object v0, p0, Laps;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 8770
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8771
    iget v1, p0, Laps;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8772
    const/4 v1, 0x1

    iget-object v2, p0, Laps;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8775
    :cond_0
    iget v1, p0, Laps;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 8776
    const/4 v1, 0x2

    iget-object v2, p0, Laps;->afY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8779
    :cond_1
    iget v1, p0, Laps;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 8780
    const/4 v1, 0x3

    iget-object v2, p0, Laps;->alK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8783
    :cond_2
    iget v1, p0, Laps;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 8784
    const/4 v1, 0x4

    iget-object v2, p0, Laps;->alL:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8787
    :cond_3
    iget v1, p0, Laps;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 8788
    const/4 v1, 0x5

    iget-object v2, p0, Laps;->ala:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8791
    :cond_4
    return v0
.end method

.method public final on()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8668
    iget-object v0, p0, Laps;->afY:Ljava/lang/String;

    return-object v0
.end method

.method public final sx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8712
    iget-object v0, p0, Laps;->ala:Ljava/lang/String;

    return-object v0
.end method

.method public final sy()Z
    .locals 1

    .prologue
    .line 8723
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8646
    iget-object v0, p0, Laps;->alK:Ljava/lang/String;

    return-object v0
.end method

.method public final to()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8690
    iget-object v0, p0, Laps;->alL:Ljava/lang/String;

    return-object v0
.end method

.method public final tp()Z
    .locals 1

    .prologue
    .line 8701
    iget v0, p0, Laps;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
