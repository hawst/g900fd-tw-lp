.class public final Lano;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afY:Ljava/lang/String;

.field private afZ:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field public ahM:Laoi;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11816
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 11817
    const/4 v0, 0x0

    iput v0, p0, Lano;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lano;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lano;->afY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lano;->afZ:Ljava/lang/String;

    iput-object v1, p0, Lano;->ahM:Laoi;

    iput-object v1, p0, Lano;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lano;->eCz:I

    .line 11818
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11728
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lano;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lano;->afh:Ljava/lang/String;

    iget v0, p0, Lano;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lano;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lano;->afY:Ljava/lang/String;

    iget v0, p0, Lano;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lano;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lano;->afZ:Ljava/lang/String;

    iget v0, p0, Lano;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lano;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lano;->ahM:Laoi;

    if-nez v0, :cond_1

    new-instance v0, Laoi;

    invoke-direct {v0}, Laoi;-><init>()V

    iput-object v0, p0, Lano;->ahM:Laoi;

    :cond_1
    iget-object v0, p0, Lano;->ahM:Laoi;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 11834
    iget v0, p0, Lano;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11835
    const/4 v0, 0x1

    iget-object v1, p0, Lano;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11837
    :cond_0
    iget v0, p0, Lano;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 11838
    const/4 v0, 0x2

    iget-object v1, p0, Lano;->afY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11840
    :cond_1
    iget v0, p0, Lano;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 11841
    const/4 v0, 0x3

    iget-object v1, p0, Lano;->afZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11843
    :cond_2
    iget-object v0, p0, Lano;->ahM:Laoi;

    if-eqz v0, :cond_3

    .line 11844
    const/4 v0, 0x4

    iget-object v1, p0, Lano;->ahM:Laoi;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 11846
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 11847
    return-void
.end method

.method public final aF(Ljava/lang/String;)Lano;
    .locals 1

    .prologue
    .line 11753
    if-nez p1, :cond_0

    .line 11754
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11756
    :cond_0
    iput-object p1, p0, Lano;->afh:Ljava/lang/String;

    .line 11757
    iget v0, p0, Lano;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lano;->aez:I

    .line 11758
    return-object p0
.end method

.method public final aG(Ljava/lang/String;)Lano;
    .locals 1

    .prologue
    .line 11775
    if-nez p1, :cond_0

    .line 11776
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11778
    :cond_0
    iput-object p1, p0, Lano;->afY:Ljava/lang/String;

    .line 11779
    iget v0, p0, Lano;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lano;->aez:I

    .line 11780
    return-object p0
.end method

.method public final aH(Ljava/lang/String;)Lano;
    .locals 1

    .prologue
    .line 11797
    if-nez p1, :cond_0

    .line 11798
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11800
    :cond_0
    iput-object p1, p0, Lano;->afZ:Ljava/lang/String;

    .line 11801
    iget v0, p0, Lano;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lano;->aez:I

    .line 11802
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11750
    iget-object v0, p0, Lano;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 11851
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 11852
    iget v1, p0, Lano;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11853
    const/4 v1, 0x1

    iget-object v2, p0, Lano;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11856
    :cond_0
    iget v1, p0, Lano;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 11857
    const/4 v1, 0x2

    iget-object v2, p0, Lano;->afY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11860
    :cond_1
    iget v1, p0, Lano;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 11861
    const/4 v1, 0x3

    iget-object v2, p0, Lano;->afZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11864
    :cond_2
    iget-object v1, p0, Lano;->ahM:Laoi;

    if-eqz v1, :cond_3

    .line 11865
    const/4 v1, 0x4

    iget-object v2, p0, Lano;->ahM:Laoi;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11868
    :cond_3
    return v0
.end method

.method public final on()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11772
    iget-object v0, p0, Lano;->afY:Ljava/lang/String;

    return-object v0
.end method

.method public final oo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11794
    iget-object v0, p0, Lano;->afZ:Ljava/lang/String;

    return-object v0
.end method
