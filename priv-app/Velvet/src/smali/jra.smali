.class public final Ljra;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ezN:Ljava/lang/String;

.field private ezO:Ljava/lang/String;

.field private ezP:Ljava/lang/String;

.field private ezQ:Ljava/lang/String;

.field public ezR:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7674
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7675
    const/4 v0, 0x0

    iput v0, p0, Ljra;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljra;->ezN:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljra;->ezO:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljra;->ezP:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljra;->ezQ:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljra;->ezR:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljra;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljra;->eCz:I

    .line 7676
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7564
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljra;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljra;->ezN:Ljava/lang/String;

    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljra;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljra;->ezO:Ljava/lang/String;

    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljra;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljra;->ezP:Ljava/lang/String;

    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljra;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljra;->ezQ:Ljava/lang/String;

    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljra;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljra;->ezR:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljra;->ezR:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljra;->ezR:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljra;->ezR:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 7693
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7694
    const/4 v0, 0x1

    iget-object v1, p0, Ljra;->ezN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7696
    :cond_0
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7697
    const/4 v0, 0x2

    iget-object v1, p0, Ljra;->ezO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7699
    :cond_1
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 7700
    const/4 v0, 0x3

    iget-object v1, p0, Ljra;->ezP:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7702
    :cond_2
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 7703
    const/4 v0, 0x4

    iget-object v1, p0, Ljra;->ezQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7705
    :cond_3
    iget-object v0, p0, Ljra;->ezR:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljra;->ezR:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 7706
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljra;->ezR:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 7707
    iget-object v1, p0, Ljra;->ezR:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 7708
    if-eqz v1, :cond_4

    .line 7709
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7706
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7713
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7714
    return-void
.end method

.method public final bsQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7586
    iget-object v0, p0, Ljra;->ezN:Ljava/lang/String;

    return-object v0
.end method

.method public final bsR()Z
    .locals 1

    .prologue
    .line 7597
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7608
    iget-object v0, p0, Ljra;->ezO:Ljava/lang/String;

    return-object v0
.end method

.method public final bsT()Z
    .locals 1

    .prologue
    .line 7619
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7630
    iget-object v0, p0, Ljra;->ezP:Ljava/lang/String;

    return-object v0
.end method

.method public final bsV()Z
    .locals 1

    .prologue
    .line 7641
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7652
    iget-object v0, p0, Ljra;->ezQ:Ljava/lang/String;

    return-object v0
.end method

.method public final bsX()Z
    .locals 1

    .prologue
    .line 7663
    iget v0, p0, Ljra;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7718
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7719
    iget v2, p0, Ljra;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 7720
    const/4 v2, 0x1

    iget-object v3, p0, Ljra;->ezN:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7723
    :cond_0
    iget v2, p0, Ljra;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 7724
    const/4 v2, 0x2

    iget-object v3, p0, Ljra;->ezO:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7727
    :cond_1
    iget v2, p0, Ljra;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 7728
    const/4 v2, 0x3

    iget-object v3, p0, Ljra;->ezP:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7731
    :cond_2
    iget v2, p0, Ljra;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 7732
    const/4 v2, 0x4

    iget-object v3, p0, Ljra;->ezQ:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7735
    :cond_3
    iget-object v2, p0, Ljra;->ezR:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljra;->ezR:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 7738
    :goto_0
    iget-object v4, p0, Ljra;->ezR:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 7739
    iget-object v4, p0, Ljra;->ezR:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 7740
    if-eqz v4, :cond_4

    .line 7741
    add-int/lit8 v3, v3, 0x1

    .line 7742
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7738
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7746
    :cond_5
    add-int/2addr v0, v2

    .line 7747
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 7749
    :cond_6
    return v0
.end method

.method public final ys(Ljava/lang/String;)Ljra;
    .locals 1

    .prologue
    .line 7589
    if-nez p1, :cond_0

    .line 7590
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7592
    :cond_0
    iput-object p1, p0, Ljra;->ezN:Ljava/lang/String;

    .line 7593
    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljra;->aez:I

    .line 7594
    return-object p0
.end method

.method public final yt(Ljava/lang/String;)Ljra;
    .locals 1

    .prologue
    .line 7611
    if-nez p1, :cond_0

    .line 7612
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7614
    :cond_0
    iput-object p1, p0, Ljra;->ezO:Ljava/lang/String;

    .line 7615
    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljra;->aez:I

    .line 7616
    return-object p0
.end method

.method public final yu(Ljava/lang/String;)Ljra;
    .locals 1

    .prologue
    .line 7633
    if-nez p1, :cond_0

    .line 7634
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7636
    :cond_0
    iput-object p1, p0, Ljra;->ezP:Ljava/lang/String;

    .line 7637
    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljra;->aez:I

    .line 7638
    return-object p0
.end method

.method public final yv(Ljava/lang/String;)Ljra;
    .locals 1

    .prologue
    .line 7655
    if-nez p1, :cond_0

    .line 7656
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7658
    :cond_0
    iput-object p1, p0, Ljra;->ezQ:Ljava/lang/String;

    .line 7659
    iget v0, p0, Ljra;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljra;->aez:I

    .line 7660
    return-object p0
.end method
