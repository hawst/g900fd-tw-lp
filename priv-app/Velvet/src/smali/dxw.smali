.class public final Ldxw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final bPT:[Ldyf;


# instance fields
.field private final bPU:Ljava/util/Map;

.field private final bPV:Lelh;

.field private final bPW:Lelh;

.field private final bPX:Lelh;

.field private final bPY:Lelh;

.field private final bPZ:Lelh;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Ldyf;

    const/4 v1, 0x0

    sget-object v2, Ldyf;->bQC:Ldyf;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ldyf;->bQD:Ldyf;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ldyf;->bQE:Ldyf;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ldyf;->bQF:Ldyf;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Ldyf;->bQG:Ldyf;

    aput-object v2, v0, v1

    sput-object v0, Ldxw;->bPT:[Ldyf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-static {v0}, Ldxw;->n([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Ldxw;->bPU:Ljava/util/Map;

    .line 56
    new-instance v1, Lelh;

    const/4 v2, 0x5

    aget-object v0, v0, v2

    invoke-direct {v1, v0, v7, v5}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v1, p0, Ldxw;->bPV:Lelh;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 59
    new-instance v1, Lelh;

    aget-object v2, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v1, p0, Ldxw;->bPW:Lelh;

    .line 61
    new-instance v1, Lelh;

    aget-object v2, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v1, p0, Ldxw;->bPX:Lelh;

    .line 63
    new-instance v1, Lelh;

    aget-object v2, v0, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v1, p0, Ldxw;->bPY:Lelh;

    .line 66
    new-instance v1, Lelh;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    invoke-direct {v1, v0, v7, v5}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v1, p0, Ldxw;->bPZ:Lelh;

    .line 67
    return-void
.end method

.method public static a(ZJJ)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 156
    if-nez p0, :cond_2

    invoke-static {p3, p4, p1, p2}, Lesi;->i(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    sget-object v0, Ldxw;->bPT:[Ldyf;

    array-length v0, v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 158
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 159
    invoke-virtual {v1, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 160
    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 162
    sget-object v5, Ldxw;->bPT:[Ldyf;

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v7, v5, v3

    .line 163
    iget v1, v7, Ldyf;->bQH:I

    add-int/lit8 v8, v4, 0x1

    if-le v1, v8, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 164
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 163
    goto :goto_1

    .line 170
    :cond_2
    sget-object v0, Ldxw;->bPT:[Ldyf;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method public static aA(J)Ljava/util/List;
    .locals 4

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, p0, p1, v2, v3}, Ldxw;->a(ZJJ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static az(J)Z
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 127
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 128
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static n([Ljava/lang/String;)Ljava/util/Map;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    move v0, v1

    .line 134
    :goto_0
    sget-object v3, Ldxw;->bPT:[Ldyf;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 135
    sget-object v3, Ldxw;->bPT:[Ldyf;

    aget-object v3, v3, v0

    aget-object v4, p0, v0

    new-instance v5, Lelh;

    invoke-direct {v5, v4, v3, v1}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-interface {v2, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_0
    return-object v2
.end method

.method public static z(Ljava/util/List;)[Lelh;
    .locals 1

    .prologue
    .line 185
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lelh;

    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lelh;

    return-object v0
.end method


# virtual methods
.method public final a(JLcom/google/android/search/shared/actions/RemindersConfigFlags;)Ljava/util/List;
    .locals 3
    .param p3    # Lcom/google/android/search/shared/actions/RemindersConfigFlags;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    invoke-static {p1, p2}, Ldxw;->az(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Ldxw;->bPW:Lelh;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    iget-object v1, p0, Ldxw;->bPX:Lelh;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aii()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    iget-object v1, p0, Ldxw;->bPY:Lelh;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_1
    iget-object v1, p0, Ldxw;->bPZ:Lelh;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    return-object v0
.end method

.method public final akX()Lelh;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Ldxw;->bPV:Lelh;

    return-object v0
.end method

.method public final akY()Lelh;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ldxw;->bPW:Lelh;

    return-object v0
.end method

.method public final akZ()Lelh;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ldxw;->bPX:Lelh;

    return-object v0
.end method

.method public final ala()Lelh;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Ldxw;->bPY:Lelh;

    return-object v0
.end method

.method public final alb()Lelh;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Ldxw;->bPZ:Lelh;

    return-object v0
.end method

.method public final b(Ldyf;)Lelh;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ldxw;->bPU:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    return-object v0
.end method

.method public final y(Ljava/util/List;)Ljava/util/List;
    .locals 4

    .prologue
    .line 70
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 72
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyf;

    .line 73
    iget-object v3, p0, Ldxw;->bPU:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_0
    iget-object v0, p0, Ldxw;->bPV:Lelh;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-object v1
.end method
