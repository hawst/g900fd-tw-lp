.class final Lcmr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private final aWe:Ljava/lang/Object;

.field private ag:I

.field private bcu:Lefq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 784
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcmr;->aWe:Ljava/lang/Object;

    .line 792
    const/4 v0, 0x0

    iput v0, p0, Lcmr;->ag:I

    .line 793
    const/4 v0, 0x0

    iput-object v0, p0, Lcmr;->bcu:Lefq;

    .line 794
    return-void
.end method

.method private static a(ILjava/io/IOException;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 875
    packed-switch p0, :pswitch_data_0

    .line 881
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 876
    :pswitch_0
    const-string v0, "loading"

    goto :goto_0

    .line 877
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed with error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 878
    :pswitch_2
    const-string v0, "cancelled"

    goto :goto_0

    .line 879
    :pswitch_3
    const-string v0, "complete"

    goto :goto_0

    .line 880
    :pswitch_4
    const-string v0, "reloading"

    goto :goto_0

    .line 875
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final QA()Z
    .locals 3

    .prologue
    .line 851
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 852
    :try_start_0
    iget v0, p0, Lcmr;->ag:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 853
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final QB()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 869
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 870
    :try_start_0
    iget v2, p0, Lcmr;->ag:I

    if-eq v2, v0, :cond_0

    iget v2, p0, Lcmr;->ag:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 871
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qn()Z
    .locals 2

    .prologue
    .line 833
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 834
    :try_start_0
    iget v0, p0, Lcmr;->ag:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 835
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qo()Lefq;
    .locals 2

    .prologue
    .line 857
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 858
    :try_start_0
    iget-object v0, p0, Lcmr;->bcu:Lefq;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 859
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 894
    const-string v0, "SearchResultState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 895
    const-string v0, "state"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcmr;->ag:I

    iget-object v2, p0, Lcmr;->bcu:Lefq;

    invoke-static {v1, v2}, Lcmr;->a(ILjava/io/IOException;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 896
    return-void
.end method

.method a(ILefq;)Z
    .locals 5

    .prologue
    .line 797
    const/4 v0, 0x0

    .line 798
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 799
    :try_start_0
    invoke-virtual {p0}, Lcmr;->Qn()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcmr;->QA()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 800
    :cond_0
    iput p1, p0, Lcmr;->ag:I

    .line 801
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 802
    iput-object p2, p0, Lcmr;->bcu:Lefq;

    .line 804
    :cond_1
    const/4 v0, 0x1

    .line 806
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    if-nez v0, :cond_3

    .line 808
    iget v1, p0, Lcmr;->ag:I

    if-eq v1, p1, :cond_3

    .line 809
    const-string v1, "Velvet.SearchResult"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot set state to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcmr;->a(ILjava/io/IOException;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " because it is already "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcmr;->ag:I

    iget-object v4, p0, Lcmr;->bcu:Lefq;

    invoke-static {v3, v4}, Lcmr;->a(ILjava/io/IOException;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    :cond_3
    return v0

    .line 806
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isCancelled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 863
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 864
    :try_start_0
    iget v2, p0, Lcmr;->ag:I

    if-ne v2, v0, :cond_0

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 865
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isComplete()Z
    .locals 3

    .prologue
    .line 839
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 840
    :try_start_0
    iget v0, p0, Lcmr;->ag:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 841
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isFailed()Z
    .locals 3

    .prologue
    .line 845
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 846
    :try_start_0
    iget v0, p0, Lcmr;->ag:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 847
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 887
    iget-object v1, p0, Lcmr;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 888
    :try_start_0
    iget v0, p0, Lcmr;->ag:I

    iget-object v2, p0, Lcmr;->bcu:Lefq;

    invoke-static {v0, v2}, Lcmr;->a(ILjava/io/IOException;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 889
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
