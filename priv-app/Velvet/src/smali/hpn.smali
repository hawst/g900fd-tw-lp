.class public final Lhpn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bNc:J

.field public dkV:Lhqs;

.field public dnE:Lhpq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public dnF:Lhpp;

.field public mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/actions/SetReminderAction;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhpn;->bNc:J

    .line 56
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/SetReminderAction;

    iput-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    .line 57
    return-void
.end method

.method private static lo(I)Lizj;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 541
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-virtual {v0, v1}, Lizj;->nV(I)Lizj;

    move-result-object v0

    .line 543
    new-array v1, v1, [Liwk;

    const/4 v2, 0x0

    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    invoke-virtual {v3, p0}, Liwk;->nb(I)Liwk;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, v0, Lizj;->dUo:[Liwk;

    .line 544
    return-object v0
.end method


# virtual methods
.method public final a(JLdyf;)V
    .locals 3
    .param p3    # Ldyf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 252
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 253
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 255
    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/search/shared/actions/SetReminderAction;->ax(J)V

    .line 257
    invoke-virtual {p0, p3}, Lhpn;->a(Ldyf;)V

    .line 258
    if-nez p3, :cond_0

    .line 259
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lhpn;->p(II)V

    .line 264
    :goto_0
    invoke-virtual {p0, v0}, Lhpn;->b(Ljava/util/Calendar;)V

    .line 265
    return-void

    .line 262
    :cond_0
    iget v1, p3, Ldyf;->bQH:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lhpn;->p(II)V

    goto :goto_0
.end method

.method public final a(Ldyf;)V
    .locals 6

    .prologue
    .line 346
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->air()Z

    move-result v0

    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v2

    iget-wide v4, p0, Lhpn;->bNc:J

    invoke-static {v0, v2, v3, v4, v5}, Ldxw;->a(ZJJ)Ljava/util/List;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->b(Ldyf;Ljava/util/List;)V

    .line 351
    iget-object v1, p0, Lhpn;->dnE:Lhpq;

    if-eqz v1, :cond_0

    .line 352
    iget-object v1, p0, Lhpn;->dnE:Lhpq;

    iget-object v2, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiq()Ldyf;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lhpq;->a(Ldyf;Ljava/util/List;)V

    .line 355
    :cond_0
    return-void
.end method

.method public final a(Ljls;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ljls;)V

    .line 119
    invoke-virtual {p0}, Lhpn;->aRq()V

    .line 120
    return-void
.end method

.method final aRq()V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiA()Ljpd;

    move-result-object v1

    invoke-virtual {v1}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiB()Ljpd;

    move-result-object v2

    invoke-virtual {v2}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lhpq;->j(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_0
    return-void
.end method

.method public final aRr()Lhpm;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const v8, 0x7f0200cd

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 141
    :goto_0
    iget-object v4, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v4}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiy()I

    move-result v4

    .line 142
    if-ne v4, v1, :cond_2

    .line 143
    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->ais()Laiw;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    .line 146
    new-instance v0, Lhpm;

    const v1, 0x7f0200a7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v4, 0x7f0a0897

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v4, v3, v2}, Lhpm;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    .line 171
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 140
    goto :goto_0

    .line 152
    :cond_1
    new-instance v1, Lhpm;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f0a0893

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v3, v4, v2, v0}, Lhpm;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    move-object v0, v1

    goto :goto_1

    .line 157
    :cond_2
    const/4 v1, 0x2

    if-ne v4, v1, :cond_3

    .line 158
    new-instance v1, Lhpm;

    const v3, 0x7f0200cb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f0a0894

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v3, v4, v2, v0}, Lhpm;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    move-object v0, v1

    goto :goto_1

    .line 163
    :cond_3
    const/4 v1, 0x4

    if-ne v4, v1, :cond_4

    .line 164
    new-instance v1, Lhpm;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f0a0895

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v3, v4, v2, v0}, Lhpm;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V

    move-object v0, v1

    goto :goto_1

    .line 170
    :cond_4
    const-string v0, "EditReminderPresenter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown trigger type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 171
    goto :goto_1
.end method

.method public final aRs()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 498
    invoke-virtual {p0}, Lhpn;->aRt()Landroid/util/Pair;

    move-result-object v2

    .line 499
    if-eqz v2, :cond_2

    .line 500
    if-eqz v2, :cond_1

    iget-object v3, p0, Lhpn;->dnE:Lhpq;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhpn;->dnE:Lhpq;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v3, v0}, Lhpq;->dk(I)V

    :cond_0
    move v0, v1

    .line 505
    :goto_0
    return v0

    .line 500
    :cond_1
    iget-object v2, p0, Lhpn;->dnF:Lhpp;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lhpn;->dnF:Lhpp;

    invoke-interface {v1, v0}, Lhpp;->bC(Z)V

    goto :goto_0

    .line 501
    :cond_2
    iget-object v2, p0, Lhpn;->dnF:Lhpp;

    if-eqz v2, :cond_3

    .line 502
    iget-object v1, p0, Lhpn;->dnF:Lhpp;

    invoke-interface {v1, v0}, Lhpp;->bC(Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 505
    goto :goto_0
.end method

.method public final aRt()Landroid/util/Pair;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiy()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 519
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiz()Ljpd;

    move-result-object v0

    .line 520
    invoke-virtual {v0}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ljpd;->esI:Ljne;

    if-eqz v1, :cond_0

    .line 521
    iget-object v0, v0, Ljpd;->esI:Ljne;

    invoke-virtual {v0}, Ljne;->bqz()I

    move-result v0

    .line 522
    packed-switch v0, :pswitch_data_0

    .line 530
    const-string v1, "EditReminderPresenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#getRequiredAliasChange: unknown alias "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 524
    :pswitch_0
    const/16 v0, 0x11

    invoke-static {v0}, Lhpn;->lo(I)Lizj;

    move-result-object v0

    const v1, 0x7f0a089d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 527
    :pswitch_1
    const/16 v0, 0x12

    invoke-static {v0}, Lhpn;->lo(I)Lizj;

    move-result-object v0

    const v1, 0x7f0a089f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 522
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Laiw;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->b(Laiw;)V

    .line 227
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0, p1}, Lhpq;->b(Laiw;)V

    .line 230
    :cond_0
    return-void
.end method

.method b(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 281
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lhpn;->r(III)V

    .line 284
    return-void
.end method

.method public final c(Lhqq;Lefk;)V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lhpn;->dkV:Lhqs;

    iget-object v0, v0, Lhqs;->aoG:Lhla;

    invoke-virtual {v0, p1, p2}, Lhla;->a(Lhqq;Lefk;)V

    .line 360
    return-void
.end method

.method public final dj(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 198
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    :cond_0
    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 201
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->dj(I)V

    .line 203
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0, p1}, Lhpq;->dj(I)V

    .line 206
    :cond_1
    return-void

    .line 198
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljpd;)V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->e(Ljpd;)V

    .line 382
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p1, Ljpd;->esI:Ljne;

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p1, Ljpd;->esI:Ljne;

    .line 385
    invoke-virtual {v0}, Ljne;->bqA()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lhpn;->dnE:Lhpq;

    invoke-virtual {v0}, Ljne;->bqz()I

    move-result v0

    invoke-interface {v1, v0}, Lhpq;->dl(I)V

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiz()Ljpd;

    move-result-object v1

    invoke-interface {v0, v1}, Lhpq;->c(Ljpd;)V

    .line 392
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0}, Lhpq;->vh()V

    goto :goto_0
.end method

.method public final em(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->em(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0, p1}, Lhpq;->em(Ljava/lang/String;)V

    .line 180
    :cond_0
    return-void
.end method

.method public final j(Ljpd;)V
    .locals 2
    .param p1    # Ljpd;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 454
    if-eqz p1, :cond_0

    .line 455
    invoke-virtual {p0}, Lhpn;->aRt()Landroid/util/Pair;

    move-result-object v0

    .line 456
    iget-object v1, p0, Lhpn;->dkV:Lhqs;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lizj;

    iget-object v1, v1, Lhqs;->aoG:Lhla;

    invoke-virtual {v1, v0, p1}, Lhla;->a(Lizj;Ljpd;)V

    .line 457
    iget-object v0, p0, Lhpn;->dnF:Lhpp;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lhpn;->dnF:Lhpp;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lhpp;->bC(Z)V

    .line 461
    :cond_0
    return-void
.end method

.method public final p(II)V
    .locals 4

    .prologue
    .line 334
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/shared/actions/SetReminderAction;->p(II)V

    .line 335
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lhpq;->E(J)V

    .line 338
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiq()Ldyf;

    move-result-object v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0}, Lhpq;->vj()V

    .line 342
    :cond_0
    return-void
.end method

.method public final r(III)V
    .locals 6

    .prologue
    .line 292
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/shared/actions/SetReminderAction;->r(III)V

    .line 294
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    if-eqz v0, :cond_1

    .line 295
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v0

    .line 296
    iget-object v2, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiq()Ldyf;

    move-result-object v2

    .line 298
    iget-object v3, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v3, v0, v1}, Lhpq;->D(J)V

    .line 300
    invoke-static {v0, v1}, Lesi;->isToday(J)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lhpn;->bNc:J

    invoke-static {v4, v5}, Ldxw;->az(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 302
    iget-object v3, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v3}, Lhpq;->vf()V

    .line 310
    :goto_0
    invoke-virtual {p0, v2}, Lhpn;->a(Ldyf;)V

    .line 313
    iget-object v2, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->ais()Laiw;

    move-result-object v2

    .line 314
    invoke-static {v0, v1, v2}, Ldxx;->a(JLaiw;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0, v1, v2}, Ldxx;->b(JLaiw;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    :cond_0
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0, v2}, Lhpq;->b(Laiw;)V

    .line 319
    :cond_1
    return-void

    .line 303
    :cond_2
    invoke-static {v0, v1}, Lesi;->aY(J)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 304
    iget-object v3, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v3}, Lhpq;->vg()V

    goto :goto_0

    .line 306
    :cond_3
    iget-object v3, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v3}, Lhpq;->vi()V

    goto :goto_0
.end method

.method public final ui()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    iget-wide v2, p0, Lhpn;->bNc:J

    invoke-interface {v0, v2, v3}, Lhpq;->F(J)V

    .line 80
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhpn;->em(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiv()Ljkt;

    move-result-object v0

    iget-object v1, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->b(Ljkt;)V

    iget-object v1, p0, Lhpn;->dnE:Lhpq;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    iget-object v0, p0, Lhpn;->dnE:Lhpq;

    invoke-interface {v0}, Lhpq;->vl()V

    .line 82
    :cond_0
    :goto_0
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v0

    iget-object v2, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiq()Ldyf;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lhpn;->a(JLdyf;)V

    .line 83
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiz()Ljpd;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhpn;->e(Ljpd;)V

    .line 84
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiy()I

    move-result v0

    invoke-virtual {p0, v0}, Lhpn;->dj(I)V

    .line 85
    iget-object v0, p0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->ais()Laiw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhpn;->b(Laiw;)V

    .line 86
    invoke-virtual {p0}, Lhpn;->aRq()V

    .line 87
    return-void

    .line 81
    :cond_1
    sget-object v1, Ljmg;->etv:Ljsm;

    invoke-virtual {v0, v1}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhpn;->dnE:Lhpq;

    sget-object v2, Ljmg;->etv:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmg;

    iget-object v2, v0, Ljmg;->etw:[Ljks;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v0, v0, Ljmg;->etw:[Ljks;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-static {v0}, Lico;->a(Ljks;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v1, v0}, Lhpq;->eA(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
