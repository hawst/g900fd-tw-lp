.class public final Lcsj;
.super Ljsl;
.source "PG"


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private ajk:Ljava/lang/String;

.field private bhM:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcsj;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcsj;->bhM:J

    const-string v0, ""

    iput-object v0, p0, Lcsj;->aeE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcsj;->ajk:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcsj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcsj;->eCz:I

    .line 90
    return-void
.end method

.method public static z([B)Lcsj;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcsj;

    invoke-direct {v0}, Lcsj;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lcsj;

    return-object v0
.end method


# virtual methods
.method public final SF()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcsj;->bhM:J

    return-wide v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcsj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lcsj;->bhM:J

    iget v0, p0, Lcsj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcsj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcsj;->aeE:Ljava/lang/String;

    iget v0, p0, Lcsj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcsj;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcsj;->ajk:Ljava/lang/String;

    iget v0, p0, Lcsj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcsj;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 105
    iget v0, p0, Lcsj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    iget-wide v2, p0, Lcsj;->bhM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 108
    :cond_0
    iget v0, p0, Lcsj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x2

    iget-object v1, p0, Lcsj;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 111
    :cond_1
    iget v0, p0, Lcsj;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 112
    const/4 v0, 0x3

    iget-object v1, p0, Lcsj;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 114
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 115
    return-void
.end method

.method public final af(J)Lcsj;
    .locals 1

    .prologue
    .line 31
    iput-wide p1, p0, Lcsj;->bhM:J

    .line 32
    iget v0, p0, Lcsj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcsj;->aez:I

    .line 33
    return-object p0
.end method

.method public final getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcsj;->aeE:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcsj;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method public final iP(Ljava/lang/String;)Lcsj;
    .locals 1

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53
    :cond_0
    iput-object p1, p0, Lcsj;->aeE:Ljava/lang/String;

    .line 54
    iget v0, p0, Lcsj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcsj;->aez:I

    .line 55
    return-object p0
.end method

.method public final iQ(Ljava/lang/String;)Lcsj;
    .locals 1

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 73
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 75
    :cond_0
    iput-object p1, p0, Lcsj;->ajk:Ljava/lang/String;

    .line 76
    iget v0, p0, Lcsj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcsj;->aez:I

    .line 77
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 119
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 120
    iget v1, p0, Lcsj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 121
    const/4 v1, 0x1

    iget-wide v2, p0, Lcsj;->bhM:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_0
    iget v1, p0, Lcsj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 125
    const/4 v1, 0x2

    iget-object v2, p0, Lcsj;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_1
    iget v1, p0, Lcsj;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 129
    const/4 v1, 0x3

    iget-object v2, p0, Lcsj;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_2
    return v0
.end method
