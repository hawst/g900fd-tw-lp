.class public final Ljjf;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public clientExperimentIds:[I

.field public eoH:[Ljjg;

.field private eoI:J

.field private eoJ:[Lgns;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 294
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 295
    const/4 v0, 0x0

    iput v0, p0, Ljjf;->aez:I

    invoke-static {}, Ljjg;->bnQ()[Ljjg;

    move-result-object v0

    iput-object v0, p0, Ljjf;->eoH:[Ljjg;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljjf;->eoI:J

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljjf;->clientExperimentIds:[I

    invoke-static {}, Lgns;->aIa()[Lgns;

    move-result-object v0

    iput-object v0, p0, Ljjf;->eoJ:[Lgns;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjf;->eCz:I

    .line 296
    return-void
.end method

.method public static ap([B)Ljjf;
    .locals 1

    .prologue
    .line 483
    new-instance v0, Ljjf;

    invoke-direct {v0}, Ljjf;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljjf;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjf;->eoH:[Ljjg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljjg;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljjf;->eoH:[Ljjg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljjg;

    invoke-direct {v3}, Ljjg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljjf;->eoH:[Ljjg;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljjg;

    invoke-direct {v3}, Ljjg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjf;->eoH:[Ljjg;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljjf;->eoI:J

    iget v0, p0, Ljjf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjf;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjf;->clientExperimentIds:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljjf;->clientExperimentIds:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljjf;->clientExperimentIds:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljjf;->clientExperimentIds:[I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljjf;->clientExperimentIds:[I

    if-nez v2, :cond_9

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_8

    iget-object v4, p0, Ljjf;->clientExperimentIds:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_9
    iget-object v2, p0, Ljjf;->clientExperimentIds:[I

    array-length v2, v2

    goto :goto_6

    :cond_a
    iput-object v0, p0, Ljjf;->clientExperimentIds:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljjf;->eoJ:[Lgns;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Lgns;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljjf;->eoJ:[Lgns;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Lgns;

    invoke-direct {v3}, Lgns;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Ljjf;->eoJ:[Lgns;

    array-length v0, v0

    goto :goto_8

    :cond_d
    new-instance v3, Lgns;

    invoke-direct {v3}, Lgns;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljjf;->eoJ:[Lgns;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Ljjf;->eoH:[Ljjg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljjf;->eoH:[Ljjg;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 313
    :goto_0
    iget-object v2, p0, Ljjf;->eoH:[Ljjg;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 314
    iget-object v2, p0, Ljjf;->eoH:[Ljjg;

    aget-object v2, v2, v0

    .line 315
    if-eqz v2, :cond_0

    .line 316
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 313
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 320
    :cond_1
    iget v0, p0, Ljjf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 321
    const/4 v0, 0x2

    iget-wide v2, p0, Ljjf;->eoI:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 323
    :cond_2
    iget-object v0, p0, Ljjf;->clientExperimentIds:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljjf;->clientExperimentIds:[I

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 324
    :goto_1
    iget-object v2, p0, Ljjf;->clientExperimentIds:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 325
    const/4 v2, 0x3

    iget-object v3, p0, Ljjf;->clientExperimentIds:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 324
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 328
    :cond_3
    iget-object v0, p0, Ljjf;->eoJ:[Lgns;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljjf;->eoJ:[Lgns;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 329
    :goto_2
    iget-object v0, p0, Ljjf;->eoJ:[Lgns;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 330
    iget-object v0, p0, Ljjf;->eoJ:[Lgns;

    aget-object v0, v0, v1

    .line 331
    if-eqz v0, :cond_4

    .line 332
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 329
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 336
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 337
    return-void
.end method

.method public final bnP()J
    .locals 2

    .prologue
    .line 272
    iget-wide v0, p0, Ljjf;->eoI:J

    return-wide v0
.end method

.method public final dt(J)Ljjf;
    .locals 1

    .prologue
    .line 275
    iput-wide p1, p0, Ljjf;->eoI:J

    .line 276
    iget v0, p0, Ljjf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjf;->aez:I

    .line 277
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 341
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 342
    iget-object v2, p0, Ljjf;->eoH:[Ljjg;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljjf;->eoH:[Ljjg;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 343
    :goto_0
    iget-object v3, p0, Ljjf;->eoH:[Ljjg;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 344
    iget-object v3, p0, Ljjf;->eoH:[Ljjg;

    aget-object v3, v3, v0

    .line 345
    if-eqz v3, :cond_0

    .line 346
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 343
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 351
    :cond_2
    iget v2, p0, Ljjf;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 352
    const/4 v2, 0x2

    iget-wide v4, p0, Ljjf;->eoI:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 355
    :cond_3
    iget-object v2, p0, Ljjf;->clientExperimentIds:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljjf;->clientExperimentIds:[I

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 357
    :goto_1
    iget-object v4, p0, Ljjf;->clientExperimentIds:[I

    array-length v4, v4

    if-ge v2, v4, :cond_4

    .line 358
    iget-object v4, p0, Ljjf;->clientExperimentIds:[I

    aget v4, v4, v2

    .line 359
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 357
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 362
    :cond_4
    add-int/2addr v0, v3

    .line 363
    iget-object v2, p0, Ljjf;->clientExperimentIds:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 365
    :cond_5
    iget-object v2, p0, Ljjf;->eoJ:[Lgns;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljjf;->eoJ:[Lgns;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 366
    :goto_2
    iget-object v2, p0, Ljjf;->eoJ:[Lgns;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 367
    iget-object v2, p0, Ljjf;->eoJ:[Lgns;

    aget-object v2, v2, v1

    .line 368
    if-eqz v2, :cond_6

    .line 369
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 366
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 374
    :cond_7
    return v0
.end method
