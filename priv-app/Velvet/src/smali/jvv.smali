.class public final Ljvv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ail:I

.field private dIj:I

.field public eIm:Ljvw;

.field public eIn:Ljvu;

.field public eIo:Ljvw;

.field private eIp:Ljvw;

.field private eIq:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2594
    const/16 v0, 0xb

    const-class v1, Ljvv;

    const v2, 0xe2d687a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 2606
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2682
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2683
    iput v0, p0, Ljvv;->aez:I

    iput v0, p0, Ljvv;->dIj:I

    iput v0, p0, Ljvv;->ail:I

    iput-object v2, p0, Ljvv;->eIm:Ljvw;

    iput-object v2, p0, Ljvv;->eIn:Ljvu;

    iput-object v2, p0, Ljvv;->eIo:Ljvw;

    iput-object v2, p0, Ljvv;->eIp:Ljvw;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljvv;->eIq:J

    iput-object v2, p0, Ljvv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvv;->eCz:I

    .line 2684
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 2587
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljvv;->dIj:I

    iget v0, p0, Ljvv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljvv;->ail:I

    iget v0, p0, Ljvv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvv;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljvv;->eIm:Ljvw;

    if-nez v0, :cond_1

    new-instance v0, Ljvw;

    invoke-direct {v0}, Ljvw;-><init>()V

    iput-object v0, p0, Ljvv;->eIm:Ljvw;

    :cond_1
    iget-object v0, p0, Ljvv;->eIm:Ljvw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljvv;->eIn:Ljvu;

    if-nez v0, :cond_2

    new-instance v0, Ljvu;

    invoke-direct {v0}, Ljvu;-><init>()V

    iput-object v0, p0, Ljvv;->eIn:Ljvu;

    :cond_2
    iget-object v0, p0, Ljvv;->eIn:Ljvu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljvv;->eIo:Ljvw;

    if-nez v0, :cond_3

    new-instance v0, Ljvw;

    invoke-direct {v0}, Ljvw;-><init>()V

    iput-object v0, p0, Ljvv;->eIo:Ljvw;

    :cond_3
    iget-object v0, p0, Ljvv;->eIo:Ljvw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljvv;->eIq:J

    iget v0, p0, Ljvv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvv;->aez:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljvv;->eIp:Ljvw;

    if-nez v0, :cond_4

    new-instance v0, Ljvw;

    invoke-direct {v0}, Ljvw;-><init>()V

    iput-object v0, p0, Ljvv;->eIp:Ljvw;

    :cond_4
    iget-object v0, p0, Ljvv;->eIp:Ljvw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 2703
    iget v0, p0, Ljvv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2704
    const/4 v0, 0x1

    iget v1, p0, Ljvv;->dIj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2706
    :cond_0
    iget v0, p0, Ljvv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2707
    const/4 v0, 0x2

    iget v1, p0, Ljvv;->ail:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2709
    :cond_1
    iget-object v0, p0, Ljvv;->eIm:Ljvw;

    if-eqz v0, :cond_2

    .line 2710
    const/4 v0, 0x3

    iget-object v1, p0, Ljvv;->eIm:Ljvw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2712
    :cond_2
    iget-object v0, p0, Ljvv;->eIn:Ljvu;

    if-eqz v0, :cond_3

    .line 2713
    const/4 v0, 0x4

    iget-object v1, p0, Ljvv;->eIn:Ljvu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2715
    :cond_3
    iget-object v0, p0, Ljvv;->eIo:Ljvw;

    if-eqz v0, :cond_4

    .line 2716
    const/4 v0, 0x5

    iget-object v1, p0, Ljvv;->eIo:Ljvw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2718
    :cond_4
    iget v0, p0, Ljvv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 2719
    const/4 v0, 0x6

    iget-wide v2, p0, Ljvv;->eIq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2721
    :cond_5
    iget-object v0, p0, Ljvv;->eIp:Ljvw;

    if-eqz v0, :cond_6

    .line 2722
    const/4 v0, 0x7

    iget-object v1, p0, Ljvv;->eIp:Ljvw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2724
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2725
    return-void
.end method

.method public final bvb()Z
    .locals 1

    .prologue
    .line 2624
    iget v0, p0, Ljvv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getEventType()I
    .locals 1

    .prologue
    .line 2616
    iget v0, p0, Ljvv;->dIj:I

    return v0
.end method

.method public final getStatus()I
    .locals 1

    .prologue
    .line 2635
    iget v0, p0, Ljvv;->ail:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 2729
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2730
    iget v1, p0, Ljvv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2731
    const/4 v1, 0x1

    iget v2, p0, Ljvv;->dIj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2734
    :cond_0
    iget v1, p0, Ljvv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2735
    const/4 v1, 0x2

    iget v2, p0, Ljvv;->ail:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2738
    :cond_1
    iget-object v1, p0, Ljvv;->eIm:Ljvw;

    if-eqz v1, :cond_2

    .line 2739
    const/4 v1, 0x3

    iget-object v2, p0, Ljvv;->eIm:Ljvw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2742
    :cond_2
    iget-object v1, p0, Ljvv;->eIn:Ljvu;

    if-eqz v1, :cond_3

    .line 2743
    const/4 v1, 0x4

    iget-object v2, p0, Ljvv;->eIn:Ljvu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2746
    :cond_3
    iget-object v1, p0, Ljvv;->eIo:Ljvw;

    if-eqz v1, :cond_4

    .line 2747
    const/4 v1, 0x5

    iget-object v2, p0, Ljvv;->eIo:Ljvw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2750
    :cond_4
    iget v1, p0, Ljvv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 2751
    const/4 v1, 0x6

    iget-wide v2, p0, Ljvv;->eIq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2754
    :cond_5
    iget-object v1, p0, Ljvv;->eIp:Ljvw;

    if-eqz v1, :cond_6

    .line 2755
    const/4 v1, 0x7

    iget-object v2, p0, Ljvv;->eIp:Ljvw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2758
    :cond_6
    return v0
.end method

.method public final sC(I)Ljvv;
    .locals 1

    .prologue
    .line 2619
    iput p1, p0, Ljvv;->dIj:I

    .line 2620
    iget v0, p0, Ljvv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvv;->aez:I

    .line 2621
    return-object p0
.end method

.method public final sD(I)Ljvv;
    .locals 1

    .prologue
    .line 2638
    iput p1, p0, Ljvv;->ail:I

    .line 2639
    iget v0, p0, Ljvv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvv;->aez:I

    .line 2640
    return-object p0
.end method
