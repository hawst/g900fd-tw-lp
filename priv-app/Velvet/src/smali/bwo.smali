.class public final Lbwo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mClock:Lemp;

.field public final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lhym;Lemp;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lbwo;->mSettings:Lhym;

    .line 58
    iput-object p2, p0, Lbwo;->mClock:Lemp;

    .line 59
    return-void
.end method


# virtual methods
.method public final fJ(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 65
    iget-object v0, p0, Lbwo;->mSettings:Lhym;

    invoke-virtual {v0, p1}, Lhym;->oE(Ljava/lang/String;)I

    move-result v1

    .line 66
    iget-object v0, p0, Lbwo;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lbwo;->mSettings:Lhym;

    invoke-virtual {v0, p1}, Lhym;->oF(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    div-long/2addr v2, v4

    .line 68
    const/4 v0, 0x7

    if-le v1, v0, :cond_0

    const-wide/16 v4, 0x5

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 70
    :goto_0
    if-eqz v0, :cond_1

    .line 71
    add-int/lit8 v1, v1, 0x1

    .line 72
    iget-object v2, p0, Lbwo;->mSettings:Lhym;

    invoke-virtual {v2, p1, v1}, Lhym;->M(Ljava/lang/String;I)V

    .line 73
    iget-object v1, p0, Lbwo;->mSettings:Lhym;

    iget-object v2, p0, Lbwo;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, p1, v2, v3}, Lhym;->t(Ljava/lang/String;J)V

    .line 79
    :cond_1
    return v0

    .line 68
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final fK(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lbwo;->mSettings:Lhym;

    invoke-virtual {v0, p1}, Lhym;->oE(Ljava/lang/String;)I

    move-result v0

    .line 87
    iget-object v1, p0, Lbwo;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 88
    const/4 v1, 0x7

    if-gt v0, v1, :cond_0

    .line 89
    add-int/lit8 v0, v0, 0x2

    .line 90
    iget-object v1, p0, Lbwo;->mSettings:Lhym;

    invoke-virtual {v1, p1, v0}, Lhym;->M(Ljava/lang/String;I)V

    .line 92
    :cond_0
    iget-object v0, p0, Lbwo;->mSettings:Lhym;

    invoke-virtual {v0, p1, v2, v3}, Lhym;->t(Ljava/lang/String;J)V

    .line 97
    return-void
.end method
