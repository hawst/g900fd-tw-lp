.class public abstract Lcgl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aD:Ljava/lang/String;

.field private final aWc:J

.field public final aWd:Lbgo;

.field final aWe:Ljava/lang/Object;

.field aWf:Ljava/util/concurrent/CountDownLatch;

.field private final aWg:Lcgt;

.field private final aWh:Leri;

.field private final aWi:Lesk;

.field private final aWj:Lesk;

.field private final aWk:Ljava/util/List;

.field aWl:I

.field aWm:Ljava/util/concurrent/CountDownLatch;

.field private final mTaskRunner:Lerk;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lerk;J)V
    .locals 4

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcgl;->aWe:Ljava/lang/Object;

    .line 114
    new-instance v0, Lcgt;

    invoke-direct {v0, p0}, Lcgt;-><init>(Lcgl;)V

    iput-object v0, p0, Lcgl;->aWg:Lcgt;

    .line 116
    new-instance v0, Lcgm;

    const-string v1, "Connect task"

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, p0, v1, v2}, Lcgm;-><init>(Lcgl;Ljava/lang/String;[I)V

    iput-object v0, p0, Lcgl;->aWh:Leri;

    .line 128
    new-instance v0, Lcgn;

    const-string v1, "Idle task"

    invoke-direct {v0, p0, v1}, Lcgn;-><init>(Lcgl;Ljava/lang/String;)V

    iput-object v0, p0, Lcgl;->aWi:Lesk;

    .line 136
    new-instance v0, Lcgo;

    const-string v1, "Connection timeout task"

    invoke-direct {v0, p0, v1}, Lcgo;-><init>(Lcgl;Ljava/lang/String;)V

    iput-object v0, p0, Lcgl;->aWj:Lesk;

    .line 145
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcgl;->aWk:Ljava/util/List;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcgl;->aWl:I

    .line 162
    iput-object p1, p0, Lcgl;->aD:Ljava/lang/String;

    .line 163
    iput-object p3, p0, Lcgl;->mTaskRunner:Lerk;

    .line 164
    iput-wide p4, p0, Lcgl;->aWc:J

    .line 165
    iget-object v0, p0, Lcgl;->aWg:Lcgt;

    iget-object v1, p0, Lcgl;->aWg:Lcgt;

    invoke-virtual {p0, p2, v0, v1}, Lcgl;->a(Landroid/content/Context;Lbgp;Lbgq;)Lbgo;

    move-result-object v0

    iput-object v0, p0, Lcgl;->aWd:Lbgo;

    .line 166
    return-void

    .line 116
    nop

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data
.end method

.method private Fb()Ljava/util/concurrent/CountDownLatch;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 271
    iget-object v1, p0, Lcgl;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 272
    :try_start_0
    iget v0, p0, Lcgl;->aWl:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcgl;->aWl:I

    if-ne v0, v3, :cond_1

    .line 274
    :cond_0
    iget-object v0, p0, Lcgl;->aWm:Ljava/util/concurrent/CountDownLatch;

    monitor-exit v1

    .line 285
    :goto_0
    return-object v0

    .line 277
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcgl;->aWl:I

    .line 278
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcgl;->aWm:Ljava/util/concurrent/CountDownLatch;

    .line 279
    iget-object v0, p0, Lcgl;->mTaskRunner:Lerk;

    iget-object v2, p0, Lcgl;->aWj:Lesk;

    const-wide/16 v4, 0x2710

    invoke-interface {v0, v2, v4, v5}, Lerk;->a(Lesk;J)V

    .line 283
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcgl;->aWf:Ljava/util/concurrent/CountDownLatch;

    .line 284
    iget-object v0, p0, Lcgl;->mTaskRunner:Lerk;

    iget-object v2, p0, Lcgl;->aWh:Leri;

    invoke-interface {v0, v2}, Lerk;->a(Leri;)V

    .line 285
    iget-object v0, p0, Lcgl;->aWm:Ljava/util/concurrent/CountDownLatch;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcgq;)V
    .locals 2

    .prologue
    .line 232
    iget-object v1, p0, Lcgl;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 233
    :try_start_0
    invoke-virtual {p0}, Lcgl;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcgl;->Ff()V

    .line 236
    iget-object v0, p0, Lcgl;->mTaskRunner:Lerk;

    invoke-interface {v0, p1}, Lerk;->a(Leri;)V

    .line 242
    :goto_0
    monitor-exit v1

    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcgl;->aWk:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    invoke-direct {p0}, Lcgl;->Fb()Ljava/util/concurrent/CountDownLatch;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgq;
    .locals 2

    .prologue
    .line 533
    new-instance v0, Lcgq;

    iget-object v1, p0, Lcgl;->aD:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p2}, Lcgq;-><init>(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method final Fc()V
    .locals 4

    .prologue
    .line 326
    iget-object v1, p0, Lcgl;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 327
    :try_start_0
    invoke-virtual {p0}, Lcgl;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcgl;->aWk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgq;

    .line 330
    iget-object v3, p0, Lcgl;->mTaskRunner:Lerk;

    invoke-interface {v3, v0}, Lerk;->a(Leri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 333
    :cond_0
    :try_start_1
    new-instance v2, Landroid/os/RemoteException;

    const-string v0, "Connection failed"

    invoke-direct {v2, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcgl;->aWk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgq;

    .line 336
    invoke-virtual {v0, v2}, Lcgq;->setException(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 339
    :cond_1
    iget-object v0, p0, Lcgl;->aWk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 340
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method final Fd()V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcgl;->mTaskRunner:Lerk;

    iget-object v1, p0, Lcgl;->aWj:Lesk;

    invoke-interface {v0, v1}, Lerk;->b(Lesk;)V

    .line 379
    return-void
.end method

.method public final Fe()V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcgl;->mTaskRunner:Lerk;

    iget-object v1, p0, Lcgl;->aWi:Lesk;

    invoke-interface {v0, v1}, Lerk;->b(Lesk;)V

    .line 387
    return-void
.end method

.method final Ff()V
    .locals 4

    .prologue
    .line 395
    invoke-virtual {p0}, Lcgl;->Fe()V

    .line 396
    iget-object v0, p0, Lcgl;->mTaskRunner:Lerk;

    iget-object v1, p0, Lcgl;->aWi:Lesk;

    iget-wide v2, p0, Lcgl;->aWc:J

    invoke-interface {v0, v1, v2, v3}, Lerk;->a(Lesk;J)V

    .line 397
    return-void
.end method

.method protected abstract a(Landroid/content/Context;Lbgp;Lbgq;)Lbgo;
.end method

.method public final a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Lcgl;->c(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgq;

    move-result-object v0

    .line 203
    invoke-direct {p0, v0}, Lcgl;->a(Lcgq;)V

    .line 204
    invoke-static {v0}, Lcgs;->b(Livq;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 253
    invoke-static {}, Lenu;->auQ()V

    .line 255
    invoke-direct {p0}, Lcgl;->Fb()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 256
    iget-object v1, p0, Lcgl;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 257
    :try_start_0
    invoke-virtual {p0}, Lcgl;->Ff()V

    .line 258
    iget v0, p0, Lcgl;->aWl:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 259
    new-instance v0, Lcgr;

    const-string v2, "Failed to connect"

    invoke-direct {v0, v2}, Lcgr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 262
    :cond_0
    :try_start_1
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    return-object v0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    new-instance v2, Lcgr;

    invoke-direct {v2, v0}, Lcgr;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final b(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;
    .locals 2

    .prologue
    .line 213
    invoke-direct {p0, p1, p2}, Lcgl;->c(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgq;

    move-result-object v0

    .line 215
    invoke-direct {p0, v0}, Lcgl;->a(Lcgq;)V

    .line 216
    new-instance v1, Lcgp;

    invoke-direct {v1, p0}, Lcgp;-><init>(Lcgl;)V

    invoke-static {v0, v1}, Livg;->a(Livq;Liuz;)Livq;

    move-result-object v0

    .line 224
    invoke-static {v0}, Lcgs;->b(Livq;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method final disconnect()V
    .locals 2

    .prologue
    .line 294
    invoke-static {}, Lenu;->auR()V

    .line 296
    iget-object v1, p0, Lcgl;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 297
    :try_start_0
    iget v0, p0, Lcgl;->aWl:I

    if-nez v0, :cond_0

    .line 298
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    :goto_0
    return-void

    .line 305
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcgl;->aWf:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcgl;->aWd:Lbgo;

    invoke-interface {v0}, Lbgo;->disconnect()V

    .line 311
    const/4 v0, 0x0

    iput v0, p0, Lcgl;->aWl:I

    .line 312
    iget-object v0, p0, Lcgl;->aWm:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 314
    invoke-virtual {p0}, Lcgl;->Fe()V

    .line 315
    invoke-virtual {p0}, Lcgl;->Fd()V

    .line 316
    invoke-virtual {p0}, Lcgl;->Fc()V

    .line 317
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final isConnected()Z
    .locals 3

    .prologue
    .line 186
    iget-object v1, p0, Lcgl;->aWe:Ljava/lang/Object;

    monitor-enter v1

    .line 187
    :try_start_0
    iget v0, p0, Lcgl;->aWl:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
