.class public final Ljbe;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dXS:[Ljbe;


# instance fields
.field private aez:I

.field private dXT:Ljava/lang/String;

.field private dXU:Ljava/lang/String;

.field private dXV:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63969
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 63970
    iput v1, p0, Ljbe;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbe;->dXT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbe;->dXU:Ljava/lang/String;

    iput-boolean v1, p0, Ljbe;->dXV:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljbe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbe;->eCz:I

    .line 63971
    return-void
.end method

.method public static beL()[Ljbe;
    .locals 2

    .prologue
    .line 63893
    sget-object v0, Ljbe;->dXS:[Ljbe;

    if-nez v0, :cond_1

    .line 63894
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 63896
    :try_start_0
    sget-object v0, Ljbe;->dXS:[Ljbe;

    if-nez v0, :cond_0

    .line 63897
    const/4 v0, 0x0

    new-array v0, v0, [Ljbe;

    sput-object v0, Ljbe;->dXS:[Ljbe;

    .line 63899
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63901
    :cond_1
    sget-object v0, Ljbe;->dXS:[Ljbe;

    return-object v0

    .line 63899
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 63887
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbe;->dXT:Ljava/lang/String;

    iget v0, p0, Ljbe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbe;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbe;->dXU:Ljava/lang/String;

    iget v0, p0, Ljbe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbe;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbe;->dXV:Z

    iget v0, p0, Ljbe;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbe;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 63986
    iget v0, p0, Ljbe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 63987
    const/4 v0, 0x1

    iget-object v1, p0, Ljbe;->dXT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 63989
    :cond_0
    iget v0, p0, Ljbe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 63990
    const/4 v0, 0x2

    iget-object v1, p0, Ljbe;->dXU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 63992
    :cond_1
    iget v0, p0, Ljbe;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 63993
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljbe;->dXV:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 63995
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 63996
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 64000
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 64001
    iget v1, p0, Ljbe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 64002
    const/4 v1, 0x1

    iget-object v2, p0, Ljbe;->dXT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64005
    :cond_0
    iget v1, p0, Ljbe;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 64006
    const/4 v1, 0x2

    iget-object v2, p0, Ljbe;->dXU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64009
    :cond_1
    iget v1, p0, Ljbe;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 64010
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljbe;->dXV:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64013
    :cond_2
    return v0
.end method
