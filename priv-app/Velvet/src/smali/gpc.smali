.class public Lgpc;
.super Ldke;
.source "PG"

# interfaces
.implements Leps;


# instance fields
.field private bmT:Z

.field private final cKU:Ljava/lang/String;

.field private final cRZ:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cSa:Ljava/util/Set;

.field private cSb:Ljli;

.field private final mConfig:Lcjs;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lcjs;Lcke;Lesm;Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 1
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    const-string v0, "Velvet.ActionDiscoveryData"

    invoke-direct {p0, v0, p3, p4}, Ldke;-><init>(Ljava/lang/String;Lesm;Ljava/util/concurrent/Executor;)V

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgpc;->cSa:Ljava/util/Set;

    .line 53
    iput-object p1, p0, Lgpc;->mConfig:Lcjs;

    .line 54
    iput-object p2, p0, Lgpc;->mSettings:Lcke;

    .line 55
    iput-object p5, p0, Lgpc;->cKU:Ljava/lang/String;

    .line 56
    invoke-direct {p0, p5}, Lgpc;->nr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpc;->cRZ:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private nr(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 110
    if-nez p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-object v1

    .line 115
    :cond_1
    const-string v0, "-"

    const/4 v3, 0x2

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 116
    aget-object v4, v0, v2

    .line 119
    array-length v3, v0

    if-le v3, v6, :cond_4

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v0, v0, v6

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    :goto_1
    iget-object v3, p0, Lgpc;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->Mh()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_0

    aget-object v2, v5, v3

    .line 124
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    move-object v1, v2

    .line 125
    goto :goto_0

    .line 123
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected final declared-synchronized E([B)V
    .locals 3
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 88
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 89
    :try_start_0
    invoke-static {p1}, Ljli;->at([B)Ljli;

    move-result-object v0

    iput-object v0, p0, Lgpc;->cSb:Ljli;

    .line 96
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpc;->bmT:Z

    .line 97
    iget-object v0, p0, Lgpc;->cSa:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemy;

    .line 98
    iget-object v2, p0, Lgpc;->cSb:Ljli;

    invoke-interface {v0, v2}, Lemy;->aj(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 92
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lgpc;->cSb:Ljli;

    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, p0, Lgpc;->cSa:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized aIX()Ljli;
    .locals 1

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgpc;->bmT:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 174
    iget-object v0, p0, Lgpc;->cSb:Ljli;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final acA()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method protected final acB()[B
    .locals 2

    .prologue
    .line 196
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Always use cache."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final acC()[B
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lgpc;->cRZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpc;->mSettings:Lcke;

    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcke;->gY(Ljava/lang/String;)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final acD()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lgpc;->cRZ:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpc;->mSettings:Lcke;

    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcke;->gX(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final acE()Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 137
    iget-object v0, p0, Lgpc;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mf()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lgpc;->cRZ:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 139
    :cond_0
    return-object v0
.end method

.method public final declared-synchronized auB()Z
    .locals 1

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgpc;->bmT:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final synthetic auC()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lgpc;->aIX()Ljli;

    move-result-object v0

    return-object v0
.end method

.method protected final b([BLjava/lang/String;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lgpc;->mSettings:Lcke;

    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcke;->d(Ljava/lang/String;[B)V

    .line 82
    iget-object v0, p0, Lgpc;->mSettings:Lcke;

    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcke;->K(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public final declared-synchronized e(Lemy;)V
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgpc;->bmT:Z

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lgpc;->cRZ:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpc;->mSettings:Lcke;

    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcke;->gZ(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lgpc;->acG()V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lgpc;->bmT:Z

    .line 182
    :cond_0
    iget-boolean v0, p0, Lgpc;->bmT:Z

    if-eqz v0, :cond_3

    .line 183
    iget-object v0, p0, Lgpc;->cSb:Ljli;

    invoke-interface {p1, v0}, Lemy;->aj(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :goto_2
    monitor-exit p0

    return-void

    .line 180
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lgpc;->acF()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 185
    :cond_3
    :try_start_2
    iget-object v0, p0, Lgpc;->cSa:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final isSupported(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63
    iget-object v0, p0, Lgpc;->cKU:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lgpc;->nr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgpc;->cRZ:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
