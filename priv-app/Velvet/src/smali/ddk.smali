.class public Lddk;
.super Lddj;
.source "PG"


# static fields
.field private static final btS:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final btR:Ljava/lang/Object;

.field private btT:J

.field private btU:Ljava/util/Map;

.field private btV:Ljava/lang/String;

.field private btW:Lcom/google/android/shared/search/Query;

.field private btX:Lcom/google/android/shared/search/Query;

.field private btY:Z

.field private btZ:Lcom/google/android/shared/search/Query;

.field private final mGsaConfigFlags:Lchk;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lddk;->btS:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Ldda;Lchk;)V
    .locals 2

    .prologue
    .line 101
    const/16 v0, 0x800

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lddk;->btR:Ljava/lang/Object;

    .line 52
    sget-object v0, Lddk;->btS:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iput-wide v0, p0, Lddk;->btT:J

    .line 71
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    .line 83
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    .line 95
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lddk;->btZ:Lcom/google/android/shared/search/Query;

    .line 102
    iput-object p2, p0, Lddk;->mGsaConfigFlags:Lchk;

    .line 103
    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 104
    return-void
.end method

.method private aaQ()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 248
    iget-object v3, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v3

    .line 250
    :try_start_0
    iget-object v2, p0, Lddk;->btV:Ljava/lang/String;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    or-int/lit8 v2, v2, 0x0

    .line 251
    const/4 v4, 0x0

    iput-object v4, p0, Lddk;->btV:Ljava/lang/String;

    .line 252
    iget-object v4, p0, Lddk;->btU:Ljava/util/Map;

    if-eqz v4, :cond_2

    :goto_1
    or-int/2addr v0, v2

    .line 253
    const/4 v1, 0x0

    iput-object v1, p0, Lddk;->btU:Ljava/util/Map;

    .line 254
    if-eqz v0, :cond_0

    .line 255
    sget-object v1, Lddk;->btS:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v4

    iput-wide v4, p0, Lddk;->btT:J

    .line 257
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    return v0

    :cond_1
    move v2, v1

    .line 250
    goto :goto_0

    :cond_2
    move v0, v1

    .line 252
    goto :goto_1

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private jy(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 120
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    const/4 p1, 0x0

    .line 123
    :cond_0
    iget-object v1, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v1

    .line 124
    :try_start_0
    iget-object v2, p0, Lddk;->btV:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 125
    iput-object p1, p0, Lddk;->btV:Ljava/lang/String;

    .line 126
    const/4 v0, 0x1

    .line 128
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    return v0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private jz(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 139
    .line 140
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 141
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 142
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 143
    invoke-virtual {v3}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 144
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 145
    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 149
    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 154
    :goto_1
    const/4 v1, 0x0

    .line 155
    iget-object v2, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v2

    .line 156
    :try_start_0
    iget-object v3, p0, Lddk;->btU:Ljava/util/Map;

    invoke-static {v3, v0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 157
    iput-object v0, p0, Lddk;->btU:Ljava/util/Map;

    .line 158
    const/4 v0, 0x1

    .line 160
    :goto_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    return v0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected final a(Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lddk;->btZ:Lcom/google/android/shared/search/Query;

    .line 200
    return-void
.end method

.method protected final a(Lddb;)V
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 234
    iget-object v1, p0, Lddk;->btZ:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lddk;->btZ:Lcom/google/android/shared/search/Query;

    .line 239
    :cond_0
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yj()Z

    move-result v0

    if-nez v0, :cond_1

    .line 240
    invoke-direct {p0}, Lddk;->aaQ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {p0}, Lddk;->notifyChanged()V

    .line 245
    :cond_1
    return-void
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    .line 418
    const-string v0, "WebAppState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 419
    iget-object v2, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v2

    .line 420
    :try_start_0
    const-string v0, "mApiFunction"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lddk;->btV:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 421
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v3

    .line 422
    iget-object v0, p0, Lddk;->btU:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 423
    const-string v0, "mRequestParams"

    invoke-virtual {v3, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lddk;->btU:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 425
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 429
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 428
    :cond_0
    :try_start_1
    const-string v0, "mVersion"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v4, p0, Lddk;->btT:J

    invoke-virtual {v0, v4, v5}, Letn;->be(J)V

    .line 429
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final aA(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 316
    invoke-static {}, Lenu;->auR()V

    .line 317
    iput-object p1, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    .line 318
    iput-object p1, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddk;->btY:Z

    .line 321
    return-void
.end method

.method public final aB(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 412
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yk()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lddk;->btZ:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aaK()J
    .locals 4

    .prologue
    .line 107
    iget-object v1, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    iget-wide v2, p0, Lddk;->btT:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aaL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    iget-object v0, p0, Lddk;->btV:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aaM()Z
    .locals 2

    .prologue
    .line 190
    iget-object v1, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v1

    .line 191
    :try_start_0
    iget-object v0, p0, Lddk;->btV:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lddk;->btU:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aaN()Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xf()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 205
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    :cond_0
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aaO()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 214
    iget-object v1, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 215
    invoke-virtual {p0, v1}, Lddk;->aB(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lddk;->aaT()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final aaP()V
    .locals 1

    .prologue
    .line 225
    invoke-direct {p0}, Lddk;->aaQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {p0}, Lddk;->notifyChanged()V

    .line 228
    :cond_0
    return-void
.end method

.method public final aaR()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 288
    invoke-static {}, Lenu;->auR()V

    .line 289
    iget-object v0, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final aaS()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 311
    invoke-static {}, Lenu;->auR()V

    .line 312
    iget-object v0, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final aaT()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 324
    iget-object v0, p0, Lddk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HY()Z

    move-result v0

    if-nez v0, :cond_1

    .line 389
    :cond_0
    :goto_0
    return v1

    .line 328
    :cond_1
    invoke-virtual {p0}, Lddk;->aaM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 333
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v4, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v4}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lddk;->btY:Z

    if-nez v0, :cond_2

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_2
    move v0, v2

    .line 344
    :goto_1
    if-nez v0, :cond_0

    .line 349
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v2

    .line 352
    :goto_2
    if-nez v0, :cond_0

    .line 356
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqc()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apU()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apU()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_3
    move v0, v2

    .line 358
    :goto_3
    iget-object v4, p0, Lddk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v4}, Lchk;->Ia()Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v0, :cond_0

    .line 362
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    if-eqz v0, :cond_d

    move v0, v2

    .line 364
    :goto_4
    iget-object v4, p0, Lddk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v4}, Lchk;->Ib()Z

    move-result v4

    if-nez v4, :cond_5

    if-nez v0, :cond_0

    .line 370
    :cond_5
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v2

    .line 372
    :goto_5
    iget-object v4, p0, Lddk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v4}, Lchk;->HZ()Z

    move-result v4

    if-nez v4, :cond_6

    if-nez v0, :cond_0

    .line 377
    :cond_6
    iget-object v0, p0, Lddk;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Ic()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0, v3}, Lddk;->aB(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    :cond_7
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xf()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 386
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_8
    move v0, v2

    .line 389
    :goto_6
    if-nez v0, :cond_9

    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XJ()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_9
    move v1, v2

    goto/16 :goto_0

    :cond_a
    move v0, v1

    .line 342
    goto/16 :goto_1

    :cond_b
    move v0, v1

    .line 349
    goto/16 :goto_2

    :cond_c
    move v0, v1

    .line 356
    goto :goto_3

    :cond_d
    move v0, v1

    .line 362
    goto :goto_4

    :cond_e
    move v0, v1

    .line 370
    goto :goto_5

    :cond_f
    move v0, v1

    .line 386
    goto :goto_6
.end method

.method public final aaU()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 394
    iget-object v0, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    .line 395
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 398
    :goto_0
    iget-object v3, p0, Lddk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xf()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 399
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    move v3, v1

    .line 401
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqj()Z

    move-result v5

    .line 402
    if-eqz v5, :cond_5

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v4

    invoke-virtual {v4}, Lehm;->asN()Z

    move-result v4

    if-eqz v4, :cond_5

    move v4, v1

    .line 404
    :goto_2
    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    if-eqz v5, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 395
    goto :goto_0

    :cond_4
    move v3, v2

    .line 399
    goto :goto_1

    :cond_5
    move v4, v2

    .line 402
    goto :goto_2
.end method

.method public final al(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 175
    iget-object v1, p0, Lddk;->btR:Ljava/lang/Object;

    monitor-enter v1

    .line 176
    :try_start_0
    sget-object v0, Lddk;->btS:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    iput-wide v2, p0, Lddk;->btT:J

    .line 177
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    invoke-direct {p0, p1}, Lddk;->jy(Ljava/lang/String;)Z

    .line 179
    invoke-direct {p0, p2}, Lddk;->jz(Ljava/lang/String;)Z

    .line 180
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddk;->btY:Z

    .line 182
    invoke-virtual {p0}, Lddk;->notifyChanged()V

    .line 183
    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final av(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 262
    invoke-static {}, Lenu;->auR()V

    .line 263
    iget-object v0, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    iput-object p1, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    .line 265
    const/4 v0, 0x1

    .line 268
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aw(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 273
    invoke-static {}, Lenu;->auR()V

    .line 274
    iget-object v0, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iput-object p1, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    .line 276
    :cond_0
    return-void
.end method

.method public final ax(Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 282
    invoke-static {}, Lenu;->auR()V

    .line 283
    iput-object p1, p0, Lddk;->btW:Lcom/google/android/shared/search/Query;

    .line 284
    return-void
.end method

.method public final ay(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 293
    invoke-static {}, Lenu;->auR()V

    .line 294
    iget-object v1, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    iput-object p1, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    .line 296
    iput-boolean v0, p0, Lddk;->btY:Z

    .line 297
    const/4 v0, 0x1

    .line 299
    :cond_0
    return v0
.end method

.method public final az(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 303
    invoke-static {}, Lenu;->auR()V

    .line 304
    iget-object v0, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iput-object p1, p0, Lddk;->btX:Lcom/google/android/shared/search/Query;

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lddk;->btY:Z

    .line 308
    :cond_0
    return-void
.end method
