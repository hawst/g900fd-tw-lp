.class final Lfik;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfcg;


# instance fields
.field private synthetic csO:Lfig;


# direct methods
.method constructor <init>(Lfig;)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lfik;->csO:Lfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Laqa;)Laqa;
    .locals 7

    .prologue
    .line 617
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 618
    iget-object v2, p1, Laqa;->ami:[Lapz;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 619
    new-instance v5, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v6, v4, Lapz;->ame:Ljde;

    invoke-direct {v5, v6}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 627
    :cond_0
    iget-object v0, p0, Lfik;->csO:Lfig;

    iget-object v2, v0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 629
    :try_start_0
    iget-object v0, p0, Lfik;->csO:Lfig;

    iget-object v0, v0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 630
    const/4 p1, 0x0

    monitor-exit v2

    .line 641
    :goto_1
    return-object p1

    .line 632
    :cond_1
    iget-object v0, p0, Lfik;->csO:Lfig;

    iget-object v0, v0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 633
    iget-object v0, p0, Lfik;->csO:Lfig;

    iget-object v0, v0, Lfig;->csL:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 634
    iget-object v0, p0, Lfik;->csO:Lfig;

    iget-object v0, v0, Lfig;->csL:Ljava/util/Map;

    iget-object v3, p0, Lfik;->csO:Lfig;

    iget-object v3, v3, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 636
    :cond_2
    iget-object v0, p0, Lfik;->csO:Lfig;

    iget-object v0, v0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 637
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 639
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lapz;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapz;

    iput-object v0, p1, Laqa;->ami:[Lapz;

    goto :goto_1

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final synthetic aS(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 614
    check-cast p1, Laqa;

    invoke-direct {p0, p1}, Lfik;->b(Laqa;)Laqa;

    move-result-object v0

    return-object v0
.end method
