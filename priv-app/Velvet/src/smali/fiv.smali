.class public final Lfiv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final ctm:I

.field public static final ctn:I

.field private static final cto:Ljava/lang/Object;


# instance fields
.field private final ctp:Lfco;

.field private final mAlarmHelper:Ldjx;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lfiv;->ctm:I

    .line 63
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lfiv;->ctn:I

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lfiv;->cto:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ldjx;Lcjs;Lguh;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lfiv;->mAlarmHelper:Ldjx;

    .line 79
    new-instance v0, Lfco;

    const/4 v1, 0x1

    invoke-direct {v0, p3, p2, v1}, Lfco;-><init>(Lguh;Lcjs;Z)V

    iput-object v0, p0, Lfiv;->ctp:Lfco;

    .line 80
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/app/PendingIntent;Lfiu;)V
    .locals 12

    .prologue
    .line 130
    iget-object v0, p3, Lfiu;->ctk:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 131
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 132
    iget-object v0, p3, Lfiu;->ctk:Ljava/util/List;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    const-string v2, "limit is negative"

    invoke-static {v1, v2}, Lifv;->c(ZLjava/lang/Object;)V

    new-instance v1, Likn;

    const/16 v2, 0xa

    invoke-direct {v1, v0, v2}, Likn;-><init>(Ljava/lang/Iterable;I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfit;

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "entry_trigger_conditions."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lfit;->ctd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 135
    new-instance v1, Lbri;

    invoke-direct {v1}, Lbri;-><init>()V

    iget-object v2, v0, Lfit;->cte:Ljbp;

    invoke-virtual {v2}, Ljbp;->mR()D

    move-result-wide v2

    iget-object v4, v0, Lfit;->cte:Ljbp;

    invoke-virtual {v4}, Ljbp;->mS()D

    move-result-wide v4

    iget-object v6, v0, Lfit;->cte:Ljbp;

    invoke-virtual {v6}, Ljbp;->bfc()D

    move-result-wide v10

    double-to-float v6, v10

    invoke-virtual/range {v1 .. v6}, Lbri;->a(DDF)Lbri;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lbri;->L(J)Lbri;

    move-result-object v2

    iput-object v9, v2, Lbri;->aFS:Ljava/lang/String;

    .line 141
    iget-boolean v1, v0, Lfit;->ctg:Z

    if-eqz v1, :cond_0

    sget v1, Lfiv;->ctn:I

    .line 144
    :goto_1
    iget v0, v0, Lfit;->ctf:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 145
    const/4 v0, 0x4

    iput v0, v2, Lbri;->aFY:I

    iput v1, v2, Lbri;->aFZ:I

    iput v1, v2, Lbri;->aGa:I

    .line 154
    :goto_2
    invoke-virtual {v2}, Lbri;->Ae()Lbrh;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 141
    :cond_0
    sget v1, Lfiv;->ctm:I

    goto :goto_1

    .line 150
    :cond_1
    const/4 v0, 0x2

    iput v0, v2, Lbri;->aFY:I

    iput v1, v2, Lbri;->aFZ:I

    goto :goto_2

    .line 161
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 162
    iget-object v0, p0, Lfiv;->ctp:Lfco;

    invoke-virtual {v0, v7, p2}, Lfco;->a(Ljava/util/List;Landroid/app/PendingIntent;)V

    .line 164
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Lfiu;Landroid/app/PendingIntent;)V
    .locals 14

    .prologue
    .line 100
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {p1, v0, v1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->b(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    move-result-object v9

    .line 103
    sget-object v10, Lfiv;->cto:Ljava/lang/Object;

    monitor-enter v10

    .line 104
    :try_start_0
    iget-object v2, p0, Lfiv;->ctp:Lfco;

    invoke-virtual {v2, v9}, Lfco;->e(Landroid/app/PendingIntent;)V

    .line 105
    move-object/from16 v0, p3

    iget-object v2, v0, Lfiu;->cti:Ljava/lang/Long;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {p1, v0, v1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;

    move-result-object v3

    iget-object v4, p0, Lfiv;->mAlarmHelper:Ldjx;

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v12, 0x3e8

    mul-long/2addr v6, v12

    invoke-virtual {v4, v5, v6, v7, v3}, Ldjx;->a(IJLandroid/app/PendingIntent;)V

    .line 106
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "entry_trigger_conditions."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p3

    iget-object v2, v0, Lfiu;->ctj:Ljava/lang/Float;

    if-eqz v2, :cond_1

    move-object/from16 v0, p3

    iget-object v6, v0, Lfiu;->mLocation:Landroid/location/Location;

    new-instance v3, Lbri;

    invoke-direct {v3}, Lbri;-><init>()V

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual/range {v3 .. v8}, Lbri;->a(DDF)Lbri;

    move-result-object v2

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v4, v5}, Lbri;->L(J)Lbri;

    move-result-object v3

    move-object/from16 v0, p3

    iget-boolean v2, v0, Lfiu;->ctg:Z

    if-eqz v2, :cond_2

    sget v2, Lfiv;->ctn:I

    :goto_0
    iput v2, v3, Lbri;->aFZ:I

    const/4 v2, 0x2

    iput v2, v3, Lbri;->aFY:I

    iput-object v11, v3, Lbri;->aFS:Ljava/lang/String;

    invoke-virtual {v3}, Lbri;->Ae()Lbrh;

    move-result-object v2

    iget-object v3, p0, Lfiv;->ctp:Lfco;

    invoke-static {v2}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v2

    invoke-virtual {v3, v2, v9}, Lfco;->a(Ljava/util/List;Landroid/app/PendingIntent;)V

    .line 107
    :cond_1
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {p0, v0, v9, v1}, Lfiv;->a(Ljava/lang/String;Landroid/app/PendingIntent;Lfiu;)V

    .line 108
    monitor-exit v10

    return-void

    .line 106
    :cond_2
    sget v2, Lfiv;->ctm:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v2

    monitor-exit v10

    throw v2
.end method
