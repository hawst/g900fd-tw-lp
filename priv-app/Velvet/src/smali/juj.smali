.class public final Ljuj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFj:[Ljuj;


# instance fields
.field private aez:I

.field private dPv:[B

.field private eFk:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 427
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 428
    const/4 v0, 0x0

    iput v0, p0, Ljuj;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljuj;->name:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuj;->eFk:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljuj;->dPv:[B

    const/4 v0, 0x0

    iput-object v0, p0, Ljuj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuj;->eCz:I

    .line 429
    return-void
.end method

.method public static buI()[Ljuj;
    .locals 2

    .prologue
    .line 367
    sget-object v0, Ljuj;->eFj:[Ljuj;

    if-nez v0, :cond_1

    .line 368
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 370
    :try_start_0
    sget-object v0, Ljuj;->eFj:[Ljuj;

    if-nez v0, :cond_0

    .line 371
    const/4 v0, 0x0

    new-array v0, v0, [Ljuj;

    sput-object v0, Ljuj;->eFj:[Ljuj;

    .line 373
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    :cond_1
    sget-object v0, Ljuj;->eFj:[Ljuj;

    return-object v0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 361
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuj;->name:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuj;->eFk:Ljava/lang/String;

    iget v0, p0, Ljuj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuj;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljuj;->dPv:[B

    iget v0, p0, Ljuj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuj;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 444
    const/4 v0, 0x1

    iget-object v1, p0, Ljuj;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 445
    iget v0, p0, Ljuj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 446
    const/4 v0, 0x2

    iget-object v1, p0, Ljuj;->eFk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 448
    :cond_0
    iget v0, p0, Ljuj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 449
    const/4 v0, 0x3

    iget-object v1, p0, Ljuj;->dPv:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 451
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 452
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 456
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 457
    const/4 v1, 0x1

    iget-object v2, p0, Ljuj;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    iget v1, p0, Ljuj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 460
    const/4 v1, 0x2

    iget-object v2, p0, Ljuj;->eFk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 463
    :cond_0
    iget v1, p0, Ljuj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 464
    const/4 v1, 0x3

    iget-object v2, p0, Ljuj;->dPv:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_1
    return v0
.end method
