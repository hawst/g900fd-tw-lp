.class final Liov;
.super Ljava/util/AbstractCollection;
.source "PG"


# instance fields
.field private synthetic dFQ:Liou;


# direct methods
.method constructor <init>(Liou;)V
    .locals 0

    .prologue
    .line 1613
    iput-object p1, p0, Liov;->dFQ:Liou;

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 1634
    iget-object v0, p0, Liov;->dFQ:Liou;

    invoke-virtual {v0}, Liou;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1635
    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 1638
    iget-object v0, p0, Liov;->dFQ:Liou;

    invoke-virtual {v0}, Liou;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 1615
    iget-object v0, p0, Liov;->dFQ:Liou;

    invoke-virtual {v0}, Liou;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1616
    new-instance v1, Liow;

    invoke-direct {v1, p0, v0}, Liow;-><init>(Liov;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1642
    iget-object v0, p0, Liov;->dFQ:Liou;

    iget-object v0, v0, Liou;->dFP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1643
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1644
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1645
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Liov;->dFQ:Liou;

    iget-object v2, v2, Liou;->dBv:Lifw;

    invoke-interface {v2, v0}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1646
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1647
    const/4 v0, 0x1

    .line 1650
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    .line 1654
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1655
    const/4 v0, 0x0

    .line 1656
    iget-object v1, p0, Liov;->dFQ:Liou;

    iget-object v1, v1, Liou;->dFP:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 1657
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1658
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1659
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Liov;->dFQ:Liou;

    iget-object v3, v3, Liou;->dBv:Lifw;

    invoke-interface {v3, v0}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1660
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1661
    const/4 v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 1663
    goto :goto_0

    .line 1664
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    .line 1668
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1669
    const/4 v0, 0x0

    .line 1670
    iget-object v1, p0, Liov;->dFQ:Liou;

    iget-object v1, v1, Liou;->dFP:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 1671
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1672
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1673
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Liov;->dFQ:Liou;

    iget-object v3, v3, Liou;->dBv:Lifw;

    invoke-interface {v3, v0}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1675
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1676
    const/4 v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 1678
    goto :goto_0

    .line 1679
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1630
    iget-object v0, p0, Liov;->dFQ:Liou;

    invoke-virtual {v0}, Liou;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1684
    invoke-virtual {p0}, Liov;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1688
    invoke-virtual {p0}, Liov;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
