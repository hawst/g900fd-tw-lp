.class public final Lcnk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private mCoreSearchServices:Lcfo;

.field private mGsaPreferenceController:Lchr;

.field private mPersonListSupplier:Ligi;

.field private mPlayLoggerProxy:Lcop;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchr;Lcfo;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p2, p0, Lcnk;->mGsaPreferenceController:Lchr;

    .line 63
    iput-object p3, p0, Lcnk;->mCoreSearchServices:Lcfo;

    .line 64
    new-instance v0, Lcnl;

    invoke-direct {v0, p1, p3}, Lcnl;-><init>(Landroid/content/Context;Lcfo;)V

    iput-object v0, p0, Lcnk;->mPersonListSupplier:Ligi;

    .line 65
    new-instance v0, Lcop;

    const-string v1, "ContactLogger"

    const/16 v2, 0x18

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcop;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcnk;->mPlayLoggerProxy:Lcop;

    .line 66
    return-void
.end method

.method public constructor <init>(Lchr;Lcfo;Ligi;Lcop;)V
    .locals 0
    .param p1    # Lchr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ligi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lcop;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcnk;->mGsaPreferenceController:Lchr;

    .line 75
    iput-object p2, p0, Lcnk;->mCoreSearchServices:Lcfo;

    .line 76
    iput-object p3, p0, Lcnk;->mPersonListSupplier:Ligi;

    .line 77
    iput-object p4, p0, Lcnk;->mPlayLoggerProxy:Lcop;

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Lcnm;)Z
    .locals 10
    .param p1    # Lcnm;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    .line 82
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v2

    .line 83
    const/16 v0, 0xe8

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 87
    iget-object v0, p0, Lcnk;->mPersonListSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 88
    const-string v4, "ContactLogger"

    invoke-static {v4, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 89
    const-string v4, "ContactLogger"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "logContacts(): Found "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " contacts in DB"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    invoke-virtual {p0, v0}, Lcnk;->n(Ljava/util/List;)Lcom/google/common/logging/ContactLog$ContactList;

    move-result-object v0

    .line 93
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 95
    iget-object v4, p0, Lcnk;->mGsaPreferenceController:Lchr;

    invoke-virtual {v4}, Lchr;->Kt()Lcyg;

    move-result-object v4

    .line 96
    const-string v5, "KEY_CONTACTS_LOGGER_HASH"

    invoke-interface {v4, v5, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 97
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v6

    .line 98
    const-string v7, "ContactLogger"

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 99
    const-string v7, "ContactLogger"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "logContacts(): Old Hash = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : New Hash = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_1
    if-ne v5, v6, :cond_2

    invoke-virtual {p1}, Lcnm;->QH()Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    .line 114
    :goto_0
    const/16 v1, 0xe9

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 117
    return v0

    .line 107
    :cond_2
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "KEY_CONTACTS_LOGGER_HASH"

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 108
    iget-object v1, p0, Lcnk;->mPlayLoggerProxy:Lcop;

    iget-object v4, p0, Lcnk;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DL()Lcrh;

    move-result-object v4

    invoke-virtual {v4}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcnm;->getStartTime()J

    move-result-wide v6

    invoke-virtual {v1, v4, v6, v7, v0}, Lcop;->a(Ljava/lang/String;J[B)V

    .line 112
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final n(Ljava/util/List;)Lcom/google/common/logging/ContactLog$ContactList;
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 137
    new-instance v2, Lcom/google/common/logging/ContactLog$ContactList;

    invoke-direct {v2}, Lcom/google/common/logging/ContactLog$ContactList;-><init>()V

    .line 138
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 139
    new-instance v4, Lisz;

    invoke-direct {v4}, Lisz;-><init>()V

    .line 145
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lisz;->bU(J)Lisz;

    .line 146
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lisz;->py(Ljava/lang/String;)Lisz;

    .line 149
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alY()Ljava/util/Set;

    move-result-object v1

    const-class v5, Ljava/lang/String;

    invoke-static {v1, v5}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v4, Lisz;->dHk:[Ljava/lang/String;

    .line 150
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiG()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    new-instance v6, Litb;

    invoke-direct {v6}, Litb;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Litb;->pB(Ljava/lang/String;)Litb;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v6, v1}, Litb;->pC(Ljava/lang/String;)Litb;

    :cond_1
    new-array v1, v9, [Litb;

    aput-object v6, v1, v8

    iput-object v1, v4, Lisz;->dHm:[Litb;

    goto :goto_1

    .line 151
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiH()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    new-instance v6, Lita;

    invoke-direct {v6}, Lita;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lita;->pz(Ljava/lang/String;)Lita;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v6, v1}, Lita;->pA(Ljava/lang/String;)Lita;

    :cond_3
    new-array v1, v9, [Lita;

    aput-object v6, v1, v8

    iput-object v1, v4, Lisz;->dHn:[Lita;

    goto :goto_2

    .line 152
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->aiI()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    new-instance v5, Litc;

    invoke-direct {v5}, Litc;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Litc;->pD(Ljava/lang/String;)Litc;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v5, v0}, Litc;->pE(Ljava/lang/String;)Litc;

    :cond_5
    new-array v0, v9, [Litc;

    aput-object v5, v0, v8

    iput-object v0, v4, Lisz;->dHo:[Litc;

    goto :goto_3

    .line 153
    :cond_6
    new-array v0, v9, [Lisz;

    aput-object v4, v0, v8

    iput-object v0, v2, Lcom/google/common/logging/ContactLog$ContactList;->dHp:[Lisz;

    goto/16 :goto_0

    .line 155
    :cond_7
    return-object v2
.end method
