.class public final Ldqp;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# static fields
.field private static final bFF:F


# instance fields
.field private bFG:F

.field private bFH:F

.field private final ob:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 31
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Ldqp;->bFF:F

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 37
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ldqp;->bFG:F

    .line 43
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 45
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    const v1, 0x3e926e98    # 0.286f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 46
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 47
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFFF)V
    .locals 6

    .prologue
    .line 91
    iget v0, p0, Ldqp;->bFH:F

    .line 92
    mul-float v1, p2, v0

    mul-float v2, p3, v0

    mul-float v3, p4, v0

    mul-float v4, p5, v0

    iget-object v5, p0, Ldqp;->ob:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 93
    return-void
.end method


# virtual methods
.method public final E(F)V
    .locals 0

    .prologue
    .line 100
    iput p1, p0, Ldqp;->bFG:F

    .line 101
    invoke-virtual {p0}, Ldqp;->invalidateSelf()V

    .line 102
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 58
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 61
    invoke-virtual {p0}, Ldqp;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Ldqp;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 63
    iget v0, p0, Ldqp;->bFG:F

    sub-float v0, v2, v0

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 68
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, Ldqp;->bFG:F

    sub-float/2addr v0, v1

    const v1, 0x3e926e98    # 0.286f

    mul-float/2addr v0, v1

    add-float v4, v2, v0

    .line 69
    neg-float v2, v4

    move-object v0, p0

    move-object v1, p1

    move v5, v3

    invoke-direct/range {v0 .. v5}, Ldqp;->a(Landroid/graphics/Canvas;FFFF)V

    .line 72
    const/high16 v0, 0x42340000    # 45.0f

    iget v1, p0, Ldqp;->bFG:F

    mul-float v6, v0, v1

    .line 74
    iget v0, p0, Ldqp;->bFG:F

    sget v1, Ldqp;->bFF:F

    sub-float/2addr v1, v4

    mul-float/2addr v0, v1

    add-float/2addr v4, v0

    .line 76
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 77
    neg-float v2, v4

    sget v3, Ldqp;->bFF:F

    sget v5, Ldqp;->bFF:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldqp;->a(Landroid/graphics/Canvas;FFFF)V

    .line 78
    const/high16 v0, -0x40000000    # -2.0f

    mul-float/2addr v0, v6

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 79
    neg-float v2, v4

    sget v0, Ldqp;->bFF:F

    neg-float v3, v0

    sget v0, Ldqp;->bFF:F

    neg-float v5, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldqp;->a(Landroid/graphics/Canvas;FFFF)V

    .line 81
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 82
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, -0x2

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x40249ba6    # 2.572f

    div-float/2addr v0, v1

    iput v0, p0, Ldqp;->bFH:F

    .line 53
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    const v1, 0x3e926e98    # 0.286f

    iget v2, p0, Ldqp;->bFH:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 112
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Ldqp;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 117
    return-void
.end method
