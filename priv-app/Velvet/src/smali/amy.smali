.class public final Lamy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile afP:[Lamy;


# instance fields
.field private aez:I

.field public afA:Ljbj;

.field private afQ:J

.field private afR:[B


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 431
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 432
    const/4 v0, 0x0

    iput v0, p0, Lamy;->aez:I

    iput-object v2, p0, Lamy;->afA:Ljbj;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamy;->afQ:J

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lamy;->afR:[B

    iput-object v2, p0, Lamy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamy;->eCz:I

    .line 433
    return-void
.end method

.method public static nZ()[Lamy;
    .locals 2

    .prologue
    .line 374
    sget-object v0, Lamy;->afP:[Lamy;

    if-nez v0, :cond_1

    .line 375
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 377
    :try_start_0
    sget-object v0, Lamy;->afP:[Lamy;

    if-nez v0, :cond_0

    .line 378
    const/4 v0, 0x0

    new-array v0, v0, [Lamy;

    sput-object v0, Lamy;->afP:[Lamy;

    .line 380
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    :cond_1
    sget-object v0, Lamy;->afP:[Lamy;

    return-object v0

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final A(J)Lamy;
    .locals 1

    .prologue
    .line 396
    iput-wide p1, p0, Lamy;->afQ:J

    .line 397
    iget v0, p0, Lamy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamy;->aez:I

    .line 398
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 368
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lamy;->afA:Ljbj;

    if-nez v0, :cond_1

    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    iput-object v0, p0, Lamy;->afA:Ljbj;

    :cond_1
    iget-object v0, p0, Lamy;->afA:Ljbj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamy;->afQ:J

    iget v0, p0, Lamy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamy;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lamy;->afR:[B

    iget v0, p0, Lamy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamy;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 448
    iget-object v0, p0, Lamy;->afA:Ljbj;

    if-eqz v0, :cond_0

    .line 449
    const/4 v0, 0x1

    iget-object v1, p0, Lamy;->afA:Ljbj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 451
    :cond_0
    iget v0, p0, Lamy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 452
    const/4 v0, 0x2

    iget-wide v2, p0, Lamy;->afQ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 454
    :cond_1
    iget v0, p0, Lamy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 455
    const/4 v0, 0x3

    iget-object v1, p0, Lamy;->afR:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 457
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 458
    return-void
.end method

.method public final d([B)Lamy;
    .locals 1

    .prologue
    .line 415
    if-nez p1, :cond_0

    .line 416
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 418
    :cond_0
    iput-object p1, p0, Lamy;->afR:[B

    .line 419
    iget v0, p0, Lamy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamy;->aez:I

    .line 420
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 462
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 463
    iget-object v1, p0, Lamy;->afA:Ljbj;

    if-eqz v1, :cond_0

    .line 464
    const/4 v1, 0x1

    iget-object v2, p0, Lamy;->afA:Ljbj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_0
    iget v1, p0, Lamy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 468
    const/4 v1, 0x2

    iget-wide v2, p0, Lamy;->afQ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_1
    iget v1, p0, Lamy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 472
    const/4 v1, 0x3

    iget-object v2, p0, Lamy;->afR:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    :cond_2
    return v0
.end method

.method public final oa()J
    .locals 2

    .prologue
    .line 393
    iget-wide v0, p0, Lamy;->afQ:J

    return-wide v0
.end method

.method public final ob()[B
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lamy;->afR:[B

    return-object v0
.end method

.method public final oc()Z
    .locals 1

    .prologue
    .line 423
    iget v0, p0, Lamy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final od()Lamy;
    .locals 1

    .prologue
    .line 426
    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lamy;->afR:[B

    .line 427
    iget v0, p0, Lamy;->aez:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lamy;->aez:I

    .line 428
    return-object p0
.end method
