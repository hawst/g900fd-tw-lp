.class public final Ljyi;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eLK:[Ljyi;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 401
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 402
    const/4 v0, 0x0

    iput v0, p0, Ljyi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljyi;->bmI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljyi;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljyi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyi;->eCz:I

    .line 403
    return-void
.end method

.method public static bwf()[Ljyi;
    .locals 2

    .prologue
    .line 344
    sget-object v0, Ljyi;->eLK:[Ljyi;

    if-nez v0, :cond_1

    .line 345
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 347
    :try_start_0
    sget-object v0, Ljyi;->eLK:[Ljyi;

    if-nez v0, :cond_0

    .line 348
    const/4 v0, 0x0

    new-array v0, v0, [Ljyi;

    sput-object v0, Ljyi;->eLK:[Ljyi;

    .line 350
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    :cond_1
    sget-object v0, Ljyi;->eLK:[Ljyi;

    return-object v0

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 338
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyi;->bmI:Ljava/lang/String;

    iget v0, p0, Ljyi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyi;->dHr:Ljava/lang/String;

    iget v0, p0, Ljyi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 417
    iget v0, p0, Ljyi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 418
    const/4 v0, 0x1

    iget-object v1, p0, Ljyi;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 420
    :cond_0
    iget v0, p0, Ljyi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 421
    const/4 v0, 0x2

    iget-object v1, p0, Ljyi;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 423
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 424
    return-void
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Ljyi;->bmI:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Ljyi;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 428
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 429
    iget v1, p0, Ljyi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 430
    const/4 v1, 0x1

    iget-object v2, p0, Ljyi;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 433
    :cond_0
    iget v1, p0, Ljyi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 434
    const/4 v1, 0x2

    iget-object v2, p0, Ljyi;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 437
    :cond_1
    return v0
.end method

.method public final zN(Ljava/lang/String;)Ljyi;
    .locals 1

    .prologue
    .line 363
    if-nez p1, :cond_0

    .line 364
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 366
    :cond_0
    iput-object p1, p0, Ljyi;->bmI:Ljava/lang/String;

    .line 367
    iget v0, p0, Ljyi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyi;->aez:I

    .line 368
    return-object p0
.end method

.method public final zO(Ljava/lang/String;)Ljyi;
    .locals 1

    .prologue
    .line 385
    if-nez p1, :cond_0

    .line 386
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 388
    :cond_0
    iput-object p1, p0, Ljyi;->dHr:Ljava/lang/String;

    .line 389
    iget v0, p0, Ljyi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyi;->aez:I

    .line 390
    return-object p0
.end method
