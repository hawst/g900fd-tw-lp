.class public final Lftz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected final mActivityHelper:Lfzw;

.field private final mClock:Lemp;

.field private final mRunner:Lerk;


# direct methods
.method public constructor <init>(Lfzw;Lerk;Lemp;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lftz;->mActivityHelper:Lfzw;

    .line 26
    iput-object p2, p0, Lftz;->mRunner:Lerk;

    .line 27
    iput-object p3, p0, Lftz;->mClock:Lemp;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;Lanh;)Lfro;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-virtual {p3}, Lanh;->oP()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    const-string v1, "ModulePresenterFactory"

    const-string v2, "Module type not set!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_0
    return-object v0

    .line 39
    :cond_0
    invoke-virtual {p3}, Lanh;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 215
    const-string v1, "ModulePresenterFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown card module type requested: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lanh;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 41
    :pswitch_0
    new-instance v0, Lfto;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfto;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 44
    :pswitch_1
    new-instance v0, Lfxx;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfxx;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 47
    :pswitch_2
    new-instance v0, Lfzu;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzu;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 51
    :pswitch_3
    new-instance v0, Lfzt;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzt;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 54
    :pswitch_4
    new-instance v0, Lfxz;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfxz;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 57
    :pswitch_5
    new-instance v0, Lfxy;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfxy;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 60
    :pswitch_6
    new-instance v0, Lfsu;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfsu;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 64
    :pswitch_7
    new-instance v0, Lfst;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfst;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 67
    :pswitch_8
    new-instance v0, Lfuo;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfuo;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 70
    :pswitch_9
    new-instance v0, Lfxv;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfxv;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 73
    :pswitch_a
    new-instance v0, Lfub;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfub;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 76
    :pswitch_b
    new-instance v0, Lfue;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfue;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto :goto_0

    .line 81
    :pswitch_c
    new-instance v0, Lfuf;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfuf;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 84
    :pswitch_d
    new-instance v0, Lfuh;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfuh;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 87
    :pswitch_e
    new-instance v0, Lfug;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfug;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 91
    :pswitch_f
    new-instance v0, Lfui;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfui;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 94
    :pswitch_10
    new-instance v0, Lftu;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lftu;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 100
    :pswitch_11
    new-instance v0, Lfxw;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfxw;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 103
    :pswitch_12
    new-instance v0, Lfsv;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfsv;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 106
    :pswitch_13
    new-instance v0, Lfrw;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfrw;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 109
    :pswitch_14
    new-instance v0, Lfym;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lftz;->mClock:Lemp;

    invoke-direct {v0, p1, p2, v1, v2}, Lfym;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V

    goto/16 :goto_0

    .line 113
    :pswitch_15
    new-instance v0, Lfyh;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfyh;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 116
    :pswitch_16
    new-instance v0, Lfru;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfru;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 119
    :pswitch_17
    new-instance v0, Lftr;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lftr;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 123
    :pswitch_18
    new-instance v0, Lftt;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lftt;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 127
    :pswitch_19
    new-instance v0, Lfts;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfts;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 131
    :pswitch_1a
    new-instance v0, Lfyz;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfyz;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 135
    :pswitch_1b
    new-instance v0, Lful;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lful;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 138
    :pswitch_1c
    new-instance v0, Lfsw;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfsw;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 142
    :pswitch_1d
    new-instance v0, Lfuk;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfuk;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 145
    :pswitch_1e
    new-instance v0, Lfzs;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzs;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 148
    :pswitch_1f
    new-instance v0, Lftl;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lftl;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 151
    :pswitch_20
    new-instance v0, Lfzi;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lftz;->mRunner:Lerk;

    invoke-direct {v0, p1, p2, v1, v2}, Lfzi;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lerp;)V

    goto/16 :goto_0

    .line 155
    :pswitch_21
    new-instance v0, Lfzo;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzo;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 158
    :pswitch_22
    new-instance v0, Lfzp;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzp;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 161
    :pswitch_23
    new-instance v0, Lfzh;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lftz;->mClock:Lemp;

    invoke-direct {v0, p1, p2, v1, v2}, Lfzh;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V

    goto/16 :goto_0

    .line 165
    :pswitch_24
    new-instance v0, Lfzd;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzd;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 169
    :pswitch_25
    new-instance v0, Lfya;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lftz;->mRunner:Lerk;

    invoke-direct {v0, p1, p2, v1, v2}, Lfya;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lerk;)V

    goto/16 :goto_0

    .line 173
    :pswitch_26
    new-instance v0, Lfsy;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfsy;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 177
    :pswitch_27
    new-instance v0, Lftg;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lftg;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 180
    :pswitch_28
    new-instance v0, Lfte;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfte;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 183
    :pswitch_29
    new-instance v0, Lfyi;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfyi;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 187
    :pswitch_2a
    new-instance v0, Lfzm;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzm;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 190
    :pswitch_2b
    new-instance v0, Lfzc;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lftz;->mClock:Lemp;

    invoke-direct {v0, p1, p2, v1, v2}, Lfzc;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V

    goto/16 :goto_0

    .line 194
    :pswitch_2c
    new-instance v0, Lfzn;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfzn;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 197
    :pswitch_2d
    new-instance v0, Lfrv;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfrv;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 200
    :pswitch_2e
    new-instance v0, Lfuc;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfuc;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 204
    :pswitch_2f
    new-instance v0, Lfza;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lftz;->mClock:Lemp;

    invoke-direct {v0, p1, p2, v1, v2}, Lfza;-><init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V

    goto/16 :goto_0

    .line 208
    :pswitch_30
    new-instance v0, Lfua;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lfua;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 212
    :pswitch_31
    new-instance v0, Lftm;

    iget-object v1, p0, Lftz;->mActivityHelper:Lfzw;

    invoke-direct {v0, p1, p2, v1}, Lftm;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    goto/16 :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_10
        :pswitch_8
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_b
        :pswitch_f
        :pswitch_9
        :pswitch_11
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1e
        :pswitch_1d
        :pswitch_1f
        :pswitch_11
        :pswitch_11
        :pswitch_20
        :pswitch_c
        :pswitch_d
        :pswitch_23
        :pswitch_11
        :pswitch_24
        :pswitch_18
        :pswitch_c
        :pswitch_25
        :pswitch_21
        :pswitch_26
        :pswitch_e
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_22
        :pswitch_12
        :pswitch_2b
        :pswitch_a
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_19
        :pswitch_31
    .end packed-switch
.end method
