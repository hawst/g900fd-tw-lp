.class public final Ljxq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public eKW:[I

.field private eKX:J

.field private eKY:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Ljxq;->aez:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljxq;->eKW:[I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljxq;->eKX:J

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljxq;->eKY:[I

    const/4 v0, 0x0

    iput-object v0, p0, Ljxq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxq;->eCz:I

    .line 54
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljxq;->eKW:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljxq;->eKW:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljxq;->eKW:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljxq;->eKW:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljxq;->eKW:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljxq;->eKW:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljxq;->eKW:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljxq;->eKW:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljxq;->eKX:J

    iget v0, p0, Ljxq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxq;->aez:I

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x18

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljxq;->eKY:[I

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljxq;->eKY:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Ljxq;->eKY:[I

    array-length v0, v0

    goto :goto_6

    :cond_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljxq;->eKY:[I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_b

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljxq;->eKY:[I

    if-nez v2, :cond_d

    move v2, v1

    :goto_9
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_c

    iget-object v4, p0, Ljxq;->eKY:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_a
    array-length v4, v0

    if-ge v2, v4, :cond_e

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_d
    iget-object v2, p0, Ljxq;->eKY:[I

    array-length v2, v2

    goto :goto_9

    :cond_e
    iput-object v0, p0, Ljxq;->eKY:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x1a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Ljxq;->eKW:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljxq;->eKW:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 70
    :goto_0
    iget-object v2, p0, Ljxq;->eKW:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 71
    const/4 v2, 0x1

    iget-object v3, p0, Ljxq;->eKW:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_0
    iget v0, p0, Ljxq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 75
    const/4 v0, 0x2

    iget-wide v2, p0, Ljxq;->eKX:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 77
    :cond_1
    iget-object v0, p0, Ljxq;->eKY:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljxq;->eKY:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 78
    :goto_1
    iget-object v0, p0, Ljxq;->eKY:[I

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 79
    const/4 v0, 0x3

    iget-object v2, p0, Ljxq;->eKY:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 82
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 83
    return-void
.end method

.method public final bvN()J
    .locals 2

    .prologue
    .line 33
    iget-wide v0, p0, Ljxq;->eKX:J

    return-wide v0
.end method

.method public final bvO()Z
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Ljxq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dN(J)Ljxq;
    .locals 1

    .prologue
    .line 36
    iput-wide p1, p0, Ljxq;->eKX:J

    .line 37
    iget v0, p0, Ljxq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxq;->aez:I

    .line 38
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-super {p0}, Ljsl;->lF()I

    move-result v3

    .line 88
    iget-object v0, p0, Ljxq;->eKW:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljxq;->eKW:[I

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    move v2, v1

    .line 90
    :goto_0
    iget-object v4, p0, Ljxq;->eKW:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 91
    iget-object v4, p0, Ljxq;->eKW:[I

    aget v4, v4, v0

    .line 92
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_0
    add-int v0, v3, v2

    .line 96
    iget-object v2, p0, Ljxq;->eKW:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 98
    :goto_1
    iget v2, p0, Ljxq;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 99
    const/4 v2, 0x2

    iget-wide v4, p0, Ljxq;->eKX:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 102
    :cond_1
    iget-object v2, p0, Ljxq;->eKY:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljxq;->eKY:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 104
    :goto_2
    iget-object v3, p0, Ljxq;->eKY:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 105
    iget-object v3, p0, Ljxq;->eKY:[I

    aget v3, v3, v1

    .line 106
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 104
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 109
    :cond_2
    add-int/2addr v0, v2

    .line 110
    iget-object v1, p0, Ljxq;->eKY:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 112
    :cond_3
    return v0

    :cond_4
    move v0, v3

    goto :goto_1
.end method
