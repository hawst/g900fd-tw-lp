.class public final Ljrv;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eBQ:[Ljrv;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private eBR:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 69
    iput v1, p0, Ljrv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljrv;->agq:Ljava/lang/String;

    iput v1, p0, Ljrv;->eBR:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljrv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrv;->eCz:I

    .line 70
    return-void
.end method

.method public static btG()[Ljrv;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljrv;->eBQ:[Ljrv;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljrv;->eBQ:[Ljrv;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljrv;

    sput-object v0, Ljrv;->eBQ:[Ljrv;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljrv;->eBQ:[Ljrv;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrv;->agq:Ljava/lang/String;

    iget v0, p0, Ljrv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrv;->eBR:I

    iget v0, p0, Ljrv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrv;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Ljrv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    iget-object v1, p0, Ljrv;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 87
    :cond_0
    iget v0, p0, Ljrv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 88
    const/4 v0, 0x2

    iget v1, p0, Ljrv;->eBR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 90
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 91
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 96
    iget v1, p0, Ljrv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 97
    const/4 v1, 0x1

    iget-object v2, p0, Ljrv;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_0
    iget v1, p0, Ljrv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 101
    const/4 v1, 0x2

    iget v2, p0, Ljrv;->eBR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    :cond_1
    return v0
.end method
