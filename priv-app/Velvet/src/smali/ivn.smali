.class final Livn;
.super Liuw;
.source "PG"


# instance fields
.field dLp:Lijj;

.field private dLq:Z

.field private dLr:Ljava/util/concurrent/atomic/AtomicInteger;

.field dLs:Ljava/util/List;


# direct methods
.method constructor <init>(Lijj;ZLjava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 1293
    invoke-direct {p0}, Liuw;-><init>()V

    .line 1294
    iput-object p1, p0, Livn;->dLp:Lijj;

    .line 1295
    invoke-virtual {p1}, Lijj;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Livn;->dLs:Ljava/util/List;

    .line 1296
    const/4 v0, 0x0

    iput-boolean v0, p0, Livn;->dLq:Z

    .line 1297
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Lijj;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1299
    invoke-direct {p0, p3}, Livn;->b(Ljava/util/concurrent/Executor;)V

    .line 1300
    return-void
.end method

.method static synthetic a(Livn;ILjava/util/concurrent/Future;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1276
    iget-object v2, p0, Livn;->dLs:Ljava/util/List;

    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v2, :cond_2

    :cond_0
    iget-boolean v0, p0, Livn;->dLq:Z

    const-string v1, "Future was done before all dependencies completed"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_0
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v3

    const-string v4, "Tried to set value from future which is not done"

    invoke-static {v3, v4}, Lifv;->d(ZLjava/lang/Object;)V

    invoke-static {p2}, Liwc;->d(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-ltz v2, :cond_3

    :goto_1
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    if-nez v2, :cond_1

    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    iget-boolean v2, p0, Livn;->dLq:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Livn;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    iget-object v2, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-ltz v2, :cond_6

    :goto_2
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    if-nez v2, :cond_1

    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    if-eqz v0, :cond_7

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_2
    iget-boolean v3, p0, Livn;->dLq:Z

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {p0, v2}, Livn;->h(Ljava/lang/Throwable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    iget-object v2, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-ltz v2, :cond_9

    :goto_3
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    if-nez v2, :cond_1

    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    if-eqz v0, :cond_a

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto/16 :goto_0

    :catch_2
    move-exception v2

    :try_start_3
    iget-boolean v3, p0, Livn;->dLq:Z

    if-eqz v3, :cond_b

    invoke-virtual {p0, v2}, Livn;->h(Ljava/lang/Throwable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_b
    iget-object v2, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-ltz v2, :cond_c

    :goto_4
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    if-nez v2, :cond_1

    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    if-eqz v0, :cond_d

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto :goto_4

    :cond_d
    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto/16 :goto_0

    :catch_3
    move-exception v2

    :try_start_4
    invoke-virtual {p0, v2}, Livn;->h(Ljava/lang/Throwable;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v2, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    if-ltz v2, :cond_e

    :goto_5
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    if-nez v2, :cond_1

    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    if-eqz v0, :cond_f

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto :goto_5

    :cond_f
    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Livn;->dLr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v3

    if-ltz v3, :cond_11

    :goto_6
    const-string v1, "Less than 0 remaining futures"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    if-nez v3, :cond_10

    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    if-eqz v0, :cond_12

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    :cond_10
    :goto_7
    throw v2

    :cond_11
    move v0, v1

    goto :goto_6

    :cond_12
    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto :goto_7
.end method

.method private aFL()Ljava/util/List;
    .locals 3

    .prologue
    .line 1404
    iget-object v0, p0, Livn;->dLp:Lijj;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Livn;->isDone()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livq;

    :cond_1
    :goto_0
    invoke-interface {v0}, Livq;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    invoke-interface {v0}, Livq;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    throw v0

    :catch_2
    move-exception v2

    iget-boolean v2, p0, Livn;->dLq:Z

    if-eqz v2, :cond_1

    .line 1408
    :cond_2
    invoke-super {p0}, Liuw;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private b(Ljava/util/concurrent/Executor;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1304
    new-instance v0, Livo;

    invoke-direct {v0, p0}, Livo;-><init>(Livn;)V

    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Livn;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1319
    iget-object v0, p0, Livn;->dLp:Lijj;

    invoke-virtual {v0}, Lijj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1320
    iget-object v0, p0, Livn;->dLs:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Livn;->bB(Ljava/lang/Object;)Z

    .line 1347
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1325
    :goto_0
    iget-object v2, p0, Livn;->dLp:Lijj;

    invoke-virtual {v2}, Lijj;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1326
    iget-object v2, p0, Livn;->dLs:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1336
    :cond_2
    iget-object v2, p0, Livn;->dLp:Lijj;

    .line 1337
    :goto_1
    invoke-virtual {v2}, Lijj;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1338
    invoke-virtual {v2, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livq;

    .line 1340
    new-instance v3, Livp;

    invoke-direct {v3, p0, v1, v0}, Livp;-><init>(Livn;ILivq;)V

    invoke-interface {v0, v3, p1}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1337
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1276
    invoke-direct {p0}, Livn;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
