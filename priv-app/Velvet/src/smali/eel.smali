.class public final Leel;
.super Leeo;
.source "PG"


# direct methods
.method public constructor <init>(Lekz;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Leeo;-><init>(Lekz;)V

    .line 40
    return-void
.end method

.method public static a([Leox;[Leox;)[I
    .locals 6

    .prologue
    .line 88
    new-instance v0, Leov;

    invoke-direct {v0, p0, p1}, Leov;-><init>([Leox;[Leox;)V

    .line 89
    invoke-virtual {v0}, Leov;->auZ()I

    .line 90
    array-length v1, p1

    .line 91
    new-array v2, v1, [I

    .line 92
    invoke-virtual {v0}, Leov;->ava()[Leow;

    move-result-object v3

    .line 93
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 94
    aget-object v4, v3, v0

    iget v4, v4, Leow;->bus:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 95
    aget-object v4, v3, v0

    iget v4, v4, Leow;->ry:I

    aput v4, v2, v0

    .line 93
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    const/4 v4, -0x1

    aput v4, v2, v0

    goto :goto_1

    .line 100
    :cond_1
    return-object v2
.end method

.method public static kN(Ljava/lang/String;)[Leox;
    .locals 12

    .prologue
    const/16 v11, 0x22

    const/16 v10, 0x20

    const/16 v9, 0x9

    const/4 v4, 0x0

    .line 105
    .line 106
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 107
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 109
    new-array v7, v5, [Leox;

    move v1, v4

    move v3, v4

    .line 111
    :goto_0
    if-ge v3, v5, :cond_3

    .line 112
    :goto_1
    if-ge v3, v5, :cond_1

    aget-char v0, v6, v3

    if-eq v0, v10, :cond_0

    aget-char v0, v6, v3

    if-eq v0, v9, :cond_0

    aget-char v0, v6, v3

    if-ne v0, v11, :cond_1

    .line 113
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v2, v3

    .line 116
    :goto_2
    if-ge v2, v5, :cond_2

    aget-char v0, v6, v2

    if-eq v0, v10, :cond_2

    aget-char v0, v6, v2

    if-eq v0, v9, :cond_2

    aget-char v0, v6, v2

    if-eq v0, v11, :cond_2

    .line 117
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 120
    :cond_2
    if-eq v3, v2, :cond_4

    .line 121
    add-int/lit8 v0, v1, 0x1

    new-instance v8, Leox;

    invoke-direct {v8, v6, v3, v2}, Leox;-><init>([CII)V

    aput-object v8, v7, v1

    :goto_3
    move v1, v0

    move v3, v2

    .line 123
    goto :goto_0

    .line 125
    :cond_3
    new-array v0, v1, [Leox;

    .line 126
    invoke-static {v7, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    return-object v0

    :cond_4
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;II)Landroid/text/Spanned;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {v0}, Leel;->kN(Ljava/lang/String;)[Leox;

    move-result-object v3

    .line 49
    invoke-static {p2}, Leel;->kN(Ljava/lang/String;)[Leox;

    move-result-object v4

    .line 50
    invoke-static {v3, v4}, Leel;->a([Leox;[Leox;)[I

    move-result-object v5

    .line 56
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 58
    array-length v7, v5

    move v2, v1

    .line 59
    :goto_0
    if-ge v2, v7, :cond_0

    .line 60
    aget-object v8, v4, v2

    .line 62
    aget v0, v5, v2

    .line 63
    if-ltz v0, :cond_1

    .line 64
    aget-object v0, v3, v0

    invoke-virtual {v0}, Leox;->length()I

    move-result v0

    .line 66
    :goto_1
    iget v9, v8, Leox;->bgm:I

    add-int/2addr v9, v0

    iget v10, v8, Leox;->fi:I

    invoke-virtual {p0, p4, v6, v9, v10}, Leel;->a(ILandroid/text/Spannable;II)V

    .line 67
    iget v9, v8, Leox;->bgm:I

    iget v8, v8, Leox;->bgm:I

    add-int/2addr v0, v8

    invoke-virtual {p0, p3, v6, v9, v0}, Leel;->a(ILandroid/text/Spannable;II)V

    .line 59
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 70
    :cond_0
    return-object v6

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected final synthetic b(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2, p3, p4}, Leel;->a(Ljava/lang/String;Ljava/lang/String;II)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method
