.class public final Ljbr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field private dZe:Ljava/lang/String;

.field private dZf:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28815
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 28816
    const/4 v0, 0x0

    iput v0, p0, Ljbr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbr;->dZe:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbr;->afb:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbr;->dZf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljbr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbr;->eCz:I

    .line 28817
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 28730
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbr;->dZe:Ljava/lang/String;

    iget v0, p0, Ljbr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbr;->afb:Ljava/lang/String;

    iget v0, p0, Ljbr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbr;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbr;->dZf:Ljava/lang/String;

    iget v0, p0, Ljbr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbr;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 28832
    iget v0, p0, Ljbr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 28833
    const/4 v0, 0x1

    iget-object v1, p0, Ljbr;->dZe:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28835
    :cond_0
    iget v0, p0, Ljbr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 28836
    const/4 v0, 0x2

    iget-object v1, p0, Ljbr;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28838
    :cond_1
    iget v0, p0, Ljbr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 28839
    const/4 v0, 0x3

    iget-object v1, p0, Ljbr;->dZf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 28841
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 28842
    return-void
.end method

.method public final baV()Z
    .locals 1

    .prologue
    .line 28785
    iget v0, p0, Ljbr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28752
    iget-object v0, p0, Ljbr;->dZe:Ljava/lang/String;

    return-object v0
.end method

.method public final bfn()Z
    .locals 1

    .prologue
    .line 28763
    iget v0, p0, Ljbr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28796
    iget-object v0, p0, Ljbr;->dZf:Ljava/lang/String;

    return-object v0
.end method

.method public final bfp()Z
    .locals 1

    .prologue
    .line 28807
    iget v0, p0, Ljbr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28774
    iget-object v0, p0, Ljbr;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 28846
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 28847
    iget v1, p0, Ljbr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 28848
    const/4 v1, 0x1

    iget-object v2, p0, Ljbr;->dZe:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28851
    :cond_0
    iget v1, p0, Ljbr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 28852
    const/4 v1, 0x2

    iget-object v2, p0, Ljbr;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28855
    :cond_1
    iget v1, p0, Ljbr;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 28856
    const/4 v1, 0x3

    iget-object v2, p0, Ljbr;->dZf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28859
    :cond_2
    return v0
.end method

.method public final sv(Ljava/lang/String;)Ljbr;
    .locals 1

    .prologue
    .line 28755
    if-nez p1, :cond_0

    .line 28756
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28758
    :cond_0
    iput-object p1, p0, Ljbr;->dZe:Ljava/lang/String;

    .line 28759
    iget v0, p0, Ljbr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbr;->aez:I

    .line 28760
    return-object p0
.end method

.method public final sw(Ljava/lang/String;)Ljbr;
    .locals 1

    .prologue
    .line 28777
    if-nez p1, :cond_0

    .line 28778
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28780
    :cond_0
    iput-object p1, p0, Ljbr;->afb:Ljava/lang/String;

    .line 28781
    iget v0, p0, Ljbr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbr;->aez:I

    .line 28782
    return-object p0
.end method

.method public final sx(Ljava/lang/String;)Ljbr;
    .locals 1

    .prologue
    .line 28799
    if-nez p1, :cond_0

    .line 28800
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28802
    :cond_0
    iput-object p1, p0, Ljbr;->dZf:Ljava/lang/String;

    .line 28803
    iget v0, p0, Ljbr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbr;->aez:I

    .line 28804
    return-object p0
.end method
