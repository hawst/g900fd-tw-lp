.class public final Lixz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dOW:I

.field private dOX:Ljava/lang/String;

.field private dOY:Ljava/lang/String;

.field private dOZ:J

.field private dPa:J

.field public dPb:Liws;

.field private dPc:I

.field public dPd:Lizc;

.field private dPe:I

.field private dPf:Z

.field private dza:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1347
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1348
    iput v1, p0, Lixz;->aez:I

    iput v1, p0, Lixz;->dOW:I

    const-string v0, ""

    iput-object v0, p0, Lixz;->dOX:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lixz;->dOY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lixz;->dza:Ljava/lang/String;

    iput-wide v4, p0, Lixz;->dOZ:J

    iput-wide v4, p0, Lixz;->dPa:J

    iput-object v2, p0, Lixz;->dPb:Liws;

    iput v1, p0, Lixz;->dPc:I

    iput-object v2, p0, Lixz;->dPd:Lizc;

    iput v1, p0, Lixz;->dPe:I

    iput-boolean v1, p0, Lixz;->dPf:Z

    iput-object v2, p0, Lixz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixz;->eCz:I

    .line 1349
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1103
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lixz;->dOW:I

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixz;->dOX:Ljava/lang/String;

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixz;->dOY:Ljava/lang/String;

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lixz;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixz;->dza:Ljava/lang/String;

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lixz;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lixz;->dOZ:J

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lixz;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lixz;->dPb:Liws;

    if-nez v0, :cond_1

    new-instance v0, Liws;

    invoke-direct {v0}, Liws;-><init>()V

    iput-object v0, p0, Lixz;->dPb:Liws;

    :cond_1
    iget-object v0, p0, Lixz;->dPb:Liws;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lixz;->dPa:J

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lixz;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lixz;->dPc:I

    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lixz;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lixz;->dPd:Lizc;

    if-nez v0, :cond_2

    new-instance v0, Lizc;

    invoke-direct {v0}, Lizc;-><init>()V

    iput-object v0, p0, Lixz;->dPd:Lizc;

    :cond_2
    iget-object v0, p0, Lixz;->dPd:Lizc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Lixz;->dPe:I

    iget v0, p0, Lixz;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lixz;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lixz;->dPf:Z

    iget v0, p0, Lixz;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lixz;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1372
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1373
    const/4 v0, 0x1

    iget v1, p0, Lixz;->dOW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1375
    :cond_0
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1376
    const/4 v0, 0x2

    iget-object v1, p0, Lixz;->dOX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1378
    :cond_1
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1379
    const/4 v0, 0x3

    iget-object v1, p0, Lixz;->dOY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1381
    :cond_2
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1382
    const/4 v0, 0x4

    iget-object v1, p0, Lixz;->dza:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1384
    :cond_3
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1385
    const/4 v0, 0x5

    iget-wide v2, p0, Lixz;->dOZ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1387
    :cond_4
    iget-object v0, p0, Lixz;->dPb:Liws;

    if-eqz v0, :cond_5

    .line 1388
    const/4 v0, 0x6

    iget-object v1, p0, Lixz;->dPb:Liws;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1390
    :cond_5
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 1391
    const/4 v0, 0x7

    iget-wide v2, p0, Lixz;->dPa:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1393
    :cond_6
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 1394
    const/16 v0, 0x8

    iget v1, p0, Lixz;->dPc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1396
    :cond_7
    iget-object v0, p0, Lixz;->dPd:Lizc;

    if-eqz v0, :cond_8

    .line 1397
    const/16 v0, 0x9

    iget-object v1, p0, Lixz;->dPd:Lizc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1399
    :cond_8
    iget v0, p0, Lixz;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_9

    .line 1400
    const/16 v0, 0xa

    iget v1, p0, Lixz;->dPe:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1402
    :cond_9
    iget v0, p0, Lixz;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_a

    .line 1403
    const/16 v0, 0xb

    iget-boolean v1, p0, Lixz;->dPf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1405
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1406
    return-void
.end method

.method public final awn()I
    .locals 1

    .prologue
    .line 1290
    iget v0, p0, Lixz;->dPc:I

    return v0
.end method

.method public final bbi()I
    .locals 1

    .prologue
    .line 1164
    iget v0, p0, Lixz;->dOW:I

    return v0
.end method

.method public final bbj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1183
    iget-object v0, p0, Lixz;->dOX:Ljava/lang/String;

    return-object v0
.end method

.method public final bbk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lixz;->dOY:Ljava/lang/String;

    return-object v0
.end method

.method public final bbl()J
    .locals 2

    .prologue
    .line 1268
    iget-wide v0, p0, Lixz;->dPa:J

    return-wide v0
.end method

.method public final bbm()Z
    .locals 1

    .prologue
    .line 1276
    iget v0, p0, Lixz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bbn()I
    .locals 1

    .prologue
    .line 1312
    iget v0, p0, Lixz;->dPe:I

    return v0
.end method

.method public final bbo()Z
    .locals 1

    .prologue
    .line 1331
    iget-boolean v0, p0, Lixz;->dPf:Z

    return v0
.end method

.method public final cl(J)Lixz;
    .locals 1

    .prologue
    .line 1271
    iput-wide p1, p0, Lixz;->dPa:J

    .line 1272
    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lixz;->aez:I

    .line 1273
    return-object p0
.end method

.method public final hk(Z)Lixz;
    .locals 1

    .prologue
    .line 1334
    iput-boolean p1, p0, Lixz;->dPf:Z

    .line 1335
    iget v0, p0, Lixz;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lixz;->aez:I

    .line 1336
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1410
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1411
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1412
    const/4 v1, 0x1

    iget v2, p0, Lixz;->dOW:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1415
    :cond_0
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1416
    const/4 v1, 0x2

    iget-object v2, p0, Lixz;->dOX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1419
    :cond_1
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1420
    const/4 v1, 0x3

    iget-object v2, p0, Lixz;->dOY:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1423
    :cond_2
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1424
    const/4 v1, 0x4

    iget-object v2, p0, Lixz;->dza:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1427
    :cond_3
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1428
    const/4 v1, 0x5

    iget-wide v2, p0, Lixz;->dOZ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1431
    :cond_4
    iget-object v1, p0, Lixz;->dPb:Liws;

    if-eqz v1, :cond_5

    .line 1432
    const/4 v1, 0x6

    iget-object v2, p0, Lixz;->dPb:Liws;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1435
    :cond_5
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 1436
    const/4 v1, 0x7

    iget-wide v2, p0, Lixz;->dPa:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1439
    :cond_6
    iget v1, p0, Lixz;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 1440
    const/16 v1, 0x8

    iget v2, p0, Lixz;->dPc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1443
    :cond_7
    iget-object v1, p0, Lixz;->dPd:Lizc;

    if-eqz v1, :cond_8

    .line 1444
    const/16 v1, 0x9

    iget-object v2, p0, Lixz;->dPd:Lizc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1447
    :cond_8
    iget v1, p0, Lixz;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_9

    .line 1448
    const/16 v1, 0xa

    iget v2, p0, Lixz;->dPe:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1451
    :cond_9
    iget v1, p0, Lixz;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_a

    .line 1452
    const/16 v1, 0xb

    iget-boolean v2, p0, Lixz;->dPf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1455
    :cond_a
    return v0
.end method

.method public final nt(I)Lixz;
    .locals 1

    .prologue
    .line 1167
    const/4 v0, 0x1

    iput v0, p0, Lixz;->dOW:I

    .line 1168
    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixz;->aez:I

    .line 1169
    return-object p0
.end method

.method public final nu(I)Lixz;
    .locals 1

    .prologue
    .line 1293
    iput p1, p0, Lixz;->dPc:I

    .line 1294
    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lixz;->aez:I

    .line 1295
    return-object p0
.end method

.method public final nv(I)Lixz;
    .locals 1

    .prologue
    .line 1315
    iput p1, p0, Lixz;->dPe:I

    .line 1316
    iget v0, p0, Lixz;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lixz;->aez:I

    .line 1317
    return-object p0
.end method

.method public final ri(Ljava/lang/String;)Lixz;
    .locals 1

    .prologue
    .line 1186
    if-nez p1, :cond_0

    .line 1187
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1189
    :cond_0
    iput-object p1, p0, Lixz;->dOX:Ljava/lang/String;

    .line 1190
    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixz;->aez:I

    .line 1191
    return-object p0
.end method

.method public final rj(Ljava/lang/String;)Lixz;
    .locals 1

    .prologue
    .line 1208
    if-nez p1, :cond_0

    .line 1209
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1211
    :cond_0
    iput-object p1, p0, Lixz;->dOY:Ljava/lang/String;

    .line 1212
    iget v0, p0, Lixz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lixz;->aez:I

    .line 1213
    return-object p0
.end method
