.class public Lgmh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lceq;


# instance fields
.field private aTF:Z

.field private final bbd:Lenw;

.field private final cPe:Ljava/io/InputStream;

.field private final rV:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;II)V
    .locals 2
    .param p1    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-static {p1, p2}, Lgfd;->b(Ljava/io/InputStream;I)Ljava/io/InputStream;

    move-result-object v0

    new-array v1, p3, [B

    invoke-direct {p0, v0, v1}, Lgmh;-><init>(Ljava/io/InputStream;[B)V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;[B)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lgmh;->cPe:Ljava/io/InputStream;

    .line 51
    iput-object p2, p0, Lgmh;->rV:[B

    .line 52
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lgmh;->bbd:Lenw;

    .line 53
    return-void
.end method

.method private aHn()V
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lgmh;->aTF:Z

    if-nez v0, :cond_0

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmh;->aTF:Z

    .line 91
    iget-object v0, p0, Lgmh;->cPe:Ljava/io/InputStream;

    invoke-static {v0}, Leoo;->i(Ljava/io/InputStream;)V

    .line 93
    :cond_0
    return-void
.end method


# virtual methods
.method public final Dr()Ljwv;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 57
    iget-object v1, p0, Lgmh;->bbd:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    .line 60
    :try_start_0
    iget-boolean v1, p0, Lgmh;->aTF:Z

    if-eqz v1, :cond_0

    .line 69
    :goto_0
    return-object v0

    .line 64
    :cond_0
    iget-object v1, p0, Lgmh;->cPe:Ljava/io/InputStream;

    iget-object v2, p0, Lgmh;->rV:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lgmh;->rV:[B

    array-length v4, v4

    invoke-static {v1, v2, v3, v4}, Leoo;->a(Ljava/io/InputStream;[BII)I

    move-result v1

    .line 65
    if-lez v1, :cond_1

    .line 66
    iget-object v0, p0, Lgmh;->rV:[B

    invoke-virtual {p0, v0, v1}, Lgmh;->b([BI)Ljwv;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_1
    invoke-direct {p0}, Lgmh;->aHn()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    invoke-direct {p0}, Lgmh;->aHn()V

    .line 73
    new-instance v1, Leie;

    const v2, 0x2000b

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method

.method protected b([BI)Ljwv;
    .locals 1

    .prologue
    .line 78
    invoke-static {p1, p2}, Lcek;->b([BI)Ljwv;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lgmh;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 85
    invoke-direct {p0}, Lgmh;->aHn()V

    .line 86
    return-void
.end method

.method public final yo()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method
