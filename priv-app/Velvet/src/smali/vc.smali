.class public final Lvc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic HC:Lcom/android/launcher3/Folder;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Folder;)V
    .locals 0

    .prologue
    .line 1173
    iput-object p1, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1176
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v1, v1, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v1, Lvy;->JA:J

    iget-object v1, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v1, v1, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v4, v1, Lvy;->Bd:J

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/launcher3/Launcher;->a(JJ)Lcom/android/launcher3/CellLayout;

    move-result-object v9

    .line 1178
    const/4 v1, 0x0

    .line 1180
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    if-ne v0, v10, :cond_0

    .line 1181
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-object v0, v0, Lvy;->IA:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ladh;

    .line 1182
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    const v2, 0x7f04001c

    invoke-virtual {v0, v2, v9, v1}, Lcom/android/launcher3/Launcher;->a(ILandroid/view/ViewGroup;Ladh;)Landroid/view/View;

    move-result-object v8

    .line 1184
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v2, v2, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v2, Lvy;->JA:J

    iget-object v4, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v4, v4, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v4, v4, Lvy;->Bd:J

    iget-object v6, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v6, v6, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget v6, v6, Lvy;->Bb:I

    iget-object v7, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v7, v7, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget v7, v7, Lvy;->Bc:I

    invoke-static/range {v0 .. v7}, Lzi;->a(Landroid/content/Context;Lwq;JJII)V

    move-object v1, v8

    .line 1187
    :cond_0
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    if-gt v0, v10, :cond_3

    .line 1189
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v2, v2, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-static {v0, v2}, Lzi;->b(Landroid/content/Context;Lwq;)V

    .line 1190
    if-eqz v9, :cond_1

    .line 1192
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    invoke-static {v0}, Lcom/android/launcher3/Folder;->f(Lcom/android/launcher3/Folder;)Lcom/android/launcher3/FolderIcon;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 1194
    :cond_1
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    invoke-static {v0}, Lcom/android/launcher3/Folder;->f(Lcom/android/launcher3/Folder;)Lcom/android/launcher3/FolderIcon;

    move-result-object v0

    instance-of v0, v0, Luo;

    if-eqz v0, :cond_2

    .line 1195
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v2, v0, Lcom/android/launcher3/Folder;->yg:Lty;

    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    invoke-static {v0}, Lcom/android/launcher3/Folder;->f(Lcom/android/launcher3/Folder;)Lcom/android/launcher3/FolderIcon;

    move-result-object v0

    check-cast v0, Luo;

    invoke-virtual {v2, v0}, Lty;->c(Luo;)V

    .line 1197
    :cond_2
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->GM:Lvy;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->b(Lvy;)V

    .line 1202
    :cond_3
    if-eqz v1, :cond_4

    .line 1203
    iget-object v0, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v0, v0, Lcom/android/launcher3/Folder;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v0

    iget-object v2, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v2, v2, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v2, v2, Lvy;->JA:J

    iget-object v4, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v4, v4, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget-wide v4, v4, Lvy;->Bd:J

    iget-object v6, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v6, v6, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget v6, v6, Lvy;->Bb:I

    iget-object v7, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v7, v7, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget v7, v7, Lvy;->Bc:I

    iget-object v8, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v8, v8, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget v8, v8, Lvy;->AY:I

    iget-object v9, p0, Lvc;->HC:Lcom/android/launcher3/Folder;

    iget-object v9, v9, Lcom/android/launcher3/Folder;->GM:Lvy;

    iget v9, v9, Lvy;->AZ:I

    invoke-virtual/range {v0 .. v9}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIII)V

    .line 1206
    :cond_4
    return-void
.end method
