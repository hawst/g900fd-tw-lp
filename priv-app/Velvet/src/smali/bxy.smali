.class public final Lbxy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static aOv:I


# instance fields
.field private aMF:I

.field private aOA:Ljava/lang/String;

.field private aOB:Lepp;

.field private final aOw:Ljava/util/concurrent/Executor;

.field final aOx:Ligi;

.field aOy:Z

.field private aOz:J

.field atG:Leqo;

.field final mAdaptiveTtsHelper:Lbwo;

.field private mAudioManager:Landroid/media/AudioManager;

.field final mClock:Lemp;

.field final mContext:Landroid/content/Context;

.field final mEventBus:Ldda;

.field private final mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

.field final mVoiceSearchServices:Lhhq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x5

    sput v0, Lbxy;->aOv:I

    return-void
.end method

.method public constructor <init>(Leqo;Ljava/util/concurrent/Executor;Landroid/content/Context;Landroid/media/AudioManager;Lbwo;Ldda;Lhhq;Lemp;Ligi;Lcom/google/android/handsfree/MessageBuffer;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Lbxz;

    const-string v1, "Message interaction timeout"

    invoke-direct {v0, p0, v1}, Lbxz;-><init>(Lbxy;Ljava/lang/String;)V

    iput-object v0, p0, Lbxy;->aOB:Lepp;

    .line 102
    iput-object p1, p0, Lbxy;->atG:Leqo;

    .line 103
    iput-object p2, p0, Lbxy;->aOw:Ljava/util/concurrent/Executor;

    .line 104
    iput-object p3, p0, Lbxy;->mContext:Landroid/content/Context;

    .line 105
    iput-object p5, p0, Lbxy;->mAdaptiveTtsHelper:Lbwo;

    .line 106
    iput-object p6, p0, Lbxy;->mEventBus:Ldda;

    .line 107
    iput-object p7, p0, Lbxy;->mVoiceSearchServices:Lhhq;

    .line 108
    iput-object p8, p0, Lbxy;->mClock:Lemp;

    .line 109
    iput-object p9, p0, Lbxy;->aOx:Ligi;

    .line 110
    iput-object p10, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    .line 111
    iput-object p4, p0, Lbxy;->mAudioManager:Landroid/media/AudioManager;

    .line 112
    return-void
.end method

.method private BG()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lbxy;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lbxy;->aOz:J

    .line 247
    return-void
.end method

.method private fP(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 266
    invoke-static {}, Lenu;->auR()V

    .line 268
    iget-object v0, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/handsfree/MessageBuffer;->fO(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/handsfree/MessageBuffer;->fN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 272
    :goto_0
    iget-object v0, p0, Lbxy;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOS()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgij;

    invoke-interface {v0}, Lgij;->aFY()Ligi;

    move-result-object v4

    check-cast v4, Lgii;

    .line 275
    iget-object v7, p0, Lbxy;->aOw:Ljava/util/concurrent/Executor;

    new-instance v0, Lbya;

    const-string v2, "Creating notification announcement"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbya;-><init>(Lbxy;Ljava/lang/String;[ILgii;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 328
    return-void

    .line 268
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final BB()V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lbxy;->atG:Leqo;

    iget-object v1, p0, Lbxy;->aOB:Lepp;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 173
    iget-object v0, p0, Lbxy;->atG:Leqo;

    iget-object v1, p0, Lbxy;->aOB:Lepp;

    const-wide/16 v2, 0x3a98

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxy;->aOy:Z

    .line 175
    return-void
.end method

.method public final BC()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lbxy;->atG:Leqo;

    iget-object v1, p0, Lbxy;->aOB:Lepp;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 183
    iget-boolean v0, p0, Lbxy;->aOy:Z

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxy;->aOy:Z

    .line 187
    iget-object v0, p0, Lbxy;->atG:Leqo;

    iget-object v1, p0, Lbxy;->aOB:Lepp;

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 189
    :cond_0
    return-void
.end method

.method public final BD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lbxy;->aOA:Ljava/lang/String;

    return-object v0
.end method

.method public final BE()V
    .locals 2

    .prologue
    .line 221
    iget-boolean v0, p0, Lbxy;->aOy:Z

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    invoke-virtual {v0}, Lcom/google/android/handsfree/MessageBuffer;->Bw()Ljava/lang/String;

    move-result-object v0

    .line 223
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    invoke-direct {p0, v0}, Lbxy;->fP(Ljava/lang/String;)V

    .line 229
    :cond_0
    invoke-direct {p0}, Lbxy;->BG()V

    .line 230
    return-void
.end method

.method public final BF()V
    .locals 8

    .prologue
    .line 234
    iget-object v0, p0, Lbxy;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lbxy;->aOz:J

    sget v4, Lbxy;->aOv:I

    int-to-long v4, v4

    const-wide/32 v6, 0xea60

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lbxy;->aOA:Ljava/lang/String;

    .line 237
    iget-object v0, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    invoke-virtual {v0}, Lcom/google/android/handsfree/MessageBuffer;->clear()V

    .line 239
    :cond_0
    return-void

    .line 234
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Bx()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0}, Lbxy;->BG()V

    .line 206
    iget-object v0, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    iget-object v1, p0, Lbxy;->aOA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/handsfree/MessageBuffer;->fN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a(Lgii;I)V
    .locals 6

    .prologue
    .line 331
    iget-object v0, p0, Lbxy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 332
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 333
    const-wide/32 v4, 0x493e0

    invoke-virtual {p1, v3, v4, v5}, Lgii;->p(Ljava/lang/String;J)V

    .line 332
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_0
    return-void
.end method

.method public final fM(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 158
    iget-object v0, p0, Lbxy;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Lbxy;->aMF:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 159
    iput-object p1, p0, Lbxy;->aOA:Ljava/lang/String;

    .line 160
    iget-object v0, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/handsfree/MessageBuffer;->fM(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lbxy;->atG:Leqo;

    iget-object v1, p0, Lbxy;->aOB:Lepp;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 162
    iget-object v0, p0, Lbxy;->atG:Leqo;

    iget-object v1, p0, Lbxy;->aOB:Lepp;

    const-wide/16 v2, 0x3a98

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxy;->aOy:Z

    .line 164
    invoke-direct {p0}, Lbxy;->BG()V

    .line 165
    return-void
.end method

.method public final w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lbxy;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aau()Ldbh;

    move-result-object v0

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    .line 127
    iget-object v2, p0, Lbxy;->mMessageBuffer:Lcom/google/android/handsfree/MessageBuffer;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbxy;->aOy:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, p1, p2, v0}, Lcom/google/android/handsfree/MessageBuffer;->b(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    .line 139
    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lbxy;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    iput v0, p0, Lbxy;->aMF:I

    .line 145
    iget-object v0, p0, Lbxy;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 146
    invoke-direct {p0, p1}, Lbxy;->fP(Ljava/lang/String;)V

    .line 150
    :cond_0
    invoke-direct {p0}, Lbxy;->BG()V

    .line 151
    return-void

    :cond_1
    move v0, v1

    .line 127
    goto :goto_0
.end method
