.class public final Lgbf;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static E(Lizj;)Ljal;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lizj;->dSc:Ljal;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lizj;->dSc:Ljal;

    .line 171
    :goto_0
    return-object v0

    .line 164
    :cond_0
    iget-object v0, p0, Lizj;->dSt:Ljal;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lizj;->dSt:Ljal;

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lizj;->dTi:Ljal;

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lizj;->dTi:Ljal;

    goto :goto_0

    .line 168
    :cond_2
    iget-object v0, p0, Lizj;->dTj:Ljal;

    if-eqz v0, :cond_3

    .line 169
    iget-object v0, p0, Lizj;->dTj:Ljal;

    goto :goto_0

    .line 171
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static I(Lizj;)Ljava/lang/String;
    .locals 3
    .param p0    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 73
    if-eqz p0, :cond_1

    const/16 v1, 0x10

    new-array v2, v0, [I

    invoke-static {p0, v1, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_2

    .line 74
    :cond_1
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    :cond_2
    invoke-static {p0}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static J(Lizj;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 83
    invoke-static {p0}, Lgbf;->L(Lizj;)Ljbp;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljbp;->bfb()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {v0}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static K(Lizj;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 94
    invoke-static {p0}, Lgbf;->L(Lizj;)Ljbp;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static L(Lizj;)Ljbp;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lizj;->dSc:Ljal;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lizj;->dSc:Ljal;

    .line 120
    iget-object v1, v0, Ljal;->dWL:Ljak;

    if-eqz v1, :cond_0

    .line 121
    iget-object v0, v0, Ljal;->dWL:Ljak;

    .line 122
    iget-object v1, v0, Ljak;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 123
    iget-object v0, v0, Ljak;->aeB:Ljbp;

    .line 127
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljak;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Ljak;->clz:Ljcu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljak;->clz:Ljcu;

    iget-object v0, v0, Ljcu;->dME:Lixn;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Ljak;->clz:Ljcu;

    iget-object v0, v0, Ljcu;->dME:Lixn;

    .line 107
    iget-object v1, v0, Lixn;->dNR:Ljcn;

    if-eqz v1, :cond_0

    .line 108
    iget-object v0, v0, Lixn;->dNR:Ljcn;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljak;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 135
    iget-object v1, p0, Ljak;->dMX:[Ljbg;

    invoke-static {v1}, Lgbb;->a([Ljbg;)Ljbg;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljbg;->beU()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    invoke-virtual {v1}, Ljbg;->beT()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljbg;->beS()Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 138
    :cond_1
    invoke-virtual {v1}, Ljbg;->beP()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljbg;->beO()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljak;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 34
    .line 36
    if-eqz p1, :cond_7

    .line 38
    iget-object v0, p1, Ljak;->aeB:Ljbp;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljak;->aeB:Ljbp;

    move-object v3, v0

    .line 39
    :goto_0
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 44
    :goto_1
    iget-object v2, p1, Ljak;->clz:Ljcu;

    if-eqz v2, :cond_3

    iget-object v2, p1, Ljak;->clz:Ljcu;

    .line 45
    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljcu;->baV()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {v2}, Ljcu;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    .line 50
    :goto_3
    if-eqz v2, :cond_5

    move-object v0, v2

    .line 60
    :cond_0
    :goto_4
    if-eqz v0, :cond_6

    :goto_5
    return-object v0

    :cond_1
    move-object v3, v1

    .line 38
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 39
    goto :goto_1

    :cond_3
    move-object v2, v1

    .line 44
    goto :goto_2

    :cond_4
    move-object v2, v1

    .line 45
    goto :goto_3

    .line 52
    :cond_5
    if-nez v0, :cond_0

    .line 54
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljbp;->nH()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v3}, Ljbp;->nI()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 55
    const v0, 0x7f0a01e0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v3}, Ljbp;->mR()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v3}, Ljbp;->mS()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    goto :goto_4

    .line 60
    :cond_6
    const v0, 0x7f0a0171

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_7
    move-object v0, v1

    goto :goto_4
.end method
