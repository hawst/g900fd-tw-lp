.class final enum Lkck;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum eRO:Lkck;

.field public static final enum eRP:Lkck;

.field public static final enum eRQ:Lkck;

.field private static final synthetic eRR:[Lkck;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 409
    new-instance v0, Lkck;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lkck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkck;->eRO:Lkck;

    .line 410
    new-instance v0, Lkck;

    const-string v1, "SAME_CONNECTION"

    invoke-direct {v0, v1, v3}, Lkck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkck;->eRP:Lkck;

    .line 411
    new-instance v0, Lkck;

    const-string v1, "DIFFERENT_CONNECTION"

    invoke-direct {v0, v1, v4}, Lkck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkck;->eRQ:Lkck;

    .line 408
    const/4 v0, 0x3

    new-array v0, v0, [Lkck;

    sget-object v1, Lkck;->eRO:Lkck;

    aput-object v1, v0, v2

    sget-object v1, Lkck;->eRP:Lkck;

    aput-object v1, v0, v3

    sget-object v1, Lkck;->eRQ:Lkck;

    aput-object v1, v0, v4

    sput-object v0, Lkck;->eRR:[Lkck;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkck;
    .locals 1

    .prologue
    .line 408
    const-class v0, Lkck;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkck;

    return-object v0
.end method

.method public static values()[Lkck;
    .locals 1

    .prologue
    .line 408
    sget-object v0, Lkck;->eRR:[Lkck;

    invoke-virtual {v0}, [Lkck;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkck;

    return-object v0
.end method
