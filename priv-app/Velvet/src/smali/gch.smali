.class final Lgch;
.super Lepm;
.source "PG"


# instance fields
.field final synthetic cFE:Lgce;


# direct methods
.method public constructor <init>(Lgce;)V
    .locals 2

    .prologue
    .line 359
    iput-object p1, p0, Lgch;->cFE:Lgce;

    .line 360
    const-string v0, "ServerEndpointingTimeoutProcessor"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 361
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 365
    iget-object v0, p0, Lgch;->cFE:Lgce;

    iget-boolean v0, v0, Lgce;->aTn:Z

    if-eqz v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lgch;->cFE:Lgce;

    iget-object v0, v0, Lgce;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lgch;->cFE:Lgce;

    iget-wide v2, v2, Lgce;->cFA:J

    sub-long/2addr v0, v2

    .line 372
    iget-object v2, p0, Lgch;->cFE:Lgce;

    iget-wide v2, v2, Lgce;->cFy:J

    sub-long v0, v2, v0

    .line 373
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 374
    const-string v0, "DefaultRecognitionState"

    const-string v1, "Timed out waiting for server activity"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    new-instance v0, Leie;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Using the network for endpointing and have had no network response in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgch;->cFE:Lgce;

    iget-wide v2, v2, Lgce;->cFy:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x10018

    invoke-direct {v0, v1, v2}, Leie;-><init>(Ljava/lang/String;I)V

    .line 379
    iget-object v1, p0, Lgch;->cFE:Lgce;

    iget-object v1, v1, Lgce;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lgci;

    const-string v3, "Timed out waiting for server"

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-direct {v2, p0, v3, v4, v0}, Lgci;-><init>(Lgch;Ljava/lang/String;[ILeiq;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 387
    :cond_1
    iget-object v2, p0, Lgch;->cFE:Lgce;

    iget-object v3, p0, Lgch;->cFE:Lgce;

    iget-object v3, v3, Lgce;->aSr:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v4, 0xa

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, p0, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, v2, Lgce;->cFB:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method
