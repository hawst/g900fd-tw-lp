.class public final Leyx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cmi:Leyx;


# instance fields
.field private final cmj:Ljava/util/Map;

.field private final cmk:Ljava/util/Map;

.field private final cml:Lijj;

.field final cmm:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 39
    new-instance v0, Leyx;

    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v1

    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v2

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Leyx;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Collection;Z)V

    sput-object v0, Leyx;->cmi:Leyx;

    return-void
.end method

.method constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Collection;Z)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    iput-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    .line 60
    invoke-static {p2}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    iput-object v0, p0, Leyx;->cmk:Ljava/util/Map;

    .line 61
    invoke-static {p3}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Leyx;->cml:Lijj;

    .line 62
    iput-boolean p4, p0, Leyx;->cmm:Z

    .line 68
    iget-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Leyx;->cmk:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 69
    const-string v0, "CalendarMemoryStore"

    const-string v1, "CalendarMemoryStore maps have inconsistent sizes."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    return-void
.end method

.method static a(Lamm;)Z
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lamm;->nc()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lamm;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lamo;)Z
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lamo;->ni()Z

    move-result v0

    return v0
.end method

.method static a(Lamp;)Z
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lamp;->nv()Z

    move-result v0

    return v0
.end method

.method static a(Lamp;Lamp;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 225
    invoke-virtual {p0}, Lamp;->nw()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lamp;->afr:Lamq;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lamp;->nw()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lamp;->afr:Lamq;

    if-eqz v1, :cond_0

    .line 229
    iget-object v1, p0, Lamp;->afr:Lamq;

    .line 230
    iget-object v2, p1, Lamp;->afr:Lamq;

    .line 231
    invoke-virtual {v1}, Lamq;->mR()D

    move-result-wide v4

    invoke-virtual {v2}, Lamq;->mR()D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_0

    invoke-virtual {v1}, Lamq;->mS()D

    move-result-wide v4

    invoke-virtual {v2}, Lamq;->mS()D

    move-result-wide v2

    cmpl-double v1, v4, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 234
    :cond_0
    return v0
.end method

.method public static b(Lamm;)I
    .locals 4

    .prologue
    .line 299
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lamm;->nd()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lamm;->nb()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lamm;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lamm;->getId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static b(Lamo;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 247
    :try_start_0
    const-string v0, "SHA1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 253
    invoke-virtual {p0}, Lamo;->nh()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0}, Lamo;->nj()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lamo;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lamo;->nk()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lamo;->nm()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p0}, Lamo;->no()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lamo;->nr()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0}, Lamo;->ns()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p0}, Lamo;->nt()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lijj;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    .line 263
    const-string v1, "|"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 264
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 266
    invoke-virtual {v9}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 248
    :catch_0
    move-exception v0

    .line 249
    const-string v1, "CalendarMemoryStore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find \'SHA1\' message digest: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Lamk;)Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lamk;->aeQ:Lamo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamk;->aeQ:Lamo;

    invoke-virtual {v0}, Lamo;->ni()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamk;->aeR:Lamp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lamk;->aeR:Lamp;

    invoke-virtual {v0}, Lamp;->nv()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(Lamo;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 280
    :try_start_0
    const-string v0, "SHA1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 286
    invoke-virtual {p0}, Lamo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lamo;->nk()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lamo;->nm()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lamo;->no()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lijj;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v1

    .line 291
    const-string v2, "|"

    invoke-static {v2, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 292
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 294
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    const-string v1, "CalendarMemoryStore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find \'SHA1\' message digest: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Leyx;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 151
    invoke-virtual {p0, p1, p2}, Leyx;->bj(J)Lamk;

    move-result-object v2

    .line 152
    if-nez v2, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-object v0

    .line 156
    :cond_1
    iget-object v1, v2, Lamk;->aeS:Lamn;

    .line 159
    if-eqz v1, :cond_4

    if-eqz p4, :cond_2

    invoke-virtual {v1}, Lamn;->nf()Z

    move-result v3

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v3, v4, :cond_4

    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {v1}, Lamn;->ne()Z

    move-result v3

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v3, v4, :cond_4

    :cond_3
    if-eqz p5, :cond_0

    invoke-virtual {v1}, Lamn;->ng()Z

    move-result v3

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eq v3, v4, :cond_0

    .line 167
    :cond_4
    iget-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    invoke-static {v0}, Lior;->t(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v3

    .line 168
    iget-object v0, p0, Leyx;->cmk:Ljava/util/Map;

    invoke-static {v0}, Lior;->t(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v4

    .line 171
    new-instance v0, Lamn;

    invoke-direct {v0}, Lamn;-><init>()V

    .line 172
    if-eqz v1, :cond_8

    .line 173
    invoke-static {v1, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamn;

    move-object v1, v0

    .line 176
    :goto_1
    if-eqz p4, :cond_5

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lamn;->aH(Z)Lamn;

    .line 177
    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lamn;->aG(Z)Lamn;

    .line 178
    :cond_6
    if-eqz p5, :cond_7

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lamn;->aI(Z)Lamn;

    .line 181
    :cond_7
    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    invoke-static {v2, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    .line 182
    iput-object v1, v0, Lamk;->aeS:Lamn;

    .line 184
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    iget-object v1, v0, Lamk;->aeR:Lamp;

    invoke-virtual {v1}, Lamp;->nu()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    new-instance v0, Leyx;

    iget-object v1, p0, Leyx;->cml:Lijj;

    iget-boolean v2, p0, Leyx;->cmm:Z

    invoke-direct {v0, v3, v4, v1, v2}, Leyx;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Collection;Z)V

    goto/16 :goto_0

    :cond_8
    move-object v1, v0

    goto :goto_1
.end method

.method public final awH()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Leyx;->cml:Lijj;

    return-object v0
.end method

.method public final awI()Leyz;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Leyz;

    invoke-direct {v0, p0}, Leyz;-><init>(Leyx;)V

    return-object v0
.end method

.method final awJ()Leza;
    .locals 1

    .prologue
    .line 131
    new-instance v0, Leza;

    invoke-direct {v0, p0}, Leza;-><init>(Leyx;)V

    return-object v0
.end method

.method final awK()Ljava/util/Set;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final awx()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Leyx;->cmm:Z

    return v0
.end method

.method public final bj(J)Lamk;
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    return-object v0
.end method

.method public final lF(Ljava/lang/String;)Lamk;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Leyx;->cmk:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method final values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Leyx;->cmj:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
