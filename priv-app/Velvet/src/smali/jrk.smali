.class public final Ljrk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eAE:[Ljrk;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field public eAF:Ljkh;

.field private eqd:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2413
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2414
    iput v0, p0, Ljrk;->aez:I

    iput-object v1, p0, Ljrk;->eAF:Ljkh;

    iput v0, p0, Ljrk;->eqd:I

    const-string v0, ""

    iput-object v0, p0, Ljrk;->akf:Ljava/lang/String;

    iput-object v1, p0, Ljrk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrk;->eCz:I

    .line 2415
    return-void
.end method

.method public static btn()[Ljrk;
    .locals 2

    .prologue
    .line 2356
    sget-object v0, Ljrk;->eAE:[Ljrk;

    if-nez v0, :cond_1

    .line 2357
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2359
    :try_start_0
    sget-object v0, Ljrk;->eAE:[Ljrk;

    if-nez v0, :cond_0

    .line 2360
    const/4 v0, 0x0

    new-array v0, v0, [Ljrk;

    sput-object v0, Ljrk;->eAE:[Ljrk;

    .line 2362
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2364
    :cond_1
    sget-object v0, Ljrk;->eAE:[Ljrk;

    return-object v0

    .line 2362
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2350
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljrk;->eAF:Ljkh;

    if-nez v0, :cond_1

    new-instance v0, Ljkh;

    invoke-direct {v0}, Ljkh;-><init>()V

    iput-object v0, p0, Ljrk;->eAF:Ljkh;

    :cond_1
    iget-object v0, p0, Ljrk;->eAF:Ljkh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljrk;->eqd:I

    iget v0, p0, Ljrk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljrk;->akf:Ljava/lang/String;

    iget v0, p0, Ljrk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2430
    iget-object v0, p0, Ljrk;->eAF:Ljkh;

    if-eqz v0, :cond_0

    .line 2431
    const/4 v0, 0x1

    iget-object v1, p0, Ljrk;->eAF:Ljkh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2433
    :cond_0
    iget v0, p0, Ljrk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 2434
    const/4 v0, 0x2

    iget v1, p0, Ljrk;->eqd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2436
    :cond_1
    iget v0, p0, Ljrk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 2437
    const/4 v0, 0x3

    iget-object v1, p0, Ljrk;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2439
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2440
    return-void
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2394
    iget-object v0, p0, Ljrk;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2444
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2445
    iget-object v1, p0, Ljrk;->eAF:Ljkh;

    if-eqz v1, :cond_0

    .line 2446
    const/4 v1, 0x1

    iget-object v2, p0, Ljrk;->eAF:Ljkh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2449
    :cond_0
    iget v1, p0, Ljrk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 2450
    const/4 v1, 0x2

    iget v2, p0, Ljrk;->eqd:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2453
    :cond_1
    iget v1, p0, Ljrk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 2454
    const/4 v1, 0x3

    iget-object v2, p0, Ljrk;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2457
    :cond_2
    return v0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 2405
    iget v0, p0, Ljrk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
