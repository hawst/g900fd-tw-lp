.class public final Lcnn;
.super Lepk;
.source "PG"


# instance fields
.field private mContactLogger:Lcnk;

.field private final mContactLoggerConfig:Lcnm;

.field private final mGsaPreferenceController:Lchr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchr;Lcfo;Lcnm;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lcnm;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 38
    const-string v0, "ContactLoggerTask"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, v1}, Lepk;-><init>(Ljava/lang/String;[I)V

    .line 39
    iput-object p2, p0, Lcnn;->mGsaPreferenceController:Lchr;

    .line 40
    iput-object p4, p0, Lcnn;->mContactLoggerConfig:Lcnm;

    .line 41
    new-instance v0, Lcnk;

    invoke-direct {v0, p1, p2, p3}, Lcnk;-><init>(Landroid/content/Context;Lchr;Lcfo;)V

    iput-object v0, p0, Lcnn;->mContactLogger:Lcnk;

    .line 42
    return-void
.end method

.method public static a(Lchr;Lcfo;JZ)Lcnm;
    .locals 8
    .param p0    # Lchr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 55
    new-instance v0, Lcnm;

    invoke-direct {v0}, Lcnm;-><init>()V

    invoke-virtual {v0, p2, p3}, Lcnm;->ab(J)Lcnm;

    move-result-object v1

    .line 58
    invoke-static {p1}, Lcnn;->a(Lcfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcnm;->cv(Z)Lcnm;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-virtual {v1}, Lcnm;->getStartTime()J

    move-result-wide v2

    const-string v4, "KEY_CONTACTS_LOGGER_TIMESTAMP"

    const-wide/16 v6, 0x0

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x414b774000000000L    # 3600000.0

    div-double/2addr v2, v4

    invoke-virtual {p1}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->FN()I

    move-result v0

    int-to-long v4, v0

    long-to-double v4, v4

    cmpg-double v0, v2, v4

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcnm;->cv(Z)Lcnm;

    move-result-object v0

    goto :goto_0

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 66
    :cond_2
    invoke-virtual {p0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-virtual {v1}, Lcnm;->getStartTime()J

    move-result-wide v2

    const-string v4, "KEY_CONTACTS_LOGGER_TIMESTAMP"

    const-wide/16 v6, 0x0

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x414b774000000000L    # 3600000.0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4038000000000000L    # 24.0

    div-double/2addr v2, v4

    invoke-virtual {p1}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->FO()I

    move-result v0

    int-to-long v4, v0

    long-to-double v4, v4

    cmpl-double v0, v2, v4

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    .line 67
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcnm;->cv(Z)Lcnm;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcnm;->cw(Z)Lcnm;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 70
    :cond_4
    if-nez p4, :cond_5

    .line 72
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcnm;->cv(Z)Lcnm;

    move-result-object v0

    goto :goto_0

    .line 78
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcnm;->cv(Z)Lcnm;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcnm;->cw(Z)Lcnm;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcfo;)Z
    .locals 4
    .param p0    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->FL()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    const-string v1, "ContactLoggerTask"

    const-string v2, "canRun() : Disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :goto_0
    return v0

    .line 88
    :cond_0
    invoke-virtual {p0}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-interface {v1}, Lcke;->CS()Z

    move-result v1

    if-nez v1, :cond_1

    .line 89
    const-string v1, "ContactLoggerTask"

    const-string v2, "canRun() : Opted Out"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcfo;->DL()Lcrh;

    move-result-object v1

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    const-string v1, "ContactLoggerTask"

    const-string v2, "canRun() : No Account"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 100
    :cond_2
    const-string v0, "ContactLoggerTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "canRun() : Account = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcnk;)Lcnn;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcnn;->mContactLogger:Lcnk;

    .line 47
    return-object p0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcnn;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final ub()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 147
    const-string v0, "ContactLoggerTask"

    const-string v1, "call()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcnn;->mContactLogger:Lcnk;

    iget-object v1, p0, Lcnn;->mContactLoggerConfig:Lcnm;

    invoke-virtual {v0, v1}, Lcnk;->a(Lcnm;)Z

    move-result v0

    .line 149
    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcnn;->mGsaPreferenceController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "KEY_CONTACTS_LOGGER_TIMESTAMP"

    iget-object v2, p0, Lcnn;->mContactLoggerConfig:Lcnm;

    invoke-virtual {v2}, Lcnm;->getStartTime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 156
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
