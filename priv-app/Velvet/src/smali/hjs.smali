.class public abstract Lhjs;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lelb;


# instance fields
.field private TAG:Ljava/lang/String;

.field protected cXh:Lhjo;

.field public diO:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClassicAbstractCardView."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhjs;->TAG:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public final aQe()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lhjs;->cXh:Lhjo;

    invoke-virtual {v0}, Lhjo;->aQe()V

    .line 69
    return-void
.end method

.method public final aQg()Lhjo;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lhjs;->cXh:Lhjo;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjo;

    return-object v0
.end method

.method public aQh()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lhjs;->cXh:Lhjo;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lhjs;->cXh:Lhjo;

    invoke-virtual {v0, p0}, Lhjo;->a(Lelb;)V

    .line 55
    :cond_0
    return-void
.end method

.method public final aQi()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjs;->diO:Z

    .line 88
    return-void
.end method

.method public final aQj()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lhjs;->diO:Z

    return v0
.end method

.method public auj()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public final b(Lhjo;)V
    .locals 2

    .prologue
    .line 40
    iput-object p1, p0, Lhjs;->cXh:Lhjo;

    .line 41
    invoke-virtual {p0}, Lhjs;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lhjs;->TAG:Ljava/lang/String;

    const-string v1, "#handleAttach - setController"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {p0}, Lhjs;->aQh()V

    .line 45
    :cond_0
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    .line 105
    if-eqz p1, :cond_0

    .line 106
    invoke-virtual {p0}, Lhjs;->aQg()Lhjo;

    move-result-object v0

    invoke-virtual {v0}, Lhjo;->anO()Z

    .line 108
    :cond_0
    return-void
.end method

.method public p(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public uM()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lhjs;->cXh:Lhjo;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lhjs;->cXh:Lhjo;

    invoke-virtual {v0}, Lhjo;->detach()V

    .line 64
    :cond_0
    return-void
.end method
