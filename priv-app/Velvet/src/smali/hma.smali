.class final Lhma;
.super Lepp;
.source "PG"


# instance fields
.field private synthetic dkJ:Lcom/google/android/search/shared/actions/MediaControlAction;

.field private synthetic dkK:Landroid/media/RemoteController;

.field private synthetic dkL:Landroid/media/AudioManager;


# direct methods
.method constructor <init>(Lhlz;Ljava/lang/String;Lcom/google/android/search/shared/actions/MediaControlAction;Landroid/media/RemoteController;Landroid/media/AudioManager;)V
    .locals 0

    .prologue
    .line 75
    iput-object p3, p0, Lhma;->dkJ:Lcom/google/android/search/shared/actions/MediaControlAction;

    iput-object p4, p0, Lhma;->dkK:Landroid/media/RemoteController;

    iput-object p5, p0, Lhma;->dkL:Landroid/media/AudioManager;

    invoke-direct {p0, p2}, Lepp;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 78
    iget-object v0, p0, Lhma;->dkJ:Lcom/google/android/search/shared/actions/MediaControlAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/MediaControlAction;->ahB()I

    move-result v0

    .line 79
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 80
    iget-object v1, p0, Lhma;->dkK:Landroid/media/RemoteController;

    if-nez v1, :cond_0

    .line 90
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, Lhma;->dkK:Landroid/media/RemoteController;

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v3, v0}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 85
    iget-object v1, p0, Lhma;->dkK:Landroid/media/RemoteController;

    new-instance v2, Landroid/view/KeyEvent;

    const/4 v3, 0x1

    invoke-direct {v2, v3, v0}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    .line 86
    iget-object v0, p0, Lhma;->dkL:Landroid/media/AudioManager;

    iget-object v1, p0, Lhma;->dkK:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteController(Landroid/media/RemoteController;)V

    goto :goto_0

    .line 88
    :cond_1
    const-string v0, "MediaControlActionExecutor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhma;->dkJ:Lcom/google/android/search/shared/actions/MediaControlAction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
