.class public final Lcxw;
.super Lcvc;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private final blx:Z

.field private final bly:Z

.field private final bmb:Lcha;

.field private final mActivity:Landroid/app/Activity;

.field private final mBackgroundTasks:Lgpp;

.field private final mCardsPrefs:Lcxs;

.field private final mCoreSearchServices:Lcfo;

.field private final mFlags:Lchk;

.field private final mGsaPreferenceController:Lchr;

.field private final mLocationSettings:Lcob;

.field private final mLoginHelper:Lcrh;

.field private final mSearchConfig:Lcjs;

.field private final mSettings:Lcke;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcfo;Landroid/app/Activity;Lchr;ZZ)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcvc;-><init>()V

    .line 67
    iput-object p1, p0, Lcxw;->mCoreSearchServices:Lcfo;

    .line 68
    invoke-virtual {p1}, Lcfo;->DD()Lcjs;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mSearchConfig:Lcjs;

    .line 69
    invoke-virtual {p1}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mSettings:Lcke;

    .line 70
    invoke-virtual {p1}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mFlags:Lchk;

    .line 71
    invoke-virtual {p1}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mLoginHelper:Lcrh;

    .line 72
    invoke-virtual {p1}, Lcfo;->DI()Lcpn;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mUrlHelper:Lcpn;

    .line 73
    iput-object p2, p0, Lcxw;->mActivity:Landroid/app/Activity;

    .line 74
    invoke-virtual {p1}, Lcfo;->DP()Lcob;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mLocationSettings:Lcob;

    .line 75
    invoke-virtual {p1}, Lcfo;->DO()Lgpp;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mBackgroundTasks:Lgpp;

    .line 76
    iget-object v0, p0, Lcxw;->mSettings:Lcke;

    invoke-interface {v0, p0}, Lcke;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 77
    iget-object v0, p0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    iput-object v0, p0, Lcxw;->mCardsPrefs:Lcxs;

    .line 78
    iget-object v0, p0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Em()Lcha;

    move-result-object v0

    iput-object v0, p0, Lcxw;->bmb:Lcha;

    .line 79
    iput-object p3, p0, Lcxw;->mGsaPreferenceController:Lchr;

    .line 80
    iput-boolean p4, p0, Lcxw;->blx:Z

    .line 81
    iput-boolean p5, p0, Lcxw;->bly:Z

    .line 82
    return-void
.end method


# virtual methods
.method protected final e(Landroid/preference/Preference;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 97
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 108
    :cond_0
    :goto_0
    return-object v0

    .line 98
    :cond_1
    const-string v1, "debug_search_domain_override"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debug_search_scheme_override"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debug_search_host_param"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debug_js_injection_enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debug_js_server_address"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "debug_icing_extensive_logging_enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 104
    :cond_2
    const-string v0, "debug_search_controller"

    goto :goto_0

    .line 105
    :cond_3
    invoke-static {v0}, Lcvy;->ea(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106
    const-string v0, "account_settings_controller"

    goto :goto_0
.end method

.method protected final f(Landroid/preference/Preference;)Lcxt;
    .locals 19

    .prologue
    .line 113
    invoke-virtual/range {p0 .. p1}, Lcxw;->e(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v1

    .line 115
    const-string v2, "google_location_settings"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    new-instance v1, Lcxa;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcxw;->mLocationSettings:Lcob;

    invoke-direct {v1, v2, v3, v4}, Lcxa;-><init>(Lcke;Landroid/app/Activity;Lcob;)V

    .line 168
    :goto_0
    return-object v1

    .line 117
    :cond_0
    const-string v2, "account_settings_controller"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 118
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v13

    .line 119
    invoke-virtual {v13}, Lgql;->aJq()Lhhq;

    move-result-object v1

    iget-object v0, v1, Lhhq;->mSettings:Lhym;

    move-object/from16 v17, v0

    .line 121
    move-object/from16 v0, p0

    iget-object v1, v0, Lcxw;->mActivity:Landroid/app/Activity;

    instance-of v1, v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v1, :cond_1

    .line 122
    move-object/from16 v0, p0

    iget-object v1, v0, Lcxw;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->wr()Leoj;

    move-result-object v1

    .line 126
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mActivity:Landroid/app/Activity;

    invoke-virtual/range {v17 .. v17}, Lhym;->aFc()Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v2, v0, v1}, Lcss;->a(Landroid/content/Context;Lhym;Leoj;)Lcsq;

    move-result-object v18

    .line 128
    new-instance v1, Lcvy;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcxw;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->DI()Lcpn;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->DN()Landroid/database/DataSetObservable;

    move-result-object v7

    invoke-virtual {v13}, Lgql;->aJr()Lfdb;

    move-result-object v8

    invoke-virtual {v8}, Lfdb;->axI()Lfcx;

    move-result-object v8

    new-instance v9, Leon;

    invoke-direct {v9}, Leon;-><init>()V

    invoke-virtual {v13}, Lgql;->aJR()Lcgh;

    move-result-object v10

    invoke-interface {v10}, Lcgh;->EU()Lcrr;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v11}, Lcfo;->DP()Lcob;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v12}, Lcfo;->En()Leue;

    move-result-object v12

    iget-object v13, v13, Lgql;->mAsyncServices:Lema;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v14}, Lcfo;->DB()Landroid/accounts/AccountManager;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcxw;->blx:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcxw;->bly:Z

    move/from16 v16, v0

    invoke-direct/range {v1 .. v18}, Lcvy;-><init>(Lcke;Lcrh;Lchk;Landroid/app/Activity;Lcpn;Landroid/database/DataSetObservable;Lfcx;Leom;Lcrr;Lcob;Leue;Lerp;Landroid/accounts/AccountManager;ZZLhym;Lcsq;)V

    goto/16 :goto_0

    .line 124
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 141
    :cond_2
    const-string v2, "commute_sharing"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 142
    new-instance v1, Lcfk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mCardsPrefs:Lcxs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DF()Lcin;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcfk;-><init>(Lcxs;Lcin;)V

    goto/16 :goto_0

    .line 144
    :cond_3
    const-string v2, "use_google_com"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 145
    new-instance v1, Lcys;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mSearchConfig:Lcjs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcxw;->mUrlHelper:Lcpn;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcxw;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2, v3, v4, v5}, Lcys;-><init>(Lcjs;Lcke;Lcpn;Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 147
    :cond_4
    const-string v2, "tos"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 148
    new-instance v1, Lcyq;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mSearchConfig:Lcjs;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcxw;->mUrlHelper:Lcpn;

    invoke-direct {v1, v2, v3, v4}, Lcyq;-><init>(Lcke;Lcjs;Lcpn;)V

    goto/16 :goto_0

    .line 149
    :cond_5
    const-string v2, "location_tos"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 150
    new-instance v1, Lcxb;

    invoke-direct {v1}, Lcxb;-><init>()V

    goto/16 :goto_0

    .line 151
    :cond_6
    const-string v2, "personalized_search_bool"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 152
    new-instance v1, Lcxr;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mSearchConfig:Lcjs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mLoginHelper:Lcrh;

    invoke-direct {v1, v2, v3}, Lcxr;-><init>(Lcjs;Lcrh;)V

    goto/16 :goto_0

    .line 153
    :cond_7
    const-string v2, "safe_search"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 154
    new-instance v1, Lcxx;

    invoke-direct {v1}, Lcxx;-><init>()V

    goto/16 :goto_0

    .line 155
    :cond_8
    const-string v2, "debug_search_controller"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 156
    new-instance v1, Lcvp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mSearchConfig:Lcjs;

    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcvp;-><init>(Lcke;Lcjs;Lckw;)V

    goto/16 :goto_0

    .line 157
    :cond_9
    const-string v2, "icing_manage_storage"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "icing_privacy_settings"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 158
    :cond_a
    new-instance v1, Lcwx;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->bmb:Lcha;

    invoke-direct {v1, v2}, Lcwx;-><init>(Lcha;)V

    goto/16 :goto_0

    .line 159
    :cond_b
    const-string v2, "contact_upload_for_communication_actions_enabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 160
    new-instance v1, Lcve;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mFlags:Lchk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcxw;->mLoginHelper:Lcrh;

    invoke-direct {v1, v2, v3, v4}, Lcve;-><init>(Lchk;Lcke;Lcrh;)V

    goto/16 :goto_0

    .line 162
    :cond_c
    const-string v2, "gel_usage_stats"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 163
    new-instance v1, Lcvx;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcxw;->mFlags:Lchk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcxw;->mGsaPreferenceController:Lchr;

    invoke-direct {v1, v2, v3, v4}, Lcvx;-><init>(Landroid/content/Context;Lchk;Lchr;)V

    goto/16 :goto_0

    .line 165
    :cond_d
    const-string v2, "high_contrast_mode_enabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 166
    new-instance v1, Lcvs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcxw;->mSettings:Lcke;

    invoke-direct {v1, v2}, Lcvs;-><init>(Lcke;)V

    goto/16 :goto_0

    .line 168
    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcxw;->mSettings:Lcke;

    invoke-interface {v0, p0}, Lcke;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 87
    invoke-super {p0}, Lcvc;->onDestroy()V

    .line 88
    return-void
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 173
    const-string v0, "use_google_com"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcxw;->mBackgroundTasks:Lgpp;

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    const-string v0, "contact_upload_for_communication_actions_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179
    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcxw;->mBackgroundTasks:Lgpp;

    const-string v1, "log_contact_accounts_to_clearcut"

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    goto :goto_0

    .line 183
    :cond_2
    const-string v0, "high_contrast_mode_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcxw;->mBackgroundTasks:Lgpp;

    const-string v1, "sync_gel_prefs"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    goto :goto_0
.end method
