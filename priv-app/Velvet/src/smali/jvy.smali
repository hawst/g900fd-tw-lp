.class public final Ljvy;
.super Ljsl;
.source "PG"


# instance fields
.field public eIu:[Lkmq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 36
    invoke-static {}, Lkmq;->bCn()[Lkmq;

    move-result-object v0

    iput-object v0, p0, Ljvy;->eIu:[Lkmq;

    const/4 v0, 0x0

    iput-object v0, p0, Ljvy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvy;->eCz:I

    .line 37
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvy;->eIu:[Lkmq;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lkmq;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljvy;->eIu:[Lkmq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lkmq;

    invoke-direct {v3}, Lkmq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljvy;->eIu:[Lkmq;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lkmq;

    invoke-direct {v3}, Lkmq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvy;->eIu:[Lkmq;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Ljvy;->eIu:[Lkmq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljvy;->eIu:[Lkmq;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 50
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljvy;->eIu:[Lkmq;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 51
    iget-object v1, p0, Ljvy;->eIu:[Lkmq;

    aget-object v1, v1, v0

    .line 52
    if-eqz v1, :cond_0

    .line 53
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 50
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 58
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 62
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 63
    iget-object v0, p0, Ljvy;->eIu:[Lkmq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljvy;->eIu:[Lkmq;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 64
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljvy;->eIu:[Lkmq;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 65
    iget-object v2, p0, Ljvy;->eIu:[Lkmq;

    aget-object v2, v2, v0

    .line 66
    if-eqz v2, :cond_0

    .line 67
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 64
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_1
    return v1
.end method
