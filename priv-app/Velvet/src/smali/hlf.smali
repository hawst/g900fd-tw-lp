.class public Lhlf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bbd:Lenw;

.field public bbe:Ljava/lang/String;

.field private diT:Lglu;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public diX:Z

.field private djU:Lhlg;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field djc:Lcmk;

.field final mClock:Lemp;

.field private final mCookies:Lgpf;

.field final mGsaConfigFlags:Lchk;

.field private final mSdchManager:Lczz;

.field private final mSearchUrlHelper:Lcpn;

.field private final mStaticContentCache:Lcpq;

.field public final mVss:Lhhq;


# direct methods
.method public constructor <init>(Lhhq;Lemp;Lcpn;Lchk;Lczz;Lgpf;Lcpq;)V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Lhlf;->mVss:Lhhq;

    .line 129
    iput-object p2, p0, Lhlf;->mClock:Lemp;

    .line 130
    iput-object p3, p0, Lhlf;->mSearchUrlHelper:Lcpn;

    .line 131
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lhlf;->bbd:Lenw;

    .line 132
    iput-object p4, p0, Lhlf;->mGsaConfigFlags:Lchk;

    .line 133
    iput-object p5, p0, Lhlf;->mSdchManager:Lczz;

    .line 134
    iput-object p6, p0, Lhlf;->mCookies:Lgpf;

    .line 135
    iput-object p7, p0, Lhlf;->mStaticContentCache:Lcpq;

    .line 136
    return-void
.end method

.method private a(Lgfc;Lcom/google/android/shared/search/Query;Z)Lgnj;
    .locals 9
    .param p1    # Lgfc;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x9

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 309
    .line 310
    new-instance v6, Lgmy;

    invoke-direct {v6}, Lgmy;-><init>()V

    .line 311
    if-eqz p1, :cond_5

    .line 312
    invoke-virtual {p1}, Lgfc;->asV()[B

    move-result-object v0

    iput-object v0, v6, Lgmy;->mAudio:[B

    .line 313
    invoke-virtual {p1}, Lgfc;->getSampleRate()I

    move-result v0

    iput v0, v6, Lgmy;->cPK:I

    .line 314
    invoke-static {p1}, Lgfd;->a(Lgfc;)Lgff;

    move-result-object v0

    invoke-virtual {v0}, Lgff;->aFB()I

    move-result v0

    iput v0, v6, Lgmy;->cPI:I

    move v0, v1

    .line 333
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqd()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aTE()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-virtual {v3}, Lchk;->HQ()Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v2

    :goto_1
    if-eqz v3, :cond_0

    .line 334
    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v3}, Lhhq;->aPh()Lgfb;

    move-result-object v3

    invoke-interface {v3}, Lgfb;->aFy()Lgfc;

    move-result-object v3

    .line 335
    if-eqz v3, :cond_0

    .line 337
    iput v4, v6, Lgmy;->cPJ:I

    .line 338
    invoke-virtual {v3}, Lgfc;->getSampleRate()I

    move-result v7

    iput v7, v6, Lgmy;->cPL:I

    .line 339
    invoke-virtual {v3}, Lgfc;->asV()[B

    move-result-object v3

    iput-object v3, v6, Lgmy;->cPN:[B

    .line 343
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqG()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 344
    iput-boolean v1, v6, Lgmy;->cHB:Z

    .line 347
    :cond_1
    new-instance v7, Lgnk;

    invoke-direct {v7}, Lgnk;-><init>()V

    .line 348
    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v8, v3, Lhhq;->mSettings:Lhym;

    .line 352
    invoke-virtual {v8}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v7, Lgnk;->cQb:Ljava/lang/String;

    invoke-virtual {v8}, Lhym;->aTI()Ljava/util/List;

    move-result-object v3

    iput-object v3, v7, Lgnk;->cQc:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqe()Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v3, Lgjo;->cMK:Lgjo;

    :goto_2
    iput-object v3, v7, Lgnk;->cQe:Lgjo;

    sget-object v3, Lgjl;->cMB:Lgjl;

    iput-object v3, v7, Lgnk;->cOM:Lgjl;

    if-eqz p1, :cond_a

    move v3, v2

    :goto_3
    iput-boolean v3, v7, Lgnk;->cQa:Z

    new-instance v3, Ljtp;

    invoke-direct {v3}, Ljtp;-><init>()V

    iput-object v3, v7, Lgnk;->cPw:Ljtp;

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v3

    if-eqz v3, :cond_b

    move v3, v4

    :goto_4
    invoke-virtual {v7, v3}, Lgnk;->kt(I)Lgnk;

    move-result-object v4

    invoke-virtual {v8}, Lhym;->aHP()Z

    move-result v3

    iput-boolean v3, v4, Lgnk;->cPu:Z

    invoke-virtual {v6}, Lgmy;->aHB()Lgmx;

    move-result-object v3

    iput-object v3, v4, Lgnk;->cPY:Lgmx;

    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-virtual {v3}, Lchk;->Hq()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_2
    move v3, v2

    :goto_5
    iput-boolean v3, v4, Lgnk;->cFx:Z

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lgnk;->mRequestId:Ljava/lang/String;

    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-virtual {v3}, Lchk;->FH()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v3

    if-nez v3, :cond_d

    :goto_6
    iput-boolean v2, v4, Lgnk;->aok:Z

    invoke-static {}, Ldmh;->YZ()Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "handsfree"

    :goto_7
    iput-object v1, v4, Lgnk;->axT:Ljava/lang/String;

    iput-boolean v0, v4, Lgnk;->cQf:Z

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqe()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->YP()Ljava/lang/String;

    move-result-object v5

    :cond_3
    iput-object v5, v4, Lgnk;->cQd:Ljava/lang/String;

    .line 376
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->Mb()Ljava/lang/String;

    move-result-object v0

    .line 378
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 379
    iput-object v0, v7, Lgnk;->aTr:Ljava/lang/String;

    .line 382
    :cond_4
    invoke-virtual {v7}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    return-object v0

    .line 317
    :cond_5
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqY()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 318
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqY()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v6, Lgmy;->cPM:Landroid/net/Uri;

    move v0, v1

    .line 319
    goto/16 :goto_0

    .line 320
    :cond_6
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Hl()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTN()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqb()Z

    move-result v0

    if-nez v0, :cond_7

    .line 324
    iput v4, v6, Lgmy;->cPI:I

    .line 325
    const/16 v0, 0x3e80

    iput v0, v6, Lgmy;->cPK:I

    move v0, v2

    goto/16 :goto_0

    .line 326
    :cond_7
    if-eqz p3, :cond_f

    .line 327
    iput-boolean v2, v6, Lgmy;->cPO:Z

    .line 328
    iput-boolean v2, v6, Lgmy;->cPF:Z

    .line 329
    iput-boolean v2, v6, Lgmy;->cPG:Z

    move v0, v1

    .line 330
    goto/16 :goto_0

    :cond_8
    move v3, v1

    .line 333
    goto/16 :goto_1

    .line 352
    :cond_9
    sget-object v3, Lgjo;->cMP:Lgjo;

    goto/16 :goto_2

    :cond_a
    move v3, v1

    goto/16 :goto_3

    :cond_b
    const/4 v3, 0x2

    goto/16 :goto_4

    :cond_c
    move v3, v1

    goto/16 :goto_5

    :cond_d
    move v2, v1

    goto/16 :goto_6

    :cond_e
    move-object v1, v5

    goto :goto_7

    :cond_f
    move v0, v2

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/shared/search/Query;Lhli;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 258
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhlf;->bbe:Ljava/lang/String;

    .line 259
    iput-boolean v6, p0, Lhlf;->diX:Z

    .line 260
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFf()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aAq()Lgno;

    move-result-object v0

    invoke-virtual {v0}, Lgno;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFg()Z

    move-result v0

    if-nez v0, :cond_1

    .line 263
    :cond_0
    invoke-virtual {p0, p2, p1, v5}, Lhlf;->a(Lhli;Lcom/google/android/shared/search/Query;Z)V

    .line 281
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqH()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269
    new-instance v0, Lhlg;

    invoke-direct {v0, p0, p2, p1}, Lhlg;-><init>(Lhlf;Lhli;Lcom/google/android/shared/search/Query;)V

    iput-object v0, p0, Lhlf;->djU:Lhlg;

    .line 270
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPk()Lgkg;

    move-result-object v0

    iget-object v1, p0, Lhlf;->djU:Lhlg;

    iget-object v2, p0, Lhlf;->mVss:Lhhq;

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Lgjl;

    sget-object v4, Lgjl;->cMB:Lgjl;

    aput-object v4, v3, v5

    sget-object v4, Lgjl;->cMC:Lgjl;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lgkg;->a(Lefk;Ljava/lang/String;[Lgjl;)V

    .line 276
    invoke-interface {p2}, Lhli;->Nq()V

    goto :goto_0

    .line 278
    :cond_2
    new-instance v0, Leie;

    const v1, 0x1001a

    invoke-direct {v0, v1}, Leie;-><init>(I)V

    invoke-virtual {p0, p1, p2, v0}, Lhlf;->a(Lcom/google/android/shared/search/Query;Lhli;Leiq;)V

    goto :goto_0
.end method


# virtual methods
.method public final I(ZZ)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 184
    iget-boolean v0, p0, Lhlf;->diX:Z

    if-eqz v0, :cond_0

    .line 187
    const/16 v0, 0x12

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lhlf;->bbe:Ljava/lang/String;

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 191
    invoke-virtual {p0, p1, p2}, Lhlf;->J(ZZ)V

    .line 193
    :cond_0
    return-void
.end method

.method final J(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 204
    iget-object v0, p0, Lhlf;->diT:Lglu;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lhlf;->diT:Lglu;

    invoke-virtual {v0}, Lglu;->invalidate()V

    .line 206
    iput-object v2, p0, Lhlf;->diT:Lglu;

    .line 208
    :cond_0
    iget-object v0, p0, Lhlf;->djc:Lcmk;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 209
    iget-object v0, p0, Lhlf;->djc:Lcmk;

    invoke-virtual {v0}, Lcmk;->cancel()V

    .line 211
    :cond_1
    iput-object v2, p0, Lhlf;->djc:Lcmk;

    .line 212
    iget-object v0, p0, Lhlf;->djU:Lhlg;

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPk()Lgkg;

    move-result-object v0

    iget-object v1, p0, Lhlf;->djU:Lhlg;

    invoke-virtual {v0, v1}, Lgkg;->c(Lefk;)V

    .line 214
    iput-object v2, p0, Lhlf;->djU:Lhlg;

    .line 218
    :cond_2
    iget-boolean v0, p0, Lhlf;->diX:Z

    if-eqz v0, :cond_4

    .line 219
    const-string v0, "no_match"

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 221
    if-eqz p1, :cond_3

    .line 222
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 227
    :cond_3
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    iget-object v1, p0, Lhlf;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 228
    iput-object v2, p0, Lhlf;->bbe:Ljava/lang/String;

    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhlf;->diX:Z

    .line 232
    :cond_4
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lhli;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 145
    iget-object v0, p0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 146
    iget-boolean v0, p0, Lhlf;->diX:Z

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "VoiceSearchController"

    const-string v1, "Recognition already in progress!"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 148
    invoke-virtual {p0, v3, v4}, Lhlf;->J(ZZ)V

    .line 151
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqF()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    iget-object v0, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPg()Lgfb;

    move-result-object v0

    invoke-interface {v0}, Lgfb;->aFy()Lgfc;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqM()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lgfb;->mz(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2}, Lhlf;->b(Lcom/google/android/shared/search/Query;Lhli;)V

    .line 156
    :goto_0
    return-void

    .line 152
    :cond_2
    invoke-direct {p0, v1, p1, v3}, Lhlf;->a(Lgfc;Lcom/google/android/shared/search/Query;Z)Lgnj;

    move-result-object v0

    new-instance v1, Lglu;

    new-instance v2, Lhlh;

    invoke-direct {v2, p0, p2, p1}, Lhlh;-><init>(Lhlf;Lhli;Lcom/google/android/shared/search/Query;)V

    invoke-direct {v1, v2}, Lglu;-><init>(Lglx;)V

    iput-object v1, p0, Lhlf;->diT:Lglu;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhlf;->bbe:Ljava/lang/String;

    iput-boolean v4, p0, Lhlf;->diX:Z

    invoke-interface {p2}, Lhli;->Nt()V

    iget-object v1, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v1

    iget-object v2, p0, Lhlf;->diT:Lglu;

    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    goto :goto_0

    .line 154
    :cond_3
    invoke-direct {p0, p1, p2}, Lhlf;->b(Lcom/google/android/shared/search/Query;Lhli;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/shared/search/Query;Lhli;Leiq;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 285
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {p3, v2, v3, v4, v5}, Lege;->a(Lefr;JJ)V

    .line 286
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lhlf;->J(ZZ)V

    .line 287
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p3, v0}, Lhli;->a(Leiq;Ljava/lang/String;)V

    .line 288
    return-void

    .line 286
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Lhli;Lcom/google/android/shared/search/Query;Z)V
    .locals 5

    .prologue
    .line 294
    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v1

    invoke-direct {p0, v0, p2, v1}, Lhlf;->a(Lgfc;Lcom/google/android/shared/search/Query;Z)Lgnj;

    move-result-object v0

    .line 295
    new-instance v1, Lglu;

    new-instance v2, Lhlh;

    invoke-direct {v2, p0, p1, p2}, Lhlh;-><init>(Lhlf;Lhli;Lcom/google/android/shared/search/Query;)V

    invoke-direct {v1, v2}, Lglu;-><init>(Lglx;)V

    iput-object v1, p0, Lhlf;->diT:Lglu;

    .line 297
    iget-object v1, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v1

    iget-object v2, p0, Lhlf;->diT:Lglu;

    iget-object v3, p0, Lhlf;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    iget-object v4, p0, Lhlf;->mVss:Lhhq;

    invoke-virtual {v4}, Lhhq;->aPg()Lgfb;

    move-result-object v4

    invoke-interface {v1, v0, v2, v3, v4}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 300
    if-nez p3, :cond_0

    .line 303
    invoke-interface {p1}, Lhli;->Nq()V

    .line 305
    :cond_0
    return-void
.end method

.method protected bb(Lcom/google/android/shared/search/Query;)Lcmk;
    .locals 9

    .prologue
    .line 729
    iget-object v0, p0, Lhlf;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->j(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v6

    .line 733
    new-instance v5, Legl;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v0

    sget-object v2, Leoi;->cgG:Leoi;

    invoke-virtual {v2}, Leoi;->auU()J

    move-result-wide v2

    invoke-direct {v5, v0, v1, v2, v3}, Legl;-><init>(JJ)V

    .line 735
    new-instance v0, Lcmk;

    iget-object v1, p0, Lhlf;->mVss:Lhhq;

    iget-object v1, v1, Lhhq;->mAsyncServices:Lema;

    iget-object v2, p0, Lhlf;->mGsaConfigFlags:Lchk;

    iget-object v3, p0, Lhlf;->mSearchUrlHelper:Lcpn;

    iget-object v4, p0, Lhlf;->mSdchManager:Lczz;

    iget-object v7, p0, Lhlf;->mCookies:Lgpf;

    iget-object v8, p0, Lhlf;->mStaticContentCache:Lcpq;

    invoke-direct/range {v0 .. v8}, Lcmk;-><init>(Lema;Lchk;Lcpn;Lczz;Legl;Lcom/google/android/search/shared/api/UriRequest;Lgpf;Lcpq;)V

    return-object v0
.end method
