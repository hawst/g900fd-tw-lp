.class public final Lfrv;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 7

    .prologue
    const v6, 0x7f1102e7

    const v5, 0x7f1100be

    const v4, 0x7f090081

    .line 31
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v2, v0, Lanh;->ahl:Lane;

    .line 33
    invoke-virtual {v2}, Lane;->oJ()Z

    move-result v0

    .line 35
    invoke-virtual {v2}, Lane;->oy()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    invoke-virtual {v2}, Lane;->ox()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v6, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 38
    :cond_0
    invoke-virtual {v2}, Lane;->oA()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 39
    invoke-virtual {v2}, Lane;->oz()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v5, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 41
    :cond_1
    if-nez v0, :cond_2

    .line 42
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lfrv;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 44
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lfrv;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 47
    :cond_2
    invoke-virtual {v2}, Lane;->oC()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 48
    const v0, 0x7f1102e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;

    .line 50
    invoke-virtual {v2}, Lane;->oB()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setVisibility(I)V

    .line 52
    invoke-virtual {v2}, Lane;->oE()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 53
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2}, Lane;->oD()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setTextColor(I)V

    .line 56
    :cond_3
    invoke-virtual {v2}, Lane;->oG()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 57
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2}, Lane;->oF()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setBackgroundColor(I)V

    .line 61
    :cond_4
    invoke-virtual {v2}, Lane;->oI()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 62
    const v0, 0x7f1100bf

    invoke-virtual {v2}, Lane;->oH()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 64
    :cond_5
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 26
    const v0, 0x7f040106

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
