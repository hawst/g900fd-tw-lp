.class public final Ljbl;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dYH:[Ljbl;


# instance fields
.field private aeB:Ljbp;

.field private aez:I

.field private dYI:J

.field private dYJ:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 32629
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 32630
    const/4 v0, 0x0

    iput v0, p0, Ljbl;->aez:I

    iput-object v1, p0, Ljbl;->aeB:Ljbp;

    iput-wide v2, p0, Ljbl;->dYI:J

    iput-wide v2, p0, Ljbl;->dYJ:J

    iput-object v1, p0, Ljbl;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbl;->eCz:I

    .line 32631
    return-void
.end method

.method public static beX()[Ljbl;
    .locals 2

    .prologue
    .line 32575
    sget-object v0, Ljbl;->dYH:[Ljbl;

    if-nez v0, :cond_1

    .line 32576
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 32578
    :try_start_0
    sget-object v0, Ljbl;->dYH:[Ljbl;

    if-nez v0, :cond_0

    .line 32579
    const/4 v0, 0x0

    new-array v0, v0, [Ljbl;

    sput-object v0, Ljbl;->dYH:[Ljbl;

    .line 32581
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32583
    :cond_1
    sget-object v0, Ljbl;->dYH:[Ljbl;

    return-object v0

    .line 32581
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 32569
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbl;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljbl;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljbl;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljbl;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljbl;->dYI:J

    iget v0, p0, Ljbl;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbl;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljbl;->dYJ:J

    iget v0, p0, Ljbl;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbl;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 32646
    iget-object v0, p0, Ljbl;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 32647
    const/4 v0, 0x1

    iget-object v1, p0, Ljbl;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 32649
    :cond_0
    iget v0, p0, Ljbl;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 32650
    const/4 v0, 0x2

    iget-wide v2, p0, Ljbl;->dYI:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 32652
    :cond_1
    iget v0, p0, Ljbl;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 32653
    const/4 v0, 0x3

    iget-wide v2, p0, Ljbl;->dYJ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 32655
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 32656
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 32660
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 32661
    iget-object v1, p0, Ljbl;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 32662
    const/4 v1, 0x1

    iget-object v2, p0, Ljbl;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32665
    :cond_0
    iget v1, p0, Ljbl;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 32666
    const/4 v1, 0x2

    iget-wide v2, p0, Ljbl;->dYI:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 32669
    :cond_1
    iget v1, p0, Ljbl;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 32670
    const/4 v1, 0x3

    iget-wide v2, p0, Ljbl;->dYJ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 32673
    :cond_2
    return v0
.end method
