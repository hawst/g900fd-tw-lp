.class public final Legs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bYM:Legv;

.field private static bYN:Legs;

.field public static bYO:Z


# instance fields
.field private final bYP:Ljava/lang/Object;

.field private final bYQ:Ljava/util/List;

.field public bYR:Ljava/util/List;

.field private bYS:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Legv;

    invoke-direct {v0}, Legv;-><init>()V

    sput-object v0, Legs;->bYM:Legv;

    .line 43
    new-instance v0, Legs;

    invoke-direct {v0}, Legs;-><init>()V

    sput-object v0, Legs;->bYN:Legs;

    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Legs;->bYO:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Legs;->bYP:Ljava/lang/Object;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Legs;->bYQ:Ljava/util/List;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Legs;->bYR:Ljava/util/List;

    .line 71
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Legs;->bYS:J

    .line 89
    return-void
.end method

.method public static J(Ljava/util/List;)V
    .locals 5

    .prologue
    .line 320
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyr;

    .line 321
    iget-object v2, v0, Ljyr;->eMq:[Ljyo;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 322
    iget-object v4, v4, Ljyo;->eLV:[Ljyc;

    invoke-static {v4}, Lehd;->a([Ljyc;)V

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 325
    :cond_1
    return-void
.end method

.method private static K(Ljava/util/List;)Ljyr;
    .locals 2

    .prologue
    .line 356
    new-instance v1, Ljyr;

    invoke-direct {v1}, Ljyr;-><init>()V

    .line 357
    const-class v0, Ljyo;

    invoke-static {p0, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljyo;

    iput-object v0, v1, Ljyr;->eMq:[Ljyo;

    .line 359
    return-object v1
.end method

.method private a(Ljava/util/List;Legu;)V
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Legu;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    invoke-static {v0}, Legs;->K(Ljava/util/List;)Ljyr;

    move-result-object v0

    invoke-direct {p0, v0}, Legs;->b(Ljyr;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Legs;->bYR:Ljava/util/List;

    .line 119
    :cond_0
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auV()J

    move-result-wide v2

    iput-wide v2, p0, Legs;->bYS:J

    .line 121
    iget-wide v2, p0, Legs;->bYS:J

    const-wide/16 v4, -0x1

    invoke-static {v2, v3, v4, v5}, Legs;->d(JJ)Ljyo;

    move-result-object v2

    .line 122
    new-instance v0, Ljyq;

    invoke-direct {v0}, Ljyq;-><init>()V

    invoke-virtual {p2}, Legu;->aoS()I

    move-result v3

    invoke-virtual {v0, v3}, Ljyq;->tm(I)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->oY()I

    move-result v3

    invoke-virtual {v0, v3}, Ljyq;->tl(I)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->aoO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljyq;->zT(Ljava/lang/String;)Ljyq;

    move-result-object v3

    invoke-virtual {p2}, Legu;->aoP()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljyq;->jv(Z)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->aoQ()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljyq;->jw(Z)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->afW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljyq;->zS(Ljava/lang/String;)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->aoR()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljyq;->jx(Z)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->aoU()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljyq;->jy(Z)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->aoV()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljyq;->jz(Z)Ljyq;

    move-result-object v0

    invoke-virtual {p2}, Legu;->aoT()Ljse;

    move-result-object v3

    iput-object v3, v0, Ljyq;->eMj:Ljse;

    invoke-virtual {p2}, Legu;->aoP()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Legu;->aoM()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p2}, Legu;->aoM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljyq;->zR(Ljava/lang/String;)Ljyq;

    :cond_1
    :goto_1
    invoke-virtual {p2}, Legu;->alk()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Legu;->alk()I

    move-result v1

    invoke-virtual {v0, v1}, Ljyq;->tj(I)Ljyq;

    :cond_2
    invoke-virtual {p2}, Legu;->aoN()I

    move-result v1

    if-ltz v1, :cond_3

    invoke-virtual {p2}, Legu;->aoN()I

    move-result v1

    invoke-virtual {v0, v1}, Ljyq;->tk(I)Ljyq;

    :cond_3
    iput-object v0, v2, Ljyo;->eLQ:Ljyq;

    .line 123
    const-class v0, Ljyc;

    invoke-static {p1, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljyc;

    iput-object v0, v2, Ljyo;->eLV:[Ljyc;

    .line 124
    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    return-void

    :cond_4
    move v0, v1

    .line 122
    goto :goto_0

    :cond_5
    const-string v3, "ActionCardEventLogger"

    const-string v4, "Impression Data with null event id."

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v3, v4, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static aoH()Legs;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Legs;->bYN:Legs;

    return-object v0
.end method

.method public static aoI()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    sput-boolean v0, Legs;->bYO:Z

    .line 83
    return-void
.end method

.method private b(Ljyr;)V
    .locals 2

    .prologue
    .line 340
    iget-object v1, p0, Legs;->bYP:Ljava/lang/Object;

    monitor-enter v1

    .line 341
    :try_start_0
    iget-object v0, p0, Legs;->bYQ:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static d(JJ)Ljyo;
    .locals 4

    .prologue
    .line 423
    new-instance v0, Ljyo;

    invoke-direct {v0}, Ljyo;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljyo;->dR(J)Ljyo;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljyo;->dQ(J)Ljyo;

    move-result-object v0

    .line 427
    const-wide/16 v2, -0x1

    cmp-long v1, p2, v2

    if-lez v1, :cond_0

    .line 428
    invoke-virtual {v0, p2, p3}, Ljyo;->dS(J)Ljyo;

    .line 430
    :cond_0
    return-object v0
.end method

.method private w(III)V
    .locals 4

    .prologue
    .line 269
    iget-wide v0, p0, Legs;->bYS:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 274
    const-string v0, "ActionCardEventLogger"

    const-string v1, "Interaction on a VE with no impression."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 277
    :cond_0
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auV()J

    move-result-wide v0

    iget-wide v2, p0, Legs;->bYS:J

    invoke-static {v0, v1, v2, v3}, Legs;->d(JJ)Ljyo;

    move-result-object v0

    .line 279
    new-instance v1, Ljyb;

    invoke-direct {v1}, Ljyb;-><init>()V

    invoke-virtual {v1, p1}, Ljyb;->tb(I)Ljyb;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljyb;->tc(I)Ljyb;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljyb;->td(I)Ljyb;

    move-result-object v1

    iput-object v1, v0, Ljyo;->eLW:Ljyb;

    .line 281
    iget-object v1, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    return-void
.end method


# virtual methods
.method public final a(ILjava/util/List;Legu;)V
    .locals 6
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Legu;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 139
    sget-boolean v0, Legs;->bYO:Z

    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 147
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 148
    new-instance v3, Ljyc;

    invoke-direct {v3}, Ljyc;-><init>()V

    .line 149
    invoke-virtual {v3, p1}, Ljyc;->tf(I)Ljyc;

    .line 150
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, v3, Ljyc;->dKU:[I

    move v0, v1

    .line 152
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 153
    iget-object v4, v3, Ljyc;->dKU:[I

    add-int/lit8 v5, v0, 0x1

    aput v5, v4, v0

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 156
    :cond_1
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 157
    new-instance v3, Ljyc;

    invoke-direct {v3}, Ljyc;-><init>()V

    .line 158
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Ljyc;->tf(I)Ljyc;

    .line 159
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 161
    :cond_2
    invoke-direct {p0, v2, p3}, Legs;->a(Ljava/util/List;Legu;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Legu;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Legu;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 99
    sget-boolean v0, Legs;->bYO:Z

    if-nez v0, :cond_0

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    sget-object v0, Legs;->bYM:Legv;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lehd;->a(Landroid/view/View;Lehe;Z)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Legs;->a(Ljava/util/List;Legu;)V

    goto :goto_0
.end method

.method public final aF(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 172
    sget-boolean v0, Legs;->bYO:Z

    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 175
    :cond_0
    sget-object v0, Legs;->bYM:Legv;

    invoke-static {p1, v0, v5}, Lehd;->a(Landroid/view/View;Lehe;Z)Ljava/util/List;

    move-result-object v2

    .line 177
    const/4 v0, 0x0

    .line 178
    iget-object v1, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 181
    const-string v1, "ActionCardEventLogger"

    const-string v3, "No previous ACLE when logCardUpdateImpression"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v1, v0

    .line 187
    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, v1, Ljyo;->eLW:Ljyb;

    if-eqz v0, :cond_3

    iget-object v0, v1, Ljyo;->eLV:[Ljyc;

    if-eqz v0, :cond_1

    iget-object v0, v1, Ljyo;->eLV:[Ljyc;

    array-length v0, v0

    if-nez v0, :cond_3

    .line 191
    :cond_1
    iget-object v0, v1, Ljyo;->eLV:[Ljyc;

    invoke-static {v0, v2}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljyc;

    iput-object v0, v1, Ljyo;->eLV:[Ljyc;

    .line 194
    invoke-virtual {v1}, Ljyo;->bwk()J

    move-result-wide v0

    iput-wide v0, p0, Legs;->bYS:J

    goto :goto_0

    .line 183
    :cond_2
    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    iget-object v1, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyo;

    move-object v1, v0

    goto :goto_1

    .line 201
    :cond_3
    const-string v0, "ActionCardEventLogger"

    const-string v1, "CardUpdateImpression without an interaction."

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 203
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auV()J

    move-result-wide v4

    .line 204
    iget-wide v0, p0, Legs;->bYS:J

    invoke-static {v4, v5, v0, v1}, Legs;->d(JJ)Ljyo;

    move-result-object v1

    .line 205
    const-class v0, Ljyc;

    invoke-static {v2, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljyc;

    iput-object v0, v1, Ljyo;->eLV:[Ljyc;

    .line 207
    iput-wide v4, p0, Legs;->bYS:J

    .line 208
    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final aI(II)V
    .locals 1

    .prologue
    .line 256
    sget-boolean v0, Legs;->bYO:Z

    if-nez v0, :cond_0

    .line 260
    :goto_0
    return-void

    .line 259
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Legs;->w(III)V

    goto :goto_0
.end method

.method public final aoJ()Ljava/util/List;
    .locals 3

    .prologue
    .line 293
    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Legs;->bYR:Ljava/util/List;

    invoke-static {v0}, Legs;->K(Ljava/util/List;)Ljyr;

    move-result-object v0

    invoke-direct {p0, v0}, Legs;->b(Ljyr;)V

    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Legs;->bYR:Ljava/util/List;

    .line 299
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Legs;->bYS:J

    .line 302
    iget-object v1, p0, Legs;->bYP:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_0
    iget-object v0, p0, Legs;->bYQ:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    .line 304
    iget-object v2, p0, Legs;->bYQ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 305
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    return-object v0

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final q(Landroid/view/View;I)V
    .locals 5
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 223
    sget-boolean v0, Legs;->bYO:Z

    if-nez v0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    invoke-static {p1}, Lehd;->aG(Landroid/view/View;)I

    move-result v3

    .line 229
    if-ltz v3, :cond_0

    .line 235
    const v0, 0x7f11000f

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_2

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_1
    const-string v4, "ve_index can only be an Integer."

    invoke-static {v1, v4}, Lifv;->c(ZLjava/lang/Object;)V

    .line 239
    const/4 v1, -0x1

    .line 240
    if-nez v0, :cond_4

    .line 242
    const-string v0, "ActionCardEventLogger"

    const-string v4, "Interaction on a VE with no ve_index."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 246
    :goto_2
    invoke-direct {p0, v3, v0, p2}, Legs;->w(III)V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 236
    goto :goto_1

    .line 244
    :cond_4
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2
.end method
