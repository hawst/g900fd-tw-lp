.class public interface abstract Lbnp;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lbnm;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
.end method

.method public abstract a(Lbnm;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/internal/ph;)Lbjg;
.end method

.method public abstract a(Lbnm;Ljava/lang/String;I)Lbjg;
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIIZ)Lbjg;
.end method

.method public abstract a(Lbnm;JZ)V
.end method

.method public abstract a(Lbnm;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lbnm;Lcom/google/android/gms/internal/oq;Ljava/util/List;Lcom/google/android/gms/internal/ov;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;II)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJ)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;II)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/internal/jn;)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V
.end method

.method public abstract a(Lbnm;Ljava/lang/String;Z[Ljava/lang/String;)V
.end method

.method public abstract a(Lbnm;ZZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lbnm;ZZLjava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract b(Lbnm;JZ)Lbjg;
.end method

.method public abstract b(Lbnm;Ljava/lang/String;)Lbjg;
.end method

.method public abstract b(Lbnm;Ljava/lang/String;II)Lbjg;
.end method

.method public abstract b(Lbnm;Ljava/lang/String;Ljava/lang/String;II)Lbjg;
.end method

.method public abstract b(Lbnm;Landroid/os/Bundle;)V
.end method

.method public abstract b(Lbnm;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Lbnm;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract b(Lbnm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract bJ(Z)V
.end method

.method public abstract c(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
.end method

.method public abstract c(Lbnm;Ljava/lang/String;Ljava/lang/String;I)Lbjg;
.end method

.method public abstract c(Lbnm;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract h(Landroid/net/Uri;)Landroid/os/Bundle;
.end method

.method public abstract u(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract v(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract zS()Z
.end method
