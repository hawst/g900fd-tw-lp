.class public final Ljtz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eEo:[Ljtz;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private eEm:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 548
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 549
    const/4 v0, 0x0

    iput v0, p0, Ljtz;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljtz;->agq:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljtz;->eEm:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljtz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljtz;->eCz:I

    .line 550
    return-void
.end method

.method public static buD()[Ljtz;
    .locals 2

    .prologue
    .line 494
    sget-object v0, Ljtz;->eEo:[Ljtz;

    if-nez v0, :cond_1

    .line 495
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 497
    :try_start_0
    sget-object v0, Ljtz;->eEo:[Ljtz;

    if-nez v0, :cond_0

    .line 498
    const/4 v0, 0x0

    new-array v0, v0, [Ljtz;

    sput-object v0, Ljtz;->eEo:[Ljtz;

    .line 500
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    :cond_1
    sget-object v0, Ljtz;->eEo:[Ljtz;

    return-object v0

    .line 500
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 488
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljtz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljtz;->agq:Ljava/lang/String;

    iget v0, p0, Ljtz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljtz;->eEm:F

    iget v0, p0, Ljtz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljtz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 564
    iget v0, p0, Ljtz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 565
    const/4 v0, 0x1

    iget-object v1, p0, Ljtz;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 567
    :cond_0
    iget v0, p0, Ljtz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 568
    const/4 v0, 0x2

    iget v1, p0, Ljtz;->eEm:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 570
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 571
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Ljtz;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 575
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 576
    iget v1, p0, Ljtz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 577
    const/4 v1, 0x1

    iget-object v2, p0, Ljtz;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    :cond_0
    iget v1, p0, Ljtz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 581
    const/4 v1, 0x2

    iget v2, p0, Ljtz;->eEm:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 584
    :cond_1
    return v0
.end method

.method public final yX(Ljava/lang/String;)Ljtz;
    .locals 1

    .prologue
    .line 513
    if-nez p1, :cond_0

    .line 514
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 516
    :cond_0
    iput-object p1, p0, Ljtz;->agq:Ljava/lang/String;

    .line 517
    iget v0, p0, Ljtz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtz;->aez:I

    .line 518
    return-object p0
.end method
