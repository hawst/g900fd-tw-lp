.class public final Ldra;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "PG"


# instance fields
.field private final bGH:Lcom/google/android/search/searchplate/SimpleSearchText;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputConnection;Lcom/google/android/search/searchplate/SimpleSearchText;)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 18
    iput-object p2, p0, Ldra;->bGH:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 19
    return-void
.end method


# virtual methods
.method public final beginBatchEdit()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Ldra;->bGH:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->onBeginBatchEdit()V

    .line 46
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->beginBatchEdit()Z

    move-result v0

    return v0
.end method

.method public final commitText(Ljava/lang/CharSequence;I)Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ldra;->bGH:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->t(Ljava/lang/CharSequence;)V

    .line 25
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    return v0
.end method

.method public final endBatchEdit()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Ldra;->bGH:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->onEndBatchEdit()V

    .line 53
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->endBatchEdit()Z

    move-result v0

    return v0
.end method

.method public final finishComposingText()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Ldra;->bGH:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afK()V

    .line 39
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->finishComposingText()Z

    move-result v0

    return v0
.end method

.method public final setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldra;->bGH:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->s(Ljava/lang/CharSequence;)V

    .line 32
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->setComposingText(Ljava/lang/CharSequence;I)Z

    move-result v0

    return v0
.end method
