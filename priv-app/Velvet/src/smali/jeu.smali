.class public final Ljeu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efS:Z

.field private ega:Z

.field private egb:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 12871
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12872
    const/4 v0, 0x0

    iput v0, p0, Ljeu;->aez:I

    iput-object v2, p0, Ljeu;->efN:Ljez;

    iput-boolean v1, p0, Ljeu;->ega:Z

    iput-boolean v1, p0, Ljeu;->efS:Z

    iput-boolean v1, p0, Ljeu;->egb:Z

    iput-object v2, p0, Ljeu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljeu;->eCz:I

    .line 12873
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 12792
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljeu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljeu;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljeu;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljeu;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljeu;->ega:Z

    iget v0, p0, Ljeu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljeu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljeu;->efS:Z

    iget v0, p0, Ljeu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljeu;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljeu;->egb:Z

    iget v0, p0, Ljeu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljeu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12889
    iget-object v0, p0, Ljeu;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 12890
    const/4 v0, 0x1

    iget-object v1, p0, Ljeu;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 12892
    :cond_0
    iget v0, p0, Ljeu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 12893
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljeu;->ega:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12895
    :cond_1
    iget v0, p0, Ljeu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 12896
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljeu;->efS:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12898
    :cond_2
    iget v0, p0, Ljeu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 12899
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljeu;->egb:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12901
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12902
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12906
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12907
    iget-object v1, p0, Ljeu;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 12908
    const/4 v1, 0x1

    iget-object v2, p0, Ljeu;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12911
    :cond_0
    iget v1, p0, Ljeu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 12912
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljeu;->ega:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12915
    :cond_1
    iget v1, p0, Ljeu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 12916
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljeu;->efS:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12919
    :cond_2
    iget v1, p0, Ljeu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 12920
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljeu;->egb:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12923
    :cond_3
    return v0
.end method
