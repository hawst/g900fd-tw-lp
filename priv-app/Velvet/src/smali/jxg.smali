.class public final Ljxg;
.super Ljsl;
.source "PG"


# static fields
.field public static final eKr:Ljsm;


# instance fields
.field private aez:I

.field private eBD:Ljava/lang/String;

.field private eGH:Ljava/lang/String;

.field public eKs:Ljvv;

.field public eKt:Ljvr;

.field private eKu:Ljvp;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 934
    const/16 v0, 0xb

    const-class v1, Ljxg;

    const v2, 0x9907ca

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxg;->eKr:Ljsm;

    .line 940
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1000
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1001
    const/4 v0, 0x0

    iput v0, p0, Ljxg;->aez:I

    iput-object v1, p0, Ljxg;->eKs:Ljvv;

    iput-object v1, p0, Ljxg;->eKt:Ljvr;

    iput-object v1, p0, Ljxg;->eKu:Ljvp;

    const-string v0, ""

    iput-object v0, p0, Ljxg;->eBD:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljxg;->eGH:Ljava/lang/String;

    iput-object v1, p0, Ljxg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxg;->eCz:I

    .line 1002
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 927
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljxg;->eKs:Ljvv;

    if-nez v0, :cond_1

    new-instance v0, Ljvv;

    invoke-direct {v0}, Ljvv;-><init>()V

    iput-object v0, p0, Ljxg;->eKs:Ljvv;

    :cond_1
    iget-object v0, p0, Ljxg;->eKs:Ljvv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljxg;->eKt:Ljvr;

    if-nez v0, :cond_2

    new-instance v0, Ljvr;

    invoke-direct {v0}, Ljvr;-><init>()V

    iput-object v0, p0, Ljxg;->eKt:Ljvr;

    :cond_2
    iget-object v0, p0, Ljxg;->eKt:Ljvr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxg;->eBD:Ljava/lang/String;

    iget v0, p0, Ljxg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxg;->eGH:Ljava/lang/String;

    iget v0, p0, Ljxg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxg;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljxg;->eKu:Ljvp;

    if-nez v0, :cond_3

    new-instance v0, Ljvp;

    invoke-direct {v0}, Ljvp;-><init>()V

    iput-object v0, p0, Ljxg;->eKu:Ljvp;

    :cond_3
    iget-object v0, p0, Ljxg;->eKu:Ljvp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Ljxg;->eKs:Ljvv;

    if-eqz v0, :cond_0

    .line 1020
    const/4 v0, 0x1

    iget-object v1, p0, Ljxg;->eKs:Ljvv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1022
    :cond_0
    iget-object v0, p0, Ljxg;->eKt:Ljvr;

    if-eqz v0, :cond_1

    .line 1023
    const/4 v0, 0x2

    iget-object v1, p0, Ljxg;->eKt:Ljvr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1025
    :cond_1
    iget v0, p0, Ljxg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1026
    const/4 v0, 0x3

    iget-object v1, p0, Ljxg;->eBD:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1028
    :cond_2
    iget v0, p0, Ljxg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 1029
    const/4 v0, 0x4

    iget-object v1, p0, Ljxg;->eGH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1031
    :cond_3
    iget-object v0, p0, Ljxg;->eKu:Ljvp;

    if-eqz v0, :cond_4

    .line 1032
    const/4 v0, 0x5

    iget-object v1, p0, Ljxg;->eKu:Ljvp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1034
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1035
    return-void
.end method

.method public final getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 959
    iget-object v0, p0, Ljxg;->eBD:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1039
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1040
    iget-object v1, p0, Ljxg;->eKs:Ljvv;

    if-eqz v1, :cond_0

    .line 1041
    const/4 v1, 0x1

    iget-object v2, p0, Ljxg;->eKs:Ljvv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1044
    :cond_0
    iget-object v1, p0, Ljxg;->eKt:Ljvr;

    if-eqz v1, :cond_1

    .line 1045
    const/4 v1, 0x2

    iget-object v2, p0, Ljxg;->eKt:Ljvr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1048
    :cond_1
    iget v1, p0, Ljxg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 1049
    const/4 v1, 0x3

    iget-object v2, p0, Ljxg;->eBD:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1052
    :cond_2
    iget v1, p0, Ljxg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 1053
    const/4 v1, 0x4

    iget-object v2, p0, Ljxg;->eGH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1056
    :cond_3
    iget-object v1, p0, Ljxg;->eKu:Ljvp;

    if-eqz v1, :cond_4

    .line 1057
    const/4 v1, 0x5

    iget-object v2, p0, Ljxg;->eKu:Ljvp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1060
    :cond_4
    return v0
.end method

.method public final zL(Ljava/lang/String;)Ljxg;
    .locals 1

    .prologue
    .line 962
    if-nez p1, :cond_0

    .line 963
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 965
    :cond_0
    iput-object p1, p0, Ljxg;->eBD:Ljava/lang/String;

    .line 966
    iget v0, p0, Ljxg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxg;->aez:I

    .line 967
    return-object p0
.end method
