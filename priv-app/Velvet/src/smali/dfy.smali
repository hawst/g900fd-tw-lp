.class public final Ldfy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldeb;


# instance fields
.field private bwJ:Ldef;

.field private bwK:Ldem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ldem;ILdef;Lddw;)V
    .locals 3

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldfy;->bwJ:Ldef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfy;->bwK:Ldem;

    if-eq v0, p1, :cond_2

    .line 30
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldfy;->bwJ:Ldef;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Ldfy;->bwK:Ldem;

    .line 32
    invoke-virtual {p1}, Ldem;->abn()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 33
    invoke-interface {v0}, Ldef;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 34
    iput-object v0, p0, Ldfy;->bwJ:Ldef;

    .line 35
    invoke-virtual {p1}, Ldem;->abl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36
    iput-object p1, p0, Ldfy;->bwK:Ldem;

    .line 45
    :cond_2
    iget-object v0, p0, Ldfy;->bwJ:Ldef;

    if-eqz v0, :cond_3

    .line 46
    iget-object v0, p0, Ldfy;->bwJ:Ldef;

    invoke-interface {v0}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 48
    invoke-interface {p4}, Lddw;->getCount()I

    move-result v2

    if-ge v2, p2, :cond_4

    .line 49
    invoke-interface {p4, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Ldem;->abl()Z

    move-result v0

    if-nez v0, :cond_4

    .line 55
    if-eqz p3, :cond_5

    invoke-interface {p3}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    invoke-interface {p4}, Lddw;->getCount()I

    move-result v2

    if-ge v2, p2, :cond_5

    invoke-interface {p4, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_1

    .line 58
    :cond_4
    invoke-virtual {p1}, Ldem;->abl()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 59
    invoke-interface {p4}, Lddw;->aaZ()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    :cond_5
    monitor-exit p0

    return-void
.end method
