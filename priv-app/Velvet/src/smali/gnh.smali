.class public final Lgnh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final cPV:Lgdm;


# direct methods
.method public constructor <init>(Lgdm;)V
    .locals 0
    .param p1    # Lgdm;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lgnh;->cPV:Lgdm;

    .line 26
    return-void
.end method

.method private static u([Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p0    # [Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 36
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_2

    .line 40
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 101
    :cond_1
    return-object v0

    .line 43
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 44
    array-length v3, p0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, p0, v1

    .line 45
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 49
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v6, ":"

    invoke-direct {v5, v4, v6, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 53
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    .line 54
    const/4 v6, 0x5

    if-ne v4, v6, :cond_3

    .line 59
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 64
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 65
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 66
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    .line 67
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 70
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_3

    .line 75
    :try_start_0
    new-instance v9, Lkex;

    invoke-direct {v9}, Lkex;-><init>()V

    .line 82
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v4}, Lkex;->ua(I)Lkex;

    .line 83
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v9, v4}, Lkex;->ub(I)Lkex;

    .line 85
    new-instance v4, Lkex;

    invoke-direct {v4}, Lkex;-><init>()V

    .line 86
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Lkex;->ua(I)Lkex;

    .line 87
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Lkex;->ub(I)Lkex;

    .line 89
    new-instance v6, Lkey;

    invoke-direct {v6}, Lkey;-><init>()V

    .line 90
    iput-object v9, v6, Lkey;->eVn:Lkex;

    .line 91
    iput-object v4, v6, Lkey;->eVm:Lkex;

    .line 93
    new-instance v4, Lgng;

    invoke-direct {v4, v6, v5}, Lgng;-><init>(Lkey;Ljava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lgnh;->cPV:Lgdm;

    invoke-interface {v0}, Lgdm;->Gc()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgnh;->u([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lgnh;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
