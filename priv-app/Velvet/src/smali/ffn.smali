.class public final Lffn;
.super Lffx;
.source "PG"


# instance fields
.field private final cqf:Landroid/location/Location;

.field private final cqg:Landroid/location/Location;


# direct methods
.method public constructor <init>(Landroid/location/Location;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lffx;-><init>()V

    .line 71
    iput-object p1, p0, Lffn;->cqf:Landroid/location/Location;

    .line 72
    iput-object p2, p0, Lffn;->cqg:Landroid/location/Location;

    .line 73
    return-void
.end method

.method private a([Ljbp;Landroid/location/Location;)Z
    .locals 12

    .prologue
    .line 136
    array-length v10, p1

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v10, :cond_2

    aget-object v11, p1, v9

    .line 137
    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {v11}, Ljbp;->mR()D

    move-result-wide v0

    invoke-virtual {v11}, Ljbp;->mS()D

    move-result-wide v2

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-double v0, v0

    invoke-virtual {v11}, Ljbp;->bfc()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    invoke-virtual {v11}, Ljbp;->bfc()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    .line 138
    const/4 v0, 0x1

    .line 141
    :goto_2
    return v0

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 136
    :cond_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 141
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final b(Lizj;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 77
    iget-object v2, p1, Lizj;->dUr:Ljcg;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lizj;->dUr:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lizj;->dUr:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    iget-object v2, v2, Ljie;->eaW:[Ljbp;

    array-length v2, v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lizj;->dUr:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    iget-object v2, v2, Ljie;->end:[I

    array-length v2, v2

    if-nez v2, :cond_2

    :cond_0
    move p2, v0

    .line 114
    :cond_1
    :goto_0
    return p2

    .line 84
    :cond_2
    iget-object v2, p1, Lizj;->dUr:Ljcg;

    iget-object v2, v2, Ljcg;->dRY:Ljie;

    .line 87
    iget-object v3, v2, Ljie;->end:[I

    array-length v3, v3

    if-gt v3, v1, :cond_1

    .line 90
    iget-object v3, v2, Ljie;->end:[I

    aget v3, v3, v0

    .line 93
    packed-switch v3, :pswitch_data_0

    .line 110
    const-string v0, "NotificationGeofencer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown trigger condition type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 95
    :pswitch_0
    iget-object v3, p0, Lffn;->cqf:Landroid/location/Location;

    if-eqz v3, :cond_3

    iget-object v3, v2, Ljie;->eaW:[Ljbp;

    iget-object v4, p0, Lffn;->cqf:Landroid/location/Location;

    invoke-direct {p0, v3, v4}, Lffn;->a([Ljbp;Landroid/location/Location;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103
    :goto_1
    if-eqz v1, :cond_4

    if-nez p2, :cond_4

    iget-object v3, p0, Lffn;->cqg:Landroid/location/Location;

    if-eqz v3, :cond_4

    iget-object v2, v2, Ljie;->eaW:[Ljbp;

    iget-object v3, p0, Lffn;->cqg:Landroid/location/Location;

    invoke-direct {p0, v2, v3}, Lffn;->a([Ljbp;Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_4

    move p2, v0

    .line 106
    goto :goto_0

    :cond_3
    move v1, v0

    .line 95
    goto :goto_1

    :cond_4
    move p2, v1

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method
