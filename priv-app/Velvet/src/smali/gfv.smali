.class final Lgfv;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private cIZ:Lgft;

.field private final cJa:I


# direct methods
.method constructor <init>(Lgft;I)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 456
    iput-object p1, p0, Lgfv;->cIZ:Lgft;

    .line 457
    iput p2, p0, Lgfv;->cJa:I

    .line 458
    return-void
.end method


# virtual methods
.method public final declared-synchronized close()V
    .locals 2

    .prologue
    .line 479
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgfv;->cIZ:Lgft;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lgfv;->cIZ:Lgft;

    iget v1, p0, Lgfv;->cJa:I

    invoke-virtual {v0, v1}, Lgft;->remove(I)V

    .line 482
    const/4 v0, 0x0

    iput-object v0, p0, Lgfv;->cIZ:Lgft;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    :cond_0
    monitor-exit p0

    return-void

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final read()I
    .locals 2

    .prologue
    .line 462
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Find some other app to be inefficient in."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final declared-synchronized read([BII)I
    .locals 2

    .prologue
    .line 467
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgfv;->cIZ:Lgft;

    if-nez v0, :cond_0

    .line 468
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Secondary Tee stream closed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 470
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgfv;->cIZ:Lgft;

    iget v1, p0, Lgfv;->cJa:I

    invoke-virtual {v0, v1, p1, p2, p3}, Lgft;->a(I[BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 471
    if-nez v0, :cond_1

    .line 472
    const/4 v0, -0x1

    .line 474
    :cond_1
    monitor-exit p0

    return v0
.end method
