.class public final Lcng;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bcN:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final mExtrasConsumer:Lcoi;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final mGsaConfig:Lchk;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final mPreloadTaskHandler:Lcpt;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final mStaticContentCache:Lcpq;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lchk;Ljava/lang/String;Lcoi;Lcpt;Lcpq;)V
    .locals 1
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcoi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lcpt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lcng;->mGsaConfig:Lchk;

    .line 35
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcng;->bcN:Ljava/lang/String;

    .line 36
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoi;

    iput-object v0, p0, Lcng;->mExtrasConsumer:Lcoi;

    .line 37
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpt;

    iput-object v0, p0, Lcng;->mPreloadTaskHandler:Lcpt;

    .line 38
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpq;

    iput-object v0, p0, Lcng;->mStaticContentCache:Lcpq;

    .line 39
    return-void
.end method
