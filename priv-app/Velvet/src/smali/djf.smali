.class final Ldjf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field final bzN:Ldiu;

.field final bzO:Ldgk;

.field private final bzP:J

.field final bzQ:Ljava/util/ArrayList;

.field private final bzR:Ljava/lang/Runnable;

.field final synthetic bzS:Ldje;


# direct methods
.method public constructor <init>(Ldje;Ldiu;Ldgk;J)V
    .locals 2

    .prologue
    .line 129
    iput-object p1, p0, Ldjf;->bzS:Ldje;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v0, Ldjg;

    invoke-direct {v0, p0}, Ldjg;-><init>(Ldjf;)V

    iput-object v0, p0, Ldjf;->bzR:Ljava/lang/Runnable;

    .line 132
    iput-object p2, p0, Ldjf;->bzN:Ldiu;

    .line 133
    iput-object p3, p0, Ldjf;->bzO:Ldgk;

    .line 134
    iput-wide p4, p0, Ldjf;->bzP:J

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldjf;->bzQ:Ljava/util/ArrayList;

    .line 136
    return-void
.end method


# virtual methods
.method final acr()V
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Ldjf;->bzS:Ldje;

    invoke-static {v0}, Ldje;->b(Ldje;)Leqo;

    move-result-object v0

    iget-object v1, p0, Ldjf;->bzR:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 167
    iget-wide v0, p0, Ldjf;->bzP:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    iget-boolean v0, v0, Ldje;->bzL:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    iget-boolean v0, v0, Ldje;->bzM:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 170
    iget-object v0, p0, Ldjf;->bzS:Ldje;

    invoke-static {v0}, Ldje;->c(Ldje;)Leqo;

    move-result-object v0

    iget-object v1, p0, Ldjf;->bzR:Ljava/lang/Runnable;

    iget-wide v2, p0, Ldjf;->bzP:J

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 176
    :goto_1
    return-void

    .line 167
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 174
    :cond_2
    invoke-virtual {p0}, Ldjf;->acs()V

    goto :goto_1
.end method

.method final acs()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 220
    iget-object v0, p0, Ldjf;->bzQ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    iget-object v2, p0, Ldjf;->bzO:Ldgk;

    iget-object v3, p0, Ldjf;->bzQ:Ljava/util/ArrayList;

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    iget-boolean v0, v0, Ldje;->bzL:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    iget-boolean v0, v0, Ldje;->bzM:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v1, v3, v0}, Ldgk;->b(ILjava/util/List;Z)V

    .line 224
    iget-object v0, p0, Ldjf;->bzQ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 226
    :cond_0
    return-void

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 114
    check-cast p1, Ldef;

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    invoke-static {v0}, Ldje;->a(Ldje;)Leqo;

    move-result-object v0

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Ldjf;->bzN:Ldiu;

    invoke-virtual {v0}, Ldiu;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    iput-boolean v1, v0, Ldje;->bzM:Z

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Ldjf;->bzS:Ldje;

    iget-object v0, v0, Ldje;->bzI:Ldir;

    invoke-virtual {v0, p1}, Ldir;->a(Ldef;)V

    iget-object v0, p0, Ldjf;->bzQ:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Ldjf;->acr()V

    return v1
.end method

.method public final dw(Z)Ldgk;
    .locals 1

    .prologue
    .line 179
    new-instance v0, Ldjh;

    invoke-direct {v0, p0, p1}, Ldjh;-><init>(Ldjf;Z)V

    return-object v0
.end method
