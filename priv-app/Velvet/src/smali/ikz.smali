.class final Likz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private dDF:Ljava/util/Iterator;

.field private dDG:Ljava/util/Iterator;

.field private synthetic dDH:Ljava/util/Iterator;


# direct methods
.method constructor <init>(Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 528
    iput-object p1, p0, Likz;->dDH:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 529
    invoke-static {}, Likr;->aXh()Lirv;

    move-result-object v0

    iput-object v0, p0, Likz;->dDG:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 543
    :goto_0
    iget-object v0, p0, Likz;->dDG:Ljava/util/Iterator;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Likz;->dDH:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    iget-object v0, p0, Likz;->dDH:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    iput-object v0, p0, Likz;->dDG:Ljava/util/Iterator;

    goto :goto_0

    .line 546
    :cond_0
    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 550
    invoke-virtual {p0}, Likz;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 551
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 553
    :cond_0
    iget-object v0, p0, Likz;->dDG:Ljava/util/Iterator;

    iput-object v0, p0, Likz;->dDF:Ljava/util/Iterator;

    .line 554
    iget-object v0, p0, Likz;->dDG:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Likz;->dDF:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "no calls to next() since last call to remove()"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 560
    iget-object v0, p0, Likz;->dDF:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 561
    const/4 v0, 0x0

    iput-object v0, p0, Likz;->dDF:Ljava/util/Iterator;

    .line 562
    return-void

    .line 558
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
