.class public final Lggo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final aTW:Ljava/util/concurrent/Executor;

.field final cJA:Lggk;

.field private final cJB:Ljava/lang/Object;

.field private cJC:Lggp;

.field private cJD:Ljava/util/List;

.field private cJE:J

.field final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lcke;Lggk;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lggk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lggo;->mSearchSettings:Lcke;

    .line 42
    iput-object p2, p0, Lggo;->cJA:Lggk;

    .line 43
    iput-object p3, p0, Lggo;->aTW:Ljava/util/concurrent/Executor;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lggo;->cJB:Ljava/lang/Object;

    .line 45
    return-void
.end method


# virtual methods
.method final a(Ljava/util/List;J)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79
    iget-object v1, p0, Lggo;->cJB:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_0
    iput-object p1, p0, Lggo;->cJD:Ljava/util/List;

    .line 81
    iput-wide p2, p0, Lggo;->cJE:J

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lggo;->cJC:Lggp;

    .line 83
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aFL()Ljava/util/List;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 50
    iget-object v1, p0, Lggo;->cJB:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lggo;->cJE:J

    sub-long/2addr v2, v4

    .line 55
    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    iget-object v0, p0, Lggo;->cJD:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "ContactLabelSupplier"

    const-string v2, "get() : Cache hit"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 73
    :goto_0
    iget-object v0, p0, Lggo;->cJD:Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 57
    :cond_0
    iget-object v0, p0, Lggo;->cJC:Lggp;

    if-eqz v0, :cond_1

    .line 58
    const-string v0, "ContactLabelSupplier"

    const-string v2, "get() : Already running"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 59
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_2

    .line 63
    const-string v0, "ContactLabelSupplier"

    const-string v2, "get() : Execute runnable (UI thread)"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 64
    new-instance v0, Lggp;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lggp;-><init>(Lggo;B)V

    iput-object v0, p0, Lggo;->cJC:Lggp;

    .line 65
    iget-object v0, p0, Lggo;->aTW:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lggo;->cJC:Lggp;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 68
    :cond_2
    const-string v0, "ContactLabelSupplier"

    const-string v2, "get() : Execute directly (BG thread)"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 69
    new-instance v0, Lggp;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lggp;-><init>(Lggo;B)V

    iput-object v0, p0, Lggo;->cJC:Lggp;

    .line 70
    iget-object v0, p0, Lggo;->cJC:Lggp;

    invoke-virtual {v0}, Lggp;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lggo;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
