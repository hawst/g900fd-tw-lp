.class public final Lhox;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private synthetic dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 55
    if-nez p2, :cond_0

    .line 58
    iget-object v1, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    iget-object v0, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V

    .line 66
    :goto_0
    iget-object v0, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-static {v0, p2}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->a(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;Z)Z

    .line 67
    iget-object v0, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->refreshDrawableState()V

    .line 68
    iget-object v0, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->d(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V

    .line 69
    iget-object v0, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->b(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V

    .line 70
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lhox;->dnb:Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    invoke-static {v1}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->c(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method
