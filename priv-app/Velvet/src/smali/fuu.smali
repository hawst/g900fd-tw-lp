.class public final Lfuu;
.super Lfuz;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;

.field private final mIntentUtils:Leom;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Leom;Lgan;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 50
    iput-object p4, p0, Lfuu;->mIntentUtils:Leom;

    .line 51
    iput-object p5, p0, Lfuu;->mFifeImageUrlUtil:Lgan;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const v9, 0x7f0a03a2

    const/4 v10, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 56
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v4, v1, Lizj;->dSH:Lixj;

    .line 58
    new-instance v5, Lang;

    invoke-direct {v5}, Lang;-><init>()V

    .line 59
    invoke-virtual {v4}, Lixj;->baa()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lixj;->aZZ()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v4, Lixj;->dNA:Ljcn;

    if-eqz v1, :cond_0

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    iget-object v6, p0, Lfuz;->mEntry:Lizj;

    iput-object v6, v1, Lanh;->ahu:Lizj;

    const/4 v6, 0x2

    invoke-virtual {v1, v6}, Lanh;->cm(I)Lanh;

    new-instance v6, Lanz;

    invoke-direct {v6}, Lanz;-><init>()V

    iput-object v6, v1, Lanh;->agy:Lanz;

    iget-object v6, v1, Lanh;->agy:Lanz;

    new-instance v7, Laoi;

    invoke-direct {v7}, Laoi;-><init>()V

    iput-object v7, v6, Lanz;->aiU:Laoi;

    iget-object v6, v1, Lanh;->agy:Lanz;

    iget-object v6, v6, Lanz;->aiU:Laoi;

    iget-object v4, v4, Lixj;->dNA:Ljcn;

    invoke-virtual {v4}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Laoi;->bQ(Ljava/lang/String;)Laoi;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    iput-object v4, v1, Lanh;->ahu:Lizj;

    invoke-virtual {v1, v3}, Lanh;->cm(I)Lanh;

    new-instance v3, Lany;

    invoke-direct {v3}, Lany;-><init>()V

    iput-object v3, v1, Lanh;->agw:Lany;

    iget-object v3, v1, Lanh;->agw:Lany;

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lany;->bh(Ljava/lang/String;)Lany;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00bf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lanh;->cn(I)Lanh;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lanh;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    :goto_1
    iput-object v0, v5, Lang;->ags:[Lanh;

    .line 62
    invoke-virtual {v5, v2}, Lang;->aS(Z)Lang;

    .line 63
    return-object v5

    :cond_1
    move v1, v3

    .line 59
    goto :goto_0

    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    iget-object v7, p0, Lfuz;->mEntry:Lizj;

    iput-object v7, v1, Lanh;->ahu:Lizj;

    const/16 v7, 0xe

    invoke-virtual {v1, v7}, Lanh;->cm(I)Lanh;

    new-instance v7, Lanf;

    invoke-direct {v7}, Lanf;-><init>()V

    iput-object v7, v1, Lanh;->agL:Lanf;

    iget-object v7, v1, Lanh;->agL:Lanf;

    invoke-virtual {v4}, Lixj;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lanf;->al(Ljava/lang/String;)Lanf;

    iget-object v7, v4, Lixj;->aiS:Ljcn;

    if-eqz v7, :cond_3

    iget-object v7, v4, Lixj;->aiS:Ljcn;

    invoke-virtual {v7}, Ljcn;->qG()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, v1, Lanh;->agL:Lanf;

    invoke-virtual {p0, p1, v4}, Lfuu;->a(Landroid/content/Context;Lixj;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lanf;->am(Ljava/lang/String;)Lanf;

    :cond_3
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lfuu;->mIntentUtils:Leom;

    invoke-static {}, Lgaq;->aEb()Landroid/content/Intent;

    move-result-object v7

    invoke-interface {v1, p1, v7}, Leom;->e(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v4}, Lixj;->bag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lixj;->bah()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lixj;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v7, v8, v9}, Lgaq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    new-instance v7, Laoj;

    invoke-direct {v7}, Laoj;-><init>()V

    const v8, 0x7f0a03a5

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v8, Lfsa;

    const/16 v9, 0x2b

    invoke-direct {v8, v9}, Lfsa;-><init>(I)V

    const v9, 0x7f0201c1

    invoke-virtual {v8, v9}, Lfsa;->jB(I)Lfsa;

    move-result-object v8

    invoke-virtual {v1, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1, v2}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v1

    new-instance v8, Lanh;

    invoke-direct {v8}, Lanh;-><init>()V

    invoke-virtual {v8, v10}, Lanh;->cm(I)Lanh;

    iput-object v7, v8, Lanh;->agz:Laoj;

    iput-object v1, v8, Lanh;->ahs:Lani;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v8, Lanh;->ahu:Lizj;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/sidekick/shared/renderingcontext/BirthdayCardContext;->f(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/BirthdayCardContext;

    move-result-object v1

    invoke-virtual {v4}, Lixj;->bac()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v4}, Lixj;->bab()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/android/sidekick/shared/renderingcontext/BirthdayCardContext;->lU(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_5

    new-instance v7, Laoj;

    invoke-direct {v7}, Laoj;-><init>()V

    const v0, 0x7f0a0601

    new-array v8, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lixj;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-virtual {p1, v0, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v0, Lfsa;

    const/16 v3, 0x2c

    invoke-direct {v0, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f02010d

    invoke-virtual {v0, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v3, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v1

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    invoke-virtual {v0, v10}, Lanh;->cm(I)Lanh;

    iput-object v7, v0, Lanh;->agz:Laoj;

    iput-object v1, v0, Lanh;->ahs:Lani;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    :cond_5
    if-eqz v0, :cond_6

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v4}, Lixj;->bad()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v4}, Lixj;->getPhone()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/android/sidekick/shared/renderingcontext/BirthdayCardContext;->lU(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_8
    move-object v1, v0

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Lixj;)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v3, 0x7f0d0165

    .line 209
    iget-object v0, p2, Lixj;->aiS:Ljcn;

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p2, Lixj;->aiS:Ljcn;

    .line 211
    invoke-virtual {v0}, Ljcn;->bgJ()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 215
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 217
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 219
    iget-object v3, p0, Lfuu;->mFifeImageUrlUtil:Lgan;

    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v2, v0}, Lgan;->b(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    :cond_0
    :goto_0
    return-object v0

    .line 221
    :cond_1
    invoke-virtual {v0}, Ljcn;->bgJ()I

    move-result v1

    if-nez v1, :cond_2

    .line 223
    invoke-virtual {v0}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 224
    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 230
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
