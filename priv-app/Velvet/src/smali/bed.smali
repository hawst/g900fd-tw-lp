.class public abstract Lbed;
.super Lbeg;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lbeg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Lbem;)I
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v7, 0x0

    .line 142
    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v10, v0

    .line 145
    :goto_0
    invoke-virtual {p0}, Lbed;->xD()Lbdx;

    move-result-object v0

    check-cast v0, Lbdw;

    invoke-interface {v0}, Lbdw;->xu()[Lbem;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not all tables in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " were registered when the AppDataSearchProvider was created."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v10, v0

    goto :goto_0

    .line 150
    :cond_1
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 151
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbem;

    .line 152
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "content"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lbed;->xA()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "appdatasearch"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lbem;->xF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, v0, Lbem;->axX:Ljava/util/List;

    iget-object v5, v0, Lbem;->axY:[I

    iget-boolean v1, v0, Lbem;->aya:Z

    if-eqz v1, :cond_4

    new-array v1, v13, [Lcom/google/android/gms/appdatasearch/Feature;

    const/4 v2, 0x0

    new-instance v6, Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v6, v13}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    aput-object v6, v1, v2

    move-object v2, v1

    :goto_2
    if-eqz v5, :cond_2

    new-instance v1, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    invoke-direct {v1, v5, v2}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;-><init>([I[Lcom/google/android/gms/appdatasearch/Feature;)V

    :goto_3
    invoke-virtual {v0}, Lbem;->xF()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->eN(Ljava/lang/String;)Lbcf;

    move-result-object v2

    iget-object v5, v0, Lbem;->axW:Ljava/lang/String;

    iput-object v5, v2, Lbcf;->auJ:Ljava/lang/String;

    iput-object v3, v2, Lbcf;->awp:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Lbcf;->a(Ljava/lang/Iterable;)Lbcf;

    move-result-object v6

    iput-object v1, v6, Lbcf;->awr:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v0, v0, Lbem;->axZ:Z

    iput-boolean v0, v6, Lbcf;->aws:Z

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v1, v6, Lbcf;->mName:Ljava/lang/String;

    iget-object v2, v6, Lbcf;->auJ:Ljava/lang/String;

    iget-object v3, v6, Lbcf;->awp:Landroid/net/Uri;

    iget-object v4, v6, Lbcf;->awq:Ljava/util/List;

    iget-object v5, v6, Lbcf;->awq:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v5, v6, Lbcf;->awr:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v6, v6, Lbcf;->aws:Z

    move-object v8, v7

    move-object v9, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;ZLandroid/accounts/Account;Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_2
    move-object v1, v7

    goto :goto_3

    .line 154
    :cond_3
    invoke-virtual {p0}, Lbed;->xx()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    .line 156
    new-instance v1, Lbef;

    invoke-direct {v1, p0, v11, v0, v10}, Lbef;-><init>(Lbed;Ljava/util/Set;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Ljava/util/List;)V

    .line 171
    const-string v0, "registerCorpora"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lbed;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_4
    move-object v2, v7

    goto :goto_2
.end method

.method protected abstract a(Lbdy;)Lbdw;
.end method

.method protected final b(Lbdy;)Lbdx;
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lbed;->a(Lbdy;)Lbdw;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Lbeg;->onCreate()Z

    move-result v0

    .line 66
    invoke-virtual {p0}, Lbed;->xz()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lbed;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lbgt;->T(Landroid/content/Context;)I

    move-result v1

    .line 68
    if-nez v1, :cond_0

    .line 69
    new-instance v1, Lbee;

    invoke-direct {v1, p0}, Lbee;-><init>(Lbed;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lbee;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    :cond_0
    return v0
.end method

.method protected abstract xx()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
.end method

.method public final xy()Lbdw;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lbed;->xD()Lbdx;

    move-result-object v0

    check-cast v0, Lbdw;

    return-object v0
.end method

.method protected xz()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method
