.class public abstract Lqz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lrj;


# static fields
.field private static wt:Ljava/util/WeakHashMap;

.field private static wu:Ljava/lang/ThreadLocal;


# instance fields
.field protected ag:I

.field protected cs:I

.field protected ny:I

.field protected wo:I

.field private wp:I

.field private wq:I

.field wr:Z

.field private ws:Lrb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lqz;->wt:Ljava/util/WeakHashMap;

    .line 55
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lqz;->wu:Ljava/lang/ThreadLocal;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lqz;-><init>(Lrb;II)V

    .line 68
    return-void
.end method

.method protected constructor <init>(Lrb;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput v0, p0, Lqz;->cs:I

    .line 44
    iput v0, p0, Lqz;->ny:I

    .line 45
    iput v0, p0, Lqz;->wo:I

    .line 52
    iput-object v1, p0, Lqz;->ws:Lrb;

    .line 58
    iput-object v1, p0, Lqz;->ws:Lrb;

    .line 59
    iput v2, p0, Lqz;->cs:I

    .line 60
    iput v2, p0, Lqz;->ag:I

    .line 61
    sget-object v1, Lqz;->wt:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 62
    :try_start_0
    sget-object v0, Lqz;->wt:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static dL()V
    .locals 4

    .prologue
    .line 205
    sget-object v1, Lqz;->wt:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 206
    :try_start_0
    sget-object v0, Lqz;->wt:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqz;

    .line 207
    const/4 v3, 0x0

    iput v3, v0, Lqz;->ag:I

    .line 208
    const/4 v3, 0x0

    iput-object v3, v0, Lqz;->ws:Lrb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method protected final a(Lrb;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lqz;->ws:Lrb;

    .line 72
    return-void
.end method

.method public final a(Lrb;IIII)V
    .locals 6

    .prologue
    .line 143
    move-object v0, p1

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lrb;->a(Lqz;IIII)V

    .line 144
    return-void
.end method

.method protected abstract b(Lrb;)Z
.end method

.method public dG()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public dH()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lqz;->wp:I

    return v0
.end method

.method public dI()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lqz;->wq:I

    return v0
.end method

.method public final dJ()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lqz;->wr:Z

    return v0
.end method

.method protected abstract dK()I
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 184
    sget-object v0, Lqz;->wu:Ljava/lang/ThreadLocal;

    const-class v1, Lqz;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 185
    invoke-virtual {p0}, Lqz;->recycle()V

    .line 186
    sget-object v0, Lqz;->wu:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 187
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lqz;->wo:I

    return v0
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lqz;->cs:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lqz;->ny:I

    return v0
.end method

.method public final isLoaded()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 154
    iget v1, p0, Lqz;->ag:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycle()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 160
    iget-object v0, p0, Lqz;->ws:Lrb;

    if-eqz v0, :cond_0

    iget v1, p0, Lqz;->cs:I

    if-eq v1, v2, :cond_0

    invoke-interface {v0, p0}, Lrb;->a(Lqz;)Z

    iput v2, p0, Lqz;->cs:I

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lqz;->ag:I

    const/4 v0, 0x0

    iput-object v0, p0, Lqz;->ws:Lrb;

    .line 161
    return-void
.end method

.method public final setSize(II)V
    .locals 5

    .prologue
    const/16 v2, 0x1000

    const/4 v1, 0x0

    .line 79
    iput p1, p0, Lqz;->ny:I

    .line 80
    iput p2, p0, Lqz;->wo:I

    .line 81
    if-lez p1, :cond_2

    invoke-static {p1}, Lqm;->ay(I)I

    move-result v0

    :goto_0
    iput v0, p0, Lqz;->wp:I

    .line 82
    if-lez p2, :cond_3

    invoke-static {p2}, Lqm;->ay(I)I

    move-result v0

    :goto_1
    iput v0, p0, Lqz;->wq:I

    .line 83
    iget v0, p0, Lqz;->wp:I

    if-gt v0, v2, :cond_0

    iget v0, p0, Lqz;->wq:I

    if-le v0, v2, :cond_1

    .line 84
    :cond_0
    const-string v0, "BasicTexture"

    const-string v2, "texture is too large: %d x %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lqz;->wp:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x1

    iget v4, p0, Lqz;->wq:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 87
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 81
    goto :goto_0

    :cond_3
    move v0, v1

    .line 82
    goto :goto_1
.end method
