.class public final Lfnh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lesm;


# instance fields
.field Kl:Z

.field private final blv:Lerp;

.field private final cwO:Ljava/util/concurrent/Executor;

.field final cwP:Z

.field final cwQ:Ljava/lang/Object;

.field final cwR:Ljava/util/Map;

.field final mNowRemoteClient:Lfml;

.field final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lerp;Ljava/util/concurrent/Executor;Landroid/content/res/Resources;Lfml;Z)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfnh;->cwQ:Ljava/lang/Object;

    .line 41
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lfnh;->cwR:Ljava/util/Map;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfnh;->Kl:Z

    .line 51
    iput-object p1, p0, Lfnh;->blv:Lerp;

    .line 52
    iput-object p2, p0, Lfnh;->cwO:Ljava/util/concurrent/Executor;

    .line 53
    iput-object p3, p0, Lfnh;->mResources:Landroid/content/res/Resources;

    .line 54
    iput-object p4, p0, Lfnh;->mNowRemoteClient:Lfml;

    .line 55
    iput-boolean p5, p0, Lfnh;->cwP:Z

    .line 56
    return-void
.end method

.method private L(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 122
    iget-boolean v0, p0, Lfnh;->Kl:Z

    if-eqz v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lfnh;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-boolean v0, p0, Lfnh;->cwP:Z

    if-nez v0, :cond_2

    .line 135
    iget-object v0, p0, Lfnh;->mNowRemoteClient:Lfml;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "load-image-uri"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lfml;->f(ILandroid/os/Bundle;)V

    .line 138
    :cond_2
    new-instance v0, Lfni;

    iget-object v1, p0, Lfnh;->blv:Lerp;

    invoke-interface {v1}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Lfnh;->cwO:Ljava/util/concurrent/Executor;

    invoke-direct {v0, p0, v1, v2, p1}, Lfni;-><init>(Lfnh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/net/Uri;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfni;->a([Ljava/lang/Object;)Lenp;

    goto :goto_0
.end method


# virtual methods
.method public final D(Landroid/net/Uri;)Leml;
    .locals 3

    .prologue
    .line 72
    iget-object v1, p0, Lfnh;->cwQ:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lfnh;->cwR:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnj;

    .line 74
    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lfnj;

    invoke-direct {v0, p0}, Lfnj;-><init>(Lfnh;)V

    .line 76
    iget-object v2, p0, Lfnh;->cwR:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-direct {p0, p1}, Lfnh;->L(Landroid/net/Uri;)V

    .line 79
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final clearCache()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public final resume()V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfnh;->Kl:Z

    .line 100
    iget-object v1, p0, Lfnh;->cwQ:Ljava/lang/Object;

    monitor-enter v1

    .line 102
    :try_start_0
    iget-object v0, p0, Lfnh;->cwR:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 103
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 105
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnj;

    invoke-virtual {v0}, Lfnj;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 114
    :cond_1
    :try_start_1
    iget-object v0, p0, Lfnh;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    invoke-virtual {p0}, Lfnh;->uK()V

    .line 117
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    return v0
.end method

.method public final uK()V
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lfnh;->cwQ:Ljava/lang/Object;

    monitor-enter v1

    .line 85
    :try_start_0
    iget-object v0, p0, Lfnh;->cwR:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 87
    invoke-direct {p0, v0}, Lfnh;->L(Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
