.class public final Lfwj;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfyj;


# instance fields
.field private final cCF:Ljal;

.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 43
    iget-object v0, p1, Lizj;->dTj:Ljal;

    iput-object v0, p0, Lfwj;->cCF:Ljal;

    .line 44
    iput-object p4, p0, Lfwj;->mTimeToLeaveFactory:Lfyk;

    .line 45
    iput-object p5, p0, Lfwj;->mStringEvaluator:Lgbr;

    .line 46
    return-void
.end method

.method private bh(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 126
    iget-object v0, p0, Lfwj;->cCF:Ljal;

    invoke-virtual {v0}, Ljal;->bek()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    .line 128
    const v6, 0x10010

    move-object v1, p1

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bi(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 133
    iget-object v0, p0, Lfwj;->cCF:Ljal;

    invoke-virtual {v0}, Ljal;->bek()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    .line 135
    const/4 v6, 0x1

    move-object v1, p1

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final M(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lfwj;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {v0}, Lgbf;->a(Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f0201e1

    const/16 v9, 0x6f

    const/4 v8, 0x1

    .line 50
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lftf;

    iget-object v0, p0, Lfwj;->cCF:Ljal;

    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    iget-object v5, p0, Lfuz;->mCardContainer:Lfmt;

    iget-object v6, p0, Lfwj;->mStringEvaluator:Lgbr;

    invoke-direct {v3, v0, v4, v5, v6}, Lftf;-><init>(Ljal;Lizj;Lfmt;Lgbr;)V

    iget-object v0, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfwj;->mTimeToLeaveFactory:Lfyk;

    invoke-virtual {v3, p1, v0, v4}, Lftf;->a(Landroid/content/Context;Lemp;Lfyk;)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfwj;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-virtual {v3}, Lftf;->aDp()Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfwj;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-virtual {v3, p1}, Lftf;->aR(Landroid/content/Context;)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfwj;->a(Ljava/util/List;Ljava/lang/Object;)V

    iget-object v0, p0, Lfwj;->cCF:Ljal;

    iget-object v4, v0, Ljal;->dWL:Ljak;

    new-instance v5, Laou;

    invoke-direct {v5}, Laou;-><init>()V

    const v0, 0x7f0a035a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    invoke-virtual {v4}, Ljak;->beh()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Ljak;->beg()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v6, "www."

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5, v0}, Laou;->ct(Ljava/lang/String;)Laou;

    :cond_0
    new-instance v0, Lfyg;

    invoke-direct {v0, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0a035b

    invoke-direct {p0, p1}, Lfwj;->bh(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    const v6, 0x7f0a035c

    invoke-direct {p0, p1}, Lfwj;->bi(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    new-array v6, v8, [Laow;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    iput-object v6, v5, Laou;->akm:[Laow;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v6, 0x10

    invoke-virtual {v0, v6}, Lanh;->cm(I)Lanh;

    iget-object v6, p0, Lfuz;->mEntry:Lizj;

    iput-object v6, v0, Lanh;->ahu:Lizj;

    iput-object v5, v0, Lanh;->agN:Laou;

    invoke-virtual {v4}, Ljak;->bdT()Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, Lfsa;

    const/16 v6, 0xcf

    invoke-direct {v5, v6}, Lfsa;-><init>(I)V

    invoke-virtual {v5, v10}, Lfsa;->jB(I)Lfsa;

    move-result-object v5

    invoke-virtual {v4}, Ljak;->bdS()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4, v11}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v4

    iput-object v4, v0, Lanh;->ahs:Lani;

    :cond_1
    :goto_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-virtual {v3, p1, v0, v8}, Lftf;->a(Landroid/content/Context;Lfmt;Z)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfwj;->a(Ljava/util/List;Ljava/lang/Object;)V

    iget-object v0, p0, Lfwj;->mEntry:Lizj;

    iget-object v3, p0, Lfwj;->cCF:Ljal;

    iget-object v3, v3, Ljal;->dWL:Ljak;

    iget-object v3, v3, Ljak;->dMX:[Ljbg;

    invoke-static {p1, v0, v3, v9}, Lfsx;->a(Landroid/content/Context;Lizj;[Ljbg;I)Lanh;

    move-result-object v0

    invoke-static {v2, v0}, Lfwj;->a(Ljava/util/List;Ljava/lang/Object;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 52
    invoke-virtual {v1, v8}, Lang;->aS(Z)Lang;

    .line 53
    return-object v1

    .line 51
    :cond_2
    invoke-static {v4}, Lgbf;->b(Ljak;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v4}, Ljak;->beh()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Lfsa;

    invoke-direct {v5, v9}, Lfsa;-><init>(I)V

    invoke-virtual {v5, v10}, Lfsa;->jB(I)Lfsa;

    move-result-object v5

    invoke-virtual {v4}, Ljak;->beg()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4, v11}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v4

    iput-object v4, v0, Lanh;->ahs:Lani;

    goto :goto_1
.end method

.method public final aDC()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lfwj;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {v0}, Lgbf;->b(Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bd(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lfwj;->cCF:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-static {p1, v0}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final be(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lfwj;->bh(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-direct {p0, p1}, Lfwj;->bi(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 149
    const-string v2, " \u00b7 "

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
