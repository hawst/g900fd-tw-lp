.class public final Ljvu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eHU:Ljuc;

.field private eHV:Ljuc;

.field private eHW:Ljuc;

.field private eHX:[Ljuf;

.field public eIb:[Ljvt;

.field private eIc:J

.field private eId:J

.field private eIe:Ljvl;

.field private eIf:F

.field private eIg:Z

.field private eIh:J

.field private eIi:J

.field private eIj:[B

.field private eIk:[Ljvs;

.field private eIl:[Ljvz;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2230
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2231
    iput v4, p0, Ljvu;->aez:I

    invoke-static {}, Ljvt;->bve()[Ljvt;

    move-result-object v0

    iput-object v0, p0, Ljvu;->eIb:[Ljvt;

    iput-wide v2, p0, Ljvu;->eIc:J

    iput-wide v2, p0, Ljvu;->eId:J

    iput-object v1, p0, Ljvu;->eHW:Ljuc;

    iput-object v1, p0, Ljvu;->eHV:Ljuc;

    iput-object v1, p0, Ljvu;->eHU:Ljuc;

    invoke-static {}, Ljuf;->buF()[Ljuf;

    move-result-object v0

    iput-object v0, p0, Ljvu;->eHX:[Ljuf;

    iput-object v1, p0, Ljvu;->eIe:Ljvl;

    const/4 v0, 0x0

    iput v0, p0, Ljvu;->eIf:F

    iput-boolean v4, p0, Ljvu;->eIg:Z

    iput-wide v2, p0, Ljvu;->eIh:J

    iput-wide v2, p0, Ljvu;->eIi:J

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljvu;->eIj:[B

    invoke-static {}, Ljvs;->bvd()[Ljvs;

    move-result-object v0

    iput-object v0, p0, Ljvu;->eIk:[Ljvs;

    invoke-static {}, Ljvz;->bvh()[Ljvz;

    move-result-object v0

    iput-object v0, p0, Ljvu;->eIl:[Ljvz;

    iput-object v1, p0, Ljvu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvu;->eCz:I

    .line 2232
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2051
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvu;->eIb:[Ljvt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljvt;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljvu;->eIb:[Ljvt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljvt;

    invoke-direct {v3}, Ljvt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljvu;->eIb:[Ljvt;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljvt;

    invoke-direct {v3}, Ljvt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvu;->eIb:[Ljvt;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvu;->eIc:J

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvu;->eId:J

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvu;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljvu;->eHW:Ljuc;

    if-nez v0, :cond_4

    new-instance v0, Ljuc;

    invoke-direct {v0}, Ljuc;-><init>()V

    iput-object v0, p0, Ljvu;->eHW:Ljuc;

    :cond_4
    iget-object v0, p0, Ljvu;->eHW:Ljuc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljvu;->eHV:Ljuc;

    if-nez v0, :cond_5

    new-instance v0, Ljuc;

    invoke-direct {v0}, Ljuc;-><init>()V

    iput-object v0, p0, Ljvu;->eHV:Ljuc;

    :cond_5
    iget-object v0, p0, Ljvu;->eHV:Ljuc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvu;->eHX:[Ljuf;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljuf;

    if-eqz v0, :cond_6

    iget-object v3, p0, Ljvu;->eHX:[Ljuf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Ljuf;

    invoke-direct {v3}, Ljuf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Ljvu;->eHX:[Ljuf;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Ljuf;

    invoke-direct {v3}, Ljuf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvu;->eHX:[Ljuf;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljvu;->eIe:Ljvl;

    if-nez v0, :cond_9

    new-instance v0, Ljvl;

    invoke-direct {v0}, Ljvl;-><init>()V

    iput-object v0, p0, Ljvu;->eIe:Ljvl;

    :cond_9
    iget-object v0, p0, Ljvu;->eIe:Ljvl;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljvu;->eIf:F

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvu;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljvu;->eIg:Z

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljvu;->aez:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljvu;->eHU:Ljuc;

    if-nez v0, :cond_a

    new-instance v0, Ljuc;

    invoke-direct {v0}, Ljuc;-><init>()V

    iput-object v0, p0, Ljvu;->eHU:Ljuc;

    :cond_a
    iget-object v0, p0, Ljvu;->eHU:Ljuc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvu;->eIh:J

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljvu;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvu;->eIi:J

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljvu;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljvu;->eIj:[B

    iget v0, p0, Ljvu;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljvu;->aez:I

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x72

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvu;->eIk:[Ljvs;

    if-nez v0, :cond_c

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljvs;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljvu;->eIk:[Ljvs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljvs;

    invoke-direct {v3}, Ljvs;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_c
    iget-object v0, p0, Ljvu;->eIk:[Ljvs;

    array-length v0, v0

    goto :goto_5

    :cond_d
    new-instance v3, Ljvs;

    invoke-direct {v3}, Ljvs;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvu;->eIk:[Ljvs;

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvu;->eIl:[Ljvz;

    if-nez v0, :cond_f

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ljvz;

    if-eqz v0, :cond_e

    iget-object v3, p0, Ljvu;->eIl:[Ljvz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    new-instance v3, Ljvz;

    invoke-direct {v3}, Ljvz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_f
    iget-object v0, p0, Ljvu;->eIl:[Ljvz;

    array-length v0, v0

    goto :goto_7

    :cond_10
    new-instance v3, Ljvz;

    invoke-direct {v3}, Ljvz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvu;->eIl:[Ljvz;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x45 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2259
    iget-object v0, p0, Ljvu;->eIb:[Ljvt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljvu;->eIb:[Ljvt;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2260
    :goto_0
    iget-object v2, p0, Ljvu;->eIb:[Ljvt;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2261
    iget-object v2, p0, Ljvu;->eIb:[Ljvt;

    aget-object v2, v2, v0

    .line 2262
    if-eqz v2, :cond_0

    .line 2263
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 2260
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2267
    :cond_1
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 2268
    const/4 v0, 0x2

    iget-wide v2, p0, Ljvu;->eIc:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2270
    :cond_2
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 2271
    const/4 v0, 0x3

    iget-wide v2, p0, Ljvu;->eId:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2273
    :cond_3
    iget-object v0, p0, Ljvu;->eHW:Ljuc;

    if-eqz v0, :cond_4

    .line 2274
    const/4 v0, 0x4

    iget-object v2, p0, Ljvu;->eHW:Ljuc;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 2276
    :cond_4
    iget-object v0, p0, Ljvu;->eHV:Ljuc;

    if-eqz v0, :cond_5

    .line 2277
    const/4 v0, 0x5

    iget-object v2, p0, Ljvu;->eHV:Ljuc;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 2279
    :cond_5
    iget-object v0, p0, Ljvu;->eHX:[Ljuf;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljvu;->eHX:[Ljuf;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 2280
    :goto_1
    iget-object v2, p0, Ljvu;->eHX:[Ljuf;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 2281
    iget-object v2, p0, Ljvu;->eHX:[Ljuf;

    aget-object v2, v2, v0

    .line 2282
    if-eqz v2, :cond_6

    .line 2283
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 2280
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2287
    :cond_7
    iget-object v0, p0, Ljvu;->eIe:Ljvl;

    if-eqz v0, :cond_8

    .line 2288
    const/4 v0, 0x7

    iget-object v2, p0, Ljvu;->eIe:Ljvl;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 2290
    :cond_8
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_9

    .line 2291
    const/16 v0, 0x8

    iget v2, p0, Ljvu;->eIf:F

    invoke-virtual {p1, v0, v2}, Ljsj;->a(IF)V

    .line 2293
    :cond_9
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_a

    .line 2294
    const/16 v0, 0x9

    iget-boolean v2, p0, Ljvu;->eIg:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 2296
    :cond_a
    iget-object v0, p0, Ljvu;->eHU:Ljuc;

    if-eqz v0, :cond_b

    .line 2297
    const/16 v0, 0xa

    iget-object v2, p0, Ljvu;->eHU:Ljuc;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 2299
    :cond_b
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_c

    .line 2300
    const/16 v0, 0xb

    iget-wide v2, p0, Ljvu;->eIh:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2302
    :cond_c
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_d

    .line 2303
    const/16 v0, 0xc

    iget-wide v2, p0, Ljvu;->eIi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 2305
    :cond_d
    iget v0, p0, Ljvu;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_e

    .line 2306
    const/16 v0, 0xd

    iget-object v2, p0, Ljvu;->eIj:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    .line 2308
    :cond_e
    iget-object v0, p0, Ljvu;->eIk:[Ljvs;

    if-eqz v0, :cond_10

    iget-object v0, p0, Ljvu;->eIk:[Ljvs;

    array-length v0, v0

    if-lez v0, :cond_10

    move v0, v1

    .line 2309
    :goto_2
    iget-object v2, p0, Ljvu;->eIk:[Ljvs;

    array-length v2, v2

    if-ge v0, v2, :cond_10

    .line 2310
    iget-object v2, p0, Ljvu;->eIk:[Ljvs;

    aget-object v2, v2, v0

    .line 2311
    if-eqz v2, :cond_f

    .line 2312
    const/16 v3, 0xe

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 2309
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2316
    :cond_10
    iget-object v0, p0, Ljvu;->eIl:[Ljvz;

    if-eqz v0, :cond_12

    iget-object v0, p0, Ljvu;->eIl:[Ljvz;

    array-length v0, v0

    if-lez v0, :cond_12

    .line 2317
    :goto_3
    iget-object v0, p0, Ljvu;->eIl:[Ljvz;

    array-length v0, v0

    if-ge v1, v0, :cond_12

    .line 2318
    iget-object v0, p0, Ljvu;->eIl:[Ljvz;

    aget-object v0, v0, v1

    .line 2319
    if-eqz v0, :cond_11

    .line 2320
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 2317
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2324
    :cond_12
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2325
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2329
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2330
    iget-object v2, p0, Ljvu;->eIb:[Ljvt;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljvu;->eIb:[Ljvt;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 2331
    :goto_0
    iget-object v3, p0, Ljvu;->eIb:[Ljvt;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 2332
    iget-object v3, p0, Ljvu;->eIb:[Ljvt;

    aget-object v3, v3, v0

    .line 2333
    if-eqz v3, :cond_0

    .line 2334
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2331
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2339
    :cond_2
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 2340
    const/4 v2, 0x2

    iget-wide v4, p0, Ljvu;->eIc:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2343
    :cond_3
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    .line 2344
    const/4 v2, 0x3

    iget-wide v4, p0, Ljvu;->eId:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2347
    :cond_4
    iget-object v2, p0, Ljvu;->eHW:Ljuc;

    if-eqz v2, :cond_5

    .line 2348
    const/4 v2, 0x4

    iget-object v3, p0, Ljvu;->eHW:Ljuc;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2351
    :cond_5
    iget-object v2, p0, Ljvu;->eHV:Ljuc;

    if-eqz v2, :cond_6

    .line 2352
    const/4 v2, 0x5

    iget-object v3, p0, Ljvu;->eHV:Ljuc;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2355
    :cond_6
    iget-object v2, p0, Ljvu;->eHX:[Ljuf;

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljvu;->eHX:[Ljuf;

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v0

    move v0, v1

    .line 2356
    :goto_1
    iget-object v3, p0, Ljvu;->eHX:[Ljuf;

    array-length v3, v3

    if-ge v0, v3, :cond_8

    .line 2357
    iget-object v3, p0, Ljvu;->eHX:[Ljuf;

    aget-object v3, v3, v0

    .line 2358
    if-eqz v3, :cond_7

    .line 2359
    const/4 v4, 0x6

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2356
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    move v0, v2

    .line 2364
    :cond_9
    iget-object v2, p0, Ljvu;->eIe:Ljvl;

    if-eqz v2, :cond_a

    .line 2365
    const/4 v2, 0x7

    iget-object v3, p0, Ljvu;->eIe:Ljvl;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2368
    :cond_a
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_b

    .line 2369
    const/16 v2, 0x8

    iget v3, p0, Ljvu;->eIf:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 2372
    :cond_b
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_c

    .line 2373
    const/16 v2, 0x9

    iget-boolean v3, p0, Ljvu;->eIg:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2376
    :cond_c
    iget-object v2, p0, Ljvu;->eHU:Ljuc;

    if-eqz v2, :cond_d

    .line 2377
    const/16 v2, 0xa

    iget-object v3, p0, Ljvu;->eHU:Ljuc;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2380
    :cond_d
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_e

    .line 2381
    const/16 v2, 0xb

    iget-wide v4, p0, Ljvu;->eIh:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2384
    :cond_e
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_f

    .line 2385
    const/16 v2, 0xc

    iget-wide v4, p0, Ljvu;->eIi:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2388
    :cond_f
    iget v2, p0, Ljvu;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_10

    .line 2389
    const/16 v2, 0xd

    iget-object v3, p0, Ljvu;->eIj:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 2392
    :cond_10
    iget-object v2, p0, Ljvu;->eIk:[Ljvs;

    if-eqz v2, :cond_13

    iget-object v2, p0, Ljvu;->eIk:[Ljvs;

    array-length v2, v2

    if-lez v2, :cond_13

    move v2, v0

    move v0, v1

    .line 2393
    :goto_2
    iget-object v3, p0, Ljvu;->eIk:[Ljvs;

    array-length v3, v3

    if-ge v0, v3, :cond_12

    .line 2394
    iget-object v3, p0, Ljvu;->eIk:[Ljvs;

    aget-object v3, v3, v0

    .line 2395
    if-eqz v3, :cond_11

    .line 2396
    const/16 v4, 0xe

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2393
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_12
    move v0, v2

    .line 2401
    :cond_13
    iget-object v2, p0, Ljvu;->eIl:[Ljvz;

    if-eqz v2, :cond_15

    iget-object v2, p0, Ljvu;->eIl:[Ljvz;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 2402
    :goto_3
    iget-object v2, p0, Ljvu;->eIl:[Ljvz;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 2403
    iget-object v2, p0, Ljvu;->eIl:[Ljvz;

    aget-object v2, v2, v1

    .line 2404
    if-eqz v2, :cond_14

    .line 2405
    const/16 v3, 0xf

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2402
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2410
    :cond_15
    return v0
.end method
