.class public final Larl;
.super Larc;
.source "PG"


# instance fields
.field private mLoginHelper:Lcrh;

.field private mSearchHistoryHelper:Lcrr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Larc;-><init>()V

    return-void
.end method

.method static synthetic a(Larl;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Larl;->cZ(I)V

    return-void
.end method

.method public static bs(Z)Larl;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Larl;

    invoke-direct {v0}, Larl;-><init>()V

    .line 37
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 38
    const-string v2, "always_on_hotword"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 39
    invoke-virtual {v0, v1}, Larl;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v0
.end method

.method private cZ(I)V
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Larl;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ay:Lq;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 124
    :cond_0
    return-void
.end method


# virtual methods
.method final a(Lemy;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 127
    iget-object v0, p0, Larl;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 128
    if-nez v0, :cond_0

    .line 129
    const-string v1, "OptinScreenFragment"

    const-string v2, "No account found, can\'t enable Audio History"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 130
    const v1, 0x7f0a059f

    invoke-direct {p0, v1}, Larl;->cZ(I)V

    .line 131
    invoke-virtual {p0, v4}, Larl;->bp(Z)V

    .line 134
    :cond_0
    iget-object v1, p0, Larl;->mSearchHistoryHelper:Lcrr;

    new-instance v2, Larp;

    invoke-direct {v2, p0, p1}, Larp;-><init>(Larl;Lemy;)V

    invoke-virtual {v1, v0, v5, v5, v2}, Lcrr;->a(Landroid/accounts/Account;ZZLemy;)V

    .line 148
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1}, Larc;->onAttach(Landroid/app/Activity;)V

    .line 115
    const/16 v0, 0x124

    invoke-virtual {p0, v0}, Larl;->db(I)V

    .line 117
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Larc;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJR()Lcgh;

    move-result-object v0

    invoke-interface {v0}, Lcgh;->EU()Lcrr;

    move-result-object v0

    iput-object v0, p0, Larl;->mSearchHistoryHelper:Lcrr;

    .line 50
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Larl;->mLoginHelper:Lcrh;

    .line 51
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 56
    const v0, 0x7f04006b

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 58
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->am:Landroid/os/Bundle;

    const-string v1, "always_on_hotword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 60
    const v0, 0x7f11018b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    if-eqz v3, :cond_0

    const v1, 0x7f0a06f3

    :goto_0
    invoke-virtual {p0, v1}, Larl;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const v0, 0x7f11018c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 66
    if-eqz v3, :cond_1

    const v1, 0x7f0a06f5

    :goto_1
    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {}, Larl;->tR()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v1, v3}, Larl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    const v0, 0x7f11018d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    const v1, 0x7f0a06f7

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {}, Larl;->tR()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v1, v3}, Larl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    const v0, 0x7f11018e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 75
    const v1, 0x7f0a010f

    invoke-virtual {p0, v1}, Larl;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 76
    const v3, 0x7f0a06f8

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Larl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 80
    const v0, 0x7f110190

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Larm;

    invoke-direct {v1, p0}, Larm;-><init>(Larl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v0, 0x7f11018f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Laro;

    invoke-direct {v1, p0}, Laro;-><init>(Larl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    return-object v2

    .line 61
    :cond_0
    const v1, 0x7f0a06f2

    goto/16 :goto_0

    .line 66
    :cond_1
    const v1, 0x7f0a06f4

    goto :goto_1
.end method
