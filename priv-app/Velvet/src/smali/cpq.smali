.class public Lcpq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private volatile aZM:Ljava/util/concurrent/Future;

.field private final bfC:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile bfD:Leeb;

.field private volatile bfE:Ljava/util/concurrent/Future;

.field private bfF:Ljava/util/concurrent/LinkedBlockingDeque;

.field private bfG:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mContext:Landroid/content/Context;

.field private final mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mGsaConfig:Lchk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lchk;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcpq;->bfC:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 77
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcpq;->bfG:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 81
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 82
    iput-object p1, p0, Lcpq;->mContext:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lcpq;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 84
    iput-object p3, p0, Lcpq;->mGsaConfig:Lchk;

    .line 85
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lchk;)Lcpq;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcpq;

    invoke-direct {v0, p0, p1, p2}, Lcpq;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lchk;)V

    return-object v0
.end method


# virtual methods
.method public PE()Leeb;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcpq;->bfD:Leeb;

    return-object v0
.end method

.method public PF()Ljava/util/List;
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 151
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpp;

    .line 152
    iget-object v0, v0, Lcpp;->content:[B

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :cond_0
    return-object v1
.end method

.method public PG()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    return v0
.end method

.method public PH()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 211
    iget-object v0, p0, Lcpq;->bfC:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 212
    return-void
.end method

.method public PI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 220
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpp;

    .line 221
    iget-object v0, v0, Lcpp;->bfB:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    :cond_0
    const-string v0, ","

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    invoke-virtual {v0}, Lifj;->aVV()Lifj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public PJ()V
    .locals 4

    .prologue
    .line 253
    iget-object v0, p0, Lcpq;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Ki()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcpq;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcps;

    const-string v2, "Initialize cache with contents from flash"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lcps;-><init>(Lcpq;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcpq;->aZM:Ljava/util/concurrent/Future;

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public PK()V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcpq;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Ki()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcpq;->bfG:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lcpq;->bfE:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcpq;->bfE:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    .line 237
    iget-object v0, p0, Lcpq;->bfE:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 239
    :cond_2
    iget-object v0, p0, Lcpq;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcpr;

    const-string v2, "Persist static contents to flash"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lcpr;-><init>(Lcpq;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcpq;->bfE:Ljava/util/concurrent/Future;

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final Se()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 283
    new-instance v3, Lcpu;

    invoke-direct {v3}, Lcpu;-><init>()V

    iget-object v1, p0, Lcpq;->bfD:Leeb;

    invoke-static {v1}, Leeb;->c(Leeb;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcpu;->bfI:Ljava/lang/String;

    iget-object v1, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v1

    new-array v4, v1, [Lcpp;

    iget-object v1, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpp;

    aput-object v0, v4, v1

    move v1, v2

    goto :goto_0

    :cond_0
    iput-object v4, v3, Lcpu;->bfJ:[Lcpp;

    .line 284
    const/4 v1, 0x0

    .line 286
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcpq;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "scc.ser"

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    :try_start_1
    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 289
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 290
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 297
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 298
    :goto_1
    return-void

    .line 292
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    :try_start_2
    const-string v1, "StaticContentCache"

    const-string v2, "Error saving the static contents to flash"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 295
    iget-object v1, p0, Lcpq;->bfG:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 297
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_3

    .line 292
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public final Sf()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v0, 0x0

    const/16 v8, 0x15f

    .line 303
    new-instance v3, Ljava/io/File;

    iget-object v1, p0, Lcpq;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "scc.ser"

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 304
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v1, v4, v10

    if-gtz v1, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    const/4 v2, 0x0

    .line 311
    new-instance v4, Legl;

    sget-object v1, Leoi;->cgG:Leoi;

    invoke-virtual {v1}, Leoi;->auU()J

    move-result-wide v6

    invoke-direct {v4, v10, v11, v6, v7}, Legl;-><init>(JJ)V

    .line 313
    const/16 v1, 0x15e

    :try_start_0
    invoke-static {v1, v4}, Lege;->a(ILegl;)V

    .line 315
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-int v2, v2

    new-array v2, v2, [B

    .line 317
    invoke-virtual {v1, v2}, Ljava/io/FileInputStream;->read([B)I

    .line 318
    invoke-static {v2}, Lcpu;->y([B)Lcpu;

    move-result-object v2

    .line 319
    iget-object v3, v2, Lcpu;->bfI:Ljava/lang/String;

    invoke-static {v3}, Leeb;->kM(Ljava/lang/String;)Leeb;

    move-result-object v3

    iput-object v3, p0, Lcpq;->bfD:Leeb;

    .line 320
    iget-object v3, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v3}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 321
    iget-object v2, v2, Lcpu;->bfJ:[Lcpp;

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v5, v2, v0

    .line 322
    iget-object v6, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v6, v5}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 331
    :cond_2
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 332
    invoke-static {v8, v4}, Lege;->a(ILegl;)V

    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 325
    :goto_2
    const v2, 0xf9576d

    :try_start_2
    invoke-static {v2}, Lhwt;->lx(I)V

    .line 326
    const-string v2, "StaticContentCache"

    const-string v3, "Error while parsing cache entry for static contents"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v5}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 331
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 332
    invoke-static {v8, v4}, Lege;->a(ILegl;)V

    goto :goto_0

    .line 327
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 328
    :goto_3
    const v2, 0xf9576d

    :try_start_3
    invoke-static {v2}, Lhwt;->lx(I)V

    .line 329
    const-string v2, "StaticContentCache"

    const-string v3, "IO Error while parsing cache entry for static contents"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v5}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 331
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 332
    invoke-static {v8, v4}, Lege;->a(ILegl;)V

    goto :goto_0

    .line 331
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 332
    invoke-static {v8, v4}, Lege;->a(ILegl;)V

    throw v0

    .line 331
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 327
    :catch_2
    move-exception v0

    goto :goto_3

    .line 324
    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method public a(I[BLjava/lang/String;Z)V
    .locals 4
    .param p2    # [B
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 124
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    if-eqz p4, :cond_2

    .line 127
    iget-object v0, p0, Lcpq;->bfC:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eq p1, v0, :cond_1

    .line 128
    const-string v0, "StaticContentCache"

    const-string v1, "Cannot save the pellet contents in cache as the serial number is different from the expected value."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcpq;->bfC:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 136
    :cond_2
    iget-object v0, p0, Lcpq;->aZM:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcpq;->aZM:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_3

    .line 137
    iget-object v0, p0, Lcpq;->aZM:Ljava/util/concurrent/Future;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcpq;->aZM:Ljava/util/concurrent/Future;

    .line 140
    :cond_3
    new-instance v0, Lcpp;

    invoke-direct {v0}, Lcpp;-><init>()V

    .line 141
    iput-object p2, v0, Lcpp;->content:[B

    .line 142
    iput-object p3, v0, Lcpp;->bfB:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v0, p0, Lcpq;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Ki()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcpq;->bfG:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method public a(Leeb;)V
    .locals 0

    .prologue
    .line 101
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iput-object p1, p0, Lcpq;->bfD:Leeb;

    .line 103
    return-void
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 267
    const-string v0, "Cached SRP Prefix Pellet fingerprints"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcpq;->PI()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 269
    return-void
.end method

.method public h([Ljava/lang/String;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 166
    .line 167
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    array-length v1, p1

    if-ge v0, v1, :cond_5

    :cond_0
    move v1, v3

    .line 170
    :goto_0
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 171
    array-length v0, p1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v5

    move v4, v2

    .line 174
    :goto_1
    array-length v0, p1

    if-ge v4, v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 175
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpp;

    .line 176
    iget-object v7, v0, Lcpp;->bfB:Ljava/lang/String;

    aget-object v8, p1, v4

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 177
    iget-object v0, v0, Lcpp;->content:[B

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move v1, v3

    .line 183
    :cond_2
    if-eqz v1, :cond_3

    .line 185
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 188
    const-string v0, "StaticContentCache"

    const-string v1, "Error inline loading the pellets from cache."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 189
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 199
    :goto_2
    return-object v0

    .line 192
    :cond_3
    iget-object v0, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    array-length v1, p1

    if-lt v0, v1, :cond_4

    .line 194
    :try_start_0
    iget-object v1, p0, Lcpq;->bfF:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingDeque;->removeLast()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 196
    :catch_0
    move-exception v1

    const-string v1, "StaticContentCache"

    const-string v3, "Error while removing invalid pellets from cache."

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_4
    move-object v0, v5

    .line 199
    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_0
.end method
