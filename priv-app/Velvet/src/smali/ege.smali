.class public final Lege;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static bYk:Ljava/lang/ThreadLocal;

.field private static volatile bYl:Legg;

.field private static final bYm:Ljava/lang/Object;

.field private static bYn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lege;->bYm:Ljava/lang/Object;

    .line 78
    return-void
.end method

.method private static a(IJJ)Litu;
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 194
    invoke-static {p0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 195
    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 196
    invoke-static {p1, p2}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    .line 198
    :cond_0
    cmp-long v1, p3, v2

    if-eqz v1, :cond_1

    .line 199
    invoke-virtual {v0, p3, p4}, Litu;->bW(J)Litu;

    .line 201
    :cond_1
    return-object v0
.end method

.method static a(IIJJ)V
    .locals 2

    .prologue
    .line 292
    invoke-static {p0, p2, p3, p4, p5}, Lege;->a(IJJ)Litu;

    move-result-object v0

    .line 293
    new-instance v1, Litq;

    invoke-direct {v1}, Litq;-><init>()V

    invoke-virtual {v1, p1}, Litq;->mx(I)Litq;

    move-result-object v1

    iput-object v1, v0, Litu;->dIX:Litq;

    .line 294
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 295
    return-void
.end method

.method public static a(ILegl;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 240
    iget-wide v0, p1, Legl;->amT:J

    iget-wide v2, p1, Legl;->bUI:J

    invoke-static {p0, v0, v1, v2, v3}, Lege;->a(IJJ)Litu;

    move-result-object v0

    .line 242
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 243
    return-void
.end method

.method public static a(Lefr;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 262
    invoke-static {}, Lenu;->auQ()V

    .line 264
    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legq;

    .line 265
    if-eqz v0, :cond_0

    .line 266
    iget-wide v0, v0, Legq;->amT:J

    .line 268
    :goto_0
    invoke-static {p0, v0, v1, v2, v3}, Lege;->a(Lefr;JJ)V

    .line 269
    return-void

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static a(Lefr;JJ)V
    .locals 7

    .prologue
    .line 278
    invoke-interface {p0}, Lefr;->aot()Ljava/lang/Exception;

    move-result-object v1

    .line 279
    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lefr;

    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lefr;

    invoke-static {v0}, Lege;->a(Lefr;)V

    .line 285
    :cond_0
    const-string v0, "EventLogger"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 287
    invoke-interface {p0}, Lefr;->aou()I

    move-result v0

    invoke-interface {p0}, Lefr;->getErrorCode()I

    move-result v1

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lege;->a(IIJJ)V

    .line 288
    return-void
.end method

.method public static a(Legg;)V
    .locals 1

    .prologue
    .line 344
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legg;

    sput-object v0, Lege;->bYl:Legg;

    .line 345
    return-void
.end method

.method public static a(Legm;)V
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Legm;->bYu:I

    invoke-static {v0, p0}, Lege;->b(ILegh;)V

    .line 177
    return-void
.end method

.method public static a(Litu;)V
    .locals 3

    .prologue
    .line 223
    invoke-virtual {p0}, Litu;->getEventType()I

    move-result v0

    new-instance v1, Lega;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lega;-><init>(Litu;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lege;->b(ILegh;)V

    .line 225
    return-void
.end method

.method public static a(Litu;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 209
    invoke-virtual {p0}, Litu;->getEventType()I

    move-result v0

    new-instance v1, Lega;

    invoke-direct {v1, p0, p1}, Lega;-><init>(Litu;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lege;->b(ILegh;)V

    .line 211
    return-void
.end method

.method public static a(Livq;IIIILegl;)V
    .locals 6

    .prologue
    .line 165
    new-instance v0, Legf;

    const/16 v3, 0xd8

    move v1, p1

    move v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Legf;-><init>(IIIILegl;)V

    invoke-static {p0, v0}, Livg;->a(Livq;Livf;)V

    .line 167
    return-void
.end method

.method public static aov()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    sget-object v1, Lege;->bYm:Ljava/lang/Object;

    monitor-enter v1

    .line 111
    :try_start_0
    sget-object v0, Lege;->bYn:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static aow()Legg;
    .locals 1

    .prologue
    .line 349
    sget-object v0, Lege;->bYl:Legg;

    return-object v0
.end method

.method private static b(ILegh;)V
    .locals 3

    .prologue
    .line 246
    sget-object v0, Lege;->bYl:Legg;

    if-nez v0, :cond_0

    .line 248
    const-string v0, "EventLogger"

    const-string v1, "No EventLoggerStore set on EventLogger, event will be ignored.Initialize EventLogger in your Application#onCreate method"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_0
    sget-object v0, Lege;->bYl:Legg;

    invoke-interface {v0, p0, p1}, Legg;->a(ILegh;)V

    goto :goto_0
.end method

.method public static c(Letj;)V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 301
    invoke-static {}, Legj;->aox()Legn;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Legn;->aoA()Legp;

    move-result-object v3

    .line 308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v0, v4

    .line 309
    invoke-static {}, Lesp;->avE()Ljava/text/DateFormat;

    move-result-object v6

    .line 310
    invoke-interface {v3}, Legp;->size()I

    move-result v0

    add-int/lit16 v0, v0, -0xc8

    invoke-static {v14, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Legp;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 311
    new-instance v0, Ljava/util/Date;

    invoke-interface {v3, v1}, Legp;->hw(I)J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 312
    invoke-interface {v3, v1}, Legp;->hv(I)I

    move-result v0

    invoke-static {v0}, Lenj;->ii(I)Ljava/lang/String;

    move-result-object v2

    .line 313
    invoke-interface {v3, v1}, Legp;->hx(I)Legh;

    move-result-object v0

    .line 314
    instance-of v8, v0, Lega;

    if-eqz v8, :cond_4

    .line 315
    check-cast v0, Lega;

    .line 316
    iget-object v8, v0, Lega;->bYe:Litu;

    .line 317
    new-instance v9, Ljava/util/ArrayList;

    const/4 v10, 0x2

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 318
    invoke-virtual {v8}, Litu;->zK()Ljava/lang/String;

    move-result-object v10

    .line 319
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 320
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "request ID: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v10}, Leoi;->lj(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    :cond_0
    iget-object v0, v0, Lega;->bYf:Ljava/lang/String;

    .line 323
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 324
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "GWS event ID: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    :cond_1
    invoke-virtual {v8}, Litu;->aYU()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 327
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v0, "client: "

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Litu;->aYT()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "unknown"

    :goto_1
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    :cond_2
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; "

    invoke-static {v2}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v2

    invoke-virtual {v2, v9}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333
    :goto_2
    invoke-virtual {p0, v7}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v2

    invoke-virtual {v2, v0, v14}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 310
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 327
    :pswitch_0
    const-string v0, "gel"

    goto :goto_1

    :pswitch_1
    const-string v0, "velvet"

    goto :goto_1

    :pswitch_2
    const-string v0, "hotwordservice"

    goto :goto_1

    :pswitch_3
    const-string v0, "speakerid-enrollment"

    goto :goto_1

    :pswitch_4
    const-string v0, "default"

    goto :goto_1

    :pswitch_5
    const-string v0, "onevoice"

    goto :goto_1

    :pswitch_6
    const-string v0, "clockwork"

    goto :goto_1

    .line 335
    :cond_3
    return-void

    :cond_4
    move-object v0, v2

    goto :goto_2

    .line 327
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method public static ed()V
    .locals 1

    .prologue
    .line 159
    invoke-static {}, Legj;->aoy()Legg;

    move-result-object v0

    .line 160
    invoke-static {v0}, Lege;->a(Legg;)V

    .line 161
    return-void
.end method

.method public static hs(I)Litu;
    .locals 1

    .prologue
    .line 189
    new-instance v0, Litu;

    invoke-direct {v0}, Litu;-><init>()V

    invoke-virtual {v0, p0}, Litu;->mB(I)Litu;

    move-result-object v0

    return-object v0
.end method

.method public static ht(I)V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lege;->b(ILegh;)V

    .line 233
    return-void
.end method

.method public static kP(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x0

    .line 91
    sget-object v1, Lege;->bYm:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    sget-object v2, Lege;->bYn:Ljava/lang/String;

    invoke-static {v2, p0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 93
    new-instance v0, Legb;

    sget-object v2, Lege;->bYn:Ljava/lang/String;

    invoke-direct {v0, v2, p0}, Legb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sput-object p0, Lege;->bYn:Ljava/lang/String;

    .line 97
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    if-eqz v0, :cond_1

    .line 100
    invoke-static {v0}, Legm;->a(Legb;)Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    .line 102
    :cond_1
    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static kQ(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 354
    const-string v0, "gel"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    const/4 v0, 0x1

    .line 371
    :goto_0
    return v0

    .line 356
    :cond_0
    const-string v0, "velvet"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    const/4 v0, 0x2

    goto :goto_0

    .line 358
    :cond_1
    const-string v0, "hotwordservice"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 359
    const/4 v0, 0x5

    goto :goto_0

    .line 360
    :cond_2
    const-string v0, "speakerid-enrollment"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 361
    const/4 v0, 0x6

    goto :goto_0

    .line 362
    :cond_3
    const-string v0, "default"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 363
    const/4 v0, 0x3

    goto :goto_0

    .line 364
    :cond_4
    const-string v0, "onevoice"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365
    const/4 v0, 0x4

    goto :goto_0

    .line 366
    :cond_5
    const-string v0, "clockwork"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 367
    const/4 v0, 0x7

    goto :goto_0

    .line 368
    :cond_6
    const-string v0, "assistant-query-entry"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 369
    const/16 v0, 0x8

    goto :goto_0

    .line 371
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method
