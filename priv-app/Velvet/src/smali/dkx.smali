.class public Ldkx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final bAY:Ljava/nio/charset/Charset;


# instance fields
.field private final bAZ:Ligi;

.field private final bBa:Ldlf;

.field private final bBb:Landroid/net/ConnectivityManager;

.field private final bBc:Lbzj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    sput-object v0, Ldkx;->bAY:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Ldla;Ldlf;Ligi;Landroid/net/ConnectivityManager;Lbzj;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligi;

    iput-object v0, p0, Ldkx;->bAZ:Ligi;

    .line 99
    iput-object p2, p0, Ldkx;->bBa:Ldlf;

    .line 100
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzj;

    iput-object v0, p0, Ldkx;->bBc:Lbzj;

    .line 101
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Ldkx;->bBb:Landroid/net/ConnectivityManager;

    .line 102
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;ZZZI)Lbzh;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 221
    if-eqz p5, :cond_0

    .line 222
    iget-object v1, p0, Ldkx;->bBa:Ldlf;

    invoke-interface {v1, p1}, Ldlf;->in(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 226
    :cond_0
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    const/4 v1, -0x1

    if-ne p6, v1, :cond_1

    move p6, v0

    :cond_1
    iget-object v1, p0, Ldkx;->bBc:Lbzj;

    invoke-interface {v1, p6}, Lbzj;->et(I)Lbzi;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v1, p0, Ldkx;->bBc:Lbzj;

    invoke-interface {v1, v0}, Lbzj;->et(I)Lbzi;

    move-result-object v0

    const-string v1, "The stable HTTP engine must always be available."

    invoke-static {v0, v1}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    :goto_0
    invoke-interface {v0, v2, p3, p4}, Lbzi;->a(Ljava/net/URL;ZZ)Lbzh;

    move-result-object v2

    .line 237
    if-eqz p2, :cond_2

    .line 238
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 239
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 240
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 242
    invoke-interface {v2, v1, v0}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 227
    :catch_0
    move-exception v0

    .line 228
    new-instance v1, Lefs;

    const v2, 0x40001

    invoke-direct {v1, p1, v0, v2}, Lefs;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1

    .line 245
    :cond_2
    const-string v1, "User-Agent"

    iget-object v0, p0, Ldkx;->bAZ:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    return-object v2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;[BIZLdlg;ZI)Ljava/lang/Object;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Ldkx;->acM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    const/4 v0, 0x0

    .line 209
    :goto_0
    return-object v0

    .line 197
    :cond_0
    if-nez p2, :cond_2

    .line 198
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 200
    :goto_1
    const-string v1, "Content-Length"

    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p5

    move v5, p7

    move v6, p8

    invoke-direct/range {v0 .. v6}, Ldkx;->a(Ljava/lang/String;Ljava/util/Map;ZZZI)Lbzh;

    move-result-object v0

    .line 205
    :try_start_0
    const-string v1, "POST"

    invoke-interface {v0, v1}, Lbzh;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    invoke-virtual {p6, v0, p3, p4}, Ldlg;->b(Lbzh;[BI)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 200
    :cond_1
    array-length v0, p3

    goto :goto_2

    .line 206
    :catch_0
    move-exception v0

    .line 207
    new-instance v1, Lefs;

    const v2, 0x4000c

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    :cond_2
    move-object v2, p2

    goto :goto_1
.end method

.method static a(Ljava/io/InputStream;I)[B
    .locals 4

    .prologue
    .line 367
    const/4 v1, 0x0

    .line 370
    if-gez p1, :cond_1

    .line 371
    :try_start_0
    invoke-static {p0}, Leoo;->j(Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 379
    :goto_0
    if-eqz p0, :cond_0

    .line 380
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 386
    :cond_0
    :goto_1
    return-object v0

    .line 373
    :cond_1
    :try_start_2
    new-array v1, p1, [B

    .line 374
    invoke-static {p0, v1}, Leoo;->a(Ljava/io/InputStream;[B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 379
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_2

    .line 380
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 384
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    const-string v1, "HttpHelper"

    const-string v2, "Failed to read byte response from InputStream."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method static c(Lbzh;)Ljava/nio/charset/Charset;
    .locals 2

    .prologue
    .line 275
    invoke-interface {p0}, Lbzh;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 276
    if-nez v0, :cond_0

    sget-object v0, Ldkx;->bAY:Ljava/nio/charset/Charset;

    .line 282
    :goto_0
    return-object v0

    .line 278
    :cond_0
    const-string v1, "charset="

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 279
    if-gez v1, :cond_1

    sget-object v0, Ldkx;->bAY:Ljava/nio/charset/Charset;

    goto :goto_0

    .line 280
    :cond_1
    add-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 282
    invoke-static {v0}, Ldkx;->jN(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_0
.end method

.method private static jN(Ljava/lang/String;)Ljava/nio/charset/Charset;
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 286
    if-nez p0, :cond_0

    sget-object v0, Ldkx;->bAY:Ljava/nio/charset/Charset;

    .line 296
    :goto_0
    return-object v0

    .line 288
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/nio/charset/Charset;->isSupported(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 289
    const-string v0, "HttpHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported charset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 290
    sget-object v0, Ldkx;->bAY:Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 293
    :catch_0
    move-exception v0

    const-string v0, "HttpHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal charset name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v5, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 294
    sget-object v0, Ldkx;->bAY:Ljava/nio/charset/Charset;

    goto :goto_0

    .line 296
    :cond_1
    invoke-static {p0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final Ce()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 108
    move v0, v1

    :goto_0
    if-gtz v0, :cond_1

    .line 110
    iget-object v2, p0, Ldkx;->bBc:Lbzj;

    invoke-interface {v2, v1}, Lbzj;->et(I)Lbzi;

    move-result-object v2

    .line 111
    if-eqz v2, :cond_0

    .line 112
    invoke-interface {v2}, Lbzi;->Ce()V

    .line 108
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_1
    return-void
.end method

.method public final a(Ldlb;ILdlg;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 148
    invoke-virtual {p1}, Ldlb;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ldlb;->getHeaders()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1}, Ldlb;->getUseCaches()Z

    move-result v3

    invoke-virtual {p1}, Ldlb;->acO()Z

    move-result v4

    invoke-virtual {p1}, Ldlb;->acN()Z

    move-result v5

    invoke-virtual {p1}, Ldlb;->acQ()I

    move-result v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Ldkx;->a(Ljava/lang/String;Ljava/util/Map;ZZZI)Lbzh;

    move-result-object v0

    .line 152
    :try_start_0
    const-string v1, "GET"

    invoke-interface {v0, v1}, Lbzh;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    invoke-virtual {p1}, Ldlb;->getFollowRedirects()Z

    move-result v1

    invoke-interface {v0, v1}, Lbzh;->setInstanceFollowRedirects(Z)V

    .line 157
    invoke-virtual {p1}, Ldlb;->getUseCaches()Z

    move-result v1

    invoke-interface {v0, v1}, Lbzh;->setUseCaches(Z)V

    .line 158
    invoke-virtual {p0}, Ldkx;->acM()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ldlb;->getUseCaches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    const-string v1, "Cache-Control"

    const-string v2, "only-if-cached"

    invoke-interface {v0, v1, v2}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1}, Ldlb;->acP()I

    move-result v1

    .line 165
    if-lez v1, :cond_1

    .line 166
    const-string v2, "Cache-Control"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "max-stale="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1, p2}, Ldlg;->b(Lbzh;[BI)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    new-instance v1, Lefs;

    const v2, 0x4000b

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 169
    :cond_1
    const-string v1, "Cache-Control"

    const-string v2, "max-stale"

    invoke-interface {v0, v1, v2}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ldld;I)Ljava/lang/String;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 179
    invoke-virtual {p1}, Ldld;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ldld;->getHeaders()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1}, Ldld;->acS()[B

    move-result-object v3

    invoke-virtual {p1}, Ldld;->acO()Z

    move-result v5

    new-instance v6, Ldle;

    invoke-direct {v6}, Ldle;-><init>()V

    invoke-virtual {p1}, Ldld;->acN()Z

    move-result v7

    invoke-virtual {p1}, Ldld;->acQ()I

    move-result v8

    move-object v0, p0

    move v4, p2

    invoke-direct/range {v0 .. v8}, Ldkx;->a(Ljava/lang/String;Ljava/util/Map;[BIZLdlg;ZI)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final acM()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Ldkx;->bBb:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 303
    :cond_0
    const/4 v0, 0x0

    .line 305
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ldlb;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    new-instance v0, Ldle;

    invoke-direct {v0}, Ldle;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Ldkx;->a(Ldlb;ILdlg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ldld;I)[B
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p1}, Ldld;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ldld;->getHeaders()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1}, Ldld;->acS()[B

    move-result-object v3

    invoke-virtual {p1}, Ldld;->acO()Z

    move-result v5

    new-instance v6, Ldky;

    invoke-direct {v6}, Ldky;-><init>()V

    invoke-virtual {p1}, Ldld;->acN()Z

    move-result v7

    invoke-virtual {p1}, Ldld;->acQ()I

    move-result v8

    move-object v0, p0

    move v4, p2

    invoke-direct/range {v0 .. v8}, Ldkx;->a(Ljava/lang/String;Ljava/util/Map;[BIZLdlg;ZI)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public final c(Ldlb;I)[B
    .locals 1

    .prologue
    .line 138
    new-instance v0, Ldky;

    invoke-direct {v0}, Ldky;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Ldkx;->a(Ldlb;ILdlg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public final d(Ldlb;I)Ldjy;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Ldkz;

    invoke-direct {v0}, Ldkz;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Ldkx;->a(Ldlb;ILdlg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjy;

    return-object v0
.end method
