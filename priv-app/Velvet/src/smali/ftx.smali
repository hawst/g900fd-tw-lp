.class public final Lftx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cBV:I

.field cBW:Z

.field final mProvider:Lfty;


# direct methods
.method public constructor <init>(Lfty;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lftx;-><init>(Lfty;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lfty;I)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lftx;->cBW:Z

    .line 44
    iput-object p1, p0, Lftx;->mProvider:Lfty;

    .line 45
    iput p2, p0, Lftx;->cBV:I

    .line 46
    return-void
.end method

.method public static aN(Ljava/lang/String;Ljava/lang/String;)Lanh;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 172
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 173
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 174
    new-instance v1, Lany;

    invoke-direct {v1}, Lany;-><init>()V

    iput-object v1, v0, Lanh;->agw:Lany;

    .line 175
    iget-object v1, v0, Lanh;->agw:Lany;

    invoke-virtual {v1, p0}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 176
    if-eqz p1, :cond_0

    .line 177
    iget-object v1, v0, Lanh;->agw:Lany;

    invoke-virtual {v1, p1}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 179
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final aS(Landroid/content/Context;)Lang;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0}, Lfty;->aAv()Lizq;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0}, Lfty;->getEntry()Lizj;

    move-result-object v0

    .line 68
    :goto_0
    if-eqz v0, :cond_1

    .line 69
    iget-object v2, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v2, p1, v0}, Lfty;->b(Landroid/content/Context;Lizj;)[Lanh;

    move-result-object v2

    .line 71
    if-eqz v2, :cond_1

    .line 72
    new-instance v0, Lang;

    invoke-direct {v0}, Lang;-><init>()V

    .line 73
    iput-object v2, v0, Lang;->ags:[Lanh;

    .line 74
    iget-object v1, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v1}, Lfty;->aDq()Z

    move-result v1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 135
    :goto_1
    return-object v0

    .line 63
    :cond_0
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0}, Lfty;->aAw()Lizj;

    move-result-object v0

    goto :goto_0

    .line 80
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 83
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0, p1}, Lfty;->aT(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_2

    .line 85
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 91
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0}, Lfty;->aAv()Lizq;

    move-result-object v0

    .line 92
    iget-object v4, v0, Lizq;->dUX:[Lizj;

    array-length v5, v4

    move v0, v1

    :goto_2
    if-ge v0, v5, :cond_4

    aget-object v6, v4, v0

    .line 93
    iget-object v7, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v7, p1, v6}, Lfty;->a(Landroid/content/Context;Lizj;)Lanh;

    move-result-object v7

    .line 94
    if-eqz v7, :cond_3

    .line 95
    iput-object v6, v7, Lanh;->ahu:Lizj;

    .line 96
    invoke-virtual {v7, v8}, Lanh;->aT(Z)Lanh;

    .line 97
    iget-boolean v6, p0, Lftx;->cBW:Z

    invoke-virtual {v7, v6}, Lanh;->aU(Z)Lanh;

    .line 98
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 104
    :cond_4
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0, p1}, Lfty;->aU(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_5

    .line 106
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_5
    if-nez v0, :cond_6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 112
    :goto_3
    iget-object v4, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v4}, Lfty;->ul()Z

    move-result v4

    if-nez v4, :cond_9

    iget v4, p0, Lftx;->cBV:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_9

    iget v4, p0, Lftx;->cBV:I

    add-int/lit8 v4, v4, 0x1

    if-le v0, v4, :cond_9

    .line 115
    iget v0, p0, Lftx;->cBV:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 117
    iget-object v4, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v4, p1, v0}, Lfty;->b(Landroid/content/Context;Ljava/util/List;)Lanh;

    move-result-object v4

    move v0, v1

    .line 121
    :goto_4
    iget v1, p0, Lftx;->cBV:I

    if-ge v0, v1, :cond_7

    .line 122
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 111
    :cond_6
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 124
    :cond_7
    if-eqz v4, :cond_8

    .line 125
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_8
    :goto_5
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 133
    iget-object v0, v1, Lang;->ags:[Lanh;

    invoke-static {v0, v2}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 134
    invoke-virtual {v1, v8}, Lang;->aS(Z)Lang;

    move-object v0, v1

    .line 135
    goto/16 :goto_1

    .line 129
    :cond_9
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final j(Landroid/content/Context;I)Lanh;
    .locals 3

    .prologue
    .line 148
    if-nez p2, :cond_0

    .line 150
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0, p1}, Lfty;->aT(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    .line 153
    :cond_0
    add-int/lit8 v0, p2, -0x1

    .line 154
    iget-object v1, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v1}, Lfty;->aAv()Lizq;

    move-result-object v1

    iget-object v1, v1, Lizq;->dUX:[Lizj;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 155
    iget-object v1, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v1}, Lfty;->aAv()Lizq;

    move-result-object v1

    iget-object v1, v1, Lizq;->dUX:[Lizj;

    aget-object v0, v1, v0

    .line 156
    iget-object v1, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v1, p1, v0}, Lfty;->a(Landroid/content/Context;Lizj;)Lanh;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0}, Lfty;->aAv()Lizq;

    move-result-object v0

    iget-object v0, v0, Lizq;->dUX:[Lizj;

    array-length v0, v0

    if-ne p2, v0, :cond_2

    .line 161
    iget-object v0, p0, Lftx;->mProvider:Lfty;

    invoke-interface {v0, p1}, Lfty;->aU(Landroid/content/Context;)Lanh;

    move-result-object v0

    goto :goto_0

    .line 164
    :cond_2
    const-string v0, "ListCardModuleBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested update of invalid module index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v0, 0x0

    goto :goto_0
.end method
