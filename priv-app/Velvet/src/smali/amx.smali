.class public final Lamx;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile afH:[Lamx;


# instance fields
.field private aez:I

.field public afA:Ljbj;

.field private afI:Z

.field private afJ:Z

.field private afK:J

.field private afL:J

.field private afM:J

.field public afN:[J

.field private afO:I

.field public entry:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 151
    iput v1, p0, Lamx;->aez:I

    iput-object v2, p0, Lamx;->entry:Lizj;

    iput-object v2, p0, Lamx;->afA:Ljbj;

    iput-boolean v1, p0, Lamx;->afI:Z

    iput-boolean v1, p0, Lamx;->afJ:Z

    iput-wide v4, p0, Lamx;->afK:J

    iput-wide v4, p0, Lamx;->afL:J

    iput-wide v4, p0, Lamx;->afM:J

    sget-object v0, Ljsu;->eCB:[J

    iput-object v0, p0, Lamx;->afN:[J

    iput v1, p0, Lamx;->afO:I

    iput-object v2, p0, Lamx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamx;->eCz:I

    .line 152
    return-void
.end method

.method public static nO()[Lamx;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lamx;->afH:[Lamx;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lamx;->afH:[Lamx;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lamx;

    sput-object v0, Lamx;->afH:[Lamx;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lamx;->afH:[Lamx;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lamx;->entry:Lizj;

    if-nez v0, :cond_1

    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    iput-object v0, p0, Lamx;->entry:Lizj;

    :cond_1
    iget-object v0, p0, Lamx;->entry:Lizj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lamx;->afA:Ljbj;

    if-nez v0, :cond_2

    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    iput-object v0, p0, Lamx;->afA:Ljbj;

    :cond_2
    iget-object v0, p0, Lamx;->afA:Ljbj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamx;->afI:Z

    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamx;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamx;->afJ:Z

    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamx;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lamx;->afK:J

    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamx;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lamx;->afL:J

    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamx;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lamx;->afM:J

    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamx;->aez:I

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lamx;->afN:[J

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_3

    iget-object v3, p0, Lamx;->afN:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lamx;->afN:[J

    array-length v0, v0

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lamx;->afN:[J

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btR()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Lamx;->afN:[J

    if-nez v2, :cond_8

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_7

    iget-object v4, p0, Lamx;->afN:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_9

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    iget-object v2, p0, Lamx;->afN:[J

    array-length v2, v2

    goto :goto_4

    :cond_9
    iput-object v0, p0, Lamx;->afN:[J

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lamx;->afO:I

    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lamx;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lamx;->entry:Lizj;

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x1

    iget-object v1, p0, Lamx;->entry:Lizj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 176
    :cond_0
    iget-object v0, p0, Lamx;->afA:Ljbj;

    if-eqz v0, :cond_1

    .line 177
    const/4 v0, 0x2

    iget-object v1, p0, Lamx;->afA:Ljbj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 179
    :cond_1
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 180
    const/4 v0, 0x3

    iget-boolean v1, p0, Lamx;->afI:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 182
    :cond_2
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 183
    const/4 v0, 0x4

    iget-boolean v1, p0, Lamx;->afJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 185
    :cond_3
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 186
    const/4 v0, 0x5

    iget-wide v2, p0, Lamx;->afK:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 188
    :cond_4
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 189
    const/4 v0, 0x6

    iget-wide v2, p0, Lamx;->afL:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 191
    :cond_5
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 192
    const/4 v0, 0x7

    iget-wide v2, p0, Lamx;->afM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 194
    :cond_6
    iget-object v0, p0, Lamx;->afN:[J

    if-eqz v0, :cond_7

    iget-object v0, p0, Lamx;->afN:[J

    array-length v0, v0

    if-lez v0, :cond_7

    .line 195
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lamx;->afN:[J

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 196
    const/16 v1, 0x8

    iget-object v2, p0, Lamx;->afN:[J

    aget-wide v2, v2, v0

    invoke-virtual {p1, v1, v2, v3}, Ljsj;->h(IJ)V

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_7
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 200
    const/16 v0, 0x9

    iget v1, p0, Lamx;->afO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 202
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 203
    return-void
.end method

.method public final aM(Z)Lamx;
    .locals 1

    .prologue
    .line 39
    iput-boolean p1, p0, Lamx;->afI:Z

    .line 40
    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamx;->aez:I

    .line 41
    return-object p0
.end method

.method public final aN(Z)Lamx;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lamx;->afJ:Z

    .line 59
    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamx;->aez:I

    .line 60
    return-object p0
.end method

.method public final cg(I)Lamx;
    .locals 1

    .prologue
    .line 137
    iput p1, p0, Lamx;->afO:I

    .line 138
    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lamx;->aez:I

    .line 139
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 207
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 208
    iget-object v2, p0, Lamx;->entry:Lizj;

    if-eqz v2, :cond_0

    .line 209
    const/4 v2, 0x1

    iget-object v3, p0, Lamx;->entry:Lizj;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 212
    :cond_0
    iget-object v2, p0, Lamx;->afA:Ljbj;

    if-eqz v2, :cond_1

    .line 213
    const/4 v2, 0x2

    iget-object v3, p0, Lamx;->afA:Ljbj;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 216
    :cond_1
    iget v2, p0, Lamx;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 217
    const/4 v2, 0x3

    iget-boolean v3, p0, Lamx;->afI:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 220
    :cond_2
    iget v2, p0, Lamx;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_3

    .line 221
    const/4 v2, 0x4

    iget-boolean v3, p0, Lamx;->afJ:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 224
    :cond_3
    iget v2, p0, Lamx;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_4

    .line 225
    const/4 v2, 0x5

    iget-wide v4, p0, Lamx;->afK:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 228
    :cond_4
    iget v2, p0, Lamx;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_5

    .line 229
    const/4 v2, 0x6

    iget-wide v4, p0, Lamx;->afL:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 232
    :cond_5
    iget v2, p0, Lamx;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_6

    .line 233
    const/4 v2, 0x7

    iget-wide v4, p0, Lamx;->afM:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 236
    :cond_6
    iget-object v2, p0, Lamx;->afN:[J

    if-eqz v2, :cond_8

    iget-object v2, p0, Lamx;->afN:[J

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    .line 238
    :goto_0
    iget-object v3, p0, Lamx;->afN:[J

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 239
    iget-object v3, p0, Lamx;->afN:[J

    aget-wide v4, v3, v1

    .line 240
    invoke-static {v4, v5}, Ljsj;->dJ(J)I

    move-result v3

    add-int/2addr v2, v3

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 243
    :cond_7
    add-int/2addr v0, v2

    .line 244
    iget-object v1, p0, Lamx;->afN:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 246
    :cond_8
    iget v1, p0, Lamx;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_9

    .line 247
    const/16 v1, 0x9

    iget v2, p0, Lamx;->afO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_9
    return v0
.end method

.method public final nP()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lamx;->afI:Z

    return v0
.end method

.method public final nQ()Z
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nR()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lamx;->afJ:Z

    return v0
.end method

.method public final nS()Z
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nT()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lamx;->afK:J

    return-wide v0
.end method

.method public final nU()Z
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nV()Lamx;
    .locals 2

    .prologue
    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamx;->afK:J

    .line 86
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lamx;->aez:I

    .line 87
    return-object p0
.end method

.method public final nW()J
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lamx;->afM:J

    return-wide v0
.end method

.method public final nX()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lamx;->afO:I

    return v0
.end method

.method public final nY()Z
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lamx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y(J)Lamx;
    .locals 1

    .prologue
    .line 77
    iput-wide p1, p0, Lamx;->afK:J

    .line 78
    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamx;->aez:I

    .line 79
    return-object p0
.end method

.method public final z(J)Lamx;
    .locals 1

    .prologue
    .line 115
    iput-wide p1, p0, Lamx;->afM:J

    .line 116
    iget v0, p0, Lamx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamx;->aez:I

    .line 117
    return-object p0
.end method
