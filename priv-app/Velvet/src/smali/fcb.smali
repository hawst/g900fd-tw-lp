.class final Lfcb;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private synthetic coy:Lfca;


# direct methods
.method constructor <init>(Lfca;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lfcb;->coy:Lfca;

    .line 119
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 120
    return-void
.end method

.method private a(Lfce;)V
    .locals 4

    .prologue
    const/high16 v3, 0x80000

    .line 148
    iget-boolean v0, p1, Lfce;->coB:Z

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lfcb;->coy:Lfca;

    iget-object v0, v0, Lfca;->mFileWriter:Lfck;

    iget-object v1, p1, Lfce;->coz:Ljava/lang/String;

    iget-object v2, p1, Lfce;->rQ:[B

    invoke-virtual {v0, v1, v2, v3}, Lfck;->a(Ljava/lang/String;[BI)Z

    .line 159
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p1, Lfce;->rQ:[B

    array-length v0, v0

    if-gt v0, v3, :cond_1

    .line 153
    iget-object v0, p0, Lfcb;->coy:Lfca;

    iget-object v0, v0, Lfca;->mFileWriter:Lfck;

    iget-object v1, p1, Lfce;->coz:Ljava/lang/String;

    iget-object v2, p1, Lfce;->rQ:[B

    invoke-virtual {v0, v1, v2}, Lfck;->h(Ljava/lang/String;[B)Z

    goto :goto_0

    .line 155
    :cond_1
    const-string v0, "AsyncFileStorageImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Data is too large ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lfce;->rQ:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes) to write to disk: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lfce;->coz:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private lH(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lfcb;->coy:Lfca;

    iget-object v0, v0, Lfca;->mFileWriter:Lfck;

    invoke-virtual {v0, p1}, Lfck;->lH(Ljava/lang/String;)V

    .line 196
    return-void
.end method

.method private t(Ljava/lang/String;Z)[B
    .locals 2

    .prologue
    const/high16 v1, 0x80000

    .line 189
    if-eqz p2, :cond_0

    iget-object v0, p0, Lfcb;->coy:Lfca;

    iget-object v0, v0, Lfca;->mFileReader:Lfcj;

    invoke-virtual {v0, p1, v1}, Lfcj;->B(Ljava/lang/String;I)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfcb;->coy:Lfca;

    iget-object v0, v0, Lfca;->mFileReader:Lfcj;

    invoke-virtual {v0, p1, v1}, Lfcj;->A(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 124
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 142
    const-string v0, "AsyncFileStorageImpl"

    const-string v1, "Unknown message sent to AsyncFileStorageHandler"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 126
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfce;

    invoke-direct {p0, v0}, Lfcb;->a(Lfce;)V

    goto :goto_0

    .line 130
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcc;

    :try_start_0
    iget-object v1, v0, Lfcc;->coz:Ljava/lang/String;

    iget-boolean v2, v0, Lfcc;->coB:Z

    invoke-direct {p0, v1, v2}, Lfcb;->t(Ljava/lang/String;Z)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    iget-object v0, v0, Lfcc;->coA:Lifg;

    invoke-interface {v0, v1}, Lifg;->as(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v0, v0, Lfcc;->coA:Lifg;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lifg;->as(Ljava/lang/Object;)Ljava/lang/Object;

    throw v1

    .line 131
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lfcb;->lH(Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcd;

    iget-object v1, v0, Lfcd;->coz:Ljava/lang/String;

    iget-boolean v2, v0, Lfcd;->coB:Z

    invoke-direct {p0, v1, v2}, Lfcb;->t(Ljava/lang/String;Z)[B

    move-result-object v1

    iget-object v2, v0, Lfcd;->coA:Lifg;

    invoke-interface {v2, v1}, Lifg;->as(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    iget-object v0, v0, Lfcd;->coz:Ljava/lang/String;

    invoke-direct {p0, v0}, Lfcb;->lH(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v2, Lfce;

    iget-object v3, v0, Lfcd;->coz:Ljava/lang/String;

    iget-boolean v0, v0, Lfcd;->coB:Z

    invoke-direct {v2, v3, v1, v0}, Lfce;-><init>(Ljava/lang/String;[BZ)V

    invoke-direct {p0, v2}, Lfcb;->a(Lfce;)V

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
