.class public final Lfca;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfbz;


# instance fields
.field final mFileReader:Lfcj;

.field final mFileWriter:Lfck;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lfcj;Lfck;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lfca;->mFileReader:Lfcj;

    .line 40
    iput-object p2, p0, Lfca;->mFileWriter:Lfck;

    .line 41
    return-void
.end method

.method private declared-synchronized getHandler()Landroid/os/Handler;
    .locals 3

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfca;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AsyncFileStorageImpl"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 47
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 48
    new-instance v1, Lfcb;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lfcb;-><init>(Lfca;Landroid/os/Looper;)V

    iput-object v1, p0, Lfca;->mHandler:Landroid/os/Handler;

    .line 51
    :cond_0
    iget-object v0, p0, Lfca;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lifg;)V
    .locals 4

    .prologue
    .line 73
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lfca;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Lfcc;

    const/4 v3, 0x1

    invoke-direct {v2, p1, p2, v3}, Lfcc;-><init>(Ljava/lang/String;Lifg;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 74
    return-void
.end method

.method public final b(Ljava/lang/String;Lifg;)V
    .locals 4

    .prologue
    .line 96
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lfca;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Lfcd;

    const/4 v3, 0x1

    invoke-direct {v2, p1, p2, v3}, Lfcd;-><init>(Ljava/lang/String;Lifg;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 97
    return-void
.end method

.method public final g(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lfca;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lfce;

    invoke-direct {v1, p1, p2, v2}, Lfce;-><init>(Ljava/lang/String;[BZ)V

    invoke-virtual {v0, v2, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 57
    return-void
.end method

.method public final lH(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 109
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    invoke-direct {p0}, Lfca;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 111
    return-void
.end method
