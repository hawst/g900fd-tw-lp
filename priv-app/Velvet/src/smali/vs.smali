.class public final Lvs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static In:Landroid/graphics/drawable/Drawable;

.field public static Io:Landroid/graphics/drawable/Drawable;

.field public static Ip:I

.field public static Iq:I


# instance fields
.field public GU:Lcom/android/launcher3/FolderIcon;

.field public Ij:I

.field public Ik:I

.field public Il:F

.field public Im:F

.field private Ir:Landroid/animation/ValueAnimator;

.field private Is:Landroid/animation/ValueAnimator;

.field private xt:Lcom/android/launcher3/CellLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 198
    sput-object v1, Lvs;->In:Landroid/graphics/drawable/Drawable;

    .line 199
    sput-object v1, Lvs;->Io:Landroid/graphics/drawable/Drawable;

    .line 200
    sput v0, Lvs;->Ip:I

    .line 201
    sput v0, Lvs;->Iq:I

    return-void
.end method

.method public constructor <init>(Lcom/android/launcher3/Launcher;Lcom/android/launcher3/FolderIcon;)V
    .locals 3

    .prologue
    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lvs;->GU:Lcom/android/launcher3/FolderIcon;

    .line 207
    iput-object p2, p0, Lvs;->GU:Lcom/android/launcher3/FolderIcon;

    .line 208
    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 212
    invoke-static {}, Lcom/android/launcher3/FolderIcon;->gP()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 214
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FolderRingAnimator loading drawables on non-UI thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_0
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v1

    .line 219
    iget-object v1, v1, Lyu;->Mk:Lur;

    invoke-virtual {v1}, Lur;->gl()Ltu;

    move-result-object v1

    .line 220
    iget v1, v1, Ltu;->DS:I

    sput v1, Lvs;->Ip:I

    .line 221
    const v1, 0x7f0d008a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lvs;->Iq:I

    .line 222
    const v1, 0x7f020289

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lvs;->In:Landroid/graphics/drawable/Drawable;

    .line 223
    const v1, 0x7f020288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lvs;->Io:Landroid/graphics/drawable/Drawable;

    .line 224
    const v1, 0x7f02028a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/launcher3/FolderIcon;->HK:Landroid/graphics/drawable/Drawable;

    .line 225
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/launcher3/FolderIcon;->R(Z)Z

    .line 227
    :cond_1
    return-void
.end method

.method static synthetic c(Lvs;)Lcom/android/launcher3/CellLayout;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lvs;->xt:Lcom/android/launcher3/CellLayout;

    return-object v0
.end method


# virtual methods
.method public final J(II)V
    .locals 0

    .prologue
    .line 298
    iput p1, p0, Lvs;->Ij:I

    .line 299
    iput p2, p0, Lvs;->Ik:I

    .line 300
    return-void
.end method

.method public final gQ()V
    .locals 4

    .prologue
    .line 230
    iget-object v0, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 233
    :cond_0
    iget-object v0, p0, Lvs;->xt:Lcom/android/launcher3/CellLayout;

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    .line 234
    iget-object v0, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 236
    sget v0, Lvs;->Ip:I

    .line 237
    iget-object v1, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    new-instance v2, Lvt;

    invoke-direct {v2, p0, v0}, Lvt;-><init>(Lvs;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 247
    iget-object v0, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    new-instance v1, Lvu;

    invoke-direct {v1, p0}, Lvu;-><init>(Lvs;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 255
    iget-object v0, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 256
    return-void

    .line 233
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final gR()V
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lvs;->Ir:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 262
    :cond_0
    iget-object v0, p0, Lvs;->xt:Lcom/android/launcher3/CellLayout;

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    .line 263
    iget-object v0, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 265
    sget v0, Lvs;->Ip:I

    .line 266
    iget-object v1, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    new-instance v2, Lvv;

    invoke-direct {v2, p0, v0}, Lvv;-><init>(Lvs;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 276
    iget-object v0, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    new-instance v1, Lvw;

    invoke-direct {v1, p0}, Lvw;-><init>(Lvs;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 287
    iget-object v0, p0, Lvs;->Is:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 288
    return-void

    .line 262
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final gS()F
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lvs;->Il:F

    return v0
.end method

.method public final gT()F
    .locals 1

    .prologue
    .line 311
    iget v0, p0, Lvs;->Im:F

    return v0
.end method

.method public final h(Lcom/android/launcher3/CellLayout;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lvs;->xt:Lcom/android/launcher3/CellLayout;

    .line 304
    return-void
.end method
