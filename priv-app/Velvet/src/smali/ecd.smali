.class public final Lecd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final aMD:Leqo;

.field private final bUH:Ljava/lang/Runnable;

.field private final bUI:J

.field private bUJ:Z

.field private bUK:Z

.field private bUL:Z

.field private bUM:Z

.field private bUN:Z

.field private bUO:Z

.field private bUP:Z

.field private mClosed:Z


# direct methods
.method public constructor <init>(Leqo;J)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lece;

    const-string v1, "uiIdle"

    invoke-direct {v0, p0, v1}, Lece;-><init>(Lecd;Ljava/lang/String;)V

    iput-object v0, p0, Lecd;->bUH:Ljava/lang/Runnable;

    .line 44
    iput-boolean v2, p0, Lecd;->bUJ:Z

    .line 45
    iput-boolean v2, p0, Lecd;->bUK:Z

    .line 46
    iput-boolean v2, p0, Lecd;->bUL:Z

    .line 47
    iput-boolean v2, p0, Lecd;->bUM:Z

    .line 48
    iput-boolean v2, p0, Lecd;->bUN:Z

    .line 50
    iput-boolean v2, p0, Lecd;->bUO:Z

    .line 51
    iput-boolean v2, p0, Lecd;->bUP:Z

    .line 53
    iput-boolean v2, p0, Lecd;->mClosed:Z

    .line 57
    iput-object p1, p0, Lecd;->aMD:Leqo;

    .line 58
    iput-wide p2, p0, Lecd;->bUI:J

    .line 59
    return-void
.end method

.method private db(I)V
    .locals 4

    .prologue
    .line 123
    invoke-static {p1}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-wide v2, p0, Lecd;->bUI:J

    invoke-virtual {v0, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 125
    return-void
.end method


# virtual methods
.method public final amN()V
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUJ:Z

    if-nez v0, :cond_0

    .line 70
    const/16 v0, 0x168

    invoke-direct {p0, v0}, Lecd;->db(I)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lecd;->bUJ:Z

    .line 73
    :cond_0
    return-void
.end method

.method public final amO()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 76
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUP:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUL:Z

    if-nez v0, :cond_0

    .line 78
    const/16 v0, 0x16a

    invoke-direct {p0, v0}, Lecd;->db(I)V

    .line 79
    iput-boolean v1, p0, Lecd;->bUL:Z

    .line 80
    iput-boolean v1, p0, Lecd;->bUO:Z

    .line 82
    :cond_0
    return-void
.end method

.method public final amP()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 85
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUP:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUM:Z

    if-nez v0, :cond_0

    .line 87
    const/16 v0, 0x16b

    invoke-direct {p0, v0}, Lecd;->db(I)V

    .line 88
    iput-boolean v1, p0, Lecd;->bUM:Z

    .line 89
    iput-boolean v1, p0, Lecd;->bUO:Z

    .line 91
    :cond_0
    return-void
.end method

.method public final amQ()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 94
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUO:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUN:Z

    if-nez v0, :cond_0

    .line 96
    const/16 v0, 0x16c

    invoke-direct {p0, v0}, Lecd;->db(I)V

    .line 97
    iput-boolean v1, p0, Lecd;->bUN:Z

    .line 98
    iput-boolean v1, p0, Lecd;->bUP:Z

    .line 100
    :cond_0
    return-void
.end method

.method public final amR()V
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lecd;->bUK:Z

    if-nez v0, :cond_0

    .line 109
    const/16 v0, 0x169

    invoke-direct {p0, v0}, Lecd;->db(I)V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lecd;->bUK:Z

    .line 112
    :cond_0
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 116
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lecd;->aMD:Leqo;

    iget-object v1, p0, Lecd;->bUH:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lecd;->mClosed:Z

    .line 120
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lecd;->mClosed:Z

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lecd;->aMD:Leqo;

    iget-object v1, p0, Lecd;->bUH:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->j(Ljava/lang/Runnable;)V

    .line 65
    :cond_0
    return-void
.end method
