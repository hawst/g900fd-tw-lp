.class final Legf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livf;


# instance fields
.field private final bDv:I

.field private final bYo:I

.field private final bYp:I

.field private final bbk:Legl;


# direct methods
.method constructor <init>(IIIILegl;)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput p2, p0, Legf;->bYo:I

    .line 130
    iput p3, p0, Legf;->bYp:I

    .line 131
    iput p4, p0, Legf;->bDv:I

    .line 132
    iput-object p5, p0, Legf;->bbk:Legl;

    .line 133
    iget-object v0, p0, Legf;->bbk:Legl;

    invoke-static {p1, v0}, Lege;->a(ILegl;)V

    .line 134
    return-void
.end method


# virtual methods
.method public final az(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 138
    iget v0, p0, Legf;->bYo:I

    iget-object v1, p0, Legf;->bbk:Legl;

    invoke-static {v0, v1}, Lege;->a(ILegl;)V

    .line 139
    return-void
.end method

.method public final d(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 145
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_1

    .line 146
    :cond_0
    const-string v0, "EventLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Future for event cancelled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Legf;->bYo:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 151
    :goto_0
    iget v0, p0, Legf;->bYo:I

    iget-object v1, p0, Legf;->bbk:Legl;

    invoke-static {v0, v1}, Lege;->a(ILegl;)V

    .line 152
    return-void

    .line 148
    :cond_1
    iget v0, p0, Legf;->bYp:I

    iget v1, p0, Legf;->bDv:I

    iget-object v2, p0, Legf;->bbk:Legl;

    iget-wide v2, v2, Legl;->amT:J

    iget-object v4, p0, Legf;->bbk:Legl;

    iget-wide v4, v4, Legl;->bUI:J

    invoke-static/range {v0 .. v5}, Lege;->a(IIJJ)V

    goto :goto_0
.end method
