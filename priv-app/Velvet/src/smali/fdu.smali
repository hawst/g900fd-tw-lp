.class public final Lfdu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdr;


# instance fields
.field private cpA:Landroid/location/Location;

.field final cpB:Leqx;

.field final cpn:Lcoc;

.field public final cpo:Lfek;

.field private cpp:I

.field final cpq:Lfee;

.field private final cpr:Lemq;

.field final cps:Ljava/lang/Runnable;

.field final cpt:Ljava/lang/Runnable;

.field final cpu:Ljava/util/WeakHashMap;

.field cpv:Landroid/os/HandlerThread;

.field cpw:Landroid/os/Handler;

.field final cpx:Lenw;

.field private final cpy:Ljava/util/Set;

.field private cpz:Landroid/location/Location;

.field final dK:Ljava/lang/Object;

.field final mClock:Lemp;

.field private final mFileReader:Lfcj;

.field private final mFileWriter:Lfck;

.field private final mGmsLocationProvider:Lguh;

.field private final mGsaPreferenceController:Lchr;

.field private final mLocalBroadcastManager:Lcn;

.field private final mLocationManager:Lfdq;

.field final mLocationSettings:Lcob;

.field private final mLocationStorage:Lfep;


# direct methods
.method public constructor <init>(Lfdq;Lguh;Lemp;Lfcj;Lfck;Lcob;Lchr;Lcn;Lfep;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Lfee;

    invoke-direct {v0, p0}, Lfee;-><init>(Lfdu;)V

    iput-object v0, p0, Lfdu;->cpq:Lfee;

    .line 105
    new-instance v0, Lfed;

    invoke-direct {v0, p0}, Lfed;-><init>(Lfdu;)V

    iput-object v0, p0, Lfdu;->cpr:Lemq;

    .line 106
    new-instance v0, Lfef;

    invoke-direct {v0, p0}, Lfef;-><init>(Lfdu;)V

    iput-object v0, p0, Lfdu;->cps:Ljava/lang/Runnable;

    .line 108
    new-instance v0, Lfec;

    invoke-direct {v0, p0}, Lfec;-><init>(Lfdu;)V

    iput-object v0, p0, Lfdu;->cpt:Ljava/lang/Runnable;

    .line 111
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    .line 116
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfdu;->dK:Ljava/lang/Object;

    .line 121
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lfdu;->cpx:Lenw;

    .line 124
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfdu;->cpy:Ljava/util/Set;

    .line 132
    const-string v0, "LocationOracleImpl"

    sget-object v1, Lfeh;->cpG:Lfeh;

    invoke-static {v0, v1}, Leqx;->a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lfeh;->cpG:Lfeh;

    new-array v2, v4, [Lfeh;

    sget-object v3, Lfeh;->cpH:Lfeh;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lfeh;->cpH:Lfeh;

    new-array v2, v4, [Lfeh;

    sget-object v3, Lfeh;->cpI:Lfeh;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lfeh;->cpH:Lfeh;

    new-array v2, v4, [Lfeh;

    sget-object v3, Lfeh;->cpG:Lfeh;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lfeh;->cpI:Lfeh;

    new-array v2, v4, [Lfeh;

    sget-object v3, Lfeh;->cpH:Lfeh;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lfeh;->cpI:Lfeh;

    new-array v2, v4, [Lfeh;

    sget-object v3, Lfeh;->cpG:Lfeh;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    iput-boolean v4, v0, Leqy;->chN:Z

    iput-boolean v4, v0, Leqy;->chJ:Z

    invoke-virtual {v0}, Leqy;->avx()Leqx;

    move-result-object v0

    iput-object v0, p0, Lfdu;->cpB:Leqx;

    .line 166
    new-instance v0, Lfek;

    invoke-direct {v0, p3}, Lfek;-><init>(Lemp;)V

    iput-object v0, p0, Lfdu;->cpo:Lfek;

    .line 167
    iput-object p1, p0, Lfdu;->mLocationManager:Lfdq;

    .line 168
    iput-object p2, p0, Lfdu;->mGmsLocationProvider:Lguh;

    .line 169
    iput-object p4, p0, Lfdu;->mFileReader:Lfcj;

    .line 170
    iput-object p5, p0, Lfdu;->mFileWriter:Lfck;

    .line 171
    iput-object p3, p0, Lfdu;->mClock:Lemp;

    .line 172
    iput-object p6, p0, Lfdu;->mLocationSettings:Lcob;

    .line 173
    new-instance v0, Lfeg;

    invoke-direct {v0, p0}, Lfeg;-><init>(Lfdu;)V

    iput-object v0, p0, Lfdu;->cpn:Lcoc;

    .line 174
    iput-object p7, p0, Lfdu;->mGsaPreferenceController:Lchr;

    .line 175
    iput-object p8, p0, Lfdu;->mLocalBroadcastManager:Lcn;

    .line 176
    iput-object p9, p0, Lfdu;->mLocationStorage:Lfep;

    .line 177
    return-void
.end method

.method private static Q(Ljava/util/List;)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 885
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 886
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_1

    .line 887
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    :goto_1
    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x4

    aput-object v1, v5, v0

    const/4 v0, 0x5

    aput-object v2, v5, v0

    const/4 v0, 0x6

    aput-object v2, v5, v0

    invoke-static {v5}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    aput v0, v4, v3

    .line 886
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 887
    goto :goto_1

    .line 889
    :cond_1
    invoke-static {v4}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    return v0
.end method

.method private e(Landroid/location/Location;)V
    .locals 4
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 644
    iget-object v0, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 645
    iget-object v0, p0, Lfdu;->cpB:Leqx;

    sget-object v1, Lfeh;->cpI:Lfeh;

    invoke-virtual {v0, v1}, Leqx;->d(Ljava/lang/Enum;)V

    .line 647
    iget-object v0, p0, Lfdu;->cpz:Landroid/location/Location;

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v0, p0, Lfdu;->cpz:Landroid/location/Location;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lfdu;->cpz:Landroid/location/Location;

    invoke-static {v0, p1}, Lgay;->e(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 664
    :cond_1
    :goto_0
    return-void

    .line 655
    :cond_2
    iget-object v1, p0, Lfdu;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 656
    :try_start_0
    iget-object v0, p0, Lfdu;->cpy:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfds;

    .line 657
    iget-object v3, p0, Lfdu;->cpz:Landroid/location/Location;

    invoke-interface {v0, v3, p1}, Lfds;->b(Landroid/location/Location;Landroid/location/Location;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 659
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 661
    iget-object v0, p0, Lfdu;->mLocationStorage:Lfep;

    iget-object v1, p0, Lfdu;->mGsaPreferenceController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "lastgeofenceloc"

    invoke-virtual {v0, p1, v1, v2}, Lfep;->a(Landroid/location/Location;Lcyg;Ljava/lang/String;)V

    .line 663
    iput-object p1, p0, Lfdu;->cpz:Landroid/location/Location;

    goto :goto_0
.end method

.method private f(Landroid/location/Location;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 674
    iget-object v1, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    .line 675
    iget-object v1, p0, Lfdu;->cpB:Leqx;

    sget-object v2, Lfeh;->cpI:Lfeh;

    invoke-virtual {v1, v2}, Leqx;->d(Ljava/lang/Enum;)V

    .line 677
    iget-object v1, p0, Lfdu;->cpA:Landroid/location/Location;

    iget-object v2, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v2}, Lenw;->auS()Lenw;

    if-eqz p1, :cond_3

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 678
    iget-object v0, p0, Lfdu;->mLocationStorage:Lfep;

    iget-object v1, p0, Lfdu;->mGsaPreferenceController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "lastloc"

    invoke-virtual {v0, p1, v1, v2}, Lfep;->a(Landroid/location/Location;Lcyg;Ljava/lang/String;)V

    .line 680
    iput-object p1, p0, Lfdu;->cpA:Landroid/location/Location;

    .line 682
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.LOCATION_CHANGED_SIGNIFICANTLY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 683
    const-string v1, "location"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 684
    iget-object v1, p0, Lfdu;->mLocalBroadcastManager:Lcn;

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 686
    :cond_1
    return-void

    .line 677
    :cond_2
    invoke-static {v1, p1}, Lgay;->d(Landroid/location/Location;Landroid/location/Location;)F

    move-result v1

    const v2, 0x461c4000    # 10000.0f

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final Db()V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lfdu;->cpB:Leqx;

    sget-object v1, Lfeh;->cpG:Lfeh;

    invoke-virtual {v0, v1}, Leqx;->d(Ljava/lang/Enum;)V

    .line 278
    iget-object v0, p0, Lfdu;->cpB:Leqx;

    sget-object v1, Lfeh;->cpH:Lfeh;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    .line 281
    iget-object v0, p0, Lfdu;->mLocationSettings:Lcob;

    iget-object v1, p0, Lfdu;->cpn:Lcoc;

    invoke-interface {v0, v1}, Lcob;->a(Lcoc;)V

    .line 282
    iget-object v0, p0, Lfdu;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lfdu;->startListening()V

    .line 285
    :cond_0
    return-void
.end method

.method public final a(Lfds;)V
    .locals 2

    .prologue
    .line 628
    iget-object v1, p0, Lfdu;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 629
    :try_start_0
    iget-object v0, p0, Lfdu;->cpy:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 630
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final varargs a(Z[Landroid/location/Location;)V
    .locals 8
    .param p2    # [Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 568
    iget-object v1, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    .line 571
    array-length v2, p2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p2, v1

    .line 572
    if-eqz v3, :cond_0

    .line 573
    iget-object v0, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v0, v3}, Lfek;->addLocation(Landroid/location/Location;)V

    .line 574
    const/4 v0, 0x1

    .line 571
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 579
    :cond_1
    if-eqz v0, :cond_6

    .line 580
    iget-object v0, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    iget-object v0, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v0}, Lfek;->ayG()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lilw;->aD(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lfdu;->Q(Ljava/util/List;)I

    move-result v2

    iget v1, p0, Lfdu;->cpp:I

    if-eq v2, v1, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    new-instance v5, Lamh;

    invoke-direct {v5}, Lamh;-><init>()V

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v5, v1}, Lamh;->Q(Ljava/lang/String;)Lamh;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lamh;->b(D)Lamh;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lamh;->c(D)Lamh;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lamh;->p(J)Lamh;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {v1, v0}, Lamh;->w(F)Lamh;

    :cond_2
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v1, ""

    goto :goto_2

    :cond_4
    new-instance v1, Lami;

    invoke-direct {v1}, Lami;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lamh;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamh;

    iput-object v0, v1, Lami;->aeO:[Lamh;

    iget-object v0, p0, Lfdu;->mFileWriter:Lfck;

    const-string v3, "loracle"

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    const/high16 v4, 0x80000

    invoke-virtual {v0, v3, v1, v4}, Lfck;->a(Ljava/lang/String;[BI)Z

    move-result v0

    if-eqz v0, :cond_5

    iput v2, p0, Lfdu;->cpp:I

    .line 581
    :cond_5
    invoke-virtual {p0}, Lfdu;->ayy()Landroid/location/Location;

    move-result-object v0

    .line 582
    invoke-direct {p0, v0}, Lfdu;->e(Landroid/location/Location;)V

    .line 583
    invoke-direct {p0, v0}, Lfdu;->f(Landroid/location/Location;)V

    .line 584
    if-eqz p1, :cond_6

    .line 585
    iget-object v0, p0, Lfdu;->cpq:Lfee;

    invoke-virtual {v0}, Lfee;->reset()V

    .line 588
    :cond_6
    return-void
.end method

.method public final ayA()Landroid/location/Location;
    .locals 1

    .prologue
    .line 473
    new-instance v0, Lfdz;

    invoke-direct {v0, p0}, Lfdz;-><init>(Lfdu;)V

    invoke-virtual {p0, v0}, Lfdu;->b(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    return-object v0
.end method

.method public final ayB()Livq;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lfdu;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKB()Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final ayC()Livq;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lfdu;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKA()Lcgs;

    move-result-object v0

    return-object v0
.end method

.method final ayD()Landroid/os/Handler;
    .locals 4

    .prologue
    .line 258
    iget-object v1, p0, Lfdu;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_0
    iget-object v0, p0, Lfdu;->cpv:Landroid/os/HandlerThread;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    :goto_0
    iget-object v0, p0, Lfdu;->cpw:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 264
    :try_start_1
    iget-object v0, p0, Lfdu;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 265
    :catch_0
    move-exception v0

    .line 266
    :try_start_2
    const-string v2, "LocationOracleImpl"

    const-string v3, "Interrupted while waiting for thread start"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 269
    :cond_0
    :try_start_3
    iget-object v0, p0, Lfdu;->cpw:Landroid/os/Handler;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v0
.end method

.method final ayE()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 540
    iget-object v0, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 545
    invoke-virtual {p0}, Lfdu;->zH()Landroid/location/Location;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_0

    .line 549
    new-array v1, v4, [Landroid/location/Location;

    aput-object v0, v1, v3

    invoke-virtual {p0, v4, v1}, Lfdu;->a(Z[Landroid/location/Location;)V

    .line 559
    :goto_0
    return-void

    .line 555
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/location/Location;

    iget-object v1, p0, Lfdu;->mLocationManager:Lfdq;

    const-string v2, "network"

    invoke-interface {v1, v2}, Lfdq;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v1, p0, Lfdu;->mLocationManager:Lfdq;

    const-string v2, "gps"

    invoke-interface {v1, v2}, Lfdq;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v3, v0}, Lfdu;->a(Z[Landroid/location/Location;)V

    goto :goto_0
.end method

.method final ayF()V
    .locals 4

    .prologue
    .line 717
    iget-object v0, p0, Lfdu;->mGmsLocationProvider:Lguh;

    const-wide/32 v2, 0x45948

    invoke-virtual {v0, v2, v3}, Lguh;->bH(J)Lcgs;

    .line 718
    return-void
.end method

.method public final ayy()Landroid/location/Location;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 448
    iget-object v0, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v0}, Lfek;->ayz()Ljava/util/List;

    move-result-object v0

    .line 450
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 454
    :goto_0
    return-object v0

    .line 453
    :cond_0
    const-string v0, "LocationOracleImpl"

    const-string v1, "Best location was null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ayz()Ljava/util/List;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v0}, Lfek;->ayz()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 818
    new-instance v0, Ljava/util/concurrent/FutureTask;

    invoke-direct {v0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 819
    invoke-virtual {p0}, Lfdu;->ayD()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 821
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 827
    :goto_0
    return-object v0

    .line 822
    :catch_0
    move-exception v0

    .line 823
    const-string v1, "LocationOracleImpl"

    const-string v2, "Unexpected interruption"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 827
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 824
    :catch_1
    move-exception v0

    .line 825
    const-string v1, "LocationOracleImpl"

    const-string v2, "Unexpected exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final bk(J)V
    .locals 5

    .prologue
    .line 486
    invoke-virtual {p0}, Lfdu;->ayy()Landroid/location/Location;

    move-result-object v0

    .line 487
    if-eqz v0, :cond_0

    iget-object v1, p0, Lfdu;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    sub-long v0, v2, v0

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    .line 517
    :goto_0
    return-void

    .line 491
    :cond_0
    invoke-virtual {p0}, Lfdu;->ayD()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lfea;

    invoke-direct {v1, p0, p1, p2}, Lfea;-><init>(Lfdu;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final d(Landroid/location/Location;)V
    .locals 3

    .prologue
    .line 727
    iget-object v0, p0, Lfdu;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    :goto_0
    return-void

    .line 732
    :cond_0
    iget-object v1, p0, Lfdu;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 733
    :try_start_0
    iget-object v0, p0, Lfdu;->cpu:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 734
    const-string v0, "LocationOracleImpl"

    const-string v2, "Not started: ignore location"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 750
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 737
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lfdu;->ayD()Landroid/os/Handler;

    move-result-object v0

    new-instance v2, Lfeb;

    invoke-direct {v2, p0, p1}, Lfeb;-><init>(Lfdu;Landroid/location/Location;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 750
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final lK(Ljava/lang/String;)Lfdt;
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lfdv;

    invoke-direct {v0, p0, p1}, Lfdv;-><init>(Lfdu;Ljava/lang/String;)V

    .line 216
    return-object v0
.end method

.method final startListening()V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 333
    iget-object v2, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v2}, Lenw;->auS()Lenw;

    .line 335
    iget-object v2, p0, Lfdu;->mLocationSettings:Lcob;

    invoke-interface {v2}, Lcob;->QO()Z

    move-result v2

    if-nez v2, :cond_0

    .line 354
    :goto_0
    return-void

    .line 339
    :cond_0
    iget-object v2, p0, Lfdu;->cpB:Leqx;

    sget-object v3, Lfeh;->cpI:Lfeh;

    invoke-virtual {v2, v3}, Leqx;->a(Ljava/lang/Enum;)V

    .line 342
    invoke-virtual {p0}, Lfdu;->ayF()V

    .line 343
    iget-object v2, p0, Lfdu;->mClock:Lemp;

    iget-object v3, p0, Lfdu;->cpr:Lemq;

    invoke-interface {v2, v3}, Lemp;->a(Lemq;)V

    .line 344
    iget-object v2, p0, Lfdu;->cpq:Lfee;

    invoke-virtual {v2}, Lfee;->reset()V

    .line 347
    iget-object v2, p0, Lfdu;->mFileReader:Lfcj;

    const-string v3, "loracle"

    const/high16 v4, 0x80000

    invoke-virtual {v2, v3, v4}, Lfcj;->B(Ljava/lang/String;I)[B

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_0
    new-instance v3, Lami;

    invoke-direct {v3}, Lami;-><init>()V

    invoke-static {v3, v2}, Ljsr;->c(Ljsr;[B)Ljsr;

    iget-object v2, v3, Lami;->aeO:[Lamh;

    array-length v2, v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v3, v3, Lami;->aeO:[Lamh;

    array-length v5, v3

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v3, v2

    new-instance v7, Landroid/location/Location;

    invoke-virtual {v6}, Lamh;->getProvider()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lamh;->mR()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {v6}, Lamh;->mS()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {v6}, Lamh;->getTimestampMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/location/Location;->setTime(J)V

    invoke-virtual {v6}, Lamh;->mU()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v6}, Lamh;->mT()F

    move-result v6

    invoke-virtual {v7, v6}, Landroid/location/Location;->setAccuracy(F)V

    :cond_1
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v6, v7}, Lfek;->addLocation(Landroid/location/Location;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-static {v4}, Lfdu;->Q(Ljava/util/List;)I

    move-result v2

    iput v2, p0, Lfdu;->cpp:I

    iget-object v2, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v2}, Lfek;->ayy()Landroid/location/Location;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v3, p0, Lfdu;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    sub-long v2, v4, v2

    const-wide/32 v4, 0x45948

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lfdu;->ayE()V

    .line 350
    :cond_4
    :goto_3
    iget-object v0, p0, Lfdu;->mLocationStorage:Lfep;

    iget-object v1, p0, Lfdu;->mGsaPreferenceController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "lastgeofenceloc"

    invoke-virtual {v0, v1, v2}, Lfep;->a(Lcyg;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lfdu;->cpz:Landroid/location/Location;

    .line 352
    iget-object v0, p0, Lfdu;->mLocationStorage:Lfep;

    iget-object v1, p0, Lfdu;->mGsaPreferenceController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "lastloc"

    invoke-virtual {v0, v1, v2}, Lfep;->a(Lcyg;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lfdu;->cpA:Landroid/location/Location;

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 347
    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "LocationOracleImpl"

    const-string v1, "File storage contained invalid data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lfdu;->ayE()V

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lfdu;->ayE()V

    throw v0
.end method

.method final stopListening()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 358
    iget-object v0, p0, Lfdu;->cpx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 361
    invoke-direct {p0, v1}, Lfdu;->e(Landroid/location/Location;)V

    .line 362
    invoke-direct {p0, v1}, Lfdu;->f(Landroid/location/Location;)V

    .line 364
    iget-object v0, p0, Lfdu;->cpB:Leqx;

    sget-object v1, Lfeh;->cpH:Lfeh;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    .line 367
    iget-object v0, p0, Lfdu;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKx()Lcgs;

    .line 368
    iget-object v0, p0, Lfdu;->mClock:Lemp;

    iget-object v1, p0, Lfdu;->cpr:Lemq;

    invoke-interface {v0, v1}, Lemp;->b(Lemq;)V

    .line 369
    iget-object v0, p0, Lfdu;->cpw:Landroid/os/Handler;

    iget-object v1, p0, Lfdu;->cps:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 370
    iget-object v0, p0, Lfdu;->cpw:Landroid/os/Handler;

    iget-object v1, p0, Lfdu;->cpq:Lfee;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 373
    iget-object v0, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v0}, Lfek;->ayH()V

    .line 374
    iget-object v0, p0, Lfdu;->mFileWriter:Lfck;

    const-string v1, "loracle"

    invoke-virtual {v0, v1}, Lfck;->lH(Ljava/lang/String;)V

    .line 375
    return-void
.end method

.method public final tv()Z
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lfdu;->cpo:Lfek;

    invoke-virtual {v0}, Lfek;->ayz()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final zH()Landroid/location/Location;
    .locals 4

    .prologue
    .line 531
    iget-object v0, p0, Lfdu;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKB()Lcgs;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcgs;->c(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    return-object v0
.end method
