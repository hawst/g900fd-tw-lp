.class public final Lieh;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAh:Ljava/lang/String;

.field private dAi:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2571
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2572
    iput v1, p0, Lieh;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lieh;->dAh:Ljava/lang/String;

    iput-boolean v1, p0, Lieh;->dAi:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lieh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lieh;->eCz:I

    .line 2573
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2511
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lieh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lieh;->dAh:Ljava/lang/String;

    iget v0, p0, Lieh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lieh;->dAi:Z

    iget v0, p0, Lieh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieh;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2587
    iget v0, p0, Lieh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2588
    const/4 v0, 0x1

    iget-object v1, p0, Lieh;->dAh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2590
    :cond_0
    iget v0, p0, Lieh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2591
    const/4 v0, 0x2

    iget-boolean v1, p0, Lieh;->dAi:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2593
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2594
    return-void
.end method

.method public final aVD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2533
    iget-object v0, p0, Lieh;->dAh:Ljava/lang/String;

    return-object v0
.end method

.method public final aVE()Z
    .locals 1

    .prologue
    .line 2544
    iget v0, p0, Lieh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aVF()Z
    .locals 1

    .prologue
    .line 2555
    iget-boolean v0, p0, Lieh;->dAi:Z

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2598
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2599
    iget v1, p0, Lieh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2600
    const/4 v1, 0x1

    iget-object v2, p0, Lieh;->dAh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2603
    :cond_0
    iget v1, p0, Lieh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2604
    const/4 v1, 0x2

    iget-boolean v2, p0, Lieh;->dAi:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2607
    :cond_1
    return v0
.end method

.method public final pm(Ljava/lang/String;)Lieh;
    .locals 1

    .prologue
    .line 2536
    if-nez p1, :cond_0

    .line 2537
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2539
    :cond_0
    iput-object p1, p0, Lieh;->dAh:Ljava/lang/String;

    .line 2540
    iget v0, p0, Lieh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieh;->aez:I

    .line 2541
    return-object p0
.end method
