.class public final Lcmh;
.super Lcmy;
.source "PG"

# interfaces
.implements Lcoi;
.implements Lcpt;


# instance fields
.field private aTF:Z

.field private bbA:Z

.field private final bbH:Ljava/lang/String;

.field private final bbI:Z

.field private bbJ:Z

.field private bbK:Lcma;

.field private bbL:Z

.field private bbM:Lcmg;

.field private bbN:Z

.field private bbO:Leeb;

.field private final bbP:Lcmc;

.field private final bbk:Legl;

.field private final dK:Ljava/lang/Object;

.field private final mCookies:Lgpf;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mGsaConfig:Lchk;

.field private final mHttpHelper:Ldkx;

.field private final mQuery:Lcom/google/android/shared/search/Query;

.field private final mSdchManager:Lczz;

.field private final mStaticContentCache:Lcpq;

.field private mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcom/google/android/shared/search/Query;Ljava/lang/String;ZLchk;Ldkx;Lcpn;Ljava/util/concurrent/ExecutorService;Lczz;Legl;Lgpf;Lcpq;)V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Lcmy;-><init>()V

    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcmh;->dK:Ljava/lang/Object;

    .line 107
    new-instance v0, Lcmi;

    invoke-direct {v0, p0}, Lcmi;-><init>(Lcmh;)V

    iput-object v0, p0, Lcmh;->bbP:Lcmc;

    .line 178
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqx()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 179
    iput-object p1, p0, Lcmh;->mQuery:Lcom/google/android/shared/search/Query;

    .line 180
    iput-object p2, p0, Lcmh;->bbH:Ljava/lang/String;

    .line 181
    iput-boolean p3, p0, Lcmh;->bbI:Z

    .line 182
    iput-object p4, p0, Lcmh;->mGsaConfig:Lchk;

    .line 183
    iput-object p5, p0, Lcmh;->mHttpHelper:Ldkx;

    .line 184
    iput-object p6, p0, Lcmh;->mUrlHelper:Lcpn;

    .line 185
    iput-object p7, p0, Lcmh;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 186
    iput-object p8, p0, Lcmh;->mSdchManager:Lczz;

    .line 187
    iput-object p9, p0, Lcmh;->bbk:Legl;

    .line 188
    iput-object p10, p0, Lcmh;->mCookies:Lgpf;

    .line 189
    iput-object p11, p0, Lcmh;->mStaticContentCache:Lcpq;

    .line 190
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private PZ()V
    .locals 7

    .prologue
    .line 210
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0}, Lcmz;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 211
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-ne v0, v1, :cond_0

    .line 246
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v1, p0, Lcmh;->mStaticContentCache:Lcpq;

    invoke-virtual {v1}, Lcpq;->PE()Leeb;

    move-result-object v1

    .line 219
    iget-object v2, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 220
    :try_start_0
    iget-boolean v3, p0, Lcmh;->bbL:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcmh;->bbN:Z

    if-eqz v3, :cond_1

    if-nez v1, :cond_3

    :cond_1
    iget-boolean v3, p0, Lcmh;->bbJ:Z

    if-nez v3, :cond_3

    .line 222
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 224
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcmh;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    .line 225
    iget-boolean v4, p0, Lcmh;->bbN:Z

    .line 226
    iget-object v5, p0, Lcmh;->bbK:Lcma;

    invoke-virtual {v5}, Lcma;->PT()Ljava/io/InputStream;

    move-result-object v5

    .line 227
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcmh;->bbL:Z

    .line 228
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcpn;->a(Lcom/google/android/search/shared/api/UriRequest;Ljava/lang/String;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 234
    if-eqz v4, :cond_4

    .line 238
    new-instance v2, Ldki;

    const-string v3, "preload"

    new-instance v4, Lcmj;

    invoke-direct {v4, p0, v3}, Lcmj;-><init>(Lcmh;Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ldki;-><init>(Ldkj;)V

    .line 240
    new-instance v3, Lcmg;

    iget-object v4, p0, Lcmh;->bbk:Legl;

    invoke-direct {v3, v2, v4}, Lcmg;-><init>(Ldkc;Legl;)V

    iput-object v3, p0, Lcmh;->bbM:Lcmg;

    .line 241
    new-instance v3, Ldmg;

    invoke-direct {v3, v2, v5}, Ldmg;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    .line 242
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v2

    new-instance v4, Ldyo;

    invoke-direct {v4, v0, v1, v3}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    invoke-interface {v2, v4}, Lcmz;->b(Ldyo;)V

    goto :goto_0

    .line 244
    :cond_4
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v1

    new-instance v2, Ldyo;

    iget-object v3, p0, Lcmh;->bbO:Leeb;

    invoke-direct {v2, v0, v3, v5}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    invoke-interface {v1, v2}, Lcmz;->b(Ldyo;)V

    goto :goto_0
.end method

.method private Qa()V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lcmh;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->JY()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 256
    :try_start_0
    iget-object v0, p0, Lcmh;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    .line 257
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    iget-object v1, p0, Lcmh;->mUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->t(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v0}, Lcmh;->d(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcmh;->mUrlHelper:Lcpn;

    iget-object v1, p0, Lcmh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcpn;->q(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcmh;->mStaticContentCache:Lcpq;

    invoke-virtual {v0}, Lcpq;->PF()Ljava/util/List;

    move-result-object v0

    .line 267
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 270
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcmh;->bbN:Z

    .line 271
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 272
    invoke-direct {p0}, Lcmh;->PZ()V

    .line 273
    iget-object v1, p0, Lcmh;->bbM:Lcmg;

    if-eqz v1, :cond_0

    .line 274
    const/16 v1, 0x14a

    iget-object v2, p0, Lcmh;->bbk:Legl;

    invoke-static {v1, v2}, Lege;->a(ILegl;)V

    .line 279
    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 280
    iget-object v2, p0, Lcmh;->bbM:Lcmg;

    invoke-virtual {v2, v0}, Lcmg;->w([B)V
    :try_end_2
    .catch Lefs; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v1

    invoke-interface {v1, v0}, Lcmz;->c(Lefq;)V

    .line 285
    :cond_2
    iget-object v0, p0, Lcmh;->bbM:Lcmg;

    invoke-virtual {v0}, Lcmg;->PY()V

    goto :goto_0

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 271
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private Qb()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 413
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 414
    :try_start_0
    iget-boolean v2, p0, Lcmh;->bbN:Z

    .line 415
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    iget-object v1, p0, Lcmh;->bbM:Lcmg;

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 419
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 420
    :try_start_1
    iget-object v0, p0, Lcmh;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    .line 421
    iget-object v2, p0, Lcmh;->bbK:Lcma;

    invoke-virtual {v2}, Lcma;->PU()Ljava/io/InputStream;

    move-result-object v2

    .line 425
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcmh;->bbN:Z

    .line 426
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 427
    iget-object v1, p0, Lcmh;->mUrlHelper:Lcpn;

    invoke-static {v0}, Lcpn;->b(Lcom/google/android/search/shared/api/UriRequest;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 428
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v1

    new-instance v3, Ldyo;

    iget-object v4, p0, Lcmh;->bbO:Leeb;

    invoke-direct {v3, v0, v4, v2}, Ldyo;-><init>(Lcom/google/android/search/shared/api/UriRequest;Leeb;Ljava/io/InputStream;)V

    invoke-interface {v1, v3}, Lcmz;->a(Ldyo;)V

    .line 429
    const/4 v0, 0x1

    .line 431
    :cond_0
    return v0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 426
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcmh;Leeb;)Leeb;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcmh;->bbO:Leeb;

    return-object p1
.end method

.method static synthetic a(Lcmh;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcmh;->dK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcmh;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcmh;->bbA:Z

    return p1
.end method

.method static synthetic b(Lcmh;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcmh;->bbA:Z

    return v0
.end method

.method static synthetic b(Lcmh;Z)Z
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcmh;->aTF:Z

    return p1
.end method

.method static synthetic c(Lcmh;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcmh;->aTF:Z

    return v0
.end method

.method static synthetic c(Lcmh;Z)Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmh;->bbJ:Z

    return v0
.end method

.method static synthetic d(Lcmh;)Lcma;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcmh;->bbK:Lcma;

    return-object v0
.end method

.method private static d(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apT()Lehm;

    move-result-object v0

    invoke-virtual {v0}, Lehm;->asL()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcmh;)Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcmh;->bbJ:Z

    return v0
.end method

.method static synthetic f(Lcmh;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcmh;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    return-object v0
.end method

.method static synthetic g(Lcmh;)Lgpf;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcmh;->mCookies:Lgpf;

    return-object v0
.end method

.method static synthetic h(Lcmh;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcmh;->PZ()V

    return-void
.end method

.method static synthetic i(Lcmh;)Lcpq;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcmh;->mStaticContentCache:Lcpq;

    return-object v0
.end method

.method static synthetic j(Lcmh;)Leeb;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcmh;->bbO:Leeb;

    return-object v0
.end method

.method static synthetic k(Lcmh;)Z
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcmh;->Qb()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final N(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 450
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    new-instance v1, Lcqt;

    iget-object v2, p0, Lcmh;->bbH:Ljava/lang/String;

    invoke-direct {v1, p1, p2, v2}, Lcqt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcmz;->a(Lcqt;)V

    .line 452
    return-void
.end method

.method public final PB()V
    .locals 1

    .prologue
    .line 472
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0}, Lcmz;->Qz()V

    .line 473
    return-void
.end method

.method public final PC()Z
    .locals 1

    .prologue
    .line 494
    invoke-direct {p0}, Lcmh;->Qb()Z

    move-result v0

    return v0
.end method

.method public final Qc()Lcmc;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcmh;->bbP:Lcmc;

    return-object v0
.end method

.method public final a(Lcms;)V
    .locals 1

    .prologue
    .line 467
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->b(Lcms;)V

    .line 468
    return-void
.end method

.method public final a(Lcmz;)V
    .locals 13
    .param p1    # Lcmz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 316
    invoke-super {p0, p1}, Lcmy;->a(Lcmz;)V

    .line 319
    const/4 v11, 0x0

    .line 328
    :try_start_0
    invoke-virtual {p0}, Lcmh;->isFailed()Z

    move-result v0

    if-nez v0, :cond_8

    .line 329
    iget-object v0, p0, Lcmh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Lcmh;->c(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 330
    :try_start_1
    invoke-virtual {v10}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v12, Ldlb;

    invoke-virtual {v10}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v12, v0, v1}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcmh;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->JG()Z

    move-result v0

    invoke-virtual {v12, v0}, Ldlb;->dz(Z)V

    .line 331
    invoke-virtual {v12}, Ldlb;->getUrl()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 337
    :try_start_2
    iget-boolean v0, p0, Lcmh;->bbI:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcmh;->mUrlHelper:Lcpn;

    iget-object v1, p0, Lcmh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcpn;->q(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v6

    :goto_0
    iget-object v1, p0, Lcmh;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Lcmh;->d(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v6, 0x4

    :cond_1
    :goto_1
    new-instance v0, Lclu;

    iget-object v1, p0, Lcmh;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcmh;->mGsaConfig:Lchk;

    iget-object v4, p0, Lcmh;->mSdchManager:Lczz;

    iget-object v5, p0, Lcmh;->bbk:Legl;

    iget-object v8, p0, Lcmh;->mStaticContentCache:Lcpq;

    move-object v3, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lclu;-><init>(Ljava/util/concurrent/ExecutorService;Lchk;Lcoi;Lczz;Legl;ILcpt;Lcpq;)V

    iget-object v1, p0, Lcmh;->mHttpHelper:Ldkx;

    const/16 v2, 0xb

    invoke-virtual {v1, v12, v2, v0}, Ldkx;->a(Ldlb;ILdlg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcma;
    :try_end_2
    .catch Lefq; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-object v9, v10

    .line 345
    :goto_2
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 346
    :try_start_3
    iput-object v0, p0, Lcmh;->bbK:Lcma;

    .line 347
    iput-object v9, p0, Lcmh;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    .line 348
    if-nez v0, :cond_2

    .line 349
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcmh;->bbA:Z

    .line 351
    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 352
    if-eqz v0, :cond_3

    .line 355
    iget-object v1, p0, Lcmh;->bbP:Lcmc;

    invoke-virtual {v0, v1}, Lcma;->a(Lcmc;)V

    .line 356
    invoke-direct {p0}, Lcmh;->Qa()V

    .line 359
    :cond_3
    return-void

    .line 337
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    if-eqz v0, :cond_1

    const/4 v6, 0x2

    goto :goto_1

    .line 338
    :catch_0
    move-exception v0

    .line 339
    :try_start_4
    invoke-interface {p1, v0}, Lcmz;->c(Lefq;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-object v0, v9

    move-object v9, v10

    goto :goto_2

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 345
    :catchall_1
    move-exception v0

    move-object v10, v9

    :goto_3
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 346
    :try_start_5
    iput-object v11, p0, Lcmh;->bbK:Lcma;

    .line 347
    iput-object v10, p0, Lcmh;->mUriRequest:Lcom/google/android/search/shared/api/UriRequest;

    .line 348
    if-nez v9, :cond_6

    .line 349
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcmh;->bbA:Z

    .line 351
    :cond_6
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 352
    if-eqz v9, :cond_7

    .line 355
    iget-object v1, p0, Lcmh;->bbP:Lcmc;

    invoke-virtual {v9, v1}, Lcma;->a(Lcmc;)V

    .line 356
    invoke-direct {p0}, Lcmh;->Qa()V

    :cond_7
    throw v0

    .line 351
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 345
    :catchall_3
    move-exception v0

    goto :goto_3

    :cond_8
    move-object v0, v9

    goto :goto_2
.end method

.method public final a(Lcom/google/android/velvet/ActionData;)V
    .locals 0

    .prologue
    .line 443
    invoke-virtual {p0, p1}, Lcmh;->c(Lcom/google/android/velvet/ActionData;)V

    .line 446
    return-void
.end method

.method public final a(Ljyw;)V
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->b(Ljyw;)V

    .line 483
    return-void
.end method

.method public final c(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lcmh;->bbI:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmh;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->o(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcmh;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->k(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    goto :goto_0
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 194
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-boolean v0, p0, Lcmh;->aTF:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcmh;->bbA:Z

    if-nez v0, :cond_0

    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmh;->bbA:Z

    .line 197
    iget-object v0, p0, Lcmh;->bbK:Lcma;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 199
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final cu(Z)V
    .locals 1

    .prologue
    .line 462
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    :goto_0
    invoke-virtual {p0, v0}, Lcmh;->c(Lcom/google/android/velvet/ActionData;)V

    .line 463
    return-void

    .line 462
    :cond_0
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    goto :goto_0
.end method

.method public final fe(I)V
    .locals 1

    .prologue
    .line 487
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->fe(I)V

    .line 488
    return-void
.end method

.method public final g([I)V
    .locals 1

    .prologue
    .line 477
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->h([I)V

    .line 478
    return-void
.end method

.method public final hn(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 456
    invoke-virtual {p0}, Lcmh;->QE()Lcmz;

    move-result-object v0

    invoke-interface {v0, p1}, Lcmz;->hr(Ljava/lang/String;)V

    .line 457
    invoke-direct {p0}, Lcmh;->PZ()V

    .line 458
    return-void
.end method

.method public final isFailed()Z
    .locals 2

    .prologue
    .line 204
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 205
    :try_start_0
    iget-boolean v0, p0, Lcmh;->bbA:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 507
    iget-object v1, p0, Lcmh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 509
    :try_start_0
    iget-object v0, p0, Lcmh;->bbK:Lcma;

    if-nez v0, :cond_0

    .line 510
    const-string v0, "not started"

    .line 516
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpFetchTask{"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 511
    :cond_0
    iget-object v0, p0, Lcmh;->bbK:Lcma;

    invoke-virtual {v0}, Lcma;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 512
    const-string v0, "complete"

    goto :goto_0

    .line 514
    :cond_1
    const-string v0, "not complete"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 517
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
