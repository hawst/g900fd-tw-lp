.class public final Ljda;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ebg:[Ljda;


# instance fields
.field public eaV:Ljbp;

.field public ebh:[Ljbp;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3894
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3895
    iput-object v1, p0, Ljda;->eaV:Ljbp;

    invoke-static {}, Ljbp;->beZ()[Ljbp;

    move-result-object v0

    iput-object v0, p0, Ljda;->ebh:[Ljbp;

    iput-object v1, p0, Ljda;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljda;->eCz:I

    .line 3896
    return-void
.end method

.method public static bgR()[Ljda;
    .locals 2

    .prologue
    .line 3877
    sget-object v0, Ljda;->ebg:[Ljda;

    if-nez v0, :cond_1

    .line 3878
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3880
    :try_start_0
    sget-object v0, Ljda;->ebg:[Ljda;

    if-nez v0, :cond_0

    .line 3881
    const/4 v0, 0x0

    new-array v0, v0, [Ljda;

    sput-object v0, Ljda;->ebg:[Ljda;

    .line 3883
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3885
    :cond_1
    sget-object v0, Ljda;->ebg:[Ljda;

    return-object v0

    .line 3883
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3871
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljda;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljda;->eaV:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljda;->eaV:Ljbp;

    :cond_1
    iget-object v0, p0, Ljda;->eaV:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljda;->ebh:[Ljbp;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbp;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljda;->ebh:[Ljbp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljda;->ebh:[Ljbp;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljda;->ebh:[Ljbp;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 3909
    iget-object v0, p0, Ljda;->eaV:Ljbp;

    if-eqz v0, :cond_0

    .line 3910
    const/4 v0, 0x1

    iget-object v1, p0, Ljda;->eaV:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3912
    :cond_0
    iget-object v0, p0, Ljda;->ebh:[Ljbp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljda;->ebh:[Ljbp;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3913
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljda;->ebh:[Ljbp;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3914
    iget-object v1, p0, Ljda;->ebh:[Ljbp;

    aget-object v1, v1, v0

    .line 3915
    if-eqz v1, :cond_1

    .line 3916
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 3913
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3920
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3921
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 3925
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3926
    iget-object v1, p0, Ljda;->eaV:Ljbp;

    if-eqz v1, :cond_0

    .line 3927
    const/4 v1, 0x1

    iget-object v2, p0, Ljda;->eaV:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3930
    :cond_0
    iget-object v1, p0, Ljda;->ebh:[Ljbp;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljda;->ebh:[Ljbp;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 3931
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljda;->ebh:[Ljbp;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3932
    iget-object v2, p0, Ljda;->ebh:[Ljbp;

    aget-object v2, v2, v0

    .line 3933
    if-eqz v2, :cond_1

    .line 3934
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3931
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 3939
    :cond_3
    return v0
.end method
