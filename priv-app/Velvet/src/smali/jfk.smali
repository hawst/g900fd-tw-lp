.class public final Ljfk;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private eid:Z

.field private eie:Z

.field private eif:Z

.field private eig:Z

.field private eih:Z

.field private eii:Z

.field public eij:[Ljfl;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 13395
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13396
    iput v0, p0, Ljfk;->aez:I

    iput-object v2, p0, Ljfk;->efN:Ljez;

    iput-boolean v0, p0, Ljfk;->eid:Z

    iput-boolean v0, p0, Ljfk;->eie:Z

    iput-boolean v0, p0, Ljfk;->eif:Z

    iput-boolean v1, p0, Ljfk;->eig:Z

    iput-boolean v1, p0, Ljfk;->eih:Z

    iput-boolean v1, p0, Ljfk;->eii:Z

    invoke-static {}, Ljfl;->bjr()[Ljfl;

    move-result-object v0

    iput-object v0, p0, Ljfk;->eij:[Ljfl;

    iput-object v2, p0, Ljfk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfk;->eCz:I

    .line 13397
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12979
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljfk;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfk;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljfk;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfk;->eid:Z

    iget v0, p0, Ljfk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfk;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfk;->eie:Z

    iget v0, p0, Ljfk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfk;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfk;->eif:Z

    iget v0, p0, Ljfk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfk;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfk;->eig:Z

    iget v0, p0, Ljfk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfk;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfk;->eih:Z

    iget v0, p0, Ljfk;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfk;->aez:I

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljfk;->eij:[Ljfl;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljfl;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljfk;->eij:[Ljfl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljfl;

    invoke-direct {v3}, Ljfl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljfk;->eij:[Ljfl;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljfl;

    invoke-direct {v3}, Ljfl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljfk;->eij:[Ljfl;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfk;->eii:Z

    iget v0, p0, Ljfk;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljfk;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 13417
    iget-object v0, p0, Ljfk;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 13418
    const/4 v0, 0x1

    iget-object v1, p0, Ljfk;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 13420
    :cond_0
    iget v0, p0, Ljfk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 13421
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfk;->eid:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13423
    :cond_1
    iget v0, p0, Ljfk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 13424
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljfk;->eie:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13426
    :cond_2
    iget v0, p0, Ljfk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 13427
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfk;->eif:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13429
    :cond_3
    iget v0, p0, Ljfk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 13430
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljfk;->eig:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13432
    :cond_4
    iget v0, p0, Ljfk;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 13433
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljfk;->eih:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13435
    :cond_5
    iget-object v0, p0, Ljfk;->eij:[Ljfl;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljfk;->eij:[Ljfl;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 13436
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljfk;->eij:[Ljfl;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 13437
    iget-object v1, p0, Ljfk;->eij:[Ljfl;

    aget-object v1, v1, v0

    .line 13438
    if-eqz v1, :cond_6

    .line 13439
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 13436
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13443
    :cond_7
    iget v0, p0, Ljfk;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 13444
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljfk;->eii:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13446
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13447
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 13451
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13452
    iget-object v1, p0, Ljfk;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 13453
    const/4 v1, 0x1

    iget-object v2, p0, Ljfk;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13456
    :cond_0
    iget v1, p0, Ljfk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 13457
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfk;->eid:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13460
    :cond_1
    iget v1, p0, Ljfk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 13461
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljfk;->eie:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13464
    :cond_2
    iget v1, p0, Ljfk;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 13465
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfk;->eif:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13468
    :cond_3
    iget v1, p0, Ljfk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 13469
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljfk;->eig:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13472
    :cond_4
    iget v1, p0, Ljfk;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 13473
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljfk;->eih:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13476
    :cond_5
    iget-object v1, p0, Ljfk;->eij:[Ljfl;

    if-eqz v1, :cond_8

    iget-object v1, p0, Ljfk;->eij:[Ljfl;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 13477
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljfk;->eij:[Ljfl;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 13478
    iget-object v2, p0, Ljfk;->eij:[Ljfl;

    aget-object v2, v2, v0

    .line 13479
    if-eqz v2, :cond_6

    .line 13480
    const/4 v3, 0x7

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13477
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    move v0, v1

    .line 13485
    :cond_8
    iget v1, p0, Ljfk;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_9

    .line 13486
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljfk;->eii:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 13489
    :cond_9
    return v0
.end method
