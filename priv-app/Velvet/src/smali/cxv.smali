.class final Lcxv;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final blk:Landroid/preference/Preference;

.field private final bmz:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 61
    iput-object p1, p0, Lcxv;->mContext:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcxv;->blk:Landroid/preference/Preference;

    .line 63
    iput-object p3, p0, Lcxv;->bmz:Landroid/net/Uri;

    .line 64
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x0

    iget-object v1, p0, Lcxv;->bmz:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcxv;->bmz:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcxv;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcxv;->bmz:Landroid/net/Uri;

    invoke-static {v1, v2}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcxv;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcxv;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0170

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 55
    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcxv;->blk:Landroid/preference/Preference;

    invoke-static {v0}, Lcxu;->m(Landroid/preference/Preference;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcxv;->bmz:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxv;->blk:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
