.class public final Ljqn;
.super Ljsl;
.source "PG"


# static fields
.field public static final eza:Ljsm;


# instance fields
.field private aez:I

.field private ezb:I

.field private ezc:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3855
    const/16 v0, 0xb

    const-class v1, Ljqn;

    const v2, 0x206db76a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqn;->eza:Ljsm;

    .line 3868
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3913
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3914
    iput v0, p0, Ljqn;->aez:I

    iput v0, p0, Ljqn;->ezb:I

    iput-boolean v0, p0, Ljqn;->ezc:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljqn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqn;->eCz:I

    .line 3915
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3848
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqn;->ezb:I

    iget v0, p0, Ljqn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqn;->ezc:Z

    iget v0, p0, Ljqn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqn;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3929
    iget v0, p0, Ljqn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3930
    const/4 v0, 0x1

    iget v1, p0, Ljqn;->ezb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3932
    :cond_0
    iget v0, p0, Ljqn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3933
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljqn;->ezc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 3935
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3936
    return-void
.end method

.method public final akb()I
    .locals 1

    .prologue
    .line 3878
    iget v0, p0, Ljqn;->ezb:I

    return v0
.end method

.method public final akc()Z
    .locals 1

    .prologue
    .line 3897
    iget-boolean v0, p0, Ljqn;->ezc:Z

    return v0
.end method

.method public final iV(Z)Ljqn;
    .locals 1

    .prologue
    .line 3900
    iput-boolean p1, p0, Ljqn;->ezc:Z

    .line 3901
    iget v0, p0, Ljqn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqn;->aez:I

    .line 3902
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3940
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3941
    iget v1, p0, Ljqn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3942
    const/4 v1, 0x1

    iget v2, p0, Ljqn;->ezb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3945
    :cond_0
    iget v1, p0, Ljqn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3946
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljqn;->ezc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3949
    :cond_1
    return v0
.end method

.method public final rw(I)Ljqn;
    .locals 1

    .prologue
    .line 3881
    iput p1, p0, Ljqn;->ezb:I

    .line 3882
    iget v0, p0, Ljqn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqn;->aez:I

    .line 3883
    return-object p0
.end method
