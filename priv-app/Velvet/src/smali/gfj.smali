.class final Lgfj;
.super Landroid/media/AudioRecord;
.source "PG"


# instance fields
.field private cID:[B

.field private cIE:I

.field private hV:J


# direct methods
.method constructor <init>(Lgfi;)V
    .locals 14

    .prologue
    const/16 v2, 0x1f40

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    const/4 v6, 0x0

    .line 33
    const/4 v1, 0x6

    const/16 v3, 0x10

    const/4 v4, 0x2

    const v5, 0x1f400

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 29
    const/16 v0, 0x3e80

    new-array v0, v0, [B

    iput-object v0, p0, Lgfj;->cID:[B

    .line 30
    iput v6, p0, Lgfj;->cIE:I

    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgfj;->hV:J

    move v0, v6

    .line 37
    :goto_0
    if-ge v0, v2, :cond_0

    .line 38
    int-to-double v4, v0

    const-wide v8, 0x40bf400000000000L    # 8000.0

    div-double/2addr v4, v8

    .line 39
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v8, v4

    mul-double/2addr v8, v12

    const-wide v10, 0x4060b66666666666L    # 133.7

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    .line 40
    mul-double/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v4, v8

    .line 42
    const-wide v8, 0x405fc00000000000L    # 127.0

    mul-double/2addr v4, v8

    double-to-int v1, v4

    int-to-byte v1, v1

    .line 43
    iget-object v3, p0, Lgfj;->cID:[B

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    aput-byte v1, v3, v4

    .line 44
    iget-object v1, p0, Lgfj;->cID:[B

    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x0

    aput-byte v6, v1, v3

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public final getRecordingState()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x3

    return v0
.end method

.method public final read([BII)I
    .locals 8

    .prologue
    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lgfj;->hV:J

    sub-long v2, v0, v2

    .line 55
    const/4 v0, 0x0

    .line 56
    :goto_0
    iget v1, p0, Lgfj;->cIE:I

    int-to-long v4, v1

    const-wide/16 v6, 0x8

    mul-long/2addr v6, v2

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    if-ge v0, p3, :cond_0

    .line 57
    iget v1, p0, Lgfj;->cIE:I

    mul-int/lit8 v1, v1, 0x2

    iget-object v4, p0, Lgfj;->cID:[B

    array-length v4, v4

    rem-int/2addr v1, v4

    .line 58
    add-int v4, p2, v0

    iget-object v5, p0, Lgfj;->cID:[B

    aget-byte v5, v5, v1

    aput-byte v5, p1, v4

    .line 59
    add-int/lit8 v0, v0, 0x1

    .line 60
    add-int v4, p2, v0

    iget-object v5, p0, Lgfj;->cID:[B

    add-int/lit8 v1, v1, 0x1

    aget-byte v1, v5, v1

    aput-byte v1, p1, v4

    .line 61
    add-int/lit8 v0, v0, 0x1

    .line 62
    iget v1, p0, Lgfj;->cIE:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lgfj;->cIE:I

    goto :goto_0

    .line 65
    :cond_0
    if-nez v0, :cond_1

    .line 68
    const-wide/16 v2, 0x14

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_1
    :goto_1
    return v0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public final release()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public final startRecording()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public final startRecording(Landroid/media/MediaSyncEvent;)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public final stop()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method
