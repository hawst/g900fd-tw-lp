.class public Lgmd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lceq;


# instance fields
.field private aTF:Z

.field private final anL:I

.field private final cOV:Ljava/io/InputStream;

.field private final cOW:Ljava/lang/String;

.field private final cOX:I

.field private final cOY:I

.field private final cOZ:I

.field private final cOx:Lenw;

.field private final cPa:I

.field private final cPb:I

.field protected cPc:Lgeo;

.field private final rV:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;IIIIII)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lgmd;->cOV:Ljava/io/InputStream;

    .line 57
    iput-object p2, p0, Lgmd;->cOW:Ljava/lang/String;

    .line 58
    iput p3, p0, Lgmd;->anL:I

    .line 59
    iput p4, p0, Lgmd;->cOX:I

    .line 60
    iput p5, p0, Lgmd;->cOY:I

    .line 61
    iput p6, p0, Lgmd;->cOZ:I

    .line 62
    iput p7, p0, Lgmd;->cPa:I

    .line 63
    iput p8, p0, Lgmd;->cPb:I

    .line 64
    iget v0, p0, Lgmd;->cOZ:I

    new-array v0, v0, [B

    iput-object v0, p0, Lgmd;->rV:[B

    .line 65
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Lgmd;->cOx:Lenw;

    .line 66
    return-void
.end method

.method private aHn()V
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lgmd;->aTF:Z

    if-nez v0, :cond_0

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmd;->aTF:Z

    .line 126
    iget-object v0, p0, Lgmd;->cPc:Lgeo;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 128
    :cond_0
    return-void
.end method


# virtual methods
.method public final Dr()Ljwv;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 70
    iget-object v1, p0, Lgmd;->cOx:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    .line 73
    :try_start_0
    iget-boolean v1, p0, Lgmd;->aTF:Z

    if-eqz v1, :cond_0

    .line 98
    :goto_0
    return-object v0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lgmd;->aHm()V

    .line 85
    iget v1, p0, Lgmd;->cPb:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lgmd;->cPc:Lgeo;

    invoke-virtual {v1}, Lgeo;->aFp()I

    move-result v1

    iget v2, p0, Lgmd;->anL:I

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lgmd;->cPb:I

    mul-int/2addr v2, v3

    if-le v1, v2, :cond_1

    .line 89
    invoke-direct {p0}, Lgmd;->aHn()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 103
    invoke-direct {p0}, Lgmd;->aHn()V

    .line 104
    new-instance v1, Leie;

    const v2, 0x2000c

    invoke-direct {v1, v0, v2}, Leie;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 93
    :cond_1
    :try_start_1
    iget-object v1, p0, Lgmd;->cPc:Lgeo;

    iget-object v2, p0, Lgmd;->rV:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lgmd;->rV:[B

    array-length v4, v4

    invoke-static {v1, v2, v3, v4}, Liso;->a(Ljava/io/InputStream;[BII)I

    move-result v1

    .line 94
    if-lez v1, :cond_2

    .line 95
    iget-object v0, p0, Lgmd;->rV:[B

    invoke-static {v0, v1}, Lcek;->b([BI)Ljwv;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_2
    invoke-direct {p0}, Lgmd;->aHn()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected aHm()V
    .locals 7

    .prologue
    .line 116
    iget-object v0, p0, Lgmd;->cPc:Lgeo;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lgeo;

    iget-object v1, p0, Lgmd;->cOV:Ljava/io/InputStream;

    iget-object v2, p0, Lgmd;->cOW:Ljava/lang/String;

    iget v3, p0, Lgmd;->anL:I

    iget v4, p0, Lgmd;->cOY:I

    iget v5, p0, Lgmd;->cPa:I

    iget v6, p0, Lgmd;->cOX:I

    invoke-direct/range {v0 .. v6}, Lgeo;-><init>(Ljava/io/InputStream;Ljava/lang/String;IIII)V

    iput-object v0, p0, Lgmd;->cPc:Lgeo;

    .line 121
    :cond_0
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lgmd;->cOx:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 111
    invoke-direct {p0}, Lgmd;->aHn()V

    .line 112
    return-void
.end method

.method public final yo()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    return v0
.end method
